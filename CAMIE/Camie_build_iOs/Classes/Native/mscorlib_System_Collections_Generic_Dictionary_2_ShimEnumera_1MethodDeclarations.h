﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Byte>
struct ShimEnumerator_t2995;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte>
struct Dictionary_2_t2983;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m15995_gshared (ShimEnumerator_t2995 * __this, Dictionary_2_t2983 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m15995(__this, ___host, method) (( void (*) (ShimEnumerator_t2995 *, Dictionary_2_t2983 *, const MethodInfo*))ShimEnumerator__ctor_m15995_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Byte>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m15996_gshared (ShimEnumerator_t2995 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m15996(__this, method) (( bool (*) (ShimEnumerator_t2995 *, const MethodInfo*))ShimEnumerator_MoveNext_m15996_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Byte>::get_Entry()
extern "C" DictionaryEntry_t1430  ShimEnumerator_get_Entry_m15997_gshared (ShimEnumerator_t2995 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m15997(__this, method) (( DictionaryEntry_t1430  (*) (ShimEnumerator_t2995 *, const MethodInfo*))ShimEnumerator_get_Entry_m15997_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Byte>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m15998_gshared (ShimEnumerator_t2995 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m15998(__this, method) (( Object_t * (*) (ShimEnumerator_t2995 *, const MethodInfo*))ShimEnumerator_get_Key_m15998_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Byte>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m15999_gshared (ShimEnumerator_t2995 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m15999(__this, method) (( Object_t * (*) (ShimEnumerator_t2995 *, const MethodInfo*))ShimEnumerator_get_Value_m15999_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Byte>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m16000_gshared (ShimEnumerator_t2995 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m16000(__this, method) (( Object_t * (*) (ShimEnumerator_t2995 *, const MethodInfo*))ShimEnumerator_get_Current_m16000_gshared)(__this, method)
