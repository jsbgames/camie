﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>
struct InternalEnumerator_1_t3478;
// System.Object
struct Object_t;
// System.Reflection.Emit.ConstructorBuilder
struct ConstructorBuilder_t1847;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22312(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3478 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22313(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3478 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::Dispose()
#define InternalEnumerator_1_Dispose_m22314(__this, method) (( void (*) (InternalEnumerator_1_t3478 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22315(__this, method) (( bool (*) (InternalEnumerator_1_t3478 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::get_Current()
#define InternalEnumerator_1_get_Current_m22316(__this, method) (( ConstructorBuilder_t1847 * (*) (InternalEnumerator_1_t3478 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
