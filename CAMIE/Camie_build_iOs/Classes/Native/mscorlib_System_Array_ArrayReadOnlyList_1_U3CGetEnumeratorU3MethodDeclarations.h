﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t3452;
// System.Object
struct Object_t;

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m22197_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3452 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m22197(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3452 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m22197_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m22198_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3452 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m22198(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3452 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m22198_gshared)(__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m22199_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3452 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m22199(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3452 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m22199_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m22200_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3452 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m22200(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t3452 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m22200_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m22201_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3452 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m22201(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3452 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m22201_gshared)(__this, method)
