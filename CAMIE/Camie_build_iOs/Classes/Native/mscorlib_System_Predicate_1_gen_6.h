﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t55;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Material>
struct  Predicate_1_t2876  : public MulticastDelegate_t549
{
};
