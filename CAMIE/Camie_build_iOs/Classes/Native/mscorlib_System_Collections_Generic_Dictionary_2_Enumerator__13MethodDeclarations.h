﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>
struct Enumerator_t3012;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t247;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>
struct Dictionary_2_t389;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"
#define Enumerator__ctor_m16242(__this, ___dictionary, method) (( void (*) (Enumerator_t3012 *, Dictionary_2_t389 *, const MethodInfo*))Enumerator__ctor_m13691_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16243(__this, method) (( Object_t * (*) (Enumerator_t3012 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13692_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16244(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3012 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13693_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16245(__this, method) (( Object_t * (*) (Enumerator_t3012 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13694_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16246(__this, method) (( Object_t * (*) (Enumerator_t3012 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13695_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::MoveNext()
#define Enumerator_MoveNext_m16247(__this, method) (( bool (*) (Enumerator_t3012 *, const MethodInfo*))Enumerator_MoveNext_m13696_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::get_Current()
#define Enumerator_get_Current_m16248(__this, method) (( KeyValuePair_2_t3010  (*) (Enumerator_t3012 *, const MethodInfo*))Enumerator_get_Current_m13697_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16249(__this, method) (( String_t* (*) (Enumerator_t3012 *, const MethodInfo*))Enumerator_get_CurrentKey_m13698_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16250(__this, method) (( AudioSource_t247 * (*) (Enumerator_t3012 *, const MethodInfo*))Enumerator_get_CurrentValue_m13699_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::VerifyState()
#define Enumerator_VerifyState_m16251(__this, method) (( void (*) (Enumerator_t3012 *, const MethodInfo*))Enumerator_VerifyState_m13700_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16252(__this, method) (( void (*) (Enumerator_t3012 *, const MethodInfo*))Enumerator_VerifyCurrent_m13701_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.AudioSource>::Dispose()
#define Enumerator_Dispose_m16253(__this, method) (( void (*) (Enumerator_t3012 *, const MethodInfo*))Enumerator_Dispose_m13702_gshared)(__this, method)
