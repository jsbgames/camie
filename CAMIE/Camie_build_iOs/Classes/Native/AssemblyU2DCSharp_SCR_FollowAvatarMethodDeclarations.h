﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_FollowAvatar
struct SCR_FollowAvatar_t352;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.Transform
struct Transform_t1;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void SCR_FollowAvatar::.ctor()
extern "C" void SCR_FollowAvatar__ctor_m1300 (SCR_FollowAvatar_t352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FollowAvatar::Start()
extern "C" void SCR_FollowAvatar_Start_m1301 (SCR_FollowAvatar_t352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FollowAvatar::DeleteExtraCameras()
extern "C" void SCR_FollowAvatar_DeleteExtraCameras_m1302 (SCR_FollowAvatar_t352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FollowAvatar::SetBounds()
extern "C" void SCR_FollowAvatar_SetBounds_m1303 (SCR_FollowAvatar_t352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_FollowAvatar::ZoomInOn(UnityEngine.Transform)
extern "C" Object_t * SCR_FollowAvatar_ZoomInOn_m1304 (SCR_FollowAvatar_t352 * __this, Transform_t1 * ___zoomTo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_FollowAvatar::FollowAvatar()
extern "C" Object_t * SCR_FollowAvatar_FollowAvatar_m1305 (SCR_FollowAvatar_t352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SCR_FollowAvatar::BindPosition(UnityEngine.Vector3)
extern "C" Vector3_t4  SCR_FollowAvatar_BindPosition_m1306 (SCR_FollowAvatar_t352 * __this, Vector3_t4  ___newPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
