﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// EventSystemChecker
struct EventSystemChecker_t309;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void EventSystemChecker::.ctor()
extern "C" void EventSystemChecker__ctor_m1147 (EventSystemChecker_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EventSystemChecker::Start()
extern "C" Object_t * EventSystemChecker_Start_m1148 (EventSystemChecker_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
