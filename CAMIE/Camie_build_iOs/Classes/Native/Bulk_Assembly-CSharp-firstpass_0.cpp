﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <Module>
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// UnityStandardAssets._2D.Camera2DFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Camera.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets._2D.Camera2DFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_CameraMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"


// System.Void UnityStandardAssets._2D.Camera2DFollow::.ctor()
extern "C" void Camera2DFollow__ctor_m0 (Camera2DFollow_t2 * __this, const MethodInfo* method)
{
	{
		__this->___damping_3 = (1.0f);
		__this->___lookAheadFactor_4 = (3.0f);
		__this->___lookAheadReturnSpeed_5 = (0.5f);
		__this->___lookAheadMoveThreshold_6 = (0.1f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.Camera2DFollow::Start()
extern "C" void Camera2DFollow_Start_m1 (Camera2DFollow_t2 * __this, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	{
		Transform_t1 * L_0 = (__this->___target_2);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		__this->___m_LastTargetPosition_8 = L_1;
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4  L_3 = Transform_get_position_m593(L_2, /*hidden argument*/NULL);
		Transform_t1 * L_4 = (__this->___target_2);
		NullCheck(L_4);
		Vector3_t4  L_5 = Transform_get_position_m593(L_4, /*hidden argument*/NULL);
		Vector3_t4  L_6 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = ((&V_0)->___z_3);
		__this->___m_OffsetZ_7 = L_7;
		Transform_t1 * L_8 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_parent_m596(L_8, (Transform_t1 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.Camera2DFollow::Update()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void Camera2DFollow_Update_m2 (Camera2DFollow_t2 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Vector3_t4  V_2 = {0};
	Vector3_t4  V_3 = {0};
	Vector3_t4  V_4 = {0};
	{
		Transform_t1 * L_0 = (__this->___target_2);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		Vector3_t4  L_2 = (__this->___m_LastTargetPosition_8);
		Vector3_t4  L_3 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_4 = L_3;
		float L_4 = ((&V_4)->___x_1);
		V_0 = L_4;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_6 = fabsf(L_5);
		float L_7 = (__this->___lookAheadMoveThreshold_6);
		V_1 = ((((float)L_6) > ((float)L_7))? 1 : 0);
		bool L_8 = V_1;
		if (!L_8)
		{
			goto IL_005b;
		}
	}
	{
		float L_9 = (__this->___lookAheadFactor_4);
		Vector3_t4  L_10 = Vector3_get_right_m597(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_11 = Vector3_op_Multiply_m598(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_13 = Mathf_Sign_m599(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		Vector3_t4  L_14 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
		__this->___m_LookAheadPos_10 = L_14;
		goto IL_007d;
	}

IL_005b:
	{
		Vector3_t4  L_15 = (__this->___m_LookAheadPos_10);
		Vector3_t4  L_16 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_17 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = (__this->___lookAheadReturnSpeed_5);
		Vector3_t4  L_19 = Vector3_MoveTowards_m603(NULL /*static, unused*/, L_15, L_16, ((float)((float)L_17*(float)L_18)), /*hidden argument*/NULL);
		__this->___m_LookAheadPos_10 = L_19;
	}

IL_007d:
	{
		Transform_t1 * L_20 = (__this->___target_2);
		NullCheck(L_20);
		Vector3_t4  L_21 = Transform_get_position_m593(L_20, /*hidden argument*/NULL);
		Vector3_t4  L_22 = (__this->___m_LookAheadPos_10);
		Vector3_t4  L_23 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		Vector3_t4  L_24 = Vector3_get_forward_m605(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_25 = (__this->___m_OffsetZ_7);
		Vector3_t4  L_26 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		Vector3_t4  L_27 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_23, L_26, /*hidden argument*/NULL);
		V_2 = L_27;
		Transform_t1 * L_28 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4  L_29 = Transform_get_position_m593(L_28, /*hidden argument*/NULL);
		Vector3_t4  L_30 = V_2;
		Vector3_t4 * L_31 = &(__this->___m_CurrentVelocity_9);
		float L_32 = (__this->___damping_3);
		Vector3_t4  L_33 = Vector3_SmoothDamp_m606(NULL /*static, unused*/, L_29, L_30, L_31, L_32, /*hidden argument*/NULL);
		V_3 = L_33;
		Transform_t1 * L_34 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4  L_35 = V_3;
		NullCheck(L_34);
		Transform_set_position_m607(L_34, L_35, /*hidden argument*/NULL);
		Transform_t1 * L_36 = (__this->___target_2);
		NullCheck(L_36);
		Vector3_t4  L_37 = Transform_get_position_m593(L_36, /*hidden argument*/NULL);
		__this->___m_LastTargetPosition_8 = L_37;
		return;
	}
}
// UnityStandardAssets._2D.CameraFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Camera_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets._2D.CameraFollow
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Camera_0MethodDeclarations.h"

// System.String
#include "mscorlib_System_String.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"


// System.Void UnityStandardAssets._2D.CameraFollow::.ctor()
extern "C" void CameraFollow__ctor_m3 (CameraFollow_t5 * __this, const MethodInfo* method)
{
	{
		__this->___xMargin_2 = (1.0f);
		__this->___yMargin_3 = (1.0f);
		__this->___xSmooth_4 = (8.0f);
		__this->___ySmooth_5 = (8.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.CameraFollow::Awake()
extern "C" void CameraFollow_Awake_m4 (CameraFollow_t5 * __this, const MethodInfo* method)
{
	{
		GameObject_t78 * L_0 = GameObject_FindGameObjectWithTag_m608(NULL /*static, unused*/, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1 * L_1 = GameObject_get_transform_m609(L_0, /*hidden argument*/NULL);
		__this->___m_Player_8 = L_1;
		return;
	}
}
// System.Boolean UnityStandardAssets._2D.CameraFollow::CheckXMargin()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" bool CameraFollow_CheckXMargin_m5 (CameraFollow_t5 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ((&V_0)->___x_1);
		Transform_t1 * L_3 = (__this->___m_Player_8);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = ((&V_1)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_6 = fabsf(((float)((float)L_2-(float)L_5)));
		float L_7 = (__this->___xMargin_2);
		return ((((float)L_6) > ((float)L_7))? 1 : 0);
	}
}
// System.Boolean UnityStandardAssets._2D.CameraFollow::CheckYMargin()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" bool CameraFollow_CheckYMargin_m6 (CameraFollow_t5 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ((&V_0)->___y_2);
		Transform_t1 * L_3 = (__this->___m_Player_8);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = ((&V_1)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_6 = fabsf(((float)((float)L_2-(float)L_5)));
		float L_7 = (__this->___yMargin_3);
		return ((((float)L_6) > ((float)L_7))? 1 : 0);
	}
}
// System.Void UnityStandardAssets._2D.CameraFollow::Update()
extern "C" void CameraFollow_Update_m7 (CameraFollow_t5 * __this, const MethodInfo* method)
{
	{
		CameraFollow_TrackPlayer_m8(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.CameraFollow::TrackPlayer()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void CameraFollow_TrackPlayer_m8 (CameraFollow_t5 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t4  V_2 = {0};
	Vector3_t4  V_3 = {0};
	Vector3_t4  V_4 = {0};
	Vector3_t4  V_5 = {0};
	Vector3_t4  V_6 = {0};
	Vector3_t4  V_7 = {0};
	Vector3_t4  V_8 = {0};
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		float L_2 = ((&V_2)->___x_1);
		V_0 = L_2;
		Transform_t1 * L_3 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		float L_5 = ((&V_3)->___y_2);
		V_1 = L_5;
		bool L_6 = CameraFollow_CheckXMargin_m5(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_006d;
		}
	}
	{
		Transform_t1 * L_7 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4  L_8 = Transform_get_position_m593(L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = ((&V_4)->___x_1);
		Transform_t1 * L_10 = (__this->___m_Player_8);
		NullCheck(L_10);
		Vector3_t4  L_11 = Transform_get_position_m593(L_10, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = ((&V_5)->___x_1);
		float L_13 = (__this->___xSmooth_4);
		float L_14 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_15 = Mathf_Lerp_m610(NULL /*static, unused*/, L_9, L_12, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		V_0 = L_15;
	}

IL_006d:
	{
		bool L_16 = CameraFollow_CheckYMargin_m6(__this, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b2;
		}
	}
	{
		Transform_t1 * L_17 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t4  L_18 = Transform_get_position_m593(L_17, /*hidden argument*/NULL);
		V_6 = L_18;
		float L_19 = ((&V_6)->___y_2);
		Transform_t1 * L_20 = (__this->___m_Player_8);
		NullCheck(L_20);
		Vector3_t4  L_21 = Transform_get_position_m593(L_20, /*hidden argument*/NULL);
		V_7 = L_21;
		float L_22 = ((&V_7)->___y_2);
		float L_23 = (__this->___ySmooth_5);
		float L_24 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Lerp_m610(NULL /*static, unused*/, L_19, L_22, ((float)((float)L_23*(float)L_24)), /*hidden argument*/NULL);
		V_1 = L_25;
	}

IL_00b2:
	{
		float L_26 = V_0;
		Vector2_t6 * L_27 = &(__this->___minXAndY_7);
		float L_28 = (L_27->___x_1);
		Vector2_t6 * L_29 = &(__this->___maxXAndY_6);
		float L_30 = (L_29->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_31 = Mathf_Clamp_m611(NULL /*static, unused*/, L_26, L_28, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		float L_32 = V_1;
		Vector2_t6 * L_33 = &(__this->___minXAndY_7);
		float L_34 = (L_33->___y_2);
		Vector2_t6 * L_35 = &(__this->___maxXAndY_6);
		float L_36 = (L_35->___y_2);
		float L_37 = Mathf_Clamp_m611(NULL /*static, unused*/, L_32, L_34, L_36, /*hidden argument*/NULL);
		V_1 = L_37;
		Transform_t1 * L_38 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		float L_39 = V_0;
		float L_40 = V_1;
		Transform_t1 * L_41 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t4  L_42 = Transform_get_position_m593(L_41, /*hidden argument*/NULL);
		V_8 = L_42;
		float L_43 = ((&V_8)->___z_3);
		Vector3_t4  L_44 = {0};
		Vector3__ctor_m612(&L_44, L_39, L_40, L_43, /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_set_position_m607(L_38, L_44, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets._2D.Platformer2DUserControl
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Platfo.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets._2D.Platformer2DUserControl
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_PlatfoMethodDeclarations.h"

// UnityStandardAssets._2D.PlatformerCharacter2D
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Platfo_0.h"
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_4MethodDeclarations.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
// UnityStandardAssets._2D.PlatformerCharacter2D
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Platfo_0MethodDeclarations.h"
struct Component_t219;
struct PlatformerCharacter2D_t7;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Component_t219;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m614_gshared (Component_t219 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m614(__this, method) (( Object_t * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityStandardAssets._2D.PlatformerCharacter2D>()
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets._2D.PlatformerCharacter2D>()
#define Component_GetComponent_TisPlatformerCharacter2D_t7_m613(__this, method) (( PlatformerCharacter2D_t7 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets._2D.Platformer2DUserControl::.ctor()
extern "C" void Platformer2DUserControl__ctor_m9 (Platformer2DUserControl_t8 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.Platformer2DUserControl::Awake()
extern const MethodInfo* Component_GetComponent_TisPlatformerCharacter2D_t7_m613_MethodInfo_var;
extern "C" void Platformer2DUserControl_Awake_m10 (Platformer2DUserControl_t8 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisPlatformerCharacter2D_t7_m613_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlatformerCharacter2D_t7 * L_0 = Component_GetComponent_TisPlatformerCharacter2D_t7_m613(__this, /*hidden argument*/Component_GetComponent_TisPlatformerCharacter2D_t7_m613_MethodInfo_var);
		__this->___m_Character_2 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets._2D.Platformer2DUserControl::Update()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void Platformer2DUserControl_Update_m11 (Platformer2DUserControl_t8 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_Jump_3);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		bool L_1 = CrossPlatformInputManager_GetButtonDown_m104(NULL /*static, unused*/, (String_t*) &_stringLiteral2, /*hidden argument*/NULL);
		__this->___m_Jump_3 = L_1;
	}

IL_001b:
	{
		return;
	}
}
// System.Void UnityStandardAssets._2D.Platformer2DUserControl::FixedUpdate()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void Platformer2DUserControl_FixedUpdate_m12 (Platformer2DUserControl_t8 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m615(NULL /*static, unused*/, ((int32_t)306), /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		float L_1 = CrossPlatformInputManager_GetAxis_m100(NULL /*static, unused*/, (String_t*) &_stringLiteral3, /*hidden argument*/NULL);
		V_1 = L_1;
		PlatformerCharacter2D_t7 * L_2 = (__this->___m_Character_2);
		float L_3 = V_1;
		bool L_4 = V_0;
		bool L_5 = (__this->___m_Jump_3);
		NullCheck(L_2);
		PlatformerCharacter2D_Move_m16(L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		__this->___m_Jump_3 = 0;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.Collider2D
#include "UnityEngine_UnityEngine_Collider2D.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
// UnityEngine.Physics2D
#include "UnityEngine_UnityEngine_Physics2DMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2DMethodDeclarations.h"
struct Component_t219;
struct Animator_t9;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t9_m616(__this, method) (( Animator_t9 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct Rigidbody2D_t10;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t10_m617(__this, method) (( Rigidbody2D_t10 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets._2D.PlatformerCharacter2D::.ctor()
extern "C" void PlatformerCharacter2D__ctor_m13 (PlatformerCharacter2D_t7 * __this, const MethodInfo* method)
{
	{
		__this->___m_MaxSpeed_4 = (10.0f);
		__this->___m_JumpForce_5 = (400.0f);
		__this->___m_CrouchSpeed_6 = (0.36f);
		__this->___m_FacingRight_14 = 1;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Awake()
extern const MethodInfo* Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t10_m617_MethodInfo_var;
extern "C" void PlatformerCharacter2D_Awake_m14 (PlatformerCharacter2D_t7 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		Component_GetComponent_TisRigidbody2D_t10_m617_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1 * L_1 = Transform_Find_m618(L_0, (String_t*) &_stringLiteral4, /*hidden argument*/NULL);
		__this->___m_GroundCheck_9 = L_1;
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t1 * L_3 = Transform_Find_m618(L_2, (String_t*) &_stringLiteral5, /*hidden argument*/NULL);
		__this->___m_CeilingCheck_11 = L_3;
		Animator_t9 * L_4 = Component_GetComponent_TisAnimator_t9_m616(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var);
		__this->___m_Anim_12 = L_4;
		Rigidbody2D_t10 * L_5 = Component_GetComponent_TisRigidbody2D_t10_m617(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t10_m617_MethodInfo_var);
		__this->___m_Rigidbody2D_13 = L_5;
		return;
	}
}
// System.Void UnityStandardAssets._2D.PlatformerCharacter2D::FixedUpdate()
extern TypeInfo* Physics2D_t222_il2cpp_TypeInfo_var;
extern "C" void PlatformerCharacter2D_FixedUpdate_m15 (PlatformerCharacter2D_t7 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t222_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	Collider2DU5BU5D_t221* V_0 = {0};
	int32_t V_1 = 0;
	Vector2_t6  V_2 = {0};
	{
		__this->___m_Grounded_10 = 0;
		Transform_t1 * L_0 = (__this->___m_GroundCheck_9);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		Vector2_t6  L_2 = Vector2_op_Implicit_m619(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		LayerMask_t11  L_3 = (__this->___m_WhatIsGround_8);
		int32_t L_4 = LayerMask_op_Implicit_m620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t222_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t221* L_5 = Physics2D_OverlapCircleAll_m621(NULL /*static, unused*/, L_2, (0.2f), L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		V_1 = 0;
		goto IL_0057;
	}

IL_0034:
	{
		Collider2DU5BU5D_t221* L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck((*(Collider2D_t214 **)(Collider2D_t214 **)SZArrayLdElema(L_6, L_8)));
		GameObject_t78 * L_9 = Component_get_gameObject_m622((*(Collider2D_t214 **)(Collider2D_t214 **)SZArrayLdElema(L_6, L_8)), /*hidden argument*/NULL);
		GameObject_t78 * L_10 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		bool L_11 = Object_op_Inequality_m623(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0053;
		}
	}
	{
		__this->___m_Grounded_10 = 1;
	}

IL_0053:
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_13 = V_1;
		Collider2DU5BU5D_t221* L_14 = V_0;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)(((Array_t *)L_14)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		Animator_t9 * L_15 = (__this->___m_Anim_12);
		bool L_16 = (__this->___m_Grounded_10);
		NullCheck(L_15);
		Animator_SetBool_m624(L_15, (String_t*) &_stringLiteral6, L_16, /*hidden argument*/NULL);
		Animator_t9 * L_17 = (__this->___m_Anim_12);
		Rigidbody2D_t10 * L_18 = (__this->___m_Rigidbody2D_13);
		NullCheck(L_18);
		Vector2_t6  L_19 = Rigidbody2D_get_velocity_m625(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		float L_20 = ((&V_2)->___y_2);
		NullCheck(L_17);
		Animator_SetFloat_m626(L_17, (String_t*) &_stringLiteral7, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Move(System.Single,System.Boolean,System.Boolean)
extern TypeInfo* Physics2D_t222_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void PlatformerCharacter2D_Move_m16 (PlatformerCharacter2D_t7 * __this, float ___move, bool ___crouch, bool ___jump, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t222_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6  V_0 = {0};
	float G_B9_0 = 0.0f;
	{
		bool L_0 = ___crouch;
		if (L_0)
		{
			goto IL_004d;
		}
	}
	{
		Animator_t9 * L_1 = (__this->___m_Anim_12);
		NullCheck(L_1);
		bool L_2 = Animator_GetBool_m627(L_1, (String_t*) &_stringLiteral8, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004d;
		}
	}
	{
		Transform_t1 * L_3 = (__this->___m_CeilingCheck_11);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		Vector2_t6  L_5 = Vector2_op_Implicit_m619(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		LayerMask_t11  L_6 = (__this->___m_WhatIsGround_8);
		int32_t L_7 = LayerMask_op_Implicit_m620(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t222_il2cpp_TypeInfo_var);
		Collider2D_t214 * L_8 = Physics2D_OverlapCircle_m628(NULL /*static, unused*/, L_5, (0.01f), L_7, /*hidden argument*/NULL);
		bool L_9 = Object_op_Implicit_m629(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004d;
		}
	}
	{
		___crouch = 1;
	}

IL_004d:
	{
		Animator_t9 * L_10 = (__this->___m_Anim_12);
		bool L_11 = ___crouch;
		NullCheck(L_10);
		Animator_SetBool_m624(L_10, (String_t*) &_stringLiteral8, L_11, /*hidden argument*/NULL);
		bool L_12 = (__this->___m_Grounded_10);
		if (L_12)
		{
			goto IL_0074;
		}
	}
	{
		bool L_13 = (__this->___m_AirControl_7);
		if (!L_13)
		{
			goto IL_0108;
		}
	}

IL_0074:
	{
		bool L_14 = ___crouch;
		if (!L_14)
		{
			goto IL_0087;
		}
	}
	{
		float L_15 = ___move;
		float L_16 = (__this->___m_CrouchSpeed_6);
		G_B9_0 = ((float)((float)L_15*(float)L_16));
		goto IL_0088;
	}

IL_0087:
	{
		float L_17 = ___move;
		G_B9_0 = L_17;
	}

IL_0088:
	{
		___move = G_B9_0;
		Animator_t9 * L_18 = (__this->___m_Anim_12);
		float L_19 = ___move;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_20 = fabsf(L_19);
		NullCheck(L_18);
		Animator_SetFloat_m626(L_18, (String_t*) &_stringLiteral9, L_20, /*hidden argument*/NULL);
		Rigidbody2D_t10 * L_21 = (__this->___m_Rigidbody2D_13);
		float L_22 = ___move;
		float L_23 = (__this->___m_MaxSpeed_4);
		Rigidbody2D_t10 * L_24 = (__this->___m_Rigidbody2D_13);
		NullCheck(L_24);
		Vector2_t6  L_25 = Rigidbody2D_get_velocity_m625(L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		float L_26 = ((&V_0)->___y_2);
		Vector2_t6  L_27 = {0};
		Vector2__ctor_m630(&L_27, ((float)((float)L_22*(float)L_23)), L_26, /*hidden argument*/NULL);
		NullCheck(L_21);
		Rigidbody2D_set_velocity_m631(L_21, L_27, /*hidden argument*/NULL);
		float L_28 = ___move;
		if ((!(((float)L_28) > ((float)(0.0f)))))
		{
			goto IL_00ec;
		}
	}
	{
		bool L_29 = (__this->___m_FacingRight_14);
		if (L_29)
		{
			goto IL_00ec;
		}
	}
	{
		PlatformerCharacter2D_Flip_m17(__this, /*hidden argument*/NULL);
		goto IL_0108;
	}

IL_00ec:
	{
		float L_30 = ___move;
		if ((!(((float)L_30) < ((float)(0.0f)))))
		{
			goto IL_0108;
		}
	}
	{
		bool L_31 = (__this->___m_FacingRight_14);
		if (!L_31)
		{
			goto IL_0108;
		}
	}
	{
		PlatformerCharacter2D_Flip_m17(__this, /*hidden argument*/NULL);
	}

IL_0108:
	{
		bool L_32 = (__this->___m_Grounded_10);
		if (!L_32)
		{
			goto IL_0161;
		}
	}
	{
		bool L_33 = ___jump;
		if (!L_33)
		{
			goto IL_0161;
		}
	}
	{
		Animator_t9 * L_34 = (__this->___m_Anim_12);
		NullCheck(L_34);
		bool L_35 = Animator_GetBool_m627(L_34, (String_t*) &_stringLiteral6, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0161;
		}
	}
	{
		__this->___m_Grounded_10 = 0;
		Animator_t9 * L_36 = (__this->___m_Anim_12);
		NullCheck(L_36);
		Animator_SetBool_m624(L_36, (String_t*) &_stringLiteral6, 0, /*hidden argument*/NULL);
		Rigidbody2D_t10 * L_37 = (__this->___m_Rigidbody2D_13);
		float L_38 = (__this->___m_JumpForce_5);
		Vector2_t6  L_39 = {0};
		Vector2__ctor_m630(&L_39, (0.0f), L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		Rigidbody2D_AddForce_m632(L_37, L_39, /*hidden argument*/NULL);
	}

IL_0161:
	{
		return;
	}
}
// System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Flip()
extern "C" void PlatformerCharacter2D_Flip_m17 (PlatformerCharacter2D_t7 * __this, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	{
		bool L_0 = (__this->___m_FacingRight_14);
		__this->___m_FacingRight_14 = ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		Transform_t1 * L_1 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4  L_2 = Transform_get_localScale_m633(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4 * L_3 = (&V_0);
		float L_4 = (L_3->___x_1);
		L_3->___x_1 = ((float)((float)L_4*(float)(-1.0f)));
		Transform_t1 * L_5 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4  L_6 = V_0;
		NullCheck(L_5);
		Transform_set_localScale_m634(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets._2D.Restarter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_Restar.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets._2D.Restarter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets__2D_RestarMethodDeclarations.h"

// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"


// System.Void UnityStandardAssets._2D.Restarter::.ctor()
extern "C" void Restarter__ctor_m18 (Restarter_t12 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.Restarter::OnTriggerEnter2D(UnityEngine.Collider2D)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Restarter_OnTriggerEnter2D_m19 (Restarter_t12 * __this, Collider2D_t214 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider2D_t214 * L_0 = ___other;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m635(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m636(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_3 = Application_get_loadedLevelName_m637(NULL /*static, unused*/, /*hidden argument*/NULL);
		Application_LoadLevel_m638(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Ab.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_AbMethodDeclarations.h"



// UnityStandardAssets.Cameras.AbstractTargetFollower
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Ab_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Cameras.AbstractTargetFollower
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Ab_0MethodDeclarations.h"

// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"
struct Component_t219;
struct Rigidbody_t14;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t14_m639(__this, method) (( Rigidbody_t14 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::.ctor()
extern "C" void AbstractTargetFollower__ctor_m20 (AbstractTargetFollower_t15 * __this, const MethodInfo* method)
{
	{
		__this->___m_AutoTargetPlayer_3 = 1;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::Start()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t14_m639_MethodInfo_var;
extern "C" void AbstractTargetFollower_Start_m21 (AbstractTargetFollower_t15 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRigidbody_t14_m639_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_AutoTargetPlayer_3);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		AbstractTargetFollower_FindAndTargetPlayer_m25(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		Transform_t1 * L_1 = (__this->___m_Target_2);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		return;
	}

IL_0023:
	{
		Transform_t1 * L_3 = (__this->___m_Target_2);
		NullCheck(L_3);
		Rigidbody_t14 * L_4 = Component_GetComponent_TisRigidbody_t14_m639(L_3, /*hidden argument*/Component_GetComponent_TisRigidbody_t14_m639_MethodInfo_var);
		__this->___targetRigidbody_5 = L_4;
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FixedUpdate()
extern "C" void AbstractTargetFollower_FixedUpdate_m22 (AbstractTargetFollower_t15 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_AutoTargetPlayer_3);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t1 * L_1 = (__this->___m_Target_2);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		Transform_t1 * L_3 = (__this->___m_Target_2);
		NullCheck(L_3);
		GameObject_t78 * L_4 = Component_get_gameObject_m622(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_get_activeSelf_m641(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0037;
		}
	}

IL_0031:
	{
		AbstractTargetFollower_FindAndTargetPlayer_m25(__this, /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_6 = (__this->___m_UpdateType_4);
		if (L_6)
		{
			goto IL_004d;
		}
	}
	{
		float L_7 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(5 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single) */, __this, L_7);
	}

IL_004d:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::LateUpdate()
extern "C" void AbstractTargetFollower_LateUpdate_m23 (AbstractTargetFollower_t15 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_AutoTargetPlayer_3);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t1 * L_1 = (__this->___m_Target_2);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		Transform_t1 * L_3 = (__this->___m_Target_2);
		NullCheck(L_3);
		GameObject_t78 * L_4 = Component_get_gameObject_m622(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_get_activeSelf_m641(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0037;
		}
	}

IL_0031:
	{
		AbstractTargetFollower_FindAndTargetPlayer_m25(__this, /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_6 = (__this->___m_UpdateType_4);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_004e;
		}
	}
	{
		float L_7 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(5 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single) */, __this, L_7);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::ManualUpdate()
extern "C" void AbstractTargetFollower_ManualUpdate_m24 (AbstractTargetFollower_t15 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_AutoTargetPlayer_3);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t1 * L_1 = (__this->___m_Target_2);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		Transform_t1 * L_3 = (__this->___m_Target_2);
		NullCheck(L_3);
		GameObject_t78 * L_4 = Component_get_gameObject_m622(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_get_activeSelf_m641(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0037;
		}
	}

IL_0031:
	{
		AbstractTargetFollower_FindAndTargetPlayer_m25(__this, /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_6 = (__this->___m_UpdateType_4);
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_004e;
		}
	}
	{
		float L_7 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(5 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single) */, __this, L_7);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single)
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FindAndTargetPlayer()
extern "C" void AbstractTargetFollower_FindAndTargetPlayer_m25 (AbstractTargetFollower_t15 * __this, const MethodInfo* method)
{
	GameObject_t78 * V_0 = {0};
	{
		GameObject_t78 * L_0 = GameObject_FindGameObjectWithTag_m608(NULL /*static, unused*/, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t78 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m629(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t78 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t1 * L_4 = GameObject_get_transform_m609(L_3, /*hidden argument*/NULL);
		VirtActionInvoker1< Transform_t1 * >::Invoke(6 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform) */, __this, L_4);
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform)
extern "C" void AbstractTargetFollower_SetTarget_m26 (AbstractTargetFollower_t15 * __this, Transform_t1 * ___newTransform, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = ___newTransform;
		__this->___m_Target_2 = L_0;
		return;
	}
}
// UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::get_Target()
extern "C" Transform_t1 * AbstractTargetFollower_get_Target_m27 (AbstractTargetFollower_t15 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = (__this->___m_Target_2);
		return L_0;
	}
}
// UnityStandardAssets.Cameras.AutoCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Au.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Cameras.AutoCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_AuMethodDeclarations.h"

// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityStandardAssets.Cameras.PivotBasedCameraRig
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_PiMethodDeclarations.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"


// System.Void UnityStandardAssets.Cameras.AutoCam::.ctor()
extern "C" void AutoCam__ctor_m28 (AutoCam_t16 * __this, const MethodInfo* method)
{
	{
		__this->___m_MoveSpeed_9 = (3.0f);
		__this->___m_TurnSpeed_10 = (1.0f);
		__this->___m_RollSpeed_11 = (0.2f);
		__this->___m_FollowTilt_13 = 1;
		__this->___m_SpinTurnLimit_14 = (90.0f);
		__this->___m_TargetVelocityLowerLimit_15 = (4.0f);
		__this->___m_SmoothTurnTime_16 = (0.2f);
		Vector3_t4  L_0 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_RollUp_20 = L_0;
		PivotBasedCameraRig__ctor_m41(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AutoCam::FollowTarget(System.Single)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void AutoCam_FollowTarget_m29 (AutoCam_t16 * __this, float ___deltaTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Quaternion_t19  V_6 = {0};
	Vector3_t4  V_7 = {0};
	Vector3_t4  V_8 = {0};
	float G_B13_0 = 0.0f;
	AutoCam_t16 * G_B24_0 = {0};
	AutoCam_t16 * G_B23_0 = {0};
	Vector3_t4  G_B25_0 = {0};
	AutoCam_t16 * G_B25_1 = {0};
	{
		float L_0 = ___deltaTime;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_001c;
		}
	}
	{
		Transform_t1 * L_1 = (((AbstractTargetFollower_t15 *)__this)->___m_Target_2);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}

IL_001c:
	{
		return;
	}

IL_001d:
	{
		Transform_t1 * L_3 = (((AbstractTargetFollower_t15 *)__this)->___m_Target_2);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_forward_m643(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Transform_t1 * L_5 = (((AbstractTargetFollower_t15 *)__this)->___m_Target_2);
		NullCheck(L_5);
		Vector3_t4  L_6 = Transform_get_up_m644(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		bool L_7 = (__this->___m_FollowVelocity_12);
		if (!L_7)
		{
			goto IL_00b6;
		}
	}
	{
		bool L_8 = Application_get_isPlaying_m645(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00b6;
		}
	}
	{
		Rigidbody_t14 * L_9 = (((AbstractTargetFollower_t15 *)__this)->___targetRigidbody_5);
		NullCheck(L_9);
		Vector3_t4  L_10 = Rigidbody_get_velocity_m646(L_9, /*hidden argument*/NULL);
		V_7 = L_10;
		float L_11 = Vector3_get_magnitude_m647((&V_7), /*hidden argument*/NULL);
		float L_12 = (__this->___m_TargetVelocityLowerLimit_15);
		if ((!(((float)L_11) > ((float)L_12))))
		{
			goto IL_0089;
		}
	}
	{
		Rigidbody_t14 * L_13 = (((AbstractTargetFollower_t15 *)__this)->___targetRigidbody_5);
		NullCheck(L_13);
		Vector3_t4  L_14 = Rigidbody_get_velocity_m646(L_13, /*hidden argument*/NULL);
		V_8 = L_14;
		Vector3_t4  L_15 = Vector3_get_normalized_m648((&V_8), /*hidden argument*/NULL);
		V_0 = L_15;
		Vector3_t4  L_16 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_16;
		goto IL_008f;
	}

IL_0089:
	{
		Vector3_t4  L_17 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_17;
	}

IL_008f:
	{
		float L_18 = (__this->___m_CurrentTurnAmount_18);
		float* L_19 = &(__this->___m_TurnSpeedVelocityChange_19);
		float L_20 = (__this->___m_SmoothTurnTime_16);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_21 = Mathf_SmoothDamp_m649(NULL /*static, unused*/, L_18, (1.0f), L_19, L_20, /*hidden argument*/NULL);
		__this->___m_CurrentTurnAmount_18 = L_21;
		goto IL_0175;
	}

IL_00b6:
	{
		float L_22 = ((&V_0)->___x_1);
		float L_23 = ((&V_0)->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_24 = atan2f(L_22, L_23);
		V_2 = ((float)((float)L_24*(float)(57.29578f)));
		float L_25 = (__this->___m_SpinTurnLimit_14);
		if ((!(((float)L_25) > ((float)(0.0f)))))
		{
			goto IL_0163;
		}
	}
	{
		float L_26 = (__this->___m_LastFlatAngle_17);
		float L_27 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_28 = Mathf_DeltaAngle_m650(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		float L_29 = fabsf(L_28);
		float L_30 = ___deltaTime;
		V_3 = ((float)((float)L_29/(float)L_30));
		float L_31 = (__this->___m_SpinTurnLimit_14);
		float L_32 = (__this->___m_SpinTurnLimit_14);
		float L_33 = V_3;
		float L_34 = Mathf_InverseLerp_m651(NULL /*static, unused*/, L_31, ((float)((float)L_32*(float)(0.75f))), L_33, /*hidden argument*/NULL);
		V_4 = L_34;
		float L_35 = (__this->___m_CurrentTurnAmount_18);
		float L_36 = V_4;
		if ((!(((float)L_35) > ((float)L_36))))
		{
			goto IL_0125;
		}
	}
	{
		G_B13_0 = (0.1f);
		goto IL_012a;
	}

IL_0125:
	{
		G_B13_0 = (1.0f);
	}

IL_012a:
	{
		V_5 = G_B13_0;
		bool L_37 = Application_get_isPlaying_m645(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0156;
		}
	}
	{
		float L_38 = (__this->___m_CurrentTurnAmount_18);
		float L_39 = V_4;
		float* L_40 = &(__this->___m_TurnSpeedVelocityChange_19);
		float L_41 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_42 = Mathf_SmoothDamp_m649(NULL /*static, unused*/, L_38, L_39, L_40, L_41, /*hidden argument*/NULL);
		__this->___m_CurrentTurnAmount_18 = L_42;
		goto IL_015e;
	}

IL_0156:
	{
		float L_43 = V_4;
		__this->___m_CurrentTurnAmount_18 = L_43;
	}

IL_015e:
	{
		goto IL_016e;
	}

IL_0163:
	{
		__this->___m_CurrentTurnAmount_18 = (1.0f);
	}

IL_016e:
	{
		float L_44 = V_2;
		__this->___m_LastFlatAngle_17 = L_44;
	}

IL_0175:
	{
		Transform_t1 * L_45 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_46 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_t4  L_47 = Transform_get_position_m593(L_46, /*hidden argument*/NULL);
		Transform_t1 * L_48 = (((AbstractTargetFollower_t15 *)__this)->___m_Target_2);
		NullCheck(L_48);
		Vector3_t4  L_49 = Transform_get_position_m593(L_48, /*hidden argument*/NULL);
		float L_50 = ___deltaTime;
		float L_51 = (__this->___m_MoveSpeed_9);
		Vector3_t4  L_52 = Vector3_Lerp_m652(NULL /*static, unused*/, L_47, L_49, ((float)((float)L_50*(float)L_51)), /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_set_position_m607(L_45, L_52, /*hidden argument*/NULL);
		bool L_53 = (__this->___m_FollowTilt_13);
		if (L_53)
		{
			goto IL_01d7;
		}
	}
	{
		(&V_0)->___y_2 = (0.0f);
		float L_54 = Vector3_get_sqrMagnitude_m653((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_54) < ((float)(1.401298E-45f)))))
		{
			goto IL_01d7;
		}
	}
	{
		Transform_t1 * L_55 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_55);
		Vector3_t4  L_56 = Transform_get_forward_m643(L_55, /*hidden argument*/NULL);
		V_0 = L_56;
	}

IL_01d7:
	{
		Vector3_t4  L_57 = V_0;
		Vector3_t4  L_58 = (__this->___m_RollUp_20);
		Quaternion_t19  L_59 = Quaternion_LookRotation_m654(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		V_6 = L_59;
		float L_60 = (__this->___m_RollSpeed_11);
		G_B23_0 = __this;
		if ((!(((float)L_60) > ((float)(0.0f)))))
		{
			G_B24_0 = __this;
			goto IL_020f;
		}
	}
	{
		Vector3_t4  L_61 = (__this->___m_RollUp_20);
		Vector3_t4  L_62 = V_1;
		float L_63 = (__this->___m_RollSpeed_11);
		float L_64 = ___deltaTime;
		Vector3_t4  L_65 = Vector3_Slerp_m655(NULL /*static, unused*/, L_61, L_62, ((float)((float)L_63*(float)L_64)), /*hidden argument*/NULL);
		G_B25_0 = L_65;
		G_B25_1 = G_B23_0;
		goto IL_0214;
	}

IL_020f:
	{
		Vector3_t4  L_66 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B25_0 = L_66;
		G_B25_1 = G_B24_0;
	}

IL_0214:
	{
		NullCheck(G_B25_1);
		G_B25_1->___m_RollUp_20 = G_B25_0;
		Transform_t1 * L_67 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_68 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_68);
		Quaternion_t19  L_69 = Transform_get_rotation_m656(L_68, /*hidden argument*/NULL);
		Quaternion_t19  L_70 = V_6;
		float L_71 = (__this->___m_TurnSpeed_10);
		float L_72 = (__this->___m_CurrentTurnAmount_18);
		float L_73 = ___deltaTime;
		Quaternion_t19  L_74 = Quaternion_Lerp_m657(NULL /*static, unused*/, L_69, L_70, ((float)((float)((float)((float)L_71*(float)L_72))*(float)L_73)), /*hidden argument*/NULL);
		NullCheck(L_67);
		Transform_set_rotation_m658(L_67, L_74, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Cameras.FreeLookCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Fr.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Cameras.FreeLookCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_FrMethodDeclarations.h"

// UnityStandardAssets.Cameras.PivotBasedCameraRig
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Pi.h"
// UnityEngine.CursorLockMode
#include "UnityEngine_UnityEngine_CursorLockMode.h"
// UnityEngine.Cursor
#include "UnityEngine_UnityEngine_CursorMethodDeclarations.h"


// System.Void UnityStandardAssets.Cameras.FreeLookCam::.ctor()
extern "C" void FreeLookCam__ctor_m30 (FreeLookCam_t18 * __this, const MethodInfo* method)
{
	{
		__this->___m_MoveSpeed_10 = (1.0f);
		__this->___m_TurnSpeed_11 = (1.5f);
		__this->___m_TurnSmoothing_12 = (0.1f);
		__this->___m_TiltMax_13 = (75.0f);
		__this->___m_TiltMin_14 = (45.0f);
		PivotBasedCameraRig__ctor_m41(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.FreeLookCam::Awake()
extern "C" void FreeLookCam_Awake_m31 (FreeLookCam_t18 * __this, const MethodInfo* method)
{
	Quaternion_t19  V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		PivotBasedCameraRig_Awake_m42(__this, /*hidden argument*/NULL);
		bool L_0 = (__this->___m_LockCursor_15);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		Cursor_set_lockState_m659(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		bool L_1 = (__this->___m_LockCursor_15);
		Cursor_set_visible_m660(NULL /*static, unused*/, ((((int32_t)L_1) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Transform_t1 * L_2 = (((PivotBasedCameraRig_t17 *)__this)->___m_Pivot_7);
		NullCheck(L_2);
		Quaternion_t19  L_3 = Transform_get_rotation_m656(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector3_t4  L_4 = Quaternion_get_eulerAngles_m661((&V_0), /*hidden argument*/NULL);
		__this->___m_PivotEulers_19 = L_4;
		Transform_t1 * L_5 = (((PivotBasedCameraRig_t17 *)__this)->___m_Pivot_7);
		NullCheck(L_5);
		Transform_t1 * L_6 = Component_get_transform_m594(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Quaternion_t19  L_7 = Transform_get_localRotation_m662(L_6, /*hidden argument*/NULL);
		__this->___m_PivotTargetRot_20 = L_7;
		Transform_t1 * L_8 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Quaternion_t19  L_9 = Transform_get_localRotation_m662(L_8, /*hidden argument*/NULL);
		__this->___m_TransformTargetRot_21 = L_9;
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.FreeLookCam::Update()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" void FreeLookCam_Update_m32 (FreeLookCam_t18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		FreeLookCam_HandleRotationMovement_m35(__this, /*hidden argument*/NULL);
		bool L_0 = (__this->___m_LockCursor_15);
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButtonUp_m663(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		bool L_2 = (__this->___m_LockCursor_15);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		G_B5_0 = 1;
		goto IL_002e;
	}

IL_002d:
	{
		G_B5_0 = 0;
	}

IL_002e:
	{
		Cursor_set_lockState_m659(NULL /*static, unused*/, G_B5_0, /*hidden argument*/NULL);
		bool L_3 = (__this->___m_LockCursor_15);
		Cursor_set_visible_m660(NULL /*static, unused*/, ((((int32_t)L_3) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.FreeLookCam::OnDisable()
extern "C" void FreeLookCam_OnDisable_m33 (FreeLookCam_t18 * __this, const MethodInfo* method)
{
	{
		Cursor_set_lockState_m659(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Cursor_set_visible_m660(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.FreeLookCam::FollowTarget(System.Single)
extern "C" void FreeLookCam_FollowTarget_m34 (FreeLookCam_t18 * __this, float ___deltaTime, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = (((AbstractTargetFollower_t15 *)__this)->___m_Target_2);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_3 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		Transform_t1 * L_5 = (((AbstractTargetFollower_t15 *)__this)->___m_Target_2);
		NullCheck(L_5);
		Vector3_t4  L_6 = Transform_get_position_m593(L_5, /*hidden argument*/NULL);
		float L_7 = ___deltaTime;
		float L_8 = (__this->___m_MoveSpeed_10);
		Vector3_t4  L_9 = Vector3_Lerp_m652(NULL /*static, unused*/, L_4, L_6, ((float)((float)L_7*(float)L_8)), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m607(L_2, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.FreeLookCam::HandleRotationMovement()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void FreeLookCam_HandleRotationMovement_m35 (FreeLookCam_t18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	FreeLookCam_t18 * G_B5_0 = {0};
	FreeLookCam_t18 * G_B4_0 = {0};
	float G_B6_0 = 0.0f;
	FreeLookCam_t18 * G_B6_1 = {0};
	{
		float L_0 = Time_get_timeScale_m664(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_0) < ((float)(1.401298E-45f)))))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		float L_1 = CrossPlatformInputManager_GetAxis_m100(NULL /*static, unused*/, (String_t*) &_stringLiteral10, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = CrossPlatformInputManager_GetAxis_m100(NULL /*static, unused*/, (String_t*) &_stringLiteral11, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = (__this->___m_LookAngle_17);
		float L_4 = V_0;
		float L_5 = (__this->___m_TurnSpeed_11);
		__this->___m_LookAngle_17 = ((float)((float)L_3+(float)((float)((float)L_4*(float)L_5))));
		float L_6 = (__this->___m_LookAngle_17);
		Quaternion_t19  L_7 = Quaternion_Euler_m665(NULL /*static, unused*/, (0.0f), L_6, (0.0f), /*hidden argument*/NULL);
		__this->___m_TransformTargetRot_21 = L_7;
		bool L_8 = (__this->___m_VerticalAutoReturn_16);
		if (!L_8)
		{
			goto IL_00a0;
		}
	}
	{
		float L_9 = V_1;
		G_B4_0 = __this;
		if ((!(((float)L_9) > ((float)(0.0f)))))
		{
			G_B5_0 = __this;
			goto IL_0084;
		}
	}
	{
		float L_10 = (__this->___m_TiltMin_14);
		float L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Lerp_m610(NULL /*static, unused*/, (0.0f), ((-L_10)), L_11, /*hidden argument*/NULL);
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		goto IL_0096;
	}

IL_0084:
	{
		float L_13 = (__this->___m_TiltMax_13);
		float L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_15 = Mathf_Lerp_m610(NULL /*static, unused*/, (0.0f), L_13, ((-L_14)), /*hidden argument*/NULL);
		G_B6_0 = L_15;
		G_B6_1 = G_B5_0;
	}

IL_0096:
	{
		NullCheck(G_B6_1);
		G_B6_1->___m_TiltAngle_18 = G_B6_0;
		goto IL_00d3;
	}

IL_00a0:
	{
		float L_16 = (__this->___m_TiltAngle_18);
		float L_17 = V_1;
		float L_18 = (__this->___m_TurnSpeed_11);
		__this->___m_TiltAngle_18 = ((float)((float)L_16-(float)((float)((float)L_17*(float)L_18))));
		float L_19 = (__this->___m_TiltAngle_18);
		float L_20 = (__this->___m_TiltMin_14);
		float L_21 = (__this->___m_TiltMax_13);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Clamp_m611(NULL /*static, unused*/, L_19, ((-L_20)), L_21, /*hidden argument*/NULL);
		__this->___m_TiltAngle_18 = L_22;
	}

IL_00d3:
	{
		float L_23 = (__this->___m_TiltAngle_18);
		Vector3_t4 * L_24 = &(__this->___m_PivotEulers_19);
		float L_25 = (L_24->___y_2);
		Vector3_t4 * L_26 = &(__this->___m_PivotEulers_19);
		float L_27 = (L_26->___z_3);
		Quaternion_t19  L_28 = Quaternion_Euler_m665(NULL /*static, unused*/, L_23, L_25, L_27, /*hidden argument*/NULL);
		__this->___m_PivotTargetRot_20 = L_28;
		float L_29 = (__this->___m_TurnSmoothing_12);
		if ((!(((float)L_29) > ((float)(0.0f)))))
		{
			goto IL_0169;
		}
	}
	{
		Transform_t1 * L_30 = (((PivotBasedCameraRig_t17 *)__this)->___m_Pivot_7);
		Transform_t1 * L_31 = (((PivotBasedCameraRig_t17 *)__this)->___m_Pivot_7);
		NullCheck(L_31);
		Quaternion_t19  L_32 = Transform_get_localRotation_m662(L_31, /*hidden argument*/NULL);
		Quaternion_t19  L_33 = (__this->___m_PivotTargetRot_20);
		float L_34 = (__this->___m_TurnSmoothing_12);
		float L_35 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t19  L_36 = Quaternion_Slerp_m666(NULL /*static, unused*/, L_32, L_33, ((float)((float)L_34*(float)L_35)), /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_localRotation_m667(L_30, L_36, /*hidden argument*/NULL);
		Transform_t1 * L_37 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_38 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		Quaternion_t19  L_39 = Transform_get_localRotation_m662(L_38, /*hidden argument*/NULL);
		Quaternion_t19  L_40 = (__this->___m_TransformTargetRot_21);
		float L_41 = (__this->___m_TurnSmoothing_12);
		float L_42 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t19  L_43 = Quaternion_Slerp_m666(NULL /*static, unused*/, L_39, L_40, ((float)((float)L_41*(float)L_42)), /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_set_localRotation_m667(L_37, L_43, /*hidden argument*/NULL);
		goto IL_018b;
	}

IL_0169:
	{
		Transform_t1 * L_44 = (((PivotBasedCameraRig_t17 *)__this)->___m_Pivot_7);
		Quaternion_t19  L_45 = (__this->___m_PivotTargetRot_20);
		NullCheck(L_44);
		Transform_set_localRotation_m667(L_44, L_45, /*hidden argument*/NULL);
		Transform_t1 * L_46 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Quaternion_t19  L_47 = (__this->___m_TransformTargetRot_21);
		NullCheck(L_46);
		Transform_set_localRotation_m667(L_46, L_47, /*hidden argument*/NULL);
	}

IL_018b:
	{
		return;
	}
}
// UnityStandardAssets.Cameras.HandHeldCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Ha.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Cameras.HandHeldCam
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_HaMethodDeclarations.h"

// UnityStandardAssets.Cameras.LookatTarget
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Lo.h"
// UnityStandardAssets.Cameras.LookatTarget
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_LoMethodDeclarations.h"


// System.Void UnityStandardAssets.Cameras.HandHeldCam::.ctor()
extern "C" void HandHeldCam__ctor_m36 (HandHeldCam_t20 * __this, const MethodInfo* method)
{
	{
		__this->___m_SwaySpeed_11 = (0.5f);
		__this->___m_BaseSwayAmount_12 = (0.5f);
		__this->___m_TrackingSwayAmount_13 = (0.5f);
		LookatTarget__ctor_m38(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.HandHeldCam::FollowTarget(System.Single)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void HandHeldCam_FollowTarget_m37 (HandHeldCam_t20 * __this, float ___deltaTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = ___deltaTime;
		LookatTarget_FollowTarget_m40(__this, L_0, /*hidden argument*/NULL);
		float L_1 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = (__this->___m_SwaySpeed_11);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_3 = Mathf_PerlinNoise_m669(NULL /*static, unused*/, (0.0f), ((float)((float)L_1*(float)L_2)), /*hidden argument*/NULL);
		V_0 = ((float)((float)L_3-(float)(0.5f)));
		float L_4 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = (__this->___m_SwaySpeed_11);
		float L_6 = Mathf_PerlinNoise_m669(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)L_4*(float)L_5))+(float)(100.0f))), /*hidden argument*/NULL);
		V_1 = ((float)((float)L_6-(float)(0.5f)));
		float L_7 = V_0;
		float L_8 = (__this->___m_BaseSwayAmount_12);
		V_0 = ((float)((float)L_7*(float)L_8));
		float L_9 = V_1;
		float L_10 = (__this->___m_BaseSwayAmount_12);
		V_1 = ((float)((float)L_9*(float)L_10));
		float L_11 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_12 = (__this->___m_SwaySpeed_11);
		float L_13 = Mathf_PerlinNoise_m669(NULL /*static, unused*/, (0.0f), ((float)((float)L_11*(float)L_12)), /*hidden argument*/NULL);
		float L_14 = (__this->___m_TrackingBias_14);
		V_2 = ((float)((float)((float)((float)L_13-(float)(0.5f)))+(float)L_14));
		float L_15 = Time_get_time_m668(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = (__this->___m_SwaySpeed_11);
		float L_17 = Mathf_PerlinNoise_m669(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)L_15*(float)L_16))+(float)(100.0f))), /*hidden argument*/NULL);
		float L_18 = (__this->___m_TrackingBias_14);
		V_3 = ((float)((float)((float)((float)L_17-(float)(0.5f)))+(float)L_18));
		float L_19 = V_2;
		float L_20 = (__this->___m_TrackingSwayAmount_13);
		Vector3_t4 * L_21 = &(((LookatTarget_t21 *)__this)->___m_FollowVelocity_10);
		float L_22 = (L_21->___x_1);
		V_2 = ((float)((float)L_19*(float)((float)((float)((-L_20))*(float)L_22))));
		float L_23 = V_3;
		float L_24 = (__this->___m_TrackingSwayAmount_13);
		Vector3_t4 * L_25 = &(((LookatTarget_t21 *)__this)->___m_FollowVelocity_10);
		float L_26 = (L_25->___y_2);
		V_3 = ((float)((float)L_23*(float)((float)((float)L_24*(float)L_26))));
		Transform_t1 * L_27 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		float L_28 = V_0;
		float L_29 = V_2;
		float L_30 = V_1;
		float L_31 = V_3;
		NullCheck(L_27);
		Transform_Rotate_m670(L_27, ((float)((float)L_28+(float)L_29)), ((float)((float)L_30+(float)L_31)), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityStandardAssets.Cameras.LookatTarget::.ctor()
extern "C" void LookatTarget__ctor_m38 (LookatTarget_t21 * __this, const MethodInfo* method)
{
	{
		__this->___m_FollowSpeed_7 = (1.0f);
		AbstractTargetFollower__ctor_m20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.LookatTarget::Start()
extern "C" void LookatTarget_Start_m39 (LookatTarget_t21 * __this, const MethodInfo* method)
{
	{
		AbstractTargetFollower_Start_m21(__this, /*hidden argument*/NULL);
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t19  L_1 = Transform_get_localRotation_m662(L_0, /*hidden argument*/NULL);
		__this->___m_OriginalRotation_9 = L_1;
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.LookatTarget::FollowTarget(System.Single)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void LookatTarget_FollowTarget_m40 (LookatTarget_t21 * __this, float ___deltaTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_t4  V_3 = {0};
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Quaternion_t19  L_1 = (__this->___m_OriginalRotation_9);
		NullCheck(L_0);
		Transform_set_localRotation_m667(L_0, L_1, /*hidden argument*/NULL);
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_3 = (((AbstractTargetFollower_t15 *)__this)->___m_Target_2);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4  L_5 = Transform_InverseTransformPoint_m671(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = ((&V_0)->___x_1);
		float L_7 = ((&V_0)->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_8 = atan2f(L_6, L_7);
		V_1 = ((float)((float)L_8*(float)(57.29578f)));
		float L_9 = V_1;
		Vector2_t6 * L_10 = &(__this->___m_RotationRange_6);
		float L_11 = (L_10->___y_2);
		Vector2_t6 * L_12 = &(__this->___m_RotationRange_6);
		float L_13 = (L_12->___y_2);
		float L_14 = Mathf_Clamp_m611(NULL /*static, unused*/, L_9, ((float)((float)((-L_11))*(float)(0.5f))), ((float)((float)L_13*(float)(0.5f))), /*hidden argument*/NULL);
		V_1 = L_14;
		Transform_t1 * L_15 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Quaternion_t19  L_16 = (__this->___m_OriginalRotation_9);
		float L_17 = V_1;
		Quaternion_t19  L_18 = Quaternion_Euler_m665(NULL /*static, unused*/, (0.0f), L_17, (0.0f), /*hidden argument*/NULL);
		Quaternion_t19  L_19 = Quaternion_op_Multiply_m672(NULL /*static, unused*/, L_16, L_18, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localRotation_m667(L_15, L_19, /*hidden argument*/NULL);
		Transform_t1 * L_20 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_21 = (((AbstractTargetFollower_t15 *)__this)->___m_Target_2);
		NullCheck(L_21);
		Vector3_t4  L_22 = Transform_get_position_m593(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t4  L_23 = Transform_InverseTransformPoint_m671(L_20, L_22, /*hidden argument*/NULL);
		V_0 = L_23;
		float L_24 = ((&V_0)->___y_2);
		float L_25 = ((&V_0)->___z_3);
		float L_26 = atan2f(L_24, L_25);
		V_2 = ((float)((float)L_26*(float)(57.29578f)));
		float L_27 = V_2;
		Vector2_t6 * L_28 = &(__this->___m_RotationRange_6);
		float L_29 = (L_28->___x_1);
		Vector2_t6 * L_30 = &(__this->___m_RotationRange_6);
		float L_31 = (L_30->___x_1);
		float L_32 = Mathf_Clamp_m611(NULL /*static, unused*/, L_27, ((float)((float)((-L_29))*(float)(0.5f))), ((float)((float)L_31*(float)(0.5f))), /*hidden argument*/NULL);
		V_2 = L_32;
		Vector3_t4 * L_33 = &(__this->___m_FollowAngles_8);
		float L_34 = (L_33->___x_1);
		Vector3_t4 * L_35 = &(__this->___m_FollowAngles_8);
		float L_36 = (L_35->___x_1);
		float L_37 = V_2;
		float L_38 = Mathf_DeltaAngle_m650(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		Vector3_t4 * L_39 = &(__this->___m_FollowAngles_8);
		float L_40 = (L_39->___y_2);
		Vector3_t4 * L_41 = &(__this->___m_FollowAngles_8);
		float L_42 = (L_41->___y_2);
		float L_43 = V_1;
		float L_44 = Mathf_DeltaAngle_m650(NULL /*static, unused*/, L_42, L_43, /*hidden argument*/NULL);
		Vector3__ctor_m673((&V_3), ((float)((float)L_34+(float)L_38)), ((float)((float)L_40+(float)L_44)), /*hidden argument*/NULL);
		Vector3_t4  L_45 = (__this->___m_FollowAngles_8);
		Vector3_t4  L_46 = V_3;
		Vector3_t4 * L_47 = &(__this->___m_FollowVelocity_10);
		float L_48 = (__this->___m_FollowSpeed_7);
		Vector3_t4  L_49 = Vector3_SmoothDamp_m606(NULL /*static, unused*/, L_45, L_46, L_47, L_48, /*hidden argument*/NULL);
		__this->___m_FollowAngles_8 = L_49;
		Transform_t1 * L_50 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Quaternion_t19  L_51 = (__this->___m_OriginalRotation_9);
		Vector3_t4 * L_52 = &(__this->___m_FollowAngles_8);
		float L_53 = (L_52->___x_1);
		Vector3_t4 * L_54 = &(__this->___m_FollowAngles_8);
		float L_55 = (L_54->___y_2);
		Quaternion_t19  L_56 = Quaternion_Euler_m665(NULL /*static, unused*/, ((-L_53)), L_55, (0.0f), /*hidden argument*/NULL);
		Quaternion_t19  L_57 = Quaternion_op_Multiply_m672(NULL /*static, unused*/, L_51, L_56, /*hidden argument*/NULL);
		NullCheck(L_50);
		Transform_set_localRotation_m667(L_50, L_57, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
struct Component_t219;
struct Camera_t27;
struct Component_t219;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C" Object_t * Component_GetComponentInChildren_TisObject_t_m675_gshared (Component_t219 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisObject_t_m675(__this, method) (( Object_t * (*) (Component_t219 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m675_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Camera>()
#define Component_GetComponentInChildren_TisCamera_t27_m674(__this, method) (( Camera_t27 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m675_gshared)(__this, method)


// System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::.ctor()
extern "C" void PivotBasedCameraRig__ctor_m41 (PivotBasedCameraRig_t17 * __this, const MethodInfo* method)
{
	{
		AbstractTargetFollower__ctor_m20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::Awake()
extern const MethodInfo* Component_GetComponentInChildren_TisCamera_t27_m674_MethodInfo_var;
extern "C" void PivotBasedCameraRig_Awake_m42 (PivotBasedCameraRig_t17 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentInChildren_TisCamera_t27_m674_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t27 * L_0 = Component_GetComponentInChildren_TisCamera_t27_m674(__this, /*hidden argument*/Component_GetComponentInChildren_TisCamera_t27_m674_MethodInfo_var);
		NullCheck(L_0);
		Transform_t1 * L_1 = Component_get_transform_m594(L_0, /*hidden argument*/NULL);
		__this->___m_Cam_6 = L_1;
		Transform_t1 * L_2 = (__this->___m_Cam_6);
		NullCheck(L_2);
		Transform_t1 * L_3 = Transform_get_parent_m676(L_2, /*hidden argument*/NULL);
		__this->___m_Pivot_7 = L_3;
		return;
	}
}
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Pr.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_PrMethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"


// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer::.ctor()
extern "C" void RayHitComparer__ctor_m43 (RayHitComparer_t22 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer::Compare(System.Object,System.Object)
extern TypeInfo* RaycastHit_t24_il2cpp_TypeInfo_var;
extern "C" int32_t RayHitComparer_Compare_m44 (RayHitComparer_t22 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RaycastHit_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t24  V_0 = {0};
	float V_1 = 0.0f;
	RaycastHit_t24  V_2 = {0};
	{
		Object_t * L_0 = ___x;
		V_0 = ((*(RaycastHit_t24 *)((RaycastHit_t24 *)UnBox (L_0, RaycastHit_t24_il2cpp_TypeInfo_var))));
		float L_1 = RaycastHit_get_distance_m678((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		Object_t * L_2 = ___y;
		V_2 = ((*(RaycastHit_t24 *)((RaycastHit_t24 *)UnBox (L_2, RaycastHit_t24_il2cpp_TypeInfo_var))));
		float L_3 = RaycastHit_get_distance_m678((&V_2), /*hidden argument*/NULL);
		int32_t L_4 = Single_CompareTo_m679((&V_1), L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Pr_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Pr_0MethodDeclarations.h"

// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"


// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::.ctor()
extern "C" void ProtectCameraFromWallClip__ctor_m45 (ProtectCameraFromWallClip_t25 * __this, const MethodInfo* method)
{
	{
		__this->___clipMoveTime_2 = (0.05f);
		__this->___returnTime_3 = (0.4f);
		__this->___sphereCastRadius_4 = (0.1f);
		__this->___closestDistance_6 = (0.5f);
		__this->___dontClipTag_7 = (String_t*) &_stringLiteral1;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.Cameras.ProtectCameraFromWallClip::get_protecting()
extern "C" bool ProtectCameraFromWallClip_get_protecting_m46 (ProtectCameraFromWallClip_t25 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CprotectingU3Ek__BackingField_16);
		return L_0;
	}
}
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::set_protecting(System.Boolean)
extern "C" void ProtectCameraFromWallClip_set_protecting_m47 (ProtectCameraFromWallClip_t25 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CprotectingU3Ek__BackingField_16 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::Start()
extern TypeInfo* RayHitComparer_t22_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisCamera_t27_m674_MethodInfo_var;
extern "C" void ProtectCameraFromWallClip_Start_m48 (ProtectCameraFromWallClip_t25 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RayHitComparer_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		Component_GetComponentInChildren_TisCamera_t27_m674_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4  V_0 = {0};
	{
		Camera_t27 * L_0 = Component_GetComponentInChildren_TisCamera_t27_m674(__this, /*hidden argument*/Component_GetComponentInChildren_TisCamera_t27_m674_MethodInfo_var);
		NullCheck(L_0);
		Transform_t1 * L_1 = Component_get_transform_m594(L_0, /*hidden argument*/NULL);
		__this->___m_Cam_8 = L_1;
		Transform_t1 * L_2 = (__this->___m_Cam_8);
		NullCheck(L_2);
		Transform_t1 * L_3 = Transform_get_parent_m676(L_2, /*hidden argument*/NULL);
		__this->___m_Pivot_9 = L_3;
		Transform_t1 * L_4 = (__this->___m_Cam_8);
		NullCheck(L_4);
		Vector3_t4  L_5 = Transform_get_localPosition_m680(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = Vector3_get_magnitude_m647((&V_0), /*hidden argument*/NULL);
		__this->___m_OriginalDist_10 = L_6;
		float L_7 = (__this->___m_OriginalDist_10);
		__this->___m_CurrentDist_12 = L_7;
		RayHitComparer_t22 * L_8 = (RayHitComparer_t22 *)il2cpp_codegen_object_new (RayHitComparer_t22_il2cpp_TypeInfo_var);
		RayHitComparer__ctor_m43(L_8, /*hidden argument*/NULL);
		__this->___m_RayHitComparer_15 = L_8;
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::LateUpdate()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void ProtectCameraFromWallClip_LateUpdate_m49 (ProtectCameraFromWallClip_t25 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	ColliderU5BU5D_t135* V_1 = {0};
	bool V_2 = false;
	bool V_3 = false;
	int32_t V_4 = 0;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	Vector3_t4  V_7 = {0};
	float* G_B22_0 = {0};
	float G_B22_1 = 0.0f;
	float G_B22_2 = 0.0f;
	ProtectCameraFromWallClip_t25 * G_B22_3 = {0};
	float* G_B21_0 = {0};
	float G_B21_1 = 0.0f;
	float G_B21_2 = 0.0f;
	ProtectCameraFromWallClip_t25 * G_B21_3 = {0};
	float G_B23_0 = 0.0f;
	float* G_B23_1 = {0};
	float G_B23_2 = 0.0f;
	float G_B23_3 = 0.0f;
	ProtectCameraFromWallClip_t25 * G_B23_4 = {0};
	{
		float L_0 = (__this->___m_OriginalDist_10);
		V_0 = L_0;
		Ray_t26 * L_1 = &(__this->___m_Ray_13);
		Transform_t1 * L_2 = (__this->___m_Pivot_9);
		NullCheck(L_2);
		Vector3_t4  L_3 = Transform_get_position_m593(L_2, /*hidden argument*/NULL);
		Transform_t1 * L_4 = (__this->___m_Pivot_9);
		NullCheck(L_4);
		Vector3_t4  L_5 = Transform_get_forward_m643(L_4, /*hidden argument*/NULL);
		float L_6 = (__this->___sphereCastRadius_4);
		Vector3_t4  L_7 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Vector3_t4  L_8 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_3, L_7, /*hidden argument*/NULL);
		Ray_set_origin_m681(L_1, L_8, /*hidden argument*/NULL);
		Ray_t26 * L_9 = &(__this->___m_Ray_13);
		Transform_t1 * L_10 = (__this->___m_Pivot_9);
		NullCheck(L_10);
		Vector3_t4  L_11 = Transform_get_forward_m643(L_10, /*hidden argument*/NULL);
		Vector3_t4  L_12 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Ray_set_direction_m683(L_9, L_12, /*hidden argument*/NULL);
		Ray_t26 * L_13 = &(__this->___m_Ray_13);
		Vector3_t4  L_14 = Ray_get_origin_m684(L_13, /*hidden argument*/NULL);
		float L_15 = (__this->___sphereCastRadius_4);
		ColliderU5BU5D_t135* L_16 = Physics_OverlapSphere_m685(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		V_2 = 0;
		V_3 = 0;
		V_4 = 0;
		goto IL_00be;
	}

IL_0076:
	{
		ColliderU5BU5D_t135* L_17 = V_1;
		int32_t L_18 = V_4;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		NullCheck((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_17, L_19)));
		bool L_20 = Collider_get_isTrigger_m686((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_17, L_19)), /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00b8;
		}
	}
	{
		ColliderU5BU5D_t135* L_21 = V_1;
		int32_t L_22 = V_4;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		NullCheck((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_21, L_23)));
		Rigidbody_t14 * L_24 = Collider_get_attachedRigidbody_m687((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_21, L_23)), /*hidden argument*/NULL);
		bool L_25 = Object_op_Inequality_m623(NULL /*static, unused*/, L_24, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00b1;
		}
	}
	{
		ColliderU5BU5D_t135* L_26 = V_1;
		int32_t L_27 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		NullCheck((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_26, L_28)));
		Rigidbody_t14 * L_29 = Collider_get_attachedRigidbody_m687((*(Collider_t138 **)(Collider_t138 **)SZArrayLdElema(L_26, L_28)), /*hidden argument*/NULL);
		String_t* L_30 = (__this->___dontClipTag_7);
		NullCheck(L_29);
		bool L_31 = Component_CompareTag_m688(L_29, L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_00b8;
		}
	}

IL_00b1:
	{
		V_2 = 1;
		goto IL_00c8;
	}

IL_00b8:
	{
		int32_t L_32 = V_4;
		V_4 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00be:
	{
		int32_t L_33 = V_4;
		ColliderU5BU5D_t135* L_34 = V_1;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)(((Array_t *)L_34)->max_length))))))
		{
			goto IL_0076;
		}
	}

IL_00c8:
	{
		bool L_35 = V_2;
		if (!L_35)
		{
			goto IL_011d;
		}
	}
	{
		Ray_t26 * L_36 = &(__this->___m_Ray_13);
		Ray_t26 * L_37 = L_36;
		Vector3_t4  L_38 = Ray_get_origin_m684(L_37, /*hidden argument*/NULL);
		Transform_t1 * L_39 = (__this->___m_Pivot_9);
		NullCheck(L_39);
		Vector3_t4  L_40 = Transform_get_forward_m643(L_39, /*hidden argument*/NULL);
		float L_41 = (__this->___sphereCastRadius_4);
		Vector3_t4  L_42 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		Vector3_t4  L_43 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_38, L_42, /*hidden argument*/NULL);
		Ray_set_origin_m681(L_37, L_43, /*hidden argument*/NULL);
		Ray_t26  L_44 = (__this->___m_Ray_13);
		float L_45 = (__this->___m_OriginalDist_10);
		float L_46 = (__this->___sphereCastRadius_4);
		RaycastHitU5BU5D_t23* L_47 = Physics_RaycastAll_m689(NULL /*static, unused*/, L_44, ((float)((float)L_45-(float)L_46)), /*hidden argument*/NULL);
		__this->___m_Hits_14 = L_47;
		goto IL_0141;
	}

IL_011d:
	{
		Ray_t26  L_48 = (__this->___m_Ray_13);
		float L_49 = (__this->___sphereCastRadius_4);
		float L_50 = (__this->___m_OriginalDist_10);
		float L_51 = (__this->___sphereCastRadius_4);
		RaycastHitU5BU5D_t23* L_52 = Physics_SphereCastAll_m690(NULL /*static, unused*/, L_48, L_49, ((float)((float)L_50+(float)L_51)), /*hidden argument*/NULL);
		__this->___m_Hits_14 = L_52;
	}

IL_0141:
	{
		RaycastHitU5BU5D_t23* L_53 = (__this->___m_Hits_14);
		RayHitComparer_t22 * L_54 = (__this->___m_RayHitComparer_15);
		Array_Sort_m691(NULL /*static, unused*/, (Array_t *)(Array_t *)L_53, L_54, /*hidden argument*/NULL);
		V_5 = (std::numeric_limits<float>::infinity());
		V_6 = 0;
		goto IL_0223;
	}

IL_0161:
	{
		RaycastHitU5BU5D_t23* L_55 = (__this->___m_Hits_14);
		int32_t L_56 = V_6;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_56);
		float L_57 = RaycastHit_get_distance_m678(((RaycastHit_t24 *)(RaycastHit_t24 *)SZArrayLdElema(L_55, L_56)), /*hidden argument*/NULL);
		float L_58 = V_5;
		if ((!(((float)L_57) < ((float)L_58))))
		{
			goto IL_021d;
		}
	}
	{
		RaycastHitU5BU5D_t23* L_59 = (__this->___m_Hits_14);
		int32_t L_60 = V_6;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		Collider_t138 * L_61 = RaycastHit_get_collider_m692(((RaycastHit_t24 *)(RaycastHit_t24 *)SZArrayLdElema(L_59, L_60)), /*hidden argument*/NULL);
		NullCheck(L_61);
		bool L_62 = Collider_get_isTrigger_m686(L_61, /*hidden argument*/NULL);
		if (L_62)
		{
			goto IL_021d;
		}
	}
	{
		RaycastHitU5BU5D_t23* L_63 = (__this->___m_Hits_14);
		int32_t L_64 = V_6;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, L_64);
		Collider_t138 * L_65 = RaycastHit_get_collider_m692(((RaycastHit_t24 *)(RaycastHit_t24 *)SZArrayLdElema(L_63, L_64)), /*hidden argument*/NULL);
		NullCheck(L_65);
		Rigidbody_t14 * L_66 = Collider_get_attachedRigidbody_m687(L_65, /*hidden argument*/NULL);
		bool L_67 = Object_op_Inequality_m623(NULL /*static, unused*/, L_66, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_01df;
		}
	}
	{
		RaycastHitU5BU5D_t23* L_68 = (__this->___m_Hits_14);
		int32_t L_69 = V_6;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, L_69);
		Collider_t138 * L_70 = RaycastHit_get_collider_m692(((RaycastHit_t24 *)(RaycastHit_t24 *)SZArrayLdElema(L_68, L_69)), /*hidden argument*/NULL);
		NullCheck(L_70);
		Rigidbody_t14 * L_71 = Collider_get_attachedRigidbody_m687(L_70, /*hidden argument*/NULL);
		String_t* L_72 = (__this->___dontClipTag_7);
		NullCheck(L_71);
		bool L_73 = Component_CompareTag_m688(L_71, L_72, /*hidden argument*/NULL);
		if (L_73)
		{
			goto IL_021d;
		}
	}

IL_01df:
	{
		RaycastHitU5BU5D_t23* L_74 = (__this->___m_Hits_14);
		int32_t L_75 = V_6;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, L_75);
		float L_76 = RaycastHit_get_distance_m678(((RaycastHit_t24 *)(RaycastHit_t24 *)SZArrayLdElema(L_74, L_75)), /*hidden argument*/NULL);
		V_5 = L_76;
		Transform_t1 * L_77 = (__this->___m_Pivot_9);
		RaycastHitU5BU5D_t23* L_78 = (__this->___m_Hits_14);
		int32_t L_79 = V_6;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, L_79);
		Vector3_t4  L_80 = RaycastHit_get_point_m693(((RaycastHit_t24 *)(RaycastHit_t24 *)SZArrayLdElema(L_78, L_79)), /*hidden argument*/NULL);
		NullCheck(L_77);
		Vector3_t4  L_81 = Transform_InverseTransformPoint_m671(L_77, L_80, /*hidden argument*/NULL);
		V_7 = L_81;
		float L_82 = ((&V_7)->___z_3);
		V_0 = ((-L_82));
		V_3 = 1;
	}

IL_021d:
	{
		int32_t L_83 = V_6;
		V_6 = ((int32_t)((int32_t)L_83+(int32_t)1));
	}

IL_0223:
	{
		int32_t L_84 = V_6;
		RaycastHitU5BU5D_t23* L_85 = (__this->___m_Hits_14);
		NullCheck(L_85);
		if ((((int32_t)L_84) < ((int32_t)(((int32_t)(((Array_t *)L_85)->max_length))))))
		{
			goto IL_0161;
		}
	}
	{
		bool L_86 = V_3;
		if (!L_86)
		{
			goto IL_026a;
		}
	}
	{
		Ray_t26 * L_87 = &(__this->___m_Ray_13);
		Vector3_t4  L_88 = Ray_get_origin_m684(L_87, /*hidden argument*/NULL);
		Transform_t1 * L_89 = (__this->___m_Pivot_9);
		NullCheck(L_89);
		Vector3_t4  L_90 = Transform_get_forward_m643(L_89, /*hidden argument*/NULL);
		Vector3_t4  L_91 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_90, /*hidden argument*/NULL);
		float L_92 = V_0;
		float L_93 = (__this->___sphereCastRadius_4);
		Vector3_t4  L_94 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_91, ((float)((float)L_92+(float)L_93)), /*hidden argument*/NULL);
		Color_t65  L_95 = Color_get_red_m694(NULL /*static, unused*/, /*hidden argument*/NULL);
		Debug_DrawRay_m695(NULL /*static, unused*/, L_88, L_94, L_95, /*hidden argument*/NULL);
	}

IL_026a:
	{
		bool L_96 = V_3;
		ProtectCameraFromWallClip_set_protecting_m47(__this, L_96, /*hidden argument*/NULL);
		float L_97 = (__this->___m_CurrentDist_12);
		float L_98 = V_0;
		float* L_99 = &(__this->___m_MoveVelocity_11);
		float L_100 = (__this->___m_CurrentDist_12);
		float L_101 = V_0;
		G_B21_0 = L_99;
		G_B21_1 = L_98;
		G_B21_2 = L_97;
		G_B21_3 = __this;
		if ((!(((float)L_100) > ((float)L_101))))
		{
			G_B22_0 = L_99;
			G_B22_1 = L_98;
			G_B22_2 = L_97;
			G_B22_3 = __this;
			goto IL_0296;
		}
	}
	{
		float L_102 = (__this->___clipMoveTime_2);
		G_B23_0 = L_102;
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		G_B23_4 = G_B21_3;
		goto IL_029c;
	}

IL_0296:
	{
		float L_103 = (__this->___returnTime_3);
		G_B23_0 = L_103;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_029c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_104 = Mathf_SmoothDamp_m649(NULL /*static, unused*/, G_B23_3, G_B23_2, G_B23_1, G_B23_0, /*hidden argument*/NULL);
		NullCheck(G_B23_4);
		G_B23_4->___m_CurrentDist_12 = L_104;
		float L_105 = (__this->___m_CurrentDist_12);
		float L_106 = (__this->___closestDistance_6);
		float L_107 = (__this->___m_OriginalDist_10);
		float L_108 = Mathf_Clamp_m611(NULL /*static, unused*/, L_105, L_106, L_107, /*hidden argument*/NULL);
		__this->___m_CurrentDist_12 = L_108;
		Transform_t1 * L_109 = (__this->___m_Cam_8);
		Vector3_t4  L_110 = Vector3_get_forward_m605(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_111 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_110, /*hidden argument*/NULL);
		float L_112 = (__this->___m_CurrentDist_12);
		Vector3_t4  L_113 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_111, L_112, /*hidden argument*/NULL);
		NullCheck(L_109);
		Transform_set_localPosition_m696(L_109, L_113, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Cameras.TargetFieldOfView
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_Ta.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.Cameras.TargetFieldOfView
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cameras_TaMethodDeclarations.h"

// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
// UnityEngine.TrailRenderer
#include "UnityEngine_UnityEngine_TrailRenderer.h"
// UnityEngine.ParticleRenderer
#include "UnityEngine_UnityEngine_ParticleRenderer.h"
// UnityEngine.ParticleSystemRenderer
#include "UnityEngine_UnityEngine_ParticleSystemRenderer.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
struct Component_t219;
struct RendererU5BU5D_t223;
struct Component_t219;
struct ObjectU5BU5D_t224;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t224* Component_GetComponentsInChildren_TisObject_t_m698_gshared (Component_t219 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m698(__this, method) (( ObjectU5BU5D_t224* (*) (Component_t219 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m698_gshared)(__this, method)
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>()
#define Component_GetComponentsInChildren_TisRenderer_t154_m697(__this, method) (( RendererU5BU5D_t223* (*) (Component_t219 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m698_gshared)(__this, method)


// System.Void UnityStandardAssets.Cameras.TargetFieldOfView::.ctor()
extern "C" void TargetFieldOfView__ctor_m50 (TargetFieldOfView_t28 * __this, const MethodInfo* method)
{
	{
		__this->___m_FovAdjustTime_6 = (1.0f);
		__this->___m_ZoomAmountMultiplier_7 = (2.0f);
		AbstractTargetFollower__ctor_m20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.TargetFieldOfView::Start()
extern const MethodInfo* Component_GetComponentInChildren_TisCamera_t27_m674_MethodInfo_var;
extern "C" void TargetFieldOfView_Start_m51 (TargetFieldOfView_t28 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentInChildren_TisCamera_t27_m674_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		s_Il2CppMethodIntialized = true;
	}
	{
		AbstractTargetFollower_Start_m21(__this, /*hidden argument*/NULL);
		Transform_t1 * L_0 = (((AbstractTargetFollower_t15 *)__this)->___m_Target_2);
		bool L_1 = (__this->___m_IncludeEffectsInSize_8);
		float L_2 = TargetFieldOfView_MaxBoundsExtent_m54(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___m_BoundSize_9 = L_2;
		Camera_t27 * L_3 = Component_GetComponentInChildren_TisCamera_t27_m674(__this, /*hidden argument*/Component_GetComponentInChildren_TisCamera_t27_m674_MethodInfo_var);
		__this->___m_Cam_11 = L_3;
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.TargetFieldOfView::FollowTarget(System.Single)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void TargetFieldOfView_FollowTarget_m52 (TargetFieldOfView_t28 * __this, float ___deltaTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t4  V_2 = {0};
	{
		Transform_t1 * L_0 = (((AbstractTargetFollower_t15 *)__this)->___m_Target_2);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4  L_3 = Transform_get_position_m593(L_2, /*hidden argument*/NULL);
		Vector3_t4  L_4 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Vector3_get_magnitude_m647((&V_2), /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = (__this->___m_BoundSize_9);
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_8 = atan2f(L_6, L_7);
		float L_9 = (__this->___m_ZoomAmountMultiplier_7);
		V_1 = ((float)((float)((float)((float)L_8*(float)(57.29578f)))*(float)L_9));
		Camera_t27 * L_10 = (__this->___m_Cam_11);
		Camera_t27 * L_11 = (__this->___m_Cam_11);
		NullCheck(L_11);
		float L_12 = Camera_get_fieldOfView_m699(L_11, /*hidden argument*/NULL);
		float L_13 = V_1;
		float* L_14 = &(__this->___m_FovAdjustVelocity_10);
		float L_15 = (__this->___m_FovAdjustTime_6);
		float L_16 = Mathf_SmoothDamp_m649(NULL /*static, unused*/, L_12, L_13, L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		Camera_set_fieldOfView_m700(L_10, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.TargetFieldOfView::SetTarget(UnityEngine.Transform)
extern "C" void TargetFieldOfView_SetTarget_m53 (TargetFieldOfView_t28 * __this, Transform_t1 * ___newTransform, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = ___newTransform;
		AbstractTargetFollower_SetTarget_m26(__this, L_0, /*hidden argument*/NULL);
		Transform_t1 * L_1 = ___newTransform;
		bool L_2 = (__this->___m_IncludeEffectsInSize_8);
		float L_3 = TargetFieldOfView_MaxBoundsExtent_m54(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		__this->___m_BoundSize_9 = L_3;
		return;
	}
}
// System.Single UnityStandardAssets.Cameras.TargetFieldOfView::MaxBoundsExtent(UnityEngine.Transform,System.Boolean)
extern TypeInfo* Bounds_t225_il2cpp_TypeInfo_var;
extern TypeInfo* TrailRenderer_t226_il2cpp_TypeInfo_var;
extern TypeInfo* ParticleRenderer_t227_il2cpp_TypeInfo_var;
extern TypeInfo* ParticleSystemRenderer_t228_il2cpp_TypeInfo_var;
extern TypeInfo* SingleU5BU5D_t211_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t154_m697_MethodInfo_var;
extern "C" float TargetFieldOfView_MaxBoundsExtent_m54 (Object_t * __this /* static, unused */, Transform_t1 * ___obj, bool ___includeEffects, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Bounds_t225_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		TrailRenderer_t226_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		ParticleRenderer_t227_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		ParticleSystemRenderer_t228_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		SingleU5BU5D_t211_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		Component_GetComponentsInChildren_TisRenderer_t154_m697_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t223* V_0 = {0};
	Bounds_t225  V_1 = {0};
	bool V_2 = false;
	Renderer_t154 * V_3 = {0};
	RendererU5BU5D_t223* V_4 = {0};
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	Vector3_t4  V_7 = {0};
	Vector3_t4  V_8 = {0};
	Vector3_t4  V_9 = {0};
	{
		Transform_t1 * L_0 = ___obj;
		NullCheck(L_0);
		RendererU5BU5D_t223* L_1 = Component_GetComponentsInChildren_TisRenderer_t154_m697(L_0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t154_m697_MethodInfo_var);
		V_0 = L_1;
		Initobj (Bounds_t225_il2cpp_TypeInfo_var, (&V_1));
		V_2 = 0;
		RendererU5BU5D_t223* L_2 = V_0;
		V_4 = L_2;
		V_5 = 0;
		goto IL_006a;
	}

IL_001c:
	{
		RendererU5BU5D_t223* L_3 = V_4;
		int32_t L_4 = V_5;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_3 = (*(Renderer_t154 **)(Renderer_t154 **)SZArrayLdElema(L_3, L_5));
		Renderer_t154 * L_6 = V_3;
		if (((TrailRenderer_t226 *)IsInst(L_6, TrailRenderer_t226_il2cpp_TypeInfo_var)))
		{
			goto IL_0064;
		}
	}
	{
		Renderer_t154 * L_7 = V_3;
		if (((ParticleRenderer_t227 *)IsInst(L_7, ParticleRenderer_t227_il2cpp_TypeInfo_var)))
		{
			goto IL_0064;
		}
	}
	{
		Renderer_t154 * L_8 = V_3;
		if (((ParticleSystemRenderer_t228 *)IsInst(L_8, ParticleSystemRenderer_t228_il2cpp_TypeInfo_var)))
		{
			goto IL_0064;
		}
	}
	{
		bool L_9 = V_2;
		if (L_9)
		{
			goto IL_0057;
		}
	}
	{
		V_2 = 1;
		Renderer_t154 * L_10 = V_3;
		NullCheck(L_10);
		Bounds_t225  L_11 = Renderer_get_bounds_m701(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		goto IL_0064;
	}

IL_0057:
	{
		Renderer_t154 * L_12 = V_3;
		NullCheck(L_12);
		Bounds_t225  L_13 = Renderer_get_bounds_m701(L_12, /*hidden argument*/NULL);
		Bounds_Encapsulate_m702((&V_1), L_13, /*hidden argument*/NULL);
	}

IL_0064:
	{
		int32_t L_14 = V_5;
		V_5 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_006a:
	{
		int32_t L_15 = V_5;
		RendererU5BU5D_t223* L_16 = V_4;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)(((Array_t *)L_16)->max_length))))))
		{
			goto IL_001c;
		}
	}
	{
		SingleU5BU5D_t211* L_17 = ((SingleU5BU5D_t211*)SZArrayNew(SingleU5BU5D_t211_il2cpp_TypeInfo_var, 3));
		Vector3_t4  L_18 = Bounds_get_extents_m703((&V_1), /*hidden argument*/NULL);
		V_7 = L_18;
		float L_19 = ((&V_7)->___x_1);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		*((float*)(float*)SZArrayLdElema(L_17, 0)) = (float)L_19;
		SingleU5BU5D_t211* L_20 = L_17;
		Vector3_t4  L_21 = Bounds_get_extents_m703((&V_1), /*hidden argument*/NULL);
		V_8 = L_21;
		float L_22 = ((&V_8)->___y_2);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 1);
		*((float*)(float*)SZArrayLdElema(L_20, 1)) = (float)L_22;
		SingleU5BU5D_t211* L_23 = L_20;
		Vector3_t4  L_24 = Bounds_get_extents_m703((&V_1), /*hidden argument*/NULL);
		V_9 = L_24;
		float L_25 = ((&V_9)->___z_3);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		*((float*)(float*)SZArrayLdElema(L_23, 2)) = (float)L_25;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_26 = Mathf_Max_m704(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		V_6 = L_26;
		float L_27 = V_6;
		return L_27;
	}
}
// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatfMethodDeclarations.h"

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_2.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_2MethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"


// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::.ctor()
extern "C" void AxisTouchButton__ctor_m55 (AxisTouchButton_t29 * __this, const MethodInfo* method)
{
	{
		__this->___axisName_2 = (String_t*) &_stringLiteral3;
		__this->___axisValue_3 = (1.0f);
		__this->___responseSpeed_4 = (3.0f);
		__this->___returnToCentreSpeed_5 = (3.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnEnable()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern TypeInfo* VirtualAxis_t30_il2cpp_TypeInfo_var;
extern "C" void AxisTouchButton_OnEnable_m56 (AxisTouchButton_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		VirtualAxis_t30_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___axisName_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		bool L_1 = CrossPlatformInputManager_AxisExists_m93(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_2 = (__this->___axisName_2);
		VirtualAxis_t30 * L_3 = (VirtualAxis_t30 *)il2cpp_codegen_object_new (VirtualAxis_t30_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m69(L_3, L_2, /*hidden argument*/NULL);
		__this->___m_Axis_7 = L_3;
		VirtualAxis_t30 * L_4 = (__this->___m_Axis_7);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m95(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_0031:
	{
		String_t* L_5 = (__this->___axisName_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualAxis_t30 * L_6 = CrossPlatformInputManager_VirtualAxisReference_m99(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->___m_Axis_7 = L_6;
	}

IL_0042:
	{
		AxisTouchButton_FindPairedButton_m57(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::FindPairedButton()
extern const Il2CppType* AxisTouchButton_t29_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* AxisTouchButtonU5BU5D_t229_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void AxisTouchButton_FindPairedButton_m57 (AxisTouchButton_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AxisTouchButton_t29_0_0_0_var = il2cpp_codegen_type_from_index(21);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		AxisTouchButtonU5BU5D_t229_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	AxisTouchButtonU5BU5D_t229* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m705(NULL /*static, unused*/, LoadTypeToken(AxisTouchButton_t29_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t230* L_1 = Object_FindObjectsOfType_m706(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((AxisTouchButtonU5BU5D_t229*)IsInst(L_1, AxisTouchButtonU5BU5D_t229_il2cpp_TypeInfo_var));
		AxisTouchButtonU5BU5D_t229* L_2 = V_0;
		if (!L_2)
		{
			goto IL_005e;
		}
	}
	{
		V_1 = 0;
		goto IL_0055;
	}

IL_0022:
	{
		AxisTouchButtonU5BU5D_t229* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck((*(AxisTouchButton_t29 **)(AxisTouchButton_t29 **)SZArrayLdElema(L_3, L_5)));
		String_t* L_6 = ((*(AxisTouchButton_t29 **)(AxisTouchButton_t29 **)SZArrayLdElema(L_3, L_5))->___axisName_2);
		String_t* L_7 = (__this->___axisName_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m636(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0051;
		}
	}
	{
		AxisTouchButtonU5BU5D_t229* L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		bool L_12 = Object_op_Inequality_m623(NULL /*static, unused*/, (*(AxisTouchButton_t29 **)(AxisTouchButton_t29 **)SZArrayLdElema(L_9, L_11)), __this, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0051;
		}
	}
	{
		AxisTouchButtonU5BU5D_t229* L_13 = V_0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		__this->___m_PairedWith_6 = (*(AxisTouchButton_t29 **)(AxisTouchButton_t29 **)SZArrayLdElema(L_13, L_15));
	}

IL_0051:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0055:
	{
		int32_t L_17 = V_1;
		AxisTouchButtonU5BU5D_t229* L_18 = V_0;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)(((Array_t *)L_18)->max_length))))))
		{
			goto IL_0022;
		}
	}

IL_005e:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnDisable()
extern "C" void AxisTouchButton_OnDisable_m58 (AxisTouchButton_t29 * __this, const MethodInfo* method)
{
	{
		VirtualAxis_t30 * L_0 = (__this->___m_Axis_7);
		NullCheck(L_0);
		VirtualAxis_Remove_m75(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void AxisTouchButton_OnPointerDown_m59 (AxisTouchButton_t29 * __this, PointerEventData_t215 * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		AxisTouchButton_t29 * L_0 = (__this->___m_PairedWith_6);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		AxisTouchButton_FindPairedButton_m57(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		VirtualAxis_t30 * L_2 = (__this->___m_Axis_7);
		VirtualAxis_t30 * L_3 = (__this->___m_Axis_7);
		NullCheck(L_3);
		float L_4 = VirtualAxis_get_GetValue_m77(L_3, /*hidden argument*/NULL);
		float L_5 = (__this->___axisValue_3);
		float L_6 = (__this->___responseSpeed_4);
		float L_7 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_8 = Mathf_MoveTowards_m707(NULL /*static, unused*/, L_4, L_5, ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtualAxis_Update_m76(L_2, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void AxisTouchButton_OnPointerUp_m60 (AxisTouchButton_t29 * __this, PointerEventData_t215 * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		VirtualAxis_t30 * L_0 = (__this->___m_Axis_7);
		VirtualAxis_t30 * L_1 = (__this->___m_Axis_7);
		NullCheck(L_1);
		float L_2 = VirtualAxis_get_GetValue_m77(L_1, /*hidden argument*/NULL);
		float L_3 = (__this->___responseSpeed_4);
		float L_4 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_5 = Mathf_MoveTowards_m707(NULL /*static, unused*/, L_2, (0.0f), ((float)((float)L_3*(float)L_4)), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtualAxis_Update_m76(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.CrossPlatformInput.ButtonHandler
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.ButtonHandler
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_0MethodDeclarations.h"



// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::.ctor()
extern "C" void ButtonHandler__ctor_m61 (ButtonHandler_t31 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::OnEnable()
extern "C" void ButtonHandler_OnEnable_m62 (ButtonHandler_t31 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetDownState()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void ButtonHandler_SetDownState_m63 (ButtonHandler_t31 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___Name_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetButtonDown_m106(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetUpState()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void ButtonHandler_SetUpState_m64 (ButtonHandler_t31 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___Name_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetButtonUp_m107(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisPositiveState()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void ButtonHandler_SetAxisPositiveState_m65 (ButtonHandler_t31 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___Name_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetAxisPositive_m108(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNeutralState()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void ButtonHandler_SetAxisNeutralState_m66 (ButtonHandler_t31 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___Name_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetAxisZero_m110(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNegativeState()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void ButtonHandler_SetAxisNegativeState_m67 (ButtonHandler_t31 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___Name_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetAxisNegative_m109(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::Update()
extern "C" void ButtonHandler_Update_m68 (ButtonHandler_t31 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_1.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_1MethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif



// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::.ctor(System.String)
extern "C" void VirtualAxis__ctor_m69 (VirtualAxis_t30 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		VirtualAxis__ctor_m70(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::.ctor(System.String,System.Boolean)
extern "C" void VirtualAxis__ctor_m70 (VirtualAxis_t30 * __this, String_t* ___name, bool ___matchToInputSettings, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		VirtualAxis_set_name_m72(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___matchToInputSettings;
		VirtualAxis_set_matchWithInputManager_m74(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_name()
extern "C" String_t* VirtualAxis_get_name_m71 (VirtualAxis_t30 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CnameU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::set_name(System.String)
extern "C" void VirtualAxis_set_name_m72 (VirtualAxis_t30 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CnameU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_matchWithInputManager()
extern "C" bool VirtualAxis_get_matchWithInputManager_m73 (VirtualAxis_t30 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CmatchWithInputManagerU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::set_matchWithInputManager(System.Boolean)
extern "C" void VirtualAxis_set_matchWithInputManager_m74 (VirtualAxis_t30 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CmatchWithInputManagerU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::Remove()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void VirtualAxis_Remove_m75 (VirtualAxis_t30 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = VirtualAxis_get_name_m71(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_UnRegisterVirtualAxis_m97(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::Update(System.Single)
extern "C" void VirtualAxis_Update_m76 (VirtualAxis_t30 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Value_0 = L_0;
		return;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_GetValue()
extern "C" float VirtualAxis_get_GetValue_m77 (VirtualAxis_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Value_0);
		return L_0;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_GetValueRaw()
extern "C" float VirtualAxis_get_GetValueRaw_m78 (VirtualAxis_t30 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Value_0);
		return L_0;
	}
}
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_3.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_3MethodDeclarations.h"



// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::.ctor(System.String)
extern "C" void VirtualButton__ctor_m79 (VirtualButton_t33 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		VirtualButton__ctor_m80(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::.ctor(System.String,System.Boolean)
extern "C" void VirtualButton__ctor_m80 (VirtualButton_t33 * __this, String_t* ___name, bool ___matchToInputSettings, const MethodInfo* method)
{
	{
		__this->___m_LastPressedFrame_0 = ((int32_t)-5);
		__this->___m_ReleasedFrame_1 = ((int32_t)-5);
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		VirtualButton_set_name_m82(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___matchToInputSettings;
		VirtualButton_set_matchWithInputManager_m84(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_name()
extern "C" String_t* VirtualButton_get_name_m81 (VirtualButton_t33 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CnameU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::set_name(System.String)
extern "C" void VirtualButton_set_name_m82 (VirtualButton_t33 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CnameU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_matchWithInputManager()
extern "C" bool VirtualButton_get_matchWithInputManager_m83 (VirtualButton_t33 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CmatchWithInputManagerU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::set_matchWithInputManager(System.Boolean)
extern "C" void VirtualButton_set_matchWithInputManager_m84 (VirtualButton_t33 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CmatchWithInputManagerU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Pressed()
extern "C" void VirtualButton_Pressed_m85 (VirtualButton_t33 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Pressed_2);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->___m_Pressed_2 = 1;
		int32_t L_1 = Time_get_frameCount_m708(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_LastPressedFrame_0 = L_1;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Released()
extern "C" void VirtualButton_Released_m86 (VirtualButton_t33 * __this, const MethodInfo* method)
{
	{
		__this->___m_Pressed_2 = 0;
		int32_t L_0 = Time_get_frameCount_m708(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_ReleasedFrame_1 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Remove()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void VirtualButton_Remove_m87 (VirtualButton_t33 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = VirtualButton_get_name_m81(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_UnRegisterVirtualButton_m98(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButton()
extern "C" bool VirtualButton_get_GetButton_m88 (VirtualButton_t33 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Pressed_2);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButtonDown()
extern "C" bool VirtualButton_get_GetButtonDown_m89 (VirtualButton_t33 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_LastPressedFrame_0);
		int32_t L_1 = Time_get_frameCount_m708(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((((int32_t)((int32_t)((int32_t)L_0-(int32_t)L_1))) == ((int32_t)(-1)))? 1 : 0);
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButtonUp()
extern "C" bool VirtualButton_get_GetButtonUp_m90 (VirtualButton_t33 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_ReleasedFrame_1);
		int32_t L_1 = Time_get_frameCount_m708(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)((int32_t)((int32_t)L_1-(int32_t)1))))? 1 : 0);
	}
}
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_4.h"
#ifndef _MSC_VER
#else
#endif

// UnityStandardAssets.CrossPlatformInput.VirtualInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_10.h"
// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_9.h"
// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_11.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// UnityStandardAssets.CrossPlatformInput.VirtualInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_10MethodDeclarations.h"
// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_9MethodDeclarations.h"
// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_11MethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"


// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::.cctor()
extern TypeInfo* MobileInput_t40_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern TypeInfo* StandaloneInput_t41_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager__cctor_m91 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MobileInput_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		StandaloneInput_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	{
		MobileInput_t40 * L_0 = (MobileInput_t40 *)il2cpp_codegen_object_new (MobileInput_t40_il2cpp_TypeInfo_var);
		MobileInput__ctor_m131(L_0, /*hidden argument*/NULL);
		((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___s_TouchInput_1 = L_0;
		StandaloneInput_t41 * L_1 = (StandaloneInput_t41 *)il2cpp_codegen_object_new (StandaloneInput_t41_il2cpp_TypeInfo_var);
		StandaloneInput__ctor_m145(L_1, /*hidden argument*/NULL);
		((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___s_HardwareInput_2 = L_1;
		VirtualInput_t34 * L_2 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___s_TouchInput_1;
		((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0 = L_2;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SwitchActiveInputMethod(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SwitchActiveInputMethod_m92 (Object_t * __this /* static, unused */, int32_t ___activeInputMethod, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___activeInputMethod;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0023;
		}
	}
	{
		goto IL_0032;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_3 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___s_HardwareInput_2;
		((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0 = L_3;
		goto IL_0032;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_4 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___s_TouchInput_1;
		((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0 = L_4;
		goto IL_0032;
	}

IL_0032:
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::AxisExists(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" bool CrossPlatformInputManager_AxisExists_m93 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = VirtualInput_AxisExists_m173(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::ButtonExists(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" bool CrossPlatformInputManager_ButtonExists_m94 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = VirtualInput_ButtonExists_m174(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_RegisterVirtualAxis_m95 (Object_t * __this /* static, unused */, VirtualAxis_t30 * ___axis, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		VirtualAxis_t30 * L_1 = ___axis;
		NullCheck(L_0);
		VirtualInput_RegisterVirtualAxis_m175(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_RegisterVirtualButton_m96 (Object_t * __this /* static, unused */, VirtualButton_t33 * ___button, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		VirtualButton_t33 * L_1 = ___button;
		NullCheck(L_0);
		VirtualInput_RegisterVirtualButton_m176(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualAxis(System.String)
extern TypeInfo* ArgumentNullException_t231_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_UnRegisterVirtualAxis_m97 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t231_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t231 * L_1 = (ArgumentNullException_t231 *)il2cpp_codegen_object_new (ArgumentNullException_t231_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m709(L_1, (String_t*) &_stringLiteral12, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_2 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_3 = ___name;
		NullCheck(L_2);
		VirtualInput_UnRegisterVirtualAxis_m177(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualButton(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_UnRegisterVirtualButton_m98 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtualInput_UnRegisterVirtualButton_m178(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::VirtualAxisReference(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" VirtualAxis_t30 * CrossPlatformInputManager_VirtualAxisReference_m99 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtualAxis_t30 * L_2 = VirtualInput_VirtualAxisReference_m179(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" float CrossPlatformInputManager_GetAxis_m100 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		float L_1 = CrossPlatformInputManager_GetAxis_m102(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxisRaw(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" float CrossPlatformInputManager_GetAxisRaw_m101 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		float L_1 = CrossPlatformInputManager_GetAxis_m102(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String,System.Boolean)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" float CrossPlatformInputManager_GetAxis_m102 (Object_t * __this /* static, unused */, String_t* ___name, bool ___raw, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		bool L_2 = ___raw;
		NullCheck(L_0);
		float L_3 = (float)VirtFuncInvoker2< float, String_t*, bool >::Invoke(4 /* System.Single UnityStandardAssets.CrossPlatformInput.VirtualInput::GetAxis(System.String,System.Boolean) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButton(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" bool CrossPlatformInputManager_GetButton_m103 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(5 /* System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButton(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonDown(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" bool CrossPlatformInputManager_GetButtonDown_m104 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(6 /* System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonDown(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonUp(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" bool CrossPlatformInputManager_GetButtonUp_m105 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(7 /* System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonUp(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonDown(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetButtonDown_m106 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(8 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonDown(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonUp(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetButtonUp_m107 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonUp(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisPositive(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetAxisPositive_m108 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisPositive(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisNegative(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetAxisNegative_m109 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisNegative(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisZero(System.String)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetAxisZero_m110 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(12 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisZero(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxis(System.String,System.Single)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetAxis_m111 (Object_t * __this /* static, unused */, String_t* ___name, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		float L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, float >::Invoke(13 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxis(System.String,System.Single) */, L_0, L_1, L_2);
		return;
	}
}
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::get_mousePosition()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" Vector3_t4  CrossPlatformInputManager_get_mousePosition_m112 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		NullCheck(L_0);
		Vector3_t4  L_1 = (Vector3_t4 )VirtFuncInvoker0< Vector3_t4  >::Invoke(14 /* UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::MousePosition() */, L_0);
		return L_1;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionX(System.Single)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetVirtualMousePositionX_m113 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		float L_1 = ___f;
		NullCheck(L_0);
		VirtualInput_SetVirtualMousePositionX_m180(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionY(System.Single)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetVirtualMousePositionY_m114 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		float L_1 = ___f;
		NullCheck(L_0);
		VirtualInput_SetVirtualMousePositionY_m181(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionZ(System.Single)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetVirtualMousePositionZ_m115 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		VirtualInput_t34 * L_0 = ((CrossPlatformInputManager_t35_StaticFields*)CrossPlatformInputManager_t35_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		float L_1 = ___f;
		NullCheck(L_0);
		VirtualInput_SetVirtualMousePositionZ_m182(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_5.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_5MethodDeclarations.h"



// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::.ctor()
extern "C" void InputAxisScrollbar__ctor_m116 (InputAxisScrollbar_t36 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::Update()
extern "C" void InputAxisScrollbar_Update_m117 (InputAxisScrollbar_t36 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::HandleInput(System.Single)
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void InputAxisScrollbar_HandleInput_m118 (InputAxisScrollbar_t36 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___axis_2);
		float L_1 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetAxis_m111(NULL /*static, unused*/, L_0, ((float)((float)((float)((float)L_1*(float)(2.0f)))-(float)(1.0f))), /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_6.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_6MethodDeclarations.h"



// UnityStandardAssets.CrossPlatformInput.Joystick
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_7.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.Joystick
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_7MethodDeclarations.h"

// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventDataMethodDeclarations.h"


// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::.ctor()
extern "C" void Joystick__ctor_m119 (Joystick_t38 * __this, const MethodInfo* method)
{
	{
		__this->___MovementRange_2 = ((int32_t)100);
		__this->___horizontalAxisName_4 = (String_t*) &_stringLiteral3;
		__this->___verticalAxisName_5 = (String_t*) &_stringLiteral13;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnEnable()
extern "C" void Joystick_OnEnable_m120 (Joystick_t38 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		__this->___m_StartPos_6 = L_1;
		Joystick_CreateVirtualAxes_m122(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::UpdateVirtualAxes(UnityEngine.Vector3)
extern "C" void Joystick_UpdateVirtualAxes_m121 (Joystick_t38 * __this, Vector3_t4  ___value, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	{
		Vector3_t4  L_0 = (__this->___m_StartPos_6);
		Vector3_t4  L_1 = ___value;
		Vector3_t4  L_2 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = ((&V_0)->___y_2);
		(&V_0)->___y_2 = ((-L_3));
		Vector3_t4  L_4 = V_0;
		int32_t L_5 = (__this->___MovementRange_2);
		Vector3_t4  L_6 = Vector3_op_Division_m710(NULL /*static, unused*/, L_4, (((float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		bool L_7 = (__this->___m_UseX_7);
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		VirtualAxis_t30 * L_8 = (__this->___m_HorizontalVirtualAxis_9);
		float L_9 = ((&V_0)->___x_1);
		NullCheck(L_8);
		VirtualAxis_Update_m76(L_8, ((-L_9)), /*hidden argument*/NULL);
	}

IL_0048:
	{
		bool L_10 = (__this->___m_UseY_8);
		if (!L_10)
		{
			goto IL_0065;
		}
	}
	{
		VirtualAxis_t30 * L_11 = (__this->___m_VerticalVirtualAxis_10);
		float L_12 = ((&V_0)->___y_2);
		NullCheck(L_11);
		VirtualAxis_Update_m76(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::CreateVirtualAxes()
extern TypeInfo* VirtualAxis_t30_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void Joystick_CreateVirtualAxes_m122 (Joystick_t38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualAxis_t30_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	Joystick_t38 * G_B2_0 = {0};
	Joystick_t38 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Joystick_t38 * G_B3_1 = {0};
	Joystick_t38 * G_B5_0 = {0};
	Joystick_t38 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Joystick_t38 * G_B6_1 = {0};
	{
		int32_t L_0 = (__this->___axesToUse_3);
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = (__this->___axesToUse_3);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)1))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0018:
	{
		NullCheck(G_B3_1);
		G_B3_1->___m_UseX_7 = G_B3_0;
		int32_t L_2 = (__this->___axesToUse_3);
		G_B4_0 = __this;
		if (!L_2)
		{
			G_B5_0 = __this;
			goto IL_0034;
		}
	}
	{
		int32_t L_3 = (__this->___axesToUse_3);
		G_B6_0 = ((((int32_t)L_3) == ((int32_t)2))? 1 : 0);
		G_B6_1 = G_B4_0;
		goto IL_0035;
	}

IL_0034:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
	}

IL_0035:
	{
		NullCheck(G_B6_1);
		G_B6_1->___m_UseY_8 = G_B6_0;
		bool L_4 = (__this->___m_UseX_7);
		if (!L_4)
		{
			goto IL_0061;
		}
	}
	{
		String_t* L_5 = (__this->___horizontalAxisName_4);
		VirtualAxis_t30 * L_6 = (VirtualAxis_t30 *)il2cpp_codegen_object_new (VirtualAxis_t30_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m69(L_6, L_5, /*hidden argument*/NULL);
		__this->___m_HorizontalVirtualAxis_9 = L_6;
		VirtualAxis_t30 * L_7 = (__this->___m_HorizontalVirtualAxis_9);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m95(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_8 = (__this->___m_UseY_8);
		if (!L_8)
		{
			goto IL_0088;
		}
	}
	{
		String_t* L_9 = (__this->___verticalAxisName_5);
		VirtualAxis_t30 * L_10 = (VirtualAxis_t30 *)il2cpp_codegen_object_new (VirtualAxis_t30_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m69(L_10, L_9, /*hidden argument*/NULL);
		__this->___m_VerticalVirtualAxis_10 = L_10;
		VirtualAxis_t30 * L_11 = (__this->___m_VerticalVirtualAxis_10);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m95(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0088:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void Joystick_OnDrag_m123 (Joystick_t38 * __this, PointerEventData_t215 * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector2_t6  V_3 = {0};
	Vector2_t6  V_4 = {0};
	{
		Vector3_t4  L_0 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = (__this->___m_UseX_7);
		if (!L_1)
		{
			goto IL_004a;
		}
	}
	{
		PointerEventData_t215 * L_2 = ___data;
		NullCheck(L_2);
		Vector2_t6  L_3 = PointerEventData_get_position_m711(L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		float L_4 = ((&V_3)->___x_1);
		Vector3_t4 * L_5 = &(__this->___m_StartPos_6);
		float L_6 = (L_5->___x_1);
		V_1 = (((int32_t)((float)((float)L_4-(float)L_6))));
		int32_t L_7 = V_1;
		int32_t L_8 = (__this->___MovementRange_2);
		int32_t L_9 = (__this->___MovementRange_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_Clamp_m712(NULL /*static, unused*/, L_7, ((-L_8)), L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		int32_t L_11 = V_1;
		(&V_0)->___x_1 = (((float)L_11));
	}

IL_004a:
	{
		bool L_12 = (__this->___m_UseY_8);
		if (!L_12)
		{
			goto IL_008f;
		}
	}
	{
		PointerEventData_t215 * L_13 = ___data;
		NullCheck(L_13);
		Vector2_t6  L_14 = PointerEventData_get_position_m711(L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		float L_15 = ((&V_4)->___y_2);
		Vector3_t4 * L_16 = &(__this->___m_StartPos_6);
		float L_17 = (L_16->___y_2);
		V_2 = (((int32_t)((float)((float)L_15-(float)L_17))));
		int32_t L_18 = V_2;
		int32_t L_19 = (__this->___MovementRange_2);
		int32_t L_20 = (__this->___MovementRange_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_21 = Mathf_Clamp_m712(NULL /*static, unused*/, L_18, ((-L_19)), L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		int32_t L_22 = V_2;
		(&V_0)->___y_2 = (((float)L_22));
	}

IL_008f:
	{
		Transform_t1 * L_23 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4 * L_24 = &(__this->___m_StartPos_6);
		float L_25 = (L_24->___x_1);
		float L_26 = ((&V_0)->___x_1);
		Vector3_t4 * L_27 = &(__this->___m_StartPos_6);
		float L_28 = (L_27->___y_2);
		float L_29 = ((&V_0)->___y_2);
		Vector3_t4 * L_30 = &(__this->___m_StartPos_6);
		float L_31 = (L_30->___z_3);
		float L_32 = ((&V_0)->___z_3);
		Vector3_t4  L_33 = {0};
		Vector3__ctor_m612(&L_33, ((float)((float)L_25+(float)L_26)), ((float)((float)L_28+(float)L_29)), ((float)((float)L_31+(float)L_32)), /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_position_m607(L_23, L_33, /*hidden argument*/NULL);
		Transform_t1 * L_34 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		Vector3_t4  L_35 = Transform_get_position_m593(L_34, /*hidden argument*/NULL);
		Joystick_UpdateVirtualAxes_m121(__this, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" void Joystick_OnPointerUp_m124 (Joystick_t38 * __this, PointerEventData_t215 * ___data, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4  L_1 = (__this->___m_StartPos_6);
		NullCheck(L_0);
		Transform_set_position_m607(L_0, L_1, /*hidden argument*/NULL);
		Vector3_t4  L_2 = (__this->___m_StartPos_6);
		Joystick_UpdateVirtualAxes_m121(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" void Joystick_OnPointerDown_m125 (Joystick_t38 * __this, PointerEventData_t215 * ___data, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDisable()
extern "C" void Joystick_OnDisable_m126 (Joystick_t38 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_UseX_7);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		VirtualAxis_t30 * L_1 = (__this->___m_HorizontalVirtualAxis_9);
		NullCheck(L_1);
		VirtualAxis_Remove_m75(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		bool L_2 = (__this->___m_UseY_8);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		VirtualAxis_t30 * L_3 = (__this->___m_VerticalVirtualAxis_10);
		NullCheck(L_3);
		VirtualAxis_Remove_m75(L_3, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// UnityStandardAssets.CrossPlatformInput.MobileControlRig
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_8.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.MobileControlRig
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_8MethodDeclarations.h"



// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::.ctor()
extern "C" void MobileControlRig__ctor_m127 (MobileControlRig_t39 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::OnEnable()
extern "C" void MobileControlRig_OnEnable_m128 (MobileControlRig_t39 * __this, const MethodInfo* method)
{
	{
		MobileControlRig_CheckEnableControlRig_m129(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::CheckEnableControlRig()
extern "C" void MobileControlRig_CheckEnableControlRig_m129 (MobileControlRig_t39 * __this, const MethodInfo* method)
{
	{
		MobileControlRig_EnableControlRig_m130(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::EnableControlRig(System.Boolean)
extern TypeInfo* IEnumerator_t217_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t1_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t233_il2cpp_TypeInfo_var;
extern "C" void MobileControlRig_EnableControlRig_m130 (MobileControlRig_t39 * __this, bool ___enabled, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t217_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		Transform_t1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		IDisposable_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1 * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_0);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0029;
		}

IL_0011:
		{
			Object_t * L_2 = V_1;
			NullCheck(L_2);
			Object_t * L_3 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t217_il2cpp_TypeInfo_var, L_2);
			V_0 = ((Transform_t1 *)Castclass(L_3, Transform_t1_il2cpp_TypeInfo_var));
			Transform_t1 * L_4 = V_0;
			NullCheck(L_4);
			GameObject_t78 * L_5 = Component_get_gameObject_m622(L_4, /*hidden argument*/NULL);
			bool L_6 = ___enabled;
			NullCheck(L_5);
			GameObject_SetActive_m713(L_5, L_6, /*hidden argument*/NULL);
		}

IL_0029:
		{
			Object_t * L_7 = V_1;
			NullCheck(L_7);
			bool L_8 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t217_il2cpp_TypeInfo_var, L_7);
			if (L_8)
			{
				goto IL_0011;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x4B, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		{
			Object_t * L_9 = V_1;
			V_2 = ((Object_t *)IsInst(L_9, IDisposable_t233_il2cpp_TypeInfo_var));
			Object_t * L_10 = V_2;
			if (L_10)
			{
				goto IL_0044;
			}
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(57)
		}

IL_0044:
		{
			Object_t * L_11 = V_2;
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t233_il2cpp_TypeInfo_var, L_11);
			IL2CPP_END_FINALLY(57)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_004b:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_0.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_0MethodDeclarations.h"


// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::.ctor()
extern "C" void MobileInput__ctor_m131 (MobileInput_t40 * __this, const MethodInfo* method)
{
	{
		VirtualInput__ctor_m170(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddButton(System.String)
extern TypeInfo* VirtualButton_t33_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void MobileInput_AddButton_m132 (MobileInput_t40 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualButton_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		VirtualButton_t33 * L_1 = (VirtualButton_t33 *)il2cpp_codegen_object_new (VirtualButton_t33_il2cpp_TypeInfo_var);
		VirtualButton__ctor_m79(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualButton_m96(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddAxes(System.String)
extern TypeInfo* VirtualAxis_t30_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void MobileInput_AddAxes_m133 (MobileInput_t40 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualAxis_t30_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		VirtualAxis_t30 * L_1 = (VirtualAxis_t30 *)il2cpp_codegen_object_new (VirtualAxis_t30_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m69(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m95(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetAxis(System.String,System.Boolean)
extern "C" float MobileInput_GetAxis_m134 (MobileInput_t40 * __this, String_t* ___name, bool ___raw, const MethodInfo* method)
{
	{
		Dictionary_2_t50 * L_0 = (((VirtualInput_t34 *)__this)->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddAxes_m133(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t50 * L_4 = (((VirtualInput_t34 *)__this)->___m_VirtualAxes_0);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualAxis_t30 * L_6 = (VirtualAxis_t30 *)VirtFuncInvoker1< VirtualAxis_t30 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		float L_7 = VirtualAxis_get_GetValue_m77(L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonDown(System.String)
extern "C" void MobileInput_SetButtonDown_m135 (MobileInput_t40 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t51 * L_0 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddButton_m132(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t51 * L_4 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualButton_t33 * L_6 = (VirtualButton_t33 *)VirtFuncInvoker1< VirtualButton_t33 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		VirtualButton_Pressed_m85(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonUp(System.String)
extern "C" void MobileInput_SetButtonUp_m136 (MobileInput_t40 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t51 * L_0 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddButton_m132(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t51 * L_4 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualButton_t33 * L_6 = (VirtualButton_t33 *)VirtFuncInvoker1< VirtualButton_t33 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		VirtualButton_Released_m86(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisPositive(System.String)
extern "C" void MobileInput_SetAxisPositive_m137 (MobileInput_t40 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t50 * L_0 = (((VirtualInput_t34 *)__this)->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddAxes_m133(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t50 * L_4 = (((VirtualInput_t34 *)__this)->___m_VirtualAxes_0);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualAxis_t30 * L_6 = (VirtualAxis_t30 *)VirtFuncInvoker1< VirtualAxis_t30 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		VirtualAxis_Update_m76(L_6, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisNegative(System.String)
extern "C" void MobileInput_SetAxisNegative_m138 (MobileInput_t40 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t50 * L_0 = (((VirtualInput_t34 *)__this)->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddAxes_m133(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t50 * L_4 = (((VirtualInput_t34 *)__this)->___m_VirtualAxes_0);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualAxis_t30 * L_6 = (VirtualAxis_t30 *)VirtFuncInvoker1< VirtualAxis_t30 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		VirtualAxis_Update_m76(L_6, (-1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisZero(System.String)
extern "C" void MobileInput_SetAxisZero_m139 (MobileInput_t40 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t50 * L_0 = (((VirtualInput_t34 *)__this)->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddAxes_m133(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t50 * L_4 = (((VirtualInput_t34 *)__this)->___m_VirtualAxes_0);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualAxis_t30 * L_6 = (VirtualAxis_t30 *)VirtFuncInvoker1< VirtualAxis_t30 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		VirtualAxis_Update_m76(L_6, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxis(System.String,System.Single)
extern "C" void MobileInput_SetAxis_m140 (MobileInput_t40 * __this, String_t* ___name, float ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t50 * L_0 = (((VirtualInput_t34 *)__this)->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddAxes_m133(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t50 * L_4 = (((VirtualInput_t34 *)__this)->___m_VirtualAxes_0);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualAxis_t30 * L_6 = (VirtualAxis_t30 *)VirtFuncInvoker1< VirtualAxis_t30 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_4, L_5);
		float L_7 = ___value;
		NullCheck(L_6);
		VirtualAxis_Update_m76(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonDown(System.String)
extern "C" bool MobileInput_GetButtonDown_m141 (MobileInput_t40 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t51 * L_0 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t51 * L_3 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtualButton_t33 * L_5 = (VirtualButton_t33 *)VirtFuncInvoker1< VirtualButton_t33 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_3, L_4);
		NullCheck(L_5);
		bool L_6 = VirtualButton_get_GetButtonDown_m89(L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0023:
	{
		String_t* L_7 = ___name;
		MobileInput_AddButton_m132(__this, L_7, /*hidden argument*/NULL);
		Dictionary_2_t51 * L_8 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_9 = ___name;
		NullCheck(L_8);
		VirtualButton_t33 * L_10 = (VirtualButton_t33 *)VirtFuncInvoker1< VirtualButton_t33 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_8, L_9);
		NullCheck(L_10);
		bool L_11 = VirtualButton_get_GetButtonDown_m89(L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonUp(System.String)
extern "C" bool MobileInput_GetButtonUp_m142 (MobileInput_t40 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t51 * L_0 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t51 * L_3 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtualButton_t33 * L_5 = (VirtualButton_t33 *)VirtFuncInvoker1< VirtualButton_t33 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_3, L_4);
		NullCheck(L_5);
		bool L_6 = VirtualButton_get_GetButtonUp_m90(L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0023:
	{
		String_t* L_7 = ___name;
		MobileInput_AddButton_m132(__this, L_7, /*hidden argument*/NULL);
		Dictionary_2_t51 * L_8 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_9 = ___name;
		NullCheck(L_8);
		VirtualButton_t33 * L_10 = (VirtualButton_t33 *)VirtFuncInvoker1< VirtualButton_t33 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_8, L_9);
		NullCheck(L_10);
		bool L_11 = VirtualButton_get_GetButtonUp_m90(L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButton(System.String)
extern "C" bool MobileInput_GetButton_m143 (MobileInput_t40 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t51 * L_0 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t51 * L_3 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtualButton_t33 * L_5 = (VirtualButton_t33 *)VirtFuncInvoker1< VirtualButton_t33 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_3, L_4);
		NullCheck(L_5);
		bool L_6 = VirtualButton_get_GetButton_m88(L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0023:
	{
		String_t* L_7 = ___name;
		MobileInput_AddButton_m132(__this, L_7, /*hidden argument*/NULL);
		Dictionary_2_t51 * L_8 = (((VirtualInput_t34 *)__this)->___m_VirtualButtons_1);
		String_t* L_9 = ___name;
		NullCheck(L_8);
		VirtualButton_t33 * L_10 = (VirtualButton_t33 *)VirtFuncInvoker1< VirtualButton_t33 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_8, L_9);
		NullCheck(L_10);
		bool L_11 = VirtualButton_get_GetButton_m88(L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::MousePosition()
extern "C" Vector3_t4  MobileInput_MousePosition_m144 (MobileInput_t40 * __this, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = VirtualInput_get_virtualMousePosition_m171(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Exception
#include "mscorlib_System_Exception.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"


// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::.ctor()
extern "C" void StandaloneInput__ctor_m145 (StandaloneInput_t41 * __this, const MethodInfo* method)
{
	{
		VirtualInput__ctor_m170(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetAxis(System.String,System.Boolean)
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" float StandaloneInput_GetAxis_m146 (StandaloneInput_t41 * __this, String_t* ___name, bool ___raw, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___raw;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxisRaw_m714(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_0011:
	{
		String_t* L_3 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		float L_4 = Input_GetAxis_m715(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButton(System.String)
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" bool StandaloneInput_GetButton_m147 (StandaloneInput_t41 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButton_m716(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonDown(System.String)
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" bool StandaloneInput_GetButtonDown_m148 (StandaloneInput_t41 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButtonDown_m717(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonUp(System.String)
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" bool StandaloneInput_GetButtonUp_m149 (StandaloneInput_t41 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButtonUp_m718(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonDown(System.String)
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern "C" void StandaloneInput_SetButtonDown_m150 (StandaloneInput_t41 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t232 * L_0 = (Exception_t232 *)il2cpp_codegen_object_new (Exception_t232_il2cpp_TypeInfo_var);
		Exception__ctor_m719(L_0, (String_t*) &_stringLiteral14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonUp(System.String)
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern "C" void StandaloneInput_SetButtonUp_m151 (StandaloneInput_t41 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t232 * L_0 = (Exception_t232 *)il2cpp_codegen_object_new (Exception_t232_il2cpp_TypeInfo_var);
		Exception__ctor_m719(L_0, (String_t*) &_stringLiteral14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisPositive(System.String)
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern "C" void StandaloneInput_SetAxisPositive_m152 (StandaloneInput_t41 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t232 * L_0 = (Exception_t232 *)il2cpp_codegen_object_new (Exception_t232_il2cpp_TypeInfo_var);
		Exception__ctor_m719(L_0, (String_t*) &_stringLiteral14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisNegative(System.String)
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern "C" void StandaloneInput_SetAxisNegative_m153 (StandaloneInput_t41 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t232 * L_0 = (Exception_t232 *)il2cpp_codegen_object_new (Exception_t232_il2cpp_TypeInfo_var);
		Exception__ctor_m719(L_0, (String_t*) &_stringLiteral14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisZero(System.String)
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern "C" void StandaloneInput_SetAxisZero_m154 (StandaloneInput_t41 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t232 * L_0 = (Exception_t232 *)il2cpp_codegen_object_new (Exception_t232_il2cpp_TypeInfo_var);
		Exception__ctor_m719(L_0, (String_t*) &_stringLiteral14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxis(System.String,System.Single)
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern "C" void StandaloneInput_SetAxis_m155 (StandaloneInput_t41 * __this, String_t* ___name, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t232 * L_0 = (Exception_t232 *)il2cpp_codegen_object_new (Exception_t232_il2cpp_TypeInfo_var);
		Exception__ctor_m719(L_0, (String_t*) &_stringLiteral14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::MousePosition()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" Vector3_t4  StandaloneInput_MousePosition_m156 (StandaloneInput_t41 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_0 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_12.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_12MethodDeclarations.h"



// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_13.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_13MethodDeclarations.h"



// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_14.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_14MethodDeclarations.h"



// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::.ctor()
extern "C" void AxisMapping__ctor_m157 (AxisMapping_t44 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.CrossPlatformInput.TiltInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_15.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.TiltInput
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_15MethodDeclarations.h"

// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"


// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::.ctor()
extern "C" void TiltInput__ctor_m158 (TiltInput_t45 * __this, const MethodInfo* method)
{
	{
		__this->___fullTiltAngle_4 = (25.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnEnable()
extern TypeInfo* VirtualAxis_t30_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void TiltInput_OnEnable_m159 (TiltInput_t45 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualAxis_t30_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		AxisMapping_t44 * L_0 = (__this->___mapping_2);
		NullCheck(L_0);
		int32_t L_1 = (L_0->___type_0);
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		AxisMapping_t44 * L_2 = (__this->___mapping_2);
		NullCheck(L_2);
		String_t* L_3 = (L_2->___axisName_1);
		VirtualAxis_t30 * L_4 = (VirtualAxis_t30 *)il2cpp_codegen_object_new (VirtualAxis_t30_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m69(L_4, L_3, /*hidden argument*/NULL);
		__this->___m_SteerAxis_6 = L_4;
		VirtualAxis_t30 * L_5 = (__this->___m_SteerAxis_6);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m95(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::Update()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void TiltInput_Update_m160 (TiltInput_t45 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = {0};
	Vector3_t4  V_3 = {0};
	Vector3_t4  V_4 = {0};
	Vector3_t4  V_5 = {0};
	Vector3_t4  V_6 = {0};
	int32_t V_7 = {0};
	{
		V_0 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_0 = Input_get_acceleration_m721(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_1 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Inequality_m722(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_009c;
		}
	}
	{
		int32_t L_3 = (__this->___tiltAroundAxis_3);
		V_2 = L_3;
		int32_t L_4 = V_2;
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_0067;
		}
	}
	{
		goto IL_009c;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_6 = Input_get_acceleration_m721(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = ((&V_3)->___x_1);
		Vector3_t4  L_8 = Input_get_acceleration_m721(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = ((&V_4)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_10 = atan2f(L_7, ((-L_9)));
		float L_11 = (__this->___centreAngleOffset_5);
		V_0 = ((float)((float)((float)((float)L_10*(float)(57.29578f)))+(float)L_11));
		goto IL_009c;
	}

IL_0067:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_12 = Input_get_acceleration_m721(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_12;
		float L_13 = ((&V_5)->___z_3);
		Vector3_t4  L_14 = Input_get_acceleration_m721(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_14;
		float L_15 = ((&V_6)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_16 = atan2f(L_13, ((-L_15)));
		float L_17 = (__this->___centreAngleOffset_5);
		V_0 = ((float)((float)((float)((float)L_16*(float)(57.29578f)))+(float)L_17));
		goto IL_009c;
	}

IL_009c:
	{
		float L_18 = (__this->___fullTiltAngle_4);
		float L_19 = (__this->___fullTiltAngle_4);
		float L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_21 = Mathf_InverseLerp_m651(NULL /*static, unused*/, ((-L_18)), L_19, L_20, /*hidden argument*/NULL);
		V_1 = ((float)((float)((float)((float)L_21*(float)(2.0f)))-(float)(1.0f)));
		AxisMapping_t44 * L_22 = (__this->___mapping_2);
		NullCheck(L_22);
		int32_t L_23 = (L_22->___type_0);
		V_7 = L_23;
		int32_t L_24 = V_7;
		if (L_24 == 0)
		{
			goto IL_00e5;
		}
		if (L_24 == 1)
		{
			goto IL_00f6;
		}
		if (L_24 == 2)
		{
			goto IL_0108;
		}
		if (L_24 == 3)
		{
			goto IL_011a;
		}
	}
	{
		goto IL_012c;
	}

IL_00e5:
	{
		VirtualAxis_t30 * L_25 = (__this->___m_SteerAxis_6);
		float L_26 = V_1;
		NullCheck(L_25);
		VirtualAxis_Update_m76(L_25, L_26, /*hidden argument*/NULL);
		goto IL_012c;
	}

IL_00f6:
	{
		float L_27 = V_1;
		int32_t L_28 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetVirtualMousePositionX_m113(NULL /*static, unused*/, ((float)((float)L_27*(float)(((float)L_28)))), /*hidden argument*/NULL);
		goto IL_012c;
	}

IL_0108:
	{
		float L_29 = V_1;
		int32_t L_30 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetVirtualMousePositionY_m114(NULL /*static, unused*/, ((float)((float)L_29*(float)(((float)L_30)))), /*hidden argument*/NULL);
		goto IL_012c;
	}

IL_011a:
	{
		float L_31 = V_1;
		int32_t L_32 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetVirtualMousePositionZ_m115(NULL /*static, unused*/, ((float)((float)L_31*(float)(((float)L_32)))), /*hidden argument*/NULL);
		goto IL_012c;
	}

IL_012c:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnDisable()
extern "C" void TiltInput_OnDisable_m161 (TiltInput_t45 * __this, const MethodInfo* method)
{
	{
		VirtualAxis_t30 * L_0 = (__this->___m_SteerAxis_6);
		NullCheck(L_0);
		VirtualAxis_Remove_m75(L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_16.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_16MethodDeclarations.h"



// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_17.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_17MethodDeclarations.h"



// UnityStandardAssets.CrossPlatformInput.TouchPad
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_18.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.CrossPlatformInput.TouchPad
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_18MethodDeclarations.h"

// UnityEngine.UI.Image
#include "UnityEngine_UI_UnityEngine_UI_Image.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
struct Component_t219;
struct Image_t48;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t48_m724(__this, method) (( Image_t48 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::.ctor()
extern "C" void TouchPad__ctor_m162 (TouchPad_t49 * __this, const MethodInfo* method)
{
	{
		__this->___horizontalAxisName_4 = (String_t*) &_stringLiteral3;
		__this->___verticalAxisName_5 = (String_t*) &_stringLiteral13;
		__this->___Xsensitivity_6 = (1.0f);
		__this->___Ysensitivity_7 = (1.0f);
		__this->___m_Id_16 = (-1);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnEnable()
extern const MethodInfo* Component_GetComponent_TisImage_t48_m724_MethodInfo_var;
extern "C" void TouchPad_OnEnable_m163 (TouchPad_t49 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisImage_t48_m724_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		s_Il2CppMethodIntialized = true;
	}
	{
		TouchPad_CreateVirtualAxes_m164(__this, /*hidden argument*/NULL);
		Image_t48 * L_0 = Component_GetComponent_TisImage_t48_m724(__this, /*hidden argument*/Component_GetComponent_TisImage_t48_m724_MethodInfo_var);
		__this->___m_Image_19 = L_0;
		Image_t48 * L_1 = (__this->___m_Image_19);
		NullCheck(L_1);
		Transform_t1 * L_2 = Component_get_transform_m594(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4  L_3 = Transform_get_position_m593(L_2, /*hidden argument*/NULL);
		__this->___m_Center_18 = L_3;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::CreateVirtualAxes()
extern TypeInfo* VirtualAxis_t30_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void TouchPad_CreateVirtualAxes_m164 (TouchPad_t49 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualAxis_t30_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	TouchPad_t49 * G_B2_0 = {0};
	TouchPad_t49 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	TouchPad_t49 * G_B3_1 = {0};
	TouchPad_t49 * G_B5_0 = {0};
	TouchPad_t49 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	TouchPad_t49 * G_B6_1 = {0};
	{
		int32_t L_0 = (__this->___axesToUse_2);
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = (__this->___axesToUse_2);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)1))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0018:
	{
		NullCheck(G_B3_1);
		G_B3_1->___m_UseX_11 = G_B3_0;
		int32_t L_2 = (__this->___axesToUse_2);
		G_B4_0 = __this;
		if (!L_2)
		{
			G_B5_0 = __this;
			goto IL_0034;
		}
	}
	{
		int32_t L_3 = (__this->___axesToUse_2);
		G_B6_0 = ((((int32_t)L_3) == ((int32_t)2))? 1 : 0);
		G_B6_1 = G_B4_0;
		goto IL_0035;
	}

IL_0034:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
	}

IL_0035:
	{
		NullCheck(G_B6_1);
		G_B6_1->___m_UseY_12 = G_B6_0;
		bool L_4 = (__this->___m_UseX_11);
		if (!L_4)
		{
			goto IL_0061;
		}
	}
	{
		String_t* L_5 = (__this->___horizontalAxisName_4);
		VirtualAxis_t30 * L_6 = (VirtualAxis_t30 *)il2cpp_codegen_object_new (VirtualAxis_t30_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m69(L_6, L_5, /*hidden argument*/NULL);
		__this->___m_HorizontalVirtualAxis_13 = L_6;
		VirtualAxis_t30 * L_7 = (__this->___m_HorizontalVirtualAxis_13);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m95(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_8 = (__this->___m_UseY_12);
		if (!L_8)
		{
			goto IL_0088;
		}
	}
	{
		String_t* L_9 = (__this->___verticalAxisName_5);
		VirtualAxis_t30 * L_10 = (VirtualAxis_t30 *)il2cpp_codegen_object_new (VirtualAxis_t30_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m69(L_10, L_9, /*hidden argument*/NULL);
		__this->___m_VerticalVirtualAxis_14 = L_10;
		VirtualAxis_t30 * L_11 = (__this->___m_VerticalVirtualAxis_14);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m95(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0088:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::UpdateVirtualAxes(UnityEngine.Vector3)
extern "C" void TouchPad_UpdateVirtualAxes_m165 (TouchPad_t49 * __this, Vector3_t4  ___value, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = Vector3_get_normalized_m648((&___value), /*hidden argument*/NULL);
		___value = L_0;
		bool L_1 = (__this->___m_UseX_11);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		VirtualAxis_t30 * L_2 = (__this->___m_HorizontalVirtualAxis_13);
		float L_3 = ((&___value)->___x_1);
		NullCheck(L_2);
		VirtualAxis_Update_m76(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0026:
	{
		bool L_4 = (__this->___m_UseY_12);
		if (!L_4)
		{
			goto IL_0043;
		}
	}
	{
		VirtualAxis_t30 * L_5 = (__this->___m_VerticalVirtualAxis_14);
		float L_6 = ((&___value)->___y_2);
		NullCheck(L_5);
		VirtualAxis_Update_m76(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" void TouchPad_OnPointerDown_m166 (TouchPad_t49 * __this, PointerEventData_t215 * ___data, const MethodInfo* method)
{
	{
		__this->___m_Dragging_15 = 1;
		PointerEventData_t215 * L_0 = ___data;
		NullCheck(L_0);
		int32_t L_1 = PointerEventData_get_pointerId_m725(L_0, /*hidden argument*/NULL);
		__this->___m_Id_16 = L_1;
		int32_t L_2 = (__this->___controlStyle_3);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		PointerEventData_t215 * L_3 = ___data;
		NullCheck(L_3);
		Vector2_t6  L_4 = PointerEventData_get_position_m711(L_3, /*hidden argument*/NULL);
		Vector3_t4  L_5 = Vector2_op_Implicit_m726(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->___m_Center_18 = L_5;
	}

IL_002f:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::Update()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" void TouchPad_Update_m167 (TouchPad_t49 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6  V_0 = {0};
	Vector2_t6  V_1 = {0};
	Vector2_t6  V_2 = {0};
	Vector2_t6  V_3 = {0};
	{
		bool L_0 = (__this->___m_Dragging_15);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		int32_t L_1 = Input_get_touchCount_m727(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Id_16);
		if ((((int32_t)L_1) < ((int32_t)((int32_t)((int32_t)L_2+(int32_t)1)))))
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_3 = (__this->___m_Id_16);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_4 = (__this->___controlStyle_3);
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_0062;
		}
	}
	{
		Vector2_t6  L_5 = (__this->___m_PreviousTouchPos_17);
		Vector3_t4  L_6 = Vector2_op_Implicit_m726(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->___m_Center_18 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_7 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = (__this->___m_Id_16);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Vector2_t6  L_9 = Touch_get_position_m729(((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_7, L_8)), /*hidden argument*/NULL);
		__this->___m_PreviousTouchPos_17 = L_9;
	}

IL_0062:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_10 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = (__this->___m_Id_16);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		Vector2_t6  L_12 = Touch_get_position_m729(((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_10, L_11)), /*hidden argument*/NULL);
		V_2 = L_12;
		float L_13 = ((&V_2)->___x_1);
		Vector3_t4 * L_14 = &(__this->___m_Center_18);
		float L_15 = (L_14->___x_1);
		TouchU5BU5D_t234* L_16 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_17 = (__this->___m_Id_16);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		Vector2_t6  L_18 = Touch_get_position_m729(((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_16, L_17)), /*hidden argument*/NULL);
		V_3 = L_18;
		float L_19 = ((&V_3)->___y_2);
		Vector3_t4 * L_20 = &(__this->___m_Center_18);
		float L_21 = (L_20->___y_2);
		Vector2__ctor_m630((&V_1), ((float)((float)L_13-(float)L_15)), ((float)((float)L_19-(float)L_21)), /*hidden argument*/NULL);
		Vector2_t6  L_22 = Vector2_get_normalized_m730((&V_1), /*hidden argument*/NULL);
		V_0 = L_22;
		Vector2_t6 * L_23 = (&V_0);
		float L_24 = (L_23->___x_1);
		float L_25 = (__this->___Xsensitivity_6);
		L_23->___x_1 = ((float)((float)L_24*(float)L_25));
		Vector2_t6 * L_26 = (&V_0);
		float L_27 = (L_26->___y_2);
		float L_28 = (__this->___Ysensitivity_7);
		L_26->___y_2 = ((float)((float)L_27*(float)L_28));
		float L_29 = ((&V_0)->___x_1);
		float L_30 = ((&V_0)->___y_2);
		Vector3_t4  L_31 = {0};
		Vector3__ctor_m612(&L_31, L_29, L_30, (0.0f), /*hidden argument*/NULL);
		TouchPad_UpdateVirtualAxes_m165(__this, L_31, /*hidden argument*/NULL);
	}

IL_0109:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" void TouchPad_OnPointerUp_m168 (TouchPad_t49 * __this, PointerEventData_t215 * ___data, const MethodInfo* method)
{
	{
		__this->___m_Dragging_15 = 0;
		__this->___m_Id_16 = (-1);
		Vector3_t4  L_0 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		TouchPad_UpdateVirtualAxes_m165(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnDisable()
extern TypeInfo* CrossPlatformInputManager_t35_il2cpp_TypeInfo_var;
extern "C" void TouchPad_OnDisable_m169 (TouchPad_t49 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___horizontalAxisName_4);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		bool L_1 = CrossPlatformInputManager_AxisExists_m93(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_2 = (__this->___horizontalAxisName_4);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_UnRegisterVirtualAxis_m97(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		String_t* L_3 = (__this->___verticalAxisName_5);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		bool L_4 = CrossPlatformInputManager_AxisExists_m93(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_5 = (__this->___verticalAxisName_5);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t35_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_UnRegisterVirtualAxis_m97(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"


// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::.ctor()
extern TypeInfo* Dictionary_2_t50_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t51_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t52_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m731_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m732_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m733_MethodInfo_var;
extern "C" void VirtualInput__ctor_m170 (VirtualInput_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t50_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		Dictionary_2_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		List_1_t52_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		Dictionary_2__ctor_m731_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		Dictionary_2__ctor_m732_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483657);
		List_1__ctor_m733_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t50 * L_0 = (Dictionary_2_t50 *)il2cpp_codegen_object_new (Dictionary_2_t50_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m731(L_0, /*hidden argument*/Dictionary_2__ctor_m731_MethodInfo_var);
		__this->___m_VirtualAxes_0 = L_0;
		Dictionary_2_t51 * L_1 = (Dictionary_2_t51 *)il2cpp_codegen_object_new (Dictionary_2_t51_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m732(L_1, /*hidden argument*/Dictionary_2__ctor_m732_MethodInfo_var);
		__this->___m_VirtualButtons_1 = L_1;
		List_1_t52 * L_2 = (List_1_t52 *)il2cpp_codegen_object_new (List_1_t52_il2cpp_TypeInfo_var);
		List_1__ctor_m733(L_2, /*hidden argument*/List_1__ctor_m733_MethodInfo_var);
		__this->___m_AlwaysUseVirtual_2 = L_2;
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::get_virtualMousePosition()
extern "C" Vector3_t4  VirtualInput_get_virtualMousePosition_m171 (VirtualInput_t34 * __this, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = (__this->___U3CvirtualMousePositionU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::set_virtualMousePosition(UnityEngine.Vector3)
extern "C" void VirtualInput_set_virtualMousePosition_m172 (VirtualInput_t34 * __this, Vector3_t4  ___value, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = ___value;
		__this->___U3CvirtualMousePositionU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::AxisExists(System.String)
extern "C" bool VirtualInput_AxisExists_m173 (VirtualInput_t34 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t50 * L_0 = (__this->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::ButtonExists(System.String)
extern "C" bool VirtualInput_ButtonExists_m174 (VirtualInput_t34 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t51 * L_0 = (__this->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void VirtualInput_RegisterVirtualAxis_m175 (VirtualInput_t34 * __this, VirtualAxis_t30 * ___axis, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t50 * L_0 = (__this->___m_VirtualAxes_0);
		VirtualAxis_t30 * L_1 = ___axis;
		NullCheck(L_1);
		String_t* L_2 = VirtualAxis_get_name_m71(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_3 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_2);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		VirtualAxis_t30 * L_4 = ___axis;
		NullCheck(L_4);
		String_t* L_5 = VirtualAxis_get_name_m71(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m734(NULL /*static, unused*/, (String_t*) &_stringLiteral15, L_5, (String_t*) &_stringLiteral16, /*hidden argument*/NULL);
		Debug_LogError_m735(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_0035:
	{
		Dictionary_2_t50 * L_7 = (__this->___m_VirtualAxes_0);
		VirtualAxis_t30 * L_8 = ___axis;
		NullCheck(L_8);
		String_t* L_9 = VirtualAxis_get_name_m71(L_8, /*hidden argument*/NULL);
		VirtualAxis_t30 * L_10 = ___axis;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, VirtualAxis_t30 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::Add(!0,!1) */, L_7, L_9, L_10);
		VirtualAxis_t30 * L_11 = ___axis;
		NullCheck(L_11);
		bool L_12 = VirtualAxis_get_matchWithInputManager_m73(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0063;
		}
	}
	{
		List_1_t52 * L_13 = (__this->___m_AlwaysUseVirtual_2);
		VirtualAxis_t30 * L_14 = ___axis;
		NullCheck(L_14);
		String_t* L_15 = VirtualAxis_get_name_m71(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_13, L_15);
	}

IL_0063:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void VirtualInput_RegisterVirtualButton_m176 (VirtualInput_t34 * __this, VirtualButton_t33 * ___button, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t51 * L_0 = (__this->___m_VirtualButtons_1);
		VirtualButton_t33 * L_1 = ___button;
		NullCheck(L_1);
		String_t* L_2 = VirtualButton_get_name_m81(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_3 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_2);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		VirtualButton_t33 * L_4 = ___button;
		NullCheck(L_4);
		String_t* L_5 = VirtualButton_get_name_m81(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m734(NULL /*static, unused*/, (String_t*) &_stringLiteral17, L_5, (String_t*) &_stringLiteral16, /*hidden argument*/NULL);
		Debug_LogError_m735(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_0035:
	{
		Dictionary_2_t51 * L_7 = (__this->___m_VirtualButtons_1);
		VirtualButton_t33 * L_8 = ___button;
		NullCheck(L_8);
		String_t* L_9 = VirtualButton_get_name_m81(L_8, /*hidden argument*/NULL);
		VirtualButton_t33 * L_10 = ___button;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, VirtualButton_t33 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::Add(!0,!1) */, L_7, L_9, L_10);
		VirtualButton_t33 * L_11 = ___button;
		NullCheck(L_11);
		bool L_12 = VirtualButton_get_matchWithInputManager_m83(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0063;
		}
	}
	{
		List_1_t52 * L_13 = (__this->___m_AlwaysUseVirtual_2);
		VirtualButton_t33 * L_14 = ___button;
		NullCheck(L_14);
		String_t* L_15 = VirtualButton_get_name_m81(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_13, L_15);
	}

IL_0063:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualAxis(System.String)
extern "C" void VirtualInput_UnRegisterVirtualAxis_m177 (VirtualInput_t34 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t50 * L_0 = (__this->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t50 * L_3 = (__this->___m_VirtualAxes_0);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::Remove(!0) */, L_3, L_4);
	}

IL_001e:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualButton(System.String)
extern "C" void VirtualInput_UnRegisterVirtualButton_m178 (VirtualInput_t34 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t51 * L_0 = (__this->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t51 * L_3 = (__this->___m_VirtualButtons_1);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::Remove(!0) */, L_3, L_4);
	}

IL_001e:
	{
		return;
	}
}
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.VirtualInput::VirtualAxisReference(System.String)
extern "C" VirtualAxis_t30 * VirtualInput_VirtualAxisReference_m179 (VirtualInput_t34 * __this, String_t* ___name, const MethodInfo* method)
{
	VirtualAxis_t30 * G_B3_0 = {0};
	{
		Dictionary_2_t50 * L_0 = (__this->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Dictionary_2_t50 * L_3 = (__this->___m_VirtualAxes_0);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtualAxis_t30 * L_5 = (VirtualAxis_t30 *)VirtFuncInvoker1< VirtualAxis_t30 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_3, L_4);
		G_B3_0 = L_5;
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = ((VirtualAxis_t30 *)(NULL));
	}

IL_0023:
	{
		return G_B3_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionX(System.Single)
extern "C" void VirtualInput_SetVirtualMousePositionX_m180 (VirtualInput_t34 * __this, float ___f, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	{
		float L_0 = ___f;
		Vector3_t4  L_1 = VirtualInput_get_virtualMousePosition_m171(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ((&V_0)->___y_2);
		Vector3_t4  L_3 = VirtualInput_get_virtualMousePosition_m171(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = ((&V_1)->___z_3);
		Vector3_t4  L_5 = {0};
		Vector3__ctor_m612(&L_5, L_0, L_2, L_4, /*hidden argument*/NULL);
		VirtualInput_set_virtualMousePosition_m172(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionY(System.Single)
extern "C" void VirtualInput_SetVirtualMousePositionY_m181 (VirtualInput_t34 * __this, float ___f, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	{
		Vector3_t4  L_0 = VirtualInput_get_virtualMousePosition_m171(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___x_1);
		float L_2 = ___f;
		Vector3_t4  L_3 = VirtualInput_get_virtualMousePosition_m171(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = ((&V_1)->___z_3);
		Vector3_t4  L_5 = {0};
		Vector3__ctor_m612(&L_5, L_1, L_2, L_4, /*hidden argument*/NULL);
		VirtualInput_set_virtualMousePosition_m172(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionZ(System.Single)
extern "C" void VirtualInput_SetVirtualMousePositionZ_m182 (VirtualInput_t34 * __this, float ___f, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	{
		Vector3_t4  L_0 = VirtualInput_get_virtualMousePosition_m171(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___x_1);
		Vector3_t4  L_2 = VirtualInput_get_virtualMousePosition_m171(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = ((&V_1)->___y_2);
		float L_4 = ___f;
		Vector3_t4  L_5 = {0};
		Vector3__ctor_m612(&L_5, L_1, L_3, L_4, /*hidden argument*/NULL);
		VirtualInput_set_virtualMousePosition_m172(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.VirtualInput::GetAxis(System.String,System.Boolean)
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButton(System.String)
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonDown(System.String)
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonUp(System.String)
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonDown(System.String)
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonUp(System.String)
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisPositive(System.String)
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisNegative(System.String)
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisZero(System.String)
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxis(System.String,System.Single)
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::MousePosition()
// UnityStandardAssets.ImageEffects.AAMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.AAMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffecMethodDeclarations.h"



// UnityStandardAssets.ImageEffects.Antialiasing
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Antialiasing
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_0MethodDeclarations.h"

// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// UnityStandardAssets.ImageEffects.PostEffectsBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_1.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTexture.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// UnityStandardAssets.ImageEffects.PostEffectsBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_1MethodDeclarations.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"
// UnityEngine.Graphics
#include "UnityEngine_UnityEngine_GraphicsMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTextureMethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.Antialiasing::.ctor()
extern "C" void Antialiasing__ctor_m183 (Antialiasing_t56 * __this, const MethodInfo* method)
{
	{
		__this->___mode_5 = 1;
		__this->___offsetScale_7 = (0.2f);
		__this->___blurRadius_8 = (18.0f);
		__this->___edgeThresholdMin_9 = (0.05f);
		__this->___edgeThreshold_10 = (0.2f);
		__this->___edgeSharpness_11 = (4.0f);
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::CurrentAAMaterial()
extern "C" Material_t55 * Antialiasing_CurrentAAMaterial_m184 (Antialiasing_t56 * __this, const MethodInfo* method)
{
	Material_t55 * V_0 = {0};
	int32_t V_1 = {0};
	{
		V_0 = (Material_t55 *)NULL;
		int32_t L_0 = (__this->___mode_5);
		V_1 = L_0;
		int32_t L_1 = V_1;
		if (L_1 == 0)
		{
			goto IL_003c;
		}
		if (L_1 == 1)
		{
			goto IL_0030;
		}
		if (L_1 == 2)
		{
			goto IL_0048;
		}
		if (L_1 == 3)
		{
			goto IL_0054;
		}
		if (L_1 == 4)
		{
			goto IL_0060;
		}
		if (L_1 == 5)
		{
			goto IL_006c;
		}
		if (L_1 == 6)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_0084;
	}

IL_0030:
	{
		Material_t55 * L_2 = (__this->___materialFXAAIII_26);
		V_0 = L_2;
		goto IL_008b;
	}

IL_003c:
	{
		Material_t55 * L_3 = (__this->___materialFXAAII_24);
		V_0 = L_3;
		goto IL_008b;
	}

IL_0048:
	{
		Material_t55 * L_4 = (__this->___materialFXAAPreset2_20);
		V_0 = L_4;
		goto IL_008b;
	}

IL_0054:
	{
		Material_t55 * L_5 = (__this->___materialFXAAPreset3_22);
		V_0 = L_5;
		goto IL_008b;
	}

IL_0060:
	{
		Material_t55 * L_6 = (__this->___nfaa_18);
		V_0 = L_6;
		goto IL_008b;
	}

IL_006c:
	{
		Material_t55 * L_7 = (__this->___ssaa_14);
		V_0 = L_7;
		goto IL_008b;
	}

IL_0078:
	{
		Material_t55 * L_8 = (__this->___dlaa_16);
		V_0 = L_8;
		goto IL_008b;
	}

IL_0084:
	{
		V_0 = (Material_t55 *)NULL;
		goto IL_008b;
	}

IL_008b:
	{
		Material_t55 * L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.Antialiasing::CheckResources()
extern "C" bool Antialiasing_CheckResources_m185 (Antialiasing_t56 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___shaderFXAAPreset2_19);
		Material_t55 * L_1 = (__this->___materialFXAAPreset2_20);
		Material_t55 * L_2 = PostEffectsBase_CreateMaterial_m329(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___materialFXAAPreset2_20 = L_2;
		Shader_t54 * L_3 = (__this->___shaderFXAAPreset3_21);
		Material_t55 * L_4 = (__this->___materialFXAAPreset3_22);
		Material_t55 * L_5 = PostEffectsBase_CreateMaterial_m329(__this, L_3, L_4, /*hidden argument*/NULL);
		__this->___materialFXAAPreset3_22 = L_5;
		Shader_t54 * L_6 = (__this->___shaderFXAAII_23);
		Material_t55 * L_7 = (__this->___materialFXAAII_24);
		Material_t55 * L_8 = PostEffectsBase_CreateMaterial_m329(__this, L_6, L_7, /*hidden argument*/NULL);
		__this->___materialFXAAII_24 = L_8;
		Shader_t54 * L_9 = (__this->___shaderFXAAIII_25);
		Material_t55 * L_10 = (__this->___materialFXAAIII_26);
		Material_t55 * L_11 = PostEffectsBase_CreateMaterial_m329(__this, L_9, L_10, /*hidden argument*/NULL);
		__this->___materialFXAAIII_26 = L_11;
		Shader_t54 * L_12 = (__this->___nfaaShader_17);
		Material_t55 * L_13 = (__this->___nfaa_18);
		Material_t55 * L_14 = PostEffectsBase_CreateMaterial_m329(__this, L_12, L_13, /*hidden argument*/NULL);
		__this->___nfaa_18 = L_14;
		Shader_t54 * L_15 = (__this->___ssaaShader_13);
		Material_t55 * L_16 = (__this->___ssaa_14);
		Material_t55 * L_17 = PostEffectsBase_CreateMaterial_m329(__this, L_15, L_16, /*hidden argument*/NULL);
		__this->___ssaa_14 = L_17;
		Shader_t54 * L_18 = (__this->___dlaaShader_15);
		Material_t55 * L_19 = (__this->___dlaa_16);
		Material_t55 * L_20 = PostEffectsBase_CreateMaterial_m329(__this, L_18, L_19, /*hidden argument*/NULL);
		__this->___dlaa_16 = L_20;
		Shader_t54 * L_21 = (__this->___ssaaShader_13);
		NullCheck(L_21);
		bool L_22 = Shader_get_isSupported_m736(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00cc;
		}
	}
	{
		PostEffectsBase_NotSupported_m339(__this, /*hidden argument*/NULL);
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		bool L_23 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_23;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Antialiasing::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Antialiasing_OnRenderImage_m186 (Antialiasing_t56 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	RenderTexture_t101 * V_0 = {0};
	Material_t55 * G_B21_0 = {0};
	RenderTexture_t101 * G_B21_1 = {0};
	RenderTexture_t101 * G_B21_2 = {0};
	Material_t55 * G_B20_0 = {0};
	RenderTexture_t101 * G_B20_1 = {0};
	RenderTexture_t101 * G_B20_2 = {0};
	int32_t G_B22_0 = 0;
	Material_t55 * G_B22_1 = {0};
	RenderTexture_t101 * G_B22_2 = {0};
	RenderTexture_t101 * G_B22_3 = {0};
	Material_t55 * G_B27_0 = {0};
	RenderTexture_t101 * G_B27_1 = {0};
	RenderTexture_t101 * G_B27_2 = {0};
	Material_t55 * G_B26_0 = {0};
	RenderTexture_t101 * G_B26_1 = {0};
	RenderTexture_t101 * G_B26_2 = {0};
	int32_t G_B28_0 = 0;
	Material_t55 * G_B28_1 = {0};
	RenderTexture_t101 * G_B28_2 = {0};
	RenderTexture_t101 * G_B28_3 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.Antialiasing::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		int32_t L_3 = (__this->___mode_5);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0084;
		}
	}
	{
		Material_t55 * L_4 = (__this->___materialFXAAIII_26);
		bool L_5 = Object_op_Inequality_m623(NULL /*static, unused*/, L_4, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0084;
		}
	}
	{
		Material_t55 * L_6 = (__this->___materialFXAAIII_26);
		float L_7 = (__this->___edgeThresholdMin_9);
		NullCheck(L_6);
		Material_SetFloat_m738(L_6, (String_t*) &_stringLiteral18, L_7, /*hidden argument*/NULL);
		Material_t55 * L_8 = (__this->___materialFXAAIII_26);
		float L_9 = (__this->___edgeThreshold_10);
		NullCheck(L_8);
		Material_SetFloat_m738(L_8, (String_t*) &_stringLiteral19, L_9, /*hidden argument*/NULL);
		Material_t55 * L_10 = (__this->___materialFXAAIII_26);
		float L_11 = (__this->___edgeSharpness_11);
		NullCheck(L_10);
		Material_SetFloat_m738(L_10, (String_t*) &_stringLiteral20, L_11, /*hidden argument*/NULL);
		RenderTexture_t101 * L_12 = ___source;
		RenderTexture_t101 * L_13 = ___destination;
		Material_t55 * L_14 = (__this->___materialFXAAIII_26);
		Graphics_Blit_m739(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		goto IL_0236;
	}

IL_0084:
	{
		int32_t L_15 = (__this->___mode_5);
		if ((!(((uint32_t)L_15) == ((uint32_t)3))))
		{
			goto IL_00b3;
		}
	}
	{
		Material_t55 * L_16 = (__this->___materialFXAAPreset3_22);
		bool L_17 = Object_op_Inequality_m623(NULL /*static, unused*/, L_16, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00b3;
		}
	}
	{
		RenderTexture_t101 * L_18 = ___source;
		RenderTexture_t101 * L_19 = ___destination;
		Material_t55 * L_20 = (__this->___materialFXAAPreset3_22);
		Graphics_Blit_m739(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		goto IL_0236;
	}

IL_00b3:
	{
		int32_t L_21 = (__this->___mode_5);
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00f0;
		}
	}
	{
		Material_t55 * L_22 = (__this->___materialFXAAPreset2_20);
		bool L_23 = Object_op_Inequality_m623(NULL /*static, unused*/, L_22, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00f0;
		}
	}
	{
		RenderTexture_t101 * L_24 = ___source;
		NullCheck(L_24);
		Texture_set_anisoLevel_m740(L_24, 4, /*hidden argument*/NULL);
		RenderTexture_t101 * L_25 = ___source;
		RenderTexture_t101 * L_26 = ___destination;
		Material_t55 * L_27 = (__this->___materialFXAAPreset2_20);
		Graphics_Blit_m739(NULL /*static, unused*/, L_25, L_26, L_27, /*hidden argument*/NULL);
		RenderTexture_t101 * L_28 = ___source;
		NullCheck(L_28);
		Texture_set_anisoLevel_m740(L_28, 0, /*hidden argument*/NULL);
		goto IL_0236;
	}

IL_00f0:
	{
		int32_t L_29 = (__this->___mode_5);
		if (L_29)
		{
			goto IL_011e;
		}
	}
	{
		Material_t55 * L_30 = (__this->___materialFXAAII_24);
		bool L_31 = Object_op_Inequality_m623(NULL /*static, unused*/, L_30, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_011e;
		}
	}
	{
		RenderTexture_t101 * L_32 = ___source;
		RenderTexture_t101 * L_33 = ___destination;
		Material_t55 * L_34 = (__this->___materialFXAAII_24);
		Graphics_Blit_m739(NULL /*static, unused*/, L_32, L_33, L_34, /*hidden argument*/NULL);
		goto IL_0236;
	}

IL_011e:
	{
		int32_t L_35 = (__this->___mode_5);
		if ((!(((uint32_t)L_35) == ((uint32_t)5))))
		{
			goto IL_014d;
		}
	}
	{
		Material_t55 * L_36 = (__this->___ssaa_14);
		bool L_37 = Object_op_Inequality_m623(NULL /*static, unused*/, L_36, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_014d;
		}
	}
	{
		RenderTexture_t101 * L_38 = ___source;
		RenderTexture_t101 * L_39 = ___destination;
		Material_t55 * L_40 = (__this->___ssaa_14);
		Graphics_Blit_m739(NULL /*static, unused*/, L_38, L_39, L_40, /*hidden argument*/NULL);
		goto IL_0236;
	}

IL_014d:
	{
		int32_t L_41 = (__this->___mode_5);
		if ((!(((uint32_t)L_41) == ((uint32_t)6))))
		{
			goto IL_01bb;
		}
	}
	{
		Material_t55 * L_42 = (__this->___dlaa_16);
		bool L_43 = Object_op_Inequality_m623(NULL /*static, unused*/, L_42, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_01bb;
		}
	}
	{
		RenderTexture_t101 * L_44 = ___source;
		NullCheck(L_44);
		Texture_set_anisoLevel_m740(L_44, 0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_45 = ___source;
		NullCheck(L_45);
		int32_t L_46 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_45);
		RenderTexture_t101 * L_47 = ___source;
		NullCheck(L_47);
		int32_t L_48 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_47);
		RenderTexture_t101 * L_49 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, L_46, L_48, /*hidden argument*/NULL);
		V_0 = L_49;
		RenderTexture_t101 * L_50 = ___source;
		RenderTexture_t101 * L_51 = V_0;
		Material_t55 * L_52 = (__this->___dlaa_16);
		Graphics_Blit_m742(NULL /*static, unused*/, L_50, L_51, L_52, 0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_53 = V_0;
		RenderTexture_t101 * L_54 = ___destination;
		Material_t55 * L_55 = (__this->___dlaa_16);
		bool L_56 = (__this->___dlaaSharp_12);
		G_B20_0 = L_55;
		G_B20_1 = L_54;
		G_B20_2 = L_53;
		if (!L_56)
		{
			G_B21_0 = L_55;
			G_B21_1 = L_54;
			G_B21_2 = L_53;
			goto IL_01aa;
		}
	}
	{
		G_B22_0 = 2;
		G_B22_1 = G_B20_0;
		G_B22_2 = G_B20_1;
		G_B22_3 = G_B20_2;
		goto IL_01ab;
	}

IL_01aa:
	{
		G_B22_0 = 1;
		G_B22_1 = G_B21_0;
		G_B22_2 = G_B21_1;
		G_B22_3 = G_B21_2;
	}

IL_01ab:
	{
		Graphics_Blit_m742(NULL /*static, unused*/, G_B22_3, G_B22_2, G_B22_1, G_B22_0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_57 = V_0;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		goto IL_0236;
	}

IL_01bb:
	{
		int32_t L_58 = (__this->___mode_5);
		if ((!(((uint32_t)L_58) == ((uint32_t)4))))
		{
			goto IL_022f;
		}
	}
	{
		Material_t55 * L_59 = (__this->___nfaa_18);
		bool L_60 = Object_op_Inequality_m623(NULL /*static, unused*/, L_59, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_022f;
		}
	}
	{
		RenderTexture_t101 * L_61 = ___source;
		NullCheck(L_61);
		Texture_set_anisoLevel_m740(L_61, 0, /*hidden argument*/NULL);
		Material_t55 * L_62 = (__this->___nfaa_18);
		float L_63 = (__this->___offsetScale_7);
		NullCheck(L_62);
		Material_SetFloat_m738(L_62, (String_t*) &_stringLiteral21, L_63, /*hidden argument*/NULL);
		Material_t55 * L_64 = (__this->___nfaa_18);
		float L_65 = (__this->___blurRadius_8);
		NullCheck(L_64);
		Material_SetFloat_m738(L_64, (String_t*) &_stringLiteral22, L_65, /*hidden argument*/NULL);
		RenderTexture_t101 * L_66 = ___source;
		RenderTexture_t101 * L_67 = ___destination;
		Material_t55 * L_68 = (__this->___nfaa_18);
		bool L_69 = (__this->___showGeneratedNormals_6);
		G_B26_0 = L_68;
		G_B26_1 = L_67;
		G_B26_2 = L_66;
		if (!L_69)
		{
			G_B27_0 = L_68;
			G_B27_1 = L_67;
			G_B27_2 = L_66;
			goto IL_0224;
		}
	}
	{
		G_B28_0 = 1;
		G_B28_1 = G_B26_0;
		G_B28_2 = G_B26_1;
		G_B28_3 = G_B26_2;
		goto IL_0225;
	}

IL_0224:
	{
		G_B28_0 = 0;
		G_B28_1 = G_B27_0;
		G_B28_2 = G_B27_1;
		G_B28_3 = G_B27_2;
	}

IL_0225:
	{
		Graphics_Blit_m742(NULL /*static, unused*/, G_B28_3, G_B28_2, G_B28_1, G_B28_0, /*hidden argument*/NULL);
		goto IL_0236;
	}

IL_022f:
	{
		RenderTexture_t101 * L_70 = ___source;
		RenderTexture_t101 * L_71 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_70, L_71, /*hidden argument*/NULL);
	}

IL_0236:
	{
		return;
	}
}
// UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_2.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_2MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.Bloom/TweakMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_3.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Bloom/TweakMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_3MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_4.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_4MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_5.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_5MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.Bloom/BloomQuality
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_6.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Bloom/BloomQuality
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_6MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.Bloom
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_7.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Bloom
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_7MethodDeclarations.h"

// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_Mathf.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
struct Component_t219;
struct Camera_t27;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t27_m744(__this, method) (( Camera_t27 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.ImageEffects.Bloom::.ctor()
extern "C" void Bloom__ctor_m187 (Bloom_t64 * __this, const MethodInfo* method)
{
	{
		__this->___screenBlendMode_6 = 1;
		__this->___sepBlurSpread_9 = (2.5f);
		__this->___quality_10 = 1;
		__this->___bloomIntensity_11 = (0.5f);
		__this->___bloomThreshold_12 = (0.5f);
		Color_t65  L_0 = Color_get_white_m745(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___bloomThresholdColor_13 = L_0;
		__this->___bloomBlurIterations_14 = 2;
		__this->___hollywoodFlareBlurIterations_15 = 2;
		__this->___lensflareMode_17 = 1;
		__this->___hollyStretchWidth_18 = (2.5f);
		__this->___lensflareThreshold_20 = (0.3f);
		__this->___lensFlareSaturation_21 = (0.75f);
		Color_t65  L_1 = {0};
		Color__ctor_m746(&L_1, (0.4f), (0.4f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->___flareColorA_22 = L_1;
		Color_t65  L_2 = {0};
		Color__ctor_m746(&L_2, (0.4f), (0.8f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->___flareColorB_23 = L_2;
		Color_t65  L_3 = {0};
		Color__ctor_m746(&L_3, (0.8f), (0.4f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->___flareColorC_24 = L_3;
		Color_t65  L_4 = {0};
		Color__ctor_m746(&L_4, (0.8f), (0.4f), (0.0f), (0.75f), /*hidden argument*/NULL);
		__this->___flareColorD_25 = L_4;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.Bloom::CheckResources()
extern "C" bool Bloom_CheckResources_m188 (Bloom_t64 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___screenBlendShader_29);
		Material_t55 * L_1 = (__this->___screenBlend_30);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___screenBlend_30 = L_2;
		Shader_t54 * L_3 = (__this->___lensFlareShader_27);
		Material_t55 * L_4 = (__this->___lensFlareMaterial_28);
		Material_t55 * L_5 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_3, L_4, /*hidden argument*/NULL);
		__this->___lensFlareMaterial_28 = L_5;
		Shader_t54 * L_6 = (__this->___blurAndFlaresShader_31);
		Material_t55 * L_7 = (__this->___blurAndFlaresMaterial_32);
		Material_t55 * L_8 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_6, L_7, /*hidden argument*/NULL);
		__this->___blurAndFlaresMaterial_32 = L_8;
		Shader_t54 * L_9 = (__this->___brightPassFilterShader_33);
		Material_t55 * L_10 = (__this->___brightPassFilterMaterial_34);
		Material_t55 * L_11 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_9, L_10, /*hidden argument*/NULL);
		__this->___brightPassFilterMaterial_34 = L_11;
		bool L_12 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_12)
		{
			goto IL_0079;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0079:
	{
		bool L_13 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_13;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Bloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void Bloom_OnRenderImage_m189 (Bloom_t64 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	RenderTexture_t101 * V_8 = {0};
	RenderTexture_t101 * V_9 = {0};
	RenderTexture_t101 * V_10 = {0};
	RenderTexture_t101 * V_11 = {0};
	int32_t V_12 = 0;
	float V_13 = 0.0f;
	RenderTexture_t101 * V_14 = {0};
	RenderTexture_t101 * V_15 = {0};
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	RenderTexture_t101 * V_21 = {0};
	Bloom_t64 * G_B5_0 = {0};
	Bloom_t64 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Bloom_t64 * G_B6_1 = {0};
	Bloom_t64 * G_B10_0 = {0};
	Bloom_t64 * G_B9_0 = {0};
	int32_t G_B11_0 = 0;
	Bloom_t64 * G_B11_1 = {0};
	int32_t G_B16_0 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.Bloom::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		__this->___doHdr_8 = 0;
		int32_t L_3 = (__this->___hdr_7);
		if (L_3)
		{
			goto IL_004a;
		}
	}
	{
		RenderTexture_t101 * L_4 = ___source;
		NullCheck(L_4);
		int32_t L_5 = RenderTexture_get_format_m747(L_4, /*hidden argument*/NULL);
		G_B4_0 = __this;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			G_B5_0 = __this;
			goto IL_003f;
		}
	}
	{
		Camera_t27 * L_6 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_6);
		bool L_7 = Camera_get_hdr_m748(L_6, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_7));
		G_B6_1 = G_B4_0;
		goto IL_0040;
	}

IL_003f:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0040:
	{
		NullCheck(G_B6_1);
		G_B6_1->___doHdr_8 = G_B6_0;
		goto IL_0059;
	}

IL_004a:
	{
		int32_t L_8 = (__this->___hdr_7);
		__this->___doHdr_8 = ((((int32_t)L_8) == ((int32_t)1))? 1 : 0);
	}

IL_0059:
	{
		bool L_9 = (__this->___doHdr_8);
		G_B9_0 = __this;
		if (!L_9)
		{
			G_B10_0 = __this;
			goto IL_006d;
		}
	}
	{
		bool L_10 = (((PostEffectsBase_t57 *)__this)->___supportHDRTextures_2);
		G_B11_0 = ((int32_t)(L_10));
		G_B11_1 = G_B9_0;
		goto IL_006e;
	}

IL_006d:
	{
		G_B11_0 = 0;
		G_B11_1 = G_B10_0;
	}

IL_006e:
	{
		NullCheck(G_B11_1);
		G_B11_1->___doHdr_8 = G_B11_0;
		int32_t L_11 = (__this->___screenBlendMode_6);
		V_0 = L_11;
		bool L_12 = (__this->___doHdr_8);
		if (!L_12)
		{
			goto IL_0087;
		}
	}
	{
		V_0 = 1;
	}

IL_0087:
	{
		bool L_13 = (__this->___doHdr_8);
		if (!L_13)
		{
			goto IL_0098;
		}
	}
	{
		G_B16_0 = 2;
		goto IL_0099;
	}

IL_0098:
	{
		G_B16_0 = 7;
	}

IL_0099:
	{
		V_1 = G_B16_0;
		RenderTexture_t101 * L_14 = ___source;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_14);
		V_2 = ((int32_t)((int32_t)L_15/(int32_t)2));
		RenderTexture_t101 * L_16 = ___source;
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_16);
		V_3 = ((int32_t)((int32_t)L_17/(int32_t)2));
		RenderTexture_t101 * L_18 = ___source;
		NullCheck(L_18);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_18);
		V_4 = ((int32_t)((int32_t)L_19/(int32_t)4));
		RenderTexture_t101 * L_20 = ___source;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_20);
		V_5 = ((int32_t)((int32_t)L_21/(int32_t)4));
		RenderTexture_t101 * L_22 = ___source;
		NullCheck(L_22);
		int32_t L_23 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_22);
		RenderTexture_t101 * L_24 = ___source;
		NullCheck(L_24);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_24);
		V_6 = ((float)((float)((float)((float)(1.0f)*(float)(((float)L_23))))/(float)((float)((float)(1.0f)*(float)(((float)L_25))))));
		V_7 = (0.001953125f);
		int32_t L_26 = V_4;
		int32_t L_27 = V_5;
		int32_t L_28 = V_1;
		RenderTexture_t101 * L_29 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_26, L_27, 0, L_28, /*hidden argument*/NULL);
		V_8 = L_29;
		int32_t L_30 = V_2;
		int32_t L_31 = V_3;
		int32_t L_32 = V_1;
		RenderTexture_t101 * L_33 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_30, L_31, 0, L_32, /*hidden argument*/NULL);
		V_9 = L_33;
		int32_t L_34 = (__this->___quality_10);
		if ((((int32_t)L_34) <= ((int32_t)0)))
		{
			goto IL_0150;
		}
	}
	{
		RenderTexture_t101 * L_35 = ___source;
		RenderTexture_t101 * L_36 = V_9;
		Material_t55 * L_37 = (__this->___screenBlend_30);
		Graphics_Blit_m742(NULL /*static, unused*/, L_35, L_36, L_37, 2, /*hidden argument*/NULL);
		int32_t L_38 = V_4;
		int32_t L_39 = V_5;
		int32_t L_40 = V_1;
		RenderTexture_t101 * L_41 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_38, L_39, 0, L_40, /*hidden argument*/NULL);
		V_10 = L_41;
		RenderTexture_t101 * L_42 = V_9;
		RenderTexture_t101 * L_43 = V_10;
		Material_t55 * L_44 = (__this->___screenBlend_30);
		Graphics_Blit_m742(NULL /*static, unused*/, L_42, L_43, L_44, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_45 = V_10;
		RenderTexture_t101 * L_46 = V_8;
		Material_t55 * L_47 = (__this->___screenBlend_30);
		Graphics_Blit_m742(NULL /*static, unused*/, L_45, L_46, L_47, 6, /*hidden argument*/NULL);
		RenderTexture_t101 * L_48 = V_10;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		goto IL_0168;
	}

IL_0150:
	{
		RenderTexture_t101 * L_49 = ___source;
		RenderTexture_t101 * L_50 = V_9;
		Graphics_Blit_m737(NULL /*static, unused*/, L_49, L_50, /*hidden argument*/NULL);
		RenderTexture_t101 * L_51 = V_9;
		RenderTexture_t101 * L_52 = V_8;
		Material_t55 * L_53 = (__this->___screenBlend_30);
		Graphics_Blit_m742(NULL /*static, unused*/, L_51, L_52, L_53, 6, /*hidden argument*/NULL);
	}

IL_0168:
	{
		RenderTexture_t101 * L_54 = V_9;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_4;
		int32_t L_56 = V_5;
		int32_t L_57 = V_1;
		RenderTexture_t101 * L_58 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_55, L_56, 0, L_57, /*hidden argument*/NULL);
		V_11 = L_58;
		float L_59 = (__this->___bloomThreshold_12);
		Color_t65  L_60 = (__this->___bloomThresholdColor_13);
		Color_t65  L_61 = Color_op_Multiply_m750(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		RenderTexture_t101 * L_62 = V_8;
		RenderTexture_t101 * L_63 = V_11;
		Bloom_BrightFilter_m193(__this, L_61, L_62, L_63, /*hidden argument*/NULL);
		int32_t L_64 = (__this->___bloomBlurIterations_14);
		if ((((int32_t)L_64) >= ((int32_t)1)))
		{
			goto IL_01af;
		}
	}
	{
		__this->___bloomBlurIterations_14 = 1;
		goto IL_01c4;
	}

IL_01af:
	{
		int32_t L_65 = (__this->___bloomBlurIterations_14);
		if ((((int32_t)L_65) <= ((int32_t)((int32_t)10))))
		{
			goto IL_01c4;
		}
	}
	{
		__this->___bloomBlurIterations_14 = ((int32_t)10);
	}

IL_01c4:
	{
		V_12 = 0;
		goto IL_02db;
	}

IL_01cc:
	{
		int32_t L_66 = V_12;
		float L_67 = (__this->___sepBlurSpread_9);
		V_13 = ((float)((float)((float)((float)(1.0f)+(float)((float)((float)(((float)L_66))*(float)(0.25f)))))*(float)L_67));
		int32_t L_68 = V_4;
		int32_t L_69 = V_5;
		int32_t L_70 = V_1;
		RenderTexture_t101 * L_71 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_68, L_69, 0, L_70, /*hidden argument*/NULL);
		V_14 = L_71;
		Material_t55 * L_72 = (__this->___blurAndFlaresMaterial_32);
		float L_73 = V_13;
		float L_74 = V_7;
		Vector4_t236  L_75 = {0};
		Vector4__ctor_m751(&L_75, (0.0f), ((float)((float)L_73*(float)L_74)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_72);
		Material_SetVector_m752(L_72, (String_t*) &_stringLiteral23, L_75, /*hidden argument*/NULL);
		RenderTexture_t101 * L_76 = V_11;
		RenderTexture_t101 * L_77 = V_14;
		Material_t55 * L_78 = (__this->___blurAndFlaresMaterial_32);
		Graphics_Blit_m742(NULL /*static, unused*/, L_76, L_77, L_78, 4, /*hidden argument*/NULL);
		RenderTexture_t101 * L_79 = V_11;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		RenderTexture_t101 * L_80 = V_14;
		V_11 = L_80;
		int32_t L_81 = V_4;
		int32_t L_82 = V_5;
		int32_t L_83 = V_1;
		RenderTexture_t101 * L_84 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_81, L_82, 0, L_83, /*hidden argument*/NULL);
		V_14 = L_84;
		Material_t55 * L_85 = (__this->___blurAndFlaresMaterial_32);
		float L_86 = V_13;
		float L_87 = V_6;
		float L_88 = V_7;
		Vector4_t236  L_89 = {0};
		Vector4__ctor_m751(&L_89, ((float)((float)((float)((float)L_86/(float)L_87))*(float)L_88)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_85);
		Material_SetVector_m752(L_85, (String_t*) &_stringLiteral23, L_89, /*hidden argument*/NULL);
		RenderTexture_t101 * L_90 = V_11;
		RenderTexture_t101 * L_91 = V_14;
		Material_t55 * L_92 = (__this->___blurAndFlaresMaterial_32);
		Graphics_Blit_m742(NULL /*static, unused*/, L_90, L_91, L_92, 4, /*hidden argument*/NULL);
		RenderTexture_t101 * L_93 = V_11;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		RenderTexture_t101 * L_94 = V_14;
		V_11 = L_94;
		int32_t L_95 = (__this->___quality_10);
		if ((((int32_t)L_95) <= ((int32_t)0)))
		{
			goto IL_02d5;
		}
	}
	{
		int32_t L_96 = V_12;
		if (L_96)
		{
			goto IL_02bd;
		}
	}
	{
		RenderTexture_t101 * L_97 = V_8;
		Graphics_SetRenderTarget_m753(NULL /*static, unused*/, L_97, /*hidden argument*/NULL);
		Color_t65  L_98 = Color_get_black_m754(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m755(NULL /*static, unused*/, 0, 1, L_98, /*hidden argument*/NULL);
		RenderTexture_t101 * L_99 = V_11;
		RenderTexture_t101 * L_100 = V_8;
		Graphics_Blit_m737(NULL /*static, unused*/, L_99, L_100, /*hidden argument*/NULL);
		goto IL_02d5;
	}

IL_02bd:
	{
		RenderTexture_t101 * L_101 = V_8;
		NullCheck(L_101);
		RenderTexture_MarkRestoreExpected_m756(L_101, /*hidden argument*/NULL);
		RenderTexture_t101 * L_102 = V_11;
		RenderTexture_t101 * L_103 = V_8;
		Material_t55 * L_104 = (__this->___screenBlend_30);
		Graphics_Blit_m742(NULL /*static, unused*/, L_102, L_103, L_104, ((int32_t)10), /*hidden argument*/NULL);
	}

IL_02d5:
	{
		int32_t L_105 = V_12;
		V_12 = ((int32_t)((int32_t)L_105+(int32_t)1));
	}

IL_02db:
	{
		int32_t L_106 = V_12;
		int32_t L_107 = (__this->___bloomBlurIterations_14);
		if ((((int32_t)L_106) < ((int32_t)L_107)))
		{
			goto IL_01cc;
		}
	}
	{
		int32_t L_108 = (__this->___quality_10);
		if ((((int32_t)L_108) <= ((int32_t)0)))
		{
			goto IL_0317;
		}
	}
	{
		RenderTexture_t101 * L_109 = V_11;
		Graphics_SetRenderTarget_m753(NULL /*static, unused*/, L_109, /*hidden argument*/NULL);
		Color_t65  L_110 = Color_get_black_m754(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m755(NULL /*static, unused*/, 0, 1, L_110, /*hidden argument*/NULL);
		RenderTexture_t101 * L_111 = V_8;
		RenderTexture_t101 * L_112 = V_11;
		Material_t55 * L_113 = (__this->___screenBlend_30);
		Graphics_Blit_m742(NULL /*static, unused*/, L_111, L_112, L_113, 6, /*hidden argument*/NULL);
	}

IL_0317:
	{
		float L_114 = (__this->___lensflareIntensity_19);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_115 = ((Mathf_t218_StaticFields*)Mathf_t218_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		if ((!(((float)L_114) > ((float)L_115))))
		{
			goto IL_0710;
		}
	}
	{
		int32_t L_116 = V_4;
		int32_t L_117 = V_5;
		int32_t L_118 = V_1;
		RenderTexture_t101 * L_119 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_116, L_117, 0, L_118, /*hidden argument*/NULL);
		V_15 = L_119;
		int32_t L_120 = (__this->___lensflareMode_17);
		if (L_120)
		{
			goto IL_042f;
		}
	}
	{
		float L_121 = (__this->___lensflareThreshold_20);
		RenderTexture_t101 * L_122 = V_11;
		RenderTexture_t101 * L_123 = V_15;
		Bloom_BrightFilter_m192(__this, L_121, L_122, L_123, /*hidden argument*/NULL);
		int32_t L_124 = (__this->___quality_10);
		if ((((int32_t)L_124) <= ((int32_t)0)))
		{
			goto IL_0411;
		}
	}
	{
		Material_t55 * L_125 = (__this->___blurAndFlaresMaterial_32);
		RenderTexture_t101 * L_126 = V_8;
		NullCheck(L_126);
		int32_t L_127 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_126);
		Vector4_t236  L_128 = {0};
		Vector4__ctor_m751(&L_128, (0.0f), ((float)((float)(1.5f)/(float)((float)((float)(1.0f)*(float)(((float)L_127)))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_125);
		Material_SetVector_m752(L_125, (String_t*) &_stringLiteral23, L_128, /*hidden argument*/NULL);
		RenderTexture_t101 * L_129 = V_8;
		Graphics_SetRenderTarget_m753(NULL /*static, unused*/, L_129, /*hidden argument*/NULL);
		Color_t65  L_130 = Color_get_black_m754(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m755(NULL /*static, unused*/, 0, 1, L_130, /*hidden argument*/NULL);
		RenderTexture_t101 * L_131 = V_15;
		RenderTexture_t101 * L_132 = V_8;
		Material_t55 * L_133 = (__this->___blurAndFlaresMaterial_32);
		Graphics_Blit_m742(NULL /*static, unused*/, L_131, L_132, L_133, 4, /*hidden argument*/NULL);
		Material_t55 * L_134 = (__this->___blurAndFlaresMaterial_32);
		RenderTexture_t101 * L_135 = V_8;
		NullCheck(L_135);
		int32_t L_136 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_135);
		Vector4_t236  L_137 = {0};
		Vector4__ctor_m751(&L_137, ((float)((float)(1.5f)/(float)((float)((float)(1.0f)*(float)(((float)L_136)))))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_134);
		Material_SetVector_m752(L_134, (String_t*) &_stringLiteral23, L_137, /*hidden argument*/NULL);
		RenderTexture_t101 * L_138 = V_15;
		Graphics_SetRenderTarget_m753(NULL /*static, unused*/, L_138, /*hidden argument*/NULL);
		Color_t65  L_139 = Color_get_black_m754(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m755(NULL /*static, unused*/, 0, 1, L_139, /*hidden argument*/NULL);
		RenderTexture_t101 * L_140 = V_8;
		RenderTexture_t101 * L_141 = V_15;
		Material_t55 * L_142 = (__this->___blurAndFlaresMaterial_32);
		Graphics_Blit_m742(NULL /*static, unused*/, L_140, L_141, L_142, 4, /*hidden argument*/NULL);
	}

IL_0411:
	{
		RenderTexture_t101 * L_143 = V_15;
		RenderTexture_t101 * L_144 = V_15;
		Bloom_Vignette_m194(__this, (0.975f), L_143, L_144, /*hidden argument*/NULL);
		RenderTexture_t101 * L_145 = V_15;
		RenderTexture_t101 * L_146 = V_11;
		Bloom_BlendFlares_m191(__this, L_145, L_146, /*hidden argument*/NULL);
		goto IL_0709;
	}

IL_042f:
	{
		float L_147 = (__this->___flareRotation_16);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_148 = cosf(L_147);
		V_16 = ((float)((float)(1.0f)*(float)L_148));
		float L_149 = (__this->___flareRotation_16);
		float L_150 = sinf(L_149);
		V_17 = ((float)((float)(1.0f)*(float)L_150));
		float L_151 = (__this->___hollyStretchWidth_18);
		float L_152 = V_6;
		float L_153 = V_7;
		V_18 = ((float)((float)((float)((float)((float)((float)L_151*(float)(1.0f)))/(float)L_152))*(float)L_153));
		Material_t55 * L_154 = (__this->___blurAndFlaresMaterial_32);
		float L_155 = V_16;
		float L_156 = V_17;
		Vector4_t236  L_157 = {0};
		Vector4__ctor_m751(&L_157, L_155, L_156, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_154);
		Material_SetVector_m752(L_154, (String_t*) &_stringLiteral23, L_157, /*hidden argument*/NULL);
		Material_t55 * L_158 = (__this->___blurAndFlaresMaterial_32);
		float L_159 = (__this->___lensflareThreshold_20);
		Vector4_t236  L_160 = {0};
		Vector4__ctor_m751(&L_160, L_159, (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_158);
		Material_SetVector_m752(L_158, (String_t*) &_stringLiteral24, L_160, /*hidden argument*/NULL);
		Material_t55 * L_161 = (__this->___blurAndFlaresMaterial_32);
		Color_t65 * L_162 = &(__this->___flareColorA_22);
		float L_163 = (L_162->___r_0);
		Color_t65 * L_164 = &(__this->___flareColorA_22);
		float L_165 = (L_164->___g_1);
		Color_t65 * L_166 = &(__this->___flareColorA_22);
		float L_167 = (L_166->___b_2);
		Color_t65 * L_168 = &(__this->___flareColorA_22);
		float L_169 = (L_168->___a_3);
		Vector4_t236  L_170 = {0};
		Vector4__ctor_m751(&L_170, L_163, L_165, L_167, L_169, /*hidden argument*/NULL);
		Color_t65 * L_171 = &(__this->___flareColorA_22);
		float L_172 = (L_171->___a_3);
		Vector4_t236  L_173 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_170, L_172, /*hidden argument*/NULL);
		float L_174 = (__this->___lensflareIntensity_19);
		Vector4_t236  L_175 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_173, L_174, /*hidden argument*/NULL);
		NullCheck(L_161);
		Material_SetVector_m752(L_161, (String_t*) &_stringLiteral25, L_175, /*hidden argument*/NULL);
		Material_t55 * L_176 = (__this->___blurAndFlaresMaterial_32);
		float L_177 = (__this->___lensFlareSaturation_21);
		NullCheck(L_176);
		Material_SetFloat_m738(L_176, (String_t*) &_stringLiteral26, L_177, /*hidden argument*/NULL);
		RenderTexture_t101 * L_178 = V_8;
		NullCheck(L_178);
		RenderTexture_DiscardContents_m758(L_178, /*hidden argument*/NULL);
		RenderTexture_t101 * L_179 = V_15;
		RenderTexture_t101 * L_180 = V_8;
		Material_t55 * L_181 = (__this->___blurAndFlaresMaterial_32);
		Graphics_Blit_m742(NULL /*static, unused*/, L_179, L_180, L_181, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_182 = V_15;
		NullCheck(L_182);
		RenderTexture_DiscardContents_m758(L_182, /*hidden argument*/NULL);
		RenderTexture_t101 * L_183 = V_8;
		RenderTexture_t101 * L_184 = V_15;
		Material_t55 * L_185 = (__this->___blurAndFlaresMaterial_32);
		Graphics_Blit_m742(NULL /*static, unused*/, L_183, L_184, L_185, 3, /*hidden argument*/NULL);
		Material_t55 * L_186 = (__this->___blurAndFlaresMaterial_32);
		float L_187 = V_16;
		float L_188 = V_18;
		float L_189 = V_17;
		float L_190 = V_18;
		Vector4_t236  L_191 = {0};
		Vector4__ctor_m751(&L_191, ((float)((float)L_187*(float)L_188)), ((float)((float)L_189*(float)L_190)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_186);
		Material_SetVector_m752(L_186, (String_t*) &_stringLiteral23, L_191, /*hidden argument*/NULL);
		Material_t55 * L_192 = (__this->___blurAndFlaresMaterial_32);
		float L_193 = (__this->___hollyStretchWidth_18);
		NullCheck(L_192);
		Material_SetFloat_m738(L_192, (String_t*) &_stringLiteral27, L_193, /*hidden argument*/NULL);
		RenderTexture_t101 * L_194 = V_8;
		NullCheck(L_194);
		RenderTexture_DiscardContents_m758(L_194, /*hidden argument*/NULL);
		RenderTexture_t101 * L_195 = V_15;
		RenderTexture_t101 * L_196 = V_8;
		Material_t55 * L_197 = (__this->___blurAndFlaresMaterial_32);
		Graphics_Blit_m742(NULL /*static, unused*/, L_195, L_196, L_197, 1, /*hidden argument*/NULL);
		Material_t55 * L_198 = (__this->___blurAndFlaresMaterial_32);
		float L_199 = (__this->___hollyStretchWidth_18);
		NullCheck(L_198);
		Material_SetFloat_m738(L_198, (String_t*) &_stringLiteral27, ((float)((float)L_199*(float)(2.0f))), /*hidden argument*/NULL);
		RenderTexture_t101 * L_200 = V_15;
		NullCheck(L_200);
		RenderTexture_DiscardContents_m758(L_200, /*hidden argument*/NULL);
		RenderTexture_t101 * L_201 = V_8;
		RenderTexture_t101 * L_202 = V_15;
		Material_t55 * L_203 = (__this->___blurAndFlaresMaterial_32);
		Graphics_Blit_m742(NULL /*static, unused*/, L_201, L_202, L_203, 1, /*hidden argument*/NULL);
		Material_t55 * L_204 = (__this->___blurAndFlaresMaterial_32);
		float L_205 = (__this->___hollyStretchWidth_18);
		NullCheck(L_204);
		Material_SetFloat_m738(L_204, (String_t*) &_stringLiteral27, ((float)((float)L_205*(float)(4.0f))), /*hidden argument*/NULL);
		RenderTexture_t101 * L_206 = V_8;
		NullCheck(L_206);
		RenderTexture_DiscardContents_m758(L_206, /*hidden argument*/NULL);
		RenderTexture_t101 * L_207 = V_15;
		RenderTexture_t101 * L_208 = V_8;
		Material_t55 * L_209 = (__this->___blurAndFlaresMaterial_32);
		Graphics_Blit_m742(NULL /*static, unused*/, L_207, L_208, L_209, 1, /*hidden argument*/NULL);
		V_19 = 0;
		goto IL_06b4;
	}

IL_061a:
	{
		float L_210 = (__this->___hollyStretchWidth_18);
		float L_211 = V_6;
		float L_212 = V_7;
		V_18 = ((float)((float)((float)((float)((float)((float)L_210*(float)(2.0f)))/(float)L_211))*(float)L_212));
		Material_t55 * L_213 = (__this->___blurAndFlaresMaterial_32);
		float L_214 = V_18;
		float L_215 = V_16;
		float L_216 = V_18;
		float L_217 = V_17;
		Vector4_t236  L_218 = {0};
		Vector4__ctor_m751(&L_218, ((float)((float)L_214*(float)L_215)), ((float)((float)L_216*(float)L_217)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_213);
		Material_SetVector_m752(L_213, (String_t*) &_stringLiteral23, L_218, /*hidden argument*/NULL);
		RenderTexture_t101 * L_219 = V_15;
		NullCheck(L_219);
		RenderTexture_DiscardContents_m758(L_219, /*hidden argument*/NULL);
		RenderTexture_t101 * L_220 = V_8;
		RenderTexture_t101 * L_221 = V_15;
		Material_t55 * L_222 = (__this->___blurAndFlaresMaterial_32);
		Graphics_Blit_m742(NULL /*static, unused*/, L_220, L_221, L_222, 4, /*hidden argument*/NULL);
		Material_t55 * L_223 = (__this->___blurAndFlaresMaterial_32);
		float L_224 = V_18;
		float L_225 = V_16;
		float L_226 = V_18;
		float L_227 = V_17;
		Vector4_t236  L_228 = {0};
		Vector4__ctor_m751(&L_228, ((float)((float)L_224*(float)L_225)), ((float)((float)L_226*(float)L_227)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_223);
		Material_SetVector_m752(L_223, (String_t*) &_stringLiteral23, L_228, /*hidden argument*/NULL);
		RenderTexture_t101 * L_229 = V_8;
		NullCheck(L_229);
		RenderTexture_DiscardContents_m758(L_229, /*hidden argument*/NULL);
		RenderTexture_t101 * L_230 = V_15;
		RenderTexture_t101 * L_231 = V_8;
		Material_t55 * L_232 = (__this->___blurAndFlaresMaterial_32);
		Graphics_Blit_m742(NULL /*static, unused*/, L_230, L_231, L_232, 4, /*hidden argument*/NULL);
		int32_t L_233 = V_19;
		V_19 = ((int32_t)((int32_t)L_233+(int32_t)1));
	}

IL_06b4:
	{
		int32_t L_234 = V_19;
		int32_t L_235 = (__this->___hollywoodFlareBlurIterations_15);
		if ((((int32_t)L_234) < ((int32_t)L_235)))
		{
			goto IL_061a;
		}
	}
	{
		int32_t L_236 = (__this->___lensflareMode_17);
		if ((!(((uint32_t)L_236) == ((uint32_t)1))))
		{
			goto IL_06e1;
		}
	}
	{
		RenderTexture_t101 * L_237 = V_8;
		RenderTexture_t101 * L_238 = V_11;
		Bloom_AddTo_m190(__this, (1.0f), L_237, L_238, /*hidden argument*/NULL);
		goto IL_0709;
	}

IL_06e1:
	{
		RenderTexture_t101 * L_239 = V_8;
		RenderTexture_t101 * L_240 = V_15;
		Bloom_Vignette_m194(__this, (1.0f), L_239, L_240, /*hidden argument*/NULL);
		RenderTexture_t101 * L_241 = V_15;
		RenderTexture_t101 * L_242 = V_8;
		Bloom_BlendFlares_m191(__this, L_241, L_242, /*hidden argument*/NULL);
		RenderTexture_t101 * L_243 = V_8;
		RenderTexture_t101 * L_244 = V_11;
		Bloom_AddTo_m190(__this, (1.0f), L_243, L_244, /*hidden argument*/NULL);
	}

IL_0709:
	{
		RenderTexture_t101 * L_245 = V_15;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_245, /*hidden argument*/NULL);
	}

IL_0710:
	{
		int32_t L_246 = V_0;
		V_20 = L_246;
		Material_t55 * L_247 = (__this->___screenBlend_30);
		float L_248 = (__this->___bloomIntensity_11);
		NullCheck(L_247);
		Material_SetFloat_m738(L_247, (String_t*) &_stringLiteral28, L_248, /*hidden argument*/NULL);
		Material_t55 * L_249 = (__this->___screenBlend_30);
		RenderTexture_t101 * L_250 = ___source;
		NullCheck(L_249);
		Material_SetTexture_m759(L_249, (String_t*) &_stringLiteral29, L_250, /*hidden argument*/NULL);
		int32_t L_251 = (__this->___quality_10);
		if ((((int32_t)L_251) <= ((int32_t)0)))
		{
			goto IL_0776;
		}
	}
	{
		int32_t L_252 = V_2;
		int32_t L_253 = V_3;
		int32_t L_254 = V_1;
		RenderTexture_t101 * L_255 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_252, L_253, 0, L_254, /*hidden argument*/NULL);
		V_21 = L_255;
		RenderTexture_t101 * L_256 = V_11;
		RenderTexture_t101 * L_257 = V_21;
		Graphics_Blit_m737(NULL /*static, unused*/, L_256, L_257, /*hidden argument*/NULL);
		RenderTexture_t101 * L_258 = V_21;
		RenderTexture_t101 * L_259 = ___destination;
		Material_t55 * L_260 = (__this->___screenBlend_30);
		int32_t L_261 = V_20;
		Graphics_Blit_m742(NULL /*static, unused*/, L_258, L_259, L_260, L_261, /*hidden argument*/NULL);
		RenderTexture_t101 * L_262 = V_21;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_262, /*hidden argument*/NULL);
		goto IL_0786;
	}

IL_0776:
	{
		RenderTexture_t101 * L_263 = V_11;
		RenderTexture_t101 * L_264 = ___destination;
		Material_t55 * L_265 = (__this->___screenBlend_30);
		int32_t L_266 = V_20;
		Graphics_Blit_m742(NULL /*static, unused*/, L_263, L_264, L_265, L_266, /*hidden argument*/NULL);
	}

IL_0786:
	{
		RenderTexture_t101 * L_267 = V_8;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_267, /*hidden argument*/NULL);
		RenderTexture_t101 * L_268 = V_11;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_268, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Bloom::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Bloom_AddTo_m190 (Bloom_t64 * __this, float ___intensity_, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___screenBlend_30);
		float L_1 = ___intensity_;
		NullCheck(L_0);
		Material_SetFloat_m738(L_0, (String_t*) &_stringLiteral28, L_1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_2 = ___to;
		NullCheck(L_2);
		RenderTexture_MarkRestoreExpected_m756(L_2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_3 = ___from;
		RenderTexture_t101 * L_4 = ___to;
		Material_t55 * L_5 = (__this->___screenBlend_30);
		Graphics_Blit_m742(NULL /*static, unused*/, L_3, L_4, L_5, ((int32_t)9), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Bloom::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Bloom_BlendFlares_m191 (Bloom_t64 * __this, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___lensFlareMaterial_28);
		Color_t65 * L_1 = &(__this->___flareColorA_22);
		float L_2 = (L_1->___r_0);
		Color_t65 * L_3 = &(__this->___flareColorA_22);
		float L_4 = (L_3->___g_1);
		Color_t65 * L_5 = &(__this->___flareColorA_22);
		float L_6 = (L_5->___b_2);
		Color_t65 * L_7 = &(__this->___flareColorA_22);
		float L_8 = (L_7->___a_3);
		Vector4_t236  L_9 = {0};
		Vector4__ctor_m751(&L_9, L_2, L_4, L_6, L_8, /*hidden argument*/NULL);
		float L_10 = (__this->___lensflareIntensity_19);
		Vector4_t236  L_11 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m752(L_0, (String_t*) &_stringLiteral30, L_11, /*hidden argument*/NULL);
		Material_t55 * L_12 = (__this->___lensFlareMaterial_28);
		Color_t65 * L_13 = &(__this->___flareColorB_23);
		float L_14 = (L_13->___r_0);
		Color_t65 * L_15 = &(__this->___flareColorB_23);
		float L_16 = (L_15->___g_1);
		Color_t65 * L_17 = &(__this->___flareColorB_23);
		float L_18 = (L_17->___b_2);
		Color_t65 * L_19 = &(__this->___flareColorB_23);
		float L_20 = (L_19->___a_3);
		Vector4_t236  L_21 = {0};
		Vector4__ctor_m751(&L_21, L_14, L_16, L_18, L_20, /*hidden argument*/NULL);
		float L_22 = (__this->___lensflareIntensity_19);
		Vector4_t236  L_23 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_12);
		Material_SetVector_m752(L_12, (String_t*) &_stringLiteral31, L_23, /*hidden argument*/NULL);
		Material_t55 * L_24 = (__this->___lensFlareMaterial_28);
		Color_t65 * L_25 = &(__this->___flareColorC_24);
		float L_26 = (L_25->___r_0);
		Color_t65 * L_27 = &(__this->___flareColorC_24);
		float L_28 = (L_27->___g_1);
		Color_t65 * L_29 = &(__this->___flareColorC_24);
		float L_30 = (L_29->___b_2);
		Color_t65 * L_31 = &(__this->___flareColorC_24);
		float L_32 = (L_31->___a_3);
		Vector4_t236  L_33 = {0};
		Vector4__ctor_m751(&L_33, L_26, L_28, L_30, L_32, /*hidden argument*/NULL);
		float L_34 = (__this->___lensflareIntensity_19);
		Vector4_t236  L_35 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_24);
		Material_SetVector_m752(L_24, (String_t*) &_stringLiteral32, L_35, /*hidden argument*/NULL);
		Material_t55 * L_36 = (__this->___lensFlareMaterial_28);
		Color_t65 * L_37 = &(__this->___flareColorD_25);
		float L_38 = (L_37->___r_0);
		Color_t65 * L_39 = &(__this->___flareColorD_25);
		float L_40 = (L_39->___g_1);
		Color_t65 * L_41 = &(__this->___flareColorD_25);
		float L_42 = (L_41->___b_2);
		Color_t65 * L_43 = &(__this->___flareColorD_25);
		float L_44 = (L_43->___a_3);
		Vector4_t236  L_45 = {0};
		Vector4__ctor_m751(&L_45, L_38, L_40, L_42, L_44, /*hidden argument*/NULL);
		float L_46 = (__this->___lensflareIntensity_19);
		Vector4_t236  L_47 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		NullCheck(L_36);
		Material_SetVector_m752(L_36, (String_t*) &_stringLiteral33, L_47, /*hidden argument*/NULL);
		RenderTexture_t101 * L_48 = ___to;
		NullCheck(L_48);
		RenderTexture_MarkRestoreExpected_m756(L_48, /*hidden argument*/NULL);
		RenderTexture_t101 * L_49 = ___from;
		RenderTexture_t101 * L_50 = ___to;
		Material_t55 * L_51 = (__this->___lensFlareMaterial_28);
		Graphics_Blit_m739(NULL /*static, unused*/, L_49, L_50, L_51, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Bloom::BrightFilter(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Bloom_BrightFilter_m192 (Bloom_t64 * __this, float ___thresh, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___brightPassFilterMaterial_34);
		float L_1 = ___thresh;
		float L_2 = ___thresh;
		float L_3 = ___thresh;
		float L_4 = ___thresh;
		Vector4_t236  L_5 = {0};
		Vector4__ctor_m751(&L_5, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m752(L_0, (String_t*) &_stringLiteral24, L_5, /*hidden argument*/NULL);
		RenderTexture_t101 * L_6 = ___from;
		RenderTexture_t101 * L_7 = ___to;
		Material_t55 * L_8 = (__this->___brightPassFilterMaterial_34);
		Graphics_Blit_m742(NULL /*static, unused*/, L_6, L_7, L_8, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Bloom::BrightFilter(UnityEngine.Color,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Bloom_BrightFilter_m193 (Bloom_t64 * __this, Color_t65  ___threshColor, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___brightPassFilterMaterial_34);
		Color_t65  L_1 = ___threshColor;
		Vector4_t236  L_2 = Color_op_Implicit_m760(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m752(L_0, (String_t*) &_stringLiteral24, L_2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_3 = ___from;
		RenderTexture_t101 * L_4 = ___to;
		Material_t55 * L_5 = (__this->___brightPassFilterMaterial_34);
		Graphics_Blit_m742(NULL /*static, unused*/, L_3, L_4, L_5, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Bloom::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Bloom_Vignette_m194 (Bloom_t64 * __this, float ___amount, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, const MethodInfo* method)
{
	RenderTexture_t101 * G_B4_0 = {0};
	Material_t55 * G_B6_0 = {0};
	RenderTexture_t101 * G_B6_1 = {0};
	RenderTexture_t101 * G_B6_2 = {0};
	Material_t55 * G_B5_0 = {0};
	RenderTexture_t101 * G_B5_1 = {0};
	RenderTexture_t101 * G_B5_2 = {0};
	int32_t G_B7_0 = 0;
	Material_t55 * G_B7_1 = {0};
	RenderTexture_t101 * G_B7_2 = {0};
	RenderTexture_t101 * G_B7_3 = {0};
	{
		Texture2D_t63 * L_0 = (__this->___lensFlareVignetteMask_26);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0063;
		}
	}
	{
		Material_t55 * L_2 = (__this->___screenBlend_30);
		Texture2D_t63 * L_3 = (__this->___lensFlareVignetteMask_26);
		NullCheck(L_2);
		Material_SetTexture_m759(L_2, (String_t*) &_stringLiteral29, L_3, /*hidden argument*/NULL);
		RenderTexture_t101 * L_4 = ___to;
		NullCheck(L_4);
		RenderTexture_MarkRestoreExpected_m756(L_4, /*hidden argument*/NULL);
		RenderTexture_t101 * L_5 = ___from;
		RenderTexture_t101 * L_6 = ___to;
		bool L_7 = Object_op_Equality_m640(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003e;
		}
	}
	{
		G_B4_0 = ((RenderTexture_t101 *)(NULL));
		goto IL_003f;
	}

IL_003e:
	{
		RenderTexture_t101 * L_8 = ___from;
		G_B4_0 = L_8;
	}

IL_003f:
	{
		RenderTexture_t101 * L_9 = ___to;
		Material_t55 * L_10 = (__this->___screenBlend_30);
		RenderTexture_t101 * L_11 = ___from;
		RenderTexture_t101 * L_12 = ___to;
		bool L_13 = Object_op_Equality_m640(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		G_B5_0 = L_10;
		G_B5_1 = L_9;
		G_B5_2 = G_B4_0;
		if (!L_13)
		{
			G_B6_0 = L_10;
			G_B6_1 = L_9;
			G_B6_2 = G_B4_0;
			goto IL_0058;
		}
	}
	{
		G_B7_0 = 7;
		G_B7_1 = G_B5_0;
		G_B7_2 = G_B5_1;
		G_B7_3 = G_B5_2;
		goto IL_0059;
	}

IL_0058:
	{
		G_B7_0 = 3;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
		G_B7_3 = G_B6_2;
	}

IL_0059:
	{
		Graphics_Blit_m742(NULL /*static, unused*/, G_B7_3, G_B7_2, G_B7_1, G_B7_0, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0063:
	{
		RenderTexture_t101 * L_14 = ___from;
		RenderTexture_t101 * L_15 = ___to;
		bool L_16 = Object_op_Inequality_m623(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0088;
		}
	}
	{
		RenderTexture_t101 * L_17 = ___to;
		Graphics_SetRenderTarget_m753(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Color_t65  L_18 = Color_get_black_m754(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m755(NULL /*static, unused*/, 0, 1, L_18, /*hidden argument*/NULL);
		RenderTexture_t101 * L_19 = ___from;
		RenderTexture_t101 * L_20 = ___to;
		Graphics_Blit_m737(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
	}

IL_0088:
	{
		return;
	}
}
// UnityStandardAssets.ImageEffects.LensflareStyle34
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_8.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.LensflareStyle34
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_8MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.TweakMode34
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_9.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.TweakMode34
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_9MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.HDRBloomMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_10.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.HDRBloomMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_10MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.BloomScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_11.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.BloomScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_11MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.BloomAndFlares
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_12.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.BloomAndFlares
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_12MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::.ctor()
extern "C" void BloomAndFlares__ctor_m195 (BloomAndFlares_t70 * __this, const MethodInfo* method)
{
	{
		__this->___screenBlendMode_6 = 1;
		__this->___sepBlurSpread_9 = (1.5f);
		__this->___useSrcAlphaAsMask_10 = (0.5f);
		__this->___bloomIntensity_11 = (1.0f);
		__this->___bloomThreshold_12 = (0.5f);
		__this->___bloomBlurIterations_13 = 2;
		__this->___hollywoodFlareBlurIterations_15 = 2;
		__this->___lensflareMode_16 = 1;
		__this->___hollyStretchWidth_17 = (3.5f);
		__this->___lensflareIntensity_18 = (1.0f);
		__this->___lensflareThreshold_19 = (0.3f);
		Color_t65  L_0 = {0};
		Color__ctor_m746(&L_0, (0.4f), (0.4f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->___flareColorA_20 = L_0;
		Color_t65  L_1 = {0};
		Color__ctor_m746(&L_1, (0.4f), (0.8f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->___flareColorB_21 = L_1;
		Color_t65  L_2 = {0};
		Color__ctor_m746(&L_2, (0.8f), (0.4f), (0.8f), (0.75f), /*hidden argument*/NULL);
		__this->___flareColorC_22 = L_2;
		Color_t65  L_3 = {0};
		Color__ctor_m746(&L_3, (0.8f), (0.4f), (0.0f), (0.75f), /*hidden argument*/NULL);
		__this->___flareColorD_23 = L_3;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.BloomAndFlares::CheckResources()
extern "C" bool BloomAndFlares_CheckResources_m196 (BloomAndFlares_t70 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___screenBlendShader_33);
		Material_t55 * L_1 = (__this->___screenBlend_34);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___screenBlend_34 = L_2;
		Shader_t54 * L_3 = (__this->___lensFlareShader_25);
		Material_t55 * L_4 = (__this->___lensFlareMaterial_26);
		Material_t55 * L_5 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_3, L_4, /*hidden argument*/NULL);
		__this->___lensFlareMaterial_26 = L_5;
		Shader_t54 * L_6 = (__this->___vignetteShader_27);
		Material_t55 * L_7 = (__this->___vignetteMaterial_28);
		Material_t55 * L_8 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_6, L_7, /*hidden argument*/NULL);
		__this->___vignetteMaterial_28 = L_8;
		Shader_t54 * L_9 = (__this->___separableBlurShader_29);
		Material_t55 * L_10 = (__this->___separableBlurMaterial_30);
		Material_t55 * L_11 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_9, L_10, /*hidden argument*/NULL);
		__this->___separableBlurMaterial_30 = L_11;
		Shader_t54 * L_12 = (__this->___addBrightStuffOneOneShader_31);
		Material_t55 * L_13 = (__this->___addBrightStuffBlendOneOneMaterial_32);
		Material_t55 * L_14 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_12, L_13, /*hidden argument*/NULL);
		__this->___addBrightStuffBlendOneOneMaterial_32 = L_14;
		Shader_t54 * L_15 = (__this->___hollywoodFlaresShader_35);
		Material_t55 * L_16 = (__this->___hollywoodFlaresMaterial_36);
		Material_t55 * L_17 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_15, L_16, /*hidden argument*/NULL);
		__this->___hollywoodFlaresMaterial_36 = L_17;
		Shader_t54 * L_18 = (__this->___brightPassFilterShader_37);
		Material_t55 * L_19 = (__this->___brightPassFilterMaterial_38);
		Material_t55 * L_20 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_18, L_19, /*hidden argument*/NULL);
		__this->___brightPassFilterMaterial_38 = L_20;
		bool L_21 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_21)
		{
			goto IL_00c1;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		bool L_22 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_22;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void BloomAndFlares_OnRenderImage_m197 (BloomAndFlares_t70 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	RenderTexture_t101 * V_2 = {0};
	RenderTexture_t101 * V_3 = {0};
	RenderTexture_t101 * V_4 = {0};
	RenderTexture_t101 * V_5 = {0};
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	RenderTexture_t101 * V_10 = {0};
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	BloomAndFlares_t70 * G_B5_0 = {0};
	BloomAndFlares_t70 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	BloomAndFlares_t70 * G_B6_1 = {0};
	BloomAndFlares_t70 * G_B10_0 = {0};
	BloomAndFlares_t70 * G_B9_0 = {0};
	int32_t G_B11_0 = 0;
	BloomAndFlares_t70 * G_B11_1 = {0};
	int32_t G_B16_0 = 0;
	RenderTexture_t101 * G_B22_0 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.BloomAndFlares::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		__this->___doHdr_8 = 0;
		int32_t L_3 = (__this->___hdr_7);
		if (L_3)
		{
			goto IL_004a;
		}
	}
	{
		RenderTexture_t101 * L_4 = ___source;
		NullCheck(L_4);
		int32_t L_5 = RenderTexture_get_format_m747(L_4, /*hidden argument*/NULL);
		G_B4_0 = __this;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			G_B5_0 = __this;
			goto IL_003f;
		}
	}
	{
		Camera_t27 * L_6 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_6);
		bool L_7 = Camera_get_hdr_m748(L_6, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_7));
		G_B6_1 = G_B4_0;
		goto IL_0040;
	}

IL_003f:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0040:
	{
		NullCheck(G_B6_1);
		G_B6_1->___doHdr_8 = G_B6_0;
		goto IL_0059;
	}

IL_004a:
	{
		int32_t L_8 = (__this->___hdr_7);
		__this->___doHdr_8 = ((((int32_t)L_8) == ((int32_t)1))? 1 : 0);
	}

IL_0059:
	{
		bool L_9 = (__this->___doHdr_8);
		G_B9_0 = __this;
		if (!L_9)
		{
			G_B10_0 = __this;
			goto IL_006d;
		}
	}
	{
		bool L_10 = (((PostEffectsBase_t57 *)__this)->___supportHDRTextures_2);
		G_B11_0 = ((int32_t)(L_10));
		G_B11_1 = G_B9_0;
		goto IL_006e;
	}

IL_006d:
	{
		G_B11_0 = 0;
		G_B11_1 = G_B10_0;
	}

IL_006e:
	{
		NullCheck(G_B11_1);
		G_B11_1->___doHdr_8 = G_B11_0;
		int32_t L_11 = (__this->___screenBlendMode_6);
		V_0 = L_11;
		bool L_12 = (__this->___doHdr_8);
		if (!L_12)
		{
			goto IL_0087;
		}
	}
	{
		V_0 = 1;
	}

IL_0087:
	{
		bool L_13 = (__this->___doHdr_8);
		if (!L_13)
		{
			goto IL_0098;
		}
	}
	{
		G_B16_0 = 2;
		goto IL_0099;
	}

IL_0098:
	{
		G_B16_0 = 7;
	}

IL_0099:
	{
		V_1 = G_B16_0;
		RenderTexture_t101 * L_14 = ___source;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_14);
		RenderTexture_t101 * L_16 = ___source;
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_16);
		int32_t L_18 = V_1;
		RenderTexture_t101 * L_19 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_15/(int32_t)2)), ((int32_t)((int32_t)L_17/(int32_t)2)), 0, L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		RenderTexture_t101 * L_20 = ___source;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_20);
		RenderTexture_t101 * L_22 = ___source;
		NullCheck(L_22);
		int32_t L_23 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_22);
		int32_t L_24 = V_1;
		RenderTexture_t101 * L_25 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_21/(int32_t)4)), ((int32_t)((int32_t)L_23/(int32_t)4)), 0, L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		RenderTexture_t101 * L_26 = ___source;
		NullCheck(L_26);
		int32_t L_27 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_26);
		RenderTexture_t101 * L_28 = ___source;
		NullCheck(L_28);
		int32_t L_29 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_28);
		int32_t L_30 = V_1;
		RenderTexture_t101 * L_31 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_27/(int32_t)4)), ((int32_t)((int32_t)L_29/(int32_t)4)), 0, L_30, /*hidden argument*/NULL);
		V_4 = L_31;
		RenderTexture_t101 * L_32 = ___source;
		NullCheck(L_32);
		int32_t L_33 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_32);
		RenderTexture_t101 * L_34 = ___source;
		NullCheck(L_34);
		int32_t L_35 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_34);
		int32_t L_36 = V_1;
		RenderTexture_t101 * L_37 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_33/(int32_t)4)), ((int32_t)((int32_t)L_35/(int32_t)4)), 0, L_36, /*hidden argument*/NULL);
		V_5 = L_37;
		RenderTexture_t101 * L_38 = ___source;
		NullCheck(L_38);
		int32_t L_39 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_38);
		RenderTexture_t101 * L_40 = ___source;
		NullCheck(L_40);
		int32_t L_41 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_40);
		V_6 = ((float)((float)((float)((float)(1.0f)*(float)(((float)L_39))))/(float)((float)((float)(1.0f)*(float)(((float)L_41))))));
		V_7 = (0.001953125f);
		RenderTexture_t101 * L_42 = ___source;
		RenderTexture_t101 * L_43 = V_2;
		Material_t55 * L_44 = (__this->___screenBlend_34);
		Graphics_Blit_m742(NULL /*static, unused*/, L_42, L_43, L_44, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_45 = V_2;
		RenderTexture_t101 * L_46 = V_3;
		Material_t55 * L_47 = (__this->___screenBlend_34);
		Graphics_Blit_m742(NULL /*static, unused*/, L_45, L_46, L_47, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_48 = V_2;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		float L_49 = (__this->___bloomThreshold_12);
		float L_50 = (__this->___useSrcAlphaAsMask_10);
		RenderTexture_t101 * L_51 = V_3;
		RenderTexture_t101 * L_52 = V_4;
		BloomAndFlares_BrightFilter_m200(__this, L_49, L_50, L_51, L_52, /*hidden argument*/NULL);
		RenderTexture_t101 * L_53 = V_3;
		NullCheck(L_53);
		RenderTexture_DiscardContents_m758(L_53, /*hidden argument*/NULL);
		int32_t L_54 = (__this->___bloomBlurIterations_13);
		if ((((int32_t)L_54) >= ((int32_t)1)))
		{
			goto IL_0170;
		}
	}
	{
		__this->___bloomBlurIterations_13 = 1;
	}

IL_0170:
	{
		V_8 = 0;
		goto IL_0227;
	}

IL_0178:
	{
		int32_t L_55 = V_8;
		float L_56 = (__this->___sepBlurSpread_9);
		V_9 = ((float)((float)((float)((float)(1.0f)+(float)((float)((float)(((float)L_55))*(float)(0.5f)))))*(float)L_56));
		Material_t55 * L_57 = (__this->___separableBlurMaterial_30);
		float L_58 = V_9;
		float L_59 = V_7;
		Vector4_t236  L_60 = {0};
		Vector4__ctor_m751(&L_60, (0.0f), ((float)((float)L_58*(float)L_59)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_57);
		Material_SetVector_m752(L_57, (String_t*) &_stringLiteral34, L_60, /*hidden argument*/NULL);
		int32_t L_61 = V_8;
		if (L_61)
		{
			goto IL_01c7;
		}
	}
	{
		RenderTexture_t101 * L_62 = V_4;
		G_B22_0 = L_62;
		goto IL_01c8;
	}

IL_01c7:
	{
		RenderTexture_t101 * L_63 = V_3;
		G_B22_0 = L_63;
	}

IL_01c8:
	{
		V_10 = G_B22_0;
		RenderTexture_t101 * L_64 = V_10;
		RenderTexture_t101 * L_65 = V_5;
		Material_t55 * L_66 = (__this->___separableBlurMaterial_30);
		Graphics_Blit_m739(NULL /*static, unused*/, L_64, L_65, L_66, /*hidden argument*/NULL);
		RenderTexture_t101 * L_67 = V_10;
		NullCheck(L_67);
		RenderTexture_DiscardContents_m758(L_67, /*hidden argument*/NULL);
		Material_t55 * L_68 = (__this->___separableBlurMaterial_30);
		float L_69 = V_9;
		float L_70 = V_6;
		float L_71 = V_7;
		Vector4_t236  L_72 = {0};
		Vector4__ctor_m751(&L_72, ((float)((float)((float)((float)L_69/(float)L_70))*(float)L_71)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_68);
		Material_SetVector_m752(L_68, (String_t*) &_stringLiteral34, L_72, /*hidden argument*/NULL);
		RenderTexture_t101 * L_73 = V_5;
		RenderTexture_t101 * L_74 = V_3;
		Material_t55 * L_75 = (__this->___separableBlurMaterial_30);
		Graphics_Blit_m739(NULL /*static, unused*/, L_73, L_74, L_75, /*hidden argument*/NULL);
		RenderTexture_t101 * L_76 = V_5;
		NullCheck(L_76);
		RenderTexture_DiscardContents_m758(L_76, /*hidden argument*/NULL);
		int32_t L_77 = V_8;
		V_8 = ((int32_t)((int32_t)L_77+(int32_t)1));
	}

IL_0227:
	{
		int32_t L_78 = V_8;
		int32_t L_79 = (__this->___bloomBlurIterations_13);
		if ((((int32_t)L_78) < ((int32_t)L_79)))
		{
			goto IL_0178;
		}
	}
	{
		bool L_80 = (__this->___lensflares_14);
		if (!L_80)
		{
			goto IL_05e1;
		}
	}
	{
		int32_t L_81 = (__this->___lensflareMode_16);
		if (L_81)
		{
			goto IL_028f;
		}
	}
	{
		float L_82 = (__this->___lensflareThreshold_19);
		RenderTexture_t101 * L_83 = V_3;
		RenderTexture_t101 * L_84 = V_5;
		BloomAndFlares_BrightFilter_m200(__this, L_82, (0.0f), L_83, L_84, /*hidden argument*/NULL);
		RenderTexture_t101 * L_85 = V_3;
		NullCheck(L_85);
		RenderTexture_DiscardContents_m758(L_85, /*hidden argument*/NULL);
		RenderTexture_t101 * L_86 = V_5;
		RenderTexture_t101 * L_87 = V_4;
		BloomAndFlares_Vignette_m201(__this, (0.975f), L_86, L_87, /*hidden argument*/NULL);
		RenderTexture_t101 * L_88 = V_5;
		NullCheck(L_88);
		RenderTexture_DiscardContents_m758(L_88, /*hidden argument*/NULL);
		RenderTexture_t101 * L_89 = V_4;
		RenderTexture_t101 * L_90 = V_3;
		BloomAndFlares_BlendFlares_m199(__this, L_89, L_90, /*hidden argument*/NULL);
		RenderTexture_t101 * L_91 = V_4;
		NullCheck(L_91);
		RenderTexture_DiscardContents_m758(L_91, /*hidden argument*/NULL);
		goto IL_05e1;
	}

IL_028f:
	{
		Material_t55 * L_92 = (__this->___hollywoodFlaresMaterial_36);
		float L_93 = (__this->___lensflareThreshold_19);
		float L_94 = (__this->___lensflareThreshold_19);
		Vector4_t236  L_95 = {0};
		Vector4__ctor_m751(&L_95, L_93, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)-(float)L_94)))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_92);
		Material_SetVector_m752(L_92, (String_t*) &_stringLiteral35, L_95, /*hidden argument*/NULL);
		Material_t55 * L_96 = (__this->___hollywoodFlaresMaterial_36);
		Color_t65 * L_97 = &(__this->___flareColorA_20);
		float L_98 = (L_97->___r_0);
		Color_t65 * L_99 = &(__this->___flareColorA_20);
		float L_100 = (L_99->___g_1);
		Color_t65 * L_101 = &(__this->___flareColorA_20);
		float L_102 = (L_101->___b_2);
		Color_t65 * L_103 = &(__this->___flareColorA_20);
		float L_104 = (L_103->___a_3);
		Vector4_t236  L_105 = {0};
		Vector4__ctor_m751(&L_105, L_98, L_100, L_102, L_104, /*hidden argument*/NULL);
		Color_t65 * L_106 = &(__this->___flareColorA_20);
		float L_107 = (L_106->___a_3);
		Vector4_t236  L_108 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_105, L_107, /*hidden argument*/NULL);
		float L_109 = (__this->___lensflareIntensity_18);
		Vector4_t236  L_110 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_108, L_109, /*hidden argument*/NULL);
		NullCheck(L_96);
		Material_SetVector_m752(L_96, (String_t*) &_stringLiteral36, L_110, /*hidden argument*/NULL);
		RenderTexture_t101 * L_111 = V_5;
		RenderTexture_t101 * L_112 = V_4;
		Material_t55 * L_113 = (__this->___hollywoodFlaresMaterial_36);
		Graphics_Blit_m742(NULL /*static, unused*/, L_111, L_112, L_113, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_114 = V_5;
		NullCheck(L_114);
		RenderTexture_DiscardContents_m758(L_114, /*hidden argument*/NULL);
		RenderTexture_t101 * L_115 = V_4;
		RenderTexture_t101 * L_116 = V_5;
		Material_t55 * L_117 = (__this->___hollywoodFlaresMaterial_36);
		Graphics_Blit_m742(NULL /*static, unused*/, L_115, L_116, L_117, 3, /*hidden argument*/NULL);
		RenderTexture_t101 * L_118 = V_4;
		NullCheck(L_118);
		RenderTexture_DiscardContents_m758(L_118, /*hidden argument*/NULL);
		Material_t55 * L_119 = (__this->___hollywoodFlaresMaterial_36);
		float L_120 = (__this->___sepBlurSpread_9);
		float L_121 = V_6;
		float L_122 = V_7;
		Vector4_t236  L_123 = {0};
		Vector4__ctor_m751(&L_123, ((float)((float)((float)((float)((float)((float)L_120*(float)(1.0f)))/(float)L_121))*(float)L_122)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_119);
		Material_SetVector_m752(L_119, (String_t*) &_stringLiteral34, L_123, /*hidden argument*/NULL);
		Material_t55 * L_124 = (__this->___hollywoodFlaresMaterial_36);
		float L_125 = (__this->___hollyStretchWidth_17);
		NullCheck(L_124);
		Material_SetFloat_m738(L_124, (String_t*) &_stringLiteral37, L_125, /*hidden argument*/NULL);
		RenderTexture_t101 * L_126 = V_5;
		RenderTexture_t101 * L_127 = V_4;
		Material_t55 * L_128 = (__this->___hollywoodFlaresMaterial_36);
		Graphics_Blit_m742(NULL /*static, unused*/, L_126, L_127, L_128, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_129 = V_5;
		NullCheck(L_129);
		RenderTexture_DiscardContents_m758(L_129, /*hidden argument*/NULL);
		Material_t55 * L_130 = (__this->___hollywoodFlaresMaterial_36);
		float L_131 = (__this->___hollyStretchWidth_17);
		NullCheck(L_130);
		Material_SetFloat_m738(L_130, (String_t*) &_stringLiteral37, ((float)((float)L_131*(float)(2.0f))), /*hidden argument*/NULL);
		RenderTexture_t101 * L_132 = V_4;
		RenderTexture_t101 * L_133 = V_5;
		Material_t55 * L_134 = (__this->___hollywoodFlaresMaterial_36);
		Graphics_Blit_m742(NULL /*static, unused*/, L_132, L_133, L_134, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_135 = V_4;
		NullCheck(L_135);
		RenderTexture_DiscardContents_m758(L_135, /*hidden argument*/NULL);
		Material_t55 * L_136 = (__this->___hollywoodFlaresMaterial_36);
		float L_137 = (__this->___hollyStretchWidth_17);
		NullCheck(L_136);
		Material_SetFloat_m738(L_136, (String_t*) &_stringLiteral37, ((float)((float)L_137*(float)(4.0f))), /*hidden argument*/NULL);
		RenderTexture_t101 * L_138 = V_5;
		RenderTexture_t101 * L_139 = V_4;
		Material_t55 * L_140 = (__this->___hollywoodFlaresMaterial_36);
		Graphics_Blit_m742(NULL /*static, unused*/, L_138, L_139, L_140, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_141 = V_5;
		NullCheck(L_141);
		RenderTexture_DiscardContents_m758(L_141, /*hidden argument*/NULL);
		int32_t L_142 = (__this->___lensflareMode_16);
		if ((!(((uint32_t)L_142) == ((uint32_t)1))))
		{
			goto IL_04f2;
		}
	}
	{
		V_11 = 0;
		goto IL_04cb;
	}

IL_042d:
	{
		Material_t55 * L_143 = (__this->___separableBlurMaterial_30);
		float L_144 = (__this->___hollyStretchWidth_17);
		float L_145 = V_6;
		float L_146 = V_7;
		Vector4_t236  L_147 = {0};
		Vector4__ctor_m751(&L_147, ((float)((float)((float)((float)((float)((float)L_144*(float)(2.0f)))/(float)L_145))*(float)L_146)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_143);
		Material_SetVector_m752(L_143, (String_t*) &_stringLiteral34, L_147, /*hidden argument*/NULL);
		RenderTexture_t101 * L_148 = V_4;
		RenderTexture_t101 * L_149 = V_5;
		Material_t55 * L_150 = (__this->___separableBlurMaterial_30);
		Graphics_Blit_m739(NULL /*static, unused*/, L_148, L_149, L_150, /*hidden argument*/NULL);
		RenderTexture_t101 * L_151 = V_4;
		NullCheck(L_151);
		RenderTexture_DiscardContents_m758(L_151, /*hidden argument*/NULL);
		Material_t55 * L_152 = (__this->___separableBlurMaterial_30);
		float L_153 = (__this->___hollyStretchWidth_17);
		float L_154 = V_6;
		float L_155 = V_7;
		Vector4_t236  L_156 = {0};
		Vector4__ctor_m751(&L_156, ((float)((float)((float)((float)((float)((float)L_153*(float)(2.0f)))/(float)L_154))*(float)L_155)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_152);
		Material_SetVector_m752(L_152, (String_t*) &_stringLiteral34, L_156, /*hidden argument*/NULL);
		RenderTexture_t101 * L_157 = V_5;
		RenderTexture_t101 * L_158 = V_4;
		Material_t55 * L_159 = (__this->___separableBlurMaterial_30);
		Graphics_Blit_m739(NULL /*static, unused*/, L_157, L_158, L_159, /*hidden argument*/NULL);
		RenderTexture_t101 * L_160 = V_5;
		NullCheck(L_160);
		RenderTexture_DiscardContents_m758(L_160, /*hidden argument*/NULL);
		int32_t L_161 = V_11;
		V_11 = ((int32_t)((int32_t)L_161+(int32_t)1));
	}

IL_04cb:
	{
		int32_t L_162 = V_11;
		int32_t L_163 = (__this->___hollywoodFlareBlurIterations_15);
		if ((((int32_t)L_162) < ((int32_t)L_163)))
		{
			goto IL_042d;
		}
	}
	{
		RenderTexture_t101 * L_164 = V_4;
		RenderTexture_t101 * L_165 = V_3;
		BloomAndFlares_AddTo_m198(__this, (1.0f), L_164, L_165, /*hidden argument*/NULL);
		RenderTexture_t101 * L_166 = V_4;
		NullCheck(L_166);
		RenderTexture_DiscardContents_m758(L_166, /*hidden argument*/NULL);
		goto IL_05e1;
	}

IL_04f2:
	{
		V_12 = 0;
		goto IL_0598;
	}

IL_04fa:
	{
		Material_t55 * L_167 = (__this->___separableBlurMaterial_30);
		float L_168 = (__this->___hollyStretchWidth_17);
		float L_169 = V_6;
		float L_170 = V_7;
		Vector4_t236  L_171 = {0};
		Vector4__ctor_m751(&L_171, ((float)((float)((float)((float)((float)((float)L_168*(float)(2.0f)))/(float)L_169))*(float)L_170)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_167);
		Material_SetVector_m752(L_167, (String_t*) &_stringLiteral34, L_171, /*hidden argument*/NULL);
		RenderTexture_t101 * L_172 = V_4;
		RenderTexture_t101 * L_173 = V_5;
		Material_t55 * L_174 = (__this->___separableBlurMaterial_30);
		Graphics_Blit_m739(NULL /*static, unused*/, L_172, L_173, L_174, /*hidden argument*/NULL);
		RenderTexture_t101 * L_175 = V_4;
		NullCheck(L_175);
		RenderTexture_DiscardContents_m758(L_175, /*hidden argument*/NULL);
		Material_t55 * L_176 = (__this->___separableBlurMaterial_30);
		float L_177 = (__this->___hollyStretchWidth_17);
		float L_178 = V_6;
		float L_179 = V_7;
		Vector4_t236  L_180 = {0};
		Vector4__ctor_m751(&L_180, ((float)((float)((float)((float)((float)((float)L_177*(float)(2.0f)))/(float)L_178))*(float)L_179)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_176);
		Material_SetVector_m752(L_176, (String_t*) &_stringLiteral34, L_180, /*hidden argument*/NULL);
		RenderTexture_t101 * L_181 = V_5;
		RenderTexture_t101 * L_182 = V_4;
		Material_t55 * L_183 = (__this->___separableBlurMaterial_30);
		Graphics_Blit_m739(NULL /*static, unused*/, L_181, L_182, L_183, /*hidden argument*/NULL);
		RenderTexture_t101 * L_184 = V_5;
		NullCheck(L_184);
		RenderTexture_DiscardContents_m758(L_184, /*hidden argument*/NULL);
		int32_t L_185 = V_12;
		V_12 = ((int32_t)((int32_t)L_185+(int32_t)1));
	}

IL_0598:
	{
		int32_t L_186 = V_12;
		int32_t L_187 = (__this->___hollywoodFlareBlurIterations_15);
		if ((((int32_t)L_186) < ((int32_t)L_187)))
		{
			goto IL_04fa;
		}
	}
	{
		RenderTexture_t101 * L_188 = V_4;
		RenderTexture_t101 * L_189 = V_5;
		BloomAndFlares_Vignette_m201(__this, (1.0f), L_188, L_189, /*hidden argument*/NULL);
		RenderTexture_t101 * L_190 = V_4;
		NullCheck(L_190);
		RenderTexture_DiscardContents_m758(L_190, /*hidden argument*/NULL);
		RenderTexture_t101 * L_191 = V_5;
		RenderTexture_t101 * L_192 = V_4;
		BloomAndFlares_BlendFlares_m199(__this, L_191, L_192, /*hidden argument*/NULL);
		RenderTexture_t101 * L_193 = V_5;
		NullCheck(L_193);
		RenderTexture_DiscardContents_m758(L_193, /*hidden argument*/NULL);
		RenderTexture_t101 * L_194 = V_4;
		RenderTexture_t101 * L_195 = V_3;
		BloomAndFlares_AddTo_m198(__this, (1.0f), L_194, L_195, /*hidden argument*/NULL);
		RenderTexture_t101 * L_196 = V_4;
		NullCheck(L_196);
		RenderTexture_DiscardContents_m758(L_196, /*hidden argument*/NULL);
	}

IL_05e1:
	{
		Material_t55 * L_197 = (__this->___screenBlend_34);
		float L_198 = (__this->___bloomIntensity_11);
		NullCheck(L_197);
		Material_SetFloat_m738(L_197, (String_t*) &_stringLiteral28, L_198, /*hidden argument*/NULL);
		Material_t55 * L_199 = (__this->___screenBlend_34);
		RenderTexture_t101 * L_200 = ___source;
		NullCheck(L_199);
		Material_SetTexture_m759(L_199, (String_t*) &_stringLiteral29, L_200, /*hidden argument*/NULL);
		RenderTexture_t101 * L_201 = V_3;
		RenderTexture_t101 * L_202 = ___destination;
		Material_t55 * L_203 = (__this->___screenBlend_34);
		int32_t L_204 = V_0;
		Graphics_Blit_m742(NULL /*static, unused*/, L_201, L_202, L_203, L_204, /*hidden argument*/NULL);
		RenderTexture_t101 * L_205 = V_3;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_205, /*hidden argument*/NULL);
		RenderTexture_t101 * L_206 = V_4;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_206, /*hidden argument*/NULL);
		RenderTexture_t101 * L_207 = V_5;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_207, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void BloomAndFlares_AddTo_m198 (BloomAndFlares_t70 * __this, float ___intensity_, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___addBrightStuffBlendOneOneMaterial_32);
		float L_1 = ___intensity_;
		NullCheck(L_0);
		Material_SetFloat_m738(L_0, (String_t*) &_stringLiteral28, L_1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_2 = ___from;
		RenderTexture_t101 * L_3 = ___to;
		Material_t55 * L_4 = (__this->___addBrightStuffBlendOneOneMaterial_32);
		Graphics_Blit_m739(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void BloomAndFlares_BlendFlares_m199 (BloomAndFlares_t70 * __this, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___lensFlareMaterial_26);
		Color_t65 * L_1 = &(__this->___flareColorA_20);
		float L_2 = (L_1->___r_0);
		Color_t65 * L_3 = &(__this->___flareColorA_20);
		float L_4 = (L_3->___g_1);
		Color_t65 * L_5 = &(__this->___flareColorA_20);
		float L_6 = (L_5->___b_2);
		Color_t65 * L_7 = &(__this->___flareColorA_20);
		float L_8 = (L_7->___a_3);
		Vector4_t236  L_9 = {0};
		Vector4__ctor_m751(&L_9, L_2, L_4, L_6, L_8, /*hidden argument*/NULL);
		float L_10 = (__this->___lensflareIntensity_18);
		Vector4_t236  L_11 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m752(L_0, (String_t*) &_stringLiteral30, L_11, /*hidden argument*/NULL);
		Material_t55 * L_12 = (__this->___lensFlareMaterial_26);
		Color_t65 * L_13 = &(__this->___flareColorB_21);
		float L_14 = (L_13->___r_0);
		Color_t65 * L_15 = &(__this->___flareColorB_21);
		float L_16 = (L_15->___g_1);
		Color_t65 * L_17 = &(__this->___flareColorB_21);
		float L_18 = (L_17->___b_2);
		Color_t65 * L_19 = &(__this->___flareColorB_21);
		float L_20 = (L_19->___a_3);
		Vector4_t236  L_21 = {0};
		Vector4__ctor_m751(&L_21, L_14, L_16, L_18, L_20, /*hidden argument*/NULL);
		float L_22 = (__this->___lensflareIntensity_18);
		Vector4_t236  L_23 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_12);
		Material_SetVector_m752(L_12, (String_t*) &_stringLiteral31, L_23, /*hidden argument*/NULL);
		Material_t55 * L_24 = (__this->___lensFlareMaterial_26);
		Color_t65 * L_25 = &(__this->___flareColorC_22);
		float L_26 = (L_25->___r_0);
		Color_t65 * L_27 = &(__this->___flareColorC_22);
		float L_28 = (L_27->___g_1);
		Color_t65 * L_29 = &(__this->___flareColorC_22);
		float L_30 = (L_29->___b_2);
		Color_t65 * L_31 = &(__this->___flareColorC_22);
		float L_32 = (L_31->___a_3);
		Vector4_t236  L_33 = {0};
		Vector4__ctor_m751(&L_33, L_26, L_28, L_30, L_32, /*hidden argument*/NULL);
		float L_34 = (__this->___lensflareIntensity_18);
		Vector4_t236  L_35 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_24);
		Material_SetVector_m752(L_24, (String_t*) &_stringLiteral32, L_35, /*hidden argument*/NULL);
		Material_t55 * L_36 = (__this->___lensFlareMaterial_26);
		Color_t65 * L_37 = &(__this->___flareColorD_23);
		float L_38 = (L_37->___r_0);
		Color_t65 * L_39 = &(__this->___flareColorD_23);
		float L_40 = (L_39->___g_1);
		Color_t65 * L_41 = &(__this->___flareColorD_23);
		float L_42 = (L_41->___b_2);
		Color_t65 * L_43 = &(__this->___flareColorD_23);
		float L_44 = (L_43->___a_3);
		Vector4_t236  L_45 = {0};
		Vector4__ctor_m751(&L_45, L_38, L_40, L_42, L_44, /*hidden argument*/NULL);
		float L_46 = (__this->___lensflareIntensity_18);
		Vector4_t236  L_47 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		NullCheck(L_36);
		Material_SetVector_m752(L_36, (String_t*) &_stringLiteral33, L_47, /*hidden argument*/NULL);
		RenderTexture_t101 * L_48 = ___from;
		RenderTexture_t101 * L_49 = ___to;
		Material_t55 * L_50 = (__this->___lensFlareMaterial_26);
		Graphics_Blit_m739(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::BrightFilter(System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void BloomAndFlares_BrightFilter_m200 (BloomAndFlares_t70 * __this, float ___thresh, float ___useAlphaAsMask, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___doHdr_8);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		Material_t55 * L_1 = (__this->___brightPassFilterMaterial_38);
		float L_2 = ___thresh;
		Vector4_t236  L_3 = {0};
		Vector4__ctor_m751(&L_3, L_2, (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Material_SetVector_m752(L_1, (String_t*) &_stringLiteral38, L_3, /*hidden argument*/NULL);
		goto IL_0062;
	}

IL_0035:
	{
		Material_t55 * L_4 = (__this->___brightPassFilterMaterial_38);
		float L_5 = ___thresh;
		float L_6 = ___thresh;
		Vector4_t236  L_7 = {0};
		Vector4__ctor_m751(&L_7, L_5, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)-(float)L_6)))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		Material_SetVector_m752(L_4, (String_t*) &_stringLiteral38, L_7, /*hidden argument*/NULL);
	}

IL_0062:
	{
		Material_t55 * L_8 = (__this->___brightPassFilterMaterial_38);
		float L_9 = ___useAlphaAsMask;
		NullCheck(L_8);
		Material_SetFloat_m738(L_8, (String_t*) &_stringLiteral39, L_9, /*hidden argument*/NULL);
		RenderTexture_t101 * L_10 = ___from;
		RenderTexture_t101 * L_11 = ___to;
		Material_t55 * L_12 = (__this->___brightPassFilterMaterial_38);
		Graphics_Blit_m739(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void BloomAndFlares_Vignette_m201 (BloomAndFlares_t70 * __this, float ___amount, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, const MethodInfo* method)
{
	{
		Texture2D_t63 * L_0 = (__this->___lensFlareVignetteMask_24);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		Material_t55 * L_2 = (__this->___screenBlend_34);
		Texture2D_t63 * L_3 = (__this->___lensFlareVignetteMask_24);
		NullCheck(L_2);
		Material_SetTexture_m759(L_2, (String_t*) &_stringLiteral29, L_3, /*hidden argument*/NULL);
		RenderTexture_t101 * L_4 = ___from;
		RenderTexture_t101 * L_5 = ___to;
		Material_t55 * L_6 = (__this->___screenBlend_34);
		Graphics_Blit_m742(NULL /*static, unused*/, L_4, L_5, L_6, 3, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0039:
	{
		Material_t55 * L_7 = (__this->___vignetteMaterial_28);
		float L_8 = ___amount;
		NullCheck(L_7);
		Material_SetFloat_m738(L_7, (String_t*) &_stringLiteral40, L_8, /*hidden argument*/NULL);
		RenderTexture_t101 * L_9 = ___from;
		RenderTexture_t101 * L_10 = ___to;
		Material_t55 * L_11 = (__this->___vignetteMaterial_28);
		Graphics_Blit_m739(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// UnityStandardAssets.ImageEffects.BloomOptimized/Resolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_13.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.BloomOptimized/Resolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_13MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.BloomOptimized/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_14.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.BloomOptimized/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_14MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.BloomOptimized
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_15.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.BloomOptimized
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_15MethodDeclarations.h"

// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"


// System.Void UnityStandardAssets.ImageEffects.BloomOptimized::.ctor()
extern "C" void BloomOptimized__ctor_m202 (BloomOptimized_t73 * __this, const MethodInfo* method)
{
	{
		__this->___threshold_5 = (0.25f);
		__this->___intensity_6 = (0.75f);
		__this->___blurSize_7 = (1.0f);
		__this->___blurIterations_9 = 1;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.BloomOptimized::CheckResources()
extern "C" bool BloomOptimized_CheckResources_m203 (BloomOptimized_t73 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___fastBloomShader_11);
		Material_t55 * L_1 = (__this->___fastBloomMaterial_12);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___fastBloomMaterial_12 = L_2;
		bool L_3 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		bool L_4 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_4;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnDisable()
extern "C" void BloomOptimized_OnDisable_m204 (BloomOptimized_t73 * __this, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___fastBloomMaterial_12);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t55 * L_2 = (__this->___fastBloomMaterial_12);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void BloomOptimized_OnRenderImage_m205 (BloomOptimized_t73 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RenderTexture_t101 * V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	RenderTexture_t101 * V_7 = {0};
	int32_t G_B5_0 = 0;
	float G_B8_0 = 0.0f;
	int32_t G_B11_0 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.BloomOptimized::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		int32_t L_3 = (__this->___resolution_8);
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		G_B5_0 = 4;
		goto IL_0025;
	}

IL_0024:
	{
		G_B5_0 = 2;
	}

IL_0025:
	{
		V_0 = G_B5_0;
		int32_t L_4 = (__this->___resolution_8);
		if (L_4)
		{
			goto IL_003b;
		}
	}
	{
		G_B8_0 = (0.5f);
		goto IL_0040;
	}

IL_003b:
	{
		G_B8_0 = (1.0f);
	}

IL_0040:
	{
		V_1 = G_B8_0;
		Material_t55 * L_5 = (__this->___fastBloomMaterial_12);
		float L_6 = (__this->___blurSize_7);
		float L_7 = V_1;
		float L_8 = (__this->___threshold_5);
		float L_9 = (__this->___intensity_6);
		Vector4_t236  L_10 = {0};
		Vector4__ctor_m751(&L_10, ((float)((float)L_6*(float)L_7)), (0.0f), L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_SetVector_m752(L_5, (String_t*) &_stringLiteral41, L_10, /*hidden argument*/NULL);
		RenderTexture_t101 * L_11 = ___source;
		NullCheck(L_11);
		Texture_set_filterMode_m762(L_11, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_12 = ___source;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_12);
		int32_t L_14 = V_0;
		V_2 = ((int32_t)((int32_t)L_13/(int32_t)L_14));
		RenderTexture_t101 * L_15 = ___source;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_15);
		int32_t L_17 = V_0;
		V_3 = ((int32_t)((int32_t)L_16/(int32_t)L_17));
		int32_t L_18 = V_2;
		int32_t L_19 = V_3;
		RenderTexture_t101 * L_20 = ___source;
		NullCheck(L_20);
		int32_t L_21 = RenderTexture_get_format_m747(L_20, /*hidden argument*/NULL);
		RenderTexture_t101 * L_22 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_18, L_19, 0, L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		RenderTexture_t101 * L_23 = V_4;
		NullCheck(L_23);
		Texture_set_filterMode_m762(L_23, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_24 = ___source;
		RenderTexture_t101 * L_25 = V_4;
		Material_t55 * L_26 = (__this->___fastBloomMaterial_12);
		Graphics_Blit_m742(NULL /*static, unused*/, L_24, L_25, L_26, 1, /*hidden argument*/NULL);
		int32_t L_27 = (__this->___blurType_10);
		if (L_27)
		{
			goto IL_00c0;
		}
	}
	{
		G_B11_0 = 0;
		goto IL_00c1;
	}

IL_00c0:
	{
		G_B11_0 = 2;
	}

IL_00c1:
	{
		V_5 = G_B11_0;
		V_6 = 0;
		goto IL_0175;
	}

IL_00cb:
	{
		Material_t55 * L_28 = (__this->___fastBloomMaterial_12);
		float L_29 = (__this->___blurSize_7);
		float L_30 = V_1;
		int32_t L_31 = V_6;
		float L_32 = (__this->___threshold_5);
		float L_33 = (__this->___intensity_6);
		Vector4_t236  L_34 = {0};
		Vector4__ctor_m751(&L_34, ((float)((float)((float)((float)L_29*(float)L_30))+(float)((float)((float)(((float)L_31))*(float)(1.0f))))), (0.0f), L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_28);
		Material_SetVector_m752(L_28, (String_t*) &_stringLiteral41, L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_2;
		int32_t L_36 = V_3;
		RenderTexture_t101 * L_37 = ___source;
		NullCheck(L_37);
		int32_t L_38 = RenderTexture_get_format_m747(L_37, /*hidden argument*/NULL);
		RenderTexture_t101 * L_39 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_35, L_36, 0, L_38, /*hidden argument*/NULL);
		V_7 = L_39;
		RenderTexture_t101 * L_40 = V_7;
		NullCheck(L_40);
		Texture_set_filterMode_m762(L_40, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_41 = V_4;
		RenderTexture_t101 * L_42 = V_7;
		Material_t55 * L_43 = (__this->___fastBloomMaterial_12);
		int32_t L_44 = V_5;
		Graphics_Blit_m742(NULL /*static, unused*/, L_41, L_42, L_43, ((int32_t)((int32_t)2+(int32_t)L_44)), /*hidden argument*/NULL);
		RenderTexture_t101 * L_45 = V_4;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		RenderTexture_t101 * L_46 = V_7;
		V_4 = L_46;
		int32_t L_47 = V_2;
		int32_t L_48 = V_3;
		RenderTexture_t101 * L_49 = ___source;
		NullCheck(L_49);
		int32_t L_50 = RenderTexture_get_format_m747(L_49, /*hidden argument*/NULL);
		RenderTexture_t101 * L_51 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_47, L_48, 0, L_50, /*hidden argument*/NULL);
		V_7 = L_51;
		RenderTexture_t101 * L_52 = V_7;
		NullCheck(L_52);
		Texture_set_filterMode_m762(L_52, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_53 = V_4;
		RenderTexture_t101 * L_54 = V_7;
		Material_t55 * L_55 = (__this->___fastBloomMaterial_12);
		int32_t L_56 = V_5;
		Graphics_Blit_m742(NULL /*static, unused*/, L_53, L_54, L_55, ((int32_t)((int32_t)3+(int32_t)L_56)), /*hidden argument*/NULL);
		RenderTexture_t101 * L_57 = V_4;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		RenderTexture_t101 * L_58 = V_7;
		V_4 = L_58;
		int32_t L_59 = V_6;
		V_6 = ((int32_t)((int32_t)L_59+(int32_t)1));
	}

IL_0175:
	{
		int32_t L_60 = V_6;
		int32_t L_61 = (__this->___blurIterations_9);
		if ((((int32_t)L_60) < ((int32_t)L_61)))
		{
			goto IL_00cb;
		}
	}
	{
		Material_t55 * L_62 = (__this->___fastBloomMaterial_12);
		RenderTexture_t101 * L_63 = V_4;
		NullCheck(L_62);
		Material_SetTexture_m759(L_62, (String_t*) &_stringLiteral42, L_63, /*hidden argument*/NULL);
		RenderTexture_t101 * L_64 = ___source;
		RenderTexture_t101 * L_65 = ___destination;
		Material_t55 * L_66 = (__this->___fastBloomMaterial_12);
		Graphics_Blit_m742(NULL /*static, unused*/, L_64, L_65, L_66, 0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_67 = V_4;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.Blur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_16.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Blur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_16MethodDeclarations.h"

// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.Blur::.ctor()
extern "C" void Blur__ctor_m206 (Blur_t74 * __this, const MethodInfo* method)
{
	{
		__this->___iterations_2 = 3;
		__this->___blurSpread_3 = (0.6f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Blur::.cctor()
extern "C" void Blur__cctor_m207 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.Blur::get_material()
extern TypeInfo* Blur_t74_il2cpp_TypeInfo_var;
extern TypeInfo* Material_t55_il2cpp_TypeInfo_var;
extern "C" Material_t55 * Blur_get_material_m208 (Blur_t74 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Blur_t74_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		Material_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Blur_t74_il2cpp_TypeInfo_var);
		Material_t55 * L_0 = ((Blur_t74_StaticFields*)Blur_t74_il2cpp_TypeInfo_var->static_fields)->___m_Material_5;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Shader_t54 * L_2 = (__this->___blurShader_4);
		Material_t55 * L_3 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m763(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Blur_t74_il2cpp_TypeInfo_var);
		((Blur_t74_StaticFields*)Blur_t74_il2cpp_TypeInfo_var->static_fields)->___m_Material_5 = L_3;
		Material_t55 * L_4 = ((Blur_t74_StaticFields*)Blur_t74_il2cpp_TypeInfo_var->static_fields)->___m_Material_5;
		NullCheck(L_4);
		Object_set_hideFlags_m764(L_4, ((int32_t)52), /*hidden argument*/NULL);
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Blur_t74_il2cpp_TypeInfo_var);
		Material_t55 * L_5 = ((Blur_t74_StaticFields*)Blur_t74_il2cpp_TypeInfo_var->static_fields)->___m_Material_5;
		return L_5;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Blur::OnDisable()
extern TypeInfo* Blur_t74_il2cpp_TypeInfo_var;
extern "C" void Blur_OnDisable_m209 (Blur_t74 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Blur_t74_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Blur_t74_il2cpp_TypeInfo_var);
		Material_t55 * L_0 = ((Blur_t74_StaticFields*)Blur_t74_il2cpp_TypeInfo_var->static_fields)->___m_Material_5;
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Blur_t74_il2cpp_TypeInfo_var);
		Material_t55 * L_2 = ((Blur_t74_StaticFields*)Blur_t74_il2cpp_TypeInfo_var->static_fields)->___m_Material_5;
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Blur::Start()
extern "C" void Blur_Start_m210 (Blur_t74 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m765(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		Shader_t54 * L_1 = (__this->___blurShader_4);
		bool L_2 = Object_op_Implicit_m629(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		Material_t55 * L_3 = Blur_get_material_m208(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Shader_t54 * L_4 = Material_get_shader_m767(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = Shader_get_isSupported_m736(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003f;
		}
	}

IL_0037:
	{
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_003f:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Blur::FourTapCone(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32)
extern TypeInfo* Vector2U5BU5D_t237_il2cpp_TypeInfo_var;
extern "C" void Blur_FourTapCone_m211 (Blur_t74 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___dest, int32_t ___iteration, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2U5BU5D_t237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___iteration;
		float L_1 = (__this->___blurSpread_3);
		V_0 = ((float)((float)(0.5f)+(float)((float)((float)(((float)L_0))*(float)L_1))));
		RenderTexture_t101 * L_2 = ___source;
		RenderTexture_t101 * L_3 = ___dest;
		Material_t55 * L_4 = Blur_get_material_m208(__this, /*hidden argument*/NULL);
		Vector2U5BU5D_t237* L_5 = ((Vector2U5BU5D_t237*)SZArrayNew(Vector2U5BU5D_t237_il2cpp_TypeInfo_var, 4));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		float L_6 = V_0;
		float L_7 = V_0;
		Vector2_t6  L_8 = {0};
		Vector2__ctor_m630(&L_8, ((-L_6)), ((-L_7)), /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_5, 0)) = L_8;
		Vector2U5BU5D_t237* L_9 = L_5;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		float L_10 = V_0;
		float L_11 = V_0;
		Vector2_t6  L_12 = {0};
		Vector2__ctor_m630(&L_12, ((-L_10)), L_11, /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_9, 1)) = L_12;
		Vector2U5BU5D_t237* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		float L_14 = V_0;
		float L_15 = V_0;
		Vector2_t6  L_16 = {0};
		Vector2__ctor_m630(&L_16, L_14, L_15, /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_13, 2)) = L_16;
		Vector2U5BU5D_t237* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		float L_18 = V_0;
		float L_19 = V_0;
		Vector2_t6  L_20 = {0};
		Vector2__ctor_m630(&L_20, L_18, ((-L_19)), /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_17, 3)) = L_20;
		Graphics_BlitMultiTap_m768(NULL /*static, unused*/, L_2, L_3, L_4, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Blur::DownSample4x(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* Vector2U5BU5D_t237_il2cpp_TypeInfo_var;
extern "C" void Blur_DownSample4x_m212 (Blur_t74 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___dest, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2U5BU5D_t237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		RenderTexture_t101 * L_0 = ___source;
		RenderTexture_t101 * L_1 = ___dest;
		Material_t55 * L_2 = Blur_get_material_m208(__this, /*hidden argument*/NULL);
		Vector2U5BU5D_t237* L_3 = ((Vector2U5BU5D_t237*)SZArrayNew(Vector2U5BU5D_t237_il2cpp_TypeInfo_var, 4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		float L_4 = V_0;
		float L_5 = V_0;
		Vector2_t6  L_6 = {0};
		Vector2__ctor_m630(&L_6, ((-L_4)), ((-L_5)), /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_3, 0)) = L_6;
		Vector2U5BU5D_t237* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		float L_8 = V_0;
		float L_9 = V_0;
		Vector2_t6  L_10 = {0};
		Vector2__ctor_m630(&L_10, ((-L_8)), L_9, /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_7, 1)) = L_10;
		Vector2U5BU5D_t237* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		float L_12 = V_0;
		float L_13 = V_0;
		Vector2_t6  L_14 = {0};
		Vector2__ctor_m630(&L_14, L_12, L_13, /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_11, 2)) = L_14;
		Vector2U5BU5D_t237* L_15 = L_11;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 3);
		float L_16 = V_0;
		float L_17 = V_0;
		Vector2_t6  L_18 = {0};
		Vector2__ctor_m630(&L_18, L_16, ((-L_17)), /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_15, 3)) = L_18;
		Graphics_BlitMultiTap_m768(NULL /*static, unused*/, L_0, L_1, L_2, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Blur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Blur_OnRenderImage_m213 (Blur_t74 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RenderTexture_t101 * V_2 = {0};
	int32_t V_3 = 0;
	RenderTexture_t101 * V_4 = {0};
	{
		RenderTexture_t101 * L_0 = ___source;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_0);
		V_0 = ((int32_t)((int32_t)L_1/(int32_t)4));
		RenderTexture_t101 * L_2 = ___source;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_2);
		V_1 = ((int32_t)((int32_t)L_3/(int32_t)4));
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		RenderTexture_t101 * L_6 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, L_4, L_5, 0, /*hidden argument*/NULL);
		V_2 = L_6;
		RenderTexture_t101 * L_7 = ___source;
		RenderTexture_t101 * L_8 = V_2;
		Blur_DownSample4x_m212(__this, L_7, L_8, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_004b;
	}

IL_002a:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = V_1;
		RenderTexture_t101 * L_11 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, L_9, L_10, 0, /*hidden argument*/NULL);
		V_4 = L_11;
		RenderTexture_t101 * L_12 = V_2;
		RenderTexture_t101 * L_13 = V_4;
		int32_t L_14 = V_3;
		Blur_FourTapCone_m211(__this, L_12, L_13, L_14, /*hidden argument*/NULL);
		RenderTexture_t101 * L_15 = V_2;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		RenderTexture_t101 * L_16 = V_4;
		V_2 = L_16;
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_004b:
	{
		int32_t L_18 = V_3;
		int32_t L_19 = (__this->___iterations_2);
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_002a;
		}
	}
	{
		RenderTexture_t101 * L_20 = V_2;
		RenderTexture_t101 * L_21 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		RenderTexture_t101 * L_22 = V_2;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.BlurOptimized/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_17.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.BlurOptimized/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_17MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.BlurOptimized
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_18.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.BlurOptimized
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_18MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::.ctor()
extern "C" void BlurOptimized__ctor_m214 (BlurOptimized_t76 * __this, const MethodInfo* method)
{
	{
		__this->___downsample_5 = 1;
		__this->___blurSize_6 = (3.0f);
		__this->___blurIterations_7 = 2;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.BlurOptimized::CheckResources()
extern "C" bool BlurOptimized_CheckResources_m215 (BlurOptimized_t76 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___blurShader_9);
		Material_t55 * L_1 = (__this->___blurMaterial_10);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___blurMaterial_10 = L_2;
		bool L_3 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		bool L_4 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_4;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnDisable()
extern "C" void BlurOptimized_OnDisable_m216 (BlurOptimized_t76 * __this, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___blurMaterial_10);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t55 * L_2 = (__this->___blurMaterial_10);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void BlurOptimized_OnRenderImage_m217 (BlurOptimized_t76 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RenderTexture_t101 * V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	RenderTexture_t101 * V_7 = {0};
	int32_t G_B5_0 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.BlurOptimized::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		int32_t L_3 = (__this->___downsample_5);
		V_0 = ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_3&(int32_t)((int32_t)31)))))))))));
		Material_t55 * L_4 = (__this->___blurMaterial_10);
		float L_5 = (__this->___blurSize_6);
		float L_6 = V_0;
		float L_7 = (__this->___blurSize_6);
		float L_8 = V_0;
		Vector4_t236  L_9 = {0};
		Vector4__ctor_m751(&L_9, ((float)((float)L_5*(float)L_6)), ((float)((float)((-L_7))*(float)L_8)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		Material_SetVector_m752(L_4, (String_t*) &_stringLiteral41, L_9, /*hidden argument*/NULL);
		RenderTexture_t101 * L_10 = ___source;
		NullCheck(L_10);
		Texture_set_filterMode_m762(L_10, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_11 = ___source;
		NullCheck(L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_11);
		int32_t L_13 = (__this->___downsample_5);
		V_1 = ((int32_t)((int32_t)L_12>>(int32_t)((int32_t)((int32_t)L_13&(int32_t)((int32_t)31)))));
		RenderTexture_t101 * L_14 = ___source;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_14);
		int32_t L_16 = (__this->___downsample_5);
		V_2 = ((int32_t)((int32_t)L_15>>(int32_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)31)))));
		int32_t L_17 = V_1;
		int32_t L_18 = V_2;
		RenderTexture_t101 * L_19 = ___source;
		NullCheck(L_19);
		int32_t L_20 = RenderTexture_get_format_m747(L_19, /*hidden argument*/NULL);
		RenderTexture_t101 * L_21 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_17, L_18, 0, L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		RenderTexture_t101 * L_22 = V_3;
		NullCheck(L_22);
		Texture_set_filterMode_m762(L_22, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_23 = ___source;
		RenderTexture_t101 * L_24 = V_3;
		Material_t55 * L_25 = (__this->___blurMaterial_10);
		Graphics_Blit_m742(NULL /*static, unused*/, L_23, L_24, L_25, 0, /*hidden argument*/NULL);
		int32_t L_26 = (__this->___blurType_8);
		if (L_26)
		{
			goto IL_00ba;
		}
	}
	{
		G_B5_0 = 0;
		goto IL_00bb;
	}

IL_00ba:
	{
		G_B5_0 = 2;
	}

IL_00bb:
	{
		V_4 = G_B5_0;
		V_5 = 0;
		goto IL_0172;
	}

IL_00c5:
	{
		int32_t L_27 = V_5;
		V_6 = ((float)((float)(((float)L_27))*(float)(1.0f)));
		Material_t55 * L_28 = (__this->___blurMaterial_10);
		float L_29 = (__this->___blurSize_6);
		float L_30 = V_0;
		float L_31 = V_6;
		float L_32 = (__this->___blurSize_6);
		float L_33 = V_0;
		float L_34 = V_6;
		Vector4_t236  L_35 = {0};
		Vector4__ctor_m751(&L_35, ((float)((float)((float)((float)L_29*(float)L_30))+(float)L_31)), ((float)((float)((float)((float)((-L_32))*(float)L_33))-(float)L_34)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_28);
		Material_SetVector_m752(L_28, (String_t*) &_stringLiteral41, L_35, /*hidden argument*/NULL);
		int32_t L_36 = V_1;
		int32_t L_37 = V_2;
		RenderTexture_t101 * L_38 = ___source;
		NullCheck(L_38);
		int32_t L_39 = RenderTexture_get_format_m747(L_38, /*hidden argument*/NULL);
		RenderTexture_t101 * L_40 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_36, L_37, 0, L_39, /*hidden argument*/NULL);
		V_7 = L_40;
		RenderTexture_t101 * L_41 = V_7;
		NullCheck(L_41);
		Texture_set_filterMode_m762(L_41, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_42 = V_3;
		RenderTexture_t101 * L_43 = V_7;
		Material_t55 * L_44 = (__this->___blurMaterial_10);
		int32_t L_45 = V_4;
		Graphics_Blit_m742(NULL /*static, unused*/, L_42, L_43, L_44, ((int32_t)((int32_t)1+(int32_t)L_45)), /*hidden argument*/NULL);
		RenderTexture_t101 * L_46 = V_3;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		RenderTexture_t101 * L_47 = V_7;
		V_3 = L_47;
		int32_t L_48 = V_1;
		int32_t L_49 = V_2;
		RenderTexture_t101 * L_50 = ___source;
		NullCheck(L_50);
		int32_t L_51 = RenderTexture_get_format_m747(L_50, /*hidden argument*/NULL);
		RenderTexture_t101 * L_52 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_48, L_49, 0, L_51, /*hidden argument*/NULL);
		V_7 = L_52;
		RenderTexture_t101 * L_53 = V_7;
		NullCheck(L_53);
		Texture_set_filterMode_m762(L_53, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_54 = V_3;
		RenderTexture_t101 * L_55 = V_7;
		Material_t55 * L_56 = (__this->___blurMaterial_10);
		int32_t L_57 = V_4;
		Graphics_Blit_m742(NULL /*static, unused*/, L_54, L_55, L_56, ((int32_t)((int32_t)2+(int32_t)L_57)), /*hidden argument*/NULL);
		RenderTexture_t101 * L_58 = V_3;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		RenderTexture_t101 * L_59 = V_7;
		V_3 = L_59;
		int32_t L_60 = V_5;
		V_5 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_0172:
	{
		int32_t L_61 = V_5;
		int32_t L_62 = (__this->___blurIterations_7);
		if ((((int32_t)L_61) < ((int32_t)L_62)))
		{
			goto IL_00c5;
		}
	}
	{
		RenderTexture_t101 * L_63 = V_3;
		RenderTexture_t101 * L_64 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
		RenderTexture_t101 * L_65 = V_3;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.CameraMotionBlur/MotionBlurFilter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_19.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.CameraMotionBlur/MotionBlurFilter
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_19MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.CameraMotionBlur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_20.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.CameraMotionBlur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_20MethodDeclarations.h"

// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.DepthTextureMode
#include "UnityEngine_UnityEngine_DepthTextureMode.h"
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapMode.h"
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
struct GameObject_t78;
struct Camera_t27;
struct GameObject_t78;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::GetComponent<System.Object>()
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m771_gshared (GameObject_t78 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m771(__this, method) (( Object_t * (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m771_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t27_m770(__this, method) (( Camera_t27 * (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m771_gshared)(__this, method)


// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::.ctor()
extern "C" void CameraMotionBlur__ctor_m218 (CameraMotionBlur_t79 * __this, const MethodInfo* method)
{
	{
		__this->___filterType_6 = 2;
		Vector3_t4  L_0 = Vector3_get_one_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___previewScale_8 = L_0;
		__this->___rotationScale_10 = (1.0f);
		__this->___maxVelocity_11 = (8.0f);
		__this->___minVelocity_12 = (0.1f);
		__this->___velocityScale_13 = (0.375f);
		__this->___softZDistance_14 = (0.005f);
		__this->___velocityDownsample_15 = 1;
		LayerMask_t11  L_1 = LayerMask_op_Implicit_m773(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->___excludeLayers_16 = L_1;
		__this->___jitter_24 = (0.05f);
		__this->___showVelocityScale_26 = (1.0f);
		Vector3_t4  L_2 = Vector3_get_forward_m605(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___prevFrameForward_31 = L_2;
		Vector3_t4  L_3 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___prevFrameUp_32 = L_3;
		Vector3_t4  L_4 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___prevFramePos_33 = L_4;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::.cctor()
extern TypeInfo* CameraMotionBlur_t79_il2cpp_TypeInfo_var;
extern "C" void CameraMotionBlur__cctor_m219 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraMotionBlur_t79_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	{
		((CameraMotionBlur_t79_StaticFields*)CameraMotionBlur_t79_il2cpp_TypeInfo_var->static_fields)->___MAX_RADIUS_5 = (10.0f);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::CalculateViewProjection()
extern "C" void CameraMotionBlur_CalculateViewProjection_m220 (CameraMotionBlur_t79 * __this, const MethodInfo* method)
{
	Matrix4x4_t80  V_0 = {0};
	Matrix4x4_t80  V_1 = {0};
	{
		Camera_t27 * L_0 = (__this->____camera_34);
		NullCheck(L_0);
		Matrix4x4_t80  L_1 = Camera_get_worldToCameraMatrix_m774(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Camera_t27 * L_2 = (__this->____camera_34);
		NullCheck(L_2);
		Matrix4x4_t80  L_3 = Camera_get_projectionMatrix_m775(L_2, /*hidden argument*/NULL);
		Matrix4x4_t80  L_4 = GL_GetGPUProjectionMatrix_m776(NULL /*static, unused*/, L_3, 1, /*hidden argument*/NULL);
		V_1 = L_4;
		Matrix4x4_t80  L_5 = V_1;
		Matrix4x4_t80  L_6 = V_0;
		Matrix4x4_t80  L_7 = Matrix4x4_op_Multiply_m777(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		__this->___currentViewProjMat_27 = L_7;
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::Start()
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void CameraMotionBlur_Start_m221 (CameraMotionBlur_t79 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	{
		VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::CheckResources() */, __this);
		Camera_t27 * L_0 = (__this->____camera_34);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		Camera_t27 * L_2 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		__this->____camera_34 = L_2;
	}

IL_0024:
	{
		GameObject_t78 * L_3 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeInHierarchy_m778(L_3, /*hidden argument*/NULL);
		__this->___wasActive_30 = L_4;
		CameraMotionBlur_CalculateViewProjection_m220(__this, /*hidden argument*/NULL);
		CameraMotionBlur_Remember_m226(__this, /*hidden argument*/NULL);
		__this->___wasActive_30 = 0;
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnEnable()
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void CameraMotionBlur_OnEnable_m222 (CameraMotionBlur_t79 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t27 * L_0 = (__this->____camera_34);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Camera_t27 * L_2 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		__this->____camera_34 = L_2;
	}

IL_001d:
	{
		Camera_t27 * L_3 = (__this->____camera_34);
		Camera_t27 * L_4 = L_3;
		NullCheck(L_4);
		int32_t L_5 = Camera_get_depthTextureMode_m779(L_4, /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_set_depthTextureMode_m780(L_4, ((int32_t)((int32_t)L_5|(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnDisable()
extern "C" void CameraMotionBlur_OnDisable_m223 (CameraMotionBlur_t79 * __this, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___motionBlurMaterial_21);
		bool L_1 = Object_op_Inequality_m623(NULL /*static, unused*/, (Object_t164 *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		Material_t55 * L_2 = (__this->___motionBlurMaterial_21);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->___motionBlurMaterial_21 = (Material_t55 *)NULL;
	}

IL_0023:
	{
		Material_t55 * L_3 = (__this->___dx11MotionBlurMaterial_22);
		bool L_4 = Object_op_Inequality_m623(NULL /*static, unused*/, (Object_t164 *)NULL, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0046;
		}
	}
	{
		Material_t55 * L_5 = (__this->___dx11MotionBlurMaterial_22);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->___dx11MotionBlurMaterial_22 = (Material_t55 *)NULL;
	}

IL_0046:
	{
		GameObject_t78 * L_6 = (__this->___tmpCam_17);
		bool L_7 = Object_op_Inequality_m623(NULL /*static, unused*/, (Object_t164 *)NULL, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0069;
		}
	}
	{
		GameObject_t78 * L_8 = (__this->___tmpCam_17);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->___tmpCam_17 = (GameObject_t78 *)NULL;
	}

IL_0069:
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::CheckResources()
extern "C" bool CameraMotionBlur_CheckResources_m224 (CameraMotionBlur_t79 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m335(__this, 1, 1, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___shader_18);
		Material_t55 * L_1 = (__this->___motionBlurMaterial_21);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___motionBlurMaterial_21 = L_2;
		bool L_3 = (((PostEffectsBase_t57 *)__this)->___supportDX11_3);
		if (!L_3)
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_4 = (__this->___filterType_6);
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_0050;
		}
	}
	{
		Shader_t54 * L_5 = (__this->___dx11MotionBlurShader_19);
		Material_t55 * L_6 = (__this->___dx11MotionBlurMaterial_22);
		Material_t55 * L_7 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_5, L_6, /*hidden argument*/NULL);
		__this->___dx11MotionBlurMaterial_22 = L_7;
	}

IL_0050:
	{
		bool L_8 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_8)
		{
			goto IL_0061;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_9 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_9;
	}
}
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* CameraMotionBlur_t79_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void CameraMotionBlur_OnRenderImage_m225 (CameraMotionBlur_t79 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraMotionBlur_t79_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	RenderTexture_t101 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	bool V_5 = false;
	RenderTexture_t101 * V_6 = {0};
	RenderTexture_t101 * V_7 = {0};
	Matrix4x4_t80  V_8 = {0};
	Matrix4x4_t80  V_9 = {0};
	Matrix4x4_t80  V_10 = {0};
	Matrix4x4_t80  V_11 = {0};
	Vector4_t236  V_12 = {0};
	float V_13 = 0.0f;
	Vector3_t4  V_14 = {0};
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	Camera_t27 * V_17 = {0};
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		int32_t L_3 = (__this->___filterType_6);
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		CameraMotionBlur_StartFrame_m228(__this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		bool L_4 = SystemInfo_SupportsRenderTextureFormat_m781(NULL /*static, unused*/, ((int32_t)13), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		G_B7_0 = ((int32_t)13);
		goto IL_0038;
	}

IL_0037:
	{
		G_B7_0 = 2;
	}

IL_0038:
	{
		V_0 = G_B7_0;
		RenderTexture_t101 * L_5 = ___source;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_5);
		int32_t L_7 = (__this->___velocityDownsample_15);
		IL2CPP_RUNTIME_CLASS_INIT(CameraMotionBlur_t79_il2cpp_TypeInfo_var);
		int32_t L_8 = CameraMotionBlur_divRoundUp_m229(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		RenderTexture_t101 * L_9 = ___source;
		NullCheck(L_9);
		int32_t L_10 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_9);
		int32_t L_11 = (__this->___velocityDownsample_15);
		int32_t L_12 = CameraMotionBlur_divRoundUp_m229(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		RenderTexture_t101 * L_14 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_8, L_12, 0, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		V_2 = 1;
		V_3 = 1;
		float L_15 = (__this->___maxVelocity_11);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Max_m782(NULL /*static, unused*/, (2.0f), L_15, /*hidden argument*/NULL);
		__this->___maxVelocity_11 = L_16;
		float L_17 = (__this->___maxVelocity_11);
		V_4 = L_17;
		int32_t L_18 = (__this->___filterType_6);
		if ((!(((uint32_t)L_18) == ((uint32_t)3))))
		{
			goto IL_009f;
		}
	}
	{
		Material_t55 * L_19 = (__this->___dx11MotionBlurMaterial_22);
		bool L_20 = Object_op_Equality_m640(NULL /*static, unused*/, L_19, (Object_t164 *)NULL, /*hidden argument*/NULL);
		G_B10_0 = ((int32_t)(L_20));
		goto IL_00a0;
	}

IL_009f:
	{
		G_B10_0 = 0;
	}

IL_00a0:
	{
		V_5 = G_B10_0;
		int32_t L_21 = (__this->___filterType_6);
		if ((((int32_t)L_21) == ((int32_t)2)))
		{
			goto IL_00c1;
		}
	}
	{
		bool L_22 = V_5;
		if (L_22)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_23 = (__this->___filterType_6);
		if ((!(((uint32_t)L_23) == ((uint32_t)4))))
		{
			goto IL_010d;
		}
	}

IL_00c1:
	{
		float L_24 = (__this->___maxVelocity_11);
		IL2CPP_RUNTIME_CLASS_INIT(CameraMotionBlur_t79_il2cpp_TypeInfo_var);
		float L_25 = ((CameraMotionBlur_t79_StaticFields*)CameraMotionBlur_t79_il2cpp_TypeInfo_var->static_fields)->___MAX_RADIUS_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_26 = Mathf_Min_m783(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		__this->___maxVelocity_11 = L_26;
		RenderTexture_t101 * L_27 = V_1;
		NullCheck(L_27);
		int32_t L_28 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_27);
		float L_29 = (__this->___maxVelocity_11);
		int32_t L_30 = CameraMotionBlur_divRoundUp_m229(NULL /*static, unused*/, L_28, (((int32_t)L_29)), /*hidden argument*/NULL);
		V_2 = L_30;
		RenderTexture_t101 * L_31 = V_1;
		NullCheck(L_31);
		int32_t L_32 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_31);
		float L_33 = (__this->___maxVelocity_11);
		int32_t L_34 = CameraMotionBlur_divRoundUp_m229(NULL /*static, unused*/, L_32, (((int32_t)L_33)), /*hidden argument*/NULL);
		V_3 = L_34;
		RenderTexture_t101 * L_35 = V_1;
		NullCheck(L_35);
		int32_t L_36 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_35);
		int32_t L_37 = V_2;
		V_4 = (((float)((int32_t)((int32_t)L_36/(int32_t)L_37))));
		goto IL_013e;
	}

IL_010d:
	{
		RenderTexture_t101 * L_38 = V_1;
		NullCheck(L_38);
		int32_t L_39 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_38);
		float L_40 = (__this->___maxVelocity_11);
		IL2CPP_RUNTIME_CLASS_INIT(CameraMotionBlur_t79_il2cpp_TypeInfo_var);
		int32_t L_41 = CameraMotionBlur_divRoundUp_m229(NULL /*static, unused*/, L_39, (((int32_t)L_40)), /*hidden argument*/NULL);
		V_2 = L_41;
		RenderTexture_t101 * L_42 = V_1;
		NullCheck(L_42);
		int32_t L_43 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_42);
		float L_44 = (__this->___maxVelocity_11);
		int32_t L_45 = CameraMotionBlur_divRoundUp_m229(NULL /*static, unused*/, L_43, (((int32_t)L_44)), /*hidden argument*/NULL);
		V_3 = L_45;
		RenderTexture_t101 * L_46 = V_1;
		NullCheck(L_46);
		int32_t L_47 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_46);
		int32_t L_48 = V_2;
		V_4 = (((float)((int32_t)((int32_t)L_47/(int32_t)L_48))));
	}

IL_013e:
	{
		int32_t L_49 = V_2;
		int32_t L_50 = V_3;
		int32_t L_51 = V_0;
		RenderTexture_t101 * L_52 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_49, L_50, 0, L_51, /*hidden argument*/NULL);
		V_6 = L_52;
		int32_t L_53 = V_2;
		int32_t L_54 = V_3;
		int32_t L_55 = V_0;
		RenderTexture_t101 * L_56 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_53, L_54, 0, L_55, /*hidden argument*/NULL);
		V_7 = L_56;
		RenderTexture_t101 * L_57 = V_1;
		NullCheck(L_57);
		Texture_set_filterMode_m762(L_57, 0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_58 = V_6;
		NullCheck(L_58);
		Texture_set_filterMode_m762(L_58, 0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_59 = V_7;
		NullCheck(L_59);
		Texture_set_filterMode_m762(L_59, 0, /*hidden argument*/NULL);
		Texture2D_t63 * L_60 = (__this->___noiseTexture_23);
		bool L_61 = Object_op_Implicit_m629(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_0187;
		}
	}
	{
		Texture2D_t63 * L_62 = (__this->___noiseTexture_23);
		NullCheck(L_62);
		Texture_set_filterMode_m762(L_62, 0, /*hidden argument*/NULL);
	}

IL_0187:
	{
		RenderTexture_t101 * L_63 = ___source;
		NullCheck(L_63);
		Texture_set_wrapMode_m784(L_63, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_64 = V_1;
		NullCheck(L_64);
		Texture_set_wrapMode_m784(L_64, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_65 = V_7;
		NullCheck(L_65);
		Texture_set_wrapMode_m784(L_65, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_66 = V_6;
		NullCheck(L_66);
		Texture_set_wrapMode_m784(L_66, 1, /*hidden argument*/NULL);
		CameraMotionBlur_CalculateViewProjection_m220(__this, /*hidden argument*/NULL);
		GameObject_t78 * L_67 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_67);
		bool L_68 = GameObject_get_activeInHierarchy_m778(L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_01cc;
		}
	}
	{
		bool L_69 = (__this->___wasActive_30);
		if (L_69)
		{
			goto IL_01cc;
		}
	}
	{
		CameraMotionBlur_Remember_m226(__this, /*hidden argument*/NULL);
	}

IL_01cc:
	{
		GameObject_t78 * L_70 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_70);
		bool L_71 = GameObject_get_activeInHierarchy_m778(L_70, /*hidden argument*/NULL);
		__this->___wasActive_30 = L_71;
		Matrix4x4_t80  L_72 = (__this->___currentViewProjMat_27);
		Matrix4x4_t80  L_73 = Matrix4x4_Inverse_m785(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
		V_8 = L_73;
		Material_t55 * L_74 = (__this->___motionBlurMaterial_21);
		Matrix4x4_t80  L_75 = V_8;
		NullCheck(L_74);
		Material_SetMatrix_m786(L_74, (String_t*) &_stringLiteral43, L_75, /*hidden argument*/NULL);
		Material_t55 * L_76 = (__this->___motionBlurMaterial_21);
		Matrix4x4_t80  L_77 = (__this->___prevViewProjMat_28);
		NullCheck(L_76);
		Material_SetMatrix_m786(L_76, (String_t*) &_stringLiteral44, L_77, /*hidden argument*/NULL);
		Material_t55 * L_78 = (__this->___motionBlurMaterial_21);
		Matrix4x4_t80  L_79 = (__this->___prevViewProjMat_28);
		Matrix4x4_t80  L_80 = V_8;
		Matrix4x4_t80  L_81 = Matrix4x4_op_Multiply_m777(NULL /*static, unused*/, L_79, L_80, /*hidden argument*/NULL);
		NullCheck(L_78);
		Material_SetMatrix_m786(L_78, (String_t*) &_stringLiteral45, L_81, /*hidden argument*/NULL);
		Material_t55 * L_82 = (__this->___motionBlurMaterial_21);
		float L_83 = V_4;
		NullCheck(L_82);
		Material_SetFloat_m738(L_82, (String_t*) &_stringLiteral46, L_83, /*hidden argument*/NULL);
		Material_t55 * L_84 = (__this->___motionBlurMaterial_21);
		float L_85 = V_4;
		NullCheck(L_84);
		Material_SetFloat_m738(L_84, (String_t*) &_stringLiteral47, L_85, /*hidden argument*/NULL);
		Material_t55 * L_86 = (__this->___motionBlurMaterial_21);
		float L_87 = (__this->___minVelocity_12);
		NullCheck(L_86);
		Material_SetFloat_m738(L_86, (String_t*) &_stringLiteral48, L_87, /*hidden argument*/NULL);
		Material_t55 * L_88 = (__this->___motionBlurMaterial_21);
		float L_89 = (__this->___velocityScale_13);
		NullCheck(L_88);
		Material_SetFloat_m738(L_88, (String_t*) &_stringLiteral49, L_89, /*hidden argument*/NULL);
		Material_t55 * L_90 = (__this->___motionBlurMaterial_21);
		float L_91 = (__this->___jitter_24);
		NullCheck(L_90);
		Material_SetFloat_m738(L_90, (String_t*) &_stringLiteral50, L_91, /*hidden argument*/NULL);
		Material_t55 * L_92 = (__this->___motionBlurMaterial_21);
		Texture2D_t63 * L_93 = (__this->___noiseTexture_23);
		NullCheck(L_92);
		Material_SetTexture_m759(L_92, (String_t*) &_stringLiteral51, L_93, /*hidden argument*/NULL);
		Material_t55 * L_94 = (__this->___motionBlurMaterial_21);
		RenderTexture_t101 * L_95 = V_1;
		NullCheck(L_94);
		Material_SetTexture_m759(L_94, (String_t*) &_stringLiteral52, L_95, /*hidden argument*/NULL);
		Material_t55 * L_96 = (__this->___motionBlurMaterial_21);
		RenderTexture_t101 * L_97 = V_7;
		NullCheck(L_96);
		Material_SetTexture_m759(L_96, (String_t*) &_stringLiteral53, L_97, /*hidden argument*/NULL);
		Material_t55 * L_98 = (__this->___motionBlurMaterial_21);
		RenderTexture_t101 * L_99 = V_6;
		NullCheck(L_98);
		Material_SetTexture_m759(L_98, (String_t*) &_stringLiteral54, L_99, /*hidden argument*/NULL);
		bool L_100 = (__this->___preview_7);
		if (!L_100)
		{
			goto IL_037c;
		}
	}
	{
		Camera_t27 * L_101 = (__this->____camera_34);
		NullCheck(L_101);
		Matrix4x4_t80  L_102 = Camera_get_worldToCameraMatrix_m774(L_101, /*hidden argument*/NULL);
		V_9 = L_102;
		Matrix4x4_t80  L_103 = Matrix4x4_get_identity_m787(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_10 = L_103;
		Vector3_t4  L_104 = (__this->___previewScale_8);
		Vector3_t4  L_105 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_104, (0.3333f), /*hidden argument*/NULL);
		Quaternion_t19  L_106 = Quaternion_get_identity_m788(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_107 = Vector3_get_one_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_SetTRS_m789((&V_10), L_105, L_106, L_107, /*hidden argument*/NULL);
		Camera_t27 * L_108 = (__this->____camera_34);
		NullCheck(L_108);
		Matrix4x4_t80  L_109 = Camera_get_projectionMatrix_m775(L_108, /*hidden argument*/NULL);
		Matrix4x4_t80  L_110 = GL_GetGPUProjectionMatrix_m776(NULL /*static, unused*/, L_109, 1, /*hidden argument*/NULL);
		V_11 = L_110;
		Matrix4x4_t80  L_111 = V_11;
		Matrix4x4_t80  L_112 = V_10;
		Matrix4x4_t80  L_113 = Matrix4x4_op_Multiply_m777(NULL /*static, unused*/, L_111, L_112, /*hidden argument*/NULL);
		Matrix4x4_t80  L_114 = V_9;
		Matrix4x4_t80  L_115 = Matrix4x4_op_Multiply_m777(NULL /*static, unused*/, L_113, L_114, /*hidden argument*/NULL);
		__this->___prevViewProjMat_28 = L_115;
		Material_t55 * L_116 = (__this->___motionBlurMaterial_21);
		Matrix4x4_t80  L_117 = (__this->___prevViewProjMat_28);
		NullCheck(L_116);
		Material_SetMatrix_m786(L_116, (String_t*) &_stringLiteral44, L_117, /*hidden argument*/NULL);
		Material_t55 * L_118 = (__this->___motionBlurMaterial_21);
		Matrix4x4_t80  L_119 = (__this->___prevViewProjMat_28);
		Matrix4x4_t80  L_120 = V_8;
		Matrix4x4_t80  L_121 = Matrix4x4_op_Multiply_m777(NULL /*static, unused*/, L_119, L_120, /*hidden argument*/NULL);
		NullCheck(L_118);
		Material_SetMatrix_m786(L_118, (String_t*) &_stringLiteral45, L_121, /*hidden argument*/NULL);
	}

IL_037c:
	{
		int32_t L_122 = (__this->___filterType_6);
		if (L_122)
		{
			goto IL_05cf;
		}
	}
	{
		Vector4_t236  L_123 = Vector4_get_zero_m790(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_12 = L_123;
		Transform_t1 * L_124 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_124);
		Vector3_t4  L_125 = Transform_get_up_m644(L_124, /*hidden argument*/NULL);
		Vector3_t4  L_126 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_127 = Vector3_Dot_m791(NULL /*static, unused*/, L_125, L_126, /*hidden argument*/NULL);
		V_13 = L_127;
		Vector3_t4  L_128 = (__this->___prevFramePos_33);
		Transform_t1 * L_129 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_129);
		Vector3_t4  L_130 = Transform_get_position_m593(L_129, /*hidden argument*/NULL);
		Vector3_t4  L_131 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_128, L_130, /*hidden argument*/NULL);
		V_14 = L_131;
		float L_132 = Vector3_get_magnitude_m647((&V_14), /*hidden argument*/NULL);
		V_15 = L_132;
		V_16 = (1.0f);
		Transform_t1 * L_133 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_133);
		Vector3_t4  L_134 = Transform_get_up_m644(L_133, /*hidden argument*/NULL);
		Vector3_t4  L_135 = (__this->___prevFrameUp_32);
		float L_136 = Vector3_Angle_m792(NULL /*static, unused*/, L_134, L_135, /*hidden argument*/NULL);
		Camera_t27 * L_137 = (__this->____camera_34);
		NullCheck(L_137);
		float L_138 = Camera_get_fieldOfView_m699(L_137, /*hidden argument*/NULL);
		RenderTexture_t101 * L_139 = ___source;
		NullCheck(L_139);
		int32_t L_140 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_139);
		V_16 = ((float)((float)((float)((float)L_136/(float)L_138))*(float)((float)((float)(((float)L_140))*(float)(0.75f)))));
		float L_141 = (__this->___rotationScale_10);
		float L_142 = V_16;
		(&V_12)->___x_1 = ((float)((float)L_141*(float)L_142));
		Transform_t1 * L_143 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_143);
		Vector3_t4  L_144 = Transform_get_forward_m643(L_143, /*hidden argument*/NULL);
		Vector3_t4  L_145 = (__this->___prevFrameForward_31);
		float L_146 = Vector3_Angle_m792(NULL /*static, unused*/, L_144, L_145, /*hidden argument*/NULL);
		Camera_t27 * L_147 = (__this->____camera_34);
		NullCheck(L_147);
		float L_148 = Camera_get_fieldOfView_m699(L_147, /*hidden argument*/NULL);
		RenderTexture_t101 * L_149 = ___source;
		NullCheck(L_149);
		int32_t L_150 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_149);
		V_16 = ((float)((float)((float)((float)L_146/(float)L_148))*(float)((float)((float)(((float)L_150))*(float)(0.75f)))));
		float L_151 = (__this->___rotationScale_10);
		float L_152 = V_13;
		float L_153 = V_16;
		(&V_12)->___y_2 = ((float)((float)((float)((float)L_151*(float)L_152))*(float)L_153));
		Transform_t1 * L_154 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_154);
		Vector3_t4  L_155 = Transform_get_forward_m643(L_154, /*hidden argument*/NULL);
		Vector3_t4  L_156 = (__this->___prevFrameForward_31);
		float L_157 = Vector3_Angle_m792(NULL /*static, unused*/, L_155, L_156, /*hidden argument*/NULL);
		Camera_t27 * L_158 = (__this->____camera_34);
		NullCheck(L_158);
		float L_159 = Camera_get_fieldOfView_m699(L_158, /*hidden argument*/NULL);
		RenderTexture_t101 * L_160 = ___source;
		NullCheck(L_160);
		int32_t L_161 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_160);
		V_16 = ((float)((float)((float)((float)L_157/(float)L_159))*(float)((float)((float)(((float)L_161))*(float)(0.75f)))));
		float L_162 = (__this->___rotationScale_10);
		float L_163 = V_13;
		float L_164 = V_16;
		(&V_12)->___z_3 = ((float)((float)((float)((float)L_162*(float)((float)((float)(1.0f)-(float)L_163))))*(float)L_164));
		float L_165 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_166 = ((Mathf_t218_StaticFields*)Mathf_t218_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		if ((!(((float)L_165) > ((float)L_166))))
		{
			goto IL_0553;
		}
	}
	{
		float L_167 = (__this->___movementScale_9);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_168 = ((Mathf_t218_StaticFields*)Mathf_t218_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		if ((!(((float)L_167) > ((float)L_168))))
		{
			goto IL_0553;
		}
	}
	{
		float L_169 = (__this->___movementScale_9);
		Transform_t1 * L_170 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_170);
		Vector3_t4  L_171 = Transform_get_forward_m643(L_170, /*hidden argument*/NULL);
		Vector3_t4  L_172 = V_14;
		float L_173 = Vector3_Dot_m791(NULL /*static, unused*/, L_171, L_172, /*hidden argument*/NULL);
		RenderTexture_t101 * L_174 = ___source;
		NullCheck(L_174);
		int32_t L_175 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_174);
		(&V_12)->___w_4 = ((float)((float)((float)((float)L_169*(float)L_173))*(float)((float)((float)(((float)L_175))*(float)(0.5f)))));
		Vector4_t236 * L_176 = (&V_12);
		float L_177 = (L_176->___x_1);
		float L_178 = (__this->___movementScale_9);
		Transform_t1 * L_179 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_179);
		Vector3_t4  L_180 = Transform_get_up_m644(L_179, /*hidden argument*/NULL);
		Vector3_t4  L_181 = V_14;
		float L_182 = Vector3_Dot_m791(NULL /*static, unused*/, L_180, L_181, /*hidden argument*/NULL);
		RenderTexture_t101 * L_183 = ___source;
		NullCheck(L_183);
		int32_t L_184 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_183);
		L_176->___x_1 = ((float)((float)L_177+(float)((float)((float)((float)((float)L_178*(float)L_182))*(float)((float)((float)(((float)L_184))*(float)(0.5f)))))));
		Vector4_t236 * L_185 = (&V_12);
		float L_186 = (L_185->___y_2);
		float L_187 = (__this->___movementScale_9);
		Transform_t1 * L_188 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_188);
		Vector3_t4  L_189 = Transform_get_right_m793(L_188, /*hidden argument*/NULL);
		Vector3_t4  L_190 = V_14;
		float L_191 = Vector3_Dot_m791(NULL /*static, unused*/, L_189, L_190, /*hidden argument*/NULL);
		RenderTexture_t101 * L_192 = ___source;
		NullCheck(L_192);
		int32_t L_193 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_192);
		L_185->___y_2 = ((float)((float)L_186+(float)((float)((float)((float)((float)L_187*(float)L_191))*(float)((float)((float)(((float)L_193))*(float)(0.5f)))))));
	}

IL_0553:
	{
		bool L_194 = (__this->___preview_7);
		if (!L_194)
		{
			goto IL_05b8;
		}
	}
	{
		Material_t55 * L_195 = (__this->___motionBlurMaterial_21);
		Vector3_t4 * L_196 = &(__this->___previewScale_8);
		float L_197 = (L_196->___y_2);
		Vector3_t4 * L_198 = &(__this->___previewScale_8);
		float L_199 = (L_198->___x_1);
		Vector3_t4 * L_200 = &(__this->___previewScale_8);
		float L_201 = (L_200->___z_3);
		Vector4_t236  L_202 = {0};
		Vector4__ctor_m751(&L_202, L_197, L_199, (0.0f), L_201, /*hidden argument*/NULL);
		Vector4_t236  L_203 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_202, (0.5f), /*hidden argument*/NULL);
		Camera_t27 * L_204 = (__this->____camera_34);
		NullCheck(L_204);
		float L_205 = Camera_get_fieldOfView_m699(L_204, /*hidden argument*/NULL);
		Vector4_t236  L_206 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_203, L_205, /*hidden argument*/NULL);
		NullCheck(L_195);
		Material_SetVector_m752(L_195, (String_t*) &_stringLiteral55, L_206, /*hidden argument*/NULL);
		goto IL_05ca;
	}

IL_05b8:
	{
		Material_t55 * L_207 = (__this->___motionBlurMaterial_21);
		Vector4_t236  L_208 = V_12;
		NullCheck(L_207);
		Material_SetVector_m752(L_207, (String_t*) &_stringLiteral55, L_208, /*hidden argument*/NULL);
	}

IL_05ca:
	{
		goto IL_0660;
	}

IL_05cf:
	{
		RenderTexture_t101 * L_209 = ___source;
		RenderTexture_t101 * L_210 = V_1;
		Material_t55 * L_211 = (__this->___motionBlurMaterial_21);
		Graphics_Blit_m742(NULL /*static, unused*/, L_209, L_210, L_211, 0, /*hidden argument*/NULL);
		V_17 = (Camera_t27 *)NULL;
		LayerMask_t11 * L_212 = &(__this->___excludeLayers_16);
		int32_t L_213 = LayerMask_get_value_m794(L_212, /*hidden argument*/NULL);
		if (!L_213)
		{
			goto IL_05f8;
		}
	}
	{
		Camera_t27 * L_214 = CameraMotionBlur_GetTmpCam_m227(__this, /*hidden argument*/NULL);
		V_17 = L_214;
	}

IL_05f8:
	{
		Camera_t27 * L_215 = V_17;
		bool L_216 = Object_op_Implicit_m629(NULL /*static, unused*/, L_215, /*hidden argument*/NULL);
		if (!L_216)
		{
			goto IL_0660;
		}
	}
	{
		LayerMask_t11 * L_217 = &(__this->___excludeLayers_16);
		int32_t L_218 = LayerMask_get_value_m794(L_217, /*hidden argument*/NULL);
		if (!L_218)
		{
			goto IL_0660;
		}
	}
	{
		Shader_t54 * L_219 = (__this->___replacementClear_20);
		bool L_220 = Object_op_Implicit_m629(NULL /*static, unused*/, L_219, /*hidden argument*/NULL);
		if (!L_220)
		{
			goto IL_0660;
		}
	}
	{
		Shader_t54 * L_221 = (__this->___replacementClear_20);
		NullCheck(L_221);
		bool L_222 = Shader_get_isSupported_m736(L_221, /*hidden argument*/NULL);
		if (!L_222)
		{
			goto IL_0660;
		}
	}
	{
		Camera_t27 * L_223 = V_17;
		RenderTexture_t101 * L_224 = V_1;
		NullCheck(L_223);
		Camera_set_targetTexture_m795(L_223, L_224, /*hidden argument*/NULL);
		Camera_t27 * L_225 = V_17;
		LayerMask_t11  L_226 = (__this->___excludeLayers_16);
		int32_t L_227 = LayerMask_op_Implicit_m620(NULL /*static, unused*/, L_226, /*hidden argument*/NULL);
		NullCheck(L_225);
		Camera_set_cullingMask_m796(L_225, L_227, /*hidden argument*/NULL);
		Camera_t27 * L_228 = V_17;
		Shader_t54 * L_229 = (__this->___replacementClear_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_230 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_228);
		Camera_RenderWithShader_m797(L_228, L_229, L_230, /*hidden argument*/NULL);
	}

IL_0660:
	{
		bool L_231 = (__this->___preview_7);
		if (L_231)
		{
			goto IL_068c;
		}
	}
	{
		int32_t L_232 = Time_get_frameCount_m708(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_233 = (__this->___prevFrameCount_29);
		if ((((int32_t)L_232) == ((int32_t)L_233)))
		{
			goto IL_068c;
		}
	}
	{
		int32_t L_234 = Time_get_frameCount_m708(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___prevFrameCount_29 = L_234;
		CameraMotionBlur_Remember_m226(__this, /*hidden argument*/NULL);
	}

IL_068c:
	{
		RenderTexture_t101 * L_235 = ___source;
		NullCheck(L_235);
		Texture_set_filterMode_m762(L_235, 1, /*hidden argument*/NULL);
		bool L_236 = (__this->___showVelocity_25);
		if (!L_236)
		{
			goto IL_06c7;
		}
	}
	{
		Material_t55 * L_237 = (__this->___motionBlurMaterial_21);
		float L_238 = (__this->___showVelocityScale_26);
		NullCheck(L_237);
		Material_SetFloat_m738(L_237, (String_t*) &_stringLiteral56, L_238, /*hidden argument*/NULL);
		RenderTexture_t101 * L_239 = V_1;
		RenderTexture_t101 * L_240 = ___destination;
		Material_t55 * L_241 = (__this->___motionBlurMaterial_21);
		Graphics_Blit_m742(NULL /*static, unused*/, L_239, L_240, L_241, 1, /*hidden argument*/NULL);
		goto IL_08a8;
	}

IL_06c7:
	{
		int32_t L_242 = (__this->___filterType_6);
		if ((!(((uint32_t)L_242) == ((uint32_t)3))))
		{
			goto IL_07b9;
		}
	}
	{
		bool L_243 = V_5;
		if (L_243)
		{
			goto IL_07b9;
		}
	}
	{
		Material_t55 * L_244 = (__this->___dx11MotionBlurMaterial_22);
		float L_245 = (__this->___minVelocity_12);
		NullCheck(L_244);
		Material_SetFloat_m738(L_244, (String_t*) &_stringLiteral48, L_245, /*hidden argument*/NULL);
		Material_t55 * L_246 = (__this->___dx11MotionBlurMaterial_22);
		float L_247 = (__this->___velocityScale_13);
		NullCheck(L_246);
		Material_SetFloat_m738(L_246, (String_t*) &_stringLiteral49, L_247, /*hidden argument*/NULL);
		Material_t55 * L_248 = (__this->___dx11MotionBlurMaterial_22);
		float L_249 = (__this->___jitter_24);
		NullCheck(L_248);
		Material_SetFloat_m738(L_248, (String_t*) &_stringLiteral50, L_249, /*hidden argument*/NULL);
		Material_t55 * L_250 = (__this->___dx11MotionBlurMaterial_22);
		Texture2D_t63 * L_251 = (__this->___noiseTexture_23);
		NullCheck(L_250);
		Material_SetTexture_m759(L_250, (String_t*) &_stringLiteral51, L_251, /*hidden argument*/NULL);
		Material_t55 * L_252 = (__this->___dx11MotionBlurMaterial_22);
		RenderTexture_t101 * L_253 = V_1;
		NullCheck(L_252);
		Material_SetTexture_m759(L_252, (String_t*) &_stringLiteral52, L_253, /*hidden argument*/NULL);
		Material_t55 * L_254 = (__this->___dx11MotionBlurMaterial_22);
		RenderTexture_t101 * L_255 = V_7;
		NullCheck(L_254);
		Material_SetTexture_m759(L_254, (String_t*) &_stringLiteral53, L_255, /*hidden argument*/NULL);
		Material_t55 * L_256 = (__this->___dx11MotionBlurMaterial_22);
		float L_257 = (__this->___softZDistance_14);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_258 = Mathf_Max_m782(NULL /*static, unused*/, (0.00025f), L_257, /*hidden argument*/NULL);
		NullCheck(L_256);
		Material_SetFloat_m738(L_256, (String_t*) &_stringLiteral57, L_258, /*hidden argument*/NULL);
		Material_t55 * L_259 = (__this->___dx11MotionBlurMaterial_22);
		float L_260 = V_4;
		NullCheck(L_259);
		Material_SetFloat_m738(L_259, (String_t*) &_stringLiteral47, L_260, /*hidden argument*/NULL);
		RenderTexture_t101 * L_261 = V_1;
		RenderTexture_t101 * L_262 = V_6;
		Material_t55 * L_263 = (__this->___dx11MotionBlurMaterial_22);
		Graphics_Blit_m742(NULL /*static, unused*/, L_261, L_262, L_263, 0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_264 = V_6;
		RenderTexture_t101 * L_265 = V_7;
		Material_t55 * L_266 = (__this->___dx11MotionBlurMaterial_22);
		Graphics_Blit_m742(NULL /*static, unused*/, L_264, L_265, L_266, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_267 = ___source;
		RenderTexture_t101 * L_268 = ___destination;
		Material_t55 * L_269 = (__this->___dx11MotionBlurMaterial_22);
		Graphics_Blit_m742(NULL /*static, unused*/, L_267, L_268, L_269, 2, /*hidden argument*/NULL);
		goto IL_08a8;
	}

IL_07b9:
	{
		int32_t L_270 = (__this->___filterType_6);
		if ((((int32_t)L_270) == ((int32_t)2)))
		{
			goto IL_07cc;
		}
	}
	{
		bool L_271 = V_5;
		if (!L_271)
		{
			goto IL_081e;
		}
	}

IL_07cc:
	{
		Material_t55 * L_272 = (__this->___motionBlurMaterial_21);
		float L_273 = (__this->___softZDistance_14);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_274 = Mathf_Max_m782(NULL /*static, unused*/, (0.00025f), L_273, /*hidden argument*/NULL);
		NullCheck(L_272);
		Material_SetFloat_m738(L_272, (String_t*) &_stringLiteral57, L_274, /*hidden argument*/NULL);
		RenderTexture_t101 * L_275 = V_1;
		RenderTexture_t101 * L_276 = V_6;
		Material_t55 * L_277 = (__this->___motionBlurMaterial_21);
		Graphics_Blit_m742(NULL /*static, unused*/, L_275, L_276, L_277, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_278 = V_6;
		RenderTexture_t101 * L_279 = V_7;
		Material_t55 * L_280 = (__this->___motionBlurMaterial_21);
		Graphics_Blit_m742(NULL /*static, unused*/, L_278, L_279, L_280, 3, /*hidden argument*/NULL);
		RenderTexture_t101 * L_281 = ___source;
		RenderTexture_t101 * L_282 = ___destination;
		Material_t55 * L_283 = (__this->___motionBlurMaterial_21);
		Graphics_Blit_m742(NULL /*static, unused*/, L_281, L_282, L_283, 4, /*hidden argument*/NULL);
		goto IL_08a8;
	}

IL_081e:
	{
		int32_t L_284 = (__this->___filterType_6);
		if (L_284)
		{
			goto IL_083c;
		}
	}
	{
		RenderTexture_t101 * L_285 = ___source;
		RenderTexture_t101 * L_286 = ___destination;
		Material_t55 * L_287 = (__this->___motionBlurMaterial_21);
		Graphics_Blit_m742(NULL /*static, unused*/, L_285, L_286, L_287, 6, /*hidden argument*/NULL);
		goto IL_08a8;
	}

IL_083c:
	{
		int32_t L_288 = (__this->___filterType_6);
		if ((!(((uint32_t)L_288) == ((uint32_t)4))))
		{
			goto IL_089a;
		}
	}
	{
		Material_t55 * L_289 = (__this->___motionBlurMaterial_21);
		float L_290 = (__this->___softZDistance_14);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_291 = Mathf_Max_m782(NULL /*static, unused*/, (0.00025f), L_290, /*hidden argument*/NULL);
		NullCheck(L_289);
		Material_SetFloat_m738(L_289, (String_t*) &_stringLiteral57, L_291, /*hidden argument*/NULL);
		RenderTexture_t101 * L_292 = V_1;
		RenderTexture_t101 * L_293 = V_6;
		Material_t55 * L_294 = (__this->___motionBlurMaterial_21);
		Graphics_Blit_m742(NULL /*static, unused*/, L_292, L_293, L_294, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_295 = V_6;
		RenderTexture_t101 * L_296 = V_7;
		Material_t55 * L_297 = (__this->___motionBlurMaterial_21);
		Graphics_Blit_m742(NULL /*static, unused*/, L_295, L_296, L_297, 3, /*hidden argument*/NULL);
		RenderTexture_t101 * L_298 = ___source;
		RenderTexture_t101 * L_299 = ___destination;
		Material_t55 * L_300 = (__this->___motionBlurMaterial_21);
		Graphics_Blit_m742(NULL /*static, unused*/, L_298, L_299, L_300, 7, /*hidden argument*/NULL);
		goto IL_08a8;
	}

IL_089a:
	{
		RenderTexture_t101 * L_301 = ___source;
		RenderTexture_t101 * L_302 = ___destination;
		Material_t55 * L_303 = (__this->___motionBlurMaterial_21);
		Graphics_Blit_m742(NULL /*static, unused*/, L_301, L_302, L_303, 5, /*hidden argument*/NULL);
	}

IL_08a8:
	{
		RenderTexture_t101 * L_304 = V_1;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_304, /*hidden argument*/NULL);
		RenderTexture_t101 * L_305 = V_6;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_305, /*hidden argument*/NULL);
		RenderTexture_t101 * L_306 = V_7;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_306, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::Remember()
extern "C" void CameraMotionBlur_Remember_m226 (CameraMotionBlur_t79 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t80  L_0 = (__this->___currentViewProjMat_27);
		__this->___prevViewProjMat_28 = L_0;
		Transform_t1 * L_1 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4  L_2 = Transform_get_forward_m643(L_1, /*hidden argument*/NULL);
		__this->___prevFrameForward_31 = L_2;
		Transform_t1 * L_3 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_up_m644(L_3, /*hidden argument*/NULL);
		__this->___prevFrameUp_32 = L_4;
		Transform_t1 * L_5 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4  L_6 = Transform_get_position_m593(L_5, /*hidden argument*/NULL);
		__this->___prevFramePos_33 = L_6;
		return;
	}
}
// UnityEngine.Camera UnityStandardAssets.ImageEffects.CameraMotionBlur::GetTmpCam()
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t238_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t78_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCamera_t27_m770_MethodInfo_var;
extern "C" Camera_t27 * CameraMotionBlur_GetTmpCam_m227 (CameraMotionBlur_t79 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		TypeU5BU5D_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		GameObject_t78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		GameObject_GetComponent_TisCamera_t27_m770_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483660);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	GameObject_t78 * V_1 = {0};
	{
		GameObject_t78 * L_0 = (__this->___tmpCam_17);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006a;
		}
	}
	{
		Camera_t27 * L_2 = (__this->____camera_34);
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m798(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m734(NULL /*static, unused*/, (String_t*) &_stringLiteral58, L_3, (String_t*) &_stringLiteral59, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		GameObject_t78 * L_6 = GameObject_Find_m799(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GameObject_t78 * L_7 = V_1;
		bool L_8 = Object_op_Equality_m640(NULL /*static, unused*/, (Object_t164 *)NULL, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0063;
		}
	}
	{
		String_t* L_9 = V_0;
		TypeU5BU5D_t238* L_10 = ((TypeU5BU5D_t238*)SZArrayNew(TypeU5BU5D_t238_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m705(NULL /*static, unused*/, LoadTypeToken(Camera_t27_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_11);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_10, 0)) = (Type_t *)L_11;
		GameObject_t78 * L_12 = (GameObject_t78 *)il2cpp_codegen_object_new (GameObject_t78_il2cpp_TypeInfo_var);
		GameObject__ctor_m800(L_12, L_9, L_10, /*hidden argument*/NULL);
		__this->___tmpCam_17 = L_12;
		goto IL_006a;
	}

IL_0063:
	{
		GameObject_t78 * L_13 = V_1;
		__this->___tmpCam_17 = L_13;
	}

IL_006a:
	{
		GameObject_t78 * L_14 = (__this->___tmpCam_17);
		NullCheck(L_14);
		Object_set_hideFlags_m764(L_14, ((int32_t)52), /*hidden argument*/NULL);
		GameObject_t78 * L_15 = (__this->___tmpCam_17);
		NullCheck(L_15);
		Transform_t1 * L_16 = GameObject_get_transform_m609(L_15, /*hidden argument*/NULL);
		Camera_t27 * L_17 = (__this->____camera_34);
		NullCheck(L_17);
		Transform_t1 * L_18 = Component_get_transform_m594(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t4  L_19 = Transform_get_position_m593(L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_position_m607(L_16, L_19, /*hidden argument*/NULL);
		GameObject_t78 * L_20 = (__this->___tmpCam_17);
		NullCheck(L_20);
		Transform_t1 * L_21 = GameObject_get_transform_m609(L_20, /*hidden argument*/NULL);
		Camera_t27 * L_22 = (__this->____camera_34);
		NullCheck(L_22);
		Transform_t1 * L_23 = Component_get_transform_m594(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Quaternion_t19  L_24 = Transform_get_rotation_m656(L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_rotation_m658(L_21, L_24, /*hidden argument*/NULL);
		GameObject_t78 * L_25 = (__this->___tmpCam_17);
		NullCheck(L_25);
		Transform_t1 * L_26 = GameObject_get_transform_m609(L_25, /*hidden argument*/NULL);
		Camera_t27 * L_27 = (__this->____camera_34);
		NullCheck(L_27);
		Transform_t1 * L_28 = Component_get_transform_m594(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4  L_29 = Transform_get_localScale_m633(L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_localScale_m634(L_26, L_29, /*hidden argument*/NULL);
		GameObject_t78 * L_30 = (__this->___tmpCam_17);
		NullCheck(L_30);
		Camera_t27 * L_31 = GameObject_GetComponent_TisCamera_t27_m770(L_30, /*hidden argument*/GameObject_GetComponent_TisCamera_t27_m770_MethodInfo_var);
		Camera_t27 * L_32 = (__this->____camera_34);
		NullCheck(L_31);
		Camera_CopyFrom_m801(L_31, L_32, /*hidden argument*/NULL);
		GameObject_t78 * L_33 = (__this->___tmpCam_17);
		NullCheck(L_33);
		Camera_t27 * L_34 = GameObject_GetComponent_TisCamera_t27_m770(L_33, /*hidden argument*/GameObject_GetComponent_TisCamera_t27_m770_MethodInfo_var);
		NullCheck(L_34);
		Behaviour_set_enabled_m766(L_34, 0, /*hidden argument*/NULL);
		GameObject_t78 * L_35 = (__this->___tmpCam_17);
		NullCheck(L_35);
		Camera_t27 * L_36 = GameObject_GetComponent_TisCamera_t27_m770(L_35, /*hidden argument*/GameObject_GetComponent_TisCamera_t27_m770_MethodInfo_var);
		NullCheck(L_36);
		Camera_set_depthTextureMode_m780(L_36, 0, /*hidden argument*/NULL);
		GameObject_t78 * L_37 = (__this->___tmpCam_17);
		NullCheck(L_37);
		Camera_t27 * L_38 = GameObject_GetComponent_TisCamera_t27_m770(L_37, /*hidden argument*/GameObject_GetComponent_TisCamera_t27_m770_MethodInfo_var);
		NullCheck(L_38);
		Camera_set_clearFlags_m802(L_38, 4, /*hidden argument*/NULL);
		GameObject_t78 * L_39 = (__this->___tmpCam_17);
		NullCheck(L_39);
		Camera_t27 * L_40 = GameObject_GetComponent_TisCamera_t27_m770(L_39, /*hidden argument*/GameObject_GetComponent_TisCamera_t27_m770_MethodInfo_var);
		return L_40;
	}
}
// System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::StartFrame()
extern "C" void CameraMotionBlur_StartFrame_m228 (CameraMotionBlur_t79 * __this, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = (__this->___prevFramePos_33);
		Transform_t1 * L_1 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4  L_2 = Transform_get_position_m593(L_1, /*hidden argument*/NULL);
		Vector3_t4  L_3 = Vector3_Slerp_m655(NULL /*static, unused*/, L_0, L_2, (0.75f), /*hidden argument*/NULL);
		__this->___prevFramePos_33 = L_3;
		return;
	}
}
// System.Int32 UnityStandardAssets.ImageEffects.CameraMotionBlur::divRoundUp(System.Int32,System.Int32)
extern "C" int32_t CameraMotionBlur_divRoundUp_m229 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___d, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x;
		int32_t L_1 = ___d;
		int32_t L_2 = ___d;
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1))-(int32_t)1))/(int32_t)L_2));
	}
}
// UnityStandardAssets.ImageEffects.ColorCorrectionCurves/ColorCorrectionMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_21.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ColorCorrectionCurves/ColorCorrectionMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_21MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.ColorCorrectionCurves
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_22.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ColorCorrectionCurves
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_22MethodDeclarations.h"

// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurve.h"
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::.ctor()
extern TypeInfo* KeyframeU5BU5D_t239_il2cpp_TypeInfo_var;
extern TypeInfo* AnimationCurve_t82_il2cpp_TypeInfo_var;
extern "C" void ColorCorrectionCurves__ctor_m230 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyframeU5BU5D_t239_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		AnimationCurve_t82_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	{
		KeyframeU5BU5D_t239* L_0 = ((KeyframeU5BU5D_t239*)SZArrayNew(KeyframeU5BU5D_t239_il2cpp_TypeInfo_var, 2));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Keyframe_t240  L_1 = {0};
		Keyframe__ctor_m803(&L_1, (0.0f), (0.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_0, 0)) = L_1;
		KeyframeU5BU5D_t239* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Keyframe_t240  L_3 = {0};
		Keyframe__ctor_m803(&L_3, (1.0f), (1.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_2, 1)) = L_3;
		AnimationCurve_t82 * L_4 = (AnimationCurve_t82 *)il2cpp_codegen_object_new (AnimationCurve_t82_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m804(L_4, L_2, /*hidden argument*/NULL);
		__this->___redChannel_5 = L_4;
		KeyframeU5BU5D_t239* L_5 = ((KeyframeU5BU5D_t239*)SZArrayNew(KeyframeU5BU5D_t239_il2cpp_TypeInfo_var, 2));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		Keyframe_t240  L_6 = {0};
		Keyframe__ctor_m803(&L_6, (0.0f), (0.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_5, 0)) = L_6;
		KeyframeU5BU5D_t239* L_7 = L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		Keyframe_t240  L_8 = {0};
		Keyframe__ctor_m803(&L_8, (1.0f), (1.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_7, 1)) = L_8;
		AnimationCurve_t82 * L_9 = (AnimationCurve_t82 *)il2cpp_codegen_object_new (AnimationCurve_t82_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m804(L_9, L_7, /*hidden argument*/NULL);
		__this->___greenChannel_6 = L_9;
		KeyframeU5BU5D_t239* L_10 = ((KeyframeU5BU5D_t239*)SZArrayNew(KeyframeU5BU5D_t239_il2cpp_TypeInfo_var, 2));
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		Keyframe_t240  L_11 = {0};
		Keyframe__ctor_m803(&L_11, (0.0f), (0.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_10, 0)) = L_11;
		KeyframeU5BU5D_t239* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		Keyframe_t240  L_13 = {0};
		Keyframe__ctor_m803(&L_13, (1.0f), (1.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_12, 1)) = L_13;
		AnimationCurve_t82 * L_14 = (AnimationCurve_t82 *)il2cpp_codegen_object_new (AnimationCurve_t82_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m804(L_14, L_12, /*hidden argument*/NULL);
		__this->___blueChannel_7 = L_14;
		KeyframeU5BU5D_t239* L_15 = ((KeyframeU5BU5D_t239*)SZArrayNew(KeyframeU5BU5D_t239_il2cpp_TypeInfo_var, 2));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		Keyframe_t240  L_16 = {0};
		Keyframe__ctor_m803(&L_16, (0.0f), (0.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_15, 0)) = L_16;
		KeyframeU5BU5D_t239* L_17 = L_15;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		Keyframe_t240  L_18 = {0};
		Keyframe__ctor_m803(&L_18, (1.0f), (1.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_17, 1)) = L_18;
		AnimationCurve_t82 * L_19 = (AnimationCurve_t82 *)il2cpp_codegen_object_new (AnimationCurve_t82_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m804(L_19, L_17, /*hidden argument*/NULL);
		__this->___zCurve_9 = L_19;
		KeyframeU5BU5D_t239* L_20 = ((KeyframeU5BU5D_t239*)SZArrayNew(KeyframeU5BU5D_t239_il2cpp_TypeInfo_var, 2));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		Keyframe_t240  L_21 = {0};
		Keyframe__ctor_m803(&L_21, (0.0f), (0.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_20, 0)) = L_21;
		KeyframeU5BU5D_t239* L_22 = L_20;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 1);
		Keyframe_t240  L_23 = {0};
		Keyframe__ctor_m803(&L_23, (1.0f), (1.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_22, 1)) = L_23;
		AnimationCurve_t82 * L_24 = (AnimationCurve_t82 *)il2cpp_codegen_object_new (AnimationCurve_t82_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m804(L_24, L_22, /*hidden argument*/NULL);
		__this->___depthRedChannel_10 = L_24;
		KeyframeU5BU5D_t239* L_25 = ((KeyframeU5BU5D_t239*)SZArrayNew(KeyframeU5BU5D_t239_il2cpp_TypeInfo_var, 2));
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		Keyframe_t240  L_26 = {0};
		Keyframe__ctor_m803(&L_26, (0.0f), (0.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_25, 0)) = L_26;
		KeyframeU5BU5D_t239* L_27 = L_25;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 1);
		Keyframe_t240  L_28 = {0};
		Keyframe__ctor_m803(&L_28, (1.0f), (1.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_27, 1)) = L_28;
		AnimationCurve_t82 * L_29 = (AnimationCurve_t82 *)il2cpp_codegen_object_new (AnimationCurve_t82_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m804(L_29, L_27, /*hidden argument*/NULL);
		__this->___depthGreenChannel_11 = L_29;
		KeyframeU5BU5D_t239* L_30 = ((KeyframeU5BU5D_t239*)SZArrayNew(KeyframeU5BU5D_t239_il2cpp_TypeInfo_var, 2));
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		Keyframe_t240  L_31 = {0};
		Keyframe__ctor_m803(&L_31, (0.0f), (0.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_30, 0)) = L_31;
		KeyframeU5BU5D_t239* L_32 = L_30;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 1);
		Keyframe_t240  L_33 = {0};
		Keyframe__ctor_m803(&L_33, (1.0f), (1.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_32, 1)) = L_33;
		AnimationCurve_t82 * L_34 = (AnimationCurve_t82 *)il2cpp_codegen_object_new (AnimationCurve_t82_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m804(L_34, L_32, /*hidden argument*/NULL);
		__this->___depthBlueChannel_12 = L_34;
		__this->___saturation_19 = (1.0f);
		Color_t65  L_35 = Color_get_white_m745(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___selectiveFromColor_21 = L_35;
		Color_t65  L_36 = Color_get_white_m745(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___selectiveToColor_22 = L_36;
		__this->___updateTextures_24 = 1;
		__this->___updateTexturesOnStartup_28 = 1;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Start()
extern "C" void ColorCorrectionCurves_Start_m231 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_Start_m333(__this, /*hidden argument*/NULL);
		__this->___updateTexturesOnStartup_28 = 1;
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Awake()
extern "C" void ColorCorrectionCurves_Awake_m232 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::CheckResources()
extern TypeInfo* Texture2D_t63_il2cpp_TypeInfo_var;
extern "C" bool ColorCorrectionCurves_CheckResources_m233 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___mode_23);
		PostEffectsBase_CheckSupport_m334(__this, ((((int32_t)L_0) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		Shader_t54 * L_1 = (__this->___simpleColorCorrectionCurvesShader_26);
		Material_t55 * L_2 = (__this->___ccMaterial_13);
		Material_t55 * L_3 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_1, L_2, /*hidden argument*/NULL);
		__this->___ccMaterial_13 = L_3;
		Shader_t54 * L_4 = (__this->___colorCorrectionCurvesShader_25);
		Material_t55 * L_5 = (__this->___ccDepthMaterial_14);
		Material_t55 * L_6 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_4, L_5, /*hidden argument*/NULL);
		__this->___ccDepthMaterial_14 = L_6;
		Shader_t54 * L_7 = (__this->___colorCorrectionSelectiveShader_27);
		Material_t55 * L_8 = (__this->___selectiveCcMaterial_15);
		Material_t55 * L_9 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_7, L_8, /*hidden argument*/NULL);
		__this->___selectiveCcMaterial_15 = L_9;
		Texture2D_t63 * L_10 = (__this->___rgbChannelTex_16);
		bool L_11 = Object_op_Implicit_m629(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_007c;
		}
	}
	{
		Texture2D_t63 * L_12 = (Texture2D_t63 *)il2cpp_codegen_object_new (Texture2D_t63_il2cpp_TypeInfo_var);
		Texture2D__ctor_m805(L_12, ((int32_t)256), 4, 5, 0, 1, /*hidden argument*/NULL);
		__this->___rgbChannelTex_16 = L_12;
	}

IL_007c:
	{
		Texture2D_t63 * L_13 = (__this->___rgbDepthChannelTex_17);
		bool L_14 = Object_op_Implicit_m629(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_00a0;
		}
	}
	{
		Texture2D_t63 * L_15 = (Texture2D_t63 *)il2cpp_codegen_object_new (Texture2D_t63_il2cpp_TypeInfo_var);
		Texture2D__ctor_m805(L_15, ((int32_t)256), 4, 5, 0, 1, /*hidden argument*/NULL);
		__this->___rgbDepthChannelTex_17 = L_15;
	}

IL_00a0:
	{
		Texture2D_t63 * L_16 = (__this->___zCurveTex_18);
		bool L_17 = Object_op_Implicit_m629(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00c4;
		}
	}
	{
		Texture2D_t63 * L_18 = (Texture2D_t63 *)il2cpp_codegen_object_new (Texture2D_t63_il2cpp_TypeInfo_var);
		Texture2D__ctor_m805(L_18, ((int32_t)256), 1, 5, 0, 1, /*hidden argument*/NULL);
		__this->___zCurveTex_18 = L_18;
	}

IL_00c4:
	{
		Texture2D_t63 * L_19 = (__this->___rgbChannelTex_16);
		NullCheck(L_19);
		Object_set_hideFlags_m764(L_19, ((int32_t)52), /*hidden argument*/NULL);
		Texture2D_t63 * L_20 = (__this->___rgbDepthChannelTex_17);
		NullCheck(L_20);
		Object_set_hideFlags_m764(L_20, ((int32_t)52), /*hidden argument*/NULL);
		Texture2D_t63 * L_21 = (__this->___zCurveTex_18);
		NullCheck(L_21);
		Object_set_hideFlags_m764(L_21, ((int32_t)52), /*hidden argument*/NULL);
		Texture2D_t63 * L_22 = (__this->___rgbChannelTex_16);
		NullCheck(L_22);
		Texture_set_wrapMode_m784(L_22, 1, /*hidden argument*/NULL);
		Texture2D_t63 * L_23 = (__this->___rgbDepthChannelTex_17);
		NullCheck(L_23);
		Texture_set_wrapMode_m784(L_23, 1, /*hidden argument*/NULL);
		Texture2D_t63 * L_24 = (__this->___zCurveTex_18);
		NullCheck(L_24);
		Texture_set_wrapMode_m784(L_24, 1, /*hidden argument*/NULL);
		bool L_25 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_25)
		{
			goto IL_0120;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0120:
	{
		bool L_26 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_26;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateParameters()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void ColorCorrectionCurves_UpdateParameters_m234 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::CheckResources() */, __this);
		AnimationCurve_t82 * L_0 = (__this->___redChannel_5);
		if (!L_0)
		{
			goto IL_0216;
		}
	}
	{
		AnimationCurve_t82 * L_1 = (__this->___greenChannel_6);
		if (!L_1)
		{
			goto IL_0216;
		}
	}
	{
		AnimationCurve_t82 * L_2 = (__this->___blueChannel_7);
		if (!L_2)
		{
			goto IL_0216;
		}
	}
	{
		V_0 = (0.0f);
		goto IL_01ea;
	}

IL_0033:
	{
		AnimationCurve_t82 * L_3 = (__this->___redChannel_5);
		float L_4 = V_0;
		NullCheck(L_3);
		float L_5 = AnimationCurve_Evaluate_m806(L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp_m611(NULL /*static, unused*/, L_5, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_6;
		AnimationCurve_t82 * L_7 = (__this->___greenChannel_6);
		float L_8 = V_0;
		NullCheck(L_7);
		float L_9 = AnimationCurve_Evaluate_m806(L_7, L_8, /*hidden argument*/NULL);
		float L_10 = Mathf_Clamp_m611(NULL /*static, unused*/, L_9, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_2 = L_10;
		AnimationCurve_t82 * L_11 = (__this->___blueChannel_7);
		float L_12 = V_0;
		NullCheck(L_11);
		float L_13 = AnimationCurve_Evaluate_m806(L_11, L_12, /*hidden argument*/NULL);
		float L_14 = Mathf_Clamp_m611(NULL /*static, unused*/, L_13, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_3 = L_14;
		Texture2D_t63 * L_15 = (__this->___rgbChannelTex_16);
		float L_16 = V_0;
		float L_17 = floorf(((float)((float)L_16*(float)(255.0f))));
		float L_18 = V_1;
		float L_19 = V_1;
		float L_20 = V_1;
		Color_t65  L_21 = {0};
		Color__ctor_m807(&L_21, L_18, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_15);
		Texture2D_SetPixel_m808(L_15, (((int32_t)L_17)), 0, L_21, /*hidden argument*/NULL);
		Texture2D_t63 * L_22 = (__this->___rgbChannelTex_16);
		float L_23 = V_0;
		float L_24 = floorf(((float)((float)L_23*(float)(255.0f))));
		float L_25 = V_2;
		float L_26 = V_2;
		float L_27 = V_2;
		Color_t65  L_28 = {0};
		Color__ctor_m807(&L_28, L_25, L_26, L_27, /*hidden argument*/NULL);
		NullCheck(L_22);
		Texture2D_SetPixel_m808(L_22, (((int32_t)L_24)), 1, L_28, /*hidden argument*/NULL);
		Texture2D_t63 * L_29 = (__this->___rgbChannelTex_16);
		float L_30 = V_0;
		float L_31 = floorf(((float)((float)L_30*(float)(255.0f))));
		float L_32 = V_3;
		float L_33 = V_3;
		float L_34 = V_3;
		Color_t65  L_35 = {0};
		Color__ctor_m807(&L_35, L_32, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_29);
		Texture2D_SetPixel_m808(L_29, (((int32_t)L_31)), 2, L_35, /*hidden argument*/NULL);
		AnimationCurve_t82 * L_36 = (__this->___zCurve_9);
		float L_37 = V_0;
		NullCheck(L_36);
		float L_38 = AnimationCurve_Evaluate_m806(L_36, L_37, /*hidden argument*/NULL);
		float L_39 = Mathf_Clamp_m611(NULL /*static, unused*/, L_38, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_4 = L_39;
		Texture2D_t63 * L_40 = (__this->___zCurveTex_18);
		float L_41 = V_0;
		float L_42 = floorf(((float)((float)L_41*(float)(255.0f))));
		float L_43 = V_4;
		float L_44 = V_4;
		float L_45 = V_4;
		Color_t65  L_46 = {0};
		Color__ctor_m807(&L_46, L_43, L_44, L_45, /*hidden argument*/NULL);
		NullCheck(L_40);
		Texture2D_SetPixel_m808(L_40, (((int32_t)L_42)), 0, L_46, /*hidden argument*/NULL);
		AnimationCurve_t82 * L_47 = (__this->___depthRedChannel_10);
		float L_48 = V_0;
		NullCheck(L_47);
		float L_49 = AnimationCurve_Evaluate_m806(L_47, L_48, /*hidden argument*/NULL);
		float L_50 = Mathf_Clamp_m611(NULL /*static, unused*/, L_49, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_50;
		AnimationCurve_t82 * L_51 = (__this->___depthGreenChannel_11);
		float L_52 = V_0;
		NullCheck(L_51);
		float L_53 = AnimationCurve_Evaluate_m806(L_51, L_52, /*hidden argument*/NULL);
		float L_54 = Mathf_Clamp_m611(NULL /*static, unused*/, L_53, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_2 = L_54;
		AnimationCurve_t82 * L_55 = (__this->___depthBlueChannel_12);
		float L_56 = V_0;
		NullCheck(L_55);
		float L_57 = AnimationCurve_Evaluate_m806(L_55, L_56, /*hidden argument*/NULL);
		float L_58 = Mathf_Clamp_m611(NULL /*static, unused*/, L_57, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_3 = L_58;
		Texture2D_t63 * L_59 = (__this->___rgbDepthChannelTex_17);
		float L_60 = V_0;
		float L_61 = floorf(((float)((float)L_60*(float)(255.0f))));
		float L_62 = V_1;
		float L_63 = V_1;
		float L_64 = V_1;
		Color_t65  L_65 = {0};
		Color__ctor_m807(&L_65, L_62, L_63, L_64, /*hidden argument*/NULL);
		NullCheck(L_59);
		Texture2D_SetPixel_m808(L_59, (((int32_t)L_61)), 0, L_65, /*hidden argument*/NULL);
		Texture2D_t63 * L_66 = (__this->___rgbDepthChannelTex_17);
		float L_67 = V_0;
		float L_68 = floorf(((float)((float)L_67*(float)(255.0f))));
		float L_69 = V_2;
		float L_70 = V_2;
		float L_71 = V_2;
		Color_t65  L_72 = {0};
		Color__ctor_m807(&L_72, L_69, L_70, L_71, /*hidden argument*/NULL);
		NullCheck(L_66);
		Texture2D_SetPixel_m808(L_66, (((int32_t)L_68)), 1, L_72, /*hidden argument*/NULL);
		Texture2D_t63 * L_73 = (__this->___rgbDepthChannelTex_17);
		float L_74 = V_0;
		float L_75 = floorf(((float)((float)L_74*(float)(255.0f))));
		float L_76 = V_3;
		float L_77 = V_3;
		float L_78 = V_3;
		Color_t65  L_79 = {0};
		Color__ctor_m807(&L_79, L_76, L_77, L_78, /*hidden argument*/NULL);
		NullCheck(L_73);
		Texture2D_SetPixel_m808(L_73, (((int32_t)L_75)), 2, L_79, /*hidden argument*/NULL);
		float L_80 = V_0;
		V_0 = ((float)((float)L_80+(float)(0.003921569f)));
	}

IL_01ea:
	{
		float L_81 = V_0;
		if ((((float)L_81) <= ((float)(1.0f))))
		{
			goto IL_0033;
		}
	}
	{
		Texture2D_t63 * L_82 = (__this->___rgbChannelTex_16);
		NullCheck(L_82);
		Texture2D_Apply_m809(L_82, /*hidden argument*/NULL);
		Texture2D_t63 * L_83 = (__this->___rgbDepthChannelTex_17);
		NullCheck(L_83);
		Texture2D_Apply_m809(L_83, /*hidden argument*/NULL);
		Texture2D_t63 * L_84 = (__this->___zCurveTex_18);
		NullCheck(L_84);
		Texture2D_Apply_m809(L_84, /*hidden argument*/NULL);
	}

IL_0216:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateTextures()
extern "C" void ColorCorrectionCurves_UpdateTextures_m235 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method)
{
	{
		ColorCorrectionCurves_UpdateParameters_m234(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void ColorCorrectionCurves_OnRenderImage_m236 (ColorCorrectionCurves_t83 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t101 * V_0 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		bool L_3 = (__this->___updateTexturesOnStartup_28);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		ColorCorrectionCurves_UpdateParameters_m234(__this, /*hidden argument*/NULL);
		__this->___updateTexturesOnStartup_28 = 0;
	}

IL_002b:
	{
		bool L_4 = (__this->___useDepthCorrection_8);
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		Camera_t27 * L_5 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		Camera_t27 * L_6 = L_5;
		NullCheck(L_6);
		int32_t L_7 = Camera_get_depthTextureMode_m779(L_6, /*hidden argument*/NULL);
		NullCheck(L_6);
		Camera_set_depthTextureMode_m780(L_6, ((int32_t)((int32_t)L_7|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0049:
	{
		RenderTexture_t101 * L_8 = ___destination;
		V_0 = L_8;
		bool L_9 = (__this->___selectiveCc_20);
		if (!L_9)
		{
			goto IL_0068;
		}
	}
	{
		RenderTexture_t101 * L_10 = ___source;
		NullCheck(L_10);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_10);
		RenderTexture_t101 * L_12 = ___source;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_12);
		RenderTexture_t101 * L_14 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0068:
	{
		bool L_15 = (__this->___useDepthCorrection_8);
		if (!L_15)
		{
			goto IL_00dd;
		}
	}
	{
		Material_t55 * L_16 = (__this->___ccDepthMaterial_14);
		Texture2D_t63 * L_17 = (__this->___rgbChannelTex_16);
		NullCheck(L_16);
		Material_SetTexture_m759(L_16, (String_t*) &_stringLiteral60, L_17, /*hidden argument*/NULL);
		Material_t55 * L_18 = (__this->___ccDepthMaterial_14);
		Texture2D_t63 * L_19 = (__this->___zCurveTex_18);
		NullCheck(L_18);
		Material_SetTexture_m759(L_18, (String_t*) &_stringLiteral61, L_19, /*hidden argument*/NULL);
		Material_t55 * L_20 = (__this->___ccDepthMaterial_14);
		Texture2D_t63 * L_21 = (__this->___rgbDepthChannelTex_17);
		NullCheck(L_20);
		Material_SetTexture_m759(L_20, (String_t*) &_stringLiteral62, L_21, /*hidden argument*/NULL);
		Material_t55 * L_22 = (__this->___ccDepthMaterial_14);
		float L_23 = (__this->___saturation_19);
		NullCheck(L_22);
		Material_SetFloat_m738(L_22, (String_t*) &_stringLiteral26, L_23, /*hidden argument*/NULL);
		RenderTexture_t101 * L_24 = ___source;
		RenderTexture_t101 * L_25 = V_0;
		Material_t55 * L_26 = (__this->___ccDepthMaterial_14);
		Graphics_Blit_m739(NULL /*static, unused*/, L_24, L_25, L_26, /*hidden argument*/NULL);
		goto IL_0116;
	}

IL_00dd:
	{
		Material_t55 * L_27 = (__this->___ccMaterial_13);
		Texture2D_t63 * L_28 = (__this->___rgbChannelTex_16);
		NullCheck(L_27);
		Material_SetTexture_m759(L_27, (String_t*) &_stringLiteral60, L_28, /*hidden argument*/NULL);
		Material_t55 * L_29 = (__this->___ccMaterial_13);
		float L_30 = (__this->___saturation_19);
		NullCheck(L_29);
		Material_SetFloat_m738(L_29, (String_t*) &_stringLiteral26, L_30, /*hidden argument*/NULL);
		RenderTexture_t101 * L_31 = ___source;
		RenderTexture_t101 * L_32 = V_0;
		Material_t55 * L_33 = (__this->___ccMaterial_13);
		Graphics_Blit_m739(NULL /*static, unused*/, L_31, L_32, L_33, /*hidden argument*/NULL);
	}

IL_0116:
	{
		bool L_34 = (__this->___selectiveCc_20);
		if (!L_34)
		{
			goto IL_0160;
		}
	}
	{
		Material_t55 * L_35 = (__this->___selectiveCcMaterial_15);
		Color_t65  L_36 = (__this->___selectiveFromColor_21);
		NullCheck(L_35);
		Material_SetColor_m810(L_35, (String_t*) &_stringLiteral63, L_36, /*hidden argument*/NULL);
		Material_t55 * L_37 = (__this->___selectiveCcMaterial_15);
		Color_t65  L_38 = (__this->___selectiveToColor_22);
		NullCheck(L_37);
		Material_SetColor_m810(L_37, (String_t*) &_stringLiteral64, L_38, /*hidden argument*/NULL);
		RenderTexture_t101 * L_39 = V_0;
		RenderTexture_t101 * L_40 = ___destination;
		Material_t55 * L_41 = (__this->___selectiveCcMaterial_15);
		Graphics_Blit_m739(NULL /*static, unused*/, L_39, L_40, L_41, /*hidden argument*/NULL);
		RenderTexture_t101 * L_42 = V_0;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
	}

IL_0160:
	{
		return;
	}
}
// UnityStandardAssets.ImageEffects.ColorCorrectionLookup
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_23.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ColorCorrectionLookup
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_23MethodDeclarations.h"

// UnityEngine.Texture3D
#include "UnityEngine_UnityEngine_Texture3D.h"
// UnityEngine.ColorSpace
#include "UnityEngine_UnityEngine_ColorSpace.h"
// UnityEngine.Texture3D
#include "UnityEngine_UnityEngine_Texture3DMethodDeclarations.h"
// UnityEngine.QualitySettings
#include "UnityEngine_UnityEngine_QualitySettingsMethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void ColorCorrectionLookup__ctor_m237 (ColorCorrectionLookup_t85 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___basedOnTempTex_8 = L_0;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::CheckResources()
extern "C" bool ColorCorrectionLookup_CheckResources_m238 (ColorCorrectionLookup_t85 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___shader_5);
		Material_t55 * L_1 = (__this->___material_6);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___material_6 = L_2;
		bool L_3 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		bool L_4 = SystemInfo_get_supports3DTextures_m811(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_003b:
	{
		bool L_5 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_5;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDisable()
extern "C" void ColorCorrectionLookup_OnDisable_m239 (ColorCorrectionLookup_t85 * __this, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___material_6);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Material_t55 * L_2 = (__this->___material_6);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->___material_6 = (Material_t55 *)NULL;
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDestroy()
extern "C" void ColorCorrectionLookup_OnDestroy_m240 (ColorCorrectionLookup_t85 * __this, const MethodInfo* method)
{
	{
		Texture3D_t84 * L_0 = (__this->___converted3DLut_7);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Texture3D_t84 * L_2 = (__this->___converted3DLut_7);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		__this->___converted3DLut_7 = (Texture3D_t84 *)NULL;
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::SetIdentityLut()
extern TypeInfo* ColorU5BU5D_t241_il2cpp_TypeInfo_var;
extern TypeInfo* Texture3D_t84_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void ColorCorrectionLookup_SetIdentityLut_m241 (ColorCorrectionLookup_t85 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ColorU5BU5D_t241_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Texture3D_t84_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ColorU5BU5D_t241* V_1 = {0};
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_0 = ((int32_t)16);
		int32_t L_0 = V_0;
		int32_t L_1 = V_0;
		int32_t L_2 = V_0;
		V_1 = ((ColorU5BU5D_t241*)SZArrayNew(ColorU5BU5D_t241_il2cpp_TypeInfo_var, ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)L_1))*(int32_t)L_2))));
		int32_t L_3 = V_0;
		V_2 = ((float)((float)(1.0f)/(float)((float)((float)((float)((float)(1.0f)*(float)(((float)L_3))))-(float)(1.0f)))));
		V_3 = 0;
		goto IL_009c;
	}

IL_002a:
	{
		V_4 = 0;
		goto IL_0090;
	}

IL_0032:
	{
		V_5 = 0;
		goto IL_0082;
	}

IL_003a:
	{
		ColorU5BU5D_t241* L_4 = V_1;
		int32_t L_5 = V_3;
		int32_t L_6 = V_4;
		int32_t L_7 = V_0;
		int32_t L_8 = V_5;
		int32_t L_9 = V_0;
		int32_t L_10 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, ((int32_t)((int32_t)((int32_t)((int32_t)L_5+(int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7))))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8*(int32_t)L_9))*(int32_t)L_10)))));
		int32_t L_11 = V_3;
		float L_12 = V_2;
		int32_t L_13 = V_4;
		float L_14 = V_2;
		int32_t L_15 = V_5;
		float L_16 = V_2;
		Color_t65  L_17 = {0};
		Color__ctor_m746(&L_17, ((float)((float)((float)((float)(((float)L_11))*(float)(1.0f)))*(float)L_12)), ((float)((float)((float)((float)(((float)L_13))*(float)(1.0f)))*(float)L_14)), ((float)((float)((float)((float)(((float)L_15))*(float)(1.0f)))*(float)L_16)), (1.0f), /*hidden argument*/NULL);
		*((Color_t65 *)(Color_t65 *)SZArrayLdElema(L_4, ((int32_t)((int32_t)((int32_t)((int32_t)L_5+(int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7))))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8*(int32_t)L_9))*(int32_t)L_10)))))) = L_17;
		int32_t L_18 = V_5;
		V_5 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0082:
	{
		int32_t L_19 = V_5;
		int32_t L_20 = V_0;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_21 = V_4;
		V_4 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0090:
	{
		int32_t L_22 = V_4;
		int32_t L_23 = V_0;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_24 = V_3;
		V_3 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009c:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_0;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_002a;
		}
	}
	{
		Texture3D_t84 * L_27 = (__this->___converted3DLut_7);
		bool L_28 = Object_op_Implicit_m629(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00be;
		}
	}
	{
		Texture3D_t84 * L_29 = (__this->___converted3DLut_7);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_00be:
	{
		int32_t L_30 = V_0;
		int32_t L_31 = V_0;
		int32_t L_32 = V_0;
		Texture3D_t84 * L_33 = (Texture3D_t84 *)il2cpp_codegen_object_new (Texture3D_t84_il2cpp_TypeInfo_var);
		Texture3D__ctor_m812(L_33, L_30, L_31, L_32, 5, 0, /*hidden argument*/NULL);
		__this->___converted3DLut_7 = L_33;
		Texture3D_t84 * L_34 = (__this->___converted3DLut_7);
		ColorU5BU5D_t241* L_35 = V_1;
		NullCheck(L_34);
		Texture3D_SetPixels_m813(L_34, L_35, /*hidden argument*/NULL);
		Texture3D_t84 * L_36 = (__this->___converted3DLut_7);
		NullCheck(L_36);
		Texture3D_Apply_m814(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___basedOnTempTex_8 = L_37;
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::ValidDimensions(UnityEngine.Texture2D)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" bool ColorCorrectionLookup_ValidDimensions_m242 (ColorCorrectionLookup_t85 * __this, Texture2D_t63 * ___tex2d, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Texture2D_t63 * L_0 = ___tex2d;
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Texture2D_t63 * L_2 = ___tex2d;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_2);
		V_0 = L_3;
		int32_t L_4 = V_0;
		Texture2D_t63 * L_5 = ___tex2d;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_7 = sqrtf((((float)L_6)));
		int32_t L_8 = Mathf_FloorToInt_m815(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)L_8)))
		{
			goto IL_002d;
		}
	}
	{
		return 0;
	}

IL_002d:
	{
		return 1;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::Convert(UnityEngine.Texture2D,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ColorU5BU5D_t241_il2cpp_TypeInfo_var;
extern TypeInfo* Texture3D_t84_il2cpp_TypeInfo_var;
extern "C" void ColorCorrectionLookup_Convert_m243 (ColorCorrectionLookup_t85 * __this, Texture2D_t63 * ___temp2DTex, String_t* ___path, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		ColorU5BU5D_t241_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Texture3D_t84_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ColorU5BU5D_t241* V_1 = {0};
	ColorU5BU5D_t241* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		Texture2D_t63 * L_0 = ___temp2DTex;
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0126;
		}
	}
	{
		Texture2D_t63 * L_2 = ___temp2DTex;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		Texture2D_t63 * L_4 = ___temp2DTex;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_4);
		V_0 = ((int32_t)((int32_t)L_3*(int32_t)L_5));
		Texture2D_t63 * L_6 = ___temp2DTex;
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_6);
		V_0 = L_7;
		Texture2D_t63 * L_8 = ___temp2DTex;
		bool L_9 = ColorCorrectionLookup_ValidDimensions_m242(__this, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0052;
		}
	}
	{
		Texture2D_t63 * L_10 = ___temp2DTex;
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m798(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m734(NULL /*static, unused*/, (String_t*) &_stringLiteral65, L_11, (String_t*) &_stringLiteral66, /*hidden argument*/NULL);
		Debug_LogWarning_m816(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___basedOnTempTex_8 = L_13;
		return;
	}

IL_0052:
	{
		Texture2D_t63 * L_14 = ___temp2DTex;
		NullCheck(L_14);
		ColorU5BU5D_t241* L_15 = Texture2D_GetPixels_m817(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		ColorU5BU5D_t241* L_16 = V_1;
		NullCheck(L_16);
		V_2 = ((ColorU5BU5D_t241*)SZArrayNew(ColorU5BU5D_t241_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_16)->max_length)))));
		V_3 = 0;
		goto IL_00d1;
	}

IL_0069:
	{
		V_4 = 0;
		goto IL_00c5;
	}

IL_0071:
	{
		V_5 = 0;
		goto IL_00b7;
	}

IL_0079:
	{
		int32_t L_17 = V_0;
		int32_t L_18 = V_4;
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)L_17-(int32_t)L_18))-(int32_t)1));
		ColorU5BU5D_t241* L_19 = V_2;
		int32_t L_20 = V_3;
		int32_t L_21 = V_4;
		int32_t L_22 = V_0;
		int32_t L_23 = V_5;
		int32_t L_24 = V_0;
		int32_t L_25 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, ((int32_t)((int32_t)((int32_t)((int32_t)L_20+(int32_t)((int32_t)((int32_t)L_21*(int32_t)L_22))))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_23*(int32_t)L_24))*(int32_t)L_25)))));
		ColorU5BU5D_t241* L_26 = V_1;
		int32_t L_27 = V_5;
		int32_t L_28 = V_0;
		int32_t L_29 = V_3;
		int32_t L_30 = V_6;
		int32_t L_31 = V_0;
		int32_t L_32 = V_0;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_27*(int32_t)L_28))+(int32_t)L_29))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_30*(int32_t)L_31))*(int32_t)L_32)))));
		*((Color_t65 *)(Color_t65 *)SZArrayLdElema(L_19, ((int32_t)((int32_t)((int32_t)((int32_t)L_20+(int32_t)((int32_t)((int32_t)L_21*(int32_t)L_22))))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_23*(int32_t)L_24))*(int32_t)L_25)))))) = (*(Color_t65 *)((Color_t65 *)(Color_t65 *)SZArrayLdElema(L_26, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_27*(int32_t)L_28))+(int32_t)L_29))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_30*(int32_t)L_31))*(int32_t)L_32)))))));
		int32_t L_33 = V_5;
		V_5 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b7:
	{
		int32_t L_34 = V_5;
		int32_t L_35 = V_0;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_0079;
		}
	}
	{
		int32_t L_36 = V_4;
		V_4 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00c5:
	{
		int32_t L_37 = V_4;
		int32_t L_38 = V_0;
		if ((((int32_t)L_37) < ((int32_t)L_38)))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_39 = V_3;
		V_3 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_00d1:
	{
		int32_t L_40 = V_3;
		int32_t L_41 = V_0;
		if ((((int32_t)L_40) < ((int32_t)L_41)))
		{
			goto IL_0069;
		}
	}
	{
		Texture3D_t84 * L_42 = (__this->___converted3DLut_7);
		bool L_43 = Object_op_Implicit_m629(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_00f3;
		}
	}
	{
		Texture3D_t84 * L_44 = (__this->___converted3DLut_7);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		int32_t L_45 = V_0;
		int32_t L_46 = V_0;
		int32_t L_47 = V_0;
		Texture3D_t84 * L_48 = (Texture3D_t84 *)il2cpp_codegen_object_new (Texture3D_t84_il2cpp_TypeInfo_var);
		Texture3D__ctor_m812(L_48, L_45, L_46, L_47, 5, 0, /*hidden argument*/NULL);
		__this->___converted3DLut_7 = L_48;
		Texture3D_t84 * L_49 = (__this->___converted3DLut_7);
		ColorU5BU5D_t241* L_50 = V_2;
		NullCheck(L_49);
		Texture3D_SetPixels_m813(L_49, L_50, /*hidden argument*/NULL);
		Texture3D_t84 * L_51 = (__this->___converted3DLut_7);
		NullCheck(L_51);
		Texture3D_Apply_m814(L_51, /*hidden argument*/NULL);
		String_t* L_52 = ___path;
		__this->___basedOnTempTex_8 = L_52;
		goto IL_0130;
	}

IL_0126:
	{
		Debug_LogError_m735(NULL /*static, unused*/, (String_t*) &_stringLiteral67, /*hidden argument*/NULL);
	}

IL_0130:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ColorCorrectionLookup_OnRenderImage_m244 (ColorCorrectionLookup_t85 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Material_t55 * G_B7_0 = {0};
	RenderTexture_t101 * G_B7_1 = {0};
	RenderTexture_t101 * G_B7_2 = {0};
	Material_t55 * G_B6_0 = {0};
	RenderTexture_t101 * G_B6_1 = {0};
	RenderTexture_t101 * G_B6_2 = {0};
	int32_t G_B8_0 = 0;
	Material_t55 * G_B8_1 = {0};
	RenderTexture_t101 * G_B8_2 = {0};
	RenderTexture_t101 * G_B8_3 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::CheckResources() */, __this);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		bool L_1 = SystemInfo_get_supports3DTextures_m811(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}

IL_0015:
	{
		RenderTexture_t101 * L_2 = ___source;
		RenderTexture_t101 * L_3 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_001d:
	{
		Texture3D_t84 * L_4 = (__this->___converted3DLut_7);
		bool L_5 = Object_op_Equality_m640(NULL /*static, unused*/, L_4, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		ColorCorrectionLookup_SetIdentityLut_m241(__this, /*hidden argument*/NULL);
	}

IL_0034:
	{
		Texture3D_t84 * L_6 = (__this->___converted3DLut_7);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_6);
		V_0 = L_7;
		Texture3D_t84 * L_8 = (__this->___converted3DLut_7);
		NullCheck(L_8);
		Texture_set_wrapMode_m784(L_8, 1, /*hidden argument*/NULL);
		Material_t55 * L_9 = (__this->___material_6);
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		NullCheck(L_9);
		Material_SetFloat_m738(L_9, (String_t*) &_stringLiteral68, ((float)((float)(((float)((int32_t)((int32_t)L_10-(int32_t)1))))/(float)((float)((float)(1.0f)*(float)(((float)L_11)))))), /*hidden argument*/NULL);
		Material_t55 * L_12 = (__this->___material_6);
		int32_t L_13 = V_0;
		NullCheck(L_12);
		Material_SetFloat_m738(L_12, (String_t*) &_stringLiteral69, ((float)((float)(1.0f)/(float)((float)((float)(2.0f)*(float)(((float)L_13)))))), /*hidden argument*/NULL);
		Material_t55 * L_14 = (__this->___material_6);
		Texture3D_t84 * L_15 = (__this->___converted3DLut_7);
		NullCheck(L_14);
		Material_SetTexture_m759(L_14, (String_t*) &_stringLiteral70, L_15, /*hidden argument*/NULL);
		RenderTexture_t101 * L_16 = ___source;
		RenderTexture_t101 * L_17 = ___destination;
		Material_t55 * L_18 = (__this->___material_6);
		int32_t L_19 = QualitySettings_get_activeColorSpace_m818(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_18;
		G_B6_1 = L_17;
		G_B6_2 = L_16;
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			G_B7_0 = L_18;
			G_B7_1 = L_17;
			G_B7_2 = L_16;
			goto IL_00b6;
		}
	}
	{
		G_B8_0 = 1;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		goto IL_00b7;
	}

IL_00b6:
	{
		G_B8_0 = 0;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
	}

IL_00b7:
	{
		Graphics_Blit_m742(NULL /*static, unused*/, G_B8_3, G_B8_2, G_B8_1, G_B8_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.ColorCorrectionRamp
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_24.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ColorCorrectionRamp
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_24MethodDeclarations.h"

// UnityStandardAssets.ImageEffects.ImageEffectBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_25MethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::.ctor()
extern "C" void ColorCorrectionRamp__ctor_m245 (ColorCorrectionRamp_t87 * __this, const MethodInfo* method)
{
	{
		ImageEffectBase__ctor_m304(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ColorCorrectionRamp_OnRenderImage_m246 (ColorCorrectionRamp_t87 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = ImageEffectBase_get_material_m306(__this, /*hidden argument*/NULL);
		Texture_t86 * L_1 = (__this->___textureRamp_4);
		NullCheck(L_0);
		Material_SetTexture_m759(L_0, (String_t*) &_stringLiteral71, L_1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_2 = ___source;
		RenderTexture_t101 * L_3 = ___destination;
		Material_t55 * L_4 = ImageEffectBase_get_material_m306(__this, /*hidden argument*/NULL);
		Graphics_Blit_m739(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.ContrastEnhance
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_26.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ContrastEnhance
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_26MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::.ctor()
extern "C" void ContrastEnhance__ctor_m247 (ContrastEnhance_t89 * __this, const MethodInfo* method)
{
	{
		__this->___intensity_5 = (0.5f);
		__this->___blurSpread_9 = (1.0f);
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.ContrastEnhance::CheckResources()
extern "C" bool ContrastEnhance_CheckResources_m248 (ContrastEnhance_t89 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___contrastCompositeShader_11);
		Material_t55 * L_1 = (__this->___contrastCompositeMaterial_8);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___contrastCompositeMaterial_8 = L_2;
		Shader_t54 * L_3 = (__this->___separableBlurShader_10);
		Material_t55 * L_4 = (__this->___separableBlurMaterial_7);
		Material_t55 * L_5 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_3, L_4, /*hidden argument*/NULL);
		__this->___separableBlurMaterial_7 = L_5;
		bool L_6 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_6)
		{
			goto IL_0049;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0049:
	{
		bool L_7 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_7;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ContrastEnhance_OnRenderImage_m249 (ContrastEnhance_t89 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RenderTexture_t101 * V_2 = {0};
	RenderTexture_t101 * V_3 = {0};
	RenderTexture_t101 * V_4 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.ContrastEnhance::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		RenderTexture_t101 * L_3 = ___source;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_3);
		V_0 = L_4;
		RenderTexture_t101 * L_5 = ___source;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_5);
		V_1 = L_6;
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		RenderTexture_t101 * L_9 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_7/(int32_t)2)), ((int32_t)((int32_t)L_8/(int32_t)2)), 0, /*hidden argument*/NULL);
		V_2 = L_9;
		RenderTexture_t101 * L_10 = ___source;
		RenderTexture_t101 * L_11 = V_2;
		Graphics_Blit_m737(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		RenderTexture_t101 * L_14 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_12/(int32_t)4)), ((int32_t)((int32_t)L_13/(int32_t)4)), 0, /*hidden argument*/NULL);
		V_3 = L_14;
		RenderTexture_t101 * L_15 = V_2;
		RenderTexture_t101 * L_16 = V_3;
		Graphics_Blit_m737(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		RenderTexture_t101 * L_17 = V_2;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Material_t55 * L_18 = (__this->___separableBlurMaterial_7);
		float L_19 = (__this->___blurSpread_9);
		RenderTexture_t101 * L_20 = V_3;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_20);
		Vector4_t236  L_22 = {0};
		Vector4__ctor_m751(&L_22, (0.0f), ((float)((float)((float)((float)L_19*(float)(1.0f)))/(float)(((float)L_21)))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_18);
		Material_SetVector_m752(L_18, (String_t*) &_stringLiteral34, L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_0;
		int32_t L_24 = V_1;
		RenderTexture_t101 * L_25 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_23/(int32_t)4)), ((int32_t)((int32_t)L_24/(int32_t)4)), 0, /*hidden argument*/NULL);
		V_4 = L_25;
		RenderTexture_t101 * L_26 = V_3;
		RenderTexture_t101 * L_27 = V_4;
		Material_t55 * L_28 = (__this->___separableBlurMaterial_7);
		Graphics_Blit_m739(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		RenderTexture_t101 * L_29 = V_3;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		Material_t55 * L_30 = (__this->___separableBlurMaterial_7);
		float L_31 = (__this->___blurSpread_9);
		RenderTexture_t101 * L_32 = V_3;
		NullCheck(L_32);
		int32_t L_33 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_32);
		Vector4_t236  L_34 = {0};
		Vector4__ctor_m751(&L_34, ((float)((float)((float)((float)L_31*(float)(1.0f)))/(float)(((float)L_33)))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		Material_SetVector_m752(L_30, (String_t*) &_stringLiteral34, L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_0;
		int32_t L_36 = V_1;
		RenderTexture_t101 * L_37 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_35/(int32_t)4)), ((int32_t)((int32_t)L_36/(int32_t)4)), 0, /*hidden argument*/NULL);
		V_3 = L_37;
		RenderTexture_t101 * L_38 = V_4;
		RenderTexture_t101 * L_39 = V_3;
		Material_t55 * L_40 = (__this->___separableBlurMaterial_7);
		Graphics_Blit_m739(NULL /*static, unused*/, L_38, L_39, L_40, /*hidden argument*/NULL);
		RenderTexture_t101 * L_41 = V_4;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		Material_t55 * L_42 = (__this->___contrastCompositeMaterial_8);
		RenderTexture_t101 * L_43 = V_3;
		NullCheck(L_42);
		Material_SetTexture_m759(L_42, (String_t*) &_stringLiteral72, L_43, /*hidden argument*/NULL);
		Material_t55 * L_44 = (__this->___contrastCompositeMaterial_8);
		float L_45 = (__this->___intensity_5);
		NullCheck(L_44);
		Material_SetFloat_m738(L_44, (String_t*) &_stringLiteral73, L_45, /*hidden argument*/NULL);
		Material_t55 * L_46 = (__this->___contrastCompositeMaterial_8);
		float L_47 = (__this->___threshold_6);
		NullCheck(L_46);
		Material_SetFloat_m738(L_46, (String_t*) &_stringLiteral74, L_47, /*hidden argument*/NULL);
		RenderTexture_t101 * L_48 = ___source;
		RenderTexture_t101 * L_49 = ___destination;
		Material_t55 * L_50 = (__this->___contrastCompositeMaterial_8);
		Graphics_Blit_m739(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		RenderTexture_t101 * L_51 = V_3;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.ContrastStretch
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_27.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ContrastStretch
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_27MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::.ctor()
extern TypeInfo* RenderTextureU5BU5D_t90_il2cpp_TypeInfo_var;
extern "C" void ContrastStretch__ctor_m250 (ContrastStretch_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RenderTextureU5BU5D_t90_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___adaptationSpeed_2 = (0.02f);
		__this->___limitMinimum_3 = (0.2f);
		__this->___limitMaximum_4 = (0.6f);
		__this->___adaptRenderTex_5 = ((RenderTextureU5BU5D_t90*)SZArrayNew(RenderTextureU5BU5D_t90_il2cpp_TypeInfo_var, 2));
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialLum()
extern TypeInfo* Material_t55_il2cpp_TypeInfo_var;
extern "C" Material_t55 * ContrastStretch_get_materialLum_m251 (ContrastStretch_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t55 * L_0 = (__this->___m_materialLum_8);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t54 * L_2 = (__this->___shaderLum_7);
		Material_t55 * L_3 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m763(L_3, L_2, /*hidden argument*/NULL);
		__this->___m_materialLum_8 = L_3;
		Material_t55 * L_4 = (__this->___m_materialLum_8);
		NullCheck(L_4);
		Object_set_hideFlags_m764(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t55 * L_5 = (__this->___m_materialLum_8);
		return L_5;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialReduce()
extern TypeInfo* Material_t55_il2cpp_TypeInfo_var;
extern "C" Material_t55 * ContrastStretch_get_materialReduce_m252 (ContrastStretch_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t55 * L_0 = (__this->___m_materialReduce_10);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t54 * L_2 = (__this->___shaderReduce_9);
		Material_t55 * L_3 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m763(L_3, L_2, /*hidden argument*/NULL);
		__this->___m_materialReduce_10 = L_3;
		Material_t55 * L_4 = (__this->___m_materialReduce_10);
		NullCheck(L_4);
		Object_set_hideFlags_m764(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t55 * L_5 = (__this->___m_materialReduce_10);
		return L_5;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialAdapt()
extern TypeInfo* Material_t55_il2cpp_TypeInfo_var;
extern "C" Material_t55 * ContrastStretch_get_materialAdapt_m253 (ContrastStretch_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t55 * L_0 = (__this->___m_materialAdapt_12);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t54 * L_2 = (__this->___shaderAdapt_11);
		Material_t55 * L_3 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m763(L_3, L_2, /*hidden argument*/NULL);
		__this->___m_materialAdapt_12 = L_3;
		Material_t55 * L_4 = (__this->___m_materialAdapt_12);
		NullCheck(L_4);
		Object_set_hideFlags_m764(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t55 * L_5 = (__this->___m_materialAdapt_12);
		return L_5;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialApply()
extern TypeInfo* Material_t55_il2cpp_TypeInfo_var;
extern "C" Material_t55 * ContrastStretch_get_materialApply_m254 (ContrastStretch_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t55 * L_0 = (__this->___m_materialApply_14);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t54 * L_2 = (__this->___shaderApply_13);
		Material_t55 * L_3 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m763(L_3, L_2, /*hidden argument*/NULL);
		__this->___m_materialApply_14 = L_3;
		Material_t55 * L_4 = (__this->___m_materialApply_14);
		NullCheck(L_4);
		Object_set_hideFlags_m764(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t55 * L_5 = (__this->___m_materialApply_14);
		return L_5;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::Start()
extern "C" void ContrastStretch_Start_m255 (ContrastStretch_t91 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m765(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		Shader_t54 * L_1 = (__this->___shaderAdapt_11);
		NullCheck(L_1);
		bool L_2 = Shader_get_isSupported_m736(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0052;
		}
	}
	{
		Shader_t54 * L_3 = (__this->___shaderApply_13);
		NullCheck(L_3);
		bool L_4 = Shader_get_isSupported_m736(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0052;
		}
	}
	{
		Shader_t54 * L_5 = (__this->___shaderLum_7);
		NullCheck(L_5);
		bool L_6 = Shader_get_isSupported_m736(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		Shader_t54 * L_7 = (__this->___shaderReduce_9);
		NullCheck(L_7);
		bool L_8 = Shader_get_isSupported_m736(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_005a;
		}
	}

IL_0052:
	{
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_005a:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnEnable()
extern TypeInfo* RenderTexture_t101_il2cpp_TypeInfo_var;
extern "C" void ContrastStretch_OnEnable_m256 (ContrastStretch_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RenderTexture_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_003c;
	}

IL_0007:
	{
		RenderTextureU5BU5D_t90* L_0 = (__this->___adaptRenderTex_5);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		bool L_3 = Object_op_Implicit_m629(NULL /*static, unused*/, (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_0, L_2)), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		RenderTextureU5BU5D_t90* L_4 = (__this->___adaptRenderTex_5);
		int32_t L_5 = V_0;
		RenderTexture_t101 * L_6 = (RenderTexture_t101 *)il2cpp_codegen_object_new (RenderTexture_t101_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m819(L_6, 1, 1, 0, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_6);
		*((RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_4, L_5)) = (RenderTexture_t101 *)L_6;
		RenderTextureU5BU5D_t90* L_7 = (__this->___adaptRenderTex_5);
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		NullCheck((*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_7, L_9)));
		Object_set_hideFlags_m764((*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_7, L_9)), ((int32_t)61), /*hidden argument*/NULL);
	}

IL_0038:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) < ((int32_t)2)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnDisable()
extern "C" void ContrastStretch_OnDisable_m257 (ContrastStretch_t91 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0021;
	}

IL_0007:
	{
		RenderTextureU5BU5D_t90* L_0 = (__this->___adaptRenderTex_5);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Object_DestroyImmediate_m761(NULL /*static, unused*/, (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_0, L_2)), /*hidden argument*/NULL);
		RenderTextureU5BU5D_t90* L_3 = (__this->___adaptRenderTex_5);
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, NULL);
		*((RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_3, L_4)) = (RenderTexture_t101 *)NULL;
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)2)))
		{
			goto IL_0007;
		}
	}
	{
		Material_t55 * L_7 = (__this->___m_materialLum_8);
		bool L_8 = Object_op_Implicit_m629(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		Material_t55 * L_9 = (__this->___m_materialLum_8);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0043:
	{
		Material_t55 * L_10 = (__this->___m_materialReduce_10);
		bool L_11 = Object_op_Implicit_m629(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005e;
		}
	}
	{
		Material_t55 * L_12 = (__this->___m_materialReduce_10);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_005e:
	{
		Material_t55 * L_13 = (__this->___m_materialAdapt_12);
		bool L_14 = Object_op_Implicit_m629(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0079;
		}
	}
	{
		Material_t55 * L_15 = (__this->___m_materialAdapt_12);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0079:
	{
		Material_t55 * L_16 = (__this->___m_materialApply_14);
		bool L_17 = Object_op_Implicit_m629(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0094;
		}
	}
	{
		Material_t55 * L_18 = (__this->___m_materialApply_14);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ContrastStretch_OnRenderImage_m258 (ContrastStretch_t91 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	int32_t V_0 = 0;
	RenderTexture_t101 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	RenderTexture_t101 * V_6 = {0};
	{
		RenderTexture_t101 * L_0 = ___source;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_0);
		RenderTexture_t101 * L_2 = ___source;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_2);
		RenderTexture_t101 * L_4 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, ((int32_t)((int32_t)L_1/(int32_t)1)), ((int32_t)((int32_t)L_3/(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_4;
		RenderTexture_t101 * L_5 = ___source;
		RenderTexture_t101 * L_6 = V_1;
		Material_t55 * L_7 = ContrastStretch_get_materialLum_m251(__this, /*hidden argument*/NULL);
		Graphics_Blit_m739(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		goto IL_0074;
	}

IL_0028:
	{
		RenderTexture_t101 * L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_8);
		V_4 = ((int32_t)((int32_t)L_9/(int32_t)2));
		int32_t L_10 = V_4;
		if ((((int32_t)L_10) >= ((int32_t)1)))
		{
			goto IL_003d;
		}
	}
	{
		V_4 = 1;
	}

IL_003d:
	{
		RenderTexture_t101 * L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_11);
		V_5 = ((int32_t)((int32_t)L_12/(int32_t)2));
		int32_t L_13 = V_5;
		if ((((int32_t)L_13) >= ((int32_t)1)))
		{
			goto IL_0052;
		}
	}
	{
		V_5 = 1;
	}

IL_0052:
	{
		int32_t L_14 = V_4;
		int32_t L_15 = V_5;
		RenderTexture_t101 * L_16 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_6 = L_16;
		RenderTexture_t101 * L_17 = V_1;
		RenderTexture_t101 * L_18 = V_6;
		Material_t55 * L_19 = ContrastStretch_get_materialReduce_m252(__this, /*hidden argument*/NULL);
		Graphics_Blit_m739(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		RenderTexture_t101 * L_20 = V_1;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		RenderTexture_t101 * L_21 = V_6;
		V_1 = L_21;
	}

IL_0074:
	{
		RenderTexture_t101 * L_22 = V_1;
		NullCheck(L_22);
		int32_t L_23 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_22);
		if ((((int32_t)L_23) > ((int32_t)1)))
		{
			goto IL_0028;
		}
	}
	{
		RenderTexture_t101 * L_24 = V_1;
		NullCheck(L_24);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_24);
		if ((((int32_t)L_25) > ((int32_t)1)))
		{
			goto IL_0028;
		}
	}
	{
		RenderTexture_t101 * L_26 = V_1;
		ContrastStretch_CalculateAdaptation_m259(__this, L_26, /*hidden argument*/NULL);
		Material_t55 * L_27 = ContrastStretch_get_materialApply_m254(__this, /*hidden argument*/NULL);
		RenderTextureU5BU5D_t90* L_28 = (__this->___adaptRenderTex_5);
		int32_t L_29 = (__this->___curAdaptIndex_6);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		NullCheck(L_27);
		Material_SetTexture_m759(L_27, (String_t*) &_stringLiteral75, (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_28, L_30)), /*hidden argument*/NULL);
		RenderTexture_t101 * L_31 = ___source;
		RenderTexture_t101 * L_32 = ___destination;
		Material_t55 * L_33 = ContrastStretch_get_materialApply_m254(__this, /*hidden argument*/NULL);
		Graphics_Blit_m739(NULL /*static, unused*/, L_31, L_32, L_33, /*hidden argument*/NULL);
		RenderTexture_t101 * L_34 = V_1;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ContrastStretch::CalculateAdaptation(UnityEngine.Texture)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void ContrastStretch_CalculateAdaptation_m259 (ContrastStretch_t91 * __this, Texture_t86 * ___curTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		int32_t L_0 = (__this->___curAdaptIndex_6);
		V_0 = L_0;
		int32_t L_1 = (__this->___curAdaptIndex_6);
		__this->___curAdaptIndex_6 = ((int32_t)((int32_t)((int32_t)((int32_t)L_1+(int32_t)1))%(int32_t)2));
		float L_2 = (__this->___adaptationSpeed_2);
		float L_3 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_4 = powf(((float)((float)(1.0f)-(float)L_2)), ((float)((float)(30.0f)*(float)L_3)));
		V_1 = ((float)((float)(1.0f)-(float)L_4));
		float L_5 = V_1;
		float L_6 = Mathf_Clamp_m611(NULL /*static, unused*/, L_5, (0.01f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_6;
		Material_t55 * L_7 = ContrastStretch_get_materialAdapt_m253(__this, /*hidden argument*/NULL);
		Texture_t86 * L_8 = ___curTexture;
		NullCheck(L_7);
		Material_SetTexture_m759(L_7, (String_t*) &_stringLiteral76, L_8, /*hidden argument*/NULL);
		Material_t55 * L_9 = ContrastStretch_get_materialAdapt_m253(__this, /*hidden argument*/NULL);
		float L_10 = V_1;
		float L_11 = (__this->___limitMinimum_3);
		float L_12 = (__this->___limitMaximum_4);
		Vector4_t236  L_13 = {0};
		Vector4__ctor_m751(&L_13, L_10, L_11, L_12, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m752(L_9, (String_t*) &_stringLiteral77, L_13, /*hidden argument*/NULL);
		RenderTextureU5BU5D_t90* L_14 = (__this->___adaptRenderTex_5);
		int32_t L_15 = (__this->___curAdaptIndex_6);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Graphics_SetRenderTarget_m753(NULL /*static, unused*/, (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_14, L_16)), /*hidden argument*/NULL);
		Color_t65  L_17 = Color_get_black_m754(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m755(NULL /*static, unused*/, 0, 1, L_17, /*hidden argument*/NULL);
		RenderTextureU5BU5D_t90* L_18 = (__this->___adaptRenderTex_5);
		int32_t L_19 = V_0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		RenderTextureU5BU5D_t90* L_21 = (__this->___adaptRenderTex_5);
		int32_t L_22 = (__this->___curAdaptIndex_6);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		Material_t55 * L_24 = ContrastStretch_get_materialAdapt_m253(__this, /*hidden argument*/NULL);
		Graphics_Blit_m739(NULL /*static, unused*/, (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_18, L_20)), (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_21, L_23)), L_24, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.CreaseShading
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_28.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.CreaseShading
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_28MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.CreaseShading::.ctor()
extern "C" void CreaseShading__ctor_m260 (CreaseShading_t92 * __this, const MethodInfo* method)
{
	{
		__this->___intensity_5 = (0.5f);
		__this->___softness_6 = 1;
		__this->___spread_7 = (1.0f);
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.CreaseShading::CheckResources()
extern "C" bool CreaseShading_CheckResources_m261 (CreaseShading_t92 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 1, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___blurShader_8);
		Material_t55 * L_1 = (__this->___blurMaterial_9);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___blurMaterial_9 = L_2;
		Shader_t54 * L_3 = (__this->___depthFetchShader_10);
		Material_t55 * L_4 = (__this->___depthFetchMaterial_11);
		Material_t55 * L_5 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_3, L_4, /*hidden argument*/NULL);
		__this->___depthFetchMaterial_11 = L_5;
		Shader_t54 * L_6 = (__this->___creaseApplyShader_12);
		Material_t55 * L_7 = (__this->___creaseApplyMaterial_13);
		Material_t55 * L_8 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_6, L_7, /*hidden argument*/NULL);
		__this->___creaseApplyMaterial_13 = L_8;
		bool L_9 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_9)
		{
			goto IL_0061;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_10 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_10;
	}
}
// System.Void UnityStandardAssets.ImageEffects.CreaseShading::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CreaseShading_OnRenderImage_m262 (CreaseShading_t92 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	RenderTexture_t101 * V_4 = {0};
	RenderTexture_t101 * V_5 = {0};
	int32_t V_6 = 0;
	RenderTexture_t101 * V_7 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.CreaseShading::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		RenderTexture_t101 * L_3 = ___source;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_3);
		V_0 = L_4;
		RenderTexture_t101 * L_5 = ___source;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_5);
		V_1 = L_6;
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		V_2 = ((float)((float)((float)((float)(1.0f)*(float)(((float)L_7))))/(float)((float)((float)(1.0f)*(float)(((float)L_8))))));
		V_3 = (0.001953125f);
		int32_t L_9 = V_0;
		int32_t L_10 = V_1;
		RenderTexture_t101 * L_11 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, L_9, L_10, 0, /*hidden argument*/NULL);
		V_4 = L_11;
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		RenderTexture_t101 * L_14 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_12/(int32_t)2)), ((int32_t)((int32_t)L_13/(int32_t)2)), 0, /*hidden argument*/NULL);
		V_5 = L_14;
		RenderTexture_t101 * L_15 = ___source;
		RenderTexture_t101 * L_16 = V_4;
		Material_t55 * L_17 = (__this->___depthFetchMaterial_11);
		Graphics_Blit_m739(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		RenderTexture_t101 * L_18 = V_4;
		RenderTexture_t101 * L_19 = V_5;
		Graphics_Blit_m737(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_6 = 0;
		goto IL_0120;
	}

IL_0070:
	{
		int32_t L_20 = V_0;
		int32_t L_21 = V_1;
		RenderTexture_t101 * L_22 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_20/(int32_t)2)), ((int32_t)((int32_t)L_21/(int32_t)2)), 0, /*hidden argument*/NULL);
		V_7 = L_22;
		Material_t55 * L_23 = (__this->___blurMaterial_9);
		float L_24 = (__this->___spread_7);
		float L_25 = V_3;
		Vector4_t236  L_26 = {0};
		Vector4__ctor_m751(&L_26, (0.0f), ((float)((float)L_24*(float)L_25)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_23);
		Material_SetVector_m752(L_23, (String_t*) &_stringLiteral34, L_26, /*hidden argument*/NULL);
		RenderTexture_t101 * L_27 = V_5;
		RenderTexture_t101 * L_28 = V_7;
		Material_t55 * L_29 = (__this->___blurMaterial_9);
		Graphics_Blit_m739(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/NULL);
		RenderTexture_t101 * L_30 = V_5;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		RenderTexture_t101 * L_31 = V_7;
		V_5 = L_31;
		int32_t L_32 = V_0;
		int32_t L_33 = V_1;
		RenderTexture_t101 * L_34 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_32/(int32_t)2)), ((int32_t)((int32_t)L_33/(int32_t)2)), 0, /*hidden argument*/NULL);
		V_7 = L_34;
		Material_t55 * L_35 = (__this->___blurMaterial_9);
		float L_36 = (__this->___spread_7);
		float L_37 = V_3;
		float L_38 = V_2;
		Vector4_t236  L_39 = {0};
		Vector4__ctor_m751(&L_39, ((float)((float)((float)((float)L_36*(float)L_37))/(float)L_38)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_35);
		Material_SetVector_m752(L_35, (String_t*) &_stringLiteral34, L_39, /*hidden argument*/NULL);
		RenderTexture_t101 * L_40 = V_5;
		RenderTexture_t101 * L_41 = V_7;
		Material_t55 * L_42 = (__this->___blurMaterial_9);
		Graphics_Blit_m739(NULL /*static, unused*/, L_40, L_41, L_42, /*hidden argument*/NULL);
		RenderTexture_t101 * L_43 = V_5;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		RenderTexture_t101 * L_44 = V_7;
		V_5 = L_44;
		int32_t L_45 = V_6;
		V_6 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_0120:
	{
		int32_t L_46 = V_6;
		int32_t L_47 = (__this->___softness_6);
		if ((((int32_t)L_46) < ((int32_t)L_47)))
		{
			goto IL_0070;
		}
	}
	{
		Material_t55 * L_48 = (__this->___creaseApplyMaterial_13);
		RenderTexture_t101 * L_49 = V_4;
		NullCheck(L_48);
		Material_SetTexture_m759(L_48, (String_t*) &_stringLiteral78, L_49, /*hidden argument*/NULL);
		Material_t55 * L_50 = (__this->___creaseApplyMaterial_13);
		RenderTexture_t101 * L_51 = V_5;
		NullCheck(L_50);
		Material_SetTexture_m759(L_50, (String_t*) &_stringLiteral79, L_51, /*hidden argument*/NULL);
		Material_t55 * L_52 = (__this->___creaseApplyMaterial_13);
		float L_53 = (__this->___intensity_5);
		NullCheck(L_52);
		Material_SetFloat_m738(L_52, (String_t*) &_stringLiteral73, L_53, /*hidden argument*/NULL);
		RenderTexture_t101 * L_54 = ___source;
		RenderTexture_t101 * L_55 = ___destination;
		Material_t55 * L_56 = (__this->___creaseApplyMaterial_13);
		Graphics_Blit_m739(NULL /*static, unused*/, L_54, L_55, L_56, /*hidden argument*/NULL);
		RenderTexture_t101 * L_57 = V_4;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		RenderTexture_t101 * L_58 = V_5;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.DepthOfField/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_29.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.DepthOfField/BlurType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_29MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.DepthOfField/BlurSampleCount
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_30.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.DepthOfField/BlurSampleCount
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_30MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.DepthOfField
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_31.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.DepthOfField
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_31MethodDeclarations.h"

// UnityEngine.ComputeBuffer
#include "UnityEngine_UnityEngine_ComputeBuffer.h"
// UnityEngine.ComputeBufferType
#include "UnityEngine_UnityEngine_ComputeBufferType.h"
// UnityEngine.MeshTopology
#include "UnityEngine_UnityEngine_MeshTopology.h"
// UnityEngine.ComputeBuffer
#include "UnityEngine_UnityEngine_ComputeBufferMethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.DepthOfField::.ctor()
extern "C" void DepthOfField__ctor_m263 (DepthOfField_t96 * __this, const MethodInfo* method)
{
	{
		__this->___focalLength_6 = (10.0f);
		__this->___focalSize_7 = (0.05f);
		__this->___aperture_8 = (11.5f);
		__this->___maxBlurSize_10 = (2.0f);
		__this->___blurSampleCount_13 = 2;
		__this->___foregroundOverlap_15 = (1.0f);
		__this->___dx11BokehThreshold_20 = (0.5f);
		__this->___dx11SpawnHeuristic_21 = (0.0875f);
		__this->___dx11BokehScale_23 = (1.2f);
		__this->___dx11BokehIntensity_24 = (2.5f);
		__this->___focalDistance01_25 = (10.0f);
		__this->___internalBlurWidth_28 = (1.0f);
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::CheckResources()
extern "C" bool DepthOfField_CheckResources_m264 (DepthOfField_t96 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 1, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___dofHdrShader_16);
		Material_t55 * L_1 = (__this->___dofHdrMaterial_17);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___dofHdrMaterial_17 = L_2;
		bool L_3 = (((PostEffectsBase_t57 *)__this)->___supportDX11_3);
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_4 = (__this->___blurType_12);
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0055;
		}
	}
	{
		Shader_t54 * L_5 = (__this->___dx11BokehShader_18);
		Material_t55 * L_6 = (__this->___dx11bokehMaterial_19);
		Material_t55 * L_7 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_5, L_6, /*hidden argument*/NULL);
		__this->___dx11bokehMaterial_19 = L_7;
		DepthOfField_CreateComputeResources_m268(__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		bool L_8 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_8)
		{
			goto IL_0066;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0066:
	{
		bool L_9 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_9;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnEnable()
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void DepthOfField_OnEnable_m265 (DepthOfField_t96 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t27 * L_0 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		Camera_t27 * L_1 = L_0;
		NullCheck(L_1);
		int32_t L_2 = Camera_get_depthTextureMode_m779(L_1, /*hidden argument*/NULL);
		NullCheck(L_1);
		Camera_set_depthTextureMode_m780(L_1, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnDisable()
extern "C" void DepthOfField_OnDisable_m266 (DepthOfField_t96 * __this, const MethodInfo* method)
{
	{
		DepthOfField_ReleaseComputeResources_m267(__this, /*hidden argument*/NULL);
		Material_t55 * L_0 = (__this->___dofHdrMaterial_17);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Material_t55 * L_2 = (__this->___dofHdrMaterial_17);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		__this->___dofHdrMaterial_17 = (Material_t55 *)NULL;
		Material_t55 * L_3 = (__this->___dx11bokehMaterial_19);
		bool L_4 = Object_op_Implicit_m629(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0043;
		}
	}
	{
		Material_t55 * L_5 = (__this->___dx11bokehMaterial_19);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0043:
	{
		__this->___dx11bokehMaterial_19 = (Material_t55 *)NULL;
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::ReleaseComputeResources()
extern "C" void DepthOfField_ReleaseComputeResources_m267 (DepthOfField_t96 * __this, const MethodInfo* method)
{
	{
		ComputeBuffer_t95 * L_0 = (__this->___cbDrawArgs_26);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ComputeBuffer_t95 * L_1 = (__this->___cbDrawArgs_26);
		NullCheck(L_1);
		ComputeBuffer_Release_m820(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		__this->___cbDrawArgs_26 = (ComputeBuffer_t95 *)NULL;
		ComputeBuffer_t95 * L_2 = (__this->___cbPoints_27);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		ComputeBuffer_t95 * L_3 = (__this->___cbPoints_27);
		NullCheck(L_3);
		ComputeBuffer_Release_m820(L_3, /*hidden argument*/NULL);
	}

IL_0033:
	{
		__this->___cbPoints_27 = (ComputeBuffer_t95 *)NULL;
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::CreateComputeResources()
extern TypeInfo* ComputeBuffer_t95_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t242_il2cpp_TypeInfo_var;
extern "C" void DepthOfField_CreateComputeResources_m268 (DepthOfField_t96 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComputeBuffer_t95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(52);
		Int32U5BU5D_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t242* V_0 = {0};
	{
		ComputeBuffer_t95 * L_0 = (__this->___cbDrawArgs_26);
		if (L_0)
		{
			goto IL_0041;
		}
	}
	{
		ComputeBuffer_t95 * L_1 = (ComputeBuffer_t95 *)il2cpp_codegen_object_new (ComputeBuffer_t95_il2cpp_TypeInfo_var);
		ComputeBuffer__ctor_m821(L_1, 1, ((int32_t)16), ((int32_t)256), /*hidden argument*/NULL);
		__this->___cbDrawArgs_26 = L_1;
		V_0 = ((Int32U5BU5D_t242*)SZArrayNew(Int32U5BU5D_t242_il2cpp_TypeInfo_var, 4));
		Int32U5BU5D_t242* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_2, 0)) = (int32_t)0;
		Int32U5BU5D_t242* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_3, 1)) = (int32_t)1;
		Int32U5BU5D_t242* L_4 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_4, 2)) = (int32_t)0;
		Int32U5BU5D_t242* L_5 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_5, 3)) = (int32_t)0;
		ComputeBuffer_t95 * L_6 = (__this->___cbDrawArgs_26);
		Int32U5BU5D_t242* L_7 = V_0;
		NullCheck(L_6);
		ComputeBuffer_SetData_m822(L_6, (Array_t *)(Array_t *)L_7, /*hidden argument*/NULL);
	}

IL_0041:
	{
		ComputeBuffer_t95 * L_8 = (__this->___cbPoints_27);
		if (L_8)
		{
			goto IL_005f;
		}
	}
	{
		ComputeBuffer_t95 * L_9 = (ComputeBuffer_t95 *)il2cpp_codegen_object_new (ComputeBuffer_t95_il2cpp_TypeInfo_var);
		ComputeBuffer__ctor_m821(L_9, ((int32_t)90000), ((int32_t)28), 2, /*hidden argument*/NULL);
		__this->___cbPoints_27 = L_9;
	}

IL_005f:
	{
		return;
	}
}
// System.Single UnityStandardAssets.ImageEffects.DepthOfField::FocalDistance01(System.Single)
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" float DepthOfField_FocalDistance01_m269 (DepthOfField_t96 * __this, float ___worldDist, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4  V_0 = {0};
	{
		Camera_t27 * L_0 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		float L_1 = ___worldDist;
		Camera_t27 * L_2 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_2);
		float L_3 = Camera_get_nearClipPlane_m823(L_2, /*hidden argument*/NULL);
		Camera_t27 * L_4 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_4);
		Transform_t1 * L_5 = Component_get_transform_m594(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4  L_6 = Transform_get_forward_m643(L_5, /*hidden argument*/NULL);
		Vector3_t4  L_7 = Vector3_op_Multiply_m598(NULL /*static, unused*/, ((float)((float)L_1-(float)L_3)), L_6, /*hidden argument*/NULL);
		Camera_t27 * L_8 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_8);
		Transform_t1 * L_9 = Component_get_transform_m594(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t4  L_10 = Transform_get_position_m593(L_9, /*hidden argument*/NULL);
		Vector3_t4  L_11 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_12 = Camera_WorldToViewportPoint_m824(L_0, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		float L_13 = ((&V_0)->___z_3);
		Camera_t27 * L_14 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_14);
		float L_15 = Camera_get_farClipPlane_m825(L_14, /*hidden argument*/NULL);
		Camera_t27 * L_16 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_16);
		float L_17 = Camera_get_nearClipPlane_m823(L_16, /*hidden argument*/NULL);
		return ((float)((float)L_13/(float)((float)((float)L_15-(float)L_17))));
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::WriteCoc(UnityEngine.RenderTexture,System.Boolean)
extern "C" void DepthOfField_WriteCoc_m270 (DepthOfField_t96 * __this, RenderTexture_t101 * ___fromTo, bool ___fgDilate, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RenderTexture_t101 * V_2 = {0};
	float V_3 = 0.0f;
	RenderTexture_t101 * V_4 = {0};
	{
		Material_t55 * L_0 = (__this->___dofHdrMaterial_17);
		NullCheck(L_0);
		Material_SetTexture_m759(L_0, (String_t*) &_stringLiteral80, (Texture_t86 *)NULL, /*hidden argument*/NULL);
		bool L_1 = (__this->___nearBlur_14);
		if (!L_1)
		{
			goto IL_011c;
		}
	}
	{
		bool L_2 = ___fgDilate;
		if (!L_2)
		{
			goto IL_011c;
		}
	}
	{
		RenderTexture_t101 * L_3 = ___fromTo;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_3);
		V_0 = ((int32_t)((int32_t)L_4/(int32_t)2));
		RenderTexture_t101 * L_5 = ___fromTo;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_5);
		V_1 = ((int32_t)((int32_t)L_6/(int32_t)2));
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		RenderTexture_t101 * L_9 = ___fromTo;
		NullCheck(L_9);
		int32_t L_10 = RenderTexture_get_format_m747(L_9, /*hidden argument*/NULL);
		RenderTexture_t101 * L_11 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_7, L_8, 0, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		RenderTexture_t101 * L_12 = ___fromTo;
		RenderTexture_t101 * L_13 = V_2;
		Material_t55 * L_14 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_12, L_13, L_14, 4, /*hidden argument*/NULL);
		float L_15 = (__this->___internalBlurWidth_28);
		float L_16 = (__this->___foregroundOverlap_15);
		V_3 = ((float)((float)L_15*(float)L_16));
		Material_t55 * L_17 = (__this->___dofHdrMaterial_17);
		float L_18 = V_3;
		float L_19 = V_3;
		Vector4_t236  L_20 = {0};
		Vector4__ctor_m751(&L_20, (0.0f), L_18, (0.0f), L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		Material_SetVector_m752(L_17, (String_t*) &_stringLiteral23, L_20, /*hidden argument*/NULL);
		int32_t L_21 = V_0;
		int32_t L_22 = V_1;
		RenderTexture_t101 * L_23 = ___fromTo;
		NullCheck(L_23);
		int32_t L_24 = RenderTexture_get_format_m747(L_23, /*hidden argument*/NULL);
		RenderTexture_t101 * L_25 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_21, L_22, 0, L_24, /*hidden argument*/NULL);
		V_4 = L_25;
		RenderTexture_t101 * L_26 = V_2;
		RenderTexture_t101 * L_27 = V_4;
		Material_t55 * L_28 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_26, L_27, L_28, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_29 = V_2;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		Material_t55 * L_30 = (__this->___dofHdrMaterial_17);
		float L_31 = V_3;
		float L_32 = V_3;
		Vector4_t236  L_33 = {0};
		Vector4__ctor_m751(&L_33, L_31, (0.0f), (0.0f), L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		Material_SetVector_m752(L_30, (String_t*) &_stringLiteral23, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_0;
		int32_t L_35 = V_1;
		RenderTexture_t101 * L_36 = ___fromTo;
		NullCheck(L_36);
		int32_t L_37 = RenderTexture_get_format_m747(L_36, /*hidden argument*/NULL);
		RenderTexture_t101 * L_38 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_34, L_35, 0, L_37, /*hidden argument*/NULL);
		V_2 = L_38;
		RenderTexture_t101 * L_39 = V_4;
		RenderTexture_t101 * L_40 = V_2;
		Material_t55 * L_41 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_39, L_40, L_41, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_42 = V_4;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		Material_t55 * L_43 = (__this->___dofHdrMaterial_17);
		RenderTexture_t101 * L_44 = V_2;
		NullCheck(L_43);
		Material_SetTexture_m759(L_43, (String_t*) &_stringLiteral80, L_44, /*hidden argument*/NULL);
		RenderTexture_t101 * L_45 = ___fromTo;
		NullCheck(L_45);
		RenderTexture_MarkRestoreExpected_m756(L_45, /*hidden argument*/NULL);
		RenderTexture_t101 * L_46 = ___fromTo;
		RenderTexture_t101 * L_47 = ___fromTo;
		Material_t55 * L_48 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_46, L_47, L_48, ((int32_t)13), /*hidden argument*/NULL);
		RenderTexture_t101 * L_49 = V_2;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		goto IL_0130;
	}

IL_011c:
	{
		RenderTexture_t101 * L_50 = ___fromTo;
		NullCheck(L_50);
		RenderTexture_MarkRestoreExpected_m756(L_50, /*hidden argument*/NULL);
		RenderTexture_t101 * L_51 = ___fromTo;
		RenderTexture_t101 * L_52 = ___fromTo;
		Material_t55 * L_53 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_51, L_52, L_53, 0, /*hidden argument*/NULL);
	}

IL_0130:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void DepthOfField_OnRenderImage_m271 (DepthOfField_t96 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t101 * V_0 = {0};
	RenderTexture_t101 * V_1 = {0};
	RenderTexture_t101 * V_2 = {0};
	RenderTexture_t101 * V_3 = {0};
	float V_4 = 0.0f;
	RenderTexture_t101 * V_5 = {0};
	RenderTexture_t101 * V_6 = {0};
	int32_t V_7 = 0;
	Vector3_t4  V_8 = {0};
	DepthOfField_t96 * G_B8_0 = {0};
	DepthOfField_t96 * G_B7_0 = {0};
	float G_B9_0 = 0.0f;
	DepthOfField_t96 * G_B9_1 = {0};
	DepthOfField_t96 * G_B16_0 = {0};
	DepthOfField_t96 * G_B15_0 = {0};
	float G_B17_0 = 0.0f;
	DepthOfField_t96 * G_B17_1 = {0};
	String_t* G_B21_0 = {0};
	Material_t55 * G_B21_1 = {0};
	String_t* G_B20_0 = {0};
	Material_t55 * G_B20_1 = {0};
	RenderTexture_t101 * G_B22_0 = {0};
	String_t* G_B22_1 = {0};
	Material_t55 * G_B22_2 = {0};
	int32_t G_B38_0 = 0;
	Material_t55 * G_B42_0 = {0};
	RenderTexture_t101 * G_B42_1 = {0};
	RenderTexture_t101 * G_B42_2 = {0};
	Material_t55 * G_B41_0 = {0};
	RenderTexture_t101 * G_B41_1 = {0};
	RenderTexture_t101 * G_B41_2 = {0};
	int32_t G_B43_0 = 0;
	Material_t55 * G_B43_1 = {0};
	RenderTexture_t101 * G_B43_2 = {0};
	RenderTexture_t101 * G_B43_3 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		float L_3 = (__this->___aperture_8);
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_002e;
		}
	}
	{
		__this->___aperture_8 = (0.0f);
	}

IL_002e:
	{
		float L_4 = (__this->___maxBlurSize_10);
		if ((!(((float)L_4) < ((float)(0.1f)))))
		{
			goto IL_0049;
		}
	}
	{
		__this->___maxBlurSize_10 = (0.1f);
	}

IL_0049:
	{
		float L_5 = (__this->___focalSize_7);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp_m611(NULL /*static, unused*/, L_5, (0.0f), (2.0f), /*hidden argument*/NULL);
		__this->___focalSize_7 = L_6;
		float L_7 = (__this->___maxBlurSize_10);
		float L_8 = Mathf_Max_m782(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		__this->___internalBlurWidth_28 = L_8;
		Transform_t1 * L_9 = (__this->___focalTransform_9);
		bool L_10 = Object_op_Implicit_m629(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		G_B7_0 = __this;
		if (!L_10)
		{
			G_B8_0 = __this;
			goto IL_00bb;
		}
	}
	{
		Camera_t27 * L_11 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		Transform_t1 * L_12 = (__this->___focalTransform_9);
		NullCheck(L_12);
		Vector3_t4  L_13 = Transform_get_position_m593(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t4  L_14 = Camera_WorldToViewportPoint_m824(L_11, L_13, /*hidden argument*/NULL);
		V_8 = L_14;
		float L_15 = ((&V_8)->___z_3);
		Camera_t27 * L_16 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_16);
		float L_17 = Camera_get_farClipPlane_m825(L_16, /*hidden argument*/NULL);
		G_B9_0 = ((float)((float)L_15/(float)L_17));
		G_B9_1 = G_B7_0;
		goto IL_00c7;
	}

IL_00bb:
	{
		float L_18 = (__this->___focalLength_6);
		float L_19 = DepthOfField_FocalDistance01_m269(__this, L_18, /*hidden argument*/NULL);
		G_B9_0 = L_19;
		G_B9_1 = G_B8_0;
	}

IL_00c7:
	{
		NullCheck(G_B9_1);
		G_B9_1->___focalDistance01_25 = G_B9_0;
		Material_t55 * L_20 = (__this->___dofHdrMaterial_17);
		float L_21 = (__this->___focalSize_7);
		float L_22 = (__this->___aperture_8);
		float L_23 = (__this->___focalDistance01_25);
		Vector4_t236  L_24 = {0};
		Vector4__ctor_m751(&L_24, (1.0f), L_21, ((float)((float)L_22/(float)(10.0f))), L_23, /*hidden argument*/NULL);
		NullCheck(L_20);
		Material_SetVector_m752(L_20, (String_t*) &_stringLiteral81, L_24, /*hidden argument*/NULL);
		V_0 = (RenderTexture_t101 *)NULL;
		V_1 = (RenderTexture_t101 *)NULL;
		V_2 = (RenderTexture_t101 *)NULL;
		V_3 = (RenderTexture_t101 *)NULL;
		float L_25 = (__this->___internalBlurWidth_28);
		float L_26 = (__this->___foregroundOverlap_15);
		V_4 = ((float)((float)L_25*(float)L_26));
		bool L_27 = (__this->___visualizeFocus_5);
		if (!L_27)
		{
			goto IL_013c;
		}
	}
	{
		RenderTexture_t101 * L_28 = ___source;
		DepthOfField_WriteCoc_m270(__this, L_28, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_29 = ___source;
		RenderTexture_t101 * L_30 = ___destination;
		Material_t55 * L_31 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_29, L_30, L_31, ((int32_t)16), /*hidden argument*/NULL);
		goto IL_0ac0;
	}

IL_013c:
	{
		int32_t L_32 = (__this->___blurType_12);
		if ((!(((uint32_t)L_32) == ((uint32_t)1))))
		{
			goto IL_0921;
		}
	}
	{
		Material_t55 * L_33 = (__this->___dx11bokehMaterial_19);
		bool L_34 = Object_op_Implicit_m629(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_0921;
		}
	}
	{
		bool L_35 = (__this->___highResolution_11);
		if (!L_35)
		{
			goto IL_050d;
		}
	}
	{
		float L_36 = (__this->___internalBlurWidth_28);
		G_B15_0 = __this;
		if ((!(((float)L_36) < ((float)(0.1f)))))
		{
			G_B16_0 = __this;
			goto IL_017e;
		}
	}
	{
		G_B17_0 = (0.1f);
		G_B17_1 = G_B15_0;
		goto IL_0184;
	}

IL_017e:
	{
		float L_37 = (__this->___internalBlurWidth_28);
		G_B17_0 = L_37;
		G_B17_1 = G_B16_0;
	}

IL_0184:
	{
		NullCheck(G_B17_1);
		G_B17_1->___internalBlurWidth_28 = G_B17_0;
		float L_38 = (__this->___internalBlurWidth_28);
		float L_39 = (__this->___foregroundOverlap_15);
		V_4 = ((float)((float)L_38*(float)L_39));
		RenderTexture_t101 * L_40 = ___source;
		NullCheck(L_40);
		int32_t L_41 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_40);
		RenderTexture_t101 * L_42 = ___source;
		NullCheck(L_42);
		int32_t L_43 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_42);
		RenderTexture_t101 * L_44 = ___source;
		NullCheck(L_44);
		int32_t L_45 = RenderTexture_get_format_m747(L_44, /*hidden argument*/NULL);
		RenderTexture_t101 * L_46 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_41, L_43, 0, L_45, /*hidden argument*/NULL);
		V_0 = L_46;
		RenderTexture_t101 * L_47 = ___source;
		NullCheck(L_47);
		int32_t L_48 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_47);
		RenderTexture_t101 * L_49 = ___source;
		NullCheck(L_49);
		int32_t L_50 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_49);
		RenderTexture_t101 * L_51 = ___source;
		NullCheck(L_51);
		int32_t L_52 = RenderTexture_get_format_m747(L_51, /*hidden argument*/NULL);
		RenderTexture_t101 * L_53 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_48, L_50, 0, L_52, /*hidden argument*/NULL);
		V_5 = L_53;
		RenderTexture_t101 * L_54 = ___source;
		DepthOfField_WriteCoc_m270(__this, L_54, 0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_55 = ___source;
		NullCheck(L_55);
		int32_t L_56 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_55);
		RenderTexture_t101 * L_57 = ___source;
		NullCheck(L_57);
		int32_t L_58 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_57);
		RenderTexture_t101 * L_59 = ___source;
		NullCheck(L_59);
		int32_t L_60 = RenderTexture_get_format_m747(L_59, /*hidden argument*/NULL);
		RenderTexture_t101 * L_61 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_56>>(int32_t)1)), ((int32_t)((int32_t)L_58>>(int32_t)1)), 0, L_60, /*hidden argument*/NULL);
		V_2 = L_61;
		RenderTexture_t101 * L_62 = ___source;
		NullCheck(L_62);
		int32_t L_63 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_62);
		RenderTexture_t101 * L_64 = ___source;
		NullCheck(L_64);
		int32_t L_65 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_64);
		RenderTexture_t101 * L_66 = ___source;
		NullCheck(L_66);
		int32_t L_67 = RenderTexture_get_format_m747(L_66, /*hidden argument*/NULL);
		RenderTexture_t101 * L_68 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_63>>(int32_t)1)), ((int32_t)((int32_t)L_65>>(int32_t)1)), 0, L_67, /*hidden argument*/NULL);
		V_3 = L_68;
		RenderTexture_t101 * L_69 = ___source;
		RenderTexture_t101 * L_70 = V_2;
		Material_t55 * L_71 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_69, L_70, L_71, ((int32_t)15), /*hidden argument*/NULL);
		Material_t55 * L_72 = (__this->___dofHdrMaterial_17);
		Vector4_t236  L_73 = {0};
		Vector4__ctor_m751(&L_73, (0.0f), (1.5f), (0.0f), (1.5f), /*hidden argument*/NULL);
		NullCheck(L_72);
		Material_SetVector_m752(L_72, (String_t*) &_stringLiteral23, L_73, /*hidden argument*/NULL);
		RenderTexture_t101 * L_74 = V_2;
		RenderTexture_t101 * L_75 = V_3;
		Material_t55 * L_76 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_74, L_75, L_76, ((int32_t)19), /*hidden argument*/NULL);
		Material_t55 * L_77 = (__this->___dofHdrMaterial_17);
		Vector4_t236  L_78 = {0};
		Vector4__ctor_m751(&L_78, (1.5f), (0.0f), (0.0f), (1.5f), /*hidden argument*/NULL);
		NullCheck(L_77);
		Material_SetVector_m752(L_77, (String_t*) &_stringLiteral23, L_78, /*hidden argument*/NULL);
		RenderTexture_t101 * L_79 = V_3;
		RenderTexture_t101 * L_80 = V_2;
		Material_t55 * L_81 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_79, L_80, L_81, ((int32_t)19), /*hidden argument*/NULL);
		bool L_82 = (__this->___nearBlur_14);
		if (!L_82)
		{
			goto IL_02a5;
		}
	}
	{
		RenderTexture_t101 * L_83 = ___source;
		RenderTexture_t101 * L_84 = V_3;
		Material_t55 * L_85 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_83, L_84, L_85, 4, /*hidden argument*/NULL);
	}

IL_02a5:
	{
		Material_t55 * L_86 = (__this->___dx11bokehMaterial_19);
		RenderTexture_t101 * L_87 = V_2;
		NullCheck(L_86);
		Material_SetTexture_m759(L_86, (String_t*) &_stringLiteral82, L_87, /*hidden argument*/NULL);
		Material_t55 * L_88 = (__this->___dx11bokehMaterial_19);
		float L_89 = (__this->___dx11SpawnHeuristic_21);
		NullCheck(L_88);
		Material_SetFloat_m738(L_88, (String_t*) &_stringLiteral83, L_89, /*hidden argument*/NULL);
		Material_t55 * L_90 = (__this->___dx11bokehMaterial_19);
		float L_91 = (__this->___dx11BokehScale_23);
		float L_92 = (__this->___dx11BokehIntensity_24);
		float L_93 = (__this->___dx11BokehThreshold_20);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_94 = Mathf_Clamp_m611(NULL /*static, unused*/, L_93, (0.005f), (4.0f), /*hidden argument*/NULL);
		float L_95 = (__this->___internalBlurWidth_28);
		Vector4_t236  L_96 = {0};
		Vector4__ctor_m751(&L_96, L_91, L_92, L_94, L_95, /*hidden argument*/NULL);
		NullCheck(L_90);
		Material_SetVector_m752(L_90, (String_t*) &_stringLiteral84, L_96, /*hidden argument*/NULL);
		Material_t55 * L_97 = (__this->___dx11bokehMaterial_19);
		bool L_98 = (__this->___nearBlur_14);
		G_B20_0 = (String_t*) &_stringLiteral85;
		G_B20_1 = L_97;
		if (!L_98)
		{
			G_B21_0 = (String_t*) &_stringLiteral85;
			G_B21_1 = L_97;
			goto IL_0324;
		}
	}
	{
		RenderTexture_t101 * L_99 = V_3;
		G_B22_0 = L_99;
		G_B22_1 = G_B20_0;
		G_B22_2 = G_B20_1;
		goto IL_0325;
	}

IL_0324:
	{
		G_B22_0 = ((RenderTexture_t101 *)(NULL));
		G_B22_1 = G_B21_0;
		G_B22_2 = G_B21_1;
	}

IL_0325:
	{
		NullCheck(G_B22_2);
		Material_SetTexture_m759(G_B22_2, G_B22_1, G_B22_0, /*hidden argument*/NULL);
		ComputeBuffer_t95 * L_100 = (__this->___cbPoints_27);
		Graphics_SetRandomWriteTarget_m826(NULL /*static, unused*/, 1, L_100, /*hidden argument*/NULL);
		RenderTexture_t101 * L_101 = ___source;
		RenderTexture_t101 * L_102 = V_0;
		Material_t55 * L_103 = (__this->___dx11bokehMaterial_19);
		Graphics_Blit_m742(NULL /*static, unused*/, L_101, L_102, L_103, 0, /*hidden argument*/NULL);
		Graphics_ClearRandomWriteTargets_m827(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_104 = (__this->___nearBlur_14);
		if (!L_104)
		{
			goto IL_03c4;
		}
	}
	{
		Material_t55 * L_105 = (__this->___dofHdrMaterial_17);
		float L_106 = V_4;
		float L_107 = V_4;
		Vector4_t236  L_108 = {0};
		Vector4__ctor_m751(&L_108, (0.0f), L_106, (0.0f), L_107, /*hidden argument*/NULL);
		NullCheck(L_105);
		Material_SetVector_m752(L_105, (String_t*) &_stringLiteral23, L_108, /*hidden argument*/NULL);
		RenderTexture_t101 * L_109 = V_3;
		RenderTexture_t101 * L_110 = V_2;
		Material_t55 * L_111 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_109, L_110, L_111, 2, /*hidden argument*/NULL);
		Material_t55 * L_112 = (__this->___dofHdrMaterial_17);
		float L_113 = V_4;
		float L_114 = V_4;
		Vector4_t236  L_115 = {0};
		Vector4__ctor_m751(&L_115, L_113, (0.0f), (0.0f), L_114, /*hidden argument*/NULL);
		NullCheck(L_112);
		Material_SetVector_m752(L_112, (String_t*) &_stringLiteral23, L_115, /*hidden argument*/NULL);
		RenderTexture_t101 * L_116 = V_2;
		RenderTexture_t101 * L_117 = V_3;
		Material_t55 * L_118 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_116, L_117, L_118, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_119 = V_3;
		RenderTexture_t101 * L_120 = V_0;
		Material_t55 * L_121 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_119, L_120, L_121, 3, /*hidden argument*/NULL);
	}

IL_03c4:
	{
		RenderTexture_t101 * L_122 = V_0;
		RenderTexture_t101 * L_123 = V_5;
		Material_t55 * L_124 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_122, L_123, L_124, ((int32_t)20), /*hidden argument*/NULL);
		Material_t55 * L_125 = (__this->___dofHdrMaterial_17);
		float L_126 = (__this->___internalBlurWidth_28);
		float L_127 = (__this->___internalBlurWidth_28);
		Vector4_t236  L_128 = {0};
		Vector4__ctor_m751(&L_128, L_126, (0.0f), (0.0f), L_127, /*hidden argument*/NULL);
		NullCheck(L_125);
		Material_SetVector_m752(L_125, (String_t*) &_stringLiteral23, L_128, /*hidden argument*/NULL);
		RenderTexture_t101 * L_129 = V_0;
		RenderTexture_t101 * L_130 = ___source;
		Material_t55 * L_131 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_129, L_130, L_131, 5, /*hidden argument*/NULL);
		Material_t55 * L_132 = (__this->___dofHdrMaterial_17);
		float L_133 = (__this->___internalBlurWidth_28);
		float L_134 = (__this->___internalBlurWidth_28);
		Vector4_t236  L_135 = {0};
		Vector4__ctor_m751(&L_135, (0.0f), L_133, (0.0f), L_134, /*hidden argument*/NULL);
		NullCheck(L_132);
		Material_SetVector_m752(L_132, (String_t*) &_stringLiteral23, L_135, /*hidden argument*/NULL);
		RenderTexture_t101 * L_136 = ___source;
		RenderTexture_t101 * L_137 = V_5;
		Material_t55 * L_138 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_136, L_137, L_138, ((int32_t)21), /*hidden argument*/NULL);
		RenderTexture_t101 * L_139 = V_5;
		Graphics_SetRenderTarget_m753(NULL /*static, unused*/, L_139, /*hidden argument*/NULL);
		ComputeBuffer_t95 * L_140 = (__this->___cbPoints_27);
		ComputeBuffer_t95 * L_141 = (__this->___cbDrawArgs_26);
		ComputeBuffer_CopyCount_m828(NULL /*static, unused*/, L_140, L_141, 0, /*hidden argument*/NULL);
		Material_t55 * L_142 = (__this->___dx11bokehMaterial_19);
		ComputeBuffer_t95 * L_143 = (__this->___cbPoints_27);
		NullCheck(L_142);
		Material_SetBuffer_m829(L_142, (String_t*) &_stringLiteral86, L_143, /*hidden argument*/NULL);
		Material_t55 * L_144 = (__this->___dx11bokehMaterial_19);
		Texture2D_t63 * L_145 = (__this->___dx11BokehTexture_22);
		NullCheck(L_144);
		Material_SetTexture_m759(L_144, (String_t*) &_stringLiteral87, L_145, /*hidden argument*/NULL);
		Material_t55 * L_146 = (__this->___dx11bokehMaterial_19);
		RenderTexture_t101 * L_147 = ___source;
		NullCheck(L_147);
		int32_t L_148 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_147);
		RenderTexture_t101 * L_149 = ___source;
		NullCheck(L_149);
		int32_t L_150 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_149);
		float L_151 = (__this->___internalBlurWidth_28);
		Vector3_t4  L_152 = {0};
		Vector3__ctor_m612(&L_152, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)L_148)))))), ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)L_150)))))), L_151, /*hidden argument*/NULL);
		Vector4_t236  L_153 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_152, /*hidden argument*/NULL);
		NullCheck(L_146);
		Material_SetVector_m752(L_146, (String_t*) &_stringLiteral88, L_153, /*hidden argument*/NULL);
		Material_t55 * L_154 = (__this->___dx11bokehMaterial_19);
		NullCheck(L_154);
		Material_SetPass_m831(L_154, 2, /*hidden argument*/NULL);
		ComputeBuffer_t95 * L_155 = (__this->___cbDrawArgs_26);
		Graphics_DrawProceduralIndirect_m832(NULL /*static, unused*/, 5, L_155, 0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_156 = V_5;
		RenderTexture_t101 * L_157 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_156, L_157, /*hidden argument*/NULL);
		RenderTexture_t101 * L_158 = V_5;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_158, /*hidden argument*/NULL);
		RenderTexture_t101 * L_159 = V_2;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_159, /*hidden argument*/NULL);
		RenderTexture_t101 * L_160 = V_3;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_160, /*hidden argument*/NULL);
		goto IL_091c;
	}

IL_050d:
	{
		RenderTexture_t101 * L_161 = ___source;
		NullCheck(L_161);
		int32_t L_162 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_161);
		RenderTexture_t101 * L_163 = ___source;
		NullCheck(L_163);
		int32_t L_164 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_163);
		RenderTexture_t101 * L_165 = ___source;
		NullCheck(L_165);
		int32_t L_166 = RenderTexture_get_format_m747(L_165, /*hidden argument*/NULL);
		RenderTexture_t101 * L_167 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_162>>(int32_t)1)), ((int32_t)((int32_t)L_164>>(int32_t)1)), 0, L_166, /*hidden argument*/NULL);
		V_0 = L_167;
		RenderTexture_t101 * L_168 = ___source;
		NullCheck(L_168);
		int32_t L_169 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_168);
		RenderTexture_t101 * L_170 = ___source;
		NullCheck(L_170);
		int32_t L_171 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_170);
		RenderTexture_t101 * L_172 = ___source;
		NullCheck(L_172);
		int32_t L_173 = RenderTexture_get_format_m747(L_172, /*hidden argument*/NULL);
		RenderTexture_t101 * L_174 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_169>>(int32_t)1)), ((int32_t)((int32_t)L_171>>(int32_t)1)), 0, L_173, /*hidden argument*/NULL);
		V_1 = L_174;
		float L_175 = (__this->___internalBlurWidth_28);
		float L_176 = (__this->___foregroundOverlap_15);
		V_4 = ((float)((float)L_175*(float)L_176));
		RenderTexture_t101 * L_177 = ___source;
		DepthOfField_WriteCoc_m270(__this, L_177, 0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_178 = ___source;
		NullCheck(L_178);
		Texture_set_filterMode_m762(L_178, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_179 = ___source;
		RenderTexture_t101 * L_180 = V_0;
		Material_t55 * L_181 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_179, L_180, L_181, 6, /*hidden argument*/NULL);
		RenderTexture_t101 * L_182 = V_0;
		NullCheck(L_182);
		int32_t L_183 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_182);
		RenderTexture_t101 * L_184 = V_0;
		NullCheck(L_184);
		int32_t L_185 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_184);
		RenderTexture_t101 * L_186 = V_0;
		NullCheck(L_186);
		int32_t L_187 = RenderTexture_get_format_m747(L_186, /*hidden argument*/NULL);
		RenderTexture_t101 * L_188 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_183>>(int32_t)1)), ((int32_t)((int32_t)L_185>>(int32_t)1)), 0, L_187, /*hidden argument*/NULL);
		V_2 = L_188;
		RenderTexture_t101 * L_189 = V_0;
		NullCheck(L_189);
		int32_t L_190 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_189);
		RenderTexture_t101 * L_191 = V_0;
		NullCheck(L_191);
		int32_t L_192 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_191);
		RenderTexture_t101 * L_193 = V_0;
		NullCheck(L_193);
		int32_t L_194 = RenderTexture_get_format_m747(L_193, /*hidden argument*/NULL);
		RenderTexture_t101 * L_195 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_190>>(int32_t)1)), ((int32_t)((int32_t)L_192>>(int32_t)1)), 0, L_194, /*hidden argument*/NULL);
		V_3 = L_195;
		RenderTexture_t101 * L_196 = V_0;
		RenderTexture_t101 * L_197 = V_2;
		Material_t55 * L_198 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_196, L_197, L_198, ((int32_t)15), /*hidden argument*/NULL);
		Material_t55 * L_199 = (__this->___dofHdrMaterial_17);
		Vector4_t236  L_200 = {0};
		Vector4__ctor_m751(&L_200, (0.0f), (1.5f), (0.0f), (1.5f), /*hidden argument*/NULL);
		NullCheck(L_199);
		Material_SetVector_m752(L_199, (String_t*) &_stringLiteral23, L_200, /*hidden argument*/NULL);
		RenderTexture_t101 * L_201 = V_2;
		RenderTexture_t101 * L_202 = V_3;
		Material_t55 * L_203 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_201, L_202, L_203, ((int32_t)19), /*hidden argument*/NULL);
		Material_t55 * L_204 = (__this->___dofHdrMaterial_17);
		Vector4_t236  L_205 = {0};
		Vector4__ctor_m751(&L_205, (1.5f), (0.0f), (0.0f), (1.5f), /*hidden argument*/NULL);
		NullCheck(L_204);
		Material_SetVector_m752(L_204, (String_t*) &_stringLiteral23, L_205, /*hidden argument*/NULL);
		RenderTexture_t101 * L_206 = V_3;
		RenderTexture_t101 * L_207 = V_2;
		Material_t55 * L_208 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_206, L_207, L_208, ((int32_t)19), /*hidden argument*/NULL);
		V_6 = (RenderTexture_t101 *)NULL;
		bool L_209 = (__this->___nearBlur_14);
		if (!L_209)
		{
			goto IL_0667;
		}
	}
	{
		RenderTexture_t101 * L_210 = ___source;
		NullCheck(L_210);
		int32_t L_211 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_210);
		RenderTexture_t101 * L_212 = ___source;
		NullCheck(L_212);
		int32_t L_213 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_212);
		RenderTexture_t101 * L_214 = ___source;
		NullCheck(L_214);
		int32_t L_215 = RenderTexture_get_format_m747(L_214, /*hidden argument*/NULL);
		RenderTexture_t101 * L_216 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_211>>(int32_t)1)), ((int32_t)((int32_t)L_213>>(int32_t)1)), 0, L_215, /*hidden argument*/NULL);
		V_6 = L_216;
		RenderTexture_t101 * L_217 = ___source;
		RenderTexture_t101 * L_218 = V_6;
		Material_t55 * L_219 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_217, L_218, L_219, 4, /*hidden argument*/NULL);
	}

IL_0667:
	{
		Material_t55 * L_220 = (__this->___dx11bokehMaterial_19);
		RenderTexture_t101 * L_221 = V_2;
		NullCheck(L_220);
		Material_SetTexture_m759(L_220, (String_t*) &_stringLiteral82, L_221, /*hidden argument*/NULL);
		Material_t55 * L_222 = (__this->___dx11bokehMaterial_19);
		float L_223 = (__this->___dx11SpawnHeuristic_21);
		NullCheck(L_222);
		Material_SetFloat_m738(L_222, (String_t*) &_stringLiteral83, L_223, /*hidden argument*/NULL);
		Material_t55 * L_224 = (__this->___dx11bokehMaterial_19);
		float L_225 = (__this->___dx11BokehScale_23);
		float L_226 = (__this->___dx11BokehIntensity_24);
		float L_227 = (__this->___dx11BokehThreshold_20);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_228 = Mathf_Clamp_m611(NULL /*static, unused*/, L_227, (0.005f), (4.0f), /*hidden argument*/NULL);
		float L_229 = (__this->___internalBlurWidth_28);
		Vector4_t236  L_230 = {0};
		Vector4__ctor_m751(&L_230, L_225, L_226, L_228, L_229, /*hidden argument*/NULL);
		NullCheck(L_224);
		Material_SetVector_m752(L_224, (String_t*) &_stringLiteral84, L_230, /*hidden argument*/NULL);
		Material_t55 * L_231 = (__this->___dx11bokehMaterial_19);
		RenderTexture_t101 * L_232 = V_6;
		NullCheck(L_231);
		Material_SetTexture_m759(L_231, (String_t*) &_stringLiteral85, L_232, /*hidden argument*/NULL);
		ComputeBuffer_t95 * L_233 = (__this->___cbPoints_27);
		Graphics_SetRandomWriteTarget_m826(NULL /*static, unused*/, 1, L_233, /*hidden argument*/NULL);
		RenderTexture_t101 * L_234 = V_0;
		RenderTexture_t101 * L_235 = V_1;
		Material_t55 * L_236 = (__this->___dx11bokehMaterial_19);
		Graphics_Blit_m742(NULL /*static, unused*/, L_234, L_235, L_236, 0, /*hidden argument*/NULL);
		Graphics_ClearRandomWriteTargets_m827(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t101 * L_237 = V_2;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_237, /*hidden argument*/NULL);
		RenderTexture_t101 * L_238 = V_3;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_238, /*hidden argument*/NULL);
		bool L_239 = (__this->___nearBlur_14);
		if (!L_239)
		{
			goto IL_0785;
		}
	}
	{
		Material_t55 * L_240 = (__this->___dofHdrMaterial_17);
		float L_241 = V_4;
		float L_242 = V_4;
		Vector4_t236  L_243 = {0};
		Vector4__ctor_m751(&L_243, (0.0f), L_241, (0.0f), L_242, /*hidden argument*/NULL);
		NullCheck(L_240);
		Material_SetVector_m752(L_240, (String_t*) &_stringLiteral23, L_243, /*hidden argument*/NULL);
		RenderTexture_t101 * L_244 = V_6;
		RenderTexture_t101 * L_245 = V_0;
		Material_t55 * L_246 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_244, L_245, L_246, 2, /*hidden argument*/NULL);
		Material_t55 * L_247 = (__this->___dofHdrMaterial_17);
		float L_248 = V_4;
		float L_249 = V_4;
		Vector4_t236  L_250 = {0};
		Vector4__ctor_m751(&L_250, L_248, (0.0f), (0.0f), L_249, /*hidden argument*/NULL);
		NullCheck(L_247);
		Material_SetVector_m752(L_247, (String_t*) &_stringLiteral23, L_250, /*hidden argument*/NULL);
		RenderTexture_t101 * L_251 = V_0;
		RenderTexture_t101 * L_252 = V_6;
		Material_t55 * L_253 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_251, L_252, L_253, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_254 = V_6;
		RenderTexture_t101 * L_255 = V_1;
		Material_t55 * L_256 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_254, L_255, L_256, 3, /*hidden argument*/NULL);
	}

IL_0785:
	{
		Material_t55 * L_257 = (__this->___dofHdrMaterial_17);
		float L_258 = (__this->___internalBlurWidth_28);
		float L_259 = (__this->___internalBlurWidth_28);
		Vector4_t236  L_260 = {0};
		Vector4__ctor_m751(&L_260, L_258, (0.0f), (0.0f), L_259, /*hidden argument*/NULL);
		NullCheck(L_257);
		Material_SetVector_m752(L_257, (String_t*) &_stringLiteral23, L_260, /*hidden argument*/NULL);
		RenderTexture_t101 * L_261 = V_1;
		RenderTexture_t101 * L_262 = V_0;
		Material_t55 * L_263 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_261, L_262, L_263, 5, /*hidden argument*/NULL);
		Material_t55 * L_264 = (__this->___dofHdrMaterial_17);
		float L_265 = (__this->___internalBlurWidth_28);
		float L_266 = (__this->___internalBlurWidth_28);
		Vector4_t236  L_267 = {0};
		Vector4__ctor_m751(&L_267, (0.0f), L_265, (0.0f), L_266, /*hidden argument*/NULL);
		NullCheck(L_264);
		Material_SetVector_m752(L_264, (String_t*) &_stringLiteral23, L_267, /*hidden argument*/NULL);
		RenderTexture_t101 * L_268 = V_0;
		RenderTexture_t101 * L_269 = V_1;
		Material_t55 * L_270 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_268, L_269, L_270, 5, /*hidden argument*/NULL);
		RenderTexture_t101 * L_271 = V_1;
		Graphics_SetRenderTarget_m753(NULL /*static, unused*/, L_271, /*hidden argument*/NULL);
		ComputeBuffer_t95 * L_272 = (__this->___cbPoints_27);
		ComputeBuffer_t95 * L_273 = (__this->___cbDrawArgs_26);
		ComputeBuffer_CopyCount_m828(NULL /*static, unused*/, L_272, L_273, 0, /*hidden argument*/NULL);
		Material_t55 * L_274 = (__this->___dx11bokehMaterial_19);
		ComputeBuffer_t95 * L_275 = (__this->___cbPoints_27);
		NullCheck(L_274);
		Material_SetBuffer_m829(L_274, (String_t*) &_stringLiteral86, L_275, /*hidden argument*/NULL);
		Material_t55 * L_276 = (__this->___dx11bokehMaterial_19);
		Texture2D_t63 * L_277 = (__this->___dx11BokehTexture_22);
		NullCheck(L_276);
		Material_SetTexture_m759(L_276, (String_t*) &_stringLiteral87, L_277, /*hidden argument*/NULL);
		Material_t55 * L_278 = (__this->___dx11bokehMaterial_19);
		RenderTexture_t101 * L_279 = V_1;
		NullCheck(L_279);
		int32_t L_280 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_279);
		RenderTexture_t101 * L_281 = V_1;
		NullCheck(L_281);
		int32_t L_282 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_281);
		float L_283 = (__this->___internalBlurWidth_28);
		Vector3_t4  L_284 = {0};
		Vector3__ctor_m612(&L_284, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)L_280)))))), ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)L_282)))))), L_283, /*hidden argument*/NULL);
		Vector4_t236  L_285 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_284, /*hidden argument*/NULL);
		NullCheck(L_278);
		Material_SetVector_m752(L_278, (String_t*) &_stringLiteral88, L_285, /*hidden argument*/NULL);
		Material_t55 * L_286 = (__this->___dx11bokehMaterial_19);
		NullCheck(L_286);
		Material_SetPass_m831(L_286, 1, /*hidden argument*/NULL);
		ComputeBuffer_t95 * L_287 = (__this->___cbDrawArgs_26);
		Graphics_DrawProceduralIndirect_m832(NULL /*static, unused*/, 5, L_287, 0, /*hidden argument*/NULL);
		Material_t55 * L_288 = (__this->___dofHdrMaterial_17);
		RenderTexture_t101 * L_289 = V_1;
		NullCheck(L_288);
		Material_SetTexture_m759(L_288, (String_t*) &_stringLiteral89, L_289, /*hidden argument*/NULL);
		Material_t55 * L_290 = (__this->___dofHdrMaterial_17);
		RenderTexture_t101 * L_291 = V_6;
		NullCheck(L_290);
		Material_SetTexture_m759(L_290, (String_t*) &_stringLiteral80, L_291, /*hidden argument*/NULL);
		Material_t55 * L_292 = (__this->___dofHdrMaterial_17);
		RenderTexture_t101 * L_293 = ___source;
		NullCheck(L_293);
		int32_t L_294 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_293);
		RenderTexture_t101 * L_295 = V_1;
		NullCheck(L_295);
		int32_t L_296 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_295);
		float L_297 = (__this->___internalBlurWidth_28);
		Vector4_t236  L_298 = Vector4_get_one_m833(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector4_t236  L_299 = Vector4_op_Multiply_m834(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)(1.0f)*(float)(((float)L_294))))/(float)((float)((float)(1.0f)*(float)(((float)L_296))))))*(float)L_297)), L_298, /*hidden argument*/NULL);
		NullCheck(L_292);
		Material_SetVector_m752(L_292, (String_t*) &_stringLiteral23, L_299, /*hidden argument*/NULL);
		RenderTexture_t101 * L_300 = ___source;
		RenderTexture_t101 * L_301 = ___destination;
		Material_t55 * L_302 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_300, L_301, L_302, ((int32_t)9), /*hidden argument*/NULL);
		RenderTexture_t101 * L_303 = V_6;
		bool L_304 = Object_op_Implicit_m629(NULL /*static, unused*/, L_303, /*hidden argument*/NULL);
		if (!L_304)
		{
			goto IL_091c;
		}
	}
	{
		RenderTexture_t101 * L_305 = V_6;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_305, /*hidden argument*/NULL);
	}

IL_091c:
	{
		goto IL_0ac0;
	}

IL_0921:
	{
		RenderTexture_t101 * L_306 = ___source;
		NullCheck(L_306);
		Texture_set_filterMode_m762(L_306, 1, /*hidden argument*/NULL);
		bool L_307 = (__this->___highResolution_11);
		if (!L_307)
		{
			goto IL_0945;
		}
	}
	{
		float L_308 = (__this->___internalBlurWidth_28);
		__this->___internalBlurWidth_28 = ((float)((float)L_308*(float)(2.0f)));
	}

IL_0945:
	{
		RenderTexture_t101 * L_309 = ___source;
		DepthOfField_WriteCoc_m270(__this, L_309, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_310 = ___source;
		NullCheck(L_310);
		int32_t L_311 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_310);
		RenderTexture_t101 * L_312 = ___source;
		NullCheck(L_312);
		int32_t L_313 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_312);
		RenderTexture_t101 * L_314 = ___source;
		NullCheck(L_314);
		int32_t L_315 = RenderTexture_get_format_m747(L_314, /*hidden argument*/NULL);
		RenderTexture_t101 * L_316 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_311>>(int32_t)1)), ((int32_t)((int32_t)L_313>>(int32_t)1)), 0, L_315, /*hidden argument*/NULL);
		V_0 = L_316;
		RenderTexture_t101 * L_317 = ___source;
		NullCheck(L_317);
		int32_t L_318 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_317);
		RenderTexture_t101 * L_319 = ___source;
		NullCheck(L_319);
		int32_t L_320 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_319);
		RenderTexture_t101 * L_321 = ___source;
		NullCheck(L_321);
		int32_t L_322 = RenderTexture_get_format_m747(L_321, /*hidden argument*/NULL);
		RenderTexture_t101 * L_323 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_318>>(int32_t)1)), ((int32_t)((int32_t)L_320>>(int32_t)1)), 0, L_322, /*hidden argument*/NULL);
		V_1 = L_323;
		int32_t L_324 = (__this->___blurSampleCount_13);
		if ((((int32_t)L_324) == ((int32_t)2)))
		{
			goto IL_099f;
		}
	}
	{
		int32_t L_325 = (__this->___blurSampleCount_13);
		if ((!(((uint32_t)L_325) == ((uint32_t)1))))
		{
			goto IL_09a6;
		}
	}

IL_099f:
	{
		G_B38_0 = ((int32_t)17);
		goto IL_09a8;
	}

IL_09a6:
	{
		G_B38_0 = ((int32_t)11);
	}

IL_09a8:
	{
		V_7 = G_B38_0;
		bool L_326 = (__this->___highResolution_11);
		if (!L_326)
		{
			goto IL_09f4;
		}
	}
	{
		Material_t55 * L_327 = (__this->___dofHdrMaterial_17);
		float L_328 = (__this->___internalBlurWidth_28);
		float L_329 = (__this->___internalBlurWidth_28);
		Vector4_t236  L_330 = {0};
		Vector4__ctor_m751(&L_330, (0.0f), L_328, (0.025f), L_329, /*hidden argument*/NULL);
		NullCheck(L_327);
		Material_SetVector_m752(L_327, (String_t*) &_stringLiteral23, L_330, /*hidden argument*/NULL);
		RenderTexture_t101 * L_331 = ___source;
		RenderTexture_t101 * L_332 = ___destination;
		Material_t55 * L_333 = (__this->___dofHdrMaterial_17);
		int32_t L_334 = V_7;
		Graphics_Blit_m742(NULL /*static, unused*/, L_331, L_332, L_333, L_334, /*hidden argument*/NULL);
		goto IL_0ac0;
	}

IL_09f4:
	{
		Material_t55 * L_335 = (__this->___dofHdrMaterial_17);
		float L_336 = (__this->___internalBlurWidth_28);
		float L_337 = (__this->___internalBlurWidth_28);
		Vector4_t236  L_338 = {0};
		Vector4__ctor_m751(&L_338, (0.0f), L_336, (0.1f), L_337, /*hidden argument*/NULL);
		NullCheck(L_335);
		Material_SetVector_m752(L_335, (String_t*) &_stringLiteral23, L_338, /*hidden argument*/NULL);
		RenderTexture_t101 * L_339 = ___source;
		RenderTexture_t101 * L_340 = V_0;
		Material_t55 * L_341 = (__this->___dofHdrMaterial_17);
		Graphics_Blit_m742(NULL /*static, unused*/, L_339, L_340, L_341, 6, /*hidden argument*/NULL);
		RenderTexture_t101 * L_342 = V_0;
		RenderTexture_t101 * L_343 = V_1;
		Material_t55 * L_344 = (__this->___dofHdrMaterial_17);
		int32_t L_345 = V_7;
		Graphics_Blit_m742(NULL /*static, unused*/, L_342, L_343, L_344, L_345, /*hidden argument*/NULL);
		Material_t55 * L_346 = (__this->___dofHdrMaterial_17);
		RenderTexture_t101 * L_347 = V_1;
		NullCheck(L_346);
		Material_SetTexture_m759(L_346, (String_t*) &_stringLiteral89, L_347, /*hidden argument*/NULL);
		Material_t55 * L_348 = (__this->___dofHdrMaterial_17);
		NullCheck(L_348);
		Material_SetTexture_m759(L_348, (String_t*) &_stringLiteral80, (Texture_t86 *)NULL, /*hidden argument*/NULL);
		Material_t55 * L_349 = (__this->___dofHdrMaterial_17);
		Vector4_t236  L_350 = Vector4_get_one_m833(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t101 * L_351 = ___source;
		NullCheck(L_351);
		int32_t L_352 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_351);
		RenderTexture_t101 * L_353 = V_1;
		NullCheck(L_353);
		int32_t L_354 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_353);
		Vector4_t236  L_355 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_350, ((float)((float)((float)((float)(1.0f)*(float)(((float)L_352))))/(float)((float)((float)(1.0f)*(float)(((float)L_354)))))), /*hidden argument*/NULL);
		float L_356 = (__this->___internalBlurWidth_28);
		Vector4_t236  L_357 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_355, L_356, /*hidden argument*/NULL);
		NullCheck(L_349);
		Material_SetVector_m752(L_349, (String_t*) &_stringLiteral23, L_357, /*hidden argument*/NULL);
		RenderTexture_t101 * L_358 = ___source;
		RenderTexture_t101 * L_359 = ___destination;
		Material_t55 * L_360 = (__this->___dofHdrMaterial_17);
		int32_t L_361 = (__this->___blurSampleCount_13);
		G_B41_0 = L_360;
		G_B41_1 = L_359;
		G_B41_2 = L_358;
		if ((!(((uint32_t)L_361) == ((uint32_t)2))))
		{
			G_B42_0 = L_360;
			G_B42_1 = L_359;
			G_B42_2 = L_358;
			goto IL_0ab9;
		}
	}
	{
		G_B43_0 = ((int32_t)18);
		G_B43_1 = G_B41_0;
		G_B43_2 = G_B41_1;
		G_B43_3 = G_B41_2;
		goto IL_0abb;
	}

IL_0ab9:
	{
		G_B43_0 = ((int32_t)12);
		G_B43_1 = G_B42_0;
		G_B43_2 = G_B42_1;
		G_B43_3 = G_B42_2;
	}

IL_0abb:
	{
		Graphics_Blit_m742(NULL /*static, unused*/, G_B43_3, G_B43_2, G_B43_1, G_B43_0, /*hidden argument*/NULL);
	}

IL_0ac0:
	{
		RenderTexture_t101 * L_362 = V_0;
		bool L_363 = Object_op_Implicit_m629(NULL /*static, unused*/, L_362, /*hidden argument*/NULL);
		if (!L_363)
		{
			goto IL_0ad1;
		}
	}
	{
		RenderTexture_t101 * L_364 = V_0;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_364, /*hidden argument*/NULL);
	}

IL_0ad1:
	{
		RenderTexture_t101 * L_365 = V_1;
		bool L_366 = Object_op_Implicit_m629(NULL /*static, unused*/, L_365, /*hidden argument*/NULL);
		if (!L_366)
		{
			goto IL_0ae2;
		}
	}
	{
		RenderTexture_t101 * L_367 = V_1;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_367, /*hidden argument*/NULL);
	}

IL_0ae2:
	{
		return;
	}
}
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/Dof34QualitySetting
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_32.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/Dof34QualitySetting
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_32MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_33.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofResolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_33MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_34.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_34MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_35.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/BokehDestination
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_35MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_36.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_36MethodDeclarations.h"

// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityStandardAssets.ImageEffects.Quads
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_47MethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::.ctor()
extern "C" void DepthOfFieldDeprecated__ctor_m272 (DepthOfFieldDeprecated_t102 * __this, const MethodInfo* method)
{
	{
		__this->___quality_7 = 1;
		__this->___resolution_8 = 4;
		__this->___simpleTweakMode_9 = 1;
		__this->___focalPoint_10 = (1.0f);
		__this->___smoothness_11 = (0.5f);
		__this->___focalZStartCurve_13 = (1.0f);
		__this->___focalZEndCurve_14 = (1.0f);
		__this->___focalStartCurve_15 = (2.0f);
		__this->___focalEndCurve_16 = (2.0f);
		__this->___focalDistance01_17 = (0.1f);
		__this->___bluriness_20 = 2;
		__this->___maxBlurSpread_21 = (1.75f);
		__this->___foregroundBlurExtrude_22 = (1.15f);
		__this->___bokehDestination_28 = 1;
		__this->___widthOverHeight_29 = (1.25f);
		__this->___oneOverBaseSize_30 = (0.001953125f);
		__this->___bokehSupport_32 = 1;
		__this->___bokehScale_35 = (2.4f);
		__this->___bokehIntensity_36 = (0.15f);
		__this->___bokehThresholdContrast_37 = (0.1f);
		__this->___bokehThresholdLuminance_38 = (0.55f);
		__this->___bokehDownsample_39 = 1;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::.cctor()
extern TypeInfo* DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var;
extern "C" void DepthOfFieldDeprecated__cctor_m273 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	{
		((DepthOfFieldDeprecated_t102_StaticFields*)DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var->static_fields)->___SMOOTH_DOWNSAMPLE_PASS_5 = 6;
		((DepthOfFieldDeprecated_t102_StaticFields*)DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var->static_fields)->___BOKEH_EXTRA_BLUR_6 = (2.0f);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::CreateMaterials()
extern "C" void DepthOfFieldDeprecated_CreateMaterials_m274 (DepthOfFieldDeprecated_t102 * __this, const MethodInfo* method)
{
	{
		Shader_t54 * L_0 = (__this->___dofBlurShader_23);
		Material_t55 * L_1 = (__this->___dofBlurMaterial_24);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___dofBlurMaterial_24 = L_2;
		Shader_t54 * L_3 = (__this->___dofShader_25);
		Material_t55 * L_4 = (__this->___dofMaterial_26);
		Material_t55 * L_5 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_3, L_4, /*hidden argument*/NULL);
		__this->___dofMaterial_26 = L_5;
		Shader_t54 * L_6 = (__this->___bokehShader_33);
		NullCheck(L_6);
		bool L_7 = Shader_get_isSupported_m736(L_6, /*hidden argument*/NULL);
		__this->___bokehSupport_32 = L_7;
		bool L_8 = (__this->___bokeh_31);
		if (!L_8)
		{
			goto IL_007f;
		}
	}
	{
		bool L_9 = (__this->___bokehSupport_32);
		if (!L_9)
		{
			goto IL_007f;
		}
	}
	{
		Shader_t54 * L_10 = (__this->___bokehShader_33);
		bool L_11 = Object_op_Implicit_m629(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007f;
		}
	}
	{
		Shader_t54 * L_12 = (__this->___bokehShader_33);
		Material_t55 * L_13 = (__this->___bokehMaterial_40);
		Material_t55 * L_14 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_12, L_13, /*hidden argument*/NULL);
		__this->___bokehMaterial_40 = L_14;
	}

IL_007f:
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::CheckResources()
extern "C" bool DepthOfFieldDeprecated_CheckResources_m275 (DepthOfFieldDeprecated_t102 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 1, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___dofBlurShader_23);
		Material_t55 * L_1 = (__this->___dofBlurMaterial_24);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___dofBlurMaterial_24 = L_2;
		Shader_t54 * L_3 = (__this->___dofShader_25);
		Material_t55 * L_4 = (__this->___dofMaterial_26);
		Material_t55 * L_5 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_3, L_4, /*hidden argument*/NULL);
		__this->___dofMaterial_26 = L_5;
		Shader_t54 * L_6 = (__this->___bokehShader_33);
		NullCheck(L_6);
		bool L_7 = Shader_get_isSupported_m736(L_6, /*hidden argument*/NULL);
		__this->___bokehSupport_32 = L_7;
		bool L_8 = (__this->___bokeh_31);
		if (!L_8)
		{
			goto IL_0087;
		}
	}
	{
		bool L_9 = (__this->___bokehSupport_32);
		if (!L_9)
		{
			goto IL_0087;
		}
	}
	{
		Shader_t54 * L_10 = (__this->___bokehShader_33);
		bool L_11 = Object_op_Implicit_m629(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0087;
		}
	}
	{
		Shader_t54 * L_12 = (__this->___bokehShader_33);
		Material_t55 * L_13 = (__this->___bokehMaterial_40);
		Material_t55 * L_14 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_12, L_13, /*hidden argument*/NULL);
		__this->___bokehMaterial_40 = L_14;
	}

IL_0087:
	{
		bool L_15 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_15)
		{
			goto IL_0098;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0098:
	{
		bool L_16 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_16;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnDisable()
extern TypeInfo* Quads_t114_il2cpp_TypeInfo_var;
extern "C" void DepthOfFieldDeprecated_OnDisable_m276 (DepthOfFieldDeprecated_t102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quads_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(56);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		Quads_Cleanup_m349(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnEnable()
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void DepthOfFieldDeprecated_OnEnable_m277 (DepthOfFieldDeprecated_t102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t27 * L_0 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		__this->____camera_41 = L_0;
		Camera_t27 * L_1 = (__this->____camera_41);
		Camera_t27 * L_2 = L_1;
		NullCheck(L_2);
		int32_t L_3 = Camera_get_depthTextureMode_m779(L_2, /*hidden argument*/NULL);
		NullCheck(L_2);
		Camera_set_depthTextureMode_m780(L_2, ((int32_t)((int32_t)L_3|(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::FocalDistance01(System.Single)
extern "C" float DepthOfFieldDeprecated_FocalDistance01_m278 (DepthOfFieldDeprecated_t102 * __this, float ___worldDist, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	{
		Camera_t27 * L_0 = (__this->____camera_41);
		float L_1 = ___worldDist;
		Camera_t27 * L_2 = (__this->____camera_41);
		NullCheck(L_2);
		float L_3 = Camera_get_nearClipPlane_m823(L_2, /*hidden argument*/NULL);
		Camera_t27 * L_4 = (__this->____camera_41);
		NullCheck(L_4);
		Transform_t1 * L_5 = Component_get_transform_m594(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4  L_6 = Transform_get_forward_m643(L_5, /*hidden argument*/NULL);
		Vector3_t4  L_7 = Vector3_op_Multiply_m598(NULL /*static, unused*/, ((float)((float)L_1-(float)L_3)), L_6, /*hidden argument*/NULL);
		Camera_t27 * L_8 = (__this->____camera_41);
		NullCheck(L_8);
		Transform_t1 * L_9 = Component_get_transform_m594(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t4  L_10 = Transform_get_position_m593(L_9, /*hidden argument*/NULL);
		Vector3_t4  L_11 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_12 = Camera_WorldToViewportPoint_m824(L_0, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		float L_13 = ((&V_0)->___z_3);
		Camera_t27 * L_14 = (__this->____camera_41);
		NullCheck(L_14);
		float L_15 = Camera_get_farClipPlane_m825(L_14, /*hidden argument*/NULL);
		Camera_t27 * L_16 = (__this->____camera_41);
		NullCheck(L_16);
		float L_17 = Camera_get_nearClipPlane_m823(L_16, /*hidden argument*/NULL);
		return ((float)((float)L_13/(float)((float)((float)L_15-(float)L_17))));
	}
}
// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::GetDividerBasedOnQuality()
extern "C" int32_t DepthOfFieldDeprecated_GetDividerBasedOnQuality_m279 (DepthOfFieldDeprecated_t102 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		int32_t L_0 = (__this->___resolution_8);
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = 2;
		goto IL_0023;
	}

IL_0015:
	{
		int32_t L_1 = (__this->___resolution_8);
		if ((!(((uint32_t)L_1) == ((uint32_t)4))))
		{
			goto IL_0023;
		}
	}
	{
		V_0 = 2;
	}

IL_0023:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::GetLowResolutionDividerBasedOnQuality(System.Int32)
extern "C" int32_t DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m280 (DepthOfFieldDeprecated_t102 * __this, int32_t ___baseDivider, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___baseDivider;
		V_0 = L_0;
		int32_t L_1 = (__this->___resolution_8);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2*(int32_t)2));
	}

IL_0012:
	{
		int32_t L_3 = (__this->___resolution_8);
		if ((!(((uint32_t)L_3) == ((uint32_t)4))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4*(int32_t)2));
	}

IL_0022:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void DepthOfFieldDeprecated_OnRenderImage_m281 (DepthOfFieldDeprecated_t102 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector3_t4  V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Vector3_t4  V_6 = {0};
	DepthOfFieldDeprecated_t102 * G_B6_0 = {0};
	DepthOfFieldDeprecated_t102 * G_B5_0 = {0};
	int32_t G_B7_0 = 0;
	DepthOfFieldDeprecated_t102 * G_B7_1 = {0};
	float G_B10_0 = 0.0f;
	DepthOfFieldDeprecated_t102 * G_B13_0 = {0};
	DepthOfFieldDeprecated_t102 * G_B12_0 = {0};
	float G_B14_0 = 0.0f;
	DepthOfFieldDeprecated_t102 * G_B14_1 = {0};
	int32_t G_B17_0 = 0;
	int32_t G_B24_0 = 0;
	String_t* G_B27_0 = {0};
	Material_t55 * G_B27_1 = {0};
	String_t* G_B26_0 = {0};
	Material_t55 * G_B26_1 = {0};
	float G_B28_0 = 0.0f;
	String_t* G_B28_1 = {0};
	Material_t55 * G_B28_2 = {0};
	float G_B30_0 = 0.0f;
	String_t* G_B30_1 = {0};
	Material_t55 * G_B30_2 = {0};
	float G_B29_0 = 0.0f;
	String_t* G_B29_1 = {0};
	Material_t55 * G_B29_2 = {0};
	float G_B31_0 = 0.0f;
	float G_B31_1 = 0.0f;
	String_t* G_B31_2 = {0};
	Material_t55 * G_B31_3 = {0};
	RenderTexture_t101 * G_B40_0 = {0};
	RenderTexture_t101 * G_B39_0 = {0};
	RenderTexture_t101 * G_B41_0 = {0};
	RenderTexture_t101 * G_B41_1 = {0};
	Material_t55 * G_B43_0 = {0};
	RenderTexture_t101 * G_B43_1 = {0};
	RenderTexture_t101 * G_B43_2 = {0};
	Material_t55 * G_B42_0 = {0};
	RenderTexture_t101 * G_B42_1 = {0};
	RenderTexture_t101 * G_B42_2 = {0};
	int32_t G_B44_0 = 0;
	Material_t55 * G_B44_1 = {0};
	RenderTexture_t101 * G_B44_2 = {0};
	RenderTexture_t101 * G_B44_3 = {0};
	Material_t55 * G_B51_0 = {0};
	RenderTexture_t101 * G_B51_1 = {0};
	RenderTexture_t101 * G_B51_2 = {0};
	Material_t55 * G_B50_0 = {0};
	RenderTexture_t101 * G_B50_1 = {0};
	RenderTexture_t101 * G_B50_2 = {0};
	int32_t G_B52_0 = 0;
	Material_t55 * G_B52_1 = {0};
	RenderTexture_t101 * G_B52_2 = {0};
	RenderTexture_t101 * G_B52_3 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		float L_3 = (__this->___smoothness_11);
		if ((!(((float)L_3) < ((float)(0.1f)))))
		{
			goto IL_002e;
		}
	}
	{
		__this->___smoothness_11 = (0.1f);
	}

IL_002e:
	{
		bool L_4 = (__this->___bokeh_31);
		G_B5_0 = __this;
		if (!L_4)
		{
			G_B6_0 = __this;
			goto IL_0042;
		}
	}
	{
		bool L_5 = (__this->___bokehSupport_32);
		G_B7_0 = ((int32_t)(L_5));
		G_B7_1 = G_B5_0;
		goto IL_0043;
	}

IL_0042:
	{
		G_B7_0 = 0;
		G_B7_1 = G_B6_0;
	}

IL_0043:
	{
		NullCheck(G_B7_1);
		G_B7_1->___bokeh_31 = G_B7_0;
		bool L_6 = (__this->___bokeh_31);
		if (!L_6)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var);
		float L_7 = ((DepthOfFieldDeprecated_t102_StaticFields*)DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var->static_fields)->___BOKEH_EXTRA_BLUR_6;
		G_B10_0 = L_7;
		goto IL_0062;
	}

IL_005d:
	{
		G_B10_0 = (1.0f);
	}

IL_0062:
	{
		V_0 = G_B10_0;
		int32_t L_8 = (__this->___quality_7);
		V_1 = ((((int32_t)L_8) > ((int32_t)1))? 1 : 0);
		float L_9 = (__this->___focalSize_19);
		Camera_t27 * L_10 = (__this->____camera_41);
		NullCheck(L_10);
		float L_11 = Camera_get_farClipPlane_m825(L_10, /*hidden argument*/NULL);
		Camera_t27 * L_12 = (__this->____camera_41);
		NullCheck(L_12);
		float L_13 = Camera_get_nearClipPlane_m823(L_12, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_9/(float)((float)((float)L_11-(float)L_13))));
		bool L_14 = (__this->___simpleTweakMode_9);
		if (!L_14)
		{
			goto IL_0130;
		}
	}
	{
		Transform_t1 * L_15 = (__this->___objectFocus_18);
		bool L_16 = Object_op_Implicit_m629(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		G_B12_0 = __this;
		if (!L_16)
		{
			G_B13_0 = __this;
			goto IL_00d8;
		}
	}
	{
		Camera_t27 * L_17 = (__this->____camera_41);
		Transform_t1 * L_18 = (__this->___objectFocus_18);
		NullCheck(L_18);
		Vector3_t4  L_19 = Transform_get_position_m593(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t4  L_20 = Camera_WorldToViewportPoint_m824(L_17, L_19, /*hidden argument*/NULL);
		V_6 = L_20;
		float L_21 = ((&V_6)->___z_3);
		Camera_t27 * L_22 = (__this->____camera_41);
		NullCheck(L_22);
		float L_23 = Camera_get_farClipPlane_m825(L_22, /*hidden argument*/NULL);
		G_B14_0 = ((float)((float)L_21/(float)L_23));
		G_B14_1 = G_B12_0;
		goto IL_00e4;
	}

IL_00d8:
	{
		float L_24 = (__this->___focalPoint_10);
		float L_25 = DepthOfFieldDeprecated_FocalDistance01_m278(__this, L_24, /*hidden argument*/NULL);
		G_B14_0 = L_25;
		G_B14_1 = G_B13_0;
	}

IL_00e4:
	{
		NullCheck(G_B14_1);
		G_B14_1->___focalDistance01_17 = G_B14_0;
		float L_26 = (__this->___focalDistance01_17);
		float L_27 = (__this->___smoothness_11);
		__this->___focalStartCurve_15 = ((float)((float)L_26*(float)L_27));
		float L_28 = (__this->___focalStartCurve_15);
		__this->___focalEndCurve_16 = L_28;
		bool L_29 = V_1;
		if (!L_29)
		{
			goto IL_0129;
		}
	}
	{
		float L_30 = (__this->___focalPoint_10);
		Camera_t27 * L_31 = (__this->____camera_41);
		NullCheck(L_31);
		float L_32 = Camera_get_nearClipPlane_m823(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_33 = ((Mathf_t218_StaticFields*)Mathf_t218_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		G_B17_0 = ((((float)L_30) > ((float)((float)((float)L_32+(float)L_33))))? 1 : 0);
		goto IL_012a;
	}

IL_0129:
	{
		G_B17_0 = 0;
	}

IL_012a:
	{
		V_1 = G_B17_0;
		goto IL_01d0;
	}

IL_0130:
	{
		Transform_t1 * L_34 = (__this->___objectFocus_18);
		bool L_35 = Object_op_Implicit_m629(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0183;
		}
	}
	{
		Camera_t27 * L_36 = (__this->____camera_41);
		Transform_t1 * L_37 = (__this->___objectFocus_18);
		NullCheck(L_37);
		Vector3_t4  L_38 = Transform_get_position_m593(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t4  L_39 = Camera_WorldToViewportPoint_m824(L_36, L_38, /*hidden argument*/NULL);
		V_3 = L_39;
		float L_40 = ((&V_3)->___z_3);
		Camera_t27 * L_41 = (__this->____camera_41);
		NullCheck(L_41);
		float L_42 = Camera_get_farClipPlane_m825(L_41, /*hidden argument*/NULL);
		(&V_3)->___z_3 = ((float)((float)L_40/(float)L_42));
		float L_43 = ((&V_3)->___z_3);
		__this->___focalDistance01_17 = L_43;
		goto IL_0195;
	}

IL_0183:
	{
		float L_44 = (__this->___focalZDistance_12);
		float L_45 = DepthOfFieldDeprecated_FocalDistance01_m278(__this, L_44, /*hidden argument*/NULL);
		__this->___focalDistance01_17 = L_45;
	}

IL_0195:
	{
		float L_46 = (__this->___focalZStartCurve_13);
		__this->___focalStartCurve_15 = L_46;
		float L_47 = (__this->___focalZEndCurve_14);
		__this->___focalEndCurve_16 = L_47;
		bool L_48 = V_1;
		if (!L_48)
		{
			goto IL_01ce;
		}
	}
	{
		float L_49 = (__this->___focalPoint_10);
		Camera_t27 * L_50 = (__this->____camera_41);
		NullCheck(L_50);
		float L_51 = Camera_get_nearClipPlane_m823(L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_52 = ((Mathf_t218_StaticFields*)Mathf_t218_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		G_B24_0 = ((((float)L_49) > ((float)((float)((float)L_51+(float)L_52))))? 1 : 0);
		goto IL_01cf;
	}

IL_01ce:
	{
		G_B24_0 = 0;
	}

IL_01cf:
	{
		V_1 = G_B24_0;
	}

IL_01d0:
	{
		RenderTexture_t101 * L_53 = ___source;
		NullCheck(L_53);
		int32_t L_54 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_53);
		RenderTexture_t101 * L_55 = ___source;
		NullCheck(L_55);
		int32_t L_56 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_55);
		__this->___widthOverHeight_29 = ((float)((float)((float)((float)(1.0f)*(float)(((float)L_54))))/(float)((float)((float)(1.0f)*(float)(((float)L_56))))));
		__this->___oneOverBaseSize_30 = (0.001953125f);
		Material_t55 * L_57 = (__this->___dofMaterial_26);
		float L_58 = (__this->___foregroundBlurExtrude_22);
		NullCheck(L_57);
		Material_SetFloat_m738(L_57, (String_t*) &_stringLiteral90, L_58, /*hidden argument*/NULL);
		Material_t55 * L_59 = (__this->___dofMaterial_26);
		bool L_60 = (__this->___simpleTweakMode_9);
		G_B26_0 = (String_t*) &_stringLiteral81;
		G_B26_1 = L_59;
		if (!L_60)
		{
			G_B27_0 = (String_t*) &_stringLiteral81;
			G_B27_1 = L_59;
			goto IL_0239;
		}
	}
	{
		float L_61 = (__this->___focalStartCurve_15);
		G_B28_0 = ((float)((float)(1.0f)/(float)L_61));
		G_B28_1 = G_B26_0;
		G_B28_2 = G_B26_1;
		goto IL_023f;
	}

IL_0239:
	{
		float L_62 = (__this->___focalStartCurve_15);
		G_B28_0 = L_62;
		G_B28_1 = G_B27_0;
		G_B28_2 = G_B27_1;
	}

IL_023f:
	{
		bool L_63 = (__this->___simpleTweakMode_9);
		G_B29_0 = G_B28_0;
		G_B29_1 = G_B28_1;
		G_B29_2 = G_B28_2;
		if (!L_63)
		{
			G_B30_0 = G_B28_0;
			G_B30_1 = G_B28_1;
			G_B30_2 = G_B28_2;
			goto IL_025b;
		}
	}
	{
		float L_64 = (__this->___focalEndCurve_16);
		G_B31_0 = ((float)((float)(1.0f)/(float)L_64));
		G_B31_1 = G_B29_0;
		G_B31_2 = G_B29_1;
		G_B31_3 = G_B29_2;
		goto IL_0261;
	}

IL_025b:
	{
		float L_65 = (__this->___focalEndCurve_16);
		G_B31_0 = L_65;
		G_B31_1 = G_B30_0;
		G_B31_2 = G_B30_1;
		G_B31_3 = G_B30_2;
	}

IL_0261:
	{
		float L_66 = V_2;
		float L_67 = (__this->___focalDistance01_17);
		Vector4_t236  L_68 = {0};
		Vector4__ctor_m751(&L_68, G_B31_1, G_B31_0, ((float)((float)L_66*(float)(0.5f))), L_67, /*hidden argument*/NULL);
		NullCheck(G_B31_3);
		Material_SetVector_m752(G_B31_3, G_B31_2, L_68, /*hidden argument*/NULL);
		Material_t55 * L_69 = (__this->___dofMaterial_26);
		RenderTexture_t101 * L_70 = ___source;
		NullCheck(L_70);
		int32_t L_71 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_70);
		RenderTexture_t101 * L_72 = ___source;
		NullCheck(L_72);
		int32_t L_73 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_72);
		Vector4_t236  L_74 = {0};
		Vector4__ctor_m751(&L_74, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)L_71)))))), ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)L_73)))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_69);
		Material_SetVector_m752(L_69, (String_t*) &_stringLiteral91, L_74, /*hidden argument*/NULL);
		int32_t L_75 = DepthOfFieldDeprecated_GetDividerBasedOnQuality_m279(__this, /*hidden argument*/NULL);
		V_4 = L_75;
		int32_t L_76 = V_4;
		int32_t L_77 = DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m280(__this, L_76, /*hidden argument*/NULL);
		V_5 = L_77;
		bool L_78 = V_1;
		RenderTexture_t101 * L_79 = ___source;
		int32_t L_80 = V_4;
		int32_t L_81 = V_5;
		DepthOfFieldDeprecated_AllocateTextures_m288(__this, L_78, L_79, L_80, L_81, /*hidden argument*/NULL);
		RenderTexture_t101 * L_82 = ___source;
		RenderTexture_t101 * L_83 = ___source;
		Material_t55 * L_84 = (__this->___dofMaterial_26);
		Graphics_Blit_m742(NULL /*static, unused*/, L_82, L_83, L_84, 3, /*hidden argument*/NULL);
		RenderTexture_t101 * L_85 = ___source;
		RenderTexture_t101 * L_86 = (__this->___mediumRezWorkTexture_43);
		DepthOfFieldDeprecated_Downsample_m285(__this, L_85, L_86, /*hidden argument*/NULL);
		RenderTexture_t101 * L_87 = (__this->___mediumRezWorkTexture_43);
		RenderTexture_t101 * L_88 = (__this->___mediumRezWorkTexture_43);
		float L_89 = (__this->___maxBlurSpread_21);
		DepthOfFieldDeprecated_Blur_m282(__this, L_87, L_88, 1, 4, L_89, /*hidden argument*/NULL);
		bool L_90 = (__this->___bokeh_31);
		if (!L_90)
		{
			goto IL_03a3;
		}
	}
	{
		int32_t L_91 = (__this->___bokehDestination_28);
		if (!((int32_t)((int32_t)2&(int32_t)L_91)))
		{
			goto IL_03a3;
		}
	}
	{
		Material_t55 * L_92 = (__this->___dofMaterial_26);
		float L_93 = (__this->___bokehThresholdContrast_37);
		float L_94 = (__this->___bokehThresholdLuminance_38);
		Vector4_t236  L_95 = {0};
		Vector4__ctor_m751(&L_95, L_93, L_94, (0.95f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_92);
		Material_SetVector_m752(L_92, (String_t*) &_stringLiteral24, L_95, /*hidden argument*/NULL);
		RenderTexture_t101 * L_96 = (__this->___mediumRezWorkTexture_43);
		RenderTexture_t101 * L_97 = (__this->___bokehSource2_47);
		Material_t55 * L_98 = (__this->___dofMaterial_26);
		Graphics_Blit_m742(NULL /*static, unused*/, L_96, L_97, L_98, ((int32_t)11), /*hidden argument*/NULL);
		RenderTexture_t101 * L_99 = (__this->___mediumRezWorkTexture_43);
		RenderTexture_t101 * L_100 = (__this->___lowRezWorkTexture_45);
		Graphics_Blit_m737(NULL /*static, unused*/, L_99, L_100, /*hidden argument*/NULL);
		RenderTexture_t101 * L_101 = (__this->___lowRezWorkTexture_45);
		RenderTexture_t101 * L_102 = (__this->___lowRezWorkTexture_45);
		int32_t L_103 = (__this->___bluriness_20);
		float L_104 = (__this->___maxBlurSpread_21);
		float L_105 = V_0;
		DepthOfFieldDeprecated_Blur_m282(__this, L_101, L_102, L_103, 0, ((float)((float)L_104*(float)L_105)), /*hidden argument*/NULL);
		goto IL_03d4;
	}

IL_03a3:
	{
		RenderTexture_t101 * L_106 = (__this->___mediumRezWorkTexture_43);
		RenderTexture_t101 * L_107 = (__this->___lowRezWorkTexture_45);
		DepthOfFieldDeprecated_Downsample_m285(__this, L_106, L_107, /*hidden argument*/NULL);
		RenderTexture_t101 * L_108 = (__this->___lowRezWorkTexture_45);
		RenderTexture_t101 * L_109 = (__this->___lowRezWorkTexture_45);
		int32_t L_110 = (__this->___bluriness_20);
		float L_111 = (__this->___maxBlurSpread_21);
		DepthOfFieldDeprecated_Blur_m282(__this, L_108, L_109, L_110, 0, L_111, /*hidden argument*/NULL);
	}

IL_03d4:
	{
		Material_t55 * L_112 = (__this->___dofBlurMaterial_24);
		RenderTexture_t101 * L_113 = (__this->___lowRezWorkTexture_45);
		NullCheck(L_112);
		Material_SetTexture_m759(L_112, (String_t*) &_stringLiteral92, L_113, /*hidden argument*/NULL);
		Material_t55 * L_114 = (__this->___dofBlurMaterial_24);
		RenderTexture_t101 * L_115 = (__this->___mediumRezWorkTexture_43);
		NullCheck(L_114);
		Material_SetTexture_m759(L_114, (String_t*) &_stringLiteral93, L_115, /*hidden argument*/NULL);
		RenderTexture_t101 * L_116 = (__this->___finalDefocus_44);
		Material_t55 * L_117 = (__this->___dofBlurMaterial_24);
		Graphics_Blit_m742(NULL /*static, unused*/, (Texture_t86 *)NULL, L_116, L_117, 3, /*hidden argument*/NULL);
		bool L_118 = (__this->___bokeh_31);
		if (!L_118)
		{
			goto IL_0443;
		}
	}
	{
		int32_t L_119 = (__this->___bokehDestination_28);
		if (!((int32_t)((int32_t)2&(int32_t)L_119)))
		{
			goto IL_0443;
		}
	}
	{
		RenderTexture_t101 * L_120 = (__this->___bokehSource2_47);
		RenderTexture_t101 * L_121 = (__this->___bokehSource_46);
		RenderTexture_t101 * L_122 = (__this->___finalDefocus_44);
		DepthOfFieldDeprecated_AddBokeh_m286(__this, L_120, L_121, L_122, /*hidden argument*/NULL);
	}

IL_0443:
	{
		Material_t55 * L_123 = (__this->___dofMaterial_26);
		RenderTexture_t101 * L_124 = (__this->___finalDefocus_44);
		NullCheck(L_123);
		Material_SetTexture_m759(L_123, (String_t*) &_stringLiteral94, L_124, /*hidden argument*/NULL);
		Material_t55 * L_125 = (__this->___dofMaterial_26);
		RenderTexture_t101 * L_126 = (__this->___mediumRezWorkTexture_43);
		NullCheck(L_125);
		Material_SetTexture_m759(L_125, (String_t*) &_stringLiteral93, L_126, /*hidden argument*/NULL);
		RenderTexture_t101 * L_127 = ___source;
		bool L_128 = V_1;
		G_B39_0 = L_127;
		if (!L_128)
		{
			G_B40_0 = L_127;
			goto IL_0481;
		}
	}
	{
		RenderTexture_t101 * L_129 = (__this->___foregroundTexture_42);
		G_B41_0 = L_129;
		G_B41_1 = G_B39_0;
		goto IL_0482;
	}

IL_0481:
	{
		RenderTexture_t101 * L_130 = ___destination;
		G_B41_0 = L_130;
		G_B41_1 = G_B40_0;
	}

IL_0482:
	{
		Material_t55 * L_131 = (__this->___dofMaterial_26);
		bool L_132 = (__this->___visualize_27);
		G_B42_0 = L_131;
		G_B42_1 = G_B41_0;
		G_B42_2 = G_B41_1;
		if (!L_132)
		{
			G_B43_0 = L_131;
			G_B43_1 = G_B41_0;
			G_B43_2 = G_B41_1;
			goto IL_0499;
		}
	}
	{
		G_B44_0 = 2;
		G_B44_1 = G_B42_0;
		G_B44_2 = G_B42_1;
		G_B44_3 = G_B42_2;
		goto IL_049a;
	}

IL_0499:
	{
		G_B44_0 = 0;
		G_B44_1 = G_B43_0;
		G_B44_2 = G_B43_1;
		G_B44_3 = G_B43_2;
	}

IL_049a:
	{
		Graphics_Blit_m742(NULL /*static, unused*/, G_B44_3, G_B44_2, G_B44_1, G_B44_0, /*hidden argument*/NULL);
		bool L_133 = V_1;
		if (!L_133)
		{
			goto IL_0608;
		}
	}
	{
		RenderTexture_t101 * L_134 = (__this->___foregroundTexture_42);
		RenderTexture_t101 * L_135 = ___source;
		Material_t55 * L_136 = (__this->___dofMaterial_26);
		Graphics_Blit_m742(NULL /*static, unused*/, L_134, L_135, L_136, 5, /*hidden argument*/NULL);
		RenderTexture_t101 * L_137 = ___source;
		RenderTexture_t101 * L_138 = (__this->___mediumRezWorkTexture_43);
		DepthOfFieldDeprecated_Downsample_m285(__this, L_137, L_138, /*hidden argument*/NULL);
		RenderTexture_t101 * L_139 = (__this->___mediumRezWorkTexture_43);
		RenderTexture_t101 * L_140 = (__this->___mediumRezWorkTexture_43);
		float L_141 = (__this->___maxBlurSpread_21);
		DepthOfFieldDeprecated_BlurFg_m283(__this, L_139, L_140, 1, 2, L_141, /*hidden argument*/NULL);
		bool L_142 = (__this->___bokeh_31);
		if (!L_142)
		{
			goto IL_0578;
		}
	}
	{
		int32_t L_143 = (__this->___bokehDestination_28);
		if (!((int32_t)((int32_t)2&(int32_t)L_143)))
		{
			goto IL_0578;
		}
	}
	{
		Material_t55 * L_144 = (__this->___dofMaterial_26);
		float L_145 = (__this->___bokehThresholdContrast_37);
		float L_146 = (__this->___bokehThresholdLuminance_38);
		Vector4_t236  L_147 = {0};
		Vector4__ctor_m751(&L_147, ((float)((float)L_145*(float)(0.5f))), L_146, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_144);
		Material_SetVector_m752(L_144, (String_t*) &_stringLiteral24, L_147, /*hidden argument*/NULL);
		RenderTexture_t101 * L_148 = (__this->___mediumRezWorkTexture_43);
		RenderTexture_t101 * L_149 = (__this->___bokehSource2_47);
		Material_t55 * L_150 = (__this->___dofMaterial_26);
		Graphics_Blit_m742(NULL /*static, unused*/, L_148, L_149, L_150, ((int32_t)11), /*hidden argument*/NULL);
		RenderTexture_t101 * L_151 = (__this->___mediumRezWorkTexture_43);
		RenderTexture_t101 * L_152 = (__this->___lowRezWorkTexture_45);
		Graphics_Blit_m737(NULL /*static, unused*/, L_151, L_152, /*hidden argument*/NULL);
		RenderTexture_t101 * L_153 = (__this->___lowRezWorkTexture_45);
		RenderTexture_t101 * L_154 = (__this->___lowRezWorkTexture_45);
		int32_t L_155 = (__this->___bluriness_20);
		float L_156 = (__this->___maxBlurSpread_21);
		float L_157 = V_0;
		DepthOfFieldDeprecated_BlurFg_m283(__this, L_153, L_154, L_155, 1, ((float)((float)L_156*(float)L_157)), /*hidden argument*/NULL);
		goto IL_0597;
	}

IL_0578:
	{
		RenderTexture_t101 * L_158 = (__this->___mediumRezWorkTexture_43);
		RenderTexture_t101 * L_159 = (__this->___lowRezWorkTexture_45);
		int32_t L_160 = (__this->___bluriness_20);
		float L_161 = (__this->___maxBlurSpread_21);
		DepthOfFieldDeprecated_BlurFg_m283(__this, L_158, L_159, L_160, 1, L_161, /*hidden argument*/NULL);
	}

IL_0597:
	{
		RenderTexture_t101 * L_162 = (__this->___lowRezWorkTexture_45);
		RenderTexture_t101 * L_163 = (__this->___finalDefocus_44);
		Graphics_Blit_m737(NULL /*static, unused*/, L_162, L_163, /*hidden argument*/NULL);
		Material_t55 * L_164 = (__this->___dofMaterial_26);
		RenderTexture_t101 * L_165 = (__this->___finalDefocus_44);
		NullCheck(L_164);
		Material_SetTexture_m759(L_164, (String_t*) &_stringLiteral95, L_165, /*hidden argument*/NULL);
		RenderTexture_t101 * L_166 = ___source;
		RenderTexture_t101 * L_167 = ___destination;
		Material_t55 * L_168 = (__this->___dofMaterial_26);
		bool L_169 = (__this->___visualize_27);
		G_B50_0 = L_168;
		G_B50_1 = L_167;
		G_B50_2 = L_166;
		if (!L_169)
		{
			G_B51_0 = L_168;
			G_B51_1 = L_167;
			G_B51_2 = L_166;
			goto IL_05d7;
		}
	}
	{
		G_B52_0 = 1;
		G_B52_1 = G_B50_0;
		G_B52_2 = G_B50_1;
		G_B52_3 = G_B50_2;
		goto IL_05d8;
	}

IL_05d7:
	{
		G_B52_0 = 4;
		G_B52_1 = G_B51_0;
		G_B52_2 = G_B51_1;
		G_B52_3 = G_B51_2;
	}

IL_05d8:
	{
		Graphics_Blit_m742(NULL /*static, unused*/, G_B52_3, G_B52_2, G_B52_1, G_B52_0, /*hidden argument*/NULL);
		bool L_170 = (__this->___bokeh_31);
		if (!L_170)
		{
			goto IL_0608;
		}
	}
	{
		int32_t L_171 = (__this->___bokehDestination_28);
		if (!((int32_t)((int32_t)2&(int32_t)L_171)))
		{
			goto IL_0608;
		}
	}
	{
		RenderTexture_t101 * L_172 = (__this->___bokehSource2_47);
		RenderTexture_t101 * L_173 = (__this->___bokehSource_46);
		RenderTexture_t101 * L_174 = ___destination;
		DepthOfFieldDeprecated_AddBokeh_m286(__this, L_172, L_173, L_174, /*hidden argument*/NULL);
	}

IL_0608:
	{
		DepthOfFieldDeprecated_ReleaseTextures_m287(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::Blur(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness,System.Int32,System.Single)
extern "C" void DepthOfFieldDeprecated_Blur_m282 (DepthOfFieldDeprecated_t102 * __this, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, int32_t ___iterations, int32_t ___blurPass, float ___spread, const MethodInfo* method)
{
	RenderTexture_t101 * V_0 = {0};
	{
		RenderTexture_t101 * L_0 = ___to;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_0);
		RenderTexture_t101 * L_2 = ___to;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_2);
		RenderTexture_t101 * L_4 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = ___iterations;
		if ((((int32_t)L_5) <= ((int32_t)1)))
		{
			goto IL_00b1;
		}
	}
	{
		RenderTexture_t101 * L_6 = ___from;
		RenderTexture_t101 * L_7 = ___to;
		int32_t L_8 = ___blurPass;
		float L_9 = ___spread;
		RenderTexture_t101 * L_10 = V_0;
		DepthOfFieldDeprecated_BlurHex_m284(__this, L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		int32_t L_11 = ___iterations;
		if ((((int32_t)L_11) <= ((int32_t)2)))
		{
			goto IL_00ac;
		}
	}
	{
		Material_t55 * L_12 = (__this->___dofBlurMaterial_24);
		float L_13 = ___spread;
		float L_14 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_15 = {0};
		Vector4__ctor_m751(&L_15, (0.0f), ((float)((float)L_13*(float)L_14)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		Material_SetVector_m752(L_12, (String_t*) &_stringLiteral34, L_15, /*hidden argument*/NULL);
		RenderTexture_t101 * L_16 = ___to;
		RenderTexture_t101 * L_17 = V_0;
		Material_t55 * L_18 = (__this->___dofBlurMaterial_24);
		int32_t L_19 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		Material_t55 * L_20 = (__this->___dofBlurMaterial_24);
		float L_21 = ___spread;
		float L_22 = (__this->___widthOverHeight_29);
		float L_23 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_24 = {0};
		Vector4__ctor_m751(&L_24, ((float)((float)((float)((float)L_21/(float)L_22))*(float)L_23)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_20);
		Material_SetVector_m752(L_20, (String_t*) &_stringLiteral34, L_24, /*hidden argument*/NULL);
		RenderTexture_t101 * L_25 = V_0;
		RenderTexture_t101 * L_26 = ___to;
		Material_t55 * L_27 = (__this->___dofBlurMaterial_24);
		int32_t L_28 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_25, L_26, L_27, L_28, /*hidden argument*/NULL);
	}

IL_00ac:
	{
		goto IL_0130;
	}

IL_00b1:
	{
		Material_t55 * L_29 = (__this->___dofBlurMaterial_24);
		float L_30 = ___spread;
		float L_31 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_32 = {0};
		Vector4__ctor_m751(&L_32, (0.0f), ((float)((float)L_30*(float)L_31)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_29);
		Material_SetVector_m752(L_29, (String_t*) &_stringLiteral34, L_32, /*hidden argument*/NULL);
		RenderTexture_t101 * L_33 = ___from;
		RenderTexture_t101 * L_34 = V_0;
		Material_t55 * L_35 = (__this->___dofBlurMaterial_24);
		int32_t L_36 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_33, L_34, L_35, L_36, /*hidden argument*/NULL);
		Material_t55 * L_37 = (__this->___dofBlurMaterial_24);
		float L_38 = ___spread;
		float L_39 = (__this->___widthOverHeight_29);
		float L_40 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_41 = {0};
		Vector4__ctor_m751(&L_41, ((float)((float)((float)((float)L_38/(float)L_39))*(float)L_40)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_37);
		Material_SetVector_m752(L_37, (String_t*) &_stringLiteral34, L_41, /*hidden argument*/NULL);
		RenderTexture_t101 * L_42 = V_0;
		RenderTexture_t101 * L_43 = ___to;
		Material_t55 * L_44 = (__this->___dofBlurMaterial_24);
		int32_t L_45 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_42, L_43, L_44, L_45, /*hidden argument*/NULL);
	}

IL_0130:
	{
		RenderTexture_t101 * L_46 = V_0;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BlurFg(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness,System.Int32,System.Single)
extern "C" void DepthOfFieldDeprecated_BlurFg_m283 (DepthOfFieldDeprecated_t102 * __this, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, int32_t ___iterations, int32_t ___blurPass, float ___spread, const MethodInfo* method)
{
	RenderTexture_t101 * V_0 = {0};
	{
		Material_t55 * L_0 = (__this->___dofBlurMaterial_24);
		RenderTexture_t101 * L_1 = ___from;
		NullCheck(L_0);
		Material_SetTexture_m759(L_0, (String_t*) &_stringLiteral96, L_1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_2 = ___to;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_2);
		RenderTexture_t101 * L_4 = ___to;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_4);
		RenderTexture_t101 * L_6 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		int32_t L_7 = ___iterations;
		if ((((int32_t)L_7) <= ((int32_t)1)))
		{
			goto IL_00c2;
		}
	}
	{
		RenderTexture_t101 * L_8 = ___from;
		RenderTexture_t101 * L_9 = ___to;
		int32_t L_10 = ___blurPass;
		float L_11 = ___spread;
		RenderTexture_t101 * L_12 = V_0;
		DepthOfFieldDeprecated_BlurHex_m284(__this, L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		int32_t L_13 = ___iterations;
		if ((((int32_t)L_13) <= ((int32_t)2)))
		{
			goto IL_00bd;
		}
	}
	{
		Material_t55 * L_14 = (__this->___dofBlurMaterial_24);
		float L_15 = ___spread;
		float L_16 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_17 = {0};
		Vector4__ctor_m751(&L_17, (0.0f), ((float)((float)L_15*(float)L_16)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		Material_SetVector_m752(L_14, (String_t*) &_stringLiteral34, L_17, /*hidden argument*/NULL);
		RenderTexture_t101 * L_18 = ___to;
		RenderTexture_t101 * L_19 = V_0;
		Material_t55 * L_20 = (__this->___dofBlurMaterial_24);
		int32_t L_21 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		Material_t55 * L_22 = (__this->___dofBlurMaterial_24);
		float L_23 = ___spread;
		float L_24 = (__this->___widthOverHeight_29);
		float L_25 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_26 = {0};
		Vector4__ctor_m751(&L_26, ((float)((float)((float)((float)L_23/(float)L_24))*(float)L_25)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_22);
		Material_SetVector_m752(L_22, (String_t*) &_stringLiteral34, L_26, /*hidden argument*/NULL);
		RenderTexture_t101 * L_27 = V_0;
		RenderTexture_t101 * L_28 = ___to;
		Material_t55 * L_29 = (__this->___dofBlurMaterial_24);
		int32_t L_30 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_27, L_28, L_29, L_30, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		goto IL_0141;
	}

IL_00c2:
	{
		Material_t55 * L_31 = (__this->___dofBlurMaterial_24);
		float L_32 = ___spread;
		float L_33 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_34 = {0};
		Vector4__ctor_m751(&L_34, (0.0f), ((float)((float)L_32*(float)L_33)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_31);
		Material_SetVector_m752(L_31, (String_t*) &_stringLiteral34, L_34, /*hidden argument*/NULL);
		RenderTexture_t101 * L_35 = ___from;
		RenderTexture_t101 * L_36 = V_0;
		Material_t55 * L_37 = (__this->___dofBlurMaterial_24);
		int32_t L_38 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_35, L_36, L_37, L_38, /*hidden argument*/NULL);
		Material_t55 * L_39 = (__this->___dofBlurMaterial_24);
		float L_40 = ___spread;
		float L_41 = (__this->___widthOverHeight_29);
		float L_42 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_43 = {0};
		Vector4__ctor_m751(&L_43, ((float)((float)((float)((float)L_40/(float)L_41))*(float)L_42)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_39);
		Material_SetVector_m752(L_39, (String_t*) &_stringLiteral34, L_43, /*hidden argument*/NULL);
		RenderTexture_t101 * L_44 = V_0;
		RenderTexture_t101 * L_45 = ___to;
		Material_t55 * L_46 = (__this->___dofBlurMaterial_24);
		int32_t L_47 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_44, L_45, L_46, L_47, /*hidden argument*/NULL);
	}

IL_0141:
	{
		RenderTexture_t101 * L_48 = V_0;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BlurHex(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32,System.Single,UnityEngine.RenderTexture)
extern "C" void DepthOfFieldDeprecated_BlurHex_m284 (DepthOfFieldDeprecated_t102 * __this, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, int32_t ___blurPass, float ___spread, RenderTexture_t101 * ___tmp, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___dofBlurMaterial_24);
		float L_1 = ___spread;
		float L_2 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_3 = {0};
		Vector4__ctor_m751(&L_3, (0.0f), ((float)((float)L_1*(float)L_2)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m752(L_0, (String_t*) &_stringLiteral34, L_3, /*hidden argument*/NULL);
		RenderTexture_t101 * L_4 = ___from;
		RenderTexture_t101 * L_5 = ___tmp;
		Material_t55 * L_6 = (__this->___dofBlurMaterial_24);
		int32_t L_7 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		Material_t55 * L_8 = (__this->___dofBlurMaterial_24);
		float L_9 = ___spread;
		float L_10 = (__this->___widthOverHeight_29);
		float L_11 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_12 = {0};
		Vector4__ctor_m751(&L_12, ((float)((float)((float)((float)L_9/(float)L_10))*(float)L_11)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Material_SetVector_m752(L_8, (String_t*) &_stringLiteral34, L_12, /*hidden argument*/NULL);
		RenderTexture_t101 * L_13 = ___tmp;
		RenderTexture_t101 * L_14 = ___to;
		Material_t55 * L_15 = (__this->___dofBlurMaterial_24);
		int32_t L_16 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		Material_t55 * L_17 = (__this->___dofBlurMaterial_24);
		float L_18 = ___spread;
		float L_19 = (__this->___widthOverHeight_29);
		float L_20 = (__this->___oneOverBaseSize_30);
		float L_21 = ___spread;
		float L_22 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_23 = {0};
		Vector4__ctor_m751(&L_23, ((float)((float)((float)((float)L_18/(float)L_19))*(float)L_20)), ((float)((float)L_21*(float)L_22)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		Material_SetVector_m752(L_17, (String_t*) &_stringLiteral34, L_23, /*hidden argument*/NULL);
		RenderTexture_t101 * L_24 = ___to;
		RenderTexture_t101 * L_25 = ___tmp;
		Material_t55 * L_26 = (__this->___dofBlurMaterial_24);
		int32_t L_27 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_24, L_25, L_26, L_27, /*hidden argument*/NULL);
		Material_t55 * L_28 = (__this->___dofBlurMaterial_24);
		float L_29 = ___spread;
		float L_30 = (__this->___widthOverHeight_29);
		float L_31 = (__this->___oneOverBaseSize_30);
		float L_32 = ___spread;
		float L_33 = (__this->___oneOverBaseSize_30);
		Vector4_t236  L_34 = {0};
		Vector4__ctor_m751(&L_34, ((float)((float)((float)((float)L_29/(float)L_30))*(float)L_31)), ((float)((float)((-L_32))*(float)L_33)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_28);
		Material_SetVector_m752(L_28, (String_t*) &_stringLiteral34, L_34, /*hidden argument*/NULL);
		RenderTexture_t101 * L_35 = ___tmp;
		RenderTexture_t101 * L_36 = ___to;
		Material_t55 * L_37 = (__this->___dofBlurMaterial_24);
		int32_t L_38 = ___blurPass;
		Graphics_Blit_m742(NULL /*static, unused*/, L_35, L_36, L_37, L_38, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::Downsample(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var;
extern "C" void DepthOfFieldDeprecated_Downsample_m285 (DepthOfFieldDeprecated_t102 * __this, RenderTexture_t101 * ___from, RenderTexture_t101 * ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t55 * L_0 = (__this->___dofMaterial_26);
		RenderTexture_t101 * L_1 = ___to;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_1);
		RenderTexture_t101 * L_3 = ___to;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_3);
		Vector4_t236  L_5 = {0};
		Vector4__ctor_m751(&L_5, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)L_2)))))), ((float)((float)(1.0f)/(float)((float)((float)(1.0f)*(float)(((float)L_4)))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_SetVector_m752(L_0, (String_t*) &_stringLiteral91, L_5, /*hidden argument*/NULL);
		RenderTexture_t101 * L_6 = ___from;
		RenderTexture_t101 * L_7 = ___to;
		Material_t55 * L_8 = (__this->___dofMaterial_26);
		IL2CPP_RUNTIME_CLASS_INIT(DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var);
		int32_t L_9 = ((DepthOfFieldDeprecated_t102_StaticFields*)DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var->static_fields)->___SMOOTH_DOWNSAMPLE_PASS_5;
		Graphics_Blit_m742(NULL /*static, unused*/, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::AddBokeh(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* Quads_t114_il2cpp_TypeInfo_var;
extern TypeInfo* DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var;
extern "C" void DepthOfFieldDeprecated_AddBokeh_m286 (DepthOfFieldDeprecated_t102 * __this, RenderTexture_t101 * ___bokehInfo, RenderTexture_t101 * ___tempTex, RenderTexture_t101 * ___finalTarget, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quads_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(56);
		DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	MeshU5BU5D_t113* V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Mesh_t216 * V_3 = {0};
	MeshU5BU5D_t113* V_4 = {0};
	int32_t V_5 = 0;
	{
		Material_t55 * L_0 = (__this->___bokehMaterial_40);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0167;
		}
	}
	{
		RenderTexture_t101 * L_2 = ___tempTex;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_2);
		RenderTexture_t101 * L_4 = ___tempTex;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_6 = Quads_GetMeshes_m350(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		RenderTexture_t101 * L_7 = ___tempTex;
		RenderTexture_set_active_m835(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Color_t65  L_8 = {0};
		Color__ctor_m746(&L_8, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GL_Clear_m755(NULL /*static, unused*/, 0, 1, L_8, /*hidden argument*/NULL);
		GL_PushMatrix_m836(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadIdentity_m837(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t101 * L_9 = ___bokehInfo;
		NullCheck(L_9);
		Texture_set_filterMode_m762(L_9, 0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_10 = ___bokehInfo;
		NullCheck(L_10);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_10);
		RenderTexture_t101 * L_12 = ___bokehInfo;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_12);
		V_1 = ((float)((float)((float)((float)(((float)L_11))*(float)(1.0f)))/(float)((float)((float)(((float)L_13))*(float)(1.0f)))));
		RenderTexture_t101 * L_14 = ___bokehInfo;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_14);
		V_2 = ((float)((float)(2.0f)/(float)((float)((float)(1.0f)*(float)(((float)L_15))))));
		float L_16 = V_2;
		float L_17 = (__this->___bokehScale_35);
		float L_18 = (__this->___maxBlurSpread_21);
		IL2CPP_RUNTIME_CLASS_INIT(DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var);
		float L_19 = ((DepthOfFieldDeprecated_t102_StaticFields*)DepthOfFieldDeprecated_t102_il2cpp_TypeInfo_var->static_fields)->___BOKEH_EXTRA_BLUR_6;
		float L_20 = (__this->___oneOverBaseSize_30);
		V_2 = ((float)((float)L_16+(float)((float)((float)((float)((float)((float)((float)L_17*(float)L_18))*(float)L_19))*(float)L_20))));
		Material_t55 * L_21 = (__this->___bokehMaterial_40);
		RenderTexture_t101 * L_22 = ___bokehInfo;
		NullCheck(L_21);
		Material_SetTexture_m759(L_21, (String_t*) &_stringLiteral97, L_22, /*hidden argument*/NULL);
		Material_t55 * L_23 = (__this->___bokehMaterial_40);
		Texture2D_t63 * L_24 = (__this->___bokehTexture_34);
		NullCheck(L_23);
		Material_SetTexture_m759(L_23, (String_t*) &_stringLiteral87, L_24, /*hidden argument*/NULL);
		Material_t55 * L_25 = (__this->___bokehMaterial_40);
		float L_26 = V_2;
		float L_27 = V_2;
		float L_28 = V_1;
		float L_29 = V_1;
		Vector4_t236  L_30 = {0};
		Vector4__ctor_m751(&L_30, L_26, ((float)((float)L_27*(float)L_28)), (0.5f), ((float)((float)(0.5f)*(float)L_29)), /*hidden argument*/NULL);
		NullCheck(L_25);
		Material_SetVector_m752(L_25, (String_t*) &_stringLiteral98, L_30, /*hidden argument*/NULL);
		Material_t55 * L_31 = (__this->___bokehMaterial_40);
		float L_32 = (__this->___bokehIntensity_36);
		NullCheck(L_31);
		Material_SetFloat_m738(L_31, (String_t*) &_stringLiteral28, L_32, /*hidden argument*/NULL);
		Material_t55 * L_33 = (__this->___bokehMaterial_40);
		NullCheck(L_33);
		Material_SetPass_m831(L_33, 0, /*hidden argument*/NULL);
		MeshU5BU5D_t113* L_34 = V_0;
		V_4 = L_34;
		V_5 = 0;
		goto IL_0142;
	}

IL_0120:
	{
		MeshU5BU5D_t113* L_35 = V_4;
		int32_t L_36 = V_5;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
		int32_t L_37 = L_36;
		V_3 = (*(Mesh_t216 **)(Mesh_t216 **)SZArrayLdElema(L_35, L_37));
		Mesh_t216 * L_38 = V_3;
		bool L_39 = Object_op_Implicit_m629(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_013c;
		}
	}
	{
		Mesh_t216 * L_40 = V_3;
		Matrix4x4_t80  L_41 = Matrix4x4_get_identity_m787(NULL /*static, unused*/, /*hidden argument*/NULL);
		Graphics_DrawMeshNow_m838(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
	}

IL_013c:
	{
		int32_t L_42 = V_5;
		V_5 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_0142:
	{
		int32_t L_43 = V_5;
		MeshU5BU5D_t113* L_44 = V_4;
		NullCheck(L_44);
		if ((((int32_t)L_43) < ((int32_t)(((int32_t)(((Array_t *)L_44)->max_length))))))
		{
			goto IL_0120;
		}
	}
	{
		GL_PopMatrix_m839(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t101 * L_45 = ___tempTex;
		RenderTexture_t101 * L_46 = ___finalTarget;
		Material_t55 * L_47 = (__this->___dofMaterial_26);
		Graphics_Blit_m742(NULL /*static, unused*/, L_45, L_46, L_47, 8, /*hidden argument*/NULL);
		RenderTexture_t101 * L_48 = ___bokehInfo;
		NullCheck(L_48);
		Texture_set_filterMode_m762(L_48, 1, /*hidden argument*/NULL);
	}

IL_0167:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::ReleaseTextures()
extern "C" void DepthOfFieldDeprecated_ReleaseTextures_m287 (DepthOfFieldDeprecated_t102 * __this, const MethodInfo* method)
{
	{
		RenderTexture_t101 * L_0 = (__this->___foregroundTexture_42);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		RenderTexture_t101 * L_2 = (__this->___foregroundTexture_42);
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		RenderTexture_t101 * L_3 = (__this->___finalDefocus_44);
		bool L_4 = Object_op_Implicit_m629(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		RenderTexture_t101 * L_5 = (__this->___finalDefocus_44);
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		RenderTexture_t101 * L_6 = (__this->___mediumRezWorkTexture_43);
		bool L_7 = Object_op_Implicit_m629(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		RenderTexture_t101 * L_8 = (__this->___mediumRezWorkTexture_43);
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0051:
	{
		RenderTexture_t101 * L_9 = (__this->___lowRezWorkTexture_45);
		bool L_10 = Object_op_Implicit_m629(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006c;
		}
	}
	{
		RenderTexture_t101 * L_11 = (__this->___lowRezWorkTexture_45);
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_006c:
	{
		RenderTexture_t101 * L_12 = (__this->___bokehSource_46);
		bool L_13 = Object_op_Implicit_m629(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0087;
		}
	}
	{
		RenderTexture_t101 * L_14 = (__this->___bokehSource_46);
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0087:
	{
		RenderTexture_t101 * L_15 = (__this->___bokehSource2_47);
		bool L_16 = Object_op_Implicit_m629(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00a2;
		}
	}
	{
		RenderTexture_t101 * L_17 = (__this->___bokehSource2_47);
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
	}

IL_00a2:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::AllocateTextures(System.Boolean,UnityEngine.RenderTexture,System.Int32,System.Int32)
extern "C" void DepthOfFieldDeprecated_AllocateTextures_m288 (DepthOfFieldDeprecated_t102 * __this, bool ___blurForeground, RenderTexture_t101 * ___source, int32_t ___divider, int32_t ___lowTexDivider, const MethodInfo* method)
{
	{
		__this->___foregroundTexture_42 = (RenderTexture_t101 *)NULL;
		bool L_0 = ___blurForeground;
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_1);
		RenderTexture_t101 * L_3 = ___source;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_3);
		RenderTexture_t101 * L_5 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, L_2, L_4, 0, /*hidden argument*/NULL);
		__this->___foregroundTexture_42 = L_5;
	}

IL_0025:
	{
		RenderTexture_t101 * L_6 = ___source;
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_6);
		int32_t L_8 = ___divider;
		RenderTexture_t101 * L_9 = ___source;
		NullCheck(L_9);
		int32_t L_10 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_9);
		int32_t L_11 = ___divider;
		RenderTexture_t101 * L_12 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_7/(int32_t)L_8)), ((int32_t)((int32_t)L_10/(int32_t)L_11)), 0, /*hidden argument*/NULL);
		__this->___mediumRezWorkTexture_43 = L_12;
		RenderTexture_t101 * L_13 = ___source;
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_13);
		int32_t L_15 = ___divider;
		RenderTexture_t101 * L_16 = ___source;
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_16);
		int32_t L_18 = ___divider;
		RenderTexture_t101 * L_19 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_14/(int32_t)L_15)), ((int32_t)((int32_t)L_17/(int32_t)L_18)), 0, /*hidden argument*/NULL);
		__this->___finalDefocus_44 = L_19;
		RenderTexture_t101 * L_20 = ___source;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_20);
		int32_t L_22 = ___lowTexDivider;
		RenderTexture_t101 * L_23 = ___source;
		NullCheck(L_23);
		int32_t L_24 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_23);
		int32_t L_25 = ___lowTexDivider;
		RenderTexture_t101 * L_26 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_21/(int32_t)L_22)), ((int32_t)((int32_t)L_24/(int32_t)L_25)), 0, /*hidden argument*/NULL);
		__this->___lowRezWorkTexture_45 = L_26;
		__this->___bokehSource_46 = (RenderTexture_t101 *)NULL;
		__this->___bokehSource2_47 = (RenderTexture_t101 *)NULL;
		bool L_27 = (__this->___bokeh_31);
		if (!L_27)
		{
			goto IL_0131;
		}
	}
	{
		RenderTexture_t101 * L_28 = ___source;
		NullCheck(L_28);
		int32_t L_29 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_28);
		int32_t L_30 = ___lowTexDivider;
		int32_t L_31 = (__this->___bokehDownsample_39);
		RenderTexture_t101 * L_32 = ___source;
		NullCheck(L_32);
		int32_t L_33 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_32);
		int32_t L_34 = ___lowTexDivider;
		int32_t L_35 = (__this->___bokehDownsample_39);
		RenderTexture_t101 * L_36 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_29/(int32_t)((int32_t)((int32_t)L_30*(int32_t)L_31)))), ((int32_t)((int32_t)L_33/(int32_t)((int32_t)((int32_t)L_34*(int32_t)L_35)))), 0, 2, /*hidden argument*/NULL);
		__this->___bokehSource_46 = L_36;
		RenderTexture_t101 * L_37 = ___source;
		NullCheck(L_37);
		int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_37);
		int32_t L_39 = ___lowTexDivider;
		int32_t L_40 = (__this->___bokehDownsample_39);
		RenderTexture_t101 * L_41 = ___source;
		NullCheck(L_41);
		int32_t L_42 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_41);
		int32_t L_43 = ___lowTexDivider;
		int32_t L_44 = (__this->___bokehDownsample_39);
		RenderTexture_t101 * L_45 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_38/(int32_t)((int32_t)((int32_t)L_39*(int32_t)L_40)))), ((int32_t)((int32_t)L_42/(int32_t)((int32_t)((int32_t)L_43*(int32_t)L_44)))), 0, 2, /*hidden argument*/NULL);
		__this->___bokehSource2_47 = L_45;
		RenderTexture_t101 * L_46 = (__this->___bokehSource_46);
		NullCheck(L_46);
		Texture_set_filterMode_m762(L_46, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_47 = (__this->___bokehSource2_47);
		NullCheck(L_47);
		Texture_set_filterMode_m762(L_47, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_48 = (__this->___bokehSource2_47);
		RenderTexture_set_active_m835(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		Color_t65  L_49 = {0};
		Color__ctor_m746(&L_49, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GL_Clear_m755(NULL /*static, unused*/, 0, 1, L_49, /*hidden argument*/NULL);
	}

IL_0131:
	{
		RenderTexture_t101 * L_50 = ___source;
		NullCheck(L_50);
		Texture_set_filterMode_m762(L_50, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_51 = (__this->___finalDefocus_44);
		NullCheck(L_51);
		Texture_set_filterMode_m762(L_51, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_52 = (__this->___mediumRezWorkTexture_43);
		NullCheck(L_52);
		Texture_set_filterMode_m762(L_52, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_53 = (__this->___lowRezWorkTexture_45);
		NullCheck(L_53);
		Texture_set_filterMode_m762(L_53, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_54 = (__this->___foregroundTexture_42);
		bool L_55 = Object_op_Implicit_m629(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0178;
		}
	}
	{
		RenderTexture_t101 * L_56 = (__this->___foregroundTexture_42);
		NullCheck(L_56);
		Texture_set_filterMode_m762(L_56, 1, /*hidden argument*/NULL);
	}

IL_0178:
	{
		return;
	}
}
// UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_37.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.EdgeDetection/EdgeDetectMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_37MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.EdgeDetection
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_38.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.EdgeDetection
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_38MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::.ctor()
extern "C" void EdgeDetection__ctor_m289 (EdgeDetection_t104 * __this, const MethodInfo* method)
{
	{
		__this->___mode_5 = 3;
		__this->___sensitivityDepth_6 = (1.0f);
		__this->___sensitivityNormals_7 = (1.0f);
		__this->___lumThreshold_8 = (0.2f);
		__this->___edgeExp_9 = (1.0f);
		__this->___sampleDist_10 = (1.0f);
		Color_t65  L_0 = Color_get_white_m745(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___edgesOnlyBgColor_12 = L_0;
		__this->___oldMode_15 = 3;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.EdgeDetection::CheckResources()
extern "C" bool EdgeDetection_CheckResources_m290 (EdgeDetection_t104 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 1, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___edgeDetectShader_13);
		Material_t55 * L_1 = (__this->___edgeDetectMaterial_14);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___edgeDetectMaterial_14 = L_2;
		int32_t L_3 = (__this->___mode_5);
		int32_t L_4 = (__this->___oldMode_15);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0037;
		}
	}
	{
		EdgeDetection_SetCameraFlag_m292(__this, /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_5 = (__this->___mode_5);
		__this->___oldMode_15 = L_5;
		bool L_6 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0054:
	{
		bool L_7 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_7;
	}
}
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::Start()
extern "C" void EdgeDetection_Start_m291 (EdgeDetection_t104 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mode_5);
		__this->___oldMode_15 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::SetCameraFlag()
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void EdgeDetection_SetCameraFlag_m292 (EdgeDetection_t104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___mode_5);
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = (__this->___mode_5);
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0030;
		}
	}

IL_0018:
	{
		Camera_t27 * L_2 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		Camera_t27 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = Camera_get_depthTextureMode_m779(L_3, /*hidden argument*/NULL);
		NullCheck(L_3);
		Camera_set_depthTextureMode_m780(L_3, ((int32_t)((int32_t)L_4|(int32_t)1)), /*hidden argument*/NULL);
		goto IL_005a;
	}

IL_0030:
	{
		int32_t L_5 = (__this->___mode_5);
		if (!L_5)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_6 = (__this->___mode_5);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_005a;
		}
	}

IL_0047:
	{
		Camera_t27 * L_7 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		Camera_t27 * L_8 = L_7;
		NullCheck(L_8);
		int32_t L_9 = Camera_get_depthTextureMode_m779(L_8, /*hidden argument*/NULL);
		NullCheck(L_8);
		Camera_set_depthTextureMode_m780(L_8, ((int32_t)((int32_t)L_9|(int32_t)2)), /*hidden argument*/NULL);
	}

IL_005a:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnEnable()
extern "C" void EdgeDetection_OnEnable_m293 (EdgeDetection_t104 * __this, const MethodInfo* method)
{
	{
		EdgeDetection_SetCameraFlag_m292(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void EdgeDetection_OnRenderImage_m294 (EdgeDetection_t104 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	Vector2_t6  V_0 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.EdgeDetection::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		float L_3 = (__this->___sensitivityDepth_6);
		float L_4 = (__this->___sensitivityNormals_7);
		Vector2__ctor_m630((&V_0), L_3, L_4, /*hidden argument*/NULL);
		Material_t55 * L_5 = (__this->___edgeDetectMaterial_14);
		float L_6 = ((&V_0)->___x_1);
		float L_7 = ((&V_0)->___y_2);
		float L_8 = ((&V_0)->___y_2);
		Vector4_t236  L_9 = {0};
		Vector4__ctor_m751(&L_9, L_6, L_7, (1.0f), L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_SetVector_m752(L_5, (String_t*) &_stringLiteral99, L_9, /*hidden argument*/NULL);
		Material_t55 * L_10 = (__this->___edgeDetectMaterial_14);
		float L_11 = (__this->___edgesOnly_11);
		NullCheck(L_10);
		Material_SetFloat_m738(L_10, (String_t*) &_stringLiteral100, L_11, /*hidden argument*/NULL);
		Material_t55 * L_12 = (__this->___edgeDetectMaterial_14);
		float L_13 = (__this->___sampleDist_10);
		NullCheck(L_12);
		Material_SetFloat_m738(L_12, (String_t*) &_stringLiteral101, L_13, /*hidden argument*/NULL);
		Material_t55 * L_14 = (__this->___edgeDetectMaterial_14);
		Color_t65  L_15 = (__this->___edgesOnlyBgColor_12);
		Vector4_t236  L_16 = Color_op_Implicit_m760(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Material_SetVector_m752(L_14, (String_t*) &_stringLiteral102, L_16, /*hidden argument*/NULL);
		Material_t55 * L_17 = (__this->___edgeDetectMaterial_14);
		float L_18 = (__this->___edgeExp_9);
		NullCheck(L_17);
		Material_SetFloat_m738(L_17, (String_t*) &_stringLiteral103, L_18, /*hidden argument*/NULL);
		Material_t55 * L_19 = (__this->___edgeDetectMaterial_14);
		float L_20 = (__this->___lumThreshold_8);
		NullCheck(L_19);
		Material_SetFloat_m738(L_19, (String_t*) &_stringLiteral104, L_20, /*hidden argument*/NULL);
		RenderTexture_t101 * L_21 = ___source;
		RenderTexture_t101 * L_22 = ___destination;
		Material_t55 * L_23 = (__this->___edgeDetectMaterial_14);
		int32_t L_24 = (__this->___mode_5);
		Graphics_Blit_m742(NULL /*static, unused*/, L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.Fisheye
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_39.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Fisheye
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_39MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.Fisheye::.ctor()
extern "C" void Fisheye__ctor_m295 (Fisheye_t105 * __this, const MethodInfo* method)
{
	{
		__this->___strengthX_5 = (0.05f);
		__this->___strengthY_6 = (0.05f);
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.Fisheye::CheckResources()
extern "C" bool Fisheye_CheckResources_m296 (Fisheye_t105 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___fishEyeShader_7);
		Material_t55 * L_1 = (__this->___fisheyeMaterial_8);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___fisheyeMaterial_8 = L_2;
		bool L_3 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		bool L_4 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_4;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Fisheye::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Fisheye_OnRenderImage_m297 (Fisheye_t105 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.Fisheye::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		V_0 = (0.15625f);
		RenderTexture_t101 * L_3 = ___source;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_3);
		RenderTexture_t101 * L_5 = ___source;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_5);
		V_1 = ((float)((float)((float)((float)(((float)L_4))*(float)(1.0f)))/(float)((float)((float)(((float)L_6))*(float)(1.0f)))));
		Material_t55 * L_7 = (__this->___fisheyeMaterial_8);
		float L_8 = (__this->___strengthX_5);
		float L_9 = V_1;
		float L_10 = V_0;
		float L_11 = (__this->___strengthY_6);
		float L_12 = V_0;
		float L_13 = (__this->___strengthX_5);
		float L_14 = V_1;
		float L_15 = V_0;
		float L_16 = (__this->___strengthY_6);
		float L_17 = V_0;
		Vector4_t236  L_18 = {0};
		Vector4__ctor_m751(&L_18, ((float)((float)((float)((float)L_8*(float)L_9))*(float)L_10)), ((float)((float)L_11*(float)L_12)), ((float)((float)((float)((float)L_13*(float)L_14))*(float)L_15)), ((float)((float)L_16*(float)L_17)), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m752(L_7, (String_t*) &_stringLiteral73, L_18, /*hidden argument*/NULL);
		RenderTexture_t101 * L_19 = ___source;
		RenderTexture_t101 * L_20 = ___destination;
		Material_t55 * L_21 = (__this->___fisheyeMaterial_8);
		Graphics_Blit_m739(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.GlobalFog
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_40.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.GlobalFog
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_40MethodDeclarations.h"

// UnityEngine.FogMode
#include "UnityEngine_UnityEngine_FogMode.h"
// UnityEngine.RenderSettings
#include "UnityEngine_UnityEngine_RenderSettingsMethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.GlobalFog::.ctor()
extern "C" void GlobalFog__ctor_m298 (GlobalFog_t106 * __this, const MethodInfo* method)
{
	{
		__this->___distanceFog_5 = 1;
		__this->___heightFog_7 = 1;
		__this->___height_8 = (1.0f);
		__this->___heightDensity_9 = (2.0f);
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::CheckResources()
extern "C" bool GlobalFog_CheckResources_m299 (GlobalFog_t106 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 1, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___fogShader_11);
		Material_t55 * L_1 = (__this->___fogMaterial_12);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___fogMaterial_12 = L_2;
		bool L_3 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		bool L_4 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_4;
	}
}
// System.Void UnityStandardAssets.ImageEffects.GlobalFog::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void GlobalFog_OnRenderImage_m300 (GlobalFog_t106 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	Camera_t27 * V_0 = {0};
	Transform_t1 * V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Matrix4x4_t80  V_6 = {0};
	float V_7 = 0.0f;
	Vector3_t4  V_8 = {0};
	Vector3_t4  V_9 = {0};
	Vector3_t4  V_10 = {0};
	float V_11 = 0.0f;
	Vector3_t4  V_12 = {0};
	Vector3_t4  V_13 = {0};
	Vector3_t4  V_14 = {0};
	Vector3_t4  V_15 = {0};
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	int32_t V_18 = {0};
	float V_19 = 0.0f;
	float V_20 = 0.0f;
	float V_21 = 0.0f;
	Vector4_t236  V_22 = {0};
	bool V_23 = false;
	float V_24 = 0.0f;
	float V_25 = 0.0f;
	int32_t V_26 = 0;
	float G_B7_0 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B13_0 = 0.0f;
	Vector4_t236 * G_B15_0 = {0};
	Vector4_t236 * G_B14_0 = {0};
	float G_B16_0 = 0.0f;
	Vector4_t236 * G_B16_1 = {0};
	Vector4_t236 * G_B18_0 = {0};
	Vector4_t236 * G_B17_0 = {0};
	float G_B19_0 = 0.0f;
	Vector4_t236 * G_B19_1 = {0};
	float G_B21_0 = 0.0f;
	String_t* G_B21_1 = {0};
	Material_t55 * G_B21_2 = {0};
	float G_B20_0 = 0.0f;
	String_t* G_B20_1 = {0};
	Material_t55 * G_B20_2 = {0};
	int32_t G_B22_0 = 0;
	float G_B22_1 = 0.0f;
	String_t* G_B22_2 = {0};
	Material_t55 * G_B22_3 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::CheckResources() */, __this);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = (__this->___distanceFog_5);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		bool L_2 = (__this->___heightFog_7);
		if (L_2)
		{
			goto IL_0029;
		}
	}

IL_0021:
	{
		RenderTexture_t101 * L_3 = ___source;
		RenderTexture_t101 * L_4 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}

IL_0029:
	{
		Camera_t27 * L_5 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		V_0 = L_5;
		Camera_t27 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t1 * L_7 = Component_get_transform_m594(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Camera_t27 * L_8 = V_0;
		NullCheck(L_8);
		float L_9 = Camera_get_nearClipPlane_m823(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Camera_t27 * L_10 = V_0;
		NullCheck(L_10);
		float L_11 = Camera_get_farClipPlane_m825(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		Camera_t27 * L_12 = V_0;
		NullCheck(L_12);
		float L_13 = Camera_get_fieldOfView_m699(L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		Camera_t27 * L_14 = V_0;
		NullCheck(L_14);
		float L_15 = Camera_get_aspect_m840(L_14, /*hidden argument*/NULL);
		V_5 = L_15;
		Matrix4x4_t80  L_16 = Matrix4x4_get_identity_m787(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_16;
		float L_17 = V_4;
		V_7 = ((float)((float)L_17*(float)(0.5f)));
		Transform_t1 * L_18 = V_1;
		NullCheck(L_18);
		Vector3_t4  L_19 = Transform_get_right_m793(L_18, /*hidden argument*/NULL);
		float L_20 = V_2;
		Vector3_t4  L_21 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		float L_22 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_23 = tanf(((float)((float)L_22*(float)(0.0174532924f))));
		Vector3_t4  L_24 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		float L_25 = V_5;
		Vector3_t4  L_26 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Transform_t1 * L_27 = V_1;
		NullCheck(L_27);
		Vector3_t4  L_28 = Transform_get_up_m644(L_27, /*hidden argument*/NULL);
		float L_29 = V_2;
		Vector3_t4  L_30 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		float L_31 = V_7;
		float L_32 = tanf(((float)((float)L_31*(float)(0.0174532924f))));
		Vector3_t4  L_33 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_30, L_32, /*hidden argument*/NULL);
		V_9 = L_33;
		Transform_t1 * L_34 = V_1;
		NullCheck(L_34);
		Vector3_t4  L_35 = Transform_get_forward_m643(L_34, /*hidden argument*/NULL);
		float L_36 = V_2;
		Vector3_t4  L_37 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		Vector3_t4  L_38 = V_8;
		Vector3_t4  L_39 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		Vector3_t4  L_40 = V_9;
		Vector3_t4  L_41 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		V_10 = L_41;
		float L_42 = Vector3_get_magnitude_m647((&V_10), /*hidden argument*/NULL);
		float L_43 = V_3;
		float L_44 = V_2;
		V_11 = ((float)((float)((float)((float)L_42*(float)L_43))/(float)L_44));
		Vector3_Normalize_m841((&V_10), /*hidden argument*/NULL);
		Vector3_t4  L_45 = V_10;
		float L_46 = V_11;
		Vector3_t4  L_47 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		V_10 = L_47;
		Transform_t1 * L_48 = V_1;
		NullCheck(L_48);
		Vector3_t4  L_49 = Transform_get_forward_m643(L_48, /*hidden argument*/NULL);
		float L_50 = V_2;
		Vector3_t4  L_51 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_49, L_50, /*hidden argument*/NULL);
		Vector3_t4  L_52 = V_8;
		Vector3_t4  L_53 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
		Vector3_t4  L_54 = V_9;
		Vector3_t4  L_55 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		V_12 = L_55;
		Vector3_Normalize_m841((&V_12), /*hidden argument*/NULL);
		Vector3_t4  L_56 = V_12;
		float L_57 = V_11;
		Vector3_t4  L_58 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_56, L_57, /*hidden argument*/NULL);
		V_12 = L_58;
		Transform_t1 * L_59 = V_1;
		NullCheck(L_59);
		Vector3_t4  L_60 = Transform_get_forward_m643(L_59, /*hidden argument*/NULL);
		float L_61 = V_2;
		Vector3_t4  L_62 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_60, L_61, /*hidden argument*/NULL);
		Vector3_t4  L_63 = V_8;
		Vector3_t4  L_64 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_62, L_63, /*hidden argument*/NULL);
		Vector3_t4  L_65 = V_9;
		Vector3_t4  L_66 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		V_13 = L_66;
		Vector3_Normalize_m841((&V_13), /*hidden argument*/NULL);
		Vector3_t4  L_67 = V_13;
		float L_68 = V_11;
		Vector3_t4  L_69 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_67, L_68, /*hidden argument*/NULL);
		V_13 = L_69;
		Transform_t1 * L_70 = V_1;
		NullCheck(L_70);
		Vector3_t4  L_71 = Transform_get_forward_m643(L_70, /*hidden argument*/NULL);
		float L_72 = V_2;
		Vector3_t4  L_73 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_71, L_72, /*hidden argument*/NULL);
		Vector3_t4  L_74 = V_8;
		Vector3_t4  L_75 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_73, L_74, /*hidden argument*/NULL);
		Vector3_t4  L_76 = V_9;
		Vector3_t4  L_77 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_75, L_76, /*hidden argument*/NULL);
		V_14 = L_77;
		Vector3_Normalize_m841((&V_14), /*hidden argument*/NULL);
		Vector3_t4  L_78 = V_14;
		float L_79 = V_11;
		Vector3_t4  L_80 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_78, L_79, /*hidden argument*/NULL);
		V_14 = L_80;
		Vector3_t4  L_81 = V_10;
		Vector4_t236  L_82 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_81, /*hidden argument*/NULL);
		Matrix4x4_SetRow_m842((&V_6), 0, L_82, /*hidden argument*/NULL);
		Vector3_t4  L_83 = V_12;
		Vector4_t236  L_84 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		Matrix4x4_SetRow_m842((&V_6), 1, L_84, /*hidden argument*/NULL);
		Vector3_t4  L_85 = V_13;
		Vector4_t236  L_86 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_85, /*hidden argument*/NULL);
		Matrix4x4_SetRow_m842((&V_6), 2, L_86, /*hidden argument*/NULL);
		Vector3_t4  L_87 = V_14;
		Vector4_t236  L_88 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
		Matrix4x4_SetRow_m842((&V_6), 3, L_88, /*hidden argument*/NULL);
		Transform_t1 * L_89 = V_1;
		NullCheck(L_89);
		Vector3_t4  L_90 = Transform_get_position_m593(L_89, /*hidden argument*/NULL);
		V_15 = L_90;
		float L_91 = ((&V_15)->___y_2);
		float L_92 = (__this->___height_8);
		V_16 = ((float)((float)L_91-(float)L_92));
		float L_93 = V_16;
		if ((!(((float)L_93) <= ((float)(0.0f)))))
		{
			goto IL_01dc;
		}
	}
	{
		G_B7_0 = (1.0f);
		goto IL_01e1;
	}

IL_01dc:
	{
		G_B7_0 = (0.0f);
	}

IL_01e1:
	{
		V_17 = G_B7_0;
		Material_t55 * L_94 = (__this->___fogMaterial_12);
		Matrix4x4_t80  L_95 = V_6;
		NullCheck(L_94);
		Material_SetMatrix_m786(L_94, (String_t*) &_stringLiteral105, L_95, /*hidden argument*/NULL);
		Material_t55 * L_96 = (__this->___fogMaterial_12);
		Vector3_t4  L_97 = V_15;
		Vector4_t236  L_98 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_97, /*hidden argument*/NULL);
		NullCheck(L_96);
		Material_SetVector_m752(L_96, (String_t*) &_stringLiteral106, L_98, /*hidden argument*/NULL);
		Material_t55 * L_99 = (__this->___fogMaterial_12);
		float L_100 = (__this->___height_8);
		float L_101 = V_16;
		float L_102 = V_17;
		float L_103 = (__this->___heightDensity_9);
		Vector4_t236  L_104 = {0};
		Vector4__ctor_m751(&L_104, L_100, L_101, L_102, ((float)((float)L_103*(float)(0.5f))), /*hidden argument*/NULL);
		NullCheck(L_99);
		Material_SetVector_m752(L_99, (String_t*) &_stringLiteral107, L_104, /*hidden argument*/NULL);
		Material_t55 * L_105 = (__this->___fogMaterial_12);
		float L_106 = (__this->___startDistance_10);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_107 = Mathf_Max_m782(NULL /*static, unused*/, L_106, (0.0f), /*hidden argument*/NULL);
		Vector4_t236  L_108 = {0};
		Vector4__ctor_m751(&L_108, ((-L_107)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_105);
		Material_SetVector_m752(L_105, (String_t*) &_stringLiteral108, L_108, /*hidden argument*/NULL);
		int32_t L_109 = RenderSettings_get_fogMode_m843(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_18 = L_109;
		float L_110 = RenderSettings_get_fogDensity_m844(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_19 = L_110;
		float L_111 = RenderSettings_get_fogStartDistance_m845(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_20 = L_111;
		float L_112 = RenderSettings_get_fogEndDistance_m846(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_21 = L_112;
		int32_t L_113 = V_18;
		V_23 = ((((int32_t)L_113) == ((int32_t)1))? 1 : 0);
		bool L_114 = V_23;
		if (!L_114)
		{
			goto IL_02a0;
		}
	}
	{
		float L_115 = V_21;
		float L_116 = V_20;
		G_B10_0 = ((float)((float)L_115-(float)L_116));
		goto IL_02a5;
	}

IL_02a0:
	{
		G_B10_0 = (0.0f);
	}

IL_02a5:
	{
		V_24 = G_B10_0;
		float L_117 = V_24;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_118 = fabsf(L_117);
		if ((!(((float)L_118) > ((float)(0.0001f)))))
		{
			goto IL_02c5;
		}
	}
	{
		float L_119 = V_24;
		G_B13_0 = ((float)((float)(1.0f)/(float)L_119));
		goto IL_02ca;
	}

IL_02c5:
	{
		G_B13_0 = (0.0f);
	}

IL_02ca:
	{
		V_25 = G_B13_0;
		float L_120 = V_19;
		(&V_22)->___x_1 = ((float)((float)L_120*(float)(1.2011224f)));
		float L_121 = V_19;
		(&V_22)->___y_2 = ((float)((float)L_121*(float)(1.442695f)));
		bool L_122 = V_23;
		G_B14_0 = (&V_22);
		if (!L_122)
		{
			G_B15_0 = (&V_22);
			goto IL_02fb;
		}
	}
	{
		float L_123 = V_25;
		G_B16_0 = ((-L_123));
		G_B16_1 = G_B14_0;
		goto IL_0300;
	}

IL_02fb:
	{
		G_B16_0 = (0.0f);
		G_B16_1 = G_B15_0;
	}

IL_0300:
	{
		G_B16_1->___z_3 = G_B16_0;
		bool L_124 = V_23;
		G_B17_0 = (&V_22);
		if (!L_124)
		{
			G_B18_0 = (&V_22);
			goto IL_0318;
		}
	}
	{
		float L_125 = V_21;
		float L_126 = V_25;
		G_B19_0 = ((float)((float)L_125*(float)L_126));
		G_B19_1 = G_B17_0;
		goto IL_031d;
	}

IL_0318:
	{
		G_B19_0 = (0.0f);
		G_B19_1 = G_B18_0;
	}

IL_031d:
	{
		G_B19_1->___w_4 = G_B19_0;
		Material_t55 * L_127 = (__this->___fogMaterial_12);
		Vector4_t236  L_128 = V_22;
		NullCheck(L_127);
		Material_SetVector_m752(L_127, (String_t*) &_stringLiteral109, L_128, /*hidden argument*/NULL);
		Material_t55 * L_129 = (__this->___fogMaterial_12);
		int32_t L_130 = V_18;
		bool L_131 = (__this->___useRadialDistance_6);
		G_B20_0 = (((float)L_130));
		G_B20_1 = (String_t*) &_stringLiteral110;
		G_B20_2 = L_129;
		if (!L_131)
		{
			G_B21_0 = (((float)L_130));
			G_B21_1 = (String_t*) &_stringLiteral110;
			G_B21_2 = L_129;
			goto IL_0353;
		}
	}
	{
		G_B22_0 = 1;
		G_B22_1 = G_B20_0;
		G_B22_2 = G_B20_1;
		G_B22_3 = G_B20_2;
		goto IL_0354;
	}

IL_0353:
	{
		G_B22_0 = 0;
		G_B22_1 = G_B21_0;
		G_B22_2 = G_B21_1;
		G_B22_3 = G_B21_2;
	}

IL_0354:
	{
		Vector4_t236  L_132 = {0};
		Vector4__ctor_m751(&L_132, G_B22_1, (((float)G_B22_0)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(G_B22_3);
		Material_SetVector_m752(G_B22_3, G_B22_2, L_132, /*hidden argument*/NULL);
		V_26 = 0;
		bool L_133 = (__this->___distanceFog_5);
		if (!L_133)
		{
			goto IL_038a;
		}
	}
	{
		bool L_134 = (__this->___heightFog_7);
		if (!L_134)
		{
			goto IL_038a;
		}
	}
	{
		V_26 = 0;
		goto IL_03a0;
	}

IL_038a:
	{
		bool L_135 = (__this->___distanceFog_5);
		if (!L_135)
		{
			goto IL_039d;
		}
	}
	{
		V_26 = 1;
		goto IL_03a0;
	}

IL_039d:
	{
		V_26 = 2;
	}

IL_03a0:
	{
		RenderTexture_t101 * L_136 = ___source;
		RenderTexture_t101 * L_137 = ___destination;
		Material_t55 * L_138 = (__this->___fogMaterial_12);
		int32_t L_139 = V_26;
		GlobalFog_CustomGraphicsBlit_m301(NULL /*static, unused*/, L_136, L_137, L_138, L_139, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.GlobalFog::CustomGraphicsBlit(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern "C" void GlobalFog_CustomGraphicsBlit_m301 (Object_t * __this /* static, unused */, RenderTexture_t101 * ___source, RenderTexture_t101 * ___dest, Material_t55 * ___fxMaterial, int32_t ___passNr, const MethodInfo* method)
{
	{
		RenderTexture_t101 * L_0 = ___dest;
		RenderTexture_set_active_m835(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Material_t55 * L_1 = ___fxMaterial;
		RenderTexture_t101 * L_2 = ___source;
		NullCheck(L_1);
		Material_SetTexture_m759(L_1, (String_t*) &_stringLiteral87, L_2, /*hidden argument*/NULL);
		GL_PushMatrix_m836(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m847(NULL /*static, unused*/, /*hidden argument*/NULL);
		Material_t55 * L_3 = ___fxMaterial;
		int32_t L_4 = ___passNr;
		NullCheck(L_3);
		Material_SetPass_m831(L_3, L_4, /*hidden argument*/NULL);
		GL_Begin_m848(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 0, (0.0f), (0.0f), /*hidden argument*/NULL);
		GL_Vertex3_m850(NULL /*static, unused*/, (0.0f), (0.0f), (3.0f), /*hidden argument*/NULL);
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 0, (1.0f), (0.0f), /*hidden argument*/NULL);
		GL_Vertex3_m850(NULL /*static, unused*/, (1.0f), (0.0f), (2.0f), /*hidden argument*/NULL);
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 0, (1.0f), (1.0f), /*hidden argument*/NULL);
		GL_Vertex3_m850(NULL /*static, unused*/, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 0, (0.0f), (1.0f), /*hidden argument*/NULL);
		GL_Vertex3_m850(NULL /*static, unused*/, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		GL_End_m851(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m839(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.Grayscale
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_41.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Grayscale
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_41MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.Grayscale::.ctor()
extern "C" void Grayscale__ctor_m302 (Grayscale_t107 * __this, const MethodInfo* method)
{
	{
		ImageEffectBase__ctor_m304(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Grayscale::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Grayscale_OnRenderImage_m303 (Grayscale_t107 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = ImageEffectBase_get_material_m306(__this, /*hidden argument*/NULL);
		Texture_t86 * L_1 = (__this->___textureRamp_4);
		NullCheck(L_0);
		Material_SetTexture_m759(L_0, (String_t*) &_stringLiteral71, L_1, /*hidden argument*/NULL);
		Material_t55 * L_2 = ImageEffectBase_get_material_m306(__this, /*hidden argument*/NULL);
		float L_3 = (__this->___rampOffset_5);
		NullCheck(L_2);
		Material_SetFloat_m738(L_2, (String_t*) &_stringLiteral111, L_3, /*hidden argument*/NULL);
		RenderTexture_t101 * L_4 = ___source;
		RenderTexture_t101 * L_5 = ___destination;
		Material_t55 * L_6 = ImageEffectBase_get_material_m306(__this, /*hidden argument*/NULL);
		Graphics_Blit_m739(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.ImageEffectBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_25.h"
#ifndef _MSC_VER
#else
#endif



// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::.ctor()
extern "C" void ImageEffectBase__ctor_m304 (ImageEffectBase_t88 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::Start()
extern "C" void ImageEffectBase_Start_m305 (ImageEffectBase_t88 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m765(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		Shader_t54 * L_1 = (__this->___shader_2);
		bool L_2 = Object_op_Implicit_m629(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		Shader_t54 * L_3 = (__this->___shader_2);
		NullCheck(L_3);
		bool L_4 = Shader_get_isSupported_m736(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0039;
		}
	}

IL_0032:
	{
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::get_material()
extern TypeInfo* Material_t55_il2cpp_TypeInfo_var;
extern "C" Material_t55 * ImageEffectBase_get_material_m306 (ImageEffectBase_t88 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t55 * L_0 = (__this->___m_Material_3);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t54 * L_2 = (__this->___shader_2);
		Material_t55 * L_3 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m763(L_3, L_2, /*hidden argument*/NULL);
		__this->___m_Material_3 = L_3;
		Material_t55 * L_4 = (__this->___m_Material_3);
		NullCheck(L_4);
		Object_set_hideFlags_m764(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t55 * L_5 = (__this->___m_Material_3);
		return L_5;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::OnDisable()
extern "C" void ImageEffectBase_OnDisable_m307 (ImageEffectBase_t88 * __this, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___m_Material_3);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t55 * L_2 = (__this->___m_Material_3);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// UnityStandardAssets.ImageEffects.ImageEffects
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_42.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ImageEffects
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_42MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.ImageEffects::.ctor()
extern "C" void ImageEffects__ctor_m308 (ImageEffects_t108 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::RenderDistortion(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Single,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" void ImageEffects_RenderDistortion_m309 (Object_t * __this /* static, unused */, Material_t55 * ___material, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, float ___angle, Vector2_t6  ___center, Vector2_t6  ___radius, const MethodInfo* method)
{
	bool V_0 = false;
	Matrix4x4_t80  V_1 = {0};
	Vector2_t6  V_2 = {0};
	{
		RenderTexture_t101 * L_0 = ___source;
		NullCheck(L_0);
		Vector2_t6  L_1 = Texture_get_texelSize_m852(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		float L_2 = ((&V_2)->___y_2);
		V_0 = ((((float)L_2) < ((float)(0.0f)))? 1 : 0);
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		float L_4 = ((&___center)->___y_2);
		(&___center)->___y_2 = ((float)((float)(1.0f)-(float)L_4));
		float L_5 = ___angle;
		___angle = ((-L_5));
	}

IL_0034:
	{
		Vector3_t4  L_6 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = ___angle;
		Quaternion_t19  L_8 = Quaternion_Euler_m665(NULL /*static, unused*/, (0.0f), (0.0f), L_7, /*hidden argument*/NULL);
		Vector3_t4  L_9 = Vector3_get_one_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t80  L_10 = Matrix4x4_TRS_m853(NULL /*static, unused*/, L_6, L_8, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		Material_t55 * L_11 = ___material;
		Matrix4x4_t80  L_12 = V_1;
		NullCheck(L_11);
		Material_SetMatrix_m786(L_11, (String_t*) &_stringLiteral112, L_12, /*hidden argument*/NULL);
		Material_t55 * L_13 = ___material;
		float L_14 = ((&___center)->___x_1);
		float L_15 = ((&___center)->___y_2);
		float L_16 = ((&___radius)->___x_1);
		float L_17 = ((&___radius)->___y_2);
		Vector4_t236  L_18 = {0};
		Vector4__ctor_m751(&L_18, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		Material_SetVector_m752(L_13, (String_t*) &_stringLiteral113, L_18, /*hidden argument*/NULL);
		Material_t55 * L_19 = ___material;
		float L_20 = ___angle;
		NullCheck(L_19);
		Material_SetFloat_m738(L_19, (String_t*) &_stringLiteral114, ((float)((float)L_20*(float)(0.0174532924f))), /*hidden argument*/NULL);
		RenderTexture_t101 * L_21 = ___source;
		RenderTexture_t101 * L_22 = ___destination;
		Material_t55 * L_23 = ___material;
		Graphics_Blit_m739(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ImageEffects_Blit_m310 (Object_t * __this /* static, unused */, RenderTexture_t101 * ___source, RenderTexture_t101 * ___dest, const MethodInfo* method)
{
	{
		RenderTexture_t101 * L_0 = ___source;
		RenderTexture_t101 * L_1 = ___dest;
		Graphics_Blit_m737(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::BlitWithMaterial(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ImageEffects_BlitWithMaterial_m311 (Object_t * __this /* static, unused */, Material_t55 * ___material, RenderTexture_t101 * ___source, RenderTexture_t101 * ___dest, const MethodInfo* method)
{
	{
		RenderTexture_t101 * L_0 = ___source;
		RenderTexture_t101 * L_1 = ___dest;
		Material_t55 * L_2 = ___material;
		Graphics_Blit_m739(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.MotionBlur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_43.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.MotionBlur
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_43MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.MotionBlur::.ctor()
extern "C" void MotionBlur__ctor_m312 (MotionBlur_t109 * __this, const MethodInfo* method)
{
	{
		__this->___blurAmount_4 = (0.8f);
		ImageEffectBase__ctor_m304(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.MotionBlur::Start()
extern "C" void MotionBlur_Start_m313 (MotionBlur_t109 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SystemInfo_get_supportsRenderTextures_m854(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		ImageEffectBase_Start_m305(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnDisable()
extern "C" void MotionBlur_OnDisable_m314 (MotionBlur_t109 * __this, const MethodInfo* method)
{
	{
		ImageEffectBase_OnDisable_m307(__this, /*hidden argument*/NULL);
		RenderTexture_t101 * L_0 = (__this->___accumTexture_6);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* RenderTexture_t101_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void MotionBlur_OnRenderImage_m315 (MotionBlur_t109 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RenderTexture_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t101 * V_0 = {0};
	{
		RenderTexture_t101 * L_0 = (__this->___accumTexture_6);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_003d;
		}
	}
	{
		RenderTexture_t101 * L_2 = (__this->___accumTexture_6);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_2);
		RenderTexture_t101 * L_4 = ___source;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_4);
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_003d;
		}
	}
	{
		RenderTexture_t101 * L_6 = (__this->___accumTexture_6);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_6);
		RenderTexture_t101 * L_8 = ___source;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_8);
		if ((((int32_t)L_7) == ((int32_t)L_9)))
		{
			goto IL_0079;
		}
	}

IL_003d:
	{
		RenderTexture_t101 * L_10 = (__this->___accumTexture_6);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		RenderTexture_t101 * L_11 = ___source;
		NullCheck(L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_11);
		RenderTexture_t101 * L_13 = ___source;
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_13);
		RenderTexture_t101 * L_15 = (RenderTexture_t101 *)il2cpp_codegen_object_new (RenderTexture_t101_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m819(L_15, L_12, L_14, 0, /*hidden argument*/NULL);
		__this->___accumTexture_6 = L_15;
		RenderTexture_t101 * L_16 = (__this->___accumTexture_6);
		NullCheck(L_16);
		Object_set_hideFlags_m764(L_16, ((int32_t)61), /*hidden argument*/NULL);
		RenderTexture_t101 * L_17 = ___source;
		RenderTexture_t101 * L_18 = (__this->___accumTexture_6);
		Graphics_Blit_m737(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
	}

IL_0079:
	{
		bool L_19 = (__this->___extraBlur_5);
		if (!L_19)
		{
			goto IL_00c4;
		}
	}
	{
		RenderTexture_t101 * L_20 = ___source;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_20);
		RenderTexture_t101 * L_22 = ___source;
		NullCheck(L_22);
		int32_t L_23 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_22);
		RenderTexture_t101 * L_24 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_21/(int32_t)4)), ((int32_t)((int32_t)L_23/(int32_t)4)), 0, /*hidden argument*/NULL);
		V_0 = L_24;
		RenderTexture_t101 * L_25 = (__this->___accumTexture_6);
		NullCheck(L_25);
		RenderTexture_MarkRestoreExpected_m756(L_25, /*hidden argument*/NULL);
		RenderTexture_t101 * L_26 = (__this->___accumTexture_6);
		RenderTexture_t101 * L_27 = V_0;
		Graphics_Blit_m737(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		RenderTexture_t101 * L_28 = V_0;
		RenderTexture_t101 * L_29 = (__this->___accumTexture_6);
		Graphics_Blit_m737(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		RenderTexture_t101 * L_30 = V_0;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
	}

IL_00c4:
	{
		float L_31 = (__this->___blurAmount_4);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_32 = Mathf_Clamp_m611(NULL /*static, unused*/, L_31, (0.0f), (0.92f), /*hidden argument*/NULL);
		__this->___blurAmount_4 = L_32;
		Material_t55 * L_33 = ImageEffectBase_get_material_m306(__this, /*hidden argument*/NULL);
		RenderTexture_t101 * L_34 = (__this->___accumTexture_6);
		NullCheck(L_33);
		Material_SetTexture_m759(L_33, (String_t*) &_stringLiteral87, L_34, /*hidden argument*/NULL);
		Material_t55 * L_35 = ImageEffectBase_get_material_m306(__this, /*hidden argument*/NULL);
		float L_36 = (__this->___blurAmount_4);
		NullCheck(L_35);
		Material_SetFloat_m738(L_35, (String_t*) &_stringLiteral115, ((float)((float)(1.0f)-(float)L_36)), /*hidden argument*/NULL);
		RenderTexture_t101 * L_37 = (__this->___accumTexture_6);
		NullCheck(L_37);
		RenderTexture_MarkRestoreExpected_m756(L_37, /*hidden argument*/NULL);
		RenderTexture_t101 * L_38 = ___source;
		RenderTexture_t101 * L_39 = (__this->___accumTexture_6);
		Material_t55 * L_40 = ImageEffectBase_get_material_m306(__this, /*hidden argument*/NULL);
		Graphics_Blit_m739(NULL /*static, unused*/, L_38, L_39, L_40, /*hidden argument*/NULL);
		RenderTexture_t101 * L_41 = (__this->___accumTexture_6);
		RenderTexture_t101 * L_42 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.NoiseAndGrain
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_44.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.NoiseAndGrain
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_44MethodDeclarations.h"

// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::.ctor()
extern "C" void NoiseAndGrain__ctor_m316 (NoiseAndGrain_t110 * __this, const MethodInfo* method)
{
	{
		__this->___intensityMultiplier_5 = (0.25f);
		__this->___generalIntensity_6 = (0.5f);
		__this->___blackIntensity_7 = (1.0f);
		__this->___whiteIntensity_8 = (1.0f);
		__this->___midGrey_9 = (0.2f);
		Vector3_t4  L_0 = {0};
		Vector3__ctor_m612(&L_0, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->___intensities_13 = L_0;
		Vector3_t4  L_1 = {0};
		Vector3__ctor_m612(&L_1, (64.0f), (64.0f), (64.0f), /*hidden argument*/NULL);
		__this->___tiling_14 = L_1;
		__this->___monochromeTiling_15 = (64.0f);
		__this->___filterMode_16 = 1;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::.cctor()
extern TypeInfo* NoiseAndGrain_t110_il2cpp_TypeInfo_var;
extern "C" void NoiseAndGrain__cctor_m317 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NoiseAndGrain_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(57);
		s_Il2CppMethodIntialized = true;
	}
	{
		((NoiseAndGrain_t110_StaticFields*)NoiseAndGrain_t110_il2cpp_TypeInfo_var->static_fields)->___TILE_AMOUNT_22 = (64.0f);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.NoiseAndGrain::CheckResources()
extern "C" bool NoiseAndGrain_CheckResources_m318 (NoiseAndGrain_t110 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___noiseShader_18);
		Material_t55 * L_1 = (__this->___noiseMaterial_19);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___noiseMaterial_19 = L_2;
		bool L_3 = (__this->___dx11Grain_10);
		if (!L_3)
		{
			goto IL_004e;
		}
	}
	{
		bool L_4 = (((PostEffectsBase_t57 *)__this)->___supportDX11_3);
		if (!L_4)
		{
			goto IL_004e;
		}
	}
	{
		Shader_t54 * L_5 = (__this->___dx11NoiseShader_20);
		Material_t55 * L_6 = (__this->___dx11NoiseMaterial_21);
		Material_t55 * L_7 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_5, L_6, /*hidden argument*/NULL);
		__this->___dx11NoiseMaterial_21 = L_7;
	}

IL_004e:
	{
		bool L_8 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_8)
		{
			goto IL_005f;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_005f:
	{
		bool L_9 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_9;
	}
}
// System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* NoiseAndGrain_t110_il2cpp_TypeInfo_var;
extern "C" void NoiseAndGrain_OnRenderImage_m319 (NoiseAndGrain_t110 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		NoiseAndGrain_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(57);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t101 * V_0 = {0};
	RenderTexture_t101 * V_1 = {0};
	String_t* G_B9_0 = {0};
	Material_t55 * G_B9_1 = {0};
	String_t* G_B8_0 = {0};
	Material_t55 * G_B8_1 = {0};
	Vector3_t4  G_B10_0 = {0};
	String_t* G_B10_1 = {0};
	Material_t55 * G_B10_2 = {0};
	Texture2D_t63 * G_B13_0 = {0};
	Material_t55 * G_B13_1 = {0};
	RenderTexture_t101 * G_B13_2 = {0};
	RenderTexture_t101 * G_B13_3 = {0};
	Texture2D_t63 * G_B12_0 = {0};
	Material_t55 * G_B12_1 = {0};
	RenderTexture_t101 * G_B12_2 = {0};
	RenderTexture_t101 * G_B12_3 = {0};
	int32_t G_B14_0 = 0;
	Texture2D_t63 * G_B14_1 = {0};
	Material_t55 * G_B14_2 = {0};
	RenderTexture_t101 * G_B14_3 = {0};
	RenderTexture_t101 * G_B14_4 = {0};
	Texture2D_t63 * G_B17_0 = {0};
	Material_t55 * G_B17_1 = {0};
	RenderTexture_t101 * G_B17_2 = {0};
	RenderTexture_t101 * G_B17_3 = {0};
	Texture2D_t63 * G_B16_0 = {0};
	Material_t55 * G_B16_1 = {0};
	RenderTexture_t101 * G_B16_2 = {0};
	RenderTexture_t101 * G_B16_3 = {0};
	int32_t G_B18_0 = 0;
	Texture2D_t63 * G_B18_1 = {0};
	Material_t55 * G_B18_2 = {0};
	RenderTexture_t101 * G_B18_3 = {0};
	RenderTexture_t101 * G_B18_4 = {0};
	String_t* G_B24_0 = {0};
	Material_t55 * G_B24_1 = {0};
	String_t* G_B23_0 = {0};
	Material_t55 * G_B23_1 = {0};
	Vector3_t4  G_B25_0 = {0};
	String_t* G_B25_1 = {0};
	Material_t55 * G_B25_2 = {0};
	String_t* G_B27_0 = {0};
	Material_t55 * G_B27_1 = {0};
	String_t* G_B26_0 = {0};
	Material_t55 * G_B26_1 = {0};
	Vector3_t4  G_B28_0 = {0};
	String_t* G_B28_1 = {0};
	Material_t55 * G_B28_2 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.NoiseAndGrain::CheckResources() */, __this);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Texture2D_t63 * L_1 = (__this->___noiseTexture_17);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, (Object_t164 *)NULL, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0045;
		}
	}

IL_001c:
	{
		RenderTexture_t101 * L_3 = ___source;
		RenderTexture_t101 * L_4 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Texture2D_t63 * L_5 = (__this->___noiseTexture_17);
		bool L_6 = Object_op_Equality_m640(NULL /*static, unused*/, (Object_t164 *)NULL, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		Transform_t1 * L_7 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Debug_LogWarning_m855(NULL /*static, unused*/, (String_t*) &_stringLiteral116, L_7, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}

IL_0045:
	{
		float L_8 = (__this->___softness_11);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_9 = Mathf_Clamp_m611(NULL /*static, unused*/, L_8, (0.0f), (0.99f), /*hidden argument*/NULL);
		__this->___softness_11 = L_9;
		bool L_10 = (__this->___dx11Grain_10);
		if (!L_10)
		{
			goto IL_0200;
		}
	}
	{
		bool L_11 = (((PostEffectsBase_t57 *)__this)->___supportDX11_3);
		if (!L_11)
		{
			goto IL_0200;
		}
	}
	{
		Material_t55 * L_12 = (__this->___dx11NoiseMaterial_21);
		int32_t L_13 = Time_get_frameCount_m708(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		Material_SetFloat_m738(L_12, (String_t*) &_stringLiteral117, (((float)L_13)), /*hidden argument*/NULL);
		Material_t55 * L_14 = (__this->___dx11NoiseMaterial_21);
		Texture2D_t63 * L_15 = (__this->___noiseTexture_17);
		NullCheck(L_14);
		Material_SetTexture_m759(L_14, (String_t*) &_stringLiteral51, L_15, /*hidden argument*/NULL);
		Material_t55 * L_16 = (__this->___dx11NoiseMaterial_21);
		bool L_17 = (__this->___monochrome_12);
		G_B8_0 = (String_t*) &_stringLiteral118;
		G_B8_1 = L_16;
		if (!L_17)
		{
			G_B9_0 = (String_t*) &_stringLiteral118;
			G_B9_1 = L_16;
			goto IL_00c2;
		}
	}
	{
		Vector3_t4  L_18 = Vector3_get_one_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B10_0 = L_18;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_00c8;
	}

IL_00c2:
	{
		Vector3_t4  L_19 = (__this->___intensities_13);
		G_B10_0 = L_19;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_00c8:
	{
		Vector4_t236  L_20 = Vector4_op_Implicit_m830(NULL /*static, unused*/, G_B10_0, /*hidden argument*/NULL);
		NullCheck(G_B10_2);
		Material_SetVector_m752(G_B10_2, G_B10_1, L_20, /*hidden argument*/NULL);
		Material_t55 * L_21 = (__this->___dx11NoiseMaterial_21);
		float L_22 = (__this->___midGrey_9);
		float L_23 = (__this->___midGrey_9);
		float L_24 = (__this->___midGrey_9);
		Vector3_t4  L_25 = {0};
		Vector3__ctor_m612(&L_25, L_22, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)-(float)L_23)))), ((float)((float)(-1.0f)/(float)L_24)), /*hidden argument*/NULL);
		Vector4_t236  L_26 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		NullCheck(L_21);
		Material_SetVector_m752(L_21, (String_t*) &_stringLiteral119, L_26, /*hidden argument*/NULL);
		Material_t55 * L_27 = (__this->___dx11NoiseMaterial_21);
		float L_28 = (__this->___generalIntensity_6);
		float L_29 = (__this->___blackIntensity_7);
		float L_30 = (__this->___whiteIntensity_8);
		Vector3_t4  L_31 = {0};
		Vector3__ctor_m612(&L_31, L_28, L_29, L_30, /*hidden argument*/NULL);
		float L_32 = (__this->___intensityMultiplier_5);
		Vector3_t4  L_33 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		Vector4_t236  L_34 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		NullCheck(L_27);
		Material_SetVector_m752(L_27, (String_t*) &_stringLiteral120, L_34, /*hidden argument*/NULL);
		float L_35 = (__this->___softness_11);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_36 = ((Mathf_t218_StaticFields*)Mathf_t218_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		if ((!(((float)L_35) > ((float)L_36))))
		{
			goto IL_01d6;
		}
	}
	{
		RenderTexture_t101 * L_37 = ___source;
		NullCheck(L_37);
		int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_37);
		float L_39 = (__this->___softness_11);
		RenderTexture_t101 * L_40 = ___source;
		NullCheck(L_40);
		int32_t L_41 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_40);
		float L_42 = (__this->___softness_11);
		RenderTexture_t101 * L_43 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, (((int32_t)((float)((float)(((float)L_38))*(float)((float)((float)(1.0f)-(float)L_39)))))), (((int32_t)((float)((float)(((float)L_41))*(float)((float)((float)(1.0f)-(float)L_42)))))), /*hidden argument*/NULL);
		V_0 = L_43;
		RenderTexture_t101 * L_44 = ___source;
		RenderTexture_t101 * L_45 = V_0;
		Material_t55 * L_46 = (__this->___dx11NoiseMaterial_21);
		Texture2D_t63 * L_47 = (__this->___noiseTexture_17);
		bool L_48 = (__this->___monochrome_12);
		G_B12_0 = L_47;
		G_B12_1 = L_46;
		G_B12_2 = L_45;
		G_B12_3 = L_44;
		if (!L_48)
		{
			G_B13_0 = L_47;
			G_B13_1 = L_46;
			G_B13_2 = L_45;
			G_B13_3 = L_44;
			goto IL_01a6;
		}
	}
	{
		G_B14_0 = 3;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_01a7;
	}

IL_01a6:
	{
		G_B14_0 = 2;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_01a7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NoiseAndGrain_t110_il2cpp_TypeInfo_var);
		NoiseAndGrain_DrawNoiseQuadGrid_m320(NULL /*static, unused*/, G_B14_4, G_B14_3, G_B14_2, G_B14_1, G_B14_0, /*hidden argument*/NULL);
		Material_t55 * L_49 = (__this->___dx11NoiseMaterial_21);
		RenderTexture_t101 * L_50 = V_0;
		NullCheck(L_49);
		Material_SetTexture_m759(L_49, (String_t*) &_stringLiteral51, L_50, /*hidden argument*/NULL);
		RenderTexture_t101 * L_51 = ___source;
		RenderTexture_t101 * L_52 = ___destination;
		Material_t55 * L_53 = (__this->___dx11NoiseMaterial_21);
		Graphics_Blit_m742(NULL /*static, unused*/, L_51, L_52, L_53, 4, /*hidden argument*/NULL);
		RenderTexture_t101 * L_54 = V_0;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		goto IL_01fb;
	}

IL_01d6:
	{
		RenderTexture_t101 * L_55 = ___source;
		RenderTexture_t101 * L_56 = ___destination;
		Material_t55 * L_57 = (__this->___dx11NoiseMaterial_21);
		Texture2D_t63 * L_58 = (__this->___noiseTexture_17);
		bool L_59 = (__this->___monochrome_12);
		G_B16_0 = L_58;
		G_B16_1 = L_57;
		G_B16_2 = L_56;
		G_B16_3 = L_55;
		if (!L_59)
		{
			G_B17_0 = L_58;
			G_B17_1 = L_57;
			G_B17_2 = L_56;
			G_B17_3 = L_55;
			goto IL_01f5;
		}
	}
	{
		G_B18_0 = 1;
		G_B18_1 = G_B16_0;
		G_B18_2 = G_B16_1;
		G_B18_3 = G_B16_2;
		G_B18_4 = G_B16_3;
		goto IL_01f6;
	}

IL_01f5:
	{
		G_B18_0 = 0;
		G_B18_1 = G_B17_0;
		G_B18_2 = G_B17_1;
		G_B18_3 = G_B17_2;
		G_B18_4 = G_B17_3;
	}

IL_01f6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NoiseAndGrain_t110_il2cpp_TypeInfo_var);
		NoiseAndGrain_DrawNoiseQuadGrid_m320(NULL /*static, unused*/, G_B18_4, G_B18_3, G_B18_2, G_B18_1, G_B18_0, /*hidden argument*/NULL);
	}

IL_01fb:
	{
		goto IL_03b5;
	}

IL_0200:
	{
		Texture2D_t63 * L_60 = (__this->___noiseTexture_17);
		bool L_61 = Object_op_Implicit_m629(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_022d;
		}
	}
	{
		Texture2D_t63 * L_62 = (__this->___noiseTexture_17);
		NullCheck(L_62);
		Texture_set_wrapMode_m784(L_62, 0, /*hidden argument*/NULL);
		Texture2D_t63 * L_63 = (__this->___noiseTexture_17);
		int32_t L_64 = (__this->___filterMode_16);
		NullCheck(L_63);
		Texture_set_filterMode_m762(L_63, L_64, /*hidden argument*/NULL);
	}

IL_022d:
	{
		Material_t55 * L_65 = (__this->___noiseMaterial_19);
		Texture2D_t63 * L_66 = (__this->___noiseTexture_17);
		NullCheck(L_65);
		Material_SetTexture_m759(L_65, (String_t*) &_stringLiteral51, L_66, /*hidden argument*/NULL);
		Material_t55 * L_67 = (__this->___noiseMaterial_19);
		bool L_68 = (__this->___monochrome_12);
		G_B23_0 = (String_t*) &_stringLiteral118;
		G_B23_1 = L_67;
		if (!L_68)
		{
			G_B24_0 = (String_t*) &_stringLiteral118;
			G_B24_1 = L_67;
			goto IL_0263;
		}
	}
	{
		Vector3_t4  L_69 = Vector3_get_one_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B25_0 = L_69;
		G_B25_1 = G_B23_0;
		G_B25_2 = G_B23_1;
		goto IL_0269;
	}

IL_0263:
	{
		Vector3_t4  L_70 = (__this->___intensities_13);
		G_B25_0 = L_70;
		G_B25_1 = G_B24_0;
		G_B25_2 = G_B24_1;
	}

IL_0269:
	{
		Vector4_t236  L_71 = Vector4_op_Implicit_m830(NULL /*static, unused*/, G_B25_0, /*hidden argument*/NULL);
		NullCheck(G_B25_2);
		Material_SetVector_m752(G_B25_2, G_B25_1, L_71, /*hidden argument*/NULL);
		Material_t55 * L_72 = (__this->___noiseMaterial_19);
		bool L_73 = (__this->___monochrome_12);
		G_B26_0 = (String_t*) &_stringLiteral121;
		G_B26_1 = L_72;
		if (!L_73)
		{
			G_B27_0 = (String_t*) &_stringLiteral121;
			G_B27_1 = L_72;
			goto IL_029e;
		}
	}
	{
		Vector3_t4  L_74 = Vector3_get_one_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_75 = (__this->___monochromeTiling_15);
		Vector3_t4  L_76 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_74, L_75, /*hidden argument*/NULL);
		G_B28_0 = L_76;
		G_B28_1 = G_B26_0;
		G_B28_2 = G_B26_1;
		goto IL_02a4;
	}

IL_029e:
	{
		Vector3_t4  L_77 = (__this->___tiling_14);
		G_B28_0 = L_77;
		G_B28_1 = G_B27_0;
		G_B28_2 = G_B27_1;
	}

IL_02a4:
	{
		Vector4_t236  L_78 = Vector4_op_Implicit_m830(NULL /*static, unused*/, G_B28_0, /*hidden argument*/NULL);
		NullCheck(G_B28_2);
		Material_SetVector_m752(G_B28_2, G_B28_1, L_78, /*hidden argument*/NULL);
		Material_t55 * L_79 = (__this->___noiseMaterial_19);
		float L_80 = (__this->___midGrey_9);
		float L_81 = (__this->___midGrey_9);
		float L_82 = (__this->___midGrey_9);
		Vector3_t4  L_83 = {0};
		Vector3__ctor_m612(&L_83, L_80, ((float)((float)(1.0f)/(float)((float)((float)(1.0f)-(float)L_81)))), ((float)((float)(-1.0f)/(float)L_82)), /*hidden argument*/NULL);
		Vector4_t236  L_84 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		NullCheck(L_79);
		Material_SetVector_m752(L_79, (String_t*) &_stringLiteral119, L_84, /*hidden argument*/NULL);
		Material_t55 * L_85 = (__this->___noiseMaterial_19);
		float L_86 = (__this->___generalIntensity_6);
		float L_87 = (__this->___blackIntensity_7);
		float L_88 = (__this->___whiteIntensity_8);
		Vector3_t4  L_89 = {0};
		Vector3__ctor_m612(&L_89, L_86, L_87, L_88, /*hidden argument*/NULL);
		float L_90 = (__this->___intensityMultiplier_5);
		Vector3_t4  L_91 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_89, L_90, /*hidden argument*/NULL);
		Vector4_t236  L_92 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		NullCheck(L_85);
		Material_SetVector_m752(L_85, (String_t*) &_stringLiteral120, L_92, /*hidden argument*/NULL);
		float L_93 = (__this->___softness_11);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_94 = ((Mathf_t218_StaticFields*)Mathf_t218_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		if ((!(((float)L_93) > ((float)L_94))))
		{
			goto IL_03a1;
		}
	}
	{
		RenderTexture_t101 * L_95 = ___source;
		NullCheck(L_95);
		int32_t L_96 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_95);
		float L_97 = (__this->___softness_11);
		RenderTexture_t101 * L_98 = ___source;
		NullCheck(L_98);
		int32_t L_99 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_98);
		float L_100 = (__this->___softness_11);
		RenderTexture_t101 * L_101 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, (((int32_t)((float)((float)(((float)L_96))*(float)((float)((float)(1.0f)-(float)L_97)))))), (((int32_t)((float)((float)(((float)L_99))*(float)((float)((float)(1.0f)-(float)L_100)))))), /*hidden argument*/NULL);
		V_1 = L_101;
		RenderTexture_t101 * L_102 = ___source;
		RenderTexture_t101 * L_103 = V_1;
		Material_t55 * L_104 = (__this->___noiseMaterial_19);
		Texture2D_t63 * L_105 = (__this->___noiseTexture_17);
		IL2CPP_RUNTIME_CLASS_INIT(NoiseAndGrain_t110_il2cpp_TypeInfo_var);
		NoiseAndGrain_DrawNoiseQuadGrid_m320(NULL /*static, unused*/, L_102, L_103, L_104, L_105, 2, /*hidden argument*/NULL);
		Material_t55 * L_106 = (__this->___noiseMaterial_19);
		RenderTexture_t101 * L_107 = V_1;
		NullCheck(L_106);
		Material_SetTexture_m759(L_106, (String_t*) &_stringLiteral51, L_107, /*hidden argument*/NULL);
		RenderTexture_t101 * L_108 = ___source;
		RenderTexture_t101 * L_109 = ___destination;
		Material_t55 * L_110 = (__this->___noiseMaterial_19);
		Graphics_Blit_m742(NULL /*static, unused*/, L_108, L_109, L_110, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_111 = V_1;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_111, /*hidden argument*/NULL);
		goto IL_03b5;
	}

IL_03a1:
	{
		RenderTexture_t101 * L_112 = ___source;
		RenderTexture_t101 * L_113 = ___destination;
		Material_t55 * L_114 = (__this->___noiseMaterial_19);
		Texture2D_t63 * L_115 = (__this->___noiseTexture_17);
		IL2CPP_RUNTIME_CLASS_INIT(NoiseAndGrain_t110_il2cpp_TypeInfo_var);
		NoiseAndGrain_DrawNoiseQuadGrid_m320(NULL /*static, unused*/, L_112, L_113, L_114, L_115, 0, /*hidden argument*/NULL);
	}

IL_03b5:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::DrawNoiseQuadGrid(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Texture2D,System.Int32)
extern TypeInfo* NoiseAndGrain_t110_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void NoiseAndGrain_DrawNoiseQuadGrid_m320 (Object_t * __this /* static, unused */, RenderTexture_t101 * ___source, RenderTexture_t101 * ___dest, Material_t55 * ___fxMaterial, Texture2D_t63 * ___noise, int32_t ___passNr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NoiseAndGrain_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(57);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	{
		RenderTexture_t101 * L_0 = ___dest;
		RenderTexture_set_active_m835(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture2D_t63 * L_1 = ___noise;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_1);
		V_0 = ((float)((float)(((float)L_2))*(float)(1.0f)));
		RenderTexture_t101 * L_3 = ___source;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(NoiseAndGrain_t110_il2cpp_TypeInfo_var);
		float L_5 = ((NoiseAndGrain_t110_StaticFields*)NoiseAndGrain_t110_il2cpp_TypeInfo_var->static_fields)->___TILE_AMOUNT_22;
		V_1 = ((float)((float)((float)((float)(1.0f)*(float)(((float)L_4))))/(float)L_5));
		Material_t55 * L_6 = ___fxMaterial;
		RenderTexture_t101 * L_7 = ___source;
		NullCheck(L_6);
		Material_SetTexture_m759(L_6, (String_t*) &_stringLiteral87, L_7, /*hidden argument*/NULL);
		GL_PushMatrix_m836(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m847(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t101 * L_8 = ___source;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_8);
		RenderTexture_t101 * L_10 = ___source;
		NullCheck(L_10);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_10);
		V_2 = ((float)((float)((float)((float)(1.0f)*(float)(((float)L_9))))/(float)((float)((float)(1.0f)*(float)(((float)L_11))))));
		float L_12 = V_1;
		V_3 = ((float)((float)(1.0f)/(float)L_12));
		float L_13 = V_3;
		float L_14 = V_2;
		V_4 = ((float)((float)L_13*(float)L_14));
		float L_15 = V_0;
		Texture2D_t63 * L_16 = ___noise;
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_16);
		V_5 = ((float)((float)L_15/(float)((float)((float)(((float)L_17))*(float)(1.0f)))));
		Material_t55 * L_18 = ___fxMaterial;
		int32_t L_19 = ___passNr;
		NullCheck(L_18);
		Material_SetPass_m831(L_18, L_19, /*hidden argument*/NULL);
		GL_Begin_m848(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		V_6 = (0.0f);
		goto IL_01bf;
	}

IL_0093:
	{
		V_7 = (0.0f);
		goto IL_01ad;
	}

IL_009f:
	{
		float L_20 = Random_Range_m856(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_8 = L_20;
		float L_21 = Random_Range_m856(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_9 = L_21;
		float L_22 = V_8;
		float L_23 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_24 = floorf(((float)((float)L_22*(float)L_23)));
		float L_25 = V_0;
		V_8 = ((float)((float)L_24/(float)L_25));
		float L_26 = V_9;
		float L_27 = V_0;
		float L_28 = floorf(((float)((float)L_26*(float)L_27)));
		float L_29 = V_0;
		V_9 = ((float)((float)L_28/(float)L_29));
		float L_30 = V_0;
		V_10 = ((float)((float)(1.0f)/(float)L_30));
		float L_31 = V_8;
		float L_32 = V_9;
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 0, L_31, L_32, /*hidden argument*/NULL);
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 1, (0.0f), (0.0f), /*hidden argument*/NULL);
		float L_33 = V_6;
		float L_34 = V_7;
		GL_Vertex3_m850(NULL /*static, unused*/, L_33, L_34, (0.1f), /*hidden argument*/NULL);
		float L_35 = V_8;
		float L_36 = V_5;
		float L_37 = V_10;
		float L_38 = V_9;
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 0, ((float)((float)L_35+(float)((float)((float)L_36*(float)L_37)))), L_38, /*hidden argument*/NULL);
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 1, (1.0f), (0.0f), /*hidden argument*/NULL);
		float L_39 = V_6;
		float L_40 = V_3;
		float L_41 = V_7;
		GL_Vertex3_m850(NULL /*static, unused*/, ((float)((float)L_39+(float)L_40)), L_41, (0.1f), /*hidden argument*/NULL);
		float L_42 = V_8;
		float L_43 = V_5;
		float L_44 = V_10;
		float L_45 = V_9;
		float L_46 = V_5;
		float L_47 = V_10;
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 0, ((float)((float)L_42+(float)((float)((float)L_43*(float)L_44)))), ((float)((float)L_45+(float)((float)((float)L_46*(float)L_47)))), /*hidden argument*/NULL);
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 1, (1.0f), (1.0f), /*hidden argument*/NULL);
		float L_48 = V_6;
		float L_49 = V_3;
		float L_50 = V_7;
		float L_51 = V_4;
		GL_Vertex3_m850(NULL /*static, unused*/, ((float)((float)L_48+(float)L_49)), ((float)((float)L_50+(float)L_51)), (0.1f), /*hidden argument*/NULL);
		float L_52 = V_8;
		float L_53 = V_9;
		float L_54 = V_5;
		float L_55 = V_10;
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 0, L_52, ((float)((float)L_53+(float)((float)((float)L_54*(float)L_55)))), /*hidden argument*/NULL);
		GL_MultiTexCoord2_m849(NULL /*static, unused*/, 1, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_56 = V_6;
		float L_57 = V_7;
		float L_58 = V_4;
		GL_Vertex3_m850(NULL /*static, unused*/, L_56, ((float)((float)L_57+(float)L_58)), (0.1f), /*hidden argument*/NULL);
		float L_59 = V_7;
		float L_60 = V_4;
		V_7 = ((float)((float)L_59+(float)L_60));
	}

IL_01ad:
	{
		float L_61 = V_7;
		if ((((float)L_61) < ((float)(1.0f))))
		{
			goto IL_009f;
		}
	}
	{
		float L_62 = V_6;
		float L_63 = V_3;
		V_6 = ((float)((float)L_62+(float)L_63));
	}

IL_01bf:
	{
		float L_64 = V_6;
		if ((((float)L_64) < ((float)(1.0f))))
		{
			goto IL_0093;
		}
	}
	{
		GL_End_m851(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m839(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.NoiseAndScratches
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_45.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.NoiseAndScratches
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_45MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::.ctor()
extern "C" void NoiseAndScratches__ctor_m321 (NoiseAndScratches_t111 * __this, const MethodInfo* method)
{
	{
		__this->___monochrome_2 = 1;
		__this->___grainIntensityMin_4 = (0.1f);
		__this->___grainIntensityMax_5 = (0.2f);
		__this->___grainSize_6 = (2.0f);
		__this->___scratchIntensityMin_7 = (0.05f);
		__this->___scratchIntensityMax_8 = (0.25f);
		__this->___scratchFPS_9 = (10.0f);
		__this->___scratchJitter_10 = (0.01f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::Start()
extern "C" void NoiseAndScratches_Start_m322 (NoiseAndScratches_t111 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m765(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		Shader_t54 * L_1 = (__this->___shaderRGB_13);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0034;
		}
	}
	{
		Shader_t54 * L_3 = (__this->___shaderYUV_14);
		bool L_4 = Object_op_Equality_m640(NULL /*static, unused*/, L_3, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004a;
		}
	}

IL_0034:
	{
		Debug_Log_m857(NULL /*static, unused*/, (String_t*) &_stringLiteral122, /*hidden argument*/NULL);
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		goto IL_007d;
	}

IL_004a:
	{
		Shader_t54 * L_5 = (__this->___shaderRGB_13);
		NullCheck(L_5);
		bool L_6 = Shader_get_isSupported_m736(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0066;
		}
	}
	{
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		goto IL_007d;
	}

IL_0066:
	{
		Shader_t54 * L_7 = (__this->___shaderYUV_14);
		NullCheck(L_7);
		bool L_8 = Shader_get_isSupported_m736(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_007d;
		}
	}
	{
		__this->___rgbFallback_3 = 1;
	}

IL_007d:
	{
		return;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndScratches::get_material()
extern TypeInfo* Material_t55_il2cpp_TypeInfo_var;
extern "C" Material_t55 * NoiseAndScratches_get_material_m323 (NoiseAndScratches_t111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	Material_t55 * G_B9_0 = {0};
	{
		Material_t55 * L_0 = (__this->___m_MaterialRGB_15);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t54 * L_2 = (__this->___shaderRGB_13);
		Material_t55 * L_3 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m763(L_3, L_2, /*hidden argument*/NULL);
		__this->___m_MaterialRGB_15 = L_3;
		Material_t55 * L_4 = (__this->___m_MaterialRGB_15);
		NullCheck(L_4);
		Object_set_hideFlags_m764(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t55 * L_5 = (__this->___m_MaterialYUV_16);
		bool L_6 = Object_op_Equality_m640(NULL /*static, unused*/, L_5, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0069;
		}
	}
	{
		bool L_7 = (__this->___rgbFallback_3);
		if (L_7)
		{
			goto IL_0069;
		}
	}
	{
		Shader_t54 * L_8 = (__this->___shaderYUV_14);
		Material_t55 * L_9 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m763(L_9, L_8, /*hidden argument*/NULL);
		__this->___m_MaterialYUV_16 = L_9;
		Material_t55 * L_10 = (__this->___m_MaterialYUV_16);
		NullCheck(L_10);
		Object_set_hideFlags_m764(L_10, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_0069:
	{
		bool L_11 = (__this->___rgbFallback_3);
		if (L_11)
		{
			goto IL_008a;
		}
	}
	{
		bool L_12 = (__this->___monochrome_2);
		if (L_12)
		{
			goto IL_008a;
		}
	}
	{
		Material_t55 * L_13 = (__this->___m_MaterialYUV_16);
		G_B9_0 = L_13;
		goto IL_0090;
	}

IL_008a:
	{
		Material_t55 * L_14 = (__this->___m_MaterialRGB_15);
		G_B9_0 = L_14;
	}

IL_0090:
	{
		return G_B9_0;
	}
}
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnDisable()
extern "C" void NoiseAndScratches_OnDisable_m324 (NoiseAndScratches_t111 * __this, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___m_MaterialRGB_15);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t55 * L_2 = (__this->___m_MaterialRGB_15);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		Material_t55 * L_3 = (__this->___m_MaterialYUV_16);
		bool L_4 = Object_op_Implicit_m629(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		Material_t55 * L_5 = (__this->___m_MaterialYUV_16);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::SanitizeParameters()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void NoiseAndScratches_SanitizeParameters_m325 (NoiseAndScratches_t111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___grainIntensityMin_4);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp_m611(NULL /*static, unused*/, L_0, (0.0f), (5.0f), /*hidden argument*/NULL);
		__this->___grainIntensityMin_4 = L_1;
		float L_2 = (__this->___grainIntensityMax_5);
		float L_3 = Mathf_Clamp_m611(NULL /*static, unused*/, L_2, (0.0f), (5.0f), /*hidden argument*/NULL);
		__this->___grainIntensityMax_5 = L_3;
		float L_4 = (__this->___scratchIntensityMin_7);
		float L_5 = Mathf_Clamp_m611(NULL /*static, unused*/, L_4, (0.0f), (5.0f), /*hidden argument*/NULL);
		__this->___scratchIntensityMin_7 = L_5;
		float L_6 = (__this->___scratchIntensityMax_8);
		float L_7 = Mathf_Clamp_m611(NULL /*static, unused*/, L_6, (0.0f), (5.0f), /*hidden argument*/NULL);
		__this->___scratchIntensityMax_8 = L_7;
		float L_8 = (__this->___scratchFPS_9);
		float L_9 = Mathf_Clamp_m611(NULL /*static, unused*/, L_8, (1.0f), (30.0f), /*hidden argument*/NULL);
		__this->___scratchFPS_9 = L_9;
		float L_10 = (__this->___scratchJitter_10);
		float L_11 = Mathf_Clamp_m611(NULL /*static, unused*/, L_10, (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->___scratchJitter_10 = L_11;
		float L_12 = (__this->___grainSize_6);
		float L_13 = Mathf_Clamp_m611(NULL /*static, unused*/, L_12, (0.1f), (50.0f), /*hidden argument*/NULL);
		__this->___grainSize_6 = L_13;
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void NoiseAndScratches_OnRenderImage_m326 (NoiseAndScratches_t111 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	Material_t55 * V_0 = {0};
	float V_1 = 0.0f;
	{
		NoiseAndScratches_SanitizeParameters_m325(__this, /*hidden argument*/NULL);
		float L_0 = (__this->___scratchTimeLeft_17);
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		float L_1 = Random_get_value_m858(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = (__this->___scratchFPS_9);
		__this->___scratchTimeLeft_17 = ((float)((float)((float)((float)L_1*(float)(2.0f)))/(float)L_2));
		float L_3 = Random_get_value_m858(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___scratchX_18 = L_3;
		float L_4 = Random_get_value_m858(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___scratchY_19 = L_4;
	}

IL_0044:
	{
		float L_5 = (__this->___scratchTimeLeft_17);
		float L_6 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___scratchTimeLeft_17 = ((float)((float)L_5-(float)L_6));
		Material_t55 * L_7 = NoiseAndScratches_get_material_m323(__this, /*hidden argument*/NULL);
		V_0 = L_7;
		Material_t55 * L_8 = V_0;
		Texture_t86 * L_9 = (__this->___grainTexture_11);
		NullCheck(L_8);
		Material_SetTexture_m759(L_8, (String_t*) &_stringLiteral123, L_9, /*hidden argument*/NULL);
		Material_t55 * L_10 = V_0;
		Texture_t86 * L_11 = (__this->___scratchTexture_12);
		NullCheck(L_10);
		Material_SetTexture_m759(L_10, (String_t*) &_stringLiteral124, L_11, /*hidden argument*/NULL);
		float L_12 = (__this->___grainSize_6);
		V_1 = ((float)((float)(1.0f)/(float)L_12));
		Material_t55 * L_13 = V_0;
		float L_14 = Random_get_value_m858(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_15 = Random_get_value_m858(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture_t86 * L_17 = (__this->___grainTexture_11);
		NullCheck(L_17);
		int32_t L_18 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_17);
		float L_19 = V_1;
		int32_t L_20 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture_t86 * L_21 = (__this->___grainTexture_11);
		NullCheck(L_21);
		int32_t L_22 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_21);
		float L_23 = V_1;
		Vector4_t236  L_24 = {0};
		Vector4__ctor_m751(&L_24, L_14, L_15, ((float)((float)((float)((float)(((float)L_16))/(float)(((float)L_18))))*(float)L_19)), ((float)((float)((float)((float)(((float)L_20))/(float)(((float)L_22))))*(float)L_23)), /*hidden argument*/NULL);
		NullCheck(L_13);
		Material_SetVector_m752(L_13, (String_t*) &_stringLiteral125, L_24, /*hidden argument*/NULL);
		Material_t55 * L_25 = V_0;
		float L_26 = (__this->___scratchX_18);
		float L_27 = Random_get_value_m858(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_28 = (__this->___scratchJitter_10);
		float L_29 = (__this->___scratchY_19);
		float L_30 = Random_get_value_m858(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_31 = (__this->___scratchJitter_10);
		int32_t L_32 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture_t86 * L_33 = (__this->___scratchTexture_12);
		NullCheck(L_33);
		int32_t L_34 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_33);
		int32_t L_35 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture_t86 * L_36 = (__this->___scratchTexture_12);
		NullCheck(L_36);
		int32_t L_37 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_36);
		Vector4_t236  L_38 = {0};
		Vector4__ctor_m751(&L_38, ((float)((float)L_26+(float)((float)((float)L_27*(float)L_28)))), ((float)((float)L_29+(float)((float)((float)L_30*(float)L_31)))), ((float)((float)(((float)L_32))/(float)(((float)L_34)))), ((float)((float)(((float)L_35))/(float)(((float)L_37)))), /*hidden argument*/NULL);
		NullCheck(L_25);
		Material_SetVector_m752(L_25, (String_t*) &_stringLiteral126, L_38, /*hidden argument*/NULL);
		Material_t55 * L_39 = V_0;
		float L_40 = (__this->___grainIntensityMin_4);
		float L_41 = (__this->___grainIntensityMax_5);
		float L_42 = Random_Range_m856(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		float L_43 = (__this->___scratchIntensityMin_7);
		float L_44 = (__this->___scratchIntensityMax_8);
		float L_45 = Random_Range_m856(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		Vector4_t236  L_46 = {0};
		Vector4__ctor_m751(&L_46, L_42, L_45, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_39);
		Material_SetVector_m752(L_39, (String_t*) &_stringLiteral28, L_46, /*hidden argument*/NULL);
		RenderTexture_t101 * L_47 = ___source;
		RenderTexture_t101 * L_48 = ___destination;
		Material_t55 * L_49 = V_0;
		Graphics_Blit_m739(NULL /*static, unused*/, L_47, L_48, L_49, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::.ctor()
extern "C" void PostEffectsBase__ctor_m327 (PostEffectsBase_t57 * __this, const MethodInfo* method)
{
	{
		__this->___supportHDRTextures_2 = 1;
		__this->___isSupported_4 = 1;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t243_il2cpp_TypeInfo_var;
extern TypeInfo* Material_t55_il2cpp_TypeInfo_var;
extern "C" Material_t55 * PostEffectsBase_CheckShaderAndCreateMaterial_m328 (PostEffectsBase_t57 * __this, Shader_t54 * ___s, Material_t55 * ___m2Create, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		StringU5BU5D_t243_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		Material_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t54 * L_0 = ___s;
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral127, L_2, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		return (Material_t55 *)NULL;
	}

IL_0029:
	{
		Shader_t54 * L_4 = ___s;
		NullCheck(L_4);
		bool L_5 = Shader_get_isSupported_m736(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0052;
		}
	}
	{
		Material_t55 * L_6 = ___m2Create;
		bool L_7 = Object_op_Implicit_m629(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0052;
		}
	}
	{
		Material_t55 * L_8 = ___m2Create;
		NullCheck(L_8);
		Shader_t54 * L_9 = Material_get_shader_m767(L_8, /*hidden argument*/NULL);
		Shader_t54 * L_10 = ___s;
		bool L_11 = Object_op_Equality_m640(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0052;
		}
	}
	{
		Material_t55 * L_12 = ___m2Create;
		return L_12;
	}

IL_0052:
	{
		Shader_t54 * L_13 = ___s;
		NullCheck(L_13);
		bool L_14 = Shader_get_isSupported_m736(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_009f;
		}
	}
	{
		PostEffectsBase_NotSupported_m339(__this, /*hidden argument*/NULL);
		StringU5BU5D_t243* L_15 = ((StringU5BU5D_t243*)SZArrayNew(StringU5BU5D_t243_il2cpp_TypeInfo_var, 5));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, (String_t*) &_stringLiteral128);
		*((String_t**)(String_t**)SZArrayLdElema(L_15, 0)) = (String_t*)(String_t*) &_stringLiteral128;
		StringU5BU5D_t243* L_16 = L_15;
		Shader_t54 * L_17 = ___s;
		NullCheck(L_17);
		String_t* L_18 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, L_17);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_18);
		*((String_t**)(String_t**)SZArrayLdElema(L_16, 1)) = (String_t*)L_18;
		StringU5BU5D_t243* L_19 = L_16;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		ArrayElementTypeCheck (L_19, (String_t*) &_stringLiteral129);
		*((String_t**)(String_t**)SZArrayLdElema(L_19, 2)) = (String_t*)(String_t*) &_stringLiteral129;
		StringU5BU5D_t243* L_20 = L_19;
		String_t* L_21 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 3);
		ArrayElementTypeCheck (L_20, L_21);
		*((String_t**)(String_t**)SZArrayLdElema(L_20, 3)) = (String_t*)L_21;
		StringU5BU5D_t243* L_22 = L_20;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 4);
		ArrayElementTypeCheck (L_22, (String_t*) &_stringLiteral130);
		*((String_t**)(String_t**)SZArrayLdElema(L_22, 4)) = (String_t*)(String_t*) &_stringLiteral130;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m861(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		return (Material_t55 *)NULL;
	}

IL_009f:
	{
		Shader_t54 * L_24 = ___s;
		Material_t55 * L_25 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m763(L_25, L_24, /*hidden argument*/NULL);
		___m2Create = L_25;
		Material_t55 * L_26 = ___m2Create;
		NullCheck(L_26);
		Object_set_hideFlags_m764(L_26, ((int32_t)52), /*hidden argument*/NULL);
		Material_t55 * L_27 = ___m2Create;
		bool L_28 = Object_op_Implicit_m629(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00bc;
		}
	}
	{
		Material_t55 * L_29 = ___m2Create;
		return L_29;
	}

IL_00bc:
	{
		return (Material_t55 *)NULL;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Material_t55_il2cpp_TypeInfo_var;
extern "C" Material_t55 * PostEffectsBase_CreateMaterial_m329 (PostEffectsBase_t57 * __this, Shader_t54 * ___s, Material_t55 * ___m2Create, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Material_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t54 * L_0 = ___s;
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral127, L_2, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return (Material_t55 *)NULL;
	}

IL_0022:
	{
		Material_t55 * L_4 = ___m2Create;
		bool L_5 = Object_op_Implicit_m629(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		Material_t55 * L_6 = ___m2Create;
		NullCheck(L_6);
		Shader_t54 * L_7 = Material_get_shader_m767(L_6, /*hidden argument*/NULL);
		Shader_t54 * L_8 = ___s;
		bool L_9 = Object_op_Equality_m640(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004b;
		}
	}
	{
		Shader_t54 * L_10 = ___s;
		NullCheck(L_10);
		bool L_11 = Shader_get_isSupported_m736(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004b;
		}
	}
	{
		Material_t55 * L_12 = ___m2Create;
		return L_12;
	}

IL_004b:
	{
		Shader_t54 * L_13 = ___s;
		NullCheck(L_13);
		bool L_14 = Shader_get_isSupported_m736(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0058;
		}
	}
	{
		return (Material_t55 *)NULL;
	}

IL_0058:
	{
		Shader_t54 * L_15 = ___s;
		Material_t55 * L_16 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m763(L_16, L_15, /*hidden argument*/NULL);
		___m2Create = L_16;
		Material_t55 * L_17 = ___m2Create;
		NullCheck(L_17);
		Object_set_hideFlags_m764(L_17, ((int32_t)52), /*hidden argument*/NULL);
		Material_t55 * L_18 = ___m2Create;
		bool L_19 = Object_op_Implicit_m629(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0075;
		}
	}
	{
		Material_t55 * L_20 = ___m2Create;
		return L_20;
	}

IL_0075:
	{
		return (Material_t55 *)NULL;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::OnEnable()
extern "C" void PostEffectsBase_OnEnable_m330 (PostEffectsBase_t57 * __this, const MethodInfo* method)
{
	{
		__this->___isSupported_4 = 1;
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport()
extern "C" bool PostEffectsBase_CheckSupport_m331 (PostEffectsBase_t57 * __this, const MethodInfo* method)
{
	{
		bool L_0 = PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool PostEffectsBase_CheckResources_m332 (PostEffectsBase_t57 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m734(NULL /*static, unused*/, (String_t*) &_stringLiteral131, L_0, (String_t*) &_stringLiteral132, /*hidden argument*/NULL);
		Debug_LogWarning_m816(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_2 = (__this->___isSupported_4);
		return L_2;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::Start()
extern "C" void PostEffectsBase_Start_m333 (PostEffectsBase_t57 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources() */, __this);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean)
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" bool PostEffectsBase_CheckSupport_m334 (PostEffectsBase_t57 * __this, bool ___needDepth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	PostEffectsBase_t57 * G_B2_0 = {0};
	PostEffectsBase_t57 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	PostEffectsBase_t57 * G_B3_1 = {0};
	{
		__this->___isSupported_4 = 1;
		bool L_0 = SystemInfo_SupportsRenderTextureFormat_m781(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		__this->___supportHDRTextures_2 = L_0;
		int32_t L_1 = SystemInfo_get_graphicsShaderLevel_m862(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_1) < ((int32_t)((int32_t)50))))
		{
			G_B2_0 = __this;
			goto IL_0027;
		}
	}
	{
		bool L_2 = SystemInfo_get_supportsComputeShaders_m863(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		G_B3_1 = G_B1_0;
		goto IL_0028;
	}

IL_0027:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0028:
	{
		NullCheck(G_B3_1);
		G_B3_1->___supportDX11_3 = G_B3_0;
		bool L_3 = SystemInfo_get_supportsImageEffects_m765(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		bool L_4 = SystemInfo_get_supportsRenderTextures_m854(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0049;
		}
	}

IL_0041:
	{
		PostEffectsBase_NotSupported_m339(__this, /*hidden argument*/NULL);
		return 0;
	}

IL_0049:
	{
		bool L_5 = ___needDepth;
		if (!L_5)
		{
			goto IL_0062;
		}
	}
	{
		bool L_6 = SystemInfo_SupportsRenderTextureFormat_m781(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0062;
		}
	}
	{
		PostEffectsBase_NotSupported_m339(__this, /*hidden argument*/NULL);
		return 0;
	}

IL_0062:
	{
		bool L_7 = ___needDepth;
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		Camera_t27 * L_8 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		Camera_t27 * L_9 = L_8;
		NullCheck(L_9);
		int32_t L_10 = Camera_get_depthTextureMode_m779(L_9, /*hidden argument*/NULL);
		NullCheck(L_9);
		Camera_set_depthTextureMode_m780(L_9, ((int32_t)((int32_t)L_10|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_007b:
	{
		return 1;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean,System.Boolean)
extern "C" bool PostEffectsBase_CheckSupport_m335 (PostEffectsBase_t57 * __this, bool ___needDepth, bool ___needHdr, const MethodInfo* method)
{
	{
		bool L_0 = ___needDepth;
		bool L_1 = PostEffectsBase_CheckSupport_m334(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		bool L_2 = ___needHdr;
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		bool L_3 = (__this->___supportHDRTextures_2);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		PostEffectsBase_NotSupported_m339(__this, /*hidden argument*/NULL);
		return 0;
	}

IL_0027:
	{
		return 1;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::Dx11Support()
extern "C" bool PostEffectsBase_Dx11Support_m336 (PostEffectsBase_t57 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___supportDX11_3);
		return L_0;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::ReportAutoDisable()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void PostEffectsBase_ReportAutoDisable_m337 (PostEffectsBase_t57 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m734(NULL /*static, unused*/, (String_t*) &_stringLiteral133, L_0, (String_t*) &_stringLiteral134, /*hidden argument*/NULL);
		Debug_LogWarning_m816(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShader(UnityEngine.Shader)
extern TypeInfo* StringU5BU5D_t243_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool PostEffectsBase_CheckShader_m338 (PostEffectsBase_t57 * __this, Shader_t54 * ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t243_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t243* L_0 = ((StringU5BU5D_t243*)SZArrayNew(StringU5BU5D_t243_il2cpp_TypeInfo_var, 5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, (String_t*) &_stringLiteral128);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0)) = (String_t*)(String_t*) &_stringLiteral128;
		StringU5BU5D_t243* L_1 = L_0;
		Shader_t54 * L_2 = ___s;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		*((String_t**)(String_t**)SZArrayLdElema(L_1, 1)) = (String_t*)L_3;
		StringU5BU5D_t243* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, (String_t*) &_stringLiteral129);
		*((String_t**)(String_t**)SZArrayLdElema(L_4, 2)) = (String_t*)(String_t*) &_stringLiteral129;
		StringU5BU5D_t243* L_5 = L_4;
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, __this);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_6);
		*((String_t**)(String_t**)SZArrayLdElema(L_5, 3)) = (String_t*)L_6;
		StringU5BU5D_t243* L_7 = L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 4);
		ArrayElementTypeCheck (L_7, (String_t*) &_stringLiteral135);
		*((String_t**)(String_t**)SZArrayLdElema(L_7, 4)) = (String_t*)(String_t*) &_stringLiteral135;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m861(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Shader_t54 * L_9 = ___s;
		NullCheck(L_9);
		bool L_10 = Shader_get_isSupported_m736(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_004d;
		}
	}
	{
		PostEffectsBase_NotSupported_m339(__this, /*hidden argument*/NULL);
		return 0;
	}

IL_004d:
	{
		return 0;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::NotSupported()
extern "C" void PostEffectsBase_NotSupported_m339 (PostEffectsBase_t57 * __this, const MethodInfo* method)
{
	{
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		__this->___isSupported_4 = 0;
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern "C" void PostEffectsBase_DrawBorder_m340 (PostEffectsBase_t57 * __this, RenderTexture_t101 * ___dest, Material_t55 * ___material, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		RenderTexture_t101 * L_0 = ___dest;
		RenderTexture_set_active_m835(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_4 = 1;
		GL_PushMatrix_m836(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m847(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_028d;
	}

IL_001b:
	{
		Material_t55 * L_1 = ___material;
		int32_t L_2 = V_5;
		NullCheck(L_1);
		Material_SetPass_m831(L_1, L_2, /*hidden argument*/NULL);
		bool L_3 = V_4;
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		V_6 = (1.0f);
		V_7 = (0.0f);
		goto IL_004c;
	}

IL_003e:
	{
		V_6 = (0.0f);
		V_7 = (1.0f);
	}

IL_004c:
	{
		V_0 = (0.0f);
		RenderTexture_t101 * L_4 = ___dest;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_4);
		V_1 = ((float)((float)(0.0f)+(float)((float)((float)(1.0f)/(float)((float)((float)(((float)L_5))*(float)(1.0f)))))));
		V_2 = (0.0f);
		V_3 = (1.0f);
		GL_Begin_m848(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		float L_6 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_6, /*hidden argument*/NULL);
		float L_7 = V_0;
		float L_8 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_7, L_8, (0.1f), /*hidden argument*/NULL);
		float L_9 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_9, /*hidden argument*/NULL);
		float L_10 = V_1;
		float L_11 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_10, L_11, (0.1f), /*hidden argument*/NULL);
		float L_12 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_12, /*hidden argument*/NULL);
		float L_13 = V_1;
		float L_14 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_13, L_14, (0.1f), /*hidden argument*/NULL);
		float L_15 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_15, /*hidden argument*/NULL);
		float L_16 = V_0;
		float L_17 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_16, L_17, (0.1f), /*hidden argument*/NULL);
		RenderTexture_t101 * L_18 = ___dest;
		NullCheck(L_18);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_18);
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)L_19))*(float)(1.0f)))))));
		V_1 = (1.0f);
		V_2 = (0.0f);
		V_3 = (1.0f);
		float L_20 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_20, /*hidden argument*/NULL);
		float L_21 = V_0;
		float L_22 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_21, L_22, (0.1f), /*hidden argument*/NULL);
		float L_23 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_23, /*hidden argument*/NULL);
		float L_24 = V_1;
		float L_25 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_24, L_25, (0.1f), /*hidden argument*/NULL);
		float L_26 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_26, /*hidden argument*/NULL);
		float L_27 = V_1;
		float L_28 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_27, L_28, (0.1f), /*hidden argument*/NULL);
		float L_29 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_29, /*hidden argument*/NULL);
		float L_30 = V_0;
		float L_31 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_30, L_31, (0.1f), /*hidden argument*/NULL);
		V_0 = (0.0f);
		V_1 = (1.0f);
		V_2 = (0.0f);
		RenderTexture_t101 * L_32 = ___dest;
		NullCheck(L_32);
		int32_t L_33 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_32);
		V_3 = ((float)((float)(0.0f)+(float)((float)((float)(1.0f)/(float)((float)((float)(((float)L_33))*(float)(1.0f)))))));
		float L_34 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_34, /*hidden argument*/NULL);
		float L_35 = V_0;
		float L_36 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_35, L_36, (0.1f), /*hidden argument*/NULL);
		float L_37 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_37, /*hidden argument*/NULL);
		float L_38 = V_1;
		float L_39 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_38, L_39, (0.1f), /*hidden argument*/NULL);
		float L_40 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_40, /*hidden argument*/NULL);
		float L_41 = V_1;
		float L_42 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_41, L_42, (0.1f), /*hidden argument*/NULL);
		float L_43 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_43, /*hidden argument*/NULL);
		float L_44 = V_0;
		float L_45 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_44, L_45, (0.1f), /*hidden argument*/NULL);
		V_0 = (0.0f);
		V_1 = (1.0f);
		RenderTexture_t101 * L_46 = ___dest;
		NullCheck(L_46);
		int32_t L_47 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_46);
		V_2 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)L_47))*(float)(1.0f)))))));
		V_3 = (1.0f);
		float L_48 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_48, /*hidden argument*/NULL);
		float L_49 = V_0;
		float L_50 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_49, L_50, (0.1f), /*hidden argument*/NULL);
		float L_51 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_51, /*hidden argument*/NULL);
		float L_52 = V_1;
		float L_53 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_52, L_53, (0.1f), /*hidden argument*/NULL);
		float L_54 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_54, /*hidden argument*/NULL);
		float L_55 = V_1;
		float L_56 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_55, L_56, (0.1f), /*hidden argument*/NULL);
		float L_57 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_57, /*hidden argument*/NULL);
		float L_58 = V_0;
		float L_59 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_58, L_59, (0.1f), /*hidden argument*/NULL);
		GL_End_m851(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_60 = V_5;
		V_5 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_028d:
	{
		int32_t L_61 = V_5;
		Material_t55 * L_62 = ___material;
		NullCheck(L_62);
		int32_t L_63 = Material_get_passCount_m865(L_62, /*hidden argument*/NULL);
		if ((((int32_t)L_61) < ((int32_t)L_63)))
		{
			goto IL_001b;
		}
	}
	{
		GL_PopMatrix_m839(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.PostEffectsHelper
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_46.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.PostEffectsHelper
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_46MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::.ctor()
extern "C" void PostEffectsHelper__ctor_m341 (PostEffectsHelper_t112 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void PostEffectsHelper_OnRenderImage_m342 (PostEffectsHelper_t112 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	{
		Debug_Log_m857(NULL /*static, unused*/, (String_t*) &_stringLiteral136, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelPlaneAlignedWithCamera(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Camera)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m343 (Object_t * __this /* static, unused */, float ___dist, RenderTexture_t101 * ___source, RenderTexture_t101 * ___dest, Material_t55 * ___material, Camera_t27 * ___cameraForProjectionMatrix, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	{
		RenderTexture_t101 * L_0 = ___dest;
		RenderTexture_set_active_m835(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Material_t55 * L_1 = ___material;
		RenderTexture_t101 * L_2 = ___source;
		NullCheck(L_1);
		Material_SetTexture_m759(L_1, (String_t*) &_stringLiteral87, L_2, /*hidden argument*/NULL);
		V_0 = 1;
		GL_PushMatrix_m836(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadIdentity_m837(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t27 * L_3 = ___cameraForProjectionMatrix;
		NullCheck(L_3);
		Matrix4x4_t80  L_4 = Camera_get_projectionMatrix_m775(L_3, /*hidden argument*/NULL);
		GL_LoadProjectionMatrix_m866(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Camera_t27 * L_5 = ___cameraForProjectionMatrix;
		NullCheck(L_5);
		float L_6 = Camera_get_fieldOfView_m699(L_5, /*hidden argument*/NULL);
		V_1 = ((float)((float)((float)((float)L_6*(float)(0.5f)))*(float)(0.0174532924f)));
		float L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_8 = cosf(L_7);
		float L_9 = V_1;
		float L_10 = sinf(L_9);
		V_2 = ((float)((float)L_8/(float)L_10));
		Camera_t27 * L_11 = ___cameraForProjectionMatrix;
		NullCheck(L_11);
		float L_12 = Camera_get_aspect_m840(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = V_3;
		float L_14 = V_2;
		V_4 = ((float)((float)L_13/(float)((-L_14))));
		float L_15 = V_3;
		float L_16 = V_2;
		V_5 = ((float)((float)L_15/(float)L_16));
		float L_17 = V_2;
		V_6 = ((float)((float)(1.0f)/(float)((-L_17))));
		float L_18 = V_2;
		V_7 = ((float)((float)(1.0f)/(float)L_18));
		V_8 = (1.0f);
		float L_19 = V_4;
		float L_20 = ___dist;
		float L_21 = V_8;
		V_4 = ((float)((float)L_19*(float)((float)((float)L_20*(float)L_21))));
		float L_22 = V_5;
		float L_23 = ___dist;
		float L_24 = V_8;
		V_5 = ((float)((float)L_22*(float)((float)((float)L_23*(float)L_24))));
		float L_25 = V_6;
		float L_26 = ___dist;
		float L_27 = V_8;
		V_6 = ((float)((float)L_25*(float)((float)((float)L_26*(float)L_27))));
		float L_28 = V_7;
		float L_29 = ___dist;
		float L_30 = V_8;
		V_7 = ((float)((float)L_28*(float)((float)((float)L_29*(float)L_30))));
		float L_31 = ___dist;
		V_9 = ((-L_31));
		V_10 = 0;
		goto IL_0146;
	}

IL_00a9:
	{
		Material_t55 * L_32 = ___material;
		int32_t L_33 = V_10;
		NullCheck(L_32);
		Material_SetPass_m831(L_32, L_33, /*hidden argument*/NULL);
		GL_Begin_m848(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		bool L_34 = V_0;
		if (!L_34)
		{
			goto IL_00d1;
		}
	}
	{
		V_11 = (1.0f);
		V_12 = (0.0f);
		goto IL_00df;
	}

IL_00d1:
	{
		V_11 = (0.0f);
		V_12 = (1.0f);
	}

IL_00df:
	{
		float L_35 = V_11;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_35, /*hidden argument*/NULL);
		float L_36 = V_4;
		float L_37 = V_6;
		float L_38 = V_9;
		GL_Vertex3_m850(NULL /*static, unused*/, L_36, L_37, L_38, /*hidden argument*/NULL);
		float L_39 = V_11;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_39, /*hidden argument*/NULL);
		float L_40 = V_5;
		float L_41 = V_6;
		float L_42 = V_9;
		GL_Vertex3_m850(NULL /*static, unused*/, L_40, L_41, L_42, /*hidden argument*/NULL);
		float L_43 = V_12;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_43, /*hidden argument*/NULL);
		float L_44 = V_5;
		float L_45 = V_7;
		float L_46 = V_9;
		GL_Vertex3_m850(NULL /*static, unused*/, L_44, L_45, L_46, /*hidden argument*/NULL);
		float L_47 = V_12;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_47, /*hidden argument*/NULL);
		float L_48 = V_4;
		float L_49 = V_7;
		float L_50 = V_9;
		GL_Vertex3_m850(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		GL_End_m851(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_51 = V_10;
		V_10 = ((int32_t)((int32_t)L_51+(int32_t)1));
	}

IL_0146:
	{
		int32_t L_52 = V_10;
		Material_t55 * L_53 = ___material;
		NullCheck(L_53);
		int32_t L_54 = Material_get_passCount_m865(L_53, /*hidden argument*/NULL);
		if ((((int32_t)L_52) < ((int32_t)L_54)))
		{
			goto IL_00a9;
		}
	}
	{
		GL_PopMatrix_m839(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern "C" void PostEffectsHelper_DrawBorder_m344 (Object_t * __this /* static, unused */, RenderTexture_t101 * ___dest, Material_t55 * ___material, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		RenderTexture_t101 * L_0 = ___dest;
		RenderTexture_set_active_m835(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_4 = 1;
		GL_PushMatrix_m836(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m847(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_028d;
	}

IL_001b:
	{
		Material_t55 * L_1 = ___material;
		int32_t L_2 = V_5;
		NullCheck(L_1);
		Material_SetPass_m831(L_1, L_2, /*hidden argument*/NULL);
		bool L_3 = V_4;
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		V_6 = (1.0f);
		V_7 = (0.0f);
		goto IL_004c;
	}

IL_003e:
	{
		V_6 = (0.0f);
		V_7 = (1.0f);
	}

IL_004c:
	{
		V_0 = (0.0f);
		RenderTexture_t101 * L_4 = ___dest;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_4);
		V_1 = ((float)((float)(0.0f)+(float)((float)((float)(1.0f)/(float)((float)((float)(((float)L_5))*(float)(1.0f)))))));
		V_2 = (0.0f);
		V_3 = (1.0f);
		GL_Begin_m848(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		float L_6 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_6, /*hidden argument*/NULL);
		float L_7 = V_0;
		float L_8 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_7, L_8, (0.1f), /*hidden argument*/NULL);
		float L_9 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_9, /*hidden argument*/NULL);
		float L_10 = V_1;
		float L_11 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_10, L_11, (0.1f), /*hidden argument*/NULL);
		float L_12 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_12, /*hidden argument*/NULL);
		float L_13 = V_1;
		float L_14 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_13, L_14, (0.1f), /*hidden argument*/NULL);
		float L_15 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_15, /*hidden argument*/NULL);
		float L_16 = V_0;
		float L_17 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_16, L_17, (0.1f), /*hidden argument*/NULL);
		RenderTexture_t101 * L_18 = ___dest;
		NullCheck(L_18);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_18);
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)L_19))*(float)(1.0f)))))));
		V_1 = (1.0f);
		V_2 = (0.0f);
		V_3 = (1.0f);
		float L_20 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_20, /*hidden argument*/NULL);
		float L_21 = V_0;
		float L_22 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_21, L_22, (0.1f), /*hidden argument*/NULL);
		float L_23 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_23, /*hidden argument*/NULL);
		float L_24 = V_1;
		float L_25 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_24, L_25, (0.1f), /*hidden argument*/NULL);
		float L_26 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_26, /*hidden argument*/NULL);
		float L_27 = V_1;
		float L_28 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_27, L_28, (0.1f), /*hidden argument*/NULL);
		float L_29 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_29, /*hidden argument*/NULL);
		float L_30 = V_0;
		float L_31 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_30, L_31, (0.1f), /*hidden argument*/NULL);
		V_0 = (0.0f);
		V_1 = (1.0f);
		V_2 = (0.0f);
		RenderTexture_t101 * L_32 = ___dest;
		NullCheck(L_32);
		int32_t L_33 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_32);
		V_3 = ((float)((float)(0.0f)+(float)((float)((float)(1.0f)/(float)((float)((float)(((float)L_33))*(float)(1.0f)))))));
		float L_34 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_34, /*hidden argument*/NULL);
		float L_35 = V_0;
		float L_36 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_35, L_36, (0.1f), /*hidden argument*/NULL);
		float L_37 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_37, /*hidden argument*/NULL);
		float L_38 = V_1;
		float L_39 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_38, L_39, (0.1f), /*hidden argument*/NULL);
		float L_40 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_40, /*hidden argument*/NULL);
		float L_41 = V_1;
		float L_42 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_41, L_42, (0.1f), /*hidden argument*/NULL);
		float L_43 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_43, /*hidden argument*/NULL);
		float L_44 = V_0;
		float L_45 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_44, L_45, (0.1f), /*hidden argument*/NULL);
		V_0 = (0.0f);
		V_1 = (1.0f);
		RenderTexture_t101 * L_46 = ___dest;
		NullCheck(L_46);
		int32_t L_47 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_46);
		V_2 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)L_47))*(float)(1.0f)))))));
		V_3 = (1.0f);
		float L_48 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_48, /*hidden argument*/NULL);
		float L_49 = V_0;
		float L_50 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_49, L_50, (0.1f), /*hidden argument*/NULL);
		float L_51 = V_6;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_51, /*hidden argument*/NULL);
		float L_52 = V_1;
		float L_53 = V_2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_52, L_53, (0.1f), /*hidden argument*/NULL);
		float L_54 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_54, /*hidden argument*/NULL);
		float L_55 = V_1;
		float L_56 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_55, L_56, (0.1f), /*hidden argument*/NULL);
		float L_57 = V_7;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_57, /*hidden argument*/NULL);
		float L_58 = V_0;
		float L_59 = V_3;
		GL_Vertex3_m850(NULL /*static, unused*/, L_58, L_59, (0.1f), /*hidden argument*/NULL);
		GL_End_m851(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_60 = V_5;
		V_5 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_028d:
	{
		int32_t L_61 = V_5;
		Material_t55 * L_62 = ___material;
		NullCheck(L_62);
		int32_t L_63 = Material_get_passCount_m865(L_62, /*hidden argument*/NULL);
		if ((((int32_t)L_61) < ((int32_t)L_63)))
		{
			goto IL_001b;
		}
	}
	{
		GL_PopMatrix_m839(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelQuad(System.Single,System.Single,System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material)
extern "C" void PostEffectsHelper_DrawLowLevelQuad_m345 (Object_t * __this /* static, unused */, float ___x1, float ___x2, float ___y1, float ___y2, RenderTexture_t101 * ___source, RenderTexture_t101 * ___dest, Material_t55 * ___material, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		RenderTexture_t101 * L_0 = ___dest;
		RenderTexture_set_active_m835(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Material_t55 * L_1 = ___material;
		RenderTexture_t101 * L_2 = ___source;
		NullCheck(L_1);
		Material_SetTexture_m759(L_1, (String_t*) &_stringLiteral87, L_2, /*hidden argument*/NULL);
		V_0 = 1;
		GL_PushMatrix_m836(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m847(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_00bf;
	}

IL_0028:
	{
		Material_t55 * L_3 = ___material;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Material_SetPass_m831(L_3, L_4, /*hidden argument*/NULL);
		GL_Begin_m848(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_004e;
		}
	}
	{
		V_2 = (1.0f);
		V_3 = (0.0f);
		goto IL_005a;
	}

IL_004e:
	{
		V_2 = (0.0f);
		V_3 = (1.0f);
	}

IL_005a:
	{
		float L_6 = V_2;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_6, /*hidden argument*/NULL);
		float L_7 = ___x1;
		float L_8 = ___y1;
		GL_Vertex3_m850(NULL /*static, unused*/, L_7, L_8, (0.1f), /*hidden argument*/NULL);
		float L_9 = V_2;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_9, /*hidden argument*/NULL);
		float L_10 = ___x2;
		float L_11 = ___y1;
		GL_Vertex3_m850(NULL /*static, unused*/, L_10, L_11, (0.1f), /*hidden argument*/NULL);
		float L_12 = V_3;
		GL_TexCoord2_m864(NULL /*static, unused*/, (1.0f), L_12, /*hidden argument*/NULL);
		float L_13 = ___x2;
		float L_14 = ___y2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_13, L_14, (0.1f), /*hidden argument*/NULL);
		float L_15 = V_3;
		GL_TexCoord2_m864(NULL /*static, unused*/, (0.0f), L_15, /*hidden argument*/NULL);
		float L_16 = ___x1;
		float L_17 = ___y2;
		GL_Vertex3_m850(NULL /*static, unused*/, L_16, L_17, (0.1f), /*hidden argument*/NULL);
		GL_End_m851(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_00bf:
	{
		int32_t L_19 = V_1;
		Material_t55 * L_20 = ___material;
		NullCheck(L_20);
		int32_t L_21 = Material_get_passCount_m865(L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0028;
		}
	}
	{
		GL_PopMatrix_m839(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.Quads
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_47.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"


// System.Void UnityStandardAssets.ImageEffects.Quads::.ctor()
extern "C" void Quads__ctor_m346 (Quads_t114 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Quads::.cctor()
extern "C" void Quads__cctor_m347 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.Quads::HasMeshes()
extern TypeInfo* Quads_t114_il2cpp_TypeInfo_var;
extern "C" bool Quads_HasMeshes_m348 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quads_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(56);
		s_Il2CppMethodIntialized = true;
	}
	Mesh_t216 * V_0 = {0};
	MeshU5BU5D_t113* V_1 = {0};
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_0 = ((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return 0;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_1 = ((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		V_1 = L_1;
		V_2 = 0;
		goto IL_002f;
	}

IL_0019:
	{
		MeshU5BU5D_t113* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(Mesh_t216 **)(Mesh_t216 **)SZArrayLdElema(L_2, L_4));
		Mesh_t216 * L_5 = V_0;
		bool L_6 = Object_op_Equality_m640(NULL /*static, unused*/, (Object_t164 *)NULL, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002b;
		}
	}
	{
		return 0;
	}

IL_002b:
	{
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_8 = V_2;
		MeshU5BU5D_t113* L_9 = V_1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)(((Array_t *)L_9)->max_length))))))
		{
			goto IL_0019;
		}
	}
	{
		return 1;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Quads::Cleanup()
extern TypeInfo* Quads_t114_il2cpp_TypeInfo_var;
extern "C" void Quads_Cleanup_m349 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quads_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(56);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_0 = ((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		V_0 = 0;
		goto IL_003c;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_1 = ((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		bool L_4 = Object_op_Inequality_m623(NULL /*static, unused*/, (Object_t164 *)NULL, (*(Mesh_t216 **)(Mesh_t216 **)SZArrayLdElema(L_1, L_3)), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_5 = ((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Object_DestroyImmediate_m761(NULL /*static, unused*/, (*(Mesh_t216 **)(Mesh_t216 **)SZArrayLdElema(L_5, L_7)), /*hidden argument*/NULL);
		MeshU5BU5D_t113* L_8 = ((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, NULL);
		*((Mesh_t216 **)(Mesh_t216 **)SZArrayLdElema(L_8, L_9)) = (Mesh_t216 *)NULL;
	}

IL_0038:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_12 = ((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_12)->max_length))))))
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0 = (MeshU5BU5D_t113*)NULL;
		return;
	}
}
// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Quads::GetMeshes(System.Int32,System.Int32)
extern TypeInfo* Quads_t114_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* MeshU5BU5D_t113_il2cpp_TypeInfo_var;
extern "C" MeshU5BU5D_t113* Quads_GetMeshes_m350 (Object_t * __this /* static, unused */, int32_t ___totalWidth, int32_t ___totalHeight, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quads_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(56);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		MeshU5BU5D_t113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		bool L_0 = Quads_HasMeshes_m348(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___currentQuads_1;
		int32_t L_2 = ___totalWidth;
		int32_t L_3 = ___totalHeight;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)((int32_t)L_2*(int32_t)L_3))))))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_4 = ((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		return L_4;
	}

IL_001d:
	{
		V_0 = ((int32_t)10833);
		int32_t L_5 = ___totalWidth;
		int32_t L_6 = ___totalHeight;
		V_1 = ((int32_t)((int32_t)L_5*(int32_t)L_6));
		int32_t L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___currentQuads_1 = L_7;
		int32_t L_8 = V_1;
		int32_t L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_CeilToInt_m867(NULL /*static, unused*/, ((float)((float)((float)((float)(1.0f)*(float)(((float)L_8))))/(float)((float)((float)(1.0f)*(float)(((float)L_9)))))), /*hidden argument*/NULL);
		V_2 = L_10;
		int32_t L_11 = V_2;
		((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0 = ((MeshU5BU5D_t113*)SZArrayNew(MeshU5BU5D_t113_il2cpp_TypeInfo_var, L_11));
		V_3 = 0;
		V_4 = 0;
		V_3 = 0;
		goto IL_0089;
	}

IL_005b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_3;
		int32_t L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_15 = Mathf_Clamp_m712(NULL /*static, unused*/, ((int32_t)((int32_t)L_12-(int32_t)L_13)), 0, L_14, /*hidden argument*/NULL);
		int32_t L_16 = Mathf_FloorToInt_m815(NULL /*static, unused*/, (((float)L_15)), /*hidden argument*/NULL);
		V_5 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_17 = ((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		int32_t L_18 = V_4;
		int32_t L_19 = V_5;
		int32_t L_20 = V_3;
		int32_t L_21 = ___totalWidth;
		int32_t L_22 = ___totalHeight;
		Mesh_t216 * L_23 = Quads_GetMesh_m351(NULL /*static, unused*/, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		ArrayElementTypeCheck (L_17, L_23);
		*((Mesh_t216 **)(Mesh_t216 **)SZArrayLdElema(L_17, L_18)) = (Mesh_t216 *)L_23;
		int32_t L_24 = V_4;
		V_4 = ((int32_t)((int32_t)L_24+(int32_t)1));
		int32_t L_25 = V_3;
		int32_t L_26 = V_0;
		V_3 = ((int32_t)((int32_t)L_25+(int32_t)L_26));
	}

IL_0089:
	{
		int32_t L_27 = V_3;
		int32_t L_28 = V_1;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quads_t114_il2cpp_TypeInfo_var);
		MeshU5BU5D_t113* L_29 = ((Quads_t114_StaticFields*)Quads_t114_il2cpp_TypeInfo_var->static_fields)->___meshes_0;
		return L_29;
	}
}
// UnityEngine.Mesh UnityStandardAssets.ImageEffects.Quads::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* Mesh_t216_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3U5BU5D_t210_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2U5BU5D_t237_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t242_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" Mesh_t216 * Quads_GetMesh_m351 (Object_t * __this /* static, unused */, int32_t ___triCount, int32_t ___triOffset, int32_t ___totalWidth, int32_t ___totalHeight, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mesh_t216_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		Vector3U5BU5D_t210_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		Vector2U5BU5D_t237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Int32U5BU5D_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Mesh_t216 * V_0 = {0};
	Vector3U5BU5D_t210* V_1 = {0};
	Vector2U5BU5D_t237* V_2 = {0};
	Vector2U5BU5D_t237* V_3 = {0};
	Int32U5BU5D_t242* V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Vector3_t4  V_11 = {0};
	{
		Mesh_t216 * L_0 = (Mesh_t216 *)il2cpp_codegen_object_new (Mesh_t216_il2cpp_TypeInfo_var);
		Mesh__ctor_m868(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Mesh_t216 * L_1 = V_0;
		NullCheck(L_1);
		Object_set_hideFlags_m764(L_1, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_2 = ___triCount;
		V_1 = ((Vector3U5BU5D_t210*)SZArrayNew(Vector3U5BU5D_t210_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_2*(int32_t)4))));
		int32_t L_3 = ___triCount;
		V_2 = ((Vector2U5BU5D_t237*)SZArrayNew(Vector2U5BU5D_t237_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_3*(int32_t)4))));
		int32_t L_4 = ___triCount;
		V_3 = ((Vector2U5BU5D_t237*)SZArrayNew(Vector2U5BU5D_t237_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_4*(int32_t)4))));
		int32_t L_5 = ___triCount;
		V_4 = ((Int32U5BU5D_t242*)SZArrayNew(Int32U5BU5D_t242_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_5*(int32_t)6))));
		V_5 = 0;
		goto IL_01ed;
	}

IL_003b:
	{
		int32_t L_6 = V_5;
		V_6 = ((int32_t)((int32_t)L_6*(int32_t)4));
		int32_t L_7 = V_5;
		V_7 = ((int32_t)((int32_t)L_7*(int32_t)6));
		int32_t L_8 = ___triOffset;
		int32_t L_9 = V_5;
		V_8 = ((int32_t)((int32_t)L_8+(int32_t)L_9));
		int32_t L_10 = V_8;
		int32_t L_11 = ___totalWidth;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_12 = floorf((((float)((int32_t)((int32_t)L_10%(int32_t)L_11)))));
		int32_t L_13 = ___totalWidth;
		V_9 = ((float)((float)L_12/(float)(((float)L_13))));
		int32_t L_14 = V_8;
		int32_t L_15 = ___totalWidth;
		float L_16 = floorf((((float)((int32_t)((int32_t)L_14/(int32_t)L_15)))));
		int32_t L_17 = ___totalHeight;
		V_10 = ((float)((float)L_16/(float)(((float)L_17))));
		float L_18 = V_9;
		float L_19 = V_10;
		Vector3__ctor_m612((&V_11), ((float)((float)((float)((float)L_18*(float)(2.0f)))-(float)(1.0f))), ((float)((float)((float)((float)L_19*(float)(2.0f)))-(float)(1.0f))), (1.0f), /*hidden argument*/NULL);
		Vector3U5BU5D_t210* L_20 = V_1;
		int32_t L_21 = V_6;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		Vector3_t4  L_22 = V_11;
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_20, L_21)) = L_22;
		Vector3U5BU5D_t210* L_23 = V_1;
		int32_t L_24 = V_6;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)((int32_t)L_24+(int32_t)1)));
		Vector3_t4  L_25 = V_11;
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_23, ((int32_t)((int32_t)L_24+(int32_t)1)))) = L_25;
		Vector3U5BU5D_t210* L_26 = V_1;
		int32_t L_27 = V_6;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)((int32_t)L_27+(int32_t)2)));
		Vector3_t4  L_28 = V_11;
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_26, ((int32_t)((int32_t)L_27+(int32_t)2)))) = L_28;
		Vector3U5BU5D_t210* L_29 = V_1;
		int32_t L_30 = V_6;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)((int32_t)L_30+(int32_t)3)));
		Vector3_t4  L_31 = V_11;
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_29, ((int32_t)((int32_t)L_30+(int32_t)3)))) = L_31;
		Vector2U5BU5D_t237* L_32 = V_2;
		int32_t L_33 = V_6;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		Vector2_t6  L_34 = {0};
		Vector2__ctor_m630(&L_34, (0.0f), (0.0f), /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_32, L_33)) = L_34;
		Vector2U5BU5D_t237* L_35 = V_2;
		int32_t L_36 = V_6;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, ((int32_t)((int32_t)L_36+(int32_t)1)));
		Vector2_t6  L_37 = {0};
		Vector2__ctor_m630(&L_37, (1.0f), (0.0f), /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_35, ((int32_t)((int32_t)L_36+(int32_t)1)))) = L_37;
		Vector2U5BU5D_t237* L_38 = V_2;
		int32_t L_39 = V_6;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)((int32_t)L_39+(int32_t)2)));
		Vector2_t6  L_40 = {0};
		Vector2__ctor_m630(&L_40, (0.0f), (1.0f), /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_38, ((int32_t)((int32_t)L_39+(int32_t)2)))) = L_40;
		Vector2U5BU5D_t237* L_41 = V_2;
		int32_t L_42 = V_6;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, ((int32_t)((int32_t)L_42+(int32_t)3)));
		Vector2_t6  L_43 = {0};
		Vector2__ctor_m630(&L_43, (1.0f), (1.0f), /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_41, ((int32_t)((int32_t)L_42+(int32_t)3)))) = L_43;
		Vector2U5BU5D_t237* L_44 = V_3;
		int32_t L_45 = V_6;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		float L_46 = V_9;
		float L_47 = V_10;
		Vector2_t6  L_48 = {0};
		Vector2__ctor_m630(&L_48, L_46, L_47, /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_44, L_45)) = L_48;
		Vector2U5BU5D_t237* L_49 = V_3;
		int32_t L_50 = V_6;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, ((int32_t)((int32_t)L_50+(int32_t)1)));
		float L_51 = V_9;
		float L_52 = V_10;
		Vector2_t6  L_53 = {0};
		Vector2__ctor_m630(&L_53, L_51, L_52, /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_49, ((int32_t)((int32_t)L_50+(int32_t)1)))) = L_53;
		Vector2U5BU5D_t237* L_54 = V_3;
		int32_t L_55 = V_6;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)((int32_t)L_55+(int32_t)2)));
		float L_56 = V_9;
		float L_57 = V_10;
		Vector2_t6  L_58 = {0};
		Vector2__ctor_m630(&L_58, L_56, L_57, /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_54, ((int32_t)((int32_t)L_55+(int32_t)2)))) = L_58;
		Vector2U5BU5D_t237* L_59 = V_3;
		int32_t L_60 = V_6;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, ((int32_t)((int32_t)L_60+(int32_t)3)));
		float L_61 = V_9;
		float L_62 = V_10;
		Vector2_t6  L_63 = {0};
		Vector2__ctor_m630(&L_63, L_61, L_62, /*hidden argument*/NULL);
		*((Vector2_t6 *)(Vector2_t6 *)SZArrayLdElema(L_59, ((int32_t)((int32_t)L_60+(int32_t)3)))) = L_63;
		Int32U5BU5D_t242* L_64 = V_4;
		int32_t L_65 = V_7;
		int32_t L_66 = V_6;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_65);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_64, L_65)) = (int32_t)L_66;
		Int32U5BU5D_t242* L_67 = V_4;
		int32_t L_68 = V_7;
		int32_t L_69 = V_6;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)((int32_t)L_68+(int32_t)1)));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_67, ((int32_t)((int32_t)L_68+(int32_t)1)))) = (int32_t)((int32_t)((int32_t)L_69+(int32_t)1));
		Int32U5BU5D_t242* L_70 = V_4;
		int32_t L_71 = V_7;
		int32_t L_72 = V_6;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, ((int32_t)((int32_t)L_71+(int32_t)2)));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_70, ((int32_t)((int32_t)L_71+(int32_t)2)))) = (int32_t)((int32_t)((int32_t)L_72+(int32_t)2));
		Int32U5BU5D_t242* L_73 = V_4;
		int32_t L_74 = V_7;
		int32_t L_75 = V_6;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)((int32_t)L_74+(int32_t)3)));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_73, ((int32_t)((int32_t)L_74+(int32_t)3)))) = (int32_t)((int32_t)((int32_t)L_75+(int32_t)1));
		Int32U5BU5D_t242* L_76 = V_4;
		int32_t L_77 = V_7;
		int32_t L_78 = V_6;
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)((int32_t)L_77+(int32_t)4)));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_76, ((int32_t)((int32_t)L_77+(int32_t)4)))) = (int32_t)((int32_t)((int32_t)L_78+(int32_t)2));
		Int32U5BU5D_t242* L_79 = V_4;
		int32_t L_80 = V_7;
		int32_t L_81 = V_6;
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, ((int32_t)((int32_t)L_80+(int32_t)5)));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_79, ((int32_t)((int32_t)L_80+(int32_t)5)))) = (int32_t)((int32_t)((int32_t)L_81+(int32_t)3));
		int32_t L_82 = V_5;
		V_5 = ((int32_t)((int32_t)L_82+(int32_t)1));
	}

IL_01ed:
	{
		int32_t L_83 = V_5;
		int32_t L_84 = ___triCount;
		if ((((int32_t)L_83) < ((int32_t)L_84)))
		{
			goto IL_003b;
		}
	}
	{
		Mesh_t216 * L_85 = V_0;
		Vector3U5BU5D_t210* L_86 = V_1;
		NullCheck(L_85);
		Mesh_set_vertices_m869(L_85, L_86, /*hidden argument*/NULL);
		Mesh_t216 * L_87 = V_0;
		Int32U5BU5D_t242* L_88 = V_4;
		NullCheck(L_87);
		Mesh_set_triangles_m870(L_87, L_88, /*hidden argument*/NULL);
		Mesh_t216 * L_89 = V_0;
		Vector2U5BU5D_t237* L_90 = V_2;
		NullCheck(L_89);
		Mesh_set_uv_m871(L_89, L_90, /*hidden argument*/NULL);
		Mesh_t216 * L_91 = V_0;
		Vector2U5BU5D_t237* L_92 = V_3;
		NullCheck(L_91);
		Mesh_set_uv2_m872(L_91, L_92, /*hidden argument*/NULL);
		Mesh_t216 * L_93 = V_0;
		return L_93;
	}
}
// UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_48.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_48MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.ScreenOverlay
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_49.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ScreenOverlay
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_49MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.ScreenOverlay::.ctor()
extern "C" void ScreenOverlay__ctor_m352 (ScreenOverlay_t116 * __this, const MethodInfo* method)
{
	{
		__this->___blendMode_5 = 3;
		__this->___intensity_6 = (1.0f);
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.ScreenOverlay::CheckResources()
extern "C" bool ScreenOverlay_CheckResources_m353 (ScreenOverlay_t116 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 0, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___overlayShader_8);
		Material_t55 * L_1 = (__this->___overlayMaterial_9);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___overlayMaterial_9 = L_2;
		bool L_3 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		bool L_4 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_4;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ScreenOverlay::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ScreenOverlay_OnRenderImage_m354 (ScreenOverlay_t116 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	Vector4_t236  V_0 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.ScreenOverlay::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		Vector4__ctor_m751((&V_0), (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Material_t55 * L_3 = (__this->___overlayMaterial_9);
		Vector4_t236  L_4 = V_0;
		NullCheck(L_3);
		Material_SetVector_m752(L_3, (String_t*) &_stringLiteral137, L_4, /*hidden argument*/NULL);
		Material_t55 * L_5 = (__this->___overlayMaterial_9);
		float L_6 = (__this->___intensity_6);
		NullCheck(L_5);
		Material_SetFloat_m738(L_5, (String_t*) &_stringLiteral28, L_6, /*hidden argument*/NULL);
		Material_t55 * L_7 = (__this->___overlayMaterial_9);
		Texture2D_t63 * L_8 = (__this->___texture_7);
		NullCheck(L_7);
		Material_SetTexture_m759(L_7, (String_t*) &_stringLiteral138, L_8, /*hidden argument*/NULL);
		RenderTexture_t101 * L_9 = ___source;
		RenderTexture_t101 * L_10 = ___destination;
		Material_t55 * L_11 = (__this->___overlayMaterial_9);
		int32_t L_12 = (__this->___blendMode_5);
		Graphics_Blit_m742(NULL /*static, unused*/, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_50.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_50MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::.ctor()
extern "C" void ScreenSpaceAmbientObscurance__ctor_m355 (ScreenSpaceAmbientObscurance_t117 * __this, const MethodInfo* method)
{
	{
		__this->___intensity_5 = (0.5f);
		__this->___radius_6 = (0.2f);
		__this->___blurIterations_7 = 1;
		__this->___blurFilterDistance_8 = (1.25f);
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::CheckResources()
extern "C" bool ScreenSpaceAmbientObscurance_CheckResources_m356 (ScreenSpaceAmbientObscurance_t117 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 1, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___aoShader_11);
		Material_t55 * L_1 = (__this->___aoMaterial_12);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___aoMaterial_12 = L_2;
		bool L_3 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		bool L_4 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_4;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnDisable()
extern "C" void ScreenSpaceAmbientObscurance_OnDisable_m357 (ScreenSpaceAmbientObscurance_t117 * __this, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___aoMaterial_12);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t55 * L_2 = (__this->___aoMaterial_12);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		__this->___aoMaterial_12 = (Material_t55 *)NULL;
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void ScreenSpaceAmbientObscurance_OnRenderImage_m358 (ScreenSpaceAmbientObscurance_t117 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t80  V_0 = {0};
	Matrix4x4_t80  V_1 = {0};
	Vector4_t236  V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	RenderTexture_t101 * V_5 = {0};
	RenderTexture_t101 * V_6 = {0};
	int32_t V_7 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		Camera_t27 * L_3 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_3);
		Matrix4x4_t80  L_4 = Camera_get_projectionMatrix_m775(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Matrix4x4_t80  L_5 = Matrix4x4_get_inverse_m873((&V_0), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = Matrix4x4_get_Item_m874((&V_0), 0, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = Matrix4x4_get_Item_m874((&V_0), 5, /*hidden argument*/NULL);
		float L_10 = Matrix4x4_get_Item_m874((&V_0), 2, /*hidden argument*/NULL);
		float L_11 = Matrix4x4_get_Item_m874((&V_0), 0, /*hidden argument*/NULL);
		float L_12 = Matrix4x4_get_Item_m874((&V_0), 6, /*hidden argument*/NULL);
		float L_13 = Matrix4x4_get_Item_m874((&V_0), 5, /*hidden argument*/NULL);
		Vector4__ctor_m751((&V_2), ((float)((float)(-2.0f)/(float)((float)((float)(((float)L_6))*(float)L_7)))), ((float)((float)(-2.0f)/(float)((float)((float)(((float)L_8))*(float)L_9)))), ((float)((float)((float)((float)(1.0f)-(float)L_10))/(float)L_11)), ((float)((float)((float)((float)(1.0f)+(float)L_12))/(float)L_13)), /*hidden argument*/NULL);
		Material_t55 * L_14 = (__this->___aoMaterial_12);
		Vector4_t236  L_15 = V_2;
		NullCheck(L_14);
		Material_SetVector_m752(L_14, (String_t*) &_stringLiteral139, L_15, /*hidden argument*/NULL);
		Material_t55 * L_16 = (__this->___aoMaterial_12);
		Matrix4x4_t80  L_17 = V_1;
		NullCheck(L_16);
		Material_SetMatrix_m786(L_16, (String_t*) &_stringLiteral140, L_17, /*hidden argument*/NULL);
		Material_t55 * L_18 = (__this->___aoMaterial_12);
		Texture2D_t63 * L_19 = (__this->___rand_10);
		NullCheck(L_18);
		Material_SetTexture_m759(L_18, (String_t*) &_stringLiteral141, L_19, /*hidden argument*/NULL);
		Material_t55 * L_20 = (__this->___aoMaterial_12);
		float L_21 = (__this->___radius_6);
		NullCheck(L_20);
		Material_SetFloat_m738(L_20, (String_t*) &_stringLiteral142, L_21, /*hidden argument*/NULL);
		Material_t55 * L_22 = (__this->___aoMaterial_12);
		float L_23 = (__this->___radius_6);
		float L_24 = (__this->___radius_6);
		NullCheck(L_22);
		Material_SetFloat_m738(L_22, (String_t*) &_stringLiteral143, ((float)((float)L_23*(float)L_24)), /*hidden argument*/NULL);
		Material_t55 * L_25 = (__this->___aoMaterial_12);
		float L_26 = (__this->___intensity_5);
		NullCheck(L_25);
		Material_SetFloat_m738(L_25, (String_t*) &_stringLiteral28, L_26, /*hidden argument*/NULL);
		Material_t55 * L_27 = (__this->___aoMaterial_12);
		float L_28 = (__this->___blurFilterDistance_8);
		NullCheck(L_27);
		Material_SetFloat_m738(L_27, (String_t*) &_stringLiteral144, L_28, /*hidden argument*/NULL);
		RenderTexture_t101 * L_29 = ___source;
		NullCheck(L_29);
		int32_t L_30 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_29);
		V_3 = L_30;
		RenderTexture_t101 * L_31 = ___source;
		NullCheck(L_31);
		int32_t L_32 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_31);
		V_4 = L_32;
		int32_t L_33 = V_3;
		int32_t L_34 = (__this->___downsample_9);
		int32_t L_35 = V_4;
		int32_t L_36 = (__this->___downsample_9);
		RenderTexture_t101 * L_37 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, ((int32_t)((int32_t)L_33>>(int32_t)((int32_t)((int32_t)L_34&(int32_t)((int32_t)31))))), ((int32_t)((int32_t)L_35>>(int32_t)((int32_t)((int32_t)L_36&(int32_t)((int32_t)31))))), /*hidden argument*/NULL);
		V_5 = L_37;
		RenderTexture_t101 * L_38 = ___source;
		RenderTexture_t101 * L_39 = V_5;
		Material_t55 * L_40 = (__this->___aoMaterial_12);
		Graphics_Blit_m742(NULL /*static, unused*/, L_38, L_39, L_40, 0, /*hidden argument*/NULL);
		int32_t L_41 = (__this->___downsample_9);
		if ((((int32_t)L_41) <= ((int32_t)0)))
		{
			goto IL_018a;
		}
	}
	{
		int32_t L_42 = V_3;
		int32_t L_43 = V_4;
		RenderTexture_t101 * L_44 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, L_42, L_43, /*hidden argument*/NULL);
		V_6 = L_44;
		RenderTexture_t101 * L_45 = V_5;
		RenderTexture_t101 * L_46 = V_6;
		Material_t55 * L_47 = (__this->___aoMaterial_12);
		Graphics_Blit_m742(NULL /*static, unused*/, L_45, L_46, L_47, 4, /*hidden argument*/NULL);
		RenderTexture_t101 * L_48 = V_5;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		RenderTexture_t101 * L_49 = V_6;
		V_5 = L_49;
	}

IL_018a:
	{
		V_7 = 0;
		goto IL_0222;
	}

IL_0192:
	{
		Material_t55 * L_50 = (__this->___aoMaterial_12);
		Vector2_t6  L_51 = {0};
		Vector2__ctor_m630(&L_51, (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t236  L_52 = Vector4_op_Implicit_m875(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		NullCheck(L_50);
		Material_SetVector_m752(L_50, (String_t*) &_stringLiteral145, L_52, /*hidden argument*/NULL);
		int32_t L_53 = V_3;
		int32_t L_54 = V_4;
		RenderTexture_t101 * L_55 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		V_6 = L_55;
		RenderTexture_t101 * L_56 = V_5;
		RenderTexture_t101 * L_57 = V_6;
		Material_t55 * L_58 = (__this->___aoMaterial_12);
		Graphics_Blit_m742(NULL /*static, unused*/, L_56, L_57, L_58, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_59 = V_5;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		Material_t55 * L_60 = (__this->___aoMaterial_12);
		Vector2_t6  L_61 = {0};
		Vector2__ctor_m630(&L_61, (0.0f), (1.0f), /*hidden argument*/NULL);
		Vector4_t236  L_62 = Vector4_op_Implicit_m875(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		NullCheck(L_60);
		Material_SetVector_m752(L_60, (String_t*) &_stringLiteral145, L_62, /*hidden argument*/NULL);
		int32_t L_63 = V_3;
		int32_t L_64 = V_4;
		RenderTexture_t101 * L_65 = RenderTexture_GetTemporary_m741(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
		V_5 = L_65;
		RenderTexture_t101 * L_66 = V_6;
		RenderTexture_t101 * L_67 = V_5;
		Material_t55 * L_68 = (__this->___aoMaterial_12);
		Graphics_Blit_m742(NULL /*static, unused*/, L_66, L_67, L_68, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_69 = V_6;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		int32_t L_70 = V_7;
		V_7 = ((int32_t)((int32_t)L_70+(int32_t)1));
	}

IL_0222:
	{
		int32_t L_71 = V_7;
		int32_t L_72 = (__this->___blurIterations_7);
		if ((((int32_t)L_71) < ((int32_t)L_72)))
		{
			goto IL_0192;
		}
	}
	{
		Material_t55 * L_73 = (__this->___aoMaterial_12);
		RenderTexture_t101 * L_74 = V_5;
		NullCheck(L_73);
		Material_SetTexture_m759(L_73, (String_t*) &_stringLiteral146, L_74, /*hidden argument*/NULL);
		RenderTexture_t101 * L_75 = ___source;
		RenderTexture_t101 * L_76 = ___destination;
		Material_t55 * L_77 = (__this->___aoMaterial_12);
		Graphics_Blit_m742(NULL /*static, unused*/, L_75, L_76, L_77, 2, /*hidden argument*/NULL);
		RenderTexture_t101 * L_78 = V_5;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_51.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_51MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_52.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_52MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::.ctor()
extern "C" void ScreenSpaceAmbientOcclusion__ctor_m359 (ScreenSpaceAmbientOcclusion_t119 * __this, const MethodInfo* method)
{
	{
		__this->___m_Radius_2 = (0.4f);
		__this->___m_SampleCount_3 = 1;
		__this->___m_OcclusionIntensity_4 = (1.5f);
		__this->___m_Blur_5 = 2;
		__this->___m_Downsampling_6 = 2;
		__this->___m_OcclusionAttenuation_7 = (1.0f);
		__this->___m_MinZ_8 = (0.01f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::CreateMaterial(UnityEngine.Shader)
extern TypeInfo* Material_t55_il2cpp_TypeInfo_var;
extern "C" Material_t55 * ScreenSpaceAmbientOcclusion_CreateMaterial_m360 (Object_t * __this /* static, unused */, Shader_t54 * ___shader, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	Material_t55 * V_0 = {0};
	{
		Shader_t54 * L_0 = ___shader;
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (Material_t55 *)NULL;
	}

IL_000d:
	{
		Shader_t54 * L_2 = ___shader;
		Material_t55 * L_3 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m763(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Material_t55 * L_4 = V_0;
		NullCheck(L_4);
		Object_set_hideFlags_m764(L_4, ((int32_t)61), /*hidden argument*/NULL);
		Material_t55 * L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::DestroyMaterial(UnityEngine.Material)
extern "C" void ScreenSpaceAmbientOcclusion_DestroyMaterial_m361 (Object_t * __this /* static, unused */, Material_t55 * ___mat, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = ___mat;
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		Material_t55 * L_2 = ___mat;
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		___mat = (Material_t55 *)NULL;
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnDisable()
extern "C" void ScreenSpaceAmbientOcclusion_OnDisable_m362 (ScreenSpaceAmbientOcclusion_t119 * __this, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___m_SSAOMaterial_10);
		ScreenSpaceAmbientOcclusion_DestroyMaterial_m361(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::Start()
extern "C" void ScreenSpaceAmbientOcclusion_Start_m363 (ScreenSpaceAmbientOcclusion_t119 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m765(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		bool L_1 = SystemInfo_SupportsRenderTextureFormat_m781(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0024;
		}
	}

IL_0015:
	{
		__this->___m_Supported_12 = 0;
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0024:
	{
		ScreenSpaceAmbientOcclusion_CreateMaterials_m365(__this, /*hidden argument*/NULL);
		Material_t55 * L_2 = (__this->___m_SSAOMaterial_10);
		bool L_3 = Object_op_Implicit_m629(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		Material_t55 * L_4 = (__this->___m_SSAOMaterial_10);
		NullCheck(L_4);
		int32_t L_5 = Material_get_passCount_m865(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)5)))
		{
			goto IL_005a;
		}
	}

IL_004b:
	{
		__this->___m_Supported_12 = 0;
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_005a:
	{
		__this->___m_Supported_12 = 1;
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnEnable()
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void ScreenSpaceAmbientOcclusion_OnEnable_m364 (ScreenSpaceAmbientOcclusion_t119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t27 * L_0 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		Camera_t27 * L_1 = L_0;
		NullCheck(L_1);
		int32_t L_2 = Camera_get_depthTextureMode_m779(L_1, /*hidden argument*/NULL);
		NullCheck(L_1);
		Camera_set_depthTextureMode_m780(L_1, ((int32_t)((int32_t)L_2|(int32_t)2)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::CreateMaterials()
extern "C" void ScreenSpaceAmbientOcclusion_CreateMaterials_m365 (ScreenSpaceAmbientOcclusion_t119 * __this, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___m_SSAOMaterial_10);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0047;
		}
	}
	{
		Shader_t54 * L_2 = (__this->___m_SSAOShader_9);
		NullCheck(L_2);
		bool L_3 = Shader_get_isSupported_m736(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0047;
		}
	}
	{
		Shader_t54 * L_4 = (__this->___m_SSAOShader_9);
		Material_t55 * L_5 = ScreenSpaceAmbientOcclusion_CreateMaterial_m360(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->___m_SSAOMaterial_10 = L_5;
		Material_t55 * L_6 = (__this->___m_SSAOMaterial_10);
		Texture2D_t63 * L_7 = (__this->___m_RandomTexture_11);
		NullCheck(L_6);
		Material_SetTexture_m759(L_6, (String_t*) &_stringLiteral147, L_7, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void ScreenSpaceAmbientOcclusion_OnRenderImage_m366 (ScreenSpaceAmbientOcclusion_t119 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t101 * V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	bool V_7 = false;
	RenderTexture_t101 * V_8 = {0};
	RenderTexture_t101 * V_9 = {0};
	RenderTexture_t101 * G_B9_0 = {0};
	{
		bool L_0 = (__this->___m_Supported_12);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Shader_t54 * L_1 = (__this->___m_SSAOShader_9);
		NullCheck(L_1);
		bool L_2 = Shader_get_isSupported_m736(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}

IL_001b:
	{
		Behaviour_set_enabled_m766(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		ScreenSpaceAmbientOcclusion_CreateMaterials_m365(__this, /*hidden argument*/NULL);
		int32_t L_3 = (__this->___m_Downsampling_6);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_Clamp_m712(NULL /*static, unused*/, L_3, 1, 6, /*hidden argument*/NULL);
		__this->___m_Downsampling_6 = L_4;
		float L_5 = (__this->___m_Radius_2);
		float L_6 = Mathf_Clamp_m611(NULL /*static, unused*/, L_5, (0.05f), (1.0f), /*hidden argument*/NULL);
		__this->___m_Radius_2 = L_6;
		float L_7 = (__this->___m_MinZ_8);
		float L_8 = Mathf_Clamp_m611(NULL /*static, unused*/, L_7, (1.0E-05f), (0.5f), /*hidden argument*/NULL);
		__this->___m_MinZ_8 = L_8;
		float L_9 = (__this->___m_OcclusionIntensity_4);
		float L_10 = Mathf_Clamp_m611(NULL /*static, unused*/, L_9, (0.5f), (4.0f), /*hidden argument*/NULL);
		__this->___m_OcclusionIntensity_4 = L_10;
		float L_11 = (__this->___m_OcclusionAttenuation_7);
		float L_12 = Mathf_Clamp_m611(NULL /*static, unused*/, L_11, (0.2f), (2.0f), /*hidden argument*/NULL);
		__this->___m_OcclusionAttenuation_7 = L_12;
		int32_t L_13 = (__this->___m_Blur_5);
		int32_t L_14 = Mathf_Clamp_m712(NULL /*static, unused*/, L_13, 0, 4, /*hidden argument*/NULL);
		__this->___m_Blur_5 = L_14;
		RenderTexture_t101 * L_15 = ___source;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_15);
		int32_t L_17 = (__this->___m_Downsampling_6);
		RenderTexture_t101 * L_18 = ___source;
		NullCheck(L_18);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_18);
		int32_t L_20 = (__this->___m_Downsampling_6);
		RenderTexture_t101 * L_21 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, ((int32_t)((int32_t)L_16/(int32_t)L_17)), ((int32_t)((int32_t)L_19/(int32_t)L_20)), 0, /*hidden argument*/NULL);
		V_0 = L_21;
		Camera_t27 * L_22 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_22);
		float L_23 = Camera_get_fieldOfView_m699(L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		Camera_t27 * L_24 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_24);
		float L_25 = Camera_get_farClipPlane_m825(L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		float L_26 = V_1;
		float L_27 = tanf(((float)((float)((float)((float)L_26*(float)(0.0174532924f)))*(float)(0.5f))));
		float L_28 = V_2;
		V_3 = ((float)((float)L_27*(float)L_28));
		float L_29 = V_3;
		Camera_t27 * L_30 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_30);
		float L_31 = Camera_get_aspect_m840(L_30, /*hidden argument*/NULL);
		V_4 = ((float)((float)L_29*(float)L_31));
		Material_t55 * L_32 = (__this->___m_SSAOMaterial_10);
		float L_33 = V_4;
		float L_34 = V_3;
		float L_35 = V_2;
		Vector3_t4  L_36 = {0};
		Vector3__ctor_m612(&L_36, L_33, L_34, L_35, /*hidden argument*/NULL);
		Vector4_t236  L_37 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		NullCheck(L_32);
		Material_SetVector_m752(L_32, (String_t*) &_stringLiteral148, L_37, /*hidden argument*/NULL);
		Texture2D_t63 * L_38 = (__this->___m_RandomTexture_11);
		bool L_39 = Object_op_Implicit_m629(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0165;
		}
	}
	{
		Texture2D_t63 * L_40 = (__this->___m_RandomTexture_11);
		NullCheck(L_40);
		int32_t L_41 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_40);
		V_5 = L_41;
		Texture2D_t63 * L_42 = (__this->___m_RandomTexture_11);
		NullCheck(L_42);
		int32_t L_43 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_42);
		V_6 = L_43;
		goto IL_016b;
	}

IL_0165:
	{
		V_5 = 1;
		V_6 = 1;
	}

IL_016b:
	{
		Material_t55 * L_44 = (__this->___m_SSAOMaterial_10);
		RenderTexture_t101 * L_45 = V_0;
		NullCheck(L_45);
		int32_t L_46 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_45);
		int32_t L_47 = V_5;
		RenderTexture_t101 * L_48 = V_0;
		NullCheck(L_48);
		int32_t L_49 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_48);
		int32_t L_50 = V_6;
		Vector3_t4  L_51 = {0};
		Vector3__ctor_m612(&L_51, ((float)((float)(((float)L_46))/(float)(((float)L_47)))), ((float)((float)(((float)L_49))/(float)(((float)L_50)))), (0.0f), /*hidden argument*/NULL);
		Vector4_t236  L_52 = Vector4_op_Implicit_m830(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		NullCheck(L_44);
		Material_SetVector_m752(L_44, (String_t*) &_stringLiteral149, L_52, /*hidden argument*/NULL);
		Material_t55 * L_53 = (__this->___m_SSAOMaterial_10);
		float L_54 = (__this->___m_Radius_2);
		float L_55 = (__this->___m_MinZ_8);
		float L_56 = (__this->___m_OcclusionAttenuation_7);
		float L_57 = (__this->___m_OcclusionIntensity_4);
		Vector4_t236  L_58 = {0};
		Vector4__ctor_m751(&L_58, L_54, L_55, ((float)((float)(1.0f)/(float)L_56)), L_57, /*hidden argument*/NULL);
		NullCheck(L_53);
		Material_SetVector_m752(L_53, (String_t*) &_stringLiteral150, L_58, /*hidden argument*/NULL);
		int32_t L_59 = (__this->___m_Blur_5);
		V_7 = ((((int32_t)L_59) > ((int32_t)0))? 1 : 0);
		bool L_60 = V_7;
		if (!L_60)
		{
			goto IL_01eb;
		}
	}
	{
		G_B9_0 = ((RenderTexture_t101 *)(NULL));
		goto IL_01ec;
	}

IL_01eb:
	{
		RenderTexture_t101 * L_61 = ___source;
		G_B9_0 = L_61;
	}

IL_01ec:
	{
		RenderTexture_t101 * L_62 = V_0;
		Material_t55 * L_63 = (__this->___m_SSAOMaterial_10);
		int32_t L_64 = (__this->___m_SampleCount_3);
		Graphics_Blit_m742(NULL /*static, unused*/, G_B9_0, L_62, L_63, L_64, /*hidden argument*/NULL);
		bool L_65 = V_7;
		if (!L_65)
		{
			goto IL_02e4;
		}
	}
	{
		RenderTexture_t101 * L_66 = ___source;
		NullCheck(L_66);
		int32_t L_67 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_66);
		RenderTexture_t101 * L_68 = ___source;
		NullCheck(L_68);
		int32_t L_69 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_68);
		RenderTexture_t101 * L_70 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, L_67, L_69, 0, /*hidden argument*/NULL);
		V_8 = L_70;
		Material_t55 * L_71 = (__this->___m_SSAOMaterial_10);
		int32_t L_72 = (__this->___m_Blur_5);
		RenderTexture_t101 * L_73 = ___source;
		NullCheck(L_73);
		int32_t L_74 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_73);
		Vector4_t236  L_75 = {0};
		Vector4__ctor_m751(&L_75, ((float)((float)(((float)L_72))/(float)(((float)L_74)))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_71);
		Material_SetVector_m752(L_71, (String_t*) &_stringLiteral151, L_75, /*hidden argument*/NULL);
		Material_t55 * L_76 = (__this->___m_SSAOMaterial_10);
		RenderTexture_t101 * L_77 = V_0;
		NullCheck(L_76);
		Material_SetTexture_m759(L_76, (String_t*) &_stringLiteral152, L_77, /*hidden argument*/NULL);
		RenderTexture_t101 * L_78 = V_8;
		Material_t55 * L_79 = (__this->___m_SSAOMaterial_10);
		Graphics_Blit_m742(NULL /*static, unused*/, (Texture_t86 *)NULL, L_78, L_79, 3, /*hidden argument*/NULL);
		RenderTexture_t101 * L_80 = V_0;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_80, /*hidden argument*/NULL);
		RenderTexture_t101 * L_81 = ___source;
		NullCheck(L_81);
		int32_t L_82 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_81);
		RenderTexture_t101 * L_83 = ___source;
		NullCheck(L_83);
		int32_t L_84 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_83);
		RenderTexture_t101 * L_85 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, L_82, L_84, 0, /*hidden argument*/NULL);
		V_9 = L_85;
		Material_t55 * L_86 = (__this->___m_SSAOMaterial_10);
		int32_t L_87 = (__this->___m_Blur_5);
		RenderTexture_t101 * L_88 = ___source;
		NullCheck(L_88);
		int32_t L_89 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_88);
		Vector4_t236  L_90 = {0};
		Vector4__ctor_m751(&L_90, (0.0f), ((float)((float)(((float)L_87))/(float)(((float)L_89)))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_86);
		Material_SetVector_m752(L_86, (String_t*) &_stringLiteral151, L_90, /*hidden argument*/NULL);
		Material_t55 * L_91 = (__this->___m_SSAOMaterial_10);
		RenderTexture_t101 * L_92 = V_8;
		NullCheck(L_91);
		Material_SetTexture_m759(L_91, (String_t*) &_stringLiteral152, L_92, /*hidden argument*/NULL);
		RenderTexture_t101 * L_93 = ___source;
		RenderTexture_t101 * L_94 = V_9;
		Material_t55 * L_95 = (__this->___m_SSAOMaterial_10);
		Graphics_Blit_m742(NULL /*static, unused*/, L_93, L_94, L_95, 3, /*hidden argument*/NULL);
		RenderTexture_t101 * L_96 = V_8;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
		RenderTexture_t101 * L_97 = V_9;
		V_0 = L_97;
	}

IL_02e4:
	{
		Material_t55 * L_98 = (__this->___m_SSAOMaterial_10);
		RenderTexture_t101 * L_99 = V_0;
		NullCheck(L_98);
		Material_SetTexture_m759(L_98, (String_t*) &_stringLiteral152, L_99, /*hidden argument*/NULL);
		RenderTexture_t101 * L_100 = ___source;
		RenderTexture_t101 * L_101 = ___destination;
		Material_t55 * L_102 = (__this->___m_SSAOMaterial_10);
		Graphics_Blit_m742(NULL /*static, unused*/, L_100, L_101, L_102, 4, /*hidden argument*/NULL);
		RenderTexture_t101 * L_103 = V_0;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_103, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.SepiaTone
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_53.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.SepiaTone
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_53MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.SepiaTone::.ctor()
extern "C" void SepiaTone__ctor_m367 (SepiaTone_t120 * __this, const MethodInfo* method)
{
	{
		ImageEffectBase__ctor_m304(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.SepiaTone::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void SepiaTone_OnRenderImage_m368 (SepiaTone_t120 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	{
		RenderTexture_t101 * L_0 = ___source;
		RenderTexture_t101 * L_1 = ___destination;
		Material_t55 * L_2 = ImageEffectBase_get_material_m306(__this, /*hidden argument*/NULL);
		Graphics_Blit_m739(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.SunShafts/SunShaftsResolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_54.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.SunShafts/SunShaftsResolution
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_54MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.SunShafts/ShaftsScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_55.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.SunShafts/ShaftsScreenBlendMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_55MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.SunShafts
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_56.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.SunShafts
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_56MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.SunShafts::.ctor()
extern "C" void SunShafts__ctor_m369 (SunShafts_t123 * __this, const MethodInfo* method)
{
	{
		__this->___resolution_5 = 1;
		__this->___radialBlurIterations_8 = 2;
		Color_t65  L_0 = Color_get_white_m745(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___sunColor_9 = L_0;
		Color_t65  L_1 = {0};
		Color__ctor_m807(&L_1, (0.87f), (0.74f), (0.65f), /*hidden argument*/NULL);
		__this->___sunThreshold_10 = L_1;
		__this->___sunShaftBlurRadius_11 = (2.5f);
		__this->___sunShaftIntensity_12 = (1.15f);
		__this->___maxRadius_13 = (0.75f);
		__this->___useDepthTexture_14 = 1;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.SunShafts::CheckResources()
extern "C" bool SunShafts_CheckResources_m370 (SunShafts_t123 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___useDepthTexture_14);
		PostEffectsBase_CheckSupport_m334(__this, L_0, /*hidden argument*/NULL);
		Shader_t54 * L_1 = (__this->___sunShaftsShader_15);
		Material_t55 * L_2 = (__this->___sunShaftsMaterial_16);
		Material_t55 * L_3 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_1, L_2, /*hidden argument*/NULL);
		__this->___sunShaftsMaterial_16 = L_3;
		Shader_t54 * L_4 = (__this->___simpleClearShader_17);
		Material_t55 * L_5 = (__this->___simpleClearMaterial_18);
		Material_t55 * L_6 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_4, L_5, /*hidden argument*/NULL);
		__this->___simpleClearMaterial_18 = L_6;
		bool L_7 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_7)
		{
			goto IL_004e;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_004e:
	{
		bool L_8 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_8;
	}
}
// System.Void UnityStandardAssets.ImageEffects.SunShafts::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t27_m744_MethodInfo_var;
extern "C" void SunShafts_OnRenderImage_m371 (SunShafts_t123 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		Component_GetComponent_TisCamera_t27_m744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t4  V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RenderTexture_t101 * V_4 = {0};
	RenderTexture_t101 * V_5 = {0};
	int32_t V_6 = {0};
	RenderTexture_t101 * V_7 = {0};
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	int32_t G_B15_0 = 0;
	Material_t55 * G_B25_0 = {0};
	RenderTexture_t101 * G_B25_1 = {0};
	RenderTexture_t101 * G_B25_2 = {0};
	Material_t55 * G_B24_0 = {0};
	RenderTexture_t101 * G_B24_1 = {0};
	RenderTexture_t101 * G_B24_2 = {0};
	int32_t G_B26_0 = 0;
	Material_t55 * G_B26_1 = {0};
	RenderTexture_t101 * G_B26_2 = {0};
	RenderTexture_t101 * G_B26_3 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.SunShafts::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		bool L_3 = (__this->___useDepthTexture_14);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		Camera_t27 * L_4 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		Camera_t27 * L_5 = L_4;
		NullCheck(L_5);
		int32_t L_6 = Camera_get_depthTextureMode_m779(L_5, /*hidden argument*/NULL);
		NullCheck(L_5);
		Camera_set_depthTextureMode_m780(L_5, ((int32_t)((int32_t)L_6|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0031:
	{
		V_0 = 4;
		int32_t L_7 = (__this->___resolution_5);
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_0046;
		}
	}
	{
		V_0 = 2;
		goto IL_0054;
	}

IL_0046:
	{
		int32_t L_8 = (__this->___resolution_5);
		if ((!(((uint32_t)L_8) == ((uint32_t)2))))
		{
			goto IL_0054;
		}
	}
	{
		V_0 = 1;
	}

IL_0054:
	{
		Vector3_t4  L_9 = Vector3_get_one_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_10 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_9, (0.5f), /*hidden argument*/NULL);
		V_1 = L_10;
		Transform_t1 * L_11 = (__this->___sunTransform_7);
		bool L_12 = Object_op_Implicit_m629(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0090;
		}
	}
	{
		Camera_t27 * L_13 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		Transform_t1 * L_14 = (__this->___sunTransform_7);
		NullCheck(L_14);
		Vector3_t4  L_15 = Transform_get_position_m593(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t4  L_16 = Camera_WorldToViewportPoint_m824(L_13, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		goto IL_00a6;
	}

IL_0090:
	{
		Vector3__ctor_m612((&V_1), (0.5f), (0.5f), (0.0f), /*hidden argument*/NULL);
	}

IL_00a6:
	{
		RenderTexture_t101 * L_17 = ___source;
		NullCheck(L_17);
		int32_t L_18 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_17);
		int32_t L_19 = V_0;
		V_2 = ((int32_t)((int32_t)L_18/(int32_t)L_19));
		RenderTexture_t101 * L_20 = ___source;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_20);
		int32_t L_22 = V_0;
		V_3 = ((int32_t)((int32_t)L_21/(int32_t)L_22));
		int32_t L_23 = V_2;
		int32_t L_24 = V_3;
		RenderTexture_t101 * L_25 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, L_23, L_24, 0, /*hidden argument*/NULL);
		V_5 = L_25;
		Material_t55 * L_26 = (__this->___sunShaftsMaterial_16);
		Vector4_t236  L_27 = {0};
		Vector4__ctor_m751(&L_27, (1.0f), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		float L_28 = (__this->___sunShaftBlurRadius_11);
		Vector4_t236  L_29 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		Material_SetVector_m752(L_26, (String_t*) &_stringLiteral153, L_29, /*hidden argument*/NULL);
		Material_t55 * L_30 = (__this->___sunShaftsMaterial_16);
		float L_31 = ((&V_1)->___x_1);
		float L_32 = ((&V_1)->___y_2);
		float L_33 = ((&V_1)->___z_3);
		float L_34 = (__this->___maxRadius_13);
		Vector4_t236  L_35 = {0};
		Vector4__ctor_m751(&L_35, L_31, L_32, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_30);
		Material_SetVector_m752(L_30, (String_t*) &_stringLiteral154, L_35, /*hidden argument*/NULL);
		Material_t55 * L_36 = (__this->___sunShaftsMaterial_16);
		Color_t65  L_37 = (__this->___sunThreshold_10);
		Vector4_t236  L_38 = Color_op_Implicit_m760(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		Material_SetVector_m752(L_36, (String_t*) &_stringLiteral155, L_38, /*hidden argument*/NULL);
		bool L_39 = (__this->___useDepthTexture_14);
		if (L_39)
		{
			goto IL_01bc;
		}
	}
	{
		Camera_t27 * L_40 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		NullCheck(L_40);
		bool L_41 = Camera_get_hdr_m748(L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0163;
		}
	}
	{
		G_B15_0 = ((int32_t)9);
		goto IL_0164;
	}

IL_0163:
	{
		G_B15_0 = 7;
	}

IL_0164:
	{
		V_6 = G_B15_0;
		RenderTexture_t101 * L_42 = ___source;
		NullCheck(L_42);
		int32_t L_43 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_42);
		RenderTexture_t101 * L_44 = ___source;
		NullCheck(L_44);
		int32_t L_45 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_44);
		int32_t L_46 = V_6;
		RenderTexture_t101 * L_47 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_43, L_45, 0, L_46, /*hidden argument*/NULL);
		V_7 = L_47;
		RenderTexture_t101 * L_48 = V_7;
		RenderTexture_set_active_m835(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		Camera_t27 * L_49 = Component_GetComponent_TisCamera_t27_m744(__this, /*hidden argument*/Component_GetComponent_TisCamera_t27_m744_MethodInfo_var);
		GL_ClearWithSkybox_m876(NULL /*static, unused*/, 0, L_49, /*hidden argument*/NULL);
		Material_t55 * L_50 = (__this->___sunShaftsMaterial_16);
		RenderTexture_t101 * L_51 = V_7;
		NullCheck(L_50);
		Material_SetTexture_m759(L_50, (String_t*) &_stringLiteral156, L_51, /*hidden argument*/NULL);
		RenderTexture_t101 * L_52 = ___source;
		RenderTexture_t101 * L_53 = V_5;
		Material_t55 * L_54 = (__this->___sunShaftsMaterial_16);
		Graphics_Blit_m742(NULL /*static, unused*/, L_52, L_53, L_54, 3, /*hidden argument*/NULL);
		RenderTexture_t101 * L_55 = V_7;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		goto IL_01cb;
	}

IL_01bc:
	{
		RenderTexture_t101 * L_56 = ___source;
		RenderTexture_t101 * L_57 = V_5;
		Material_t55 * L_58 = (__this->___sunShaftsMaterial_16);
		Graphics_Blit_m742(NULL /*static, unused*/, L_56, L_57, L_58, 2, /*hidden argument*/NULL);
	}

IL_01cb:
	{
		RenderTexture_t101 * L_59 = V_5;
		Material_t55 * L_60 = (__this->___simpleClearMaterial_18);
		PostEffectsBase_DrawBorder_m340(__this, L_59, L_60, /*hidden argument*/NULL);
		int32_t L_61 = (__this->___radialBlurIterations_8);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_62 = Mathf_Clamp_m712(NULL /*static, unused*/, L_61, 1, 4, /*hidden argument*/NULL);
		__this->___radialBlurIterations_8 = L_62;
		float L_63 = (__this->___sunShaftBlurRadius_11);
		V_8 = ((float)((float)L_63*(float)(0.00130208337f)));
		Material_t55 * L_64 = (__this->___sunShaftsMaterial_16);
		float L_65 = V_8;
		float L_66 = V_8;
		Vector4_t236  L_67 = {0};
		Vector4__ctor_m751(&L_67, L_65, L_66, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_64);
		Material_SetVector_m752(L_64, (String_t*) &_stringLiteral153, L_67, /*hidden argument*/NULL);
		Material_t55 * L_68 = (__this->___sunShaftsMaterial_16);
		float L_69 = ((&V_1)->___x_1);
		float L_70 = ((&V_1)->___y_2);
		float L_71 = ((&V_1)->___z_3);
		float L_72 = (__this->___maxRadius_13);
		Vector4_t236  L_73 = {0};
		Vector4__ctor_m751(&L_73, L_69, L_70, L_71, L_72, /*hidden argument*/NULL);
		NullCheck(L_68);
		Material_SetVector_m752(L_68, (String_t*) &_stringLiteral154, L_73, /*hidden argument*/NULL);
		V_9 = 0;
		goto IL_032b;
	}

IL_0255:
	{
		int32_t L_74 = V_2;
		int32_t L_75 = V_3;
		RenderTexture_t101 * L_76 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, L_74, L_75, 0, /*hidden argument*/NULL);
		V_4 = L_76;
		RenderTexture_t101 * L_77 = V_5;
		RenderTexture_t101 * L_78 = V_4;
		Material_t55 * L_79 = (__this->___sunShaftsMaterial_16);
		Graphics_Blit_m742(NULL /*static, unused*/, L_77, L_78, L_79, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_80 = V_5;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_80, /*hidden argument*/NULL);
		float L_81 = (__this->___sunShaftBlurRadius_11);
		int32_t L_82 = V_9;
		V_8 = ((float)((float)((float)((float)L_81*(float)((float)((float)((float)((float)((float)((float)(((float)L_82))*(float)(2.0f)))+(float)(1.0f)))*(float)(6.0f)))))/(float)(768.0f)));
		Material_t55 * L_83 = (__this->___sunShaftsMaterial_16);
		float L_84 = V_8;
		float L_85 = V_8;
		Vector4_t236  L_86 = {0};
		Vector4__ctor_m751(&L_86, L_84, L_85, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_83);
		Material_SetVector_m752(L_83, (String_t*) &_stringLiteral153, L_86, /*hidden argument*/NULL);
		int32_t L_87 = V_2;
		int32_t L_88 = V_3;
		RenderTexture_t101 * L_89 = RenderTexture_GetTemporary_m769(NULL /*static, unused*/, L_87, L_88, 0, /*hidden argument*/NULL);
		V_5 = L_89;
		RenderTexture_t101 * L_90 = V_4;
		RenderTexture_t101 * L_91 = V_5;
		Material_t55 * L_92 = (__this->___sunShaftsMaterial_16);
		Graphics_Blit_m742(NULL /*static, unused*/, L_90, L_91, L_92, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_93 = V_4;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		float L_94 = (__this->___sunShaftBlurRadius_11);
		int32_t L_95 = V_9;
		V_8 = ((float)((float)((float)((float)L_94*(float)((float)((float)((float)((float)((float)((float)(((float)L_95))*(float)(2.0f)))+(float)(2.0f)))*(float)(6.0f)))))/(float)(768.0f)));
		Material_t55 * L_96 = (__this->___sunShaftsMaterial_16);
		float L_97 = V_8;
		float L_98 = V_8;
		Vector4_t236  L_99 = {0};
		Vector4__ctor_m751(&L_99, L_97, L_98, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_96);
		Material_SetVector_m752(L_96, (String_t*) &_stringLiteral153, L_99, /*hidden argument*/NULL);
		int32_t L_100 = V_9;
		V_9 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_032b:
	{
		int32_t L_101 = V_9;
		int32_t L_102 = (__this->___radialBlurIterations_8);
		if ((((int32_t)L_101) < ((int32_t)L_102)))
		{
			goto IL_0255;
		}
	}
	{
		float L_103 = ((&V_1)->___z_3);
		if ((!(((float)L_103) >= ((float)(0.0f)))))
		{
			goto IL_039a;
		}
	}
	{
		Material_t55 * L_104 = (__this->___sunShaftsMaterial_16);
		Color_t65 * L_105 = &(__this->___sunColor_9);
		float L_106 = (L_105->___r_0);
		Color_t65 * L_107 = &(__this->___sunColor_9);
		float L_108 = (L_107->___g_1);
		Color_t65 * L_109 = &(__this->___sunColor_9);
		float L_110 = (L_109->___b_2);
		Color_t65 * L_111 = &(__this->___sunColor_9);
		float L_112 = (L_111->___a_3);
		Vector4_t236  L_113 = {0};
		Vector4__ctor_m751(&L_113, L_106, L_108, L_110, L_112, /*hidden argument*/NULL);
		float L_114 = (__this->___sunShaftIntensity_12);
		Vector4_t236  L_115 = Vector4_op_Multiply_m757(NULL /*static, unused*/, L_113, L_114, /*hidden argument*/NULL);
		NullCheck(L_104);
		Material_SetVector_m752(L_104, (String_t*) &_stringLiteral157, L_115, /*hidden argument*/NULL);
		goto IL_03af;
	}

IL_039a:
	{
		Material_t55 * L_116 = (__this->___sunShaftsMaterial_16);
		Vector4_t236  L_117 = Vector4_get_zero_m790(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_116);
		Material_SetVector_m752(L_116, (String_t*) &_stringLiteral157, L_117, /*hidden argument*/NULL);
	}

IL_03af:
	{
		Material_t55 * L_118 = (__this->___sunShaftsMaterial_16);
		RenderTexture_t101 * L_119 = V_5;
		NullCheck(L_118);
		Material_SetTexture_m759(L_118, (String_t*) &_stringLiteral29, L_119, /*hidden argument*/NULL);
		RenderTexture_t101 * L_120 = ___source;
		RenderTexture_t101 * L_121 = ___destination;
		Material_t55 * L_122 = (__this->___sunShaftsMaterial_16);
		int32_t L_123 = (__this->___screenBlendMode_6);
		G_B24_0 = L_122;
		G_B24_1 = L_121;
		G_B24_2 = L_120;
		if (L_123)
		{
			G_B25_0 = L_122;
			G_B25_1 = L_121;
			G_B25_2 = L_120;
			goto IL_03da;
		}
	}
	{
		G_B26_0 = 0;
		G_B26_1 = G_B24_0;
		G_B26_2 = G_B24_1;
		G_B26_3 = G_B24_2;
		goto IL_03db;
	}

IL_03da:
	{
		G_B26_0 = 4;
		G_B26_1 = G_B25_0;
		G_B26_2 = G_B25_1;
		G_B26_3 = G_B25_2;
	}

IL_03db:
	{
		Graphics_Blit_m742(NULL /*static, unused*/, G_B26_3, G_B26_2, G_B26_1, G_B26_0, /*hidden argument*/NULL);
		RenderTexture_t101 * L_124 = V_5;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_124, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_57.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftMode
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_57MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftQuality
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_58.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.TiltShift/TiltShiftQuality
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_58MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.TiltShift
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_59.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.TiltShift
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_59MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.TiltShift::.ctor()
extern "C" void TiltShift__ctor_m372 (TiltShift_t126 * __this, const MethodInfo* method)
{
	{
		__this->___quality_6 = 1;
		__this->___blurArea_7 = (1.0f);
		__this->___maxBlurSize_8 = (5.0f);
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.TiltShift::CheckResources()
extern "C" bool TiltShift_CheckResources_m373 (TiltShift_t126 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m334(__this, 1, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___tiltShiftShader_10);
		Material_t55 * L_1 = (__this->___tiltShiftMaterial_11);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___tiltShiftMaterial_11 = L_2;
		bool L_3 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		bool L_4 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_4;
	}
}
// System.Void UnityStandardAssets.ImageEffects.TiltShift::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void TiltShift_OnRenderImage_m374 (TiltShift_t126 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	RenderTexture_t101 * V_0 = {0};
	int32_t V_1 = 0;
	String_t* G_B4_0 = {0};
	Material_t55 * G_B4_1 = {0};
	String_t* G_B3_0 = {0};
	Material_t55 * G_B3_1 = {0};
	float G_B5_0 = 0.0f;
	String_t* G_B5_1 = {0};
	Material_t55 * G_B5_2 = {0};
	Material_t55 * G_B9_0 = {0};
	RenderTexture_t101 * G_B9_1 = {0};
	RenderTexture_t101 * G_B9_2 = {0};
	Material_t55 * G_B8_0 = {0};
	RenderTexture_t101 * G_B8_1 = {0};
	RenderTexture_t101 * G_B8_2 = {0};
	int32_t G_B10_0 = 0;
	Material_t55 * G_B10_1 = {0};
	RenderTexture_t101 * G_B10_2 = {0};
	RenderTexture_t101 * G_B10_3 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.TiltShift::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		Material_t55 * L_3 = (__this->___tiltShiftMaterial_11);
		float L_4 = (__this->___maxBlurSize_8);
		G_B3_0 = (String_t*) &_stringLiteral158;
		G_B3_1 = L_3;
		if ((!(((float)L_4) < ((float)(0.0f)))))
		{
			G_B4_0 = (String_t*) &_stringLiteral158;
			G_B4_1 = L_3;
			goto IL_0038;
		}
	}
	{
		G_B5_0 = (0.0f);
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		goto IL_003e;
	}

IL_0038:
	{
		float L_5 = (__this->___maxBlurSize_8);
		G_B5_0 = L_5;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
	}

IL_003e:
	{
		NullCheck(G_B5_2);
		Material_SetFloat_m738(G_B5_2, G_B5_1, G_B5_0, /*hidden argument*/NULL);
		Material_t55 * L_6 = (__this->___tiltShiftMaterial_11);
		float L_7 = (__this->___blurArea_7);
		NullCheck(L_6);
		Material_SetFloat_m738(L_6, (String_t*) &_stringLiteral159, L_7, /*hidden argument*/NULL);
		RenderTexture_t101 * L_8 = ___source;
		NullCheck(L_8);
		Texture_set_filterMode_m762(L_8, 1, /*hidden argument*/NULL);
		RenderTexture_t101 * L_9 = ___destination;
		V_0 = L_9;
		int32_t L_10 = (__this->___downsample_9);
		if ((!(((float)(((float)L_10))) > ((float)(0.0f)))))
		{
			goto IL_00a7;
		}
	}
	{
		RenderTexture_t101 * L_11 = ___source;
		NullCheck(L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_11);
		int32_t L_13 = (__this->___downsample_9);
		RenderTexture_t101 * L_14 = ___source;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.RenderTexture::get_height() */, L_14);
		int32_t L_16 = (__this->___downsample_9);
		RenderTexture_t101 * L_17 = ___source;
		NullCheck(L_17);
		int32_t L_18 = RenderTexture_get_format_m747(L_17, /*hidden argument*/NULL);
		RenderTexture_t101 * L_19 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_12>>(int32_t)((int32_t)((int32_t)L_13&(int32_t)((int32_t)31))))), ((int32_t)((int32_t)L_15>>(int32_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)31))))), 0, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		RenderTexture_t101 * L_20 = V_0;
		NullCheck(L_20);
		Texture_set_filterMode_m762(L_20, 1, /*hidden argument*/NULL);
	}

IL_00a7:
	{
		int32_t L_21 = (__this->___quality_6);
		V_1 = L_21;
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22*(int32_t)2));
		RenderTexture_t101 * L_23 = ___source;
		RenderTexture_t101 * L_24 = V_0;
		Material_t55 * L_25 = (__this->___tiltShiftMaterial_11);
		int32_t L_26 = (__this->___mode_5);
		G_B8_0 = L_25;
		G_B8_1 = L_24;
		G_B8_2 = L_23;
		if (L_26)
		{
			G_B9_0 = L_25;
			G_B9_1 = L_24;
			G_B9_2 = L_23;
			goto IL_00cb;
		}
	}
	{
		int32_t L_27 = V_1;
		G_B10_0 = L_27;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		G_B10_3 = G_B8_2;
		goto IL_00ce;
	}

IL_00cb:
	{
		int32_t L_28 = V_1;
		G_B10_0 = ((int32_t)((int32_t)L_28+(int32_t)1));
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
		G_B10_3 = G_B9_2;
	}

IL_00ce:
	{
		Graphics_Blit_m742(NULL /*static, unused*/, G_B10_3, G_B10_2, G_B10_1, G_B10_0, /*hidden argument*/NULL);
		int32_t L_29 = (__this->___downsample_9);
		if ((((int32_t)L_29) <= ((int32_t)0)))
		{
			goto IL_00fe;
		}
	}
	{
		Material_t55 * L_30 = (__this->___tiltShiftMaterial_11);
		RenderTexture_t101 * L_31 = V_0;
		NullCheck(L_30);
		Material_SetTexture_m759(L_30, (String_t*) &_stringLiteral160, L_31, /*hidden argument*/NULL);
		RenderTexture_t101 * L_32 = ___source;
		RenderTexture_t101 * L_33 = ___destination;
		Material_t55 * L_34 = (__this->___tiltShiftMaterial_11);
		Graphics_Blit_m742(NULL /*static, unused*/, L_32, L_33, L_34, 6, /*hidden argument*/NULL);
	}

IL_00fe:
	{
		RenderTexture_t101 * L_35 = V_0;
		RenderTexture_t101 * L_36 = ___destination;
		bool L_37 = Object_op_Inequality_m623(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0110;
		}
	}
	{
		RenderTexture_t101 * L_38 = V_0;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
	}

IL_0110:
	{
		return;
	}
}
// UnityStandardAssets.ImageEffects.Tonemapping/TonemapperType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_60.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Tonemapping/TonemapperType
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_60MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.Tonemapping/AdaptiveTexSize
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_61.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Tonemapping/AdaptiveTexSize
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_61MethodDeclarations.h"



// UnityStandardAssets.ImageEffects.Tonemapping
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_62.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.ImageEffects.Tonemapping
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_62MethodDeclarations.h"



// System.Void UnityStandardAssets.ImageEffects.Tonemapping::.ctor()
extern "C" void Tonemapping__ctor_m375 (Tonemapping_t129 * __this, const MethodInfo* method)
{
	{
		__this->___type_5 = 3;
		__this->___adaptiveTextureSize_6 = ((int32_t)256);
		__this->___exposureAdjustment_9 = (1.5f);
		__this->___middleGrey_10 = (0.4f);
		__this->___white_11 = (2.0f);
		__this->___adaptionSpeed_12 = (1.5f);
		__this->___validRenderTextureFormat_14 = 1;
		__this->___rtFormat_17 = 2;
		PostEffectsBase__ctor_m327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::CheckResources()
extern TypeInfo* Texture2D_t63_il2cpp_TypeInfo_var;
extern "C" bool Tonemapping_CheckResources_m376 (Tonemapping_t129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		PostEffectsBase_CheckSupport_m335(__this, 0, 1, /*hidden argument*/NULL);
		Shader_t54 * L_0 = (__this->___tonemapper_13);
		Material_t55 * L_1 = (__this->___tonemapMaterial_15);
		Material_t55 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m328(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->___tonemapMaterial_15 = L_2;
		Texture2D_t63 * L_3 = (__this->___curveTex_8);
		bool L_4 = Object_op_Implicit_m629(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_5 = (__this->___type_5);
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0076;
		}
	}
	{
		Texture2D_t63 * L_6 = (Texture2D_t63 *)il2cpp_codegen_object_new (Texture2D_t63_il2cpp_TypeInfo_var);
		Texture2D__ctor_m805(L_6, ((int32_t)256), 1, 5, 0, 1, /*hidden argument*/NULL);
		__this->___curveTex_8 = L_6;
		Texture2D_t63 * L_7 = (__this->___curveTex_8);
		NullCheck(L_7);
		Texture_set_filterMode_m762(L_7, 1, /*hidden argument*/NULL);
		Texture2D_t63 * L_8 = (__this->___curveTex_8);
		NullCheck(L_8);
		Texture_set_wrapMode_m784(L_8, 1, /*hidden argument*/NULL);
		Texture2D_t63 * L_9 = (__this->___curveTex_8);
		NullCheck(L_9);
		Object_set_hideFlags_m764(L_9, ((int32_t)52), /*hidden argument*/NULL);
	}

IL_0076:
	{
		bool L_10 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		if (L_10)
		{
			goto IL_0087;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m337(__this, /*hidden argument*/NULL);
	}

IL_0087:
	{
		bool L_11 = (((PostEffectsBase_t57 *)__this)->___isSupported_4);
		return L_11;
	}
}
// System.Single UnityStandardAssets.ImageEffects.Tonemapping::UpdateCurve()
extern TypeInfo* KeyframeU5BU5D_t239_il2cpp_TypeInfo_var;
extern TypeInfo* AnimationCurve_t82_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" float Tonemapping_UpdateCurve_m377 (Tonemapping_t129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyframeU5BU5D_t239_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		AnimationCurve_t82_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Keyframe_t240  V_3 = {0};
	{
		V_0 = (1.0f);
		AnimationCurve_t82 * L_0 = (__this->___remapCurve_7);
		NullCheck(L_0);
		KeyframeU5BU5D_t239* L_1 = AnimationCurve_get_keys_m877(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))) >= ((int32_t)1)))
		{
			goto IL_0060;
		}
	}
	{
		KeyframeU5BU5D_t239* L_2 = ((KeyframeU5BU5D_t239*)SZArrayNew(KeyframeU5BU5D_t239_il2cpp_TypeInfo_var, 2));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		Keyframe_t240  L_3 = {0};
		Keyframe__ctor_m803(&L_3, (0.0f), (0.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_2, 0)) = L_3;
		KeyframeU5BU5D_t239* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		Keyframe_t240  L_5 = {0};
		Keyframe__ctor_m803(&L_5, (2.0f), (1.0f), /*hidden argument*/NULL);
		*((Keyframe_t240 *)(Keyframe_t240 *)SZArrayLdElema(L_4, 1)) = L_5;
		AnimationCurve_t82 * L_6 = (AnimationCurve_t82 *)il2cpp_codegen_object_new (AnimationCurve_t82_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m804(L_6, L_4, /*hidden argument*/NULL);
		__this->___remapCurve_7 = L_6;
	}

IL_0060:
	{
		AnimationCurve_t82 * L_7 = (__this->___remapCurve_7);
		if (!L_7)
		{
			goto IL_00fc;
		}
	}
	{
		AnimationCurve_t82 * L_8 = (__this->___remapCurve_7);
		NullCheck(L_8);
		int32_t L_9 = AnimationCurve_get_length_m878(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_009d;
		}
	}
	{
		AnimationCurve_t82 * L_10 = (__this->___remapCurve_7);
		AnimationCurve_t82 * L_11 = (__this->___remapCurve_7);
		NullCheck(L_11);
		int32_t L_12 = AnimationCurve_get_length_m878(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Keyframe_t240  L_13 = AnimationCurve_get_Item_m879(L_10, ((int32_t)((int32_t)L_12-(int32_t)1)), /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = Keyframe_get_time_m880((&V_3), /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_009d:
	{
		V_1 = (0.0f);
		goto IL_00e6;
	}

IL_00a8:
	{
		AnimationCurve_t82 * L_15 = (__this->___remapCurve_7);
		float L_16 = V_1;
		float L_17 = V_0;
		NullCheck(L_15);
		float L_18 = AnimationCurve_Evaluate_m806(L_15, ((float)((float)((float)((float)L_16*(float)(1.0f)))*(float)L_17)), /*hidden argument*/NULL);
		V_2 = L_18;
		Texture2D_t63 * L_19 = (__this->___curveTex_8);
		float L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_21 = floorf(((float)((float)L_20*(float)(255.0f))));
		float L_22 = V_2;
		float L_23 = V_2;
		float L_24 = V_2;
		Color_t65  L_25 = {0};
		Color__ctor_m807(&L_25, L_22, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_19);
		Texture2D_SetPixel_m808(L_19, (((int32_t)L_21)), 0, L_25, /*hidden argument*/NULL);
		float L_26 = V_1;
		V_1 = ((float)((float)L_26+(float)(0.003921569f)));
	}

IL_00e6:
	{
		float L_27 = V_1;
		if ((((float)L_27) <= ((float)(1.0f))))
		{
			goto IL_00a8;
		}
	}
	{
		Texture2D_t63 * L_28 = (__this->___curveTex_8);
		NullCheck(L_28);
		Texture2D_Apply_m809(L_28, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		float L_29 = V_0;
		return ((float)((float)(1.0f)/(float)L_29));
	}
}
// System.Void UnityStandardAssets.ImageEffects.Tonemapping::OnDisable()
extern "C" void Tonemapping_OnDisable_m378 (Tonemapping_t129 * __this, const MethodInfo* method)
{
	{
		RenderTexture_t101 * L_0 = (__this->___rt_16);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		RenderTexture_t101 * L_2 = (__this->___rt_16);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->___rt_16 = (RenderTexture_t101 *)NULL;
	}

IL_0022:
	{
		Material_t55 * L_3 = (__this->___tonemapMaterial_15);
		bool L_4 = Object_op_Implicit_m629(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		Material_t55 * L_5 = (__this->___tonemapMaterial_15);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->___tonemapMaterial_15 = (Material_t55 *)NULL;
	}

IL_0044:
	{
		Texture2D_t63 * L_6 = (__this->___curveTex_8);
		bool L_7 = Object_op_Implicit_m629(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0066;
		}
	}
	{
		Texture2D_t63 * L_8 = (__this->___curveTex_8);
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->___curveTex_8 = (Texture2D_t63 *)NULL;
	}

IL_0066:
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::CreateInternalRenderTexture()
extern TypeInfo* RenderTexture_t101_il2cpp_TypeInfo_var;
extern "C" bool Tonemapping_CreateInternalRenderTexture_m379 (Tonemapping_t129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RenderTexture_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		s_Il2CppMethodIntialized = true;
	}
	Tonemapping_t129 * G_B4_0 = {0};
	Tonemapping_t129 * G_B3_0 = {0};
	int32_t G_B5_0 = 0;
	Tonemapping_t129 * G_B5_1 = {0};
	{
		RenderTexture_t101 * L_0 = (__this->___rt_16);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return 0;
	}

IL_0012:
	{
		bool L_2 = SystemInfo_SupportsRenderTextureFormat_m781(NULL /*static, unused*/, ((int32_t)13), /*hidden argument*/NULL);
		G_B3_0 = __this;
		if (!L_2)
		{
			G_B4_0 = __this;
			goto IL_0026;
		}
	}
	{
		G_B5_0 = ((int32_t)13);
		G_B5_1 = G_B3_0;
		goto IL_0027;
	}

IL_0026:
	{
		G_B5_0 = 2;
		G_B5_1 = G_B4_0;
	}

IL_0027:
	{
		NullCheck(G_B5_1);
		G_B5_1->___rtFormat_17 = G_B5_0;
		int32_t L_3 = (__this->___rtFormat_17);
		RenderTexture_t101 * L_4 = (RenderTexture_t101 *)il2cpp_codegen_object_new (RenderTexture_t101_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m881(L_4, 1, 1, 0, L_3, /*hidden argument*/NULL);
		__this->___rt_16 = L_4;
		RenderTexture_t101 * L_5 = (__this->___rt_16);
		NullCheck(L_5);
		Object_set_hideFlags_m764(L_5, ((int32_t)52), /*hidden argument*/NULL);
		return 1;
	}
}
// System.Void UnityStandardAssets.ImageEffects.Tonemapping::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* RenderTextureU5BU5D_t90_il2cpp_TypeInfo_var;
extern "C" void Tonemapping_OnRenderImage_m380 (Tonemapping_t129 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		RenderTextureU5BU5D_t90_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	RenderTexture_t101 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	RenderTextureU5BU5D_t90* V_5 = {0};
	int32_t V_6 = 0;
	RenderTexture_t101 * V_7 = {0};
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Tonemapping_t129 * G_B4_0 = {0};
	Tonemapping_t129 * G_B3_0 = {0};
	float G_B5_0 = 0.0f;
	Tonemapping_t129 * G_B5_1 = {0};
	Tonemapping_t129 * G_B29_0 = {0};
	Tonemapping_t129 * G_B28_0 = {0};
	float G_B30_0 = 0.0f;
	Tonemapping_t129 * G_B30_1 = {0};
	Material_t55 * G_B32_0 = {0};
	RenderTexture_t101 * G_B32_1 = {0};
	RenderTexture_t101 * G_B32_2 = {0};
	Material_t55 * G_B31_0 = {0};
	RenderTexture_t101 * G_B31_1 = {0};
	RenderTexture_t101 * G_B31_2 = {0};
	int32_t G_B33_0 = 0;
	Material_t55 * G_B33_1 = {0};
	RenderTexture_t101 * G_B33_2 = {0};
	RenderTexture_t101 * G_B33_3 = {0};
	Tonemapping_t129 * G_B35_0 = {0};
	Tonemapping_t129 * G_B34_0 = {0};
	float G_B36_0 = 0.0f;
	Tonemapping_t129 * G_B36_1 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t101 * L_1 = ___source;
		RenderTexture_t101 * L_2 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		float L_3 = (__this->___exposureAdjustment_9);
		G_B3_0 = __this;
		if ((!(((float)L_3) < ((float)(0.001f)))))
		{
			G_B4_0 = __this;
			goto IL_002e;
		}
	}
	{
		G_B5_0 = (0.001f);
		G_B5_1 = G_B3_0;
		goto IL_0034;
	}

IL_002e:
	{
		float L_4 = (__this->___exposureAdjustment_9);
		G_B5_0 = L_4;
		G_B5_1 = G_B4_0;
	}

IL_0034:
	{
		NullCheck(G_B5_1);
		G_B5_1->___exposureAdjustment_9 = G_B5_0;
		int32_t L_5 = (__this->___type_5);
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0082;
		}
	}
	{
		float L_6 = Tonemapping_UpdateCurve_m377(__this, /*hidden argument*/NULL);
		V_0 = L_6;
		Material_t55 * L_7 = (__this->___tonemapMaterial_15);
		float L_8 = V_0;
		NullCheck(L_7);
		Material_SetFloat_m738(L_7, (String_t*) &_stringLiteral161, L_8, /*hidden argument*/NULL);
		Material_t55 * L_9 = (__this->___tonemapMaterial_15);
		Texture2D_t63 * L_10 = (__this->___curveTex_8);
		NullCheck(L_9);
		Material_SetTexture_m759(L_9, (String_t*) &_stringLiteral162, L_10, /*hidden argument*/NULL);
		RenderTexture_t101 * L_11 = ___source;
		RenderTexture_t101 * L_12 = ___destination;
		Material_t55 * L_13 = (__this->___tonemapMaterial_15);
		Graphics_Blit_m742(NULL /*static, unused*/, L_11, L_12, L_13, 4, /*hidden argument*/NULL);
		return;
	}

IL_0082:
	{
		int32_t L_14 = (__this->___type_5);
		if (L_14)
		{
			goto IL_00b2;
		}
	}
	{
		Material_t55 * L_15 = (__this->___tonemapMaterial_15);
		float L_16 = (__this->___exposureAdjustment_9);
		NullCheck(L_15);
		Material_SetFloat_m738(L_15, (String_t*) &_stringLiteral163, L_16, /*hidden argument*/NULL);
		RenderTexture_t101 * L_17 = ___source;
		RenderTexture_t101 * L_18 = ___destination;
		Material_t55 * L_19 = (__this->___tonemapMaterial_15);
		Graphics_Blit_m742(NULL /*static, unused*/, L_17, L_18, L_19, 6, /*hidden argument*/NULL);
		return;
	}

IL_00b2:
	{
		int32_t L_20 = (__this->___type_5);
		if ((!(((uint32_t)L_20) == ((uint32_t)2))))
		{
			goto IL_00e3;
		}
	}
	{
		Material_t55 * L_21 = (__this->___tonemapMaterial_15);
		float L_22 = (__this->___exposureAdjustment_9);
		NullCheck(L_21);
		Material_SetFloat_m738(L_21, (String_t*) &_stringLiteral163, L_22, /*hidden argument*/NULL);
		RenderTexture_t101 * L_23 = ___source;
		RenderTexture_t101 * L_24 = ___destination;
		Material_t55 * L_25 = (__this->___tonemapMaterial_15);
		Graphics_Blit_m742(NULL /*static, unused*/, L_23, L_24, L_25, 5, /*hidden argument*/NULL);
		return;
	}

IL_00e3:
	{
		int32_t L_26 = (__this->___type_5);
		if ((!(((uint32_t)L_26) == ((uint32_t)3))))
		{
			goto IL_0114;
		}
	}
	{
		Material_t55 * L_27 = (__this->___tonemapMaterial_15);
		float L_28 = (__this->___exposureAdjustment_9);
		NullCheck(L_27);
		Material_SetFloat_m738(L_27, (String_t*) &_stringLiteral163, L_28, /*hidden argument*/NULL);
		RenderTexture_t101 * L_29 = ___source;
		RenderTexture_t101 * L_30 = ___destination;
		Material_t55 * L_31 = (__this->___tonemapMaterial_15);
		Graphics_Blit_m742(NULL /*static, unused*/, L_29, L_30, L_31, 8, /*hidden argument*/NULL);
		return;
	}

IL_0114:
	{
		int32_t L_32 = (__this->___type_5);
		if ((!(((uint32_t)L_32) == ((uint32_t)4))))
		{
			goto IL_014b;
		}
	}
	{
		Material_t55 * L_33 = (__this->___tonemapMaterial_15);
		float L_34 = (__this->___exposureAdjustment_9);
		NullCheck(L_33);
		Material_SetFloat_m738(L_33, (String_t*) &_stringLiteral163, ((float)((float)(0.5f)*(float)L_34)), /*hidden argument*/NULL);
		RenderTexture_t101 * L_35 = ___source;
		RenderTexture_t101 * L_36 = ___destination;
		Material_t55 * L_37 = (__this->___tonemapMaterial_15);
		Graphics_Blit_m742(NULL /*static, unused*/, L_35, L_36, L_37, 7, /*hidden argument*/NULL);
		return;
	}

IL_014b:
	{
		bool L_38 = Tonemapping_CreateInternalRenderTexture_m379(__this, /*hidden argument*/NULL);
		V_1 = L_38;
		int32_t L_39 = (__this->___adaptiveTextureSize_6);
		int32_t L_40 = (__this->___adaptiveTextureSize_6);
		int32_t L_41 = (__this->___rtFormat_17);
		RenderTexture_t101 * L_42 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, L_39, L_40, 0, L_41, /*hidden argument*/NULL);
		V_2 = L_42;
		RenderTexture_t101 * L_43 = ___source;
		RenderTexture_t101 * L_44 = V_2;
		Graphics_Blit_m737(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		RenderTexture_t101 * L_45 = V_2;
		NullCheck(L_45);
		int32_t L_46 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_45);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_47 = Mathf_Log_m882(NULL /*static, unused*/, ((float)((float)(((float)L_46))*(float)(1.0f))), (2.0f), /*hidden argument*/NULL);
		V_3 = (((int32_t)L_47));
		V_4 = 2;
		int32_t L_48 = V_3;
		V_5 = ((RenderTextureU5BU5D_t90*)SZArrayNew(RenderTextureU5BU5D_t90_il2cpp_TypeInfo_var, L_48));
		V_6 = 0;
		goto IL_01cd;
	}

IL_019e:
	{
		RenderTextureU5BU5D_t90* L_49 = V_5;
		int32_t L_50 = V_6;
		RenderTexture_t101 * L_51 = V_2;
		NullCheck(L_51);
		int32_t L_52 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_51);
		int32_t L_53 = V_4;
		RenderTexture_t101 * L_54 = V_2;
		NullCheck(L_54);
		int32_t L_55 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.RenderTexture::get_width() */, L_54);
		int32_t L_56 = V_4;
		int32_t L_57 = (__this->___rtFormat_17);
		RenderTexture_t101 * L_58 = RenderTexture_GetTemporary_m749(NULL /*static, unused*/, ((int32_t)((int32_t)L_52/(int32_t)L_53)), ((int32_t)((int32_t)L_55/(int32_t)L_56)), 0, L_57, /*hidden argument*/NULL);
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		ArrayElementTypeCheck (L_49, L_58);
		*((RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_49, L_50)) = (RenderTexture_t101 *)L_58;
		int32_t L_59 = V_4;
		V_4 = ((int32_t)((int32_t)L_59*(int32_t)2));
		int32_t L_60 = V_6;
		V_6 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_01cd:
	{
		int32_t L_61 = V_6;
		int32_t L_62 = V_3;
		if ((((int32_t)L_61) < ((int32_t)L_62)))
		{
			goto IL_019e;
		}
	}
	{
		RenderTextureU5BU5D_t90* L_63 = V_5;
		int32_t L_64 = V_3;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, ((int32_t)((int32_t)L_64-(int32_t)1)));
		int32_t L_65 = ((int32_t)((int32_t)L_64-(int32_t)1));
		V_7 = (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_63, L_65));
		RenderTexture_t101 * L_66 = V_2;
		RenderTextureU5BU5D_t90* L_67 = V_5;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, 0);
		int32_t L_68 = 0;
		Material_t55 * L_69 = (__this->___tonemapMaterial_15);
		Graphics_Blit_m742(NULL /*static, unused*/, L_66, (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_67, L_68)), L_69, 1, /*hidden argument*/NULL);
		int32_t L_70 = (__this->___type_5);
		if ((!(((uint32_t)L_70) == ((uint32_t)6))))
		{
			goto IL_0239;
		}
	}
	{
		V_8 = 0;
		goto IL_022a;
	}

IL_0202:
	{
		RenderTextureU5BU5D_t90* L_71 = V_5;
		int32_t L_72 = V_8;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, L_72);
		int32_t L_73 = L_72;
		RenderTextureU5BU5D_t90* L_74 = V_5;
		int32_t L_75 = V_8;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, ((int32_t)((int32_t)L_75+(int32_t)1)));
		int32_t L_76 = ((int32_t)((int32_t)L_75+(int32_t)1));
		Material_t55 * L_77 = (__this->___tonemapMaterial_15);
		Graphics_Blit_m742(NULL /*static, unused*/, (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_71, L_73)), (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_74, L_76)), L_77, ((int32_t)9), /*hidden argument*/NULL);
		RenderTextureU5BU5D_t90* L_78 = V_5;
		int32_t L_79 = V_8;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, ((int32_t)((int32_t)L_79+(int32_t)1)));
		int32_t L_80 = ((int32_t)((int32_t)L_79+(int32_t)1));
		V_7 = (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_78, L_80));
		int32_t L_81 = V_8;
		V_8 = ((int32_t)((int32_t)L_81+(int32_t)1));
	}

IL_022a:
	{
		int32_t L_82 = V_8;
		int32_t L_83 = V_3;
		if ((((int32_t)L_82) < ((int32_t)((int32_t)((int32_t)L_83-(int32_t)1)))))
		{
			goto IL_0202;
		}
	}
	{
		goto IL_0277;
	}

IL_0239:
	{
		int32_t L_84 = (__this->___type_5);
		if ((!(((uint32_t)L_84) == ((uint32_t)5))))
		{
			goto IL_0277;
		}
	}
	{
		V_9 = 0;
		goto IL_026d;
	}

IL_024d:
	{
		RenderTextureU5BU5D_t90* L_85 = V_5;
		int32_t L_86 = V_9;
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, L_86);
		int32_t L_87 = L_86;
		RenderTextureU5BU5D_t90* L_88 = V_5;
		int32_t L_89 = V_9;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, ((int32_t)((int32_t)L_89+(int32_t)1)));
		int32_t L_90 = ((int32_t)((int32_t)L_89+(int32_t)1));
		Graphics_Blit_m737(NULL /*static, unused*/, (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_85, L_87)), (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_88, L_90)), /*hidden argument*/NULL);
		RenderTextureU5BU5D_t90* L_91 = V_5;
		int32_t L_92 = V_9;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, ((int32_t)((int32_t)L_92+(int32_t)1)));
		int32_t L_93 = ((int32_t)((int32_t)L_92+(int32_t)1));
		V_7 = (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_91, L_93));
		int32_t L_94 = V_9;
		V_9 = ((int32_t)((int32_t)L_94+(int32_t)1));
	}

IL_026d:
	{
		int32_t L_95 = V_9;
		int32_t L_96 = V_3;
		if ((((int32_t)L_95) < ((int32_t)((int32_t)((int32_t)L_96-(int32_t)1)))))
		{
			goto IL_024d;
		}
	}

IL_0277:
	{
		float L_97 = (__this->___adaptionSpeed_12);
		G_B28_0 = __this;
		if ((!(((float)L_97) < ((float)(0.001f)))))
		{
			G_B29_0 = __this;
			goto IL_0292;
		}
	}
	{
		G_B30_0 = (0.001f);
		G_B30_1 = G_B28_0;
		goto IL_0298;
	}

IL_0292:
	{
		float L_98 = (__this->___adaptionSpeed_12);
		G_B30_0 = L_98;
		G_B30_1 = G_B29_0;
	}

IL_0298:
	{
		NullCheck(G_B30_1);
		G_B30_1->___adaptionSpeed_12 = G_B30_0;
		Material_t55 * L_99 = (__this->___tonemapMaterial_15);
		float L_100 = (__this->___adaptionSpeed_12);
		NullCheck(L_99);
		Material_SetFloat_m738(L_99, (String_t*) &_stringLiteral164, L_100, /*hidden argument*/NULL);
		RenderTexture_t101 * L_101 = (__this->___rt_16);
		NullCheck(L_101);
		RenderTexture_MarkRestoreExpected_m756(L_101, /*hidden argument*/NULL);
		RenderTexture_t101 * L_102 = V_7;
		RenderTexture_t101 * L_103 = (__this->___rt_16);
		Material_t55 * L_104 = (__this->___tonemapMaterial_15);
		bool L_105 = V_1;
		G_B31_0 = L_104;
		G_B31_1 = L_103;
		G_B31_2 = L_102;
		if (!L_105)
		{
			G_B32_0 = L_104;
			G_B32_1 = L_103;
			G_B32_2 = L_102;
			goto IL_02d8;
		}
	}
	{
		G_B33_0 = 3;
		G_B33_1 = G_B31_0;
		G_B33_2 = G_B31_1;
		G_B33_3 = G_B31_2;
		goto IL_02d9;
	}

IL_02d8:
	{
		G_B33_0 = 2;
		G_B33_1 = G_B32_0;
		G_B33_2 = G_B32_1;
		G_B33_3 = G_B32_2;
	}

IL_02d9:
	{
		Graphics_Blit_m742(NULL /*static, unused*/, G_B33_3, G_B33_2, G_B33_1, G_B33_0, /*hidden argument*/NULL);
		float L_106 = (__this->___middleGrey_10);
		G_B34_0 = __this;
		if ((!(((float)L_106) < ((float)(0.001f)))))
		{
			G_B35_0 = __this;
			goto IL_02f9;
		}
	}
	{
		G_B36_0 = (0.001f);
		G_B36_1 = G_B34_0;
		goto IL_02ff;
	}

IL_02f9:
	{
		float L_107 = (__this->___middleGrey_10);
		G_B36_0 = L_107;
		G_B36_1 = G_B35_0;
	}

IL_02ff:
	{
		NullCheck(G_B36_1);
		G_B36_1->___middleGrey_10 = G_B36_0;
		Material_t55 * L_108 = (__this->___tonemapMaterial_15);
		float L_109 = (__this->___middleGrey_10);
		float L_110 = (__this->___middleGrey_10);
		float L_111 = (__this->___middleGrey_10);
		float L_112 = (__this->___white_11);
		float L_113 = (__this->___white_11);
		Vector4_t236  L_114 = {0};
		Vector4__ctor_m751(&L_114, L_109, L_110, L_111, ((float)((float)L_112*(float)L_113)), /*hidden argument*/NULL);
		NullCheck(L_108);
		Material_SetVector_m752(L_108, (String_t*) &_stringLiteral165, L_114, /*hidden argument*/NULL);
		Material_t55 * L_115 = (__this->___tonemapMaterial_15);
		RenderTexture_t101 * L_116 = (__this->___rt_16);
		NullCheck(L_115);
		Material_SetTexture_m759(L_115, (String_t*) &_stringLiteral166, L_116, /*hidden argument*/NULL);
		int32_t L_117 = (__this->___type_5);
		if ((!(((uint32_t)L_117) == ((uint32_t)5))))
		{
			goto IL_036d;
		}
	}
	{
		RenderTexture_t101 * L_118 = ___source;
		RenderTexture_t101 * L_119 = ___destination;
		Material_t55 * L_120 = (__this->___tonemapMaterial_15);
		Graphics_Blit_m742(NULL /*static, unused*/, L_118, L_119, L_120, 0, /*hidden argument*/NULL);
		goto IL_039e;
	}

IL_036d:
	{
		int32_t L_121 = (__this->___type_5);
		if ((!(((uint32_t)L_121) == ((uint32_t)6))))
		{
			goto IL_038d;
		}
	}
	{
		RenderTexture_t101 * L_122 = ___source;
		RenderTexture_t101 * L_123 = ___destination;
		Material_t55 * L_124 = (__this->___tonemapMaterial_15);
		Graphics_Blit_m742(NULL /*static, unused*/, L_122, L_123, L_124, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_039e;
	}

IL_038d:
	{
		Debug_LogError_m735(NULL /*static, unused*/, (String_t*) &_stringLiteral167, /*hidden argument*/NULL);
		RenderTexture_t101 * L_125 = ___source;
		RenderTexture_t101 * L_126 = ___destination;
		Graphics_Blit_m737(NULL /*static, unused*/, L_125, L_126, /*hidden argument*/NULL);
	}

IL_039e:
	{
		V_10 = 0;
		goto IL_03b6;
	}

IL_03a6:
	{
		RenderTextureU5BU5D_t90* L_127 = V_5;
		int32_t L_128 = V_10;
		NullCheck(L_127);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_127, L_128);
		int32_t L_129 = L_128;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, (*(RenderTexture_t101 **)(RenderTexture_t101 **)SZArrayLdElema(L_127, L_129)), /*hidden argument*/NULL);
		int32_t L_130 = V_10;
		V_10 = ((int32_t)((int32_t)L_130+(int32_t)1));
	}

IL_03b6:
	{
		int32_t L_131 = V_10;
		int32_t L_132 = V_3;
		if ((((int32_t)L_131) < ((int32_t)L_132)))
		{
			goto IL_03a6;
		}
	}
	{
		RenderTexture_t101 * L_133 = V_2;
		RenderTexture_ReleaseTemporary_m743(NULL /*static, unused*/, L_133, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
