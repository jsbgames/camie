﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityStandardAssets.CrossPlatformInput.AxisTouchButton>
struct InternalEnumerator_1_t2808;
// System.Object
struct Object_t;
// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct AxisTouchButton_t29;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityStandardAssets.CrossPlatformInput.AxisTouchButton>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m13542(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2808 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityStandardAssets.CrossPlatformInput.AxisTouchButton>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13543(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2808 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityStandardAssets.CrossPlatformInput.AxisTouchButton>::Dispose()
#define InternalEnumerator_1_Dispose_m13544(__this, method) (( void (*) (InternalEnumerator_1_t2808 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityStandardAssets.CrossPlatformInput.AxisTouchButton>::MoveNext()
#define InternalEnumerator_1_MoveNext_m13545(__this, method) (( bool (*) (InternalEnumerator_1_t2808 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityStandardAssets.CrossPlatformInput.AxisTouchButton>::get_Current()
#define InternalEnumerator_1_get_Current_m13546(__this, method) (( AxisTouchButton_t29 * (*) (InternalEnumerator_1_t2808 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
