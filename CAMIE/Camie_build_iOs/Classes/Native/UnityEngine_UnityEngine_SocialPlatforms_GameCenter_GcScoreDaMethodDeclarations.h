﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct GcScoreData_t959;
struct GcScoreData_t959_marshaled;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t979;

// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern "C" Score_t979 * GcScoreData_ToScore_m4888 (GcScoreData_t959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void GcScoreData_t959_marshal(const GcScoreData_t959& unmarshaled, GcScoreData_t959_marshaled& marshaled);
void GcScoreData_t959_marshal_back(const GcScoreData_t959_marshaled& marshaled, GcScoreData_t959& unmarshaled);
void GcScoreData_t959_marshal_cleanup(GcScoreData_t959_marshaled& marshaled);
