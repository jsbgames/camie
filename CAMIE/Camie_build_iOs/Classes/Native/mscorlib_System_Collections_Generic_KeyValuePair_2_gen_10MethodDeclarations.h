﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>
struct KeyValuePair_2_t2927;
// System.String
struct String_t;
// Reporter/Log
struct Log_t291;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m15152(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2927 *, String_t*, Log_t291 *, const MethodInfo*))KeyValuePair_2__ctor_m13651_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::get_Key()
#define KeyValuePair_2_get_Key_m15153(__this, method) (( String_t* (*) (KeyValuePair_2_t2927 *, const MethodInfo*))KeyValuePair_2_get_Key_m13652_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15154(__this, ___value, method) (( void (*) (KeyValuePair_2_t2927 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m13653_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::get_Value()
#define KeyValuePair_2_get_Value_m15155(__this, method) (( Log_t291 * (*) (KeyValuePair_2_t2927 *, const MethodInfo*))KeyValuePair_2_get_Value_m13654_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15156(__this, ___value, method) (( void (*) (KeyValuePair_2_t2927 *, Log_t291 *, const MethodInfo*))KeyValuePair_2_set_Value_m13655_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::ToString()
#define KeyValuePair_2_ToString_m15157(__this, method) (( String_t* (*) (KeyValuePair_2_t2927 *, const MethodInfo*))KeyValuePair_2_ToString_m13656_gshared)(__this, method)
