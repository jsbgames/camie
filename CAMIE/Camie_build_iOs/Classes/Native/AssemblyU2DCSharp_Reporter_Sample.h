﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// Reporter/Sample
struct  Sample_t290  : public Object_t
{
	// System.Single Reporter/Sample::time
	float ___time_0;
	// System.Byte Reporter/Sample::loadedScene
	uint8_t ___loadedScene_1;
	// System.Single Reporter/Sample::memory
	float ___memory_2;
	// System.Single Reporter/Sample::fps
	float ___fps_3;
	// System.String Reporter/Sample::fpsText
	String_t* ___fpsText_4;
};
