﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.LocalBuilder>
struct InternalEnumerator_1_t3471;
// System.Object
struct Object_t;
// System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t1865;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.LocalBuilder>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22277(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3471 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.LocalBuilder>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22278(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3471 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.LocalBuilder>::Dispose()
#define InternalEnumerator_1_Dispose_m22279(__this, method) (( void (*) (InternalEnumerator_1_t3471 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.LocalBuilder>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22280(__this, method) (( bool (*) (InternalEnumerator_1_t3471 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.LocalBuilder>::get_Current()
#define InternalEnumerator_1_get_Current_m22281(__this, method) (( LocalBuilder_t1865 * (*) (InternalEnumerator_1_t3471 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
