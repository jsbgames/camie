﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Reporter/Sample>
struct List_1_t297;
// Reporter/Sample
struct Sample_t290;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Reporter/Sample>
struct  Enumerator_t2898 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Reporter/Sample>::l
	List_1_t297 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Reporter/Sample>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Reporter/Sample>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Reporter/Sample>::current
	Sample_t290 * ___current_3;
};
