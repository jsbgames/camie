﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct WaypointList_t208;

// System.Void UnityStandardAssets.Utility.WaypointCircuit/WaypointList::.ctor()
extern "C" void WaypointList__ctor_m567 (WaypointList_t208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
