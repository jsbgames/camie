﻿#pragma once
#include <stdint.h>
// UnityEngine.ParticleSystem
struct ParticleSystem_t161;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.ParticleSystem>
struct  Predicate_1_t3225  : public MulticastDelegate_t549
{
};
