﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_CamieDirections
struct SCR_CamieDirections_t348;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t400;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t210;
// UnityEngine.Transform[]
struct TransformU5BU5D_t141;

// System.Void SCR_CamieDirections::.ctor()
extern "C" void SCR_CamieDirections__ctor_m1571 (SCR_CamieDirections_t348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_CamieDirections::Start()
extern "C" void SCR_CamieDirections_Start_m1572 (SCR_CamieDirections_t348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_CamieDirections::Update()
extern "C" void SCR_CamieDirections_Update_m1573 (SCR_CamieDirections_t348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] SCR_CamieDirections::ReorderByName(UnityEngine.Transform[])
extern "C" TransformU5BU5D_t141* SCR_CamieDirections_ReorderByName_m1574 (SCR_CamieDirections_t348 * __this, TransformU5BU5D_t141* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion[] SCR_CamieDirections::get_Angles()
extern "C" QuaternionU5BU5D_t400* SCR_CamieDirections_get_Angles_m1575 (SCR_CamieDirections_t348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] SCR_CamieDirections::get_Normals()
extern "C" Vector3U5BU5D_t210* SCR_CamieDirections_get_Normals_m1576 (SCR_CamieDirections_t348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
