﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Mesh
struct Mesh_t216;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t210;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t237;
// System.Int32[]
struct Int32U5BU5D_t242;

// System.Void UnityEngine.Mesh::.ctor()
extern "C" void Mesh__ctor_m868 (Mesh_t216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C" void Mesh_Internal_Create_m3737 (Object_t * __this /* static, unused */, Mesh_t216 * ___mono, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C" void Mesh_set_vertices_m869 (Mesh_t216 * __this, Vector3U5BU5D_t210* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern "C" void Mesh_set_uv_m871 (Mesh_t216 * __this, Vector2U5BU5D_t237* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_uv2(UnityEngine.Vector2[])
extern "C" void Mesh_set_uv2_m872 (Mesh_t216 * __this, Vector2U5BU5D_t237* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern "C" void Mesh_set_triangles_m870 (Mesh_t216 * __this, Int32U5BU5D_t242* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
