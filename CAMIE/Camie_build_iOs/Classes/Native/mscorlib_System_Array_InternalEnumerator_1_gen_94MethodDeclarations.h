﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.TailoringInfo>
struct InternalEnumerator_1_t3455;
// System.Object
struct Object_t;
// Mono.Globalization.Unicode.TailoringInfo
struct TailoringInfo_t1693;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.TailoringInfo>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22212(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3455 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.TailoringInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22213(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3455 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.TailoringInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m22214(__this, method) (( void (*) (InternalEnumerator_1_t3455 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.TailoringInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22215(__this, method) (( bool (*) (InternalEnumerator_1_t3455 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.TailoringInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m22216(__this, method) (( TailoringInfo_t1693 * (*) (InternalEnumerator_1_t3455 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
