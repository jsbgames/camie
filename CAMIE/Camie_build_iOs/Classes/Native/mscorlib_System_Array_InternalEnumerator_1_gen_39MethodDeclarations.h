﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.Text>
struct InternalEnumerator_1_t2979;
// System.Object
struct Object_t;
// UnityEngine.UI.Text
struct Text_t316;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Text>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15809(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2979 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Text>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15810(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2979 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Text>::Dispose()
#define InternalEnumerator_1_Dispose_m15811(__this, method) (( void (*) (InternalEnumerator_1_t2979 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Text>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15812(__this, method) (( bool (*) (InternalEnumerator_1_t2979 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Text>::get_Current()
#define InternalEnumerator_1_get_Current_m15813(__this, method) (( Text_t316 * (*) (InternalEnumerator_1_t2979 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
