﻿#pragma once
#include <stdint.h>
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem[]
struct DemoParticleSystemU5BU5D_t322;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList
struct  DemoParticleSystemList_t323  : public Object_t
{
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem[] UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList::items
	DemoParticleSystemU5BU5D_t322* ___items_0;
};
