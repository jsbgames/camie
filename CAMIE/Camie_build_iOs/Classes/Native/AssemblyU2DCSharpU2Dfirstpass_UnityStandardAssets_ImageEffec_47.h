﻿#pragma once
#include <stdint.h>
// UnityEngine.Mesh[]
struct MeshU5BU5D_t113;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.ImageEffects.Quads
struct  Quads_t114  : public Object_t
{
};
struct Quads_t114_StaticFields{
	// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Quads::meshes
	MeshU5BU5D_t113* ___meshes_0;
	// System.Int32 UnityStandardAssets.ImageEffects.Quads::currentQuads
	int32_t ___currentQuads_1;
};
