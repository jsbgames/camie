﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.ImageEffects.EdgeDetection
struct EdgeDetection_t104;
// UnityEngine.RenderTexture
struct RenderTexture_t101;

// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::.ctor()
extern "C" void EdgeDetection__ctor_m289 (EdgeDetection_t104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.ImageEffects.EdgeDetection::CheckResources()
extern "C" bool EdgeDetection_CheckResources_m290 (EdgeDetection_t104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::Start()
extern "C" void EdgeDetection_Start_m291 (EdgeDetection_t104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::SetCameraFlag()
extern "C" void EdgeDetection_SetCameraFlag_m292 (EdgeDetection_t104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnEnable()
extern "C" void EdgeDetection_OnEnable_m293 (EdgeDetection_t104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void EdgeDetection_OnRenderImage_m294 (EdgeDetection_t104 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method) IL2CPP_METHOD_ATTR;
