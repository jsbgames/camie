﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>
struct InternalEnumerator_1_t3369;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m21395_gshared (InternalEnumerator_1_t3369 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m21395(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3369 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m21395_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21396_gshared (InternalEnumerator_1_t3369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21396(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3369 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21396_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m21397_gshared (InternalEnumerator_1_t3369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m21397(__this, method) (( void (*) (InternalEnumerator_1_t3369 *, const MethodInfo*))InternalEnumerator_1_Dispose_m21397_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m21398_gshared (InternalEnumerator_1_t3369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m21398(__this, method) (( bool (*) (InternalEnumerator_1_t3369 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m21398_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern "C" ParameterModifier_t1914  InternalEnumerator_1_get_Current_m21399_gshared (InternalEnumerator_1_t3369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m21399(__this, method) (( ParameterModifier_t1914  (*) (InternalEnumerator_1_t3369 *, const MethodInfo*))InternalEnumerator_1_get_Current_m21399_gshared)(__this, method)
