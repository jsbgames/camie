﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t710;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
extern "C" void AssemblyConfigurationAttribute__ctor_m3468 (AssemblyConfigurationAttribute_t710 * __this, String_t* ___configuration, const MethodInfo* method) IL2CPP_METHOD_ATTR;
