﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Threading.ThreadPool
struct ThreadPool_t2169;
// System.Threading.WaitCallback
struct WaitCallback_t2253;
// System.Object
struct Object_t;
// System.Threading.RegisteredWaitHandle
struct RegisteredWaitHandle_t2164;
// System.Threading.WaitHandle
struct WaitHandle_t1637;
// System.Threading.WaitOrTimerCallback
struct WaitOrTimerCallback_t2163;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Boolean System.Threading.ThreadPool::QueueUserWorkItem(System.Threading.WaitCallback,System.Object)
extern "C" bool ThreadPool_QueueUserWorkItem_m11734 (Object_t * __this /* static, unused */, WaitCallback_t2253 * ___callBack, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.RegisteredWaitHandle System.Threading.ThreadPool::RegisterWaitForSingleObject(System.Threading.WaitHandle,System.Threading.WaitOrTimerCallback,System.Object,System.Int64,System.Boolean)
extern "C" RegisteredWaitHandle_t2164 * ThreadPool_RegisterWaitForSingleObject_m11735 (Object_t * __this /* static, unused */, WaitHandle_t1637 * ___waitObject, WaitOrTimerCallback_t2163 * ___callBack, Object_t * ___state, int64_t ___millisecondsTimeOutInterval, bool ___executeOnlyOnce, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.RegisteredWaitHandle System.Threading.ThreadPool::RegisterWaitForSingleObject(System.Threading.WaitHandle,System.Threading.WaitOrTimerCallback,System.Object,System.TimeSpan,System.Boolean)
extern "C" RegisteredWaitHandle_t2164 * ThreadPool_RegisterWaitForSingleObject_m11736 (Object_t * __this /* static, unused */, WaitHandle_t1637 * ___waitObject, WaitOrTimerCallback_t2163 * ___callBack, Object_t * ___state, TimeSpan_t1337  ___timeout, bool ___executeOnlyOnce, const MethodInfo* method) IL2CPP_METHOD_ATTR;
