﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
struct Enumerator_t3101;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t558;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m17650_gshared (Enumerator_t3101 * __this, List_1_t558 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m17650(__this, ___l, method) (( void (*) (Enumerator_t3101 *, List_1_t558 *, const MethodInfo*))Enumerator__ctor_m17650_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17651_gshared (Enumerator_t3101 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17651(__this, method) (( Object_t * (*) (Enumerator_t3101 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17651_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C" void Enumerator_Dispose_m17652_gshared (Enumerator_t3101 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17652(__this, method) (( void (*) (Enumerator_t3101 *, const MethodInfo*))Enumerator_Dispose_m17652_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern "C" void Enumerator_VerifyState_m17653_gshared (Enumerator_t3101 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17653(__this, method) (( void (*) (Enumerator_t3101 *, const MethodInfo*))Enumerator_VerifyState_m17653_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17654_gshared (Enumerator_t3101 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17654(__this, method) (( bool (*) (Enumerator_t3101 *, const MethodInfo*))Enumerator_MoveNext_m17654_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C" UIVertex_t556  Enumerator_get_Current_m17655_gshared (Enumerator_t3101 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17655(__this, method) (( UIVertex_t556  (*) (Enumerator_t3101 *, const MethodInfo*))Enumerator_get_Current_m17655_gshared)(__this, method)
