﻿#pragma once
#include <stdint.h>
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1843;
// System.Reflection.LocalVariableInfo
#include "mscorlib_System_Reflection_LocalVariableInfo.h"
// System.Reflection.Emit.LocalBuilder
struct  LocalBuilder_t1865  : public LocalVariableInfo_t1866
{
	// System.Reflection.Emit.ILGenerator System.Reflection.Emit.LocalBuilder::ilgen
	ILGenerator_t1843 * ___ilgen_3;
};
