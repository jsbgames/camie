﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct Entries_t201;

// System.Void UnityStandardAssets.Utility.TimedObjectActivator/Entries::.ctor()
extern "C" void Entries__ctor_m540 (Entries_t201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
