﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t78;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// MenuSceneLoader
struct  MenuSceneLoader_t311  : public MonoBehaviour_t3
{
	// UnityEngine.GameObject MenuSceneLoader::menuUI
	GameObject_t78 * ___menuUI_2;
	// UnityEngine.GameObject MenuSceneLoader::m_Go
	GameObject_t78 * ___m_Go_3;
};
