﻿#pragma once
#include <stdint.h>
// System.MulticastDelegate
struct MulticastDelegate_t549;
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.MulticastDelegate
struct  MulticastDelegate_t549  : public Delegate_t675
{
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t549 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t549 * ___kpm_next_10;
};
