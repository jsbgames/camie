﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ForcedReset
struct ForcedReset_t184;

// System.Void ForcedReset::.ctor()
extern "C" void ForcedReset__ctor_m496 (ForcedReset_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ForcedReset::Update()
extern "C" void ForcedReset_Update_m497 (ForcedReset_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
