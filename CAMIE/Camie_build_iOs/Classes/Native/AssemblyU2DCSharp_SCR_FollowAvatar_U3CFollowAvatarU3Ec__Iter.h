﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// SCR_FollowAvatar
struct SCR_FollowAvatar_t352;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// SCR_FollowAvatar/<FollowAvatar>c__Iterator8
struct  U3CFollowAvatarU3Ec__Iterator8_t354  : public Object_t
{
	// UnityEngine.Vector3 SCR_FollowAvatar/<FollowAvatar>c__Iterator8::<focus>__0
	Vector3_t4  ___U3CfocusU3E__0_0;
	// UnityEngine.Vector3 SCR_FollowAvatar/<FollowAvatar>c__Iterator8::<newPosition>__1
	Vector3_t4  ___U3CnewPositionU3E__1_1;
	// System.Int32 SCR_FollowAvatar/<FollowAvatar>c__Iterator8::$PC
	int32_t ___U24PC_2;
	// System.Object SCR_FollowAvatar/<FollowAvatar>c__Iterator8::$current
	Object_t * ___U24current_3;
	// SCR_FollowAvatar SCR_FollowAvatar/<FollowAvatar>c__Iterator8::<>f__this
	SCR_FollowAvatar_t352 * ___U3CU3Ef__this_4;
};
