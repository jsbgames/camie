﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SphereCollider
struct SphereCollider_t136;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityEngine.SphereCollider::INTERNAL_get_center(UnityEngine.Vector3&)
extern "C" void SphereCollider_INTERNAL_get_center_m4484 (SphereCollider_t136 * __this, Vector3_t4 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SphereCollider::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C" void SphereCollider_INTERNAL_set_center_m4485 (SphereCollider_t136 * __this, Vector3_t4 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.SphereCollider::get_center()
extern "C" Vector3_t4  SphereCollider_get_center_m884 (SphereCollider_t136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SphereCollider::set_center(UnityEngine.Vector3)
extern "C" void SphereCollider_set_center_m889 (SphereCollider_t136 * __this, Vector3_t4  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SphereCollider::get_radius()
extern "C" float SphereCollider_get_radius_m885 (SphereCollider_t136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SphereCollider::set_radius(System.Single)
extern "C" void SphereCollider_set_radius_m888 (SphereCollider_t136 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
