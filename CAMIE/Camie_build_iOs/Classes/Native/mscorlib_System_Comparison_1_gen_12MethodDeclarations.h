﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<SCR_Menu>
struct Comparison_1_t3008;
// System.Object
struct Object_t;
// SCR_Menu
struct SCR_Menu_t336;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<SCR_Menu>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m16153(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3008 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m14080_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<SCR_Menu>::Invoke(T,T)
#define Comparison_1_Invoke_m16154(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3008 *, SCR_Menu_t336 *, SCR_Menu_t336 *, const MethodInfo*))Comparison_1_Invoke_m14081_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<SCR_Menu>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m16155(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3008 *, SCR_Menu_t336 *, SCR_Menu_t336 *, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m14082_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<SCR_Menu>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m16156(__this, ___result, method) (( int32_t (*) (Comparison_1_t3008 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m14083_gshared)(__this, ___result, method)
