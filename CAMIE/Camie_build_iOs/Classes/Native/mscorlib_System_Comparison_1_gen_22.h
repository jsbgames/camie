﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas
struct Canvas_t414;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Canvas>
struct  Comparison_1_t3114  : public MulticastDelegate_t549
{
};
