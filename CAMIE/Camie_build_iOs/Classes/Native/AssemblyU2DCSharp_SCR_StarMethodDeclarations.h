﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Star
struct SCR_Star_t362;
// UnityEngine.Collider
struct Collider_t138;

// System.Void SCR_Star::.ctor()
extern "C" void SCR_Star__ctor_m1333 (SCR_Star_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Star::Start()
extern "C" void SCR_Star_Start_m1334 (SCR_Star_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Star::OnTriggerEnter(UnityEngine.Collider)
extern "C" void SCR_Star_OnTriggerEnter_m1335 (SCR_Star_t362 * __this, Collider_t138 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Star::Kill()
extern "C" void SCR_Star_Kill_m1336 (SCR_Star_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Star::Collect()
extern "C" void SCR_Star_Collect_m1337 (SCR_Star_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
