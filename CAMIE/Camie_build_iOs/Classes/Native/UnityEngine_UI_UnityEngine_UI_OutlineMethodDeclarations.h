﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Outline
struct Outline_t627;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t558;

// System.Void UnityEngine.UI.Outline::.ctor()
extern "C" void Outline__ctor_m3039 (Outline_t627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void Outline_ModifyVertices_m3040 (Outline_t627 * __this, List_1_t558 * ___verts, const MethodInfo* method) IL2CPP_METHOD_ATTR;
