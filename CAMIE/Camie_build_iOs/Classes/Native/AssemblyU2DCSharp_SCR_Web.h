﻿#pragma once
#include <stdint.h>
// UnityEngine.Animator
struct Animator_t9;
// UnityEngine.Transform
struct Transform_t1;
// SCR_SlowAvatar
#include "AssemblyU2DCSharp_SCR_SlowAvatar.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// SCR_Web
struct  SCR_Web_t398  : public SCR_SlowAvatar_t337
{
	// UnityEngine.Animator SCR_Web::anim
	Animator_t9 * ___anim_3;
	// UnityEngine.Transform SCR_Web::trans
	Transform_t1 * ___trans_4;
	// UnityEngine.Vector3 SCR_Web::reScale
	Vector3_t4  ___reScale_5;
	// System.Single SCR_Web::maxD
	float ___maxD_6;
};
