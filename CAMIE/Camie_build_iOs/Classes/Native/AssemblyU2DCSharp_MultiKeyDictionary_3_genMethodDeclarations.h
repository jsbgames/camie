﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MultiKeyDictionary`3<System.String,System.String,Reporter/Log>
struct MultiKeyDictionary_3_t299;
// System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>
struct Dictionary_2_t407;
// System.String
struct String_t;

// System.Void MultiKeyDictionary`3<System.String,System.String,Reporter/Log>::.ctor()
// MultiKeyDictionary`3<System.Object,System.Object,System.Object>
#include "AssemblyU2DCSharp_MultiKeyDictionary_3_gen_0MethodDeclarations.h"
#define MultiKeyDictionary_3__ctor_m1594(__this, method) (( void (*) (MultiKeyDictionary_3_t299 *, const MethodInfo*))MultiKeyDictionary_3__ctor_m14472_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2<T2,T3> MultiKeyDictionary`3<System.String,System.String,Reporter/Log>::get_Item(T1)
#define MultiKeyDictionary_3_get_Item_m1644(__this, ___key, method) (( Dictionary_2_t407 * (*) (MultiKeyDictionary_3_t299 *, String_t*, const MethodInfo*))MultiKeyDictionary_3_get_Item_m14473_gshared)(__this, ___key, method)
// System.Boolean MultiKeyDictionary`3<System.String,System.String,Reporter/Log>::ContainsKey(T1,T2)
#define MultiKeyDictionary_3_ContainsKey_m1697(__this, ___key1, ___key2, method) (( bool (*) (MultiKeyDictionary_3_t299 *, String_t*, String_t*, const MethodInfo*))MultiKeyDictionary_3_ContainsKey_m14474_gshared)(__this, ___key1, ___key2, method)
