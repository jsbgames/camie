﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Projector
struct Projector_t395;

// System.Single UnityEngine.Projector::get_fieldOfView()
extern "C" float Projector_get_fieldOfView_m1822 (Projector_t395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Projector::set_fieldOfView(System.Single)
extern "C" void Projector_set_fieldOfView_m1821 (Projector_t395 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
