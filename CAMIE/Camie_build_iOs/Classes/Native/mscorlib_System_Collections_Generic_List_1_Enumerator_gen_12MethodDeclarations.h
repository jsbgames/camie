﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<SCR_Puceron>
struct Enumerator_t2975;
// System.Object
struct Object_t;
// SCR_Puceron
struct SCR_Puceron_t356;
// System.Collections.Generic.List`1<SCR_Puceron>
struct List_1_t387;

// System.Void System.Collections.Generic.List`1/Enumerator<SCR_Puceron>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m15789(__this, ___l, method) (( void (*) (Enumerator_t2975 *, List_1_t387 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SCR_Puceron>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15790(__this, method) (( Object_t * (*) (Enumerator_t2975 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SCR_Puceron>::Dispose()
#define Enumerator_Dispose_m15791(__this, method) (( void (*) (Enumerator_t2975 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SCR_Puceron>::VerifyState()
#define Enumerator_VerifyState_m15792(__this, method) (( void (*) (Enumerator_t2975 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SCR_Puceron>::MoveNext()
#define Enumerator_MoveNext_m15793(__this, method) (( bool (*) (Enumerator_t2975 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SCR_Puceron>::get_Current()
#define Enumerator_get_Current_m15794(__this, method) (( SCR_Puceron_t356 * (*) (Enumerator_t2975 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
