﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// EventSystemChecker
struct EventSystemChecker_t309;
// System.Object
#include "mscorlib_System_Object.h"
// EventSystemChecker/<Start>c__Iterator1
struct  U3CStartU3Ec__Iterator1_t310  : public Object_t
{
	// System.Int32 EventSystemChecker/<Start>c__Iterator1::$PC
	int32_t ___U24PC_0;
	// System.Object EventSystemChecker/<Start>c__Iterator1::$current
	Object_t * ___U24current_1;
	// EventSystemChecker EventSystemChecker/<Start>c__Iterator1::<>f__this
	EventSystemChecker_t309 * ___U3CU3Ef__this_2;
};
