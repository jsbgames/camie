﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t243;
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// System.Runtime.Remoting.Messaging.MethodCallDictionary
struct  MethodCallDictionary_t1988  : public MethodDictionary_t1983
{
};
struct MethodCallDictionary_t1988_StaticFields{
	// System.String[] System.Runtime.Remoting.Messaging.MethodCallDictionary::InternalKeys
	StringU5BU5D_t243* ___InternalKeys_6;
};
