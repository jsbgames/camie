﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_ManagerInstantiator
struct SCR_ManagerInstantiator_t385;

// System.Void SCR_ManagerInstantiator::.ctor()
extern "C" void SCR_ManagerInstantiator__ctor_m1455 (SCR_ManagerInstantiator_t385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerInstantiator::Awake()
extern "C" void SCR_ManagerInstantiator_Awake_m1456 (SCR_ManagerInstantiator_t385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
