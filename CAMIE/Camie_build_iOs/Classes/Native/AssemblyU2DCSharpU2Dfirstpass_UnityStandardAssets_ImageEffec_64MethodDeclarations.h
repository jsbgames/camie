﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.ImageEffects.Twirl
struct Twirl_t131;
// UnityEngine.RenderTexture
struct RenderTexture_t101;

// System.Void UnityStandardAssets.ImageEffects.Twirl::.ctor()
extern "C" void Twirl__ctor_m387 (Twirl_t131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.Twirl::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void Twirl_OnRenderImage_m388 (Twirl_t131 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method) IL2CPP_METHOD_ATTR;
