﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>
struct InternalEnumerator_1_t3474;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22292_gshared (InternalEnumerator_1_t3474 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22292(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3474 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22292_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22293_gshared (InternalEnumerator_1_t3474 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22293(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3474 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22293_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22294_gshared (InternalEnumerator_1_t3474 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22294(__this, method) (( void (*) (InternalEnumerator_1_t3474 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22294_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22295_gshared (InternalEnumerator_1_t3474 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22295(__this, method) (( bool (*) (InternalEnumerator_1_t3474 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22295_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern "C" LabelFixup_t1857  InternalEnumerator_1_get_Current_m22296_gshared (InternalEnumerator_1_t3474 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22296(__this, method) (( LabelFixup_t1857  (*) (InternalEnumerator_1_t3474 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22296_gshared)(__this, method)
