﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<SCR_Menu>
struct List_1_t377;
// System.Object
struct Object_t;
// SCR_Menu
struct SCR_Menu_t336;
// System.Collections.Generic.IEnumerable`1<SCR_Menu>
struct IEnumerable_1_t3606;
// System.Collections.Generic.IEnumerator`1<SCR_Menu>
struct IEnumerator_1_t3607;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<SCR_Menu>
struct ICollection_1_t3608;
// System.Collections.ObjectModel.ReadOnlyCollection`1<SCR_Menu>
struct ReadOnlyCollection_1_t3006;
// SCR_Menu[]
struct SCR_MenuU5BU5D_t3004;
// System.Predicate`1<SCR_Menu>
struct Predicate_1_t3007;
// System.Comparison`1<SCR_Menu>
struct Comparison_1_t3008;
// System.Collections.Generic.List`1/Enumerator<SCR_Menu>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.List`1<SCR_Menu>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m1794(__this, method) (( void (*) (List_1_t377 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m16067(__this, ___collection, method) (( void (*) (List_1_t377 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::.ctor(System.Int32)
#define List_1__ctor_m16068(__this, ___capacity, method) (( void (*) (List_1_t377 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::.cctor()
#define List_1__cctor_m16069(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<SCR_Menu>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16070(__this, method) (( Object_t* (*) (List_1_t377 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16071(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t377 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<SCR_Menu>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16072(__this, method) (( Object_t * (*) (List_1_t377 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Menu>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16073(__this, ___item, method) (( int32_t (*) (List_1_t377 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Menu>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16074(__this, ___item, method) (( bool (*) (List_1_t377 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Menu>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16075(__this, ___item, method) (( int32_t (*) (List_1_t377 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16076(__this, ___index, ___item, method) (( void (*) (List_1_t377 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16077(__this, ___item, method) (( void (*) (List_1_t377 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Menu>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16078(__this, method) (( bool (*) (List_1_t377 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Menu>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16079(__this, method) (( bool (*) (List_1_t377 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SCR_Menu>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16080(__this, method) (( Object_t * (*) (List_1_t377 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Menu>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16081(__this, method) (( bool (*) (List_1_t377 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Menu>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16082(__this, method) (( bool (*) (List_1_t377 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SCR_Menu>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16083(__this, ___index, method) (( Object_t * (*) (List_1_t377 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16084(__this, ___index, ___value, method) (( void (*) (List_1_t377 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::Add(T)
#define List_1_Add_m16085(__this, ___item, method) (( void (*) (List_1_t377 *, SCR_Menu_t336 *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16086(__this, ___newCount, method) (( void (*) (List_1_t377 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16087(__this, ___collection, method) (( void (*) (List_1_t377 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16088(__this, ___enumerable, method) (( void (*) (List_1_t377 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m16089(__this, ___collection, method) (( void (*) (List_1_t377 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<SCR_Menu>::AsReadOnly()
#define List_1_AsReadOnly_m16090(__this, method) (( ReadOnlyCollection_1_t3006 * (*) (List_1_t377 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::Clear()
#define List_1_Clear_m16091(__this, method) (( void (*) (List_1_t377 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Menu>::Contains(T)
#define List_1_Contains_m16092(__this, ___item, method) (( bool (*) (List_1_t377 *, SCR_Menu_t336 *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16093(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t377 *, SCR_MenuU5BU5D_t3004*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<SCR_Menu>::Find(System.Predicate`1<T>)
#define List_1_Find_m16094(__this, ___match, method) (( SCR_Menu_t336 * (*) (List_1_t377 *, Predicate_1_t3007 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16095(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3007 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Menu>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16096(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t377 *, int32_t, int32_t, Predicate_1_t3007 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<SCR_Menu>::GetEnumerator()
#define List_1_GetEnumerator_m1797(__this, method) (( Enumerator_t423  (*) (List_1_t377 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Menu>::IndexOf(T)
#define List_1_IndexOf_m16097(__this, ___item, method) (( int32_t (*) (List_1_t377 *, SCR_Menu_t336 *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16098(__this, ___start, ___delta, method) (( void (*) (List_1_t377 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16099(__this, ___index, method) (( void (*) (List_1_t377 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::Insert(System.Int32,T)
#define List_1_Insert_m16100(__this, ___index, ___item, method) (( void (*) (List_1_t377 *, int32_t, SCR_Menu_t336 *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16101(__this, ___collection, method) (( void (*) (List_1_t377 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Menu>::Remove(T)
#define List_1_Remove_m16102(__this, ___item, method) (( bool (*) (List_1_t377 *, SCR_Menu_t336 *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Menu>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16103(__this, ___match, method) (( int32_t (*) (List_1_t377 *, Predicate_1_t3007 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m16104(__this, ___index, method) (( void (*) (List_1_t377 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::Reverse()
#define List_1_Reverse_m16105(__this, method) (( void (*) (List_1_t377 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::Sort()
#define List_1_Sort_m16106(__this, method) (( void (*) (List_1_t377 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16107(__this, ___comparison, method) (( void (*) (List_1_t377 *, Comparison_1_t3008 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<SCR_Menu>::ToArray()
#define List_1_ToArray_m16108(__this, method) (( SCR_MenuU5BU5D_t3004* (*) (List_1_t377 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::TrimExcess()
#define List_1_TrimExcess_m16109(__this, method) (( void (*) (List_1_t377 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Menu>::get_Capacity()
#define List_1_get_Capacity_m16110(__this, method) (( int32_t (*) (List_1_t377 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16111(__this, ___value, method) (( void (*) (List_1_t377 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Menu>::get_Count()
#define List_1_get_Count_m16112(__this, method) (( int32_t (*) (List_1_t377 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<SCR_Menu>::get_Item(System.Int32)
#define List_1_get_Item_m16113(__this, ___index, method) (( SCR_Menu_t336 * (*) (List_1_t377 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<SCR_Menu>::set_Item(System.Int32,T)
#define List_1_set_Item_m16114(__this, ___index, ___value, method) (( void (*) (List_1_t377 *, int32_t, SCR_Menu_t336 *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
