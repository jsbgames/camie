﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$48
struct U24ArrayTypeU2448_t2261;
struct U24ArrayTypeU2448_t2261_marshaled;

void U24ArrayTypeU2448_t2261_marshal(const U24ArrayTypeU2448_t2261& unmarshaled, U24ArrayTypeU2448_t2261_marshaled& marshaled);
void U24ArrayTypeU2448_t2261_marshal_back(const U24ArrayTypeU2448_t2261_marshaled& marshaled, U24ArrayTypeU2448_t2261& unmarshaled);
void U24ArrayTypeU2448_t2261_marshal_cleanup(U24ArrayTypeU2448_t2261_marshaled& marshaled);
