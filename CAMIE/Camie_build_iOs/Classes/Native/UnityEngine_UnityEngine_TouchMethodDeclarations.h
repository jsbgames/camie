﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Touch
struct Touch_t235;
struct Touch_t235_marshaled;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"

// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C" int32_t Touch_get_fingerId_m1778 (Touch_t235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" Vector2_t6  Touch_get_position_m729 (Touch_t235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C" int32_t Touch_get_phase_m1687 (Touch_t235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void Touch_t235_marshal(const Touch_t235& unmarshaled, Touch_t235_marshaled& marshaled);
void Touch_t235_marshal_back(const Touch_t235_marshaled& marshaled, Touch_t235& unmarshaled);
void Touch_t235_marshal_cleanup(Touch_t235_marshaled& marshaled);
