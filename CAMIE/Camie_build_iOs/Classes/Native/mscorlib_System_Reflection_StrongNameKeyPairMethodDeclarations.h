﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.StrongNameKeyPair
struct StrongNameKeyPair_t1890;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1043;
// System.Object
struct Object_t;
// System.Security.Cryptography.RSA
struct RSA_t1432;
// Mono.Security.StrongName
struct StrongName_t1748;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Reflection.StrongNameKeyPair::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void StrongNameKeyPair__ctor_m10367 (StrongNameKeyPair_t1890 * __this, SerializationInfo_t1043 * ___info, StreamingContext_t1044  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.StrongNameKeyPair::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m10368 (StrongNameKeyPair_t1890 * __this, SerializationInfo_t1043 * ___info, StreamingContext_t1044  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.StrongNameKeyPair::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10369 (StrongNameKeyPair_t1890 * __this, Object_t * ___sender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Reflection.StrongNameKeyPair::GetRSA()
extern "C" RSA_t1432 * StrongNameKeyPair_GetRSA_m10370 (StrongNameKeyPair_t1890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.StrongName System.Reflection.StrongNameKeyPair::StrongName()
extern "C" StrongName_t1748 * StrongNameKeyPair_StrongName_m10371 (StrongNameKeyPair_t1890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
