﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t2845;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Object[]
struct ObjectU5BU5D_t224;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t284;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t1196;

// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor()
extern "C" void Collection_1__ctor_m14019_gshared (Collection_1_t2845 * __this, const MethodInfo* method);
#define Collection_1__ctor_m14019(__this, method) (( void (*) (Collection_1_t2845 *, const MethodInfo*))Collection_1__ctor_m14019_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14020_gshared (Collection_1_t2845 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14020(__this, method) (( bool (*) (Collection_1_t2845 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14020_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m14021_gshared (Collection_1_t2845 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m14021(__this, ___array, ___index, method) (( void (*) (Collection_1_t2845 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m14021_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m14022_gshared (Collection_1_t2845 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m14022(__this, method) (( Object_t * (*) (Collection_1_t2845 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m14022_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m14023_gshared (Collection_1_t2845 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m14023(__this, ___value, method) (( int32_t (*) (Collection_1_t2845 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m14023_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m14024_gshared (Collection_1_t2845 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m14024(__this, ___value, method) (( bool (*) (Collection_1_t2845 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m14024_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m14025_gshared (Collection_1_t2845 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m14025(__this, ___value, method) (( int32_t (*) (Collection_1_t2845 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m14025_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m14026_gshared (Collection_1_t2845 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m14026(__this, ___index, ___value, method) (( void (*) (Collection_1_t2845 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m14026_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m14027_gshared (Collection_1_t2845 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m14027(__this, ___value, method) (( void (*) (Collection_1_t2845 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m14027_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m14028_gshared (Collection_1_t2845 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m14028(__this, method) (( bool (*) (Collection_1_t2845 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m14028_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m14029_gshared (Collection_1_t2845 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m14029(__this, method) (( Object_t * (*) (Collection_1_t2845 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m14029_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m14030_gshared (Collection_1_t2845 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m14030(__this, method) (( bool (*) (Collection_1_t2845 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m14030_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m14031_gshared (Collection_1_t2845 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m14031(__this, method) (( bool (*) (Collection_1_t2845 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m14031_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m14032_gshared (Collection_1_t2845 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m14032(__this, ___index, method) (( Object_t * (*) (Collection_1_t2845 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m14032_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m14033_gshared (Collection_1_t2845 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m14033(__this, ___index, ___value, method) (( void (*) (Collection_1_t2845 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m14033_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(T)
extern "C" void Collection_1_Add_m14034_gshared (Collection_1_t2845 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Add_m14034(__this, ___item, method) (( void (*) (Collection_1_t2845 *, Object_t *, const MethodInfo*))Collection_1_Add_m14034_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear()
extern "C" void Collection_1_Clear_m14035_gshared (Collection_1_t2845 * __this, const MethodInfo* method);
#define Collection_1_Clear_m14035(__this, method) (( void (*) (Collection_1_t2845 *, const MethodInfo*))Collection_1_Clear_m14035_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems()
extern "C" void Collection_1_ClearItems_m14036_gshared (Collection_1_t2845 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m14036(__this, method) (( void (*) (Collection_1_t2845 *, const MethodInfo*))Collection_1_ClearItems_m14036_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(T)
extern "C" bool Collection_1_Contains_m14037_gshared (Collection_1_t2845 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Contains_m14037(__this, ___item, method) (( bool (*) (Collection_1_t2845 *, Object_t *, const MethodInfo*))Collection_1_Contains_m14037_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m14038_gshared (Collection_1_t2845 * __this, ObjectU5BU5D_t224* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m14038(__this, ___array, ___index, method) (( void (*) (Collection_1_t2845 *, ObjectU5BU5D_t224*, int32_t, const MethodInfo*))Collection_1_CopyTo_m14038_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m14039_gshared (Collection_1_t2845 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m14039(__this, method) (( Object_t* (*) (Collection_1_t2845 *, const MethodInfo*))Collection_1_GetEnumerator_m14039_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m14040_gshared (Collection_1_t2845 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m14040(__this, ___item, method) (( int32_t (*) (Collection_1_t2845 *, Object_t *, const MethodInfo*))Collection_1_IndexOf_m14040_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m14041_gshared (Collection_1_t2845 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Insert_m14041(__this, ___index, ___item, method) (( void (*) (Collection_1_t2845 *, int32_t, Object_t *, const MethodInfo*))Collection_1_Insert_m14041_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m14042_gshared (Collection_1_t2845 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m14042(__this, ___index, ___item, method) (( void (*) (Collection_1_t2845 *, int32_t, Object_t *, const MethodInfo*))Collection_1_InsertItem_m14042_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(T)
extern "C" bool Collection_1_Remove_m14043_gshared (Collection_1_t2845 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Remove_m14043(__this, ___item, method) (( bool (*) (Collection_1_t2845 *, Object_t *, const MethodInfo*))Collection_1_Remove_m14043_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m14044_gshared (Collection_1_t2845 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m14044(__this, ___index, method) (( void (*) (Collection_1_t2845 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m14044_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m14045_gshared (Collection_1_t2845 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m14045(__this, ___index, method) (( void (*) (Collection_1_t2845 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m14045_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count()
extern "C" int32_t Collection_1_get_Count_m14046_gshared (Collection_1_t2845 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m14046(__this, method) (( int32_t (*) (Collection_1_t2845 *, const MethodInfo*))Collection_1_get_Count_m14046_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * Collection_1_get_Item_m14047_gshared (Collection_1_t2845 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m14047(__this, ___index, method) (( Object_t * (*) (Collection_1_t2845 *, int32_t, const MethodInfo*))Collection_1_get_Item_m14047_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m14048_gshared (Collection_1_t2845 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_set_Item_m14048(__this, ___index, ___value, method) (( void (*) (Collection_1_t2845 *, int32_t, Object_t *, const MethodInfo*))Collection_1_set_Item_m14048_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m14049_gshared (Collection_1_t2845 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_SetItem_m14049(__this, ___index, ___item, method) (( void (*) (Collection_1_t2845 *, int32_t, Object_t *, const MethodInfo*))Collection_1_SetItem_m14049_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m14050_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m14050(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m14050_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::ConvertItem(System.Object)
extern "C" Object_t * Collection_1_ConvertItem_m14051_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m14051(__this /* static, unused */, ___item, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m14051_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m14052_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m14052(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m14052_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m14053_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m14053(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m14053_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m14054_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m14054(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m14054_gshared)(__this /* static, unused */, ___list, method)
