﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t157;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityStandardAssets.Effects.SmokeParticles
struct  SmokeParticles_t158  : public MonoBehaviour_t3
{
	// UnityEngine.AudioClip[] UnityStandardAssets.Effects.SmokeParticles::extinguishSounds
	AudioClipU5BU5D_t157* ___extinguishSounds_2;
};
