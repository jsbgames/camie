﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityStandardAssets.CrossPlatformInput.AxisTouchButton[]
// UnityStandardAssets.CrossPlatformInput.AxisTouchButton[]
struct  AxisTouchButtonU5BU5D_t229  : public Array_t
{
};
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis[]
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis[]
struct  VirtualAxisU5BU5D_t2810  : public Array_t
{
};
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton[]
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton[]
struct  VirtualButtonU5BU5D_t2836  : public Array_t
{
};
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[]
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[]
struct  ReplacementDefinitionU5BU5D_t167  : public Array_t
{
};
// UnityStandardAssets.Utility.TimedObjectActivator/Entry[]
// UnityStandardAssets.Utility.TimedObjectActivator/Entry[]
struct  EntryU5BU5D_t200  : public Array_t
{
};
