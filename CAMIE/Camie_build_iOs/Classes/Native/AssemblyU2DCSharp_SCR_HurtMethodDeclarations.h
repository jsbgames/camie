﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Hurt
struct SCR_Hurt_t393;
// UnityEngine.Collider
struct Collider_t138;

// System.Void SCR_Hurt::.ctor()
extern "C" void SCR_Hurt__ctor_m1538 (SCR_Hurt_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Hurt::OnTriggerEnter(UnityEngine.Collider)
extern "C" void SCR_Hurt_OnTriggerEnter_m1539 (SCR_Hurt_t393 * __this, Collider_t138 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Hurt::AvatarWasHurt()
extern "C" void SCR_Hurt_AvatarWasHurt_m1540 (SCR_Hurt_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
