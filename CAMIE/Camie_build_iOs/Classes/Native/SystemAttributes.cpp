﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
extern TypeInfo* NeutralResourcesLanguageAttribute_t1467_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1468_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyInformationalVersionAttribute_t1469_il2cpp_TypeInfo_var;
extern TypeInfo* SatelliteContractVersionAttribute_t1470_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t713_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t712_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t711_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDefaultAliasAttribute_t1471_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t709_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t714_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t715_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t1472_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t1473_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t1474_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t1475_il2cpp_TypeInfo_var;
extern TypeInfo* InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t716_il2cpp_TypeInfo_var;
void g_System_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NeutralResourcesLanguageAttribute_t1467_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2904);
		CLSCompliantAttribute_t1468_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2905);
		AssemblyInformationalVersionAttribute_t1469_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2906);
		SatelliteContractVersionAttribute_t1470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2907);
		AssemblyCopyrightAttribute_t713_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1168);
		AssemblyProductAttribute_t712_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1167);
		AssemblyCompanyAttribute_t711_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1166);
		AssemblyDefaultAliasAttribute_t1471_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2908);
		AssemblyDescriptionAttribute_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1164);
		ComVisibleAttribute_t714_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1169);
		AssemblyTitleAttribute_t715_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1170);
		RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(107);
		CompilationRelaxationsAttribute_t1472_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2909);
		DebuggableAttribute_t1473_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2910);
		AssemblyDelaySignAttribute_t1474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2911);
		AssemblyKeyFileAttribute_t1475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2912);
		InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1857);
		AssemblyFileVersionAttribute_t716_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1171);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 18;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		NeutralResourcesLanguageAttribute_t1467 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1467 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t1467_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m6536(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1468 * tmp;
		tmp = (CLSCompliantAttribute_t1468 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1468_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m6537(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyInformationalVersionAttribute_t1469 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t1469 *)il2cpp_codegen_object_new (AssemblyInformationalVersionAttribute_t1469_il2cpp_TypeInfo_var);
		AssemblyInformationalVersionAttribute__ctor_m6538(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t1470 * tmp;
		tmp = (SatelliteContractVersionAttribute_t1470 *)il2cpp_codegen_object_new (SatelliteContractVersionAttribute_t1470_il2cpp_TypeInfo_var);
		SatelliteContractVersionAttribute__ctor_m6539(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t713 * tmp;
		tmp = (AssemblyCopyrightAttribute_t713 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t713_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m3471(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t712 * tmp;
		tmp = (AssemblyProductAttribute_t712 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t712_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m3470(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t711 * tmp;
		tmp = (AssemblyCompanyAttribute_t711 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t711_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m3469(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t1471 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t1471 *)il2cpp_codegen_object_new (AssemblyDefaultAliasAttribute_t1471_il2cpp_TypeInfo_var);
		AssemblyDefaultAliasAttribute__ctor_m6540(tmp, il2cpp_codegen_string_new_wrapper("System.dll"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t709 * tmp;
		tmp = (AssemblyDescriptionAttribute_t709 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t709_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m3467(tmp, il2cpp_codegen_string_new_wrapper("System.dll"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t714 * tmp;
		tmp = (ComVisibleAttribute_t714 *)il2cpp_codegen_object_new (ComVisibleAttribute_t714_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m3472(tmp, false, NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t715 * tmp;
		tmp = (AssemblyTitleAttribute_t715 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t715_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m3473(tmp, il2cpp_codegen_string_new_wrapper("System.dll"), NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t258 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t258 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1021(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1022(tmp, true, NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t1472 * tmp;
		tmp = (CompilationRelaxationsAttribute_t1472 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t1472_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m6541(tmp, 8, NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t1473 * tmp;
		tmp = (DebuggableAttribute_t1473 *)il2cpp_codegen_object_new (DebuggableAttribute_t1473_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m6542(tmp, 2, NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1474 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1474 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t1474_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m6543(tmp, true, NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1475 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1475 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t1475_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m6544(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("System.Net, PublicKey=00240000048000009400000006020000002400005253413100040000010001008D56C76F9E8649383049F383C44BE0EC204181822A6C31CF5EB7EF486944D032188EA1D3920763712CCB12D75FB77E9811149E6148E5D32FBAAB37611C1878DDC19E20EF135D0CB2CFF2BFEC3D115810C3D9069638FE4BE215DBF795861920E5AB6F7DB2E2CEEF136AC23D5DD2BF031700AEC232F6C6B1C785B4305C123B37AB"), NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t716 * tmp;
		tmp = (AssemblyFileVersionAttribute_t716 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t716_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m3474(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void Locale_t1260_CustomAttributesCacheGenerator_Locale_t1260_Locale_GetText_m5475_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void MonoTODOAttribute_t1261_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void GeneratedCodeAttribute_t1119_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 32767, NULL);
		AttributeUsageAttribute_set_Inherited_m5323(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t714_il2cpp_TypeInfo_var;
void Stack_1_t1476_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t714_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1169);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t714 * tmp;
		tmp = (ComVisibleAttribute_t714 *)il2cpp_codegen_object_new (ComVisibleAttribute_t714_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m3472(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void HybridDictionary_t1264_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void ListDictionary_t1263_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.MonoTODOAttribute
#include "System_System_MonoTODOAttribute.h"
// System.MonoTODOAttribute
#include "System_System_MonoTODOAttributeMethodDeclarations.h"
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void NameObjectCollectionBase_t1268_CustomAttributesCacheGenerator_NameObjectCollectionBase_FindFirstMatchedItem_m5547(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void KeysCollection_t1270_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void NameValueCollection_t1274_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void DefaultValueAttribute_t1116_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 32767, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void EditorBrowsableAttribute_t1114_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 6140, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t714_il2cpp_TypeInfo_var;
void TypeConverter_t1276_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t714_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1169);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t714 * tmp;
		tmp = (ComVisibleAttribute_t714 *)il2cpp_codegen_object_new (ComVisibleAttribute_t714_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m3472(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t714_il2cpp_TypeInfo_var;
void TypeConverterAttribute_t1277_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		ComVisibleAttribute_t714_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1169);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 32767, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t714 * tmp;
		tmp = (ComVisibleAttribute_t714 *)il2cpp_codegen_object_new (ComVisibleAttribute_t714_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m3472(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void SslPolicyErrors_t1279_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void FileWebRequest_t1284_CustomAttributesCacheGenerator_FileWebRequest__ctor_m5570(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m3495(tmp, il2cpp_codegen_string_new_wrapper("Serialization is obsoleted for this type"), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void FtpWebRequest_t1289_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void FtpWebRequest_t1289_CustomAttributesCacheGenerator_FtpWebRequest_U3CcallbackU3Em__B_m5579(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void GlobalProxySelection_t1290_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("Use WebRequest.DefaultProxy instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void HttpWebRequest_t1296_CustomAttributesCacheGenerator_HttpWebRequest__ctor_m5585(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m3495(tmp, il2cpp_codegen_string_new_wrapper("Serialization is obsoleted for this type"), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void IPv6Address_t1299_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void SecurityProtocolType_t1300_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void ServicePointManager_t1303_CustomAttributesCacheGenerator_ServicePointManager_t1303____CertificatePolicy_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m3495(tmp, il2cpp_codegen_string_new_wrapper("Use ServerCertificateValidationCallback instead"), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void ServicePointManager_t1303_CustomAttributesCacheGenerator_ServicePointManager_t1303____CheckCertificateRevocationList_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5477(tmp, il2cpp_codegen_string_new_wrapper("CRL checks not implemented"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t714_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void WebHeaderCollection_t1282_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t714_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1169);
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t714 * tmp;
		tmp = (ComVisibleAttribute_t714 *)il2cpp_codegen_object_new (ComVisibleAttribute_t714_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m3472(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void WebProxy_t1307_CustomAttributesCacheGenerator_WebProxy_t1307____UseDefaultCredentials_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5477(tmp, il2cpp_codegen_string_new_wrapper("Does not affect Credentials, since CredentialCache.DefaultCredentials is not implemented."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void WebRequest_t1285_CustomAttributesCacheGenerator_WebRequest_GetDefaultWebProxy_m5687(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5477(tmp, il2cpp_codegen_string_new_wrapper("Needs to respect Module, Proxy.AutoDetect, and Proxy.ScriptLocation config settings"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void OpenFlags_t1309_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PublicKey_t1313_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void X500DistinguishedName_t1316_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5477(tmp, il2cpp_codegen_string_new_wrapper("Some X500DistinguishedNameFlags options aren't supported, like DoNotUsePlusSign, DoNotUseQuotes and ForceUTF8Encoding"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void X500DistinguishedNameFlags_t1317_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void X509Certificate2_t1322_CustomAttributesCacheGenerator_X509Certificate2_GetNameInfo_m5727(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5477(tmp, il2cpp_codegen_string_new_wrapper("always return String.Empty for UpnName, DnsFromAlternativeName and UrlName"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void X509Certificate2_t1322_CustomAttributesCacheGenerator_X509Certificate2_Import_m5731(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5477(tmp, il2cpp_codegen_string_new_wrapper("missing KeyStorageFlags support"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void X509Certificate2_t1322_CustomAttributesCacheGenerator_X509Certificate2_Verify_m5736(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5477(tmp, il2cpp_codegen_string_new_wrapper("by default this depends on the incomplete X509Chain"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void X509Certificate2Collection_t1324_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void X509Certificate2Collection_t1324_CustomAttributesCacheGenerator_X509Certificate2Collection_AddRange_m5742(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5477(tmp, il2cpp_codegen_string_new_wrapper("Method isn't transactional (like documented)"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void X509Certificate2Collection_t1324_CustomAttributesCacheGenerator_X509Certificate2Collection_Find_m5744(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5477(tmp, il2cpp_codegen_string_new_wrapper("Does not support X509FindType.FindByTemplateName, FindByApplicationPolicy and FindByCertificatePolicy"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void X509CertificateCollection_t1294_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void X509Chain_t1334_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void X509Chain_t1334_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void X509Chain_t1334_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void X509Chain_t1334_CustomAttributesCacheGenerator_X509Chain_Build_m5766(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5477(tmp, il2cpp_codegen_string_new_wrapper("Not totally RFC3280 compliant, but neither is MS implementation..."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void X509ChainElementCollection_t1328_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void X509ChainStatusFlags_t1338_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void X509EnhancedKeyUsageExtension_t1339_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void X509ExtensionCollection_t1320_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void X509KeyUsageFlags_t1343_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void X509Store_t1333_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void X509VerificationFlags_t1350_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void AsnEncodedData_t1311_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Oid_t1312_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map10(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void OidCollection_t1336_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void CaptureCollection_t1357_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void GroupCollection_t1360_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void MatchCollection_t1362_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void Regex_t421_CustomAttributesCacheGenerator_capnames(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void Regex_t421_CustomAttributesCacheGenerator_caps(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void Regex_t421_CustomAttributesCacheGenerator_capsize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void Regex_t421_CustomAttributesCacheGenerator_capslist(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void RegexOptions_t1367_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void OpFlags_t1369_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void IntervalCollection_t1392_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void ExpressionCollection_t1395_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ComponentModel.TypeConverterAttribute
#include "System_System_ComponentModel_TypeConverterAttribute.h"
// System.ComponentModel.TypeConverterAttribute
#include "System_System_ComponentModel_TypeConverterAttributeMethodDeclarations.h"
// System.UriTypeConverter
#include "System_System_UriTypeConverter.h"
extern const Il2CppType* UriTypeConverter_t1424_0_0_0_var;
extern TypeInfo* TypeConverterAttribute_t1277_il2cpp_TypeInfo_var;
void Uri_t926_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UriTypeConverter_t1424_0_0_0_var = il2cpp_codegen_type_from_index(2914);
		TypeConverterAttribute_t1277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2767);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeConverterAttribute_t1277 * tmp;
		tmp = (TypeConverterAttribute_t1277 *)il2cpp_codegen_object_new (TypeConverterAttribute_t1277_il2cpp_TypeInfo_var);
		TypeConverterAttribute__ctor_m5562(tmp, il2cpp_codegen_type_get_object(UriTypeConverter_t1424_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Uri_t926_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map12(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Uri_t926_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Uri_t926_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map14(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Uri_t926_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map15(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Uri_t926_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map16(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void Uri_t926_CustomAttributesCacheGenerator_Uri__ctor_m6305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m6609(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void Uri_t926_CustomAttributesCacheGenerator_Uri_EscapeString_m6332(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m6609(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void Uri_t926_CustomAttributesCacheGenerator_Uri_Unescape_m6335(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m6609(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1261_il2cpp_TypeInfo_var;
void UriParser_t1416_CustomAttributesCacheGenerator_UriParser_OnRegister_m6359(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2913);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1261 * tmp;
		tmp = (MonoTODOAttribute_t1261 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1261_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m5476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3E_t1428_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_System_Assembly_AttributeGenerators[75] = 
{
	NULL,
	g_System_Assembly_CustomAttributesCacheGenerator,
	Locale_t1260_CustomAttributesCacheGenerator_Locale_t1260_Locale_GetText_m5475_Arg1_ParameterInfo,
	MonoTODOAttribute_t1261_CustomAttributesCacheGenerator,
	GeneratedCodeAttribute_t1119_CustomAttributesCacheGenerator,
	Stack_1_t1476_CustomAttributesCacheGenerator,
	HybridDictionary_t1264_CustomAttributesCacheGenerator,
	ListDictionary_t1263_CustomAttributesCacheGenerator,
	NameObjectCollectionBase_t1268_CustomAttributesCacheGenerator_NameObjectCollectionBase_FindFirstMatchedItem_m5547,
	KeysCollection_t1270_CustomAttributesCacheGenerator,
	NameValueCollection_t1274_CustomAttributesCacheGenerator,
	DefaultValueAttribute_t1116_CustomAttributesCacheGenerator,
	EditorBrowsableAttribute_t1114_CustomAttributesCacheGenerator,
	TypeConverter_t1276_CustomAttributesCacheGenerator,
	TypeConverterAttribute_t1277_CustomAttributesCacheGenerator,
	SslPolicyErrors_t1279_CustomAttributesCacheGenerator,
	FileWebRequest_t1284_CustomAttributesCacheGenerator_FileWebRequest__ctor_m5570,
	FtpWebRequest_t1289_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1C,
	FtpWebRequest_t1289_CustomAttributesCacheGenerator_FtpWebRequest_U3CcallbackU3Em__B_m5579,
	GlobalProxySelection_t1290_CustomAttributesCacheGenerator,
	HttpWebRequest_t1296_CustomAttributesCacheGenerator_HttpWebRequest__ctor_m5585,
	IPv6Address_t1299_CustomAttributesCacheGenerator,
	SecurityProtocolType_t1300_CustomAttributesCacheGenerator,
	ServicePointManager_t1303_CustomAttributesCacheGenerator_ServicePointManager_t1303____CertificatePolicy_PropertyInfo,
	ServicePointManager_t1303_CustomAttributesCacheGenerator_ServicePointManager_t1303____CheckCertificateRevocationList_PropertyInfo,
	WebHeaderCollection_t1282_CustomAttributesCacheGenerator,
	WebProxy_t1307_CustomAttributesCacheGenerator_WebProxy_t1307____UseDefaultCredentials_PropertyInfo,
	WebRequest_t1285_CustomAttributesCacheGenerator_WebRequest_GetDefaultWebProxy_m5687,
	OpenFlags_t1309_CustomAttributesCacheGenerator,
	PublicKey_t1313_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9,
	X500DistinguishedName_t1316_CustomAttributesCacheGenerator,
	X500DistinguishedNameFlags_t1317_CustomAttributesCacheGenerator,
	X509Certificate2_t1322_CustomAttributesCacheGenerator_X509Certificate2_GetNameInfo_m5727,
	X509Certificate2_t1322_CustomAttributesCacheGenerator_X509Certificate2_Import_m5731,
	X509Certificate2_t1322_CustomAttributesCacheGenerator_X509Certificate2_Verify_m5736,
	X509Certificate2Collection_t1324_CustomAttributesCacheGenerator,
	X509Certificate2Collection_t1324_CustomAttributesCacheGenerator_X509Certificate2Collection_AddRange_m5742,
	X509Certificate2Collection_t1324_CustomAttributesCacheGenerator_X509Certificate2Collection_Find_m5744,
	X509CertificateCollection_t1294_CustomAttributesCacheGenerator,
	X509Chain_t1334_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB,
	X509Chain_t1334_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapC,
	X509Chain_t1334_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapD,
	X509Chain_t1334_CustomAttributesCacheGenerator_X509Chain_Build_m5766,
	X509ChainElementCollection_t1328_CustomAttributesCacheGenerator,
	X509ChainStatusFlags_t1338_CustomAttributesCacheGenerator,
	X509EnhancedKeyUsageExtension_t1339_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapE,
	X509ExtensionCollection_t1320_CustomAttributesCacheGenerator,
	X509KeyUsageFlags_t1343_CustomAttributesCacheGenerator,
	X509Store_t1333_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF,
	X509VerificationFlags_t1350_CustomAttributesCacheGenerator,
	AsnEncodedData_t1311_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA,
	Oid_t1312_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map10,
	OidCollection_t1336_CustomAttributesCacheGenerator,
	CaptureCollection_t1357_CustomAttributesCacheGenerator,
	GroupCollection_t1360_CustomAttributesCacheGenerator,
	MatchCollection_t1362_CustomAttributesCacheGenerator,
	Regex_t421_CustomAttributesCacheGenerator_capnames,
	Regex_t421_CustomAttributesCacheGenerator_caps,
	Regex_t421_CustomAttributesCacheGenerator_capsize,
	Regex_t421_CustomAttributesCacheGenerator_capslist,
	RegexOptions_t1367_CustomAttributesCacheGenerator,
	OpFlags_t1369_CustomAttributesCacheGenerator,
	IntervalCollection_t1392_CustomAttributesCacheGenerator,
	ExpressionCollection_t1395_CustomAttributesCacheGenerator,
	Uri_t926_CustomAttributesCacheGenerator,
	Uri_t926_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map12,
	Uri_t926_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map13,
	Uri_t926_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map14,
	Uri_t926_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map15,
	Uri_t926_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map16,
	Uri_t926_CustomAttributesCacheGenerator_Uri__ctor_m6305,
	Uri_t926_CustomAttributesCacheGenerator_Uri_EscapeString_m6332,
	Uri_t926_CustomAttributesCacheGenerator_Uri_Unescape_m6335,
	UriParser_t1416_CustomAttributesCacheGenerator_UriParser_OnRegister_m6359,
	U3CPrivateImplementationDetailsU3E_t1428_CustomAttributesCacheGenerator,
};
