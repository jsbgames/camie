﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_TimeManager
struct  SCR_TimeManager_t383  : public MonoBehaviour_t3
{
	// System.Boolean SCR_TimeManager::isPaused
	bool ___isPaused_2;
	// System.Boolean SCR_TimeManager::isCounting
	bool ___isCounting_3;
	// System.Single SCR_TimeManager::timeSinceLevelStart
	float ___timeSinceLevelStart_4;
};
