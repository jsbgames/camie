﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Collision
struct Collision_t146;
// UnityEngine.Rigidbody
struct Rigidbody_t14;
// UnityEngine.Collider
struct Collider_t138;
// UnityEngine.Transform
struct Transform_t1;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t245;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.Collision::get_relativeVelocity()
extern "C" Vector3_t4  Collision_get_relativeVelocity_m913 (Collision_t146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.Collision::get_rigidbody()
extern "C" Rigidbody_t14 * Collision_get_rigidbody_m4486 (Collision_t146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.Collision::get_collider()
extern "C" Collider_t138 * Collision_get_collider_m4487 (Collision_t146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Collision::get_transform()
extern "C" Transform_t1 * Collision_get_transform_m1762 (Collision_t146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ContactPoint[] UnityEngine.Collision::get_contacts()
extern "C" ContactPointU5BU5D_t245* Collision_get_contacts_m912 (Collision_t146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
