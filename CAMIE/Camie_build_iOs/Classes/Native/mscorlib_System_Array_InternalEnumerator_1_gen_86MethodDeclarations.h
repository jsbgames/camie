﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Math.BigInteger>
struct InternalEnumerator_1_t3444;
// System.Object
struct Object_t;
// Mono.Math.BigInteger
struct BigInteger_t1520;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22147(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3444 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22148(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3444 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::Dispose()
#define InternalEnumerator_1_Dispose_m22149(__this, method) (( void (*) (InternalEnumerator_1_t3444 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22150(__this, method) (( bool (*) (InternalEnumerator_1_t3444 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::get_Current()
#define InternalEnumerator_1_get_Current_m22151(__this, method) (( BigInteger_t1520 * (*) (InternalEnumerator_1_t3444 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
