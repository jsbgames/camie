﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets._2D.CameraFollow
struct CameraFollow_t5;

// System.Void UnityStandardAssets._2D.CameraFollow::.ctor()
extern "C" void CameraFollow__ctor_m3 (CameraFollow_t5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets._2D.CameraFollow::Awake()
extern "C" void CameraFollow_Awake_m4 (CameraFollow_t5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets._2D.CameraFollow::CheckXMargin()
extern "C" bool CameraFollow_CheckXMargin_m5 (CameraFollow_t5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets._2D.CameraFollow::CheckYMargin()
extern "C" bool CameraFollow_CheckYMargin_m6 (CameraFollow_t5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets._2D.CameraFollow::Update()
extern "C" void CameraFollow_Update_m7 (CameraFollow_t5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets._2D.CameraFollow::TrackPlayer()
extern "C" void CameraFollow_TrackPlayer_m8 (CameraFollow_t5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
