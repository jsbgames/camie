﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_t269;

// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
extern "C" void DebuggerHiddenAttribute__ctor_m1045 (DebuggerHiddenAttribute_t269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
