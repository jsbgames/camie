﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Collectible
struct SCR_Collectible_t331;

// System.Void SCR_Collectible::.ctor()
extern "C" void SCR_Collectible__ctor_m1188 (SCR_Collectible_t331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Collectible::Collect()
// System.Int32 SCR_Collectible::get_PointValue()
extern "C" int32_t SCR_Collectible_get_PointValue_m1189 (SCR_Collectible_t331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Collectible::get_IsTaken()
extern "C" bool SCR_Collectible_get_IsTaken_m1190 (SCR_Collectible_t331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Collectible::set_IsTaken(System.Boolean)
extern "C" void SCR_Collectible_set_IsTaken_m1191 (SCR_Collectible_t331 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
