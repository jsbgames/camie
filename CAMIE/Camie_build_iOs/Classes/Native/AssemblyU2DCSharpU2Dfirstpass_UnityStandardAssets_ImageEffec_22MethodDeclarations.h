﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.ImageEffects.ColorCorrectionCurves
struct ColorCorrectionCurves_t83;
// UnityEngine.RenderTexture
struct RenderTexture_t101;

// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::.ctor()
extern "C" void ColorCorrectionCurves__ctor_m230 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Start()
extern "C" void ColorCorrectionCurves_Start_m231 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Awake()
extern "C" void ColorCorrectionCurves_Awake_m232 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::CheckResources()
extern "C" bool ColorCorrectionCurves_CheckResources_m233 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateParameters()
extern "C" void ColorCorrectionCurves_UpdateParameters_m234 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateTextures()
extern "C" void ColorCorrectionCurves_UpdateTextures_m235 (ColorCorrectionCurves_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ColorCorrectionCurves_OnRenderImage_m236 (ColorCorrectionCurves_t83 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method) IL2CPP_METHOD_ATTR;
