﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t54;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct  ReplacementDefinition_t166  : public Object_t
{
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::original
	Shader_t54 * ___original_0;
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::replacement
	Shader_t54 * ___replacement_1;
};
