﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.PointerInputModule/ButtonState
struct ButtonState_t492;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
struct  Comparison_1_t3076  : public MulticastDelegate_t549
{
};
