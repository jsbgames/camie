﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
struct InternalEnumerator_1_t3370;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m21400_gshared (InternalEnumerator_1_t3370 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m21400(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3370 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m21400_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21401_gshared (InternalEnumerator_1_t3370 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21401(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3370 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21401_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m21402_gshared (InternalEnumerator_1_t3370 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m21402(__this, method) (( void (*) (InternalEnumerator_1_t3370 *, const MethodInfo*))InternalEnumerator_1_Dispose_m21402_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m21403_gshared (InternalEnumerator_1_t3370 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m21403(__this, method) (( bool (*) (InternalEnumerator_1_t3370 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m21403_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern "C" HitInfo_t983  InternalEnumerator_1_get_Current_m21404_gshared (InternalEnumerator_1_t3370 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m21404(__this, method) (( HitInfo_t983  (*) (InternalEnumerator_1_t3370 *, const MethodInfo*))InternalEnumerator_1_get_Current_m21404_gshared)(__this, method)
