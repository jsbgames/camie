﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t3250;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m19849_gshared (DefaultComparer_t3250 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m19849(__this, method) (( void (*) (DefaultComparer_t3250 *, const MethodInfo*))DefaultComparer__ctor_m19849_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m19850_gshared (DefaultComparer_t3250 * __this, UILineInfo_t687  ___x, UILineInfo_t687  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m19850(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3250 *, UILineInfo_t687 , UILineInfo_t687 , const MethodInfo*))DefaultComparer_Compare_m19850_gshared)(__this, ___x, ___y, method)
