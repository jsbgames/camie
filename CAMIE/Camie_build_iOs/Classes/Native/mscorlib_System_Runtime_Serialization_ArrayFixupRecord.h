﻿#pragma once
#include <stdint.h>
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecord.h"
// System.Runtime.Serialization.ArrayFixupRecord
struct  ArrayFixupRecord_t2051  : public BaseFixupRecord_t2050
{
	// System.Int32 System.Runtime.Serialization.ArrayFixupRecord::_index
	int32_t ____index_4;
};
