﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioSource>
struct Enumerator_t428;
// System.Object
struct Object_t;
// UnityEngine.AudioSource
struct AudioSource_t247;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>
struct Dictionary_2_t389;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioSource>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7MethodDeclarations.h"
#define Enumerator__ctor_m16254(__this, ___host, method) (( void (*) (Enumerator_t428 *, Dictionary_2_t389 *, const MethodInfo*))Enumerator__ctor_m13721_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioSource>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16255(__this, method) (( Object_t * (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13722_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioSource>::Dispose()
#define Enumerator_Dispose_m16256(__this, method) (( void (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_Dispose_m13723_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioSource>::MoveNext()
#define Enumerator_MoveNext_m1817(__this, method) (( bool (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_MoveNext_m13724_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioSource>::get_Current()
#define Enumerator_get_Current_m1816(__this, method) (( AudioSource_t247 * (*) (Enumerator_t428 *, const MethodInfo*))Enumerator_get_Current_m13725_gshared)(__this, method)
