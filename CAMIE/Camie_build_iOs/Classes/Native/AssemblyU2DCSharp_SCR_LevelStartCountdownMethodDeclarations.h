﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_LevelStartCountdown
struct SCR_LevelStartCountdown_t370;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.Transform
struct Transform_t1;

// System.Void SCR_LevelStartCountdown::.ctor()
extern "C" void SCR_LevelStartCountdown__ctor_m1377 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelStartCountdown::Awake()
extern "C" void SCR_LevelStartCountdown_Awake_m1378 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelStartCountdown::OnEnable()
extern "C" void SCR_LevelStartCountdown_OnEnable_m1379 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelStartCountdown::OnDisable()
extern "C" void SCR_LevelStartCountdown_OnDisable_m1380 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelStartCountdown::Countdown()
extern "C" void SCR_LevelStartCountdown_Countdown_m1381 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_LevelStartCountdown::Rescale(System.Boolean,UnityEngine.Transform)
extern "C" Object_t * SCR_LevelStartCountdown_Rescale_m1382 (SCR_LevelStartCountdown_t370 * __this, bool ___up, Transform_t1 * ___objToScale, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelStartCountdown::Reinitialize()
extern "C" void SCR_LevelStartCountdown_Reinitialize_m1383 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelStartCountdown::HideAll()
extern "C" void SCR_LevelStartCountdown_HideAll_m1384 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelStartCountdown::FillCountdownArray()
extern "C" void SCR_LevelStartCountdown_FillCountdownArray_m1385 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
