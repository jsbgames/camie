﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t242;
// System.Collections.Generic.List`1<SCR_Puceron>
struct List_1_t387;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_PointManager
struct  SCR_PointManager_t381  : public MonoBehaviour_t3
{
	// System.Int32[] SCR_PointManager::pointsForLevelAccess
	Int32U5BU5D_t242* ___pointsForLevelAccess_7;
	// System.Int32 SCR_PointManager::ladybugsCurrLevel
	int32_t ___ladybugsCurrLevel_8;
	// System.Int32 SCR_PointManager::puceronsCurrLevel
	int32_t ___puceronsCurrLevel_9;
	// System.Int32 SCR_PointManager::puceronsMovingInLevel
	int32_t ___puceronsMovingInLevel_10;
	// System.Int32 SCR_PointManager::puceronsStaticInLevel
	int32_t ___puceronsStaticInLevel_11;
	// System.Collections.Generic.List`1<SCR_Puceron> SCR_PointManager::puceronsCollected
	List_1_t387 * ___puceronsCollected_12;
	// System.Int32[] SCR_PointManager::highscorePerLevel
	Int32U5BU5D_t242* ___highscorePerLevel_13;
	// System.Int32 SCR_PointManager::scoreCumul
	int32_t ___scoreCumul_14;
	// System.Int32 SCR_PointManager::totalScoreCurr
	int32_t ___totalScoreCurr_15;
	// System.Boolean SCR_PointManager::isScoreCounting
	bool ___isScoreCounting_16;
};
