﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>
struct DefaultComparer_t2921;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::.ctor()
extern "C" void DefaultComparer__ctor_m15092_gshared (DefaultComparer_t2921 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m15092(__this, method) (( void (*) (DefaultComparer_t2921 *, const MethodInfo*))DefaultComparer__ctor_m15092_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m15093_gshared (DefaultComparer_t2921 * __this, Vector2_t6  ___x, Vector2_t6  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m15093(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2921 *, Vector2_t6 , Vector2_t6 , const MethodInfo*))DefaultComparer_Compare_m15093_gshared)(__this, ___x, ___y, method)
