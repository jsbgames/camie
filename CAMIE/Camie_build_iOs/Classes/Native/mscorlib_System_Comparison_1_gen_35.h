﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.MatchDirectConnectInfo
struct MatchDirectConnectInfo_t913;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct  Comparison_1_t3279  : public MulticastDelegate_t549
{
};
