﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct Stack_1_t3181;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct IEnumerator_1_t3704;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t651;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_4.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m18802(__this, method) (( void (*) (Stack_1_t3181 *, const MethodInfo*))Stack_1__ctor_m16469_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m18803(__this, method) (( bool (*) (Stack_1_t3181 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m16470_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m18804(__this, method) (( Object_t * (*) (Stack_1_t3181 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m16471_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m18805(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3181 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m16472_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18806(__this, method) (( Object_t* (*) (Stack_1_t3181 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16473_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m18807(__this, method) (( Object_t * (*) (Stack_1_t3181 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m16474_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Peek()
#define Stack_1_Peek_m18808(__this, method) (( List_1_t651 * (*) (Stack_1_t3181 *, const MethodInfo*))Stack_1_Peek_m16475_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Pop()
#define Stack_1_Pop_m18809(__this, method) (( List_1_t651 * (*) (Stack_1_t3181 *, const MethodInfo*))Stack_1_Pop_m16476_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Push(T)
#define Stack_1_Push_m18810(__this, ___t, method) (( void (*) (Stack_1_t3181 *, List_1_t651 *, const MethodInfo*))Stack_1_Push_m16477_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Count()
#define Stack_1_get_Count_m18811(__this, method) (( int32_t (*) (Stack_1_t3181 *, const MethodInfo*))Stack_1_get_Count_m16478_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::GetEnumerator()
#define Stack_1_GetEnumerator_m18812(__this, method) (( Enumerator_t3705  (*) (Stack_1_t3181 *, const MethodInfo*))Stack_1_GetEnumerator_m16479_gshared)(__this, method)
