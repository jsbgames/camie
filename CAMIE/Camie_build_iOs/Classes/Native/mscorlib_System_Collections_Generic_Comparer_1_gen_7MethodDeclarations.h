﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct Comparer_1_t3501;
// System.Object
struct Object_t;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.Comparer`1<System.DateTimeOffset>::.ctor()
extern "C" void Comparer_1__ctor_m22491_gshared (Comparer_1_t3501 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m22491(__this, method) (( void (*) (Comparer_1_t3501 *, const MethodInfo*))Comparer_1__ctor_m22491_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.DateTimeOffset>::.cctor()
extern "C" void Comparer_1__cctor_m22492_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m22492(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m22492_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.DateTimeOffset>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m22493_gshared (Comparer_1_t3501 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m22493(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3501 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m22493_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.DateTimeOffset>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.DateTimeOffset>::get_Default()
extern "C" Comparer_1_t3501 * Comparer_1_get_Default_m22494_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m22494(__this /* static, unused */, method) (( Comparer_1_t3501 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m22494_gshared)(__this /* static, unused */, method)
