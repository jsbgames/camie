﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.SByte>
struct InternalEnumerator_1_t3449;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22172_gshared (InternalEnumerator_1_t3449 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22172(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3449 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22172_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22173_gshared (InternalEnumerator_1_t3449 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22173(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3449 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22173_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22174_gshared (InternalEnumerator_1_t3449 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22174(__this, method) (( void (*) (InternalEnumerator_1_t3449 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22174_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22175_gshared (InternalEnumerator_1_t3449 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22175(__this, method) (( bool (*) (InternalEnumerator_1_t3449 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22175_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C" int8_t InternalEnumerator_1_get_Current_m22176_gshared (InternalEnumerator_1_t3449 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22176(__this, method) (( int8_t (*) (InternalEnumerator_1_t3449 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22176_gshared)(__this, method)
