﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t303;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t3563;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t3564;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector2>
struct ICollection_1_t3565;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>
struct ReadOnlyCollection_1_t2915;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t237;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t2919;
// System.Comparison`1<UnityEngine.Vector2>
struct Comparison_1_t2922;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
extern "C" void List_1__ctor_m1597_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1__ctor_m1597(__this, method) (( void (*) (List_1_t303 *, const MethodInfo*))List_1__ctor_m1597_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m14955_gshared (List_1_t303 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m14955(__this, ___collection, method) (( void (*) (List_1_t303 *, Object_t*, const MethodInfo*))List_1__ctor_m14955_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Int32)
extern "C" void List_1__ctor_m14956_gshared (List_1_t303 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m14956(__this, ___capacity, method) (( void (*) (List_1_t303 *, int32_t, const MethodInfo*))List_1__ctor_m14956_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.cctor()
extern "C" void List_1__cctor_m14957_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m14957(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14957_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14958_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14958(__this, method) (( Object_t* (*) (List_1_t303 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14958_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m14959_gshared (List_1_t303 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m14959(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t303 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m14959_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m14960_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m14960(__this, method) (( Object_t * (*) (List_1_t303 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m14960_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m14961_gshared (List_1_t303 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m14961(__this, ___item, method) (( int32_t (*) (List_1_t303 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m14961_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m14962_gshared (List_1_t303 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m14962(__this, ___item, method) (( bool (*) (List_1_t303 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m14962_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m14963_gshared (List_1_t303 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m14963(__this, ___item, method) (( int32_t (*) (List_1_t303 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m14963_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m14964_gshared (List_1_t303 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m14964(__this, ___index, ___item, method) (( void (*) (List_1_t303 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m14964_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m14965_gshared (List_1_t303 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m14965(__this, ___item, method) (( void (*) (List_1_t303 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m14965_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14966_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14966(__this, method) (( bool (*) (List_1_t303 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14966_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m14967_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14967(__this, method) (( bool (*) (List_1_t303 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m14967_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m14968_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m14968(__this, method) (( Object_t * (*) (List_1_t303 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m14968_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m14969_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m14969(__this, method) (( bool (*) (List_1_t303 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m14969_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m14970_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m14970(__this, method) (( bool (*) (List_1_t303 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m14970_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m14971_gshared (List_1_t303 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m14971(__this, ___index, method) (( Object_t * (*) (List_1_t303 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m14971_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m14972_gshared (List_1_t303 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m14972(__this, ___index, ___value, method) (( void (*) (List_1_t303 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m14972_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(T)
extern "C" void List_1_Add_m14973_gshared (List_1_t303 * __this, Vector2_t6  ___item, const MethodInfo* method);
#define List_1_Add_m14973(__this, ___item, method) (( void (*) (List_1_t303 *, Vector2_t6 , const MethodInfo*))List_1_Add_m14973_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m14974_gshared (List_1_t303 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m14974(__this, ___newCount, method) (( void (*) (List_1_t303 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m14974_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m14975_gshared (List_1_t303 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m14975(__this, ___collection, method) (( void (*) (List_1_t303 *, Object_t*, const MethodInfo*))List_1_AddCollection_m14975_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m14976_gshared (List_1_t303 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m14976(__this, ___enumerable, method) (( void (*) (List_1_t303 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m14976_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m14977_gshared (List_1_t303 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m14977(__this, ___collection, method) (( void (*) (List_1_t303 *, Object_t*, const MethodInfo*))List_1_AddRange_m14977_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2915 * List_1_AsReadOnly_m14978_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m14978(__this, method) (( ReadOnlyCollection_1_t2915 * (*) (List_1_t303 *, const MethodInfo*))List_1_AsReadOnly_m14978_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear()
extern "C" void List_1_Clear_m14979_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_Clear_m14979(__this, method) (( void (*) (List_1_t303 *, const MethodInfo*))List_1_Clear_m14979_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Contains(T)
extern "C" bool List_1_Contains_m14980_gshared (List_1_t303 * __this, Vector2_t6  ___item, const MethodInfo* method);
#define List_1_Contains_m14980(__this, ___item, method) (( bool (*) (List_1_t303 *, Vector2_t6 , const MethodInfo*))List_1_Contains_m14980_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m14981_gshared (List_1_t303 * __this, Vector2U5BU5D_t237* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m14981(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t303 *, Vector2U5BU5D_t237*, int32_t, const MethodInfo*))List_1_CopyTo_m14981_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::Find(System.Predicate`1<T>)
extern "C" Vector2_t6  List_1_Find_m14982_gshared (List_1_t303 * __this, Predicate_1_t2919 * ___match, const MethodInfo* method);
#define List_1_Find_m14982(__this, ___match, method) (( Vector2_t6  (*) (List_1_t303 *, Predicate_1_t2919 *, const MethodInfo*))List_1_Find_m14982_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m14983_gshared (Object_t * __this /* static, unused */, Predicate_1_t2919 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m14983(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2919 *, const MethodInfo*))List_1_CheckMatch_m14983_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m14984_gshared (List_1_t303 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2919 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m14984(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t303 *, int32_t, int32_t, Predicate_1_t2919 *, const MethodInfo*))List_1_GetIndex_m14984_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::GetEnumerator()
extern "C" Enumerator_t2913  List_1_GetEnumerator_m14985_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m14985(__this, method) (( Enumerator_t2913  (*) (List_1_t303 *, const MethodInfo*))List_1_GetEnumerator_m14985_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m14986_gshared (List_1_t303 * __this, Vector2_t6  ___item, const MethodInfo* method);
#define List_1_IndexOf_m14986(__this, ___item, method) (( int32_t (*) (List_1_t303 *, Vector2_t6 , const MethodInfo*))List_1_IndexOf_m14986_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m14987_gshared (List_1_t303 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m14987(__this, ___start, ___delta, method) (( void (*) (List_1_t303 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m14987_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m14988_gshared (List_1_t303 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m14988(__this, ___index, method) (( void (*) (List_1_t303 *, int32_t, const MethodInfo*))List_1_CheckIndex_m14988_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m14989_gshared (List_1_t303 * __this, int32_t ___index, Vector2_t6  ___item, const MethodInfo* method);
#define List_1_Insert_m14989(__this, ___index, ___item, method) (( void (*) (List_1_t303 *, int32_t, Vector2_t6 , const MethodInfo*))List_1_Insert_m14989_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m14990_gshared (List_1_t303 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m14990(__this, ___collection, method) (( void (*) (List_1_t303 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m14990_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Remove(T)
extern "C" bool List_1_Remove_m14991_gshared (List_1_t303 * __this, Vector2_t6  ___item, const MethodInfo* method);
#define List_1_Remove_m14991(__this, ___item, method) (( bool (*) (List_1_t303 *, Vector2_t6 , const MethodInfo*))List_1_Remove_m14991_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m14992_gshared (List_1_t303 * __this, Predicate_1_t2919 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m14992(__this, ___match, method) (( int32_t (*) (List_1_t303 *, Predicate_1_t2919 *, const MethodInfo*))List_1_RemoveAll_m14992_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m14993_gshared (List_1_t303 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m14993(__this, ___index, method) (( void (*) (List_1_t303 *, int32_t, const MethodInfo*))List_1_RemoveAt_m14993_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Reverse()
extern "C" void List_1_Reverse_m14994_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_Reverse_m14994(__this, method) (( void (*) (List_1_t303 *, const MethodInfo*))List_1_Reverse_m14994_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort()
extern "C" void List_1_Sort_m14995_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_Sort_m14995(__this, method) (( void (*) (List_1_t303 *, const MethodInfo*))List_1_Sort_m14995_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m14996_gshared (List_1_t303 * __this, Comparison_1_t2922 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m14996(__this, ___comparison, method) (( void (*) (List_1_t303 *, Comparison_1_t2922 *, const MethodInfo*))List_1_Sort_m14996_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
extern "C" Vector2U5BU5D_t237* List_1_ToArray_m14997_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_ToArray_m14997(__this, method) (( Vector2U5BU5D_t237* (*) (List_1_t303 *, const MethodInfo*))List_1_ToArray_m14997_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::TrimExcess()
extern "C" void List_1_TrimExcess_m14998_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m14998(__this, method) (( void (*) (List_1_t303 *, const MethodInfo*))List_1_TrimExcess_m14998_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m14999_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m14999(__this, method) (( int32_t (*) (List_1_t303 *, const MethodInfo*))List_1_get_Capacity_m14999_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m15000_gshared (List_1_t303 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m15000(__this, ___value, method) (( void (*) (List_1_t303 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15000_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
extern "C" int32_t List_1_get_Count_m15001_gshared (List_1_t303 * __this, const MethodInfo* method);
#define List_1_get_Count_m15001(__this, method) (( int32_t (*) (List_1_t303 *, const MethodInfo*))List_1_get_Count_m15001_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C" Vector2_t6  List_1_get_Item_m15002_gshared (List_1_t303 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m15002(__this, ___index, method) (( Vector2_t6  (*) (List_1_t303 *, int32_t, const MethodInfo*))List_1_get_Item_m15002_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m15003_gshared (List_1_t303 * __this, int32_t ___index, Vector2_t6  ___value, const MethodInfo* method);
#define List_1_set_Item_m15003(__this, ___index, ___value, method) (( void (*) (List_1_t303 *, int32_t, Vector2_t6 , const MethodInfo*))List_1_set_Item_m15003_gshared)(__this, ___index, ___value, method)
