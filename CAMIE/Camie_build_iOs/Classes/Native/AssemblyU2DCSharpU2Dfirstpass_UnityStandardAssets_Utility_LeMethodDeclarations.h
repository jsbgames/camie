﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6
struct U3CDoBobCycleU3Ec__Iterator6_t186;
// System.Object
struct Object_t;

// System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::.ctor()
extern "C" void U3CDoBobCycleU3Ec__Iterator6__ctor_m498 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDoBobCycleU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m499 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDoBobCycleU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m500 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::MoveNext()
extern "C" bool U3CDoBobCycleU3Ec__Iterator6_MoveNext_m501 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::Dispose()
extern "C" void U3CDoBobCycleU3Ec__Iterator6_Dispose_m502 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::Reset()
extern "C" void U3CDoBobCycleU3Ec__Iterator6_Reset_m503 (U3CDoBobCycleU3Ec__Iterator6_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
