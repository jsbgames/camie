﻿#pragma once
#include <stdint.h>
// Scr_ShadowTarget
struct Scr_ShadowTarget_t392;
// UnityEngine.GameObject
struct GameObject_t78;
// SCR_Hurt
struct SCR_Hurt_t393;
// UnityEngine.Animator
struct Animator_t9;
// UnityEngine.Transform
struct Transform_t1;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// SCR_Bird
struct  SCR_Bird_t390  : public MonoBehaviour_t3
{
	// Scr_ShadowTarget SCR_Bird::shadow
	Scr_ShadowTarget_t392 * ___shadow_2;
	// UnityEngine.GameObject SCR_Bird::bird
	GameObject_t78 * ___bird_3;
	// SCR_Hurt SCR_Bird::beak
	SCR_Hurt_t393 * ___beak_4;
	// UnityEngine.Animator SCR_Bird::anim
	Animator_t9 * ___anim_5;
	// UnityEngine.Vector3 SCR_Bird::targetPosition
	Vector3_t4  ___targetPosition_6;
	// UnityEngine.Vector3 SCR_Bird::startPosition
	Vector3_t4  ___startPosition_7;
	// UnityEngine.Transform SCR_Bird::camieTransform
	Transform_t1 * ___camieTransform_8;
	// UnityEngine.Vector3 SCR_Bird::camieOffset
	Vector3_t4  ___camieOffset_9;
	// System.Boolean SCR_Bird::attackIsInvoked
	bool ___attackIsInvoked_10;
	// System.Boolean SCR_Bird::attackStarted
	bool ___attackStarted_11;
	// System.Int32 SCR_Bird::attacknb
	int32_t ___attacknb_12;
	// System.Int32 SCR_Bird::maxAttacks
	int32_t ___maxAttacks_13;
	// System.Single SCR_Bird::attaqueApresXSecondes
	float ___attaqueApresXSecondes_14;
	// System.Single SCR_Bird::flySpeed
	float ___flySpeed_15;
};
