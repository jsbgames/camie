﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>
struct Enumerator_t3273;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t905;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__21MethodDeclarations.h"
#define Enumerator__ctor_m20092(__this, ___dictionary, method) (( void (*) (Enumerator_t3273 *, Dictionary_2_t905 *, const MethodInfo*))Enumerator__ctor_m19990_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20093(__this, method) (( Object_t * (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19991_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20094(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19992_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20095(__this, method) (( Object_t * (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20096(__this, method) (( Object_t * (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19994_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::MoveNext()
#define Enumerator_MoveNext_m20097(__this, method) (( bool (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_MoveNext_m19995_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_Current()
#define Enumerator_get_Current_m20098(__this, method) (( KeyValuePair_2_t3270  (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_get_Current_m19996_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m20099(__this, method) (( String_t* (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_get_CurrentKey_m19997_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m20100(__this, method) (( int64_t (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_get_CurrentValue_m19998_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyState()
#define Enumerator_VerifyState_m20101(__this, method) (( void (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_VerifyState_m19999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m20102(__this, method) (( void (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_VerifyCurrent_m20000_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::Dispose()
#define Enumerator_Dispose_m20103(__this, method) (( void (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_Dispose_m20001_gshared)(__this, method)
