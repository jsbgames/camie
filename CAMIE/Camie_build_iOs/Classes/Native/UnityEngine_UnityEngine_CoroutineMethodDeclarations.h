﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Coroutine
struct Coroutine_t559;
struct Coroutine_t559_marshaled;

// System.Void UnityEngine.Coroutine::.ctor()
extern "C" void Coroutine__ctor_m3657 (Coroutine_t559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C" void Coroutine_ReleaseCoroutine_m3658 (Coroutine_t559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::Finalize()
extern "C" void Coroutine_Finalize_m3659 (Coroutine_t559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void Coroutine_t559_marshal(const Coroutine_t559& unmarshaled, Coroutine_t559_marshaled& marshaled);
void Coroutine_t559_marshal_back(const Coroutine_t559_marshaled& marshaled, Coroutine_t559& unmarshaled);
void Coroutine_t559_marshal_cleanup(Coroutine_t559_marshaled& marshaled);
