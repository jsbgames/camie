﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationCurve
struct AnimationCurve_t82;
struct AnimationCurve_t82_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t239;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m804 (AnimationCurve_t82 * __this, KeyframeU5BU5D_t239* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m4541 (AnimationCurve_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m4542 (AnimationCurve_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m4543 (AnimationCurve_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern "C" float AnimationCurve_Evaluate_m806 (AnimationCurve_t82 * __this, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::get_keys()
extern "C" KeyframeU5BU5D_t239* AnimationCurve_get_keys_m877 (AnimationCurve_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Keyframe UnityEngine.AnimationCurve::get_Item(System.Int32)
extern "C" Keyframe_t240  AnimationCurve_get_Item_m879 (AnimationCurve_t82 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimationCurve::get_length()
extern "C" int32_t AnimationCurve_get_length_m878 (AnimationCurve_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Keyframe UnityEngine.AnimationCurve::GetKey_Internal(System.Int32)
extern "C" Keyframe_t240  AnimationCurve_GetKey_Internal_m4544 (AnimationCurve_t82 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
extern "C" KeyframeU5BU5D_t239* AnimationCurve_GetKeys_m4545 (AnimationCurve_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m4546 (AnimationCurve_t82 * __this, KeyframeU5BU5D_t239* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void AnimationCurve_t82_marshal(const AnimationCurve_t82& unmarshaled, AnimationCurve_t82_marshaled& marshaled);
void AnimationCurve_t82_marshal_back(const AnimationCurve_t82_marshaled& marshaled, AnimationCurve_t82& unmarshaled);
void AnimationCurve_t82_marshal_cleanup(AnimationCurve_t82_marshaled& marshaled);
