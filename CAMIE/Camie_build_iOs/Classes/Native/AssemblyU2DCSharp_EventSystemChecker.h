﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t78;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// EventSystemChecker
struct  EventSystemChecker_t309  : public MonoBehaviour_t3
{
	// UnityEngine.GameObject EventSystemChecker::eventSystem
	GameObject_t78 * ___eventSystem_2;
};
