﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Networking.Match.BasicResponse
struct BasicResponse_t904;

// System.Void UnityEngine.Networking.Match.BasicResponse::.ctor()
extern "C" void BasicResponse__ctor_m4661 (BasicResponse_t904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
