﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Reporter/Log>
struct Predicate_1_t2903;
// System.Object
struct Object_t;
// Reporter/Log
struct Log_t291;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<Reporter/Log>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#define Predicate_1__ctor_m14747(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2903 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m14055_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Reporter/Log>::Invoke(T)
#define Predicate_1_Invoke_m14748(__this, ___obj, method) (( bool (*) (Predicate_1_t2903 *, Log_t291 *, const MethodInfo*))Predicate_1_Invoke_m14056_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Reporter/Log>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m14749(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2903 *, Log_t291 *, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m14057_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Reporter/Log>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m14750(__this, ___result, method) (( bool (*) (Predicate_1_t2903 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m14058_gshared)(__this, ___result, method)
