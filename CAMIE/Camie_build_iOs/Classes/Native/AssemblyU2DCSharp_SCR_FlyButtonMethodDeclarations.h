﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_FlyButton
struct SCR_FlyButton_t345;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t215;

// System.Void SCR_FlyButton::.ctor()
extern "C" void SCR_FlyButton__ctor_m1338 (SCR_FlyButton_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FlyButton::Start()
extern "C" void SCR_FlyButton_Start_m1339 (SCR_FlyButton_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FlyButton::Update()
extern "C" void SCR_FlyButton_Update_m1340 (SCR_FlyButton_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FlyButton::OnDisable()
extern "C" void SCR_FlyButton_OnDisable_m1341 (SCR_FlyButton_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FlyButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" void SCR_FlyButton_OnPointerDown_m1342 (SCR_FlyButton_t345 * __this, PointerEventData_t215 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FlyButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" void SCR_FlyButton_OnPointerUp_m1343 (SCR_FlyButton_t345 * __this, PointerEventData_t215 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FlyButton::ShowButton(System.Boolean)
extern "C" void SCR_FlyButton_ShowButton_m1344 (SCR_FlyButton_t345 * __this, bool ___show, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_FlyButton::get_IsPressed()
extern "C" bool SCR_FlyButton_get_IsPressed_m1345 (SCR_FlyButton_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FlyButton::set_IsPressed(System.Boolean)
extern "C" void SCR_FlyButton_set_IsPressed_m1346 (SCR_FlyButton_t345 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
