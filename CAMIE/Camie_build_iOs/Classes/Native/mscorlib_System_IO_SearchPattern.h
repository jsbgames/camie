﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t554;
// System.Object
#include "mscorlib_System_Object.h"
// System.IO.SearchPattern
struct  SearchPattern_t1826  : public Object_t
{
};
struct SearchPattern_t1826_StaticFields{
	// System.Char[] System.IO.SearchPattern::WildcardChars
	CharU5BU5D_t554* ___WildcardChars_0;
	// System.Char[] System.IO.SearchPattern::InvalidChars
	CharU5BU5D_t554* ___InvalidChars_1;
};
