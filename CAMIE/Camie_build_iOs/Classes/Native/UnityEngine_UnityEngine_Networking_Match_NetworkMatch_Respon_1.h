﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.BasicResponse
struct BasicResponse_t904;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.BasicResponse>
struct  ResponseDelegate_1_t1030  : public MulticastDelegate_t549
{
};
