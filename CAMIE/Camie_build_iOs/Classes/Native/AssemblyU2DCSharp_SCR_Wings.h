﻿#pragma once
#include <stdint.h>
// UnityEngine.Renderer
struct Renderer_t154;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_Wings
struct  SCR_Wings_t351  : public MonoBehaviour_t3
{
	// UnityEngine.Renderer SCR_Wings::rend
	Renderer_t154 * ___rend_2;
};
