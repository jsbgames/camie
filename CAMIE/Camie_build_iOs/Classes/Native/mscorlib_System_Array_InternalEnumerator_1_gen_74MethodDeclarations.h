﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>
struct InternalEnumerator_1_t3362;
// System.Object
struct Object_t;
// UnityEngine.RequireComponent
struct RequireComponent_t259;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m21285(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3362 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21286(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3362 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>::Dispose()
#define InternalEnumerator_1_Dispose_m21287(__this, method) (( void (*) (InternalEnumerator_1_t3362 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>::MoveNext()
#define InternalEnumerator_1_MoveNext_m21288(__this, method) (( bool (*) (InternalEnumerator_1_t3362 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>::get_Current()
#define InternalEnumerator_1_get_Current_m21289(__this, method) (( RequireComponent_t259 * (*) (InternalEnumerator_1_t3362 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
