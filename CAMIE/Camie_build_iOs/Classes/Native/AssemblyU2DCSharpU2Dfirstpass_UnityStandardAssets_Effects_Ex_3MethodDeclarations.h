﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2
struct U3COnCollisionEnterU3Ec__Iterator2_t148;
// System.Object
struct Object_t;

// System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::.ctor()
extern "C" void U3COnCollisionEnterU3Ec__Iterator2__ctor_m415 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3COnCollisionEnterU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3COnCollisionEnterU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m417 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::MoveNext()
extern "C" bool U3COnCollisionEnterU3Ec__Iterator2_MoveNext_m418 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::Dispose()
extern "C" void U3COnCollisionEnterU3Ec__Iterator2_Dispose_m419 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::Reset()
extern "C" void U3COnCollisionEnterU3Ec__Iterator2_Reset_m420 (U3COnCollisionEnterU3Ec__Iterator2_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
