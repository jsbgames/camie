﻿#pragma once
#include <stdint.h>
// Reporter
struct Reporter_t295;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ReporterGUI
struct  ReporterGUI_t305  : public MonoBehaviour_t3
{
	// Reporter ReporterGUI::reporter
	Reporter_t295 * ___reporter_2;
};
