﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Images
#include "AssemblyU2DCSharp_Images.h"
#ifndef _MSC_VER
#else
#endif
// Images
#include "AssemblyU2DCSharp_ImagesMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"


// System.Void Images::.ctor()
extern "C" void Images__ctor_m1079 (Images_t288 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// Reporter/_LogType
#include "AssemblyU2DCSharp_Reporter__LogType.h"
#ifndef _MSC_VER
#else
#endif
// Reporter/_LogType
#include "AssemblyU2DCSharp_Reporter__LogTypeMethodDeclarations.h"



// Reporter/Sample
#include "AssemblyU2DCSharp_Reporter_Sample.h"
#ifndef _MSC_VER
#else
#endif
// Reporter/Sample
#include "AssemblyU2DCSharp_Reporter_SampleMethodDeclarations.h"

// System.Single
#include "mscorlib_System_Single.h"


// System.Void Reporter/Sample::.ctor()
extern "C" void Sample__ctor_m1080 (Sample_t290 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Reporter/Sample::MemSize()
extern "C" float Sample_MemSize_m1081 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (13.0f);
		float L_0 = V_0;
		return L_0;
	}
}
// Reporter/Log
#include "AssemblyU2DCSharp_Reporter_Log.h"
#ifndef _MSC_VER
#else
#endif
// Reporter/Log
#include "AssemblyU2DCSharp_Reporter_LogMethodDeclarations.h"

// System.Int32
#include "mscorlib_System_Int32.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"


// System.Void Reporter/Log::.ctor()
extern "C" void Log__ctor_m1082 (Log_t291 * __this, const MethodInfo* method)
{
	{
		__this->___count_0 = 1;
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// Reporter/Log Reporter/Log::CreateCopy()
extern TypeInfo* Log_t291_il2cpp_TypeInfo_var;
extern "C" Log_t291 * Log_CreateCopy_m1083 (Log_t291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Log_t291_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(634);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = Object_MemberwiseClone_m1577(__this, /*hidden argument*/NULL);
		return ((Log_t291 *)Castclass(L_0, Log_t291_il2cpp_TypeInfo_var));
	}
}
// System.Single Reporter/Log::GetMemoryUsage()
extern "C" float Log_GetMemoryUsage_m1084 (Log_t291 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___condition_2);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1578(L_0, /*hidden argument*/NULL);
		String_t* L_2 = (__this->___stacktrace_3);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1578(L_2, /*hidden argument*/NULL);
		return (((float)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)8+(int32_t)((int32_t)((int32_t)L_1*(int32_t)2))))+(int32_t)((int32_t)((int32_t)L_3*(int32_t)2))))+(int32_t)4))));
	}
}
// Reporter/ReportView
#include "AssemblyU2DCSharp_Reporter_ReportView.h"
#ifndef _MSC_VER
#else
#endif
// Reporter/ReportView
#include "AssemblyU2DCSharp_Reporter_ReportViewMethodDeclarations.h"



// Reporter/DetailView
#include "AssemblyU2DCSharp_Reporter_DetailView.h"
#ifndef _MSC_VER
#else
#endif
// Reporter/DetailView
#include "AssemblyU2DCSharp_Reporter_DetailViewMethodDeclarations.h"



// Reporter/<readInfo>c__Iterator0
#include "AssemblyU2DCSharp_Reporter_U3CreadInfoU3Ec__Iterator0.h"
#ifndef _MSC_VER
#else
#endif
// Reporter/<readInfo>c__Iterator0
#include "AssemblyU2DCSharp_Reporter_U3CreadInfoU3Ec__Iterator0MethodDeclarations.h"

// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWW.h"
// Reporter
#include "AssemblyU2DCSharp_Reporter.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// System.IO.Path
#include "mscorlib_System_IO_PathMethodDeclarations.h"
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"


// System.Void Reporter/<readInfo>c__Iterator0::.ctor()
extern "C" void U3CreadInfoU3Ec__Iterator0__ctor_m1085 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Reporter/<readInfo>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CreadInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1086 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Object Reporter/<readInfo>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CreadInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1087 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Boolean Reporter/<readInfo>c__Iterator0::MoveNext()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Path_t401_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t294_il2cpp_TypeInfo_var;
extern "C" bool U3CreadInfoU3Ec__Iterator0_MoveNext_m1088 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Path_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		WWW_t294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U24PC_4);
		V_0 = L_0;
		__this->___U24PC_4 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0104;
		}
	}
	{
		goto IL_0150;
	}

IL_0021:
	{
		__this->___U3CprefFileU3E__0_0 = (String_t*) &_stringLiteral263;
		String_t* L_2 = (__this->___U3CprefFileU3E__0_0);
		__this->___U3CurlU3E__1_1 = L_2;
		String_t* L_3 = (__this->___U3CprefFileU3E__0_0);
		NullCheck(L_3);
		int32_t L_4 = String_IndexOf_m1579(L_3, (String_t*) &_stringLiteral264, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_009a;
		}
	}
	{
		String_t* L_5 = Application_get_streamingAssetsPath_m1580(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CstreamingAssetsPathU3E__2_2 = L_5;
		String_t* L_6 = (__this->___U3CstreamingAssetsPathU3E__2_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_8 = String_op_Equality_m636(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0083;
		}
	}
	{
		String_t* L_9 = Application_get_dataPath_m1581(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m860(NULL /*static, unused*/, L_9, (String_t*) &_stringLiteral265, /*hidden argument*/NULL);
		__this->___U3CstreamingAssetsPathU3E__2_2 = L_10;
	}

IL_0083:
	{
		String_t* L_11 = (__this->___U3CstreamingAssetsPathU3E__2_2);
		String_t* L_12 = (__this->___U3CprefFileU3E__0_0);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t401_il2cpp_TypeInfo_var);
		String_t* L_13 = Path_Combine_m1582(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		__this->___U3CurlU3E__1_1 = L_13;
	}

IL_009a:
	{
		int32_t L_14 = Application_get_platform_m1583(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_14) == ((int32_t)3)))
		{
			goto IL_00db;
		}
	}
	{
		int32_t L_15 = Application_get_platform_m1583(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_15) == ((int32_t)5)))
		{
			goto IL_00db;
		}
	}
	{
		String_t* L_16 = (__this->___U3CurlU3E__1_1);
		NullCheck(L_16);
		bool L_17 = String_Contains_m1584(L_16, (String_t*) &_stringLiteral264, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00db;
		}
	}
	{
		String_t* L_18 = (__this->___U3CurlU3E__1_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral266, L_18, /*hidden argument*/NULL);
		__this->___U3CurlU3E__1_1 = L_19;
	}

IL_00db:
	{
		String_t* L_20 = (__this->___U3CurlU3E__1_1);
		WWW_t294 * L_21 = (WWW_t294 *)il2cpp_codegen_object_new (WWW_t294_il2cpp_TypeInfo_var);
		WWW__ctor_m1585(L_21, L_20, /*hidden argument*/NULL);
		__this->___U3CwwwU3E__3_3 = L_21;
		WWW_t294 * L_22 = (__this->___U3CwwwU3E__3_3);
		__this->___U24current_5 = L_22;
		__this->___U24PC_4 = 1;
		goto IL_0152;
	}

IL_0104:
	{
		WWW_t294 * L_23 = (__this->___U3CwwwU3E__3_3);
		NullCheck(L_23);
		String_t* L_24 = WWW_get_error_m1586(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_IsNullOrEmpty_m1587(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_012e;
		}
	}
	{
		WWW_t294 * L_26 = (__this->___U3CwwwU3E__3_3);
		NullCheck(L_26);
		String_t* L_27 = WWW_get_error_m1586(L_26, /*hidden argument*/NULL);
		Debug_LogError_m735(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		goto IL_0144;
	}

IL_012e:
	{
		Reporter_t295 * L_28 = (__this->___U3CU3Ef__this_6);
		WWW_t294 * L_29 = (__this->___U3CwwwU3E__3_3);
		NullCheck(L_29);
		String_t* L_30 = WWW_get_text_m1588(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		L_28->___buildDate_31 = L_30;
	}

IL_0144:
	{
		goto IL_0150;
	}
	// Dead block : IL_0149: ldarg.0

IL_0150:
	{
		return 0;
	}

IL_0152:
	{
		return 1;
	}
}
// System.Void Reporter/<readInfo>c__Iterator0::Dispose()
extern "C" void U3CreadInfoU3Ec__Iterator0_Dispose_m1089 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_4 = (-1);
		return;
	}
}
// System.Void Reporter/<readInfo>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CreadInfoU3Ec__Iterator0_Reset_m1090 (U3CreadInfoU3Ec__Iterator0_t296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// Reporter
#include "AssemblyU2DCSharp_ReporterMethodDeclarations.h"

// System.Collections.Generic.List`1<Reporter/Sample>
#include "mscorlib_System_Collections_Generic_List_1_gen_3.h"
// System.Collections.Generic.List`1<Reporter/Log>
#include "mscorlib_System_Collections_Generic_List_1_gen_4.h"
// MultiKeyDictionary`3<System.String,System.String,Reporter/Log>
#include "AssemblyU2DCSharp_MultiKeyDictionary_3_gen.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// System.Collections.Generic.List`1<UnityEngine.Vector2>
#include "mscorlib_System_Collections_Generic_List_1_gen_5.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"
// UnityEngine.Application/LogCallback
#include "UnityEngine_UnityEngine_Application_LogCallback.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// UnityEngine.DeviceType
#include "UnityEngine_UnityEngine_DeviceType.h"
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkin.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// UnityEngine.TextClipping
#include "UnityEngine_UnityEngine_TextClipping.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleState.h"
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// UnityEngine.ImagePosition
#include "UnityEngine_UnityEngine_ImagePosition.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_2.h"
// System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_3.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.GUILayoutOption
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
// ReporterGUI
#include "AssemblyU2DCSharp_ReporterGUI.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
// System.Int64
#include "mscorlib_System_Int64.h"
// System.Collections.Generic.List`1<Reporter/Sample>
#include "mscorlib_System_Collections_Generic_List_1_gen_3MethodDeclarations.h"
// System.Collections.Generic.List`1<Reporter/Log>
#include "mscorlib_System_Collections_Generic_List_1_gen_4MethodDeclarations.h"
// MultiKeyDictionary`3<System.String,System.String,Reporter/Log>
#include "AssemblyU2DCSharp_MultiKeyDictionary_3_genMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Vector2>
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Application/LogCallback
#include "UnityEngine_UnityEngine_Application_LogCallbackMethodDeclarations.h"
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_2MethodDeclarations.h"
// System.GC
#include "mscorlib_System_GCMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_3MethodDeclarations.h"
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"
struct Object_t164;
struct GUISkin_t287;
struct Object_t164;
struct Object_t;
// Declaration !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" Object_t * Object_Instantiate_TisObject_t_m953_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Object_Instantiate_TisObject_t_m953(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Object_Instantiate_TisObject_t_m953_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 UnityEngine.Object::Instantiate<UnityEngine.GUISkin>(!!0)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GUISkin>(!!0)
#define Object_Instantiate_TisGUISkin_t287_m1589(__this /* static, unused */, p0, method) (( GUISkin_t287 * (*) (Object_t * /* static, unused */, GUISkin_t287 *, const MethodInfo*))Object_Instantiate_TisObject_t_m953_gshared)(__this /* static, unused */, p0, method)
struct GameObject_t78;
struct ReporterGUI_t305;
struct GameObject_t78;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::GetComponent<System.Object>()
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m771_gshared (GameObject_t78 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m771(__this, method) (( Object_t * (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m771_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponent<ReporterGUI>()
// !!0 UnityEngine.GameObject::GetComponent<ReporterGUI>()
#define GameObject_GetComponent_TisReporterGUI_t305_m1590(__this, method) (( ReporterGUI_t305 * (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m771_gshared)(__this, method)
struct GameObject_t78;
struct ReporterGUI_t305;
struct GameObject_t78;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::AddComponent<System.Object>()
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m975_gshared (GameObject_t78 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m975(__this, method) (( Object_t * (*) (GameObject_t78 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m975_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::AddComponent<ReporterGUI>()
// !!0 UnityEngine.GameObject::AddComponent<ReporterGUI>()
#define GameObject_AddComponent_TisReporterGUI_t305_m1591(__this, method) (( ReporterGUI_t305 * (*) (GameObject_t78 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m975_gshared)(__this, method)


// System.Void Reporter::.ctor()
extern TypeInfo* List_1_t297_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t298_il2cpp_TypeInfo_var;
extern TypeInfo* MultiKeyDictionary_3_t299_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t300_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t301_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t303_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1592_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1593_MethodInfo_var;
extern const MethodInfo* MultiKeyDictionary_3__ctor_m1594_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1595_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1597_MethodInfo_var;
extern "C" void Reporter__ctor_m1091 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t297_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(638);
		List_1_t298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		MultiKeyDictionary_3_t299_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		Dictionary_2_t300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		GUIContent_t301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(642);
		List_1_t303_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(643);
		List_1__ctor_m1592_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483681);
		List_1__ctor_m1593_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483682);
		MultiKeyDictionary_3__ctor_m1594_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483683);
		Dictionary_2__ctor_m1595_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483684);
		List_1__ctor_m1597_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483685);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t297 * L_0 = (List_1_t297 *)il2cpp_codegen_object_new (List_1_t297_il2cpp_TypeInfo_var);
		List_1__ctor_m1592(L_0, ((int32_t)216000), /*hidden argument*/List_1__ctor_m1592_MethodInfo_var);
		__this->___samples_2 = L_0;
		List_1_t298 * L_1 = (List_1_t298 *)il2cpp_codegen_object_new (List_1_t298_il2cpp_TypeInfo_var);
		List_1__ctor_m1593(L_1, /*hidden argument*/List_1__ctor_m1593_MethodInfo_var);
		__this->___logs_3 = L_1;
		List_1_t298 * L_2 = (List_1_t298 *)il2cpp_codegen_object_new (List_1_t298_il2cpp_TypeInfo_var);
		List_1__ctor_m1593(L_2, /*hidden argument*/List_1__ctor_m1593_MethodInfo_var);
		__this->___collapsedLogs_4 = L_2;
		List_1_t298 * L_3 = (List_1_t298 *)il2cpp_codegen_object_new (List_1_t298_il2cpp_TypeInfo_var);
		List_1__ctor_m1593(L_3, /*hidden argument*/List_1__ctor_m1593_MethodInfo_var);
		__this->___currentLog_5 = L_3;
		MultiKeyDictionary_3_t299 * L_4 = (MultiKeyDictionary_3_t299 *)il2cpp_codegen_object_new (MultiKeyDictionary_3_t299_il2cpp_TypeInfo_var);
		MultiKeyDictionary_3__ctor_m1594(L_4, /*hidden argument*/MultiKeyDictionary_3__ctor_m1594_MethodInfo_var);
		__this->___logsDic_6 = L_4;
		Dictionary_2_t300 * L_5 = (Dictionary_2_t300 *)il2cpp_codegen_object_new (Dictionary_2_t300_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1595(L_5, /*hidden argument*/Dictionary_2__ctor_m1595_MethodInfo_var);
		__this->___cachedString_7 = L_5;
		__this->___showLog_16 = 1;
		__this->___showWarning_17 = 1;
		__this->___showError_18 = 1;
		__this->___showClearOnNewSceneLoadedButton_25 = 1;
		__this->___showTimeButton_26 = 1;
		__this->___showSceneButton_27 = 1;
		__this->___showMemButton_28 = 1;
		__this->___showFpsButton_29 = 1;
		__this->___showSearchText_30 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___UserData_36 = L_6;
		__this->___currentView_39 = 1;
		Vector2_t6  L_7 = {0};
		Vector2__ctor_m630(&L_7, (32.0f), (32.0f), /*hidden argument*/NULL);
		__this->___size_82 = L_7;
		__this->___maxSize_83 = (20.0f);
		__this->___numOfCircleToShow_84 = 1;
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___filterText_87 = L_8;
		GUIContent_t301 * L_9 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1596(L_9, /*hidden argument*/NULL);
		__this->___tempContent_123 = L_9;
		__this->___graphSize_127 = (4.0f);
		List_1_t303 * L_10 = (List_1_t303 *)il2cpp_codegen_object_new (List_1_t303_il2cpp_TypeInfo_var);
		List_1__ctor_m1597(L_10, /*hidden argument*/List_1__ctor_m1597_MethodInfo_var);
		__this->___gestureDetector_137 = L_10;
		Vector2_t6  L_11 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___gestureSum_138 = L_11;
		__this->___lastClickTime_141 = (-1.0f);
		List_1_t298 * L_12 = (List_1_t298 *)il2cpp_codegen_object_new (List_1_t298_il2cpp_TypeInfo_var);
		List_1__ctor_m1593(L_12, /*hidden argument*/List_1__ctor_m1593_MethodInfo_var);
		__this->___threadedLogs_148 = L_12;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reporter::.cctor()
extern "C" void Reporter__cctor_m1092 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Single Reporter::get_TotalMemUsage()
extern "C" float Reporter_get_TotalMemUsage_m1093 (Reporter_t295 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___logsMemUsage_33);
		float L_1 = (__this->___graphMemUsage_34);
		return ((float)((float)L_0+(float)L_1));
	}
}
// System.Void Reporter::Awake()
extern "C" void Reporter_Awake_m1094 (Reporter_t295 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___Initialized_94);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Reporter_Initialize_m1098(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void Reporter::OnEnable()
extern "C" void Reporter_OnEnable_m1095 (Reporter_t295 * __this, const MethodInfo* method)
{
	{
		List_1_t298 * L_0 = (__this->___logs_3);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		Reporter_clear_m1101(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Reporter::OnDisable()
extern "C" void Reporter_OnDisable_m1096 (Reporter_t295 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Reporter::addSample()
extern TypeInfo* Sample_t290_il2cpp_TypeInfo_var;
extern "C" void Reporter_addSample_m1097 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Sample_t290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		s_Il2CppMethodIntialized = true;
	}
	Sample_t290 * V_0 = {0};
	{
		Sample_t290 * L_0 = (Sample_t290 *)il2cpp_codegen_object_new (Sample_t290_il2cpp_TypeInfo_var);
		Sample__ctor_m1080(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Sample_t290 * L_1 = V_0;
		float L_2 = (__this->___fps_37);
		NullCheck(L_1);
		L_1->___fps_3 = L_2;
		Sample_t290 * L_3 = V_0;
		String_t* L_4 = (__this->___fpsText_38);
		NullCheck(L_3);
		L_3->___fpsText_4 = L_4;
		Sample_t290 * L_5 = V_0;
		int32_t L_6 = Application_get_loadedLevel_m1012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->___loadedScene_1 = (((uint8_t)L_6));
		Sample_t290 * L_7 = V_0;
		float L_8 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->___time_0 = L_8;
		Sample_t290 * L_9 = V_0;
		float L_10 = (__this->___gcTotalMemory_35);
		NullCheck(L_9);
		L_9->___memory_2 = L_10;
		List_1_t297 * L_11 = (__this->___samples_2);
		Sample_t290 * L_12 = V_0;
		NullCheck(L_11);
		VirtActionInvoker1< Sample_t290 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Reporter/Sample>::Add(!0) */, L_11, L_12);
		List_1_t297 * L_13 = (__this->___samples_2);
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::get_Count() */, L_13);
		float L_15 = Sample_MemSize_m1081(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___graphMemUsage_34 = ((float)((float)((float)((float)((float)((float)(((float)L_14))*(float)L_15))/(float)(1024.0f)))/(float)(1024.0f)));
		return;
	}
}
// System.Void Reporter::Initialize()
extern TypeInfo* Reporter_t295_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t243_il2cpp_TypeInfo_var;
extern TypeInfo* LogCallback_t402_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t301_il2cpp_TypeInfo_var;
extern TypeInfo* DeviceType_t403_il2cpp_TypeInfo_var;
extern const MethodInfo* Reporter_CaptureLogThread_m1120_MethodInfo_var;
extern "C" void Reporter_Initialize_m1098 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Reporter_t295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		StringU5BU5D_t243_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		LogCallback_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(645);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		GUIContent_t301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(642);
		DeviceType_t403_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(646);
		Reporter_CaptureLogThread_m1120_MethodInfo_var = il2cpp_codegen_method_info_from_index(38);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t232 * V_0 = {0};
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Reporter_t295 * G_B7_0 = {0};
	Reporter_t295 * G_B6_0 = {0};
	int32_t G_B8_0 = 0;
	Reporter_t295 * G_B8_1 = {0};
	Reporter_t295 * G_B10_0 = {0};
	Reporter_t295 * G_B9_0 = {0};
	int32_t G_B11_0 = 0;
	Reporter_t295 * G_B11_1 = {0};
	Reporter_t295 * G_B13_0 = {0};
	Reporter_t295 * G_B12_0 = {0};
	int32_t G_B14_0 = 0;
	Reporter_t295 * G_B14_1 = {0};
	Reporter_t295 * G_B16_0 = {0};
	Reporter_t295 * G_B15_0 = {0};
	int32_t G_B17_0 = 0;
	Reporter_t295 * G_B17_1 = {0};
	Reporter_t295 * G_B19_0 = {0};
	Reporter_t295 * G_B18_0 = {0};
	int32_t G_B20_0 = 0;
	Reporter_t295 * G_B20_1 = {0};
	Reporter_t295 * G_B22_0 = {0};
	Reporter_t295 * G_B21_0 = {0};
	int32_t G_B23_0 = 0;
	Reporter_t295 * G_B23_1 = {0};
	Reporter_t295 * G_B25_0 = {0};
	Reporter_t295 * G_B24_0 = {0};
	int32_t G_B26_0 = 0;
	Reporter_t295 * G_B26_1 = {0};
	Reporter_t295 * G_B28_0 = {0};
	Reporter_t295 * G_B27_0 = {0};
	int32_t G_B29_0 = 0;
	Reporter_t295 * G_B29_1 = {0};
	Reporter_t295 * G_B31_0 = {0};
	Reporter_t295 * G_B30_0 = {0};
	int32_t G_B32_0 = 0;
	Reporter_t295 * G_B32_1 = {0};
	Reporter_t295 * G_B34_0 = {0};
	Reporter_t295 * G_B33_0 = {0};
	int32_t G_B35_0 = 0;
	Reporter_t295 * G_B35_1 = {0};
	Reporter_t295 * G_B37_0 = {0};
	Reporter_t295 * G_B36_0 = {0};
	int32_t G_B38_0 = 0;
	Reporter_t295 * G_B38_1 = {0};
	Reporter_t295 * G_B40_0 = {0};
	Reporter_t295 * G_B39_0 = {0};
	int32_t G_B41_0 = 0;
	Reporter_t295 * G_B41_1 = {0};
	Reporter_t295 * G_B43_0 = {0};
	Reporter_t295 * G_B42_0 = {0};
	int32_t G_B44_0 = 0;
	Reporter_t295 * G_B44_1 = {0};
	Reporter_t295 * G_B46_0 = {0};
	Reporter_t295 * G_B45_0 = {0};
	int32_t G_B47_0 = 0;
	Reporter_t295 * G_B47_1 = {0};
	Reporter_t295 * G_B49_0 = {0};
	Reporter_t295 * G_B48_0 = {0};
	int32_t G_B50_0 = 0;
	Reporter_t295 * G_B50_1 = {0};
	Reporter_t295 * G_B52_0 = {0};
	Reporter_t295 * G_B51_0 = {0};
	int32_t G_B53_0 = 0;
	Reporter_t295 * G_B53_1 = {0};
	Reporter_t295 * G_B55_0 = {0};
	Reporter_t295 * G_B54_0 = {0};
	int32_t G_B56_0 = 0;
	Reporter_t295 * G_B56_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Reporter_t295_il2cpp_TypeInfo_var);
		bool L_0 = ((Reporter_t295_StaticFields*)Reporter_t295_il2cpp_TypeInfo_var->static_fields)->___created_40;
		if (L_0)
		{
			goto IL_006d;
		}
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		GameObject_t78 * L_1 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SendMessage_m1599(L_1, (String_t*) &_stringLiteral196, /*hidden argument*/NULL);
		goto IL_002b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t232 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t232 *)__exception_local);
		Exception_t232 * L_2 = V_0;
		Debug_LogException_m1600(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		int32_t L_3 = Application_get_levelCount_m1601(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___scenes_85 = ((StringU5BU5D_t243*)SZArrayNew(StringU5BU5D_t243_il2cpp_TypeInfo_var, L_3));
		String_t* L_4 = Application_get_loadedLevelName_m637(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___currentScene_86 = L_4;
		GameObject_t78 * L_5 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m1602(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6 = { (void*)Reporter_CaptureLogThread_m1120_MethodInfo_var };
		LogCallback_t402 * L_7 = (LogCallback_t402 *)il2cpp_codegen_object_new (LogCallback_t402_il2cpp_TypeInfo_var);
		LogCallback__ctor_m1603(L_7, __this, L_6, /*hidden argument*/NULL);
		Application_add_logMessageReceivedThreaded_m1604(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Reporter_t295_il2cpp_TypeInfo_var);
		((Reporter_t295_StaticFields*)Reporter_t295_il2cpp_TypeInfo_var->static_fields)->___created_40 = 1;
		goto IL_0084;
	}

IL_006d:
	{
		Debug_LogWarning_m816(NULL /*static, unused*/, (String_t*) &_stringLiteral197, /*hidden argument*/NULL);
		GameObject_t78 * L_8 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_DestroyImmediate_m1605(NULL /*static, unused*/, L_8, 1, /*hidden argument*/NULL);
		return;
	}

IL_0084:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_10 = (__this->___images_41);
		NullCheck(L_10);
		Texture2D_t63 * L_11 = (L_10->___clearImage_0);
		GUIContent_t301 * L_12 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_12, L_9, L_11, (String_t*) &_stringLiteral198, /*hidden argument*/NULL);
		__this->___clearContent_42 = L_12;
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_14 = (__this->___images_41);
		NullCheck(L_14);
		Texture2D_t63 * L_15 = (L_14->___collapseImage_1);
		GUIContent_t301 * L_16 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_16, L_13, L_15, (String_t*) &_stringLiteral199, /*hidden argument*/NULL);
		__this->___collapseContent_43 = L_16;
		String_t* L_17 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_18 = (__this->___images_41);
		NullCheck(L_18);
		Texture2D_t63 * L_19 = (L_18->___clearOnNewSceneImage_2);
		GUIContent_t301 * L_20 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_20, L_17, L_19, (String_t*) &_stringLiteral200, /*hidden argument*/NULL);
		__this->___clearOnNewSceneContent_44 = L_20;
		String_t* L_21 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_22 = (__this->___images_41);
		NullCheck(L_22);
		Texture2D_t63 * L_23 = (L_22->___showTimeImage_3);
		GUIContent_t301 * L_24 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_24, L_21, L_23, (String_t*) &_stringLiteral201, /*hidden argument*/NULL);
		__this->___showTimeContent_45 = L_24;
		String_t* L_25 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_26 = (__this->___images_41);
		NullCheck(L_26);
		Texture2D_t63 * L_27 = (L_26->___showSceneImage_4);
		GUIContent_t301 * L_28 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_28, L_25, L_27, (String_t*) &_stringLiteral202, /*hidden argument*/NULL);
		__this->___showSceneContent_46 = L_28;
		String_t* L_29 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_30 = (__this->___images_41);
		NullCheck(L_30);
		Texture2D_t63 * L_31 = (L_30->___showMemoryImage_6);
		GUIContent_t301 * L_32 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_32, L_29, L_31, (String_t*) &_stringLiteral203, /*hidden argument*/NULL);
		__this->___showMemoryContent_48 = L_32;
		String_t* L_33 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_34 = (__this->___images_41);
		NullCheck(L_34);
		Texture2D_t63 * L_35 = (L_34->___softwareImage_7);
		GUIContent_t301 * L_36 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_36, L_33, L_35, (String_t*) &_stringLiteral204, /*hidden argument*/NULL);
		__this->___softwareContent_49 = L_36;
		String_t* L_37 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_38 = (__this->___images_41);
		NullCheck(L_38);
		Texture2D_t63 * L_39 = (L_38->___dateImage_8);
		GUIContent_t301 * L_40 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_40, L_37, L_39, (String_t*) &_stringLiteral205, /*hidden argument*/NULL);
		__this->___dateContent_50 = L_40;
		String_t* L_41 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_42 = (__this->___images_41);
		NullCheck(L_42);
		Texture2D_t63 * L_43 = (L_42->___showFpsImage_9);
		GUIContent_t301 * L_44 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_44, L_41, L_43, (String_t*) &_stringLiteral206, /*hidden argument*/NULL);
		__this->___showFpsContent_51 = L_44;
		String_t* L_45 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_46 = (__this->___images_41);
		NullCheck(L_46);
		Texture2D_t63 * L_47 = (L_46->___showGraphImage_10);
		GUIContent_t301 * L_48 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_48, L_45, L_47, (String_t*) &_stringLiteral207, /*hidden argument*/NULL);
		__this->___graphContent_52 = L_48;
		String_t* L_49 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_50 = (__this->___images_41);
		NullCheck(L_50);
		Texture2D_t63 * L_51 = (L_50->___infoImage_12);
		GUIContent_t301 * L_52 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_52, L_49, L_51, (String_t*) &_stringLiteral208, /*hidden argument*/NULL);
		__this->___infoContent_53 = L_52;
		String_t* L_53 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_54 = (__this->___images_41);
		NullCheck(L_54);
		Texture2D_t63 * L_55 = (L_54->___searchImage_13);
		GUIContent_t301 * L_56 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_56, L_53, L_55, (String_t*) &_stringLiteral209, /*hidden argument*/NULL);
		__this->___searchContent_54 = L_56;
		String_t* L_57 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_58 = (__this->___images_41);
		NullCheck(L_58);
		Texture2D_t63 * L_59 = (L_58->___closeImage_14);
		GUIContent_t301 * L_60 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_60, L_57, L_59, (String_t*) &_stringLiteral210, /*hidden argument*/NULL);
		__this->___closeContent_55 = L_60;
		String_t* L_61 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_62 = (__this->___images_41);
		NullCheck(L_62);
		Texture2D_t63 * L_63 = (L_62->___userImage_5);
		GUIContent_t301 * L_64 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_64, L_61, L_63, (String_t*) &_stringLiteral211, /*hidden argument*/NULL);
		__this->___userContent_47 = L_64;
		String_t* L_65 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_66 = (__this->___images_41);
		NullCheck(L_66);
		Texture2D_t63 * L_67 = (L_66->___buildFromImage_15);
		GUIContent_t301 * L_68 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_68, L_65, L_67, (String_t*) &_stringLiteral212, /*hidden argument*/NULL);
		__this->___buildFromContent_56 = L_68;
		String_t* L_69 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_70 = (__this->___images_41);
		NullCheck(L_70);
		Texture2D_t63 * L_71 = (L_70->___systemInfoImage_16);
		GUIContent_t301 * L_72 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_72, L_69, L_71, (String_t*) &_stringLiteral213, /*hidden argument*/NULL);
		__this->___systemInfoContent_57 = L_72;
		String_t* L_73 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_74 = (__this->___images_41);
		NullCheck(L_74);
		Texture2D_t63 * L_75 = (L_74->___graphicsInfoImage_17);
		GUIContent_t301 * L_76 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_76, L_73, L_75, (String_t*) &_stringLiteral214, /*hidden argument*/NULL);
		__this->___graphicsInfoContent_58 = L_76;
		String_t* L_77 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_78 = (__this->___images_41);
		NullCheck(L_78);
		Texture2D_t63 * L_79 = (L_78->___backImage_18);
		GUIContent_t301 * L_80 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_80, L_77, L_79, (String_t*) &_stringLiteral215, /*hidden argument*/NULL);
		__this->___backContent_59 = L_80;
		String_t* L_81 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_82 = (__this->___images_41);
		NullCheck(L_82);
		Texture2D_t63 * L_83 = (L_82->___cameraImage_19);
		GUIContent_t301 * L_84 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_84, L_81, L_83, (String_t*) &_stringLiteral216, /*hidden argument*/NULL);
		__this->___cameraContent_60 = L_84;
		String_t* L_85 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_86 = (__this->___images_41);
		NullCheck(L_86);
		Texture2D_t63 * L_87 = (L_86->___logImage_20);
		GUIContent_t301 * L_88 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_88, L_85, L_87, (String_t*) &_stringLiteral217, /*hidden argument*/NULL);
		__this->___logContent_61 = L_88;
		String_t* L_89 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_90 = (__this->___images_41);
		NullCheck(L_90);
		Texture2D_t63 * L_91 = (L_90->___warningImage_21);
		GUIContent_t301 * L_92 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_92, L_89, L_91, (String_t*) &_stringLiteral218, /*hidden argument*/NULL);
		__this->___warningContent_62 = L_92;
		String_t* L_93 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Images_t288 * L_94 = (__this->___images_41);
		NullCheck(L_94);
		Texture2D_t63 * L_95 = (L_94->___errorImage_22);
		GUIContent_t301 * L_96 = (GUIContent_t301 *)il2cpp_codegen_object_new (GUIContent_t301_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1606(L_96, L_93, L_95, (String_t*) &_stringLiteral219, /*hidden argument*/NULL);
		__this->___errorContent_63 = L_96;
		int32_t L_97 = PlayerPrefs_GetInt_m1607(NULL /*static, unused*/, (String_t*) &_stringLiteral220, 1, /*hidden argument*/NULL);
		__this->___currentView_39 = L_97;
		int32_t L_98 = PlayerPrefs_GetInt_m1608(NULL /*static, unused*/, (String_t*) &_stringLiteral221, /*hidden argument*/NULL);
		G_B6_0 = __this;
		if ((!(((uint32_t)L_98) == ((uint32_t)1))))
		{
			G_B7_0 = __this;
			goto IL_036c;
		}
	}
	{
		G_B8_0 = 1;
		G_B8_1 = G_B6_0;
		goto IL_036d;
	}

IL_036c:
	{
		G_B8_0 = 0;
		G_B8_1 = G_B7_0;
	}

IL_036d:
	{
		NullCheck(G_B8_1);
		G_B8_1->___show_8 = G_B8_0;
		int32_t L_99 = PlayerPrefs_GetInt_m1608(NULL /*static, unused*/, (String_t*) &_stringLiteral222, /*hidden argument*/NULL);
		G_B9_0 = __this;
		if ((!(((uint32_t)L_99) == ((uint32_t)1))))
		{
			G_B10_0 = __this;
			goto IL_0389;
		}
	}
	{
		G_B11_0 = 1;
		G_B11_1 = G_B9_0;
		goto IL_038a;
	}

IL_0389:
	{
		G_B11_0 = 0;
		G_B11_1 = G_B10_0;
	}

IL_038a:
	{
		NullCheck(G_B11_1);
		G_B11_1->___collapse_9 = G_B11_0;
		int32_t L_100 = PlayerPrefs_GetInt_m1608(NULL /*static, unused*/, (String_t*) &_stringLiteral223, /*hidden argument*/NULL);
		G_B12_0 = __this;
		if ((!(((uint32_t)L_100) == ((uint32_t)1))))
		{
			G_B13_0 = __this;
			goto IL_03a6;
		}
	}
	{
		G_B14_0 = 1;
		G_B14_1 = G_B12_0;
		goto IL_03a7;
	}

IL_03a6:
	{
		G_B14_0 = 0;
		G_B14_1 = G_B13_0;
	}

IL_03a7:
	{
		NullCheck(G_B14_1);
		G_B14_1->___clearOnNewSceneLoaded_10 = G_B14_0;
		int32_t L_101 = PlayerPrefs_GetInt_m1608(NULL /*static, unused*/, (String_t*) &_stringLiteral224, /*hidden argument*/NULL);
		G_B15_0 = __this;
		if ((!(((uint32_t)L_101) == ((uint32_t)1))))
		{
			G_B16_0 = __this;
			goto IL_03c3;
		}
	}
	{
		G_B17_0 = 1;
		G_B17_1 = G_B15_0;
		goto IL_03c4;
	}

IL_03c3:
	{
		G_B17_0 = 0;
		G_B17_1 = G_B16_0;
	}

IL_03c4:
	{
		NullCheck(G_B17_1);
		G_B17_1->___showTime_11 = G_B17_0;
		int32_t L_102 = PlayerPrefs_GetInt_m1608(NULL /*static, unused*/, (String_t*) &_stringLiteral225, /*hidden argument*/NULL);
		G_B18_0 = __this;
		if ((!(((uint32_t)L_102) == ((uint32_t)1))))
		{
			G_B19_0 = __this;
			goto IL_03e0;
		}
	}
	{
		G_B20_0 = 1;
		G_B20_1 = G_B18_0;
		goto IL_03e1;
	}

IL_03e0:
	{
		G_B20_0 = 0;
		G_B20_1 = G_B19_0;
	}

IL_03e1:
	{
		NullCheck(G_B20_1);
		G_B20_1->___showScene_12 = G_B20_0;
		int32_t L_103 = PlayerPrefs_GetInt_m1608(NULL /*static, unused*/, (String_t*) &_stringLiteral226, /*hidden argument*/NULL);
		G_B21_0 = __this;
		if ((!(((uint32_t)L_103) == ((uint32_t)1))))
		{
			G_B22_0 = __this;
			goto IL_03fd;
		}
	}
	{
		G_B23_0 = 1;
		G_B23_1 = G_B21_0;
		goto IL_03fe;
	}

IL_03fd:
	{
		G_B23_0 = 0;
		G_B23_1 = G_B22_0;
	}

IL_03fe:
	{
		NullCheck(G_B23_1);
		G_B23_1->___showMemory_13 = G_B23_0;
		int32_t L_104 = PlayerPrefs_GetInt_m1608(NULL /*static, unused*/, (String_t*) &_stringLiteral227, /*hidden argument*/NULL);
		G_B24_0 = __this;
		if ((!(((uint32_t)L_104) == ((uint32_t)1))))
		{
			G_B25_0 = __this;
			goto IL_041a;
		}
	}
	{
		G_B26_0 = 1;
		G_B26_1 = G_B24_0;
		goto IL_041b;
	}

IL_041a:
	{
		G_B26_0 = 0;
		G_B26_1 = G_B25_0;
	}

IL_041b:
	{
		NullCheck(G_B26_1);
		G_B26_1->___showFps_14 = G_B26_0;
		int32_t L_105 = PlayerPrefs_GetInt_m1608(NULL /*static, unused*/, (String_t*) &_stringLiteral228, /*hidden argument*/NULL);
		G_B27_0 = __this;
		if ((!(((uint32_t)L_105) == ((uint32_t)1))))
		{
			G_B28_0 = __this;
			goto IL_0437;
		}
	}
	{
		G_B29_0 = 1;
		G_B29_1 = G_B27_0;
		goto IL_0438;
	}

IL_0437:
	{
		G_B29_0 = 0;
		G_B29_1 = G_B28_0;
	}

IL_0438:
	{
		NullCheck(G_B29_1);
		G_B29_1->___showGraph_15 = G_B29_0;
		int32_t L_106 = PlayerPrefs_GetInt_m1607(NULL /*static, unused*/, (String_t*) &_stringLiteral229, 1, /*hidden argument*/NULL);
		G_B30_0 = __this;
		if ((!(((uint32_t)L_106) == ((uint32_t)1))))
		{
			G_B31_0 = __this;
			goto IL_0455;
		}
	}
	{
		G_B32_0 = 1;
		G_B32_1 = G_B30_0;
		goto IL_0456;
	}

IL_0455:
	{
		G_B32_0 = 0;
		G_B32_1 = G_B31_0;
	}

IL_0456:
	{
		NullCheck(G_B32_1);
		G_B32_1->___showLog_16 = G_B32_0;
		int32_t L_107 = PlayerPrefs_GetInt_m1607(NULL /*static, unused*/, (String_t*) &_stringLiteral230, 1, /*hidden argument*/NULL);
		G_B33_0 = __this;
		if ((!(((uint32_t)L_107) == ((uint32_t)1))))
		{
			G_B34_0 = __this;
			goto IL_0473;
		}
	}
	{
		G_B35_0 = 1;
		G_B35_1 = G_B33_0;
		goto IL_0474;
	}

IL_0473:
	{
		G_B35_0 = 0;
		G_B35_1 = G_B34_0;
	}

IL_0474:
	{
		NullCheck(G_B35_1);
		G_B35_1->___showWarning_17 = G_B35_0;
		int32_t L_108 = PlayerPrefs_GetInt_m1607(NULL /*static, unused*/, (String_t*) &_stringLiteral231, 1, /*hidden argument*/NULL);
		G_B36_0 = __this;
		if ((!(((uint32_t)L_108) == ((uint32_t)1))))
		{
			G_B37_0 = __this;
			goto IL_0491;
		}
	}
	{
		G_B38_0 = 1;
		G_B38_1 = G_B36_0;
		goto IL_0492;
	}

IL_0491:
	{
		G_B38_0 = 0;
		G_B38_1 = G_B37_0;
	}

IL_0492:
	{
		NullCheck(G_B38_1);
		G_B38_1->___showError_18 = G_B38_0;
		String_t* L_109 = PlayerPrefs_GetString_m1609(NULL /*static, unused*/, (String_t*) &_stringLiteral232, /*hidden argument*/NULL);
		__this->___filterText_87 = L_109;
		Vector2_t6 * L_110 = &(__this->___size_82);
		Vector2_t6 * L_111 = &(__this->___size_82);
		float L_112 = PlayerPrefs_GetFloat_m1610(NULL /*static, unused*/, (String_t*) &_stringLiteral233, (32.0f), /*hidden argument*/NULL);
		float L_113 = L_112;
		V_1 = L_113;
		L_111->___y_2 = L_113;
		float L_114 = V_1;
		L_110->___x_1 = L_114;
		int32_t L_115 = PlayerPrefs_GetInt_m1607(NULL /*static, unused*/, (String_t*) &_stringLiteral234, 1, /*hidden argument*/NULL);
		G_B39_0 = __this;
		if ((!(((uint32_t)L_115) == ((uint32_t)1))))
		{
			G_B40_0 = __this;
			goto IL_04e7;
		}
	}
	{
		G_B41_0 = 1;
		G_B41_1 = G_B39_0;
		goto IL_04e8;
	}

IL_04e7:
	{
		G_B41_0 = 0;
		G_B41_1 = G_B40_0;
	}

IL_04e8:
	{
		NullCheck(G_B41_1);
		G_B41_1->___showClearOnNewSceneLoadedButton_25 = G_B41_0;
		int32_t L_116 = PlayerPrefs_GetInt_m1607(NULL /*static, unused*/, (String_t*) &_stringLiteral235, 1, /*hidden argument*/NULL);
		G_B42_0 = __this;
		if ((!(((uint32_t)L_116) == ((uint32_t)1))))
		{
			G_B43_0 = __this;
			goto IL_0505;
		}
	}
	{
		G_B44_0 = 1;
		G_B44_1 = G_B42_0;
		goto IL_0506;
	}

IL_0505:
	{
		G_B44_0 = 0;
		G_B44_1 = G_B43_0;
	}

IL_0506:
	{
		NullCheck(G_B44_1);
		G_B44_1->___showTimeButton_26 = G_B44_0;
		int32_t L_117 = PlayerPrefs_GetInt_m1607(NULL /*static, unused*/, (String_t*) &_stringLiteral236, 1, /*hidden argument*/NULL);
		G_B45_0 = __this;
		if ((!(((uint32_t)L_117) == ((uint32_t)1))))
		{
			G_B46_0 = __this;
			goto IL_0523;
		}
	}
	{
		G_B47_0 = 1;
		G_B47_1 = G_B45_0;
		goto IL_0524;
	}

IL_0523:
	{
		G_B47_0 = 0;
		G_B47_1 = G_B46_0;
	}

IL_0524:
	{
		NullCheck(G_B47_1);
		G_B47_1->___showSceneButton_27 = G_B47_0;
		int32_t L_118 = PlayerPrefs_GetInt_m1607(NULL /*static, unused*/, (String_t*) &_stringLiteral237, 1, /*hidden argument*/NULL);
		G_B48_0 = __this;
		if ((!(((uint32_t)L_118) == ((uint32_t)1))))
		{
			G_B49_0 = __this;
			goto IL_0541;
		}
	}
	{
		G_B50_0 = 1;
		G_B50_1 = G_B48_0;
		goto IL_0542;
	}

IL_0541:
	{
		G_B50_0 = 0;
		G_B50_1 = G_B49_0;
	}

IL_0542:
	{
		NullCheck(G_B50_1);
		G_B50_1->___showMemButton_28 = G_B50_0;
		int32_t L_119 = PlayerPrefs_GetInt_m1607(NULL /*static, unused*/, (String_t*) &_stringLiteral238, 1, /*hidden argument*/NULL);
		G_B51_0 = __this;
		if ((!(((uint32_t)L_119) == ((uint32_t)1))))
		{
			G_B52_0 = __this;
			goto IL_055f;
		}
	}
	{
		G_B53_0 = 1;
		G_B53_1 = G_B51_0;
		goto IL_0560;
	}

IL_055f:
	{
		G_B53_0 = 0;
		G_B53_1 = G_B52_0;
	}

IL_0560:
	{
		NullCheck(G_B53_1);
		G_B53_1->___showFpsButton_29 = G_B53_0;
		int32_t L_120 = PlayerPrefs_GetInt_m1607(NULL /*static, unused*/, (String_t*) &_stringLiteral239, 1, /*hidden argument*/NULL);
		G_B54_0 = __this;
		if ((!(((uint32_t)L_120) == ((uint32_t)1))))
		{
			G_B55_0 = __this;
			goto IL_057d;
		}
	}
	{
		G_B56_0 = 1;
		G_B56_1 = G_B54_0;
		goto IL_057e;
	}

IL_057d:
	{
		G_B56_0 = 0;
		G_B56_1 = G_B55_0;
	}

IL_057e:
	{
		NullCheck(G_B56_1);
		G_B56_1->___showSearchText_30 = G_B56_0;
		Reporter_initializeStyle_m1099(__this, /*hidden argument*/NULL);
		__this->___Initialized_94 = 1;
		bool L_121 = (__this->___show_8);
		if (!L_121)
		{
			goto IL_05a1;
		}
	}
	{
		Reporter_doShow_m1116(__this, /*hidden argument*/NULL);
	}

IL_05a1:
	{
		String_t* L_122 = SystemInfo_get_deviceModel_m1611(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_122);
		String_t* L_123 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.String::ToString() */, L_122);
		__this->___deviceModel_88 = L_123;
		int32_t L_124 = SystemInfo_get_deviceType_m1612(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_125 = L_124;
		Object_t * L_126 = Box(DeviceType_t403_il2cpp_TypeInfo_var, &L_125);
		NullCheck(L_126);
		String_t* L_127 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_126);
		__this->___deviceType_89 = L_127;
		String_t* L_128 = SystemInfo_get_deviceName_m1613(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_128);
		String_t* L_129 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.String::ToString() */, L_128);
		__this->___deviceName_90 = L_129;
		int32_t L_130 = SystemInfo_get_graphicsMemorySize_m1614(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_130;
		String_t* L_131 = Int32_ToString_m1615((&V_2), /*hidden argument*/NULL);
		__this->___graphicsMemorySize_91 = L_131;
		int32_t L_132 = SystemInfo_get_maxTextureSize_m1616(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_132;
		String_t* L_133 = Int32_ToString_m1615((&V_3), /*hidden argument*/NULL);
		__this->___maxTextureSize_92 = L_133;
		int32_t L_134 = SystemInfo_get_systemMemorySize_m1617(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_134;
		String_t* L_135 = Int32_ToString_m1615((&V_4), /*hidden argument*/NULL);
		__this->___systemMemorySize_93 = L_135;
		return;
	}
}
// System.Void Reporter::initializeStyle()
extern TypeInfo* GUIStyle_t302_il2cpp_TypeInfo_var;
extern TypeInfo* RectOffset_t404_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGUISkin_t287_m1589_MethodInfo_var;
extern "C" void Reporter_initializeStyle_m1099 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t302_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(648);
		RectOffset_t404_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(649);
		Object_Instantiate_TisGUISkin_t287_m1589_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483687);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GUISkin_t287 * V_2 = {0};
	{
		Vector2_t6 * L_0 = &(__this->___size_82);
		float L_1 = (L_0->___x_1);
		V_0 = (((int32_t)((float)((float)L_1*(float)(0.2f)))));
		Vector2_t6 * L_2 = &(__this->___size_82);
		float L_3 = (L_2->___y_2);
		V_1 = (((int32_t)((float)((float)L_3*(float)(0.2f)))));
		GUIStyle_t302 * L_4 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_4, /*hidden argument*/NULL);
		__this->___nonStyle_66 = L_4;
		GUIStyle_t302 * L_5 = (__this->___nonStyle_66);
		NullCheck(L_5);
		GUIStyle_set_clipping_m1619(L_5, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_6 = (__this->___nonStyle_66);
		RectOffset_t404 * L_7 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m1620(L_7, 0, 0, 0, 0, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyle_set_border_m1621(L_6, L_7, /*hidden argument*/NULL);
		GUIStyle_t302 * L_8 = (__this->___nonStyle_66);
		NullCheck(L_8);
		GUIStyleState_t405 * L_9 = GUIStyle_get_normal_m1622(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GUIStyleState_set_background_m1623(L_9, (Texture2D_t63 *)NULL, /*hidden argument*/NULL);
		GUIStyle_t302 * L_10 = (__this->___nonStyle_66);
		Vector2_t6 * L_11 = &(__this->___size_82);
		float L_12 = (L_11->___y_2);
		NullCheck(L_10);
		GUIStyle_set_fontSize_m1624(L_10, (((int32_t)((float)((float)L_12/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_13 = (__this->___nonStyle_66);
		NullCheck(L_13);
		GUIStyle_set_alignment_m1625(L_13, 4, /*hidden argument*/NULL);
		GUIStyle_t302 * L_14 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_14, /*hidden argument*/NULL);
		__this->___lowerLeftFontStyle_67 = L_14;
		GUIStyle_t302 * L_15 = (__this->___lowerLeftFontStyle_67);
		NullCheck(L_15);
		GUIStyle_set_clipping_m1619(L_15, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_16 = (__this->___lowerLeftFontStyle_67);
		RectOffset_t404 * L_17 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m1620(L_17, 0, 0, 0, 0, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIStyle_set_border_m1621(L_16, L_17, /*hidden argument*/NULL);
		GUIStyle_t302 * L_18 = (__this->___lowerLeftFontStyle_67);
		NullCheck(L_18);
		GUIStyleState_t405 * L_19 = GUIStyle_get_normal_m1622(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		GUIStyleState_set_background_m1623(L_19, (Texture2D_t63 *)NULL, /*hidden argument*/NULL);
		GUIStyle_t302 * L_20 = (__this->___lowerLeftFontStyle_67);
		Vector2_t6 * L_21 = &(__this->___size_82);
		float L_22 = (L_21->___y_2);
		NullCheck(L_20);
		GUIStyle_set_fontSize_m1624(L_20, (((int32_t)((float)((float)L_22/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_23 = (__this->___lowerLeftFontStyle_67);
		NullCheck(L_23);
		GUIStyle_set_fontStyle_m1626(L_23, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_24 = (__this->___lowerLeftFontStyle_67);
		NullCheck(L_24);
		GUIStyle_set_alignment_m1625(L_24, 6, /*hidden argument*/NULL);
		GUIStyle_t302 * L_25 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_25, /*hidden argument*/NULL);
		__this->___barStyle_64 = L_25;
		GUIStyle_t302 * L_26 = (__this->___barStyle_64);
		RectOffset_t404 * L_27 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m1620(L_27, 1, 1, 1, 1, /*hidden argument*/NULL);
		NullCheck(L_26);
		GUIStyle_set_border_m1621(L_26, L_27, /*hidden argument*/NULL);
		GUIStyle_t302 * L_28 = (__this->___barStyle_64);
		NullCheck(L_28);
		GUIStyleState_t405 * L_29 = GUIStyle_get_normal_m1622(L_28, /*hidden argument*/NULL);
		Images_t288 * L_30 = (__this->___images_41);
		NullCheck(L_30);
		Texture2D_t63 * L_31 = (L_30->___barImage_23);
		NullCheck(L_29);
		GUIStyleState_set_background_m1623(L_29, L_31, /*hidden argument*/NULL);
		GUIStyle_t302 * L_32 = (__this->___barStyle_64);
		NullCheck(L_32);
		GUIStyleState_t405 * L_33 = GUIStyle_get_active_m1627(L_32, /*hidden argument*/NULL);
		Images_t288 * L_34 = (__this->___images_41);
		NullCheck(L_34);
		Texture2D_t63 * L_35 = (L_34->___button_activeImage_24);
		NullCheck(L_33);
		GUIStyleState_set_background_m1623(L_33, L_35, /*hidden argument*/NULL);
		GUIStyle_t302 * L_36 = (__this->___barStyle_64);
		NullCheck(L_36);
		GUIStyle_set_alignment_m1625(L_36, 4, /*hidden argument*/NULL);
		GUIStyle_t302 * L_37 = (__this->___barStyle_64);
		RectOffset_t404 * L_38 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m1620(L_38, 1, 1, 1, 1, /*hidden argument*/NULL);
		NullCheck(L_37);
		GUIStyle_set_margin_m1628(L_37, L_38, /*hidden argument*/NULL);
		GUIStyle_t302 * L_39 = (__this->___barStyle_64);
		NullCheck(L_39);
		GUIStyle_set_clipping_m1619(L_39, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_40 = (__this->___barStyle_64);
		Vector2_t6 * L_41 = &(__this->___size_82);
		float L_42 = (L_41->___y_2);
		NullCheck(L_40);
		GUIStyle_set_fontSize_m1624(L_40, (((int32_t)((float)((float)L_42/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_43 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_43, /*hidden argument*/NULL);
		__this->___buttonActiveStyle_65 = L_43;
		GUIStyle_t302 * L_44 = (__this->___buttonActiveStyle_65);
		RectOffset_t404 * L_45 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m1620(L_45, 1, 1, 1, 1, /*hidden argument*/NULL);
		NullCheck(L_44);
		GUIStyle_set_border_m1621(L_44, L_45, /*hidden argument*/NULL);
		GUIStyle_t302 * L_46 = (__this->___buttonActiveStyle_65);
		NullCheck(L_46);
		GUIStyleState_t405 * L_47 = GUIStyle_get_normal_m1622(L_46, /*hidden argument*/NULL);
		Images_t288 * L_48 = (__this->___images_41);
		NullCheck(L_48);
		Texture2D_t63 * L_49 = (L_48->___button_activeImage_24);
		NullCheck(L_47);
		GUIStyleState_set_background_m1623(L_47, L_49, /*hidden argument*/NULL);
		GUIStyle_t302 * L_50 = (__this->___buttonActiveStyle_65);
		NullCheck(L_50);
		GUIStyle_set_alignment_m1625(L_50, 4, /*hidden argument*/NULL);
		GUIStyle_t302 * L_51 = (__this->___buttonActiveStyle_65);
		RectOffset_t404 * L_52 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m1620(L_52, 1, 1, 1, 1, /*hidden argument*/NULL);
		NullCheck(L_51);
		GUIStyle_set_margin_m1628(L_51, L_52, /*hidden argument*/NULL);
		GUIStyle_t302 * L_53 = (__this->___buttonActiveStyle_65);
		Vector2_t6 * L_54 = &(__this->___size_82);
		float L_55 = (L_54->___y_2);
		NullCheck(L_53);
		GUIStyle_set_fontSize_m1624(L_53, (((int32_t)((float)((float)L_55/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_56 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_56, /*hidden argument*/NULL);
		__this->___backStyle_68 = L_56;
		GUIStyle_t302 * L_57 = (__this->___backStyle_68);
		NullCheck(L_57);
		GUIStyleState_t405 * L_58 = GUIStyle_get_normal_m1622(L_57, /*hidden argument*/NULL);
		Images_t288 * L_59 = (__this->___images_41);
		NullCheck(L_59);
		Texture2D_t63 * L_60 = (L_59->___even_logImage_25);
		NullCheck(L_58);
		GUIStyleState_set_background_m1623(L_58, L_60, /*hidden argument*/NULL);
		GUIStyle_t302 * L_61 = (__this->___backStyle_68);
		NullCheck(L_61);
		GUIStyle_set_clipping_m1619(L_61, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_62 = (__this->___backStyle_68);
		Vector2_t6 * L_63 = &(__this->___size_82);
		float L_64 = (L_63->___y_2);
		NullCheck(L_62);
		GUIStyle_set_fontSize_m1624(L_62, (((int32_t)((float)((float)L_64/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_65 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_65, /*hidden argument*/NULL);
		__this->___evenLogStyle_69 = L_65;
		GUIStyle_t302 * L_66 = (__this->___evenLogStyle_69);
		NullCheck(L_66);
		GUIStyleState_t405 * L_67 = GUIStyle_get_normal_m1622(L_66, /*hidden argument*/NULL);
		Images_t288 * L_68 = (__this->___images_41);
		NullCheck(L_68);
		Texture2D_t63 * L_69 = (L_68->___even_logImage_25);
		NullCheck(L_67);
		GUIStyleState_set_background_m1623(L_67, L_69, /*hidden argument*/NULL);
		GUIStyle_t302 * L_70 = (__this->___evenLogStyle_69);
		Vector2_t6 * L_71 = &(__this->___size_82);
		float L_72 = (L_71->___y_2);
		NullCheck(L_70);
		GUIStyle_set_fixedHeight_m1629(L_70, L_72, /*hidden argument*/NULL);
		GUIStyle_t302 * L_73 = (__this->___evenLogStyle_69);
		NullCheck(L_73);
		GUIStyle_set_clipping_m1619(L_73, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_74 = (__this->___evenLogStyle_69);
		NullCheck(L_74);
		GUIStyle_set_alignment_m1625(L_74, 0, /*hidden argument*/NULL);
		GUIStyle_t302 * L_75 = (__this->___evenLogStyle_69);
		NullCheck(L_75);
		GUIStyle_set_imagePosition_m1630(L_75, 0, /*hidden argument*/NULL);
		GUIStyle_t302 * L_76 = (__this->___evenLogStyle_69);
		Vector2_t6 * L_77 = &(__this->___size_82);
		float L_78 = (L_77->___y_2);
		NullCheck(L_76);
		GUIStyle_set_fontSize_m1624(L_76, (((int32_t)((float)((float)L_78/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_79 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_79, /*hidden argument*/NULL);
		__this->___oddLogStyle_70 = L_79;
		GUIStyle_t302 * L_80 = (__this->___oddLogStyle_70);
		NullCheck(L_80);
		GUIStyleState_t405 * L_81 = GUIStyle_get_normal_m1622(L_80, /*hidden argument*/NULL);
		Images_t288 * L_82 = (__this->___images_41);
		NullCheck(L_82);
		Texture2D_t63 * L_83 = (L_82->___odd_logImage_26);
		NullCheck(L_81);
		GUIStyleState_set_background_m1623(L_81, L_83, /*hidden argument*/NULL);
		GUIStyle_t302 * L_84 = (__this->___oddLogStyle_70);
		Vector2_t6 * L_85 = &(__this->___size_82);
		float L_86 = (L_85->___y_2);
		NullCheck(L_84);
		GUIStyle_set_fixedHeight_m1629(L_84, L_86, /*hidden argument*/NULL);
		GUIStyle_t302 * L_87 = (__this->___oddLogStyle_70);
		NullCheck(L_87);
		GUIStyle_set_clipping_m1619(L_87, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_88 = (__this->___oddLogStyle_70);
		NullCheck(L_88);
		GUIStyle_set_alignment_m1625(L_88, 0, /*hidden argument*/NULL);
		GUIStyle_t302 * L_89 = (__this->___oddLogStyle_70);
		NullCheck(L_89);
		GUIStyle_set_imagePosition_m1630(L_89, 0, /*hidden argument*/NULL);
		GUIStyle_t302 * L_90 = (__this->___oddLogStyle_70);
		Vector2_t6 * L_91 = &(__this->___size_82);
		float L_92 = (L_91->___y_2);
		NullCheck(L_90);
		GUIStyle_set_fontSize_m1624(L_90, (((int32_t)((float)((float)L_92/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_93 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_93, /*hidden argument*/NULL);
		__this->___logButtonStyle_71 = L_93;
		GUIStyle_t302 * L_94 = (__this->___logButtonStyle_71);
		Vector2_t6 * L_95 = &(__this->___size_82);
		float L_96 = (L_95->___y_2);
		NullCheck(L_94);
		GUIStyle_set_fixedHeight_m1629(L_94, L_96, /*hidden argument*/NULL);
		GUIStyle_t302 * L_97 = (__this->___logButtonStyle_71);
		NullCheck(L_97);
		GUIStyle_set_clipping_m1619(L_97, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_98 = (__this->___logButtonStyle_71);
		NullCheck(L_98);
		GUIStyle_set_alignment_m1625(L_98, 0, /*hidden argument*/NULL);
		GUIStyle_t302 * L_99 = (__this->___logButtonStyle_71);
		Vector2_t6 * L_100 = &(__this->___size_82);
		float L_101 = (L_100->___y_2);
		NullCheck(L_99);
		GUIStyle_set_fontSize_m1624(L_99, (((int32_t)((float)((float)L_101/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_102 = (__this->___logButtonStyle_71);
		int32_t L_103 = V_0;
		int32_t L_104 = V_0;
		int32_t L_105 = V_1;
		int32_t L_106 = V_1;
		RectOffset_t404 * L_107 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m1620(L_107, L_103, L_104, L_105, L_106, /*hidden argument*/NULL);
		NullCheck(L_102);
		GUIStyle_set_padding_m1631(L_102, L_107, /*hidden argument*/NULL);
		GUIStyle_t302 * L_108 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_108, /*hidden argument*/NULL);
		__this->___selectedLogStyle_72 = L_108;
		GUIStyle_t302 * L_109 = (__this->___selectedLogStyle_72);
		NullCheck(L_109);
		GUIStyleState_t405 * L_110 = GUIStyle_get_normal_m1622(L_109, /*hidden argument*/NULL);
		Images_t288 * L_111 = (__this->___images_41);
		NullCheck(L_111);
		Texture2D_t63 * L_112 = (L_111->___selectedImage_27);
		NullCheck(L_110);
		GUIStyleState_set_background_m1623(L_110, L_112, /*hidden argument*/NULL);
		GUIStyle_t302 * L_113 = (__this->___selectedLogStyle_72);
		Vector2_t6 * L_114 = &(__this->___size_82);
		float L_115 = (L_114->___y_2);
		NullCheck(L_113);
		GUIStyle_set_fixedHeight_m1629(L_113, L_115, /*hidden argument*/NULL);
		GUIStyle_t302 * L_116 = (__this->___selectedLogStyle_72);
		NullCheck(L_116);
		GUIStyle_set_clipping_m1619(L_116, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_117 = (__this->___selectedLogStyle_72);
		NullCheck(L_117);
		GUIStyle_set_alignment_m1625(L_117, 0, /*hidden argument*/NULL);
		GUIStyle_t302 * L_118 = (__this->___selectedLogStyle_72);
		NullCheck(L_118);
		GUIStyleState_t405 * L_119 = GUIStyle_get_normal_m1622(L_118, /*hidden argument*/NULL);
		Color_t65  L_120 = Color_get_white_m745(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_119);
		GUIStyleState_set_textColor_m1632(L_119, L_120, /*hidden argument*/NULL);
		GUIStyle_t302 * L_121 = (__this->___selectedLogStyle_72);
		Vector2_t6 * L_122 = &(__this->___size_82);
		float L_123 = (L_122->___y_2);
		NullCheck(L_121);
		GUIStyle_set_fontSize_m1624(L_121, (((int32_t)((float)((float)L_123/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_124 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_124, /*hidden argument*/NULL);
		__this->___selectedLogFontStyle_73 = L_124;
		GUIStyle_t302 * L_125 = (__this->___selectedLogFontStyle_73);
		NullCheck(L_125);
		GUIStyleState_t405 * L_126 = GUIStyle_get_normal_m1622(L_125, /*hidden argument*/NULL);
		Images_t288 * L_127 = (__this->___images_41);
		NullCheck(L_127);
		Texture2D_t63 * L_128 = (L_127->___selectedImage_27);
		NullCheck(L_126);
		GUIStyleState_set_background_m1623(L_126, L_128, /*hidden argument*/NULL);
		GUIStyle_t302 * L_129 = (__this->___selectedLogFontStyle_73);
		Vector2_t6 * L_130 = &(__this->___size_82);
		float L_131 = (L_130->___y_2);
		NullCheck(L_129);
		GUIStyle_set_fixedHeight_m1629(L_129, L_131, /*hidden argument*/NULL);
		GUIStyle_t302 * L_132 = (__this->___selectedLogFontStyle_73);
		NullCheck(L_132);
		GUIStyle_set_clipping_m1619(L_132, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_133 = (__this->___selectedLogFontStyle_73);
		NullCheck(L_133);
		GUIStyle_set_alignment_m1625(L_133, 0, /*hidden argument*/NULL);
		GUIStyle_t302 * L_134 = (__this->___selectedLogFontStyle_73);
		NullCheck(L_134);
		GUIStyleState_t405 * L_135 = GUIStyle_get_normal_m1622(L_134, /*hidden argument*/NULL);
		Color_t65  L_136 = Color_get_white_m745(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_135);
		GUIStyleState_set_textColor_m1632(L_135, L_136, /*hidden argument*/NULL);
		GUIStyle_t302 * L_137 = (__this->___selectedLogFontStyle_73);
		Vector2_t6 * L_138 = &(__this->___size_82);
		float L_139 = (L_138->___y_2);
		NullCheck(L_137);
		GUIStyle_set_fontSize_m1624(L_137, (((int32_t)((float)((float)L_139/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_140 = (__this->___selectedLogFontStyle_73);
		int32_t L_141 = V_0;
		int32_t L_142 = V_0;
		int32_t L_143 = V_1;
		int32_t L_144 = V_1;
		RectOffset_t404 * L_145 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m1620(L_145, L_141, L_142, L_143, L_144, /*hidden argument*/NULL);
		NullCheck(L_140);
		GUIStyle_set_padding_m1631(L_140, L_145, /*hidden argument*/NULL);
		GUIStyle_t302 * L_146 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_146, /*hidden argument*/NULL);
		__this->___stackLabelStyle_74 = L_146;
		GUIStyle_t302 * L_147 = (__this->___stackLabelStyle_74);
		NullCheck(L_147);
		GUIStyle_set_wordWrap_m1633(L_147, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_148 = (__this->___stackLabelStyle_74);
		Vector2_t6 * L_149 = &(__this->___size_82);
		float L_150 = (L_149->___y_2);
		NullCheck(L_148);
		GUIStyle_set_fontSize_m1624(L_148, (((int32_t)((float)((float)L_150/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_151 = (__this->___stackLabelStyle_74);
		int32_t L_152 = V_0;
		int32_t L_153 = V_0;
		int32_t L_154 = V_1;
		int32_t L_155 = V_1;
		RectOffset_t404 * L_156 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m1620(L_156, L_152, L_153, L_154, L_155, /*hidden argument*/NULL);
		NullCheck(L_151);
		GUIStyle_set_padding_m1631(L_151, L_156, /*hidden argument*/NULL);
		GUIStyle_t302 * L_157 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_157, /*hidden argument*/NULL);
		__this->___scrollerStyle_75 = L_157;
		GUIStyle_t302 * L_158 = (__this->___scrollerStyle_75);
		NullCheck(L_158);
		GUIStyleState_t405 * L_159 = GUIStyle_get_normal_m1622(L_158, /*hidden argument*/NULL);
		Images_t288 * L_160 = (__this->___images_41);
		NullCheck(L_160);
		Texture2D_t63 * L_161 = (L_160->___barImage_23);
		NullCheck(L_159);
		GUIStyleState_set_background_m1623(L_159, L_161, /*hidden argument*/NULL);
		GUIStyle_t302 * L_162 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_162, /*hidden argument*/NULL);
		__this->___searchStyle_76 = L_162;
		GUIStyle_t302 * L_163 = (__this->___searchStyle_76);
		NullCheck(L_163);
		GUIStyle_set_clipping_m1619(L_163, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_164 = (__this->___searchStyle_76);
		NullCheck(L_164);
		GUIStyle_set_alignment_m1625(L_164, 7, /*hidden argument*/NULL);
		GUIStyle_t302 * L_165 = (__this->___searchStyle_76);
		Vector2_t6 * L_166 = &(__this->___size_82);
		float L_167 = (L_166->___y_2);
		NullCheck(L_165);
		GUIStyle_set_fontSize_m1624(L_165, (((int32_t)((float)((float)L_167/(float)(2.0f))))), /*hidden argument*/NULL);
		GUIStyle_t302 * L_168 = (__this->___searchStyle_76);
		NullCheck(L_168);
		GUIStyle_set_wordWrap_m1633(L_168, 1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_169 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_169, /*hidden argument*/NULL);
		__this->___sliderBackStyle_77 = L_169;
		GUIStyle_t302 * L_170 = (__this->___sliderBackStyle_77);
		NullCheck(L_170);
		GUIStyleState_t405 * L_171 = GUIStyle_get_normal_m1622(L_170, /*hidden argument*/NULL);
		Images_t288 * L_172 = (__this->___images_41);
		NullCheck(L_172);
		Texture2D_t63 * L_173 = (L_172->___barImage_23);
		NullCheck(L_171);
		GUIStyleState_set_background_m1623(L_171, L_173, /*hidden argument*/NULL);
		GUIStyle_t302 * L_174 = (__this->___sliderBackStyle_77);
		Vector2_t6 * L_175 = &(__this->___size_82);
		float L_176 = (L_175->___y_2);
		NullCheck(L_174);
		GUIStyle_set_fixedHeight_m1629(L_174, L_176, /*hidden argument*/NULL);
		GUIStyle_t302 * L_177 = (__this->___sliderBackStyle_77);
		RectOffset_t404 * L_178 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m1620(L_178, 1, 1, 1, 1, /*hidden argument*/NULL);
		NullCheck(L_177);
		GUIStyle_set_border_m1621(L_177, L_178, /*hidden argument*/NULL);
		GUIStyle_t302 * L_179 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_179, /*hidden argument*/NULL);
		__this->___sliderThumbStyle_78 = L_179;
		GUIStyle_t302 * L_180 = (__this->___sliderThumbStyle_78);
		NullCheck(L_180);
		GUIStyleState_t405 * L_181 = GUIStyle_get_normal_m1622(L_180, /*hidden argument*/NULL);
		Images_t288 * L_182 = (__this->___images_41);
		NullCheck(L_182);
		Texture2D_t63 * L_183 = (L_182->___selectedImage_27);
		NullCheck(L_181);
		GUIStyleState_set_background_m1623(L_181, L_183, /*hidden argument*/NULL);
		GUIStyle_t302 * L_184 = (__this->___sliderThumbStyle_78);
		Vector2_t6 * L_185 = &(__this->___size_82);
		float L_186 = (L_185->___x_1);
		NullCheck(L_184);
		GUIStyle_set_fixedWidth_m1634(L_184, L_186, /*hidden argument*/NULL);
		Images_t288 * L_187 = (__this->___images_41);
		NullCheck(L_187);
		GUISkin_t287 * L_188 = (L_187->___reporterScrollerSkin_28);
		V_2 = L_188;
		GUISkin_t287 * L_189 = V_2;
		GUISkin_t287 * L_190 = Object_Instantiate_TisGUISkin_t287_m1589(NULL /*static, unused*/, L_189, /*hidden argument*/Object_Instantiate_TisGUISkin_t287_m1589_MethodInfo_var);
		__this->___toolbarScrollerSkin_79 = L_190;
		GUISkin_t287 * L_191 = (__this->___toolbarScrollerSkin_79);
		NullCheck(L_191);
		GUIStyle_t302 * L_192 = GUISkin_get_verticalScrollbar_m1635(L_191, /*hidden argument*/NULL);
		NullCheck(L_192);
		GUIStyle_set_fixedWidth_m1634(L_192, (0.0f), /*hidden argument*/NULL);
		GUISkin_t287 * L_193 = (__this->___toolbarScrollerSkin_79);
		NullCheck(L_193);
		GUIStyle_t302 * L_194 = GUISkin_get_horizontalScrollbar_m1636(L_193, /*hidden argument*/NULL);
		NullCheck(L_194);
		GUIStyle_set_fixedHeight_m1629(L_194, (0.0f), /*hidden argument*/NULL);
		GUISkin_t287 * L_195 = (__this->___toolbarScrollerSkin_79);
		NullCheck(L_195);
		GUIStyle_t302 * L_196 = GUISkin_get_verticalScrollbarThumb_m1637(L_195, /*hidden argument*/NULL);
		NullCheck(L_196);
		GUIStyle_set_fixedWidth_m1634(L_196, (0.0f), /*hidden argument*/NULL);
		GUISkin_t287 * L_197 = (__this->___toolbarScrollerSkin_79);
		NullCheck(L_197);
		GUIStyle_t302 * L_198 = GUISkin_get_horizontalScrollbarThumb_m1638(L_197, /*hidden argument*/NULL);
		NullCheck(L_198);
		GUIStyle_set_fixedHeight_m1629(L_198, (0.0f), /*hidden argument*/NULL);
		GUISkin_t287 * L_199 = V_2;
		GUISkin_t287 * L_200 = Object_Instantiate_TisGUISkin_t287_m1589(NULL /*static, unused*/, L_199, /*hidden argument*/Object_Instantiate_TisGUISkin_t287_m1589_MethodInfo_var);
		__this->___logScrollerSkin_80 = L_200;
		GUISkin_t287 * L_201 = (__this->___logScrollerSkin_80);
		NullCheck(L_201);
		GUIStyle_t302 * L_202 = GUISkin_get_verticalScrollbar_m1635(L_201, /*hidden argument*/NULL);
		Vector2_t6 * L_203 = &(__this->___size_82);
		float L_204 = (L_203->___x_1);
		NullCheck(L_202);
		GUIStyle_set_fixedWidth_m1634(L_202, ((float)((float)L_204*(float)(2.0f))), /*hidden argument*/NULL);
		GUISkin_t287 * L_205 = (__this->___logScrollerSkin_80);
		NullCheck(L_205);
		GUIStyle_t302 * L_206 = GUISkin_get_horizontalScrollbar_m1636(L_205, /*hidden argument*/NULL);
		NullCheck(L_206);
		GUIStyle_set_fixedHeight_m1629(L_206, (0.0f), /*hidden argument*/NULL);
		GUISkin_t287 * L_207 = (__this->___logScrollerSkin_80);
		NullCheck(L_207);
		GUIStyle_t302 * L_208 = GUISkin_get_verticalScrollbarThumb_m1637(L_207, /*hidden argument*/NULL);
		Vector2_t6 * L_209 = &(__this->___size_82);
		float L_210 = (L_209->___x_1);
		NullCheck(L_208);
		GUIStyle_set_fixedWidth_m1634(L_208, ((float)((float)L_210*(float)(2.0f))), /*hidden argument*/NULL);
		GUISkin_t287 * L_211 = (__this->___logScrollerSkin_80);
		NullCheck(L_211);
		GUIStyle_t302 * L_212 = GUISkin_get_horizontalScrollbarThumb_m1638(L_211, /*hidden argument*/NULL);
		NullCheck(L_212);
		GUIStyle_set_fixedHeight_m1629(L_212, (0.0f), /*hidden argument*/NULL);
		GUISkin_t287 * L_213 = V_2;
		GUISkin_t287 * L_214 = Object_Instantiate_TisGUISkin_t287_m1589(NULL /*static, unused*/, L_213, /*hidden argument*/Object_Instantiate_TisGUISkin_t287_m1589_MethodInfo_var);
		__this->___graphScrollerSkin_81 = L_214;
		GUISkin_t287 * L_215 = (__this->___graphScrollerSkin_81);
		NullCheck(L_215);
		GUIStyle_t302 * L_216 = GUISkin_get_verticalScrollbar_m1635(L_215, /*hidden argument*/NULL);
		NullCheck(L_216);
		GUIStyle_set_fixedWidth_m1634(L_216, (0.0f), /*hidden argument*/NULL);
		GUISkin_t287 * L_217 = (__this->___graphScrollerSkin_81);
		NullCheck(L_217);
		GUIStyle_t302 * L_218 = GUISkin_get_horizontalScrollbar_m1636(L_217, /*hidden argument*/NULL);
		Vector2_t6 * L_219 = &(__this->___size_82);
		float L_220 = (L_219->___x_1);
		NullCheck(L_218);
		GUIStyle_set_fixedHeight_m1629(L_218, ((float)((float)L_220*(float)(2.0f))), /*hidden argument*/NULL);
		GUISkin_t287 * L_221 = (__this->___graphScrollerSkin_81);
		NullCheck(L_221);
		GUIStyle_t302 * L_222 = GUISkin_get_verticalScrollbarThumb_m1637(L_221, /*hidden argument*/NULL);
		NullCheck(L_222);
		GUIStyle_set_fixedWidth_m1634(L_222, (0.0f), /*hidden argument*/NULL);
		GUISkin_t287 * L_223 = (__this->___graphScrollerSkin_81);
		NullCheck(L_223);
		GUIStyle_t302 * L_224 = GUISkin_get_horizontalScrollbarThumb_m1638(L_223, /*hidden argument*/NULL);
		Vector2_t6 * L_225 = &(__this->___size_82);
		float L_226 = (L_225->___x_1);
		NullCheck(L_224);
		GUIStyle_set_fixedHeight_m1629(L_224, ((float)((float)L_226*(float)(2.0f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reporter::Start()
extern TypeInfo* DateTime_t406_il2cpp_TypeInfo_var;
extern "C" void Reporter_Start_m1100 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(650);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t406  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t406_il2cpp_TypeInfo_var);
		DateTime_t406  L_0 = DateTime_get_Now_m1639(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = DateTime_ToString_m1640((&V_0), /*hidden argument*/NULL);
		__this->___logDate_32 = L_1;
		MonoBehaviour_StartCoroutine_m1641(__this, (String_t*) &_stringLiteral240, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reporter::clear()
extern "C" void Reporter_clear_m1101 (Reporter_t295 * __this, const MethodInfo* method)
{
	{
		List_1_t298 * L_0 = (__this->___logs_3);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Clear() */, L_0);
		List_1_t298 * L_1 = (__this->___collapsedLogs_4);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Clear() */, L_1);
		List_1_t298 * L_2 = (__this->___currentLog_5);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Clear() */, L_2);
		MultiKeyDictionary_3_t299 * L_3 = (__this->___logsDic_6);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::Clear() */, L_3);
		__this->___selectedLog_108 = (Log_t291 *)NULL;
		__this->___numOfLogs_19 = 0;
		__this->___numOfLogsWarning_20 = 0;
		__this->___numOfLogsError_21 = 0;
		__this->___numOfCollapsedLogs_22 = 0;
		__this->___numOfCollapsedLogsWarning_23 = 0;
		__this->___numOfCollapsedLogsError_24 = 0;
		__this->___logsMemUsage_33 = (0.0f);
		__this->___graphMemUsage_34 = (0.0f);
		List_1_t297 * L_4 = (__this->___samples_2);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Reporter/Sample>::Clear() */, L_4);
		GC_Collect_m1642(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___selectedLog_108 = (Log_t291 *)NULL;
		return;
	}
}
// System.Void Reporter::calculateCurrentLog()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* MultiKeyDictionary_3_get_Item_m1644_MethodInfo_var;
extern "C" void Reporter_calculateCurrentLog_m1102 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		MultiKeyDictionary_3_get_Item_m1644_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483688);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = {0};
	int32_t V_2 = 0;
	Log_t291 * V_3 = {0};
	int32_t V_4 = 0;
	Log_t291 * V_5 = {0};
	int32_t V_6 = 0;
	Log_t291 * V_7 = {0};
	{
		String_t* L_0 = (__this->___filterText_87);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1587(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_4 = (__this->___filterText_87);
		NullCheck(L_4);
		String_t* L_5 = String_ToLower_m1643(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
	}

IL_0027:
	{
		List_1_t298 * L_6 = (__this->___currentLog_5);
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Clear() */, L_6);
		bool L_7 = (__this->___collapse_9);
		if (!L_7)
		{
			goto IL_012f;
		}
	}
	{
		V_2 = 0;
		goto IL_0119;
	}

IL_0044:
	{
		List_1_t298 * L_8 = (__this->___collapsedLogs_4);
		int32_t L_9 = V_2;
		NullCheck(L_8);
		Log_t291 * L_10 = (Log_t291 *)VirtFuncInvoker1< Log_t291 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Reporter/Log>::get_Item(System.Int32) */, L_8, L_9);
		V_3 = L_10;
		Log_t291 * L_11 = V_3;
		NullCheck(L_11);
		int32_t L_12 = (L_11->___logType_1);
		if ((!(((uint32_t)L_12) == ((uint32_t)3))))
		{
			goto IL_006d;
		}
	}
	{
		bool L_13 = (__this->___showLog_16);
		if (L_13)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_0115;
	}

IL_006d:
	{
		Log_t291 * L_14 = V_3;
		NullCheck(L_14);
		int32_t L_15 = (L_14->___logType_1);
		if ((!(((uint32_t)L_15) == ((uint32_t)2))))
		{
			goto IL_0089;
		}
	}
	{
		bool L_16 = (__this->___showWarning_17);
		if (L_16)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_0115;
	}

IL_0089:
	{
		Log_t291 * L_17 = V_3;
		NullCheck(L_17);
		int32_t L_18 = (L_17->___logType_1);
		if (L_18)
		{
			goto IL_00a4;
		}
	}
	{
		bool L_19 = (__this->___showError_18);
		if (L_19)
		{
			goto IL_00a4;
		}
	}
	{
		goto IL_0115;
	}

IL_00a4:
	{
		Log_t291 * L_20 = V_3;
		NullCheck(L_20);
		int32_t L_21 = (L_20->___logType_1);
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_00c0;
		}
	}
	{
		bool L_22 = (__this->___showError_18);
		if (L_22)
		{
			goto IL_00c0;
		}
	}
	{
		goto IL_0115;
	}

IL_00c0:
	{
		Log_t291 * L_23 = V_3;
		NullCheck(L_23);
		int32_t L_24 = (L_23->___logType_1);
		if ((!(((uint32_t)L_24) == ((uint32_t)4))))
		{
			goto IL_00dc;
		}
	}
	{
		bool L_25 = (__this->___showError_18);
		if (L_25)
		{
			goto IL_00dc;
		}
	}
	{
		goto IL_0115;
	}

IL_00dc:
	{
		bool L_26 = V_0;
		if (!L_26)
		{
			goto IL_0109;
		}
	}
	{
		Log_t291 * L_27 = V_3;
		NullCheck(L_27);
		String_t* L_28 = (L_27->___condition_2);
		NullCheck(L_28);
		String_t* L_29 = String_ToLower_m1643(L_28, /*hidden argument*/NULL);
		String_t* L_30 = V_1;
		NullCheck(L_29);
		bool L_31 = String_Contains_m1584(L_29, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0104;
		}
	}
	{
		List_1_t298 * L_32 = (__this->___currentLog_5);
		Log_t291 * L_33 = V_3;
		NullCheck(L_32);
		VirtActionInvoker1< Log_t291 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Add(!0) */, L_32, L_33);
	}

IL_0104:
	{
		goto IL_0115;
	}

IL_0109:
	{
		List_1_t298 * L_34 = (__this->___currentLog_5);
		Log_t291 * L_35 = V_3;
		NullCheck(L_34);
		VirtActionInvoker1< Log_t291 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Add(!0) */, L_34, L_35);
	}

IL_0115:
	{
		int32_t L_36 = V_2;
		V_2 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_0119:
	{
		int32_t L_37 = V_2;
		List_1_t298 * L_38 = (__this->___collapsedLogs_4);
		NullCheck(L_38);
		int32_t L_39 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Count() */, L_38);
		if ((((int32_t)L_37) < ((int32_t)L_39)))
		{
			goto IL_0044;
		}
	}
	{
		goto IL_022a;
	}

IL_012f:
	{
		V_4 = 0;
		goto IL_0218;
	}

IL_0137:
	{
		List_1_t298 * L_40 = (__this->___logs_3);
		int32_t L_41 = V_4;
		NullCheck(L_40);
		Log_t291 * L_42 = (Log_t291 *)VirtFuncInvoker1< Log_t291 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Reporter/Log>::get_Item(System.Int32) */, L_40, L_41);
		V_5 = L_42;
		Log_t291 * L_43 = V_5;
		NullCheck(L_43);
		int32_t L_44 = (L_43->___logType_1);
		if ((!(((uint32_t)L_44) == ((uint32_t)3))))
		{
			goto IL_0163;
		}
	}
	{
		bool L_45 = (__this->___showLog_16);
		if (L_45)
		{
			goto IL_0163;
		}
	}
	{
		goto IL_0212;
	}

IL_0163:
	{
		Log_t291 * L_46 = V_5;
		NullCheck(L_46);
		int32_t L_47 = (L_46->___logType_1);
		if ((!(((uint32_t)L_47) == ((uint32_t)2))))
		{
			goto IL_0180;
		}
	}
	{
		bool L_48 = (__this->___showWarning_17);
		if (L_48)
		{
			goto IL_0180;
		}
	}
	{
		goto IL_0212;
	}

IL_0180:
	{
		Log_t291 * L_49 = V_5;
		NullCheck(L_49);
		int32_t L_50 = (L_49->___logType_1);
		if (L_50)
		{
			goto IL_019c;
		}
	}
	{
		bool L_51 = (__this->___showError_18);
		if (L_51)
		{
			goto IL_019c;
		}
	}
	{
		goto IL_0212;
	}

IL_019c:
	{
		Log_t291 * L_52 = V_5;
		NullCheck(L_52);
		int32_t L_53 = (L_52->___logType_1);
		if ((!(((uint32_t)L_53) == ((uint32_t)1))))
		{
			goto IL_01b9;
		}
	}
	{
		bool L_54 = (__this->___showError_18);
		if (L_54)
		{
			goto IL_01b9;
		}
	}
	{
		goto IL_0212;
	}

IL_01b9:
	{
		Log_t291 * L_55 = V_5;
		NullCheck(L_55);
		int32_t L_56 = (L_55->___logType_1);
		if ((!(((uint32_t)L_56) == ((uint32_t)4))))
		{
			goto IL_01d6;
		}
	}
	{
		bool L_57 = (__this->___showError_18);
		if (L_57)
		{
			goto IL_01d6;
		}
	}
	{
		goto IL_0212;
	}

IL_01d6:
	{
		bool L_58 = V_0;
		if (!L_58)
		{
			goto IL_0205;
		}
	}
	{
		Log_t291 * L_59 = V_5;
		NullCheck(L_59);
		String_t* L_60 = (L_59->___condition_2);
		NullCheck(L_60);
		String_t* L_61 = String_ToLower_m1643(L_60, /*hidden argument*/NULL);
		String_t* L_62 = V_1;
		NullCheck(L_61);
		bool L_63 = String_Contains_m1584(L_61, L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_0200;
		}
	}
	{
		List_1_t298 * L_64 = (__this->___currentLog_5);
		Log_t291 * L_65 = V_5;
		NullCheck(L_64);
		VirtActionInvoker1< Log_t291 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Add(!0) */, L_64, L_65);
	}

IL_0200:
	{
		goto IL_0212;
	}

IL_0205:
	{
		List_1_t298 * L_66 = (__this->___currentLog_5);
		Log_t291 * L_67 = V_5;
		NullCheck(L_66);
		VirtActionInvoker1< Log_t291 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Add(!0) */, L_66, L_67);
	}

IL_0212:
	{
		int32_t L_68 = V_4;
		V_4 = ((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_0218:
	{
		int32_t L_69 = V_4;
		List_1_t298 * L_70 = (__this->___logs_3);
		NullCheck(L_70);
		int32_t L_71 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Count() */, L_70);
		if ((((int32_t)L_69) < ((int32_t)L_71)))
		{
			goto IL_0137;
		}
	}

IL_022a:
	{
		Log_t291 * L_72 = (__this->___selectedLog_108);
		if (!L_72)
		{
			goto IL_02c8;
		}
	}
	{
		List_1_t298 * L_73 = (__this->___currentLog_5);
		Log_t291 * L_74 = (__this->___selectedLog_108);
		NullCheck(L_73);
		int32_t L_75 = (int32_t)VirtFuncInvoker1< int32_t, Log_t291 * >::Invoke(28 /* System.Int32 System.Collections.Generic.List`1<Reporter/Log>::IndexOf(!0) */, L_73, L_74);
		V_6 = L_75;
		int32_t L_76 = V_6;
		if ((!(((uint32_t)L_76) == ((uint32_t)(-1)))))
		{
			goto IL_02ae;
		}
	}
	{
		MultiKeyDictionary_3_t299 * L_77 = (__this->___logsDic_6);
		Log_t291 * L_78 = (__this->___selectedLog_108);
		NullCheck(L_78);
		String_t* L_79 = (L_78->___condition_2);
		NullCheck(L_77);
		Dictionary_2_t407 * L_80 = MultiKeyDictionary_3_get_Item_m1644(L_77, L_79, /*hidden argument*/MultiKeyDictionary_3_get_Item_m1644_MethodInfo_var);
		Log_t291 * L_81 = (__this->___selectedLog_108);
		NullCheck(L_81);
		String_t* L_82 = (L_81->___stacktrace_3);
		NullCheck(L_80);
		Log_t291 * L_83 = (Log_t291 *)VirtFuncInvoker1< Log_t291 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>::get_Item(!0) */, L_80, L_82);
		V_7 = L_83;
		List_1_t298 * L_84 = (__this->___currentLog_5);
		Log_t291 * L_85 = V_7;
		NullCheck(L_84);
		int32_t L_86 = (int32_t)VirtFuncInvoker1< int32_t, Log_t291 * >::Invoke(28 /* System.Int32 System.Collections.Generic.List`1<Reporter/Log>::IndexOf(!0) */, L_84, L_85);
		V_6 = L_86;
		int32_t L_87 = V_6;
		if ((((int32_t)L_87) == ((int32_t)(-1))))
		{
			goto IL_02a9;
		}
	}
	{
		Vector2_t6 * L_88 = &(__this->___scrollPosition_105);
		int32_t L_89 = V_6;
		Vector2_t6 * L_90 = &(__this->___size_82);
		float L_91 = (L_90->___y_2);
		L_88->___y_2 = ((float)((float)(((float)L_89))*(float)L_91));
	}

IL_02a9:
	{
		goto IL_02c8;
	}

IL_02ae:
	{
		Vector2_t6 * L_92 = &(__this->___scrollPosition_105);
		int32_t L_93 = V_6;
		Vector2_t6 * L_94 = &(__this->___size_82);
		float L_95 = (L_94->___y_2);
		L_92->___y_2 = ((float)((float)(((float)L_93))*(float)L_95));
	}

IL_02c8:
	{
		return;
	}
}
// System.Void Reporter::DrawInfo()
extern TypeInfo* GUI_t408_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t253_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t406_il2cpp_TypeInfo_var;
extern "C" void Reporter_DrawInfo_m1103 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		Int32_t253_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		DateTime_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(650);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6  V_0 = {0};
	float V_1 = 0.0f;
	DateTime_t406  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Rect_t304  L_0 = (__this->___screenRect_95);
		GUIStyle_t302 * L_1 = (__this->___backStyle_68);
		GUILayout_BeginArea_m1645(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector2_t6  L_2 = Reporter_getDrag_m1114(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = ((&V_0)->___x_1);
		if ((((float)L_3) == ((float)(0.0f))))
		{
			goto IL_0063;
		}
	}
	{
		Vector2_t6  L_4 = (__this->___downPos_143);
		Vector2_t6  L_5 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_6 = Vector2_op_Inequality_m1646(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0063;
		}
	}
	{
		Vector2_t6 * L_7 = &(__this->___infoScrollPosition_124);
		Vector2_t6 * L_8 = L_7;
		float L_9 = (L_8->___x_1);
		float L_10 = ((&V_0)->___x_1);
		Vector2_t6 * L_11 = &(__this->___oldInfoDrag_125);
		float L_12 = (L_11->___x_1);
		L_8->___x_1 = ((float)((float)L_9-(float)((float)((float)L_10-(float)L_12))));
	}

IL_0063:
	{
		float L_13 = ((&V_0)->___y_2);
		if ((((float)L_13) == ((float)(0.0f))))
		{
			goto IL_00ae;
		}
	}
	{
		Vector2_t6  L_14 = (__this->___downPos_143);
		Vector2_t6  L_15 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_16 = Vector2_op_Inequality_m1646(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00ae;
		}
	}
	{
		Vector2_t6 * L_17 = &(__this->___infoScrollPosition_124);
		Vector2_t6 * L_18 = L_17;
		float L_19 = (L_18->___y_2);
		float L_20 = ((&V_0)->___y_2);
		Vector2_t6 * L_21 = &(__this->___oldInfoDrag_125);
		float L_22 = (L_21->___y_2);
		L_18->___y_2 = ((float)((float)L_19+(float)((float)((float)L_20-(float)L_22))));
	}

IL_00ae:
	{
		Vector2_t6  L_23 = V_0;
		__this->___oldInfoDrag_125 = L_23;
		GUISkin_t287 * L_24 = (__this->___toolbarScrollerSkin_79);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_set_skin_m1647(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Vector2_t6  L_25 = (__this->___infoScrollPosition_124);
		Vector2_t6  L_26 = GUILayout_BeginScrollView_m1648(NULL /*static, unused*/, L_25, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___infoScrollPosition_124 = L_26;
		Vector2_t6 * L_27 = &(__this->___size_82);
		float L_28 = (L_27->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_29 = &(__this->___size_82);
		float L_30 = (L_29->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		GUIContent_t301 * L_31 = (__this->___buildFromContent_56);
		GUIStyle_t302 * L_32 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_33 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_34 = &(__this->___size_82);
		float L_35 = (L_34->___x_1);
		GUILayoutOption_t410 * L_36 = GUILayout_Width_m1651(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 0);
		ArrayElementTypeCheck (L_33, L_36);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_33, 0)) = (GUILayoutOption_t410 *)L_36;
		GUILayoutOptionU5BU5D_t409* L_37 = L_33;
		Vector2_t6 * L_38 = &(__this->___size_82);
		float L_39 = (L_38->___y_2);
		GUILayoutOption_t410 * L_40 = GUILayout_Height_m1652(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 1);
		ArrayElementTypeCheck (L_37, L_40);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_37, 1)) = (GUILayoutOption_t410 *)L_40;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_31, L_32, L_37, /*hidden argument*/NULL);
		Vector2_t6 * L_41 = &(__this->___size_82);
		float L_42 = (L_41->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		String_t* L_43 = (__this->___buildDate_31);
		GUIStyle_t302 * L_44 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_45 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_46 = &(__this->___size_82);
		float L_47 = (L_46->___y_2);
		GUILayoutOption_t410 * L_48 = GUILayout_Height_m1652(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 0);
		ArrayElementTypeCheck (L_45, L_48);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_45, 0)) = (GUILayoutOption_t410 *)L_48;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_43, L_44, L_45, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_49 = &(__this->___size_82);
		float L_50 = (L_49->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		GUIContent_t301 * L_51 = (__this->___systemInfoContent_57);
		GUIStyle_t302 * L_52 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_53 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_54 = &(__this->___size_82);
		float L_55 = (L_54->___x_1);
		GUILayoutOption_t410 * L_56 = GUILayout_Width_m1651(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
		ArrayElementTypeCheck (L_53, L_56);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_53, 0)) = (GUILayoutOption_t410 *)L_56;
		GUILayoutOptionU5BU5D_t409* L_57 = L_53;
		Vector2_t6 * L_58 = &(__this->___size_82);
		float L_59 = (L_58->___y_2);
		GUILayoutOption_t410 * L_60 = GUILayout_Height_m1652(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 1);
		ArrayElementTypeCheck (L_57, L_60);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_57, 1)) = (GUILayoutOption_t410 *)L_60;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_51, L_52, L_57, /*hidden argument*/NULL);
		Vector2_t6 * L_61 = &(__this->___size_82);
		float L_62 = (L_61->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		String_t* L_63 = (__this->___deviceModel_88);
		GUIStyle_t302 * L_64 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_65 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_66 = &(__this->___size_82);
		float L_67 = (L_66->___y_2);
		GUILayoutOption_t410 * L_68 = GUILayout_Height_m1652(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, 0);
		ArrayElementTypeCheck (L_65, L_68);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_65, 0)) = (GUILayoutOption_t410 *)L_68;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_63, L_64, L_65, /*hidden argument*/NULL);
		Vector2_t6 * L_69 = &(__this->___size_82);
		float L_70 = (L_69->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		String_t* L_71 = (__this->___deviceType_89);
		GUIStyle_t302 * L_72 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_73 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_74 = &(__this->___size_82);
		float L_75 = (L_74->___y_2);
		GUILayoutOption_t410 * L_76 = GUILayout_Height_m1652(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, 0);
		ArrayElementTypeCheck (L_73, L_76);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_73, 0)) = (GUILayoutOption_t410 *)L_76;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_71, L_72, L_73, /*hidden argument*/NULL);
		Vector2_t6 * L_77 = &(__this->___size_82);
		float L_78 = (L_77->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		String_t* L_79 = (__this->___deviceName_90);
		GUIStyle_t302 * L_80 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_81 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_82 = &(__this->___size_82);
		float L_83 = (L_82->___y_2);
		GUILayoutOption_t410 * L_84 = GUILayout_Height_m1652(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, 0);
		ArrayElementTypeCheck (L_81, L_84);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_81, 0)) = (GUILayoutOption_t410 *)L_84;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_79, L_80, L_81, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_85 = &(__this->___size_82);
		float L_86 = (L_85->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		GUIContent_t301 * L_87 = (__this->___graphicsInfoContent_58);
		GUIStyle_t302 * L_88 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_89 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_90 = &(__this->___size_82);
		float L_91 = (L_90->___x_1);
		GUILayoutOption_t410 * L_92 = GUILayout_Width_m1651(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, 0);
		ArrayElementTypeCheck (L_89, L_92);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_89, 0)) = (GUILayoutOption_t410 *)L_92;
		GUILayoutOptionU5BU5D_t409* L_93 = L_89;
		Vector2_t6 * L_94 = &(__this->___size_82);
		float L_95 = (L_94->___y_2);
		GUILayoutOption_t410 * L_96 = GUILayout_Height_m1652(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, 1);
		ArrayElementTypeCheck (L_93, L_96);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_93, 1)) = (GUILayoutOption_t410 *)L_96;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_87, L_88, L_93, /*hidden argument*/NULL);
		Vector2_t6 * L_97 = &(__this->___size_82);
		float L_98 = (L_97->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_98, /*hidden argument*/NULL);
		String_t* L_99 = SystemInfo_get_graphicsDeviceName_m1657(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t302 * L_100 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_101 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_102 = &(__this->___size_82);
		float L_103 = (L_102->___y_2);
		GUILayoutOption_t410 * L_104 = GUILayout_Height_m1652(NULL /*static, unused*/, L_103, /*hidden argument*/NULL);
		NullCheck(L_101);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_101, 0);
		ArrayElementTypeCheck (L_101, L_104);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_101, 0)) = (GUILayoutOption_t410 *)L_104;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_99, L_100, L_101, /*hidden argument*/NULL);
		Vector2_t6 * L_105 = &(__this->___size_82);
		float L_106 = (L_105->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_106, /*hidden argument*/NULL);
		String_t* L_107 = (__this->___graphicsMemorySize_91);
		GUIStyle_t302 * L_108 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_109 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_110 = &(__this->___size_82);
		float L_111 = (L_110->___y_2);
		GUILayoutOption_t410 * L_112 = GUILayout_Height_m1652(NULL /*static, unused*/, L_111, /*hidden argument*/NULL);
		NullCheck(L_109);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_109, 0);
		ArrayElementTypeCheck (L_109, L_112);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_109, 0)) = (GUILayoutOption_t410 *)L_112;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_107, L_108, L_109, /*hidden argument*/NULL);
		Vector2_t6 * L_113 = &(__this->___size_82);
		float L_114 = (L_113->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_114, /*hidden argument*/NULL);
		String_t* L_115 = (__this->___maxTextureSize_92);
		GUIStyle_t302 * L_116 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_117 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_118 = &(__this->___size_82);
		float L_119 = (L_118->___y_2);
		GUILayoutOption_t410 * L_120 = GUILayout_Height_m1652(NULL /*static, unused*/, L_119, /*hidden argument*/NULL);
		NullCheck(L_117);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_117, 0);
		ArrayElementTypeCheck (L_117, L_120);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_117, 0)) = (GUILayoutOption_t410 *)L_120;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_115, L_116, L_117, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_121 = &(__this->___size_82);
		float L_122 = (L_121->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_122, /*hidden argument*/NULL);
		Vector2_t6 * L_123 = &(__this->___size_82);
		float L_124 = (L_123->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_124, /*hidden argument*/NULL);
		Vector2_t6 * L_125 = &(__this->___size_82);
		float L_126 = (L_125->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_126, /*hidden argument*/NULL);
		int32_t L_127 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_128 = L_127;
		Object_t * L_129 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_128);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_130 = String_Concat_m954(NULL /*static, unused*/, (String_t*) &_stringLiteral241, L_129, /*hidden argument*/NULL);
		GUIStyle_t302 * L_131 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_132 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_133 = &(__this->___size_82);
		float L_134 = (L_133->___y_2);
		GUILayoutOption_t410 * L_135 = GUILayout_Height_m1652(NULL /*static, unused*/, L_134, /*hidden argument*/NULL);
		NullCheck(L_132);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_132, 0);
		ArrayElementTypeCheck (L_132, L_135);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_132, 0)) = (GUILayoutOption_t410 *)L_135;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_130, L_131, L_132, /*hidden argument*/NULL);
		Vector2_t6 * L_136 = &(__this->___size_82);
		float L_137 = (L_136->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_137, /*hidden argument*/NULL);
		int32_t L_138 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_139 = L_138;
		Object_t * L_140 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_139);
		String_t* L_141 = String_Concat_m954(NULL /*static, unused*/, (String_t*) &_stringLiteral242, L_140, /*hidden argument*/NULL);
		GUIStyle_t302 * L_142 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_143 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_144 = &(__this->___size_82);
		float L_145 = (L_144->___y_2);
		GUILayoutOption_t410 * L_146 = GUILayout_Height_m1652(NULL /*static, unused*/, L_145, /*hidden argument*/NULL);
		NullCheck(L_143);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_143, 0);
		ArrayElementTypeCheck (L_143, L_146);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_143, 0)) = (GUILayoutOption_t410 *)L_146;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_141, L_142, L_143, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_147 = &(__this->___size_82);
		float L_148 = (L_147->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_148, /*hidden argument*/NULL);
		GUIContent_t301 * L_149 = (__this->___showMemoryContent_48);
		GUIStyle_t302 * L_150 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_151 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_152 = &(__this->___size_82);
		float L_153 = (L_152->___x_1);
		GUILayoutOption_t410 * L_154 = GUILayout_Width_m1651(NULL /*static, unused*/, L_153, /*hidden argument*/NULL);
		NullCheck(L_151);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_151, 0);
		ArrayElementTypeCheck (L_151, L_154);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_151, 0)) = (GUILayoutOption_t410 *)L_154;
		GUILayoutOptionU5BU5D_t409* L_155 = L_151;
		Vector2_t6 * L_156 = &(__this->___size_82);
		float L_157 = (L_156->___y_2);
		GUILayoutOption_t410 * L_158 = GUILayout_Height_m1652(NULL /*static, unused*/, L_157, /*hidden argument*/NULL);
		NullCheck(L_155);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_155, 1);
		ArrayElementTypeCheck (L_155, L_158);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_155, 1)) = (GUILayoutOption_t410 *)L_158;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_149, L_150, L_155, /*hidden argument*/NULL);
		Vector2_t6 * L_159 = &(__this->___size_82);
		float L_160 = (L_159->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_160, /*hidden argument*/NULL);
		String_t* L_161 = (__this->___systemMemorySize_93);
		String_t* L_162 = String_Concat_m860(NULL /*static, unused*/, L_161, (String_t*) &_stringLiteral243, /*hidden argument*/NULL);
		GUIStyle_t302 * L_163 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_164 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_165 = &(__this->___size_82);
		float L_166 = (L_165->___y_2);
		GUILayoutOption_t410 * L_167 = GUILayout_Height_m1652(NULL /*static, unused*/, L_166, /*hidden argument*/NULL);
		NullCheck(L_164);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_164, 0);
		ArrayElementTypeCheck (L_164, L_167);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_164, 0)) = (GUILayoutOption_t410 *)L_167;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_162, L_163, L_164, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_168 = &(__this->___size_82);
		float L_169 = (L_168->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_169, /*hidden argument*/NULL);
		Vector2_t6 * L_170 = &(__this->___size_82);
		float L_171 = (L_170->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_171, /*hidden argument*/NULL);
		Vector2_t6 * L_172 = &(__this->___size_82);
		float L_173 = (L_172->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_173, /*hidden argument*/NULL);
		float* L_174 = &(__this->___logsMemUsage_33);
		String_t* L_175 = Single_ToString_m1658(L_174, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		String_t* L_176 = String_Concat_m734(NULL /*static, unused*/, (String_t*) &_stringLiteral244, L_175, (String_t*) &_stringLiteral243, /*hidden argument*/NULL);
		GUIStyle_t302 * L_177 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_178 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_179 = &(__this->___size_82);
		float L_180 = (L_179->___y_2);
		GUILayoutOption_t410 * L_181 = GUILayout_Height_m1652(NULL /*static, unused*/, L_180, /*hidden argument*/NULL);
		NullCheck(L_178);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_178, 0);
		ArrayElementTypeCheck (L_178, L_181);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_178, 0)) = (GUILayoutOption_t410 *)L_181;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_176, L_177, L_178, /*hidden argument*/NULL);
		Vector2_t6 * L_182 = &(__this->___size_82);
		float L_183 = (L_182->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_183, /*hidden argument*/NULL);
		float* L_184 = &(__this->___gcTotalMemory_35);
		String_t* L_185 = Single_ToString_m1658(L_184, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		String_t* L_186 = String_Concat_m734(NULL /*static, unused*/, (String_t*) &_stringLiteral246, L_185, (String_t*) &_stringLiteral243, /*hidden argument*/NULL);
		GUIStyle_t302 * L_187 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_188 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_189 = &(__this->___size_82);
		float L_190 = (L_189->___y_2);
		GUILayoutOption_t410 * L_191 = GUILayout_Height_m1652(NULL /*static, unused*/, L_190, /*hidden argument*/NULL);
		NullCheck(L_188);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_188, 0);
		ArrayElementTypeCheck (L_188, L_191);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_188, 0)) = (GUILayoutOption_t410 *)L_191;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_186, L_187, L_188, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_192 = &(__this->___size_82);
		float L_193 = (L_192->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_193, /*hidden argument*/NULL);
		GUIContent_t301 * L_194 = (__this->___softwareContent_49);
		GUIStyle_t302 * L_195 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_196 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_197 = &(__this->___size_82);
		float L_198 = (L_197->___x_1);
		GUILayoutOption_t410 * L_199 = GUILayout_Width_m1651(NULL /*static, unused*/, L_198, /*hidden argument*/NULL);
		NullCheck(L_196);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_196, 0);
		ArrayElementTypeCheck (L_196, L_199);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_196, 0)) = (GUILayoutOption_t410 *)L_199;
		GUILayoutOptionU5BU5D_t409* L_200 = L_196;
		Vector2_t6 * L_201 = &(__this->___size_82);
		float L_202 = (L_201->___y_2);
		GUILayoutOption_t410 * L_203 = GUILayout_Height_m1652(NULL /*static, unused*/, L_202, /*hidden argument*/NULL);
		NullCheck(L_200);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_200, 1);
		ArrayElementTypeCheck (L_200, L_203);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_200, 1)) = (GUILayoutOption_t410 *)L_203;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_194, L_195, L_200, /*hidden argument*/NULL);
		Vector2_t6 * L_204 = &(__this->___size_82);
		float L_205 = (L_204->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_205, /*hidden argument*/NULL);
		String_t* L_206 = SystemInfo_get_operatingSystem_m1659(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t302 * L_207 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_208 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_209 = &(__this->___size_82);
		float L_210 = (L_209->___y_2);
		GUILayoutOption_t410 * L_211 = GUILayout_Height_m1652(NULL /*static, unused*/, L_210, /*hidden argument*/NULL);
		NullCheck(L_208);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_208, 0);
		ArrayElementTypeCheck (L_208, L_211);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_208, 0)) = (GUILayoutOption_t410 *)L_211;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_206, L_207, L_208, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_212 = &(__this->___size_82);
		float L_213 = (L_212->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_213, /*hidden argument*/NULL);
		GUIContent_t301 * L_214 = (__this->___dateContent_50);
		GUIStyle_t302 * L_215 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_216 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_217 = &(__this->___size_82);
		float L_218 = (L_217->___x_1);
		GUILayoutOption_t410 * L_219 = GUILayout_Width_m1651(NULL /*static, unused*/, L_218, /*hidden argument*/NULL);
		NullCheck(L_216);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_216, 0);
		ArrayElementTypeCheck (L_216, L_219);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_216, 0)) = (GUILayoutOption_t410 *)L_219;
		GUILayoutOptionU5BU5D_t409* L_220 = L_216;
		Vector2_t6 * L_221 = &(__this->___size_82);
		float L_222 = (L_221->___y_2);
		GUILayoutOption_t410 * L_223 = GUILayout_Height_m1652(NULL /*static, unused*/, L_222, /*hidden argument*/NULL);
		NullCheck(L_220);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_220, 1);
		ArrayElementTypeCheck (L_220, L_223);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_220, 1)) = (GUILayoutOption_t410 *)L_223;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_214, L_215, L_220, /*hidden argument*/NULL);
		Vector2_t6 * L_224 = &(__this->___size_82);
		float L_225 = (L_224->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_225, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t406_il2cpp_TypeInfo_var);
		DateTime_t406  L_226 = DateTime_get_Now_m1639(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_226;
		String_t* L_227 = DateTime_ToString_m1640((&V_2), /*hidden argument*/NULL);
		GUIStyle_t302 * L_228 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_229 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_230 = &(__this->___size_82);
		float L_231 = (L_230->___y_2);
		GUILayoutOption_t410 * L_232 = GUILayout_Height_m1652(NULL /*static, unused*/, L_231, /*hidden argument*/NULL);
		NullCheck(L_229);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_229, 0);
		ArrayElementTypeCheck (L_229, L_232);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_229, 0)) = (GUILayoutOption_t410 *)L_232;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_227, L_228, L_229, /*hidden argument*/NULL);
		String_t* L_233 = (__this->___logDate_32);
		String_t* L_234 = String_Concat_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral247, L_233, /*hidden argument*/NULL);
		GUIStyle_t302 * L_235 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_236 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_237 = &(__this->___size_82);
		float L_238 = (L_237->___y_2);
		GUILayoutOption_t410 * L_239 = GUILayout_Height_m1652(NULL /*static, unused*/, L_238, /*hidden argument*/NULL);
		NullCheck(L_236);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_236, 0);
		ArrayElementTypeCheck (L_236, L_239);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_236, 0)) = (GUILayoutOption_t410 *)L_239;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_234, L_235, L_236, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_240 = &(__this->___size_82);
		float L_241 = (L_240->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_241, /*hidden argument*/NULL);
		GUIContent_t301 * L_242 = (__this->___showTimeContent_45);
		GUIStyle_t302 * L_243 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_244 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_245 = &(__this->___size_82);
		float L_246 = (L_245->___x_1);
		GUILayoutOption_t410 * L_247 = GUILayout_Width_m1651(NULL /*static, unused*/, L_246, /*hidden argument*/NULL);
		NullCheck(L_244);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_244, 0);
		ArrayElementTypeCheck (L_244, L_247);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_244, 0)) = (GUILayoutOption_t410 *)L_247;
		GUILayoutOptionU5BU5D_t409* L_248 = L_244;
		Vector2_t6 * L_249 = &(__this->___size_82);
		float L_250 = (L_249->___y_2);
		GUILayoutOption_t410 * L_251 = GUILayout_Height_m1652(NULL /*static, unused*/, L_250, /*hidden argument*/NULL);
		NullCheck(L_248);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_248, 1);
		ArrayElementTypeCheck (L_248, L_251);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_248, 1)) = (GUILayoutOption_t410 *)L_251;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_242, L_243, L_248, /*hidden argument*/NULL);
		Vector2_t6 * L_252 = &(__this->___size_82);
		float L_253 = (L_252->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_253, /*hidden argument*/NULL);
		float L_254 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_254;
		String_t* L_255 = Single_ToString_m1658((&V_3), (String_t*) &_stringLiteral248, /*hidden argument*/NULL);
		GUIStyle_t302 * L_256 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_257 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_258 = &(__this->___size_82);
		float L_259 = (L_258->___y_2);
		GUILayoutOption_t410 * L_260 = GUILayout_Height_m1652(NULL /*static, unused*/, L_259, /*hidden argument*/NULL);
		NullCheck(L_257);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_257, 0);
		ArrayElementTypeCheck (L_257, L_260);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_257, 0)) = (GUILayoutOption_t410 *)L_260;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_255, L_256, L_257, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_261 = &(__this->___size_82);
		float L_262 = (L_261->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_262, /*hidden argument*/NULL);
		GUIContent_t301 * L_263 = (__this->___showFpsContent_51);
		GUIStyle_t302 * L_264 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_265 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_266 = &(__this->___size_82);
		float L_267 = (L_266->___x_1);
		GUILayoutOption_t410 * L_268 = GUILayout_Width_m1651(NULL /*static, unused*/, L_267, /*hidden argument*/NULL);
		NullCheck(L_265);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_265, 0);
		ArrayElementTypeCheck (L_265, L_268);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_265, 0)) = (GUILayoutOption_t410 *)L_268;
		GUILayoutOptionU5BU5D_t409* L_269 = L_265;
		Vector2_t6 * L_270 = &(__this->___size_82);
		float L_271 = (L_270->___y_2);
		GUILayoutOption_t410 * L_272 = GUILayout_Height_m1652(NULL /*static, unused*/, L_271, /*hidden argument*/NULL);
		NullCheck(L_269);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_269, 1);
		ArrayElementTypeCheck (L_269, L_272);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_269, 1)) = (GUILayoutOption_t410 *)L_272;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_263, L_264, L_269, /*hidden argument*/NULL);
		Vector2_t6 * L_273 = &(__this->___size_82);
		float L_274 = (L_273->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_274, /*hidden argument*/NULL);
		String_t* L_275 = (__this->___fpsText_38);
		GUIStyle_t302 * L_276 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_277 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_278 = &(__this->___size_82);
		float L_279 = (L_278->___y_2);
		GUILayoutOption_t410 * L_280 = GUILayout_Height_m1652(NULL /*static, unused*/, L_279, /*hidden argument*/NULL);
		NullCheck(L_277);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_277, 0);
		ArrayElementTypeCheck (L_277, L_280);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_277, 0)) = (GUILayoutOption_t410 *)L_280;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_275, L_276, L_277, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_281 = &(__this->___size_82);
		float L_282 = (L_281->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_282, /*hidden argument*/NULL);
		GUIContent_t301 * L_283 = (__this->___userContent_47);
		GUIStyle_t302 * L_284 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_285 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_286 = &(__this->___size_82);
		float L_287 = (L_286->___x_1);
		GUILayoutOption_t410 * L_288 = GUILayout_Width_m1651(NULL /*static, unused*/, L_287, /*hidden argument*/NULL);
		NullCheck(L_285);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_285, 0);
		ArrayElementTypeCheck (L_285, L_288);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_285, 0)) = (GUILayoutOption_t410 *)L_288;
		GUILayoutOptionU5BU5D_t409* L_289 = L_285;
		Vector2_t6 * L_290 = &(__this->___size_82);
		float L_291 = (L_290->___y_2);
		GUILayoutOption_t410 * L_292 = GUILayout_Height_m1652(NULL /*static, unused*/, L_291, /*hidden argument*/NULL);
		NullCheck(L_289);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_289, 1);
		ArrayElementTypeCheck (L_289, L_292);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_289, 1)) = (GUILayoutOption_t410 *)L_292;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_283, L_284, L_289, /*hidden argument*/NULL);
		Vector2_t6 * L_293 = &(__this->___size_82);
		float L_294 = (L_293->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_294, /*hidden argument*/NULL);
		String_t* L_295 = (__this->___UserData_36);
		GUIStyle_t302 * L_296 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_297 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_298 = &(__this->___size_82);
		float L_299 = (L_298->___y_2);
		GUILayoutOption_t410 * L_300 = GUILayout_Height_m1652(NULL /*static, unused*/, L_299, /*hidden argument*/NULL);
		NullCheck(L_297);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_297, 0);
		ArrayElementTypeCheck (L_297, L_300);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_297, 0)) = (GUILayoutOption_t410 *)L_300;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_295, L_296, L_297, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_301 = &(__this->___size_82);
		float L_302 = (L_301->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_302, /*hidden argument*/NULL);
		GUIContent_t301 * L_303 = (__this->___showSceneContent_46);
		GUIStyle_t302 * L_304 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_305 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_306 = &(__this->___size_82);
		float L_307 = (L_306->___x_1);
		GUILayoutOption_t410 * L_308 = GUILayout_Width_m1651(NULL /*static, unused*/, L_307, /*hidden argument*/NULL);
		NullCheck(L_305);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_305, 0);
		ArrayElementTypeCheck (L_305, L_308);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_305, 0)) = (GUILayoutOption_t410 *)L_308;
		GUILayoutOptionU5BU5D_t409* L_309 = L_305;
		Vector2_t6 * L_310 = &(__this->___size_82);
		float L_311 = (L_310->___y_2);
		GUILayoutOption_t410 * L_312 = GUILayout_Height_m1652(NULL /*static, unused*/, L_311, /*hidden argument*/NULL);
		NullCheck(L_309);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_309, 1);
		ArrayElementTypeCheck (L_309, L_312);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_309, 1)) = (GUILayoutOption_t410 *)L_312;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_303, L_304, L_309, /*hidden argument*/NULL);
		Vector2_t6 * L_313 = &(__this->___size_82);
		float L_314 = (L_313->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_314, /*hidden argument*/NULL);
		String_t* L_315 = (__this->___currentScene_86);
		GUIStyle_t302 * L_316 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_317 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_318 = &(__this->___size_82);
		float L_319 = (L_318->___y_2);
		GUILayoutOption_t410 * L_320 = GUILayout_Height_m1652(NULL /*static, unused*/, L_319, /*hidden argument*/NULL);
		NullCheck(L_317);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_317, 0);
		ArrayElementTypeCheck (L_317, L_320);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_317, 0)) = (GUILayoutOption_t410 *)L_320;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_315, L_316, L_317, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_321 = &(__this->___size_82);
		float L_322 = (L_321->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_322, /*hidden argument*/NULL);
		GUIContent_t301 * L_323 = (__this->___showSceneContent_46);
		GUIStyle_t302 * L_324 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_325 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_326 = &(__this->___size_82);
		float L_327 = (L_326->___x_1);
		GUILayoutOption_t410 * L_328 = GUILayout_Width_m1651(NULL /*static, unused*/, L_327, /*hidden argument*/NULL);
		NullCheck(L_325);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_325, 0);
		ArrayElementTypeCheck (L_325, L_328);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_325, 0)) = (GUILayoutOption_t410 *)L_328;
		GUILayoutOptionU5BU5D_t409* L_329 = L_325;
		Vector2_t6 * L_330 = &(__this->___size_82);
		float L_331 = (L_330->___y_2);
		GUILayoutOption_t410 * L_332 = GUILayout_Height_m1652(NULL /*static, unused*/, L_331, /*hidden argument*/NULL);
		NullCheck(L_329);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_329, 1);
		ArrayElementTypeCheck (L_329, L_332);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_329, 1)) = (GUILayoutOption_t410 *)L_332;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_323, L_324, L_329, /*hidden argument*/NULL);
		Vector2_t6 * L_333 = &(__this->___size_82);
		float L_334 = (L_333->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_334, /*hidden argument*/NULL);
		String_t* L_335 = Application_get_unityVersion_m1660(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_336 = String_Concat_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral249, L_335, /*hidden argument*/NULL);
		GUIStyle_t302 * L_337 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_338 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_339 = &(__this->___size_82);
		float L_340 = (L_339->___y_2);
		GUILayoutOption_t410 * L_341 = GUILayout_Height_m1652(NULL /*static, unused*/, L_340, /*hidden argument*/NULL);
		NullCheck(L_338);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_338, 0);
		ArrayElementTypeCheck (L_338, L_341);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_338, 0)) = (GUILayoutOption_t410 *)L_341;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_336, L_337, L_338, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		Reporter_drawInfo_enableDisableToolBarButtons_m1104(__this, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_342 = &(__this->___size_82);
		float L_343 = (L_342->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_343, /*hidden argument*/NULL);
		Vector2_t6 * L_344 = &(__this->___size_82);
		float* L_345 = &(L_344->___x_1);
		String_t* L_346 = Single_ToString_m1658(L_345, (String_t*) &_stringLiteral251, /*hidden argument*/NULL);
		String_t* L_347 = String_Concat_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral250, L_346, /*hidden argument*/NULL);
		GUIStyle_t302 * L_348 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_349 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_350 = &(__this->___size_82);
		float L_351 = (L_350->___y_2);
		GUILayoutOption_t410 * L_352 = GUILayout_Height_m1652(NULL /*static, unused*/, L_351, /*hidden argument*/NULL);
		NullCheck(L_349);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_349, 0);
		ArrayElementTypeCheck (L_349, L_352);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_349, 0)) = (GUILayoutOption_t410 *)L_352;
		GUILayout_Label_m1654(NULL /*static, unused*/, L_347, L_348, L_349, /*hidden argument*/NULL);
		Vector2_t6 * L_353 = &(__this->___size_82);
		float L_354 = (L_353->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_354, /*hidden argument*/NULL);
		Vector2_t6 * L_355 = &(__this->___size_82);
		float L_356 = (L_355->___x_1);
		GUIStyle_t302 * L_357 = (__this->___sliderBackStyle_77);
		GUIStyle_t302 * L_358 = (__this->___sliderThumbStyle_78);
		GUILayoutOptionU5BU5D_t409* L_359 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		int32_t L_360 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t410 * L_361 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)(((float)L_360))*(float)(0.5f))), /*hidden argument*/NULL);
		NullCheck(L_359);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_359, 0);
		ArrayElementTypeCheck (L_359, L_361);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_359, 0)) = (GUILayoutOption_t410 *)L_361;
		float L_362 = GUILayout_HorizontalSlider_m1661(NULL /*static, unused*/, L_356, (16.0f), (64.0f), L_357, L_358, L_359, /*hidden argument*/NULL);
		V_1 = L_362;
		Vector2_t6 * L_363 = &(__this->___size_82);
		float L_364 = (L_363->___x_1);
		float L_365 = V_1;
		if ((((float)L_364) == ((float)L_365)))
		{
			goto IL_0b68;
		}
	}
	{
		Vector2_t6 * L_366 = &(__this->___size_82);
		Vector2_t6 * L_367 = &(__this->___size_82);
		float L_368 = V_1;
		float L_369 = L_368;
		V_4 = L_369;
		L_367->___y_2 = L_369;
		float L_370 = V_4;
		L_366->___x_1 = L_370;
		Reporter_initializeStyle_m1099(__this, /*hidden argument*/NULL);
	}

IL_0b68:
	{
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_371 = &(__this->___size_82);
		float L_372 = (L_371->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_372, /*hidden argument*/NULL);
		GUIContent_t301 * L_373 = (__this->___backContent_59);
		GUIStyle_t302 * L_374 = (__this->___barStyle_64);
		GUILayoutOptionU5BU5D_t409* L_375 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_376 = &(__this->___size_82);
		float L_377 = (L_376->___x_1);
		GUILayoutOption_t410 * L_378 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_377*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_375);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_375, 0);
		ArrayElementTypeCheck (L_375, L_378);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_375, 0)) = (GUILayoutOption_t410 *)L_378;
		GUILayoutOptionU5BU5D_t409* L_379 = L_375;
		Vector2_t6 * L_380 = &(__this->___size_82);
		float L_381 = (L_380->___y_2);
		GUILayoutOption_t410 * L_382 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_381*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_379);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_379, 1);
		ArrayElementTypeCheck (L_379, L_382);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_379, 1)) = (GUILayoutOption_t410 *)L_382;
		bool L_383 = GUILayout_Button_m1662(NULL /*static, unused*/, L_373, L_374, L_379, /*hidden argument*/NULL);
		if (!L_383)
		{
			goto IL_0be2;
		}
	}
	{
		__this->___currentView_39 = 1;
	}

IL_0be2:
	{
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndScrollView_m1663(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reporter::drawInfo_enableDisableToolBarButtons()
extern TypeInfo* GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t411_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t408_il2cpp_TypeInfo_var;
extern "C" void Reporter_drawInfo_enableDisableToolBarButtons_m1104 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		GUILayoutUtility_t411_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(654);
		GUI_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	GUIContent_t301 * G_B2_0 = {0};
	GUIContent_t301 * G_B1_0 = {0};
	GUIStyle_t302 * G_B3_0 = {0};
	GUIContent_t301 * G_B3_1 = {0};
	GUIContent_t301 * G_B7_0 = {0};
	GUIContent_t301 * G_B6_0 = {0};
	GUIStyle_t302 * G_B8_0 = {0};
	GUIContent_t301 * G_B8_1 = {0};
	GUIContent_t301 * G_B12_0 = {0};
	GUIContent_t301 * G_B11_0 = {0};
	GUIStyle_t302 * G_B13_0 = {0};
	GUIContent_t301 * G_B13_1 = {0};
	GUIContent_t301 * G_B17_0 = {0};
	GUIContent_t301 * G_B16_0 = {0};
	GUIStyle_t302 * G_B18_0 = {0};
	GUIContent_t301 * G_B18_1 = {0};
	GUIContent_t301 * G_B22_0 = {0};
	GUIContent_t301 * G_B21_0 = {0};
	GUIStyle_t302 * G_B23_0 = {0};
	GUIContent_t301 * G_B23_1 = {0};
	GUIContent_t301 * G_B27_0 = {0};
	GUIContent_t301 * G_B26_0 = {0};
	GUIStyle_t302 * G_B28_0 = {0};
	GUIContent_t301 * G_B28_1 = {0};
	{
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_0 = &(__this->___size_82);
		float L_1 = (L_0->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GUIStyle_t302 * L_2 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_3 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_4 = &(__this->___size_82);
		float L_5 = (L_4->___y_2);
		GUILayoutOption_t410 * L_6 = GUILayout_Height_m1652(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_6);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_3, 0)) = (GUILayoutOption_t410 *)L_6;
		GUILayout_Label_m1654(NULL /*static, unused*/, (String_t*) &_stringLiteral252, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t6 * L_7 = &(__this->___size_82);
		float L_8 = (L_7->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_9 = &(__this->___size_82);
		float L_10 = (L_9->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		GUIContent_t301 * L_11 = (__this->___clearOnNewSceneContent_44);
		bool L_12 = (__this->___showClearOnNewSceneLoadedButton_25);
		G_B1_0 = L_11;
		if (!L_12)
		{
			G_B2_0 = L_11;
			goto IL_0095;
		}
	}
	{
		GUIStyle_t302 * L_13 = (__this->___buttonActiveStyle_65);
		G_B3_0 = L_13;
		G_B3_1 = G_B1_0;
		goto IL_009b;
	}

IL_0095:
	{
		GUIStyle_t302 * L_14 = (__this->___barStyle_64);
		G_B3_0 = L_14;
		G_B3_1 = G_B2_0;
	}

IL_009b:
	{
		GUILayoutOptionU5BU5D_t409* L_15 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_16 = &(__this->___size_82);
		float L_17 = (L_16->___x_1);
		GUILayoutOption_t410 * L_18 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_17*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_18);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_15, 0)) = (GUILayoutOption_t410 *)L_18;
		GUILayoutOptionU5BU5D_t409* L_19 = L_15;
		Vector2_t6 * L_20 = &(__this->___size_82);
		float L_21 = (L_20->___y_2);
		GUILayoutOption_t410 * L_22 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_21*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		ArrayElementTypeCheck (L_19, L_22);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_19, 1)) = (GUILayoutOption_t410 *)L_22;
		bool L_23 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B3_1, G_B3_0, L_19, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00ec;
		}
	}
	{
		bool L_24 = (__this->___showClearOnNewSceneLoadedButton_25);
		__this->___showClearOnNewSceneLoadedButton_25 = ((((int32_t)L_24) == ((int32_t)0))? 1 : 0);
	}

IL_00ec:
	{
		GUIContent_t301 * L_25 = (__this->___showTimeContent_45);
		bool L_26 = (__this->___showTimeButton_26);
		G_B6_0 = L_25;
		if (!L_26)
		{
			G_B7_0 = L_25;
			goto IL_0108;
		}
	}
	{
		GUIStyle_t302 * L_27 = (__this->___buttonActiveStyle_65);
		G_B8_0 = L_27;
		G_B8_1 = G_B6_0;
		goto IL_010e;
	}

IL_0108:
	{
		GUIStyle_t302 * L_28 = (__this->___barStyle_64);
		G_B8_0 = L_28;
		G_B8_1 = G_B7_0;
	}

IL_010e:
	{
		GUILayoutOptionU5BU5D_t409* L_29 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_30 = &(__this->___size_82);
		float L_31 = (L_30->___x_1);
		GUILayoutOption_t410 * L_32 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_31*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 0);
		ArrayElementTypeCheck (L_29, L_32);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_29, 0)) = (GUILayoutOption_t410 *)L_32;
		GUILayoutOptionU5BU5D_t409* L_33 = L_29;
		Vector2_t6 * L_34 = &(__this->___size_82);
		float L_35 = (L_34->___y_2);
		GUILayoutOption_t410 * L_36 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_35*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 1);
		ArrayElementTypeCheck (L_33, L_36);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_33, 1)) = (GUILayoutOption_t410 *)L_36;
		bool L_37 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B8_1, G_B8_0, L_33, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_015f;
		}
	}
	{
		bool L_38 = (__this->___showTimeButton_26);
		__this->___showTimeButton_26 = ((((int32_t)L_38) == ((int32_t)0))? 1 : 0);
	}

IL_015f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t411_il2cpp_TypeInfo_var);
		Rect_t304  L_39 = GUILayoutUtility_GetLastRect_m1665(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tempRect_126 = L_39;
		Rect_t304  L_40 = (__this->___tempRect_126);
		float L_41 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_41;
		String_t* L_42 = Single_ToString_m1658((&V_0), (String_t*) &_stringLiteral251, /*hidden argument*/NULL);
		GUIStyle_t302 * L_43 = (__this->___lowerLeftFontStyle_67);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Label_m1666(NULL /*static, unused*/, L_40, L_42, L_43, /*hidden argument*/NULL);
		GUIContent_t301 * L_44 = (__this->___showSceneContent_46);
		bool L_45 = (__this->___showSceneButton_27);
		G_B11_0 = L_44;
		if (!L_45)
		{
			G_B12_0 = L_44;
			goto IL_01a9;
		}
	}
	{
		GUIStyle_t302 * L_46 = (__this->___buttonActiveStyle_65);
		G_B13_0 = L_46;
		G_B13_1 = G_B11_0;
		goto IL_01af;
	}

IL_01a9:
	{
		GUIStyle_t302 * L_47 = (__this->___barStyle_64);
		G_B13_0 = L_47;
		G_B13_1 = G_B12_0;
	}

IL_01af:
	{
		GUILayoutOptionU5BU5D_t409* L_48 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_49 = &(__this->___size_82);
		float L_50 = (L_49->___x_1);
		GUILayoutOption_t410 * L_51 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_50*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 0);
		ArrayElementTypeCheck (L_48, L_51);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_48, 0)) = (GUILayoutOption_t410 *)L_51;
		GUILayoutOptionU5BU5D_t409* L_52 = L_48;
		Vector2_t6 * L_53 = &(__this->___size_82);
		float L_54 = (L_53->___y_2);
		GUILayoutOption_t410 * L_55 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_54*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 1);
		ArrayElementTypeCheck (L_52, L_55);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_52, 1)) = (GUILayoutOption_t410 *)L_55;
		bool L_56 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B13_1, G_B13_0, L_52, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_0200;
		}
	}
	{
		bool L_57 = (__this->___showSceneButton_27);
		__this->___showSceneButton_27 = ((((int32_t)L_57) == ((int32_t)0))? 1 : 0);
	}

IL_0200:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t411_il2cpp_TypeInfo_var);
		Rect_t304  L_58 = GUILayoutUtility_GetLastRect_m1665(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tempRect_126 = L_58;
		Rect_t304  L_59 = (__this->___tempRect_126);
		String_t* L_60 = (__this->___currentScene_86);
		GUIStyle_t302 * L_61 = (__this->___lowerLeftFontStyle_67);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Label_m1666(NULL /*static, unused*/, L_59, L_60, L_61, /*hidden argument*/NULL);
		GUIContent_t301 * L_62 = (__this->___showMemoryContent_48);
		bool L_63 = (__this->___showMemButton_28);
		G_B16_0 = L_62;
		if (!L_63)
		{
			G_B17_0 = L_62;
			goto IL_023e;
		}
	}
	{
		GUIStyle_t302 * L_64 = (__this->___buttonActiveStyle_65);
		G_B18_0 = L_64;
		G_B18_1 = G_B16_0;
		goto IL_0244;
	}

IL_023e:
	{
		GUIStyle_t302 * L_65 = (__this->___barStyle_64);
		G_B18_0 = L_65;
		G_B18_1 = G_B17_0;
	}

IL_0244:
	{
		GUILayoutOptionU5BU5D_t409* L_66 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_67 = &(__this->___size_82);
		float L_68 = (L_67->___x_1);
		GUILayoutOption_t410 * L_69 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_68*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, 0);
		ArrayElementTypeCheck (L_66, L_69);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_66, 0)) = (GUILayoutOption_t410 *)L_69;
		GUILayoutOptionU5BU5D_t409* L_70 = L_66;
		Vector2_t6 * L_71 = &(__this->___size_82);
		float L_72 = (L_71->___y_2);
		GUILayoutOption_t410 * L_73 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_72*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, 1);
		ArrayElementTypeCheck (L_70, L_73);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_70, 1)) = (GUILayoutOption_t410 *)L_73;
		bool L_74 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B18_1, G_B18_0, L_70, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_0295;
		}
	}
	{
		bool L_75 = (__this->___showMemButton_28);
		__this->___showMemButton_28 = ((((int32_t)L_75) == ((int32_t)0))? 1 : 0);
	}

IL_0295:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t411_il2cpp_TypeInfo_var);
		Rect_t304  L_76 = GUILayoutUtility_GetLastRect_m1665(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tempRect_126 = L_76;
		Rect_t304  L_77 = (__this->___tempRect_126);
		float* L_78 = &(__this->___gcTotalMemory_35);
		String_t* L_79 = Single_ToString_m1658(L_78, (String_t*) &_stringLiteral251, /*hidden argument*/NULL);
		GUIStyle_t302 * L_80 = (__this->___lowerLeftFontStyle_67);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Label_m1666(NULL /*static, unused*/, L_77, L_79, L_80, /*hidden argument*/NULL);
		GUIContent_t301 * L_81 = (__this->___showFpsContent_51);
		bool L_82 = (__this->___showFpsButton_29);
		G_B21_0 = L_81;
		if (!L_82)
		{
			G_B22_0 = L_81;
			goto IL_02dd;
		}
	}
	{
		GUIStyle_t302 * L_83 = (__this->___buttonActiveStyle_65);
		G_B23_0 = L_83;
		G_B23_1 = G_B21_0;
		goto IL_02e3;
	}

IL_02dd:
	{
		GUIStyle_t302 * L_84 = (__this->___barStyle_64);
		G_B23_0 = L_84;
		G_B23_1 = G_B22_0;
	}

IL_02e3:
	{
		GUILayoutOptionU5BU5D_t409* L_85 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_86 = &(__this->___size_82);
		float L_87 = (L_86->___x_1);
		GUILayoutOption_t410 * L_88 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_87*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, 0);
		ArrayElementTypeCheck (L_85, L_88);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_85, 0)) = (GUILayoutOption_t410 *)L_88;
		GUILayoutOptionU5BU5D_t409* L_89 = L_85;
		Vector2_t6 * L_90 = &(__this->___size_82);
		float L_91 = (L_90->___y_2);
		GUILayoutOption_t410 * L_92 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_91*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, 1);
		ArrayElementTypeCheck (L_89, L_92);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_89, 1)) = (GUILayoutOption_t410 *)L_92;
		bool L_93 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B23_1, G_B23_0, L_89, /*hidden argument*/NULL);
		if (!L_93)
		{
			goto IL_0334;
		}
	}
	{
		bool L_94 = (__this->___showFpsButton_29);
		__this->___showFpsButton_29 = ((((int32_t)L_94) == ((int32_t)0))? 1 : 0);
	}

IL_0334:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t411_il2cpp_TypeInfo_var);
		Rect_t304  L_95 = GUILayoutUtility_GetLastRect_m1665(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tempRect_126 = L_95;
		Rect_t304  L_96 = (__this->___tempRect_126);
		String_t* L_97 = (__this->___fpsText_38);
		GUIStyle_t302 * L_98 = (__this->___lowerLeftFontStyle_67);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Label_m1666(NULL /*static, unused*/, L_96, L_97, L_98, /*hidden argument*/NULL);
		GUIContent_t301 * L_99 = (__this->___searchContent_54);
		bool L_100 = (__this->___showSearchText_30);
		G_B26_0 = L_99;
		if (!L_100)
		{
			G_B27_0 = L_99;
			goto IL_0372;
		}
	}
	{
		GUIStyle_t302 * L_101 = (__this->___buttonActiveStyle_65);
		G_B28_0 = L_101;
		G_B28_1 = G_B26_0;
		goto IL_0378;
	}

IL_0372:
	{
		GUIStyle_t302 * L_102 = (__this->___barStyle_64);
		G_B28_0 = L_102;
		G_B28_1 = G_B27_0;
	}

IL_0378:
	{
		GUILayoutOptionU5BU5D_t409* L_103 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_104 = &(__this->___size_82);
		float L_105 = (L_104->___x_1);
		GUILayoutOption_t410 * L_106 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_105*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, 0);
		ArrayElementTypeCheck (L_103, L_106);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_103, 0)) = (GUILayoutOption_t410 *)L_106;
		GUILayoutOptionU5BU5D_t409* L_107 = L_103;
		Vector2_t6 * L_108 = &(__this->___size_82);
		float L_109 = (L_108->___y_2);
		GUILayoutOption_t410 * L_110 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_109*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, 1);
		ArrayElementTypeCheck (L_107, L_110);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_107, 1)) = (GUILayoutOption_t410 *)L_110;
		bool L_111 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B28_1, G_B28_0, L_107, /*hidden argument*/NULL);
		if (!L_111)
		{
			goto IL_03c9;
		}
	}
	{
		bool L_112 = (__this->___showSearchText_30);
		__this->___showSearchText_30 = ((((int32_t)L_112) == ((int32_t)0))? 1 : 0);
	}

IL_03c9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t411_il2cpp_TypeInfo_var);
		Rect_t304  L_113 = GUILayoutUtility_GetLastRect_m1665(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tempRect_126 = L_113;
		Rect_t304  L_114 = (__this->___tempRect_126);
		String_t* L_115 = (__this->___filterText_87);
		GUIStyle_t302 * L_116 = (__this->___searchStyle_76);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_TextField_m1667(NULL /*static, unused*/, L_114, L_115, L_116, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reporter::DrawReport()
extern TypeInfo* GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var;
extern "C" void Reporter_DrawReport_m1105 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t304 * L_0 = &(__this->___screenRect_95);
		Rect_set_x_m1668(L_0, (0.0f), /*hidden argument*/NULL);
		Rect_t304 * L_1 = &(__this->___screenRect_95);
		Rect_set_y_m1669(L_1, (0.0f), /*hidden argument*/NULL);
		Rect_t304 * L_2 = &(__this->___screenRect_95);
		int32_t L_3 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m1670(L_2, (((float)L_3)), /*hidden argument*/NULL);
		Rect_t304 * L_4 = &(__this->___screenRect_95);
		int32_t L_5 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_height_m1671(L_4, (((float)L_5)), /*hidden argument*/NULL);
		Rect_t304  L_6 = (__this->___screenRect_95);
		GUIStyle_t302 * L_7 = (__this->___backStyle_68);
		GUILayout_BeginArea_m1645(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m1672(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIContent_t301 * L_8 = (__this->___cameraContent_60);
		GUIStyle_t302 * L_9 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_10 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_11 = &(__this->___size_82);
		float L_12 = (L_11->___x_1);
		GUILayoutOption_t410 * L_13 = GUILayout_Width_m1651(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_13);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_10, 0)) = (GUILayoutOption_t410 *)L_13;
		GUILayoutOptionU5BU5D_t409* L_14 = L_10;
		Vector2_t6 * L_15 = &(__this->___size_82);
		float L_16 = (L_15->___y_2);
		GUILayoutOption_t410 * L_17 = GUILayout_Height_m1652(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		ArrayElementTypeCheck (L_14, L_17);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_14, 1)) = (GUILayoutOption_t410 *)L_17;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_8, L_9, L_14, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t302 * L_18 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_19 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_20 = &(__this->___size_82);
		float L_21 = (L_20->___y_2);
		GUILayoutOption_t410 * L_22 = GUILayout_Height_m1652(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 0);
		ArrayElementTypeCheck (L_19, L_22);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_19, 0)) = (GUILayoutOption_t410 *)L_22;
		GUILayout_Label_m1654(NULL /*static, unused*/, (String_t*) &_stringLiteral216, L_18, L_19, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIStyle_t302 * L_23 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_24 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		Vector2_t6 * L_25 = &(__this->___size_82);
		float L_26 = (L_25->___y_2);
		GUILayoutOption_t410 * L_27 = GUILayout_Height_m1652(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		ArrayElementTypeCheck (L_24, L_27);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_24, 0)) = (GUILayoutOption_t410 *)L_27;
		GUILayout_Label_m1654(NULL /*static, unused*/, (String_t*) &_stringLiteral253, L_23, L_24, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIContent_t301 * L_28 = (__this->___backContent_59);
		GUIStyle_t302 * L_29 = (__this->___barStyle_64);
		GUILayoutOptionU5BU5D_t409* L_30 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_31 = &(__this->___size_82);
		float L_32 = (L_31->___x_1);
		GUILayoutOption_t410 * L_33 = GUILayout_Width_m1651(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		ArrayElementTypeCheck (L_30, L_33);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_30, 0)) = (GUILayoutOption_t410 *)L_33;
		GUILayoutOptionU5BU5D_t409* L_34 = L_30;
		Vector2_t6 * L_35 = &(__this->___size_82);
		float L_36 = (L_35->___y_2);
		GUILayoutOption_t410 * L_37 = GUILayout_Height_m1652(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 1);
		ArrayElementTypeCheck (L_34, L_37);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_34, 1)) = (GUILayoutOption_t410 *)L_37;
		bool L_38 = GUILayout_Button_m1662(NULL /*static, unused*/, L_28, L_29, L_34, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_017a;
		}
	}
	{
		__this->___currentView_39 = 1;
	}

IL_017a:
	{
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndVertical_m1673(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reporter::drawToolBar()
extern TypeInfo* GUI_t408_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t411_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t253_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisReporterGUI_t305_m1590_MethodInfo_var;
extern "C" void Reporter_drawToolBar_m1106 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		GUILayoutUtility_t411_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(654);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Int32_t253_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		GameObject_GetComponent_TisReporterGUI_t305_m1590_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483689);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6  V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	String_t* V_3 = {0};
	String_t* V_4 = {0};
	ReporterGUI_t305 * V_5 = {0};
	Exception_t232 * V_6 = {0};
	float V_7 = 0.0f;
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	GUIContent_t301 * G_B8_0 = {0};
	GUIContent_t301 * G_B7_0 = {0};
	GUIStyle_t302 * G_B9_0 = {0};
	GUIContent_t301 * G_B9_1 = {0};
	GUIContent_t301 * G_B14_0 = {0};
	GUIContent_t301 * G_B13_0 = {0};
	GUIStyle_t302 * G_B15_0 = {0};
	GUIContent_t301 * G_B15_1 = {0};
	GUIContent_t301 * G_B20_0 = {0};
	GUIContent_t301 * G_B19_0 = {0};
	GUIStyle_t302 * G_B21_0 = {0};
	GUIContent_t301 * G_B21_1 = {0};
	GUIContent_t301 * G_B26_0 = {0};
	GUIContent_t301 * G_B25_0 = {0};
	GUIStyle_t302 * G_B27_0 = {0};
	GUIContent_t301 * G_B27_1 = {0};
	GUIContent_t301 * G_B33_0 = {0};
	GUIContent_t301 * G_B32_0 = {0};
	GUIStyle_t302 * G_B34_0 = {0};
	GUIContent_t301 * G_B34_1 = {0};
	GUIContent_t301 * G_B40_0 = {0};
	GUIContent_t301 * G_B39_0 = {0};
	GUIStyle_t302 * G_B41_0 = {0};
	GUIContent_t301 * G_B41_1 = {0};
	GUIStyle_t302 * G_B61_0 = {0};
	GUIStyle_t302 * G_B68_0 = {0};
	GUIStyle_t302 * G_B75_0 = {0};
	{
		Rect_t304 * L_0 = &(__this->___toolBarRect_96);
		Rect_set_x_m1668(L_0, (0.0f), /*hidden argument*/NULL);
		Rect_t304 * L_1 = &(__this->___toolBarRect_96);
		Rect_set_y_m1669(L_1, (0.0f), /*hidden argument*/NULL);
		Rect_t304 * L_2 = &(__this->___toolBarRect_96);
		int32_t L_3 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m1670(L_2, (((float)L_3)), /*hidden argument*/NULL);
		Rect_t304 * L_4 = &(__this->___toolBarRect_96);
		Vector2_t6 * L_5 = &(__this->___size_82);
		float L_6 = (L_5->___y_2);
		Rect_set_height_m1671(L_4, ((float)((float)L_6*(float)(2.0f))), /*hidden argument*/NULL);
		GUISkin_t287 * L_7 = (__this->___toolbarScrollerSkin_79);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_set_skin_m1647(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Vector2_t6  L_8 = Reporter_getDrag_m1114(__this, /*hidden argument*/NULL);
		V_0 = L_8;
		float L_9 = ((&V_0)->___x_1);
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_00cd;
		}
	}
	{
		Vector2_t6  L_10 = (__this->___downPos_143);
		Vector2_t6  L_11 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_12 = Vector2_op_Inequality_m1646(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00cd;
		}
	}
	{
		Vector2_t6 * L_13 = &(__this->___downPos_143);
		float L_14 = (L_13->___y_2);
		int32_t L_15 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_16 = &(__this->___size_82);
		float L_17 = (L_16->___y_2);
		if ((!(((float)L_14) > ((float)((float)((float)(((float)L_15))-(float)((float)((float)L_17*(float)(2.0f)))))))))
		{
			goto IL_00cd;
		}
	}
	{
		Vector2_t6 * L_18 = &(__this->___toolbarScrollPosition_107);
		Vector2_t6 * L_19 = L_18;
		float L_20 = (L_19->___x_1);
		float L_21 = ((&V_0)->___x_1);
		float L_22 = (__this->___toolbarOldDrag_109);
		L_19->___x_1 = ((float)((float)L_20-(float)((float)((float)L_21-(float)L_22))));
	}

IL_00cd:
	{
		float L_23 = ((&V_0)->___x_1);
		__this->___toolbarOldDrag_109 = L_23;
		Rect_t304  L_24 = (__this->___toolBarRect_96);
		GUILayout_BeginArea_m1674(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Vector2_t6  L_25 = (__this->___toolbarScrollPosition_107);
		Vector2_t6  L_26 = GUILayout_BeginScrollView_m1648(NULL /*static, unused*/, L_25, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___toolbarScrollPosition_107 = L_26;
		GUIStyle_t302 * L_27 = (__this->___barStyle_64);
		GUILayout_BeginHorizontal_m1675(NULL /*static, unused*/, L_27, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIContent_t301 * L_28 = (__this->___clearContent_42);
		GUIStyle_t302 * L_29 = (__this->___barStyle_64);
		GUILayoutOptionU5BU5D_t409* L_30 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_31 = &(__this->___size_82);
		float L_32 = (L_31->___x_1);
		GUILayoutOption_t410 * L_33 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_32*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		ArrayElementTypeCheck (L_30, L_33);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_30, 0)) = (GUILayoutOption_t410 *)L_33;
		GUILayoutOptionU5BU5D_t409* L_34 = L_30;
		Vector2_t6 * L_35 = &(__this->___size_82);
		float L_36 = (L_35->___y_2);
		GUILayoutOption_t410 * L_37 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_36*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 1);
		ArrayElementTypeCheck (L_34, L_37);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_34, 1)) = (GUILayoutOption_t410 *)L_37;
		bool L_38 = GUILayout_Button_m1662(NULL /*static, unused*/, L_28, L_29, L_34, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0161;
		}
	}
	{
		Reporter_clear_m1101(__this, /*hidden argument*/NULL);
	}

IL_0161:
	{
		GUIContent_t301 * L_39 = (__this->___collapseContent_43);
		bool L_40 = (__this->___collapse_9);
		G_B7_0 = L_39;
		if (!L_40)
		{
			G_B8_0 = L_39;
			goto IL_017d;
		}
	}
	{
		GUIStyle_t302 * L_41 = (__this->___buttonActiveStyle_65);
		G_B9_0 = L_41;
		G_B9_1 = G_B7_0;
		goto IL_0183;
	}

IL_017d:
	{
		GUIStyle_t302 * L_42 = (__this->___barStyle_64);
		G_B9_0 = L_42;
		G_B9_1 = G_B8_0;
	}

IL_0183:
	{
		GUILayoutOptionU5BU5D_t409* L_43 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_44 = &(__this->___size_82);
		float L_45 = (L_44->___x_1);
		GUILayoutOption_t410 * L_46 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_45*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		ArrayElementTypeCheck (L_43, L_46);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_43, 0)) = (GUILayoutOption_t410 *)L_46;
		GUILayoutOptionU5BU5D_t409* L_47 = L_43;
		Vector2_t6 * L_48 = &(__this->___size_82);
		float L_49 = (L_48->___y_2);
		GUILayoutOption_t410 * L_50 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_49*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 1);
		ArrayElementTypeCheck (L_47, L_50);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_47, 1)) = (GUILayoutOption_t410 *)L_50;
		bool L_51 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B9_1, G_B9_0, L_47, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_01da;
		}
	}
	{
		bool L_52 = (__this->___collapse_9);
		__this->___collapse_9 = ((((int32_t)L_52) == ((int32_t)0))? 1 : 0);
		Reporter_calculateCurrentLog_m1102(__this, /*hidden argument*/NULL);
	}

IL_01da:
	{
		bool L_53 = (__this->___showClearOnNewSceneLoadedButton_25);
		if (!L_53)
		{
			goto IL_0258;
		}
	}
	{
		GUIContent_t301 * L_54 = (__this->___clearOnNewSceneContent_44);
		bool L_55 = (__this->___clearOnNewSceneLoaded_10);
		G_B13_0 = L_54;
		if (!L_55)
		{
			G_B14_0 = L_54;
			goto IL_0201;
		}
	}
	{
		GUIStyle_t302 * L_56 = (__this->___buttonActiveStyle_65);
		G_B15_0 = L_56;
		G_B15_1 = G_B13_0;
		goto IL_0207;
	}

IL_0201:
	{
		GUIStyle_t302 * L_57 = (__this->___barStyle_64);
		G_B15_0 = L_57;
		G_B15_1 = G_B14_0;
	}

IL_0207:
	{
		GUILayoutOptionU5BU5D_t409* L_58 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_59 = &(__this->___size_82);
		float L_60 = (L_59->___x_1);
		GUILayoutOption_t410 * L_61 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_60*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, 0);
		ArrayElementTypeCheck (L_58, L_61);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_58, 0)) = (GUILayoutOption_t410 *)L_61;
		GUILayoutOptionU5BU5D_t409* L_62 = L_58;
		Vector2_t6 * L_63 = &(__this->___size_82);
		float L_64 = (L_63->___y_2);
		GUILayoutOption_t410 * L_65 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_64*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, 1);
		ArrayElementTypeCheck (L_62, L_65);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_62, 1)) = (GUILayoutOption_t410 *)L_65;
		bool L_66 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B15_1, G_B15_0, L_62, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_0258;
		}
	}
	{
		bool L_67 = (__this->___clearOnNewSceneLoaded_10);
		__this->___clearOnNewSceneLoaded_10 = ((((int32_t)L_67) == ((int32_t)0))? 1 : 0);
	}

IL_0258:
	{
		bool L_68 = (__this->___showTimeButton_26);
		if (!L_68)
		{
			goto IL_02d6;
		}
	}
	{
		GUIContent_t301 * L_69 = (__this->___showTimeContent_45);
		bool L_70 = (__this->___showTime_11);
		G_B19_0 = L_69;
		if (!L_70)
		{
			G_B20_0 = L_69;
			goto IL_027f;
		}
	}
	{
		GUIStyle_t302 * L_71 = (__this->___buttonActiveStyle_65);
		G_B21_0 = L_71;
		G_B21_1 = G_B19_0;
		goto IL_0285;
	}

IL_027f:
	{
		GUIStyle_t302 * L_72 = (__this->___barStyle_64);
		G_B21_0 = L_72;
		G_B21_1 = G_B20_0;
	}

IL_0285:
	{
		GUILayoutOptionU5BU5D_t409* L_73 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_74 = &(__this->___size_82);
		float L_75 = (L_74->___x_1);
		GUILayoutOption_t410 * L_76 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_75*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, 0);
		ArrayElementTypeCheck (L_73, L_76);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_73, 0)) = (GUILayoutOption_t410 *)L_76;
		GUILayoutOptionU5BU5D_t409* L_77 = L_73;
		Vector2_t6 * L_78 = &(__this->___size_82);
		float L_79 = (L_78->___y_2);
		GUILayoutOption_t410 * L_80 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_79*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 1);
		ArrayElementTypeCheck (L_77, L_80);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_77, 1)) = (GUILayoutOption_t410 *)L_80;
		bool L_81 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B21_1, G_B21_0, L_77, /*hidden argument*/NULL);
		if (!L_81)
		{
			goto IL_02d6;
		}
	}
	{
		bool L_82 = (__this->___showTime_11);
		__this->___showTime_11 = ((((int32_t)L_82) == ((int32_t)0))? 1 : 0);
	}

IL_02d6:
	{
		bool L_83 = (__this->___showSceneButton_27);
		if (!L_83)
		{
			goto IL_03a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t411_il2cpp_TypeInfo_var);
		Rect_t304  L_84 = GUILayoutUtility_GetLastRect_m1665(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tempRect_126 = L_84;
		Rect_t304  L_85 = (__this->___tempRect_126);
		float L_86 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_86;
		String_t* L_87 = Single_ToString_m1658((&V_7), (String_t*) &_stringLiteral251, /*hidden argument*/NULL);
		GUIStyle_t302 * L_88 = (__this->___lowerLeftFontStyle_67);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Label_m1666(NULL /*static, unused*/, L_85, L_87, L_88, /*hidden argument*/NULL);
		GUIContent_t301 * L_89 = (__this->___showSceneContent_46);
		bool L_90 = (__this->___showScene_12);
		G_B25_0 = L_89;
		if (!L_90)
		{
			G_B26_0 = L_89;
			goto IL_032c;
		}
	}
	{
		GUIStyle_t302 * L_91 = (__this->___buttonActiveStyle_65);
		G_B27_0 = L_91;
		G_B27_1 = G_B25_0;
		goto IL_0332;
	}

IL_032c:
	{
		GUIStyle_t302 * L_92 = (__this->___barStyle_64);
		G_B27_0 = L_92;
		G_B27_1 = G_B26_0;
	}

IL_0332:
	{
		GUILayoutOptionU5BU5D_t409* L_93 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_94 = &(__this->___size_82);
		float L_95 = (L_94->___x_1);
		GUILayoutOption_t410 * L_96 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_95*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, 0);
		ArrayElementTypeCheck (L_93, L_96);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_93, 0)) = (GUILayoutOption_t410 *)L_96;
		GUILayoutOptionU5BU5D_t409* L_97 = L_93;
		Vector2_t6 * L_98 = &(__this->___size_82);
		float L_99 = (L_98->___y_2);
		GUILayoutOption_t410 * L_100 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_99*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, 1);
		ArrayElementTypeCheck (L_97, L_100);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_97, 1)) = (GUILayoutOption_t410 *)L_100;
		bool L_101 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B27_1, G_B27_0, L_97, /*hidden argument*/NULL);
		if (!L_101)
		{
			goto IL_0383;
		}
	}
	{
		bool L_102 = (__this->___showScene_12);
		__this->___showScene_12 = ((((int32_t)L_102) == ((int32_t)0))? 1 : 0);
	}

IL_0383:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t411_il2cpp_TypeInfo_var);
		Rect_t304  L_103 = GUILayoutUtility_GetLastRect_m1665(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tempRect_126 = L_103;
		Rect_t304  L_104 = (__this->___tempRect_126);
		String_t* L_105 = (__this->___currentScene_86);
		GUIStyle_t302 * L_106 = (__this->___lowerLeftFontStyle_67);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Label_m1666(NULL /*static, unused*/, L_104, L_105, L_106, /*hidden argument*/NULL);
	}

IL_03a5:
	{
		bool L_107 = (__this->___showMemButton_28);
		if (!L_107)
		{
			goto IL_044f;
		}
	}
	{
		GUIContent_t301 * L_108 = (__this->___showMemoryContent_48);
		bool L_109 = (__this->___showMemory_13);
		G_B32_0 = L_108;
		if (!L_109)
		{
			G_B33_0 = L_108;
			goto IL_03cc;
		}
	}
	{
		GUIStyle_t302 * L_110 = (__this->___buttonActiveStyle_65);
		G_B34_0 = L_110;
		G_B34_1 = G_B32_0;
		goto IL_03d2;
	}

IL_03cc:
	{
		GUIStyle_t302 * L_111 = (__this->___barStyle_64);
		G_B34_0 = L_111;
		G_B34_1 = G_B33_0;
	}

IL_03d2:
	{
		GUILayoutOptionU5BU5D_t409* L_112 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_113 = &(__this->___size_82);
		float L_114 = (L_113->___x_1);
		GUILayoutOption_t410 * L_115 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_114*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_112);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_112, 0);
		ArrayElementTypeCheck (L_112, L_115);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_112, 0)) = (GUILayoutOption_t410 *)L_115;
		GUILayoutOptionU5BU5D_t409* L_116 = L_112;
		Vector2_t6 * L_117 = &(__this->___size_82);
		float L_118 = (L_117->___y_2);
		GUILayoutOption_t410 * L_119 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_118*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_116);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_116, 1);
		ArrayElementTypeCheck (L_116, L_119);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_116, 1)) = (GUILayoutOption_t410 *)L_119;
		bool L_120 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B34_1, G_B34_0, L_116, /*hidden argument*/NULL);
		if (!L_120)
		{
			goto IL_0423;
		}
	}
	{
		bool L_121 = (__this->___showMemory_13);
		__this->___showMemory_13 = ((((int32_t)L_121) == ((int32_t)0))? 1 : 0);
	}

IL_0423:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t411_il2cpp_TypeInfo_var);
		Rect_t304  L_122 = GUILayoutUtility_GetLastRect_m1665(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tempRect_126 = L_122;
		Rect_t304  L_123 = (__this->___tempRect_126);
		float* L_124 = &(__this->___gcTotalMemory_35);
		String_t* L_125 = Single_ToString_m1658(L_124, (String_t*) &_stringLiteral251, /*hidden argument*/NULL);
		GUIStyle_t302 * L_126 = (__this->___lowerLeftFontStyle_67);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Label_m1666(NULL /*static, unused*/, L_123, L_125, L_126, /*hidden argument*/NULL);
	}

IL_044f:
	{
		bool L_127 = (__this->___showFpsButton_29);
		if (!L_127)
		{
			goto IL_04ef;
		}
	}
	{
		GUIContent_t301 * L_128 = (__this->___showFpsContent_51);
		bool L_129 = (__this->___showFps_14);
		G_B39_0 = L_128;
		if (!L_129)
		{
			G_B40_0 = L_128;
			goto IL_0476;
		}
	}
	{
		GUIStyle_t302 * L_130 = (__this->___buttonActiveStyle_65);
		G_B41_0 = L_130;
		G_B41_1 = G_B39_0;
		goto IL_047c;
	}

IL_0476:
	{
		GUIStyle_t302 * L_131 = (__this->___barStyle_64);
		G_B41_0 = L_131;
		G_B41_1 = G_B40_0;
	}

IL_047c:
	{
		GUILayoutOptionU5BU5D_t409* L_132 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_133 = &(__this->___size_82);
		float L_134 = (L_133->___x_1);
		GUILayoutOption_t410 * L_135 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_134*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_132);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_132, 0);
		ArrayElementTypeCheck (L_132, L_135);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_132, 0)) = (GUILayoutOption_t410 *)L_135;
		GUILayoutOptionU5BU5D_t409* L_136 = L_132;
		Vector2_t6 * L_137 = &(__this->___size_82);
		float L_138 = (L_137->___y_2);
		GUILayoutOption_t410 * L_139 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_138*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_136);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_136, 1);
		ArrayElementTypeCheck (L_136, L_139);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_136, 1)) = (GUILayoutOption_t410 *)L_139;
		bool L_140 = GUILayout_Button_m1662(NULL /*static, unused*/, G_B41_1, G_B41_0, L_136, /*hidden argument*/NULL);
		if (!L_140)
		{
			goto IL_04cd;
		}
	}
	{
		bool L_141 = (__this->___showFps_14);
		__this->___showFps_14 = ((((int32_t)L_141) == ((int32_t)0))? 1 : 0);
	}

IL_04cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t411_il2cpp_TypeInfo_var);
		Rect_t304  L_142 = GUILayoutUtility_GetLastRect_m1665(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tempRect_126 = L_142;
		Rect_t304  L_143 = (__this->___tempRect_126);
		String_t* L_144 = (__this->___fpsText_38);
		GUIStyle_t302 * L_145 = (__this->___lowerLeftFontStyle_67);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Label_m1666(NULL /*static, unused*/, L_143, L_144, L_145, /*hidden argument*/NULL);
	}

IL_04ef:
	{
		bool L_146 = (__this->___showSearchText_30);
		if (!L_146)
		{
			goto IL_0584;
		}
	}
	{
		GUIContent_t301 * L_147 = (__this->___searchContent_54);
		GUIStyle_t302 * L_148 = (__this->___barStyle_64);
		GUILayoutOptionU5BU5D_t409* L_149 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_150 = &(__this->___size_82);
		float L_151 = (L_150->___x_1);
		GUILayoutOption_t410 * L_152 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_151*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_149);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_149, 0);
		ArrayElementTypeCheck (L_149, L_152);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_149, 0)) = (GUILayoutOption_t410 *)L_152;
		GUILayoutOptionU5BU5D_t409* L_153 = L_149;
		Vector2_t6 * L_154 = &(__this->___size_82);
		float L_155 = (L_154->___y_2);
		GUILayoutOption_t410 * L_156 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_155*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_153);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_153, 1);
		ArrayElementTypeCheck (L_153, L_156);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_153, 1)) = (GUILayoutOption_t410 *)L_156;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_147, L_148, L_153, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t411_il2cpp_TypeInfo_var);
		Rect_t304  L_157 = GUILayoutUtility_GetLastRect_m1665(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tempRect_126 = L_157;
		Rect_t304  L_158 = (__this->___tempRect_126);
		String_t* L_159 = (__this->___filterText_87);
		GUIStyle_t302 * L_160 = (__this->___searchStyle_76);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		String_t* L_161 = GUI_TextField_m1667(NULL /*static, unused*/, L_158, L_159, L_160, /*hidden argument*/NULL);
		V_1 = L_161;
		String_t* L_162 = V_1;
		String_t* L_163 = (__this->___filterText_87);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_164 = String_op_Inequality_m1676(NULL /*static, unused*/, L_162, L_163, /*hidden argument*/NULL);
		if (!L_164)
		{
			goto IL_0584;
		}
	}
	{
		String_t* L_165 = V_1;
		__this->___filterText_87 = L_165;
		Reporter_calculateCurrentLog_m1102(__this, /*hidden argument*/NULL);
	}

IL_0584:
	{
		GUIContent_t301 * L_166 = (__this->___infoContent_53);
		GUIStyle_t302 * L_167 = (__this->___barStyle_64);
		GUILayoutOptionU5BU5D_t409* L_168 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_169 = &(__this->___size_82);
		float L_170 = (L_169->___x_1);
		GUILayoutOption_t410 * L_171 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_170*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_168);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_168, 0);
		ArrayElementTypeCheck (L_168, L_171);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_168, 0)) = (GUILayoutOption_t410 *)L_171;
		GUILayoutOptionU5BU5D_t409* L_172 = L_168;
		Vector2_t6 * L_173 = &(__this->___size_82);
		float L_174 = (L_173->___y_2);
		GUILayoutOption_t410 * L_175 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_174*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_172);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_172, 1);
		ArrayElementTypeCheck (L_172, L_175);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_172, 1)) = (GUILayoutOption_t410 *)L_175;
		bool L_176 = GUILayout_Button_m1662(NULL /*static, unused*/, L_166, L_167, L_172, /*hidden argument*/NULL);
		if (!L_176)
		{
			goto IL_05d9;
		}
	}
	{
		__this->___currentView_39 = 2;
	}

IL_05d9:
	{
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = (String_t*) &_stringLiteral254;
		bool L_177 = (__this->___collapse_9);
		if (!L_177)
		{
			goto IL_0606;
		}
	}
	{
		String_t* L_178 = V_2;
		int32_t L_179 = (__this->___numOfCollapsedLogs_22);
		int32_t L_180 = L_179;
		Object_t * L_181 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_180);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_182 = String_Concat_m954(NULL /*static, unused*/, L_178, L_181, /*hidden argument*/NULL);
		V_2 = L_182;
		goto IL_0618;
	}

IL_0606:
	{
		String_t* L_183 = V_2;
		int32_t L_184 = (__this->___numOfLogs_19);
		int32_t L_185 = L_184;
		Object_t * L_186 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_185);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_187 = String_Concat_m954(NULL /*static, unused*/, L_183, L_186, /*hidden argument*/NULL);
		V_2 = L_187;
	}

IL_0618:
	{
		V_3 = (String_t*) &_stringLiteral254;
		bool L_188 = (__this->___collapse_9);
		if (!L_188)
		{
			goto IL_0640;
		}
	}
	{
		String_t* L_189 = V_3;
		int32_t L_190 = (__this->___numOfCollapsedLogsWarning_23);
		int32_t L_191 = L_190;
		Object_t * L_192 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_191);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_193 = String_Concat_m954(NULL /*static, unused*/, L_189, L_192, /*hidden argument*/NULL);
		V_3 = L_193;
		goto IL_0652;
	}

IL_0640:
	{
		String_t* L_194 = V_3;
		int32_t L_195 = (__this->___numOfLogsWarning_20);
		int32_t L_196 = L_195;
		Object_t * L_197 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_196);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_198 = String_Concat_m954(NULL /*static, unused*/, L_194, L_197, /*hidden argument*/NULL);
		V_3 = L_198;
	}

IL_0652:
	{
		V_4 = (String_t*) &_stringLiteral254;
		bool L_199 = (__this->___collapse_9);
		if (!L_199)
		{
			goto IL_067d;
		}
	}
	{
		String_t* L_200 = V_4;
		int32_t L_201 = (__this->___numOfCollapsedLogsError_24);
		int32_t L_202 = L_201;
		Object_t * L_203 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_202);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_204 = String_Concat_m954(NULL /*static, unused*/, L_200, L_203, /*hidden argument*/NULL);
		V_4 = L_204;
		goto IL_0691;
	}

IL_067d:
	{
		String_t* L_205 = V_4;
		int32_t L_206 = (__this->___numOfLogsError_21);
		int32_t L_207 = L_206;
		Object_t * L_208 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_207);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_209 = String_Concat_m954(NULL /*static, unused*/, L_205, L_208, /*hidden argument*/NULL);
		V_4 = L_209;
	}

IL_0691:
	{
		bool L_210 = (__this->___showLog_16);
		if (!L_210)
		{
			goto IL_06a7;
		}
	}
	{
		GUIStyle_t302 * L_211 = (__this->___buttonActiveStyle_65);
		G_B61_0 = L_211;
		goto IL_06ad;
	}

IL_06a7:
	{
		GUIStyle_t302 * L_212 = (__this->___barStyle_64);
		G_B61_0 = L_212;
	}

IL_06ad:
	{
		GUILayout_BeginHorizontal_m1675(NULL /*static, unused*/, G_B61_0, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIContent_t301 * L_213 = (__this->___logContent_61);
		GUIStyle_t302 * L_214 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_215 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_216 = &(__this->___size_82);
		float L_217 = (L_216->___x_1);
		GUILayoutOption_t410 * L_218 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_217*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_215);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_215, 0);
		ArrayElementTypeCheck (L_215, L_218);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_215, 0)) = (GUILayoutOption_t410 *)L_218;
		GUILayoutOptionU5BU5D_t409* L_219 = L_215;
		Vector2_t6 * L_220 = &(__this->___size_82);
		float L_221 = (L_220->___y_2);
		GUILayoutOption_t410 * L_222 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_221*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_219);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_219, 1);
		ArrayElementTypeCheck (L_219, L_222);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_219, 1)) = (GUILayoutOption_t410 *)L_222;
		bool L_223 = GUILayout_Button_m1662(NULL /*static, unused*/, L_213, L_214, L_219, /*hidden argument*/NULL);
		if (!L_223)
		{
			goto IL_071b;
		}
	}
	{
		bool L_224 = (__this->___showLog_16);
		__this->___showLog_16 = ((((int32_t)L_224) == ((int32_t)0))? 1 : 0);
		Reporter_calculateCurrentLog_m1102(__this, /*hidden argument*/NULL);
	}

IL_071b:
	{
		String_t* L_225 = V_2;
		GUIStyle_t302 * L_226 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_227 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_228 = &(__this->___size_82);
		float L_229 = (L_228->___x_1);
		GUILayoutOption_t410 * L_230 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_229*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_227);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_227, 0);
		ArrayElementTypeCheck (L_227, L_230);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_227, 0)) = (GUILayoutOption_t410 *)L_230;
		GUILayoutOptionU5BU5D_t409* L_231 = L_227;
		Vector2_t6 * L_232 = &(__this->___size_82);
		float L_233 = (L_232->___y_2);
		GUILayoutOption_t410 * L_234 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_233*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_231);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_231, 1);
		ArrayElementTypeCheck (L_231, L_234);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_231, 1)) = (GUILayoutOption_t410 *)L_234;
		bool L_235 = GUILayout_Button_m1677(NULL /*static, unused*/, L_225, L_226, L_231, /*hidden argument*/NULL);
		if (!L_235)
		{
			goto IL_0779;
		}
	}
	{
		bool L_236 = (__this->___showLog_16);
		__this->___showLog_16 = ((((int32_t)L_236) == ((int32_t)0))? 1 : 0);
		Reporter_calculateCurrentLog_m1102(__this, /*hidden argument*/NULL);
	}

IL_0779:
	{
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_237 = (__this->___showWarning_17);
		if (!L_237)
		{
			goto IL_0794;
		}
	}
	{
		GUIStyle_t302 * L_238 = (__this->___buttonActiveStyle_65);
		G_B68_0 = L_238;
		goto IL_079a;
	}

IL_0794:
	{
		GUIStyle_t302 * L_239 = (__this->___barStyle_64);
		G_B68_0 = L_239;
	}

IL_079a:
	{
		GUILayout_BeginHorizontal_m1675(NULL /*static, unused*/, G_B68_0, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIContent_t301 * L_240 = (__this->___warningContent_62);
		GUIStyle_t302 * L_241 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_242 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_243 = &(__this->___size_82);
		float L_244 = (L_243->___x_1);
		GUILayoutOption_t410 * L_245 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_244*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_242);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_242, 0);
		ArrayElementTypeCheck (L_242, L_245);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_242, 0)) = (GUILayoutOption_t410 *)L_245;
		GUILayoutOptionU5BU5D_t409* L_246 = L_242;
		Vector2_t6 * L_247 = &(__this->___size_82);
		float L_248 = (L_247->___y_2);
		GUILayoutOption_t410 * L_249 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_248*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_246);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_246, 1);
		ArrayElementTypeCheck (L_246, L_249);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_246, 1)) = (GUILayoutOption_t410 *)L_249;
		bool L_250 = GUILayout_Button_m1662(NULL /*static, unused*/, L_240, L_241, L_246, /*hidden argument*/NULL);
		if (!L_250)
		{
			goto IL_0808;
		}
	}
	{
		bool L_251 = (__this->___showWarning_17);
		__this->___showWarning_17 = ((((int32_t)L_251) == ((int32_t)0))? 1 : 0);
		Reporter_calculateCurrentLog_m1102(__this, /*hidden argument*/NULL);
	}

IL_0808:
	{
		String_t* L_252 = V_3;
		GUIStyle_t302 * L_253 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_254 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_255 = &(__this->___size_82);
		float L_256 = (L_255->___x_1);
		GUILayoutOption_t410 * L_257 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_256*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_254);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_254, 0);
		ArrayElementTypeCheck (L_254, L_257);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_254, 0)) = (GUILayoutOption_t410 *)L_257;
		GUILayoutOptionU5BU5D_t409* L_258 = L_254;
		Vector2_t6 * L_259 = &(__this->___size_82);
		float L_260 = (L_259->___y_2);
		GUILayoutOption_t410 * L_261 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_260*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_258);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_258, 1);
		ArrayElementTypeCheck (L_258, L_261);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_258, 1)) = (GUILayoutOption_t410 *)L_261;
		bool L_262 = GUILayout_Button_m1677(NULL /*static, unused*/, L_252, L_253, L_258, /*hidden argument*/NULL);
		if (!L_262)
		{
			goto IL_0866;
		}
	}
	{
		bool L_263 = (__this->___showWarning_17);
		__this->___showWarning_17 = ((((int32_t)L_263) == ((int32_t)0))? 1 : 0);
		Reporter_calculateCurrentLog_m1102(__this, /*hidden argument*/NULL);
	}

IL_0866:
	{
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_264 = (__this->___showError_18);
		if (!L_264)
		{
			goto IL_0881;
		}
	}
	{
		GUIStyle_t302 * L_265 = (__this->___buttonActiveStyle_65);
		G_B75_0 = L_265;
		goto IL_0887;
	}

IL_0881:
	{
		GUIStyle_t302 * L_266 = (__this->___nonStyle_66);
		G_B75_0 = L_266;
	}

IL_0887:
	{
		GUILayout_BeginHorizontal_m1675(NULL /*static, unused*/, G_B75_0, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIContent_t301 * L_267 = (__this->___errorContent_63);
		GUIStyle_t302 * L_268 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_269 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_270 = &(__this->___size_82);
		float L_271 = (L_270->___x_1);
		GUILayoutOption_t410 * L_272 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_271*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_269);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_269, 0);
		ArrayElementTypeCheck (L_269, L_272);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_269, 0)) = (GUILayoutOption_t410 *)L_272;
		GUILayoutOptionU5BU5D_t409* L_273 = L_269;
		Vector2_t6 * L_274 = &(__this->___size_82);
		float L_275 = (L_274->___y_2);
		GUILayoutOption_t410 * L_276 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_275*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_273);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_273, 1);
		ArrayElementTypeCheck (L_273, L_276);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_273, 1)) = (GUILayoutOption_t410 *)L_276;
		bool L_277 = GUILayout_Button_m1662(NULL /*static, unused*/, L_267, L_268, L_273, /*hidden argument*/NULL);
		if (!L_277)
		{
			goto IL_08f5;
		}
	}
	{
		bool L_278 = (__this->___showError_18);
		__this->___showError_18 = ((((int32_t)L_278) == ((int32_t)0))? 1 : 0);
		Reporter_calculateCurrentLog_m1102(__this, /*hidden argument*/NULL);
	}

IL_08f5:
	{
		String_t* L_279 = V_4;
		GUIStyle_t302 * L_280 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_281 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_282 = &(__this->___size_82);
		float L_283 = (L_282->___x_1);
		GUILayoutOption_t410 * L_284 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_283*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_281);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_281, 0);
		ArrayElementTypeCheck (L_281, L_284);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_281, 0)) = (GUILayoutOption_t410 *)L_284;
		GUILayoutOptionU5BU5D_t409* L_285 = L_281;
		Vector2_t6 * L_286 = &(__this->___size_82);
		float L_287 = (L_286->___y_2);
		GUILayoutOption_t410 * L_288 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_287*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_285);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_285, 1);
		ArrayElementTypeCheck (L_285, L_288);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_285, 1)) = (GUILayoutOption_t410 *)L_288;
		bool L_289 = GUILayout_Button_m1677(NULL /*static, unused*/, L_279, L_280, L_285, /*hidden argument*/NULL);
		if (!L_289)
		{
			goto IL_0954;
		}
	}
	{
		bool L_290 = (__this->___showError_18);
		__this->___showError_18 = ((((int32_t)L_290) == ((int32_t)0))? 1 : 0);
		Reporter_calculateCurrentLog_m1102(__this, /*hidden argument*/NULL);
	}

IL_0954:
	{
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIContent_t301 * L_291 = (__this->___closeContent_55);
		GUIStyle_t302 * L_292 = (__this->___barStyle_64);
		GUILayoutOptionU5BU5D_t409* L_293 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_294 = &(__this->___size_82);
		float L_295 = (L_294->___x_1);
		GUILayoutOption_t410 * L_296 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)L_295*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_293);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_293, 0);
		ArrayElementTypeCheck (L_293, L_296);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_293, 0)) = (GUILayoutOption_t410 *)L_296;
		GUILayoutOptionU5BU5D_t409* L_297 = L_293;
		Vector2_t6 * L_298 = &(__this->___size_82);
		float L_299 = (L_298->___y_2);
		GUILayoutOption_t410 * L_300 = GUILayout_Height_m1652(NULL /*static, unused*/, ((float)((float)L_299*(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_297);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_297, 1);
		ArrayElementTypeCheck (L_297, L_300);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_297, 1)) = (GUILayoutOption_t410 *)L_300;
		bool L_301 = GUILayout_Button_m1662(NULL /*static, unused*/, L_291, L_292, L_297, /*hidden argument*/NULL);
		if (!L_301)
		{
			goto IL_09e5;
		}
	}
	{
		__this->___show_8 = 0;
		GameObject_t78 * L_302 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_302);
		ReporterGUI_t305 * L_303 = GameObject_GetComponent_TisReporterGUI_t305_m1590(L_302, /*hidden argument*/GameObject_GetComponent_TisReporterGUI_t305_m1590_MethodInfo_var);
		V_5 = L_303;
		ReporterGUI_t305 * L_304 = V_5;
		Object_DestroyImmediate_m761(NULL /*static, unused*/, L_304, /*hidden argument*/NULL);
	}

IL_09c2:
	try
	{ // begin try (depth: 1)
		GameObject_t78 * L_305 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_305);
		GameObject_SendMessage_m1599(L_305, (String_t*) &_stringLiteral255, /*hidden argument*/NULL);
		goto IL_09e5;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t232 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_09d7;
		throw e;
	}

CATCH_09d7:
	{ // begin catch(System.Exception)
		V_6 = ((Exception_t232 *)__exception_local);
		Exception_t232 * L_306 = V_6;
		Debug_LogException_m1600(NULL /*static, unused*/, L_306, /*hidden argument*/NULL);
		goto IL_09e5;
	} // end catch (depth: 1)

IL_09e5:
	{
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndScrollView_m1663(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reporter::DrawLogs()
extern TypeInfo* GUI_t408_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Reporter_DrawLogs_m1107 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	bool V_6 = false;
	int32_t V_7 = 0;
	Log_t291 * V_8 = {0};
	GUIContent_t301 * V_9 = {0};
	GUIStyle_t302 * V_10 = {0};
	float V_11 = 0.0f;
	Sample_t290 * V_12 = {0};
	int32_t V_13 = 0;
	Vector2_t6  V_14 = {0};
	Vector2_t6  V_15 = {0};
	Vector2_t6  V_16 = {0};
	Vector2_t6  V_17 = {0};
	Vector2_t6  V_18 = {0};
	GUIStyle_t302 * G_B33_0 = {0};
	{
		Rect_t304  L_0 = (__this->___logsRect_97);
		GUIStyle_t302 * L_1 = (__this->___backStyle_68);
		GUILayout_BeginArea_m1645(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		GUISkin_t287 * L_2 = (__this->___logScrollerSkin_80);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_set_skin_m1647(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Vector2_t6  L_3 = Reporter_getDrag_m1114(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = ((&V_0)->___y_2);
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_0086;
		}
	}
	{
		Rect_t304 * L_5 = &(__this->___logsRect_97);
		Vector2_t6 * L_6 = &(__this->___downPos_143);
		float L_7 = (L_6->___x_1);
		int32_t L_8 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_9 = &(__this->___downPos_143);
		float L_10 = (L_9->___y_2);
		Vector2_t6  L_11 = {0};
		Vector2__ctor_m630(&L_11, L_7, ((float)((float)(((float)L_8))-(float)L_10)), /*hidden argument*/NULL);
		bool L_12 = Rect_Contains_m1678(L_5, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0086;
		}
	}
	{
		Vector2_t6 * L_13 = &(__this->___scrollPosition_105);
		Vector2_t6 * L_14 = L_13;
		float L_15 = (L_14->___y_2);
		float L_16 = ((&V_0)->___y_2);
		float L_17 = (__this->___oldDrag_110);
		L_14->___y_2 = ((float)((float)L_15+(float)((float)((float)L_16-(float)L_17))));
	}

IL_0086:
	{
		Vector2_t6  L_18 = (__this->___scrollPosition_105);
		Vector2_t6  L_19 = GUILayout_BeginScrollView_m1648(NULL /*static, unused*/, L_18, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___scrollPosition_105 = L_19;
		float L_20 = ((&V_0)->___y_2);
		__this->___oldDrag_110 = L_20;
		int32_t L_21 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_22 = &(__this->___size_82);
		float L_23 = (L_22->___y_2);
		V_1 = (((int32_t)((float)((float)((float)((float)(((float)L_21))*(float)(0.75f)))/(float)L_23))));
		List_1_t298 * L_24 = (__this->___currentLog_5);
		NullCheck(L_24);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Count() */, L_24);
		V_2 = L_25;
		int32_t L_26 = V_1;
		int32_t L_27 = V_2;
		int32_t L_28 = (__this->___startIndex_113);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_29 = Mathf_Min_m1679(NULL /*static, unused*/, L_26, ((int32_t)((int32_t)L_27-(int32_t)L_28)), /*hidden argument*/NULL);
		V_1 = L_29;
		V_3 = 0;
		int32_t L_30 = (__this->___startIndex_113);
		Vector2_t6 * L_31 = &(__this->___size_82);
		float L_32 = (L_31->___y_2);
		V_4 = (((int32_t)((float)((float)(((float)L_30))*(float)L_32))));
		int32_t L_33 = V_4;
		if ((((int32_t)L_33) <= ((int32_t)0)))
		{
			goto IL_012a;
		}
	}
	{
		GUILayoutOptionU5BU5D_t409* L_34 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		int32_t L_35 = V_4;
		GUILayoutOption_t410 * L_36 = GUILayout_Height_m1652(NULL /*static, unused*/, (((float)L_35)), /*hidden argument*/NULL);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 0);
		ArrayElementTypeCheck (L_34, L_36);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_34, 0)) = (GUILayoutOption_t410 *)L_36;
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		GUILayout_Label_m1680(NULL /*static, unused*/, (String_t*) &_stringLiteral256, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_012a:
	{
		int32_t L_37 = (__this->___startIndex_113);
		int32_t L_38 = V_1;
		V_5 = ((int32_t)((int32_t)L_37+(int32_t)L_38));
		int32_t L_39 = V_5;
		int32_t L_40 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_41 = Mathf_Clamp_m712(NULL /*static, unused*/, L_39, 0, L_40, /*hidden argument*/NULL);
		V_5 = L_41;
		int32_t L_42 = V_1;
		int32_t L_43 = V_2;
		V_6 = ((((int32_t)L_42) < ((int32_t)L_43))? 1 : 0);
		int32_t L_44 = (__this->___startIndex_113);
		V_7 = L_44;
		goto IL_0973;
	}

IL_0152:
	{
		int32_t L_45 = V_7;
		List_1_t298 * L_46 = (__this->___currentLog_5);
		NullCheck(L_46);
		int32_t L_47 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Count() */, L_46);
		if ((((int32_t)L_45) < ((int32_t)L_47)))
		{
			goto IL_0169;
		}
	}
	{
		goto IL_0982;
	}

IL_0169:
	{
		List_1_t298 * L_48 = (__this->___currentLog_5);
		int32_t L_49 = V_7;
		NullCheck(L_48);
		Log_t291 * L_50 = (Log_t291 *)VirtFuncInvoker1< Log_t291 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Reporter/Log>::get_Item(System.Int32) */, L_48, L_49);
		V_8 = L_50;
		Log_t291 * L_51 = V_8;
		NullCheck(L_51);
		int32_t L_52 = (L_51->___logType_1);
		if ((!(((uint32_t)L_52) == ((uint32_t)3))))
		{
			goto IL_0195;
		}
	}
	{
		bool L_53 = (__this->___showLog_16);
		if (L_53)
		{
			goto IL_0195;
		}
	}
	{
		goto IL_096d;
	}

IL_0195:
	{
		Log_t291 * L_54 = V_8;
		NullCheck(L_54);
		int32_t L_55 = (L_54->___logType_1);
		if ((!(((uint32_t)L_55) == ((uint32_t)2))))
		{
			goto IL_01b2;
		}
	}
	{
		bool L_56 = (__this->___showWarning_17);
		if (L_56)
		{
			goto IL_01b2;
		}
	}
	{
		goto IL_096d;
	}

IL_01b2:
	{
		Log_t291 * L_57 = V_8;
		NullCheck(L_57);
		int32_t L_58 = (L_57->___logType_1);
		if (L_58)
		{
			goto IL_01ce;
		}
	}
	{
		bool L_59 = (__this->___showError_18);
		if (L_59)
		{
			goto IL_01ce;
		}
	}
	{
		goto IL_096d;
	}

IL_01ce:
	{
		Log_t291 * L_60 = V_8;
		NullCheck(L_60);
		int32_t L_61 = (L_60->___logType_1);
		if ((!(((uint32_t)L_61) == ((uint32_t)1))))
		{
			goto IL_01eb;
		}
	}
	{
		bool L_62 = (__this->___showError_18);
		if (L_62)
		{
			goto IL_01eb;
		}
	}
	{
		goto IL_096d;
	}

IL_01eb:
	{
		Log_t291 * L_63 = V_8;
		NullCheck(L_63);
		int32_t L_64 = (L_63->___logType_1);
		if ((!(((uint32_t)L_64) == ((uint32_t)4))))
		{
			goto IL_0208;
		}
	}
	{
		bool L_65 = (__this->___showError_18);
		if (L_65)
		{
			goto IL_0208;
		}
	}
	{
		goto IL_096d;
	}

IL_0208:
	{
		int32_t L_66 = V_3;
		int32_t L_67 = V_1;
		if ((((int32_t)L_66) < ((int32_t)L_67)))
		{
			goto IL_0214;
		}
	}
	{
		goto IL_0982;
	}

IL_0214:
	{
		V_9 = (GUIContent_t301 *)NULL;
		Log_t291 * L_68 = V_8;
		NullCheck(L_68);
		int32_t L_69 = (L_68->___logType_1);
		if ((!(((uint32_t)L_69) == ((uint32_t)3))))
		{
			goto IL_0231;
		}
	}
	{
		GUIContent_t301 * L_70 = (__this->___logContent_61);
		V_9 = L_70;
		goto IL_0253;
	}

IL_0231:
	{
		Log_t291 * L_71 = V_8;
		NullCheck(L_71);
		int32_t L_72 = (L_71->___logType_1);
		if ((!(((uint32_t)L_72) == ((uint32_t)2))))
		{
			goto IL_024b;
		}
	}
	{
		GUIContent_t301 * L_73 = (__this->___warningContent_62);
		V_9 = L_73;
		goto IL_0253;
	}

IL_024b:
	{
		GUIContent_t301 * L_74 = (__this->___errorContent_63);
		V_9 = L_74;
	}

IL_0253:
	{
		int32_t L_75 = (__this->___startIndex_113);
		int32_t L_76 = V_3;
		if (((int32_t)((int32_t)((int32_t)((int32_t)L_75+(int32_t)L_76))%(int32_t)2)))
		{
			goto IL_026d;
		}
	}
	{
		GUIStyle_t302 * L_77 = (__this->___evenLogStyle_69);
		G_B33_0 = L_77;
		goto IL_0273;
	}

IL_026d:
	{
		GUIStyle_t302 * L_78 = (__this->___oddLogStyle_70);
		G_B33_0 = L_78;
	}

IL_0273:
	{
		V_10 = G_B33_0;
		Log_t291 * L_79 = V_8;
		Log_t291 * L_80 = (__this->___selectedLog_108);
		if ((!(((Object_t*)(Log_t291 *)L_79) == ((Object_t*)(Log_t291 *)L_80))))
		{
			goto IL_028f;
		}
	}
	{
		GUIStyle_t302 * L_81 = (__this->___selectedLogStyle_72);
		V_10 = L_81;
		goto IL_028f;
	}

IL_028f:
	{
		GUIContent_t301 * L_82 = (__this->___tempContent_123);
		Log_t291 * L_83 = V_8;
		NullCheck(L_83);
		int32_t* L_84 = &(L_83->___count_0);
		String_t* L_85 = Int32_ToString_m1615(L_84, /*hidden argument*/NULL);
		NullCheck(L_82);
		GUIContent_set_text_m1681(L_82, L_85, /*hidden argument*/NULL);
		V_11 = (0.0f);
		bool L_86 = (__this->___collapse_9);
		if (!L_86)
		{
			goto IL_02da;
		}
	}
	{
		GUIStyle_t302 * L_87 = (__this->___barStyle_64);
		GUIContent_t301 * L_88 = (__this->___tempContent_123);
		NullCheck(L_87);
		Vector2_t6  L_89 = GUIStyle_CalcSize_m1682(L_87, L_88, /*hidden argument*/NULL);
		V_14 = L_89;
		float L_90 = ((&V_14)->___x_1);
		V_11 = ((float)((float)L_90+(float)(3.0f)));
	}

IL_02da:
	{
		Rect_t304 * L_91 = &(__this->___countRect_114);
		int32_t L_92 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_93 = V_11;
		Rect_set_x_m1668(L_91, ((float)((float)(((float)L_92))-(float)L_93)), /*hidden argument*/NULL);
		Rect_t304 * L_94 = &(__this->___countRect_114);
		Vector2_t6 * L_95 = &(__this->___size_82);
		float L_96 = (L_95->___y_2);
		int32_t L_97 = V_7;
		Rect_set_y_m1669(L_94, ((float)((float)L_96*(float)(((float)L_97)))), /*hidden argument*/NULL);
		int32_t L_98 = V_4;
		if ((((int32_t)L_98) <= ((int32_t)0)))
		{
			goto IL_0327;
		}
	}
	{
		Rect_t304 * L_99 = &(__this->___countRect_114);
		Rect_t304 * L_100 = L_99;
		float L_101 = Rect_get_y_m1683(L_100, /*hidden argument*/NULL);
		Rect_set_y_m1669(L_100, ((float)((float)L_101+(float)(8.0f))), /*hidden argument*/NULL);
	}

IL_0327:
	{
		Rect_t304 * L_102 = &(__this->___countRect_114);
		float L_103 = V_11;
		Rect_set_width_m1670(L_102, L_103, /*hidden argument*/NULL);
		Rect_t304 * L_104 = &(__this->___countRect_114);
		Vector2_t6 * L_105 = &(__this->___size_82);
		float L_106 = (L_105->___y_2);
		Rect_set_height_m1671(L_104, L_106, /*hidden argument*/NULL);
		bool L_107 = V_6;
		if (!L_107)
		{
			goto IL_0374;
		}
	}
	{
		Rect_t304 * L_108 = &(__this->___countRect_114);
		Rect_t304 * L_109 = L_108;
		float L_110 = Rect_get_x_m1684(L_109, /*hidden argument*/NULL);
		Vector2_t6 * L_111 = &(__this->___size_82);
		float L_112 = (L_111->___x_1);
		Rect_set_x_m1668(L_109, ((float)((float)L_110-(float)((float)((float)L_112*(float)(2.0f))))), /*hidden argument*/NULL);
	}

IL_0374:
	{
		List_1_t297 * L_113 = (__this->___samples_2);
		Log_t291 * L_114 = V_8;
		NullCheck(L_114);
		int32_t L_115 = (L_114->___sampleId_4);
		NullCheck(L_113);
		Sample_t290 * L_116 = (Sample_t290 *)VirtFuncInvoker1< Sample_t290 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Reporter/Sample>::get_Item(System.Int32) */, L_113, L_115);
		V_12 = L_116;
		Rect_t304  L_117 = (__this->___countRect_114);
		__this->___fpsRect_121 = L_117;
		bool L_118 = (__this->___showFps_14);
		if (!L_118)
		{
			goto IL_0441;
		}
	}
	{
		GUIContent_t301 * L_119 = (__this->___tempContent_123);
		Sample_t290 * L_120 = V_12;
		NullCheck(L_120);
		String_t* L_121 = (L_120->___fpsText_4);
		NullCheck(L_119);
		GUIContent_set_text_m1681(L_119, L_121, /*hidden argument*/NULL);
		GUIStyle_t302 * L_122 = V_10;
		GUIContent_t301 * L_123 = (__this->___tempContent_123);
		NullCheck(L_122);
		Vector2_t6  L_124 = GUIStyle_CalcSize_m1682(L_122, L_123, /*hidden argument*/NULL);
		V_15 = L_124;
		float L_125 = ((&V_15)->___x_1);
		Vector2_t6 * L_126 = &(__this->___size_82);
		float L_127 = (L_126->___x_1);
		V_11 = ((float)((float)L_125+(float)L_127));
		Rect_t304 * L_128 = &(__this->___fpsRect_121);
		Rect_t304 * L_129 = L_128;
		float L_130 = Rect_get_x_m1684(L_129, /*hidden argument*/NULL);
		float L_131 = V_11;
		Rect_set_x_m1668(L_129, ((float)((float)L_130-(float)L_131)), /*hidden argument*/NULL);
		Rect_t304 * L_132 = &(__this->___fpsRect_121);
		Vector2_t6 * L_133 = &(__this->___size_82);
		float L_134 = (L_133->___x_1);
		Rect_set_width_m1670(L_132, L_134, /*hidden argument*/NULL);
		Rect_t304  L_135 = (__this->___fpsRect_121);
		__this->___fpsLabelRect_122 = L_135;
		Rect_t304 * L_136 = &(__this->___fpsLabelRect_122);
		Rect_t304 * L_137 = L_136;
		float L_138 = Rect_get_x_m1684(L_137, /*hidden argument*/NULL);
		Vector2_t6 * L_139 = &(__this->___size_82);
		float L_140 = (L_139->___x_1);
		Rect_set_x_m1668(L_137, ((float)((float)L_138+(float)L_140)), /*hidden argument*/NULL);
		Rect_t304 * L_141 = &(__this->___fpsLabelRect_122);
		float L_142 = V_11;
		Vector2_t6 * L_143 = &(__this->___size_82);
		float L_144 = (L_143->___x_1);
		Rect_set_width_m1670(L_141, ((float)((float)L_142-(float)L_144)), /*hidden argument*/NULL);
	}

IL_0441:
	{
		Rect_t304  L_145 = (__this->___fpsRect_121);
		__this->___memoryRect_119 = L_145;
		bool L_146 = (__this->___showMemory_13);
		if (!L_146)
		{
			goto IL_0504;
		}
	}
	{
		GUIContent_t301 * L_147 = (__this->___tempContent_123);
		Sample_t290 * L_148 = V_12;
		NullCheck(L_148);
		float* L_149 = &(L_148->___memory_2);
		String_t* L_150 = Single_ToString_m1658(L_149, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		NullCheck(L_147);
		GUIContent_set_text_m1681(L_147, L_150, /*hidden argument*/NULL);
		GUIStyle_t302 * L_151 = V_10;
		GUIContent_t301 * L_152 = (__this->___tempContent_123);
		NullCheck(L_151);
		Vector2_t6  L_153 = GUIStyle_CalcSize_m1682(L_151, L_152, /*hidden argument*/NULL);
		V_16 = L_153;
		float L_154 = ((&V_16)->___x_1);
		Vector2_t6 * L_155 = &(__this->___size_82);
		float L_156 = (L_155->___x_1);
		V_11 = ((float)((float)L_154+(float)L_156));
		Rect_t304 * L_157 = &(__this->___memoryRect_119);
		Rect_t304 * L_158 = L_157;
		float L_159 = Rect_get_x_m1684(L_158, /*hidden argument*/NULL);
		float L_160 = V_11;
		Rect_set_x_m1668(L_158, ((float)((float)L_159-(float)L_160)), /*hidden argument*/NULL);
		Rect_t304 * L_161 = &(__this->___memoryRect_119);
		Vector2_t6 * L_162 = &(__this->___size_82);
		float L_163 = (L_162->___x_1);
		Rect_set_width_m1670(L_161, L_163, /*hidden argument*/NULL);
		Rect_t304  L_164 = (__this->___memoryRect_119);
		__this->___memoryLabelRect_120 = L_164;
		Rect_t304 * L_165 = &(__this->___memoryLabelRect_120);
		Rect_t304 * L_166 = L_165;
		float L_167 = Rect_get_x_m1684(L_166, /*hidden argument*/NULL);
		Vector2_t6 * L_168 = &(__this->___size_82);
		float L_169 = (L_168->___x_1);
		Rect_set_x_m1668(L_166, ((float)((float)L_167+(float)L_169)), /*hidden argument*/NULL);
		Rect_t304 * L_170 = &(__this->___memoryLabelRect_120);
		float L_171 = V_11;
		Vector2_t6 * L_172 = &(__this->___size_82);
		float L_173 = (L_172->___x_1);
		Rect_set_width_m1670(L_170, ((float)((float)L_171-(float)L_173)), /*hidden argument*/NULL);
	}

IL_0504:
	{
		Rect_t304  L_174 = (__this->___memoryRect_119);
		__this->___sceneRect_117 = L_174;
		bool L_175 = (__this->___showScene_12);
		if (!L_175)
		{
			goto IL_05c4;
		}
	}
	{
		GUIContent_t301 * L_176 = (__this->___tempContent_123);
		StringU5BU5D_t243* L_177 = (__this->___scenes_85);
		Sample_t290 * L_178 = V_12;
		NullCheck(L_178);
		uint8_t L_179 = (L_178->___loadedScene_1);
		NullCheck(L_177);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_177, L_179);
		uint8_t L_180 = L_179;
		NullCheck(L_176);
		GUIContent_set_text_m1681(L_176, (*(String_t**)(String_t**)SZArrayLdElema(L_177, L_180)), /*hidden argument*/NULL);
		GUIStyle_t302 * L_181 = V_10;
		GUIContent_t301 * L_182 = (__this->___tempContent_123);
		NullCheck(L_181);
		Vector2_t6  L_183 = GUIStyle_CalcSize_m1682(L_181, L_182, /*hidden argument*/NULL);
		V_17 = L_183;
		float L_184 = ((&V_17)->___x_1);
		Vector2_t6 * L_185 = &(__this->___size_82);
		float L_186 = (L_185->___x_1);
		V_11 = ((float)((float)L_184+(float)L_186));
		Rect_t304 * L_187 = &(__this->___sceneRect_117);
		Rect_t304 * L_188 = L_187;
		float L_189 = Rect_get_x_m1684(L_188, /*hidden argument*/NULL);
		float L_190 = V_11;
		Rect_set_x_m1668(L_188, ((float)((float)L_189-(float)L_190)), /*hidden argument*/NULL);
		Rect_t304 * L_191 = &(__this->___sceneRect_117);
		Vector2_t6 * L_192 = &(__this->___size_82);
		float L_193 = (L_192->___x_1);
		Rect_set_width_m1670(L_191, L_193, /*hidden argument*/NULL);
		Rect_t304  L_194 = (__this->___sceneRect_117);
		__this->___sceneLabelRect_118 = L_194;
		Rect_t304 * L_195 = &(__this->___sceneLabelRect_118);
		Rect_t304 * L_196 = L_195;
		float L_197 = Rect_get_x_m1684(L_196, /*hidden argument*/NULL);
		Vector2_t6 * L_198 = &(__this->___size_82);
		float L_199 = (L_198->___x_1);
		Rect_set_x_m1668(L_196, ((float)((float)L_197+(float)L_199)), /*hidden argument*/NULL);
		Rect_t304 * L_200 = &(__this->___sceneLabelRect_118);
		float L_201 = V_11;
		Vector2_t6 * L_202 = &(__this->___size_82);
		float L_203 = (L_202->___x_1);
		Rect_set_width_m1670(L_200, ((float)((float)L_201-(float)L_203)), /*hidden argument*/NULL);
	}

IL_05c4:
	{
		Rect_t304  L_204 = (__this->___sceneRect_117);
		__this->___timeRect_115 = L_204;
		bool L_205 = (__this->___showTime_11);
		if (!L_205)
		{
			goto IL_0687;
		}
	}
	{
		GUIContent_t301 * L_206 = (__this->___tempContent_123);
		Sample_t290 * L_207 = V_12;
		NullCheck(L_207);
		float* L_208 = &(L_207->___time_0);
		String_t* L_209 = Single_ToString_m1658(L_208, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		NullCheck(L_206);
		GUIContent_set_text_m1681(L_206, L_209, /*hidden argument*/NULL);
		GUIStyle_t302 * L_210 = V_10;
		GUIContent_t301 * L_211 = (__this->___tempContent_123);
		NullCheck(L_210);
		Vector2_t6  L_212 = GUIStyle_CalcSize_m1682(L_210, L_211, /*hidden argument*/NULL);
		V_18 = L_212;
		float L_213 = ((&V_18)->___x_1);
		Vector2_t6 * L_214 = &(__this->___size_82);
		float L_215 = (L_214->___x_1);
		V_11 = ((float)((float)L_213+(float)L_215));
		Rect_t304 * L_216 = &(__this->___timeRect_115);
		Rect_t304 * L_217 = L_216;
		float L_218 = Rect_get_x_m1684(L_217, /*hidden argument*/NULL);
		float L_219 = V_11;
		Rect_set_x_m1668(L_217, ((float)((float)L_218-(float)L_219)), /*hidden argument*/NULL);
		Rect_t304 * L_220 = &(__this->___timeRect_115);
		Vector2_t6 * L_221 = &(__this->___size_82);
		float L_222 = (L_221->___x_1);
		Rect_set_width_m1670(L_220, L_222, /*hidden argument*/NULL);
		Rect_t304  L_223 = (__this->___timeRect_115);
		__this->___timeLabelRect_116 = L_223;
		Rect_t304 * L_224 = &(__this->___timeLabelRect_116);
		Rect_t304 * L_225 = L_224;
		float L_226 = Rect_get_x_m1684(L_225, /*hidden argument*/NULL);
		Vector2_t6 * L_227 = &(__this->___size_82);
		float L_228 = (L_227->___x_1);
		Rect_set_x_m1668(L_225, ((float)((float)L_226+(float)L_228)), /*hidden argument*/NULL);
		Rect_t304 * L_229 = &(__this->___timeLabelRect_116);
		float L_230 = V_11;
		Vector2_t6 * L_231 = &(__this->___size_82);
		float L_232 = (L_231->___x_1);
		Rect_set_width_m1670(L_229, ((float)((float)L_230-(float)L_232)), /*hidden argument*/NULL);
	}

IL_0687:
	{
		GUIStyle_t302 * L_233 = V_10;
		GUILayout_BeginHorizontal_m1675(NULL /*static, unused*/, L_233, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Log_t291 * L_234 = V_8;
		Log_t291 * L_235 = (__this->___selectedLog_108);
		if ((!(((Object_t*)(Log_t291 *)L_234) == ((Object_t*)(Log_t291 *)L_235))))
		{
			goto IL_07e4;
		}
	}
	{
		GUIContent_t301 * L_236 = V_9;
		GUIStyle_t302 * L_237 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_238 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_239 = &(__this->___size_82);
		float L_240 = (L_239->___x_1);
		GUILayoutOption_t410 * L_241 = GUILayout_Width_m1651(NULL /*static, unused*/, L_240, /*hidden argument*/NULL);
		NullCheck(L_238);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_238, 0);
		ArrayElementTypeCheck (L_238, L_241);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_238, 0)) = (GUILayoutOption_t410 *)L_241;
		GUILayoutOptionU5BU5D_t409* L_242 = L_238;
		Vector2_t6 * L_243 = &(__this->___size_82);
		float L_244 = (L_243->___y_2);
		GUILayoutOption_t410 * L_245 = GUILayout_Height_m1652(NULL /*static, unused*/, L_244, /*hidden argument*/NULL);
		NullCheck(L_242);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_242, 1);
		ArrayElementTypeCheck (L_242, L_245);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_242, 1)) = (GUILayoutOption_t410 *)L_245;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_236, L_237, L_242, /*hidden argument*/NULL);
		Log_t291 * L_246 = V_8;
		NullCheck(L_246);
		String_t* L_247 = (L_246->___condition_2);
		GUIStyle_t302 * L_248 = (__this->___selectedLogFontStyle_73);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_247, L_248, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_249 = (__this->___showTime_11);
		if (!L_249)
		{
			goto IL_072e;
		}
	}
	{
		Rect_t304  L_250 = (__this->___timeRect_115);
		GUIContent_t301 * L_251 = (__this->___showTimeContent_45);
		GUIStyle_t302 * L_252 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Box_m1685(NULL /*static, unused*/, L_250, L_251, L_252, /*hidden argument*/NULL);
		Rect_t304  L_253 = (__this->___timeLabelRect_116);
		Sample_t290 * L_254 = V_12;
		NullCheck(L_254);
		float* L_255 = &(L_254->___time_0);
		String_t* L_256 = Single_ToString_m1658(L_255, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		GUIStyle_t302 * L_257 = V_10;
		GUI_Label_m1666(NULL /*static, unused*/, L_253, L_256, L_257, /*hidden argument*/NULL);
	}

IL_072e:
	{
		bool L_258 = (__this->___showScene_12);
		if (!L_258)
		{
			goto IL_0767;
		}
	}
	{
		Rect_t304  L_259 = (__this->___sceneRect_117);
		GUIContent_t301 * L_260 = (__this->___showSceneContent_46);
		GUIStyle_t302 * L_261 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Box_m1685(NULL /*static, unused*/, L_259, L_260, L_261, /*hidden argument*/NULL);
		Rect_t304  L_262 = (__this->___sceneLabelRect_118);
		StringU5BU5D_t243* L_263 = (__this->___scenes_85);
		Sample_t290 * L_264 = V_12;
		NullCheck(L_264);
		uint8_t L_265 = (L_264->___loadedScene_1);
		NullCheck(L_263);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_263, L_265);
		uint8_t L_266 = L_265;
		GUIStyle_t302 * L_267 = V_10;
		GUI_Label_m1666(NULL /*static, unused*/, L_262, (*(String_t**)(String_t**)SZArrayLdElema(L_263, L_266)), L_267, /*hidden argument*/NULL);
	}

IL_0767:
	{
		bool L_268 = (__this->___showMemory_13);
		if (!L_268)
		{
			goto IL_07ad;
		}
	}
	{
		Rect_t304  L_269 = (__this->___memoryRect_119);
		GUIContent_t301 * L_270 = (__this->___showMemoryContent_48);
		GUIStyle_t302 * L_271 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Box_m1685(NULL /*static, unused*/, L_269, L_270, L_271, /*hidden argument*/NULL);
		Rect_t304  L_272 = (__this->___memoryLabelRect_120);
		Sample_t290 * L_273 = V_12;
		NullCheck(L_273);
		float* L_274 = &(L_273->___memory_2);
		String_t* L_275 = Single_ToString_m1658(L_274, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_276 = String_Concat_m860(NULL /*static, unused*/, L_275, (String_t*) &_stringLiteral243, /*hidden argument*/NULL);
		GUIStyle_t302 * L_277 = V_10;
		GUI_Label_m1666(NULL /*static, unused*/, L_272, L_276, L_277, /*hidden argument*/NULL);
	}

IL_07ad:
	{
		bool L_278 = (__this->___showFps_14);
		if (!L_278)
		{
			goto IL_07df;
		}
	}
	{
		Rect_t304  L_279 = (__this->___fpsRect_121);
		GUIContent_t301 * L_280 = (__this->___showFpsContent_51);
		GUIStyle_t302 * L_281 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Box_m1685(NULL /*static, unused*/, L_279, L_280, L_281, /*hidden argument*/NULL);
		Rect_t304  L_282 = (__this->___fpsLabelRect_122);
		Sample_t290 * L_283 = V_12;
		NullCheck(L_283);
		String_t* L_284 = (L_283->___fpsText_4);
		GUIStyle_t302 * L_285 = V_10;
		GUI_Label_m1666(NULL /*static, unused*/, L_282, L_284, L_285, /*hidden argument*/NULL);
	}

IL_07df:
	{
		goto IL_093c;
	}

IL_07e4:
	{
		GUIContent_t301 * L_286 = V_9;
		GUIStyle_t302 * L_287 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_288 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_289 = &(__this->___size_82);
		float L_290 = (L_289->___x_1);
		GUILayoutOption_t410 * L_291 = GUILayout_Width_m1651(NULL /*static, unused*/, L_290, /*hidden argument*/NULL);
		NullCheck(L_288);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_288, 0);
		ArrayElementTypeCheck (L_288, L_291);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_288, 0)) = (GUILayoutOption_t410 *)L_291;
		GUILayoutOptionU5BU5D_t409* L_292 = L_288;
		Vector2_t6 * L_293 = &(__this->___size_82);
		float L_294 = (L_293->___y_2);
		GUILayoutOption_t410 * L_295 = GUILayout_Height_m1652(NULL /*static, unused*/, L_294, /*hidden argument*/NULL);
		NullCheck(L_292);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_292, 1);
		ArrayElementTypeCheck (L_292, L_295);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_292, 1)) = (GUILayoutOption_t410 *)L_295;
		bool L_296 = GUILayout_Button_m1662(NULL /*static, unused*/, L_286, L_287, L_292, /*hidden argument*/NULL);
		if (!L_296)
		{
			goto IL_082a;
		}
	}
	{
		Log_t291 * L_297 = V_8;
		__this->___selectedLog_108 = L_297;
	}

IL_082a:
	{
		Log_t291 * L_298 = V_8;
		NullCheck(L_298);
		String_t* L_299 = (L_298->___condition_2);
		GUIStyle_t302 * L_300 = (__this->___logButtonStyle_71);
		bool L_301 = GUILayout_Button_m1677(NULL /*static, unused*/, L_299, L_300, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_301)
		{
			goto IL_084f;
		}
	}
	{
		Log_t291 * L_302 = V_8;
		__this->___selectedLog_108 = L_302;
	}

IL_084f:
	{
		bool L_303 = (__this->___showTime_11);
		if (!L_303)
		{
			goto IL_088b;
		}
	}
	{
		Rect_t304  L_304 = (__this->___timeRect_115);
		GUIContent_t301 * L_305 = (__this->___showTimeContent_45);
		GUIStyle_t302 * L_306 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Box_m1685(NULL /*static, unused*/, L_304, L_305, L_306, /*hidden argument*/NULL);
		Rect_t304  L_307 = (__this->___timeLabelRect_116);
		Sample_t290 * L_308 = V_12;
		NullCheck(L_308);
		float* L_309 = &(L_308->___time_0);
		String_t* L_310 = Single_ToString_m1658(L_309, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		GUIStyle_t302 * L_311 = V_10;
		GUI_Label_m1666(NULL /*static, unused*/, L_307, L_310, L_311, /*hidden argument*/NULL);
	}

IL_088b:
	{
		bool L_312 = (__this->___showScene_12);
		if (!L_312)
		{
			goto IL_08c4;
		}
	}
	{
		Rect_t304  L_313 = (__this->___sceneRect_117);
		GUIContent_t301 * L_314 = (__this->___showSceneContent_46);
		GUIStyle_t302 * L_315 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Box_m1685(NULL /*static, unused*/, L_313, L_314, L_315, /*hidden argument*/NULL);
		Rect_t304  L_316 = (__this->___sceneLabelRect_118);
		StringU5BU5D_t243* L_317 = (__this->___scenes_85);
		Sample_t290 * L_318 = V_12;
		NullCheck(L_318);
		uint8_t L_319 = (L_318->___loadedScene_1);
		NullCheck(L_317);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_317, L_319);
		uint8_t L_320 = L_319;
		GUIStyle_t302 * L_321 = V_10;
		GUI_Label_m1666(NULL /*static, unused*/, L_316, (*(String_t**)(String_t**)SZArrayLdElema(L_317, L_320)), L_321, /*hidden argument*/NULL);
	}

IL_08c4:
	{
		bool L_322 = (__this->___showMemory_13);
		if (!L_322)
		{
			goto IL_090a;
		}
	}
	{
		Rect_t304  L_323 = (__this->___memoryRect_119);
		GUIContent_t301 * L_324 = (__this->___showMemoryContent_48);
		GUIStyle_t302 * L_325 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Box_m1685(NULL /*static, unused*/, L_323, L_324, L_325, /*hidden argument*/NULL);
		Rect_t304  L_326 = (__this->___memoryLabelRect_120);
		Sample_t290 * L_327 = V_12;
		NullCheck(L_327);
		float* L_328 = &(L_327->___memory_2);
		String_t* L_329 = Single_ToString_m1658(L_328, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_330 = String_Concat_m860(NULL /*static, unused*/, L_329, (String_t*) &_stringLiteral243, /*hidden argument*/NULL);
		GUIStyle_t302 * L_331 = V_10;
		GUI_Label_m1666(NULL /*static, unused*/, L_326, L_330, L_331, /*hidden argument*/NULL);
	}

IL_090a:
	{
		bool L_332 = (__this->___showFps_14);
		if (!L_332)
		{
			goto IL_093c;
		}
	}
	{
		Rect_t304  L_333 = (__this->___fpsRect_121);
		GUIContent_t301 * L_334 = (__this->___showFpsContent_51);
		GUIStyle_t302 * L_335 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Box_m1685(NULL /*static, unused*/, L_333, L_334, L_335, /*hidden argument*/NULL);
		Rect_t304  L_336 = (__this->___fpsLabelRect_122);
		Sample_t290 * L_337 = V_12;
		NullCheck(L_337);
		String_t* L_338 = (L_337->___fpsText_4);
		GUIStyle_t302 * L_339 = V_10;
		GUI_Label_m1666(NULL /*static, unused*/, L_336, L_338, L_339, /*hidden argument*/NULL);
	}

IL_093c:
	{
		bool L_340 = (__this->___collapse_9);
		if (!L_340)
		{
			goto IL_0964;
		}
	}
	{
		Rect_t304  L_341 = (__this->___countRect_114);
		Log_t291 * L_342 = V_8;
		NullCheck(L_342);
		int32_t* L_343 = &(L_342->___count_0);
		String_t* L_344 = Int32_ToString_m1615(L_343, /*hidden argument*/NULL);
		GUIStyle_t302 * L_345 = (__this->___barStyle_64);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Label_m1666(NULL /*static, unused*/, L_341, L_344, L_345, /*hidden argument*/NULL);
	}

IL_0964:
	{
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_346 = V_3;
		V_3 = ((int32_t)((int32_t)L_346+(int32_t)1));
	}

IL_096d:
	{
		int32_t L_347 = V_7;
		V_7 = ((int32_t)((int32_t)L_347+(int32_t)1));
	}

IL_0973:
	{
		int32_t L_348 = (__this->___startIndex_113);
		int32_t L_349 = V_3;
		int32_t L_350 = V_5;
		if ((((int32_t)((int32_t)((int32_t)L_348+(int32_t)L_349))) < ((int32_t)L_350)))
		{
			goto IL_0152;
		}
	}

IL_0982:
	{
		int32_t L_351 = V_2;
		int32_t L_352 = (__this->___startIndex_113);
		int32_t L_353 = V_1;
		Vector2_t6 * L_354 = &(__this->___size_82);
		float L_355 = (L_354->___y_2);
		V_13 = (((int32_t)((float)((float)(((float)((int32_t)((int32_t)L_351-(int32_t)((int32_t)((int32_t)L_352+(int32_t)L_353))))))*(float)L_355))));
		int32_t L_356 = V_13;
		if ((((int32_t)L_356) <= ((int32_t)0)))
		{
			goto IL_09cf;
		}
	}
	{
		GUILayoutOptionU5BU5D_t409* L_357 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		int32_t L_358 = V_13;
		GUILayoutOption_t410 * L_359 = GUILayout_Height_m1652(NULL /*static, unused*/, (((float)L_358)), /*hidden argument*/NULL);
		NullCheck(L_357);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_357, 0);
		ArrayElementTypeCheck (L_357, L_359);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_357, 0)) = (GUILayoutOption_t410 *)L_359;
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, L_357, /*hidden argument*/NULL);
		GUILayout_Label_m1680(NULL /*static, unused*/, (String_t*) &_stringLiteral254, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_09cf:
	{
		GUILayout_EndScrollView_m1663(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t304 * L_360 = &(__this->___buttomRect_102);
		Rect_set_x_m1668(L_360, (0.0f), /*hidden argument*/NULL);
		Rect_t304 * L_361 = &(__this->___buttomRect_102);
		int32_t L_362 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_363 = &(__this->___size_82);
		float L_364 = (L_363->___y_2);
		Rect_set_y_m1669(L_361, ((float)((float)(((float)L_362))-(float)L_364)), /*hidden argument*/NULL);
		Rect_t304 * L_365 = &(__this->___buttomRect_102);
		int32_t L_366 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m1670(L_365, (((float)L_366)), /*hidden argument*/NULL);
		Rect_t304 * L_367 = &(__this->___buttomRect_102);
		Vector2_t6 * L_368 = &(__this->___size_82);
		float L_369 = (L_368->___y_2);
		Rect_set_height_m1671(L_367, L_369, /*hidden argument*/NULL);
		bool L_370 = (__this->___showGraph_15);
		if (!L_370)
		{
			goto IL_0a43;
		}
	}
	{
		Reporter_drawGraph_m1108(__this, /*hidden argument*/NULL);
		goto IL_0a49;
	}

IL_0a43:
	{
		Reporter_drawStack_m1109(__this, /*hidden argument*/NULL);
	}

IL_0a49:
	{
		return;
	}
}
// System.Void Reporter::drawGraph()
extern TypeInfo* GUI_t408_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var;
extern "C" void Reporter_drawGraph_m1108 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6  V_0 = {0};
	Vector2_t6  V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Sample_t290 * V_4 = {0};
	Sample_t290 * V_5 = {0};
	{
		Rect_t304  L_0 = (__this->___stackRect_98);
		__this->___graphRect_99 = L_0;
		Rect_t304 * L_1 = &(__this->___graphRect_99);
		int32_t L_2 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_height_m1671(L_1, ((float)((float)(((float)L_2))*(float)(0.25f))), /*hidden argument*/NULL);
		GUISkin_t287 * L_3 = (__this->___graphScrollerSkin_81);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_set_skin_m1647(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector2_t6  L_4 = Reporter_getDrag_m1114(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		Rect_t304 * L_5 = &(__this->___graphRect_99);
		Vector2_t6 * L_6 = &(__this->___downPos_143);
		float L_7 = (L_6->___x_1);
		int32_t L_8 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_9 = &(__this->___downPos_143);
		float L_10 = (L_9->___y_2);
		Vector2_t6  L_11 = {0};
		Vector2__ctor_m630(&L_11, L_7, ((float)((float)(((float)L_8))-(float)L_10)), /*hidden argument*/NULL);
		bool L_12 = Rect_Contains_m1678(L_5, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00eb;
		}
	}
	{
		float L_13 = ((&V_0)->___x_1);
		if ((((float)L_13) == ((float)(0.0f))))
		{
			goto IL_00b8;
		}
	}
	{
		Vector2_t6 * L_14 = &(__this->___graphScrollerPos_132);
		Vector2_t6 * L_15 = L_14;
		float L_16 = (L_15->___x_1);
		float L_17 = ((&V_0)->___x_1);
		float L_18 = (__this->___oldDrag3_112);
		L_15->___x_1 = ((float)((float)L_16-(float)((float)((float)L_17-(float)L_18))));
		Vector2_t6 * L_19 = &(__this->___graphScrollerPos_132);
		Vector2_t6 * L_20 = &(__this->___graphScrollerPos_132);
		float L_21 = (L_20->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Max_m782(NULL /*static, unused*/, (0.0f), L_21, /*hidden argument*/NULL);
		L_19->___x_1 = L_22;
	}

IL_00b8:
	{
		Vector2_t6  L_23 = (__this->___downPos_143);
		V_1 = L_23;
		Vector2_t6  L_24 = V_1;
		Vector2_t6  L_25 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_26 = Vector2_op_Inequality_m1646(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00eb;
		}
	}
	{
		int32_t L_27 = (__this->___startFrame_128);
		float L_28 = ((&V_1)->___x_1);
		float L_29 = (__this->___graphSize_127);
		__this->___currentFrame_129 = ((int32_t)((int32_t)L_27+(int32_t)(((int32_t)((float)((float)L_28/(float)L_29))))));
	}

IL_00eb:
	{
		float L_30 = ((&V_0)->___x_1);
		__this->___oldDrag3_112 = L_30;
		Rect_t304  L_31 = (__this->___graphRect_99);
		GUIStyle_t302 * L_32 = (__this->___backStyle_68);
		GUILayout_BeginArea_m1645(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		Vector2_t6  L_33 = (__this->___graphScrollerPos_132);
		Vector2_t6  L_34 = GUILayout_BeginScrollView_m1648(NULL /*static, unused*/, L_33, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___graphScrollerPos_132 = L_34;
		Vector2_t6 * L_35 = &(__this->___graphScrollerPos_132);
		float L_36 = (L_35->___x_1);
		float L_37 = (__this->___graphSize_127);
		__this->___startFrame_128 = (((int32_t)((float)((float)L_36/(float)L_37))));
		Vector2_t6 * L_38 = &(__this->___graphScrollerPos_132);
		float L_39 = (L_38->___x_1);
		List_1_t297 * L_40 = (__this->___samples_2);
		NullCheck(L_40);
		int32_t L_41 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::get_Count() */, L_40);
		float L_42 = (__this->___graphSize_127);
		int32_t L_43 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_39) >= ((float)((float)((float)((float)((float)(((float)L_41))*(float)L_42))-(float)(((float)L_43))))))))
		{
			goto IL_017b;
		}
	}
	{
		Vector2_t6 * L_44 = &(__this->___graphScrollerPos_132);
		Vector2_t6 * L_45 = L_44;
		float L_46 = (L_45->___x_1);
		float L_47 = (__this->___graphSize_127);
		L_45->___x_1 = ((float)((float)L_46+(float)L_47));
	}

IL_017b:
	{
		GUILayoutOptionU5BU5D_t409* L_48 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 1));
		List_1_t297 * L_49 = (__this->___samples_2);
		NullCheck(L_49);
		int32_t L_50 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::get_Count() */, L_49);
		float L_51 = (__this->___graphSize_127);
		GUILayoutOption_t410 * L_52 = GUILayout_Width_m1651(NULL /*static, unused*/, ((float)((float)(((float)L_50))*(float)L_51)), /*hidden argument*/NULL);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 0);
		ArrayElementTypeCheck (L_48, L_52);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_48, 0)) = (GUILayoutOption_t410 *)L_52;
		GUILayout_Label_m1680(NULL /*static, unused*/, (String_t*) &_stringLiteral254, L_48, /*hidden argument*/NULL);
		GUILayout_EndScrollView_m1663(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___maxFpsValue_133 = (0.0f);
		__this->___minFpsValue_134 = (100000.0f);
		__this->___maxMemoryValue_135 = (0.0f);
		__this->___minMemoryValue_136 = (100000.0f);
		V_2 = 0;
		goto IL_0290;
	}

IL_01e3:
	{
		int32_t L_53 = (__this->___startFrame_128);
		int32_t L_54 = V_2;
		V_3 = ((int32_t)((int32_t)L_53+(int32_t)L_54));
		int32_t L_55 = V_3;
		List_1_t297 * L_56 = (__this->___samples_2);
		NullCheck(L_56);
		int32_t L_57 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::get_Count() */, L_56);
		if ((((int32_t)L_55) < ((int32_t)L_57)))
		{
			goto IL_0202;
		}
	}
	{
		goto IL_02a4;
	}

IL_0202:
	{
		List_1_t297 * L_58 = (__this->___samples_2);
		int32_t L_59 = V_3;
		NullCheck(L_58);
		Sample_t290 * L_60 = (Sample_t290 *)VirtFuncInvoker1< Sample_t290 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Reporter/Sample>::get_Item(System.Int32) */, L_58, L_59);
		V_4 = L_60;
		float L_61 = (__this->___maxFpsValue_133);
		Sample_t290 * L_62 = V_4;
		NullCheck(L_62);
		float L_63 = (L_62->___fps_3);
		if ((!(((float)L_61) < ((float)L_63))))
		{
			goto IL_022f;
		}
	}
	{
		Sample_t290 * L_64 = V_4;
		NullCheck(L_64);
		float L_65 = (L_64->___fps_3);
		__this->___maxFpsValue_133 = L_65;
	}

IL_022f:
	{
		float L_66 = (__this->___minFpsValue_134);
		Sample_t290 * L_67 = V_4;
		NullCheck(L_67);
		float L_68 = (L_67->___fps_3);
		if ((!(((float)L_66) > ((float)L_68))))
		{
			goto IL_024e;
		}
	}
	{
		Sample_t290 * L_69 = V_4;
		NullCheck(L_69);
		float L_70 = (L_69->___fps_3);
		__this->___minFpsValue_134 = L_70;
	}

IL_024e:
	{
		float L_71 = (__this->___maxMemoryValue_135);
		Sample_t290 * L_72 = V_4;
		NullCheck(L_72);
		float L_73 = (L_72->___memory_2);
		if ((!(((float)L_71) < ((float)L_73))))
		{
			goto IL_026d;
		}
	}
	{
		Sample_t290 * L_74 = V_4;
		NullCheck(L_74);
		float L_75 = (L_74->___memory_2);
		__this->___maxMemoryValue_135 = L_75;
	}

IL_026d:
	{
		float L_76 = (__this->___minMemoryValue_136);
		Sample_t290 * L_77 = V_4;
		NullCheck(L_77);
		float L_78 = (L_77->___memory_2);
		if ((!(((float)L_76) > ((float)L_78))))
		{
			goto IL_028c;
		}
	}
	{
		Sample_t290 * L_79 = V_4;
		NullCheck(L_79);
		float L_80 = (L_79->___memory_2);
		__this->___minMemoryValue_136 = L_80;
	}

IL_028c:
	{
		int32_t L_81 = V_2;
		V_2 = ((int32_t)((int32_t)L_81+(int32_t)1));
	}

IL_0290:
	{
		int32_t L_82 = V_2;
		int32_t L_83 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_84 = (__this->___graphSize_127);
		if ((((float)(((float)L_82))) < ((float)((float)((float)(((float)L_83))/(float)L_84)))))
		{
			goto IL_01e3;
		}
	}

IL_02a4:
	{
		int32_t L_85 = (__this->___currentFrame_129);
		if ((((int32_t)L_85) == ((int32_t)(-1))))
		{
			goto IL_050c;
		}
	}
	{
		int32_t L_86 = (__this->___currentFrame_129);
		List_1_t297 * L_87 = (__this->___samples_2);
		NullCheck(L_87);
		int32_t L_88 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::get_Count() */, L_87);
		if ((((int32_t)L_86) >= ((int32_t)L_88)))
		{
			goto IL_050c;
		}
	}
	{
		List_1_t297 * L_89 = (__this->___samples_2);
		int32_t L_90 = (__this->___currentFrame_129);
		NullCheck(L_89);
		Sample_t290 * L_91 = (Sample_t290 *)VirtFuncInvoker1< Sample_t290 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Reporter/Sample>::get_Item(System.Int32) */, L_89, L_90);
		V_5 = L_91;
		Rect_t304  L_92 = (__this->___buttomRect_102);
		GUIStyle_t302 * L_93 = (__this->___backStyle_68);
		GUILayout_BeginArea_m1645(NULL /*static, unused*/, L_92, L_93, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIContent_t301 * L_94 = (__this->___showTimeContent_45);
		GUIStyle_t302 * L_95 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_96 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_97 = &(__this->___size_82);
		float L_98 = (L_97->___x_1);
		GUILayoutOption_t410 * L_99 = GUILayout_Width_m1651(NULL /*static, unused*/, L_98, /*hidden argument*/NULL);
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, 0);
		ArrayElementTypeCheck (L_96, L_99);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_96, 0)) = (GUILayoutOption_t410 *)L_99;
		GUILayoutOptionU5BU5D_t409* L_100 = L_96;
		Vector2_t6 * L_101 = &(__this->___size_82);
		float L_102 = (L_101->___y_2);
		GUILayoutOption_t410 * L_103 = GUILayout_Height_m1652(NULL /*static, unused*/, L_102, /*hidden argument*/NULL);
		NullCheck(L_100);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_100, 1);
		ArrayElementTypeCheck (L_100, L_103);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_100, 1)) = (GUILayoutOption_t410 *)L_103;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_94, L_95, L_100, /*hidden argument*/NULL);
		Sample_t290 * L_104 = V_5;
		NullCheck(L_104);
		float* L_105 = &(L_104->___time_0);
		String_t* L_106 = Single_ToString_m1658(L_105, (String_t*) &_stringLiteral251, /*hidden argument*/NULL);
		GUIStyle_t302 * L_107 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_106, L_107, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_108 = &(__this->___size_82);
		float L_109 = (L_108->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_109, /*hidden argument*/NULL);
		GUIContent_t301 * L_110 = (__this->___showSceneContent_46);
		GUIStyle_t302 * L_111 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_112 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_113 = &(__this->___size_82);
		float L_114 = (L_113->___x_1);
		GUILayoutOption_t410 * L_115 = GUILayout_Width_m1651(NULL /*static, unused*/, L_114, /*hidden argument*/NULL);
		NullCheck(L_112);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_112, 0);
		ArrayElementTypeCheck (L_112, L_115);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_112, 0)) = (GUILayoutOption_t410 *)L_115;
		GUILayoutOptionU5BU5D_t409* L_116 = L_112;
		Vector2_t6 * L_117 = &(__this->___size_82);
		float L_118 = (L_117->___y_2);
		GUILayoutOption_t410 * L_119 = GUILayout_Height_m1652(NULL /*static, unused*/, L_118, /*hidden argument*/NULL);
		NullCheck(L_116);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_116, 1);
		ArrayElementTypeCheck (L_116, L_119);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_116, 1)) = (GUILayoutOption_t410 *)L_119;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_110, L_111, L_116, /*hidden argument*/NULL);
		StringU5BU5D_t243* L_120 = (__this->___scenes_85);
		Sample_t290 * L_121 = V_5;
		NullCheck(L_121);
		uint8_t L_122 = (L_121->___loadedScene_1);
		NullCheck(L_120);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_120, L_122);
		uint8_t L_123 = L_122;
		GUIStyle_t302 * L_124 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_120, L_123)), L_124, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_125 = &(__this->___size_82);
		float L_126 = (L_125->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_126, /*hidden argument*/NULL);
		GUIContent_t301 * L_127 = (__this->___showMemoryContent_48);
		GUIStyle_t302 * L_128 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_129 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_130 = &(__this->___size_82);
		float L_131 = (L_130->___x_1);
		GUILayoutOption_t410 * L_132 = GUILayout_Width_m1651(NULL /*static, unused*/, L_131, /*hidden argument*/NULL);
		NullCheck(L_129);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_129, 0);
		ArrayElementTypeCheck (L_129, L_132);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_129, 0)) = (GUILayoutOption_t410 *)L_132;
		GUILayoutOptionU5BU5D_t409* L_133 = L_129;
		Vector2_t6 * L_134 = &(__this->___size_82);
		float L_135 = (L_134->___y_2);
		GUILayoutOption_t410 * L_136 = GUILayout_Height_m1652(NULL /*static, unused*/, L_135, /*hidden argument*/NULL);
		NullCheck(L_133);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_133, 1);
		ArrayElementTypeCheck (L_133, L_136);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_133, 1)) = (GUILayoutOption_t410 *)L_136;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_127, L_128, L_133, /*hidden argument*/NULL);
		Sample_t290 * L_137 = V_5;
		NullCheck(L_137);
		float* L_138 = &(L_137->___memory_2);
		String_t* L_139 = Single_ToString_m1658(L_138, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		GUIStyle_t302 * L_140 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_139, L_140, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_141 = &(__this->___size_82);
		float L_142 = (L_141->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_142, /*hidden argument*/NULL);
		GUIContent_t301 * L_143 = (__this->___showFpsContent_51);
		GUIStyle_t302 * L_144 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_145 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_146 = &(__this->___size_82);
		float L_147 = (L_146->___x_1);
		GUILayoutOption_t410 * L_148 = GUILayout_Width_m1651(NULL /*static, unused*/, L_147, /*hidden argument*/NULL);
		NullCheck(L_145);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_145, 0);
		ArrayElementTypeCheck (L_145, L_148);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_145, 0)) = (GUILayoutOption_t410 *)L_148;
		GUILayoutOptionU5BU5D_t409* L_149 = L_145;
		Vector2_t6 * L_150 = &(__this->___size_82);
		float L_151 = (L_150->___y_2);
		GUILayoutOption_t410 * L_152 = GUILayout_Height_m1652(NULL /*static, unused*/, L_151, /*hidden argument*/NULL);
		NullCheck(L_149);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_149, 1);
		ArrayElementTypeCheck (L_149, L_152);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_149, 1)) = (GUILayoutOption_t410 *)L_152;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_143, L_144, L_149, /*hidden argument*/NULL);
		Sample_t290 * L_153 = V_5;
		NullCheck(L_153);
		String_t* L_154 = (L_153->___fpsText_4);
		GUIStyle_t302 * L_155 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_154, L_155, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_156 = &(__this->___size_82);
		float L_157 = (L_156->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_157, /*hidden argument*/NULL);
		GUIContent_t301 * L_158 = (__this->___graphContent_52);
		GUIStyle_t302 * L_159 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_160 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_161 = &(__this->___size_82);
		float L_162 = (L_161->___x_1);
		GUILayoutOption_t410 * L_163 = GUILayout_Width_m1651(NULL /*static, unused*/, L_162, /*hidden argument*/NULL);
		NullCheck(L_160);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_160, 0);
		ArrayElementTypeCheck (L_160, L_163);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_160, 0)) = (GUILayoutOption_t410 *)L_163;
		GUILayoutOptionU5BU5D_t409* L_164 = L_160;
		Vector2_t6 * L_165 = &(__this->___size_82);
		float L_166 = (L_165->___y_2);
		GUILayoutOption_t410 * L_167 = GUILayout_Height_m1652(NULL /*static, unused*/, L_166, /*hidden argument*/NULL);
		NullCheck(L_164);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_164, 1);
		ArrayElementTypeCheck (L_164, L_167);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_164, 1)) = (GUILayoutOption_t410 *)L_167;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_158, L_159, L_164, /*hidden argument*/NULL);
		int32_t* L_168 = &(__this->___currentFrame_129);
		String_t* L_169 = Int32_ToString_m1615(L_168, /*hidden argument*/NULL);
		GUIStyle_t302 * L_170 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_169, L_170, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_050c:
	{
		Rect_t304  L_171 = (__this->___stackRect_98);
		__this->___graphMaxRect_101 = L_171;
		Rect_t304 * L_172 = &(__this->___graphMaxRect_101);
		Vector2_t6 * L_173 = &(__this->___size_82);
		float L_174 = (L_173->___y_2);
		Rect_set_height_m1671(L_172, L_174, /*hidden argument*/NULL);
		Rect_t304  L_175 = (__this->___graphMaxRect_101);
		GUILayout_BeginArea_m1674(NULL /*static, unused*/, L_175, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIContent_t301 * L_176 = (__this->___showMemoryContent_48);
		GUIStyle_t302 * L_177 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_178 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_179 = &(__this->___size_82);
		float L_180 = (L_179->___x_1);
		GUILayoutOption_t410 * L_181 = GUILayout_Width_m1651(NULL /*static, unused*/, L_180, /*hidden argument*/NULL);
		NullCheck(L_178);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_178, 0);
		ArrayElementTypeCheck (L_178, L_181);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_178, 0)) = (GUILayoutOption_t410 *)L_181;
		GUILayoutOptionU5BU5D_t409* L_182 = L_178;
		Vector2_t6 * L_183 = &(__this->___size_82);
		float L_184 = (L_183->___y_2);
		GUILayoutOption_t410 * L_185 = GUILayout_Height_m1652(NULL /*static, unused*/, L_184, /*hidden argument*/NULL);
		NullCheck(L_182);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_182, 1);
		ArrayElementTypeCheck (L_182, L_185);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_182, 1)) = (GUILayoutOption_t410 *)L_185;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_176, L_177, L_182, /*hidden argument*/NULL);
		float* L_186 = &(__this->___maxMemoryValue_135);
		String_t* L_187 = Single_ToString_m1658(L_186, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		GUIStyle_t302 * L_188 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_187, L_188, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIContent_t301 * L_189 = (__this->___showFpsContent_51);
		GUIStyle_t302 * L_190 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_191 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_192 = &(__this->___size_82);
		float L_193 = (L_192->___x_1);
		GUILayoutOption_t410 * L_194 = GUILayout_Width_m1651(NULL /*static, unused*/, L_193, /*hidden argument*/NULL);
		NullCheck(L_191);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_191, 0);
		ArrayElementTypeCheck (L_191, L_194);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_191, 0)) = (GUILayoutOption_t410 *)L_194;
		GUILayoutOptionU5BU5D_t409* L_195 = L_191;
		Vector2_t6 * L_196 = &(__this->___size_82);
		float L_197 = (L_196->___y_2);
		GUILayoutOption_t410 * L_198 = GUILayout_Height_m1652(NULL /*static, unused*/, L_197, /*hidden argument*/NULL);
		NullCheck(L_195);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_195, 1);
		ArrayElementTypeCheck (L_195, L_198);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_195, 1)) = (GUILayoutOption_t410 *)L_198;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_189, L_190, L_195, /*hidden argument*/NULL);
		float* L_199 = &(__this->___maxFpsValue_133);
		String_t* L_200 = Single_ToString_m1658(L_199, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		GUIStyle_t302 * L_201 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_200, L_201, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t304  L_202 = (__this->___stackRect_98);
		__this->___graphMinRect_100 = L_202;
		Rect_t304 * L_203 = &(__this->___graphMinRect_100);
		Rect_t304 * L_204 = &(__this->___stackRect_98);
		float L_205 = Rect_get_y_m1683(L_204, /*hidden argument*/NULL);
		Rect_t304 * L_206 = &(__this->___stackRect_98);
		float L_207 = Rect_get_height_m1686(L_206, /*hidden argument*/NULL);
		Vector2_t6 * L_208 = &(__this->___size_82);
		float L_209 = (L_208->___y_2);
		Rect_set_y_m1669(L_203, ((float)((float)((float)((float)L_205+(float)L_207))-(float)L_209)), /*hidden argument*/NULL);
		Rect_t304 * L_210 = &(__this->___graphMinRect_100);
		Vector2_t6 * L_211 = &(__this->___size_82);
		float L_212 = (L_211->___y_2);
		Rect_set_height_m1671(L_210, L_212, /*hidden argument*/NULL);
		Rect_t304  L_213 = (__this->___graphMinRect_100);
		GUILayout_BeginArea_m1674(NULL /*static, unused*/, L_213, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIContent_t301 * L_214 = (__this->___showMemoryContent_48);
		GUIStyle_t302 * L_215 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_216 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_217 = &(__this->___size_82);
		float L_218 = (L_217->___x_1);
		GUILayoutOption_t410 * L_219 = GUILayout_Width_m1651(NULL /*static, unused*/, L_218, /*hidden argument*/NULL);
		NullCheck(L_216);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_216, 0);
		ArrayElementTypeCheck (L_216, L_219);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_216, 0)) = (GUILayoutOption_t410 *)L_219;
		GUILayoutOptionU5BU5D_t409* L_220 = L_216;
		Vector2_t6 * L_221 = &(__this->___size_82);
		float L_222 = (L_221->___y_2);
		GUILayoutOption_t410 * L_223 = GUILayout_Height_m1652(NULL /*static, unused*/, L_222, /*hidden argument*/NULL);
		NullCheck(L_220);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_220, 1);
		ArrayElementTypeCheck (L_220, L_223);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_220, 1)) = (GUILayoutOption_t410 *)L_223;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_214, L_215, L_220, /*hidden argument*/NULL);
		float* L_224 = &(__this->___minMemoryValue_136);
		String_t* L_225 = Single_ToString_m1658(L_224, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		GUIStyle_t302 * L_226 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_225, L_226, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIContent_t301 * L_227 = (__this->___showFpsContent_51);
		GUIStyle_t302 * L_228 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_229 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_230 = &(__this->___size_82);
		float L_231 = (L_230->___x_1);
		GUILayoutOption_t410 * L_232 = GUILayout_Width_m1651(NULL /*static, unused*/, L_231, /*hidden argument*/NULL);
		NullCheck(L_229);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_229, 0);
		ArrayElementTypeCheck (L_229, L_232);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_229, 0)) = (GUILayoutOption_t410 *)L_232;
		GUILayoutOptionU5BU5D_t409* L_233 = L_229;
		Vector2_t6 * L_234 = &(__this->___size_82);
		float L_235 = (L_234->___y_2);
		GUILayoutOption_t410 * L_236 = GUILayout_Height_m1652(NULL /*static, unused*/, L_235, /*hidden argument*/NULL);
		NullCheck(L_233);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_233, 1);
		ArrayElementTypeCheck (L_233, L_236);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_233, 1)) = (GUILayoutOption_t410 *)L_236;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_227, L_228, L_233, /*hidden argument*/NULL);
		float* L_237 = &(__this->___minFpsValue_134);
		String_t* L_238 = Single_ToString_m1658(L_237, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		GUIStyle_t302 * L_239 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_238, L_239, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reporter::drawStack()
extern TypeInfo* GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern "C" void Reporter_drawStack_m1109 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6  V_0 = {0};
	Sample_t290 * V_1 = {0};
	Exception_t232 * V_2 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Log_t291 * L_0 = (__this->___selectedLog_108);
		if (!L_0)
		{
			goto IL_0327;
		}
	}
	{
		Vector2_t6  L_1 = Reporter_getDrag_m1114(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ((&V_0)->___y_2);
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0075;
		}
	}
	{
		Rect_t304 * L_3 = &(__this->___stackRect_98);
		Vector2_t6 * L_4 = &(__this->___downPos_143);
		float L_5 = (L_4->___x_1);
		int32_t L_6 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_7 = &(__this->___downPos_143);
		float L_8 = (L_7->___y_2);
		Vector2_t6  L_9 = {0};
		Vector2__ctor_m630(&L_9, L_5, ((float)((float)(((float)L_6))-(float)L_8)), /*hidden argument*/NULL);
		bool L_10 = Rect_Contains_m1678(L_3, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0075;
		}
	}
	{
		Vector2_t6 * L_11 = &(__this->___scrollPosition2_106);
		Vector2_t6 * L_12 = L_11;
		float L_13 = (L_12->___y_2);
		float L_14 = ((&V_0)->___y_2);
		float L_15 = (__this->___oldDrag2_111);
		L_12->___y_2 = ((float)((float)L_13+(float)((float)((float)L_14-(float)L_15))));
	}

IL_0075:
	{
		float L_16 = ((&V_0)->___y_2);
		__this->___oldDrag2_111 = L_16;
		Rect_t304  L_17 = (__this->___stackRect_98);
		GUIStyle_t302 * L_18 = (__this->___backStyle_68);
		GUILayout_BeginArea_m1645(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector2_t6  L_19 = (__this->___scrollPosition2_106);
		Vector2_t6  L_20 = GUILayout_BeginScrollView_m1648(NULL /*static, unused*/, L_19, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		__this->___scrollPosition2_106 = L_20;
		V_1 = (Sample_t290 *)NULL;
	}

IL_00ac:
	try
	{ // begin try (depth: 1)
		List_1_t297 * L_21 = (__this->___samples_2);
		Log_t291 * L_22 = (__this->___selectedLog_108);
		NullCheck(L_22);
		int32_t L_23 = (L_22->___sampleId_4);
		NullCheck(L_21);
		Sample_t290 * L_24 = (Sample_t290 *)VirtFuncInvoker1< Sample_t290 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Reporter/Sample>::get_Item(System.Int32) */, L_21, L_23);
		V_1 = L_24;
		goto IL_00d4;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t232 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00c8;
		throw e;
	}

CATCH_00c8:
	{ // begin catch(System.Exception)
		V_2 = ((Exception_t232 *)__exception_local);
		Exception_t232 * L_25 = V_2;
		Debug_LogException_m1600(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		goto IL_00d4;
	} // end catch (depth: 1)

IL_00d4:
	{
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Log_t291 * L_26 = (__this->___selectedLog_108);
		NullCheck(L_26);
		String_t* L_27 = (L_26->___condition_2);
		GUIStyle_t302 * L_28 = (__this->___stackLabelStyle_74);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_27, L_28, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_29 = &(__this->___size_82);
		float L_30 = (L_29->___y_2);
		GUILayout_Space_m1649(NULL /*static, unused*/, ((float)((float)L_30*(float)(0.25f))), /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Log_t291 * L_31 = (__this->___selectedLog_108);
		NullCheck(L_31);
		String_t* L_32 = (L_31->___stacktrace_3);
		GUIStyle_t302 * L_33 = (__this->___stackLabelStyle_74);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_32, L_33, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_34 = &(__this->___size_82);
		float L_35 = (L_34->___y_2);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		GUILayout_EndScrollView_m1663(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t304  L_36 = (__this->___buttomRect_102);
		GUIStyle_t302 * L_37 = (__this->___backStyle_68);
		GUILayout_BeginArea_m1645(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1650(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIContent_t301 * L_38 = (__this->___showTimeContent_45);
		GUIStyle_t302 * L_39 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_40 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_41 = &(__this->___size_82);
		float L_42 = (L_41->___x_1);
		GUILayoutOption_t410 * L_43 = GUILayout_Width_m1651(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 0);
		ArrayElementTypeCheck (L_40, L_43);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_40, 0)) = (GUILayoutOption_t410 *)L_43;
		GUILayoutOptionU5BU5D_t409* L_44 = L_40;
		Vector2_t6 * L_45 = &(__this->___size_82);
		float L_46 = (L_45->___y_2);
		GUILayoutOption_t410 * L_47 = GUILayout_Height_m1652(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 1);
		ArrayElementTypeCheck (L_44, L_47);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_44, 1)) = (GUILayoutOption_t410 *)L_47;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_38, L_39, L_44, /*hidden argument*/NULL);
		Sample_t290 * L_48 = V_1;
		NullCheck(L_48);
		float* L_49 = &(L_48->___time_0);
		String_t* L_50 = Single_ToString_m1658(L_49, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		GUIStyle_t302 * L_51 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_50, L_51, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_52 = &(__this->___size_82);
		float L_53 = (L_52->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		GUIContent_t301 * L_54 = (__this->___showSceneContent_46);
		GUIStyle_t302 * L_55 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_56 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_57 = &(__this->___size_82);
		float L_58 = (L_57->___x_1);
		GUILayoutOption_t410 * L_59 = GUILayout_Width_m1651(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 0);
		ArrayElementTypeCheck (L_56, L_59);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_56, 0)) = (GUILayoutOption_t410 *)L_59;
		GUILayoutOptionU5BU5D_t409* L_60 = L_56;
		Vector2_t6 * L_61 = &(__this->___size_82);
		float L_62 = (L_61->___y_2);
		GUILayoutOption_t410 * L_63 = GUILayout_Height_m1652(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 1);
		ArrayElementTypeCheck (L_60, L_63);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_60, 1)) = (GUILayoutOption_t410 *)L_63;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_54, L_55, L_60, /*hidden argument*/NULL);
		StringU5BU5D_t243* L_64 = (__this->___scenes_85);
		Sample_t290 * L_65 = V_1;
		NullCheck(L_65);
		uint8_t L_66 = (L_65->___loadedScene_1);
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_66);
		uint8_t L_67 = L_66;
		GUIStyle_t302 * L_68 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_64, L_67)), L_68, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_69 = &(__this->___size_82);
		float L_70 = (L_69->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		GUIContent_t301 * L_71 = (__this->___showMemoryContent_48);
		GUIStyle_t302 * L_72 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_73 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_74 = &(__this->___size_82);
		float L_75 = (L_74->___x_1);
		GUILayoutOption_t410 * L_76 = GUILayout_Width_m1651(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, 0);
		ArrayElementTypeCheck (L_73, L_76);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_73, 0)) = (GUILayoutOption_t410 *)L_76;
		GUILayoutOptionU5BU5D_t409* L_77 = L_73;
		Vector2_t6 * L_78 = &(__this->___size_82);
		float L_79 = (L_78->___y_2);
		GUILayoutOption_t410 * L_80 = GUILayout_Height_m1652(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 1);
		ArrayElementTypeCheck (L_77, L_80);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_77, 1)) = (GUILayoutOption_t410 *)L_80;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_71, L_72, L_77, /*hidden argument*/NULL);
		Sample_t290 * L_81 = V_1;
		NullCheck(L_81);
		float* L_82 = &(L_81->___memory_2);
		String_t* L_83 = Single_ToString_m1658(L_82, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		GUIStyle_t302 * L_84 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_83, L_84, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		Vector2_t6 * L_85 = &(__this->___size_82);
		float L_86 = (L_85->___x_1);
		GUILayout_Space_m1649(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		GUIContent_t301 * L_87 = (__this->___showFpsContent_51);
		GUIStyle_t302 * L_88 = (__this->___nonStyle_66);
		GUILayoutOptionU5BU5D_t409* L_89 = ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 2));
		Vector2_t6 * L_90 = &(__this->___size_82);
		float L_91 = (L_90->___x_1);
		GUILayoutOption_t410 * L_92 = GUILayout_Width_m1651(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, 0);
		ArrayElementTypeCheck (L_89, L_92);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_89, 0)) = (GUILayoutOption_t410 *)L_92;
		GUILayoutOptionU5BU5D_t409* L_93 = L_89;
		Vector2_t6 * L_94 = &(__this->___size_82);
		float L_95 = (L_94->___y_2);
		GUILayoutOption_t410 * L_96 = GUILayout_Height_m1652(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, 1);
		ArrayElementTypeCheck (L_93, L_96);
		*((GUILayoutOption_t410 **)(GUILayoutOption_t410 **)SZArrayLdElema(L_93, 1)) = (GUILayoutOption_t410 *)L_96;
		GUILayout_Box_m1653(NULL /*static, unused*/, L_87, L_88, L_93, /*hidden argument*/NULL);
		Sample_t290 * L_97 = V_1;
		NullCheck(L_97);
		String_t* L_98 = (L_97->___fpsText_4);
		GUIStyle_t302 * L_99 = (__this->___nonStyle_66);
		GUILayout_Label_m1654(NULL /*static, unused*/, L_98, L_99, ((GUILayoutOptionU5BU5D_t409*)SZArrayNew(GUILayoutOptionU5BU5D_t409_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1655(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m1656(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0353;
	}

IL_0327:
	{
		Rect_t304  L_100 = (__this->___stackRect_98);
		GUIStyle_t302 * L_101 = (__this->___backStyle_68);
		GUILayout_BeginArea_m1645(NULL /*static, unused*/, L_100, L_101, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t304  L_102 = (__this->___buttomRect_102);
		GUIStyle_t302 * L_103 = (__this->___backStyle_68);
		GUILayout_BeginArea_m1645(NULL /*static, unused*/, L_102, L_103, /*hidden argument*/NULL);
		GUILayout_EndArea_m1664(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0353:
	{
		return;
	}
}
// System.Void Reporter::OnGUIDraw()
extern "C" void Reporter_OnGUIDraw_m1110 (Reporter_t295 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___show_8);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Rect_t304 * L_1 = &(__this->___screenRect_95);
		Rect_set_x_m1668(L_1, (0.0f), /*hidden argument*/NULL);
		Rect_t304 * L_2 = &(__this->___screenRect_95);
		Rect_set_y_m1669(L_2, (0.0f), /*hidden argument*/NULL);
		Rect_t304 * L_3 = &(__this->___screenRect_95);
		int32_t L_4 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m1670(L_3, (((float)L_4)), /*hidden argument*/NULL);
		Rect_t304 * L_5 = &(__this->___screenRect_95);
		int32_t L_6 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_height_m1671(L_5, (((float)L_6)), /*hidden argument*/NULL);
		Reporter_getDownPos_m1113(__this, /*hidden argument*/NULL);
		Rect_t304 * L_7 = &(__this->___logsRect_97);
		Rect_set_x_m1668(L_7, (0.0f), /*hidden argument*/NULL);
		Rect_t304 * L_8 = &(__this->___logsRect_97);
		Vector2_t6 * L_9 = &(__this->___size_82);
		float L_10 = (L_9->___y_2);
		Rect_set_y_m1669(L_8, ((float)((float)L_10*(float)(2.0f))), /*hidden argument*/NULL);
		Rect_t304 * L_11 = &(__this->___logsRect_97);
		int32_t L_12 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m1670(L_11, (((float)L_12)), /*hidden argument*/NULL);
		Rect_t304 * L_13 = &(__this->___logsRect_97);
		int32_t L_14 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_15 = &(__this->___size_82);
		float L_16 = (L_15->___y_2);
		Rect_set_height_m1671(L_13, ((float)((float)((float)((float)(((float)L_14))*(float)(0.75f)))-(float)((float)((float)L_16*(float)(2.0f))))), /*hidden argument*/NULL);
		Vector2_t6 * L_17 = &(__this->___stackRectTopLeft_103);
		L_17->___x_1 = (0.0f);
		Rect_t304 * L_18 = &(__this->___stackRect_98);
		Rect_set_x_m1668(L_18, (0.0f), /*hidden argument*/NULL);
		Vector2_t6 * L_19 = &(__this->___stackRectTopLeft_103);
		int32_t L_20 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_19->___y_2 = ((float)((float)(((float)L_20))*(float)(0.75f)));
		Rect_t304 * L_21 = &(__this->___stackRect_98);
		int32_t L_22 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_y_m1669(L_21, ((float)((float)(((float)L_22))*(float)(0.75f))), /*hidden argument*/NULL);
		Rect_t304 * L_23 = &(__this->___stackRect_98);
		int32_t L_24 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m1670(L_23, (((float)L_24)), /*hidden argument*/NULL);
		Rect_t304 * L_25 = &(__this->___stackRect_98);
		int32_t L_26 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_27 = &(__this->___size_82);
		float L_28 = (L_27->___y_2);
		Rect_set_height_m1671(L_25, ((float)((float)((float)((float)(((float)L_26))*(float)(0.25f)))-(float)L_28)), /*hidden argument*/NULL);
		Rect_t304 * L_29 = &(__this->___detailRect_104);
		Rect_set_x_m1668(L_29, (0.0f), /*hidden argument*/NULL);
		Rect_t304 * L_30 = &(__this->___detailRect_104);
		int32_t L_31 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_32 = &(__this->___size_82);
		float L_33 = (L_32->___y_2);
		Rect_set_y_m1669(L_30, ((float)((float)(((float)L_31))-(float)((float)((float)L_33*(float)(3.0f))))), /*hidden argument*/NULL);
		Rect_t304 * L_34 = &(__this->___detailRect_104);
		int32_t L_35 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m1670(L_34, (((float)L_35)), /*hidden argument*/NULL);
		Rect_t304 * L_36 = &(__this->___detailRect_104);
		Vector2_t6 * L_37 = &(__this->___size_82);
		float L_38 = (L_37->___y_2);
		Rect_set_height_m1671(L_36, ((float)((float)L_38*(float)(3.0f))), /*hidden argument*/NULL);
		int32_t L_39 = (__this->___currentView_39);
		if ((!(((uint32_t)L_39) == ((uint32_t)2))))
		{
			goto IL_01b4;
		}
	}
	{
		Reporter_DrawInfo_m1103(__this, /*hidden argument*/NULL);
		goto IL_01cc;
	}

IL_01b4:
	{
		int32_t L_40 = (__this->___currentView_39);
		if ((!(((uint32_t)L_40) == ((uint32_t)1))))
		{
			goto IL_01cc;
		}
	}
	{
		Reporter_drawToolBar_m1106(__this, /*hidden argument*/NULL);
		Reporter_DrawLogs_m1107(__this, /*hidden argument*/NULL);
	}

IL_01cc:
	{
		return;
	}
}
// System.Boolean Reporter::isGestureDone()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" bool Reporter_isGestureDone_m1111 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6  V_0 = {0};
	Vector2_t6  V_1 = {0};
	Vector2_t6  V_2 = {0};
	int32_t V_3 = 0;
	Vector2_t6  V_4 = {0};
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	int32_t V_7 = 0;
	Vector2_t6  V_8 = {0};
	Vector3_t4  V_9 = {0};
	Vector3_t4  V_10 = {0};
	Vector2_t6  V_11 = {0};
	{
		int32_t L_0 = Application_get_platform_m1583(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)11))))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m1583(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_00f0;
		}
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_2 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) == ((int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		List_1_t303 * L_3 = (__this->___gestureDetector_137);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear() */, L_3);
		__this->___gestureCount_140 = 0;
		goto IL_00eb;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_4 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = Touch_get_phase_m1687(((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_4, 0)), /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)4)))
		{
			goto IL_0067;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_6 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = Touch_get_phase_m1687(((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_6, 0)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_0077;
		}
	}

IL_0067:
	{
		List_1_t303 * L_8 = (__this->___gestureDetector_137);
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear() */, L_8);
		goto IL_00eb;
	}

IL_0077:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_9 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = Touch_get_phase_m1687(((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_9, 0)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_00eb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_11 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		Vector2_t6  L_12 = Touch_get_position_m729(((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_11, 0)), /*hidden argument*/NULL);
		V_0 = L_12;
		List_1_t303 * L_13 = (__this->___gestureDetector_137);
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count() */, L_13);
		if (!L_14)
		{
			goto IL_00df;
		}
	}
	{
		Vector2_t6  L_15 = V_0;
		List_1_t303 * L_16 = (__this->___gestureDetector_137);
		List_1_t303 * L_17 = (__this->___gestureDetector_137);
		NullCheck(L_17);
		int32_t L_18 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count() */, L_17);
		NullCheck(L_16);
		Vector2_t6  L_19 = (Vector2_t6 )VirtFuncInvoker1< Vector2_t6 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32) */, L_16, ((int32_t)((int32_t)L_18-(int32_t)1)));
		Vector2_t6  L_20 = Vector2_op_Subtraction_m1688(NULL /*static, unused*/, L_15, L_19, /*hidden argument*/NULL);
		V_8 = L_20;
		float L_21 = Vector2_get_magnitude_m1689((&V_8), /*hidden argument*/NULL);
		if ((!(((float)L_21) > ((float)(10.0f)))))
		{
			goto IL_00eb;
		}
	}

IL_00df:
	{
		List_1_t303 * L_22 = (__this->___gestureDetector_137);
		Vector2_t6  L_23 = V_0;
		NullCheck(L_22);
		VirtActionInvoker1< Vector2_t6  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0) */, L_22, L_23);
	}

IL_00eb:
	{
		goto IL_018d;
	}

IL_00f0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_24 = Input_GetMouseButtonUp_m663(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0112;
		}
	}
	{
		List_1_t303 * L_25 = (__this->___gestureDetector_137);
		NullCheck(L_25);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear() */, L_25);
		__this->___gestureCount_140 = 0;
		goto IL_018d;
	}

IL_0112:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_26 = Input_GetMouseButton_m924(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_018d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_27 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_9 = L_27;
		float L_28 = ((&V_9)->___x_1);
		Vector3_t4  L_29 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_10 = L_29;
		float L_30 = ((&V_10)->___y_2);
		Vector2__ctor_m630((&V_1), L_28, L_30, /*hidden argument*/NULL);
		List_1_t303 * L_31 = (__this->___gestureDetector_137);
		NullCheck(L_31);
		int32_t L_32 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count() */, L_31);
		if (!L_32)
		{
			goto IL_0181;
		}
	}
	{
		Vector2_t6  L_33 = V_1;
		List_1_t303 * L_34 = (__this->___gestureDetector_137);
		List_1_t303 * L_35 = (__this->___gestureDetector_137);
		NullCheck(L_35);
		int32_t L_36 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count() */, L_35);
		NullCheck(L_34);
		Vector2_t6  L_37 = (Vector2_t6 )VirtFuncInvoker1< Vector2_t6 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32) */, L_34, ((int32_t)((int32_t)L_36-(int32_t)1)));
		Vector2_t6  L_38 = Vector2_op_Subtraction_m1688(NULL /*static, unused*/, L_33, L_37, /*hidden argument*/NULL);
		V_11 = L_38;
		float L_39 = Vector2_get_magnitude_m1689((&V_11), /*hidden argument*/NULL);
		if ((!(((float)L_39) > ((float)(10.0f)))))
		{
			goto IL_018d;
		}
	}

IL_0181:
	{
		List_1_t303 * L_40 = (__this->___gestureDetector_137);
		Vector2_t6  L_41 = V_1;
		NullCheck(L_40);
		VirtActionInvoker1< Vector2_t6  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0) */, L_40, L_41);
	}

IL_018d:
	{
		List_1_t303 * L_42 = (__this->___gestureDetector_137);
		NullCheck(L_42);
		int32_t L_43 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count() */, L_42);
		if ((((int32_t)L_43) >= ((int32_t)((int32_t)10))))
		{
			goto IL_01a1;
		}
	}
	{
		return 0;
	}

IL_01a1:
	{
		Vector2_t6  L_44 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___gestureSum_138 = L_44;
		__this->___gestureLength_139 = (0.0f);
		Vector2_t6  L_45 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_45;
		V_3 = 0;
		goto IL_0241;
	}

IL_01c4:
	{
		List_1_t303 * L_46 = (__this->___gestureDetector_137);
		int32_t L_47 = V_3;
		NullCheck(L_46);
		Vector2_t6  L_48 = (Vector2_t6 )VirtFuncInvoker1< Vector2_t6 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32) */, L_46, ((int32_t)((int32_t)L_47+(int32_t)1)));
		List_1_t303 * L_49 = (__this->___gestureDetector_137);
		int32_t L_50 = V_3;
		NullCheck(L_49);
		Vector2_t6  L_51 = (Vector2_t6 )VirtFuncInvoker1< Vector2_t6 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32) */, L_49, L_50);
		Vector2_t6  L_52 = Vector2_op_Subtraction_m1688(NULL /*static, unused*/, L_48, L_51, /*hidden argument*/NULL);
		V_4 = L_52;
		float L_53 = Vector2_get_magnitude_m1689((&V_4), /*hidden argument*/NULL);
		V_5 = L_53;
		Vector2_t6  L_54 = (__this->___gestureSum_138);
		Vector2_t6  L_55 = V_4;
		Vector2_t6  L_56 = Vector2_op_Addition_m1690(NULL /*static, unused*/, L_54, L_55, /*hidden argument*/NULL);
		__this->___gestureSum_138 = L_56;
		float L_57 = (__this->___gestureLength_139);
		float L_58 = V_5;
		__this->___gestureLength_139 = ((float)((float)L_57+(float)L_58));
		Vector2_t6  L_59 = V_4;
		Vector2_t6  L_60 = V_2;
		float L_61 = Vector2_Dot_m1691(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		V_6 = L_61;
		float L_62 = V_6;
		if ((!(((float)L_62) < ((float)(0.0f)))))
		{
			goto IL_023a;
		}
	}
	{
		List_1_t303 * L_63 = (__this->___gestureDetector_137);
		NullCheck(L_63);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear() */, L_63);
		__this->___gestureCount_140 = 0;
		return 0;
	}

IL_023a:
	{
		Vector2_t6  L_64 = V_4;
		V_2 = L_64;
		int32_t L_65 = V_3;
		V_3 = ((int32_t)((int32_t)L_65+(int32_t)1));
	}

IL_0241:
	{
		int32_t L_66 = V_3;
		List_1_t303 * L_67 = (__this->___gestureDetector_137);
		NullCheck(L_67);
		int32_t L_68 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count() */, L_67);
		if ((((int32_t)L_66) < ((int32_t)((int32_t)((int32_t)L_68-(int32_t)2)))))
		{
			goto IL_01c4;
		}
	}
	{
		int32_t L_69 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_70 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)L_69+(int32_t)L_70))/(int32_t)4));
		float L_71 = (__this->___gestureLength_139);
		int32_t L_72 = V_7;
		if ((!(((float)L_71) > ((float)(((float)L_72))))))
		{
			goto IL_02b2;
		}
	}
	{
		Vector2_t6 * L_73 = &(__this->___gestureSum_138);
		float L_74 = Vector2_get_magnitude_m1689(L_73, /*hidden argument*/NULL);
		int32_t L_75 = V_7;
		if ((!(((float)L_74) < ((float)(((float)((int32_t)((int32_t)L_75/(int32_t)2))))))))
		{
			goto IL_02b2;
		}
	}
	{
		List_1_t303 * L_76 = (__this->___gestureDetector_137);
		NullCheck(L_76);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear() */, L_76);
		int32_t L_77 = (__this->___gestureCount_140);
		__this->___gestureCount_140 = ((int32_t)((int32_t)L_77+(int32_t)1));
		int32_t L_78 = (__this->___gestureCount_140);
		int32_t L_79 = (__this->___numOfCircleToShow_84);
		if ((((int32_t)L_78) < ((int32_t)L_79)))
		{
			goto IL_02b2;
		}
	}
	{
		return 1;
	}

IL_02b2:
	{
		return 0;
	}
}
// System.Boolean Reporter::isDoubleClickDone()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" bool Reporter_isDoubleClickDone_m1112 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m1583(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)11))))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m1583(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_009c;
		}
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_2 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) == ((int32_t)1)))
		{
			goto IL_0034;
		}
	}
	{
		__this->___lastClickTime_141 = (-1.0f);
		goto IL_0097;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_3 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = Touch_get_phase_m1687(((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_3, 0)), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0097;
		}
	}
	{
		float L_5 = (__this->___lastClickTime_141);
		if ((!(((float)L_5) == ((float)(-1.0f)))))
		{
			goto IL_0069;
		}
	}
	{
		float L_6 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___lastClickTime_141 = L_6;
		goto IL_0097;
	}

IL_0069:
	{
		float L_7 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = (__this->___lastClickTime_141);
		if ((!(((float)((float)((float)L_7-(float)L_8))) < ((float)(0.2f)))))
		{
			goto IL_008c;
		}
	}
	{
		__this->___lastClickTime_141 = (-1.0f);
		return 1;
	}

IL_008c:
	{
		float L_9 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___lastClickTime_141 = L_9;
	}

IL_0097:
	{
		goto IL_00f5;
	}

IL_009c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_10 = Input_GetMouseButtonDown_m977(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00f5;
		}
	}
	{
		float L_11 = (__this->___lastClickTime_141);
		if ((!(((float)L_11) == ((float)(-1.0f)))))
		{
			goto IL_00c7;
		}
	}
	{
		float L_12 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___lastClickTime_141 = L_12;
		goto IL_00f5;
	}

IL_00c7:
	{
		float L_13 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = (__this->___lastClickTime_141);
		if ((!(((float)((float)((float)L_13-(float)L_14))) < ((float)(0.2f)))))
		{
			goto IL_00ea;
		}
	}
	{
		__this->___lastClickTime_141 = (-1.0f);
		return 1;
	}

IL_00ea:
	{
		float L_15 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___lastClickTime_141 = L_15;
	}

IL_00f5:
	{
		return 0;
	}
}
// UnityEngine.Vector2 Reporter::getDownPos()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" Vector2_t6  Reporter_getDownPos_m1113 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	{
		int32_t L_0 = Application_get_platform_m1583(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)11))))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m1583(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_005b;
		}
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_2 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		if ((!(((uint32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) == ((uint32_t)1))))
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_3 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = Touch_get_phase_m1687(((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_3, 0)), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_5 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		Vector2_t6  L_6 = Touch_get_position_m729(((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_5, 0)), /*hidden argument*/NULL);
		__this->___downPos_143 = L_6;
		Vector2_t6  L_7 = (__this->___downPos_143);
		return L_7;
	}

IL_0056:
	{
		goto IL_009d;
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetMouseButtonDown_m977(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_009d;
		}
	}
	{
		Vector2_t6 * L_9 = &(__this->___downPos_143);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_10 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_10;
		float L_11 = ((&V_0)->___x_1);
		L_9->___x_1 = L_11;
		Vector2_t6 * L_12 = &(__this->___downPos_143);
		Vector3_t4  L_13 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_13;
		float L_14 = ((&V_1)->___y_2);
		L_12->___y_2 = L_14;
		Vector2_t6  L_15 = (__this->___downPos_143);
		return L_15;
	}

IL_009d:
	{
		Vector2_t6  L_16 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Vector2 Reporter::getDrag()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" Vector2_t6  Reporter_getDrag_m1114 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m1583(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)11))))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m1583(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_0046;
		}
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_2 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) == ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		Vector2_t6  L_3 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_4 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		Vector2_t6  L_5 = Touch_get_position_m729(((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_4, 0)), /*hidden argument*/NULL);
		Vector2_t6  L_6 = (__this->___downPos_143);
		Vector2_t6  L_7 = Vector2_op_Subtraction_m1688(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetMouseButton_m924(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0073;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_9 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6  L_10 = Vector2_op_Implicit_m619(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		__this->___mousePosition_144 = L_10;
		Vector2_t6  L_11 = (__this->___mousePosition_144);
		Vector2_t6  L_12 = (__this->___downPos_143);
		Vector2_t6  L_13 = Vector2_op_Subtraction_m1688(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0073:
	{
		Vector2_t6  L_14 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void Reporter::calculateStartIndex()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void Reporter_calculateStartIndex_m1115 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t6 * L_0 = &(__this->___scrollPosition_105);
		float L_1 = (L_0->___y_2);
		Vector2_t6 * L_2 = &(__this->___size_82);
		float L_3 = (L_2->___y_2);
		__this->___startIndex_113 = (((int32_t)((float)((float)L_1/(float)L_3))));
		int32_t L_4 = (__this->___startIndex_113);
		List_1_t298 * L_5 = (__this->___currentLog_5);
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Count() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_Clamp_m712(NULL /*static, unused*/, L_4, 0, L_6, /*hidden argument*/NULL);
		__this->___startIndex_113 = L_7;
		return;
	}
}
// System.Void Reporter::doShow()
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisReporterGUI_t305_m1591_MethodInfo_var;
extern "C" void Reporter_doShow_m1116 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		GameObject_AddComponent_TisReporterGUI_t305_m1591_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483690);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t232 * V_0 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		__this->___show_8 = 1;
		__this->___currentView_39 = 1;
		GameObject_t78 * L_0 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_AddComponent_TisReporterGUI_t305_m1591(L_0, /*hidden argument*/GameObject_AddComponent_TisReporterGUI_t305_m1591_MethodInfo_var);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		GameObject_t78 * L_1 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SendMessage_m1599(L_1, (String_t*) &_stringLiteral257, /*hidden argument*/NULL);
		goto IL_003b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t232 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002f;
		throw e;
	}

CATCH_002f:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t232 *)__exception_local);
		Exception_t232 * L_2 = V_0;
		Debug_LogException_m1600(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_003b;
	} // end catch (depth: 1)

IL_003b:
	{
		return;
	}
}
// System.Void Reporter::Update()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Reporter_Update_m1117 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	List_1_t298 * V_1 = {0};
	int32_t V_2 = 0;
	Log_t291 * V_3 = {0};
	float V_4 = 0.0f;
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		float* L_0 = &(__this->___fps_37);
		String_t* L_1 = Single_ToString_m1658(L_0, (String_t*) &_stringLiteral245, /*hidden argument*/NULL);
		__this->___fpsText_38 = L_1;
		int64_t L_2 = GC_GetTotalMemory_m1692(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->___gcTotalMemory_35 = ((float)((float)((float)((float)(((float)L_2))/(float)(1024.0f)))/(float)(1024.0f)));
		StringU5BU5D_t243* L_3 = (__this->___scenes_85);
		int32_t L_4 = Application_get_loadedLevel_m1012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m1587(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_3, L_5)), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		StringU5BU5D_t243* L_7 = (__this->___scenes_85);
		int32_t L_8 = Application_get_loadedLevel_m1012(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_9 = Application_get_loadedLevelName_m637(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, L_9);
		*((String_t**)(String_t**)SZArrayLdElema(L_7, L_8)) = (String_t*)L_9;
	}

IL_0056:
	{
		float L_10 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = (__this->___lastUpdate_145);
		V_0 = ((float)((float)L_10-(float)L_11));
		float L_12 = V_0;
		__this->___fps_37 = ((float)((float)(1.0f)/(float)L_12));
		float L_13 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___lastUpdate_145 = L_13;
		Reporter_calculateStartIndex_m1115(__this, /*hidden argument*/NULL);
		bool L_14 = (__this->___show_8);
		if (L_14)
		{
			goto IL_009d;
		}
	}
	{
		bool L_15 = Reporter_isGestureDone_m1111(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_009d;
		}
	}
	{
		Reporter_doShow_m1116(__this, /*hidden argument*/NULL);
	}

IL_009d:
	{
		List_1_t298 * L_16 = (__this->___threadedLogs_148);
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Count() */, L_16);
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_0113;
		}
	}
	{
		List_1_t298 * L_18 = (__this->___threadedLogs_148);
		V_1 = L_18;
		List_1_t298 * L_19 = V_1;
		Monitor_Enter_m1693(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_00bb:
	try
	{ // begin try (depth: 1)
		{
			V_2 = 0;
			goto IL_00eb;
		}

IL_00c2:
		{
			List_1_t298 * L_20 = (__this->___threadedLogs_148);
			int32_t L_21 = V_2;
			NullCheck(L_20);
			Log_t291 * L_22 = (Log_t291 *)VirtFuncInvoker1< Log_t291 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Reporter/Log>::get_Item(System.Int32) */, L_20, L_21);
			V_3 = L_22;
			Log_t291 * L_23 = V_3;
			NullCheck(L_23);
			String_t* L_24 = (L_23->___condition_2);
			Log_t291 * L_25 = V_3;
			NullCheck(L_25);
			String_t* L_26 = (L_25->___stacktrace_3);
			Log_t291 * L_27 = V_3;
			NullCheck(L_27);
			int32_t L_28 = (L_27->___logType_1);
			Reporter_AddLog_m1119(__this, L_24, L_26, L_28, /*hidden argument*/NULL);
			int32_t L_29 = V_2;
			V_2 = ((int32_t)((int32_t)L_29+(int32_t)1));
		}

IL_00eb:
		{
			int32_t L_30 = V_2;
			List_1_t298 * L_31 = (__this->___threadedLogs_148);
			NullCheck(L_31);
			int32_t L_32 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Count() */, L_31);
			if ((((int32_t)L_30) < ((int32_t)L_32)))
			{
				goto IL_00c2;
			}
		}

IL_00fc:
		{
			List_1_t298 * L_33 = (__this->___threadedLogs_148);
			NullCheck(L_33);
			VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Clear() */, L_33);
			IL2CPP_LEAVE(0x113, FINALLY_010c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_010c;
	}

FINALLY_010c:
	{ // begin finally (depth: 1)
		List_1_t298 * L_34 = V_1;
		Monitor_Exit_m1694(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(268)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(268)
	{
		IL2CPP_JUMP_TBL(0x113, IL_0113)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_0113:
	{
		float L_35 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_36 = (__this->___lastUpdate2_146);
		V_4 = ((float)((float)L_35-(float)L_36));
		float L_37 = V_4;
		if ((!(((float)L_37) > ((float)(1.0f)))))
		{
			goto IL_0138;
		}
	}
	{
		float L_38 = Time_get_realtimeSinceStartup_m961(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___lastUpdate2_146 = L_38;
	}

IL_0138:
	{
		return;
	}
}
// System.Void Reporter::CaptureLog(System.String,System.String,UnityEngine.LogType)
extern "C" void Reporter_CaptureLog_m1118 (Reporter_t295 * __this, String_t* ___condition, String_t* ___stacktrace, int32_t ___type, const MethodInfo* method)
{
	{
		String_t* L_0 = ___condition;
		String_t* L_1 = ___stacktrace;
		int32_t L_2 = ___type;
		Reporter_AddLog_m1119(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reporter::AddLog(System.String,System.String,UnityEngine.LogType)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Log_t291_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t254_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t232_il2cpp_TypeInfo_var;
extern const MethodInfo* MultiKeyDictionary_3_ContainsKey_m1697_MethodInfo_var;
extern const MethodInfo* MultiKeyDictionary_3_get_Item_m1644_MethodInfo_var;
extern "C" void Reporter_AddLog_m1119 (Reporter_t295 * __this, String_t* ___condition, String_t* ___stacktrace, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Log_t291_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(634);
		Single_t254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Exception_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		MultiKeyDictionary_3_ContainsKey_m1697_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483691);
		MultiKeyDictionary_3_get_Item_m1644_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483688);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	bool V_3 = false;
	Log_t291 * V_4 = {0};
	bool V_5 = false;
	bool V_6 = false;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	Exception_t232 * V_9 = {0};
	Log_t291 * V_10 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B4_0 = 0.0f;
	float G_B3_0 = 0.0f;
	int32_t G_B5_0 = 0;
	float G_B5_1 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B9_0 = 0.0f;
	int32_t G_B11_0 = 0;
	float G_B11_1 = 0.0f;
	{
		V_0 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_0;
		Dictionary_2_t300 * L_1 = (__this->___cachedString_7);
		String_t* L_2 = ___condition;
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_1, L_2);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		Dictionary_2_t300 * L_4 = (__this->___cachedString_7);
		String_t* L_5 = ___condition;
		NullCheck(L_4);
		String_t* L_6 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_4, L_5);
		V_1 = L_6;
		goto IL_0064;
	}

IL_002f:
	{
		String_t* L_7 = ___condition;
		V_1 = L_7;
		Dictionary_2_t300 * L_8 = (__this->___cachedString_7);
		String_t* L_9 = V_1;
		String_t* L_10 = V_1;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_8, L_9, L_10);
		float L_11 = V_0;
		String_t* L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_IsNullOrEmpty_m1587(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		G_B3_0 = L_11;
		if (!L_13)
		{
			G_B4_0 = L_11;
			goto IL_0050;
		}
	}
	{
		G_B5_0 = 0;
		G_B5_1 = G_B3_0;
		goto IL_0058;
	}

IL_0050:
	{
		String_t* L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1578(L_14, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)((int32_t)L_15*(int32_t)2));
		G_B5_1 = G_B4_0;
	}

IL_0058:
	{
		V_0 = ((float)((float)G_B5_1+(float)(((float)G_B5_0))));
		float L_16 = V_0;
		int32_t L_17 = IntPtr_get_Size_m1695(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_16+(float)(((float)L_17))));
	}

IL_0064:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_2 = L_18;
		Dictionary_2_t300 * L_19 = (__this->___cachedString_7);
		String_t* L_20 = ___stacktrace;
		NullCheck(L_19);
		bool L_21 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_19, L_20);
		if (!L_21)
		{
			goto IL_008d;
		}
	}
	{
		Dictionary_2_t300 * L_22 = (__this->___cachedString_7);
		String_t* L_23 = ___stacktrace;
		NullCheck(L_22);
		String_t* L_24 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_22, L_23);
		V_2 = L_24;
		goto IL_00c2;
	}

IL_008d:
	{
		String_t* L_25 = ___stacktrace;
		V_2 = L_25;
		Dictionary_2_t300 * L_26 = (__this->___cachedString_7);
		String_t* L_27 = V_2;
		String_t* L_28 = V_2;
		NullCheck(L_26);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_26, L_27, L_28);
		float L_29 = V_0;
		String_t* L_30 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_31 = String_IsNullOrEmpty_m1587(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		G_B9_0 = L_29;
		if (!L_31)
		{
			G_B10_0 = L_29;
			goto IL_00ae;
		}
	}
	{
		G_B11_0 = 0;
		G_B11_1 = G_B9_0;
		goto IL_00b6;
	}

IL_00ae:
	{
		String_t* L_32 = V_2;
		NullCheck(L_32);
		int32_t L_33 = String_get_Length_m1578(L_32, /*hidden argument*/NULL);
		G_B11_0 = ((int32_t)((int32_t)L_33*(int32_t)2));
		G_B11_1 = G_B10_0;
	}

IL_00b6:
	{
		V_0 = ((float)((float)G_B11_1+(float)(((float)G_B11_0))));
		float L_34 = V_0;
		int32_t L_35 = IntPtr_get_Size_m1695(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_34+(float)(((float)L_35))));
	}

IL_00c2:
	{
		V_3 = 0;
		Reporter_addSample_m1097(__this, /*hidden argument*/NULL);
		Log_t291 * L_36 = (Log_t291 *)il2cpp_codegen_object_new (Log_t291_il2cpp_TypeInfo_var);
		Log__ctor_m1082(L_36, /*hidden argument*/NULL);
		V_10 = L_36;
		Log_t291 * L_37 = V_10;
		int32_t L_38 = ___type;
		NullCheck(L_37);
		L_37->___logType_1 = L_38;
		Log_t291 * L_39 = V_10;
		String_t* L_40 = V_1;
		NullCheck(L_39);
		L_39->___condition_2 = L_40;
		Log_t291 * L_41 = V_10;
		String_t* L_42 = V_2;
		NullCheck(L_41);
		L_41->___stacktrace_3 = L_42;
		Log_t291 * L_43 = V_10;
		List_1_t297 * L_44 = (__this->___samples_2);
		NullCheck(L_44);
		int32_t L_45 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::get_Count() */, L_44);
		NullCheck(L_43);
		L_43->___sampleId_4 = ((int32_t)((int32_t)L_45-(int32_t)1));
		Log_t291 * L_46 = V_10;
		V_4 = L_46;
		float L_47 = V_0;
		Log_t291 * L_48 = V_4;
		NullCheck(L_48);
		float L_49 = Log_GetMemoryUsage_m1084(L_48, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_47+(float)L_49));
		float L_50 = (__this->___logsMemUsage_33);
		float L_51 = V_0;
		__this->___logsMemUsage_33 = ((float)((float)L_50+(float)((float)((float)((float)((float)L_51/(float)(1024.0f)))/(float)(1024.0f)))));
		float L_52 = Reporter_get_TotalMemUsage_m1093(__this, /*hidden argument*/NULL);
		float L_53 = (__this->___maxSize_83);
		if ((!(((float)L_52) > ((float)L_53))))
		{
			goto IL_015c;
		}
	}
	{
		Reporter_clear_m1101(__this, /*hidden argument*/NULL);
		float L_54 = (__this->___maxSize_83);
		float L_55 = L_54;
		Object_t * L_56 = Box(Single_t254_il2cpp_TypeInfo_var, &L_55);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_57 = String_Concat_m1696(NULL /*static, unused*/, (String_t*) &_stringLiteral258, L_56, (String_t*) &_stringLiteral259, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		return;
	}

IL_015c:
	{
		V_5 = 0;
		MultiKeyDictionary_3_t299 * L_58 = (__this->___logsDic_6);
		String_t* L_59 = V_1;
		String_t* L_60 = ___stacktrace;
		NullCheck(L_58);
		bool L_61 = MultiKeyDictionary_3_ContainsKey_m1697(L_58, L_59, L_60, /*hidden argument*/MultiKeyDictionary_3_ContainsKey_m1697_MethodInfo_var);
		if (!L_61)
		{
			goto IL_0198;
		}
	}
	{
		V_5 = 0;
		MultiKeyDictionary_3_t299 * L_62 = (__this->___logsDic_6);
		String_t* L_63 = V_1;
		NullCheck(L_62);
		Dictionary_2_t407 * L_64 = MultiKeyDictionary_3_get_Item_m1644(L_62, L_63, /*hidden argument*/MultiKeyDictionary_3_get_Item_m1644_MethodInfo_var);
		String_t* L_65 = ___stacktrace;
		NullCheck(L_64);
		Log_t291 * L_66 = (Log_t291 *)VirtFuncInvoker1< Log_t291 *, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>::get_Item(!0) */, L_64, L_65);
		Log_t291 * L_67 = L_66;
		NullCheck(L_67);
		int32_t L_68 = (L_67->___count_0);
		NullCheck(L_67);
		L_67->___count_0 = ((int32_t)((int32_t)L_68+(int32_t)1));
		goto IL_01fe;
	}

IL_0198:
	{
		V_5 = 1;
		List_1_t298 * L_69 = (__this->___collapsedLogs_4);
		Log_t291 * L_70 = V_4;
		NullCheck(L_69);
		VirtActionInvoker1< Log_t291 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Add(!0) */, L_69, L_70);
		MultiKeyDictionary_3_t299 * L_71 = (__this->___logsDic_6);
		String_t* L_72 = V_1;
		NullCheck(L_71);
		Dictionary_2_t407 * L_73 = MultiKeyDictionary_3_get_Item_m1644(L_71, L_72, /*hidden argument*/MultiKeyDictionary_3_get_Item_m1644_MethodInfo_var);
		String_t* L_74 = ___stacktrace;
		Log_t291 * L_75 = V_4;
		NullCheck(L_73);
		VirtActionInvoker2< String_t*, Log_t291 * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>::set_Item(!0,!1) */, L_73, L_74, L_75);
		int32_t L_76 = ___type;
		if ((!(((uint32_t)L_76) == ((uint32_t)3))))
		{
			goto IL_01d6;
		}
	}
	{
		int32_t L_77 = (__this->___numOfCollapsedLogs_22);
		__this->___numOfCollapsedLogs_22 = ((int32_t)((int32_t)L_77+(int32_t)1));
		goto IL_01fe;
	}

IL_01d6:
	{
		int32_t L_78 = ___type;
		if ((!(((uint32_t)L_78) == ((uint32_t)2))))
		{
			goto IL_01f0;
		}
	}
	{
		int32_t L_79 = (__this->___numOfCollapsedLogsWarning_23);
		__this->___numOfCollapsedLogsWarning_23 = ((int32_t)((int32_t)L_79+(int32_t)1));
		goto IL_01fe;
	}

IL_01f0:
	{
		int32_t L_80 = (__this->___numOfCollapsedLogsError_24);
		__this->___numOfCollapsedLogsError_24 = ((int32_t)((int32_t)L_80+(int32_t)1));
	}

IL_01fe:
	{
		int32_t L_81 = ___type;
		if ((!(((uint32_t)L_81) == ((uint32_t)3))))
		{
			goto IL_0218;
		}
	}
	{
		int32_t L_82 = (__this->___numOfLogs_19);
		__this->___numOfLogs_19 = ((int32_t)((int32_t)L_82+(int32_t)1));
		goto IL_0240;
	}

IL_0218:
	{
		int32_t L_83 = ___type;
		if ((!(((uint32_t)L_83) == ((uint32_t)2))))
		{
			goto IL_0232;
		}
	}
	{
		int32_t L_84 = (__this->___numOfLogsWarning_20);
		__this->___numOfLogsWarning_20 = ((int32_t)((int32_t)L_84+(int32_t)1));
		goto IL_0240;
	}

IL_0232:
	{
		int32_t L_85 = (__this->___numOfLogsError_21);
		__this->___numOfLogsError_21 = ((int32_t)((int32_t)L_85+(int32_t)1));
	}

IL_0240:
	{
		List_1_t298 * L_86 = (__this->___logs_3);
		Log_t291 * L_87 = V_4;
		NullCheck(L_86);
		VirtActionInvoker1< Log_t291 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Add(!0) */, L_86, L_87);
		bool L_88 = (__this->___collapse_9);
		if (!L_88)
		{
			goto IL_025f;
		}
	}
	{
		bool L_89 = V_5;
		if (!L_89)
		{
			goto IL_032f;
		}
	}

IL_025f:
	{
		V_6 = 0;
		Log_t291 * L_90 = V_4;
		NullCheck(L_90);
		int32_t L_91 = (L_90->___logType_1);
		if ((!(((uint32_t)L_91) == ((uint32_t)3))))
		{
			goto IL_027d;
		}
	}
	{
		bool L_92 = (__this->___showLog_16);
		if (L_92)
		{
			goto IL_027d;
		}
	}
	{
		V_6 = 1;
	}

IL_027d:
	{
		Log_t291 * L_93 = V_4;
		NullCheck(L_93);
		int32_t L_94 = (L_93->___logType_1);
		if ((!(((uint32_t)L_94) == ((uint32_t)2))))
		{
			goto IL_0298;
		}
	}
	{
		bool L_95 = (__this->___showWarning_17);
		if (L_95)
		{
			goto IL_0298;
		}
	}
	{
		V_6 = 1;
	}

IL_0298:
	{
		Log_t291 * L_96 = V_4;
		NullCheck(L_96);
		int32_t L_97 = (L_96->___logType_1);
		if (L_97)
		{
			goto IL_02b2;
		}
	}
	{
		bool L_98 = (__this->___showError_18);
		if (L_98)
		{
			goto IL_02b2;
		}
	}
	{
		V_6 = 1;
	}

IL_02b2:
	{
		Log_t291 * L_99 = V_4;
		NullCheck(L_99);
		int32_t L_100 = (L_99->___logType_1);
		if ((!(((uint32_t)L_100) == ((uint32_t)1))))
		{
			goto IL_02cd;
		}
	}
	{
		bool L_101 = (__this->___showError_18);
		if (L_101)
		{
			goto IL_02cd;
		}
	}
	{
		V_6 = 1;
	}

IL_02cd:
	{
		Log_t291 * L_102 = V_4;
		NullCheck(L_102);
		int32_t L_103 = (L_102->___logType_1);
		if ((!(((uint32_t)L_103) == ((uint32_t)4))))
		{
			goto IL_02e8;
		}
	}
	{
		bool L_104 = (__this->___showError_18);
		if (L_104)
		{
			goto IL_02e8;
		}
	}
	{
		V_6 = 1;
	}

IL_02e8:
	{
		bool L_105 = V_6;
		if (L_105)
		{
			goto IL_032f;
		}
	}
	{
		String_t* L_106 = (__this->___filterText_87);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_107 = String_IsNullOrEmpty_m1587(NULL /*static, unused*/, L_106, /*hidden argument*/NULL);
		if (L_107)
		{
			goto IL_0320;
		}
	}
	{
		Log_t291 * L_108 = V_4;
		NullCheck(L_108);
		String_t* L_109 = (L_108->___condition_2);
		NullCheck(L_109);
		String_t* L_110 = String_ToLower_m1643(L_109, /*hidden argument*/NULL);
		String_t* L_111 = (__this->___filterText_87);
		NullCheck(L_111);
		String_t* L_112 = String_ToLower_m1643(L_111, /*hidden argument*/NULL);
		NullCheck(L_110);
		bool L_113 = String_Contains_m1584(L_110, L_112, /*hidden argument*/NULL);
		if (!L_113)
		{
			goto IL_032f;
		}
	}

IL_0320:
	{
		List_1_t298 * L_114 = (__this->___currentLog_5);
		Log_t291 * L_115 = V_4;
		NullCheck(L_114);
		VirtActionInvoker1< Log_t291 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Add(!0) */, L_114, L_115);
		V_3 = 1;
	}

IL_032f:
	{
		bool L_116 = V_3;
		if (!L_116)
		{
			goto IL_0390;
		}
	}
	{
		Reporter_calculateStartIndex_m1115(__this, /*hidden argument*/NULL);
		List_1_t298 * L_117 = (__this->___currentLog_5);
		NullCheck(L_117);
		int32_t L_118 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Count() */, L_117);
		V_7 = L_118;
		int32_t L_119 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6 * L_120 = &(__this->___size_82);
		float L_121 = (L_120->___y_2);
		V_8 = (((int32_t)((float)((float)((float)((float)(((float)L_119))*(float)(0.75f)))/(float)L_121))));
		int32_t L_122 = (__this->___startIndex_113);
		int32_t L_123 = V_7;
		int32_t L_124 = V_8;
		if ((((int32_t)L_122) < ((int32_t)((int32_t)((int32_t)L_123-(int32_t)L_124)))))
		{
			goto IL_0390;
		}
	}
	{
		Vector2_t6 * L_125 = &(__this->___scrollPosition_105);
		Vector2_t6 * L_126 = L_125;
		float L_127 = (L_126->___y_2);
		Vector2_t6 * L_128 = &(__this->___size_82);
		float L_129 = (L_128->___y_2);
		L_126->___y_2 = ((float)((float)L_127+(float)L_129));
	}

IL_0390:
	try
	{ // begin try (depth: 1)
		GameObject_t78 * L_130 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Log_t291 * L_131 = V_4;
		NullCheck(L_130);
		GameObject_SendMessage_m1698(L_130, (String_t*) &_stringLiteral260, L_131, /*hidden argument*/NULL);
		goto IL_03b5;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t232 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_03a7;
		throw e;
	}

CATCH_03a7:
	{ // begin catch(System.Exception)
		V_9 = ((Exception_t232 *)__exception_local);
		Exception_t232 * L_132 = V_9;
		Debug_LogException_m1600(NULL /*static, unused*/, L_132, /*hidden argument*/NULL);
		goto IL_03b5;
	} // end catch (depth: 1)

IL_03b5:
	{
		return;
	}
}
// System.Void Reporter::CaptureLogThread(System.String,System.String,UnityEngine.LogType)
extern TypeInfo* Log_t291_il2cpp_TypeInfo_var;
extern "C" void Reporter_CaptureLogThread_m1120 (Reporter_t295 * __this, String_t* ___condition, String_t* ___stacktrace, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Log_t291_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(634);
		s_Il2CppMethodIntialized = true;
	}
	Log_t291 * V_0 = {0};
	List_1_t298 * V_1 = {0};
	Log_t291 * V_2 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Log_t291 * L_0 = (Log_t291 *)il2cpp_codegen_object_new (Log_t291_il2cpp_TypeInfo_var);
		Log__ctor_m1082(L_0, /*hidden argument*/NULL);
		V_2 = L_0;
		Log_t291 * L_1 = V_2;
		String_t* L_2 = ___condition;
		NullCheck(L_1);
		L_1->___condition_2 = L_2;
		Log_t291 * L_3 = V_2;
		String_t* L_4 = ___stacktrace;
		NullCheck(L_3);
		L_3->___stacktrace_3 = L_4;
		Log_t291 * L_5 = V_2;
		int32_t L_6 = ___type;
		NullCheck(L_5);
		L_5->___logType_1 = L_6;
		Log_t291 * L_7 = V_2;
		V_0 = L_7;
		List_1_t298 * L_8 = (__this->___threadedLogs_148);
		V_1 = L_8;
		List_1_t298 * L_9 = V_1;
		Monitor_Enter_m1693(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		List_1_t298 * L_10 = (__this->___threadedLogs_148);
		Log_t291 * L_11 = V_0;
		NullCheck(L_10);
		VirtActionInvoker1< Log_t291 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Reporter/Log>::Add(!0) */, L_10, L_11);
		IL2CPP_LEAVE(0x42, FINALLY_003b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		List_1_t298 * L_12 = V_1;
		Monitor_Exit_m1694(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_0042:
	{
		return;
	}
}
// System.Void Reporter::OnLevelWasLoaded()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Reporter_OnLevelWasLoaded_m1121 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___clearOnNewSceneLoaded_10);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Reporter_clear_m1101(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		String_t* L_1 = Application_get_loadedLevelName_m637(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___currentScene_86 = L_1;
		String_t* L_2 = Application_get_loadedLevelName_m637(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m734(NULL /*static, unused*/, (String_t*) &_stringLiteral261, L_2, (String_t*) &_stringLiteral262, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reporter::OnApplicationQuit()
extern "C" void Reporter_OnApplicationQuit_m1122 (Reporter_t295 * __this, const MethodInfo* method)
{
	String_t* G_B2_0 = {0};
	String_t* G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = {0};
	String_t* G_B5_0 = {0};
	String_t* G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	String_t* G_B6_1 = {0};
	String_t* G_B8_0 = {0};
	String_t* G_B7_0 = {0};
	int32_t G_B9_0 = 0;
	String_t* G_B9_1 = {0};
	String_t* G_B11_0 = {0};
	String_t* G_B10_0 = {0};
	int32_t G_B12_0 = 0;
	String_t* G_B12_1 = {0};
	String_t* G_B14_0 = {0};
	String_t* G_B13_0 = {0};
	int32_t G_B15_0 = 0;
	String_t* G_B15_1 = {0};
	String_t* G_B17_0 = {0};
	String_t* G_B16_0 = {0};
	int32_t G_B18_0 = 0;
	String_t* G_B18_1 = {0};
	String_t* G_B20_0 = {0};
	String_t* G_B19_0 = {0};
	int32_t G_B21_0 = 0;
	String_t* G_B21_1 = {0};
	String_t* G_B23_0 = {0};
	String_t* G_B22_0 = {0};
	int32_t G_B24_0 = 0;
	String_t* G_B24_1 = {0};
	String_t* G_B26_0 = {0};
	String_t* G_B25_0 = {0};
	int32_t G_B27_0 = 0;
	String_t* G_B27_1 = {0};
	String_t* G_B29_0 = {0};
	String_t* G_B28_0 = {0};
	int32_t G_B30_0 = 0;
	String_t* G_B30_1 = {0};
	String_t* G_B32_0 = {0};
	String_t* G_B31_0 = {0};
	int32_t G_B33_0 = 0;
	String_t* G_B33_1 = {0};
	String_t* G_B35_0 = {0};
	String_t* G_B34_0 = {0};
	int32_t G_B36_0 = 0;
	String_t* G_B36_1 = {0};
	String_t* G_B38_0 = {0};
	String_t* G_B37_0 = {0};
	int32_t G_B39_0 = 0;
	String_t* G_B39_1 = {0};
	String_t* G_B41_0 = {0};
	String_t* G_B40_0 = {0};
	int32_t G_B42_0 = 0;
	String_t* G_B42_1 = {0};
	String_t* G_B44_0 = {0};
	String_t* G_B43_0 = {0};
	int32_t G_B45_0 = 0;
	String_t* G_B45_1 = {0};
	String_t* G_B47_0 = {0};
	String_t* G_B46_0 = {0};
	int32_t G_B48_0 = 0;
	String_t* G_B48_1 = {0};
	String_t* G_B50_0 = {0};
	String_t* G_B49_0 = {0};
	int32_t G_B51_0 = 0;
	String_t* G_B51_1 = {0};
	{
		int32_t L_0 = (__this->___currentView_39);
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, (String_t*) &_stringLiteral220, L_0, /*hidden argument*/NULL);
		bool L_1 = (__this->___show_8);
		G_B1_0 = (String_t*) &_stringLiteral221;
		if (!L_1)
		{
			G_B2_0 = (String_t*) &_stringLiteral221;
			goto IL_0026;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_0027;
	}

IL_0026:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0027:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		bool L_2 = (__this->___collapse_9);
		G_B4_0 = (String_t*) &_stringLiteral222;
		if (!L_2)
		{
			G_B5_0 = (String_t*) &_stringLiteral222;
			goto IL_0042;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0043;
	}

IL_0042:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0043:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B6_1, G_B6_0, /*hidden argument*/NULL);
		bool L_3 = (__this->___clearOnNewSceneLoaded_10);
		G_B7_0 = (String_t*) &_stringLiteral223;
		if (!L_3)
		{
			G_B8_0 = (String_t*) &_stringLiteral223;
			goto IL_005e;
		}
	}
	{
		G_B9_0 = 1;
		G_B9_1 = G_B7_0;
		goto IL_005f;
	}

IL_005e:
	{
		G_B9_0 = 0;
		G_B9_1 = G_B8_0;
	}

IL_005f:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B9_1, G_B9_0, /*hidden argument*/NULL);
		bool L_4 = (__this->___showTime_11);
		G_B10_0 = (String_t*) &_stringLiteral224;
		if (!L_4)
		{
			G_B11_0 = (String_t*) &_stringLiteral224;
			goto IL_007a;
		}
	}
	{
		G_B12_0 = 1;
		G_B12_1 = G_B10_0;
		goto IL_007b;
	}

IL_007a:
	{
		G_B12_0 = 0;
		G_B12_1 = G_B11_0;
	}

IL_007b:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		bool L_5 = (__this->___showScene_12);
		G_B13_0 = (String_t*) &_stringLiteral225;
		if (!L_5)
		{
			G_B14_0 = (String_t*) &_stringLiteral225;
			goto IL_0096;
		}
	}
	{
		G_B15_0 = 1;
		G_B15_1 = G_B13_0;
		goto IL_0097;
	}

IL_0096:
	{
		G_B15_0 = 0;
		G_B15_1 = G_B14_0;
	}

IL_0097:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B15_1, G_B15_0, /*hidden argument*/NULL);
		bool L_6 = (__this->___showMemory_13);
		G_B16_0 = (String_t*) &_stringLiteral226;
		if (!L_6)
		{
			G_B17_0 = (String_t*) &_stringLiteral226;
			goto IL_00b2;
		}
	}
	{
		G_B18_0 = 1;
		G_B18_1 = G_B16_0;
		goto IL_00b3;
	}

IL_00b2:
	{
		G_B18_0 = 0;
		G_B18_1 = G_B17_0;
	}

IL_00b3:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B18_1, G_B18_0, /*hidden argument*/NULL);
		bool L_7 = (__this->___showFps_14);
		G_B19_0 = (String_t*) &_stringLiteral227;
		if (!L_7)
		{
			G_B20_0 = (String_t*) &_stringLiteral227;
			goto IL_00ce;
		}
	}
	{
		G_B21_0 = 1;
		G_B21_1 = G_B19_0;
		goto IL_00cf;
	}

IL_00ce:
	{
		G_B21_0 = 0;
		G_B21_1 = G_B20_0;
	}

IL_00cf:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B21_1, G_B21_0, /*hidden argument*/NULL);
		bool L_8 = (__this->___showGraph_15);
		G_B22_0 = (String_t*) &_stringLiteral228;
		if (!L_8)
		{
			G_B23_0 = (String_t*) &_stringLiteral228;
			goto IL_00ea;
		}
	}
	{
		G_B24_0 = 1;
		G_B24_1 = G_B22_0;
		goto IL_00eb;
	}

IL_00ea:
	{
		G_B24_0 = 0;
		G_B24_1 = G_B23_0;
	}

IL_00eb:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B24_1, G_B24_0, /*hidden argument*/NULL);
		bool L_9 = (__this->___showLog_16);
		G_B25_0 = (String_t*) &_stringLiteral229;
		if (!L_9)
		{
			G_B26_0 = (String_t*) &_stringLiteral229;
			goto IL_0106;
		}
	}
	{
		G_B27_0 = 1;
		G_B27_1 = G_B25_0;
		goto IL_0107;
	}

IL_0106:
	{
		G_B27_0 = 0;
		G_B27_1 = G_B26_0;
	}

IL_0107:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B27_1, G_B27_0, /*hidden argument*/NULL);
		bool L_10 = (__this->___showWarning_17);
		G_B28_0 = (String_t*) &_stringLiteral230;
		if (!L_10)
		{
			G_B29_0 = (String_t*) &_stringLiteral230;
			goto IL_0122;
		}
	}
	{
		G_B30_0 = 1;
		G_B30_1 = G_B28_0;
		goto IL_0123;
	}

IL_0122:
	{
		G_B30_0 = 0;
		G_B30_1 = G_B29_0;
	}

IL_0123:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B30_1, G_B30_0, /*hidden argument*/NULL);
		bool L_11 = (__this->___showError_18);
		G_B31_0 = (String_t*) &_stringLiteral231;
		if (!L_11)
		{
			G_B32_0 = (String_t*) &_stringLiteral231;
			goto IL_013e;
		}
	}
	{
		G_B33_0 = 1;
		G_B33_1 = G_B31_0;
		goto IL_013f;
	}

IL_013e:
	{
		G_B33_0 = 0;
		G_B33_1 = G_B32_0;
	}

IL_013f:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B33_1, G_B33_0, /*hidden argument*/NULL);
		String_t* L_12 = (__this->___filterText_87);
		PlayerPrefs_SetString_m1700(NULL /*static, unused*/, (String_t*) &_stringLiteral232, L_12, /*hidden argument*/NULL);
		Vector2_t6 * L_13 = &(__this->___size_82);
		float L_14 = (L_13->___x_1);
		PlayerPrefs_SetFloat_m1701(NULL /*static, unused*/, (String_t*) &_stringLiteral233, L_14, /*hidden argument*/NULL);
		bool L_15 = (__this->___showClearOnNewSceneLoadedButton_25);
		G_B34_0 = (String_t*) &_stringLiteral234;
		if (!L_15)
		{
			G_B35_0 = (String_t*) &_stringLiteral234;
			goto IL_017f;
		}
	}
	{
		G_B36_0 = 1;
		G_B36_1 = G_B34_0;
		goto IL_0180;
	}

IL_017f:
	{
		G_B36_0 = 0;
		G_B36_1 = G_B35_0;
	}

IL_0180:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B36_1, G_B36_0, /*hidden argument*/NULL);
		bool L_16 = (__this->___showTimeButton_26);
		G_B37_0 = (String_t*) &_stringLiteral235;
		if (!L_16)
		{
			G_B38_0 = (String_t*) &_stringLiteral235;
			goto IL_019b;
		}
	}
	{
		G_B39_0 = 1;
		G_B39_1 = G_B37_0;
		goto IL_019c;
	}

IL_019b:
	{
		G_B39_0 = 0;
		G_B39_1 = G_B38_0;
	}

IL_019c:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B39_1, G_B39_0, /*hidden argument*/NULL);
		bool L_17 = (__this->___showSceneButton_27);
		G_B40_0 = (String_t*) &_stringLiteral236;
		if (!L_17)
		{
			G_B41_0 = (String_t*) &_stringLiteral236;
			goto IL_01b7;
		}
	}
	{
		G_B42_0 = 1;
		G_B42_1 = G_B40_0;
		goto IL_01b8;
	}

IL_01b7:
	{
		G_B42_0 = 0;
		G_B42_1 = G_B41_0;
	}

IL_01b8:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B42_1, G_B42_0, /*hidden argument*/NULL);
		bool L_18 = (__this->___showMemButton_28);
		G_B43_0 = (String_t*) &_stringLiteral237;
		if (!L_18)
		{
			G_B44_0 = (String_t*) &_stringLiteral237;
			goto IL_01d3;
		}
	}
	{
		G_B45_0 = 1;
		G_B45_1 = G_B43_0;
		goto IL_01d4;
	}

IL_01d3:
	{
		G_B45_0 = 0;
		G_B45_1 = G_B44_0;
	}

IL_01d4:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B45_1, G_B45_0, /*hidden argument*/NULL);
		bool L_19 = (__this->___showFpsButton_29);
		G_B46_0 = (String_t*) &_stringLiteral238;
		if (!L_19)
		{
			G_B47_0 = (String_t*) &_stringLiteral238;
			goto IL_01ef;
		}
	}
	{
		G_B48_0 = 1;
		G_B48_1 = G_B46_0;
		goto IL_01f0;
	}

IL_01ef:
	{
		G_B48_0 = 0;
		G_B48_1 = G_B47_0;
	}

IL_01f0:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B48_1, G_B48_0, /*hidden argument*/NULL);
		bool L_20 = (__this->___showSearchText_30);
		G_B49_0 = (String_t*) &_stringLiteral239;
		if (!L_20)
		{
			G_B50_0 = (String_t*) &_stringLiteral239;
			goto IL_020b;
		}
	}
	{
		G_B51_0 = 1;
		G_B51_1 = G_B49_0;
		goto IL_020c;
	}

IL_020b:
	{
		G_B51_0 = 0;
		G_B51_1 = G_B50_0;
	}

IL_020c:
	{
		PlayerPrefs_SetInt_m1699(NULL /*static, unused*/, G_B51_1, G_B51_0, /*hidden argument*/NULL);
		PlayerPrefs_Save_m1702(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Reporter::readInfo()
extern TypeInfo* U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo_var;
extern "C" Object_t * Reporter_readInfo_m1123 (Reporter_t295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(656);
		s_Il2CppMethodIntialized = true;
	}
	U3CreadInfoU3Ec__Iterator0_t296 * V_0 = {0};
	{
		U3CreadInfoU3Ec__Iterator0_t296 * L_0 = (U3CreadInfoU3Ec__Iterator0_t296 *)il2cpp_codegen_object_new (U3CreadInfoU3Ec__Iterator0_t296_il2cpp_TypeInfo_var);
		U3CreadInfoU3Ec__Iterator0__ctor_m1085(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CreadInfoU3Ec__Iterator0_t296 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_6 = __this;
		U3CreadInfoU3Ec__Iterator0_t296 * L_2 = V_0;
		return L_2;
	}
}
#ifndef _MSC_VER
#else
#endif
// ReporterGUI
#include "AssemblyU2DCSharp_ReporterGUIMethodDeclarations.h"

struct GameObject_t78;
struct Reporter_t295;
// Declaration !!0 UnityEngine.GameObject::GetComponent<Reporter>()
// !!0 UnityEngine.GameObject::GetComponent<Reporter>()
#define GameObject_GetComponent_TisReporter_t295_m1703(__this, method) (( Reporter_t295 * (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m771_gshared)(__this, method)


// System.Void ReporterGUI::.ctor()
extern "C" void ReporterGUI__ctor_m1124 (ReporterGUI_t305 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ReporterGUI::Awake()
extern const MethodInfo* GameObject_GetComponent_TisReporter_t295_m1703_MethodInfo_var;
extern "C" void ReporterGUI_Awake_m1125 (ReporterGUI_t305 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisReporter_t295_m1703_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483692);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t78 * L_0 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Reporter_t295 * L_1 = GameObject_GetComponent_TisReporter_t295_m1703(L_0, /*hidden argument*/GameObject_GetComponent_TisReporter_t295_m1703_MethodInfo_var);
		__this->___reporter_2 = L_1;
		return;
	}
}
// System.Void ReporterGUI::OnGUI()
extern "C" void ReporterGUI_OnGUI_m1126 (ReporterGUI_t305 * __this, const MethodInfo* method)
{
	{
		Reporter_t295 * L_0 = (__this->___reporter_2);
		NullCheck(L_0);
		Reporter_OnGUIDraw_m1110(L_0, /*hidden argument*/NULL);
		return;
	}
}
// ReporterMessageReceiver
#include "AssemblyU2DCSharp_ReporterMessageReceiver.h"
#ifndef _MSC_VER
#else
#endif
// ReporterMessageReceiver
#include "AssemblyU2DCSharp_ReporterMessageReceiverMethodDeclarations.h"



// System.Void ReporterMessageReceiver::.ctor()
extern "C" void ReporterMessageReceiver__ctor_m1127 (ReporterMessageReceiver_t306 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ReporterMessageReceiver::Start()
extern const MethodInfo* GameObject_GetComponent_TisReporter_t295_m1703_MethodInfo_var;
extern "C" void ReporterMessageReceiver_Start_m1128 (ReporterMessageReceiver_t306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisReporter_t295_m1703_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483692);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t78 * L_0 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Reporter_t295 * L_1 = GameObject_GetComponent_TisReporter_t295_m1703(L_0, /*hidden argument*/GameObject_GetComponent_TisReporter_t295_m1703_MethodInfo_var);
		__this->___reporter_2 = L_1;
		return;
	}
}
// System.Void ReporterMessageReceiver::OnPreStart()
extern const MethodInfo* GameObject_GetComponent_TisReporter_t295_m1703_MethodInfo_var;
extern "C" void ReporterMessageReceiver_OnPreStart_m1129 (ReporterMessageReceiver_t306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisReporter_t295_m1703_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483692);
		s_Il2CppMethodIntialized = true;
	}
	{
		Reporter_t295 * L_0 = (__this->___reporter_2);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t78 * L_2 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Reporter_t295 * L_3 = GameObject_GetComponent_TisReporter_t295_m1703(L_2, /*hidden argument*/GameObject_GetComponent_TisReporter_t295_m1703_MethodInfo_var);
		__this->___reporter_2 = L_3;
	}

IL_0022:
	{
		int32_t L_4 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) >= ((int32_t)((int32_t)1000))))
		{
			goto IL_0050;
		}
	}
	{
		Reporter_t295 * L_5 = (__this->___reporter_2);
		Vector2_t6  L_6 = {0};
		Vector2__ctor_m630(&L_6, (32.0f), (32.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->___size_82 = L_6;
		goto IL_006a;
	}

IL_0050:
	{
		Reporter_t295 * L_7 = (__this->___reporter_2);
		Vector2_t6  L_8 = {0};
		Vector2__ctor_m630(&L_8, (48.0f), (48.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->___size_82 = L_8;
	}

IL_006a:
	{
		Reporter_t295 * L_9 = (__this->___reporter_2);
		NullCheck(L_9);
		L_9->___UserData_36 = (String_t*) &_stringLiteral267;
		return;
	}
}
// System.Void ReporterMessageReceiver::OnHideReporter()
extern "C" void ReporterMessageReceiver_OnHideReporter_m1130 (ReporterMessageReceiver_t306 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ReporterMessageReceiver::OnShowReporter()
extern "C" void ReporterMessageReceiver_OnShowReporter_m1131 (ReporterMessageReceiver_t306 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ReporterMessageReceiver::OnLog(Reporter/Log)
extern "C" void ReporterMessageReceiver_OnLog_m1132 (ReporterMessageReceiver_t306 * __this, Log_t291 * ___log, const MethodInfo* method)
{
	{
		return;
	}
}
// Rotate
#include "AssemblyU2DCSharp_Rotate.h"
#ifndef _MSC_VER
#else
#endif
// Rotate
#include "AssemblyU2DCSharp_RotateMethodDeclarations.h"

// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"


// System.Void Rotate::.ctor()
extern "C" void Rotate__ctor_m1133 (Rotate_t307 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Rotate::Start()
extern "C" void Rotate_Start_m1134 (Rotate_t307 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_eulerAngles_m1009(L_0, /*hidden argument*/NULL);
		__this->___angle_2 = L_1;
		return;
	}
}
// System.Void Rotate::Update()
extern "C" void Rotate_Update_m1135 (Rotate_t307 * __this, const MethodInfo* method)
{
	{
		Vector3_t4 * L_0 = &(__this->___angle_2);
		Vector3_t4 * L_1 = L_0;
		float L_2 = (L_1->___y_2);
		float L_3 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_1->___y_2 = ((float)((float)L_2+(float)((float)((float)L_3*(float)(100.0f)))));
		Transform_t1 * L_4 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4  L_5 = (__this->___angle_2);
		NullCheck(L_4);
		Transform_set_eulerAngles_m1704(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// TestReporter
#include "AssemblyU2DCSharp_TestReporter.h"
#ifndef _MSC_VER
#else
#endif
// TestReporter
#include "AssemblyU2DCSharp_TestReporterMethodDeclarations.h"

// System.Threading.Thread
#include "mscorlib_System_Threading_Thread.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Threading.ThreadStart
#include "mscorlib_System_Threading_ThreadStart.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Threading.ThreadStart
#include "mscorlib_System_Threading_ThreadStartMethodDeclarations.h"
// System.Threading.Thread
#include "mscorlib_System_Threading_ThreadMethodDeclarations.h"


// System.Void TestReporter::.ctor()
extern "C" void TestReporter__ctor_m1136 (TestReporter_t308 * __this, const MethodInfo* method)
{
	{
		__this->___logTestCount_2 = ((int32_t)100);
		__this->___threadLogTestCount_3 = ((int32_t)100);
		__this->___logEverySecond_4 = 1;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestReporter::Start()
extern const Il2CppType* Reporter_t295_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Reporter_t295_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t302_il2cpp_TypeInfo_var;
extern TypeInfo* ThreadStart_t413_il2cpp_TypeInfo_var;
extern TypeInfo* Thread_t412_il2cpp_TypeInfo_var;
extern const MethodInfo* TestReporter_threadLogTest_m1138_MethodInfo_var;
extern "C" void TestReporter_Start_m1137 (TestReporter_t308 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Reporter_t295_0_0_0_var = il2cpp_codegen_type_from_index(644);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Reporter_t295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		GUIStyle_t302_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(648);
		ThreadStart_t413_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		Thread_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(658);
		TestReporter_threadLogTest_m1138_MethodInfo_var = il2cpp_codegen_method_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Thread_t412 * V_2 = {0};
	{
		Application_set_runInBackground_m1705(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m705(NULL /*static, unused*/, LoadTypeToken(Reporter_t295_0_0_0_var), /*hidden argument*/NULL);
		Object_t164 * L_1 = Object_FindObjectOfType_m1706(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___reporter_6 = ((Reporter_t295 *)IsInst(L_1, Reporter_t295_il2cpp_TypeInfo_var));
		Debug_Log_m857(NULL /*static, unused*/, (String_t*) &_stringLiteral268, /*hidden argument*/NULL);
		GUIStyle_t302 * L_2 = (GUIStyle_t302 *)il2cpp_codegen_object_new (GUIStyle_t302_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m1618(L_2, /*hidden argument*/NULL);
		__this->___style_7 = L_2;
		GUIStyle_t302 * L_3 = (__this->___style_7);
		NullCheck(L_3);
		GUIStyle_set_alignment_m1625(L_3, 4, /*hidden argument*/NULL);
		GUIStyle_t302 * L_4 = (__this->___style_7);
		NullCheck(L_4);
		GUIStyleState_t405 * L_5 = GUIStyle_get_normal_m1622(L_4, /*hidden argument*/NULL);
		Color_t65  L_6 = Color_get_white_m745(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyleState_set_textColor_m1632(L_5, L_6, /*hidden argument*/NULL);
		GUIStyle_t302 * L_7 = (__this->___style_7);
		NullCheck(L_7);
		GUIStyle_set_wordWrap_m1633(L_7, 1, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_008b;
	}

IL_0069:
	{
		Debug_Log_m857(NULL /*static, unused*/, (String_t*) &_stringLiteral269, /*hidden argument*/NULL);
		Debug_LogWarning_m816(NULL /*static, unused*/, (String_t*) &_stringLiteral270, /*hidden argument*/NULL);
		Debug_LogError_m735(NULL /*static, unused*/, (String_t*) &_stringLiteral271, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_008b:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)10))))
		{
			goto IL_0069;
		}
	}
	{
		V_1 = 0;
		goto IL_00bc;
	}

IL_009a:
	{
		Debug_Log_m857(NULL /*static, unused*/, (String_t*) &_stringLiteral269, /*hidden argument*/NULL);
		Debug_LogWarning_m816(NULL /*static, unused*/, (String_t*) &_stringLiteral270, /*hidden argument*/NULL);
		Debug_LogError_m735(NULL /*static, unused*/, (String_t*) &_stringLiteral271, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_00bc:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)((int32_t)10))))
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_12 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_13 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t304  L_14 = {0};
		Rect__ctor_m1707(&L_14, (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_12/(int32_t)2))-(int32_t)((int32_t)120))))), (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_13/(int32_t)2))-(int32_t)((int32_t)225))))), (240.0f), (50.0f), /*hidden argument*/NULL);
		__this->___rect1_8 = L_14;
		int32_t L_15 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t304  L_17 = {0};
		Rect__ctor_m1707(&L_17, (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_15/(int32_t)2))-(int32_t)((int32_t)120))))), (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_16/(int32_t)2))-(int32_t)((int32_t)175))))), (240.0f), (100.0f), /*hidden argument*/NULL);
		__this->___rect2_9 = L_17;
		int32_t L_18 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_19 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t304  L_20 = {0};
		Rect__ctor_m1707(&L_20, (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_18/(int32_t)2))-(int32_t)((int32_t)120))))), (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_19/(int32_t)2))-(int32_t)((int32_t)50))))), (240.0f), (50.0f), /*hidden argument*/NULL);
		__this->___rect3_10 = L_20;
		int32_t L_21 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_22 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t304  L_23 = {0};
		Rect__ctor_m1707(&L_23, (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_21/(int32_t)2))-(int32_t)((int32_t)120))))), (((float)((int32_t)((int32_t)L_22/(int32_t)2)))), (240.0f), (50.0f), /*hidden argument*/NULL);
		__this->___rect4_11 = L_23;
		int32_t L_24 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_25 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t304  L_26 = {0};
		Rect__ctor_m1707(&L_26, (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_24/(int32_t)2))-(int32_t)((int32_t)120))))), (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_25/(int32_t)2))+(int32_t)((int32_t)50))))), (240.0f), (50.0f), /*hidden argument*/NULL);
		__this->___rect5_12 = L_26;
		int32_t L_27 = Screen_get_width_m723(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_28 = Screen_get_height_m859(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t304  L_29 = {0};
		Rect__ctor_m1707(&L_29, (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_27/(int32_t)2))-(int32_t)((int32_t)120))))), (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_28/(int32_t)2))+(int32_t)((int32_t)100))))), (240.0f), (50.0f), /*hidden argument*/NULL);
		__this->___rect6_13 = L_29;
		IntPtr_t L_30 = { (void*)TestReporter_threadLogTest_m1138_MethodInfo_var };
		ThreadStart_t413 * L_31 = (ThreadStart_t413 *)il2cpp_codegen_object_new (ThreadStart_t413_il2cpp_TypeInfo_var);
		ThreadStart__ctor_m1708(L_31, __this, L_30, /*hidden argument*/NULL);
		Thread_t412 * L_32 = (Thread_t412 *)il2cpp_codegen_object_new (Thread_t412_il2cpp_TypeInfo_var);
		Thread__ctor_m1709(L_32, L_31, /*hidden argument*/NULL);
		V_2 = L_32;
		Thread_t412 * L_33 = V_2;
		NullCheck(L_33);
		Thread_Start_m1710(L_33, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestReporter::threadLogTest()
extern "C" void TestReporter_threadLogTest_m1138 (TestReporter_t308 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0029;
	}

IL_0007:
	{
		Debug_Log_m857(NULL /*static, unused*/, (String_t*) &_stringLiteral272, /*hidden argument*/NULL);
		Debug_LogWarning_m816(NULL /*static, unused*/, (String_t*) &_stringLiteral273, /*hidden argument*/NULL);
		Debug_LogError_m735(NULL /*static, unused*/, (String_t*) &_stringLiteral274, /*hidden argument*/NULL);
		int32_t L_0 = V_0;
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_1 = V_0;
		int32_t L_2 = (__this->___threadLogTestCount_3);
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TestReporter::Update()
extern TypeInfo* Int32_t253_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void TestReporter_Update_m1139 (TestReporter_t308 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t253_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0067;
	}

IL_0007:
	{
		int32_t L_0 = (__this->___currentLogTestCount_5);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m954(NULL /*static, unused*/, (String_t*) &_stringLiteral275, L_2, /*hidden argument*/NULL);
		Debug_Log_m857(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___currentLogTestCount_5);
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7 = String_Concat_m954(NULL /*static, unused*/, (String_t*) &_stringLiteral276, L_6, /*hidden argument*/NULL);
		Debug_LogError_m735(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		int32_t L_8 = (__this->___currentLogTestCount_5);
		int32_t L_9 = L_8;
		Object_t * L_10 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_9);
		String_t* L_11 = String_Concat_m954(NULL /*static, unused*/, (String_t*) &_stringLiteral277, L_10, /*hidden argument*/NULL);
		Debug_LogWarning_m816(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
		int32_t L_13 = (__this->___currentLogTestCount_5);
		__this->___currentLogTestCount_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0067:
	{
		int32_t L_14 = (__this->___currentLogTestCount_5);
		int32_t L_15 = (__this->___logTestCount_2);
		if ((((int32_t)L_14) >= ((int32_t)L_15)))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_16 = V_0;
		if ((((int32_t)L_16) < ((int32_t)((int32_t)10))))
		{
			goto IL_0007;
		}
	}

IL_0080:
	{
		float L_17 = (__this->___elapsed_14);
		float L_18 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___elapsed_14 = ((float)((float)L_17+(float)L_18));
		float L_19 = (__this->___elapsed_14);
		if ((!(((float)L_19) >= ((float)(1.0f)))))
		{
			goto IL_00b7;
		}
	}
	{
		__this->___elapsed_14 = (0.0f);
		Debug_Log_m857(NULL /*static, unused*/, (String_t*) &_stringLiteral278, /*hidden argument*/NULL);
	}

IL_00b7:
	{
		return;
	}
}
// System.Void TestReporter::OnGUI()
extern TypeInfo* GUI_t408_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void TestReporter_OnGUI_m1140 (TestReporter_t308 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Reporter_t295 * L_0 = (__this->___reporter_6);
		bool L_1 = Object_op_Implicit_m629(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00d9;
		}
	}
	{
		Reporter_t295 * L_2 = (__this->___reporter_6);
		NullCheck(L_2);
		bool L_3 = (L_2->___show_8);
		if (L_3)
		{
			goto IL_00d9;
		}
	}
	{
		Rect_t304  L_4 = (__this->___rect1_8);
		GUIStyle_t302 * L_5 = (__this->___style_7);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Label_m1666(NULL /*static, unused*/, L_4, (String_t*) &_stringLiteral279, L_5, /*hidden argument*/NULL);
		Rect_t304  L_6 = (__this->___rect2_9);
		GUIStyle_t302 * L_7 = (__this->___style_7);
		GUI_Label_m1666(NULL /*static, unused*/, L_6, (String_t*) &_stringLiteral280, L_7, /*hidden argument*/NULL);
		Rect_t304  L_8 = (__this->___rect3_10);
		bool L_9 = GUI_Button_m1711(NULL /*static, unused*/, L_8, (String_t*) &_stringLiteral281, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006b;
		}
	}
	{
		Application_LoadLevel_m638(NULL /*static, unused*/, (String_t*) &_stringLiteral282, /*hidden argument*/NULL);
	}

IL_006b:
	{
		Rect_t304  L_10 = (__this->___rect4_11);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		bool L_11 = GUI_Button_m1711(NULL /*static, unused*/, L_10, (String_t*) &_stringLiteral283, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_008a;
		}
	}
	{
		Application_LoadLevel_m638(NULL /*static, unused*/, (String_t*) &_stringLiteral284, /*hidden argument*/NULL);
	}

IL_008a:
	{
		Rect_t304  L_12 = (__this->___rect5_12);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		bool L_13 = GUI_Button_m1711(NULL /*static, unused*/, L_12, (String_t*) &_stringLiteral285, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00a9;
		}
	}
	{
		Application_LoadLevel_m638(NULL /*static, unused*/, (String_t*) &_stringLiteral286, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		Rect_t304  L_14 = (__this->___rect6_13);
		Reporter_t295 * L_15 = (__this->___reporter_6);
		NullCheck(L_15);
		float* L_16 = &(L_15->___fps_37);
		String_t* L_17 = Single_ToString_m1658(L_16, (String_t*) &_stringLiteral251, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral287, L_17, /*hidden argument*/NULL);
		GUIStyle_t302 * L_19 = (__this->___style_7);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t408_il2cpp_TypeInfo_var);
		GUI_Label_m1666(NULL /*static, unused*/, L_14, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00d9:
	{
		return;
	}
}
// EventSystemChecker/<Start>c__Iterator1
#include "AssemblyU2DCSharp_EventSystemChecker_U3CStartU3Ec__Iterator1.h"
#ifndef _MSC_VER
#else
#endif
// EventSystemChecker/<Start>c__Iterator1
#include "AssemblyU2DCSharp_EventSystemChecker_U3CStartU3Ec__Iterator1MethodDeclarations.h"

// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"
// EventSystemChecker
#include "AssemblyU2DCSharp_EventSystemChecker.h"
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"
struct Object_t164;
struct EventSystem_t326;
struct Object_t164;
struct Object_t;
// Declaration !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" Object_t * Object_FindObjectOfType_TisObject_t_m1713_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisObject_t_m1713(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m1713_gshared)(__this /* static, unused */, method)
// Declaration !!0 UnityEngine.Object::FindObjectOfType<UnityEngine.EventSystems.EventSystem>()
// !!0 UnityEngine.Object::FindObjectOfType<UnityEngine.EventSystems.EventSystem>()
#define Object_FindObjectOfType_TisEventSystem_t326_m1712(__this /* static, unused */, method) (( EventSystem_t326 * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m1713_gshared)(__this /* static, unused */, method)
struct Object_t164;
struct GameObject_t78;
// Declaration !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t78_m1714(__this /* static, unused */, p0, method) (( GameObject_t78 * (*) (Object_t * /* static, unused */, GameObject_t78 *, const MethodInfo*))Object_Instantiate_TisObject_t_m953_gshared)(__this /* static, unused */, p0, method)


// System.Void EventSystemChecker/<Start>c__Iterator1::.ctor()
extern "C" void U3CStartU3Ec__Iterator1__ctor_m1141 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object EventSystemChecker/<Start>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1142 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object EventSystemChecker/<Start>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1143 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean EventSystemChecker/<Start>c__Iterator1::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisEventSystem_t326_m1712_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t78_m1714_MethodInfo_var;
extern "C" bool U3CStartU3Ec__Iterator1_MoveNext_m1144 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		Object_FindObjectOfType_TisEventSystem_t326_m1712_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483694);
		Object_Instantiate_TisGameObject_t78_m1714_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483695);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0038;
		}
	}
	{
		goto IL_005f;
	}

IL_0021:
	{
		WaitForEndOfFrame_t255 * L_2 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_2, /*hidden argument*/NULL);
		__this->___U24current_1 = L_2;
		__this->___U24PC_0 = 1;
		goto IL_0061;
	}

IL_0038:
	{
		EventSystem_t326 * L_3 = Object_FindObjectOfType_TisEventSystem_t326_m1712(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisEventSystem_t326_m1712_MethodInfo_var);
		bool L_4 = Object_op_Implicit_m629(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0058;
		}
	}
	{
		EventSystemChecker_t309 * L_5 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_5);
		GameObject_t78 * L_6 = (L_5->___eventSystem_2);
		Object_Instantiate_TisGameObject_t78_m1714(NULL /*static, unused*/, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_t78_m1714_MethodInfo_var);
	}

IL_0058:
	{
		__this->___U24PC_0 = (-1);
	}

IL_005f:
	{
		return 0;
	}

IL_0061:
	{
		return 1;
	}
	// Dead block : IL_0063: ldloc.1
}
// System.Void EventSystemChecker/<Start>c__Iterator1::Dispose()
extern "C" void U3CStartU3Ec__Iterator1_Dispose_m1145 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void EventSystemChecker/<Start>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CStartU3Ec__Iterator1_Reset_m1146 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// EventSystemChecker
#include "AssemblyU2DCSharp_EventSystemCheckerMethodDeclarations.h"



// System.Void EventSystemChecker::.ctor()
extern "C" void EventSystemChecker__ctor_m1147 (EventSystemChecker_t309 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator EventSystemChecker::Start()
extern TypeInfo* U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo_var;
extern "C" Object_t * EventSystemChecker_Start_m1148 (EventSystemChecker_t309 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(660);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__Iterator1_t310 * V_0 = {0};
	{
		U3CStartU3Ec__Iterator1_t310 * L_0 = (U3CStartU3Ec__Iterator1_t310 *)il2cpp_codegen_object_new (U3CStartU3Ec__Iterator1_t310_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator1__ctor_m1141(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator1_t310 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CStartU3Ec__Iterator1_t310 * L_2 = V_0;
		return L_2;
	}
}
// MenuSceneLoader
#include "AssemblyU2DCSharp_MenuSceneLoader.h"
#ifndef _MSC_VER
#else
#endif
// MenuSceneLoader
#include "AssemblyU2DCSharp_MenuSceneLoaderMethodDeclarations.h"



// System.Void MenuSceneLoader::.ctor()
extern "C" void MenuSceneLoader__ctor_m1149 (MenuSceneLoader_t311 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuSceneLoader::Awake()
extern const MethodInfo* Object_Instantiate_TisGameObject_t78_m1714_MethodInfo_var;
extern "C" void MenuSceneLoader_Awake_m1150 (MenuSceneLoader_t311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_Instantiate_TisGameObject_t78_m1714_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483695);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t78 * L_0 = (__this->___m_Go_3);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t78 * L_2 = (__this->___menuUI_2);
		GameObject_t78 * L_3 = Object_Instantiate_TisGameObject_t78_m1714(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t78_m1714_MethodInfo_var);
		__this->___m_Go_3 = L_3;
	}

IL_0022:
	{
		return;
	}
}
// PauseMenu
#include "AssemblyU2DCSharp_PauseMenu.h"
#ifndef _MSC_VER
#else
#endif
// PauseMenu
#include "AssemblyU2DCSharp_PauseMenuMethodDeclarations.h"

// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
// UnityEngine.AudioListener
#include "UnityEngine_UnityEngine_AudioListenerMethodDeclarations.h"
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
struct Component_t219;
struct Toggle_t312;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Component_t219;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m614_gshared (Component_t219 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m614(__this, method) (( Object_t * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Toggle>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Toggle>()
#define Component_GetComponent_TisToggle_t312_m1715(__this, method) (( Toggle_t312 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void PauseMenu::.ctor()
extern "C" void PauseMenu__ctor_m1151 (PauseMenu_t313 * __this, const MethodInfo* method)
{
	{
		__this->___m_TimeScaleRef_3 = (1.0f);
		__this->___m_VolumeRef_4 = (1.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseMenu::Awake()
extern const MethodInfo* Component_GetComponent_TisToggle_t312_m1715_MethodInfo_var;
extern "C" void PauseMenu_Awake_m1152 (PauseMenu_t313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisToggle_t312_m1715_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483696);
		s_Il2CppMethodIntialized = true;
	}
	{
		Toggle_t312 * L_0 = Component_GetComponent_TisToggle_t312_m1715(__this, /*hidden argument*/Component_GetComponent_TisToggle_t312_m1715_MethodInfo_var);
		__this->___m_MenuToggle_2 = L_0;
		return;
	}
}
// System.Void PauseMenu::MenuOn()
extern "C" void PauseMenu_MenuOn_m1153 (PauseMenu_t313 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_timeScale_m664(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_TimeScaleRef_3 = L_0;
		Time_set_timeScale_m1716(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		float L_1 = AudioListener_get_volume_m1717(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_VolumeRef_4 = L_1;
		AudioListener_set_volume_m1718(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->___m_Paused_5 = 1;
		return;
	}
}
// System.Void PauseMenu::MenuOff()
extern "C" void PauseMenu_MenuOff_m1154 (PauseMenu_t313 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_TimeScaleRef_3);
		Time_set_timeScale_m1716(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_1 = (__this->___m_VolumeRef_4);
		AudioListener_set_volume_m1718(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->___m_Paused_5 = 0;
		return;
	}
}
// System.Void PauseMenu::OnMenuStatusChange()
extern "C" void PauseMenu_OnMenuStatusChange_m1155 (PauseMenu_t313 * __this, const MethodInfo* method)
{
	{
		Toggle_t312 * L_0 = (__this->___m_MenuToggle_2);
		NullCheck(L_0);
		bool L_1 = Toggle_get_isOn_m1719(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (__this->___m_Paused_5);
		if (L_2)
		{
			goto IL_0026;
		}
	}
	{
		PauseMenu_MenuOn_m1153(__this, /*hidden argument*/NULL);
		goto IL_0047;
	}

IL_0026:
	{
		Toggle_t312 * L_3 = (__this->___m_MenuToggle_2);
		NullCheck(L_3);
		bool L_4 = Toggle_get_isOn_m1719(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0047;
		}
	}
	{
		bool L_5 = (__this->___m_Paused_5);
		if (!L_5)
		{
			goto IL_0047;
		}
	}
	{
		PauseMenu_MenuOff_m1154(__this, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// ReturnToMainMenu
#include "AssemblyU2DCSharp_ReturnToMainMenu.h"
#ifndef _MSC_VER
#else
#endif
// ReturnToMainMenu
#include "AssemblyU2DCSharp_ReturnToMainMenuMethodDeclarations.h"

// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
struct GameObject_t78;
struct Canvas_t414;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Canvas>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Canvas>()
#define GameObject_GetComponent_TisCanvas_t414_m1720(__this, method) (( Canvas_t414 * (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m771_gshared)(__this, method)


// System.Void ReturnToMainMenu::.ctor()
extern "C" void ReturnToMainMenu__ctor_m1156 (ReturnToMainMenu_t314 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ReturnToMainMenu::Start()
extern "C" void ReturnToMainMenu_Start_m1157 (ReturnToMainMenu_t314 * __this, const MethodInfo* method)
{
	{
		Object_DontDestroyOnLoad_m1602(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ReturnToMainMenu::OnLevelWasLoaded(System.Int32)
extern "C" void ReturnToMainMenu_OnLevelWasLoaded_m1158 (ReturnToMainMenu_t314 * __this, int32_t ___level, const MethodInfo* method)
{
	{
		__this->___m_Levelloaded_2 = 1;
		return;
	}
}
// System.Void ReturnToMainMenu::Update()
extern const MethodInfo* GameObject_GetComponent_TisCanvas_t414_m1720_MethodInfo_var;
extern "C" void ReturnToMainMenu_Update_m1159 (ReturnToMainMenu_t314 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisCanvas_t414_m1720_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483697);
		s_Il2CppMethodIntialized = true;
	}
	Canvas_t414 * V_0 = {0};
	{
		bool L_0 = (__this->___m_Levelloaded_2);
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		GameObject_t78 * L_1 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Canvas_t414 * L_2 = GameObject_GetComponent_TisCanvas_t414_m1720(L_1, /*hidden argument*/GameObject_GetComponent_TisCanvas_t414_m1720_MethodInfo_var);
		V_0 = L_2;
		Canvas_t414 * L_3 = V_0;
		NullCheck(L_3);
		Behaviour_set_enabled_m766(L_3, 0, /*hidden argument*/NULL);
		Canvas_t414 * L_4 = V_0;
		NullCheck(L_4);
		Behaviour_set_enabled_m766(L_4, 1, /*hidden argument*/NULL);
		__this->___m_Levelloaded_2 = 0;
	}

IL_002c:
	{
		return;
	}
}
// System.Void ReturnToMainMenu::GoBackToMainMenu()
extern "C" void ReturnToMainMenu_GoBackToMainMenu_m1160 (ReturnToMainMenu_t314 * __this, const MethodInfo* method)
{
	{
		Debug_Log_m857(NULL /*static, unused*/, (String_t*) &_stringLiteral288, /*hidden argument*/NULL);
		Application_LoadLevel_m638(NULL /*static, unused*/, (String_t*) &_stringLiteral289, /*hidden argument*/NULL);
		return;
	}
}
// SceneAndURLLoader
#include "AssemblyU2DCSharp_SceneAndURLLoader.h"
#ifndef _MSC_VER
#else
#endif
// SceneAndURLLoader
#include "AssemblyU2DCSharp_SceneAndURLLoaderMethodDeclarations.h"

struct Component_t219;
struct PauseMenu_t313;
struct Component_t219;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C" Object_t * Component_GetComponentInChildren_TisObject_t_m675_gshared (Component_t219 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisObject_t_m675(__this, method) (( Object_t * (*) (Component_t219 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m675_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<PauseMenu>()
// !!0 UnityEngine.Component::GetComponentInChildren<PauseMenu>()
#define Component_GetComponentInChildren_TisPauseMenu_t313_m1721(__this, method) (( PauseMenu_t313 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m675_gshared)(__this, method)


// System.Void SceneAndURLLoader::.ctor()
extern "C" void SceneAndURLLoader__ctor_m1161 (SceneAndURLLoader_t315 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SceneAndURLLoader::Awake()
extern const MethodInfo* Component_GetComponentInChildren_TisPauseMenu_t313_m1721_MethodInfo_var;
extern "C" void SceneAndURLLoader_Awake_m1162 (SceneAndURLLoader_t315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentInChildren_TisPauseMenu_t313_m1721_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483698);
		s_Il2CppMethodIntialized = true;
	}
	{
		PauseMenu_t313 * L_0 = Component_GetComponentInChildren_TisPauseMenu_t313_m1721(__this, /*hidden argument*/Component_GetComponentInChildren_TisPauseMenu_t313_m1721_MethodInfo_var);
		__this->___m_PauseMenu_2 = L_0;
		return;
	}
}
// System.Void SceneAndURLLoader::SceneLoad(System.String)
extern "C" void SceneAndURLLoader_SceneLoad_m1163 (SceneAndURLLoader_t315 * __this, String_t* ___sceneName, const MethodInfo* method)
{
	{
		PauseMenu_t313 * L_0 = (__this->___m_PauseMenu_2);
		NullCheck(L_0);
		PauseMenu_MenuOff_m1154(L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___sceneName;
		Application_LoadLevel_m638(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SceneAndURLLoader::LoadURL(System.String)
extern "C" void SceneAndURLLoader_LoadURL_m1164 (SceneAndURLLoader_t315 * __this, String_t* ___url, const MethodInfo* method)
{
	{
		String_t* L_0 = ___url;
		Application_OpenURL_m1722(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// CameraSwitch
#include "AssemblyU2DCSharp_CameraSwitch.h"
#ifndef _MSC_VER
#else
#endif
// CameraSwitch
#include "AssemblyU2DCSharp_CameraSwitchMethodDeclarations.h"

// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"


// System.Void CameraSwitch::.ctor()
extern "C" void CameraSwitch__ctor_m1165 (CameraSwitch_t317 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraSwitch::OnEnable()
extern "C" void CameraSwitch_OnEnable_m1166 (CameraSwitch_t317 * __this, const MethodInfo* method)
{
	{
		Text_t316 * L_0 = (__this->___text_3);
		GameObjectU5BU5D_t192* L_1 = (__this->___objects_2);
		int32_t L_2 = (__this->___m_CurrentActiveObject_4);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_1, L_3)));
		String_t* L_4 = Object_get_name_m798((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_1, L_3)), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		return;
	}
}
// System.Void CameraSwitch::NextCamera()
extern "C" void CameraSwitch_NextCamera_m1167 (CameraSwitch_t317 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->___m_CurrentActiveObject_4);
		GameObjectU5BU5D_t192* L_1 = (__this->___objects_2);
		NullCheck(L_1);
		if ((((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))) < ((int32_t)(((int32_t)(((Array_t *)L_1)->max_length))))))
		{
			goto IL_001b;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0023;
	}

IL_001b:
	{
		int32_t L_2 = (__this->___m_CurrentActiveObject_4);
		G_B3_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0023:
	{
		V_0 = G_B3_0;
		V_1 = 0;
		goto IL_0040;
	}

IL_002b:
	{
		GameObjectU5BU5D_t192* L_3 = (__this->___objects_2);
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_3, L_5)));
		GameObject_SetActive_m713((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_3, L_5)), ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_9 = V_1;
		GameObjectU5BU5D_t192* L_10 = (__this->___objects_2);
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_11 = V_0;
		__this->___m_CurrentActiveObject_4 = L_11;
		Text_t316 * L_12 = (__this->___text_3);
		GameObjectU5BU5D_t192* L_13 = (__this->___objects_2);
		int32_t L_14 = (__this->___m_CurrentActiveObject_4);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_13, L_15)));
		String_t* L_16 = Object_get_name_m798((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_13, L_15)), /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_16);
		return;
	}
}
// LevelReset
#include "AssemblyU2DCSharp_LevelReset.h"
#ifndef _MSC_VER
#else
#endif
// LevelReset
#include "AssemblyU2DCSharp_LevelResetMethodDeclarations.h"

// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
// UnityEngine.AsyncOperation
#include "UnityEngine_UnityEngine_AsyncOperation.h"


// System.Void LevelReset::.ctor()
extern "C" void LevelReset__ctor_m1168 (LevelReset_t318 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LevelReset::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C" void LevelReset_OnPointerClick_m1169 (LevelReset_t318 * __this, PointerEventData_t215 * ___data, const MethodInfo* method)
{
	{
		String_t* L_0 = Application_get_loadedLevelName_m637(NULL /*static, unused*/, /*hidden argument*/NULL);
		Application_LoadLevelAsync_m999(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LevelReset::Update()
extern "C" void LevelReset_Update_m1170 (LevelReset_t318 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSceMethodDeclarations.h"



// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_0.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_0MethodDeclarations.h"



// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_1.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_1MethodDeclarations.h"



// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::.ctor()
extern "C" void DemoParticleSystem__ctor_m1171 (DemoParticleSystem_t321 * __this, const MethodInfo* method)
{
	{
		__this->___camOffset_5 = ((int32_t)15);
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_2.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_2MethodDeclarations.h"



// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList::.ctor()
extern "C" void DemoParticleSystemList__ctor_m1172 (DemoParticleSystemList_t323 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.SceneUtils.ParticleSceneControls
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_3.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.SceneUtils.ParticleSceneControls
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_3MethodDeclarations.h"

// System.Collections.Generic.List`1<UnityEngine.Transform>
#include "mscorlib_System_Collections_Generic_List_1_gen_2.h"
// UnityEngine.UI.Button
#include "UnityEngine_UI_UnityEngine_UI_Button.h"
// UnityEngine.UI.Button/ButtonClickedEvent
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClickedEvent.h"
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
#include "Assembly-CSharp_ArrayTypes.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityStandardAssets.Effects.ParticleSystemMultiplier
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Effects_Pa.h"
// UnityEngine.ParticleSystem
#include "UnityEngine_UnityEngine_ParticleSystem.h"
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_gen_6.h"
// UnityEngine.UI.GraphicRaycaster
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster.h"
// System.Collections.Generic.List`1<UnityEngine.Transform>
#include "mscorlib_System_Collections_Generic_List_1_gen_2MethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.UI.Button
#include "UnityEngine_UI_UnityEngine_UI_ButtonMethodDeclarations.h"
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
// UnityEngine.ParticleSystem
#include "UnityEngine_UnityEngine_ParticleSystemMethodDeclarations.h"
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventDataMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_gen_6MethodDeclarations.h"
// UnityEngine.UI.GraphicRaycaster
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycasterMethodDeclarations.h"
struct Component_t219;
struct ParticleSystemMultiplier_t156;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Effects.ParticleSystemMultiplier>()
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Effects.ParticleSystemMultiplier>()
#define Component_GetComponent_TisParticleSystemMultiplier_t156_m895(__this, method) (( ParticleSystemMultiplier_t156 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct ParticleSystem_t161;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.ParticleSystem>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.ParticleSystem>()
#define Component_GetComponent_TisParticleSystem_t161_m939(__this, method) (( ParticleSystem_t161 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.ctor()
extern TypeInfo* List_1_t188_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1723_MethodInfo_var;
extern "C" void ParticleSceneControls__ctor_m1173 (ParticleSceneControls_t327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(95);
		List_1__ctor_m1723_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483699);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___spawnOffset_3 = (0.5f);
		__this->___multiply_4 = (1.0f);
		List_1_t188 * L_0 = (List_1_t188 *)il2cpp_codegen_object_new (List_1_t188_il2cpp_TypeInfo_var);
		List_1__ctor_m1723(L_0, /*hidden argument*/List_1__ctor_m1723_MethodInfo_var);
		__this->___m_CurrentParticleList_14 = L_0;
		Vector3_t4  L_1 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_CamOffsetVelocity_17 = L_1;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.cctor()
extern "C" void ParticleSceneControls__cctor_m1174 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Awake()
extern TypeInfo* ParticleSceneControls_t327_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAction_t416_il2cpp_TypeInfo_var;
extern const MethodInfo* ParticleSceneControls_Previous_m1177_MethodInfo_var;
extern const MethodInfo* ParticleSceneControls_Next_m1178_MethodInfo_var;
extern "C" void ParticleSceneControls_Awake_m1175 (ParticleSceneControls_t327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParticleSceneControls_t327_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(664);
		UnityAction_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(665);
		ParticleSceneControls_Previous_m1177_MethodInfo_var = il2cpp_codegen_method_info_from_index(52);
		ParticleSceneControls_Next_m1178_MethodInfo_var = il2cpp_codegen_method_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_SelectedIndex_16;
		ParticleSceneControls_Select_m1181(__this, L_0, /*hidden argument*/NULL);
		Button_t324 * L_1 = (__this->___previousButton_9);
		NullCheck(L_1);
		ButtonClickedEvent_t415 * L_2 = Button_get_onClick_m1724(L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = { (void*)ParticleSceneControls_Previous_m1177_MethodInfo_var };
		UnityAction_t416 * L_4 = (UnityAction_t416 *)il2cpp_codegen_object_new (UnityAction_t416_il2cpp_TypeInfo_var);
		UnityAction__ctor_m1725(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		UnityEvent_AddListener_m1726(L_2, L_4, /*hidden argument*/NULL);
		Button_t324 * L_5 = (__this->___nextButton_10);
		NullCheck(L_5);
		ButtonClickedEvent_t415 * L_6 = Button_get_onClick_m1724(L_5, /*hidden argument*/NULL);
		IntPtr_t L_7 = { (void*)ParticleSceneControls_Next_m1178_MethodInfo_var };
		UnityAction_t416 * L_8 = (UnityAction_t416 *)il2cpp_codegen_object_new (UnityAction_t416_il2cpp_TypeInfo_var);
		UnityAction__ctor_m1725(L_8, __this, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		UnityEvent_AddListener_m1726(L_6, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::OnDisable()
extern TypeInfo* UnityAction_t416_il2cpp_TypeInfo_var;
extern const MethodInfo* ParticleSceneControls_Previous_m1177_MethodInfo_var;
extern const MethodInfo* ParticleSceneControls_Next_m1178_MethodInfo_var;
extern "C" void ParticleSceneControls_OnDisable_m1176 (ParticleSceneControls_t327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAction_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(665);
		ParticleSceneControls_Previous_m1177_MethodInfo_var = il2cpp_codegen_method_info_from_index(52);
		ParticleSceneControls_Next_m1178_MethodInfo_var = il2cpp_codegen_method_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		Button_t324 * L_0 = (__this->___previousButton_9);
		NullCheck(L_0);
		ButtonClickedEvent_t415 * L_1 = Button_get_onClick_m1724(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = { (void*)ParticleSceneControls_Previous_m1177_MethodInfo_var };
		UnityAction_t416 * L_3 = (UnityAction_t416 *)il2cpp_codegen_object_new (UnityAction_t416_il2cpp_TypeInfo_var);
		UnityAction__ctor_m1725(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		UnityEvent_RemoveListener_m1727(L_1, L_3, /*hidden argument*/NULL);
		Button_t324 * L_4 = (__this->___nextButton_10);
		NullCheck(L_4);
		ButtonClickedEvent_t415 * L_5 = Button_get_onClick_m1724(L_4, /*hidden argument*/NULL);
		IntPtr_t L_6 = { (void*)ParticleSceneControls_Next_m1178_MethodInfo_var };
		UnityAction_t416 * L_7 = (UnityAction_t416 *)il2cpp_codegen_object_new (UnityAction_t416_il2cpp_TypeInfo_var);
		UnityAction__ctor_m1725(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		UnityEvent_RemoveListener_m1727(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Previous()
extern TypeInfo* ParticleSceneControls_t327_il2cpp_TypeInfo_var;
extern "C" void ParticleSceneControls_Previous_m1177 (ParticleSceneControls_t327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParticleSceneControls_t327_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(664);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_SelectedIndex_16;
		((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_SelectedIndex_16 = ((int32_t)((int32_t)L_0-(int32_t)1));
		int32_t L_1 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_SelectedIndex_16;
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_002b;
		}
	}
	{
		DemoParticleSystemList_t323 * L_2 = (__this->___demoParticles_2);
		NullCheck(L_2);
		DemoParticleSystemU5BU5D_t322* L_3 = (L_2->___items_0);
		NullCheck(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_SelectedIndex_16 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_3)->max_length)))-(int32_t)1));
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		int32_t L_4 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_SelectedIndex_16;
		ParticleSceneControls_Select_m1181(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Next()
extern TypeInfo* ParticleSceneControls_t327_il2cpp_TypeInfo_var;
extern "C" void ParticleSceneControls_Next_m1178 (ParticleSceneControls_t327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParticleSceneControls_t327_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(664);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_SelectedIndex_16;
		((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_SelectedIndex_16 = ((int32_t)((int32_t)L_0+(int32_t)1));
		int32_t L_1 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_SelectedIndex_16;
		DemoParticleSystemList_t323 * L_2 = (__this->___demoParticles_2);
		NullCheck(L_2);
		DemoParticleSystemU5BU5D_t322* L_3 = (L_2->___items_0);
		NullCheck(L_3);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)(((Array_t *)L_3)->max_length)))))))
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_SelectedIndex_16 = 0;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		int32_t L_4 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_SelectedIndex_16;
		ParticleSceneControls_Select_m1181(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Update()
extern TypeInfo* ParticleSceneControls_t327_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t1_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisParticleSystem_t161_m939_MethodInfo_var;
extern "C" void ParticleSceneControls_Update_m1179 (ParticleSceneControls_t327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParticleSceneControls_t327_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(664);
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Transform_t1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		Component_GetComponent_TisParticleSystem_t161_m939_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483671);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Ray_t26  V_2 = {0};
	RaycastHit_t24  V_3 = {0};
	Quaternion_t19  V_4 = {0};
	Vector3_t4  V_5 = {0};
	Vector3_t4  V_6 = {0};
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Transform_t1 * L_0 = (__this->___sceneCamera_7);
		Transform_t1 * L_1 = (__this->___sceneCamera_7);
		NullCheck(L_1);
		Vector3_t4  L_2 = Transform_get_localPosition_m680(L_1, /*hidden argument*/NULL);
		Vector3_t4  L_3 = Vector3_get_forward_m605(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_4 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_4);
		int32_t L_5 = (L_4->___camOffset_5);
		Vector3_t4  L_6 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_3, (((float)((-L_5)))), /*hidden argument*/NULL);
		Vector3_t4 * L_7 = &(__this->___m_CamOffsetVelocity_17);
		Vector3_t4  L_8 = Vector3_SmoothDamp_m606(NULL /*static, unused*/, L_2, L_6, L_7, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localPosition_m696(L_0, L_8, /*hidden argument*/NULL);
		DemoParticleSystem_t321 * L_9 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_9);
		int32_t L_10 = (L_9->___mode_1);
		if (L_10)
		{
			goto IL_004c;
		}
	}
	{
		return;
	}

IL_004c:
	{
		bool L_11 = ParticleSceneControls_CheckForGuiCollision_m1180(__this, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0058;
		}
	}
	{
		return;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_12 = Input_GetMouseButtonDown_m977(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_13 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_13);
		int32_t L_14 = (L_13->___mode_1);
		G_B7_0 = ((((int32_t)L_14) == ((int32_t)1))? 1 : 0);
		goto IL_0073;
	}

IL_0072:
	{
		G_B7_0 = 0;
	}

IL_0073:
	{
		V_0 = G_B7_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_15 = Input_GetMouseButton_m924(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_16 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_16);
		int32_t L_17 = (L_16->___mode_1);
		G_B10_0 = ((((int32_t)L_17) == ((int32_t)2))? 1 : 0);
		goto IL_008f;
	}

IL_008e:
	{
		G_B10_0 = 0;
	}

IL_008f:
	{
		V_1 = G_B10_0;
		bool L_18 = V_0;
		if (L_18)
		{
			goto IL_009c;
		}
	}
	{
		bool L_19 = V_1;
		if (!L_19)
		{
			goto IL_0272;
		}
	}

IL_009c:
	{
		Camera_t27 * L_20 = Camera_get_main_m989(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_21 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		Ray_t26  L_22 = Camera_ScreenPointToRay_m971(L_20, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		Ray_t26  L_23 = V_2;
		bool L_24 = Physics_Raycast_m991(NULL /*static, unused*/, L_23, (&V_3), /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0272;
		}
	}
	{
		Vector3_t4  L_25 = RaycastHit_get_normal_m902((&V_3), /*hidden argument*/NULL);
		Quaternion_t19  L_26 = Quaternion_LookRotation_m917(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		V_4 = L_26;
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_27 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_27);
		int32_t L_28 = (L_27->___align_2);
		if ((!(((uint32_t)L_28) == ((uint32_t)1))))
		{
			goto IL_00de;
		}
	}
	{
		Quaternion_t19  L_29 = Quaternion_get_identity_m788(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_29;
	}

IL_00de:
	{
		Vector3_t4  L_30 = RaycastHit_get_point_m693((&V_3), /*hidden argument*/NULL);
		Vector3_t4  L_31 = RaycastHit_get_normal_m902((&V_3), /*hidden argument*/NULL);
		float L_32 = (__this->___spawnOffset_3);
		Vector3_t4  L_33 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		Vector3_t4  L_34 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_30, L_33, /*hidden argument*/NULL);
		V_5 = L_34;
		Vector3_t4  L_35 = V_5;
		Vector3_t4  L_36 = (__this->___m_LastPos_18);
		Vector3_t4  L_37 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_6 = L_37;
		float L_38 = Vector3_get_magnitude_m647((&V_6), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_39 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_39);
		float L_40 = (L_39->___minDist_4);
		if ((!(((float)L_38) > ((float)L_40))))
		{
			goto IL_0272;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_41 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_41);
		int32_t L_42 = (L_41->___mode_1);
		if ((!(((uint32_t)L_42) == ((uint32_t)2))))
		{
			goto IL_0144;
		}
	}
	{
		Transform_t1 * L_43 = (__this->___m_Instance_15);
		bool L_44 = Object_op_Equality_m640(NULL /*static, unused*/, L_43, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0202;
		}
	}

IL_0144:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_45 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_45);
		Transform_t1 * L_46 = (L_45->___transform_0);
		Vector3_t4  L_47 = V_5;
		Quaternion_t19  L_48 = V_4;
		Object_t164 * L_49 = Object_Instantiate_m899(NULL /*static, unused*/, L_46, L_47, L_48, /*hidden argument*/NULL);
		__this->___m_Instance_15 = ((Transform_t1 *)Castclass(L_49, Transform_t1_il2cpp_TypeInfo_var));
		ParticleSystemMultiplier_t156 * L_50 = (__this->___m_ParticleMultiplier_13);
		bool L_51 = Object_op_Inequality_m623(NULL /*static, unused*/, L_50, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_0189;
		}
	}
	{
		Transform_t1 * L_52 = (__this->___m_Instance_15);
		NullCheck(L_52);
		ParticleSystemMultiplier_t156 * L_53 = Component_GetComponent_TisParticleSystemMultiplier_t156_m895(L_52, /*hidden argument*/Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var);
		float L_54 = (__this->___multiply_4);
		NullCheck(L_53);
		L_53->___multiplier_2 = L_54;
	}

IL_0189:
	{
		List_1_t188 * L_55 = (__this->___m_CurrentParticleList_14);
		Transform_t1 * L_56 = (__this->___m_Instance_15);
		NullCheck(L_55);
		VirtActionInvoker1< Transform_t1 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(!0) */, L_55, L_56);
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_57 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_57);
		int32_t L_58 = (L_57->___maxCount_3);
		if ((((int32_t)L_58) <= ((int32_t)0)))
		{
			goto IL_01fd;
		}
	}
	{
		List_1_t188 * L_59 = (__this->___m_CurrentParticleList_14);
		NullCheck(L_59);
		int32_t L_60 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, L_59);
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_61 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_61);
		int32_t L_62 = (L_61->___maxCount_3);
		if ((((int32_t)L_60) <= ((int32_t)L_62)))
		{
			goto IL_01fd;
		}
	}
	{
		List_1_t188 * L_63 = (__this->___m_CurrentParticleList_14);
		NullCheck(L_63);
		Transform_t1 * L_64 = (Transform_t1 *)VirtFuncInvoker1< Transform_t1 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32) */, L_63, 0);
		bool L_65 = Object_op_Inequality_m623(NULL /*static, unused*/, L_64, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_01f1;
		}
	}
	{
		List_1_t188 * L_66 = (__this->___m_CurrentParticleList_14);
		NullCheck(L_66);
		Transform_t1 * L_67 = (Transform_t1 *)VirtFuncInvoker1< Transform_t1 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32) */, L_66, 0);
		NullCheck(L_67);
		GameObject_t78 * L_68 = Component_get_gameObject_m622(L_67, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_68, /*hidden argument*/NULL);
	}

IL_01f1:
	{
		List_1_t188 * L_69 = (__this->___m_CurrentParticleList_14);
		NullCheck(L_69);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::RemoveAt(System.Int32) */, L_69, 0);
	}

IL_01fd:
	{
		goto IL_021c;
	}

IL_0202:
	{
		Transform_t1 * L_70 = (__this->___m_Instance_15);
		Vector3_t4  L_71 = V_5;
		NullCheck(L_70);
		Transform_set_position_m607(L_70, L_71, /*hidden argument*/NULL);
		Transform_t1 * L_72 = (__this->___m_Instance_15);
		Quaternion_t19  L_73 = V_4;
		NullCheck(L_72);
		Transform_set_rotation_m658(L_72, L_73, /*hidden argument*/NULL);
	}

IL_021c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_74 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_74);
		int32_t L_75 = (L_74->___mode_1);
		if ((!(((uint32_t)L_75) == ((uint32_t)2))))
		{
			goto IL_0258;
		}
	}
	{
		Transform_t1 * L_76 = (__this->___m_Instance_15);
		NullCheck(L_76);
		Transform_t1 * L_77 = Component_get_transform_m594(L_76, /*hidden argument*/NULL);
		NullCheck(L_77);
		ParticleSystem_t161 * L_78 = Component_GetComponent_TisParticleSystem_t161_m939(L_77, /*hidden argument*/Component_GetComponent_TisParticleSystem_t161_m939_MethodInfo_var);
		NullCheck(L_78);
		ParticleSystem_set_enableEmission_m921(L_78, 0, /*hidden argument*/NULL);
		Transform_t1 * L_79 = (__this->___m_Instance_15);
		NullCheck(L_79);
		Transform_t1 * L_80 = Component_get_transform_m594(L_79, /*hidden argument*/NULL);
		NullCheck(L_80);
		ParticleSystem_t161 * L_81 = Component_GetComponent_TisParticleSystem_t161_m939(L_80, /*hidden argument*/Component_GetComponent_TisParticleSystem_t161_m939_MethodInfo_var);
		NullCheck(L_81);
		ParticleSystem_Emit_m1728(L_81, 1, /*hidden argument*/NULL);
	}

IL_0258:
	{
		Transform_t1 * L_82 = (__this->___m_Instance_15);
		Transform_t1 * L_83 = RaycastHit_get_transform_m1729((&V_3), /*hidden argument*/NULL);
		NullCheck(L_82);
		Transform_set_parent_m596(L_82, L_83, /*hidden argument*/NULL);
		Vector3_t4  L_84 = V_5;
		__this->___m_LastPos_18 = L_84;
	}

IL_0272:
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.SceneUtils.ParticleSceneControls::CheckForGuiCollision()
extern TypeInfo* PointerEventData_t215_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t417_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1733_MethodInfo_var;
extern "C" bool ParticleSceneControls_CheckForGuiCollision_m1180 (ParticleSceneControls_t327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PointerEventData_t215_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		List_1_t417_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		List_1__ctor_m1733_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483702);
		s_Il2CppMethodIntialized = true;
	}
	PointerEventData_t215 * V_0 = {0};
	List_1_t417 * V_1 = {0};
	{
		EventSystem_t326 * L_0 = (__this->___eventSystem_12);
		PointerEventData_t215 * L_1 = (PointerEventData_t215 *)il2cpp_codegen_object_new (PointerEventData_t215_il2cpp_TypeInfo_var);
		PointerEventData__ctor_m1730(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		PointerEventData_t215 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_3 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6  L_4 = Vector2_op_Implicit_m619(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PointerEventData_set_pressPosition_m1731(L_2, L_4, /*hidden argument*/NULL);
		PointerEventData_t215 * L_5 = V_0;
		Vector3_t4  L_6 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6  L_7 = Vector2_op_Implicit_m619(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		PointerEventData_set_position_m1732(L_5, L_7, /*hidden argument*/NULL);
		List_1_t417 * L_8 = (List_1_t417 *)il2cpp_codegen_object_new (List_1_t417_il2cpp_TypeInfo_var);
		List_1__ctor_m1733(L_8, /*hidden argument*/List_1__ctor_m1733_MethodInfo_var);
		V_1 = L_8;
		GraphicRaycaster_t325 * L_9 = (__this->___graphicRaycaster_11);
		PointerEventData_t215 * L_10 = V_0;
		List_1_t417 * L_11 = V_1;
		NullCheck(L_9);
		VirtActionInvoker2< PointerEventData_t215 *, List_1_t417 * >::Invoke(16 /* System.Void UnityEngine.UI.GraphicRaycaster::Raycast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>) */, L_9, L_10, L_11);
		List_1_t417 * L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count() */, L_12);
		return ((((int32_t)L_13) > ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Select(System.Int32)
extern TypeInfo* ParticleSceneControls_t327_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var;
extern "C" void ParticleSceneControls_Select_m1181 (ParticleSceneControls_t327 * __this, int32_t ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParticleSceneControls_t327_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(664);
		Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		s_Il2CppMethodIntialized = true;
	}
	DemoParticleSystem_t321 * V_0 = {0};
	DemoParticleSystemU5BU5D_t322* V_1 = {0};
	int32_t V_2 = 0;
	{
		DemoParticleSystemList_t323 * L_0 = (__this->___demoParticles_2);
		NullCheck(L_0);
		DemoParticleSystemU5BU5D_t322* L_1 = (L_0->___items_0);
		int32_t L_2 = ___i;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19 = (*(DemoParticleSystem_t321 **)(DemoParticleSystem_t321 **)SZArrayLdElema(L_1, L_3));
		__this->___m_Instance_15 = (Transform_t1 *)NULL;
		DemoParticleSystemList_t323 * L_4 = (__this->___demoParticles_2);
		NullCheck(L_4);
		DemoParticleSystemU5BU5D_t322* L_5 = (L_4->___items_0);
		V_1 = L_5;
		V_2 = 0;
		goto IL_005b;
	}

IL_002c:
	{
		DemoParticleSystemU5BU5D_t322* L_6 = V_1;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_0 = (*(DemoParticleSystem_t321 **)(DemoParticleSystem_t321 **)SZArrayLdElema(L_6, L_8));
		DemoParticleSystem_t321 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_10 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		if ((((Object_t*)(DemoParticleSystem_t321 *)L_9) == ((Object_t*)(DemoParticleSystem_t321 *)L_10)))
		{
			goto IL_0057;
		}
	}
	{
		DemoParticleSystem_t321 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = (L_11->___mode_1);
		if (L_12)
		{
			goto IL_0057;
		}
	}
	{
		DemoParticleSystem_t321 * L_13 = V_0;
		NullCheck(L_13);
		Transform_t1 * L_14 = (L_13->___transform_0);
		NullCheck(L_14);
		GameObject_t78 * L_15 = Component_get_gameObject_m622(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		GameObject_SetActive_m713(L_15, 0, /*hidden argument*/NULL);
	}

IL_0057:
	{
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_2;
		DemoParticleSystemU5BU5D_t322* L_18 = V_1;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)(((Array_t *)L_18)->max_length))))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_19 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___mode_1);
		if (L_20)
		{
			goto IL_0088;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_21 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_21);
		Transform_t1 * L_22 = (L_21->___transform_0);
		NullCheck(L_22);
		GameObject_t78 * L_23 = Component_get_gameObject_m622(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		GameObject_SetActive_m713(L_23, 1, /*hidden argument*/NULL);
	}

IL_0088:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_24 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_24);
		Transform_t1 * L_25 = (L_24->___transform_0);
		NullCheck(L_25);
		ParticleSystemMultiplier_t156 * L_26 = Component_GetComponent_TisParticleSystemMultiplier_t156_m895(L_25, /*hidden argument*/Component_GetComponent_TisParticleSystemMultiplier_t156_m895_MethodInfo_var);
		__this->___m_ParticleMultiplier_13 = L_26;
		__this->___multiply_4 = (1.0f);
		bool L_27 = (__this->___clearOnChange_5);
		if (!L_27)
		{
			goto IL_00eb;
		}
	}
	{
		goto IL_00da;
	}

IL_00b8:
	{
		List_1_t188 * L_28 = (__this->___m_CurrentParticleList_14);
		NullCheck(L_28);
		Transform_t1 * L_29 = (Transform_t1 *)VirtFuncInvoker1< Transform_t1 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32) */, L_28, 0);
		NullCheck(L_29);
		GameObject_t78 * L_30 = Component_get_gameObject_m622(L_29, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		List_1_t188 * L_31 = (__this->___m_CurrentParticleList_14);
		NullCheck(L_31);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::RemoveAt(System.Int32) */, L_31, 0);
	}

IL_00da:
	{
		List_1_t188 * L_32 = (__this->___m_CurrentParticleList_14);
		NullCheck(L_32);
		int32_t L_33 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, L_32);
		if ((((int32_t)L_33) > ((int32_t)0)))
		{
			goto IL_00b8;
		}
	}

IL_00eb:
	{
		Text_t316 * L_34 = (__this->___instructionText_8);
		IL2CPP_RUNTIME_CLASS_INIT(ParticleSceneControls_t327_il2cpp_TypeInfo_var);
		DemoParticleSystem_t321 * L_35 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_35);
		String_t* L_36 = (L_35->___instructionText_6);
		NullCheck(L_34);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_34, L_36);
		Text_t316 * L_37 = (__this->___titleText_6);
		DemoParticleSystem_t321 * L_38 = ((ParticleSceneControls_t327_StaticFields*)ParticleSceneControls_t327_il2cpp_TypeInfo_var->static_fields)->___s_Selected_19;
		NullCheck(L_38);
		Transform_t1 * L_39 = (L_38->___transform_0);
		NullCheck(L_39);
		String_t* L_40 = Object_get_name_m798(L_39, /*hidden argument*/NULL);
		NullCheck(L_37);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_37, L_40);
		return;
	}
}
// UnityStandardAssets.SceneUtils.PlaceTargetWithMouse
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_PlaceTarget.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.SceneUtils.PlaceTargetWithMouse
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_PlaceTargetMethodDeclarations.h"



// System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::.ctor()
extern "C" void PlaceTargetWithMouse__ctor_m1182 (PlaceTargetWithMouse_t328 * __this, const MethodInfo* method)
{
	{
		__this->___surfaceOffset_2 = (1.5f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::Update()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" void PlaceTargetWithMouse_Update_m1183 (PlaceTargetWithMouse_t328 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t26  V_0 = {0};
	RaycastHit_t24  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m977(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Camera_t27 * L_1 = Camera_get_main_m989(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_2 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Ray_t26  L_3 = Camera_ScreenPointToRay_m971(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Ray_t26  L_4 = V_0;
		bool L_5 = Physics_Raycast_m991(NULL /*static, unused*/, L_4, (&V_1), /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002a;
		}
	}
	{
		return;
	}

IL_002a:
	{
		Transform_t1 * L_6 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4  L_7 = RaycastHit_get_point_m693((&V_1), /*hidden argument*/NULL);
		Vector3_t4  L_8 = RaycastHit_get_normal_m902((&V_1), /*hidden argument*/NULL);
		float L_9 = (__this->___surfaceOffset_2);
		Vector3_t4  L_10 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector3_t4  L_11 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_position_m607(L_6, L_11, /*hidden argument*/NULL);
		GameObject_t78 * L_12 = (__this->___setTargetOn_3);
		bool L_13 = Object_op_Inequality_m623(NULL /*static, unused*/, L_12, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_007a;
		}
	}
	{
		GameObject_t78 * L_14 = (__this->___setTargetOn_3);
		Transform_t1 * L_15 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_SendMessage_m1698(L_14, (String_t*) &_stringLiteral290, L_15, /*hidden argument*/NULL);
	}

IL_007a:
	{
		return;
	}
}
// UnityStandardAssets.SceneUtils.SlowMoButton
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_SlowMoButto.h"
#ifndef _MSC_VER
#else
#endif
// UnityStandardAssets.SceneUtils.SlowMoButton
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_SlowMoButtoMethodDeclarations.h"

// UnityEngine.UI.Image
#include "UnityEngine_UI_UnityEngine_UI_Image.h"
// UnityEngine.UI.Graphic
#include "UnityEngine_UI_UnityEngine_UI_Graphic.h"
// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_Sprite.h"
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_SelectableMethodDeclarations.h"
// UnityEngine.UI.Image
#include "UnityEngine_UI_UnityEngine_UI_ImageMethodDeclarations.h"


// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::.ctor()
extern "C" void SlowMoButton__ctor_m1184 (SlowMoButton_t330 * __this, const MethodInfo* method)
{
	{
		__this->___fullSpeed_4 = (1.0f);
		__this->___slowSpeed_5 = (0.3f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::Start()
extern "C" void SlowMoButton_Start_m1185 (SlowMoButton_t330 * __this, const MethodInfo* method)
{
	{
		__this->___m_SlowMo_7 = 0;
		return;
	}
}
// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::OnDestroy()
extern "C" void SlowMoButton_OnDestroy_m1186 (SlowMoButton_t330 * __this, const MethodInfo* method)
{
	{
		Time_set_timeScale_m1716(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::ChangeSpeed()
extern TypeInfo* Image_t48_il2cpp_TypeInfo_var;
extern "C" void SlowMoButton_ChangeSpeed_m1187 (SlowMoButton_t330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Image_t48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(32);
		s_Il2CppMethodIntialized = true;
	}
	Image_t48 * V_0 = {0};
	Image_t48 * G_B3_0 = {0};
	Image_t48 * G_B2_0 = {0};
	Sprite_t329 * G_B4_0 = {0};
	Image_t48 * G_B4_1 = {0};
	float G_B8_0 = 0.0f;
	{
		bool L_0 = (__this->___m_SlowMo_7);
		__this->___m_SlowMo_7 = ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		Button_t324 * L_1 = (__this->___button_6);
		NullCheck(L_1);
		Graphic_t418 * L_2 = Selectable_get_targetGraphic_m1734(L_1, /*hidden argument*/NULL);
		V_0 = ((Image_t48 *)IsInst(L_2, Image_t48_il2cpp_TypeInfo_var));
		Image_t48 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m623(NULL /*static, unused*/, L_3, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004e;
		}
	}
	{
		Image_t48 * L_5 = V_0;
		bool L_6 = (__this->___m_SlowMo_7);
		G_B2_0 = L_5;
		if (!L_6)
		{
			G_B3_0 = L_5;
			goto IL_0043;
		}
	}
	{
		Sprite_t329 * L_7 = (__this->___SlowSpeedTex_3);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		goto IL_0049;
	}

IL_0043:
	{
		Sprite_t329 * L_8 = (__this->___FullSpeedTex_2);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_0049:
	{
		NullCheck(G_B4_1);
		Image_set_sprite_m1735(G_B4_1, G_B4_0, /*hidden argument*/NULL);
	}

IL_004e:
	{
		Button_t324 * L_9 = (__this->___button_6);
		Image_t48 * L_10 = V_0;
		NullCheck(L_9);
		Selectable_set_targetGraphic_m1736(L_9, L_10, /*hidden argument*/NULL);
		bool L_11 = (__this->___m_SlowMo_7);
		if (!L_11)
		{
			goto IL_0070;
		}
	}
	{
		float L_12 = (__this->___slowSpeed_5);
		G_B8_0 = L_12;
		goto IL_0076;
	}

IL_0070:
	{
		float L_13 = (__this->___fullSpeed_4);
		G_B8_0 = L_13;
	}

IL_0076:
	{
		Time_set_timeScale_m1716(NULL /*static, unused*/, G_B8_0, /*hidden argument*/NULL);
		return;
	}
}
// SCR_Collectible
#include "AssemblyU2DCSharp_SCR_Collectible.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Collectible
#include "AssemblyU2DCSharp_SCR_CollectibleMethodDeclarations.h"



// System.Void SCR_Collectible::.ctor()
extern "C" void SCR_Collectible__ctor_m1188 (SCR_Collectible_t331 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Collectible::Collect()
// System.Int32 SCR_Collectible::get_PointValue()
extern "C" int32_t SCR_Collectible_get_PointValue_m1189 (SCR_Collectible_t331 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___pointValue_2);
		return L_0;
	}
}
// System.Boolean SCR_Collectible::get_IsTaken()
extern "C" bool SCR_Collectible_get_IsTaken_m1190 (SCR_Collectible_t331 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isTaken_3);
		return L_0;
	}
}
// System.Void SCR_Collectible::set_IsTaken(System.Boolean)
extern "C" void SCR_Collectible_set_IsTaken_m1191 (SCR_Collectible_t331 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___isTaken_3 = L_0;
		return;
	}
}
// SCR_Menu/<AlphaLerp>c__Iterator2
#include "AssemblyU2DCSharp_SCR_Menu_U3CAlphaLerpU3Ec__Iterator2.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Menu/<AlphaLerp>c__Iterator2
#include "AssemblyU2DCSharp_SCR_Menu_U3CAlphaLerpU3Ec__Iterator2MethodDeclarations.h"

// SCR_TimeManager
#include "AssemblyU2DCSharp_SCR_TimeManager.h"
// SCR_ManagerComponents
#include "AssemblyU2DCSharp_SCR_ManagerComponentsMethodDeclarations.h"
// SCR_TimeManager
#include "AssemblyU2DCSharp_SCR_TimeManagerMethodDeclarations.h"
// UnityEngine.UI.Graphic
#include "UnityEngine_UI_UnityEngine_UI_GraphicMethodDeclarations.h"


// System.Void SCR_Menu/<AlphaLerp>c__Iterator2::.ctor()
extern "C" void U3CAlphaLerpU3Ec__Iterator2__ctor_m1192 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_Menu/<AlphaLerp>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAlphaLerpU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1193 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_8);
		return L_0;
	}
}
// System.Object SCR_Menu/<AlphaLerp>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAlphaLerpU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1194 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_8);
		return L_0;
	}
}
// System.Boolean SCR_Menu/<AlphaLerp>c__Iterator2::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CAlphaLerpU3Ec__Iterator2_MoveNext_m1195 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	U3CAlphaLerpU3Ec__Iterator2_t332 * G_B5_0 = {0};
	U3CAlphaLerpU3Ec__Iterator2_t332 * G_B4_0 = {0};
	float G_B6_0 = 0.0f;
	U3CAlphaLerpU3Ec__Iterator2_t332 * G_B6_1 = {0};
	{
		int32_t L_0 = (__this->___U24PC_7);
		V_0 = L_0;
		__this->___U24PC_7 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0111;
		}
	}
	{
		goto IL_0128;
	}

IL_0021:
	{
		__this->___U3CalphaValueU3E__0_0 = (0.0f);
		__this->___U3CmodifierU3E__1_1 = (1.0f);
		goto IL_0111;
	}

IL_003c:
	{
		SCR_TimeManager_t383 * L_2 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = SCR_TimeManager_get_IsPaused_m1507(L_2, /*hidden argument*/NULL);
		G_B4_0 = __this;
		if (!L_3)
		{
			G_B5_0 = __this;
			goto IL_0056;
		}
	}
	{
		float L_4 = Time_get_fixedDeltaTime_m1737(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		G_B6_1 = G_B4_0;
		goto IL_005b;
	}

IL_0056:
	{
		float L_5 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_5;
		G_B6_1 = G_B5_0;
	}

IL_005b:
	{
		NullCheck(G_B6_1);
		G_B6_1->___U3CdTimeU3E__2_3 = G_B6_0;
		float L_6 = (__this->___U3CalphaValueU3E__0_0);
		float L_7 = (__this->___U3CdTimeU3E__2_3);
		float L_8 = (__this->___speed_4);
		float L_9 = (__this->___U3CmodifierU3E__1_1);
		__this->___U3CalphaValueU3E__0_0 = ((float)((float)L_6+(float)((float)((float)((float)((float)L_7*(float)L_8))*(float)L_9))));
		bool L_10 = (__this->___isRepeating_5);
		if (!L_10)
		{
			goto IL_00ac;
		}
	}
	{
		float L_11 = (__this->___U3CalphaValueU3E__0_0);
		if ((!(((float)L_11) >= ((float)(1.0f)))))
		{
			goto IL_00ac;
		}
	}
	{
		__this->___U3CmodifierU3E__1_1 = (-1.0f);
		goto IL_00c7;
	}

IL_00ac:
	{
		float L_12 = (__this->___U3CalphaValueU3E__0_0);
		if ((!(((float)L_12) <= ((float)(0.0f)))))
		{
			goto IL_00c7;
		}
	}
	{
		__this->___U3CmodifierU3E__1_1 = (1.0f);
	}

IL_00c7:
	{
		Text_t316 * L_13 = (__this->___text_2);
		NullCheck(L_13);
		Color_t65  L_14 = Graphic_get_color_m1738(L_13, /*hidden argument*/NULL);
		__this->___U3CnewColorU3E__3_6 = L_14;
		Color_t65 * L_15 = &(__this->___U3CnewColorU3E__3_6);
		float L_16 = (__this->___U3CalphaValueU3E__0_0);
		L_15->___a_3 = L_16;
		Text_t316 * L_17 = (__this->___text_2);
		Color_t65  L_18 = (__this->___U3CnewColorU3E__3_6);
		NullCheck(L_17);
		Graphic_set_color_m1739(L_17, L_18, /*hidden argument*/NULL);
		WaitForEndOfFrame_t255 * L_19 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_19, /*hidden argument*/NULL);
		__this->___U24current_8 = L_19;
		__this->___U24PC_7 = 1;
		goto IL_012a;
	}

IL_0111:
	{
		Text_t316 * L_20 = (__this->___text_2);
		NullCheck(L_20);
		bool L_21 = Behaviour_get_isActiveAndEnabled_m1740(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_003c;
		}
	}
	{
		__this->___U24PC_7 = (-1);
	}

IL_0128:
	{
		return 0;
	}

IL_012a:
	{
		return 1;
	}
	// Dead block : IL_012c: ldloc.1
}
// System.Void SCR_Menu/<AlphaLerp>c__Iterator2::Dispose()
extern "C" void U3CAlphaLerpU3Ec__Iterator2_Dispose_m1196 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_7 = (-1);
		return;
	}
}
// System.Void SCR_Menu/<AlphaLerp>c__Iterator2::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CAlphaLerpU3Ec__Iterator2_Reset_m1197 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// SCR_Menu/<AlphaLerp>c__Iterator3
#include "AssemblyU2DCSharp_SCR_Menu_U3CAlphaLerpU3Ec__Iterator3.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Menu/<AlphaLerp>c__Iterator3
#include "AssemblyU2DCSharp_SCR_Menu_U3CAlphaLerpU3Ec__Iterator3MethodDeclarations.h"



// System.Void SCR_Menu/<AlphaLerp>c__Iterator3::.ctor()
extern "C" void U3CAlphaLerpU3Ec__Iterator3__ctor_m1198 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_Menu/<AlphaLerp>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAlphaLerpU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_7);
		return L_0;
	}
}
// System.Object SCR_Menu/<AlphaLerp>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAlphaLerpU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1200 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_7);
		return L_0;
	}
}
// System.Boolean SCR_Menu/<AlphaLerp>c__Iterator3::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CAlphaLerpU3Ec__Iterator3_MoveNext_m1201 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_6);
		V_0 = L_0;
		__this->___U24PC_6 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ec;
		}
	}
	{
		goto IL_0103;
	}

IL_0021:
	{
		__this->___U3CalphaValueU3E__0_0 = (0.0f);
		__this->___U3CmodifierU3E__1_1 = (1.0f);
		goto IL_00ec;
	}

IL_003c:
	{
		float L_2 = (__this->___U3CalphaValueU3E__0_0);
		float L_3 = Time_get_fixedDeltaTime_m1737(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = (__this->___speed_3);
		float L_5 = (__this->___U3CmodifierU3E__1_1);
		__this->___U3CalphaValueU3E__0_0 = ((float)((float)L_2+(float)((float)((float)((float)((float)L_3*(float)L_4))*(float)L_5))));
		bool L_6 = (__this->___isRepeating_4);
		if (!L_6)
		{
			goto IL_0087;
		}
	}
	{
		float L_7 = (__this->___U3CalphaValueU3E__0_0);
		if ((!(((float)L_7) >= ((float)(1.0f)))))
		{
			goto IL_0087;
		}
	}
	{
		__this->___U3CmodifierU3E__1_1 = (-1.0f);
		goto IL_00a2;
	}

IL_0087:
	{
		float L_8 = (__this->___U3CalphaValueU3E__0_0);
		if ((!(((float)L_8) <= ((float)(0.0f)))))
		{
			goto IL_00a2;
		}
	}
	{
		__this->___U3CmodifierU3E__1_1 = (1.0f);
	}

IL_00a2:
	{
		Image_t48 * L_9 = (__this->___image_2);
		NullCheck(L_9);
		Color_t65  L_10 = Graphic_get_color_m1738(L_9, /*hidden argument*/NULL);
		__this->___U3CnewColorU3E__2_5 = L_10;
		Color_t65 * L_11 = &(__this->___U3CnewColorU3E__2_5);
		float L_12 = (__this->___U3CalphaValueU3E__0_0);
		L_11->___a_3 = L_12;
		Image_t48 * L_13 = (__this->___image_2);
		Color_t65  L_14 = (__this->___U3CnewColorU3E__2_5);
		NullCheck(L_13);
		Graphic_set_color_m1739(L_13, L_14, /*hidden argument*/NULL);
		WaitForEndOfFrame_t255 * L_15 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_15, /*hidden argument*/NULL);
		__this->___U24current_7 = L_15;
		__this->___U24PC_6 = 1;
		goto IL_0105;
	}

IL_00ec:
	{
		Image_t48 * L_16 = (__this->___image_2);
		NullCheck(L_16);
		bool L_17 = Behaviour_get_isActiveAndEnabled_m1740(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_003c;
		}
	}
	{
		__this->___U24PC_6 = (-1);
	}

IL_0103:
	{
		return 0;
	}

IL_0105:
	{
		return 1;
	}
	// Dead block : IL_0107: ldloc.1
}
// System.Void SCR_Menu/<AlphaLerp>c__Iterator3::Dispose()
extern "C" void U3CAlphaLerpU3Ec__Iterator3_Dispose_m1202 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_6 = (-1);
		return;
	}
}
// System.Void SCR_Menu/<AlphaLerp>c__Iterator3::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CAlphaLerpU3Ec__Iterator3_Reset_m1203 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// SCR_Menu
#include "AssemblyU2DCSharp_SCR_Menu.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Menu
#include "AssemblyU2DCSharp_SCR_MenuMethodDeclarations.h"

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_4.h"
// SCR_GUIManager
#include "AssemblyU2DCSharp_SCR_GUIManager.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_.h"
// SCR_SoundManager
#include "AssemblyU2DCSharp_SCR_SoundManager.h"
// SCR_LevelManager
#include "AssemblyU2DCSharp_SCR_LevelManager.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_4MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_MethodDeclarations.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_genMethodDeclarations.h"
// SCR_SoundManager
#include "AssemblyU2DCSharp_SCR_SoundManagerMethodDeclarations.h"
// SCR_GUIManager
#include "AssemblyU2DCSharp_SCR_GUIManagerMethodDeclarations.h"
// SCR_LevelManager
#include "AssemblyU2DCSharp_SCR_LevelManagerMethodDeclarations.h"
struct Component_t219;
struct TransformU5BU5D_t141;
struct Component_t219;
struct ObjectU5BU5D_t224;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t224* Component_GetComponentsInChildren_TisObject_t_m698_gshared (Component_t219 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m698(__this, method) (( ObjectU5BU5D_t224* (*) (Component_t219 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m698_gshared)(__this, method)
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Transform>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Transform>()
#define Component_GetComponentsInChildren_TisTransform_t1_m1001(__this, method) (( TransformU5BU5D_t141* (*) (Component_t219 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m698_gshared)(__this, method)
struct GameObject_t78;
struct Text_t316;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t316_m1741(__this, method) (( Text_t316 * (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m771_gshared)(__this, method)


// System.Void SCR_Menu::.ctor()
extern TypeInfo* Dictionary_2_t334_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1742_MethodInfo_var;
extern "C" void SCR_Menu__ctor_m1204 (SCR_Menu_t336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t334_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(668);
		Dictionary_2__ctor_m1742_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483703);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t334 * L_0 = (Dictionary_2_t334 *)il2cpp_codegen_object_new (Dictionary_2_t334_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1742(L_0, /*hidden argument*/Dictionary_2__ctor_m1742_MethodInfo_var);
		__this->___menuItems_3 = L_0;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Menu::Awake()
extern const MethodInfo* Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var;
extern "C" void SCR_Menu_Awake_m1205 (SCR_Menu_t336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483679);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		TransformU5BU5D_t141* L_1 = Component_GetComponentsInChildren_TisTransform_t1_m1001(L_0, /*hidden argument*/Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var);
		__this->___children_2 = L_1;
		TransformU5BU5D_t141* L_2 = (__this->___children_2);
		SCR_Menu_FillDictionary_m1207(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Menu::OnEnable()
extern "C" void SCR_Menu_OnEnable_m1206 (SCR_Menu_t336 * __this, const MethodInfo* method)
{
	{
		SCR_GUIManager_t378 * L_0 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m623(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		SCR_Menu_ResetMenu_m1209(__this, /*hidden argument*/NULL);
		SCR_Menu_SetMenuLanguage_m1217(__this, /*hidden argument*/NULL);
		goto IL_0022;
	}

IL_0021:
	{
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void SCR_Menu::FillDictionary(UnityEngine.Transform[])
extern "C" void SCR_Menu_FillDictionary_m1207 (SCR_Menu_t336 * __this, TransformU5BU5D_t141* ___children, const MethodInfo* method)
{
	Transform_t1 * V_0 = {0};
	TransformU5BU5D_t141* V_1 = {0};
	int32_t V_2 = 0;
	{
		TransformU5BU5D_t141* L_0 = ___children;
		V_1 = L_0;
		V_2 = 0;
		goto IL_0028;
	}

IL_0009:
	{
		TransformU5BU5D_t141* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_1, L_3));
		Dictionary_2_t334 * L_4 = (__this->___menuItems_3);
		Transform_t1 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m798(L_5, /*hidden argument*/NULL);
		Transform_t1 * L_7 = V_0;
		NullCheck(L_7);
		GameObject_t78 * L_8 = Component_get_gameObject_m622(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, GameObject_t78 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>::Add(!0,!1) */, L_4, L_6, L_8);
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_10 = V_2;
		TransformU5BU5D_t141* L_11 = V_1;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)(((Array_t *)L_11)->max_length))))))
		{
			goto IL_0009;
		}
	}
	{
		return;
	}
}
// System.Void SCR_Menu::ShowMenuItem(System.String,System.Boolean)
extern "C" void SCR_Menu_ShowMenuItem_m1208 (SCR_Menu_t336 * __this, String_t* ___itemName, bool ___show, const MethodInfo* method)
{
	GameObject_t78 * V_0 = {0};
	{
		Dictionary_2_t334 * L_0 = (__this->___menuItems_3);
		String_t* L_1 = ___itemName;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker2< bool, String_t*, GameObject_t78 ** >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>::TryGetValue(!0,!1&) */, L_0, L_1, (&V_0));
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		GameObject_t78 * L_3 = V_0;
		bool L_4 = ___show;
		NullCheck(L_3);
		GameObject_SetActive_m713(L_3, L_4, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return;
	}
}
// System.Void SCR_Menu::ResetMenu()
extern TypeInfo* Enumerator_t420_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t233_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m1743_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1744_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1745_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1746_MethodInfo_var;
extern "C" void SCR_Menu_ResetMenu_m1209 (SCR_Menu_t336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(669);
		IDisposable_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		Dictionary_2_GetEnumerator_m1743_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483704);
		Enumerator_get_Current_m1744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483705);
		KeyValuePair_2_get_Value_m1745_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		Enumerator_MoveNext_m1746_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483707);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t419  V_0 = {0};
	Enumerator_t420  V_1 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t334 * L_0 = (__this->___menuItems_3);
		NullCheck(L_0);
		Enumerator_t420  L_1 = Dictionary_2_GetEnumerator_m1743(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m1743_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			KeyValuePair_2_t419  L_2 = Enumerator_get_Current_m1744((&V_1), /*hidden argument*/Enumerator_get_Current_m1744_MethodInfo_var);
			V_0 = L_2;
			GameObject_t78 * L_3 = KeyValuePair_2_get_Value_m1745((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1745_MethodInfo_var);
			NullCheck(L_3);
			GameObject_SetActive_m713(L_3, 1, /*hidden argument*/NULL);
		}

IL_0026:
		{
			bool L_4 = Enumerator_MoveNext_m1746((&V_1), /*hidden argument*/Enumerator_MoveNext_m1746_MethodInfo_var);
			if (L_4)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t420  L_5 = V_1;
		Enumerator_t420  L_6 = L_5;
		Object_t * L_7 = Box(Enumerator_t420_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t233_il2cpp_TypeInfo_var, L_7);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_0043:
	{
		return;
	}
}
// UnityEngine.GameObject SCR_Menu::GetMenuItem(System.String)
extern "C" GameObject_t78 * SCR_Menu_GetMenuItem_m1210 (SCR_Menu_t336 * __this, String_t* ___name, const MethodInfo* method)
{
	GameObject_t78 * V_0 = {0};
	{
		Dictionary_2_t334 * L_0 = (__this->___menuItems_3);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker2< bool, String_t*, GameObject_t78 ** >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>::TryGetValue(!0,!1&) */, L_0, L_1, (&V_0));
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		GameObject_t78 * L_3 = V_0;
		return L_3;
	}

IL_0015:
	{
		return (GameObject_t78 *)NULL;
	}
}
// System.Void SCR_Menu::ModifyText(System.String,System.String)
extern const MethodInfo* GameObject_GetComponent_TisText_t316_m1741_MethodInfo_var;
extern "C" void SCR_Menu_ModifyText_m1211 (SCR_Menu_t336 * __this, String_t* ___textGuiName, String_t* ___textToWrite, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisText_t316_m1741_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483708);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t78 * V_0 = {0};
	Text_t316 * V_1 = {0};
	{
		Dictionary_2_t334 * L_0 = (__this->___menuItems_3);
		String_t* L_1 = ___textGuiName;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker2< bool, String_t*, GameObject_t78 ** >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>::TryGetValue(!0,!1&) */, L_0, L_1, (&V_0));
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		GameObject_t78 * L_3 = V_0;
		NullCheck(L_3);
		Text_t316 * L_4 = GameObject_GetComponent_TisText_t316_m1741(L_3, /*hidden argument*/GameObject_GetComponent_TisText_t316_m1741_MethodInfo_var);
		V_1 = L_4;
		Text_t316 * L_5 = V_1;
		bool L_6 = Object_op_Inequality_m623(NULL /*static, unused*/, L_5, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		Text_t316 * L_7 = V_1;
		String_t* L_8 = ___textToWrite;
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_8);
	}

IL_002d:
	{
		return;
	}
}
// System.Collections.IEnumerator SCR_Menu::AlphaLerp(UnityEngine.UI.Text,System.Single,System.Boolean)
extern TypeInfo* U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_Menu_AlphaLerp_m1212 (SCR_Menu_t336 * __this, Text_t316 * ___text, float ___speed, bool ___isRepeating, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(671);
		s_Il2CppMethodIntialized = true;
	}
	U3CAlphaLerpU3Ec__Iterator2_t332 * V_0 = {0};
	{
		U3CAlphaLerpU3Ec__Iterator2_t332 * L_0 = (U3CAlphaLerpU3Ec__Iterator2_t332 *)il2cpp_codegen_object_new (U3CAlphaLerpU3Ec__Iterator2_t332_il2cpp_TypeInfo_var);
		U3CAlphaLerpU3Ec__Iterator2__ctor_m1192(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAlphaLerpU3Ec__Iterator2_t332 * L_1 = V_0;
		Text_t316 * L_2 = ___text;
		NullCheck(L_1);
		L_1->___text_2 = L_2;
		U3CAlphaLerpU3Ec__Iterator2_t332 * L_3 = V_0;
		float L_4 = ___speed;
		NullCheck(L_3);
		L_3->___speed_4 = L_4;
		U3CAlphaLerpU3Ec__Iterator2_t332 * L_5 = V_0;
		bool L_6 = ___isRepeating;
		NullCheck(L_5);
		L_5->___isRepeating_5 = L_6;
		U3CAlphaLerpU3Ec__Iterator2_t332 * L_7 = V_0;
		Text_t316 * L_8 = ___text;
		NullCheck(L_7);
		L_7->___U3CU24U3Etext_9 = L_8;
		U3CAlphaLerpU3Ec__Iterator2_t332 * L_9 = V_0;
		float L_10 = ___speed;
		NullCheck(L_9);
		L_9->___U3CU24U3Espeed_10 = L_10;
		U3CAlphaLerpU3Ec__Iterator2_t332 * L_11 = V_0;
		bool L_12 = ___isRepeating;
		NullCheck(L_11);
		L_11->___U3CU24U3EisRepeating_11 = L_12;
		U3CAlphaLerpU3Ec__Iterator2_t332 * L_13 = V_0;
		return L_13;
	}
}
// System.Collections.IEnumerator SCR_Menu::AlphaLerp(UnityEngine.UI.Image,System.Single,System.Boolean)
extern TypeInfo* U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_Menu_AlphaLerp_m1213 (SCR_Menu_t336 * __this, Image_t48 * ___image, float ___speed, bool ___isRepeating, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	U3CAlphaLerpU3Ec__Iterator3_t333 * V_0 = {0};
	{
		U3CAlphaLerpU3Ec__Iterator3_t333 * L_0 = (U3CAlphaLerpU3Ec__Iterator3_t333 *)il2cpp_codegen_object_new (U3CAlphaLerpU3Ec__Iterator3_t333_il2cpp_TypeInfo_var);
		U3CAlphaLerpU3Ec__Iterator3__ctor_m1198(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAlphaLerpU3Ec__Iterator3_t333 * L_1 = V_0;
		Image_t48 * L_2 = ___image;
		NullCheck(L_1);
		L_1->___image_2 = L_2;
		U3CAlphaLerpU3Ec__Iterator3_t333 * L_3 = V_0;
		float L_4 = ___speed;
		NullCheck(L_3);
		L_3->___speed_3 = L_4;
		U3CAlphaLerpU3Ec__Iterator3_t333 * L_5 = V_0;
		bool L_6 = ___isRepeating;
		NullCheck(L_5);
		L_5->___isRepeating_4 = L_6;
		U3CAlphaLerpU3Ec__Iterator3_t333 * L_7 = V_0;
		Image_t48 * L_8 = ___image;
		NullCheck(L_7);
		L_7->___U3CU24U3Eimage_8 = L_8;
		U3CAlphaLerpU3Ec__Iterator3_t333 * L_9 = V_0;
		float L_10 = ___speed;
		NullCheck(L_9);
		L_9->___U3CU24U3Espeed_9 = L_10;
		U3CAlphaLerpU3Ec__Iterator3_t333 * L_11 = V_0;
		bool L_12 = ___isRepeating;
		NullCheck(L_11);
		L_11->___U3CU24U3EisRepeating_10 = L_12;
		U3CAlphaLerpU3Ec__Iterator3_t333 * L_13 = V_0;
		return L_13;
	}
}
// System.Void SCR_Menu::Options()
extern "C" void SCR_Menu_Options_m1214 (SCR_Menu_t336 * __this, const MethodInfo* method)
{
	{
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral291, 1, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral292, 1, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral293, 0, /*hidden argument*/NULL);
		SCR_SoundManager_t335 * L_0 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = SCR_SoundManager_get_SoundOn_m1498(L_0, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral294, ((((int32_t)L_1) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		SCR_SoundManager_t335 * L_2 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = SCR_SoundManager_get_SoundOn_m1498(L_2, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral295, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Menu::SwitchLanguage(System.Boolean)
extern "C" void SCR_Menu_SwitchLanguage_m1215 (SCR_Menu_t336 * __this, bool ___isFrench, const MethodInfo* method)
{
	{
		SCR_GUIManager_t378 * L_0 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___isFrench;
		NullCheck(L_0);
		SCR_GUIManager_ToggleLanguage_m1412(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Menu::ToggleSound(System.Boolean)
extern "C" void SCR_Menu_ToggleSound_m1216 (SCR_Menu_t336 * __this, bool ___soundOn, const MethodInfo* method)
{
	{
		SCR_SoundManager_t335 * L_0 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___soundOn;
		NullCheck(L_0);
		SCR_SoundManager_set_SoundOn_m1499(L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = ___soundOn;
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral294, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		bool L_3 = ___soundOn;
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral295, L_3, /*hidden argument*/NULL);
		bool L_4 = ___soundOn;
		if (L_4)
		{
			goto IL_003b;
		}
	}
	{
		SCR_SoundManager_t335 * L_5 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		SCR_SoundManager_StopAllSounds_m1497(L_5, /*hidden argument*/NULL);
		goto IL_004a;
	}

IL_003b:
	{
		SCR_SoundManager_t335 * L_6 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		SCR_SoundManager_PlaySound_m1495(L_6, (String_t*) &_stringLiteral296, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void SCR_Menu::SetMenuLanguage()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var;
extern "C" void SCR_Menu_SetMenuLanguage_m1217 (SCR_Menu_t336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483679);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		SCR_GUIManager_t378 * L_0 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		TransformU5BU5D_t141* L_2 = (__this->___children_2);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Transform_t1 * L_3 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		TransformU5BU5D_t141* L_4 = Component_GetComponentsInChildren_TisTransform_t1_m1001(L_3, /*hidden argument*/Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var);
		__this->___children_2 = L_4;
	}

IL_002d:
	{
		V_0 = 0;
		goto IL_00b2;
	}

IL_0034:
	{
		TransformU5BU5D_t141* L_5 = (__this->___children_2);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_5, L_7)));
		String_t* L_8 = Component_get_tag_m635((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_5, L_7)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m636(NULL /*static, unused*/, L_8, (String_t*) &_stringLiteral297, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0072;
		}
	}
	{
		TransformU5BU5D_t141* L_10 = (__this->___children_2);
		int32_t L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_10, L_12)));
		String_t* L_13 = Object_get_name_m798((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_10, L_12)), /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_14 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		bool L_15 = SCR_GUIManager_get_IsFrench_m1413(L_14, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, L_13, L_15, /*hidden argument*/NULL);
		goto IL_00ae;
	}

IL_0072:
	{
		TransformU5BU5D_t141* L_16 = (__this->___children_2);
		int32_t L_17 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_16, L_18)));
		String_t* L_19 = Component_get_tag_m635((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_16, L_18)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Equality_m636(NULL /*static, unused*/, L_19, (String_t*) &_stringLiteral298, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ae;
		}
	}
	{
		TransformU5BU5D_t141* L_21 = (__this->___children_2);
		int32_t L_22 = V_0;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_21, L_23)));
		String_t* L_24 = Object_get_name_m798((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_21, L_23)), /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_25 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		bool L_26 = SCR_GUIManager_get_IsFrench_m1413(L_25, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, L_24, ((((int32_t)L_26) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_00ae:
	{
		int32_t L_27 = V_0;
		V_0 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00b2:
	{
		int32_t L_28 = V_0;
		TransformU5BU5D_t141* L_29 = (__this->___children_2);
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)(((Array_t *)L_29)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		return;
	}
}
// System.Void SCR_Menu::ReturnToMainMenu()
extern "C" void SCR_Menu_ReturnToMainMenu_m1218 (SCR_Menu_t336 * __this, const MethodInfo* method)
{
	{
		SCR_LevelManager_t379 * L_0 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_LevelManager_LoadLevel_m1420(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Menu::LoadLevel(System.Int32)
extern "C" void SCR_Menu_LoadLevel_m1219 (SCR_Menu_t336 * __this, int32_t ___level, const MethodInfo* method)
{
	{
		SCR_LevelManager_t379 * L_0 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___level;
		NullCheck(L_0);
		SCR_LevelManager_LoadLevel_m1420(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Menu::ButtonSound()
extern "C" void SCR_Menu_ButtonSound_m1220 (SCR_Menu_t336 * __this, const MethodInfo* method)
{
	{
		SCR_SoundManager_t335 * L_0 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_SoundManager_PlaySound_m1495(L_0, (String_t*) &_stringLiteral299, /*hidden argument*/NULL);
		return;
	}
}
// SCR_SlowAvatar
#include "AssemblyU2DCSharp_SCR_SlowAvatar.h"
#ifndef _MSC_VER
#else
#endif
// SCR_SlowAvatar
#include "AssemblyU2DCSharp_SCR_SlowAvatarMethodDeclarations.h"

// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// SCR_ManagerComponents
#include "AssemblyU2DCSharp_SCR_ManagerComponents.h"
// SCR_Camie
#include "AssemblyU2DCSharp_SCR_Camie.h"
// SCR_Camie
#include "AssemblyU2DCSharp_SCR_CamieMethodDeclarations.h"


// System.Void SCR_SlowAvatar::.ctor()
extern "C" void SCR_SlowAvatar__ctor_m1221 (SCR_SlowAvatar_t337 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_SlowAvatar::TriggerEnterEffect()
// System.Void SCR_SlowAvatar::TriggerExitEffet()
// System.Void SCR_SlowAvatar::OnTriggerEnter(UnityEngine.Collider)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void SCR_SlowAvatar_OnTriggerEnter_m1222 (SCR_SlowAvatar_t337 * __this, Collider_t138 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t138 * L_0 = ___other;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m635(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m636(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0044;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_3 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_Camie_t338 * L_4 = SCR_ManagerComponents_get_AvatarMove_m1441(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = SCR_Camie_get_IsSlowed_m1264(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0044;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_6 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		SCR_Camie_t338 * L_7 = SCR_ManagerComponents_get_AvatarMove_m1441(L_6, /*hidden argument*/NULL);
		float L_8 = (__this->___speedDecreaseToPercent_2);
		NullCheck(L_7);
		SCR_Camie_DecreaseSpeed_m1251(L_7, L_8, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(4 /* System.Void SCR_SlowAvatar::TriggerEnterEffect() */, __this);
	}

IL_0044:
	{
		return;
	}
}
// System.Void SCR_SlowAvatar::OnTriggerExit()
extern "C" void SCR_SlowAvatar_OnTriggerExit_m1223 (SCR_SlowAvatar_t337 * __this, const MethodInfo* method)
{
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Camie_t338 * L_1 = SCR_ManagerComponents_get_AvatarMove_m1441(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		SCR_Camie_NormalSpeed_m1252(L_1, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(5 /* System.Void SCR_SlowAvatar::TriggerExitEffet() */, __this);
		return;
	}
}
// SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4
#include "AssemblyU2DCSharp_SCR_Camie_U3CGoToFrontCollisionPointU3Ec__.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4
#include "AssemblyU2DCSharp_SCR_Camie_U3CGoToFrontCollisionPointU3Ec__MethodDeclarations.h"

// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"


// System.Void SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::.ctor()
extern "C" void U3CGoToFrontCollisionPointU3Ec__Iterator4__ctor_m1224 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1225 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1226 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CGoToFrontCollisionPointU3Ec__Iterator4_MoveNext_m1227 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_010e;
		}
	}
	{
		goto IL_0160;
	}

IL_0021:
	{
		Vector3_t4  L_2 = (__this->___normal_0);
		Vector3_t4  L_3 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = Vector3_op_Inequality_m722(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0159;
		}
	}
	{
		SCR_Camie_t338 * L_5 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_5);
		L_5->___isLerping_28 = 1;
		SCR_Camie_t338 * L_6 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_6);
		Collider_t138 * L_7 = (L_6->___frontColliderChecker_37);
		NullCheck(L_7);
		Collider_set_enabled_m1747(L_7, 0, /*hidden argument*/NULL);
		goto IL_010e;
	}

IL_0058:
	{
		SCR_Camie_t338 * L_8 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_8);
		Transform_t1 * L_9 = (L_8->___trans_33);
		Vector3_t4  L_10 = (__this->___point_1);
		NullCheck(L_9);
		Transform_set_position_m607(L_9, L_10, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_11 = (__this->___U3CU3Ef__this_6);
		Vector3_t4  L_12 = (__this->___normal_0);
		NullCheck(L_11);
		L_11->___surfaceNormal_7 = L_12;
		SCR_Camie_t338 * L_13 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_13);
		SCR_Camie_GetDirection_m1250(L_13, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_14 = (__this->___U3CU3Ef__this_6);
		SCR_Camie_t338 * L_15 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_15);
		Vector3_t4  L_16 = (L_15->___surfaceNormal_7);
		NullCheck(L_14);
		L_14->___myNormal_8 = L_16;
		SCR_Camie_t338 * L_17 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_17);
		Rigidbody_t14 * L_18 = (L_17->___rb_21);
		SCR_Camie_t338 * L_19 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_19);
		float L_20 = (L_19->___gravity_4);
		SCR_Camie_t338 * L_21 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_21);
		Rigidbody_t14 * L_22 = (L_21->___rb_21);
		NullCheck(L_22);
		float L_23 = Rigidbody_get_mass_m1748(L_22, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_24 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_24);
		Vector3_t4  L_25 = (L_24->___myNormal_8);
		Vector3_t4  L_26 = Vector3_op_Multiply_m598(NULL /*static, unused*/, ((float)((float)((-L_20))*(float)L_23)), L_25, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_27 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_27);
		float L_28 = (L_27->___speedModifier_12);
		Vector3_t4  L_29 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		float L_30 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_31 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_18);
		Rigidbody_AddForce_m1749(L_18, L_31, /*hidden argument*/NULL);
		WaitForEndOfFrame_t255 * L_32 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_32, /*hidden argument*/NULL);
		__this->___U24current_3 = L_32;
		__this->___U24PC_2 = 1;
		goto IL_0162;
	}

IL_010e:
	{
		Vector3_t4  L_33 = (__this->___point_1);
		SCR_Camie_t338 * L_34 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_34);
		Transform_t1 * L_35 = (L_34->___trans_33);
		NullCheck(L_35);
		Vector3_t4  L_36 = Transform_get_position_m593(L_35, /*hidden argument*/NULL);
		float L_37 = Vector3_Distance_m1750(NULL /*static, unused*/, L_33, L_36, /*hidden argument*/NULL);
		if ((((double)(((double)L_37))) > ((double)(0.2))))
		{
			goto IL_0058;
		}
	}
	{
		SCR_Camie_t338 * L_38 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_38);
		L_38->___isLerping_28 = 0;
		SCR_Camie_t338 * L_39 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_39);
		MonoBehaviour_Invoke_m1015(L_39, (String_t*) &_stringLiteral311, (0.1f), /*hidden argument*/NULL);
	}

IL_0159:
	{
		__this->___U24PC_2 = (-1);
	}

IL_0160:
	{
		return 0;
	}

IL_0162:
	{
		return 1;
	}
	// Dead block : IL_0164: ldloc.1
}
// System.Void SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::Dispose()
extern "C" void U3CGoToFrontCollisionPointU3Ec__Iterator4_Dispose_m1228 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CGoToFrontCollisionPointU3Ec__Iterator4_Reset_m1229 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// SCR_Camie/<LandOnCollisionPoint>c__Iterator5
#include "AssemblyU2DCSharp_SCR_Camie_U3CLandOnCollisionPointU3Ec__Ite.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Camie/<LandOnCollisionPoint>c__Iterator5
#include "AssemblyU2DCSharp_SCR_Camie_U3CLandOnCollisionPointU3Ec__IteMethodDeclarations.h"

// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
// UnityEngine.SphereCollider
#include "UnityEngine_UnityEngine_SphereCollider.h"
// UnityEngine.BoxCollider
#include "UnityEngine_UnityEngine_BoxCollider.h"
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"


// System.Void SCR_Camie/<LandOnCollisionPoint>c__Iterator5::.ctor()
extern "C" void U3CLandOnCollisionPointU3Ec__Iterator5__ctor_m1230 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_Camie/<LandOnCollisionPoint>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1231 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object SCR_Camie/<LandOnCollisionPoint>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1232 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean SCR_Camie/<LandOnCollisionPoint>c__Iterator5::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CLandOnCollisionPointU3Ec__Iterator5_MoveNext_m1233 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_011e;
		}
	}
	{
		goto IL_01f7;
	}

IL_0021:
	{
		SCR_SoundManager_t335 * L_2 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_SoundManager_StopSound_m1496(L_2, (String_t*) &_stringLiteral312, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_3 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_3);
		L_3->___isFlyLerping_29 = 1;
		SCR_Camie_t338 * L_4 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_4);
		Collider_t138 * L_5 = (L_4->___frontColliderChecker_37);
		NullCheck(L_5);
		Collider_set_enabled_m1747(L_5, 0, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_6 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_6);
		L_6->___isJumping_17 = 0;
		SCR_Camie_t338 * L_7 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_7);
		L_7->___isFlyTimeCounting_15 = 0;
		SCR_Camie_t338 * L_8 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_8);
		L_8->___canFly_16 = 0;
		SCR_Camie_t338 * L_9 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_9);
		Animator_t9 * L_10 = (L_9->___anim_23);
		NullCheck(L_10);
		Animator_SetBool_m624(L_10, (String_t*) &_stringLiteral302, 0, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_11 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_11);
		SphereCollider_t136 * L_12 = (L_11->___flyCollider_38);
		NullCheck(L_12);
		Collider_set_enabled_m1747(L_12, 0, /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_009d:
	{
		SCR_Camie_t338 * L_13 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_13);
		Transform_t1 * L_14 = (L_13->___trans_33);
		SCR_Camie_t338 * L_15 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_15);
		Transform_t1 * L_16 = (L_15->___trans_33);
		NullCheck(L_16);
		Vector3_t4  L_17 = Transform_get_position_m593(L_16, /*hidden argument*/NULL);
		Vector3_t4  L_18 = (__this->___point_0);
		SCR_Camie_t338 * L_19 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_19);
		float L_20 = (L_19->___jumpSpeed_6);
		SCR_Camie_t338 * L_21 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_21);
		float L_22 = (L_21->___speedModifier_12);
		float L_23 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_24 = Vector3_Lerp_m652(NULL /*static, unused*/, L_17, L_18, ((float)((float)((float)((float)((float)((float)L_20*(float)(1.75f)))*(float)L_22))*(float)L_23)), /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_position_m607(L_14, L_24, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_25 = (__this->___U3CU3Ef__this_6);
		Vector3_t4  L_26 = (__this->___normal_1);
		NullCheck(L_25);
		L_25->___surfaceNormal_7 = L_26;
		SCR_Camie_t338 * L_27 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_27);
		SCR_Camie_GetDirection_m1250(L_27, /*hidden argument*/NULL);
		WaitForEndOfFrame_t255 * L_28 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_28, /*hidden argument*/NULL);
		__this->___U24current_3 = L_28;
		__this->___U24PC_2 = 1;
		goto IL_01f9;
	}

IL_011e:
	{
		SCR_Camie_t338 * L_29 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_29);
		Transform_t1 * L_30 = (L_29->___trans_33);
		NullCheck(L_30);
		Vector3_t4  L_31 = Transform_get_position_m593(L_30, /*hidden argument*/NULL);
		Vector3_t4  L_32 = (__this->___point_0);
		float L_33 = Vector3_Distance_m1750(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		if ((((float)L_33) > ((float)(0.3f))))
		{
			goto IL_009d;
		}
	}
	{
		SCR_Camie_t338 * L_34 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_34);
		Transform_t1 * L_35 = (L_34->___trans_33);
		Vector3_t4  L_36 = (__this->___point_0);
		NullCheck(L_35);
		Transform_set_position_m607(L_35, L_36, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_37 = (__this->___U3CU3Ef__this_6);
		Vector3_t4  L_38 = (__this->___normal_1);
		NullCheck(L_37);
		L_37->___surfaceNormal_7 = L_38;
		SCR_Camie_t338 * L_39 = (__this->___U3CU3Ef__this_6);
		Vector3_t4  L_40 = (__this->___normal_1);
		NullCheck(L_39);
		L_39->___myNormal_8 = L_40;
		SCR_Camie_t338 * L_41 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_41);
		SCR_Camie_GetDirection_m1250(L_41, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_42 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_42);
		L_42->___isGrounded_5 = 1;
		SCR_Camie_t338 * L_43 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_43);
		Collider_t138 * L_44 = (L_43->___frontColliderChecker_37);
		NullCheck(L_44);
		Collider_set_enabled_m1747(L_44, 1, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_45 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_45);
		BoxCollider_t343 * L_46 = (L_45->___myColl_22);
		NullCheck(L_46);
		Collider_set_enabled_m1747(L_46, 1, /*hidden argument*/NULL);
		SCR_SoundManager_t335 * L_47 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		SCR_SoundManager_PlaySound_m1495(L_47, (String_t*) &_stringLiteral313, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_48 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_48);
		L_48->___isFlyLerping_29 = 0;
		SCR_Camie_t338 * L_49 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_49);
		L_49->___canFly_16 = 1;
		SCR_Camie_t338 * L_50 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_50);
		MonoBehaviour_Invoke_m1015(L_50, (String_t*) &_stringLiteral311, (0.1f), /*hidden argument*/NULL);
		__this->___U24PC_2 = (-1);
	}

IL_01f7:
	{
		return 0;
	}

IL_01f9:
	{
		return 1;
	}
	// Dead block : IL_01fb: ldloc.1
}
// System.Void SCR_Camie/<LandOnCollisionPoint>c__Iterator5::Dispose()
extern "C" void U3CLandOnCollisionPointU3Ec__Iterator5_Dispose_m1234 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void SCR_Camie/<LandOnCollisionPoint>c__Iterator5::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CLandOnCollisionPointU3Ec__Iterator5_Reset_m1235 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// SCR_Camie/<Jump>c__Iterator6
#include "AssemblyU2DCSharp_SCR_Camie_U3CJumpU3Ec__Iterator6.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Camie/<Jump>c__Iterator6
#include "AssemblyU2DCSharp_SCR_Camie_U3CJumpU3Ec__Iterator6MethodDeclarations.h"



// System.Void SCR_Camie/<Jump>c__Iterator6::.ctor()
extern "C" void U3CJumpU3Ec__Iterator6__ctor_m1236 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_Camie/<Jump>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CJumpU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1237 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object SCR_Camie/<Jump>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CJumpU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1238 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean SCR_Camie/<Jump>c__Iterator6::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CJumpU3Ec__Iterator6_MoveNext_m1239 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00bc;
		}
	}
	{
		goto IL_0152;
	}

IL_0021:
	{
		SCR_SoundManager_t335 * L_2 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_SoundManager_PlaySound_m1495(L_2, (String_t*) &_stringLiteral312, /*hidden argument*/NULL);
		__this->___U3CtU3E__0_0 = (0.0f);
		SCR_Camie_t338 * L_3 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_3);
		Rigidbody_t14 * L_4 = (L_3->___rb_21);
		Rigidbody_t14 * L_5 = L_4;
		NullCheck(L_5);
		Vector3_t4  L_6 = Rigidbody_get_velocity_m646(L_5, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_7 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_7);
		Vector3_t4  L_8 = (L_7->___myNormal_8);
		SCR_Camie_t338 * L_9 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_9);
		float L_10 = (L_9->___jumpSpeed_6);
		Vector3_t4  L_11 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		Vector3_t4  L_12 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_6, L_11, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody_set_velocity_m1003(L_5, L_12, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_13 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_13);
		BoxCollider_t343 * L_14 = (L_13->___myColl_22);
		NullCheck(L_14);
		Collider_set_enabled_m1747(L_14, 0, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_0087:
	{
		float L_15 = (__this->___U3CtU3E__0_0);
		float L_16 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_17 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_17);
		float L_18 = (L_17->___speedModifier_12);
		__this->___U3CtU3E__0_0 = ((float)((float)L_15+(float)((float)((float)L_16*(float)L_18))));
		WaitForEndOfFrame_t255 * L_19 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_19, /*hidden argument*/NULL);
		__this->___U24current_2 = L_19;
		__this->___U24PC_1 = 1;
		goto IL_0154;
	}

IL_00bc:
	{
		float L_20 = (__this->___U3CtU3E__0_0);
		if ((((double)(((double)L_20))) < ((double)(0.5))))
		{
			goto IL_0087;
		}
	}
	{
		SCR_Camie_t338 * L_21 = (__this->___U3CU3Ef__this_3);
		Vector3_t4  L_22 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		L_21->___surfaceNormal_7 = L_22;
		SCR_Camie_t338 * L_23 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_23);
		L_23->___goingRight_11 = 1;
		SCR_Camie_t338 * L_24 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_24);
		SCR_Camie_GetDirection_m1250(L_24, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_25 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_25);
		SphereCollider_t136 * L_26 = (L_25->___flyCollider_38);
		NullCheck(L_26);
		Collider_set_enabled_m1747(L_26, 1, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_27 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_27);
		Rigidbody_t14 * L_28 = (L_27->___rb_21);
		Vector3_t4  L_29 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		Rigidbody_set_velocity_m1003(L_28, L_29, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_30 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_30);
		L_30->___currFlyTime_14 = (0.0f);
		SCR_Camie_t338 * L_31 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_31);
		L_31->___isFlyTimeCounting_15 = 1;
		SCR_Camie_t338 * L_32 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_32);
		SphereCollider_t136 * L_33 = (L_32->___flyCollider_38);
		NullCheck(L_33);
		Collider_set_enabled_m1747(L_33, 1, /*hidden argument*/NULL);
		__this->___U24PC_1 = (-1);
	}

IL_0152:
	{
		return 0;
	}

IL_0154:
	{
		return 1;
	}
	// Dead block : IL_0156: ldloc.1
}
// System.Void SCR_Camie/<Jump>c__Iterator6::Dispose()
extern "C" void U3CJumpU3Ec__Iterator6_Dispose_m1240 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void SCR_Camie/<Jump>c__Iterator6::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CJumpU3Ec__Iterator6_Reset_m1241 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
// SCR_CamieDirections
#include "AssemblyU2DCSharp_SCR_CamieDirections.h"
// SCR_Respawn
#include "AssemblyU2DCSharp_SCR_Respawn.h"
// SCR_Wings
#include "AssemblyU2DCSharp_SCR_Wings.h"
// SCR_FlyButton
#include "AssemblyU2DCSharp_SCR_FlyButton.h"
// SCR_Joystick
#include "AssemblyU2DCSharp_SCR_Joystick.h"
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
// SCR_Joystick/Direction
#include "AssemblyU2DCSharp_SCR_Joystick_Direction.h"
// UnityEngine.Collision
#include "UnityEngine_UnityEngine_Collision.h"
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPoint.h"
// SCR_Goutte
#include "AssemblyU2DCSharp_SCR_Goutte.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
// SCR_Respawn
#include "AssemblyU2DCSharp_SCR_RespawnMethodDeclarations.h"
// SCR_FlyButton
#include "AssemblyU2DCSharp_SCR_FlyButtonMethodDeclarations.h"
// SCR_Joystick
#include "AssemblyU2DCSharp_SCR_JoystickMethodDeclarations.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
// UnityEngine.Collision
#include "UnityEngine_UnityEngine_CollisionMethodDeclarations.h"
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPointMethodDeclarations.h"
// SCR_CamieDirections
#include "AssemblyU2DCSharp_SCR_CamieDirectionsMethodDeclarations.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
// SCR_Wings
#include "AssemblyU2DCSharp_SCR_WingsMethodDeclarations.h"
struct Component_t219;
struct Rigidbody_t14;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t14_m639(__this, method) (( Rigidbody_t14 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct BoxCollider_t343;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider>()
#define Component_GetComponent_TisBoxCollider_t343_m1751(__this, method) (( BoxCollider_t343 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct Animator_t9;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Animator>()
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Animator>()
#define Component_GetComponentInChildren_TisAnimator_t9_m1752(__this, method) (( Animator_t9 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m675_gshared)(__this, method)
struct Component_t219;
struct SCR_WingsU5BU5D_t342;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<SCR_Wings>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<SCR_Wings>()
#define Component_GetComponentsInChildren_TisSCR_Wings_t351_m1753(__this, method) (( SCR_WingsU5BU5D_t342* (*) (Component_t219 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m698_gshared)(__this, method)
struct Component_t219;
struct SCR_Respawn_t344;
// Declaration !!0 UnityEngine.Component::GetComponent<SCR_Respawn>()
// !!0 UnityEngine.Component::GetComponent<SCR_Respawn>()
#define Component_GetComponent_TisSCR_Respawn_t344_m1754(__this, method) (( SCR_Respawn_t344 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct SphereCollider_t136;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.SphereCollider>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SphereCollider>()
#define Component_GetComponent_TisSphereCollider_t136_m1755(__this, method) (( SphereCollider_t136 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void SCR_Camie::.ctor()
extern "C" void SCR_Camie__ctor_m1242 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		__this->___moveSpeed_2 = (5.0f);
		__this->___lerpSpeed_3 = (10.0f);
		__this->___gravity_4 = (10.0f);
		__this->___jumpSpeed_6 = (10.0f);
		__this->___goingRight_11 = 1;
		__this->___speedModifier_12 = (1.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Camie::Start()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t14_m639_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBoxCollider_t343_m1751_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisAnimator_t9_m1752_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisSCR_Wings_t351_m1753_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSCR_Respawn_t344_m1754_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSphereCollider_t136_m1755_MethodInfo_var;
extern "C" void SCR_Camie_Start_m1243 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRigidbody_t14_m639_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		Component_GetComponent_TisBoxCollider_t343_m1751_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483709);
		Component_GetComponentInChildren_TisAnimator_t9_m1752_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483710);
		Component_GetComponentsInChildren_TisSCR_Wings_t351_m1753_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483711);
		Component_GetComponent_TisSCR_Respawn_t344_m1754_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483712);
		Component_GetComponent_TisSphereCollider_t136_m1755_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483713);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rigidbody_t14 * L_0 = Component_GetComponent_TisRigidbody_t14_m639(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t14_m639_MethodInfo_var);
		__this->___rb_21 = L_0;
		BoxCollider_t343 * L_1 = Component_GetComponent_TisBoxCollider_t343_m1751(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider_t343_m1751_MethodInfo_var);
		__this->___myColl_22 = L_1;
		Animator_t9 * L_2 = Component_GetComponentInChildren_TisAnimator_t9_m1752(__this, /*hidden argument*/Component_GetComponentInChildren_TisAnimator_t9_m1752_MethodInfo_var);
		__this->___anim_23 = L_2;
		Transform_t1 * L_3 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_WingsU5BU5D_t342* L_4 = Component_GetComponentsInChildren_TisSCR_Wings_t351_m1753(L_3, /*hidden argument*/Component_GetComponentsInChildren_TisSCR_Wings_t351_m1753_MethodInfo_var);
		__this->___wings_19 = L_4;
		SCR_Respawn_t344 * L_5 = Component_GetComponent_TisSCR_Respawn_t344_m1754(__this, /*hidden argument*/Component_GetComponent_TisSCR_Respawn_t344_m1754_MethodInfo_var);
		__this->___respawner_24 = L_5;
		SCR_ManagerComponents_t380 * L_6 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		SCR_FlyButton_t345 * L_7 = SCR_ManagerComponents_get_FlyButton_m1447(L_6, /*hidden argument*/NULL);
		__this->___flyButton_25 = L_7;
		SCR_ManagerComponents_t380 * L_8 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		SCR_Joystick_t346 * L_9 = SCR_ManagerComponents_get_Joystick_m1445(L_8, /*hidden argument*/NULL);
		__this->___joystick_26 = L_9;
		SCR_Camie_ToggleWings_m1256(__this, 0, /*hidden argument*/NULL);
		Transform_t1 * L_10 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		__this->___trans_33 = L_10;
		SphereCollider_t136 * L_11 = Component_GetComponent_TisSphereCollider_t136_m1755(__this, /*hidden argument*/Component_GetComponent_TisSphereCollider_t136_m1755_MethodInfo_var);
		__this->___flyCollider_38 = L_11;
		SphereCollider_t136 * L_12 = (__this->___flyCollider_38);
		NullCheck(L_12);
		Collider_set_enabled_m1747(L_12, 0, /*hidden argument*/NULL);
		Vector3_t4  L_13 = (__this->___myNormal_8);
		Object_t * L_14 = SCR_Camie_Jump_m1249(__this, L_13, /*hidden argument*/NULL);
		__this->___jumpRoutine_18 = L_14;
		Transform_t1 * L_15 = (__this->___trans_33);
		NullCheck(L_15);
		Vector3_t4  L_16 = Transform_get_up_m644(L_15, /*hidden argument*/NULL);
		__this->___myNormal_8 = L_16;
		Rigidbody_t14 * L_17 = (__this->___rb_21);
		NullCheck(L_17);
		Rigidbody_set_freezeRotation_m1756(L_17, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Camie::Update()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void SCR_Camie_Update_m1244 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t26  V_0 = {0};
	RaycastHit_t24  V_1 = {0};
	Vector3_t4  V_2 = {0};
	Ray_t26  V_3 = {0};
	Ray_t26  V_4 = {0};
	RaycastHit_t24  V_5 = {0};
	RaycastHit_t24  V_6 = {0};
	Vector3_t4  V_7 = {0};
	Vector3_t4  V_8 = {0};
	Vector3_t4  V_9 = {0};
	Vector3_t4  V_10 = {0};
	Vector3_t4  V_11 = {0};
	Vector3_t4  V_12 = {0};
	{
		SCR_Joystick_t346 * L_0 = (__this->___joystick_26);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_2 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_Joystick_t346 * L_3 = SCR_ManagerComponents_get_Joystick_m1445(L_2, /*hidden argument*/NULL);
		__this->___joystick_26 = L_3;
		SCR_Joystick_t346 * L_4 = (__this->___joystick_26);
		bool L_5 = Object_op_Equality_m640(NULL /*static, unused*/, L_4, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		return;
	}

IL_0033:
	{
		SCR_Respawn_t344 * L_6 = (__this->___respawner_24);
		NullCheck(L_6);
		bool L_7 = SCR_Respawn_get_IsRespawning_m1283(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0054;
		}
	}
	{
		Rigidbody_t14 * L_8 = (__this->___rb_21);
		Vector3_t4  L_9 = Vector3_get_down_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Rigidbody_AddForce_m1749(L_8, L_9, /*hidden argument*/NULL);
		return;
	}

IL_0054:
	{
		Transform_t1 * L_10 = (__this->___trans_33);
		NullCheck(L_10);
		Vector3_t4  L_11 = Transform_get_position_m593(L_10, /*hidden argument*/NULL);
		V_7 = L_11;
		float L_12 = ((&V_7)->___y_2);
		SCR_ManagerComponents_t380 * L_13 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t1 * L_14 = SCR_ManagerComponents_get_BottomFrame_m1453(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t4  L_15 = Transform_get_position_m593(L_14, /*hidden argument*/NULL);
		V_8 = L_15;
		float L_16 = ((&V_8)->___y_2);
		if ((!(((float)L_12) < ((float)L_16))))
		{
			goto IL_0093;
		}
	}
	{
		SCR_Camie_ToggleWings_m1256(__this, 0, /*hidden argument*/NULL);
		SCR_Camie_Respawn_m1253(__this, /*hidden argument*/NULL);
		return;
	}

IL_0093:
	{
		Transform_t1 * L_17 = (__this->___trans_33);
		NullCheck(L_17);
		Vector3_t4  L_18 = Transform_get_position_m593(L_17, /*hidden argument*/NULL);
		V_9 = L_18;
		float L_19 = ((&V_9)->___z_3);
		if ((((double)(((double)L_19))) < ((double)(-0.5))))
		{
			goto IL_00d9;
		}
	}
	{
		Transform_t1 * L_20 = (__this->___trans_33);
		NullCheck(L_20);
		Vector3_t4  L_21 = Transform_get_position_m593(L_20, /*hidden argument*/NULL);
		V_10 = L_21;
		float L_22 = ((&V_10)->___z_3);
		if ((!(((double)(((double)L_22))) > ((double)(0.5)))))
		{
			goto IL_0116;
		}
	}

IL_00d9:
	{
		Transform_t1 * L_23 = (__this->___trans_33);
		Transform_t1 * L_24 = (__this->___trans_33);
		NullCheck(L_24);
		Vector3_t4  L_25 = Transform_get_position_m593(L_24, /*hidden argument*/NULL);
		V_11 = L_25;
		float L_26 = ((&V_11)->___x_1);
		Transform_t1 * L_27 = (__this->___trans_33);
		NullCheck(L_27);
		Vector3_t4  L_28 = Transform_get_position_m593(L_27, /*hidden argument*/NULL);
		V_12 = L_28;
		float L_29 = ((&V_12)->___y_2);
		Vector3_t4  L_30 = {0};
		Vector3__ctor_m612(&L_30, L_26, L_29, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_position_m607(L_23, L_30, /*hidden argument*/NULL);
	}

IL_0116:
	{
		SCR_Joystick_t346 * L_31 = (__this->___joystick_26);
		bool L_32 = Object_op_Equality_m640(NULL /*static, unused*/, L_31, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0137;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_33 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_33);
		SCR_Joystick_t346 * L_34 = SCR_ManagerComponents_get_Joystick_m1445(L_33, /*hidden argument*/NULL);
		__this->___joystick_26 = L_34;
	}

IL_0137:
	{
		bool L_35 = (__this->___isFlyLerping_29);
		if (L_35)
		{
			goto IL_014d;
		}
	}
	{
		bool L_36 = (__this->___isLerping_28);
		if (!L_36)
		{
			goto IL_014e;
		}
	}

IL_014d:
	{
		return;
	}

IL_014e:
	{
		bool L_37 = (__this->___canFly_16);
		if (!L_37)
		{
			goto IL_021b;
		}
	}
	{
		bool L_38 = (__this->___isGrounded_5);
		if (!L_38)
		{
			goto IL_021b;
		}
	}
	{
		bool L_39 = (__this->___isJumping_17);
		if (L_39)
		{
			goto IL_021b;
		}
	}
	{
		SCR_FlyButton_t345 * L_40 = (__this->___flyButton_25);
		bool L_41 = Object_op_Inequality_m623(NULL /*static, unused*/, L_40, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0190;
		}
	}
	{
		SCR_FlyButton_t345 * L_42 = (__this->___flyButton_25);
		NullCheck(L_42);
		bool L_43 = SCR_FlyButton_get_IsPressed_m1345(L_42, /*hidden argument*/NULL);
		if (L_43)
		{
			goto IL_019c;
		}
	}

IL_0190:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_44 = Input_GetKeyDown_m925(NULL /*static, unused*/, ((int32_t)102), /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_021b;
		}
	}

IL_019c:
	{
		Animator_t9 * L_45 = (__this->___anim_23);
		NullCheck(L_45);
		Animator_SetBool_m624(L_45, (String_t*) &_stringLiteral300, 0, /*hidden argument*/NULL);
		Animator_t9 * L_46 = (__this->___anim_23);
		NullCheck(L_46);
		Animator_SetBool_m624(L_46, (String_t*) &_stringLiteral301, 0, /*hidden argument*/NULL);
		Animator_t9 * L_47 = (__this->___anim_23);
		NullCheck(L_47);
		Animator_SetBool_m624(L_47, (String_t*) &_stringLiteral302, 1, /*hidden argument*/NULL);
		MonoBehaviour_CancelInvoke_m1758(__this, (String_t*) &_stringLiteral303, /*hidden argument*/NULL);
		SCR_Camie_ToggleWings_m1256(__this, 1, /*hidden argument*/NULL);
		Collider_t138 * L_48 = (__this->___frontColliderChecker_37);
		NullCheck(L_48);
		Collider_set_enabled_m1747(L_48, 0, /*hidden argument*/NULL);
		__this->___isGrounded_5 = 0;
		__this->___isJumping_17 = 1;
		Vector3_t4  L_49 = (__this->___myNormal_8);
		Object_t * L_50 = SCR_Camie_Jump_m1249(__this, L_49, /*hidden argument*/NULL);
		__this->___jumpRoutine_18 = L_50;
		Object_t * L_51 = (__this->___jumpRoutine_18);
		MonoBehaviour_StartCoroutine_m1006(__this, L_51, /*hidden argument*/NULL);
		return;
	}

IL_021b:
	{
		bool L_52 = (__this->___isJumping_17);
		if (!L_52)
		{
			goto IL_0352;
		}
	}
	{
		bool L_53 = (__this->___isFlyTimeCounting_15);
		if (!L_53)
		{
			goto IL_0352;
		}
	}
	{
		float L_54 = (__this->___currFlyTime_14);
		float L_55 = (__this->___maxFlyTime_13);
		if ((!(((float)L_54) < ((float)L_55))))
		{
			goto IL_02dd;
		}
	}
	{
		SCR_FlyButton_t345 * L_56 = (__this->___flyButton_25);
		NullCheck(L_56);
		bool L_57 = SCR_FlyButton_get_IsPressed_m1345(L_56, /*hidden argument*/NULL);
		if (L_57)
		{
			goto IL_0268;
		}
	}
	{
		bool L_58 = Application_get_isEditor_m1759(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_02dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_59 = Input_GetKey_m615(NULL /*static, unused*/, ((int32_t)102), /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_02dd;
		}
	}

IL_0268:
	{
		SCR_Joystick_t346 * L_60 = (__this->___joystick_26);
		NullCheck(L_60);
		bool L_61 = SCR_Joystick_get_IsDragging_m1359(L_60, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_02c6;
		}
	}
	{
		SCR_Joystick_t346 * L_62 = (__this->___joystick_26);
		NullCheck(L_62);
		float L_63 = SCR_Joystick_get_HorizontalAxis_m1360(L_62, /*hidden argument*/NULL);
		SCR_Joystick_t346 * L_64 = (__this->___joystick_26);
		NullCheck(L_64);
		float L_65 = SCR_Joystick_get_VerticalAxis_m1361(L_64, /*hidden argument*/NULL);
		Vector3__ctor_m612((&V_2), L_63, L_65, (0.0f), /*hidden argument*/NULL);
		Transform_t1 * L_66 = (__this->___trans_33);
		Vector3_t4  L_67 = V_2;
		float L_68 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_69 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_67, L_68, /*hidden argument*/NULL);
		float L_70 = (__this->___speedModifier_12);
		Vector3_t4  L_71 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_69, L_70, /*hidden argument*/NULL);
		float L_72 = (__this->___moveSpeed_2);
		Vector3_t4  L_73 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_71, L_72, /*hidden argument*/NULL);
		NullCheck(L_66);
		Transform_Translate_m1760(L_66, L_73, /*hidden argument*/NULL);
	}

IL_02c6:
	{
		float L_74 = (__this->___currFlyTime_14);
		float L_75 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___currFlyTime_14 = ((float)((float)L_74+(float)L_75));
		goto IL_034d;
	}

IL_02dd:
	{
		Animator_t9 * L_76 = (__this->___anim_23);
		NullCheck(L_76);
		Animator_SetBool_m624(L_76, (String_t*) &_stringLiteral302, 0, /*hidden argument*/NULL);
		SCR_SoundManager_t335 * L_77 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_77);
		SCR_SoundManager_PlaySound_m1495(L_77, (String_t*) &_stringLiteral304, /*hidden argument*/NULL);
		Rigidbody_t14 * L_78 = (__this->___rb_21);
		Vector3_t4  L_79 = (__this->___myNormal_8);
		float L_80 = (__this->___gravity_4);
		Vector3_t4  L_81 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_79, ((-L_80)), /*hidden argument*/NULL);
		Rigidbody_t14 * L_82 = (__this->___rb_21);
		NullCheck(L_82);
		float L_83 = Rigidbody_get_mass_m1748(L_82, /*hidden argument*/NULL);
		Vector3_t4  L_84 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_81, L_83, /*hidden argument*/NULL);
		float L_85 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_86 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_84, L_85, /*hidden argument*/NULL);
		NullCheck(L_78);
		Rigidbody_AddForce_m1749(L_78, L_86, /*hidden argument*/NULL);
		__this->___currFlyTime_14 = (0.0f);
		__this->___isJumping_17 = 0;
		__this->___canFly_16 = 0;
	}

IL_034d:
	{
		goto IL_03b7;
	}

IL_0352:
	{
		bool L_87 = (__this->___isJumping_17);
		if (!L_87)
		{
			goto IL_035e;
		}
	}
	{
		return;
	}

IL_035e:
	{
		SCR_FlyButton_t345 * L_88 = (__this->___flyButton_25);
		bool L_89 = Object_op_Equality_m640(NULL /*static, unused*/, L_88, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0384;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_90 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_90);
		SCR_FlyButton_t345 * L_91 = SCR_ManagerComponents_get_FlyButton_m1447(L_90, /*hidden argument*/NULL);
		__this->___flyButton_25 = L_91;
		goto IL_03b7;
	}

IL_0384:
	{
		Rigidbody_t14 * L_92 = (__this->___rb_21);
		float L_93 = (__this->___gravity_4);
		Rigidbody_t14 * L_94 = (__this->___rb_21);
		NullCheck(L_94);
		float L_95 = Rigidbody_get_mass_m1748(L_94, /*hidden argument*/NULL);
		Vector3_t4  L_96 = (__this->___myNormal_8);
		Vector3_t4  L_97 = Vector3_op_Multiply_m598(NULL /*static, unused*/, ((float)((float)((-L_93))*(float)L_95)), L_96, /*hidden argument*/NULL);
		float L_98 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_99 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_97, L_98, /*hidden argument*/NULL);
		NullCheck(L_92);
		Rigidbody_AddForce_m1749(L_92, L_99, /*hidden argument*/NULL);
	}

IL_03b7:
	{
		bool L_100 = (__this->___isJumping_17);
		if (!L_100)
		{
			goto IL_03c3;
		}
	}
	{
		return;
	}

IL_03c3:
	{
		SCR_Joystick_t346 * L_101 = (__this->___joystick_26);
		NullCheck(L_101);
		bool L_102 = SCR_Joystick_get_IsDragging_m1359(L_101, /*hidden argument*/NULL);
		if (!L_102)
		{
			goto IL_03e7;
		}
	}
	{
		SCR_Joystick_t346 * L_103 = (__this->___joystick_26);
		NullCheck(L_103);
		int32_t L_104 = SCR_Joystick_get_PointingTo_m1362(L_103, /*hidden argument*/NULL);
		__this->___goingRight_11 = ((((int32_t)L_104) == ((int32_t)1))? 1 : 0);
	}

IL_03e7:
	{
		Transform_t1 * L_105 = (__this->___trans_33);
		NullCheck(L_105);
		Vector3_t4  L_106 = Transform_get_position_m593(L_105, /*hidden argument*/NULL);
		Vector3_t4  L_107 = (__this->___myNormal_8);
		Vector3_t4  L_108 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_107, /*hidden argument*/NULL);
		Ray__ctor_m900((&V_0), L_106, L_108, /*hidden argument*/NULL);
		Ray_t26  L_109 = V_0;
		bool L_110 = Physics_Raycast_m991(NULL /*static, unused*/, L_109, (&V_1), /*hidden argument*/NULL);
		if (!L_110)
		{
			goto IL_0535;
		}
	}
	{
		float L_111 = RaycastHit_get_distance_m678((&V_1), /*hidden argument*/NULL);
		if ((!(((double)(((double)L_111))) < ((double)(1.6)))))
		{
			goto IL_0535;
		}
	}
	{
		__this->___isGrounded_5 = 1;
		Transform_t1 * L_112 = (__this->___trans_33);
		NullCheck(L_112);
		Vector3_t4  L_113 = Transform_get_position_m593(L_112, /*hidden argument*/NULL);
		Transform_t1 * L_114 = (__this->___trans_33);
		NullCheck(L_114);
		Vector3_t4  L_115 = Transform_get_right_m793(L_114, /*hidden argument*/NULL);
		Vector3_t4  L_116 = Vector3_op_Division_m710(NULL /*static, unused*/, L_115, (2.0f), /*hidden argument*/NULL);
		Vector3_t4  L_117 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_113, L_116, /*hidden argument*/NULL);
		Transform_t1 * L_118 = (__this->___frontRayTarget_35);
		NullCheck(L_118);
		Vector3_t4  L_119 = Transform_get_position_m593(L_118, /*hidden argument*/NULL);
		Vector3_t4  L_120 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_119, /*hidden argument*/NULL);
		Ray__ctor_m900((&V_3), L_117, L_120, /*hidden argument*/NULL);
		Transform_t1 * L_121 = (__this->___trans_33);
		NullCheck(L_121);
		Vector3_t4  L_122 = Transform_get_position_m593(L_121, /*hidden argument*/NULL);
		Transform_t1 * L_123 = (__this->___trans_33);
		NullCheck(L_123);
		Vector3_t4  L_124 = Transform_get_right_m793(L_123, /*hidden argument*/NULL);
		Vector3_t4  L_125 = Vector3_op_Division_m710(NULL /*static, unused*/, L_124, (2.0f), /*hidden argument*/NULL);
		Vector3_t4  L_126 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_122, L_125, /*hidden argument*/NULL);
		Transform_t1 * L_127 = (__this->___backRayTarget_36);
		NullCheck(L_127);
		Vector3_t4  L_128 = Transform_get_position_m593(L_127, /*hidden argument*/NULL);
		Vector3_t4  L_129 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_128, /*hidden argument*/NULL);
		Ray__ctor_m900((&V_4), L_126, L_129, /*hidden argument*/NULL);
		Ray_t26  L_130 = V_3;
		bool L_131 = Physics_Raycast_m991(NULL /*static, unused*/, L_130, (&V_5), /*hidden argument*/NULL);
		if (!L_131)
		{
			goto IL_0523;
		}
	}
	{
		Ray_t26  L_132 = V_4;
		bool L_133 = Physics_Raycast_m991(NULL /*static, unused*/, L_132, (&V_6), /*hidden argument*/NULL);
		if (!L_133)
		{
			goto IL_0523;
		}
	}
	{
		Vector3_t4  L_134 = RaycastHit_get_normal_m902((&V_5), /*hidden argument*/NULL);
		Vector3_t4  L_135 = RaycastHit_get_normal_m902((&V_6), /*hidden argument*/NULL);
		bool L_136 = Vector3_op_Inequality_m722(NULL /*static, unused*/, L_134, L_135, /*hidden argument*/NULL);
		if (!L_136)
		{
			goto IL_0523;
		}
	}
	{
		float L_137 = RaycastHit_get_distance_m678((&V_5), /*hidden argument*/NULL);
		if ((!(((float)L_137) < ((float)(2.0f)))))
		{
			goto IL_0523;
		}
	}
	{
		float L_138 = RaycastHit_get_distance_m678((&V_6), /*hidden argument*/NULL);
		if ((!(((float)L_138) < ((float)(2.0f)))))
		{
			goto IL_0523;
		}
	}
	{
		Vector3_t4  L_139 = RaycastHit_get_normal_m902((&V_5), /*hidden argument*/NULL);
		Vector3_t4  L_140 = RaycastHit_get_normal_m902((&V_6), /*hidden argument*/NULL);
		Vector3_t4  L_141 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_139, L_140, /*hidden argument*/NULL);
		Vector3_t4  L_142 = Vector3_op_Division_m710(NULL /*static, unused*/, L_141, (2.0f), /*hidden argument*/NULL);
		__this->___surfaceNormal_7 = L_142;
		goto IL_0530;
	}

IL_0523:
	{
		Vector3_t4  L_143 = RaycastHit_get_normal_m902((&V_1), /*hidden argument*/NULL);
		__this->___surfaceNormal_7 = L_143;
	}

IL_0530:
	{
		goto IL_0547;
	}

IL_0535:
	{
		__this->___isGrounded_5 = 0;
		Vector3_t4  L_144 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___surfaceNormal_7 = L_144;
	}

IL_0547:
	{
		__this->___canFly_16 = 1;
		__this->___isJumping_17 = 0;
		SCR_Camie_GetDirection_m1250(__this, /*hidden argument*/NULL);
		Vector3_t4  L_145 = (__this->___surfaceNormal_7);
		__this->___myNormal_8 = L_145;
		SCR_Joystick_t346 * L_146 = (__this->___joystick_26);
		bool L_147 = Object_op_Inequality_m623(NULL /*static, unused*/, L_146, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_147)
		{
			goto IL_05ff;
		}
	}
	{
		SCR_Joystick_t346 * L_148 = (__this->___joystick_26);
		NullCheck(L_148);
		bool L_149 = SCR_Joystick_get_IsDragging_m1359(L_148, /*hidden argument*/NULL);
		if (!L_149)
		{
			goto IL_05ff;
		}
	}
	{
		Transform_t1 * L_150 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		SCR_Joystick_t346 * L_151 = (__this->___joystick_26);
		NullCheck(L_151);
		float L_152 = SCR_Joystick_get_HorizontalAxis_m1360(L_151, /*hidden argument*/NULL);
		SCR_Joystick_t346 * L_153 = (__this->___joystick_26);
		NullCheck(L_153);
		float L_154 = SCR_Joystick_get_HorizontalAxis_m1360(L_153, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_155 = Mathf_Sign_m599(NULL /*static, unused*/, L_154, /*hidden argument*/NULL);
		float L_156 = (__this->___moveSpeed_2);
		float L_157 = (__this->___speedModifier_12);
		float L_158 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_150);
		Transform_Translate_m1761(L_150, ((float)((float)((float)((float)((float)((float)((float)((float)L_152*(float)L_155))*(float)L_156))*(float)L_157))*(float)L_158)), (0.0f), (0.0f), /*hidden argument*/NULL);
		Animator_t9 * L_159 = (__this->___anim_23);
		NullCheck(L_159);
		Animator_SetBool_m624(L_159, (String_t*) &_stringLiteral300, 1, /*hidden argument*/NULL);
		Animator_t9 * L_160 = (__this->___anim_23);
		NullCheck(L_160);
		Animator_SetBool_m624(L_160, (String_t*) &_stringLiteral301, 0, /*hidden argument*/NULL);
		MonoBehaviour_CancelInvoke_m1758(__this, (String_t*) &_stringLiteral303, /*hidden argument*/NULL);
		goto IL_0621;
	}

IL_05ff:
	{
		Animator_t9 * L_161 = (__this->___anim_23);
		NullCheck(L_161);
		Animator_SetBool_m624(L_161, (String_t*) &_stringLiteral300, 0, /*hidden argument*/NULL);
		Animator_t9 * L_162 = (__this->___anim_23);
		NullCheck(L_162);
		Animator_SetBool_m624(L_162, (String_t*) &_stringLiteral301, 1, /*hidden argument*/NULL);
	}

IL_0621:
	{
		Rigidbody_t14 * L_163 = (__this->___rb_21);
		float L_164 = (__this->___gravity_4);
		Rigidbody_t14 * L_165 = (__this->___rb_21);
		NullCheck(L_165);
		float L_166 = Rigidbody_get_mass_m1748(L_165, /*hidden argument*/NULL);
		Vector3_t4  L_167 = (__this->___myNormal_8);
		Vector3_t4  L_168 = Vector3_op_Multiply_m598(NULL /*static, unused*/, ((float)((float)((-L_164))*(float)L_166)), L_167, /*hidden argument*/NULL);
		float L_169 = (__this->___speedModifier_12);
		Vector3_t4  L_170 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_168, L_169, /*hidden argument*/NULL);
		float L_171 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_172 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_170, L_171, /*hidden argument*/NULL);
		NullCheck(L_163);
		Rigidbody_AddForce_m1749(L_163, L_172, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Camie::OnCollisionEnter(UnityEngine.Collision)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void SCR_Camie_OnCollisionEnter_m1245 (SCR_Camie_t338 * __this, Collision_t146 * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = SCR_Camie_get_IsFlying_m1261(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_005f;
		}
	}
	{
		bool L_1 = (__this->___isFlyLerping_29);
		if (L_1)
		{
			goto IL_005f;
		}
	}
	{
		Collision_t146 * L_2 = ___info;
		NullCheck(L_2);
		Transform_t1 * L_3 = Collision_get_transform_m1762(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = Component_get_tag_m635(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m636(NULL /*static, unused*/, L_4, (String_t*) &_stringLiteral305, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005f;
		}
	}
	{
		Collision_t146 * L_6 = ___info;
		NullCheck(L_6);
		ContactPointU5BU5D_t245* L_7 = Collision_get_contacts_m912(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		Vector3_t4  L_8 = ContactPoint_get_point_m916(((ContactPoint_t246 *)(ContactPoint_t246 *)SZArrayLdElema(L_7, 0)), /*hidden argument*/NULL);
		Collision_t146 * L_9 = ___info;
		NullCheck(L_9);
		ContactPointU5BU5D_t245* L_10 = Collision_get_contacts_m912(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		Vector3_t4  L_11 = ContactPoint_get_normal_m914(((ContactPoint_t246 *)(ContactPoint_t246 *)SZArrayLdElema(L_10, 0)), /*hidden argument*/NULL);
		Object_t * L_12 = SCR_Camie_LandOnCollisionPoint_m1248(__this, L_8, L_11, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1006(__this, L_12, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Collections.IEnumerator SCR_Camie::GoToFrontCollisionPoint(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_Camie_GoToFrontCollisionPoint_m1246 (SCR_Camie_t338 * __this, Vector3_t4  ___point, Vector3_t4  ___normal, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(676);
		s_Il2CppMethodIntialized = true;
	}
	U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * V_0 = {0};
	{
		U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * L_0 = (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 *)il2cpp_codegen_object_new (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_il2cpp_TypeInfo_var);
		U3CGoToFrontCollisionPointU3Ec__Iterator4__ctor_m1224(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * L_1 = V_0;
		Vector3_t4  L_2 = ___normal;
		NullCheck(L_1);
		L_1->___normal_0 = L_2;
		U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * L_3 = V_0;
		Vector3_t4  L_4 = ___point;
		NullCheck(L_3);
		L_3->___point_1 = L_4;
		U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * L_5 = V_0;
		Vector3_t4  L_6 = ___normal;
		NullCheck(L_5);
		L_5->___U3CU24U3Enormal_4 = L_6;
		U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * L_7 = V_0;
		Vector3_t4  L_8 = ___point;
		NullCheck(L_7);
		L_7->___U3CU24U3Epoint_5 = L_8;
		U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_6 = __this;
		U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * L_10 = V_0;
		return L_10;
	}
}
// System.Void SCR_Camie::ResetActiveCollisionDetector()
extern "C" void SCR_Camie_ResetActiveCollisionDetector_m1247 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		Collider_t138 * L_0 = (__this->___frontColliderChecker_37);
		NullCheck(L_0);
		Collider_set_enabled_m1747(L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator SCR_Camie::LandOnCollisionPoint(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_Camie_LandOnCollisionPoint_m1248 (SCR_Camie_t338 * __this, Vector3_t4  ___point, Vector3_t4  ___normal, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(677);
		s_Il2CppMethodIntialized = true;
	}
	U3CLandOnCollisionPointU3Ec__Iterator5_t340 * V_0 = {0};
	{
		U3CLandOnCollisionPointU3Ec__Iterator5_t340 * L_0 = (U3CLandOnCollisionPointU3Ec__Iterator5_t340 *)il2cpp_codegen_object_new (U3CLandOnCollisionPointU3Ec__Iterator5_t340_il2cpp_TypeInfo_var);
		U3CLandOnCollisionPointU3Ec__Iterator5__ctor_m1230(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLandOnCollisionPointU3Ec__Iterator5_t340 * L_1 = V_0;
		Vector3_t4  L_2 = ___point;
		NullCheck(L_1);
		L_1->___point_0 = L_2;
		U3CLandOnCollisionPointU3Ec__Iterator5_t340 * L_3 = V_0;
		Vector3_t4  L_4 = ___normal;
		NullCheck(L_3);
		L_3->___normal_1 = L_4;
		U3CLandOnCollisionPointU3Ec__Iterator5_t340 * L_5 = V_0;
		Vector3_t4  L_6 = ___point;
		NullCheck(L_5);
		L_5->___U3CU24U3Epoint_4 = L_6;
		U3CLandOnCollisionPointU3Ec__Iterator5_t340 * L_7 = V_0;
		Vector3_t4  L_8 = ___normal;
		NullCheck(L_7);
		L_7->___U3CU24U3Enormal_5 = L_8;
		U3CLandOnCollisionPointU3Ec__Iterator5_t340 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_6 = __this;
		U3CLandOnCollisionPointU3Ec__Iterator5_t340 * L_10 = V_0;
		return L_10;
	}
}
// System.Collections.IEnumerator SCR_Camie::Jump(UnityEngine.Vector3)
extern TypeInfo* U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_Camie_Jump_m1249 (SCR_Camie_t338 * __this, Vector3_t4  ___normal, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(678);
		s_Il2CppMethodIntialized = true;
	}
	U3CJumpU3Ec__Iterator6_t341 * V_0 = {0};
	{
		U3CJumpU3Ec__Iterator6_t341 * L_0 = (U3CJumpU3Ec__Iterator6_t341 *)il2cpp_codegen_object_new (U3CJumpU3Ec__Iterator6_t341_il2cpp_TypeInfo_var);
		U3CJumpU3Ec__Iterator6__ctor_m1236(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CJumpU3Ec__Iterator6_t341 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CJumpU3Ec__Iterator6_t341 * L_2 = V_0;
		return L_2;
	}
}
// System.Void SCR_Camie::GetDirection()
extern "C" void SCR_Camie_GetDirection_m1250 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector3_t4  V_1 = {0};
	int32_t V_2 = 0;
	Quaternion_t19  V_3 = {0};
	Quaternion_t19  G_B8_0 = {0};
	{
		V_0 = 0;
		SCR_CamieDirections_t348 * L_0 = (__this->___directionChecker_34);
		NullCheck(L_0);
		Vector3U5BU5D_t210* L_1 = SCR_CamieDirections_get_Normals_m1576(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		V_1 = (*(Vector3_t4 *)((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_1, 0)));
		V_2 = 1;
		goto IL_006f;
	}

IL_0020:
	{
		SCR_CamieDirections_t348 * L_2 = (__this->___directionChecker_34);
		NullCheck(L_2);
		Vector3U5BU5D_t210* L_3 = SCR_CamieDirections_get_Normals_m1576(L_2, /*hidden argument*/NULL);
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		Vector3_t4  L_5 = (__this->___surfaceNormal_7);
		float L_6 = Vector3_Distance_m1750(NULL /*static, unused*/, (*(Vector3_t4 *)((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_3, L_4))), L_5, /*hidden argument*/NULL);
		Vector3_t4  L_7 = V_1;
		Vector3_t4  L_8 = (__this->___surfaceNormal_7);
		float L_9 = Vector3_Distance_m1750(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if ((!(((float)L_6) < ((float)L_9))))
		{
			goto IL_006b;
		}
	}
	{
		SCR_CamieDirections_t348 * L_10 = (__this->___directionChecker_34);
		NullCheck(L_10);
		Vector3U5BU5D_t210* L_11 = SCR_CamieDirections_get_Normals_m1576(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		V_1 = (*(Vector3_t4 *)((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_11, L_12)));
		int32_t L_13 = V_2;
		V_0 = L_13;
	}

IL_006b:
	{
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_006f:
	{
		int32_t L_15 = V_2;
		SCR_CamieDirections_t348 * L_16 = (__this->___directionChecker_34);
		NullCheck(L_16);
		Vector3U5BU5D_t210* L_17 = SCR_CamieDirections_get_Normals_m1576(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)(((Array_t *)L_17)->max_length))))))
		{
			goto IL_0020;
		}
	}
	{
		Vector3_t4  L_18 = V_1;
		__this->___surfaceNormal_7 = L_18;
		bool L_19 = (__this->___goingRight_11);
		if (!L_19)
		{
			goto IL_00af;
		}
	}
	{
		SCR_CamieDirections_t348 * L_20 = (__this->___directionChecker_34);
		NullCheck(L_20);
		QuaternionU5BU5D_t400* L_21 = SCR_CamieDirections_get_Angles_m1575(L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_0;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		G_B8_0 = (*(Quaternion_t19 *)((Quaternion_t19 *)(Quaternion_t19 *)SZArrayLdElema(L_21, L_22)));
		goto IL_00d9;
	}

IL_00af:
	{
		SCR_CamieDirections_t348 * L_23 = (__this->___directionChecker_34);
		NullCheck(L_23);
		QuaternionU5BU5D_t400* L_24 = SCR_CamieDirections_get_Angles_m1575(L_23, /*hidden argument*/NULL);
		int32_t L_25 = V_0;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		Vector3_t4  L_26 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t19  L_27 = Quaternion_AngleAxis_m892(NULL /*static, unused*/, (180.0f), L_26, /*hidden argument*/NULL);
		Quaternion_t19  L_28 = Quaternion_op_Multiply_m672(NULL /*static, unused*/, (*(Quaternion_t19 *)((Quaternion_t19 *)(Quaternion_t19 *)SZArrayLdElema(L_24, L_25))), L_27, /*hidden argument*/NULL);
		G_B8_0 = L_28;
	}

IL_00d9:
	{
		V_3 = G_B8_0;
		Transform_t1 * L_29 = (__this->___trans_33);
		Transform_t1 * L_30 = (__this->___trans_33);
		NullCheck(L_30);
		Quaternion_t19  L_31 = Transform_get_rotation_m656(L_30, /*hidden argument*/NULL);
		Quaternion_t19  L_32 = V_3;
		float L_33 = (__this->___lerpSpeed_3);
		float L_34 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t19  L_35 = Quaternion_Lerp_m657(NULL /*static, unused*/, L_31, L_32, ((float)((float)((float)((float)L_33*(float)(5.0f)))*(float)L_34)), /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_rotation_m658(L_29, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Camie::DecreaseSpeed(System.Single)
extern "C" void SCR_Camie_DecreaseSpeed_m1251 (SCR_Camie_t338 * __this, float ___toPercent, const MethodInfo* method)
{
	{
		float L_0 = (__this->___speedModifier_12);
		float L_1 = ___toPercent;
		__this->___speedModifier_12 = ((float)((float)L_0/(float)L_1));
		__this->___isSlowed_32 = 1;
		return;
	}
}
// System.Void SCR_Camie::NormalSpeed()
extern "C" void SCR_Camie_NormalSpeed_m1252 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		__this->___speedModifier_12 = (1.0f);
		__this->___isSlowed_32 = 0;
		return;
	}
}
// System.Void SCR_Camie::Respawn()
extern "C" void SCR_Camie_Respawn_m1253 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		Animator_t9 * L_0 = (__this->___anim_23);
		NullCheck(L_0);
		Animator_SetBool_m624(L_0, (String_t*) &_stringLiteral302, 0, /*hidden argument*/NULL);
		Animator_t9 * L_1 = (__this->___anim_23);
		NullCheck(L_1);
		Animator_SetBool_m624(L_1, (String_t*) &_stringLiteral300, 0, /*hidden argument*/NULL);
		__this->___isJumping_17 = 0;
		__this->___currFlyTime_14 = (0.0f);
		__this->___isFlyLerping_29 = 0;
		__this->___isLerping_28 = 0;
		Vector3_t4  L_2 = Vector3_get_up_m642(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___surfaceNormal_7 = L_2;
		SCR_Camie_GetDirection_m1250(__this, /*hidden argument*/NULL);
		SCR_FlyButton_t345 * L_3 = (__this->___flyButton_25);
		NullCheck(L_3);
		SCR_FlyButton_set_IsPressed_m1346(L_3, 0, /*hidden argument*/NULL);
		SCR_Respawn_t344 * L_4 = (__this->___respawner_24);
		NullCheck(L_4);
		SCR_Respawn_StartRespawn_m1281(L_4, /*hidden argument*/NULL);
		__this->___canFly_16 = 0;
		SphereCollider_t136 * L_5 = (__this->___flyCollider_38);
		NullCheck(L_5);
		Collider_set_enabled_m1747(L_5, 0, /*hidden argument*/NULL);
		BoxCollider_t343 * L_6 = (__this->___myColl_22);
		NullCheck(L_6);
		Collider_set_enabled_m1747(L_6, 1, /*hidden argument*/NULL);
		Collider_t138 * L_7 = (__this->___frontColliderChecker_37);
		NullCheck(L_7);
		Collider_set_enabled_m1747(L_7, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Camie::BreakIdle()
extern "C" void SCR_Camie_BreakIdle_m1254 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Animator_t9 * L_0 = (__this->___anim_23);
		NullCheck(L_0);
		bool L_1 = Animator_GetBool_m627(L_0, (String_t*) &_stringLiteral306, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002a;
		}
	}
	{
		Animator_t9 * L_2 = (__this->___anim_23);
		NullCheck(L_2);
		bool L_3 = Animator_GetBool_m627(L_2, (String_t*) &_stringLiteral307, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0051;
		}
	}

IL_002a:
	{
		Animator_t9 * L_4 = (__this->___anim_23);
		NullCheck(L_4);
		Animator_SetBool_m624(L_4, (String_t*) &_stringLiteral306, 0, /*hidden argument*/NULL);
		Animator_t9 * L_5 = (__this->___anim_23);
		NullCheck(L_5);
		Animator_SetBool_m624(L_5, (String_t*) &_stringLiteral307, 0, /*hidden argument*/NULL);
		goto IL_00b4;
	}

IL_0051:
	{
		int32_t L_6 = Random_Range_m896(NULL /*static, unused*/, 0, 3, /*hidden argument*/NULL);
		V_0 = L_6;
		int32_t L_7 = V_0;
		V_1 = L_7;
		int32_t L_8 = V_1;
		if (L_8 == 0)
		{
			goto IL_0072;
		}
		if (L_8 == 1)
		{
			goto IL_0088;
		}
		if (L_8 == 2)
		{
			goto IL_009e;
		}
	}
	{
		goto IL_00b4;
	}

IL_0072:
	{
		Animator_t9 * L_9 = (__this->___anim_23);
		NullCheck(L_9);
		Animator_SetBool_m624(L_9, (String_t*) &_stringLiteral306, 1, /*hidden argument*/NULL);
		goto IL_00b4;
	}

IL_0088:
	{
		Animator_t9 * L_10 = (__this->___anim_23);
		NullCheck(L_10);
		Animator_SetBool_m624(L_10, (String_t*) &_stringLiteral307, 1, /*hidden argument*/NULL);
		goto IL_00b4;
	}

IL_009e:
	{
		Animator_t9 * L_11 = (__this->___anim_23);
		NullCheck(L_11);
		Animator_SetBool_m624(L_11, (String_t*) &_stringLiteral308, 0, /*hidden argument*/NULL);
		goto IL_00b4;
	}

IL_00b4:
	{
		return;
	}
}
// System.Void SCR_Camie::ResetIdle()
extern "C" void SCR_Camie_ResetIdle_m1255 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		Animator_t9 * L_0 = (__this->___anim_23);
		NullCheck(L_0);
		Animator_SetBool_m624(L_0, (String_t*) &_stringLiteral306, 0, /*hidden argument*/NULL);
		Animator_t9 * L_1 = (__this->___anim_23);
		NullCheck(L_1);
		Animator_SetBool_m624(L_1, (String_t*) &_stringLiteral307, 0, /*hidden argument*/NULL);
		Animator_t9 * L_2 = (__this->___anim_23);
		NullCheck(L_2);
		Animator_SetBool_m624(L_2, (String_t*) &_stringLiteral308, 0, /*hidden argument*/NULL);
		bool L_3 = MonoBehaviour_IsInvoking_m1763(__this, (String_t*) &_stringLiteral303, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0053;
		}
	}
	{
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral303, (1.5f), /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void SCR_Camie::ToggleWings(System.Boolean)
extern "C" void SCR_Camie_ToggleWings_m1256 (SCR_Camie_t338 * __this, bool ___show, const MethodInfo* method)
{
	SCR_Wings_t351 * V_0 = {0};
	SCR_WingsU5BU5D_t342* V_1 = {0};
	int32_t V_2 = 0;
	{
		SCR_WingsU5BU5D_t342* L_0 = (__this->___wings_19);
		V_1 = L_0;
		V_2 = 0;
		goto IL_001d;
	}

IL_000e:
	{
		SCR_WingsU5BU5D_t342* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(SCR_Wings_t351 **)(SCR_Wings_t351 **)SZArrayLdElema(L_1, L_3));
		SCR_Wings_t351 * L_4 = V_0;
		bool L_5 = ___show;
		NullCheck(L_4);
		SCR_Wings_SetVisible_m1287(L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_7 = V_2;
		SCR_WingsU5BU5D_t342* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)(((Array_t *)L_8)->max_length))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void SCR_Camie::InBubble(SCR_Goutte)
extern "C" void SCR_Camie_InBubble_m1257 (SCR_Camie_t338 * __this, SCR_Goutte_t347 * ___goutte, const MethodInfo* method)
{
	{
		__this->___isSlowed_32 = 1;
		Animator_t9 * L_0 = (__this->___anim_23);
		NullCheck(L_0);
		Animator_SetBool_m624(L_0, (String_t*) &_stringLiteral309, 1, /*hidden argument*/NULL);
		SCR_Goutte_t347 * L_1 = ___goutte;
		__this->___currentGoutte_31 = L_1;
		return;
	}
}
// System.Void SCR_Camie::EndBubble()
extern "C" void SCR_Camie_EndBubble_m1258 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		SCR_Camie_NormalSpeed_m1252(__this, /*hidden argument*/NULL);
		Animator_t9 * L_0 = (__this->___anim_23);
		NullCheck(L_0);
		Animator_SetBool_m624(L_0, (String_t*) &_stringLiteral309, 0, /*hidden argument*/NULL);
		SCR_Goutte_t347 * L_1 = (__this->___currentGoutte_31);
		NullCheck(L_1);
		GameObject_t78 * L_2 = Component_get_gameObject_m622(L_1, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Camie::LosePucerons()
extern "C" void SCR_Camie_LosePucerons_m1259 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		Animator_t9 * L_0 = (__this->___puceronsPerdus_30);
		NullCheck(L_0);
		Animator_SetTrigger_m1764(L_0, (String_t*) &_stringLiteral310, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SCR_Camie::get_IsGrounded()
extern "C" bool SCR_Camie_get_IsGrounded_m1260 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isGrounded_5);
		return L_0;
	}
}
// System.Boolean SCR_Camie::get_IsFlying()
extern "C" bool SCR_Camie_get_IsFlying_m1261 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (__this->___isJumping_17);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		bool L_1 = (__this->___isFlyTimeCounting_15);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 0;
	}

IL_0014:
	{
		return G_B3_0;
	}
}
// System.Boolean SCR_Camie::get_CanFly()
extern "C" bool SCR_Camie_get_CanFly_m1262 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___canFly_16);
		return L_0;
	}
}
// System.Void SCR_Camie::set_CanFly(System.Boolean)
extern "C" void SCR_Camie_set_CanFly_m1263 (SCR_Camie_t338 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___canFly_16 = L_0;
		return;
	}
}
// System.Boolean SCR_Camie::get_IsSlowed()
extern "C" bool SCR_Camie_get_IsSlowed_m1264 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isSlowed_32);
		return L_0;
	}
}
// UnityEngine.Animator SCR_Camie::get_MyAnimations()
extern "C" Animator_t9 * SCR_Camie_get_MyAnimations_m1265 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		Animator_t9 * L_0 = (__this->___anim_23);
		return L_0;
	}
}
// UnityEngine.Vector3 SCR_Camie::get_ColliderCenter()
extern "C" Vector3_t4  SCR_Camie_get_ColliderCenter_m1266 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	Bounds_t225  V_0 = {0};
	{
		BoxCollider_t343 * L_0 = (__this->___myColl_22);
		NullCheck(L_0);
		Bounds_t225  L_1 = Collider_get_bounds_m1765(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t4  L_2 = Bounds_get_center_m1766((&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// SCR_CamieDirections SCR_Camie::get_Director()
extern "C" SCR_CamieDirections_t348 * SCR_Camie_get_Director_m1267 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		SCR_CamieDirections_t348 * L_0 = (__this->___directionChecker_34);
		return L_0;
	}
}
// System.Void SCR_Camie::set_Director(SCR_CamieDirections)
extern "C" void SCR_Camie_set_Director_m1268 (SCR_Camie_t338 * __this, SCR_CamieDirections_t348 * ___value, const MethodInfo* method)
{
	{
		SCR_CamieDirections_t348 * L_0 = ___value;
		__this->___directionChecker_34 = L_0;
		return;
	}
}
// SCR_Respawn SCR_Camie::get_Respawner()
extern "C" SCR_Respawn_t344 * SCR_Camie_get_Respawner_m1269 (SCR_Camie_t338 * __this, const MethodInfo* method)
{
	{
		SCR_Respawn_t344 * L_0 = (__this->___respawner_24);
		return L_0;
	}
}
// SCR_CollisionChecker
#include "AssemblyU2DCSharp_SCR_CollisionChecker.h"
#ifndef _MSC_VER
#else
#endif
// SCR_CollisionChecker
#include "AssemblyU2DCSharp_SCR_CollisionCheckerMethodDeclarations.h"



// System.Void SCR_CollisionChecker::.ctor()
extern "C" void SCR_CollisionChecker__ctor_m1270 (SCR_CollisionChecker_t349 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_CollisionChecker::Start()
extern "C" void SCR_CollisionChecker_Start_m1271 (SCR_CollisionChecker_t349 * __this, const MethodInfo* method)
{
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Camie_t338 * L_1 = SCR_ManagerComponents_get_AvatarMove_m1441(L_0, /*hidden argument*/NULL);
		__this->___avatar_2 = L_1;
		SCR_ManagerComponents_t380 * L_2 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_Joystick_t346 * L_3 = SCR_ManagerComponents_get_Joystick_m1445(L_2, /*hidden argument*/NULL);
		__this->___joystick_3 = L_3;
		SCR_Camie_t338 * L_4 = (__this->___avatar_2);
		NullCheck(L_4);
		Transform_t1 * L_5 = Component_get_transform_m594(L_4, /*hidden argument*/NULL);
		__this->___avatarTransform_4 = L_5;
		Transform_t1 * L_6 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		__this->___myTransform_5 = L_6;
		return;
	}
}
// System.Void SCR_CollisionChecker::Update()
extern "C" void SCR_CollisionChecker_Update_m1272 (SCR_CollisionChecker_t349 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = (__this->___myTransform_5);
		Transform_t1 * L_1 = (__this->___avatarTransform_4);
		NullCheck(L_1);
		Vector3_t4  L_2 = Transform_get_position_m593(L_1, /*hidden argument*/NULL);
		Transform_t1 * L_3 = (__this->___avatarTransform_4);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_right_m793(L_3, /*hidden argument*/NULL);
		Vector3_t4  L_5 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_4, (0.75f), /*hidden argument*/NULL);
		Vector3_t4  L_6 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		Transform_t1 * L_7 = (__this->___avatarTransform_4);
		NullCheck(L_7);
		Vector3_t4  L_8 = Transform_get_up_m644(L_7, /*hidden argument*/NULL);
		Vector3_t4  L_9 = Vector3_op_Division_m710(NULL /*static, unused*/, L_8, (2.0f), /*hidden argument*/NULL);
		Vector3_t4  L_10 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m607(L_0, L_10, /*hidden argument*/NULL);
		Transform_t1 * L_11 = (__this->___myTransform_5);
		Transform_t1 * L_12 = (__this->___avatarTransform_4);
		NullCheck(L_12);
		Quaternion_t19  L_13 = Transform_get_rotation_m656(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_rotation_m658(L_11, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_CollisionChecker::OnCollisionEnter(UnityEngine.Collision)
extern "C" void SCR_CollisionChecker_OnCollisionEnter_m1273 (SCR_CollisionChecker_t349 * __this, Collision_t146 * ___info, const MethodInfo* method)
{
	{
		SCR_Camie_t338 * L_0 = (__this->___avatar_2);
		SCR_Camie_t338 * L_1 = (__this->___avatar_2);
		Collision_t146 * L_2 = ___info;
		NullCheck(L_2);
		ContactPointU5BU5D_t245* L_3 = Collision_get_contacts_m912(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		Vector3_t4  L_4 = ContactPoint_get_point_m916(((ContactPoint_t246 *)(ContactPoint_t246 *)SZArrayLdElema(L_3, 0)), /*hidden argument*/NULL);
		Collision_t146 * L_5 = ___info;
		NullCheck(L_5);
		ContactPointU5BU5D_t245* L_6 = Collision_get_contacts_m912(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		Vector3_t4  L_7 = ContactPoint_get_normal_m914(((ContactPoint_t246 *)(ContactPoint_t246 *)SZArrayLdElema(L_6, 0)), /*hidden argument*/NULL);
		NullCheck(L_1);
		Object_t * L_8 = SCR_Camie_GoToFrontCollisionPoint_m1246(L_1, L_4, L_7, /*hidden argument*/NULL);
		NullCheck(L_0);
		MonoBehaviour_StartCoroutine_m1006(L_0, L_8, /*hidden argument*/NULL);
		return;
	}
}
// SCR_ResetIdle
#include "AssemblyU2DCSharp_SCR_ResetIdle.h"
#ifndef _MSC_VER
#else
#endif
// SCR_ResetIdle
#include "AssemblyU2DCSharp_SCR_ResetIdleMethodDeclarations.h"



// System.Void SCR_ResetIdle::.ctor()
extern "C" void SCR_ResetIdle__ctor_m1274 (SCR_ResetIdle_t350 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_ResetIdle::Start()
extern "C" void SCR_ResetIdle_Start_m1275 (SCR_ResetIdle_t350 * __this, const MethodInfo* method)
{
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Camie_t338 * L_1 = SCR_ManagerComponents_get_AvatarMove_m1441(L_0, /*hidden argument*/NULL);
		__this->___camie_2 = L_1;
		return;
	}
}
// System.Void SCR_ResetIdle::ResetIdle()
extern "C" void SCR_ResetIdle_ResetIdle_m1276 (SCR_ResetIdle_t350 * __this, const MethodInfo* method)
{
	{
		SCR_Camie_t338 * L_0 = (__this->___camie_2);
		NullCheck(L_0);
		SCR_Camie_ResetIdle_m1255(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_ResetIdle::Land()
extern "C" void SCR_ResetIdle_Land_m1277 (SCR_ResetIdle_t350 * __this, const MethodInfo* method)
{
	{
		SCR_Camie_t338 * L_0 = (__this->___camie_2);
		NullCheck(L_0);
		SCR_Camie_ToggleWings_m1256(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_ResetIdle::OutOfBubble()
extern "C" void SCR_ResetIdle_OutOfBubble_m1278 (SCR_ResetIdle_t350 * __this, const MethodInfo* method)
{
	{
		SCR_Camie_t338 * L_0 = (__this->___camie_2);
		NullCheck(L_0);
		SCR_Camie_EndBubble_m1258(L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void SCR_Respawn::.ctor()
extern "C" void SCR_Respawn__ctor_m1279 (SCR_Respawn_t344 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Respawn::Start()
extern "C" void SCR_Respawn_Start_m1280 (SCR_Respawn_t344 * __this, const MethodInfo* method)
{
	{
		__this->___isRespawning_5 = 0;
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Camie_t338 * L_1 = SCR_ManagerComponents_get_AvatarMove_m1441(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1 * L_2 = Component_get_transform_m594(L_1, /*hidden argument*/NULL);
		__this->___avatarTransform_2 = L_2;
		Transform_t1 * L_3 = (__this->___avatarTransform_2);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		__this->___avatarStartPosition_3 = L_4;
		Transform_t1 * L_5 = (__this->___avatarTransform_2);
		NullCheck(L_5);
		Quaternion_t19  L_6 = Transform_get_rotation_m656(L_5, /*hidden argument*/NULL);
		__this->___avatarStartRotation_4 = L_6;
		return;
	}
}
// System.Void SCR_Respawn::StartRespawn()
extern "C" void SCR_Respawn_StartRespawn_m1281 (SCR_Respawn_t344 * __this, const MethodInfo* method)
{
	{
		__this->___isRespawning_5 = 1;
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral314, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Respawn::EndRespawn()
extern "C" void SCR_Respawn_EndRespawn_m1282 (SCR_Respawn_t344 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = (__this->___avatarTransform_2);
		Vector3_t4  L_1 = (__this->___avatarStartPosition_3);
		NullCheck(L_0);
		Transform_set_position_m607(L_0, L_1, /*hidden argument*/NULL);
		Transform_t1 * L_2 = (__this->___avatarTransform_2);
		Quaternion_t19  L_3 = (__this->___avatarStartRotation_4);
		NullCheck(L_2);
		Transform_set_rotation_m658(L_2, L_3, /*hidden argument*/NULL);
		__this->___isRespawning_5 = 0;
		SCR_ManagerComponents_t380 * L_4 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		SCR_Camie_t338 * L_5 = SCR_ManagerComponents_get_AvatarMove_m1441(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		SCR_Camie_set_CanFly_m1263(L_5, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SCR_Respawn::get_IsRespawning()
extern "C" bool SCR_Respawn_get_IsRespawning_m1283 (SCR_Respawn_t344 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isRespawning_5);
		return L_0;
	}
}
// System.Void SCR_Respawn::set_IsRespawning(System.Boolean)
extern "C" void SCR_Respawn_set_IsRespawning_m1284 (SCR_Respawn_t344 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___isRespawning_5 = L_0;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
struct Component_t219;
struct Renderer_t154;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t154_m1767(__this, method) (( Renderer_t154 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void SCR_Wings::.ctor()
extern "C" void SCR_Wings__ctor_m1285 (SCR_Wings_t351 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Wings::Awake()
extern const MethodInfo* Component_GetComponent_TisRenderer_t154_m1767_MethodInfo_var;
extern "C" void SCR_Wings_Awake_m1286 (SCR_Wings_t351 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t154_m1767_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483714);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t154 * L_0 = Component_GetComponent_TisRenderer_t154_m1767(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t154_m1767_MethodInfo_var);
		__this->___rend_2 = L_0;
		return;
	}
}
// System.Void SCR_Wings::SetVisible(System.Boolean)
extern "C" void SCR_Wings_SetVisible_m1287 (SCR_Wings_t351 * __this, bool ___isVisible, const MethodInfo* method)
{
	{
		Renderer_t154 * L_0 = (__this->___rend_2);
		bool L_1 = ___isVisible;
		NullCheck(L_0);
		Renderer_set_enabled_m927(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// SCR_FollowAvatar/<ZoomInOn>c__Iterator7
#include "AssemblyU2DCSharp_SCR_FollowAvatar_U3CZoomInOnU3Ec__Iterator.h"
#ifndef _MSC_VER
#else
#endif
// SCR_FollowAvatar/<ZoomInOn>c__Iterator7
#include "AssemblyU2DCSharp_SCR_FollowAvatar_U3CZoomInOnU3Ec__IteratorMethodDeclarations.h"

// SCR_FollowAvatar
#include "AssemblyU2DCSharp_SCR_FollowAvatar.h"
// SCR_PointManager
#include "AssemblyU2DCSharp_SCR_PointManager.h"
// SCR_FollowAvatar
#include "AssemblyU2DCSharp_SCR_FollowAvatarMethodDeclarations.h"
// SCR_PointManager
#include "AssemblyU2DCSharp_SCR_PointManagerMethodDeclarations.h"


// System.Void SCR_FollowAvatar/<ZoomInOn>c__Iterator7::.ctor()
extern "C" void U3CZoomInOnU3Ec__Iterator7__ctor_m1288 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_FollowAvatar/<ZoomInOn>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CZoomInOnU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1289 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Object SCR_FollowAvatar/<ZoomInOn>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CZoomInOnU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1290 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Boolean SCR_FollowAvatar/<ZoomInOn>c__Iterator7::MoveNext()
extern TypeInfo* Vector3_t4_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CZoomInOnU3Ec__Iterator7_MoveNext_m1291 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(62);
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4  V_1 = {0};
	Vector3_t4  V_2 = {0};
	Vector3_t4  V_3 = {0};
	Vector3_t4  V_4 = {0};
	bool V_5 = false;
	{
		int32_t L_0 = (__this->___U24PC_4);
		V_0 = L_0;
		__this->___U24PC_4 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0144;
		}
		if (L_1 == 2)
		{
			goto IL_018d;
		}
	}
	{
		goto IL_0194;
	}

IL_0025:
	{
		__this->___U3CtimeU3E__0_0 = (0.0f);
		Initobj (Vector3_t4_il2cpp_TypeInfo_var, (&V_1));
		Vector3_t4  L_2 = V_1;
		__this->___U3CtoVectorU3E__1_1 = L_2;
		__this->___U3CzoomInU3E__2_2 = 1;
		goto IL_0144;
	}

IL_004b:
	{
		Transform_t1 * L_3 = (__this->___zoomTo_3);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = ((&V_2)->___x_1);
		Transform_t1 * L_6 = (__this->___zoomTo_3);
		NullCheck(L_6);
		Vector3_t4  L_7 = Transform_get_position_m593(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		float L_8 = ((&V_3)->___y_2);
		SCR_FollowAvatar_t352 * L_9 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_9);
		float L_10 = (L_9->___plusHautQueCamie_5);
		Vector3_t4  L_11 = {0};
		Vector3__ctor_m612(&L_11, L_5, ((float)((float)L_8+(float)L_10)), (-10.0f), /*hidden argument*/NULL);
		__this->___U3CtoVectorU3E__1_1 = L_11;
		SCR_FollowAvatar_t352 * L_12 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_12);
		Transform_t1 * L_13 = Component_get_transform_m594(L_12, /*hidden argument*/NULL);
		SCR_FollowAvatar_t352 * L_14 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_14);
		Transform_t1 * L_15 = Component_get_transform_m594(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t4  L_16 = Transform_get_position_m593(L_15, /*hidden argument*/NULL);
		Vector3_t4  L_17 = (__this->___U3CtoVectorU3E__1_1);
		float L_18 = (__this->___U3CtimeU3E__0_0);
		SCR_FollowAvatar_t352 * L_19 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_19);
		float L_20 = (L_19->___zoomInSpeed_3);
		Vector3_t4  L_21 = Vector3_Lerp_m652(NULL /*static, unused*/, L_16, L_17, ((float)((float)L_18/(float)L_20)), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_position_m607(L_13, L_21, /*hidden argument*/NULL);
		SCR_FollowAvatar_t352 * L_22 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_22);
		Transform_t1 * L_23 = Component_get_transform_m594(L_22, /*hidden argument*/NULL);
		SCR_FollowAvatar_t352 * L_24 = (__this->___U3CU3Ef__this_7);
		SCR_FollowAvatar_t352 * L_25 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_25);
		Transform_t1 * L_26 = Component_get_transform_m594(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_t4  L_27 = Transform_get_position_m593(L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t4  L_28 = SCR_FollowAvatar_BindPosition_m1306(L_24, L_27, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_position_m607(L_23, L_28, /*hidden argument*/NULL);
		SCR_FollowAvatar_t352 * L_29 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_29);
		Transform_t1 * L_30 = Component_get_transform_m594(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t4  L_31 = Transform_get_position_m593(L_30, /*hidden argument*/NULL);
		V_4 = L_31;
		float L_32 = ((&V_4)->___z_3);
		__this->___U3CzoomInU3E__2_2 = ((((float)L_32) < ((float)(-10.5f)))? 1 : 0);
		float L_33 = (__this->___U3CtimeU3E__0_0);
		float L_34 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CtimeU3E__0_0 = ((float)((float)L_33+(float)L_34));
		WaitForEndOfFrame_t255 * L_35 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_35, /*hidden argument*/NULL);
		__this->___U24current_5 = L_35;
		__this->___U24PC_4 = 1;
		goto IL_0196;
	}

IL_0144:
	{
		bool L_36 = (__this->___U3CzoomInU3E__2_2);
		if (L_36)
		{
			goto IL_004b;
		}
	}
	{
		SCR_PointManager_t381 * L_37 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_37);
		SCR_PointManager_StartScoring_m1463(L_37, /*hidden argument*/NULL);
		SCR_FollowAvatar_t352 * L_38 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_38);
		MonoBehaviour_StartCoroutine_m1641(L_38, (String_t*) &_stringLiteral321, /*hidden argument*/NULL);
		SCR_FollowAvatar_t352 * L_39 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_39);
		MonoBehaviour_StopCoroutine_m1768(L_39, (String_t*) &_stringLiteral322, /*hidden argument*/NULL);
		__this->___U24current_5 = NULL;
		__this->___U24PC_4 = 2;
		goto IL_0196;
	}

IL_018d:
	{
		__this->___U24PC_4 = (-1);
	}

IL_0194:
	{
		return 0;
	}

IL_0196:
	{
		return 1;
	}
	// Dead block : IL_0198: ldloc.s V_5
}
// System.Void SCR_FollowAvatar/<ZoomInOn>c__Iterator7::Dispose()
extern "C" void U3CZoomInOnU3Ec__Iterator7_Dispose_m1292 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_4 = (-1);
		return;
	}
}
// System.Void SCR_FollowAvatar/<ZoomInOn>c__Iterator7::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CZoomInOnU3Ec__Iterator7_Reset_m1293 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// SCR_FollowAvatar/<FollowAvatar>c__Iterator8
#include "AssemblyU2DCSharp_SCR_FollowAvatar_U3CFollowAvatarU3Ec__Iter.h"
#ifndef _MSC_VER
#else
#endif
// SCR_FollowAvatar/<FollowAvatar>c__Iterator8
#include "AssemblyU2DCSharp_SCR_FollowAvatar_U3CFollowAvatarU3Ec__IterMethodDeclarations.h"



// System.Void SCR_FollowAvatar/<FollowAvatar>c__Iterator8::.ctor()
extern "C" void U3CFollowAvatarU3Ec__Iterator8__ctor_m1294 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_FollowAvatar/<FollowAvatar>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFollowAvatarU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1295 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object SCR_FollowAvatar/<FollowAvatar>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFollowAvatarU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1296 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean SCR_FollowAvatar/<FollowAvatar>c__Iterator8::MoveNext()
extern "C" bool U3CFollowAvatarU3Ec__Iterator8_MoveNext_m1297 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_010d;
		}
	}
	{
		goto IL_0119;
	}

IL_0021:
	{
		SCR_TimeManager_t383 * L_2 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = SCR_TimeManager_get_IsPaused_m1507(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_00fa;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_4 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		SCR_Camie_t338 * L_5 = SCR_ManagerComponents_get_AvatarMove_m1441(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t1 * L_6 = Component_get_transform_m594(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t4  L_7 = Transform_get_position_m593(L_6, /*hidden argument*/NULL);
		__this->___U3CfocusU3E__0_0 = L_7;
		Vector3_t4 * L_8 = &(__this->___U3CfocusU3E__0_0);
		Vector3_t4 * L_9 = L_8;
		float L_10 = (L_9->___y_2);
		SCR_FollowAvatar_t352 * L_11 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_11);
		float L_12 = (L_11->___plusHautQueCamie_5);
		L_9->___y_2 = ((float)((float)L_10+(float)L_12));
		Vector3_t4 * L_13 = &(__this->___U3CfocusU3E__0_0);
		L_13->___z_3 = (-10.0f);
		SCR_FollowAvatar_t352 * L_14 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_14);
		Transform_t1 * L_15 = Component_get_transform_m594(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t4  L_16 = Transform_get_position_m593(L_15, /*hidden argument*/NULL);
		Vector3_t4  L_17 = (__this->___U3CfocusU3E__0_0);
		SCR_FollowAvatar_t352 * L_18 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_18);
		float L_19 = (L_18->___followSpeed_2);
		float L_20 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_21 = Vector3_Lerp_m652(NULL /*static, unused*/, L_16, L_17, ((float)((float)L_19*(float)L_20)), /*hidden argument*/NULL);
		__this->___U3CnewPositionU3E__1_1 = L_21;
		SCR_FollowAvatar_t352 * L_22 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_22);
		Transform_t1 * L_23 = Component_get_transform_m594(L_22, /*hidden argument*/NULL);
		Vector3_t4 * L_24 = &(__this->___U3CnewPositionU3E__1_1);
		float L_25 = (L_24->___x_1);
		Vector3_t4 * L_26 = &(__this->___U3CnewPositionU3E__1_1);
		float L_27 = (L_26->___y_2);
		Vector3_t4  L_28 = {0};
		Vector3__ctor_m612(&L_28, L_25, L_27, (-10.0f), /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_position_m607(L_23, L_28, /*hidden argument*/NULL);
		SCR_FollowAvatar_t352 * L_29 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_29);
		Transform_t1 * L_30 = Component_get_transform_m594(L_29, /*hidden argument*/NULL);
		SCR_FollowAvatar_t352 * L_31 = (__this->___U3CU3Ef__this_4);
		Vector3_t4  L_32 = (__this->___U3CnewPositionU3E__1_1);
		NullCheck(L_31);
		Vector3_t4  L_33 = SCR_FollowAvatar_BindPosition_m1306(L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_position_m607(L_30, L_33, /*hidden argument*/NULL);
	}

IL_00fa:
	{
		__this->___U24current_3 = NULL;
		__this->___U24PC_2 = 1;
		goto IL_011b;
	}

IL_010d:
	{
		goto IL_0021;
	}
	// Dead block : IL_0112: ldarg.0

IL_0119:
	{
		return 0;
	}

IL_011b:
	{
		return 1;
	}
}
// System.Void SCR_FollowAvatar/<FollowAvatar>c__Iterator8::Dispose()
extern "C" void U3CFollowAvatarU3Ec__Iterator8_Dispose_m1298 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void SCR_FollowAvatar/<FollowAvatar>c__Iterator8::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CFollowAvatarU3Ec__Iterator8_Reset_m1299 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5MethodDeclarations.h"


// System.Void SCR_FollowAvatar::.ctor()
extern "C" void SCR_FollowAvatar__ctor_m1300 (SCR_FollowAvatar_t352 * __this, const MethodInfo* method)
{
	{
		__this->___followSpeed_2 = (4.0f);
		__this->___zoomInSpeed_3 = (5.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_FollowAvatar::Start()
extern "C" void SCR_FollowAvatar_Start_m1301 (SCR_FollowAvatar_t352 * __this, const MethodInfo* method)
{
	{
		SCR_FollowAvatar_DeleteExtraCameras_m1302(__this, /*hidden argument*/NULL);
		SCR_FollowAvatar_SetBounds_m1303(__this, /*hidden argument*/NULL);
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Camie_t338 * L_1 = SCR_ManagerComponents_get_AvatarMove_m1441(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1 * L_2 = Component_get_transform_m594(L_1, /*hidden argument*/NULL);
		Object_t * L_3 = SCR_FollowAvatar_ZoomInOn_m1304(__this, L_2, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1006(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_FollowAvatar::DeleteExtraCameras()
extern "C" void SCR_FollowAvatar_DeleteExtraCameras_m1302 (SCR_FollowAvatar_t352 * __this, const MethodInfo* method)
{
	GameObjectU5BU5D_t192* V_0 = {0};
	GameObject_t78 * V_1 = {0};
	GameObjectU5BU5D_t192* V_2 = {0};
	int32_t V_3 = 0;
	{
		GameObjectU5BU5D_t192* L_0 = GameObject_FindGameObjectsWithTag_m1769(NULL /*static, unused*/, (String_t*) &_stringLiteral315, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObjectU5BU5D_t192* L_1 = V_0;
		V_2 = L_1;
		V_3 = 0;
		goto IL_0033;
	}

IL_0014:
	{
		GameObjectU5BU5D_t192* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_1 = (*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_2, L_4));
		GameObject_t78 * L_5 = V_1;
		GameObject_t78 * L_6 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		bool L_7 = Object_op_Inequality_m623(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_002f;
		}
	}
	{
		GameObject_t78 * L_8 = V_1;
		Object_Destroy_m1008(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_002f:
	{
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_10 = V_3;
		GameObjectU5BU5D_t192* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)(((Array_t *)L_11)->max_length))))))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Void SCR_FollowAvatar::SetBounds()
extern TypeInfo* SCR_FollowAvatar_t352_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t355_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1770_MethodInfo_var;
extern "C" void SCR_FollowAvatar_SetBounds_m1303 (SCR_FollowAvatar_t352 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_FollowAvatar_t352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(679);
		Dictionary_2_t355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(680);
		Dictionary_2__ctor_m1770_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483715);
		s_Il2CppMethodIntialized = true;
	}
	GameObjectU5BU5D_t192* V_0 = {0};
	GameObject_t78 * V_1 = {0};
	GameObjectU5BU5D_t192* V_2 = {0};
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	Dictionary_2_t355 * V_5 = {0};
	int32_t V_6 = 0;
	Vector3_t4  V_7 = {0};
	Vector3_t4  V_8 = {0};
	Vector3_t4  V_9 = {0};
	Vector3_t4  V_10 = {0};
	{
		GameObjectU5BU5D_t192* L_0 = GameObject_FindGameObjectsWithTag_m1769(NULL /*static, unused*/, (String_t*) &_stringLiteral316, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObjectU5BU5D_t192* L_1 = V_0;
		V_2 = L_1;
		V_3 = 0;
		goto IL_016f;
	}

IL_0014:
	{
		GameObjectU5BU5D_t192* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_1 = (*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_2, L_4));
		GameObject_t78 * L_5 = V_1;
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m798(L_5, /*hidden argument*/NULL);
		V_4 = L_6;
		String_t* L_7 = V_4;
		if (!L_7)
		{
			goto IL_016b;
		}
	}
	{
		Dictionary_2_t355 * L_8 = ((SCR_FollowAvatar_t352_StaticFields*)SCR_FollowAvatar_t352_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_10;
		if (L_8)
		{
			goto IL_0074;
		}
	}
	{
		Dictionary_2_t355 * L_9 = (Dictionary_2_t355 *)il2cpp_codegen_object_new (Dictionary_2_t355_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1770(L_9, 4, /*hidden argument*/Dictionary_2__ctor_m1770_MethodInfo_var);
		V_5 = L_9;
		Dictionary_2_t355 * L_10 = V_5;
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_10, (String_t*) &_stringLiteral317, 0);
		Dictionary_2_t355 * L_11 = V_5;
		NullCheck(L_11);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_11, (String_t*) &_stringLiteral318, 1);
		Dictionary_2_t355 * L_12 = V_5;
		NullCheck(L_12);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_12, (String_t*) &_stringLiteral319, 2);
		Dictionary_2_t355 * L_13 = V_5;
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_13, (String_t*) &_stringLiteral320, 3);
		Dictionary_2_t355 * L_14 = V_5;
		((SCR_FollowAvatar_t352_StaticFields*)SCR_FollowAvatar_t352_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_10 = L_14;
	}

IL_0074:
	{
		Dictionary_2_t355 * L_15 = ((SCR_FollowAvatar_t352_StaticFields*)SCR_FollowAvatar_t352_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_10;
		String_t* L_16 = V_4;
		NullCheck(L_15);
		bool L_17 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_15, L_16, (&V_6));
		if (!L_17)
		{
			goto IL_016b;
		}
	}
	{
		int32_t L_18 = V_6;
		if (L_18 == 0)
		{
			goto IL_00a3;
		}
		if (L_18 == 1)
		{
			goto IL_00d9;
		}
		if (L_18 == 2)
		{
			goto IL_010f;
		}
		if (L_18 == 3)
		{
			goto IL_0135;
		}
	}
	{
		goto IL_016b;
	}

IL_00a3:
	{
		GameObject_t78 * L_19 = V_1;
		NullCheck(L_19);
		Transform_t1 * L_20 = GameObject_get_transform_m609(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t4  L_21 = Transform_get_position_m593(L_20, /*hidden argument*/NULL);
		V_7 = L_21;
		float L_22 = ((&V_7)->___x_1);
		float L_23 = (__this->___distanceDuFrame_4);
		__this->___minX_6 = ((float)((float)L_22+(float)L_23));
		SCR_ManagerComponents_t380 * L_24 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t78 * L_25 = V_1;
		NullCheck(L_25);
		Transform_t1 * L_26 = GameObject_get_transform_m609(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		SCR_ManagerComponents_set_LeftFrame_m1450(L_24, L_26, /*hidden argument*/NULL);
		goto IL_016b;
	}

IL_00d9:
	{
		GameObject_t78 * L_27 = V_1;
		NullCheck(L_27);
		Transform_t1 * L_28 = GameObject_get_transform_m609(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4  L_29 = Transform_get_position_m593(L_28, /*hidden argument*/NULL);
		V_8 = L_29;
		float L_30 = ((&V_8)->___x_1);
		float L_31 = (__this->___distanceDuFrame_4);
		__this->___maxX_7 = ((float)((float)L_30-(float)L_31));
		SCR_ManagerComponents_t380 * L_32 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t78 * L_33 = V_1;
		NullCheck(L_33);
		Transform_t1 * L_34 = GameObject_get_transform_m609(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		SCR_ManagerComponents_set_RightFrame_m1452(L_32, L_34, /*hidden argument*/NULL);
		goto IL_016b;
	}

IL_010f:
	{
		GameObject_t78 * L_35 = V_1;
		NullCheck(L_35);
		Transform_t1 * L_36 = GameObject_get_transform_m609(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t4  L_37 = Transform_get_position_m593(L_36, /*hidden argument*/NULL);
		V_9 = L_37;
		float L_38 = ((&V_9)->___y_2);
		float L_39 = (__this->___distanceDuFrame_4);
		__this->___maxY_9 = ((float)((float)L_38-(float)L_39));
		goto IL_016b;
	}

IL_0135:
	{
		GameObject_t78 * L_40 = V_1;
		NullCheck(L_40);
		Transform_t1 * L_41 = GameObject_get_transform_m609(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t4  L_42 = Transform_get_position_m593(L_41, /*hidden argument*/NULL);
		V_10 = L_42;
		float L_43 = ((&V_10)->___y_2);
		float L_44 = (__this->___distanceDuFrame_4);
		__this->___minY_8 = ((float)((float)L_43+(float)L_44));
		SCR_ManagerComponents_t380 * L_45 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t78 * L_46 = V_1;
		NullCheck(L_46);
		Transform_t1 * L_47 = GameObject_get_transform_m609(L_46, /*hidden argument*/NULL);
		NullCheck(L_45);
		SCR_ManagerComponents_set_BottomFrame_m1454(L_45, L_47, /*hidden argument*/NULL);
		goto IL_016b;
	}

IL_016b:
	{
		int32_t L_48 = V_3;
		V_3 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_016f:
	{
		int32_t L_49 = V_3;
		GameObjectU5BU5D_t192* L_50 = V_2;
		NullCheck(L_50);
		if ((((int32_t)L_49) < ((int32_t)(((int32_t)(((Array_t *)L_50)->max_length))))))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Collections.IEnumerator SCR_FollowAvatar::ZoomInOn(UnityEngine.Transform)
extern TypeInfo* U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_FollowAvatar_ZoomInOn_m1304 (SCR_FollowAvatar_t352 * __this, Transform_t1 * ___zoomTo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(681);
		s_Il2CppMethodIntialized = true;
	}
	U3CZoomInOnU3Ec__Iterator7_t353 * V_0 = {0};
	{
		U3CZoomInOnU3Ec__Iterator7_t353 * L_0 = (U3CZoomInOnU3Ec__Iterator7_t353 *)il2cpp_codegen_object_new (U3CZoomInOnU3Ec__Iterator7_t353_il2cpp_TypeInfo_var);
		U3CZoomInOnU3Ec__Iterator7__ctor_m1288(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CZoomInOnU3Ec__Iterator7_t353 * L_1 = V_0;
		Transform_t1 * L_2 = ___zoomTo;
		NullCheck(L_1);
		L_1->___zoomTo_3 = L_2;
		U3CZoomInOnU3Ec__Iterator7_t353 * L_3 = V_0;
		Transform_t1 * L_4 = ___zoomTo;
		NullCheck(L_3);
		L_3->___U3CU24U3EzoomTo_6 = L_4;
		U3CZoomInOnU3Ec__Iterator7_t353 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_7 = __this;
		U3CZoomInOnU3Ec__Iterator7_t353 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator SCR_FollowAvatar::FollowAvatar()
extern TypeInfo* U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_FollowAvatar_FollowAvatar_m1305 (SCR_FollowAvatar_t352 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(682);
		s_Il2CppMethodIntialized = true;
	}
	U3CFollowAvatarU3Ec__Iterator8_t354 * V_0 = {0};
	{
		U3CFollowAvatarU3Ec__Iterator8_t354 * L_0 = (U3CFollowAvatarU3Ec__Iterator8_t354 *)il2cpp_codegen_object_new (U3CFollowAvatarU3Ec__Iterator8_t354_il2cpp_TypeInfo_var);
		U3CFollowAvatarU3Ec__Iterator8__ctor_m1294(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFollowAvatarU3Ec__Iterator8_t354 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_4 = __this;
		U3CFollowAvatarU3Ec__Iterator8_t354 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Vector3 SCR_FollowAvatar::BindPosition(UnityEngine.Vector3)
extern "C" Vector3_t4  SCR_FollowAvatar_BindPosition_m1306 (SCR_FollowAvatar_t352 * __this, Vector3_t4  ___newPosition, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t4  V_2 = {0};
	Vector3_t4  V_3 = {0};
	Vector3_t4  V_4 = {0};
	Vector3_t4  V_5 = {0};
	Vector3_t4  V_6 = {0};
	Vector3_t4  V_7 = {0};
	Vector3_t4  V_8 = {0};
	Vector3_t4  V_9 = {0};
	Vector3_t4  V_10 = {0};
	Vector3_t4  V_11 = {0};
	float G_B5_0 = 0.0f;
	float G_B11_0 = 0.0f;
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		float L_2 = ((&V_2)->___x_1);
		float L_3 = (__this->___maxX_7);
		if ((((float)L_2) > ((float)L_3)))
		{
			goto IL_003c;
		}
	}
	{
		Transform_t1 * L_4 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4  L_5 = Transform_get_position_m593(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = ((&V_3)->___x_1);
		float L_7 = (__this->___minX_6);
		if ((!(((float)L_6) < ((float)L_7))))
		{
			goto IL_009d;
		}
	}

IL_003c:
	{
		Transform_t1 * L_8 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t4  L_9 = Transform_get_position_m593(L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		float L_10 = ((&V_4)->___x_1);
		float L_11 = (__this->___maxX_7);
		if ((!(((float)L_10) > ((float)L_11))))
		{
			goto IL_0066;
		}
	}
	{
		float L_12 = (__this->___maxX_7);
		G_B5_0 = L_12;
		goto IL_006c;
	}

IL_0066:
	{
		float L_13 = (__this->___minX_6);
		G_B5_0 = L_13;
	}

IL_006c:
	{
		V_0 = G_B5_0;
		float L_14 = V_0;
		Transform_t1 * L_15 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t4  L_16 = Transform_get_position_m593(L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = ((&V_5)->___y_2);
		Transform_t1 * L_18 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t4  L_19 = Transform_get_position_m593(L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		float L_20 = ((&V_6)->___z_3);
		Vector3__ctor_m612((&___newPosition), L_14, L_17, L_20, /*hidden argument*/NULL);
	}

IL_009d:
	{
		Transform_t1 * L_21 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t4  L_22 = Transform_get_position_m593(L_21, /*hidden argument*/NULL);
		V_7 = L_22;
		float L_23 = ((&V_7)->___y_2);
		float L_24 = (__this->___maxY_9);
		if ((((float)L_23) > ((float)L_24)))
		{
			goto IL_00db;
		}
	}
	{
		Transform_t1 * L_25 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t4  L_26 = Transform_get_position_m593(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		float L_27 = ((&V_8)->___y_2);
		float L_28 = (__this->___minY_8);
		if ((!(((float)L_27) < ((float)L_28))))
		{
			goto IL_013c;
		}
	}

IL_00db:
	{
		Transform_t1 * L_29 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t4  L_30 = Transform_get_position_m593(L_29, /*hidden argument*/NULL);
		V_9 = L_30;
		float L_31 = ((&V_9)->___y_2);
		float L_32 = (__this->___maxY_9);
		if ((!(((float)L_31) > ((float)L_32))))
		{
			goto IL_0105;
		}
	}
	{
		float L_33 = (__this->___maxY_9);
		G_B11_0 = L_33;
		goto IL_010b;
	}

IL_0105:
	{
		float L_34 = (__this->___minY_8);
		G_B11_0 = L_34;
	}

IL_010b:
	{
		V_1 = G_B11_0;
		Transform_t1 * L_35 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t4  L_36 = Transform_get_position_m593(L_35, /*hidden argument*/NULL);
		V_10 = L_36;
		float L_37 = ((&V_10)->___x_1);
		float L_38 = V_1;
		Transform_t1 * L_39 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t4  L_40 = Transform_get_position_m593(L_39, /*hidden argument*/NULL);
		V_11 = L_40;
		float L_41 = ((&V_11)->___z_3);
		Vector3__ctor_m612((&___newPosition), L_37, L_38, L_41, /*hidden argument*/NULL);
	}

IL_013c:
	{
		Vector3_t4  L_42 = ___newPosition;
		return L_42;
	}
}
// SCR_Puceron
#include "AssemblyU2DCSharp_SCR_Puceron.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Puceron
#include "AssemblyU2DCSharp_SCR_PuceronMethodDeclarations.h"

// SCR_PointManager/CollectibleType
#include "AssemblyU2DCSharp_SCR_PointManager_CollectibleType.h"
// System.Collections.Generic.List`1<SCR_Puceron>
#include "mscorlib_System_Collections_Generic_List_1_gen_7.h"
// System.Collections.Generic.List`1<SCR_Puceron>
#include "mscorlib_System_Collections_Generic_List_1_gen_7MethodDeclarations.h"


// System.Void SCR_Puceron::.ctor()
extern "C" void SCR_Puceron__ctor_m1307 (SCR_Puceron_t356 * __this, const MethodInfo* method)
{
	{
		SCR_Collectible__ctor_m1188(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Puceron::Start()
extern "C" void SCR_Puceron_Start_m1308 (SCR_Puceron_t356 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		__this->___originalPosition_4 = L_1;
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Quaternion_t19  L_3 = Transform_get_rotation_m656(L_2, /*hidden argument*/NULL);
		__this->___originalRotation_5 = L_3;
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral323, (0.1f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Puceron::UnRotate()
extern const MethodInfo* Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var;
extern "C" void SCR_Puceron_UnRotate_m1309 (SCR_Puceron_t356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483679);
		s_Il2CppMethodIntialized = true;
	}
	TransformU5BU5D_t141* V_0 = {0};
	{
		TransformU5BU5D_t141* L_0 = Component_GetComponentsInChildren_TisTransform_t1_m1001(__this, /*hidden argument*/Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var);
		V_0 = L_0;
		TransformU5BU5D_t141* L_1 = V_0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))) >= ((int32_t)1)))
		{
			goto IL_0034;
		}
	}
	{
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4  L_3 = {0};
		Vector3__ctor_m612(&L_3, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_eulerAngles_m1704(L_2, L_3, /*hidden argument*/NULL);
		goto IL_0044;
	}

IL_0034:
	{
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral323, (0.1f), /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void SCR_Puceron::OnTriggerEnter(UnityEngine.Collider)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void SCR_Puceron_OnTriggerEnter_m1310 (SCR_Puceron_t356 * __this, Collider_t138 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t138 * L_0 = ___other;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m635(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m636(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		VirtActionInvoker0::Invoke(4 /* System.Void SCR_Puceron::Collect() */, __this);
	}

IL_001b:
	{
		return;
	}
}
// System.Void SCR_Puceron::Update()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" void SCR_Puceron_Update_m1311 (SCR_Puceron_t356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (((SCR_Collectible_t331 *)__this)->___isTaken_3);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m925(NULL /*static, unused*/, 8, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		SCR_Puceron_ReturnToGame_m1314(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void SCR_Puceron::Collect()
extern "C" void SCR_Puceron_Collect_m1312 (SCR_Puceron_t356 * __this, const MethodInfo* method)
{
	{
		((SCR_Collectible_t331 *)__this)->___isTaken_3 = 1;
		SCR_PointManager_t381 * L_0 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_PointManager_AcquireCollectible_m1466(L_0, 1, /*hidden argument*/NULL);
		SCR_PointManager_t381 * L_1 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_t387 * L_2 = SCR_PointManager_get_PuceronsCollected_m1478(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< SCR_Puceron_t356 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SCR_Puceron>::Add(!0) */, L_2, __this);
		SCR_Puceron_Pool_m1313(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Puceron::Pool()
extern const MethodInfo* Component_GetComponent_TisBoxCollider_t343_m1751_MethodInfo_var;
extern "C" void SCR_Puceron_Pool_m1313 (SCR_Puceron_t356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisBoxCollider_t343_m1751_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483709);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	Vector3_t4  V_2 = {0};
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = ((&V_1)->___x_1);
		Transform_t1 * L_3 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = ((&V_2)->___y_2);
		Vector3__ctor_m612((&V_0), L_2, L_5, (-100.0f), /*hidden argument*/NULL);
		Transform_t1 * L_6 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4  L_7 = V_0;
		NullCheck(L_6);
		Transform_set_position_m607(L_6, L_7, /*hidden argument*/NULL);
		BoxCollider_t343 * L_8 = (__this->___trigger_6);
		bool L_9 = Object_op_Equality_m640(NULL /*static, unused*/, L_8, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005b;
		}
	}
	{
		BoxCollider_t343 * L_10 = Component_GetComponent_TisBoxCollider_t343_m1751(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider_t343_m1751_MethodInfo_var);
		__this->___trigger_6 = L_10;
	}

IL_005b:
	{
		BoxCollider_t343 * L_11 = (__this->___trigger_6);
		NullCheck(L_11);
		Collider_set_enabled_m1747(L_11, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Puceron::ReturnToGame()
extern "C" void SCR_Puceron_ReturnToGame_m1314 (SCR_Puceron_t356 * __this, const MethodInfo* method)
{
	{
		((SCR_Collectible_t331 *)__this)->___isTaken_3 = 0;
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4  L_1 = (__this->___originalPosition_4);
		NullCheck(L_0);
		Transform_set_position_m607(L_0, L_1, /*hidden argument*/NULL);
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Quaternion_t19  L_3 = (__this->___originalRotation_5);
		NullCheck(L_2);
		Transform_set_rotation_m658(L_2, L_3, /*hidden argument*/NULL);
		BoxCollider_t343 * L_4 = (__this->___trigger_6);
		NullCheck(L_4);
		Collider_set_enabled_m1747(L_4, 1, /*hidden argument*/NULL);
		return;
	}
}
// SCR_PuceronMove/TrajectoryType
#include "AssemblyU2DCSharp_SCR_PuceronMove_TrajectoryType.h"
#ifndef _MSC_VER
#else
#endif
// SCR_PuceronMove/TrajectoryType
#include "AssemblyU2DCSharp_SCR_PuceronMove_TrajectoryTypeMethodDeclarations.h"



// SCR_PuceronMove/<MoveToWaypoint>c__Iterator9
#include "AssemblyU2DCSharp_SCR_PuceronMove_U3CMoveToWaypointU3Ec__Ite.h"
#ifndef _MSC_VER
#else
#endif
// SCR_PuceronMove/<MoveToWaypoint>c__Iterator9
#include "AssemblyU2DCSharp_SCR_PuceronMove_U3CMoveToWaypointU3Ec__IteMethodDeclarations.h"

// SCR_PuceronMove
#include "AssemblyU2DCSharp_SCR_PuceronMove.h"
// SCR_Waypoint
#include "AssemblyU2DCSharp_SCR_Waypoint.h"
// SCR_PuceronMove
#include "AssemblyU2DCSharp_SCR_PuceronMoveMethodDeclarations.h"


// System.Void SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::.ctor()
extern "C" void U3CMoveToWaypointU3Ec__Iterator9__ctor_m1315 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CMoveToWaypointU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1316 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CMoveToWaypointU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1317 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CMoveToWaypointU3Ec__Iterator9_MoveNext_m1318 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0114;
		}
		if (L_1 == 2)
		{
			goto IL_0159;
		}
	}
	{
		goto IL_0160;
	}

IL_0025:
	{
		SCR_PuceronMove_t359 * L_2 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_2);
		L_2->___doMove_11 = 1;
		goto IL_0114;
	}

IL_0036:
	{
		SCR_PuceronMove_t359 * L_3 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_3);
		Transform_t1 * L_4 = Component_get_transform_m594(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4  L_5 = Transform_get_position_m593(L_4, /*hidden argument*/NULL);
		SCR_Waypoint_t358 * L_6 = (__this->___wp_0);
		NullCheck(L_6);
		Transform_t1 * L_7 = Component_get_transform_m594(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4  L_8 = Transform_get_position_m593(L_7, /*hidden argument*/NULL);
		float L_9 = Vector3_Distance_m1750(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		if ((!(((float)L_9) < ((float)(0.5f)))))
		{
			goto IL_0071;
		}
	}
	{
		SCR_PuceronMove_t359 * L_10 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_10);
		L_10->___doMove_11 = 0;
	}

IL_0071:
	{
		SCR_PuceronMove_t359 * L_11 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_11);
		Transform_t1 * L_12 = Component_get_transform_m594(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t4  L_13 = Transform_get_position_m593(L_12, /*hidden argument*/NULL);
		SCR_Waypoint_t358 * L_14 = (__this->___wp_0);
		NullCheck(L_14);
		Transform_t1 * L_15 = Component_get_transform_m594(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t4  L_16 = Transform_get_position_m593(L_15, /*hidden argument*/NULL);
		Vector3_t4  L_17 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		Vector3_t4  L_18 = Vector3_op_UnaryNegation_m682(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		__this->___U3CtargetU3E__0_1 = L_18;
		SCR_PuceronMove_t359 * L_19 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_19);
		Transform_t1 * L_20 = Component_get_transform_m594(L_19, /*hidden argument*/NULL);
		Vector3_t4  L_21 = (__this->___U3CtargetU3E__0_1);
		NullCheck(L_20);
		Transform_set_right_m1771(L_20, L_21, /*hidden argument*/NULL);
		SCR_PuceronMove_t359 * L_22 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_22);
		Transform_t1 * L_23 = Component_get_transform_m594(L_22, /*hidden argument*/NULL);
		SCR_PuceronMove_t359 * L_24 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_24);
		Transform_t1 * L_25 = Component_get_transform_m594(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t4  L_26 = Transform_get_position_m593(L_25, /*hidden argument*/NULL);
		SCR_Waypoint_t358 * L_27 = (__this->___wp_0);
		NullCheck(L_27);
		Transform_t1 * L_28 = Component_get_transform_m594(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4  L_29 = Transform_get_position_m593(L_28, /*hidden argument*/NULL);
		SCR_PuceronMove_t359 * L_30 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_30);
		float L_31 = (L_30->___speed_8);
		float L_32 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_33 = Vector3_Lerp_m652(NULL /*static, unused*/, L_26, L_29, ((float)((float)L_31*(float)L_32)), /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_position_m607(L_23, L_33, /*hidden argument*/NULL);
		WaitForEndOfFrame_t255 * L_34 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_34, /*hidden argument*/NULL);
		__this->___U24current_3 = L_34;
		__this->___U24PC_2 = 1;
		goto IL_0162;
	}

IL_0114:
	{
		SCR_PuceronMove_t359 * L_35 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_35);
		bool L_36 = (L_35->___doMove_11);
		if (L_36)
		{
			goto IL_0036;
		}
	}
	{
		SCR_PuceronMove_t359 * L_37 = (__this->___U3CU3Ef__this_5);
		SCR_PuceronMove_t359 * L_38 = (__this->___U3CU3Ef__this_5);
		SCR_PuceronMove_t359 * L_39 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_39);
		SCR_Waypoint_t358 * L_40 = (SCR_Waypoint_t358 *)VirtFuncInvoker0< SCR_Waypoint_t358 * >::Invoke(8 /* SCR_Waypoint SCR_PuceronMove::GetNextWaypoint() */, L_39);
		NullCheck(L_38);
		Object_t * L_41 = (Object_t *)VirtFuncInvoker1< Object_t *, SCR_Waypoint_t358 * >::Invoke(9 /* System.Collections.IEnumerator SCR_PuceronMove::MoveToWaypoint(SCR_Waypoint) */, L_38, L_40);
		NullCheck(L_37);
		MonoBehaviour_StartCoroutine_m1006(L_37, L_41, /*hidden argument*/NULL);
		__this->___U24current_3 = NULL;
		__this->___U24PC_2 = 2;
		goto IL_0162;
	}

IL_0159:
	{
		__this->___U24PC_2 = (-1);
	}

IL_0160:
	{
		return 0;
	}

IL_0162:
	{
		return 1;
	}
	// Dead block : IL_0164: ldloc.1
}
// System.Void SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::Dispose()
extern "C" void U3CMoveToWaypointU3Ec__Iterator9_Dispose_m1319 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CMoveToWaypointU3Ec__Iterator9_Reset_m1320 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Component_t219;
struct SCR_WaypointU5BU5D_t361;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<SCR_Waypoint>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<SCR_Waypoint>()
#define Component_GetComponentsInChildren_TisSCR_Waypoint_t358_m1772(__this, method) (( SCR_WaypointU5BU5D_t361* (*) (Component_t219 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m698_gshared)(__this, method)


// System.Void SCR_PuceronMove::.ctor()
extern "C" void SCR_PuceronMove__ctor_m1321 (SCR_PuceronMove_t359 * __this, const MethodInfo* method)
{
	{
		__this->___speed_8 = (1.0f);
		__this->___goingToIndex_10 = 1;
		SCR_Puceron__ctor_m1307(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_PuceronMove::Start()
extern "C" void SCR_PuceronMove_Start_m1322 (SCR_PuceronMove_t359 * __this, const MethodInfo* method)
{
	{
		SCR_Puceron_Start_m1308(__this, /*hidden argument*/NULL);
		SCR_WaypointU5BU5D_t361* L_0 = (SCR_WaypointU5BU5D_t361*)VirtFuncInvoker0< SCR_WaypointU5BU5D_t361* >::Invoke(5 /* SCR_Waypoint[] SCR_PuceronMove::GetWaypoints() */, __this);
		__this->___myWaypoints_9 = L_0;
		SCR_PointManager_t381 * L_1 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		SCR_PointManager_IncrementMovingPucerons_m1464(L_1, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(7 /* System.Void SCR_PuceronMove::UnChildWaypoints() */, __this);
		return;
	}
}
// System.Void SCR_PuceronMove::Update()
extern "C" void SCR_PuceronMove_Update_m1323 (SCR_PuceronMove_t359 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isStarted_13);
		if (L_0)
		{
			goto IL_0027;
		}
	}
	{
		SCR_TimeManager_t383 * L_1 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = SCR_TimeManager_get_IsCounting_m1508(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		VirtActionInvoker0::Invoke(10 /* System.Void SCR_PuceronMove::StartMove() */, __this);
		__this->___isStarted_13 = 1;
	}

IL_0027:
	{
		return;
	}
}
// System.Void SCR_PuceronMove::Collect()
extern "C" void SCR_PuceronMove_Collect_m1324 (SCR_PuceronMove_t359 * __this, const MethodInfo* method)
{
	{
		SCR_Puceron_Collect_m1312(__this, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(11 /* System.Void SCR_PuceronMove::StopMove() */, __this);
		return;
	}
}
// System.Void SCR_PuceronMove::ReturnToGame()
extern "C" void SCR_PuceronMove_ReturnToGame_m1325 (SCR_PuceronMove_t359 * __this, const MethodInfo* method)
{
	{
		SCR_Puceron_ReturnToGame_m1314(__this, /*hidden argument*/NULL);
		__this->___goingToIndex_10 = 1;
		VirtActionInvoker0::Invoke(10 /* System.Void SCR_PuceronMove::StartMove() */, __this);
		return;
	}
}
// SCR_Waypoint[] SCR_PuceronMove::GetWaypoints()
extern const MethodInfo* Component_GetComponentsInChildren_TisSCR_Waypoint_t358_m1772_MethodInfo_var;
extern "C" SCR_WaypointU5BU5D_t361* SCR_PuceronMove_GetWaypoints_m1326 (SCR_PuceronMove_t359 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentsInChildren_TisSCR_Waypoint_t358_m1772_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483716);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_WaypointU5BU5D_t361* L_0 = Component_GetComponentsInChildren_TisSCR_Waypoint_t358_m1772(__this, /*hidden argument*/Component_GetComponentsInChildren_TisSCR_Waypoint_t358_m1772_MethodInfo_var);
		SCR_WaypointU5BU5D_t361* L_1 = (SCR_WaypointU5BU5D_t361*)VirtFuncInvoker2< SCR_WaypointU5BU5D_t361*, SCR_WaypointU5BU5D_t361*, bool >::Invoke(6 /* SCR_Waypoint[] SCR_PuceronMove::SortWaypoints(SCR_Waypoint[],System.Boolean) */, __this, L_0, 1);
		return L_1;
	}
}
// SCR_Waypoint[] SCR_PuceronMove::SortWaypoints(SCR_Waypoint[],System.Boolean)
extern TypeInfo* SCR_WaypointU5BU5D_t361_il2cpp_TypeInfo_var;
extern "C" SCR_WaypointU5BU5D_t361* SCR_PuceronMove_SortWaypoints_m1327 (SCR_PuceronMove_t359 * __this, SCR_WaypointU5BU5D_t361* ___unorderedArray, bool ___isInit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_WaypointU5BU5D_t361_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(684);
		s_Il2CppMethodIntialized = true;
	}
	SCR_WaypointU5BU5D_t361* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = {0};
	{
		SCR_WaypointU5BU5D_t361* L_0 = ___unorderedArray;
		NullCheck(L_0);
		V_0 = ((SCR_WaypointU5BU5D_t361*)SZArrayNew(SCR_WaypointU5BU5D_t361_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_0)->max_length)))));
		bool L_1 = ___isInit;
		if (!L_1)
		{
			goto IL_0035;
		}
	}
	{
		V_1 = 0;
		goto IL_0027;
	}

IL_0016:
	{
		SCR_WaypointU5BU5D_t361* L_2 = V_0;
		SCR_WaypointU5BU5D_t361* L_3 = ___unorderedArray;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck((*(SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_3, L_5)));
		int32_t L_6 = ((*(SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_3, L_5))->___id_2);
		SCR_WaypointU5BU5D_t361* L_7 = ___unorderedArray;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_6);
		ArrayElementTypeCheck (L_2, (*(SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_7, L_9)));
		*((SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_2, L_6)) = (SCR_Waypoint_t358 *)(*(SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_7, L_9));
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_11 = V_1;
		SCR_WaypointU5BU5D_t361* L_12 = ___unorderedArray;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_12)->max_length))))))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_00bd;
	}

IL_0035:
	{
		int32_t L_13 = (__this->___trajectoire_7);
		V_5 = L_13;
		int32_t L_14 = V_5;
		if (!L_14)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_15 = V_5;
		if ((((int32_t)L_15) == ((int32_t)1)))
		{
			goto IL_007d;
		}
	}
	{
		goto IL_00bd;
	}

IL_0051:
	{
		__this->___goingToIndex_10 = 0;
		V_2 = 0;
		goto IL_006f;
	}

IL_005f:
	{
		SCR_WaypointU5BU5D_t361* L_16 = V_0;
		int32_t L_17 = V_2;
		SCR_WaypointU5BU5D_t361* L_18 = ___unorderedArray;
		SCR_WaypointU5BU5D_t361* L_19 = ___unorderedArray;
		NullCheck(L_19);
		int32_t L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_19)->max_length)))-(int32_t)1))-(int32_t)L_20)));
		int32_t L_21 = ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_19)->max_length)))-(int32_t)1))-(int32_t)L_20));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		ArrayElementTypeCheck (L_16, (*(SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_18, L_21)));
		*((SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_16, L_17)) = (SCR_Waypoint_t358 *)(*(SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_18, L_21));
		int32_t L_22 = V_2;
		V_2 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_006f:
	{
		int32_t L_23 = V_2;
		SCR_WaypointU5BU5D_t361* L_24 = ___unorderedArray;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)(((Array_t *)L_24)->max_length))))))
		{
			goto IL_005f;
		}
	}
	{
		goto IL_00bd;
	}

IL_007d:
	{
		int32_t L_25 = (__this->___goingToIndex_10);
		V_3 = ((int32_t)((int32_t)L_25-(int32_t)1));
		V_4 = 0;
		goto IL_00a7;
	}

IL_008e:
	{
		SCR_WaypointU5BU5D_t361* L_26 = V_0;
		int32_t L_27 = V_4;
		SCR_WaypointU5BU5D_t361* L_28 = ___unorderedArray;
		int32_t L_29 = V_3;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		ArrayElementTypeCheck (L_26, (*(SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_28, L_30)));
		*((SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_26, L_27)) = (SCR_Waypoint_t358 *)(*(SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_28, L_30));
		int32_t L_31 = V_3;
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		V_3 = L_32;
		SCR_WaypointU5BU5D_t361* L_33 = ___unorderedArray;
		NullCheck(L_33);
		V_3 = ((int32_t)((int32_t)L_32%(int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_33)->max_length)))-(int32_t)1))));
		int32_t L_34 = V_4;
		V_4 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00a7:
	{
		int32_t L_35 = V_4;
		SCR_WaypointU5BU5D_t361* L_36 = ___unorderedArray;
		NullCheck(L_36);
		if ((((int32_t)L_35) < ((int32_t)(((int32_t)(((Array_t *)L_36)->max_length))))))
		{
			goto IL_008e;
		}
	}
	{
		__this->___goingToIndex_10 = 0;
		goto IL_00bd;
	}

IL_00bd:
	{
		SCR_WaypointU5BU5D_t361* L_37 = V_0;
		return L_37;
	}
}
// System.Void SCR_PuceronMove::UnChildWaypoints()
extern "C" void SCR_PuceronMove_UnChildWaypoints_m1328 (SCR_PuceronMove_t359 * __this, const MethodInfo* method)
{
	SCR_Waypoint_t358 * V_0 = {0};
	SCR_WaypointU5BU5D_t361* V_1 = {0};
	int32_t V_2 = 0;
	{
		SCR_WaypointU5BU5D_t361* L_0 = (__this->___myWaypoints_9);
		V_1 = L_0;
		V_2 = 0;
		goto IL_0022;
	}

IL_000e:
	{
		SCR_WaypointU5BU5D_t361* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_1, L_3));
		SCR_Waypoint_t358 * L_4 = V_0;
		NullCheck(L_4);
		Transform_t1 * L_5 = Component_get_transform_m594(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_parent_m596(L_5, (Transform_t1 *)NULL, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0022:
	{
		int32_t L_7 = V_2;
		SCR_WaypointU5BU5D_t361* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)(((Array_t *)L_8)->max_length))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// SCR_Waypoint SCR_PuceronMove::GetNextWaypoint()
extern "C" SCR_Waypoint_t358 * SCR_PuceronMove_GetNextWaypoint_m1329 (SCR_PuceronMove_t359 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___goingToIndex_10);
		__this->___goingToIndex_10 = ((int32_t)((int32_t)L_0+(int32_t)1));
		int32_t L_1 = (__this->___goingToIndex_10);
		SCR_WaypointU5BU5D_t361* L_2 = (__this->___myWaypoints_9);
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)(((Array_t *)L_2)->max_length)))))))
		{
			goto IL_003b;
		}
	}
	{
		SCR_WaypointU5BU5D_t361* L_3 = (__this->___myWaypoints_9);
		SCR_WaypointU5BU5D_t361* L_4 = (SCR_WaypointU5BU5D_t361*)VirtFuncInvoker2< SCR_WaypointU5BU5D_t361*, SCR_WaypointU5BU5D_t361*, bool >::Invoke(6 /* SCR_Waypoint[] SCR_PuceronMove::SortWaypoints(SCR_Waypoint[],System.Boolean) */, __this, L_3, 0);
		__this->___myWaypoints_9 = L_4;
		__this->___goingToIndex_10 = 0;
	}

IL_003b:
	{
		SCR_WaypointU5BU5D_t361* L_5 = (__this->___myWaypoints_9);
		int32_t L_6 = (__this->___goingToIndex_10);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		return (*(SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_5, L_7));
	}
}
// System.Collections.IEnumerator SCR_PuceronMove::MoveToWaypoint(SCR_Waypoint)
extern TypeInfo* U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_PuceronMove_MoveToWaypoint_m1330 (SCR_PuceronMove_t359 * __this, SCR_Waypoint_t358 * ___wp, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(685);
		s_Il2CppMethodIntialized = true;
	}
	U3CMoveToWaypointU3Ec__Iterator9_t360 * V_0 = {0};
	{
		U3CMoveToWaypointU3Ec__Iterator9_t360 * L_0 = (U3CMoveToWaypointU3Ec__Iterator9_t360 *)il2cpp_codegen_object_new (U3CMoveToWaypointU3Ec__Iterator9_t360_il2cpp_TypeInfo_var);
		U3CMoveToWaypointU3Ec__Iterator9__ctor_m1315(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMoveToWaypointU3Ec__Iterator9_t360 * L_1 = V_0;
		SCR_Waypoint_t358 * L_2 = ___wp;
		NullCheck(L_1);
		L_1->___wp_0 = L_2;
		U3CMoveToWaypointU3Ec__Iterator9_t360 * L_3 = V_0;
		SCR_Waypoint_t358 * L_4 = ___wp;
		NullCheck(L_3);
		L_3->___U3CU24U3Ewp_4 = L_4;
		U3CMoveToWaypointU3Ec__Iterator9_t360 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_5 = __this;
		U3CMoveToWaypointU3Ec__Iterator9_t360 * L_6 = V_0;
		return L_6;
	}
}
// System.Void SCR_PuceronMove::StartMove()
extern "C" void SCR_PuceronMove_StartMove_m1331 (SCR_PuceronMove_t359 * __this, const MethodInfo* method)
{
	{
		__this->___doMove_11 = 1;
		SCR_WaypointU5BU5D_t361* L_0 = (__this->___myWaypoints_9);
		int32_t L_1 = (__this->___goingToIndex_10);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Object_t * L_3 = (Object_t *)VirtFuncInvoker1< Object_t *, SCR_Waypoint_t358 * >::Invoke(9 /* System.Collections.IEnumerator SCR_PuceronMove::MoveToWaypoint(SCR_Waypoint) */, __this, (*(SCR_Waypoint_t358 **)(SCR_Waypoint_t358 **)SZArrayLdElema(L_0, L_2)));
		MonoBehaviour_StartCoroutine_m1006(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_PuceronMove::StopMove()
extern "C" void SCR_PuceronMove_StopMove_m1332 (SCR_PuceronMove_t359 * __this, const MethodInfo* method)
{
	{
		__this->___doMove_11 = 0;
		MonoBehaviour_StopAllCoroutines_m1773(__this, /*hidden argument*/NULL);
		return;
	}
}
// SCR_Star
#include "AssemblyU2DCSharp_SCR_Star.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Star
#include "AssemblyU2DCSharp_SCR_StarMethodDeclarations.h"

struct Component_t219;
struct Animator_t9;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t9_m616(__this, method) (( Animator_t9 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct Collider_t138;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t138_m883(__this, method) (( Collider_t138 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void SCR_Star::.ctor()
extern "C" void SCR_Star__ctor_m1333 (SCR_Star_t362 * __this, const MethodInfo* method)
{
	{
		SCR_Collectible__ctor_m1188(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Star::Start()
extern const MethodInfo* Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var;
extern "C" void SCR_Star_Start_m1334 (SCR_Star_t362 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t78 * V_0 = {0};
	{
		Animator_t9 * L_0 = Component_GetComponent_TisAnimator_t9_m616(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var);
		__this->___starAnim_4 = L_0;
		Transform_t1 * L_1 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1 * L_2 = Transform_GetChild_m1774(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t78 * L_3 = Component_get_gameObject_m622(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t78 * L_4 = V_0;
		NullCheck(L_4);
		Transform_t1 * L_5 = GameObject_get_transform_m609(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_parent_m596(L_5, (Transform_t1 *)NULL, /*hidden argument*/NULL);
		Transform_t1 * L_6 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		GameObject_t78 * L_7 = V_0;
		NullCheck(L_7);
		Transform_t1 * L_8 = GameObject_get_transform_m609(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t4  L_9 = Transform_get_position_m593(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_position_m607(L_6, L_9, /*hidden argument*/NULL);
		GameObject_t78 * L_10 = V_0;
		NullCheck(L_10);
		GameObject_SetActive_m713(L_10, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Star::OnTriggerEnter(UnityEngine.Collider)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void SCR_Star_OnTriggerEnter_m1335 (SCR_Star_t362 * __this, Collider_t138 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t138 * L_0 = ___other;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m635(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m636(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		VirtActionInvoker0::Invoke(4 /* System.Void SCR_Star::Collect() */, __this);
	}

IL_001b:
	{
		return;
	}
}
// System.Void SCR_Star::Kill()
extern "C" void SCR_Star_Kill_m1336 (SCR_Star_t362 * __this, const MethodInfo* method)
{
	{
		GameObject_t78 * L_0 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Star::Collect()
extern const MethodInfo* Component_GetComponent_TisCollider_t138_m883_MethodInfo_var;
extern "C" void SCR_Star_Collect_m1337 (SCR_Star_t362 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCollider_t138_m883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483661);
		s_Il2CppMethodIntialized = true;
	}
	{
		((SCR_Collectible_t331 *)__this)->___isTaken_3 = 1;
		Animator_t9 * L_0 = (__this->___starAnim_4);
		bool L_1 = (((SCR_Collectible_t331 *)__this)->___isTaken_3);
		NullCheck(L_0);
		Animator_SetBool_m624(L_0, (String_t*) &_stringLiteral324, L_1, /*hidden argument*/NULL);
		SCR_PointManager_t381 * L_2 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_PointManager_AcquireCollectible_m1466(L_2, 0, /*hidden argument*/NULL);
		Collider_t138 * L_3 = Component_GetComponent_TisCollider_t138_m883(__this, /*hidden argument*/Component_GetComponent_TisCollider_t138_m883_MethodInfo_var);
		NullCheck(L_3);
		Collider_set_enabled_m1747(L_3, 0, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral325, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

struct Component_t219;
struct Button_t324;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
#define Component_GetComponent_TisButton_t324_m1775(__this, method) (( Button_t324 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct Image_t48;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t48_m724(__this, method) (( Image_t48 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void SCR_FlyButton::.ctor()
extern "C" void SCR_FlyButton__ctor_m1338 (SCR_FlyButton_t345 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_FlyButton::Start()
extern const MethodInfo* Component_GetComponent_TisButton_t324_m1775_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisImage_t48_m724_MethodInfo_var;
extern "C" void SCR_FlyButton_Start_m1339 (SCR_FlyButton_t345 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisButton_t324_m1775_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483717);
		Component_GetComponent_TisImage_t48_m724_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_FlyButton_t345 * L_1 = SCR_ManagerComponents_get_FlyButton_m1447(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_3 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_ManagerComponents_set_FlyButton_m1448(L_3, __this, /*hidden argument*/NULL);
		GameObject_t78 * L_4 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m1602(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_003c;
	}

IL_0030:
	{
		GameObject_t78 * L_5 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}

IL_003c:
	{
		Button_t324 * L_6 = Component_GetComponent_TisButton_t324_m1775(__this, /*hidden argument*/Component_GetComponent_TisButton_t324_m1775_MethodInfo_var);
		__this->___button_4 = L_6;
		Image_t48 * L_7 = Component_GetComponent_TisImage_t48_m724(__this, /*hidden argument*/Component_GetComponent_TisImage_t48_m724_MethodInfo_var);
		__this->___image_5 = L_7;
		__this->___shown_3 = 1;
		return;
	}
}
// System.Void SCR_FlyButton::Update()
extern "C" void SCR_FlyButton_Update_m1340 (SCR_FlyButton_t345 * __this, const MethodInfo* method)
{
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Camie_t338 * L_1 = SCR_ManagerComponents_get_AvatarMove_m1441(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		bool L_3 = (__this->___shown_3);
		SCR_ManagerComponents_t380 * L_4 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		SCR_Camie_t338 * L_5 = SCR_ManagerComponents_get_AvatarMove_m1441(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = SCR_Camie_get_CanFly_m1262(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)L_6)))
		{
			goto IL_0051;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_7 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		SCR_Camie_t338 * L_8 = SCR_ManagerComponents_get_AvatarMove_m1441(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = SCR_Camie_get_CanFly_m1262(L_8, /*hidden argument*/NULL);
		__this->___shown_3 = L_9;
		bool L_10 = (__this->___shown_3);
		SCR_FlyButton_ShowButton_m1344(__this, L_10, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void SCR_FlyButton::OnDisable()
extern "C" void SCR_FlyButton_OnDisable_m1341 (SCR_FlyButton_t345 * __this, const MethodInfo* method)
{
	{
		__this->___isPressed_2 = 0;
		return;
	}
}
// System.Void SCR_FlyButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" void SCR_FlyButton_OnPointerDown_m1342 (SCR_FlyButton_t345 * __this, PointerEventData_t215 * ___eventData, const MethodInfo* method)
{
	{
		__this->___isPressed_2 = 1;
		return;
	}
}
// System.Void SCR_FlyButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" void SCR_FlyButton_OnPointerUp_m1343 (SCR_FlyButton_t345 * __this, PointerEventData_t215 * ___eventData, const MethodInfo* method)
{
	{
		__this->___isPressed_2 = 0;
		return;
	}
}
// System.Void SCR_FlyButton::ShowButton(System.Boolean)
extern "C" void SCR_FlyButton_ShowButton_m1344 (SCR_FlyButton_t345 * __this, bool ___show, const MethodInfo* method)
{
	{
		Image_t48 * L_0 = (__this->___image_5);
		bool L_1 = ___show;
		NullCheck(L_0);
		Behaviour_set_enabled_m766(L_0, L_1, /*hidden argument*/NULL);
		Button_t324 * L_2 = (__this->___button_4);
		bool L_3 = ___show;
		NullCheck(L_2);
		Behaviour_set_enabled_m766(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SCR_FlyButton::get_IsPressed()
extern "C" bool SCR_FlyButton_get_IsPressed_m1345 (SCR_FlyButton_t345 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isPressed_2);
		return L_0;
	}
}
// System.Void SCR_FlyButton::set_IsPressed(System.Boolean)
extern "C" void SCR_FlyButton_set_IsPressed_m1346 (SCR_FlyButton_t345 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___isPressed_2 = L_0;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// SCR_Joystick/Direction
#include "AssemblyU2DCSharp_SCR_Joystick_DirectionMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif

// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// SCR_JoystickDirection
#include "AssemblyU2DCSharp_SCR_JoystickDirection.h"
struct Component_t219;
struct RectTransform_t364;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t364_m1776(__this, method) (( RectTransform_t364 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct SCR_JoystickDirectionU5BU5D_t365;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<SCR_JoystickDirection>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<SCR_JoystickDirection>()
#define Component_GetComponentsInChildren_TisSCR_JoystickDirection_t366_m1777(__this, method) (( SCR_JoystickDirectionU5BU5D_t365* (*) (Component_t219 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m698_gshared)(__this, method)


// System.Void SCR_Joystick::.ctor()
extern "C" void SCR_Joystick__ctor_m1347 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Joystick::Start()
extern TypeInfo* IEnumerator_t217_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t1_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t233_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisImage_t48_m724_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t364_m1776_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisSCR_JoystickDirection_t366_m1777_MethodInfo_var;
extern "C" void SCR_Joystick_Start_m1348 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t217_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		Transform_t1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		IDisposable_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		Component_GetComponent_TisImage_t48_m724_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		Component_GetComponent_TisRectTransform_t364_m1776_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483718);
		Component_GetComponentsInChildren_TisSCR_JoystickDirection_t366_m1777_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483719);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1 * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Joystick_t346 * L_1 = SCR_ManagerComponents_get_Joystick_m1445(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_3 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_ManagerComponents_set_Joystick_m1446(L_3, __this, /*hidden argument*/NULL);
		GameObject_t78 * L_4 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m1602(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_003c;
	}

IL_0030:
	{
		GameObject_t78 * L_5 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}

IL_003c:
	{
		Image_t48 * L_6 = Component_GetComponent_TisImage_t48_m724(__this, /*hidden argument*/Component_GetComponent_TisImage_t48_m724_MethodInfo_var);
		__this->___img_11 = L_6;
		SCR_ManagerComponents_t380 * L_7 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		SCR_Camie_t338 * L_8 = SCR_ManagerComponents_get_AvatarMove_m1441(L_7, /*hidden argument*/NULL);
		__this->___avatar_10 = L_8;
		RectTransform_t364 * L_9 = Component_GetComponent_TisRectTransform_t364_m1776(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t364_m1776_MethodInfo_var);
		__this->___rectTransform_7 = L_9;
		Transform_t1 * L_10 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t1 * L_11 = Transform_get_parent_m676(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Object_t * L_12 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_11);
		V_1 = L_12;
	}

IL_0075:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a7;
		}

IL_007a:
		{
			Object_t * L_13 = V_1;
			NullCheck(L_13);
			Object_t * L_14 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t217_il2cpp_TypeInfo_var, L_13);
			V_0 = ((Transform_t1 *)Castclass(L_14, Transform_t1_il2cpp_TypeInfo_var));
			Transform_t1 * L_15 = V_0;
			NullCheck(L_15);
			String_t* L_16 = Object_get_name_m798(L_15, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_17 = String_op_Equality_m636(NULL /*static, unused*/, L_16, (String_t*) &_stringLiteral326, /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_00a7;
			}
		}

IL_009b:
		{
			Transform_t1 * L_18 = V_0;
			__this->___center_8 = L_18;
			goto IL_00b2;
		}

IL_00a7:
		{
			Object_t * L_19 = V_1;
			NullCheck(L_19);
			bool L_20 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t217_il2cpp_TypeInfo_var, L_19);
			if (L_20)
			{
				goto IL_007a;
			}
		}

IL_00b2:
		{
			IL2CPP_LEAVE(0xC9, FINALLY_00b7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_00b7;
	}

FINALLY_00b7:
	{ // begin finally (depth: 1)
		{
			Object_t * L_21 = V_1;
			V_2 = ((Object_t *)IsInst(L_21, IDisposable_t233_il2cpp_TypeInfo_var));
			Object_t * L_22 = V_2;
			if (L_22)
			{
				goto IL_00c2;
			}
		}

IL_00c1:
		{
			IL2CPP_END_FINALLY(183)
		}

IL_00c2:
		{
			Object_t * L_23 = V_2;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t233_il2cpp_TypeInfo_var, L_23);
			IL2CPP_END_FINALLY(183)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(183)
	{
		IL2CPP_JUMP_TBL(0xC9, IL_00c9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_00c9:
	{
		Transform_t1 * L_24 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t1 * L_25 = Transform_get_parent_m676(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		SCR_JoystickDirectionU5BU5D_t365* L_26 = Component_GetComponentsInChildren_TisSCR_JoystickDirection_t366_m1777(L_25, /*hidden argument*/Component_GetComponentsInChildren_TisSCR_JoystickDirection_t366_m1777_MethodInfo_var);
		__this->___directionPoints_12 = L_26;
		__this->___activeDirection_14 = 1;
		__this->___pointingTo_13 = 1;
		return;
	}
}
// System.Void SCR_Joystick::OnLevelWasLoaded()
extern "C" void SCR_Joystick_OnLevelWasLoaded_m1349 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Camie_t338 * L_1 = SCR_ManagerComponents_get_AvatarMove_m1441(L_0, /*hidden argument*/NULL);
		__this->___avatar_10 = L_1;
		__this->___activeDirection_14 = 1;
		__this->___pointingTo_13 = 1;
		__this->___isDragging_2 = 0;
		return;
	}
}
// System.Void SCR_Joystick::OnDisable()
extern "C" void SCR_Joystick_OnDisable_m1350 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	{
		__this->___isDragging_2 = 0;
		return;
	}
}
// System.Void SCR_Joystick::Update()
extern TypeInfo* Input_t220_il2cpp_TypeInfo_var;
extern "C" void SCR_Joystick_Update_m1351 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t235  V_0 = {0};
	TouchU5BU5D_t234* V_1 = {0};
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector3_t4  V_5 = {0};
	Vector3_t4  V_6 = {0};
	Vector3_t4  V_7 = {0};
	Vector3_t4  V_8 = {0};
	Vector3_t4  V_9 = {0};
	Vector3_t4  V_10 = {0};
	{
		SCR_Camie_t338 * L_0 = (__this->___avatar_10);
		NullCheck(L_0);
		SCR_Respawn_t344 * L_1 = SCR_Camie_get_Respawner_m1269(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = SCR_Respawn_get_IsRespawning_m1283(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		__this->___isDragging_2 = 0;
	}

IL_001c:
	{
		bool L_3 = (__this->___isDragging_2);
		if (!L_3)
		{
			goto IL_01ca;
		}
	}
	{
		bool L_4 = Application_get_isEditor_m1759(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		Vector3_t4  L_5 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_5;
		float L_6 = ((&V_5)->___x_1);
		Vector3_t4  L_7 = Input_get_mousePosition_m720(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_7;
		float L_8 = ((&V_6)->___y_2);
		Vector2_t6  L_9 = {0};
		Vector2__ctor_m630(&L_9, L_6, L_8, /*hidden argument*/NULL);
		__this->___currTouchPosition_5 = L_9;
		goto IL_00aa;
	}

IL_005d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t220_il2cpp_TypeInfo_var);
		TouchU5BU5D_t234* L_10 = Input_get_touches_m728(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_10;
		V_2 = 0;
		goto IL_00a1;
	}

IL_006a:
	{
		TouchU5BU5D_t234* L_11 = V_1;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		V_0 = (*(Touch_t235 *)((Touch_t235 *)(Touch_t235 *)SZArrayLdElema(L_11, L_12)));
		int32_t L_13 = Touch_get_fingerId_m1778((&V_0), /*hidden argument*/NULL);
		int32_t L_14 = (__this->___touchId_4);
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_009d;
		}
	}
	{
		Vector2_t6  L_15 = Touch_get_position_m729((&V_0), /*hidden argument*/NULL);
		__this->___currTouchPosition_5 = L_15;
		Touch_t235  L_16 = V_0;
		__this->___currTouch_3 = L_16;
	}

IL_009d:
	{
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_00a1:
	{
		int32_t L_18 = V_2;
		TouchU5BU5D_t234* L_19 = V_1;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)(((Array_t *)L_19)->max_length))))))
		{
			goto IL_006a;
		}
	}

IL_00aa:
	{
		SCR_Joystick_FollowTouch_m1354(__this, /*hidden argument*/NULL);
		SCR_Joystick_AdjustPosition_m1355(__this, /*hidden argument*/NULL);
		int32_t L_20 = SCR_Joystick_GetClosestDirection_m1357(__this, /*hidden argument*/NULL);
		__this->___pointingTo_13 = L_20;
		int32_t L_21 = (__this->___pointingTo_13);
		int32_t L_22 = (__this->___activeDirection_14);
		if ((((int32_t)L_21) == ((int32_t)L_22)))
		{
			goto IL_00e5;
		}
	}
	{
		int32_t L_23 = (__this->___pointingTo_13);
		int32_t L_24 = SCR_Joystick_SwitchDirections_m1358(__this, L_23, /*hidden argument*/NULL);
		__this->___activeDirection_14 = L_24;
	}

IL_00e5:
	{
		bool L_25 = Application_get_isEditor_m1759(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_0118;
		}
	}
	{
		Touch_t235 * L_26 = &(__this->___currTouch_3);
		int32_t L_27 = Touch_get_phase_m1687(L_26, /*hidden argument*/NULL);
		if ((((int32_t)L_27) == ((int32_t)4)))
		{
			goto IL_0111;
		}
	}
	{
		Touch_t235 * L_28 = &(__this->___currTouch_3);
		int32_t L_29 = Touch_get_phase_m1687(L_28, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_29) == ((uint32_t)3))))
		{
			goto IL_0118;
		}
	}

IL_0111:
	{
		__this->___isDragging_2 = 0;
	}

IL_0118:
	{
		Transform_t1 * L_30 = (__this->___center_8);
		NullCheck(L_30);
		Vector3_t4  L_31 = Transform_get_position_m593(L_30, /*hidden argument*/NULL);
		V_7 = L_31;
		float L_32 = ((&V_7)->___x_1);
		Transform_t1 * L_33 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t4  L_34 = Transform_get_position_m593(L_33, /*hidden argument*/NULL);
		V_8 = L_34;
		float L_35 = ((&V_8)->___x_1);
		float L_36 = (__this->___radius_6);
		V_3 = ((float)((float)((-((float)((float)L_32-(float)L_35))))/(float)L_36));
		Transform_t1 * L_37 = (__this->___center_8);
		NullCheck(L_37);
		Vector3_t4  L_38 = Transform_get_position_m593(L_37, /*hidden argument*/NULL);
		V_9 = L_38;
		float L_39 = ((&V_9)->___y_2);
		Transform_t1 * L_40 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t4  L_41 = Transform_get_position_m593(L_40, /*hidden argument*/NULL);
		V_10 = L_41;
		float L_42 = ((&V_10)->___y_2);
		float L_43 = (__this->___radius_6);
		V_4 = ((float)((float)((-((float)((float)L_39-(float)L_42))))/(float)L_43));
		float L_44 = V_3;
		float L_45 = V_4;
		Vector3_t4  L_46 = {0};
		Vector3__ctor_m612(&L_46, L_44, L_45, (0.0f), /*hidden argument*/NULL);
		__this->___axis_9 = L_46;
		SCR_ManagerComponents_t380 * L_47 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		SCR_Camie_t338 * L_48 = SCR_ManagerComponents_get_AvatarMove_m1441(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		bool L_49 = SCR_Camie_get_IsGrounded_m1260(L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_01c5;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_50 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_50);
		SCR_FlyButton_t345 * L_51 = SCR_ManagerComponents_get_FlyButton_m1447(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		bool L_52 = SCR_FlyButton_get_IsPressed_m1345(L_51, /*hidden argument*/NULL);
		if (L_52)
		{
			goto IL_01c5;
		}
	}
	{
		Vector3_t4  L_53 = (__this->___axis_9);
		SCR_Joystick_ToAvatarAngle_m1356(__this, L_53, /*hidden argument*/NULL);
	}

IL_01c5:
	{
		goto IL_020c;
	}

IL_01ca:
	{
		RectTransform_t364 * L_54 = (__this->___rectTransform_7);
		NullCheck(L_54);
		Vector3_t4  L_55 = Transform_get_position_m593(L_54, /*hidden argument*/NULL);
		Transform_t1 * L_56 = (__this->___center_8);
		NullCheck(L_56);
		Vector3_t4  L_57 = Transform_get_position_m593(L_56, /*hidden argument*/NULL);
		bool L_58 = Vector3_op_Inequality_m722(NULL /*static, unused*/, L_55, L_57, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_020c;
		}
	}
	{
		RectTransform_t364 * L_59 = (__this->___rectTransform_7);
		Transform_t1 * L_60 = (__this->___center_8);
		NullCheck(L_60);
		Vector3_t4  L_61 = Transform_get_position_m593(L_60, /*hidden argument*/NULL);
		NullCheck(L_59);
		Transform_set_position_m607(L_59, L_61, /*hidden argument*/NULL);
		int32_t L_62 = SCR_Joystick_GetClosestDirection_m1357(__this, /*hidden argument*/NULL);
		__this->___pointingTo_13 = L_62;
	}

IL_020c:
	{
		return;
	}
}
// System.Void SCR_Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" void SCR_Joystick_OnPointerDown_m1352 (SCR_Joystick_t346 * __this, PointerEventData_t215 * ___eventData, const MethodInfo* method)
{
	{
		__this->___isDragging_2 = 1;
		PointerEventData_t215 * L_0 = ___eventData;
		NullCheck(L_0);
		Vector2_t6  L_1 = PointerEventData_get_position_m711(L_0, /*hidden argument*/NULL);
		__this->___currTouchPosition_5 = L_1;
		PointerEventData_t215 * L_2 = ___eventData;
		NullCheck(L_2);
		int32_t L_3 = PointerEventData_get_pointerId_m725(L_2, /*hidden argument*/NULL);
		__this->___touchId_4 = L_3;
		return;
	}
}
// System.Void SCR_Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" void SCR_Joystick_OnPointerUp_m1353 (SCR_Joystick_t346 * __this, PointerEventData_t215 * ___eventData, const MethodInfo* method)
{
	{
		__this->___isDragging_2 = 0;
		return;
	}
}
// System.Void SCR_Joystick::FollowTouch()
extern "C" void SCR_Joystick_FollowTouch_m1354 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	{
		Vector2_t6 * L_0 = &(__this->___currTouchPosition_5);
		float L_1 = (L_0->___x_1);
		Vector2_t6 * L_2 = &(__this->___currTouchPosition_5);
		float L_3 = (L_2->___y_2);
		Vector3__ctor_m612((&V_0), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		RectTransform_t364 * L_4 = (__this->___rectTransform_7);
		Vector3_t4  L_5 = V_0;
		NullCheck(L_4);
		Transform_set_position_m607(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Joystick::AdjustPosition()
extern "C" void SCR_Joystick_AdjustPosition_m1355 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	{
		RectTransform_t364 * L_0 = (__this->___rectTransform_7);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t4  L_2 = V_0;
		Transform_t1 * L_3 = (__this->___center_8);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		float L_5 = Vector3_Distance_m1750(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		float L_6 = (__this->___radius_6);
		if ((!(((float)L_5) >= ((float)L_6))))
		{
			goto IL_0069;
		}
	}
	{
		Vector3_t4  L_7 = V_0;
		Transform_t1 * L_8 = (__this->___center_8);
		NullCheck(L_8);
		Vector3_t4  L_9 = Transform_get_position_m593(L_8, /*hidden argument*/NULL);
		Vector3_t4  L_10 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		Transform_t1 * L_11 = (__this->___center_8);
		NullCheck(L_11);
		Vector3_t4  L_12 = Transform_get_position_m593(L_11, /*hidden argument*/NULL);
		Vector3_t4  L_13 = Vector3_get_normalized_m648((&V_1), /*hidden argument*/NULL);
		float L_14 = (__this->___radius_6);
		Vector3_t4  L_15 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector3_t4  L_16 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		RectTransform_t364 * L_17 = (__this->___rectTransform_7);
		Vector3_t4  L_18 = V_0;
		NullCheck(L_17);
		Transform_set_position_m607(L_17, L_18, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return;
	}
}
// UnityEngine.Vector3 SCR_Joystick::ToAvatarAngle(UnityEngine.Vector3)
extern "C" Vector3_t4  SCR_Joystick_ToAvatarAngle_m1356 (SCR_Joystick_t346 * __this, Vector3_t4  ___vector, const MethodInfo* method)
{
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Camie_t338 * L_1 = SCR_ManagerComponents_get_AvatarMove_m1441(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1 * L_2 = Component_get_transform_m594(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Quaternion_t19  L_3 = Transform_get_rotation_m656(L_2, /*hidden argument*/NULL);
		Vector3_t4  L_4 = ___vector;
		Vector3_t4  L_5 = Quaternion_op_Multiply_m891(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// SCR_Joystick/Direction SCR_Joystick::GetClosestDirection()
extern "C" int32_t SCR_Joystick_GetClosestDirection_m1357 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	SCR_JoystickDirection_t366 * V_1 = {0};
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		SCR_JoystickDirectionU5BU5D_t365* L_0 = (__this->___directionPoints_12);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		V_1 = (*(SCR_JoystickDirection_t366 **)(SCR_JoystickDirection_t366 **)SZArrayLdElema(L_0, L_1));
		V_2 = 0;
		goto IL_004f;
	}

IL_0016:
	{
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4  L_3 = Transform_get_position_m593(L_2, /*hidden argument*/NULL);
		SCR_JoystickDirectionU5BU5D_t365* L_4 = (__this->___directionPoints_12);
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((*(SCR_JoystickDirection_t366 **)(SCR_JoystickDirection_t366 **)SZArrayLdElema(L_4, L_6)));
		Transform_t1 * L_7 = Component_get_transform_m594((*(SCR_JoystickDirection_t366 **)(SCR_JoystickDirection_t366 **)SZArrayLdElema(L_4, L_6)), /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4  L_8 = Transform_get_position_m593(L_7, /*hidden argument*/NULL);
		float L_9 = Vector3_Distance_m1750(NULL /*static, unused*/, L_3, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		float L_10 = V_3;
		float L_11 = V_0;
		if ((!(((float)L_10) < ((float)L_11))))
		{
			goto IL_004b;
		}
	}
	{
		float L_12 = V_3;
		V_0 = L_12;
		SCR_JoystickDirectionU5BU5D_t365* L_13 = (__this->___directionPoints_12);
		int32_t L_14 = V_2;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		V_1 = (*(SCR_JoystickDirection_t366 **)(SCR_JoystickDirection_t366 **)SZArrayLdElema(L_13, L_15));
	}

IL_004b:
	{
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_004f:
	{
		int32_t L_17 = V_2;
		SCR_JoystickDirectionU5BU5D_t365* L_18 = (__this->___directionPoints_12);
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)(((Array_t *)L_18)->max_length))))))
		{
			goto IL_0016;
		}
	}
	{
		SCR_JoystickDirection_t366 * L_19 = V_1;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___myDirection_2);
		return L_20;
	}
}
// SCR_Joystick/Direction SCR_Joystick::SwitchDirections(SCR_Joystick/Direction)
extern "C" int32_t SCR_Joystick_SwitchDirections_m1358 (SCR_Joystick_t346 * __this, int32_t ___newDir, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		V_0 = 1;
		int32_t L_0 = ___newDir;
		V_1 = L_0;
		int32_t L_1 = V_1;
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_0022;
	}

IL_0016:
	{
		goto IL_0022;
	}

IL_001b:
	{
		V_0 = 0;
		goto IL_0022;
	}

IL_0022:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean SCR_Joystick::get_IsDragging()
extern "C" bool SCR_Joystick_get_IsDragging_m1359 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isDragging_2);
		return L_0;
	}
}
// System.Single SCR_Joystick::get_HorizontalAxis()
extern "C" float SCR_Joystick_get_HorizontalAxis_m1360 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	{
		Vector3_t4 * L_0 = &(__this->___axis_9);
		float L_1 = (L_0->___x_1);
		return L_1;
	}
}
// System.Single SCR_Joystick::get_VerticalAxis()
extern "C" float SCR_Joystick_get_VerticalAxis_m1361 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	{
		Vector3_t4 * L_0 = &(__this->___axis_9);
		float L_1 = (L_0->___y_2);
		return L_1;
	}
}
// SCR_Joystick/Direction SCR_Joystick::get_PointingTo()
extern "C" int32_t SCR_Joystick_get_PointingTo_m1362 (SCR_Joystick_t346 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___pointingTo_13);
		return L_0;
	}
}
#ifndef _MSC_VER
#else
#endif
// SCR_JoystickDirection
#include "AssemblyU2DCSharp_SCR_JoystickDirectionMethodDeclarations.h"



// System.Void SCR_JoystickDirection::.ctor()
extern "C" void SCR_JoystickDirection__ctor_m1363 (SCR_JoystickDirection_t366 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// SCR_LevelEndMenu
#include "AssemblyU2DCSharp_SCR_LevelEndMenu.h"
#ifndef _MSC_VER
#else
#endif
// SCR_LevelEndMenu
#include "AssemblyU2DCSharp_SCR_LevelEndMenuMethodDeclarations.h"

#include "UnityEngine.UI_ArrayTypes.h"
struct GameObject_t78;
struct Image_t48;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t48_m1779(__this, method) (( Image_t48 * (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m771_gshared)(__this, method)


// System.Void SCR_LevelEndMenu::.ctor()
extern TypeInfo* TextU5BU5D_t369_il2cpp_TypeInfo_var;
extern "C" void SCR_LevelEndMenu__ctor_m1364 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextU5BU5D_t369_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(689);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___newHighScoreText_8 = ((TextU5BU5D_t369*)SZArrayNew(TextU5BU5D_t369_il2cpp_TypeInfo_var, 2));
		SCR_Menu__ctor_m1204(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_LevelEndMenu::Awake()
extern TypeInfo* SCR_LevelEndMenu_t367_il2cpp_TypeInfo_var;
extern "C" void SCR_LevelEndMenu_Awake_m1365 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_LevelEndMenu_t367_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(690);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_GUIManager_t378 * L_0 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_LevelEndMenu_t367 * L_1 = SCR_GUIManager_get_LevelEndMenu_m1414(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, __this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		((SCR_LevelEndMenu_t367_StaticFields*)SCR_LevelEndMenu_t367_il2cpp_TypeInfo_var->static_fields)->___instance_5 = __this;
		GameObject_t78 * L_3 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m1602(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_002b:
	{
		GameObject_t78 * L_4 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		SCR_Menu_Awake_m1205(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_LevelEndMenu::OnEnable()
extern const MethodInfo* GameObject_GetComponent_TisText_t316_m1741_MethodInfo_var;
extern "C" void SCR_LevelEndMenu_OnEnable_m1366 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisText_t316_m1741_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483708);
		s_Il2CppMethodIntialized = true;
	}
	SCR_LevelEndMenu_t367 * G_B4_0 = {0};
	SCR_LevelEndMenu_t367 * G_B3_0 = {0};
	int32_t G_B5_0 = 0;
	SCR_LevelEndMenu_t367 * G_B5_1 = {0};
	{
		SCR_GUIManager_t378 * L_0 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		SCR_Menu_OnEnable_m1206(__this, /*hidden argument*/NULL);
		TextU5BU5D_t369* L_2 = (__this->___newHighScoreText_8);
		GameObject_t78 * L_3 = SCR_Menu_GetMenuItem_m1210(__this, (String_t*) &_stringLiteral327, /*hidden argument*/NULL);
		NullCheck(L_3);
		Text_t316 * L_4 = GameObject_GetComponent_TisText_t316_m1741(L_3, /*hidden argument*/GameObject_GetComponent_TisText_t316_m1741_MethodInfo_var);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_4);
		*((Text_t316 **)(Text_t316 **)SZArrayLdElema(L_2, 0)) = (Text_t316 *)L_4;
		TextU5BU5D_t369* L_5 = (__this->___newHighScoreText_8);
		GameObject_t78 * L_6 = SCR_Menu_GetMenuItem_m1210(__this, (String_t*) &_stringLiteral328, /*hidden argument*/NULL);
		NullCheck(L_6);
		Text_t316 * L_7 = GameObject_GetComponent_TisText_t316_m1741(L_6, /*hidden argument*/GameObject_GetComponent_TisText_t316_m1741_MethodInfo_var);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_7);
		*((Text_t316 **)(Text_t316 **)SZArrayLdElema(L_5, 1)) = (Text_t316 *)L_7;
		SCR_LevelManager_t379 * L_8 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = SCR_LevelManager_get_GameplayLevel_m1422(L_8, /*hidden argument*/NULL);
		SpriteU5BU5D_t368* L_10 = (__this->___lockedLevels_6);
		NullCheck(L_10);
		G_B3_0 = __this;
		if ((((int32_t)L_9) >= ((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_10)->max_length)))-(int32_t)1)))))
		{
			G_B4_0 = __this;
			goto IL_0086;
		}
	}
	{
		SCR_PointManager_t381 * L_11 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = SCR_PointManager_get_ScoreCumul_m1475(L_11, /*hidden argument*/NULL);
		SCR_PointManager_t381 * L_13 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		Int32U5BU5D_t242* L_14 = SCR_PointManager_get_PointsForLevelAccess_m1476(L_13, /*hidden argument*/NULL);
		SCR_LevelManager_t379 * L_15 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = SCR_LevelManager_get_GameplayLevel_m1422(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		G_B5_0 = ((((int32_t)L_12) > ((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_17))))? 1 : 0);
		G_B5_1 = G_B3_0;
		goto IL_0087;
	}

IL_0086:
	{
		G_B5_0 = 0;
		G_B5_1 = G_B4_0;
	}

IL_0087:
	{
		NullCheck(G_B5_1);
		G_B5_1->___canGoToNext_9 = G_B5_0;
		SCR_LevelEndMenu_AssignButtonImages_m1367(__this, /*hidden argument*/NULL);
		SCR_PointManager_t381 * L_18 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = SCR_PointManager_get_TotalScore_m1472(L_18, /*hidden argument*/NULL);
		SCR_PointManager_t381 * L_20 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		Int32U5BU5D_t242* L_21 = SCR_PointManager_get_HighScores_m1473(L_20, /*hidden argument*/NULL);
		SCR_LevelManager_t379 * L_22 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = SCR_LevelManager_get_GameplayLevel_m1422(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_23);
		int32_t L_24 = L_23;
		if ((!(((uint32_t)L_19) == ((uint32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_21, L_24))))))
		{
			goto IL_00bc;
		}
	}
	{
		SCR_LevelEndMenu_NewHighScore_m1370(__this, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		return;
	}
}
// System.Void SCR_LevelEndMenu::AssignButtonImages()
extern const MethodInfo* GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var;
extern "C" void SCR_LevelEndMenu_AssignButtonImages_m1367 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483720);
		s_Il2CppMethodIntialized = true;
	}
	Image_t48 * V_0 = {0};
	Image_t48 * V_1 = {0};
	Image_t48 * G_B6_0 = {0};
	Image_t48 * G_B5_0 = {0};
	Sprite_t329 * G_B7_0 = {0};
	Image_t48 * G_B7_1 = {0};
	{
		GameObject_t78 * L_0 = SCR_Menu_GetMenuItem_m1210(__this, (String_t*) &_stringLiteral329, /*hidden argument*/NULL);
		NullCheck(L_0);
		Image_t48 * L_1 = GameObject_GetComponent_TisImage_t48_m1779(L_0, /*hidden argument*/GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var);
		V_0 = L_1;
		bool L_2 = (__this->___canGoToNext_9);
		if (!L_2)
		{
			goto IL_003e;
		}
	}
	{
		GameObject_t78 * L_3 = SCR_Menu_GetMenuItem_m1210(__this, (String_t*) &_stringLiteral330, /*hidden argument*/NULL);
		NullCheck(L_3);
		Image_t48 * L_4 = GameObject_GetComponent_TisImage_t48_m1779(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var);
		V_1 = L_4;
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral331, 0, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_003e:
	{
		GameObject_t78 * L_5 = SCR_Menu_GetMenuItem_m1210(__this, (String_t*) &_stringLiteral331, /*hidden argument*/NULL);
		NullCheck(L_5);
		Image_t48 * L_6 = GameObject_GetComponent_TisImage_t48_m1779(L_5, /*hidden argument*/GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var);
		V_1 = L_6;
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral330, 0, /*hidden argument*/NULL);
	}

IL_005b:
	{
		SCR_LevelManager_t379 * L_7 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = SCR_LevelManager_get_GameplayLevel_m1422(L_7, /*hidden argument*/NULL);
		SpriteU5BU5D_t368* L_9 = (__this->___lockedLevels_6);
		NullCheck(L_9);
		if ((((int32_t)L_8) >= ((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_9)->max_length)))-(int32_t)1)))))
		{
			goto IL_00ca;
		}
	}
	{
		Image_t48 * L_10 = V_0;
		SpriteU5BU5D_t368* L_11 = (__this->___unlockedLevels_7);
		SCR_LevelManager_t379 * L_12 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = SCR_LevelManager_get_GameplayLevel_m1422(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_13-(int32_t)1)));
		int32_t L_14 = ((int32_t)((int32_t)L_13-(int32_t)1));
		NullCheck(L_10);
		Image_set_sprite_m1735(L_10, (*(Sprite_t329 **)(Sprite_t329 **)SZArrayLdElema(L_11, L_14)), /*hidden argument*/NULL);
		Image_t48 * L_15 = V_1;
		bool L_16 = (__this->___canGoToNext_9);
		G_B5_0 = L_15;
		if (!L_16)
		{
			G_B6_0 = L_15;
			goto IL_00af;
		}
	}
	{
		SpriteU5BU5D_t368* L_17 = (__this->___unlockedLevels_7);
		SCR_LevelManager_t379 * L_18 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = SCR_LevelManager_get_GameplayLevel_m1422(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_19);
		int32_t L_20 = L_19;
		G_B7_0 = (*(Sprite_t329 **)(Sprite_t329 **)SZArrayLdElema(L_17, L_20));
		G_B7_1 = G_B5_0;
		goto IL_00c0;
	}

IL_00af:
	{
		SpriteU5BU5D_t368* L_21 = (__this->___lockedLevels_6);
		SCR_LevelManager_t379 * L_22 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = SCR_LevelManager_get_GameplayLevel_m1422(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_23);
		int32_t L_24 = L_23;
		G_B7_0 = (*(Sprite_t329 **)(Sprite_t329 **)SZArrayLdElema(L_21, L_24));
		G_B7_1 = G_B6_0;
	}

IL_00c0:
	{
		NullCheck(G_B7_1);
		Image_set_sprite_m1735(G_B7_1, G_B7_0, /*hidden argument*/NULL);
		goto IL_00d7;
	}

IL_00ca:
	{
		Image_t48 * L_25 = V_1;
		NullCheck(L_25);
		String_t* L_26 = Object_get_name_m798(L_25, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, L_26, 0, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.Void SCR_LevelEndMenu::Reload()
extern "C" void SCR_LevelEndMenu_Reload_m1368 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_loadedLevel_m1012(NULL /*static, unused*/, /*hidden argument*/NULL);
		SCR_Menu_LoadLevel_m1219(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_LevelEndMenu::LoadNext()
extern "C" void SCR_LevelEndMenu_LoadNext_m1369 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_loadedLevel_m1012(NULL /*static, unused*/, /*hidden argument*/NULL);
		SCR_Menu_LoadLevel_m1219(__this, ((int32_t)((int32_t)L_0+(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_LevelEndMenu::NewHighScore()
extern "C" void SCR_LevelEndMenu_NewHighScore_m1370 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method)
{
	Text_t316 * V_0 = {0};
	TextU5BU5D_t369* V_1 = {0};
	int32_t V_2 = 0;
	{
		SCR_Menu_SetMenuLanguage_m1217(__this, /*hidden argument*/NULL);
		TextU5BU5D_t369* L_0 = (__this->___newHighScoreText_8);
		V_1 = L_0;
		V_2 = 0;
		goto IL_0040;
	}

IL_0014:
	{
		TextU5BU5D_t369* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(Text_t316 **)(Text_t316 **)SZArrayLdElema(L_1, L_3));
		Text_t316 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = Behaviour_get_isActiveAndEnabled_m1740(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		Text_t316 * L_6 = V_0;
		Object_t * L_7 = SCR_Menu_AlphaLerp_m1212(__this, L_6, (1.0f), 1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1006(__this, L_7, /*hidden argument*/NULL);
		goto IL_0049;
	}

IL_003c:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_9 = V_2;
		TextU5BU5D_t369* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_0014;
		}
	}

IL_0049:
	{
		return;
	}
}
// SCR_LevelStartCountdown/<Rescale>c__IteratorA
#include "AssemblyU2DCSharp_SCR_LevelStartCountdown_U3CRescaleU3Ec__It.h"
#ifndef _MSC_VER
#else
#endif
// SCR_LevelStartCountdown/<Rescale>c__IteratorA
#include "AssemblyU2DCSharp_SCR_LevelStartCountdown_U3CRescaleU3Ec__ItMethodDeclarations.h"

// SCR_LevelStartCountdown
#include "AssemblyU2DCSharp_SCR_LevelStartCountdown.h"
// SCR_LevelStartCountdown
#include "AssemblyU2DCSharp_SCR_LevelStartCountdownMethodDeclarations.h"


// System.Void SCR_LevelStartCountdown/<Rescale>c__IteratorA::.ctor()
extern "C" void U3CRescaleU3Ec__IteratorA__ctor_m1371 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_LevelStartCountdown/<Rescale>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CRescaleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1372 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Object SCR_LevelStartCountdown/<Rescale>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CRescaleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1373 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Boolean SCR_LevelStartCountdown/<Rescale>c__IteratorA::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CRescaleU3Ec__IteratorA_MoveNext_m1374 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4  V_1 = {0};
	bool V_2 = false;
	{
		int32_t L_0 = (__this->___U24PC_3);
		V_0 = L_0;
		__this->___U24PC_3 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_00cb;
		}
		if (L_1 == 2)
		{
			goto IL_0161;
		}
	}
	{
		goto IL_01e6;
	}

IL_0025:
	{
		__this->___U3CtimeU3E__0_0 = (0.0f);
		bool L_2 = (__this->___up_1);
		if (!L_2)
		{
			goto IL_00f7;
		}
	}
	{
		SCR_LevelStartCountdown_t370 * L_3 = (__this->___U3CU3Ef__this_7);
		Transform_t1 * L_4 = (__this->___objToScale_2);
		NullCheck(L_4);
		String_t* L_5 = Object_get_name_m798(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_Menu_ShowMenuItem_m1208(L_3, L_5, 1, /*hidden argument*/NULL);
		Transform_t1 * L_6 = (__this->___objToScale_2);
		Vector3_t4  L_7 = {0};
		Vector3__ctor_m612(&L_7, (0.1f), (0.1f), (0.1f), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localScale_m634(L_6, L_7, /*hidden argument*/NULL);
		goto IL_00cb;
	}

IL_0076:
	{
		Transform_t1 * L_8 = (__this->___objToScale_2);
		Transform_t1 * L_9 = (__this->___objToScale_2);
		NullCheck(L_9);
		Vector3_t4  L_10 = Transform_get_localScale_m633(L_9, /*hidden argument*/NULL);
		Vector3_t4  L_11 = Vector3_get_one_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_12 = (__this->___U3CtimeU3E__0_0);
		Vector3_t4  L_13 = Vector3_Lerp_m652(NULL /*static, unused*/, L_10, L_11, ((float)((float)L_12/(float)(60.0f))), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_localScale_m634(L_8, L_13, /*hidden argument*/NULL);
		float L_14 = (__this->___U3CtimeU3E__0_0);
		float L_15 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CtimeU3E__0_0 = ((float)((float)L_14+(float)L_15));
		WaitForEndOfFrame_t255 * L_16 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_16, /*hidden argument*/NULL);
		__this->___U24current_4 = L_16;
		__this->___U24PC_3 = 1;
		goto IL_01e8;
	}

IL_00cb:
	{
		float L_17 = (__this->___U3CtimeU3E__0_0);
		if ((((float)L_17) < ((float)(0.5f))))
		{
			goto IL_0076;
		}
	}
	{
		SCR_LevelStartCountdown_t370 * L_18 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_18);
		L_18->___getBigger_8 = 0;
		SCR_LevelStartCountdown_t370 * L_19 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_19);
		SCR_LevelStartCountdown_Countdown_m1381(L_19, /*hidden argument*/NULL);
		goto IL_01df;
	}

IL_00f7:
	{
		Transform_t1 * L_20 = (__this->___objToScale_2);
		Vector3_t4  L_21 = Vector3_get_one_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_localScale_m634(L_20, L_21, /*hidden argument*/NULL);
		goto IL_0161;
	}

IL_010c:
	{
		Transform_t1 * L_22 = (__this->___objToScale_2);
		Transform_t1 * L_23 = (__this->___objToScale_2);
		NullCheck(L_23);
		Vector3_t4  L_24 = Transform_get_localScale_m633(L_23, /*hidden argument*/NULL);
		Vector3_t4  L_25 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_26 = (__this->___U3CtimeU3E__0_0);
		Vector3_t4  L_27 = Vector3_Lerp_m652(NULL /*static, unused*/, L_24, L_25, ((float)((float)L_26/(float)(60.0f))), /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_set_localScale_m634(L_22, L_27, /*hidden argument*/NULL);
		float L_28 = (__this->___U3CtimeU3E__0_0);
		float L_29 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CtimeU3E__0_0 = ((float)((float)L_28+(float)L_29));
		WaitForEndOfFrame_t255 * L_30 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_30, /*hidden argument*/NULL);
		__this->___U24current_4 = L_30;
		__this->___U24PC_3 = 2;
		goto IL_01e8;
	}

IL_0161:
	{
		Transform_t1 * L_31 = (__this->___objToScale_2);
		NullCheck(L_31);
		Vector3_t4  L_32 = Transform_get_localScale_m633(L_31, /*hidden argument*/NULL);
		V_1 = L_32;
		float L_33 = ((&V_1)->___x_1);
		if ((((float)L_33) > ((float)(0.5f))))
		{
			goto IL_010c;
		}
	}
	{
		SCR_LevelStartCountdown_t370 * L_34 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_34);
		L_34->___getBigger_8 = 1;
		SCR_LevelStartCountdown_t370 * L_35 = (__this->___U3CU3Ef__this_7);
		Transform_t1 * L_36 = (__this->___objToScale_2);
		NullCheck(L_36);
		String_t* L_37 = Object_get_name_m798(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		SCR_Menu_ShowMenuItem_m1208(L_35, L_37, 0, /*hidden argument*/NULL);
		SCR_LevelStartCountdown_t370 * L_38 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_38);
		int32_t L_39 = (L_38->___activeText_7);
		if ((((int32_t)L_39) == ((int32_t)3)))
		{
			goto IL_01d5;
		}
	}
	{
		SCR_LevelStartCountdown_t370 * L_40 = (__this->___U3CU3Ef__this_7);
		SCR_LevelStartCountdown_t370 * L_41 = L_40;
		NullCheck(L_41);
		int32_t L_42 = (L_41->___activeText_7);
		NullCheck(L_41);
		L_41->___activeText_7 = ((int32_t)((int32_t)L_42+(int32_t)1));
		SCR_LevelStartCountdown_t370 * L_43 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_43);
		SCR_LevelStartCountdown_Countdown_m1381(L_43, /*hidden argument*/NULL);
		goto IL_01df;
	}

IL_01d5:
	{
		SCR_GUIManager_t378 * L_44 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_44);
		SCR_GUIManager_OpenLevelMenus_m1410(L_44, /*hidden argument*/NULL);
	}

IL_01df:
	{
		__this->___U24PC_3 = (-1);
	}

IL_01e6:
	{
		return 0;
	}

IL_01e8:
	{
		return 1;
	}
	// Dead block : IL_01ea: ldloc.2
}
// System.Void SCR_LevelStartCountdown/<Rescale>c__IteratorA::Dispose()
extern "C" void U3CRescaleU3Ec__IteratorA_Dispose_m1375 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_3 = (-1);
		return;
	}
}
// System.Void SCR_LevelStartCountdown/<Rescale>c__IteratorA::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CRescaleU3Ec__IteratorA_Reset_m1376 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void SCR_LevelStartCountdown::.ctor()
extern TypeInfo* GameObjectU5BU5D_t192_il2cpp_TypeInfo_var;
extern "C" void SCR_LevelStartCountdown__ctor_m1377 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObjectU5BU5D_t192_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___countdownTexts_6 = ((GameObjectU5BU5D_t192*)SZArrayNew(GameObjectU5BU5D_t192_il2cpp_TypeInfo_var, 4));
		__this->___getBigger_8 = 1;
		SCR_Menu__ctor_m1204(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_LevelStartCountdown::Awake()
extern TypeInfo* SCR_LevelStartCountdown_t370_il2cpp_TypeInfo_var;
extern "C" void SCR_LevelStartCountdown_Awake_m1378 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_LevelStartCountdown_t370_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(691);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_LevelStartCountdown_t370 * L_0 = ((SCR_LevelStartCountdown_t370_StaticFields*)SCR_LevelStartCountdown_t370_il2cpp_TypeInfo_var->static_fields)->___instance_5;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		((SCR_LevelStartCountdown_t370_StaticFields*)SCR_LevelStartCountdown_t370_il2cpp_TypeInfo_var->static_fields)->___instance_5 = __this;
		GameObject_t78 * L_2 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m1602(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_0026:
	{
		GameObject_t78 * L_3 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0032:
	{
		SCR_Menu_Awake_m1205(__this, /*hidden argument*/NULL);
		SCR_LevelStartCountdown_FillCountdownArray_m1385(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_LevelStartCountdown::OnEnable()
extern "C" void SCR_LevelStartCountdown_OnEnable_m1379 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method)
{
	{
		SCR_Menu_OnEnable_m1206(__this, /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_0 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		SCR_LevelStartCountdown_Reinitialize_m1383(__this, /*hidden argument*/NULL);
		int32_t L_2 = Application_get_loadedLevel_m1012(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		SCR_LevelStartCountdown_Countdown_m1381(__this, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void SCR_LevelStartCountdown::OnDisable()
extern "C" void SCR_LevelStartCountdown_OnDisable_m1380 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_StopAllCoroutines_m1773(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_LevelStartCountdown::Countdown()
extern "C" void SCR_LevelStartCountdown_Countdown_m1381 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___getBigger_8);
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		GameObjectU5BU5D_t192* L_1 = (__this->___countdownTexts_6);
		int32_t L_2 = (__this->___activeText_7);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_1, L_3)));
		Transform_t1 * L_4 = GameObject_get_transform_m609((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_1, L_3)), /*hidden argument*/NULL);
		Object_t * L_5 = SCR_LevelStartCountdown_Rescale_m1382(__this, 1, L_4, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1006(__this, L_5, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0030:
	{
		GameObjectU5BU5D_t192* L_6 = (__this->___countdownTexts_6);
		int32_t L_7 = (__this->___activeText_7);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_6, L_8)));
		Transform_t1 * L_9 = GameObject_get_transform_m609((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_6, L_8)), /*hidden argument*/NULL);
		Object_t * L_10 = SCR_LevelStartCountdown_Rescale_m1382(__this, 0, L_9, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1006(__this, L_10, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Collections.IEnumerator SCR_LevelStartCountdown::Rescale(System.Boolean,UnityEngine.Transform)
extern TypeInfo* U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_LevelStartCountdown_Rescale_m1382 (SCR_LevelStartCountdown_t370 * __this, bool ___up, Transform_t1 * ___objToScale, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(692);
		s_Il2CppMethodIntialized = true;
	}
	U3CRescaleU3Ec__IteratorA_t371 * V_0 = {0};
	{
		U3CRescaleU3Ec__IteratorA_t371 * L_0 = (U3CRescaleU3Ec__IteratorA_t371 *)il2cpp_codegen_object_new (U3CRescaleU3Ec__IteratorA_t371_il2cpp_TypeInfo_var);
		U3CRescaleU3Ec__IteratorA__ctor_m1371(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRescaleU3Ec__IteratorA_t371 * L_1 = V_0;
		bool L_2 = ___up;
		NullCheck(L_1);
		L_1->___up_1 = L_2;
		U3CRescaleU3Ec__IteratorA_t371 * L_3 = V_0;
		Transform_t1 * L_4 = ___objToScale;
		NullCheck(L_3);
		L_3->___objToScale_2 = L_4;
		U3CRescaleU3Ec__IteratorA_t371 * L_5 = V_0;
		bool L_6 = ___up;
		NullCheck(L_5);
		L_5->___U3CU24U3Eup_5 = L_6;
		U3CRescaleU3Ec__IteratorA_t371 * L_7 = V_0;
		Transform_t1 * L_8 = ___objToScale;
		NullCheck(L_7);
		L_7->___U3CU24U3EobjToScale_6 = L_8;
		U3CRescaleU3Ec__IteratorA_t371 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_7 = __this;
		U3CRescaleU3Ec__IteratorA_t371 * L_10 = V_0;
		return L_10;
	}
}
// System.Void SCR_LevelStartCountdown::Reinitialize()
extern "C" void SCR_LevelStartCountdown_Reinitialize_m1383 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method)
{
	{
		SCR_LevelStartCountdown_HideAll_m1384(__this, /*hidden argument*/NULL);
		__this->___activeText_7 = 0;
		__this->___getBigger_8 = 1;
		return;
	}
}
// System.Void SCR_LevelStartCountdown::HideAll()
extern "C" void SCR_LevelStartCountdown_HideAll_m1384 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001f;
	}

IL_0007:
	{
		GameObjectU5BU5D_t192* L_0 = (__this->___countdownTexts_6);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_0, L_2)));
		String_t* L_3 = Object_get_name_m798((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_0, L_2)), /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, L_3, 0, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_5 = V_0;
		GameObjectU5BU5D_t192* L_6 = (__this->___countdownTexts_6);
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)(((Array_t *)L_6)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void SCR_LevelStartCountdown::FillCountdownArray()
extern "C" void SCR_LevelStartCountdown_FillCountdownArray_m1385 (SCR_LevelStartCountdown_t370 * __this, const MethodInfo* method)
{
	{
		GameObjectU5BU5D_t192* L_0 = (__this->___countdownTexts_6);
		GameObject_t78 * L_1 = SCR_Menu_GetMenuItem_m1210(__this, (String_t*) &_stringLiteral332, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_0, 0)) = (GameObject_t78 *)L_1;
		GameObjectU5BU5D_t192* L_2 = (__this->___countdownTexts_6);
		GameObject_t78 * L_3 = SCR_Menu_GetMenuItem_m1210(__this, (String_t*) &_stringLiteral333, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_2, 1)) = (GameObject_t78 *)L_3;
		GameObjectU5BU5D_t192* L_4 = (__this->___countdownTexts_6);
		GameObject_t78 * L_5 = SCR_Menu_GetMenuItem_m1210(__this, (String_t*) &_stringLiteral334, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_4, 2)) = (GameObject_t78 *)L_5;
		GameObjectU5BU5D_t192* L_6 = (__this->___countdownTexts_6);
		GameObject_t78 * L_7 = SCR_Menu_GetMenuItem_m1210(__this, (String_t*) &_stringLiteral335, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		*((GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_6, 3)) = (GameObject_t78 *)L_7;
		return;
	}
}
// SCR_MainMenu
#include "AssemblyU2DCSharp_SCR_MainMenu.h"
#ifndef _MSC_VER
#else
#endif
// SCR_MainMenu
#include "AssemblyU2DCSharp_SCR_MainMenuMethodDeclarations.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6.h"
// System.Text.RegularExpressions.Match
#include "System_System_Text_RegularExpressions_Match.h"
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6MethodDeclarations.h"
// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
// System.Text.RegularExpressions.Capture
#include "System_System_Text_RegularExpressions_CaptureMethodDeclarations.h"
struct GameObject_t78;
struct TextU5BU5D_t369;
struct GameObject_t78;
struct ObjectU5BU5D_t224;
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t224* GameObject_GetComponentsInChildren_TisObject_t_m1781_gshared (GameObject_t78 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisObject_t_m1781(__this, method) (( ObjectU5BU5D_t224* (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m1781_gshared)(__this, method)
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.UI.Text>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.UI.Text>()
#define GameObject_GetComponentsInChildren_TisText_t316_m1780(__this, method) (( TextU5BU5D_t369* (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m1781_gshared)(__this, method)


// System.Void SCR_MainMenu::.ctor()
extern TypeInfo* GameObjectU5BU5D_t192_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t373_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1782_MethodInfo_var;
extern "C" void SCR_MainMenu__ctor_m1386 (SCR_MainMenu_t372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObjectU5BU5D_t192_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		Dictionary_2_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(693);
		Dictionary_2__ctor_m1782_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483721);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___levelSets_15 = ((GameObjectU5BU5D_t192*)SZArrayNew(GameObjectU5BU5D_t192_il2cpp_TypeInfo_var, 5));
		Dictionary_2_t373 * L_0 = (Dictionary_2_t373 *)il2cpp_codegen_object_new (Dictionary_2_t373_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1782(L_0, /*hidden argument*/Dictionary_2__ctor_m1782_MethodInfo_var);
		__this->___levelsLocked_17 = L_0;
		SCR_Menu__ctor_m1204(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_MainMenu::.cctor()
extern "C" void SCR_MainMenu__cctor_m1387 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SCR_MainMenu::Awake()
extern TypeInfo* SCR_MainMenu_t372_il2cpp_TypeInfo_var;
extern "C" void SCR_MainMenu_Awake_m1388 (SCR_MainMenu_t372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_MainMenu_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(694);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SCR_MainMenu_t372_il2cpp_TypeInfo_var);
		SCR_MainMenu_t372 * L_0 = ((SCR_MainMenu_t372_StaticFields*)SCR_MainMenu_t372_il2cpp_TypeInfo_var->static_fields)->___instance_5;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SCR_MainMenu_t372_il2cpp_TypeInfo_var);
		((SCR_MainMenu_t372_StaticFields*)SCR_MainMenu_t372_il2cpp_TypeInfo_var->static_fields)->___instance_5 = __this;
		GameObject_t78 * L_2 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m1602(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_0026:
	{
		GameObject_t78 * L_3 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0032:
	{
		SCR_Menu_Awake_m1205(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_MainMenu::OnEnable()
extern "C" void SCR_MainMenu_OnEnable_m1389 (SCR_MainMenu_t372 * __this, const MethodInfo* method)
{
	{
		SCR_GUIManager_t378 * L_0 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		SCR_Menu_OnEnable_m1206(__this, /*hidden argument*/NULL);
		SCR_MainMenu_LevelSetsInit_m1392(__this, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral289, 0, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral336, 0, /*hidden argument*/NULL);
		__this->___timerOn_7 = 1;
		__this->___time_8 = (0.0f);
		return;
	}
}
// System.Void SCR_MainMenu::Update()
extern const MethodInfo* GameObject_GetComponentsInChildren_TisText_t316_m1780_MethodInfo_var;
extern "C" void SCR_MainMenu_Update_m1390 (SCR_MainMenu_t372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponentsInChildren_TisText_t316_m1780_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483722);
		s_Il2CppMethodIntialized = true;
	}
	TextU5BU5D_t369* V_0 = {0};
	Text_t316 * V_1 = {0};
	TextU5BU5D_t369* V_2 = {0};
	int32_t V_3 = 0;
	{
		bool L_0 = (__this->___timerOn_7);
		if (!L_0)
		{
			goto IL_0084;
		}
	}
	{
		float L_1 = (__this->___time_8);
		float L_2 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___time_8 = ((float)((float)L_1+(float)L_2));
		float L_3 = (__this->___time_8);
		float L_4 = (__this->___timeBeforePressToContinue_9);
		if ((!(((float)L_3) >= ((float)L_4))))
		{
			goto IL_0084;
		}
	}
	{
		GameObject_t78 * L_5 = SCR_Menu_GetMenuItem_m1210(__this, (String_t*) &_stringLiteral337, /*hidden argument*/NULL);
		NullCheck(L_5);
		TextU5BU5D_t369* L_6 = GameObject_GetComponentsInChildren_TisText_t316_m1780(L_5, /*hidden argument*/GameObject_GetComponentsInChildren_TisText_t316_m1780_MethodInfo_var);
		V_0 = L_6;
		TextU5BU5D_t369* L_7 = V_0;
		V_2 = L_7;
		V_3 = 0;
		goto IL_0074;
	}

IL_0048:
	{
		TextU5BU5D_t369* L_8 = V_2;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_1 = (*(Text_t316 **)(Text_t316 **)SZArrayLdElema(L_8, L_10));
		Text_t316 * L_11 = V_1;
		NullCheck(L_11);
		bool L_12 = Behaviour_get_isActiveAndEnabled_m1740(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		Text_t316 * L_13 = V_1;
		Object_t * L_14 = SCR_Menu_AlphaLerp_m1212(__this, L_13, (0.3f), 0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1006(__this, L_14, /*hidden argument*/NULL);
		goto IL_007d;
	}

IL_0070:
	{
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0074:
	{
		int32_t L_16 = V_3;
		TextU5BU5D_t369* L_17 = V_2;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)(((Array_t *)L_17)->max_length))))))
		{
			goto IL_0048;
		}
	}

IL_007d:
	{
		__this->___timerOn_7 = 0;
	}

IL_0084:
	{
		return;
	}
}
// System.Void SCR_MainMenu::OpenMenu()
extern const MethodInfo* GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var;
extern "C" void SCR_MainMenu_OpenMenu_m1391 (SCR_MainMenu_t372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483720);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral336, 0, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral289, 1, /*hidden argument*/NULL);
		__this->___hasSelectedALevel_14 = 0;
		GameObject_t78 * L_0 = SCR_Menu_GetMenuItem_m1210(__this, (String_t*) &_stringLiteral338, /*hidden argument*/NULL);
		NullCheck(L_0);
		Image_t48 * L_1 = GameObject_GetComponent_TisImage_t48_m1779(L_0, /*hidden argument*/GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var);
		__this->___selectedLevelImg_12 = L_1;
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral338, 0, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral339, 0, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral340, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_MainMenu::LevelSetsInit()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t217_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t1_il2cpp_TypeInfo_var;
extern TypeInfo* Regex_t421_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t233_il2cpp_TypeInfo_var;
extern "C" void SCR_MainMenu_LevelSetsInit_m1392 (SCR_MainMenu_t372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		IEnumerator_t217_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		Transform_t1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		Regex_t421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(695);
		IDisposable_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Transform_t1 * V_1 = {0};
	Object_t * V_2 = {0};
	int32_t V_3 = 0;
	bool V_4 = false;
	bool V_5 = false;
	int32_t V_6 = 0;
	Object_t * V_7 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		__this->___currentLevelSet_16 = 0;
		Dictionary_2_t373 * L_0 = (__this->___levelsLocked_17);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::Clear() */, L_0);
		SCR_PointManager_t381 * L_1 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral341, (0.01f), /*hidden argument*/NULL);
		return;
	}

IL_0033:
	{
		V_0 = 0;
		goto IL_015a;
	}

IL_003a:
	{
		GameObjectU5BU5D_t192* L_3 = (__this->___levelSets_15);
		int32_t L_4 = V_0;
		int32_t L_5 = V_0;
		V_6 = ((int32_t)((int32_t)L_5+(int32_t)1));
		String_t* L_6 = Int32_ToString_m1615((&V_6), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m860(NULL /*static, unused*/, (String_t*) &_stringLiteral342, L_6, /*hidden argument*/NULL);
		GameObject_t78 * L_8 = SCR_Menu_GetMenuItem_m1210(__this, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_8);
		*((GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_3, L_4)) = (GameObject_t78 *)L_8;
		GameObjectU5BU5D_t192* L_9 = (__this->___levelSets_15);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_9, L_11)));
		Transform_t1 * L_12 = GameObject_get_transform_m609((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_9, L_11)), /*hidden argument*/NULL);
		NullCheck(L_12);
		Object_t * L_13 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_12);
		V_2 = L_13;
	}

IL_0071:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0105;
		}

IL_0076:
		{
			Object_t * L_14 = V_2;
			NullCheck(L_14);
			Object_t * L_15 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t217_il2cpp_TypeInfo_var, L_14);
			V_1 = ((Transform_t1 *)Castclass(L_15, Transform_t1_il2cpp_TypeInfo_var));
			Transform_t1 * L_16 = V_1;
			NullCheck(L_16);
			String_t* L_17 = Object_get_name_m798(L_16, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Regex_t421_il2cpp_TypeInfo_var);
			Match_t422 * L_18 = Regex_Match_m1783(NULL /*static, unused*/, L_17, (String_t*) &_stringLiteral343, /*hidden argument*/NULL);
			NullCheck(L_18);
			String_t* L_19 = Capture_get_Value_m1784(L_18, /*hidden argument*/NULL);
			int32_t L_20 = Int32_Parse_m1785(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
			V_3 = ((int32_t)((int32_t)L_20-(int32_t)1));
			SCR_PointManager_t381 * L_21 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_21);
			int32_t L_22 = SCR_PointManager_get_ScoreCumul_m1475(L_21, /*hidden argument*/NULL);
			SCR_PointManager_t381 * L_23 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_23);
			Int32U5BU5D_t242* L_24 = SCR_PointManager_get_PointsForLevelAccess_m1476(L_23, /*hidden argument*/NULL);
			int32_t L_25 = V_3;
			NullCheck(L_24);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
			int32_t L_26 = L_25;
			V_4 = ((((int32_t)L_22) > ((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_24, L_26))))? 1 : 0);
			Dictionary_2_t373 * L_27 = (__this->___levelsLocked_17);
			int32_t L_28 = V_3;
			NullCheck(L_27);
			bool L_29 = (bool)VirtFuncInvoker2< bool, int32_t, bool* >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::TryGetValue(!0,!1&) */, L_27, L_28, (&V_5));
			if (L_29)
			{
				goto IL_00da;
			}
		}

IL_00cc:
		{
			Dictionary_2_t373 * L_30 = (__this->___levelsLocked_17);
			int32_t L_31 = V_3;
			bool L_32 = V_4;
			NullCheck(L_30);
			VirtActionInvoker2< int32_t, bool >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::Add(!0,!1) */, L_30, L_31, L_32);
		}

IL_00da:
		{
			String_t* L_33 = Int32_ToString_m1615((&V_3), /*hidden argument*/NULL);
			bool L_34 = V_4;
			SCR_Menu_ShowMenuItem_m1208(__this, L_33, ((((int32_t)L_34) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
			String_t* L_35 = Int32_ToString_m1615((&V_3), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_36 = String_Concat_m860(NULL /*static, unused*/, L_35, (String_t*) &_stringLiteral344, /*hidden argument*/NULL);
			bool L_37 = V_4;
			SCR_Menu_ShowMenuItem_m1208(__this, L_36, L_37, /*hidden argument*/NULL);
		}

IL_0105:
		{
			Object_t * L_38 = V_2;
			NullCheck(L_38);
			bool L_39 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t217_il2cpp_TypeInfo_var, L_38);
			if (L_39)
			{
				goto IL_0076;
			}
		}

IL_0110:
		{
			IL2CPP_LEAVE(0x12A, FINALLY_0115);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_0115;
	}

FINALLY_0115:
	{ // begin finally (depth: 1)
		{
			Object_t * L_40 = V_2;
			V_7 = ((Object_t *)IsInst(L_40, IDisposable_t233_il2cpp_TypeInfo_var));
			Object_t * L_41 = V_7;
			if (L_41)
			{
				goto IL_0122;
			}
		}

IL_0121:
		{
			IL2CPP_END_FINALLY(277)
		}

IL_0122:
		{
			Object_t * L_42 = V_7;
			NullCheck(L_42);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t233_il2cpp_TypeInfo_var, L_42);
			IL2CPP_END_FINALLY(277)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(277)
	{
		IL2CPP_JUMP_TBL(0x12A, IL_012a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_012a:
	{
		int32_t L_43 = V_0;
		int32_t L_44 = (__this->___currentLevelSet_16);
		if ((((int32_t)L_43) <= ((int32_t)L_44)))
		{
			goto IL_0156;
		}
	}
	{
		GameObjectU5BU5D_t192* L_45 = (__this->___levelSets_15);
		int32_t L_46 = V_0;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_45, L_47)));
		String_t* L_48 = Object_get_name_m798((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_45, L_47)), /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, L_48, 0, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral345, 0, /*hidden argument*/NULL);
	}

IL_0156:
	{
		int32_t L_49 = V_0;
		V_0 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_015a:
	{
		int32_t L_50 = V_0;
		GameObjectU5BU5D_t192* L_51 = (__this->___levelSets_15);
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)(((int32_t)(((Array_t *)L_51)->max_length))))))
		{
			goto IL_003a;
		}
	}
	{
		return;
	}
}
// System.Void SCR_MainMenu::ShowTutorials()
extern "C" void SCR_MainMenu_ShowTutorials_m1393 (SCR_MainMenu_t372 * __this, const MethodInfo* method)
{
	{
		SCR_Menu_SetMenuLanguage_m1217(__this, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral336, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_MainMenu::ShowLevelInfo(System.Int32)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t253_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var;
extern "C" void SCR_MainMenu_ShowLevelInfo_m1394 (SCR_MainMenu_t372 * __this, int32_t ___level, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Int32_t253_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483720);
		s_Il2CppMethodIntialized = true;
	}
	Image_t48 * V_0 = {0};
	float V_1 = 0.0f;
	String_t* G_B6_0 = {0};
	String_t* G_B6_1 = {0};
	SCR_MainMenu_t372 * G_B6_2 = {0};
	String_t* G_B5_0 = {0};
	String_t* G_B5_1 = {0};
	SCR_MainMenu_t372 * G_B5_2 = {0};
	String_t* G_B7_0 = {0};
	String_t* G_B7_1 = {0};
	String_t* G_B7_2 = {0};
	SCR_MainMenu_t372 * G_B7_3 = {0};
	String_t* G_B10_0 = {0};
	SCR_MainMenu_t372 * G_B10_1 = {0};
	String_t* G_B9_0 = {0};
	SCR_MainMenu_t372 * G_B9_1 = {0};
	String_t* G_B11_0 = {0};
	String_t* G_B11_1 = {0};
	SCR_MainMenu_t372 * G_B11_2 = {0};
	{
		Dictionary_2_t373 * L_0 = (__this->___levelsLocked_17);
		int32_t L_1 = ___level;
		bool* L_2 = &(__this->___selectedIsLocked_11);
		NullCheck(L_0);
		bool L_3 = (bool)VirtFuncInvoker2< bool, int32_t, bool* >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>::TryGetValue(!0,!1&) */, L_0, L_1, L_2);
		if (!L_3)
		{
			goto IL_014f;
		}
	}
	{
		bool L_4 = (__this->___hasSelectedALevel_14);
		if (L_4)
		{
			goto IL_004d;
		}
	}
	{
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral338, 1, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral339, 1, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral340, 1, /*hidden argument*/NULL);
		__this->___hasSelectedALevel_14 = 1;
	}

IL_004d:
	{
		bool L_5 = (__this->___selectedIsLocked_11);
		if (!L_5)
		{
			goto IL_00d2;
		}
	}
	{
		String_t* L_6 = Int32_ToString_m1615((&___level), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m860(NULL /*static, unused*/, L_6, (String_t*) &_stringLiteral344, /*hidden argument*/NULL);
		GameObject_t78 * L_8 = SCR_Menu_GetMenuItem_m1210(__this, L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Image_t48 * L_9 = GameObject_GetComponent_TisImage_t48_m1779(L_8, /*hidden argument*/GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var);
		V_0 = L_9;
		Image_t48 * L_10 = (__this->___selectedLevelImg_12);
		Image_t48 * L_11 = V_0;
		NullCheck(L_11);
		Sprite_t329 * L_12 = Image_get_sprite_m1786(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Image_set_sprite_m1735(L_10, L_12, /*hidden argument*/NULL);
		V_1 = (0.0f);
		String_t* L_13 = Single_ToString_m1787((&V_1), /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_14 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		bool L_15 = SCR_GUIManager_get_IsFrench_m1413(L_14, /*hidden argument*/NULL);
		G_B5_0 = L_13;
		G_B5_1 = (String_t*) &_stringLiteral339;
		G_B5_2 = __this;
		if (!L_15)
		{
			G_B6_0 = L_13;
			G_B6_1 = (String_t*) &_stringLiteral339;
			G_B6_2 = __this;
			goto IL_00b2;
		}
	}
	{
		G_B7_0 = (String_t*) &_stringLiteral346;
		G_B7_1 = G_B5_0;
		G_B7_2 = G_B5_1;
		G_B7_3 = G_B5_2;
		goto IL_00b7;
	}

IL_00b2:
	{
		G_B7_0 = (String_t*) &_stringLiteral347;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
		G_B7_3 = G_B6_2;
	}

IL_00b7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m860(NULL /*static, unused*/, G_B7_1, G_B7_0, /*hidden argument*/NULL);
		NullCheck(G_B7_3);
		SCR_Menu_ModifyText_m1211(G_B7_3, G_B7_2, L_16, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral340, 0, /*hidden argument*/NULL);
		goto IL_014f;
	}

IL_00d2:
	{
		String_t* L_17 = Int32_ToString_m1615((&___level), /*hidden argument*/NULL);
		GameObject_t78 * L_18 = SCR_Menu_GetMenuItem_m1210(__this, L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Image_t48 * L_19 = GameObject_GetComponent_TisImage_t48_m1779(L_18, /*hidden argument*/GameObject_GetComponent_TisImage_t48_m1779_MethodInfo_var);
		V_0 = L_19;
		int32_t L_20 = ___level;
		__this->___selectedLevel_13 = L_20;
		Image_t48 * L_21 = (__this->___selectedLevelImg_12);
		Image_t48 * L_22 = V_0;
		NullCheck(L_22);
		Sprite_t329 * L_23 = Image_get_sprite_m1786(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Image_set_sprite_m1735(L_21, L_23, /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_24 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		bool L_25 = SCR_GUIManager_get_IsFrench_m1413(L_24, /*hidden argument*/NULL);
		G_B9_0 = (String_t*) &_stringLiteral339;
		G_B9_1 = __this;
		if (!L_25)
		{
			G_B10_0 = (String_t*) &_stringLiteral339;
			G_B10_1 = __this;
			goto IL_011c;
		}
	}
	{
		G_B11_0 = (String_t*) &_stringLiteral348;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		goto IL_0121;
	}

IL_011c:
	{
		G_B11_0 = (String_t*) &_stringLiteral349;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
	}

IL_0121:
	{
		SCR_PointManager_t381 * L_26 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		Int32U5BU5D_t242* L_27 = SCR_PointManager_get_HighScores_m1473(L_26, /*hidden argument*/NULL);
		int32_t L_28 = (__this->___selectedLevel_13);
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)L_28-(int32_t)1)));
		int32_t L_29 = ((int32_t)((int32_t)L_28-(int32_t)1));
		int32_t L_30 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29));
		Object_t * L_31 = Box(Int32_t253_il2cpp_TypeInfo_var, &L_30);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m954(NULL /*static, unused*/, G_B11_0, L_31, /*hidden argument*/NULL);
		NullCheck(G_B11_2);
		SCR_Menu_ModifyText_m1211(G_B11_2, G_B11_1, L_32, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral340, 1, /*hidden argument*/NULL);
	}

IL_014f:
	{
		return;
	}
}
// System.Void SCR_MainMenu::StartLevel()
extern "C" void SCR_MainMenu_StartLevel_m1395 (SCR_MainMenu_t372 * __this, const MethodInfo* method)
{
	{
		SCR_LevelManager_t379 * L_0 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___selectedLevel_13);
		NullCheck(L_0);
		SCR_LevelManager_LoadLevel_m1420(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_MainMenu::ScrollLevelSets(System.Boolean)
extern "C" void SCR_MainMenu_ScrollLevelSets_m1396 (SCR_MainMenu_t372 * __this, bool ___goRight, const MethodInfo* method)
{
	{
		GameObjectU5BU5D_t192* L_0 = (__this->___levelSets_15);
		int32_t L_1 = (__this->___currentLevelSet_16);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_0, L_2)));
		String_t* L_3 = Object_get_name_m798((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_0, L_2)), /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, L_3, 0, /*hidden argument*/NULL);
		bool L_4 = ___goRight;
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_5 = (__this->___currentLevelSet_16);
		__this->___currentLevelSet_16 = ((int32_t)((int32_t)L_5+(int32_t)1));
		goto IL_0040;
	}

IL_0032:
	{
		int32_t L_6 = (__this->___currentLevelSet_16);
		__this->___currentLevelSet_16 = ((int32_t)((int32_t)L_6-(int32_t)1));
	}

IL_0040:
	{
		int32_t L_7 = (__this->___currentLevelSet_16);
		GameObjectU5BU5D_t192* L_8 = (__this->___levelSets_15);
		NullCheck(L_8);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral350, ((((int32_t)((((int32_t)L_7) == ((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_8)->max_length)))-(int32_t)1))))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_9 = (__this->___currentLevelSet_16);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral345, ((((int32_t)((((int32_t)L_9) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		GameObjectU5BU5D_t192* L_10 = (__this->___levelSets_15);
		int32_t L_11 = (__this->___currentLevelSet_16);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_10, L_12)));
		String_t* L_13 = Object_get_name_m798((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_10, L_12)), /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, L_13, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SCR_MainMenu::get_IsFromSplashScreen()
extern TypeInfo* SCR_MainMenu_t372_il2cpp_TypeInfo_var;
extern "C" bool SCR_MainMenu_get_IsFromSplashScreen_m1397 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_MainMenu_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(694);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SCR_MainMenu_t372_il2cpp_TypeInfo_var);
		bool L_0 = ((SCR_MainMenu_t372_StaticFields*)SCR_MainMenu_t372_il2cpp_TypeInfo_var->static_fields)->___isFromSplashScreen_6;
		return L_0;
	}
}
// System.Void SCR_MainMenu::set_IsFromSplashScreen(System.Boolean)
extern TypeInfo* SCR_MainMenu_t372_il2cpp_TypeInfo_var;
extern "C" void SCR_MainMenu_set_IsFromSplashScreen_m1398 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_MainMenu_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(694);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(SCR_MainMenu_t372_il2cpp_TypeInfo_var);
		((SCR_MainMenu_t372_StaticFields*)SCR_MainMenu_t372_il2cpp_TypeInfo_var->static_fields)->___isFromSplashScreen_6 = L_0;
		return;
	}
}
// SCR_PauseMenu
#include "AssemblyU2DCSharp_SCR_PauseMenu.h"
#ifndef _MSC_VER
#else
#endif
// SCR_PauseMenu
#include "AssemblyU2DCSharp_SCR_PauseMenuMethodDeclarations.h"

// SCR_GUIManager/TextType
#include "AssemblyU2DCSharp_SCR_GUIManager_TextType.h"


// System.Void SCR_PauseMenu::.ctor()
extern TypeInfo* GameObjectU5BU5D_t192_il2cpp_TypeInfo_var;
extern "C" void SCR_PauseMenu__ctor_m1399 (SCR_PauseMenu_t374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObjectU5BU5D_t192_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ladybugGUI_6 = ((GameObjectU5BU5D_t192*)SZArrayNew(GameObjectU5BU5D_t192_il2cpp_TypeInfo_var, 3));
		SCR_Menu__ctor_m1204(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_PauseMenu::Awake()
extern "C" void SCR_PauseMenu_Awake_m1400 (SCR_PauseMenu_t374 * __this, const MethodInfo* method)
{
	{
		SCR_PauseMenu_t374 * L_0 = (__this->___instance_5);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		__this->___instance_5 = __this;
		GameObject_t78 * L_2 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m1602(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_0034;
	}

IL_0028:
	{
		GameObject_t78 * L_3 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0034:
	{
		SCR_Menu_Awake_m1205(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_PauseMenu::OnEnable()
extern "C" void SCR_PauseMenu_OnEnable_m1401 (SCR_PauseMenu_t374 * __this, const MethodInfo* method)
{
	{
		SCR_Menu_OnEnable_m1206(__this, /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_0 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral351, 0, /*hidden argument*/NULL);
		SCR_PauseMenu_ShowLadybugCollectibles_m1403(__this, 0, /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_2 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_GUIManager_RelayTextInfo_m1411(L_2, 1, /*hidden argument*/NULL);
		SCR_Menu_SetMenuLanguage_m1217(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_PauseMenu::PauseGame(System.Boolean)
extern "C" void SCR_PauseMenu_PauseGame_m1402 (SCR_PauseMenu_t374 * __this, bool ___pause, const MethodInfo* method)
{
	{
		bool L_0 = ___pause;
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral352, ((((int32_t)L_0) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		bool L_1 = ___pause;
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral351, L_1, /*hidden argument*/NULL);
		bool L_2 = ___pause;
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral291, 0, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral292, 0, /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, (String_t*) &_stringLiteral293, 1, /*hidden argument*/NULL);
	}

IL_0045:
	{
		SCR_TimeManager_t383 * L_3 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_TimeManager_Pause_m1506(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_PauseMenu::ShowLadybugCollectibles(System.Int32)
extern "C" void SCR_PauseMenu_ShowLadybugCollectibles_m1403 (SCR_PauseMenu_t374 * __this, int32_t ___collected, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_0043;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		V_1 = ((int32_t)((int32_t)L_0+(int32_t)1));
		int32_t L_1 = V_1;
		int32_t L_2 = ___collected;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_002b;
		}
	}
	{
		GameObjectU5BU5D_t192* L_3 = (__this->___ladybugGUI_6);
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_3, L_5)));
		String_t* L_6 = Object_get_name_m798((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_3, L_5)), /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, L_6, 1, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_002b:
	{
		GameObjectU5BU5D_t192* L_7 = (__this->___ladybugGUI_6);
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		NullCheck((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_7, L_9)));
		String_t* L_10 = Object_get_name_m798((*(GameObject_t78 **)(GameObject_t78 **)SZArrayLdElema(L_7, L_9)), /*hidden argument*/NULL);
		SCR_Menu_ShowMenuItem_m1208(__this, L_10, 0, /*hidden argument*/NULL);
	}

IL_003f:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_12 = V_0;
		GameObjectU5BU5D_t192* L_13 = (__this->___ladybugGUI_6);
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)(((Array_t *)L_13)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// SCR_GUIManager/TextType
#include "AssemblyU2DCSharp_SCR_GUIManager_TextTypeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.List`1<SCR_Menu>
#include "mscorlib_System_Collections_Generic_List_1_gen_8.h"
// System.Collections.Generic.List`1/Enumerator<SCR_Menu>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"
// UnityEngine.SystemLanguage
#include "UnityEngine_UnityEngine_SystemLanguage.h"
// System.Collections.Generic.List`1<SCR_Menu>
#include "mscorlib_System_Collections_Generic_List_1_gen_8MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<SCR_Menu>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0MethodDeclarations.h"
struct Object_t164;
struct CanvasU5BU5D_t376;
struct Object_t164;
struct ObjectU5BU5D_t224;
// Declaration !!0[] UnityEngine.Object::FindObjectsOfType<System.Object>()
// !!0[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C" ObjectU5BU5D_t224* Object_FindObjectsOfType_TisObject_t_m951_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisObject_t_m951(__this /* static, unused */, method) (( ObjectU5BU5D_t224* (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisObject_t_m951_gshared)(__this /* static, unused */, method)
// Declaration !!0[] UnityEngine.Object::FindObjectsOfType<UnityEngine.Canvas>()
// !!0[] UnityEngine.Object::FindObjectsOfType<UnityEngine.Canvas>()
#define Object_FindObjectsOfType_TisCanvas_t414_m1788(__this /* static, unused */, method) (( CanvasU5BU5D_t376* (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisObject_t_m951_gshared)(__this /* static, unused */, method)
struct Object_t164;
struct Canvas_t414;
// Declaration !!0 UnityEngine.Object::Instantiate<UnityEngine.Canvas>(!!0)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.Canvas>(!!0)
#define Object_Instantiate_TisCanvas_t414_m1789(__this /* static, unused */, p0, method) (( Canvas_t414 * (*) (Object_t * /* static, unused */, Canvas_t414 *, const MethodInfo*))Object_Instantiate_TisObject_t_m953_gshared)(__this /* static, unused */, p0, method)
struct Component_t219;
struct SCR_Menu_t336;
// Declaration !!0 UnityEngine.Component::GetComponent<SCR_Menu>()
// !!0 UnityEngine.Component::GetComponent<SCR_Menu>()
#define Component_GetComponent_TisSCR_Menu_t336_m1790(__this, method) (( SCR_Menu_t336 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct SCR_PauseMenu_t374;
// Declaration !!0 UnityEngine.Component::GetComponent<SCR_PauseMenu>()
// !!0 UnityEngine.Component::GetComponent<SCR_PauseMenu>()
#define Component_GetComponent_TisSCR_PauseMenu_t374_m1791(__this, method) (( SCR_PauseMenu_t374 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct SCR_MainMenu_t372;
// Declaration !!0 UnityEngine.Component::GetComponent<SCR_MainMenu>()
// !!0 UnityEngine.Component::GetComponent<SCR_MainMenu>()
#define Component_GetComponent_TisSCR_MainMenu_t372_m1792(__this, method) (( SCR_MainMenu_t372 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct SCR_LevelEndMenu_t367;
// Declaration !!0 UnityEngine.Component::GetComponent<SCR_LevelEndMenu>()
// !!0 UnityEngine.Component::GetComponent<SCR_LevelEndMenu>()
#define Component_GetComponent_TisSCR_LevelEndMenu_t367_m1793(__this, method) (( SCR_LevelEndMenu_t367 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void SCR_GUIManager::.ctor()
extern TypeInfo* List_1_t377_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1794_MethodInfo_var;
extern "C" void SCR_GUIManager__ctor_m1404 (SCR_GUIManager_t378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t377_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(697);
		List_1__ctor_m1794_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483723);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t377 * L_0 = (List_1_t377 *)il2cpp_codegen_object_new (List_1_t377_il2cpp_TypeInfo_var);
		List_1__ctor_m1794(L_0, /*hidden argument*/List_1__ctor_m1794_MethodInfo_var);
		__this->___menus_3 = L_0;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_GUIManager::Awake()
extern TypeInfo* Enumerator_t423_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t233_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectsOfType_TisCanvas_t414_m1788_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisCanvas_t414_m1789_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSCR_Menu_t336_m1790_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1797_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1798_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSCR_PauseMenu_t374_m1791_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1799_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSCR_MainMenu_t372_m1792_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSCR_LevelEndMenu_t367_m1793_MethodInfo_var;
extern "C" void SCR_GUIManager_Awake_m1405 (SCR_GUIManager_t378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(699);
		IDisposable_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		Object_FindObjectsOfType_TisCanvas_t414_m1788_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483724);
		Object_Instantiate_TisCanvas_t414_m1789_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483725);
		Component_GetComponent_TisSCR_Menu_t336_m1790_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483726);
		List_1_GetEnumerator_m1797_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483727);
		Enumerator_get_Current_m1798_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483728);
		Component_GetComponent_TisSCR_PauseMenu_t374_m1791_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483729);
		Enumerator_MoveNext_m1799_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483730);
		Component_GetComponent_TisSCR_MainMenu_t372_m1792_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483731);
		Component_GetComponent_TisSCR_LevelEndMenu_t367_m1793_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483732);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	String_t* V_2 = {0};
	SCR_Menu_t336 * V_3 = {0};
	SCR_Menu_t336 * V_4 = {0};
	Enumerator_t423  V_5 = {0};
	SCR_Menu_t336 * V_6 = {0};
	Enumerator_t423  V_7 = {0};
	SCR_Menu_t336 * V_8 = {0};
	Enumerator_t423  V_9 = {0};
	int32_t V_10 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SCR_GUIManager_t378 * L_0 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		SCR_ManagerComponents_set_GUIManager_m1438(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		CanvasU5BU5D_t376* L_2 = Object_FindObjectsOfType_TisCanvas_t414_m1788(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectsOfType_TisCanvas_t414_m1788_MethodInfo_var);
		__this->___gameCanvases_2 = L_2;
		CanvasU5BU5D_t376* L_3 = (__this->___gameCanvases_2);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		bool L_5 = Object_op_Equality_m640(NULL /*static, unused*/, (*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_3, L_4)), (Object_t164 *)NULL, /*hidden argument*/NULL);
		V_0 = L_5;
		V_1 = 0;
		goto IL_00cc;
	}

IL_003d:
	{
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0090;
		}
	}
	{
		CanvasU5BU5D_t376* L_7 = (__this->___gameCanvases_2);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		NullCheck((*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_7, L_9)));
		String_t* L_10 = Object_get_name_m798((*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_7, L_9)), /*hidden argument*/NULL);
		V_2 = L_10;
		CanvasU5BU5D_t376* L_11 = (__this->___gameCanvases_2);
		int32_t L_12 = V_1;
		CanvasU5BU5D_t376* L_13 = (__this->___gameCanvases_2);
		int32_t L_14 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		Canvas_t414 * L_16 = Object_Instantiate_TisCanvas_t414_m1789(NULL /*static, unused*/, (*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_13, L_15)), /*hidden argument*/Object_Instantiate_TisCanvas_t414_m1789_MethodInfo_var);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		ArrayElementTypeCheck (L_11, L_16);
		*((Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_11, L_12)) = (Canvas_t414 *)L_16;
		CanvasU5BU5D_t376* L_17 = (__this->___gameCanvases_2);
		int32_t L_18 = V_1;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		NullCheck((*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_17, L_19)));
		String_t* L_20 = Object_get_name_m798((*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_17, L_19)), /*hidden argument*/NULL);
		NullCheck(L_20);
		bool L_21 = String_Contains_m1584(L_20, (String_t*) &_stringLiteral353, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0090;
		}
	}
	{
		CanvasU5BU5D_t376* L_22 = (__this->___gameCanvases_2);
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		String_t* L_25 = V_2;
		NullCheck((*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_22, L_24)));
		Object_set_name_m1795((*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_22, L_24)), L_25, /*hidden argument*/NULL);
	}

IL_0090:
	{
		CanvasU5BU5D_t376* L_26 = (__this->___gameCanvases_2);
		int32_t L_27 = V_1;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		NullCheck((*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_26, L_28)));
		GameObject_t78 * L_29 = Component_get_gameObject_m622((*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_26, L_28)), /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m1602(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		CanvasU5BU5D_t376* L_30 = (__this->___gameCanvases_2);
		int32_t L_31 = V_1;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_31);
		int32_t L_32 = L_31;
		NullCheck((*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_30, L_32)));
		SCR_Menu_t336 * L_33 = Component_GetComponent_TisSCR_Menu_t336_m1790((*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_30, L_32)), /*hidden argument*/Component_GetComponent_TisSCR_Menu_t336_m1790_MethodInfo_var);
		V_3 = L_33;
		SCR_Menu_t336 * L_34 = V_3;
		bool L_35 = Object_op_Inequality_m623(NULL /*static, unused*/, L_34, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00c8;
		}
	}
	{
		List_1_t377 * L_36 = (__this->___menus_3);
		SCR_Menu_t336 * L_37 = V_3;
		NullCheck(L_36);
		VirtActionInvoker1< SCR_Menu_t336 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SCR_Menu>::Add(!0) */, L_36, L_37);
	}

IL_00c8:
	{
		int32_t L_38 = V_1;
		V_1 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00cc:
	{
		int32_t L_39 = V_1;
		CanvasU5BU5D_t376* L_40 = (__this->___gameCanvases_2);
		NullCheck(L_40);
		if ((((int32_t)L_39) < ((int32_t)(((int32_t)(((Array_t *)L_40)->max_length))))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_41 = Application_get_systemLanguage_m1796(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_10 = L_41;
		int32_t L_42 = V_10;
		if ((((int32_t)L_42) == ((int32_t)((int32_t)14))))
		{
			goto IL_00ef;
		}
	}
	{
		goto IL_00fb;
	}

IL_00ef:
	{
		__this->___isFrench_7 = 0;
		goto IL_0107;
	}

IL_00fb:
	{
		__this->___isFrench_7 = 1;
		goto IL_0107;
	}

IL_0107:
	{
		List_1_t377 * L_43 = (__this->___menus_3);
		NullCheck(L_43);
		Enumerator_t423  L_44 = List_1_GetEnumerator_m1797(L_43, /*hidden argument*/List_1_GetEnumerator_m1797_MethodInfo_var);
		V_5 = L_44;
	}

IL_0114:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0145;
		}

IL_0119:
		{
			SCR_Menu_t336 * L_45 = Enumerator_get_Current_m1798((&V_5), /*hidden argument*/Enumerator_get_Current_m1798_MethodInfo_var);
			V_4 = L_45;
			SCR_Menu_t336 * L_46 = V_4;
			NullCheck(L_46);
			SCR_PauseMenu_t374 * L_47 = Component_GetComponent_TisSCR_PauseMenu_t374_m1791(L_46, /*hidden argument*/Component_GetComponent_TisSCR_PauseMenu_t374_m1791_MethodInfo_var);
			__this->___pauseMenu_4 = L_47;
			SCR_PauseMenu_t374 * L_48 = (__this->___pauseMenu_4);
			bool L_49 = Object_op_Inequality_m623(NULL /*static, unused*/, L_48, (Object_t164 *)NULL, /*hidden argument*/NULL);
			if (!L_49)
			{
				goto IL_0145;
			}
		}

IL_0140:
		{
			goto IL_0151;
		}

IL_0145:
		{
			bool L_50 = Enumerator_MoveNext_m1799((&V_5), /*hidden argument*/Enumerator_MoveNext_m1799_MethodInfo_var);
			if (L_50)
			{
				goto IL_0119;
			}
		}

IL_0151:
		{
			IL2CPP_LEAVE(0x163, FINALLY_0156);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_0156;
	}

FINALLY_0156:
	{ // begin finally (depth: 1)
		Enumerator_t423  L_51 = V_5;
		Enumerator_t423  L_52 = L_51;
		Object_t * L_53 = Box(Enumerator_t423_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_53);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t233_il2cpp_TypeInfo_var, L_53);
		IL2CPP_END_FINALLY(342)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(342)
	{
		IL2CPP_JUMP_TBL(0x163, IL_0163)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_0163:
	{
		List_1_t377 * L_54 = (__this->___menus_3);
		NullCheck(L_54);
		Enumerator_t423  L_55 = List_1_GetEnumerator_m1797(L_54, /*hidden argument*/List_1_GetEnumerator_m1797_MethodInfo_var);
		V_7 = L_55;
	}

IL_0170:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01bd;
		}

IL_0175:
		{
			SCR_Menu_t336 * L_56 = Enumerator_get_Current_m1798((&V_7), /*hidden argument*/Enumerator_get_Current_m1798_MethodInfo_var);
			V_6 = L_56;
			SCR_Menu_t336 * L_57 = V_6;
			NullCheck(L_57);
			GameObject_t78 * L_58 = Component_get_gameObject_m622(L_57, /*hidden argument*/NULL);
			SCR_PauseMenu_t374 * L_59 = (__this->___pauseMenu_4);
			NullCheck(L_59);
			GameObject_t78 * L_60 = Component_get_gameObject_m622(L_59, /*hidden argument*/NULL);
			bool L_61 = Object_op_Inequality_m623(NULL /*static, unused*/, L_58, L_60, /*hidden argument*/NULL);
			if (!L_61)
			{
				goto IL_01bd;
			}
		}

IL_019a:
		{
			SCR_Menu_t336 * L_62 = V_6;
			NullCheck(L_62);
			SCR_MainMenu_t372 * L_63 = Component_GetComponent_TisSCR_MainMenu_t372_m1792(L_62, /*hidden argument*/Component_GetComponent_TisSCR_MainMenu_t372_m1792_MethodInfo_var);
			__this->___mainMenu_5 = L_63;
			SCR_MainMenu_t372 * L_64 = (__this->___mainMenu_5);
			bool L_65 = Object_op_Inequality_m623(NULL /*static, unused*/, L_64, (Object_t164 *)NULL, /*hidden argument*/NULL);
			if (!L_65)
			{
				goto IL_01bd;
			}
		}

IL_01b8:
		{
			goto IL_01c9;
		}

IL_01bd:
		{
			bool L_66 = Enumerator_MoveNext_m1799((&V_7), /*hidden argument*/Enumerator_MoveNext_m1799_MethodInfo_var);
			if (L_66)
			{
				goto IL_0175;
			}
		}

IL_01c9:
		{
			IL2CPP_LEAVE(0x1DB, FINALLY_01ce);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_01ce;
	}

FINALLY_01ce:
	{ // begin finally (depth: 1)
		Enumerator_t423  L_67 = V_7;
		Enumerator_t423  L_68 = L_67;
		Object_t * L_69 = Box(Enumerator_t423_il2cpp_TypeInfo_var, &L_68);
		NullCheck(L_69);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t233_il2cpp_TypeInfo_var, L_69);
		IL2CPP_END_FINALLY(462)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(462)
	{
		IL2CPP_JUMP_TBL(0x1DB, IL_01db)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_01db:
	{
		List_1_t377 * L_70 = (__this->___menus_3);
		NullCheck(L_70);
		Enumerator_t423  L_71 = List_1_GetEnumerator_m1797(L_70, /*hidden argument*/List_1_GetEnumerator_m1797_MethodInfo_var);
		V_9 = L_71;
	}

IL_01e8:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0251;
		}

IL_01ed:
		{
			SCR_Menu_t336 * L_72 = Enumerator_get_Current_m1798((&V_9), /*hidden argument*/Enumerator_get_Current_m1798_MethodInfo_var);
			V_8 = L_72;
			SCR_Menu_t336 * L_73 = V_8;
			NullCheck(L_73);
			GameObject_t78 * L_74 = Component_get_gameObject_m622(L_73, /*hidden argument*/NULL);
			SCR_PauseMenu_t374 * L_75 = (__this->___pauseMenu_4);
			NullCheck(L_75);
			GameObject_t78 * L_76 = Component_get_gameObject_m622(L_75, /*hidden argument*/NULL);
			bool L_77 = Object_op_Inequality_m623(NULL /*static, unused*/, L_74, L_76, /*hidden argument*/NULL);
			if (!L_77)
			{
				goto IL_0251;
			}
		}

IL_0212:
		{
			SCR_Menu_t336 * L_78 = V_8;
			NullCheck(L_78);
			GameObject_t78 * L_79 = Component_get_gameObject_m622(L_78, /*hidden argument*/NULL);
			SCR_MainMenu_t372 * L_80 = (__this->___mainMenu_5);
			NullCheck(L_80);
			GameObject_t78 * L_81 = Component_get_gameObject_m622(L_80, /*hidden argument*/NULL);
			bool L_82 = Object_op_Inequality_m623(NULL /*static, unused*/, L_79, L_81, /*hidden argument*/NULL);
			if (!L_82)
			{
				goto IL_0251;
			}
		}

IL_022e:
		{
			SCR_Menu_t336 * L_83 = V_8;
			NullCheck(L_83);
			SCR_LevelEndMenu_t367 * L_84 = Component_GetComponent_TisSCR_LevelEndMenu_t367_m1793(L_83, /*hidden argument*/Component_GetComponent_TisSCR_LevelEndMenu_t367_m1793_MethodInfo_var);
			__this->___lvlEndMenu_6 = L_84;
			SCR_LevelEndMenu_t367 * L_85 = (__this->___lvlEndMenu_6);
			bool L_86 = Object_op_Inequality_m623(NULL /*static, unused*/, L_85, (Object_t164 *)NULL, /*hidden argument*/NULL);
			if (!L_86)
			{
				goto IL_0251;
			}
		}

IL_024c:
		{
			goto IL_025d;
		}

IL_0251:
		{
			bool L_87 = Enumerator_MoveNext_m1799((&V_9), /*hidden argument*/Enumerator_MoveNext_m1799_MethodInfo_var);
			if (L_87)
			{
				goto IL_01ed;
			}
		}

IL_025d:
		{
			IL2CPP_LEAVE(0x26F, FINALLY_0262);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_0262;
	}

FINALLY_0262:
	{ // begin finally (depth: 1)
		Enumerator_t423  L_88 = V_9;
		Enumerator_t423  L_89 = L_88;
		Object_t * L_90 = Box(Enumerator_t423_il2cpp_TypeInfo_var, &L_89);
		NullCheck(L_90);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t233_il2cpp_TypeInfo_var, L_90);
		IL2CPP_END_FINALLY(610)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(610)
	{
		IL2CPP_JUMP_TBL(0x26F, IL_026f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_026f:
	{
		SCR_GUIManager_LevelInit_m1407(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_GUIManager::OnLevelWasLoaded()
extern "C" void SCR_GUIManager_OnLevelWasLoaded_m1406 (SCR_GUIManager_t378 * __this, const MethodInfo* method)
{
	{
		SCR_GUIManager_LevelInit_m1407(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_GUIManager::LevelInit()
extern "C" void SCR_GUIManager_LevelInit_m1407 (SCR_GUIManager_t378 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_loadedLevel_m1012(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0020;
		}
	}
	{
		SCR_GUIManager_CloseAllMenus_m1408(__this, /*hidden argument*/NULL);
		SCR_GUIManager_OpenMenu_m1409(__this, (String_t*) &_stringLiteral354, /*hidden argument*/NULL);
		goto IL_0031;
	}

IL_0020:
	{
		SCR_GUIManager_CloseAllMenus_m1408(__this, /*hidden argument*/NULL);
		SCR_GUIManager_OpenMenu_m1409(__this, (String_t*) &_stringLiteral355, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void SCR_GUIManager::CloseAllMenus()
extern "C" void SCR_GUIManager_CloseAllMenus_m1408 (SCR_GUIManager_t378 * __this, const MethodInfo* method)
{
	Canvas_t414 * V_0 = {0};
	CanvasU5BU5D_t376* V_1 = {0};
	int32_t V_2 = 0;
	{
		CanvasU5BU5D_t376* L_0 = (__this->___gameCanvases_2);
		V_1 = L_0;
		V_2 = 0;
		goto IL_002d;
	}

IL_000e:
	{
		CanvasU5BU5D_t376* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_1, L_3));
		Canvas_t414 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = Behaviour_get_isActiveAndEnabled_m1740(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		Canvas_t414 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_t78 * L_7 = Component_get_gameObject_m622(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_m713(L_7, 0, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_9 = V_2;
		CanvasU5BU5D_t376* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void SCR_GUIManager::OpenMenu(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void SCR_GUIManager_OpenMenu_m1409 (SCR_GUIManager_t378 * __this, String_t* ___menuName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	Canvas_t414 * V_0 = {0};
	CanvasU5BU5D_t376* V_1 = {0};
	int32_t V_2 = 0;
	{
		CanvasU5BU5D_t376* L_0 = (__this->___gameCanvases_2);
		V_1 = L_0;
		V_2 = 0;
		goto IL_0038;
	}

IL_000e:
	{
		CanvasU5BU5D_t376* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(Canvas_t414 **)(Canvas_t414 **)SZArrayLdElema(L_1, L_3));
		Canvas_t414 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = Object_get_name_m798(L_4, /*hidden argument*/NULL);
		String_t* L_6 = ___menuName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m636(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0034;
		}
	}
	{
		Canvas_t414 * L_8 = V_0;
		NullCheck(L_8);
		GameObject_t78 * L_9 = Component_get_gameObject_m622(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_SetActive_m713(L_9, 1, /*hidden argument*/NULL);
		goto IL_0041;
	}

IL_0034:
	{
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_11 = V_2;
		CanvasU5BU5D_t376* L_12 = V_1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_12)->max_length))))))
		{
			goto IL_000e;
		}
	}

IL_0041:
	{
		return;
	}
}
// System.Void SCR_GUIManager::OpenLevelMenus()
extern "C" void SCR_GUIManager_OpenLevelMenus_m1410 (SCR_GUIManager_t378 * __this, const MethodInfo* method)
{
	{
		SCR_GUIManager_CloseAllMenus_m1408(__this, /*hidden argument*/NULL);
		SCR_GUIManager_OpenMenu_m1409(__this, (String_t*) &_stringLiteral356, /*hidden argument*/NULL);
		SCR_GUIManager_OpenMenu_m1409(__this, (String_t*) &_stringLiteral357, /*hidden argument*/NULL);
		SCR_TimeManager_t383 * L_0 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_TimeManager_SetTimer_m1505(L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_GUIManager::RelayTextInfo(SCR_GUIManager/TextType)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void SCR_GUIManager_RelayTextInfo_m1411 (SCR_GUIManager_t378 * __this, int32_t ___collectibleType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	{
		int32_t L_0 = ___collectibleType;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0068;
		}
		if (L_1 == 2)
		{
			goto IL_00b8;
		}
		if (L_1 == 3)
		{
			goto IL_011e;
		}
	}
	{
		goto IL_0173;
	}

IL_001d:
	{
		SCR_TimeManager_t383 * L_2 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = SCR_TimeManager_get_IsCounting_m1508(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		SCR_PauseMenu_t374 * L_4 = (__this->___pauseMenu_4);
		SCR_PointManager_t381 * L_5 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = SCR_PointManager_get_LadybugsCurrentLevel_m1470(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		SCR_PauseMenu_ShowLadybugCollectibles_m1403(L_4, L_6, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_0046:
	{
		SCR_PauseMenu_t374 * L_7 = (__this->___pauseMenu_4);
		bool L_8 = Object_op_Inequality_m623(NULL /*static, unused*/, L_7, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0063;
		}
	}
	{
		SCR_PauseMenu_t374 * L_9 = (__this->___pauseMenu_4);
		NullCheck(L_9);
		SCR_PauseMenu_ShowLadybugCollectibles_m1403(L_9, 0, /*hidden argument*/NULL);
	}

IL_0063:
	{
		goto IL_0173;
	}

IL_0068:
	{
		SCR_TimeManager_t383 * L_10 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = SCR_TimeManager_get_IsCounting_m1508(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_009e;
		}
	}
	{
		SCR_PauseMenu_t374 * L_12 = (__this->___pauseMenu_4);
		SCR_PointManager_t381 * L_13 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = SCR_PointManager_get_PuceronsCurrentLevel_m1471(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		String_t* L_15 = Int32_ToString_m1615((&V_1), /*hidden argument*/NULL);
		NullCheck(L_12);
		SCR_Menu_ModifyText_m1211(L_12, (String_t*) &_stringLiteral358, L_15, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_009e:
	{
		SCR_PauseMenu_t374 * L_16 = (__this->___pauseMenu_4);
		NullCheck(L_16);
		SCR_Menu_ModifyText_m1211(L_16, (String_t*) &_stringLiteral359, (String_t*) &_stringLiteral360, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		goto IL_0173;
	}

IL_00b8:
	{
		SCR_TimeManager_t383 * L_17 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		bool L_18 = SCR_TimeManager_get_IsCounting_m1508(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00f3;
		}
	}
	{
		SCR_PauseMenu_t374 * L_19 = (__this->___pauseMenu_4);
		SCR_TimeManager_t383 * L_20 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		float L_21 = SCR_TimeManager_get_TimeSinceLevelStart_m1509(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_22 = roundf(L_21);
		V_2 = L_22;
		String_t* L_23 = Single_ToString_m1787((&V_2), /*hidden argument*/NULL);
		NullCheck(L_19);
		SCR_Menu_ModifyText_m1211(L_19, (String_t*) &_stringLiteral359, L_23, /*hidden argument*/NULL);
		goto IL_0119;
	}

IL_00f3:
	{
		SCR_PauseMenu_t374 * L_24 = (__this->___pauseMenu_4);
		bool L_25 = Object_op_Inequality_m623(NULL /*static, unused*/, L_24, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0119;
		}
	}
	{
		SCR_PauseMenu_t374 * L_26 = (__this->___pauseMenu_4);
		NullCheck(L_26);
		SCR_Menu_ModifyText_m1211(L_26, (String_t*) &_stringLiteral359, (String_t*) &_stringLiteral360, /*hidden argument*/NULL);
	}

IL_0119:
	{
		goto IL_0173;
	}

IL_011e:
	{
		SCR_LevelEndMenu_t367 * L_27 = (__this->___lvlEndMenu_6);
		SCR_PointManager_t381 * L_28 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = SCR_PointManager_get_TotalScore_m1472(L_28, /*hidden argument*/NULL);
		V_3 = L_29;
		String_t* L_30 = Int32_ToString_m1615((&V_3), /*hidden argument*/NULL);
		NullCheck(L_27);
		SCR_Menu_ModifyText_m1211(L_27, (String_t*) &_stringLiteral361, L_30, /*hidden argument*/NULL);
		SCR_LevelEndMenu_t367 * L_31 = (__this->___lvlEndMenu_6);
		SCR_PointManager_t381 * L_32 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_32);
		Int32U5BU5D_t242* L_33 = SCR_PointManager_get_HighScores_m1473(L_32, /*hidden argument*/NULL);
		SCR_LevelManager_t379 * L_34 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_34);
		int32_t L_35 = SCR_LevelManager_get_GameplayLevel_m1422(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_35);
		String_t* L_36 = Int32_ToString_m1615(((int32_t*)(int32_t*)SZArrayLdElema(L_33, L_35)), /*hidden argument*/NULL);
		NullCheck(L_31);
		SCR_Menu_ModifyText_m1211(L_31, (String_t*) &_stringLiteral362, L_36, /*hidden argument*/NULL);
		goto IL_0173;
	}

IL_0173:
	{
		return;
	}
}
// System.Void SCR_GUIManager::ToggleLanguage(System.Boolean)
extern TypeInfo* Enumerator_t423_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t233_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1797_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1798_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1799_MethodInfo_var;
extern "C" void SCR_GUIManager_ToggleLanguage_m1412 (SCR_GUIManager_t378 * __this, bool ___french, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(699);
		IDisposable_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		List_1_GetEnumerator_m1797_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483727);
		Enumerator_get_Current_m1798_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483728);
		Enumerator_MoveNext_m1799_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483730);
		s_Il2CppMethodIntialized = true;
	}
	SCR_Menu_t336 * V_0 = {0};
	Enumerator_t423  V_1 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (__this->___isFrench_7);
		__this->___isFrench_7 = ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		List_1_t377 * L_1 = (__this->___menus_3);
		NullCheck(L_1);
		Enumerator_t423  L_2 = List_1_GetEnumerator_m1797(L_1, /*hidden argument*/List_1_GetEnumerator_m1797_MethodInfo_var);
		V_1 = L_2;
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002e;
		}

IL_0020:
		{
			SCR_Menu_t336 * L_3 = Enumerator_get_Current_m1798((&V_1), /*hidden argument*/Enumerator_get_Current_m1798_MethodInfo_var);
			V_0 = L_3;
			SCR_Menu_t336 * L_4 = V_0;
			NullCheck(L_4);
			SCR_Menu_SetMenuLanguage_m1217(L_4, /*hidden argument*/NULL);
		}

IL_002e:
		{
			bool L_5 = Enumerator_MoveNext_m1799((&V_1), /*hidden argument*/Enumerator_MoveNext_m1799_MethodInfo_var);
			if (L_5)
			{
				goto IL_0020;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x4B, FINALLY_003f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		Enumerator_t423  L_6 = V_1;
		Enumerator_t423  L_7 = L_6;
		Object_t * L_8 = Box(Enumerator_t423_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t233_il2cpp_TypeInfo_var, L_8);
		IL2CPP_END_FINALLY(63)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_004b:
	{
		return;
	}
}
// System.Boolean SCR_GUIManager::get_IsFrench()
extern "C" bool SCR_GUIManager_get_IsFrench_m1413 (SCR_GUIManager_t378 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isFrench_7);
		return L_0;
	}
}
// SCR_LevelEndMenu SCR_GUIManager::get_LevelEndMenu()
extern "C" SCR_LevelEndMenu_t367 * SCR_GUIManager_get_LevelEndMenu_m1414 (SCR_GUIManager_t378 * __this, const MethodInfo* method)
{
	{
		SCR_LevelEndMenu_t367 * L_0 = (__this->___lvlEndMenu_6);
		return L_0;
	}
}
// SCR_MainMenu SCR_GUIManager::get_MainMenu()
extern "C" SCR_MainMenu_t372 * SCR_GUIManager_get_MainMenu_m1415 (SCR_GUIManager_t378 * __this, const MethodInfo* method)
{
	{
		SCR_MainMenu_t372 * L_0 = (__this->___mainMenu_5);
		return L_0;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void SCR_LevelManager::.ctor()
extern "C" void SCR_LevelManager__ctor_m1416 (SCR_LevelManager_t379 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_LevelManager::Awake()
extern "C" void SCR_LevelManager_Awake_m1417 (SCR_LevelManager_t379 * __this, const MethodInfo* method)
{
	{
		SCR_LevelManager_t379 * L_0 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		SCR_ManagerComponents_set_LevelManager_m1432(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		return;
	}
}
// System.Void SCR_LevelManager::OnLevelWasLoaded()
extern "C" void SCR_LevelManager_OnLevelWasLoaded_m1418 (SCR_LevelManager_t379 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_loadedLevel_m1012(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___currentLevel_3 = L_0;
		int32_t L_1 = (__this->___currentLevel_3);
		__this->___isMenu_5 = ((((int32_t)L_1) < ((int32_t)1))? 1 : 0);
		bool L_2 = (__this->___isMenu_5);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		__this->___gameplayLevel_4 = 0;
		goto IL_003d;
	}

IL_0031:
	{
		int32_t L_3 = (__this->___currentLevel_3);
		__this->___gameplayLevel_4 = L_3;
	}

IL_003d:
	{
		return;
	}
}
// System.Void SCR_LevelManager::EndLevel()
extern "C" void SCR_LevelManager_EndLevel_m1419 (SCR_LevelManager_t379 * __this, const MethodInfo* method)
{
	{
		SCR_PointManager_t381 * L_0 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_PointManager_ValidateScores_m1465(L_0, /*hidden argument*/NULL);
		SCR_TimeManager_t383 * L_1 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		SCR_TimeManager_Pause_m1506(L_1, /*hidden argument*/NULL);
		SCR_TimeManager_t383 * L_2 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_TimeManager_SetTimer_m1505(L_2, 0, /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_3 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_GUIManager_CloseAllMenus_m1408(L_3, /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_4 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		SCR_GUIManager_OpenMenu_m1409(L_4, (String_t*) &_stringLiteral363, /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_5 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		SCR_GUIManager_RelayTextInfo_m1411(L_5, 3, /*hidden argument*/NULL);
		SCR_SoundManager_t335 * L_6 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		SCR_SoundManager_PlaySound_m1495(L_6, (String_t*) &_stringLiteral364, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_LevelManager::LoadLevel(System.Int32)
extern "C" void SCR_LevelManager_LoadLevel_m1420 (SCR_LevelManager_t379 * __this, int32_t ___level, const MethodInfo* method)
{
	{
		SCR_TimeManager_t383 * L_0 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = SCR_TimeManager_get_IsPaused_m1507(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		SCR_TimeManager_t383 * L_2 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_TimeManager_Pause_m1506(L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		bool L_3 = Application_get_isLoadingLevel_m1800(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_4 = ___level;
		Application_LoadLevel_m1013(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Int32 SCR_LevelManager::get_CurrentLevel()
extern "C" int32_t SCR_LevelManager_get_CurrentLevel_m1421 (SCR_LevelManager_t379 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___currentLevel_3);
		return L_0;
	}
}
// System.Int32 SCR_LevelManager::get_GameplayLevel()
extern "C" int32_t SCR_LevelManager_get_GameplayLevel_m1422 (SCR_LevelManager_t379 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___gameplayLevel_4);
		return L_0;
	}
}
// System.Boolean SCR_LevelManager::get_IsMenu()
extern "C" bool SCR_LevelManager_get_IsMenu_m1423 (SCR_LevelManager_t379 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isMenu_5);
		return L_0;
	}
}
#ifndef _MSC_VER
#else
#endif

// SCR_SaveData
#include "AssemblyU2DCSharp_SCR_SaveData.h"
// SCR_Spider
#include "AssemblyU2DCSharp_SCR_Spider.h"
struct GameObject_t78;
struct SCR_Camie_t338;
// Declaration !!0 UnityEngine.GameObject::GetComponent<SCR_Camie>()
// !!0 UnityEngine.GameObject::GetComponent<SCR_Camie>()
#define GameObject_GetComponent_TisSCR_Camie_t338_m1801(__this, method) (( SCR_Camie_t338 * (*) (GameObject_t78 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m771_gshared)(__this, method)


// System.Void SCR_ManagerComponents::.ctor()
extern "C" void SCR_ManagerComponents__ctor_m1424 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_ManagerComponents::Awake()
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSCR_Camie_t338_m1801_MethodInfo_var;
extern "C" void SCR_ManagerComponents_Awake_m1425 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		GameObject_GetComponent_TisSCR_Camie_t338_m1801_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483733);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_ManagerComponents_t380 * L_0 = ((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		bool L_1 = Object_op_Inequality_m623(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_2 = ((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		bool L_3 = Object_op_Inequality_m623(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		GameObject_t78 * L_4 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}

IL_002c:
	{
		((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___instance_2 = __this;
		GameObject_t78 * L_5 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m1602(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		String_t* L_6 = Application_get_loadedLevelName_m637(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = String_Contains_m1584(L_6, (String_t*) &_stringLiteral365, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_8 = Application_get_loadedLevelName_m637(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = String_Contains_m1584(L_8, (String_t*) &_stringLiteral366, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007a;
		}
	}

IL_0065:
	{
		GameObject_t78 * L_10 = GameObject_FindGameObjectWithTag_m608(NULL /*static, unused*/, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_10);
		SCR_Camie_t338 * L_11 = GameObject_GetComponent_TisSCR_Camie_t338_m1801(L_10, /*hidden argument*/GameObject_GetComponent_TisSCR_Camie_t338_m1801_MethodInfo_var);
		__this->___avMove_9 = L_11;
	}

IL_007a:
	{
		return;
	}
}
// System.Void SCR_ManagerComponents::OnLevelWasLoaded()
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSCR_Camie_t338_m1801_MethodInfo_var;
extern "C" void SCR_ManagerComponents_OnLevelWasLoaded_m1426 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		GameObject_GetComponent_TisSCR_Camie_t338_m1801_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483733);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_ManagerComponents_t380 * L_0 = ((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_2 = Application_get_loadedLevelName_m637(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = String_Contains_m1584(L_2, (String_t*) &_stringLiteral365, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_4 = Application_get_loadedLevelName_m637(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = String_Contains_m1584(L_4, (String_t*) &_stringLiteral366, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004d;
		}
	}

IL_0038:
	{
		GameObject_t78 * L_6 = GameObject_FindGameObjectWithTag_m608(NULL /*static, unused*/, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_6);
		SCR_Camie_t338 * L_7 = GameObject_GetComponent_TisSCR_Camie_t338_m1801(L_6, /*hidden argument*/GameObject_GetComponent_TisSCR_Camie_t338_m1801_MethodInfo_var);
		__this->___avMove_9 = L_7;
	}

IL_004d:
	{
		return;
	}
}
// SCR_ManagerComponents SCR_ManagerComponents::get_ManagerInstance()
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" SCR_ManagerComponents_t380 * SCR_ManagerComponents_get_ManagerInstance_m1427 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_ManagerComponents_t380 * L_0 = ((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_ManagerInstance(SCR_ManagerComponents)
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" void SCR_ManagerComponents_set_ManagerInstance_m1428 (Object_t * __this /* static, unused */, SCR_ManagerComponents_t380 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_ManagerComponents_t380 * L_0 = ___value;
		((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___instance_2 = L_0;
		return;
	}
}
// SCR_PointManager SCR_ManagerComponents::get_PointManager()
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" SCR_PointManager_t381 * SCR_ManagerComponents_get_PointManager_m1429 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_PointManager_t381 * L_0 = ((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___pointManager_3;
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_PointManager(SCR_PointManager)
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" void SCR_ManagerComponents_set_PointManager_m1430 (Object_t * __this /* static, unused */, SCR_PointManager_t381 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_PointManager_t381 * L_0 = ___value;
		((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___pointManager_3 = L_0;
		return;
	}
}
// SCR_LevelManager SCR_ManagerComponents::get_LevelManager()
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" SCR_LevelManager_t379 * SCR_ManagerComponents_get_LevelManager_m1431 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_LevelManager_t379 * L_0 = ((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___levelManager_4;
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_LevelManager(SCR_LevelManager)
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" void SCR_ManagerComponents_set_LevelManager_m1432 (Object_t * __this /* static, unused */, SCR_LevelManager_t379 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_LevelManager_t379 * L_0 = ___value;
		((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___levelManager_4 = L_0;
		return;
	}
}
// SCR_SaveData SCR_ManagerComponents::get_SaveData()
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" SCR_SaveData_t382 * SCR_ManagerComponents_get_SaveData_m1433 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_SaveData_t382 * L_0 = ((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___saveData_5;
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_SaveData(SCR_SaveData)
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" void SCR_ManagerComponents_set_SaveData_m1434 (Object_t * __this /* static, unused */, SCR_SaveData_t382 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_SaveData_t382 * L_0 = ___value;
		((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___saveData_5 = L_0;
		return;
	}
}
// SCR_TimeManager SCR_ManagerComponents::get_TimeManager()
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" SCR_TimeManager_t383 * SCR_ManagerComponents_get_TimeManager_m1435 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_TimeManager_t383 * L_0 = ((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___timeManager_6;
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_TimeManager(SCR_TimeManager)
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" void SCR_ManagerComponents_set_TimeManager_m1436 (Object_t * __this /* static, unused */, SCR_TimeManager_t383 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_TimeManager_t383 * L_0 = ___value;
		((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___timeManager_6 = L_0;
		return;
	}
}
// SCR_GUIManager SCR_ManagerComponents::get_GUIManager()
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" SCR_GUIManager_t378 * SCR_ManagerComponents_get_GUIManager_m1437 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_GUIManager_t378 * L_0 = ((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___guiManager_7;
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_GUIManager(SCR_GUIManager)
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" void SCR_ManagerComponents_set_GUIManager_m1438 (Object_t * __this /* static, unused */, SCR_GUIManager_t378 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_GUIManager_t378 * L_0 = ___value;
		((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___guiManager_7 = L_0;
		return;
	}
}
// SCR_SoundManager SCR_ManagerComponents::get_SoundManager()
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" SCR_SoundManager_t335 * SCR_ManagerComponents_get_SoundManager_m1439 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_SoundManager_t335 * L_0 = ((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___soundManager_8;
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_SoundManager(SCR_SoundManager)
extern TypeInfo* SCR_ManagerComponents_t380_il2cpp_TypeInfo_var;
extern "C" void SCR_ManagerComponents_set_SoundManager_m1440 (Object_t * __this /* static, unused */, SCR_SoundManager_t335 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SCR_ManagerComponents_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_SoundManager_t335 * L_0 = ___value;
		((SCR_ManagerComponents_t380_StaticFields*)SCR_ManagerComponents_t380_il2cpp_TypeInfo_var->static_fields)->___soundManager_8 = L_0;
		return;
	}
}
// SCR_Camie SCR_ManagerComponents::get_AvatarMove()
extern "C" SCR_Camie_t338 * SCR_ManagerComponents_get_AvatarMove_m1441 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method)
{
	{
		SCR_Camie_t338 * L_0 = (__this->___avMove_9);
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_AvatarMove(SCR_Camie)
extern "C" void SCR_ManagerComponents_set_AvatarMove_m1442 (SCR_ManagerComponents_t380 * __this, SCR_Camie_t338 * ___value, const MethodInfo* method)
{
	{
		SCR_Camie_t338 * L_0 = ___value;
		__this->___avMove_9 = L_0;
		return;
	}
}
// SCR_Spider SCR_ManagerComponents::get_Spider()
extern "C" SCR_Spider_t384 * SCR_ManagerComponents_get_Spider_m1443 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method)
{
	{
		SCR_Spider_t384 * L_0 = (__this->___sceneSpider_12);
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_Spider(SCR_Spider)
extern "C" void SCR_ManagerComponents_set_Spider_m1444 (SCR_ManagerComponents_t380 * __this, SCR_Spider_t384 * ___value, const MethodInfo* method)
{
	{
		SCR_Spider_t384 * L_0 = ___value;
		__this->___sceneSpider_12 = L_0;
		return;
	}
}
// SCR_Joystick SCR_ManagerComponents::get_Joystick()
extern "C" SCR_Joystick_t346 * SCR_ManagerComponents_get_Joystick_m1445 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method)
{
	{
		SCR_Joystick_t346 * L_0 = (__this->___joystick_10);
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_Joystick(SCR_Joystick)
extern "C" void SCR_ManagerComponents_set_Joystick_m1446 (SCR_ManagerComponents_t380 * __this, SCR_Joystick_t346 * ___value, const MethodInfo* method)
{
	{
		SCR_Joystick_t346 * L_0 = ___value;
		__this->___joystick_10 = L_0;
		return;
	}
}
// SCR_FlyButton SCR_ManagerComponents::get_FlyButton()
extern "C" SCR_FlyButton_t345 * SCR_ManagerComponents_get_FlyButton_m1447 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method)
{
	{
		SCR_FlyButton_t345 * L_0 = (__this->___flyButton_11);
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_FlyButton(SCR_FlyButton)
extern "C" void SCR_ManagerComponents_set_FlyButton_m1448 (SCR_ManagerComponents_t380 * __this, SCR_FlyButton_t345 * ___value, const MethodInfo* method)
{
	{
		SCR_FlyButton_t345 * L_0 = ___value;
		__this->___flyButton_11 = L_0;
		return;
	}
}
// UnityEngine.Transform SCR_ManagerComponents::get_LeftFrame()
extern "C" Transform_t1 * SCR_ManagerComponents_get_LeftFrame_m1449 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = (__this->___leftFrame_13);
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_LeftFrame(UnityEngine.Transform)
extern "C" void SCR_ManagerComponents_set_LeftFrame_m1450 (SCR_ManagerComponents_t380 * __this, Transform_t1 * ___value, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = ___value;
		__this->___leftFrame_13 = L_0;
		return;
	}
}
// UnityEngine.Transform SCR_ManagerComponents::get_RightFrame()
extern "C" Transform_t1 * SCR_ManagerComponents_get_RightFrame_m1451 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = (__this->___rightFrame_14);
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_RightFrame(UnityEngine.Transform)
extern "C" void SCR_ManagerComponents_set_RightFrame_m1452 (SCR_ManagerComponents_t380 * __this, Transform_t1 * ___value, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = ___value;
		__this->___rightFrame_14 = L_0;
		return;
	}
}
// UnityEngine.Transform SCR_ManagerComponents::get_BottomFrame()
extern "C" Transform_t1 * SCR_ManagerComponents_get_BottomFrame_m1453 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = (__this->___bottomFrame_15);
		return L_0;
	}
}
// System.Void SCR_ManagerComponents::set_BottomFrame(UnityEngine.Transform)
extern "C" void SCR_ManagerComponents_set_BottomFrame_m1454 (SCR_ManagerComponents_t380 * __this, Transform_t1 * ___value, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = ___value;
		__this->___bottomFrame_15 = L_0;
		return;
	}
}
// SCR_ManagerInstantiator
#include "AssemblyU2DCSharp_SCR_ManagerInstantiator.h"
#ifndef _MSC_VER
#else
#endif
// SCR_ManagerInstantiator
#include "AssemblyU2DCSharp_SCR_ManagerInstantiatorMethodDeclarations.h"

struct Object_t164;
struct SCR_ManagerComponents_t380;
// Declaration !!0 UnityEngine.Object::Instantiate<SCR_ManagerComponents>(!!0)
// !!0 UnityEngine.Object::Instantiate<SCR_ManagerComponents>(!!0)
#define Object_Instantiate_TisSCR_ManagerComponents_t380_m1802(__this /* static, unused */, p0, method) (( SCR_ManagerComponents_t380 * (*) (Object_t * /* static, unused */, SCR_ManagerComponents_t380 *, const MethodInfo*))Object_Instantiate_TisObject_t_m953_gshared)(__this /* static, unused */, p0, method)


// System.Void SCR_ManagerInstantiator::.ctor()
extern "C" void SCR_ManagerInstantiator__ctor_m1455 (SCR_ManagerInstantiator_t385 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_ManagerInstantiator::Awake()
extern const MethodInfo* Object_Instantiate_TisSCR_ManagerComponents_t380_m1802_MethodInfo_var;
extern "C" void SCR_ManagerInstantiator_Awake_m1456 (SCR_ManagerInstantiator_t385 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_Instantiate_TisSCR_ManagerComponents_t380_m1802_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483734);
		s_Il2CppMethodIntialized = true;
	}
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_2 = (__this->___manager_2);
		SCR_ManagerComponents_t380 * L_3 = Object_Instantiate_TisSCR_ManagerComponents_t380_m1802(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisSCR_ManagerComponents_t380_m1802_MethodInfo_var);
		__this->___manager_2 = L_3;
	}

IL_0021:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// SCR_PointManager/CollectibleType
#include "AssemblyU2DCSharp_SCR_PointManager_CollectibleTypeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif

// SCR_SaveData
#include "AssemblyU2DCSharp_SCR_SaveDataMethodDeclarations.h"


// System.Void SCR_PointManager::.ctor()
extern TypeInfo* Int32U5BU5D_t242_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t387_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1803_MethodInfo_var;
extern "C" void SCR_PointManager__ctor_m1457 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		List_1_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(703);
		List_1__ctor_m1803_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483735);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___pointsForLevelAccess_7 = ((Int32U5BU5D_t242*)SZArrayNew(Int32U5BU5D_t242_il2cpp_TypeInfo_var, ((int32_t)30)));
		__this->___puceronsStaticInLevel_11 = ((int32_t)10);
		List_1_t387 * L_0 = (List_1_t387 *)il2cpp_codegen_object_new (List_1_t387_il2cpp_TypeInfo_var);
		List_1__ctor_m1803(L_0, /*hidden argument*/List_1__ctor_m1803_MethodInfo_var);
		__this->___puceronsCollected_12 = L_0;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_PointManager::Awake()
extern TypeInfo* Int32U5BU5D_t242_il2cpp_TypeInfo_var;
extern "C" void SCR_PointManager_Awake_m1458 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		SCR_PointManager_t381 * L_0 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		SCR_ManagerComponents_set_PointManager_m1430(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		__this->___highscorePerLevel_13 = ((Int32U5BU5D_t242*)SZArrayNew(Int32U5BU5D_t242_il2cpp_TypeInfo_var, ((int32_t)30)));
		V_0 = 0;
		V_1 = 0;
		goto IL_0047;
	}

IL_0032:
	{
		Int32U5BU5D_t242* L_2 = (__this->___pointsForLevelAccess_7);
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_2, L_3)) = (int32_t)L_4;
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)((int32_t)600)));
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)30))))
		{
			goto IL_0032;
		}
	}
	{
		__this->___isScoreCounting_16 = 0;
		return;
	}
}
// System.Void SCR_PointManager::Start()
extern "C" void SCR_PointManager_Start_m1459 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		SCR_PointManager_LevelInit_m1462(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_PointManager::OnLevelWasLoaded()
extern "C" void SCR_PointManager_OnLevelWasLoaded_m1460 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		SCR_PointManager_LevelInit_m1462(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_PointManager::Update()
extern "C" void SCR_PointManager_Update_m1461 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___puceronsCurrLevel_9);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0026;
		}
	}
	{
		SCR_TimeManager_t383 * L_1 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = SCR_TimeManager_get_IsCounting_m1508(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		SCR_LevelManager_t379 * L_3 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_LevelManager_EndLevel_m1419(L_3, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void SCR_PointManager::LevelInit()
extern "C" void SCR_PointManager_LevelInit_m1462 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		__this->___puceronsMovingInLevel_10 = 0;
		__this->___puceronsStaticInLevel_11 = ((int32_t)10);
		__this->___totalScoreCurr_15 = 0;
		__this->___puceronsCurrLevel_9 = 0;
		__this->___ladybugsCurrLevel_8 = 0;
		List_1_t387 * L_0 = (__this->___puceronsCollected_12);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<SCR_Puceron>::Clear() */, L_0);
		SCR_GUIManager_t378 * L_1 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		SCR_GUIManager_RelayTextInfo_m1411(L_1, 0, /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_2 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_GUIManager_RelayTextInfo_m1411(L_2, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_PointManager::StartScoring()
extern "C" void SCR_PointManager_StartScoring_m1463 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		__this->___isScoreCounting_16 = 1;
		return;
	}
}
// System.Void SCR_PointManager::IncrementMovingPucerons()
extern "C" void SCR_PointManager_IncrementMovingPucerons_m1464 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___puceronsMovingInLevel_10);
		__this->___puceronsMovingInLevel_10 = ((int32_t)((int32_t)L_0+(int32_t)1));
		int32_t L_1 = (__this->___puceronsStaticInLevel_11);
		__this->___puceronsStaticInLevel_11 = ((int32_t)((int32_t)L_1-(int32_t)1));
		return;
	}
}
// System.Void SCR_PointManager::ValidateScores()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void SCR_PointManager_ValidateScores_m1465 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___isScoreCounting_16 = 0;
		Int32U5BU5D_t242* L_0 = SCR_PointManager_get_HighScores_m1473(__this, /*hidden argument*/NULL);
		SCR_LevelManager_t379 * L_1 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = SCR_LevelManager_get_GameplayLevel_m1422(L_1, /*hidden argument*/NULL);
		int32_t L_3 = (__this->___ladybugsCurrLevel_8);
		int32_t L_4 = (__this->___puceronsStaticInLevel_11);
		SCR_TimeManager_t383 * L_5 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		float L_6 = SCR_TimeManager_get_TimeSinceLevelStart_m1509(L_5, /*hidden argument*/NULL);
		int32_t L_7 = (__this->___puceronsMovingInLevel_10);
		SCR_TimeManager_t383 * L_8 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		float L_9 = SCR_TimeManager_get_TimeSinceLevelStart_m1509(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_CeilToInt_m867(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)((int32_t)((int32_t)((int32_t)200)*(int32_t)L_3))))+(float)((float)((float)(((float)((int32_t)((int32_t)7*(int32_t)L_4))))/(float)((float)((float)L_6/(float)(75.0f)))))))+(float)((float)((float)(((float)((int32_t)((int32_t)((int32_t)10)*(int32_t)L_7))))/(float)((float)((float)L_9/(float)(75.0f))))))), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_2);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, L_2)) = (int32_t)L_10;
		SCR_LevelManager_t379 * L_11 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = SCR_LevelManager_get_GameplayLevel_m1422(L_11, /*hidden argument*/NULL);
		bool L_13 = SCR_PointManager_CompareHighscore_m1468(__this, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0086;
		}
	}
	{
		SCR_LevelManager_t379 * L_14 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = SCR_LevelManager_get_GameplayLevel_m1422(L_14, /*hidden argument*/NULL);
		SCR_PointManager_SetNewHighScore_m1469(__this, L_15, /*hidden argument*/NULL);
	}

IL_0086:
	{
		return;
	}
}
// System.Void SCR_PointManager::AcquireCollectible(SCR_PointManager/CollectibleType)
extern "C" void SCR_PointManager_AcquireCollectible_m1466 (SCR_PointManager_t381 * __this, int32_t ___type, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___type;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_008e;
	}

IL_0014:
	{
		int32_t L_3 = (__this->___ladybugsCurrLevel_8);
		__this->___ladybugsCurrLevel_8 = ((int32_t)((int32_t)L_3+(int32_t)1));
		int32_t L_4 = (__this->___ladybugsCurrLevel_8);
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_0042;
		}
	}
	{
		SCR_SoundManager_t335 * L_5 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		SCR_SoundManager_PlaySound_m1495(L_5, (String_t*) &_stringLiteral367, /*hidden argument*/NULL);
		goto IL_0051;
	}

IL_0042:
	{
		SCR_SoundManager_t335 * L_6 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		SCR_SoundManager_PlaySound_m1495(L_6, (String_t*) &_stringLiteral368, /*hidden argument*/NULL);
	}

IL_0051:
	{
		SCR_GUIManager_t378 * L_7 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		SCR_GUIManager_RelayTextInfo_m1411(L_7, 0, /*hidden argument*/NULL);
		goto IL_008e;
	}

IL_0061:
	{
		int32_t L_8 = (__this->___puceronsCurrLevel_9);
		__this->___puceronsCurrLevel_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		SCR_SoundManager_t335 * L_9 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		SCR_SoundManager_PlaySound_m1495(L_9, (String_t*) &_stringLiteral369, /*hidden argument*/NULL);
		SCR_GUIManager_t378 * L_10 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		SCR_GUIManager_RelayTextInfo_m1411(L_10, 1, /*hidden argument*/NULL);
		goto IL_008e;
	}

IL_008e:
	{
		return;
	}
}
// System.Void SCR_PointManager::LosePucerons()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_TrimExcess_m1804_MethodInfo_var;
extern "C" void SCR_PointManager_LosePucerons_m1467 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		List_1_TrimExcess_m1804_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483736);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	SCR_Puceron_t356 * V_1 = {0};
	{
		List_1_t387 * L_0 = (__this->___puceronsCollected_12);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SCR_Puceron>::get_Count() */, L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00a9;
		}
	}
	{
		SCR_SoundManager_t335 * L_2 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_SoundManager_PlaySound_m1495(L_2, (String_t*) &_stringLiteral370, /*hidden argument*/NULL);
		SCR_ManagerComponents_t380 * L_3 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_Camie_t338 * L_4 = SCR_ManagerComponents_get_AvatarMove_m1441(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		SCR_Camie_LosePucerons_m1259(L_4, /*hidden argument*/NULL);
		V_0 = 1;
		List_1_t387 * L_5 = (__this->___puceronsCollected_12);
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SCR_Puceron>::get_Count() */, L_5);
		if ((((int32_t)L_6) <= ((int32_t)1)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = (__this->___puceronsCurrLevel_9);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_8 = Mathf_FloorToInt_m815(NULL /*static, unused*/, ((float)((float)(((float)L_7))/(float)(2.0f))), /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0055:
	{
		goto IL_009d;
	}

IL_005a:
	{
		List_1_t387 * L_9 = (__this->___puceronsCollected_12);
		int32_t L_10 = V_0;
		int32_t L_11 = Random_Range_m896(NULL /*static, unused*/, 0, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		SCR_Puceron_t356 * L_12 = (SCR_Puceron_t356 *)VirtFuncInvoker1< SCR_Puceron_t356 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SCR_Puceron>::get_Item(System.Int32) */, L_9, L_11);
		V_1 = L_12;
		SCR_Puceron_t356 * L_13 = V_1;
		NullCheck(L_13);
		SCR_Puceron_ReturnToGame_m1314(L_13, /*hidden argument*/NULL);
		List_1_t387 * L_14 = (__this->___puceronsCollected_12);
		SCR_Puceron_t356 * L_15 = V_1;
		NullCheck(L_14);
		VirtFuncInvoker1< bool, SCR_Puceron_t356 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<SCR_Puceron>::Remove(!0) */, L_14, L_15);
		List_1_t387 * L_16 = (__this->___puceronsCollected_12);
		NullCheck(L_16);
		List_1_TrimExcess_m1804(L_16, /*hidden argument*/List_1_TrimExcess_m1804_MethodInfo_var);
		int32_t L_17 = (__this->___puceronsCurrLevel_9);
		__this->___puceronsCurrLevel_9 = ((int32_t)((int32_t)L_17-(int32_t)1));
		int32_t L_18 = V_0;
		V_0 = ((int32_t)((int32_t)L_18-(int32_t)1));
	}

IL_009d:
	{
		int32_t L_19 = V_0;
		if ((((int32_t)L_19) > ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		goto IL_00b3;
	}

IL_00a9:
	{
		SCR_LevelManager_t379 * L_20 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		SCR_LevelManager_EndLevel_m1419(L_20, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		SCR_GUIManager_t378 * L_21 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		SCR_GUIManager_RelayTextInfo_m1411(L_21, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SCR_PointManager::CompareHighscore(System.Int32)
extern "C" bool SCR_PointManager_CompareHighscore_m1468 (SCR_PointManager_t381 * __this, int32_t ___level, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___totalScoreCurr_15);
		Int32U5BU5D_t242* L_1 = (__this->___highscorePerLevel_13);
		int32_t L_2 = ___level;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		return ((((int32_t)L_0) > ((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_3))))? 1 : 0);
	}
}
// System.Void SCR_PointManager::SetNewHighScore(System.Int32)
extern "C" void SCR_PointManager_SetNewHighScore_m1469 (SCR_PointManager_t381 * __this, int32_t ___level, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Int32U5BU5D_t242* V_1 = {0};
	int32_t V_2 = 0;
	{
		Int32U5BU5D_t242* L_0 = (__this->___highscorePerLevel_13);
		int32_t L_1 = ___level;
		int32_t L_2 = (__this->___totalScoreCurr_15);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, L_1)) = (int32_t)L_2;
		SCR_GUIManager_t378 * L_3 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_LevelEndMenu_t367 * L_4 = SCR_GUIManager_get_LevelEndMenu_m1414(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		SCR_LevelEndMenu_NewHighScore_m1370(L_4, /*hidden argument*/NULL);
		SCR_SaveData_t382 * L_5 = SCR_ManagerComponents_get_SaveData_m1433(NULL /*static, unused*/, /*hidden argument*/NULL);
		SCR_LevelManager_t379 * L_6 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = SCR_LevelManager_get_GameplayLevel_m1422(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		SCR_SaveData_Save_m1485(L_5, L_7, /*hidden argument*/NULL);
		__this->___scoreCumul_14 = 0;
		Int32U5BU5D_t242* L_8 = (__this->___highscorePerLevel_13);
		V_1 = L_8;
		V_2 = 0;
		goto IL_005c;
	}

IL_0046:
	{
		Int32U5BU5D_t242* L_9 = V_1;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		V_0 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_9, L_11));
		int32_t L_12 = (__this->___scoreCumul_14);
		int32_t L_13 = V_0;
		__this->___scoreCumul_14 = ((int32_t)((int32_t)L_12+(int32_t)L_13));
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_15 = V_2;
		Int32U5BU5D_t242* L_16 = V_1;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)(((Array_t *)L_16)->max_length))))))
		{
			goto IL_0046;
		}
	}
	{
		return;
	}
}
// System.Int32 SCR_PointManager::get_LadybugsCurrentLevel()
extern "C" int32_t SCR_PointManager_get_LadybugsCurrentLevel_m1470 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ladybugsCurrLevel_8);
		return L_0;
	}
}
// System.Int32 SCR_PointManager::get_PuceronsCurrentLevel()
extern "C" int32_t SCR_PointManager_get_PuceronsCurrentLevel_m1471 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___puceronsCurrLevel_9);
		return L_0;
	}
}
// System.Int32 SCR_PointManager::get_TotalScore()
extern "C" int32_t SCR_PointManager_get_TotalScore_m1472 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___totalScoreCurr_15);
		return L_0;
	}
}
// System.Int32[] SCR_PointManager::get_HighScores()
extern "C" Int32U5BU5D_t242* SCR_PointManager_get_HighScores_m1473 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t242* L_0 = (__this->___highscorePerLevel_13);
		return L_0;
	}
}
// System.Void SCR_PointManager::set_HighScores(System.Int32[])
extern "C" void SCR_PointManager_set_HighScores_m1474 (SCR_PointManager_t381 * __this, Int32U5BU5D_t242* ___value, const MethodInfo* method)
{
	{
		Int32U5BU5D_t242* L_0 = ___value;
		__this->___highscorePerLevel_13 = L_0;
		return;
	}
}
// System.Int32 SCR_PointManager::get_ScoreCumul()
extern "C" int32_t SCR_PointManager_get_ScoreCumul_m1475 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___scoreCumul_14);
		return L_0;
	}
}
// System.Int32[] SCR_PointManager::get_PointsForLevelAccess()
extern "C" Int32U5BU5D_t242* SCR_PointManager_get_PointsForLevelAccess_m1476 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t242* L_0 = (__this->___pointsForLevelAccess_7);
		return L_0;
	}
}
// System.Boolean SCR_PointManager::get_IsScoreCounting()
extern "C" bool SCR_PointManager_get_IsScoreCounting_m1477 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isScoreCounting_16);
		return L_0;
	}
}
// System.Collections.Generic.List`1<SCR_Puceron> SCR_PointManager::get_PuceronsCollected()
extern "C" List_1_t387 * SCR_PointManager_get_PuceronsCollected_m1478 (SCR_PointManager_t381 * __this, const MethodInfo* method)
{
	{
		List_1_t387 * L_0 = (__this->___puceronsCollected_12);
		return L_0;
	}
}
// System.Void SCR_PointManager::set_PuceronsCollected(System.Collections.Generic.List`1<SCR_Puceron>)
extern "C" void SCR_PointManager_set_PuceronsCollected_m1479 (SCR_PointManager_t381 * __this, List_1_t387 * ___value, const MethodInfo* method)
{
	{
		List_1_t387 * L_0 = ___value;
		__this->___puceronsCollected_12 = L_0;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina.h"
// System.IO.FileStream
#include "mscorlib_System_IO_FileStream.h"
// Data
#include "AssemblyU2DCSharp_Data.h"
// System.IO.Stream
#include "mscorlib_System_IO_Stream.h"
// System.IO.FileMode
#include "mscorlib_System_IO_FileMode.h"
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_BinaMethodDeclarations.h"
// System.IO.File
#include "mscorlib_System_IO_FileMethodDeclarations.h"
// Data
#include "AssemblyU2DCSharp_DataMethodDeclarations.h"
// System.IO.Stream
#include "mscorlib_System_IO_StreamMethodDeclarations.h"


// System.Void SCR_SaveData::.ctor()
extern "C" void SCR_SaveData__ctor_m1480 (SCR_SaveData_t382 * __this, const MethodInfo* method)
{
	{
		__this->___saveFile_2 = (String_t*) &_stringLiteral371;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_SaveData::Awake()
extern "C" void SCR_SaveData_Awake_m1481 (SCR_SaveData_t382 * __this, const MethodInfo* method)
{
	{
		SCR_SaveData_t382 * L_0 = SCR_ManagerComponents_get_SaveData_m1433(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		SCR_ManagerComponents_set_SaveData_m1434(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		return;
	}
}
// System.Void SCR_SaveData::Start()
extern "C" void SCR_SaveData_Start_m1482 (SCR_SaveData_t382 * __this, const MethodInfo* method)
{
	{
		SCR_SaveData_t382 * L_0 = SCR_ManagerComponents_get_SaveData_m1433(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m623(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		SCR_SaveData_Load_m1486(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_SaveData::GetDataValues(System.Int32)
extern "C" void SCR_SaveData_GetDataValues_m1483 (SCR_SaveData_t382 * __this, int32_t ___level, const MethodInfo* method)
{
	{
		Int32U5BU5D_t242* L_0 = (__this->___highscores_4);
		int32_t L_1 = ___level;
		SCR_PointManager_t381 * L_2 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Int32U5BU5D_t242* L_3 = SCR_PointManager_get_HighScores_m1473(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___level;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, L_1)) = (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_3, L_5));
		return;
	}
}
// System.Void SCR_SaveData::SetDataValues()
extern "C" void SCR_SaveData_SetDataValues_m1484 (SCR_SaveData_t382 * __this, const MethodInfo* method)
{
	{
		SCR_PointManager_t381 * L_0 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int32U5BU5D_t242* L_1 = (__this->___highscores_4);
		NullCheck(L_0);
		SCR_PointManager_set_HighScores_m1474(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_SaveData::Save(System.Int32)
extern TypeInfo* BinaryFormatter_t424_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Data_t388_il2cpp_TypeInfo_var;
extern "C" void SCR_SaveData_Save_m1485 (SCR_SaveData_t382 * __this, int32_t ___level, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BinaryFormatter_t424_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(704);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Data_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(705);
		s_Il2CppMethodIntialized = true;
	}
	BinaryFormatter_t424 * V_0 = {0};
	FileStream_t425 * V_1 = {0};
	{
		int32_t L_0 = ___level;
		SCR_SaveData_GetDataValues_m1483(__this, L_0, /*hidden argument*/NULL);
		BinaryFormatter_t424 * L_1 = (BinaryFormatter_t424 *)il2cpp_codegen_object_new (BinaryFormatter_t424_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m1805(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Application_get_persistentDataPath_m1806(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = (__this->___saveFile_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m860(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		FileStream_t425 * L_5 = File_Create_m1807(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Data_t388 * L_6 = (__this->___data_3);
		if (L_6)
		{
			goto IL_0039;
		}
	}
	{
		Data_t388 * L_7 = (Data_t388 *)il2cpp_codegen_object_new (Data_t388_il2cpp_TypeInfo_var);
		Data__ctor_m1488(L_7, /*hidden argument*/NULL);
		__this->___data_3 = L_7;
	}

IL_0039:
	{
		Data_t388 * L_8 = (__this->___data_3);
		Int32U5BU5D_t242* L_9 = (__this->___highscores_4);
		NullCheck(L_8);
		L_8->___highscores_0 = L_9;
		BinaryFormatter_t424 * L_10 = V_0;
		FileStream_t425 * L_11 = V_1;
		Data_t388 * L_12 = (__this->___data_3);
		NullCheck(L_10);
		VirtActionInvoker2< Stream_t426 *, Object_t * >::Invoke(8 /* System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Serialize(System.IO.Stream,System.Object) */, L_10, L_11, L_12);
		FileStream_t425 * L_13 = V_1;
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_13);
		return;
	}
}
// System.Void SCR_SaveData::Load()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* BinaryFormatter_t424_il2cpp_TypeInfo_var;
extern TypeInfo* Data_t388_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t242_il2cpp_TypeInfo_var;
extern "C" void SCR_SaveData_Load_m1486 (SCR_SaveData_t382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		BinaryFormatter_t424_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(704);
		Data_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(705);
		Int32U5BU5D_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	BinaryFormatter_t424 * V_0 = {0};
	FileStream_t425 * V_1 = {0};
	int32_t V_2 = 0;
	{
		String_t* L_0 = Application_get_persistentDataPath_m1806(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = (__this->___saveFile_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m860(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		bool L_3 = File_Exists_m1808(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0065;
		}
	}
	{
		BinaryFormatter_t424 * L_4 = (BinaryFormatter_t424 *)il2cpp_codegen_object_new (BinaryFormatter_t424_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m1805(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = Application_get_persistentDataPath_m1806(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = (__this->___saveFile_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m860(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		FileStream_t425 * L_8 = File_Open_m1809(NULL /*static, unused*/, L_7, 3, /*hidden argument*/NULL);
		V_1 = L_8;
		BinaryFormatter_t424 * L_9 = V_0;
		FileStream_t425 * L_10 = V_1;
		NullCheck(L_9);
		Object_t * L_11 = (Object_t *)VirtFuncInvoker1< Object_t *, Stream_t426 * >::Invoke(7 /* System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Deserialize(System.IO.Stream) */, L_9, L_10);
		__this->___data_3 = ((Data_t388 *)Castclass(L_11, Data_t388_il2cpp_TypeInfo_var));
		FileStream_t425 * L_12 = V_1;
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_12);
		Data_t388 * L_13 = (__this->___data_3);
		NullCheck(L_13);
		Int32U5BU5D_t242* L_14 = (L_13->___highscores_0);
		__this->___highscores_4 = L_14;
		goto IL_0094;
	}

IL_0065:
	{
		__this->___highscores_4 = ((Int32U5BU5D_t242*)SZArrayNew(Int32U5BU5D_t242_il2cpp_TypeInfo_var, ((int32_t)30)));
		V_2 = 0;
		goto IL_0086;
	}

IL_0079:
	{
		Int32U5BU5D_t242* L_15 = (__this->___highscores_4);
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_15, L_16)) = (int32_t)0;
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0086:
	{
		int32_t L_18 = V_2;
		Int32U5BU5D_t242* L_19 = (__this->___highscores_4);
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)(((Array_t *)L_19)->max_length))))))
		{
			goto IL_0079;
		}
	}

IL_0094:
	{
		SCR_SaveData_SetDataValues_m1484(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_SaveData::ResetSave()
extern TypeInfo* BinaryFormatter_t424_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Data_t388_il2cpp_TypeInfo_var;
extern "C" void SCR_SaveData_ResetSave_m1487 (SCR_SaveData_t382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BinaryFormatter_t424_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(704);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Data_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(705);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	BinaryFormatter_t424 * V_1 = {0};
	FileStream_t425 * V_2 = {0};
	{
		V_0 = 0;
		goto IL_0014;
	}

IL_0007:
	{
		Int32U5BU5D_t242* L_0 = (__this->___highscores_4);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, L_1)) = (int32_t)0;
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0014:
	{
		int32_t L_3 = V_0;
		Int32U5BU5D_t242* L_4 = (__this->___highscores_4);
		NullCheck(L_4);
		if ((((int32_t)L_3) < ((int32_t)(((int32_t)(((Array_t *)L_4)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		SCR_SaveData_SetDataValues_m1484(__this, /*hidden argument*/NULL);
		BinaryFormatter_t424 * L_5 = (BinaryFormatter_t424 *)il2cpp_codegen_object_new (BinaryFormatter_t424_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m1805(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6 = Application_get_persistentDataPath_m1806(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = (__this->___saveFile_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m860(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		FileStream_t425 * L_9 = File_Create_m1807(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Data_t388 * L_10 = (__this->___data_3);
		if (L_10)
		{
			goto IL_005a;
		}
	}
	{
		Data_t388 * L_11 = (Data_t388 *)il2cpp_codegen_object_new (Data_t388_il2cpp_TypeInfo_var);
		Data__ctor_m1488(L_11, /*hidden argument*/NULL);
		__this->___data_3 = L_11;
	}

IL_005a:
	{
		Data_t388 * L_12 = (__this->___data_3);
		Int32U5BU5D_t242* L_13 = (__this->___highscores_4);
		NullCheck(L_12);
		L_12->___highscores_0 = L_13;
		BinaryFormatter_t424 * L_14 = V_1;
		FileStream_t425 * L_15 = V_2;
		Data_t388 * L_16 = (__this->___data_3);
		NullCheck(L_14);
		VirtActionInvoker2< Stream_t426 *, Object_t * >::Invoke(8 /* System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Serialize(System.IO.Stream,System.Object) */, L_14, L_15, L_16);
		FileStream_t425 * L_17 = V_2;
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_17);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void Data::.ctor()
extern "C" void Data__ctor_m1488 (Data_t388 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_7.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_0.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_7MethodDeclarations.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_0MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollecMethodDeclarations.h"
struct Component_t219;
struct AudioSourceU5BU5D_t427;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.AudioSource>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.AudioSource>()
#define Component_GetComponentsInChildren_TisAudioSource_t247_m1810(__this, method) (( AudioSourceU5BU5D_t427* (*) (Component_t219 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m698_gshared)(__this, method)


// System.Void SCR_SoundManager::.ctor()
extern TypeInfo* Dictionary_2_t389_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1811_MethodInfo_var;
extern "C" void SCR_SoundManager__ctor_m1489 (SCR_SoundManager_t335 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(706);
		Dictionary_2__ctor_m1811_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483737);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___soundOn_2 = 1;
		Dictionary_2_t389 * L_0 = (Dictionary_2_t389 *)il2cpp_codegen_object_new (Dictionary_2_t389_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1811(L_0, /*hidden argument*/Dictionary_2__ctor_m1811_MethodInfo_var);
		__this->___sources_3 = L_0;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_SoundManager::Awake()
extern "C" void SCR_SoundManager_Awake_m1490 (SCR_SoundManager_t335 * __this, const MethodInfo* method)
{
	{
		SCR_SoundManager_t335 * L_0 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		SCR_ManagerComponents_set_SoundManager_m1440(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		SCR_SoundManager_FillSoundDictionary_m1493(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_SoundManager::Start()
extern "C" void SCR_SoundManager_Start_m1491 (SCR_SoundManager_t335 * __this, const MethodInfo* method)
{
	{
		SCR_SoundManager_LevelInit_m1494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_SoundManager::OnLevelWasLoaded()
extern "C" void SCR_SoundManager_OnLevelWasLoaded_m1492 (SCR_SoundManager_t335 * __this, const MethodInfo* method)
{
	{
		SCR_SoundManager_LevelInit_m1494(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_SoundManager::FillSoundDictionary()
extern const MethodInfo* Component_GetComponentsInChildren_TisAudioSource_t247_m1810_MethodInfo_var;
extern "C" void SCR_SoundManager_FillSoundDictionary_m1493 (SCR_SoundManager_t335 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentsInChildren_TisAudioSource_t247_m1810_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483738);
		s_Il2CppMethodIntialized = true;
	}
	AudioSourceU5BU5D_t427* V_0 = {0};
	AudioSource_t247 * V_1 = {0};
	AudioSourceU5BU5D_t427* V_2 = {0};
	int32_t V_3 = 0;
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioSourceU5BU5D_t427* L_1 = Component_GetComponentsInChildren_TisAudioSource_t247_m1810(L_0, /*hidden argument*/Component_GetComponentsInChildren_TisAudioSource_t247_m1810_MethodInfo_var);
		V_0 = L_1;
		AudioSourceU5BU5D_t427* L_2 = V_0;
		V_2 = L_2;
		V_3 = 0;
		goto IL_002f;
	}

IL_0015:
	{
		AudioSourceU5BU5D_t427* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = (*(AudioSource_t247 **)(AudioSource_t247 **)SZArrayLdElema(L_3, L_5));
		Dictionary_2_t389 * L_6 = (__this->___sources_3);
		AudioSource_t247 * L_7 = V_1;
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m798(L_7, /*hidden argument*/NULL);
		AudioSource_t247 * L_9 = V_1;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, AudioSource_t247 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::Add(!0,!1) */, L_6, L_8, L_9);
		int32_t L_10 = V_3;
		V_3 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_11 = V_3;
		AudioSourceU5BU5D_t427* L_12 = V_2;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_12)->max_length))))))
		{
			goto IL_0015;
		}
	}
	{
		return;
	}
}
// System.Void SCR_SoundManager::LevelInit()
extern "C" void SCR_SoundManager_LevelInit_m1494 (SCR_SoundManager_t335 * __this, const MethodInfo* method)
{
	{
		SCR_SoundManager_StopAllSounds_m1497(__this, /*hidden argument*/NULL);
		SCR_LevelManager_t379 * L_0 = SCR_ManagerComponents_get_LevelManager_m1431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = SCR_LevelManager_get_CurrentLevel_m1421(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		SCR_SoundManager_PlaySound_m1495(__this, (String_t*) &_stringLiteral372, /*hidden argument*/NULL);
		goto IL_0030;
	}

IL_0025:
	{
		SCR_SoundManager_PlaySound_m1495(__this, (String_t*) &_stringLiteral296, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void SCR_SoundManager::PlaySound(System.String)
extern "C" void SCR_SoundManager_PlaySound_m1495 (SCR_SoundManager_t335 * __this, String_t* ___name, const MethodInfo* method)
{
	AudioSource_t247 * V_0 = {0};
	{
		bool L_0 = (__this->___soundOn_2);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Dictionary_2_t389 * L_1 = (__this->___sources_3);
		String_t* L_2 = ___name;
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker2< bool, String_t*, AudioSource_t247 ** >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::TryGetValue(!0,!1&) */, L_1, L_2, (&V_0));
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		AudioSource_t247 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = AudioSource_get_isPlaying_m1812(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0030;
		}
	}
	{
		AudioSource_t247 * L_6 = V_0;
		NullCheck(L_6);
		AudioSource_Play_m938(L_6, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void SCR_SoundManager::StopSound(System.String)
extern "C" void SCR_SoundManager_StopSound_m1496 (SCR_SoundManager_t335 * __this, String_t* ___name, const MethodInfo* method)
{
	AudioSource_t247 * V_0 = {0};
	{
		Dictionary_2_t389 * L_0 = (__this->___sources_3);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker2< bool, String_t*, AudioSource_t247 ** >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>::TryGetValue(!0,!1&) */, L_0, L_1, (&V_0));
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		AudioSource_t247 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = AudioSource_get_isPlaying_m1812(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		AudioSource_t247 * L_5 = V_0;
		NullCheck(L_5);
		AudioSource_Stop_m1813(L_5, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void SCR_SoundManager::StopAllSounds()
extern TypeInfo* Enumerator_t428_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t233_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m1814_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m1815_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1816_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1817_MethodInfo_var;
extern "C" void SCR_SoundManager_StopAllSounds_m1497 (SCR_SoundManager_t335 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t428_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(707);
		IDisposable_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		Dictionary_2_get_Values_m1814_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483739);
		ValueCollection_GetEnumerator_m1815_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483740);
		Enumerator_get_Current_m1816_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483741);
		Enumerator_MoveNext_m1817_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483742);
		s_Il2CppMethodIntialized = true;
	}
	AudioSource_t247 * V_0 = {0};
	Enumerator_t428  V_1 = {0};
	Exception_t232 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t232 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t389 * L_0 = (__this->___sources_3);
		NullCheck(L_0);
		ValueCollection_t429 * L_1 = Dictionary_2_get_Values_m1814(L_0, /*hidden argument*/Dictionary_2_get_Values_m1814_MethodInfo_var);
		NullCheck(L_1);
		Enumerator_t428  L_2 = ValueCollection_GetEnumerator_m1815(L_1, /*hidden argument*/ValueCollection_GetEnumerator_m1815_MethodInfo_var);
		V_1 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002f;
		}

IL_0016:
		{
			AudioSource_t247 * L_3 = Enumerator_get_Current_m1816((&V_1), /*hidden argument*/Enumerator_get_Current_m1816_MethodInfo_var);
			V_0 = L_3;
			AudioSource_t247 * L_4 = V_0;
			NullCheck(L_4);
			bool L_5 = AudioSource_get_isPlaying_m1812(L_4, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_002f;
			}
		}

IL_0029:
		{
			AudioSource_t247 * L_6 = V_0;
			NullCheck(L_6);
			AudioSource_Stop_m1813(L_6, /*hidden argument*/NULL);
		}

IL_002f:
		{
			bool L_7 = Enumerator_MoveNext_m1817((&V_1), /*hidden argument*/Enumerator_MoveNext_m1817_MethodInfo_var);
			if (L_7)
			{
				goto IL_0016;
			}
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x4C, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t232 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Enumerator_t428  L_8 = V_1;
		Enumerator_t428  L_9 = L_8;
		Object_t * L_10 = Box(Enumerator_t428_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t233_il2cpp_TypeInfo_var, L_10);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t232 *)
	}

IL_004c:
	{
		return;
	}
}
// System.Boolean SCR_SoundManager::get_SoundOn()
extern "C" bool SCR_SoundManager_get_SoundOn_m1498 (SCR_SoundManager_t335 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___soundOn_2);
		return L_0;
	}
}
// System.Void SCR_SoundManager::set_SoundOn(System.Boolean)
extern "C" void SCR_SoundManager_set_SoundOn_m1499 (SCR_SoundManager_t335 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___soundOn_2 = L_0;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void SCR_TimeManager::.ctor()
extern "C" void SCR_TimeManager__ctor_m1500 (SCR_TimeManager_t383 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_TimeManager::Awake()
extern "C" void SCR_TimeManager_Awake_m1501 (SCR_TimeManager_t383 * __this, const MethodInfo* method)
{
	{
		SCR_TimeManager_t383 * L_0 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		SCR_ManagerComponents_set_TimeManager_m1436(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		return;
	}
}
// System.Void SCR_TimeManager::Start()
extern "C" void SCR_TimeManager_Start_m1502 (SCR_TimeManager_t383 * __this, const MethodInfo* method)
{
	{
		SCR_GUIManager_t378 * L_0 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_GUIManager_RelayTextInfo_m1411(L_0, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_TimeManager::OnLevelWasLoaded()
extern "C" void SCR_TimeManager_OnLevelWasLoaded_m1503 (SCR_TimeManager_t383 * __this, const MethodInfo* method)
{
	{
		__this->___isPaused_2 = 0;
		__this->___isCounting_3 = 0;
		__this->___timeSinceLevelStart_4 = (0.0f);
		SCR_GUIManager_t378 * L_0 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_GUIManager_RelayTextInfo_m1411(L_0, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_TimeManager::Update()
extern "C" void SCR_TimeManager_Update_m1504 (SCR_TimeManager_t383 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isCounting_3);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		bool L_1 = (__this->___isPaused_2);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		float L_2 = (__this->___timeSinceLevelStart_4);
		float L_3 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___timeSinceLevelStart_4 = ((float)((float)L_2+(float)L_3));
		SCR_GUIManager_t378 * L_4 = SCR_ManagerComponents_get_GUIManager_m1437(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		SCR_GUIManager_RelayTextInfo_m1411(L_4, 2, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void SCR_TimeManager::SetTimer(System.Boolean)
extern "C" void SCR_TimeManager_SetTimer_m1505 (SCR_TimeManager_t383 * __this, bool ___turnOn, const MethodInfo* method)
{
	{
		bool L_0 = ___turnOn;
		__this->___isCounting_3 = L_0;
		return;
	}
}
// System.Void SCR_TimeManager::Pause()
extern "C" void SCR_TimeManager_Pause_m1506 (SCR_TimeManager_t383 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isPaused_2);
		__this->___isPaused_2 = ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = (__this->___isPaused_2);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		Time_set_timeScale_m1716(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		goto IL_0033;
	}

IL_0029:
	{
		Time_set_timeScale_m1716(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Boolean SCR_TimeManager::get_IsPaused()
extern "C" bool SCR_TimeManager_get_IsPaused_m1507 (SCR_TimeManager_t383 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isPaused_2);
		return L_0;
	}
}
// System.Boolean SCR_TimeManager::get_IsCounting()
extern "C" bool SCR_TimeManager_get_IsCounting_m1508 (SCR_TimeManager_t383 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isCounting_3);
		return L_0;
	}
}
// System.Single SCR_TimeManager::get_TimeSinceLevelStart()
extern "C" float SCR_TimeManager_get_TimeSinceLevelStart_m1509 (SCR_TimeManager_t383 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___timeSinceLevelStart_4);
		return L_0;
	}
}
// SCR_Bird/<Landing>c__IteratorB
#include "AssemblyU2DCSharp_SCR_Bird_U3CLandingU3Ec__IteratorB.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Bird/<Landing>c__IteratorB
#include "AssemblyU2DCSharp_SCR_Bird_U3CLandingU3Ec__IteratorBMethodDeclarations.h"

// SCR_Bird
#include "AssemblyU2DCSharp_SCR_Bird.h"
// SCR_Bird
#include "AssemblyU2DCSharp_SCR_BirdMethodDeclarations.h"


// System.Void SCR_Bird/<Landing>c__IteratorB::.ctor()
extern "C" void U3CLandingU3Ec__IteratorB__ctor_m1510 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_Bird/<Landing>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CLandingU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1511 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object SCR_Bird/<Landing>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CLandingU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1512 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean SCR_Bird/<Landing>c__IteratorB::MoveNext()
extern "C" bool U3CLandingU3Ec__IteratorB_MoveNext_m1513 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003f;
		}
	}
	{
		goto IL_0046;
	}

IL_0021:
	{
		SCR_Bird_t390 * L_2 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_2);
		SCR_Bird_AttackStart_m1522(L_2, /*hidden argument*/NULL);
		__this->___U24current_1 = NULL;
		__this->___U24PC_0 = 1;
		goto IL_0048;
	}

IL_003f:
	{
		__this->___U24PC_0 = (-1);
	}

IL_0046:
	{
		return 0;
	}

IL_0048:
	{
		return 1;
	}
	// Dead block : IL_004a: ldloc.1
}
// System.Void SCR_Bird/<Landing>c__IteratorB::Dispose()
extern "C" void U3CLandingU3Ec__IteratorB_Dispose_m1514 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void SCR_Bird/<Landing>c__IteratorB::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CLandingU3Ec__IteratorB_Reset_m1515 (U3CLandingU3Ec__IteratorB_t391 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// SCR_Hurt
#include "AssemblyU2DCSharp_SCR_Hurt.h"
// Scr_ShadowTarget
#include "AssemblyU2DCSharp_Scr_ShadowTarget.h"
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
struct Component_t219;
struct SCR_Hurt_t393;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<SCR_Hurt>()
// !!0 UnityEngine.Component::GetComponentInChildren<SCR_Hurt>()
#define Component_GetComponentInChildren_TisSCR_Hurt_t393_m1818(__this, method) (( SCR_Hurt_t393 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m675_gshared)(__this, method)


// System.Void SCR_Bird::.ctor()
extern "C" void SCR_Bird__ctor_m1516 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___targetPosition_6 = L_0;
		Vector3_t4  L_1 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___startPosition_7 = L_1;
		Vector3_t4  L_2 = {0};
		Vector3__ctor_m612(&L_2, (0.0f), (0.0f), (1.3f), /*hidden argument*/NULL);
		__this->___camieOffset_9 = L_2;
		__this->___maxAttacks_13 = 3;
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Bird::Start()
extern const MethodInfo* Component_GetComponentInChildren_TisSCR_Hurt_t393_m1818_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var;
extern "C" void SCR_Bird_Start_m1517 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentInChildren_TisSCR_Hurt_t393_m1818_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483743);
		Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Hurt_t393 * L_1 = Component_GetComponentInChildren_TisSCR_Hurt_t393_m1818(L_0, /*hidden argument*/Component_GetComponentInChildren_TisSCR_Hurt_t393_m1818_MethodInfo_var);
		__this->___beak_4 = L_1;
		Animator_t9 * L_2 = Component_GetComponent_TisAnimator_t9_m616(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var);
		__this->___anim_5 = L_2;
		SCR_ManagerComponents_t380 * L_3 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_Camie_t338 * L_4 = SCR_ManagerComponents_get_AvatarMove_m1441(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1 * L_5 = Component_get_transform_m594(L_4, /*hidden argument*/NULL);
		__this->___camieTransform_8 = L_5;
		SCR_Bird_Pool_m1529(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Bird::Update()
extern "C" void SCR_Bird_Update_m1518 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		SCR_Bird_Fly_m1520(__this, /*hidden argument*/NULL);
		bool L_0 = (__this->___attackIsInvoked_10);
		if (L_0)
		{
			goto IL_0038;
		}
	}
	{
		SCR_TimeManager_t383 * L_1 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = SCR_TimeManager_get_IsCounting_m1508(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		float L_3 = (__this->___attaqueApresXSecondes_14);
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral373, L_3, /*hidden argument*/NULL);
		__this->___attackIsInvoked_10 = 1;
	}

IL_0038:
	{
		return;
	}
}
// System.Void SCR_Bird::SetToIdlePosition()
extern "C" void SCR_Bird_SetToIdlePosition_m1519 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = (__this->___camieTransform_8);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		SCR_ManagerComponents_t380 * L_2 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t1 * L_3 = SCR_ManagerComponents_get_LeftFrame_m1449(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		float L_5 = Vector3_Distance_m1750(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Transform_t1 * L_6 = (__this->___camieTransform_8);
		NullCheck(L_6);
		Vector3_t4  L_7 = Transform_get_position_m593(L_6, /*hidden argument*/NULL);
		SCR_ManagerComponents_t380 * L_8 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t1 * L_9 = SCR_ManagerComponents_get_RightFrame_m1451(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t4  L_10 = Transform_get_position_m593(L_9, /*hidden argument*/NULL);
		float L_11 = Vector3_Distance_m1750(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)L_11))))
		{
			goto IL_0043;
		}
	}

IL_0043:
	{
		Transform_t1 * L_12 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_13 = (__this->___camieTransform_8);
		NullCheck(L_13);
		Vector3_t4  L_14 = Transform_get_position_m593(L_13, /*hidden argument*/NULL);
		Vector3_t4  L_15 = (__this->___camieOffset_9);
		Vector3_t4  L_16 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_position_m607(L_12, L_16, /*hidden argument*/NULL);
		SCR_SoundManager_t335 * L_17 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		SCR_SoundManager_PlaySound_m1495(L_17, (String_t*) &_stringLiteral374, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1641(__this, (String_t*) &_stringLiteral375, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Bird::Fly()
extern "C" void SCR_Bird_Fly_m1520 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		Scr_ShadowTarget_t392 * L_0 = (__this->___shadow_2);
		NullCheck(L_0);
		bool L_1 = (L_0->___canMove_3);
		if (!L_1)
		{
			goto IL_0048;
		}
	}
	{
		Transform_t1 * L_2 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_3 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_position_m593(L_3, /*hidden argument*/NULL);
		Vector3_t4  L_5 = (__this->___targetPosition_6);
		Vector3_t4  L_6 = (__this->___camieOffset_9);
		Vector3_t4  L_7 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		float L_8 = (__this->___flySpeed_15);
		float L_9 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_10 = Vector3_Lerp_m652(NULL /*static, unused*/, L_4, L_7, ((float)((float)L_8*(float)L_9)), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m607(L_2, L_10, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Collections.IEnumerator SCR_Bird::Landing()
extern TypeInfo* U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_Bird_Landing_m1521 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(709);
		s_Il2CppMethodIntialized = true;
	}
	U3CLandingU3Ec__IteratorB_t391 * V_0 = {0};
	{
		U3CLandingU3Ec__IteratorB_t391 * L_0 = (U3CLandingU3Ec__IteratorB_t391 *)il2cpp_codegen_object_new (U3CLandingU3Ec__IteratorB_t391_il2cpp_TypeInfo_var);
		U3CLandingU3Ec__IteratorB__ctor_m1510(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLandingU3Ec__IteratorB_t391 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CLandingU3Ec__IteratorB_t391 * L_2 = V_0;
		return L_2;
	}
}
// System.Void SCR_Bird::AttackStart()
extern "C" void SCR_Bird_AttackStart_m1522 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		SCR_Bird_GetTargetPoint_m1524(__this, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral376, (3.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Bird::Attack()
extern "C" void SCR_Bird_Attack_m1523 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		__this->___attackStarted_11 = 1;
		Animator_t9 * L_0 = (__this->___anim_5);
		NullCheck(L_0);
		Animator_SetBool_m624(L_0, (String_t*) &_stringLiteral377, 1, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___attacknb_12);
		int32_t L_2 = (__this->___maxAttacks_13);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0034;
		}
	}
	{
		SCR_Bird_EndAttack_m1528(__this, /*hidden argument*/NULL);
		goto IL_003a;
	}

IL_0034:
	{
		SCR_Bird_AttackStart_m1522(__this, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Void SCR_Bird::GetTargetPoint()
extern "C" void SCR_Bird_GetTargetPoint_m1524 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Camie_t338 * L_1 = SCR_ManagerComponents_get_AvatarMove_m1441(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1 * L_2 = Component_get_transform_m594(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4  L_3 = Transform_get_position_m593(L_2, /*hidden argument*/NULL);
		__this->___targetPosition_6 = L_3;
		return;
	}
}
// System.Void SCR_Bird::GoIdle()
extern "C" void SCR_Bird_GoIdle_m1525 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___attackStarted_11);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Animator_t9 * L_1 = (__this->___anim_5);
		NullCheck(L_1);
		Animator_SetTrigger_m1764(L_1, (String_t*) &_stringLiteral378, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void SCR_Bird::CanMove()
extern "C" void SCR_Bird_CanMove_m1526 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___attackStarted_11);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		Scr_ShadowTarget_t392 * L_1 = (__this->___shadow_2);
		Scr_ShadowTarget_t392 * L_2 = (__this->___shadow_2);
		NullCheck(L_2);
		bool L_3 = (L_2->___canMove_3);
		NullCheck(L_1);
		L_1->___canMove_3 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
	}

IL_0024:
	{
		return;
	}
}
// System.Void SCR_Bird::BlobShadow()
extern "C" void SCR_Bird_BlobShadow_m1527 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___attackStarted_11);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		Scr_ShadowTarget_t392 * L_1 = (__this->___shadow_2);
		Scr_ShadowTarget_t392 * L_2 = (__this->___shadow_2);
		NullCheck(L_2);
		bool L_3 = (L_2->___canGrow_4);
		NullCheck(L_1);
		L_1->___canGrow_4 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
	}

IL_0024:
	{
		return;
	}
}
// System.Void SCR_Bird::EndAttack()
extern "C" void SCR_Bird_EndAttack_m1528 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		SCR_Hurt_t393 * L_0 = (__this->___beak_4);
		NullCheck(L_0);
		Behaviour_set_enabled_m766(L_0, 0, /*hidden argument*/NULL);
		Animator_t9 * L_1 = (__this->___anim_5);
		NullCheck(L_1);
		AnimatorClipInfoU5BU5D_t430* L_2 = Animator_GetCurrentAnimatorClipInfo_m1819(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral379, (((float)(((int32_t)(((Array_t *)L_2)->max_length))))), /*hidden argument*/NULL);
		GameObject_t78 * L_3 = (__this->___bird_3);
		NullCheck(L_3);
		GameObject_SetActive_m713(L_3, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Bird::Pool()
extern "C" void SCR_Bird_Pool_m1529 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4  L_1 = {0};
		Vector3__ctor_m612(&L_1, (-100.0f), (-100.0f), (-100.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m607(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Bird::AddAttack()
extern "C" void SCR_Bird_AddAttack_m1530 (SCR_Bird_t390 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___attacknb_12);
		__this->___attacknb_12 = ((int32_t)((int32_t)L_0+(int32_t)1));
		return;
	}
}
// SCR_FallOutOfScreen
#include "AssemblyU2DCSharp_SCR_FallOutOfScreen.h"
#ifndef _MSC_VER
#else
#endif
// SCR_FallOutOfScreen
#include "AssemblyU2DCSharp_SCR_FallOutOfScreenMethodDeclarations.h"



// System.Void SCR_FallOutOfScreen::.ctor()
extern "C" void SCR_FallOutOfScreen__ctor_m1531 (SCR_FallOutOfScreen_t394 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_FallOutOfScreen::OnBecameVisible()
extern const MethodInfo* Component_GetComponent_TisRenderer_t154_m1767_MethodInfo_var;
extern "C" void SCR_FallOutOfScreen_OnBecameVisible_m1532 (SCR_FallOutOfScreen_t394 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t154_m1767_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483714);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t154 * L_0 = Component_GetComponent_TisRenderer_t154_m1767(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t154_m1767_MethodInfo_var);
		NullCheck(L_0);
		Renderer_set_enabled_m927(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_FallOutOfScreen::OnTriggerEnter(UnityEngine.Collider)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void SCR_FallOutOfScreen_OnTriggerEnter_m1533 (SCR_FallOutOfScreen_t394 * __this, Collider_t138 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t138 * L_0 = ___other;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m635(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m636(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral380, (0.5f), /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void SCR_FallOutOfScreen::CallAvatarRespawn()
extern "C" void SCR_FallOutOfScreen_CallAvatarRespawn_m1534 (SCR_FallOutOfScreen_t394 * __this, const MethodInfo* method)
{
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Camie_t338 * L_1 = SCR_ManagerComponents_get_AvatarMove_m1441(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		SCR_Camie_Respawn_m1253(L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// SCR_Goutte
#include "AssemblyU2DCSharp_SCR_GoutteMethodDeclarations.h"



// System.Void SCR_Goutte::.ctor()
extern "C" void SCR_Goutte__ctor_m1535 (SCR_Goutte_t347 * __this, const MethodInfo* method)
{
	{
		SCR_SlowAvatar__ctor_m1221(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Goutte::TriggerEnterEffect()
extern "C" void SCR_Goutte_TriggerEnterEffect_m1536 (SCR_Goutte_t347 * __this, const MethodInfo* method)
{
	{
		SCR_SoundManager_t335 * L_0 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_SoundManager_PlaySound_m1495(L_0, (String_t*) &_stringLiteral381, /*hidden argument*/NULL);
		SCR_ManagerComponents_t380 * L_1 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		SCR_Camie_t338 * L_2 = SCR_ManagerComponents_get_AvatarMove_m1441(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_Camie_InBubble_m1257(L_2, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Goutte::TriggerExitEffet()
extern "C" void SCR_Goutte_TriggerExitEffet_m1537 (SCR_Goutte_t347 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// SCR_Hurt
#include "AssemblyU2DCSharp_SCR_HurtMethodDeclarations.h"



// System.Void SCR_Hurt::.ctor()
extern "C" void SCR_Hurt__ctor_m1538 (SCR_Hurt_t393 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Hurt::OnTriggerEnter(UnityEngine.Collider)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void SCR_Hurt_OnTriggerEnter_m1539 (SCR_Hurt_t393 * __this, Collider_t138 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t138 * L_0 = ___other;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m635(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m636(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		SCR_TimeManager_t383 * L_3 = SCR_ManagerComponents_get_TimeManager_m1435(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = SCR_TimeManager_get_IsCounting_m1508(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		SCR_Hurt_AvatarWasHurt_m1540(__this, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void SCR_Hurt::AvatarWasHurt()
extern "C" void SCR_Hurt_AvatarWasHurt_m1540 (SCR_Hurt_t393 * __this, const MethodInfo* method)
{
	{
		SCR_PointManager_t381 * L_0 = SCR_ManagerComponents_get_PointManager_m1429(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_PointManager_LosePucerons_m1467(L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// SCR_Waypoint
#include "AssemblyU2DCSharp_SCR_WaypointMethodDeclarations.h"



// System.Void SCR_Waypoint::.ctor()
extern "C" void SCR_Waypoint__ctor_m1541 (SCR_Waypoint_t358 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Scr_ShadowTarget
#include "AssemblyU2DCSharp_Scr_ShadowTargetMethodDeclarations.h"

// UnityEngine.Projector
#include "UnityEngine_UnityEngine_Projector.h"
// UnityEngine.Projector
#include "UnityEngine_UnityEngine_ProjectorMethodDeclarations.h"


// System.Void Scr_ShadowTarget::.ctor()
extern "C" void Scr_ShadowTarget__ctor_m1542 (Scr_ShadowTarget_t392 * __this, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = {0};
		Vector3__ctor_m612(&L_0, (0.0f), (0.0f), (-4.0f), /*hidden argument*/NULL);
		__this->___camieOffset_2 = L_0;
		__this->___growSpeed_8 = (0.5f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Scr_ShadowTarget::Start()
extern "C" void Scr_ShadowTarget_Start_m1543 (Scr_ShadowTarget_t392 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_SetParent_m1820(L_0, (Transform_t1 *)NULL, /*hidden argument*/NULL);
		Projector_t395 * L_1 = (__this->___projector_7);
		NullCheck(L_1);
		Projector_set_fieldOfView_m1821(L_1, (0.0f), /*hidden argument*/NULL);
		SCR_ManagerComponents_t380 * L_2 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_Camie_t338 * L_3 = SCR_ManagerComponents_get_AvatarMove_m1441(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1 * L_4 = Component_get_transform_m594(L_3, /*hidden argument*/NULL);
		__this->___camieTransform_5 = L_4;
		return;
	}
}
// System.Void Scr_ShadowTarget::Update()
extern "C" void Scr_ShadowTarget_Update_m1544 (Scr_ShadowTarget_t392 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___canMove_3);
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		Transform_t1 * L_1 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Transform_t1 * L_2 = (__this->___camieTransform_5);
		NullCheck(L_2);
		Vector3_t4  L_3 = Transform_get_position_m593(L_2, /*hidden argument*/NULL);
		Vector3_t4  L_4 = (__this->___camieOffset_2);
		Vector3_t4  L_5 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m607(L_1, L_5, /*hidden argument*/NULL);
	}

IL_002c:
	{
		bool L_6 = (__this->___canGrow_4);
		if (!L_6)
		{
			goto IL_007c;
		}
	}
	{
		SCR_Bird_t390 * L_7 = (__this->___attackCount_6);
		NullCheck(L_7);
		int32_t L_8 = (L_7->___attacknb_12);
		if ((((int32_t)L_8) == ((int32_t)3)))
		{
			goto IL_007c;
		}
	}
	{
		float L_9 = (__this->___growSpeed_8);
		float L_10 = (__this->___maxSize_9);
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0077;
		}
	}
	{
		Projector_t395 * L_11 = (__this->___projector_7);
		Projector_t395 * L_12 = L_11;
		NullCheck(L_12);
		float L_13 = Projector_get_fieldOfView_m1822(L_12, /*hidden argument*/NULL);
		float L_14 = (__this->___growSpeed_8);
		float L_15 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		Projector_set_fieldOfView_m1821(L_12, ((float)((float)L_13+(float)((float)((float)L_14*(float)L_15)))), /*hidden argument*/NULL);
	}

IL_0077:
	{
		goto IL_008c;
	}

IL_007c:
	{
		Projector_t395 * L_16 = (__this->___projector_7);
		NullCheck(L_16);
		Projector_set_fieldOfView_m1821(L_16, (0.0f), /*hidden argument*/NULL);
	}

IL_008c:
	{
		return;
	}
}
// SCR_Spider/<ChaseAvatar>c__IteratorC
#include "AssemblyU2DCSharp_SCR_Spider_U3CChaseAvatarU3Ec__IteratorC.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Spider/<ChaseAvatar>c__IteratorC
#include "AssemblyU2DCSharp_SCR_Spider_U3CChaseAvatarU3Ec__IteratorCMethodDeclarations.h"

// SCR_Spider
#include "AssemblyU2DCSharp_SCR_SpiderMethodDeclarations.h"


// System.Void SCR_Spider/<ChaseAvatar>c__IteratorC::.ctor()
extern "C" void U3CChaseAvatarU3Ec__IteratorC__ctor_m1545 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_Spider/<ChaseAvatar>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CChaseAvatarU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1546 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object SCR_Spider/<ChaseAvatar>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CChaseAvatarU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1547 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean SCR_Spider/<ChaseAvatar>c__IteratorC::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CChaseAvatarU3Ec__IteratorC_MoveNext_m1548 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4  V_1 = {0};
	Vector3_t4  V_2 = {0};
	Vector3_t4  V_3 = {0};
	Vector3_t4  V_4 = {0};
	Vector3_t4  V_5 = {0};
	bool V_6 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_017c;
		}
	}
	{
		goto IL_0193;
	}

IL_0021:
	{
		__this->___U3CspeedU3E__0_0 = (4.0f);
		goto IL_017c;
	}

IL_0031:
	{
		SCR_Spider_t384 * L_2 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_2);
		Transform_t1 * L_3 = Component_get_transform_m594(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1 * L_4 = Transform_get_parent_m676(L_3, /*hidden argument*/NULL);
		SCR_Spider_t384 * L_5 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_5);
		Transform_t1 * L_6 = Component_get_transform_m594(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1 * L_7 = Transform_get_parent_m676(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4  L_8 = Transform_get_position_m593(L_7, /*hidden argument*/NULL);
		SCR_Spider_t384 * L_9 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_9);
		Transform_t1 * L_10 = Component_get_transform_m594(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t1 * L_11 = Transform_get_parent_m676(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t4  L_12 = Transform_get_position_m593(L_11, /*hidden argument*/NULL);
		Vector3_t4  L_13 = Vector3_get_down_m1757(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_14 = Vector3_op_Addition_m604(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		float L_15 = (__this->___U3CspeedU3E__0_0);
		float L_16 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_17 = Vector3_Lerp_m652(NULL /*static, unused*/, L_8, L_14, ((float)((float)L_15*(float)L_16)), /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_m607(L_4, L_17, /*hidden argument*/NULL);
		SCR_Spider_t384 * L_18 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_18);
		Transform_t1 * L_19 = Component_get_transform_m594(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t4  L_20 = Transform_get_position_m593(L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		float L_21 = ((&V_1)->___y_2);
		SCR_ManagerComponents_t380 * L_22 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		SCR_Camie_t338 * L_23 = SCR_ManagerComponents_get_AvatarMove_m1441(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t1 * L_24 = Component_get_transform_m594(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t4  L_25 = Transform_get_position_m593(L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		float L_26 = ((&V_2)->___y_2);
		if ((!(((float)L_21) > ((float)L_26))))
		{
			goto IL_015a;
		}
	}
	{
		SCR_ManagerComponents_t380 * L_27 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_27);
		SCR_Camie_t338 * L_28 = SCR_ManagerComponents_get_AvatarMove_m1441(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_t1 * L_29 = Component_get_transform_m594(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t4  L_30 = Transform_get_position_m593(L_29, /*hidden argument*/NULL);
		V_3 = L_30;
		float L_31 = ((&V_3)->___x_1);
		SCR_Spider_t384 * L_32 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_32);
		Transform_t1 * L_33 = Component_get_transform_m594(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t4  L_34 = Transform_get_position_m593(L_33, /*hidden argument*/NULL);
		V_4 = L_34;
		float L_35 = ((&V_4)->___y_2);
		SCR_Spider_t384 * L_36 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_36);
		Transform_t1 * L_37 = Component_get_transform_m594(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_t4  L_38 = Transform_get_position_m593(L_37, /*hidden argument*/NULL);
		V_5 = L_38;
		float L_39 = ((&V_5)->___z_3);
		Vector3_t4  L_40 = {0};
		Vector3__ctor_m612(&L_40, L_31, L_35, L_39, /*hidden argument*/NULL);
		__this->___U3CtargetXU3E__1_1 = L_40;
		SCR_Spider_t384 * L_41 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_41);
		Transform_t1 * L_42 = Component_get_transform_m594(L_41, /*hidden argument*/NULL);
		SCR_Spider_t384 * L_43 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_43);
		Transform_t1 * L_44 = Component_get_transform_m594(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_t4  L_45 = Transform_get_position_m593(L_44, /*hidden argument*/NULL);
		Vector3_t4  L_46 = (__this->___U3CtargetXU3E__1_1);
		float L_47 = (__this->___U3CspeedU3E__0_0);
		float L_48 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_49 = Vector3_Slerp_m655(NULL /*static, unused*/, L_45, L_46, ((float)((float)((float)((float)L_47/(float)(8.0f)))*(float)L_48)), /*hidden argument*/NULL);
		NullCheck(L_42);
		Transform_set_position_m607(L_42, L_49, /*hidden argument*/NULL);
	}

IL_015a:
	{
		SCR_Spider_t384 * L_50 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_50);
		SCR_Spider_DrawWebString_m1558(L_50, /*hidden argument*/NULL);
		WaitForEndOfFrame_t255 * L_51 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_51, /*hidden argument*/NULL);
		__this->___U24current_3 = L_51;
		__this->___U24PC_2 = 1;
		goto IL_0195;
	}

IL_017c:
	{
		SCR_Spider_t384 * L_52 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_52);
		bool L_53 = (L_52->___isActive_4);
		if (L_53)
		{
			goto IL_0031;
		}
	}
	{
		__this->___U24PC_2 = (-1);
	}

IL_0193:
	{
		return 0;
	}

IL_0195:
	{
		return 1;
	}
	// Dead block : IL_0197: ldloc.s V_6
}
// System.Void SCR_Spider/<ChaseAvatar>c__IteratorC::Dispose()
extern "C" void U3CChaseAvatarU3Ec__IteratorC_Dispose_m1549 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void SCR_Spider/<ChaseAvatar>c__IteratorC::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CChaseAvatarU3Ec__IteratorC_Reset_m1550 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.LineRenderer
#include "UnityEngine_UnityEngine_LineRenderer.h"
// UnityEngine.LineRenderer
#include "UnityEngine_UnityEngine_LineRendererMethodDeclarations.h"
struct Component_t219;
struct ParticleSystem_t161;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.ParticleSystem>()
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.ParticleSystem>()
#define Component_GetComponentInChildren_TisParticleSystem_t161_m1823(__this, method) (( ParticleSystem_t161 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m675_gshared)(__this, method)
struct Component_t219;
struct LineRenderer_t397;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.LineRenderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.LineRenderer>()
#define Component_GetComponent_TisLineRenderer_t397_m1824(__this, method) (( LineRenderer_t397 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void SCR_Spider::.ctor()
extern "C" void SCR_Spider__ctor_m1551 (SCR_Spider_t384 * __this, const MethodInfo* method)
{
	{
		__this->___totalLeafPlayTime_6 = (2.0f);
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Spider::Start()
extern const MethodInfo* Component_GetComponentInChildren_TisParticleSystem_t161_m1823_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisLineRenderer_t397_m1824_MethodInfo_var;
extern "C" void SCR_Spider_Start_m1552 (SCR_Spider_t384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentInChildren_TisParticleSystem_t161_m1823_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483744);
		Component_GetComponent_TisLineRenderer_t397_m1824_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483745);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4  V_0 = {0};
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_ManagerComponents_set_Spider_m1444(L_0, __this, /*hidden argument*/NULL);
		Transform_t1 * L_1 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4  L_2 = Transform_get_position_m593(L_1, /*hidden argument*/NULL);
		__this->___startPosition_2 = L_2;
		ParticleSystem_t161 * L_3 = Component_GetComponentInChildren_TisParticleSystem_t161_m1823(__this, /*hidden argument*/Component_GetComponentInChildren_TisParticleSystem_t161_m1823_MethodInfo_var);
		__this->___leaves_5 = L_3;
		ParticleSystem_t161 * L_4 = (__this->___leaves_5);
		NullCheck(L_4);
		ParticleSystem_Stop_m1825(L_4, /*hidden argument*/NULL);
		ParticleSystem_t161 * L_5 = (__this->___leaves_5);
		NullCheck(L_5);
		Transform_t1 * L_6 = Component_get_transform_m594(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_parent_m596(L_6, (Transform_t1 *)NULL, /*hidden argument*/NULL);
		__this->___dropLeaves_8 = 0;
		LineRenderer_t397 * L_7 = Component_GetComponent_TisLineRenderer_t397_m1824(__this, /*hidden argument*/Component_GetComponent_TisLineRenderer_t397_m1824_MethodInfo_var);
		__this->___line_3 = L_7;
		LineRenderer_t397 * L_8 = (__this->___line_3);
		NullCheck(L_8);
		LineRenderer_SetVertexCount_m1826(L_8, 2, /*hidden argument*/NULL);
		LineRenderer_t397 * L_9 = (__this->___line_3);
		NullCheck(L_9);
		LineRenderer_SetWidth_m1827(L_9, (0.1f), (0.1f), /*hidden argument*/NULL);
		Vector3_t4  L_10 = (__this->___startPosition_2);
		V_0 = L_10;
		Vector3_t4 * L_11 = (&V_0);
		float L_12 = (L_11->___z_3);
		L_11->___z_3 = ((float)((float)L_12+(float)(1.0f)));
		LineRenderer_t397 * L_13 = (__this->___line_3);
		Vector3_t4  L_14 = V_0;
		NullCheck(L_13);
		LineRenderer_SetPosition_m1828(L_13, 0, L_14, /*hidden argument*/NULL);
		LineRenderer_t397 * L_15 = (__this->___line_3);
		NullCheck(L_15);
		Renderer_set_enabled_m927(L_15, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Spider::FixedUpdate()
extern "C" void SCR_Spider_FixedUpdate_m1553 (SCR_Spider_t384 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___dropLeaves_8);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		SCR_Spider_DropLeaves_m1557(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void SCR_Spider::OnTriggerEnter(UnityEngine.Collider)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void SCR_Spider_OnTriggerEnter_m1554 (SCR_Spider_t384 * __this, Collider_t138 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t138 * L_0 = ___other;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m635(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m636(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral316, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		bool L_3 = MonoBehaviour_IsInvoking_m1763(__this, (String_t*) &_stringLiteral192, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0041;
		}
	}
	{
		LineRenderer_t397 * L_4 = (__this->___line_3);
		NullCheck(L_4);
		Renderer_set_enabled_m927(L_4, 0, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m1015(__this, (String_t*) &_stringLiteral192, (2.0f), /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void SCR_Spider::Activate()
extern "C" void SCR_Spider_Activate_m1555 (SCR_Spider_t384 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isActive_4);
		if (L_0)
		{
			goto IL_0034;
		}
	}
	{
		__this->___isActive_4 = 1;
		__this->___dropLeaves_8 = 1;
		LineRenderer_t397 * L_1 = (__this->___line_3);
		NullCheck(L_1);
		Renderer_set_enabled_m927(L_1, 1, /*hidden argument*/NULL);
		SCR_SoundManager_t335 * L_2 = SCR_ManagerComponents_get_SoundManager_m1439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SCR_SoundManager_PlaySound_m1495(L_2, (String_t*) &_stringLiteral382, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void SCR_Spider::Reset()
extern "C" void SCR_Spider_Reset_m1556 (SCR_Spider_t384 * __this, const MethodInfo* method)
{
	{
		__this->___isActive_4 = 0;
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		Vector3_t4  L_1 = (__this->___startPosition_2);
		NullCheck(L_0);
		Transform_set_position_m607(L_0, L_1, /*hidden argument*/NULL);
		__this->___dropLeaves_8 = 0;
		__this->___currentLeafPlayTime_7 = (0.0f);
		return;
	}
}
// System.Void SCR_Spider::DropLeaves()
extern "C" void SCR_Spider_DropLeaves_m1557 (SCR_Spider_t384 * __this, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	Vector3_t4  V_1 = {0};
	Vector3_t4  V_2 = {0};
	{
		ParticleSystem_t161 * L_0 = (__this->___leaves_5);
		NullCheck(L_0);
		bool L_1 = ParticleSystem_get_isStopped_m1829(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0076;
		}
	}
	{
		ParticleSystem_t161 * L_2 = (__this->___leaves_5);
		NullCheck(L_2);
		ParticleSystem_Play_m935(L_2, /*hidden argument*/NULL);
		SCR_ManagerComponents_t380 * L_3 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SCR_Camie_t338 * L_4 = SCR_ManagerComponents_get_AvatarMove_m1441(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1 * L_5 = Component_get_transform_m594(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4  L_6 = Transform_get_position_m593(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = ((&V_1)->___x_1);
		Camera_t27 * L_8 = Camera_get_main_m989(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_9 = Vector3_get_zero_m601(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t4  L_10 = Camera_ScreenToWorldPoint_m1830(L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = ((&V_2)->___y_2);
		Vector3__ctor_m612((&V_0), L_7, ((float)((float)L_11+(float)(10.0f))), (10.0f), /*hidden argument*/NULL);
		ParticleSystem_t161 * L_12 = (__this->___leaves_5);
		NullCheck(L_12);
		Transform_t1 * L_13 = Component_get_transform_m594(L_12, /*hidden argument*/NULL);
		Vector3_t4  L_14 = V_0;
		NullCheck(L_13);
		Transform_set_position_m607(L_13, L_14, /*hidden argument*/NULL);
		goto IL_00ce;
	}

IL_0076:
	{
		float L_15 = (__this->___currentLeafPlayTime_7);
		float L_16 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___currentLeafPlayTime_7 = ((float)((float)L_15+(float)L_16));
		float L_17 = (__this->___currentLeafPlayTime_7);
		float L_18 = (__this->___totalLeafPlayTime_6);
		if ((!(((float)L_17) < ((float)L_18))))
		{
			goto IL_00b0;
		}
	}
	{
		float L_19 = (__this->___currentLeafPlayTime_7);
		float L_20 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___currentLeafPlayTime_7 = ((float)((float)L_19+(float)L_20));
		goto IL_00ce;
	}

IL_00b0:
	{
		__this->___dropLeaves_8 = 0;
		ParticleSystem_t161 * L_21 = (__this->___leaves_5);
		NullCheck(L_21);
		ParticleSystem_Stop_m1825(L_21, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1641(__this, (String_t*) &_stringLiteral383, /*hidden argument*/NULL);
	}

IL_00ce:
	{
		return;
	}
}
// System.Void SCR_Spider::DrawWebString()
extern "C" void SCR_Spider_DrawWebString_m1558 (SCR_Spider_t384 * __this, const MethodInfo* method)
{
	Vector3_t4  V_0 = {0};
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4  L_1 = Transform_get_position_m593(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t4 * L_2 = (&V_0);
		float L_3 = (L_2->___z_3);
		L_2->___z_3 = ((float)((float)L_3+(float)(1.0f)));
		LineRenderer_t397 * L_4 = (__this->___line_3);
		Vector3_t4  L_5 = V_0;
		NullCheck(L_4);
		LineRenderer_SetPosition_m1828(L_4, 1, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator SCR_Spider::ChaseAvatar()
extern TypeInfo* U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_Spider_ChaseAvatar_m1559 (SCR_Spider_t384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(711);
		s_Il2CppMethodIntialized = true;
	}
	U3CChaseAvatarU3Ec__IteratorC_t396 * V_0 = {0};
	{
		U3CChaseAvatarU3Ec__IteratorC_t396 * L_0 = (U3CChaseAvatarU3Ec__IteratorC_t396 *)il2cpp_codegen_object_new (U3CChaseAvatarU3Ec__IteratorC_t396_il2cpp_TypeInfo_var);
		U3CChaseAvatarU3Ec__IteratorC__ctor_m1545(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CChaseAvatarU3Ec__IteratorC_t396 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_4 = __this;
		U3CChaseAvatarU3Ec__IteratorC_t396 * L_2 = V_0;
		return L_2;
	}
}
// SCR_Web/<DecreaseSize>c__IteratorD
#include "AssemblyU2DCSharp_SCR_Web_U3CDecreaseSizeU3Ec__IteratorD.h"
#ifndef _MSC_VER
#else
#endif
// SCR_Web/<DecreaseSize>c__IteratorD
#include "AssemblyU2DCSharp_SCR_Web_U3CDecreaseSizeU3Ec__IteratorDMethodDeclarations.h"

// SCR_Web
#include "AssemblyU2DCSharp_SCR_Web.h"


// System.Void SCR_Web/<DecreaseSize>c__IteratorD::.ctor()
extern "C" void U3CDecreaseSizeU3Ec__IteratorD__ctor_m1560 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m677(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SCR_Web/<DecreaseSize>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDecreaseSizeU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1561 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object SCR_Web/<DecreaseSize>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDecreaseSizeU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1562 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean SCR_Web/<DecreaseSize>c__IteratorD::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t255_il2cpp_TypeInfo_var;
extern "C" bool U3CDecreaseSizeU3Ec__IteratorD_MoveNext_m1563 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4  V_1 = {0};
	bool V_2 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ab;
		}
	}
	{
		goto IL_00d7;
	}

IL_0021:
	{
		__this->___U3CiU3E__0_0 = 0;
		goto IL_00b9;
	}

IL_002d:
	{
		SCR_Web_t398 * L_2 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_2);
		Transform_t1 * L_3 = Component_get_transform_m594(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t4  L_4 = Transform_get_localScale_m633(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = ((&V_1)->___x_1);
		if ((!(((float)L_5) > ((float)(0.0f)))))
		{
			goto IL_0084;
		}
	}
	{
		SCR_Web_t398 * L_6 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_6);
		Transform_t1 * L_7 = Component_get_transform_m594(L_6, /*hidden argument*/NULL);
		Transform_t1 * L_8 = L_7;
		NullCheck(L_8);
		Vector3_t4  L_9 = Transform_get_localScale_m633(L_8, /*hidden argument*/NULL);
		SCR_Web_t398 * L_10 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_10);
		Vector3_t4  L_11 = (L_10->___reScale_5);
		float L_12 = Time_get_deltaTime_m602(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4  L_13 = Vector3_op_Multiply_m600(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector3_t4  L_14 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_9, L_13, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_localScale_m634(L_8, L_14, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_0084:
	{
		SCR_Web_t398 * L_15 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_15);
		GameObject_t78 * L_16 = Component_get_gameObject_m622(L_15, /*hidden argument*/NULL);
		Object_Destroy_m1008(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_0094:
	{
		WaitForEndOfFrame_t255 * L_17 = (WaitForEndOfFrame_t255 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t255_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m995(L_17, /*hidden argument*/NULL);
		__this->___U24current_2 = L_17;
		__this->___U24PC_1 = 1;
		goto IL_00d9;
	}

IL_00ab:
	{
		int32_t L_18 = (__this->___U3CiU3E__0_0);
		__this->___U3CiU3E__0_0 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_00b9:
	{
		int32_t L_19 = (__this->___U3CiU3E__0_0);
		SCR_Web_t398 * L_20 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_20);
		float L_21 = (L_20->___maxD_6);
		if ((((float)(((float)L_19))) < ((float)L_21)))
		{
			goto IL_002d;
		}
	}
	{
		__this->___U24PC_1 = (-1);
	}

IL_00d7:
	{
		return 0;
	}

IL_00d9:
	{
		return 1;
	}
	// Dead block : IL_00db: ldloc.2
}
// System.Void SCR_Web/<DecreaseSize>c__IteratorD::Dispose()
extern "C" void U3CDecreaseSizeU3Ec__IteratorD_Dispose_m1564 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void SCR_Web/<DecreaseSize>c__IteratorD::Reset()
extern TypeInfo* NotSupportedException_t244_il2cpp_TypeInfo_var;
extern "C" void U3CDecreaseSizeU3Ec__IteratorD_Reset_m1565 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t244 * L_0 = (NotSupportedException_t244 *)il2cpp_codegen_object_new (NotSupportedException_t244_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m905(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// SCR_Web
#include "AssemblyU2DCSharp_SCR_WebMethodDeclarations.h"



// System.Void SCR_Web::.ctor()
extern "C" void SCR_Web__ctor_m1566 (SCR_Web_t398 * __this, const MethodInfo* method)
{
	{
		Vector3_t4  L_0 = {0};
		Vector3__ctor_m612(&L_0, (0.01f), (0.01f), (0.0f), /*hidden argument*/NULL);
		__this->___reScale_5 = L_0;
		SCR_SlowAvatar__ctor_m1221(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Web::Start()
extern const MethodInfo* Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var;
extern "C" void SCR_Web_Start_m1567 (SCR_Web_t398 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t9 * L_0 = Component_GetComponent_TisAnimator_t9_m616(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9_m616_MethodInfo_var);
		__this->___anim_3 = L_0;
		Animator_t9 * L_1 = (__this->___anim_3);
		NullCheck(L_1);
		Behaviour_set_enabled_m766(L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Web::TriggerEnterEffect()
extern "C" void SCR_Web_TriggerEnterEffect_m1568 (SCR_Web_t398 * __this, const MethodInfo* method)
{
	{
		Debug_Log_m857(NULL /*static, unused*/, (String_t*) &_stringLiteral384, /*hidden argument*/NULL);
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Spider_t384 * L_1 = SCR_ManagerComponents_get_Spider_m1443(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		SCR_Spider_Activate_m1555(L_1, /*hidden argument*/NULL);
		Animator_t9 * L_2 = (__this->___anim_3);
		NullCheck(L_2);
		Behaviour_set_enabled_m766(L_2, 1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1641(__this, (String_t*) &_stringLiteral385, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_Web::TriggerExitEffet()
extern "C" void SCR_Web_TriggerExitEffet_m1569 (SCR_Web_t398 * __this, const MethodInfo* method)
{
	{
		Animator_t9 * L_0 = (__this->___anim_3);
		NullCheck(L_0);
		Behaviour_set_enabled_m766(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator SCR_Web::DecreaseSize()
extern TypeInfo* U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo_var;
extern "C" Object_t * SCR_Web_DecreaseSize_m1570 (SCR_Web_t398 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		s_Il2CppMethodIntialized = true;
	}
	U3CDecreaseSizeU3Ec__IteratorD_t399 * V_0 = {0};
	{
		U3CDecreaseSizeU3Ec__IteratorD_t399 * L_0 = (U3CDecreaseSizeU3Ec__IteratorD_t399 *)il2cpp_codegen_object_new (U3CDecreaseSizeU3Ec__IteratorD_t399_il2cpp_TypeInfo_var);
		U3CDecreaseSizeU3Ec__IteratorD__ctor_m1560(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDecreaseSizeU3Ec__IteratorD_t399 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CDecreaseSizeU3Ec__IteratorD_t399 * L_2 = V_0;
		return L_2;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void SCR_CamieDirections::.ctor()
extern "C" void SCR_CamieDirections__ctor_m1571 (SCR_CamieDirections_t348 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SCR_CamieDirections::Start()
extern TypeInfo* QuaternionU5BU5D_t400_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3U5BU5D_t210_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var;
extern "C" void SCR_CamieDirections_Start_m1572 (SCR_CamieDirections_t348 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuaternionU5BU5D_t400_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(713);
		Vector3U5BU5D_t210_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483679);
		s_Il2CppMethodIntialized = true;
	}
	TransformU5BU5D_t141* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t4  V_3 = {0};
	{
		SCR_ManagerComponents_t380 * L_0 = SCR_ManagerComponents_get_ManagerInstance_m1427(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SCR_Camie_t338 * L_1 = SCR_ManagerComponents_get_AvatarMove_m1441(L_0, /*hidden argument*/NULL);
		__this->___avatar_2 = L_1;
		SCR_Camie_t338 * L_2 = (__this->___avatar_2);
		NullCheck(L_2);
		SCR_Camie_set_Director_m1268(L_2, __this, /*hidden argument*/NULL);
		TransformU5BU5D_t141* L_3 = Component_GetComponentsInChildren_TisTransform_t1_m1001(__this, /*hidden argument*/Component_GetComponentsInChildren_TisTransform_t1_m1001_MethodInfo_var);
		TransformU5BU5D_t141* L_4 = SCR_CamieDirections_ReorderByName_m1574(__this, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		__this->___angles_3 = ((QuaternionU5BU5D_t400*)SZArrayNew(QuaternionU5BU5D_t400_il2cpp_TypeInfo_var, ((int32_t)24)));
		__this->___normals_4 = ((Vector3U5BU5D_t210*)SZArrayNew(Vector3U5BU5D_t210_il2cpp_TypeInfo_var, ((int32_t)24)));
		V_1 = 0;
		V_2 = 0;
		goto IL_00b1;
	}

IL_004c:
	{
		TransformU5BU5D_t141* L_5 = V_0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Transform_t1 * L_8 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		bool L_9 = Object_op_Inequality_m623(NULL /*static, unused*/, (*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_5, L_7)), L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00ad;
		}
	}
	{
		QuaternionU5BU5D_t400* L_10 = (__this->___angles_3);
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		TransformU5BU5D_t141* L_12 = V_0;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_12, L_14)));
		Quaternion_t19  L_15 = Transform_get_rotation_m656((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_12, L_14)), /*hidden argument*/NULL);
		*((Quaternion_t19 *)(Quaternion_t19 *)SZArrayLdElema(L_10, L_11)) = L_15;
		Vector3U5BU5D_t210* L_16 = (__this->___normals_4);
		int32_t L_17 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		TransformU5BU5D_t141* L_18 = V_0;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_18, L_20)));
		Vector3_t4  L_21 = Transform_get_position_m593((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_18, L_20)), /*hidden argument*/NULL);
		Transform_t1 * L_22 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t4  L_23 = Transform_get_position_m593(L_22, /*hidden argument*/NULL);
		Vector3_t4  L_24 = Vector3_op_Subtraction_m595(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		V_3 = L_24;
		Vector3_t4  L_25 = Vector3_get_normalized_m648((&V_3), /*hidden argument*/NULL);
		*((Vector3_t4 *)(Vector3_t4 *)SZArrayLdElema(L_16, L_17)) = L_25;
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00ad:
	{
		int32_t L_27 = V_2;
		V_2 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_28 = V_2;
		TransformU5BU5D_t141* L_29 = V_0;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_29)->max_length)))-(int32_t)1)))))
		{
			goto IL_004c;
		}
	}
	{
		return;
	}
}
// System.Void SCR_CamieDirections::Update()
extern "C" void SCR_CamieDirections_Update_m1573 (SCR_CamieDirections_t348 * __this, const MethodInfo* method)
{
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		SCR_Camie_t338 * L_1 = (__this->___avatar_2);
		NullCheck(L_1);
		Vector3_t4  L_2 = SCR_Camie_get_ColliderCenter_m1266(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m607(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform[] SCR_CamieDirections::ReorderByName(UnityEngine.Transform[])
extern TypeInfo* TransformU5BU5D_t141_il2cpp_TypeInfo_var;
extern "C" TransformU5BU5D_t141* SCR_CamieDirections_ReorderByName_m1574 (SCR_CamieDirections_t348 * __this, TransformU5BU5D_t141* ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TransformU5BU5D_t141_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(105);
		s_Il2CppMethodIntialized = true;
	}
	TransformU5BU5D_t141* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		TransformU5BU5D_t141* L_0 = ___array;
		NullCheck(L_0);
		V_0 = ((TransformU5BU5D_t141*)SZArrayNew(TransformU5BU5D_t141_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_0)->max_length)))));
		V_1 = 0;
		goto IL_003f;
	}

IL_0010:
	{
		TransformU5BU5D_t141* L_1 = ___array;
		NullCheck(L_1);
		V_2 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))-(int32_t)1));
		TransformU5BU5D_t141* L_2 = ___array;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_2, L_4)));
		String_t* L_5 = Object_get_name_m798((*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_2, L_4)), /*hidden argument*/NULL);
		bool L_6 = Int32_TryParse_m1831(NULL /*static, unused*/, L_5, (&V_2), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		TransformU5BU5D_t141* L_7 = V_0;
		int32_t L_8 = V_2;
		TransformU5BU5D_t141* L_9 = ___array;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, (*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_9, L_11)));
		*((Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_7, L_8)) = (Transform_t1 *)(*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_9, L_11));
		goto IL_003b;
	}

IL_0035:
	{
		TransformU5BU5D_t141* L_12 = V_0;
		int32_t L_13 = V_2;
		TransformU5BU5D_t141* L_14 = ___array;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		ArrayElementTypeCheck (L_12, (*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_14, L_16)));
		*((Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_12, L_13)) = (Transform_t1 *)(*(Transform_t1 **)(Transform_t1 **)SZArrayLdElema(L_14, L_16));
	}

IL_003b:
	{
		int32_t L_17 = V_1;
		V_1 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_18 = V_1;
		TransformU5BU5D_t141* L_19 = ___array;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)(((Array_t *)L_19)->max_length))))))
		{
			goto IL_0010;
		}
	}
	{
		TransformU5BU5D_t141* L_20 = V_0;
		return L_20;
	}
}
// UnityEngine.Quaternion[] SCR_CamieDirections::get_Angles()
extern "C" QuaternionU5BU5D_t400* SCR_CamieDirections_get_Angles_m1575 (SCR_CamieDirections_t348 * __this, const MethodInfo* method)
{
	{
		QuaternionU5BU5D_t400* L_0 = (__this->___angles_3);
		return L_0;
	}
}
// UnityEngine.Vector3[] SCR_CamieDirections::get_Normals()
extern "C" Vector3U5BU5D_t210* SCR_CamieDirections_get_Normals_m1576 (SCR_CamieDirections_t348 * __this, const MethodInfo* method)
{
	{
		Vector3U5BU5D_t210* L_0 = (__this->___normals_4);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
