﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_LevelEndMenu
struct SCR_LevelEndMenu_t367;

// System.Void SCR_LevelEndMenu::.ctor()
extern "C" void SCR_LevelEndMenu__ctor_m1364 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelEndMenu::Awake()
extern "C" void SCR_LevelEndMenu_Awake_m1365 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelEndMenu::OnEnable()
extern "C" void SCR_LevelEndMenu_OnEnable_m1366 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelEndMenu::AssignButtonImages()
extern "C" void SCR_LevelEndMenu_AssignButtonImages_m1367 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelEndMenu::Reload()
extern "C" void SCR_LevelEndMenu_Reload_m1368 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelEndMenu::LoadNext()
extern "C" void SCR_LevelEndMenu_LoadNext_m1369 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelEndMenu::NewHighScore()
extern "C" void SCR_LevelEndMenu_NewHighScore_m1370 (SCR_LevelEndMenu_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
