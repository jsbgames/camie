﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>
struct Collection_1_t2938;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2933;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t3572;
// System.Collections.Generic.IList`1<UnityEngine.EventSystems.RaycastResult>
struct IList_1_t2936;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void Collection_1__ctor_m15297_gshared (Collection_1_t2938 * __this, const MethodInfo* method);
#define Collection_1__ctor_m15297(__this, method) (( void (*) (Collection_1_t2938 *, const MethodInfo*))Collection_1__ctor_m15297_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15298_gshared (Collection_1_t2938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15298(__this, method) (( bool (*) (Collection_1_t2938 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15298_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15299_gshared (Collection_1_t2938 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m15299(__this, ___array, ___index, method) (( void (*) (Collection_1_t2938 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m15299_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m15300_gshared (Collection_1_t2938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m15300(__this, method) (( Object_t * (*) (Collection_1_t2938 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m15300_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m15301_gshared (Collection_1_t2938 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m15301(__this, ___value, method) (( int32_t (*) (Collection_1_t2938 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m15301_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m15302_gshared (Collection_1_t2938 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m15302(__this, ___value, method) (( bool (*) (Collection_1_t2938 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m15302_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m15303_gshared (Collection_1_t2938 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m15303(__this, ___value, method) (( int32_t (*) (Collection_1_t2938 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m15303_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m15304_gshared (Collection_1_t2938 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m15304(__this, ___index, ___value, method) (( void (*) (Collection_1_t2938 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m15304_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m15305_gshared (Collection_1_t2938 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m15305(__this, ___value, method) (( void (*) (Collection_1_t2938 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m15305_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m15306_gshared (Collection_1_t2938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m15306(__this, method) (( bool (*) (Collection_1_t2938 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m15306_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m15307_gshared (Collection_1_t2938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m15307(__this, method) (( Object_t * (*) (Collection_1_t2938 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m15307_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m15308_gshared (Collection_1_t2938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m15308(__this, method) (( bool (*) (Collection_1_t2938 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m15308_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m15309_gshared (Collection_1_t2938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m15309(__this, method) (( bool (*) (Collection_1_t2938 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m15309_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m15310_gshared (Collection_1_t2938 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m15310(__this, ___index, method) (( Object_t * (*) (Collection_1_t2938 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m15310_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m15311_gshared (Collection_1_t2938 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m15311(__this, ___index, ___value, method) (( void (*) (Collection_1_t2938 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m15311_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void Collection_1_Add_m15312_gshared (Collection_1_t2938 * __this, RaycastResult_t486  ___item, const MethodInfo* method);
#define Collection_1_Add_m15312(__this, ___item, method) (( void (*) (Collection_1_t2938 *, RaycastResult_t486 , const MethodInfo*))Collection_1_Add_m15312_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void Collection_1_Clear_m15313_gshared (Collection_1_t2938 * __this, const MethodInfo* method);
#define Collection_1_Clear_m15313(__this, method) (( void (*) (Collection_1_t2938 *, const MethodInfo*))Collection_1_Clear_m15313_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ClearItems()
extern "C" void Collection_1_ClearItems_m15314_gshared (Collection_1_t2938 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m15314(__this, method) (( void (*) (Collection_1_t2938 *, const MethodInfo*))Collection_1_ClearItems_m15314_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool Collection_1_Contains_m15315_gshared (Collection_1_t2938 * __this, RaycastResult_t486  ___item, const MethodInfo* method);
#define Collection_1_Contains_m15315(__this, ___item, method) (( bool (*) (Collection_1_t2938 *, RaycastResult_t486 , const MethodInfo*))Collection_1_Contains_m15315_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m15316_gshared (Collection_1_t2938 * __this, RaycastResultU5BU5D_t2933* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m15316(__this, ___array, ___index, method) (( void (*) (Collection_1_t2938 *, RaycastResultU5BU5D_t2933*, int32_t, const MethodInfo*))Collection_1_CopyTo_m15316_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m15317_gshared (Collection_1_t2938 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m15317(__this, method) (( Object_t* (*) (Collection_1_t2938 *, const MethodInfo*))Collection_1_GetEnumerator_m15317_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m15318_gshared (Collection_1_t2938 * __this, RaycastResult_t486  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m15318(__this, ___item, method) (( int32_t (*) (Collection_1_t2938 *, RaycastResult_t486 , const MethodInfo*))Collection_1_IndexOf_m15318_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m15319_gshared (Collection_1_t2938 * __this, int32_t ___index, RaycastResult_t486  ___item, const MethodInfo* method);
#define Collection_1_Insert_m15319(__this, ___index, ___item, method) (( void (*) (Collection_1_t2938 *, int32_t, RaycastResult_t486 , const MethodInfo*))Collection_1_Insert_m15319_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m15320_gshared (Collection_1_t2938 * __this, int32_t ___index, RaycastResult_t486  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m15320(__this, ___index, ___item, method) (( void (*) (Collection_1_t2938 *, int32_t, RaycastResult_t486 , const MethodInfo*))Collection_1_InsertItem_m15320_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool Collection_1_Remove_m15321_gshared (Collection_1_t2938 * __this, RaycastResult_t486  ___item, const MethodInfo* method);
#define Collection_1_Remove_m15321(__this, ___item, method) (( bool (*) (Collection_1_t2938 *, RaycastResult_t486 , const MethodInfo*))Collection_1_Remove_m15321_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m15322_gshared (Collection_1_t2938 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m15322(__this, ___index, method) (( void (*) (Collection_1_t2938 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m15322_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m15323_gshared (Collection_1_t2938 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m15323(__this, ___index, method) (( void (*) (Collection_1_t2938 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m15323_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t Collection_1_get_Count_m15324_gshared (Collection_1_t2938 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m15324(__this, method) (( int32_t (*) (Collection_1_t2938 *, const MethodInfo*))Collection_1_get_Count_m15324_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t486  Collection_1_get_Item_m15325_gshared (Collection_1_t2938 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m15325(__this, ___index, method) (( RaycastResult_t486  (*) (Collection_1_t2938 *, int32_t, const MethodInfo*))Collection_1_get_Item_m15325_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m15326_gshared (Collection_1_t2938 * __this, int32_t ___index, RaycastResult_t486  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m15326(__this, ___index, ___value, method) (( void (*) (Collection_1_t2938 *, int32_t, RaycastResult_t486 , const MethodInfo*))Collection_1_set_Item_m15326_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m15327_gshared (Collection_1_t2938 * __this, int32_t ___index, RaycastResult_t486  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m15327(__this, ___index, ___item, method) (( void (*) (Collection_1_t2938 *, int32_t, RaycastResult_t486 , const MethodInfo*))Collection_1_SetItem_m15327_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m15328_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m15328(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m15328_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ConvertItem(System.Object)
extern "C" RaycastResult_t486  Collection_1_ConvertItem_m15329_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m15329(__this /* static, unused */, ___item, method) (( RaycastResult_t486  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m15329_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m15330_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m15330(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m15330_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m15331_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m15331(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m15331_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m15332_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m15332(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m15332_gshared)(__this /* static, unused */, ___list, method)
