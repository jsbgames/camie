﻿#pragma once
#include <stdint.h>
// SCR_PauseMenu
struct SCR_PauseMenu_t374;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t192;
// SCR_Menu
#include "AssemblyU2DCSharp_SCR_Menu.h"
// SCR_PauseMenu
struct  SCR_PauseMenu_t374  : public SCR_Menu_t336
{
	// SCR_PauseMenu SCR_PauseMenu::instance
	SCR_PauseMenu_t374 * ___instance_5;
	// UnityEngine.GameObject[] SCR_PauseMenu::ladybugGUI
	GameObjectU5BU5D_t192* ___ladybugGUI_6;
};
