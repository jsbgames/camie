﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.QualitySettings
struct QualitySettings_t800;
// UnityEngine.ColorSpace
#include "UnityEngine_UnityEngine_ColorSpace.h"

// System.Void UnityEngine.QualitySettings::set_shadowDistance(System.Single)
extern "C" void QualitySettings_set_shadowDistance_m992 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C" int32_t QualitySettings_get_activeColorSpace_m818 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
