﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>
struct InternalEnumerator_1_t3197;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18970_gshared (InternalEnumerator_1_t3197 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18970(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3197 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18970_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18971_gshared (InternalEnumerator_1_t3197 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18971(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3197 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18971_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18972_gshared (InternalEnumerator_1_t3197 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18972(__this, method) (( void (*) (InternalEnumerator_1_t3197 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18972_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18973_gshared (InternalEnumerator_1_t3197 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18973(__this, method) (( bool (*) (InternalEnumerator_1_t3197 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18973_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern "C" GcScoreData_t959  InternalEnumerator_1_get_Current_m18974_gshared (InternalEnumerator_1_t3197 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18974(__this, method) (( GcScoreData_t959  (*) (InternalEnumerator_1_t3197 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18974_gshared)(__this, method)
