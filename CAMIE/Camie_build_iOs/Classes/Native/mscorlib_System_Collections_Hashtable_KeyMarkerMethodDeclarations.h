﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Hashtable/KeyMarker
struct KeyMarker_t1768;

// System.Void System.Collections.Hashtable/KeyMarker::.ctor()
extern "C" void KeyMarker__ctor_m9076 (KeyMarker_t1768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/KeyMarker::.cctor()
extern "C" void KeyMarker__cctor_m9077 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
