﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t2251;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Runtime.Remoting.Messaging.HeaderHandler
struct  HeaderHandler_t2252  : public MulticastDelegate_t549
{
};
