﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.Vector2>
struct Comparer_1_t2920;
// System.Object
struct Object_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.ctor()
extern "C" void Comparer_1__ctor_m15088_gshared (Comparer_1_t2920 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m15088(__this, method) (( void (*) (Comparer_1_t2920 *, const MethodInfo*))Comparer_1__ctor_m15088_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.cctor()
extern "C" void Comparer_1__cctor_m15089_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m15089(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m15089_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m15090_gshared (Comparer_1_t2920 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m15090(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2920 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m15090_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::get_Default()
extern "C" Comparer_1_t2920 * Comparer_1_get_Default_m15091_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m15091(__this /* static, unused */, method) (( Comparer_1_t2920 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m15091_gshared)(__this /* static, unused */, method)
