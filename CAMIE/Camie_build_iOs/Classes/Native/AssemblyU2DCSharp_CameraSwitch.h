﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t192;
// UnityEngine.UI.Text
struct Text_t316;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraSwitch
struct  CameraSwitch_t317  : public MonoBehaviour_t3
{
	// UnityEngine.GameObject[] CameraSwitch::objects
	GameObjectU5BU5D_t192* ___objects_2;
	// UnityEngine.UI.Text CameraSwitch::text
	Text_t316 * ___text_3;
	// System.Int32 CameraSwitch::m_CurrentActiveObject
	int32_t ___m_CurrentActiveObject_4;
};
