﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3
struct U3CDragObjectU3Ec__Iterator3_t175;
// System.Object
struct Object_t;

// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::.ctor()
extern "C" void U3CDragObjectU3Ec__Iterator3__ctor_m460 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDragObjectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m461 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDragObjectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m462 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::MoveNext()
extern "C" bool U3CDragObjectU3Ec__Iterator3_MoveNext_m463 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::Dispose()
extern "C" void U3CDragObjectU3Ec__Iterator3_Dispose_m464 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator3::Reset()
extern "C" void U3CDragObjectU3Ec__Iterator3_Reset_m465 (U3CDragObjectU3Ec__Iterator3_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
