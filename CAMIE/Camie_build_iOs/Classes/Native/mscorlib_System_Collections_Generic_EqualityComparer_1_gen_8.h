﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.UInt64>
struct EqualityComparer_1_t3305;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.UInt64>
struct  EqualityComparer_1_t3305  : public Object_t
{
};
struct EqualityComparer_1_t3305_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt64>::_default
	EqualityComparer_1_t3305 * ____default_0;
};
