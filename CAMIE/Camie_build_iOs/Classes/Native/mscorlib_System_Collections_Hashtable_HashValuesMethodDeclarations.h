﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Hashtable/HashValues
struct HashValues_t1772;
// System.Object
struct Object_t;
// System.Collections.Hashtable
struct Hashtable_t1262;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void System.Collections.Hashtable/HashValues::.ctor(System.Collections.Hashtable)
extern "C" void HashValues__ctor_m9093 (HashValues_t1772 * __this, Hashtable_t1262 * ___host, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Hashtable/HashValues::get_Count()
extern "C" int32_t HashValues_get_Count_m9094 (HashValues_t1772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Hashtable/HashValues::get_IsSynchronized()
extern "C" bool HashValues_get_IsSynchronized_m9095 (HashValues_t1772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Hashtable/HashValues::get_SyncRoot()
extern "C" Object_t * HashValues_get_SyncRoot_m9096 (HashValues_t1772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/HashValues::CopyTo(System.Array,System.Int32)
extern "C" void HashValues_CopyTo_m9097 (HashValues_t1772 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Hashtable/HashValues::GetEnumerator()
extern "C" Object_t * HashValues_GetEnumerator_m9098 (HashValues_t1772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
