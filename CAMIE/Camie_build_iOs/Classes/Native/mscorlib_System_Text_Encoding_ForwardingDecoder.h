﻿#pragma once
#include <stdint.h>
// System.Text.Encoding
struct Encoding_t1022;
// System.Text.Decoder
#include "mscorlib_System_Text_Decoder.h"
// System.Text.Encoding/ForwardingDecoder
struct  ForwardingDecoder_t2147  : public Decoder_t1804
{
	// System.Text.Encoding System.Text.Encoding/ForwardingDecoder::encoding
	Encoding_t1022 * ___encoding_2;
};
