﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t224;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct  ConstructorDelegate_t939  : public MulticastDelegate_t549
{
};
