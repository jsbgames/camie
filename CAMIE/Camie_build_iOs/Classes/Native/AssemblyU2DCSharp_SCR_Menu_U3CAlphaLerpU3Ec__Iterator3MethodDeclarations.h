﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Menu/<AlphaLerp>c__Iterator3
struct U3CAlphaLerpU3Ec__Iterator3_t333;
// System.Object
struct Object_t;

// System.Void SCR_Menu/<AlphaLerp>c__Iterator3::.ctor()
extern "C" void U3CAlphaLerpU3Ec__Iterator3__ctor_m1198 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Menu/<AlphaLerp>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAlphaLerpU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Menu/<AlphaLerp>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAlphaLerpU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1200 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Menu/<AlphaLerp>c__Iterator3::MoveNext()
extern "C" bool U3CAlphaLerpU3Ec__Iterator3_MoveNext_m1201 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu/<AlphaLerp>c__Iterator3::Dispose()
extern "C" void U3CAlphaLerpU3Ec__Iterator3_Dispose_m1202 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu/<AlphaLerp>c__Iterator3::Reset()
extern "C" void U3CAlphaLerpU3Ec__Iterator3_Reset_m1203 (U3CAlphaLerpU3Ec__Iterator3_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
