﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>
struct InternalEnumerator_1_t3464;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22242_gshared (InternalEnumerator_1_t3464 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22242(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3464 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22242_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22243_gshared (InternalEnumerator_1_t3464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22243(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3464 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22243_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22244_gshared (InternalEnumerator_1_t3464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22244(__this, method) (( void (*) (InternalEnumerator_1_t3464 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22244_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22245_gshared (InternalEnumerator_1_t3464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22245(__this, method) (( bool (*) (InternalEnumerator_1_t3464 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22245_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern "C" Slot_t1776  InternalEnumerator_1_get_Current_m22246_gshared (InternalEnumerator_1_t3464 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22246(__this, method) (( Slot_t1776  (*) (InternalEnumerator_1_t3464 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22246_gshared)(__this, method)
