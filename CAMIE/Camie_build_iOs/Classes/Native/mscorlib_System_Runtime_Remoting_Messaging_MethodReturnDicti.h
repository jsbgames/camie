﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t243;
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
struct  MethodReturnDictionary_t1991  : public MethodDictionary_t1983
{
};
struct MethodReturnDictionary_t1991_StaticFields{
	// System.String[] System.Runtime.Remoting.Messaging.MethodReturnDictionary::InternalReturnKeys
	StringU5BU5D_t243* ___InternalReturnKeys_6;
	// System.String[] System.Runtime.Remoting.Messaging.MethodReturnDictionary::InternalExceptionKeys
	StringU5BU5D_t243* ___InternalExceptionKeys_7;
};
