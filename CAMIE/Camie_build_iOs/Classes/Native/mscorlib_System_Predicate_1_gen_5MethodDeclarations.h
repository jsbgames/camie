﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.Rigidbody>
struct Predicate_1_t2868;
// System.Object
struct Object_t;
// UnityEngine.Rigidbody
struct Rigidbody_t14;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<UnityEngine.Rigidbody>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#define Predicate_1__ctor_m14241(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2868 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m14055_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Rigidbody>::Invoke(T)
#define Predicate_1_Invoke_m14242(__this, ___obj, method) (( bool (*) (Predicate_1_t2868 *, Rigidbody_t14 *, const MethodInfo*))Predicate_1_Invoke_m14056_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Rigidbody>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m14243(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2868 *, Rigidbody_t14 *, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m14057_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Rigidbody>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m14244(__this, ___result, method) (( bool (*) (Predicate_1_t2868 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m14058_gshared)(__this, ___result, method)
