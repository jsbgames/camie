﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// SCR_Camie
struct SCR_Camie_t338;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4
struct  U3CGoToFrontCollisionPointU3Ec__Iterator4_t339  : public Object_t
{
	// UnityEngine.Vector3 SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::normal
	Vector3_t4  ___normal_0;
	// UnityEngine.Vector3 SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::point
	Vector3_t4  ___point_1;
	// System.Int32 SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::$PC
	int32_t ___U24PC_2;
	// System.Object SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::$current
	Object_t * ___U24current_3;
	// UnityEngine.Vector3 SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::<$>normal
	Vector3_t4  ___U3CU24U3Enormal_4;
	// UnityEngine.Vector3 SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::<$>point
	Vector3_t4  ___U3CU24U3Epoint_5;
	// SCR_Camie SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::<>f__this
	SCR_Camie_t338 * ___U3CU3Ef__this_6;
};
