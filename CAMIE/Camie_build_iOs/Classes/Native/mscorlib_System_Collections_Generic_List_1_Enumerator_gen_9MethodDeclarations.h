﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Reporter/Log>
struct Enumerator_t2904;
// System.Object
struct Object_t;
// Reporter/Log
struct Log_t291;
// System.Collections.Generic.List`1<Reporter/Log>
struct List_1_t298;

// System.Void System.Collections.Generic.List`1/Enumerator<Reporter/Log>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m14751(__this, ___l, method) (( void (*) (Enumerator_t2904 *, List_1_t298 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Reporter/Log>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14752(__this, method) (( Object_t * (*) (Enumerator_t2904 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Reporter/Log>::Dispose()
#define Enumerator_Dispose_m14753(__this, method) (( void (*) (Enumerator_t2904 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Reporter/Log>::VerifyState()
#define Enumerator_VerifyState_m14754(__this, method) (( void (*) (Enumerator_t2904 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Reporter/Log>::MoveNext()
#define Enumerator_MoveNext_m14755(__this, method) (( bool (*) (Enumerator_t2904 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Reporter/Log>::get_Current()
#define Enumerator_get_Current_m14756(__this, method) (( Log_t291 * (*) (Enumerator_t2904 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
