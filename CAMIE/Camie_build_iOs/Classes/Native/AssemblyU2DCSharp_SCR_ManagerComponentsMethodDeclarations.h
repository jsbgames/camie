﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_ManagerComponents
struct SCR_ManagerComponents_t380;
// SCR_PointManager
struct SCR_PointManager_t381;
// SCR_LevelManager
struct SCR_LevelManager_t379;
// SCR_SaveData
struct SCR_SaveData_t382;
// SCR_TimeManager
struct SCR_TimeManager_t383;
// SCR_GUIManager
struct SCR_GUIManager_t378;
// SCR_SoundManager
struct SCR_SoundManager_t335;
// SCR_Camie
struct SCR_Camie_t338;
// SCR_Spider
struct SCR_Spider_t384;
// SCR_Joystick
struct SCR_Joystick_t346;
// SCR_FlyButton
struct SCR_FlyButton_t345;
// UnityEngine.Transform
struct Transform_t1;

// System.Void SCR_ManagerComponents::.ctor()
extern "C" void SCR_ManagerComponents__ctor_m1424 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::Awake()
extern "C" void SCR_ManagerComponents_Awake_m1425 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::OnLevelWasLoaded()
extern "C" void SCR_ManagerComponents_OnLevelWasLoaded_m1426 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_ManagerComponents SCR_ManagerComponents::get_ManagerInstance()
extern "C" SCR_ManagerComponents_t380 * SCR_ManagerComponents_get_ManagerInstance_m1427 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_ManagerInstance(SCR_ManagerComponents)
extern "C" void SCR_ManagerComponents_set_ManagerInstance_m1428 (Object_t * __this /* static, unused */, SCR_ManagerComponents_t380 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_PointManager SCR_ManagerComponents::get_PointManager()
extern "C" SCR_PointManager_t381 * SCR_ManagerComponents_get_PointManager_m1429 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_PointManager(SCR_PointManager)
extern "C" void SCR_ManagerComponents_set_PointManager_m1430 (Object_t * __this /* static, unused */, SCR_PointManager_t381 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_LevelManager SCR_ManagerComponents::get_LevelManager()
extern "C" SCR_LevelManager_t379 * SCR_ManagerComponents_get_LevelManager_m1431 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_LevelManager(SCR_LevelManager)
extern "C" void SCR_ManagerComponents_set_LevelManager_m1432 (Object_t * __this /* static, unused */, SCR_LevelManager_t379 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_SaveData SCR_ManagerComponents::get_SaveData()
extern "C" SCR_SaveData_t382 * SCR_ManagerComponents_get_SaveData_m1433 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_SaveData(SCR_SaveData)
extern "C" void SCR_ManagerComponents_set_SaveData_m1434 (Object_t * __this /* static, unused */, SCR_SaveData_t382 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_TimeManager SCR_ManagerComponents::get_TimeManager()
extern "C" SCR_TimeManager_t383 * SCR_ManagerComponents_get_TimeManager_m1435 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_TimeManager(SCR_TimeManager)
extern "C" void SCR_ManagerComponents_set_TimeManager_m1436 (Object_t * __this /* static, unused */, SCR_TimeManager_t383 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_GUIManager SCR_ManagerComponents::get_GUIManager()
extern "C" SCR_GUIManager_t378 * SCR_ManagerComponents_get_GUIManager_m1437 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_GUIManager(SCR_GUIManager)
extern "C" void SCR_ManagerComponents_set_GUIManager_m1438 (Object_t * __this /* static, unused */, SCR_GUIManager_t378 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_SoundManager SCR_ManagerComponents::get_SoundManager()
extern "C" SCR_SoundManager_t335 * SCR_ManagerComponents_get_SoundManager_m1439 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_SoundManager(SCR_SoundManager)
extern "C" void SCR_ManagerComponents_set_SoundManager_m1440 (Object_t * __this /* static, unused */, SCR_SoundManager_t335 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_Camie SCR_ManagerComponents::get_AvatarMove()
extern "C" SCR_Camie_t338 * SCR_ManagerComponents_get_AvatarMove_m1441 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_AvatarMove(SCR_Camie)
extern "C" void SCR_ManagerComponents_set_AvatarMove_m1442 (SCR_ManagerComponents_t380 * __this, SCR_Camie_t338 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_Spider SCR_ManagerComponents::get_Spider()
extern "C" SCR_Spider_t384 * SCR_ManagerComponents_get_Spider_m1443 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_Spider(SCR_Spider)
extern "C" void SCR_ManagerComponents_set_Spider_m1444 (SCR_ManagerComponents_t380 * __this, SCR_Spider_t384 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_Joystick SCR_ManagerComponents::get_Joystick()
extern "C" SCR_Joystick_t346 * SCR_ManagerComponents_get_Joystick_m1445 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_Joystick(SCR_Joystick)
extern "C" void SCR_ManagerComponents_set_Joystick_m1446 (SCR_ManagerComponents_t380 * __this, SCR_Joystick_t346 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_FlyButton SCR_ManagerComponents::get_FlyButton()
extern "C" SCR_FlyButton_t345 * SCR_ManagerComponents_get_FlyButton_m1447 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_FlyButton(SCR_FlyButton)
extern "C" void SCR_ManagerComponents_set_FlyButton_m1448 (SCR_ManagerComponents_t380 * __this, SCR_FlyButton_t345 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform SCR_ManagerComponents::get_LeftFrame()
extern "C" Transform_t1 * SCR_ManagerComponents_get_LeftFrame_m1449 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_LeftFrame(UnityEngine.Transform)
extern "C" void SCR_ManagerComponents_set_LeftFrame_m1450 (SCR_ManagerComponents_t380 * __this, Transform_t1 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform SCR_ManagerComponents::get_RightFrame()
extern "C" Transform_t1 * SCR_ManagerComponents_get_RightFrame_m1451 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_RightFrame(UnityEngine.Transform)
extern "C" void SCR_ManagerComponents_set_RightFrame_m1452 (SCR_ManagerComponents_t380 * __this, Transform_t1 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform SCR_ManagerComponents::get_BottomFrame()
extern "C" Transform_t1 * SCR_ManagerComponents_get_BottomFrame_m1453 (SCR_ManagerComponents_t380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ManagerComponents::set_BottomFrame(UnityEngine.Transform)
extern "C" void SCR_ManagerComponents_set_BottomFrame_m1454 (SCR_ManagerComponents_t380 * __this, Transform_t1 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
