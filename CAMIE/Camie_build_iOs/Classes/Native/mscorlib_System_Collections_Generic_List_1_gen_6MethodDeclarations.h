﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t417;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerable_1_t3571;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t3572;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.RaycastResult>
struct ICollection_1_t3573;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
struct ReadOnlyCollection_1_t2937;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2933;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2941;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t454;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void List_1__ctor_m1733_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1__ctor_m1733(__this, method) (( void (*) (List_1_t417 *, const MethodInfo*))List_1__ctor_m1733_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m15208_gshared (List_1_t417 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m15208(__this, ___collection, method) (( void (*) (List_1_t417 *, Object_t*, const MethodInfo*))List_1__ctor_m15208_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m15209_gshared (List_1_t417 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m15209(__this, ___capacity, method) (( void (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1__ctor_m15209_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern "C" void List_1__cctor_m15210_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m15210(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15210_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15211_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15211(__this, method) (( Object_t* (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15211_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15212_gshared (List_1_t417 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m15212(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t417 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15212_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m15213_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15213(__this, method) (( Object_t * (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15213_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m15214_gshared (List_1_t417 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m15214(__this, ___item, method) (( int32_t (*) (List_1_t417 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m15214_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m15215_gshared (List_1_t417 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m15215(__this, ___item, method) (( bool (*) (List_1_t417 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m15215_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m15216_gshared (List_1_t417 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m15216(__this, ___item, method) (( int32_t (*) (List_1_t417 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m15216_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m15217_gshared (List_1_t417 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m15217(__this, ___index, ___item, method) (( void (*) (List_1_t417 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m15217_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m15218_gshared (List_1_t417 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m15218(__this, ___item, method) (( void (*) (List_1_t417 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m15218_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15219_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15219(__this, method) (( bool (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15219_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m15220_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15220(__this, method) (( bool (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15220_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m15221_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m15221(__this, method) (( Object_t * (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15221_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m15222_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m15222(__this, method) (( bool (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m15223_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m15223(__this, method) (( bool (*) (List_1_t417 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15223_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m15224_gshared (List_1_t417 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m15224(__this, ___index, method) (( Object_t * (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m15224_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m15225_gshared (List_1_t417 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m15225(__this, ___index, ___value, method) (( void (*) (List_1_t417 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m15225_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void List_1_Add_m15226_gshared (List_1_t417 * __this, RaycastResult_t486  ___item, const MethodInfo* method);
#define List_1_Add_m15226(__this, ___item, method) (( void (*) (List_1_t417 *, RaycastResult_t486 , const MethodInfo*))List_1_Add_m15226_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m15227_gshared (List_1_t417 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m15227(__this, ___newCount, method) (( void (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15227_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m15228_gshared (List_1_t417 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m15228(__this, ___collection, method) (( void (*) (List_1_t417 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15228_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m15229_gshared (List_1_t417 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m15229(__this, ___enumerable, method) (( void (*) (List_1_t417 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15229_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m15230_gshared (List_1_t417 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m15230(__this, ___collection, method) (( void (*) (List_1_t417 *, Object_t*, const MethodInfo*))List_1_AddRange_m15230_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2937 * List_1_AsReadOnly_m15231_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m15231(__this, method) (( ReadOnlyCollection_1_t2937 * (*) (List_1_t417 *, const MethodInfo*))List_1_AsReadOnly_m15231_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void List_1_Clear_m15232_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_Clear_m15232(__this, method) (( void (*) (List_1_t417 *, const MethodInfo*))List_1_Clear_m15232_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool List_1_Contains_m15233_gshared (List_1_t417 * __this, RaycastResult_t486  ___item, const MethodInfo* method);
#define List_1_Contains_m15233(__this, ___item, method) (( bool (*) (List_1_t417 *, RaycastResult_t486 , const MethodInfo*))List_1_Contains_m15233_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m15234_gshared (List_1_t417 * __this, RaycastResultU5BU5D_t2933* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m15234(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t417 *, RaycastResultU5BU5D_t2933*, int32_t, const MethodInfo*))List_1_CopyTo_m15234_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Find(System.Predicate`1<T>)
extern "C" RaycastResult_t486  List_1_Find_m15235_gshared (List_1_t417 * __this, Predicate_1_t2941 * ___match, const MethodInfo* method);
#define List_1_Find_m15235(__this, ___match, method) (( RaycastResult_t486  (*) (List_1_t417 *, Predicate_1_t2941 *, const MethodInfo*))List_1_Find_m15235_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m15236_gshared (Object_t * __this /* static, unused */, Predicate_1_t2941 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m15236(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2941 *, const MethodInfo*))List_1_CheckMatch_m15236_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m15237_gshared (List_1_t417 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2941 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m15237(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t417 *, int32_t, int32_t, Predicate_1_t2941 *, const MethodInfo*))List_1_GetIndex_m15237_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Enumerator_t2935  List_1_GetEnumerator_m15238_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m15238(__this, method) (( Enumerator_t2935  (*) (List_1_t417 *, const MethodInfo*))List_1_GetEnumerator_m15238_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m15239_gshared (List_1_t417 * __this, RaycastResult_t486  ___item, const MethodInfo* method);
#define List_1_IndexOf_m15239(__this, ___item, method) (( int32_t (*) (List_1_t417 *, RaycastResult_t486 , const MethodInfo*))List_1_IndexOf_m15239_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m15240_gshared (List_1_t417 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m15240(__this, ___start, ___delta, method) (( void (*) (List_1_t417 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15240_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m15241_gshared (List_1_t417 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m15241(__this, ___index, method) (( void (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15241_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m15242_gshared (List_1_t417 * __this, int32_t ___index, RaycastResult_t486  ___item, const MethodInfo* method);
#define List_1_Insert_m15242(__this, ___index, ___item, method) (( void (*) (List_1_t417 *, int32_t, RaycastResult_t486 , const MethodInfo*))List_1_Insert_m15242_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m15243_gshared (List_1_t417 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m15243(__this, ___collection, method) (( void (*) (List_1_t417 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15243_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool List_1_Remove_m15244_gshared (List_1_t417 * __this, RaycastResult_t486  ___item, const MethodInfo* method);
#define List_1_Remove_m15244(__this, ___item, method) (( bool (*) (List_1_t417 *, RaycastResult_t486 , const MethodInfo*))List_1_Remove_m15244_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m15245_gshared (List_1_t417 * __this, Predicate_1_t2941 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m15245(__this, ___match, method) (( int32_t (*) (List_1_t417 *, Predicate_1_t2941 *, const MethodInfo*))List_1_RemoveAll_m15245_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m15246_gshared (List_1_t417 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m15246(__this, ___index, method) (( void (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_RemoveAt_m15246_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Reverse()
extern "C" void List_1_Reverse_m15247_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_Reverse_m15247(__this, method) (( void (*) (List_1_t417 *, const MethodInfo*))List_1_Reverse_m15247_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort()
extern "C" void List_1_Sort_m15248_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_Sort_m15248(__this, method) (( void (*) (List_1_t417 *, const MethodInfo*))List_1_Sort_m15248_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m3061_gshared (List_1_t417 * __this, Comparison_1_t454 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m3061(__this, ___comparison, method) (( void (*) (List_1_t417 *, Comparison_1_t454 *, const MethodInfo*))List_1_Sort_m3061_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::ToArray()
extern "C" RaycastResultU5BU5D_t2933* List_1_ToArray_m15249_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_ToArray_m15249(__this, method) (( RaycastResultU5BU5D_t2933* (*) (List_1_t417 *, const MethodInfo*))List_1_ToArray_m15249_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m15250_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m15250(__this, method) (( void (*) (List_1_t417 *, const MethodInfo*))List_1_TrimExcess_m15250_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m15251_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m15251(__this, method) (( int32_t (*) (List_1_t417 *, const MethodInfo*))List_1_get_Capacity_m15251_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m15252_gshared (List_1_t417 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m15252(__this, ___value, method) (( void (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15252_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t List_1_get_Count_m15253_gshared (List_1_t417 * __this, const MethodInfo* method);
#define List_1_get_Count_m15253(__this, method) (( int32_t (*) (List_1_t417 *, const MethodInfo*))List_1_get_Count_m15253_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t486  List_1_get_Item_m15254_gshared (List_1_t417 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m15254(__this, ___index, method) (( RaycastResult_t486  (*) (List_1_t417 *, int32_t, const MethodInfo*))List_1_get_Item_m15254_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m15255_gshared (List_1_t417 * __this, int32_t ___index, RaycastResult_t486  ___value, const MethodInfo* method);
#define List_1_set_Item_m15255(__this, ___index, ___value, method) (( void (*) (List_1_t417 *, int32_t, RaycastResult_t486 , const MethodInfo*))List_1_set_Item_m15255_gshared)(__this, ___index, ___value, method)
