﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t666;
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// UnityEngine.Canvas
struct  Canvas_t414  : public Behaviour_t250
{
};
struct Canvas_t414_StaticFields{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t666 * ___willRenderCanvases_2;
};
