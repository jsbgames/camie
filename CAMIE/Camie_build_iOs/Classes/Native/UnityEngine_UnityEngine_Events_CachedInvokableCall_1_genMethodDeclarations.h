﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t1103;
// UnityEngine.Object
struct Object_t164;
struct Object_t164_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t224;

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C" void CachedInvokableCall_1__ctor_m5240_gshared (CachedInvokableCall_1_t1103 * __this, Object_t164 * ___target, MethodInfo_t * ___theFunction, float ___argument, const MethodInfo* method);
#define CachedInvokableCall_1__ctor_m5240(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t1103 *, Object_t164 *, MethodInfo_t *, float, const MethodInfo*))CachedInvokableCall_1__ctor_m5240_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C" void CachedInvokableCall_1_Invoke_m21528_gshared (CachedInvokableCall_1_t1103 * __this, ObjectU5BU5D_t224* ___args, const MethodInfo* method);
#define CachedInvokableCall_1_Invoke_m21528(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t1103 *, ObjectU5BU5D_t224*, const MethodInfo*))CachedInvokableCall_1_Invoke_m21528_gshared)(__this, ___args, method)
