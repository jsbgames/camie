﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIWordWrapSizer
struct GUIWordWrapSizer_t822;
// UnityEngine.GUIStyle
struct GUIStyle_t302;
// UnityEngine.GUIContent
struct GUIContent_t301;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t409;

// System.Void UnityEngine.GUIWordWrapSizer::.ctor(UnityEngine.GUIStyle,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C" void GUIWordWrapSizer__ctor_m3917 (GUIWordWrapSizer_t822 * __this, GUIStyle_t302 * ____style, GUIContent_t301 * ____content, GUILayoutOptionU5BU5D_t409* ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIWordWrapSizer::CalcWidth()
extern "C" void GUIWordWrapSizer_CalcWidth_m3918 (GUIWordWrapSizer_t822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIWordWrapSizer::CalcHeight()
extern "C" void GUIWordWrapSizer_CalcHeight_m3919 (GUIWordWrapSizer_t822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
