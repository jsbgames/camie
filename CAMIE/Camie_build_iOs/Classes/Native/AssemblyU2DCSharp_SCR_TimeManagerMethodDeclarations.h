﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_TimeManager
struct SCR_TimeManager_t383;

// System.Void SCR_TimeManager::.ctor()
extern "C" void SCR_TimeManager__ctor_m1500 (SCR_TimeManager_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_TimeManager::Awake()
extern "C" void SCR_TimeManager_Awake_m1501 (SCR_TimeManager_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_TimeManager::Start()
extern "C" void SCR_TimeManager_Start_m1502 (SCR_TimeManager_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_TimeManager::OnLevelWasLoaded()
extern "C" void SCR_TimeManager_OnLevelWasLoaded_m1503 (SCR_TimeManager_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_TimeManager::Update()
extern "C" void SCR_TimeManager_Update_m1504 (SCR_TimeManager_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_TimeManager::SetTimer(System.Boolean)
extern "C" void SCR_TimeManager_SetTimer_m1505 (SCR_TimeManager_t383 * __this, bool ___turnOn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_TimeManager::Pause()
extern "C" void SCR_TimeManager_Pause_m1506 (SCR_TimeManager_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_TimeManager::get_IsPaused()
extern "C" bool SCR_TimeManager_get_IsPaused_m1507 (SCR_TimeManager_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_TimeManager::get_IsCounting()
extern "C" bool SCR_TimeManager_get_IsCounting_m1508 (SCR_TimeManager_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SCR_TimeManager::get_TimeSinceLevelStart()
extern "C" float SCR_TimeManager_get_TimeSinceLevelStart_m1509 (SCR_TimeManager_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
