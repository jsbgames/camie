﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Goutte
struct SCR_Goutte_t347;

// System.Void SCR_Goutte::.ctor()
extern "C" void SCR_Goutte__ctor_m1535 (SCR_Goutte_t347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Goutte::TriggerEnterEffect()
extern "C" void SCR_Goutte_TriggerEnterEffect_m1536 (SCR_Goutte_t347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Goutte::TriggerExitEffet()
extern "C" void SCR_Goutte_TriggerExitEffet_m1537 (SCR_Goutte_t347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
