﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct AxisTouchButton_t29;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t215;

// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::.ctor()
extern "C" void AxisTouchButton__ctor_m55 (AxisTouchButton_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnEnable()
extern "C" void AxisTouchButton_OnEnable_m56 (AxisTouchButton_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::FindPairedButton()
extern "C" void AxisTouchButton_FindPairedButton_m57 (AxisTouchButton_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnDisable()
extern "C" void AxisTouchButton_OnDisable_m58 (AxisTouchButton_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" void AxisTouchButton_OnPointerDown_m59 (AxisTouchButton_t29 * __this, PointerEventData_t215 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" void AxisTouchButton_OnPointerUp_m60 (AxisTouchButton_t29 * __this, PointerEventData_t215 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
