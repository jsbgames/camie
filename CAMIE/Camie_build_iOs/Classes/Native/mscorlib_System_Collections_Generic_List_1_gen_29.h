﻿#pragma once
#include <stdint.h>
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t150;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct  List_1_t1024  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::_items
	ParticleSystemU5BU5D_t150* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::_version
	int32_t ____version_3;
};
struct List_1_t1024_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::EmptyArray
	ParticleSystemU5BU5D_t150* ___EmptyArray_4;
};
