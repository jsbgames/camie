﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.LineRenderer
struct LineRenderer_t397;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityEngine.LineRenderer::SetWidth(System.Single,System.Single)
extern "C" void LineRenderer_SetWidth_m1827 (LineRenderer_t397 * __this, float ___start, float ___end, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetWidth(UnityEngine.LineRenderer,System.Single,System.Single)
extern "C" void LineRenderer_INTERNAL_CALL_SetWidth_m3758 (Object_t * __this /* static, unused */, LineRenderer_t397 * ___self, float ___start, float ___end, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::SetVertexCount(System.Int32)
extern "C" void LineRenderer_SetVertexCount_m1826 (LineRenderer_t397 * __this, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetVertexCount(UnityEngine.LineRenderer,System.Int32)
extern "C" void LineRenderer_INTERNAL_CALL_SetVertexCount_m3759 (Object_t * __this /* static, unused */, LineRenderer_t397 * ___self, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::SetPosition(System.Int32,UnityEngine.Vector3)
extern "C" void LineRenderer_SetPosition_m1828 (LineRenderer_t397 * __this, int32_t ___index, Vector3_t4  ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetPosition(UnityEngine.LineRenderer,System.Int32,UnityEngine.Vector3&)
extern "C" void LineRenderer_INTERNAL_CALL_SetPosition_m3760 (Object_t * __this /* static, unused */, LineRenderer_t397 * ___self, int32_t ___index, Vector3_t4 * ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
