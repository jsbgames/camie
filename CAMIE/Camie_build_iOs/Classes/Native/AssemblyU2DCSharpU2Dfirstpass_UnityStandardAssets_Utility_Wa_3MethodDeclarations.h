﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.WaypointProgressTracker
struct WaypointProgressTracker_t213;
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_0.h"

// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::.ctor()
extern "C" void WaypointProgressTracker__ctor_m581 (WaypointProgressTracker_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_targetPoint()
extern "C" RoutePoint_t209  WaypointProgressTracker_get_targetPoint_m582 (WaypointProgressTracker_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_targetPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern "C" void WaypointProgressTracker_set_targetPoint_m583 (WaypointProgressTracker_t213 * __this, RoutePoint_t209  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_speedPoint()
extern "C" RoutePoint_t209  WaypointProgressTracker_get_speedPoint_m584 (WaypointProgressTracker_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_speedPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern "C" void WaypointProgressTracker_set_speedPoint_m585 (WaypointProgressTracker_t213 * __this, RoutePoint_t209  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_progressPoint()
extern "C" RoutePoint_t209  WaypointProgressTracker_get_progressPoint_m586 (WaypointProgressTracker_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_progressPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern "C" void WaypointProgressTracker_set_progressPoint_m587 (WaypointProgressTracker_t213 * __this, RoutePoint_t209  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Start()
extern "C" void WaypointProgressTracker_Start_m588 (WaypointProgressTracker_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Reset()
extern "C" void WaypointProgressTracker_Reset_m589 (WaypointProgressTracker_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Update()
extern "C" void WaypointProgressTracker_Update_m590 (WaypointProgressTracker_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::OnDrawGizmos()
extern "C" void WaypointProgressTracker_OnDrawGizmos_m591 (WaypointProgressTracker_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
