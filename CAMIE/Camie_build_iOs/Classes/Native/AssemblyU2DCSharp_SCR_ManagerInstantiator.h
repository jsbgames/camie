﻿#pragma once
#include <stdint.h>
// SCR_ManagerComponents
struct SCR_ManagerComponents_t380;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_ManagerInstantiator
struct  SCR_ManagerInstantiator_t385  : public MonoBehaviour_t3
{
	// SCR_ManagerComponents SCR_ManagerInstantiator::manager
	SCR_ManagerComponents_t380 * ___manager_2;
};
