﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.FPSCounter
struct FPSCounter_t182;

// System.Void UnityStandardAssets.Utility.FPSCounter::.ctor()
extern "C" void FPSCounter__ctor_m491 (FPSCounter_t182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FPSCounter::Start()
extern "C" void FPSCounter_Start_m492 (FPSCounter_t182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FPSCounter::Update()
extern "C" void FPSCounter_Update_m493 (FPSCounter_t182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
