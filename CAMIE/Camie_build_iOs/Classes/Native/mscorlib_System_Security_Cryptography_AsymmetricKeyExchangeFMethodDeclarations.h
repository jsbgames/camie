﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
struct AsymmetricKeyExchangeFormatter_t2068;
// System.Byte[]
struct ByteU5BU5D_t850;

// System.Void System.Security.Cryptography.AsymmetricKeyExchangeFormatter::.ctor()
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m11006 (AsymmetricKeyExchangeFormatter_t2068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.AsymmetricKeyExchangeFormatter::CreateKeyExchange(System.Byte[])
