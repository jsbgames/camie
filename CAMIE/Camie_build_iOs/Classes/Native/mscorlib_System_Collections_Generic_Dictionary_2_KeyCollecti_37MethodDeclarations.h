﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>
struct Enumerator_t3410;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t3406;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21863_gshared (Enumerator_t3410 * __this, Dictionary_2_t3406 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m21863(__this, ___host, method) (( void (*) (Enumerator_t3410 *, Dictionary_2_t3406 *, const MethodInfo*))Enumerator__ctor_m21863_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21864_gshared (Enumerator_t3410 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21864(__this, method) (( Object_t * (*) (Enumerator_t3410 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21864_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m21865_gshared (Enumerator_t3410 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21865(__this, method) (( void (*) (Enumerator_t3410 *, const MethodInfo*))Enumerator_Dispose_m21865_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21866_gshared (Enumerator_t3410 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21866(__this, method) (( bool (*) (Enumerator_t3410 *, const MethodInfo*))Enumerator_MoveNext_m21866_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m21867_gshared (Enumerator_t3410 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21867(__this, method) (( Object_t * (*) (Enumerator_t3410 *, const MethodInfo*))Enumerator_get_Current_m21867_gshared)(__this, method)
