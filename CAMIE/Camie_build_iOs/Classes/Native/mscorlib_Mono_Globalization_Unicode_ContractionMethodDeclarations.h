﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.Contraction
struct Contraction_t1694;
// System.Char[]
struct CharU5BU5D_t554;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t850;

// System.Void Mono.Globalization.Unicode.Contraction::.ctor(System.Char[],System.String,System.Byte[])
extern "C" void Contraction__ctor_m8439 (Contraction_t1694 * __this, CharU5BU5D_t554* ___source, String_t* ___replacement, ByteU5BU5D_t850* ___sortkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
