﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t1642;
// System.Byte[]
struct ByteU5BU5D_t850;

// System.Void System.Security.Cryptography.HMACSHA1::.ctor()
extern "C" void HMACSHA1__ctor_m11082 (HMACSHA1_t1642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA1::.ctor(System.Byte[])
extern "C" void HMACSHA1__ctor_m11083 (HMACSHA1_t1642 * __this, ByteU5BU5D_t850* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
