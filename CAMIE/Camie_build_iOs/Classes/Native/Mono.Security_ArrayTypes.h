﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct  BigIntegerU5BU5D_t1636  : public Array_t
{
};
struct BigIntegerU5BU5D_t1636_StaticFields{
};
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
struct  ClientCertificateTypeU5BU5D_t1611  : public Array_t
{
};
