﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMAC.h"
// System.Security.Cryptography.HMACSHA384
struct  HMACSHA384_t2078  : public HMAC_t1643
{
	// System.Boolean System.Security.Cryptography.HMACSHA384::legacy
	bool ___legacy_11;
};
struct HMACSHA384_t2078_StaticFields{
	// System.Boolean System.Security.Cryptography.HMACSHA384::legacy_mode
	bool ___legacy_mode_10;
};
