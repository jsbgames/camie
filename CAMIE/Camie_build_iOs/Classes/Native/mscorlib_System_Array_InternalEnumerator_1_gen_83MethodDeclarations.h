﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>
struct InternalEnumerator_1_t3440;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"

// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22128_gshared (InternalEnumerator_1_t3440 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22128(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3440 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22128_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22129_gshared (InternalEnumerator_1_t3440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22129(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3440 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22129_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22130_gshared (InternalEnumerator_1_t3440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22130(__this, method) (( void (*) (InternalEnumerator_1_t3440 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22130_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22131_gshared (InternalEnumerator_1_t3440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22131(__this, method) (( bool (*) (InternalEnumerator_1_t3440 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22131_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern "C" Mark_t1382  InternalEnumerator_1_get_Current_m22132_gshared (InternalEnumerator_1_t3440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22132(__this, method) (( Mark_t1382  (*) (InternalEnumerator_1_t3440 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22132_gshared)(__this, method)
