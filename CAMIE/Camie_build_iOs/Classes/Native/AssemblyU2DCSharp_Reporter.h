﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Reporter/Sample>
struct List_1_t297;
// System.Collections.Generic.List`1<Reporter/Log>
struct List_1_t298;
// MultiKeyDictionary`3<System.String,System.String,Reporter/Log>
struct MultiKeyDictionary_3_t299;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t300;
// System.String
struct String_t;
// Images
struct Images_t288;
// UnityEngine.GUIContent
struct GUIContent_t301;
// UnityEngine.GUIStyle
struct GUIStyle_t302;
// UnityEngine.GUISkin
struct GUISkin_t287;
// System.String[]
struct StringU5BU5D_t243;
// Reporter/Log
struct Log_t291;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t303;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Reporter/ReportView
#include "AssemblyU2DCSharp_Reporter_ReportView.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Reporter
struct  Reporter_t295  : public MonoBehaviour_t3
{
	// System.Collections.Generic.List`1<Reporter/Sample> Reporter::samples
	List_1_t297 * ___samples_2;
	// System.Collections.Generic.List`1<Reporter/Log> Reporter::logs
	List_1_t298 * ___logs_3;
	// System.Collections.Generic.List`1<Reporter/Log> Reporter::collapsedLogs
	List_1_t298 * ___collapsedLogs_4;
	// System.Collections.Generic.List`1<Reporter/Log> Reporter::currentLog
	List_1_t298 * ___currentLog_5;
	// MultiKeyDictionary`3<System.String,System.String,Reporter/Log> Reporter::logsDic
	MultiKeyDictionary_3_t299 * ___logsDic_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Reporter::cachedString
	Dictionary_2_t300 * ___cachedString_7;
	// System.Boolean Reporter::show
	bool ___show_8;
	// System.Boolean Reporter::collapse
	bool ___collapse_9;
	// System.Boolean Reporter::clearOnNewSceneLoaded
	bool ___clearOnNewSceneLoaded_10;
	// System.Boolean Reporter::showTime
	bool ___showTime_11;
	// System.Boolean Reporter::showScene
	bool ___showScene_12;
	// System.Boolean Reporter::showMemory
	bool ___showMemory_13;
	// System.Boolean Reporter::showFps
	bool ___showFps_14;
	// System.Boolean Reporter::showGraph
	bool ___showGraph_15;
	// System.Boolean Reporter::showLog
	bool ___showLog_16;
	// System.Boolean Reporter::showWarning
	bool ___showWarning_17;
	// System.Boolean Reporter::showError
	bool ___showError_18;
	// System.Int32 Reporter::numOfLogs
	int32_t ___numOfLogs_19;
	// System.Int32 Reporter::numOfLogsWarning
	int32_t ___numOfLogsWarning_20;
	// System.Int32 Reporter::numOfLogsError
	int32_t ___numOfLogsError_21;
	// System.Int32 Reporter::numOfCollapsedLogs
	int32_t ___numOfCollapsedLogs_22;
	// System.Int32 Reporter::numOfCollapsedLogsWarning
	int32_t ___numOfCollapsedLogsWarning_23;
	// System.Int32 Reporter::numOfCollapsedLogsError
	int32_t ___numOfCollapsedLogsError_24;
	// System.Boolean Reporter::showClearOnNewSceneLoadedButton
	bool ___showClearOnNewSceneLoadedButton_25;
	// System.Boolean Reporter::showTimeButton
	bool ___showTimeButton_26;
	// System.Boolean Reporter::showSceneButton
	bool ___showSceneButton_27;
	// System.Boolean Reporter::showMemButton
	bool ___showMemButton_28;
	// System.Boolean Reporter::showFpsButton
	bool ___showFpsButton_29;
	// System.Boolean Reporter::showSearchText
	bool ___showSearchText_30;
	// System.String Reporter::buildDate
	String_t* ___buildDate_31;
	// System.String Reporter::logDate
	String_t* ___logDate_32;
	// System.Single Reporter::logsMemUsage
	float ___logsMemUsage_33;
	// System.Single Reporter::graphMemUsage
	float ___graphMemUsage_34;
	// System.Single Reporter::gcTotalMemory
	float ___gcTotalMemory_35;
	// System.String Reporter::UserData
	String_t* ___UserData_36;
	// System.Single Reporter::fps
	float ___fps_37;
	// System.String Reporter::fpsText
	String_t* ___fpsText_38;
	// Reporter/ReportView Reporter::currentView
	int32_t ___currentView_39;
	// Images Reporter::images
	Images_t288 * ___images_41;
	// UnityEngine.GUIContent Reporter::clearContent
	GUIContent_t301 * ___clearContent_42;
	// UnityEngine.GUIContent Reporter::collapseContent
	GUIContent_t301 * ___collapseContent_43;
	// UnityEngine.GUIContent Reporter::clearOnNewSceneContent
	GUIContent_t301 * ___clearOnNewSceneContent_44;
	// UnityEngine.GUIContent Reporter::showTimeContent
	GUIContent_t301 * ___showTimeContent_45;
	// UnityEngine.GUIContent Reporter::showSceneContent
	GUIContent_t301 * ___showSceneContent_46;
	// UnityEngine.GUIContent Reporter::userContent
	GUIContent_t301 * ___userContent_47;
	// UnityEngine.GUIContent Reporter::showMemoryContent
	GUIContent_t301 * ___showMemoryContent_48;
	// UnityEngine.GUIContent Reporter::softwareContent
	GUIContent_t301 * ___softwareContent_49;
	// UnityEngine.GUIContent Reporter::dateContent
	GUIContent_t301 * ___dateContent_50;
	// UnityEngine.GUIContent Reporter::showFpsContent
	GUIContent_t301 * ___showFpsContent_51;
	// UnityEngine.GUIContent Reporter::graphContent
	GUIContent_t301 * ___graphContent_52;
	// UnityEngine.GUIContent Reporter::infoContent
	GUIContent_t301 * ___infoContent_53;
	// UnityEngine.GUIContent Reporter::searchContent
	GUIContent_t301 * ___searchContent_54;
	// UnityEngine.GUIContent Reporter::closeContent
	GUIContent_t301 * ___closeContent_55;
	// UnityEngine.GUIContent Reporter::buildFromContent
	GUIContent_t301 * ___buildFromContent_56;
	// UnityEngine.GUIContent Reporter::systemInfoContent
	GUIContent_t301 * ___systemInfoContent_57;
	// UnityEngine.GUIContent Reporter::graphicsInfoContent
	GUIContent_t301 * ___graphicsInfoContent_58;
	// UnityEngine.GUIContent Reporter::backContent
	GUIContent_t301 * ___backContent_59;
	// UnityEngine.GUIContent Reporter::cameraContent
	GUIContent_t301 * ___cameraContent_60;
	// UnityEngine.GUIContent Reporter::logContent
	GUIContent_t301 * ___logContent_61;
	// UnityEngine.GUIContent Reporter::warningContent
	GUIContent_t301 * ___warningContent_62;
	// UnityEngine.GUIContent Reporter::errorContent
	GUIContent_t301 * ___errorContent_63;
	// UnityEngine.GUIStyle Reporter::barStyle
	GUIStyle_t302 * ___barStyle_64;
	// UnityEngine.GUIStyle Reporter::buttonActiveStyle
	GUIStyle_t302 * ___buttonActiveStyle_65;
	// UnityEngine.GUIStyle Reporter::nonStyle
	GUIStyle_t302 * ___nonStyle_66;
	// UnityEngine.GUIStyle Reporter::lowerLeftFontStyle
	GUIStyle_t302 * ___lowerLeftFontStyle_67;
	// UnityEngine.GUIStyle Reporter::backStyle
	GUIStyle_t302 * ___backStyle_68;
	// UnityEngine.GUIStyle Reporter::evenLogStyle
	GUIStyle_t302 * ___evenLogStyle_69;
	// UnityEngine.GUIStyle Reporter::oddLogStyle
	GUIStyle_t302 * ___oddLogStyle_70;
	// UnityEngine.GUIStyle Reporter::logButtonStyle
	GUIStyle_t302 * ___logButtonStyle_71;
	// UnityEngine.GUIStyle Reporter::selectedLogStyle
	GUIStyle_t302 * ___selectedLogStyle_72;
	// UnityEngine.GUIStyle Reporter::selectedLogFontStyle
	GUIStyle_t302 * ___selectedLogFontStyle_73;
	// UnityEngine.GUIStyle Reporter::stackLabelStyle
	GUIStyle_t302 * ___stackLabelStyle_74;
	// UnityEngine.GUIStyle Reporter::scrollerStyle
	GUIStyle_t302 * ___scrollerStyle_75;
	// UnityEngine.GUIStyle Reporter::searchStyle
	GUIStyle_t302 * ___searchStyle_76;
	// UnityEngine.GUIStyle Reporter::sliderBackStyle
	GUIStyle_t302 * ___sliderBackStyle_77;
	// UnityEngine.GUIStyle Reporter::sliderThumbStyle
	GUIStyle_t302 * ___sliderThumbStyle_78;
	// UnityEngine.GUISkin Reporter::toolbarScrollerSkin
	GUISkin_t287 * ___toolbarScrollerSkin_79;
	// UnityEngine.GUISkin Reporter::logScrollerSkin
	GUISkin_t287 * ___logScrollerSkin_80;
	// UnityEngine.GUISkin Reporter::graphScrollerSkin
	GUISkin_t287 * ___graphScrollerSkin_81;
	// UnityEngine.Vector2 Reporter::size
	Vector2_t6  ___size_82;
	// System.Single Reporter::maxSize
	float ___maxSize_83;
	// System.Int32 Reporter::numOfCircleToShow
	int32_t ___numOfCircleToShow_84;
	// System.String[] Reporter::scenes
	StringU5BU5D_t243* ___scenes_85;
	// System.String Reporter::currentScene
	String_t* ___currentScene_86;
	// System.String Reporter::filterText
	String_t* ___filterText_87;
	// System.String Reporter::deviceModel
	String_t* ___deviceModel_88;
	// System.String Reporter::deviceType
	String_t* ___deviceType_89;
	// System.String Reporter::deviceName
	String_t* ___deviceName_90;
	// System.String Reporter::graphicsMemorySize
	String_t* ___graphicsMemorySize_91;
	// System.String Reporter::maxTextureSize
	String_t* ___maxTextureSize_92;
	// System.String Reporter::systemMemorySize
	String_t* ___systemMemorySize_93;
	// System.Boolean Reporter::Initialized
	bool ___Initialized_94;
	// UnityEngine.Rect Reporter::screenRect
	Rect_t304  ___screenRect_95;
	// UnityEngine.Rect Reporter::toolBarRect
	Rect_t304  ___toolBarRect_96;
	// UnityEngine.Rect Reporter::logsRect
	Rect_t304  ___logsRect_97;
	// UnityEngine.Rect Reporter::stackRect
	Rect_t304  ___stackRect_98;
	// UnityEngine.Rect Reporter::graphRect
	Rect_t304  ___graphRect_99;
	// UnityEngine.Rect Reporter::graphMinRect
	Rect_t304  ___graphMinRect_100;
	// UnityEngine.Rect Reporter::graphMaxRect
	Rect_t304  ___graphMaxRect_101;
	// UnityEngine.Rect Reporter::buttomRect
	Rect_t304  ___buttomRect_102;
	// UnityEngine.Vector2 Reporter::stackRectTopLeft
	Vector2_t6  ___stackRectTopLeft_103;
	// UnityEngine.Rect Reporter::detailRect
	Rect_t304  ___detailRect_104;
	// UnityEngine.Vector2 Reporter::scrollPosition
	Vector2_t6  ___scrollPosition_105;
	// UnityEngine.Vector2 Reporter::scrollPosition2
	Vector2_t6  ___scrollPosition2_106;
	// UnityEngine.Vector2 Reporter::toolbarScrollPosition
	Vector2_t6  ___toolbarScrollPosition_107;
	// Reporter/Log Reporter::selectedLog
	Log_t291 * ___selectedLog_108;
	// System.Single Reporter::toolbarOldDrag
	float ___toolbarOldDrag_109;
	// System.Single Reporter::oldDrag
	float ___oldDrag_110;
	// System.Single Reporter::oldDrag2
	float ___oldDrag2_111;
	// System.Single Reporter::oldDrag3
	float ___oldDrag3_112;
	// System.Int32 Reporter::startIndex
	int32_t ___startIndex_113;
	// UnityEngine.Rect Reporter::countRect
	Rect_t304  ___countRect_114;
	// UnityEngine.Rect Reporter::timeRect
	Rect_t304  ___timeRect_115;
	// UnityEngine.Rect Reporter::timeLabelRect
	Rect_t304  ___timeLabelRect_116;
	// UnityEngine.Rect Reporter::sceneRect
	Rect_t304  ___sceneRect_117;
	// UnityEngine.Rect Reporter::sceneLabelRect
	Rect_t304  ___sceneLabelRect_118;
	// UnityEngine.Rect Reporter::memoryRect
	Rect_t304  ___memoryRect_119;
	// UnityEngine.Rect Reporter::memoryLabelRect
	Rect_t304  ___memoryLabelRect_120;
	// UnityEngine.Rect Reporter::fpsRect
	Rect_t304  ___fpsRect_121;
	// UnityEngine.Rect Reporter::fpsLabelRect
	Rect_t304  ___fpsLabelRect_122;
	// UnityEngine.GUIContent Reporter::tempContent
	GUIContent_t301 * ___tempContent_123;
	// UnityEngine.Vector2 Reporter::infoScrollPosition
	Vector2_t6  ___infoScrollPosition_124;
	// UnityEngine.Vector2 Reporter::oldInfoDrag
	Vector2_t6  ___oldInfoDrag_125;
	// UnityEngine.Rect Reporter::tempRect
	Rect_t304  ___tempRect_126;
	// System.Single Reporter::graphSize
	float ___graphSize_127;
	// System.Int32 Reporter::startFrame
	int32_t ___startFrame_128;
	// System.Int32 Reporter::currentFrame
	int32_t ___currentFrame_129;
	// UnityEngine.Vector3 Reporter::tempVector1
	Vector3_t4  ___tempVector1_130;
	// UnityEngine.Vector3 Reporter::tempVector2
	Vector3_t4  ___tempVector2_131;
	// UnityEngine.Vector2 Reporter::graphScrollerPos
	Vector2_t6  ___graphScrollerPos_132;
	// System.Single Reporter::maxFpsValue
	float ___maxFpsValue_133;
	// System.Single Reporter::minFpsValue
	float ___minFpsValue_134;
	// System.Single Reporter::maxMemoryValue
	float ___maxMemoryValue_135;
	// System.Single Reporter::minMemoryValue
	float ___minMemoryValue_136;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Reporter::gestureDetector
	List_1_t303 * ___gestureDetector_137;
	// UnityEngine.Vector2 Reporter::gestureSum
	Vector2_t6  ___gestureSum_138;
	// System.Single Reporter::gestureLength
	float ___gestureLength_139;
	// System.Int32 Reporter::gestureCount
	int32_t ___gestureCount_140;
	// System.Single Reporter::lastClickTime
	float ___lastClickTime_141;
	// UnityEngine.Vector2 Reporter::startPos
	Vector2_t6  ___startPos_142;
	// UnityEngine.Vector2 Reporter::downPos
	Vector2_t6  ___downPos_143;
	// UnityEngine.Vector2 Reporter::mousePosition
	Vector2_t6  ___mousePosition_144;
	// System.Single Reporter::lastUpdate
	float ___lastUpdate_145;
	// System.Single Reporter::lastUpdate2
	float ___lastUpdate2_146;
	// UnityEngine.Rect Reporter::temp
	Rect_t304  ___temp_147;
	// System.Collections.Generic.List`1<Reporter/Log> Reporter::threadedLogs
	List_1_t298 * ___threadedLogs_148;
};
struct Reporter_t295_StaticFields{
	// System.Boolean Reporter::created
	bool ___created_40;
};
