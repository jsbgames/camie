﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACRIPEMD160
struct HMACRIPEMD160_t2076;
// System.Byte[]
struct ByteU5BU5D_t850;

// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor()
extern "C" void HMACRIPEMD160__ctor_m11080 (HMACRIPEMD160_t2076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor(System.Byte[])
extern "C" void HMACRIPEMD160__ctor_m11081 (HMACRIPEMD160_t2076 * __this, ByteU5BU5D_t850* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
