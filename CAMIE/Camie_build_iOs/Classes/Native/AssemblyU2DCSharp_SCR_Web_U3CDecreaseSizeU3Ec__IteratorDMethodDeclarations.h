﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Web/<DecreaseSize>c__IteratorD
struct U3CDecreaseSizeU3Ec__IteratorD_t399;
// System.Object
struct Object_t;

// System.Void SCR_Web/<DecreaseSize>c__IteratorD::.ctor()
extern "C" void U3CDecreaseSizeU3Ec__IteratorD__ctor_m1560 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Web/<DecreaseSize>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDecreaseSizeU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1561 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Web/<DecreaseSize>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDecreaseSizeU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1562 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Web/<DecreaseSize>c__IteratorD::MoveNext()
extern "C" bool U3CDecreaseSizeU3Ec__IteratorD_MoveNext_m1563 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Web/<DecreaseSize>c__IteratorD::Dispose()
extern "C" void U3CDecreaseSizeU3Ec__IteratorD_Dispose_m1564 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Web/<DecreaseSize>c__IteratorD::Reset()
extern "C" void U3CDecreaseSizeU3Ec__IteratorD_Reset_m1565 (U3CDecreaseSizeU3Ec__IteratorD_t399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
