﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct KeyValuePair_2_t2832;
// System.String
struct String_t;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_t30;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m13762(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2832 *, String_t*, VirtualAxis_t30 *, const MethodInfo*))KeyValuePair_2__ctor_m13651_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Key()
#define KeyValuePair_2_get_Key_m13763(__this, method) (( String_t* (*) (KeyValuePair_2_t2832 *, const MethodInfo*))KeyValuePair_2_get_Key_m13652_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m13764(__this, ___value, method) (( void (*) (KeyValuePair_2_t2832 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m13653_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Value()
#define KeyValuePair_2_get_Value_m13765(__this, method) (( VirtualAxis_t30 * (*) (KeyValuePair_2_t2832 *, const MethodInfo*))KeyValuePair_2_get_Value_m13654_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m13766(__this, ___value, method) (( void (*) (KeyValuePair_2_t2832 *, VirtualAxis_t30 *, const MethodInfo*))KeyValuePair_2_set_Value_m13655_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ToString()
#define KeyValuePair_2_ToString_m13767(__this, method) (( String_t* (*) (KeyValuePair_2_t2832 *, const MethodInfo*))KeyValuePair_2_ToString_m13656_gshared)(__this, method)
