﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t55;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Material>
struct  Comparison_1_t2878  : public MulticastDelegate_t549
{
};
