﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>
struct ReadOnlyCollection_1_t2896;
// Reporter/Sample
struct Sample_t290;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Reporter/Sample>
struct IList_1_t2895;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// Reporter/Sample[]
struct SampleU5BU5D_t2894;
// System.Collections.Generic.IEnumerator`1<Reporter/Sample>
struct IEnumerator_1_t3547;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m14624(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2896 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m13989_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14625(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2896 *, Sample_t290 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m13990_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14626(__this, method) (( void (*) (ReadOnlyCollection_1_t2896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m13991_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14627(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2896 *, int32_t, Sample_t290 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m13992_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14628(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2896 *, Sample_t290 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m13993_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14629(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2896 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m13994_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14630(__this, ___index, method) (( Sample_t290 * (*) (ReadOnlyCollection_1_t2896 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m13995_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14631(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2896 *, int32_t, Sample_t290 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m13996_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14632(__this, method) (( bool (*) (ReadOnlyCollection_1_t2896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13997_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14633(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2896 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m13998_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14634(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m13999_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m14635(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2896 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m14000_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m14636(__this, method) (( void (*) (ReadOnlyCollection_1_t2896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m14001_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m14637(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2896 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m14002_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14638(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2896 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14003_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m14639(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2896 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m14004_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m14640(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2896 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m14005_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14641(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2896 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14006_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14642(__this, method) (( bool (*) (ReadOnlyCollection_1_t2896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14007_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14643(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14008_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14644(__this, method) (( bool (*) (ReadOnlyCollection_1_t2896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14009_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14645(__this, method) (( bool (*) (ReadOnlyCollection_1_t2896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14010_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m14646(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2896 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m14011_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m14647(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2896 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m14012_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::Contains(T)
#define ReadOnlyCollection_1_Contains_m14648(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2896 *, Sample_t290 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m14013_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m14649(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2896 *, SampleU5BU5D_t2894*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m14014_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m14650(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2896 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m14015_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m14651(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2896 *, Sample_t290 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m14016_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::get_Count()
#define ReadOnlyCollection_1_get_Count_m14652(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2896 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m14017_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m14653(__this, ___index, method) (( Sample_t290 * (*) (ReadOnlyCollection_1_t2896 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m14018_gshared)(__this, ___index, method)
