﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MenuSceneLoader
struct MenuSceneLoader_t311;

// System.Void MenuSceneLoader::.ctor()
extern "C" void MenuSceneLoader__ctor_m1149 (MenuSceneLoader_t311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuSceneLoader::Awake()
extern "C" void MenuSceneLoader_Awake_m1150 (MenuSceneLoader_t311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
