﻿#pragma once
#include <stdint.h>
// Reporter
struct Reporter_t295;
// UnityEngine.GUIStyle
struct GUIStyle_t302;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// TestReporter
struct  TestReporter_t308  : public MonoBehaviour_t3
{
	// System.Int32 TestReporter::logTestCount
	int32_t ___logTestCount_2;
	// System.Int32 TestReporter::threadLogTestCount
	int32_t ___threadLogTestCount_3;
	// System.Boolean TestReporter::logEverySecond
	bool ___logEverySecond_4;
	// System.Int32 TestReporter::currentLogTestCount
	int32_t ___currentLogTestCount_5;
	// Reporter TestReporter::reporter
	Reporter_t295 * ___reporter_6;
	// UnityEngine.GUIStyle TestReporter::style
	GUIStyle_t302 * ___style_7;
	// UnityEngine.Rect TestReporter::rect1
	Rect_t304  ___rect1_8;
	// UnityEngine.Rect TestReporter::rect2
	Rect_t304  ___rect2_9;
	// UnityEngine.Rect TestReporter::rect3
	Rect_t304  ___rect3_10;
	// UnityEngine.Rect TestReporter::rect4
	Rect_t304  ___rect4_11;
	// UnityEngine.Rect TestReporter::rect5
	Rect_t304  ___rect5_12;
	// UnityEngine.Rect TestReporter::rect6
	Rect_t304  ___rect6_13;
	// System.Single TestReporter::elapsed
	float ___elapsed_14;
};
