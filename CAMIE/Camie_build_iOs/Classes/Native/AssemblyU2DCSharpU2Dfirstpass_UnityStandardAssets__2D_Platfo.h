﻿#pragma once
#include <stdint.h>
// UnityStandardAssets._2D.PlatformerCharacter2D
struct PlatformerCharacter2D_t7;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityStandardAssets._2D.Platformer2DUserControl
struct  Platformer2DUserControl_t8  : public MonoBehaviour_t3
{
	// UnityStandardAssets._2D.PlatformerCharacter2D UnityStandardAssets._2D.Platformer2DUserControl::m_Character
	PlatformerCharacter2D_t7 * ___m_Character_2;
	// System.Boolean UnityStandardAssets._2D.Platformer2DUserControl::m_Jump
	bool ___m_Jump_3;
};
