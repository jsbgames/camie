﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.X509.X509Crl
struct X509Crl_t1435;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t1460;
// System.Byte[]
struct ByteU5BU5D_t850;
// System.String
struct String_t;
// Mono.Security.X509.X509Crl/X509CrlEntry
struct X509CrlEntry_t1437;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1321;
// System.Security.Cryptography.DSA
struct DSA_t1431;
// System.Security.Cryptography.RSA
struct RSA_t1432;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1310;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void Mono.Security.X509.X509Crl::.ctor(System.Byte[])
extern "C" void X509Crl__ctor_m6930 (X509Crl_t1435 * __this, ByteU5BU5D_t850* ___crl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Crl::Parse(System.Byte[])
extern "C" void X509Crl_Parse_m6931 (X509Crl_t1435 * __this, ByteU5BU5D_t850* ___crl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl::get_Extensions()
extern "C" X509ExtensionCollection_t1460 * X509Crl_get_Extensions_m6480 (X509Crl_t1435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl::get_Hash()
extern "C" ByteU5BU5D_t850* X509Crl_get_Hash_m6932 (X509Crl_t1435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::get_IssuerName()
extern "C" String_t* X509Crl_get_IssuerName_m6488 (X509Crl_t1435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509Crl::get_NextUpdate()
extern "C" DateTime_t406  X509Crl_get_NextUpdate_m6486 (X509Crl_t1435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::Compare(System.Byte[],System.Byte[])
extern "C" bool X509Crl_Compare_m6933 (X509Crl_t1435 * __this, ByteU5BU5D_t850* ___array1, ByteU5BU5D_t850* ___array2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(Mono.Security.X509.X509Certificate)
extern "C" X509CrlEntry_t1437 * X509Crl_GetCrlEntry_m6484 (X509Crl_t1435 * __this, X509Certificate_t1321 * ___x509, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(System.Byte[])
extern "C" X509CrlEntry_t1437 * X509Crl_GetCrlEntry_m6934 (X509Crl_t1435 * __this, ByteU5BU5D_t850* ___serialNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::GetHashName()
extern "C" String_t* X509Crl_GetHashName_m6935 (X509Crl_t1435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.DSA)
extern "C" bool X509Crl_VerifySignature_m6936 (X509Crl_t1435 * __this, DSA_t1431 * ___dsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.RSA)
extern "C" bool X509Crl_VerifySignature_m6937 (X509Crl_t1435 * __this, RSA_t1432 * ___rsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" bool X509Crl_VerifySignature_m6483 (X509Crl_t1435 * __this, AsymmetricAlgorithm_t1310 * ___aa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
