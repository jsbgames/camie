﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Reporter/Sample>
struct List_1_t297;
// System.Object
struct Object_t;
// Reporter/Sample
struct Sample_t290;
// System.Collections.Generic.IEnumerable`1<Reporter/Sample>
struct IEnumerable_1_t3546;
// System.Collections.Generic.IEnumerator`1<Reporter/Sample>
struct IEnumerator_1_t3547;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<Reporter/Sample>
struct ICollection_1_t3548;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Sample>
struct ReadOnlyCollection_1_t2896;
// Reporter/Sample[]
struct SampleU5BU5D_t2894;
// System.Predicate`1<Reporter/Sample>
struct Predicate_1_t2897;
// System.Comparison`1<Reporter/Sample>
struct Comparison_1_t2899;
// System.Collections.Generic.List`1/Enumerator<Reporter/Sample>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_8.h"

// System.Void System.Collections.Generic.List`1<Reporter/Sample>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m14575(__this, method) (( void (*) (List_1_t297 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m14576(__this, ___collection, method) (( void (*) (List_1_t297 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::.ctor(System.Int32)
#define List_1__ctor_m1592(__this, ___capacity, method) (( void (*) (List_1_t297 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::.cctor()
#define List_1__cctor_m14577(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14578(__this, method) (( Object_t* (*) (List_1_t297 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m14579(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t297 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m14580(__this, method) (( Object_t * (*) (List_1_t297 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m14581(__this, ___item, method) (( int32_t (*) (List_1_t297 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m14582(__this, ___item, method) (( bool (*) (List_1_t297 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m14583(__this, ___item, method) (( int32_t (*) (List_1_t297 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m14584(__this, ___index, ___item, method) (( void (*) (List_1_t297 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m14585(__this, ___item, method) (( void (*) (List_1_t297 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14586(__this, method) (( bool (*) (List_1_t297 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14587(__this, method) (( bool (*) (List_1_t297 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m14588(__this, method) (( Object_t * (*) (List_1_t297 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m14589(__this, method) (( bool (*) (List_1_t297 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m14590(__this, method) (( bool (*) (List_1_t297 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m14591(__this, ___index, method) (( Object_t * (*) (List_1_t297 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m14592(__this, ___index, ___value, method) (( void (*) (List_1_t297 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::Add(T)
#define List_1_Add_m14593(__this, ___item, method) (( void (*) (List_1_t297 *, Sample_t290 *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m14594(__this, ___newCount, method) (( void (*) (List_1_t297 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m14595(__this, ___collection, method) (( void (*) (List_1_t297 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m14596(__this, ___enumerable, method) (( void (*) (List_1_t297 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m14597(__this, ___collection, method) (( void (*) (List_1_t297 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Reporter/Sample>::AsReadOnly()
#define List_1_AsReadOnly_m14598(__this, method) (( ReadOnlyCollection_1_t2896 * (*) (List_1_t297 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::Clear()
#define List_1_Clear_m14599(__this, method) (( void (*) (List_1_t297 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Sample>::Contains(T)
#define List_1_Contains_m14600(__this, ___item, method) (( bool (*) (List_1_t297 *, Sample_t290 *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m14601(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t297 *, SampleU5BU5D_t2894*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Reporter/Sample>::Find(System.Predicate`1<T>)
#define List_1_Find_m14602(__this, ___match, method) (( Sample_t290 * (*) (List_1_t297 *, Predicate_1_t2897 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m14603(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2897 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m14604(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t297 *, int32_t, int32_t, Predicate_1_t2897 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Reporter/Sample>::GetEnumerator()
#define List_1_GetEnumerator_m14605(__this, method) (( Enumerator_t2898  (*) (List_1_t297 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::IndexOf(T)
#define List_1_IndexOf_m14606(__this, ___item, method) (( int32_t (*) (List_1_t297 *, Sample_t290 *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m14607(__this, ___start, ___delta, method) (( void (*) (List_1_t297 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m14608(__this, ___index, method) (( void (*) (List_1_t297 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::Insert(System.Int32,T)
#define List_1_Insert_m14609(__this, ___index, ___item, method) (( void (*) (List_1_t297 *, int32_t, Sample_t290 *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m14610(__this, ___collection, method) (( void (*) (List_1_t297 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Sample>::Remove(T)
#define List_1_Remove_m14611(__this, ___item, method) (( bool (*) (List_1_t297 *, Sample_t290 *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m14612(__this, ___match, method) (( int32_t (*) (List_1_t297 *, Predicate_1_t2897 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m14613(__this, ___index, method) (( void (*) (List_1_t297 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::Reverse()
#define List_1_Reverse_m14614(__this, method) (( void (*) (List_1_t297 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::Sort()
#define List_1_Sort_m14615(__this, method) (( void (*) (List_1_t297 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m14616(__this, ___comparison, method) (( void (*) (List_1_t297 *, Comparison_1_t2899 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Reporter/Sample>::ToArray()
#define List_1_ToArray_m14617(__this, method) (( SampleU5BU5D_t2894* (*) (List_1_t297 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::TrimExcess()
#define List_1_TrimExcess_m14618(__this, method) (( void (*) (List_1_t297 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::get_Capacity()
#define List_1_get_Capacity_m14619(__this, method) (( int32_t (*) (List_1_t297 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m14620(__this, ___value, method) (( void (*) (List_1_t297 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Sample>::get_Count()
#define List_1_get_Count_m14621(__this, method) (( int32_t (*) (List_1_t297 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<Reporter/Sample>::get_Item(System.Int32)
#define List_1_get_Item_m14622(__this, ___index, method) (( Sample_t290 * (*) (List_1_t297 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Reporter/Sample>::set_Item(System.Int32,T)
#define List_1_set_Item_m14623(__this, ___index, ___value, method) (( void (*) (List_1_t297 *, int32_t, Sample_t290 *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
