﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t1;
// System.Object
struct Object_t;
// SCR_LevelStartCountdown
struct SCR_LevelStartCountdown_t370;
// System.Object
#include "mscorlib_System_Object.h"
// SCR_LevelStartCountdown/<Rescale>c__IteratorA
struct  U3CRescaleU3Ec__IteratorA_t371  : public Object_t
{
	// System.Single SCR_LevelStartCountdown/<Rescale>c__IteratorA::<time>__0
	float ___U3CtimeU3E__0_0;
	// System.Boolean SCR_LevelStartCountdown/<Rescale>c__IteratorA::up
	bool ___up_1;
	// UnityEngine.Transform SCR_LevelStartCountdown/<Rescale>c__IteratorA::objToScale
	Transform_t1 * ___objToScale_2;
	// System.Int32 SCR_LevelStartCountdown/<Rescale>c__IteratorA::$PC
	int32_t ___U24PC_3;
	// System.Object SCR_LevelStartCountdown/<Rescale>c__IteratorA::$current
	Object_t * ___U24current_4;
	// System.Boolean SCR_LevelStartCountdown/<Rescale>c__IteratorA::<$>up
	bool ___U3CU24U3Eup_5;
	// UnityEngine.Transform SCR_LevelStartCountdown/<Rescale>c__IteratorA::<$>objToScale
	Transform_t1 * ___U3CU24U3EobjToScale_6;
	// SCR_LevelStartCountdown SCR_LevelStartCountdown/<Rescale>c__IteratorA::<>f__this
	SCR_LevelStartCountdown_t370 * ___U3CU3Ef__this_7;
};
