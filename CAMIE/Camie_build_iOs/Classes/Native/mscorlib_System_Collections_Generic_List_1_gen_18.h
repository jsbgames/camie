﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t376;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct  List_1_t653  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Canvas>::_items
	CanvasU5BU5D_t376* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Canvas>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Canvas>::_version
	int32_t ____version_3;
};
struct List_1_t653_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Canvas>::EmptyArray
	CanvasU5BU5D_t376* ___EmptyArray_4;
};
