﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Camie
struct SCR_Camie_t338;
// UnityEngine.Animator
struct Animator_t9;
// SCR_CamieDirections
struct SCR_CamieDirections_t348;
// SCR_Respawn
struct SCR_Respawn_t344;
// UnityEngine.Collision
struct Collision_t146;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// SCR_Goutte
struct SCR_Goutte_t347;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void SCR_Camie::.ctor()
extern "C" void SCR_Camie__ctor_m1242 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::Start()
extern "C" void SCR_Camie_Start_m1243 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::Update()
extern "C" void SCR_Camie_Update_m1244 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::OnCollisionEnter(UnityEngine.Collision)
extern "C" void SCR_Camie_OnCollisionEnter_m1245 (SCR_Camie_t338 * __this, Collision_t146 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_Camie::GoToFrontCollisionPoint(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Object_t * SCR_Camie_GoToFrontCollisionPoint_m1246 (SCR_Camie_t338 * __this, Vector3_t4  ___point, Vector3_t4  ___normal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::ResetActiveCollisionDetector()
extern "C" void SCR_Camie_ResetActiveCollisionDetector_m1247 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_Camie::LandOnCollisionPoint(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Object_t * SCR_Camie_LandOnCollisionPoint_m1248 (SCR_Camie_t338 * __this, Vector3_t4  ___point, Vector3_t4  ___normal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_Camie::Jump(UnityEngine.Vector3)
extern "C" Object_t * SCR_Camie_Jump_m1249 (SCR_Camie_t338 * __this, Vector3_t4  ___normal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::GetDirection()
extern "C" void SCR_Camie_GetDirection_m1250 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::DecreaseSpeed(System.Single)
extern "C" void SCR_Camie_DecreaseSpeed_m1251 (SCR_Camie_t338 * __this, float ___toPercent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::NormalSpeed()
extern "C" void SCR_Camie_NormalSpeed_m1252 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::Respawn()
extern "C" void SCR_Camie_Respawn_m1253 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::BreakIdle()
extern "C" void SCR_Camie_BreakIdle_m1254 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::ResetIdle()
extern "C" void SCR_Camie_ResetIdle_m1255 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::ToggleWings(System.Boolean)
extern "C" void SCR_Camie_ToggleWings_m1256 (SCR_Camie_t338 * __this, bool ___show, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::InBubble(SCR_Goutte)
extern "C" void SCR_Camie_InBubble_m1257 (SCR_Camie_t338 * __this, SCR_Goutte_t347 * ___goutte, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::EndBubble()
extern "C" void SCR_Camie_EndBubble_m1258 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::LosePucerons()
extern "C" void SCR_Camie_LosePucerons_m1259 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Camie::get_IsGrounded()
extern "C" bool SCR_Camie_get_IsGrounded_m1260 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Camie::get_IsFlying()
extern "C" bool SCR_Camie_get_IsFlying_m1261 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Camie::get_CanFly()
extern "C" bool SCR_Camie_get_CanFly_m1262 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::set_CanFly(System.Boolean)
extern "C" void SCR_Camie_set_CanFly_m1263 (SCR_Camie_t338 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Camie::get_IsSlowed()
extern "C" bool SCR_Camie_get_IsSlowed_m1264 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Animator SCR_Camie::get_MyAnimations()
extern "C" Animator_t9 * SCR_Camie_get_MyAnimations_m1265 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SCR_Camie::get_ColliderCenter()
extern "C" Vector3_t4  SCR_Camie_get_ColliderCenter_m1266 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_CamieDirections SCR_Camie::get_Director()
extern "C" SCR_CamieDirections_t348 * SCR_Camie_get_Director_m1267 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie::set_Director(SCR_CamieDirections)
extern "C" void SCR_Camie_set_Director_m1268 (SCR_Camie_t338 * __this, SCR_CamieDirections_t348 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_Respawn SCR_Camie::get_Respawner()
extern "C" SCR_Respawn_t344 * SCR_Camie_get_Respawner_m1269 (SCR_Camie_t338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
