﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct U24ArrayTypeU2412_t1427;
struct U24ArrayTypeU2412_t1427_marshaled;

void U24ArrayTypeU2412_t1427_marshal(const U24ArrayTypeU2412_t1427& unmarshaled, U24ArrayTypeU2412_t1427_marshaled& marshaled);
void U24ArrayTypeU2412_t1427_marshal_back(const U24ArrayTypeU2412_t1427_marshaled& marshaled, U24ArrayTypeU2412_t1427& unmarshaled);
void U24ArrayTypeU2412_t1427_marshal_cleanup(U24ArrayTypeU2412_t1427_marshaled& marshaled);
