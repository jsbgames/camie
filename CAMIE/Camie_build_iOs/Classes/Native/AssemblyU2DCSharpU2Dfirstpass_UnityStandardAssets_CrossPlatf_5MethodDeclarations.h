﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct InputAxisScrollbar_t36;

// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::.ctor()
extern "C" void InputAxisScrollbar__ctor_m116 (InputAxisScrollbar_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::Update()
extern "C" void InputAxisScrollbar_Update_m117 (InputAxisScrollbar_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::HandleInput(System.Single)
extern "C" void InputAxisScrollbar_HandleInput_m118 (InputAxisScrollbar_t36 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
