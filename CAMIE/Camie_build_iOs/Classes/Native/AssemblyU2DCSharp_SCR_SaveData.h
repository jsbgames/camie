﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Data
struct Data_t388;
// System.Int32[]
struct Int32U5BU5D_t242;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_SaveData
struct  SCR_SaveData_t382  : public MonoBehaviour_t3
{
	// System.String SCR_SaveData::saveFile
	String_t* ___saveFile_2;
	// Data SCR_SaveData::data
	Data_t388 * ___data_3;
	// System.Int32[] SCR_SaveData::highscores
	Int32U5BU5D_t242* ___highscores_4;
};
