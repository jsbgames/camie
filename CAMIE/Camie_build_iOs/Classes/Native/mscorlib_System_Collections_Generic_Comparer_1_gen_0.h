﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Vector2>
struct Comparer_1_t2920;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Vector2>
struct  Comparer_1_t2920  : public Object_t
{
};
struct Comparer_1_t2920_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::_default
	Comparer_1_t2920 * ____default_0;
};
