﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>
struct  KeyValuePair_2_t2984 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>::value
	uint8_t ___value_1;
};
