﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t27;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Camera/CameraCallback
struct  CameraCallback_t856  : public MulticastDelegate_t549
{
};
