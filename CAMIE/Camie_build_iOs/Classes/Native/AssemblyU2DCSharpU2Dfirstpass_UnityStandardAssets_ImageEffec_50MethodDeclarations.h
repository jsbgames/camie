﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance
struct ScreenSpaceAmbientObscurance_t117;
// UnityEngine.RenderTexture
struct RenderTexture_t101;

// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::.ctor()
extern "C" void ScreenSpaceAmbientObscurance__ctor_m355 (ScreenSpaceAmbientObscurance_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::CheckResources()
extern "C" bool ScreenSpaceAmbientObscurance_CheckResources_m356 (ScreenSpaceAmbientObscurance_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnDisable()
extern "C" void ScreenSpaceAmbientObscurance_OnDisable_m357 (ScreenSpaceAmbientObscurance_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ScreenSpaceAmbientObscurance_OnRenderImage_m358 (ScreenSpaceAmbientObscurance_t117 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method) IL2CPP_METHOD_ATTR;
