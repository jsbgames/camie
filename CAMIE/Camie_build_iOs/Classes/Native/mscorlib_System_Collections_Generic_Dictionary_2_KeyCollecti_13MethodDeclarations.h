﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Byte>
struct Enumerator_t2988;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte>
struct Dictionary_2_t2983;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15943_gshared (Enumerator_t2988 * __this, Dictionary_2_t2983 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15943(__this, ___host, method) (( void (*) (Enumerator_t2988 *, Dictionary_2_t2983 *, const MethodInfo*))Enumerator__ctor_m15943_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15944_gshared (Enumerator_t2988 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15944(__this, method) (( Object_t * (*) (Enumerator_t2988 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15944_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m15945_gshared (Enumerator_t2988 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15945(__this, method) (( void (*) (Enumerator_t2988 *, const MethodInfo*))Enumerator_Dispose_m15945_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15946_gshared (Enumerator_t2988 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15946(__this, method) (( bool (*) (Enumerator_t2988 *, const MethodInfo*))Enumerator_MoveNext_m15946_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Byte>::get_Current()
extern "C" int32_t Enumerator_get_Current_m15947_gshared (Enumerator_t2988 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15947(__this, method) (( int32_t (*) (Enumerator_t2988 *, const MethodInfo*))Enumerator_get_Current_m15947_gshared)(__this, method)
