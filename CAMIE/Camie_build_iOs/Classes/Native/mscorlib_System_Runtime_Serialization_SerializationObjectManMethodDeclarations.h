﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.SerializationObjectManager/<RegisterObject>c__AnonStorey3
struct U3CRegisterObjectU3Ec__AnonStorey3_t2066;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.Serialization.SerializationObjectManager/<RegisterObject>c__AnonStorey3::.ctor()
extern "C" void U3CRegisterObjectU3Ec__AnonStorey3__ctor_m10989 (U3CRegisterObjectU3Ec__AnonStorey3_t2066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationObjectManager/<RegisterObject>c__AnonStorey3::<>m__2(System.Runtime.Serialization.StreamingContext)
extern "C" void U3CRegisterObjectU3Ec__AnonStorey3_U3CU3Em__2_m10990 (U3CRegisterObjectU3Ec__AnonStorey3_t2066 * __this, StreamingContext_t1044  ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
