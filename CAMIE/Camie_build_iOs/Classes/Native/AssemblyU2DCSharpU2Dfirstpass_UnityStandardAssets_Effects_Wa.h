﻿#pragma once
#include <stdint.h>
// UnityEngine.ParticleCollisionEvent[]
struct ParticleCollisionEventU5BU5D_t159;
// UnityEngine.ParticleSystem
struct ParticleSystem_t161;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityStandardAssets.Effects.WaterHoseParticles
struct  WaterHoseParticles_t162  : public MonoBehaviour_t3
{
	// System.Single UnityStandardAssets.Effects.WaterHoseParticles::force
	float ___force_3;
	// UnityEngine.ParticleCollisionEvent[] UnityStandardAssets.Effects.WaterHoseParticles::m_CollisionEvents
	ParticleCollisionEventU5BU5D_t159* ___m_CollisionEvents_4;
	// UnityEngine.ParticleSystem UnityStandardAssets.Effects.WaterHoseParticles::m_ParticleSystem
	ParticleSystem_t161 * ___m_ParticleSystem_5;
};
struct WaterHoseParticles_t162_StaticFields{
	// System.Single UnityStandardAssets.Effects.WaterHoseParticles::lastSoundTime
	float ___lastSoundTime_2;
};
