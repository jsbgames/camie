﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SuppressUnmanagedCodeSecurityAttribute
struct SuppressUnmanagedCodeSecurityAttribute_t2130;

// System.Void System.Security.SuppressUnmanagedCodeSecurityAttribute::.ctor()
extern "C" void SuppressUnmanagedCodeSecurityAttribute__ctor_m11413 (SuppressUnmanagedCodeSecurityAttribute_t2130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
