﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t3080;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t284;
// System.Object[]
struct ObjectU5BU5D_t224;
// System.Predicate`1<System.Object>
struct Predicate_1_t2846;
// System.Comparison`1<System.Object>
struct Comparison_1_t2852;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C" void IndexedSet_1__ctor_m17276_gshared (IndexedSet_1_t3080 * __this, const MethodInfo* method);
#define IndexedSet_1__ctor_m17276(__this, method) (( void (*) (IndexedSet_1_t3080 *, const MethodInfo*))IndexedSet_1__ctor_m17276_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17278_gshared (IndexedSet_1_t3080 * __this, const MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17278(__this, method) (( Object_t * (*) (IndexedSet_1_t3080 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17278_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C" void IndexedSet_1_Add_m17280_gshared (IndexedSet_1_t3080 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Add_m17280(__this, ___item, method) (( void (*) (IndexedSet_1_t3080 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m17280_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C" bool IndexedSet_1_Remove_m17282_gshared (IndexedSet_1_t3080 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Remove_m17282(__this, ___item, method) (( bool (*) (IndexedSet_1_t3080 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m17282_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C" Object_t* IndexedSet_1_GetEnumerator_m17284_gshared (IndexedSet_1_t3080 * __this, const MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m17284(__this, method) (( Object_t* (*) (IndexedSet_1_t3080 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m17284_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C" void IndexedSet_1_Clear_m17286_gshared (IndexedSet_1_t3080 * __this, const MethodInfo* method);
#define IndexedSet_1_Clear_m17286(__this, method) (( void (*) (IndexedSet_1_t3080 *, const MethodInfo*))IndexedSet_1_Clear_m17286_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C" bool IndexedSet_1_Contains_m17288_gshared (IndexedSet_1_t3080 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Contains_m17288(__this, ___item, method) (( bool (*) (IndexedSet_1_t3080 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m17288_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void IndexedSet_1_CopyTo_m17290_gshared (IndexedSet_1_t3080 * __this, ObjectU5BU5D_t224* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define IndexedSet_1_CopyTo_m17290(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t3080 *, ObjectU5BU5D_t224*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m17290_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C" int32_t IndexedSet_1_get_Count_m17292_gshared (IndexedSet_1_t3080 * __this, const MethodInfo* method);
#define IndexedSet_1_get_Count_m17292(__this, method) (( int32_t (*) (IndexedSet_1_t3080 *, const MethodInfo*))IndexedSet_1_get_Count_m17292_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C" bool IndexedSet_1_get_IsReadOnly_m17294_gshared (IndexedSet_1_t3080 * __this, const MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m17294(__this, method) (( bool (*) (IndexedSet_1_t3080 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m17294_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C" int32_t IndexedSet_1_IndexOf_m17296_gshared (IndexedSet_1_t3080 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_IndexOf_m17296(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t3080 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m17296_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C" void IndexedSet_1_Insert_m17298_gshared (IndexedSet_1_t3080 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Insert_m17298(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t3080 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m17298_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C" void IndexedSet_1_RemoveAt_m17300_gshared (IndexedSet_1_t3080 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_RemoveAt_m17300(__this, ___index, method) (( void (*) (IndexedSet_1_t3080 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m17300_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * IndexedSet_1_get_Item_m17302_gshared (IndexedSet_1_t3080 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_get_Item_m17302(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t3080 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m17302_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C" void IndexedSet_1_set_Item_m17304_gshared (IndexedSet_1_t3080 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define IndexedSet_1_set_Item_m17304(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t3080 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m17304_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" void IndexedSet_1_RemoveAll_m17305_gshared (IndexedSet_1_t3080 * __this, Predicate_1_t2846 * ___match, const MethodInfo* method);
#define IndexedSet_1_RemoveAll_m17305(__this, ___match, method) (( void (*) (IndexedSet_1_t3080 *, Predicate_1_t2846 *, const MethodInfo*))IndexedSet_1_RemoveAll_m17305_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void IndexedSet_1_Sort_m17306_gshared (IndexedSet_1_t3080 * __this, Comparison_1_t2852 * ___sortLayoutFunction, const MethodInfo* method);
#define IndexedSet_1_Sort_m17306(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t3080 *, Comparison_1_t2852 *, const MethodInfo*))IndexedSet_1_Sort_m17306_gshared)(__this, ___sortLayoutFunction, method)
