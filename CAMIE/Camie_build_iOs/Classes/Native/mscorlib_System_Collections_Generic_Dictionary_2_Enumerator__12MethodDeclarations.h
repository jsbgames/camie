﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>
struct Enumerator_t3002;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t373;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__11MethodDeclarations.h"
#define Enumerator__ctor_m16050(__this, ___dictionary, method) (( void (*) (Enumerator_t3002 *, Dictionary_2_t373 *, const MethodInfo*))Enumerator__ctor_m15948_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16051(__this, method) (( Object_t * (*) (Enumerator_t3002 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16052(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3002 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15950_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16053(__this, method) (( Object_t * (*) (Enumerator_t3002 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15951_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16054(__this, method) (( Object_t * (*) (Enumerator_t3002 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15952_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m16055(__this, method) (( bool (*) (Enumerator_t3002 *, const MethodInfo*))Enumerator_MoveNext_m15953_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::get_Current()
#define Enumerator_get_Current_m16056(__this, method) (( KeyValuePair_2_t2999  (*) (Enumerator_t3002 *, const MethodInfo*))Enumerator_get_Current_m15954_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16057(__this, method) (( int32_t (*) (Enumerator_t3002 *, const MethodInfo*))Enumerator_get_CurrentKey_m15955_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16058(__this, method) (( bool (*) (Enumerator_t3002 *, const MethodInfo*))Enumerator_get_CurrentValue_m15956_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m16059(__this, method) (( void (*) (Enumerator_t3002 *, const MethodInfo*))Enumerator_VerifyState_m15957_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16060(__this, method) (( void (*) (Enumerator_t3002 *, const MethodInfo*))Enumerator_VerifyCurrent_m15958_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Boolean>::Dispose()
#define Enumerator_Dispose_m16061(__this, method) (( void (*) (Enumerator_t3002 *, const MethodInfo*))Enumerator_Dispose_m15959_gshared)(__this, method)
