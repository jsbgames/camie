﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t2319;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C" void GenericComparer_1__ctor_m12647_gshared (GenericComparer_1_t2319 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m12647(__this, method) (( void (*) (GenericComparer_1_t2319 *, const MethodInfo*))GenericComparer_1__ctor_m12647_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m22528_gshared (GenericComparer_1_t2319 * __this, TimeSpan_t1337  ___x, TimeSpan_t1337  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m22528(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2319 *, TimeSpan_t1337 , TimeSpan_t1337 , const MethodInfo*))GenericComparer_1_Compare_m22528_gshared)(__this, ___x, ___y, method)
