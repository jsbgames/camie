﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t1;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// SCR_Respawn
struct  SCR_Respawn_t344  : public MonoBehaviour_t3
{
	// UnityEngine.Transform SCR_Respawn::avatarTransform
	Transform_t1 * ___avatarTransform_2;
	// UnityEngine.Vector3 SCR_Respawn::avatarStartPosition
	Vector3_t4  ___avatarStartPosition_3;
	// UnityEngine.Quaternion SCR_Respawn::avatarStartRotation
	Quaternion_t19  ___avatarStartRotation_4;
	// System.Boolean SCR_Respawn::isRespawning
	bool ___isRespawning_5;
};
