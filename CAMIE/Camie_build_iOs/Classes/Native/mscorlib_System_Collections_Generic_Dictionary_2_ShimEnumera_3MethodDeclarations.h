﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>
struct ShimEnumerator_t3266;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t3254;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m20037_gshared (ShimEnumerator_t3266 * __this, Dictionary_2_t3254 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m20037(__this, ___host, method) (( void (*) (ShimEnumerator_t3266 *, Dictionary_2_t3254 *, const MethodInfo*))ShimEnumerator__ctor_m20037_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m20038_gshared (ShimEnumerator_t3266 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m20038(__this, method) (( bool (*) (ShimEnumerator_t3266 *, const MethodInfo*))ShimEnumerator_MoveNext_m20038_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Entry()
extern "C" DictionaryEntry_t1430  ShimEnumerator_get_Entry_m20039_gshared (ShimEnumerator_t3266 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m20039(__this, method) (( DictionaryEntry_t1430  (*) (ShimEnumerator_t3266 *, const MethodInfo*))ShimEnumerator_get_Entry_m20039_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m20040_gshared (ShimEnumerator_t3266 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m20040(__this, method) (( Object_t * (*) (ShimEnumerator_t3266 *, const MethodInfo*))ShimEnumerator_get_Key_m20040_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m20041_gshared (ShimEnumerator_t3266 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m20041(__this, method) (( Object_t * (*) (ShimEnumerator_t3266 *, const MethodInfo*))ShimEnumerator_get_Value_m20041_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m20042_gshared (ShimEnumerator_t3266 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m20042(__this, method) (( Object_t * (*) (ShimEnumerator_t3266 *, const MethodInfo*))ShimEnumerator_get_Current_m20042_gshared)(__this, method)
