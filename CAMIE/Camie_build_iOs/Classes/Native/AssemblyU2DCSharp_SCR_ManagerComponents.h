﻿#pragma once
#include <stdint.h>
// SCR_ManagerComponents
struct SCR_ManagerComponents_t380;
// SCR_PointManager
struct SCR_PointManager_t381;
// SCR_LevelManager
struct SCR_LevelManager_t379;
// SCR_SaveData
struct SCR_SaveData_t382;
// SCR_TimeManager
struct SCR_TimeManager_t383;
// SCR_GUIManager
struct SCR_GUIManager_t378;
// SCR_SoundManager
struct SCR_SoundManager_t335;
// SCR_Camie
struct SCR_Camie_t338;
// SCR_Joystick
struct SCR_Joystick_t346;
// SCR_FlyButton
struct SCR_FlyButton_t345;
// SCR_Spider
struct SCR_Spider_t384;
// UnityEngine.Transform
struct Transform_t1;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_ManagerComponents
struct  SCR_ManagerComponents_t380  : public MonoBehaviour_t3
{
	// SCR_Camie SCR_ManagerComponents::avMove
	SCR_Camie_t338 * ___avMove_9;
	// SCR_Joystick SCR_ManagerComponents::joystick
	SCR_Joystick_t346 * ___joystick_10;
	// SCR_FlyButton SCR_ManagerComponents::flyButton
	SCR_FlyButton_t345 * ___flyButton_11;
	// SCR_Spider SCR_ManagerComponents::sceneSpider
	SCR_Spider_t384 * ___sceneSpider_12;
	// UnityEngine.Transform SCR_ManagerComponents::leftFrame
	Transform_t1 * ___leftFrame_13;
	// UnityEngine.Transform SCR_ManagerComponents::rightFrame
	Transform_t1 * ___rightFrame_14;
	// UnityEngine.Transform SCR_ManagerComponents::bottomFrame
	Transform_t1 * ___bottomFrame_15;
};
struct SCR_ManagerComponents_t380_StaticFields{
	// SCR_ManagerComponents SCR_ManagerComponents::instance
	SCR_ManagerComponents_t380 * ___instance_2;
	// SCR_PointManager SCR_ManagerComponents::pointManager
	SCR_PointManager_t381 * ___pointManager_3;
	// SCR_LevelManager SCR_ManagerComponents::levelManager
	SCR_LevelManager_t379 * ___levelManager_4;
	// SCR_SaveData SCR_ManagerComponents::saveData
	SCR_SaveData_t382 * ___saveData_5;
	// SCR_TimeManager SCR_ManagerComponents::timeManager
	SCR_TimeManager_t383 * ___timeManager_6;
	// SCR_GUIManager SCR_ManagerComponents::guiManager
	SCR_GUIManager_t378 * ___guiManager_7;
	// SCR_SoundManager SCR_ManagerComponents::soundManager
	SCR_SoundManager_t335 * ___soundManager_8;
};
