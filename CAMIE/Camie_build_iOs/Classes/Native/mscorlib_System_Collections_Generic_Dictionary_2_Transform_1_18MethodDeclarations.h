﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Boolean,System.Collections.DictionaryEntry>
struct Transform_1_t2981;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Boolean,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Byte,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_16MethodDeclarations.h"
#define Transform_1__ctor_m16012(__this, ___object, ___method, method) (( void (*) (Transform_1_t2981 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m15987_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Boolean,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m16013(__this, ___key, ___value, method) (( DictionaryEntry_t1430  (*) (Transform_1_t2981 *, int32_t, bool, const MethodInfo*))Transform_1_Invoke_m15988_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Boolean,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m16014(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2981 *, int32_t, bool, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m15989_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Boolean,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m16015(__this, ___result, method) (( DictionaryEntry_t1430  (*) (Transform_1_t2981 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m15990_gshared)(__this, ___result, method)
