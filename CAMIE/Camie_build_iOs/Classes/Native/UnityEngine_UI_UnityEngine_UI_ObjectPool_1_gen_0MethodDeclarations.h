﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct ObjectPool_1_t521;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct UnityAction_1_t524;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t558;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m3177(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t521 *, UnityAction_1_t524 *, UnityAction_1_t524 *, const MethodInfo*))ObjectPool_1__ctor_m16456_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countAll()
#define ObjectPool_1_get_countAll_m17612(__this, method) (( int32_t (*) (ObjectPool_1_t521 *, const MethodInfo*))ObjectPool_1_get_countAll_m16458_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m17613(__this, ___value, method) (( void (*) (ObjectPool_1_t521 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m16460_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countActive()
#define ObjectPool_1_get_countActive_m17614(__this, method) (( int32_t (*) (ObjectPool_1_t521 *, const MethodInfo*))ObjectPool_1_get_countActive_m16462_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m17615(__this, method) (( int32_t (*) (ObjectPool_1_t521 *, const MethodInfo*))ObjectPool_1_get_countInactive_m16464_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Get()
#define ObjectPool_1_Get_m3183(__this, method) (( List_1_t558 * (*) (ObjectPool_1_t521 *, const MethodInfo*))ObjectPool_1_Get_m16466_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Release(T)
#define ObjectPool_1_Release_m3187(__this, ___element, method) (( void (*) (ObjectPool_1_t521 *, List_1_t558 *, const MethodInfo*))ObjectPool_1_Release_m16468_gshared)(__this, ___element, method)
