﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform
struct RectTransform_t364;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.RectTransform>
struct  Predicate_1_t3175  : public MulticastDelegate_t549
{
};
