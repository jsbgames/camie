﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>
struct Enumerator_t3298;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t3292;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20426_gshared (Enumerator_t3298 * __this, Dictionary_2_t3292 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m20426(__this, ___dictionary, method) (( void (*) (Enumerator_t3298 *, Dictionary_2_t3292 *, const MethodInfo*))Enumerator__ctor_m20426_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20427_gshared (Enumerator_t3298 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20427(__this, method) (( Object_t * (*) (Enumerator_t3298 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20427_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1430  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20428_gshared (Enumerator_t3298 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20428(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3298 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20428_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20429_gshared (Enumerator_t3298 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20429(__this, method) (( Object_t * (*) (Enumerator_t3298 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20429_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20430_gshared (Enumerator_t3298 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20430(__this, method) (( Object_t * (*) (Enumerator_t3298 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20430_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20431_gshared (Enumerator_t3298 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20431(__this, method) (( bool (*) (Enumerator_t3298 *, const MethodInfo*))Enumerator_MoveNext_m20431_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3293  Enumerator_get_Current_m20432_gshared (Enumerator_t3298 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20432(__this, method) (( KeyValuePair_2_t3293  (*) (Enumerator_t3298 *, const MethodInfo*))Enumerator_get_Current_m20432_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::get_CurrentKey()
extern "C" uint64_t Enumerator_get_CurrentKey_m20433_gshared (Enumerator_t3298 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m20433(__this, method) (( uint64_t (*) (Enumerator_t3298 *, const MethodInfo*))Enumerator_get_CurrentKey_m20433_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m20434_gshared (Enumerator_t3298 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m20434(__this, method) (( Object_t * (*) (Enumerator_t3298 *, const MethodInfo*))Enumerator_get_CurrentValue_m20434_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m20435_gshared (Enumerator_t3298 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m20435(__this, method) (( void (*) (Enumerator_t3298 *, const MethodInfo*))Enumerator_VerifyState_m20435_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m20436_gshared (Enumerator_t3298 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m20436(__this, method) (( void (*) (Enumerator_t3298 *, const MethodInfo*))Enumerator_VerifyCurrent_m20436_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m20437_gshared (Enumerator_t3298 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20437(__this, method) (( void (*) (Enumerator_t3298 *, const MethodInfo*))Enumerator_Dispose_m20437_gshared)(__this, method)
