﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.ImageEffects.MotionBlur
struct MotionBlur_t109;
// UnityEngine.RenderTexture
struct RenderTexture_t101;

// System.Void UnityStandardAssets.ImageEffects.MotionBlur::.ctor()
extern "C" void MotionBlur__ctor_m312 (MotionBlur_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.MotionBlur::Start()
extern "C" void MotionBlur_Start_m313 (MotionBlur_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnDisable()
extern "C" void MotionBlur_OnDisable_m314 (MotionBlur_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void MotionBlur_OnRenderImage_m315 (MotionBlur_t109 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method) IL2CPP_METHOD_ATTR;
