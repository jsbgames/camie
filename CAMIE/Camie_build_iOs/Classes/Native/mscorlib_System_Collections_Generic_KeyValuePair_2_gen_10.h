﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Reporter/Log
struct Log_t291;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>
struct  KeyValuePair_2_t2927 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,Reporter/Log>::value
	Log_t291 * ___value_1;
};
