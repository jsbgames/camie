﻿#pragma once
#include <stdint.h>
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t210;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.RectTransformUtility
struct  RectTransformUtility_t674  : public Object_t
{
};
struct RectTransformUtility_t674_StaticFields{
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_t210* ___s_Corners_0;
};
