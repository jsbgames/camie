﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Reporter/Sample
struct Sample_t290;

// System.Void Reporter/Sample::.ctor()
extern "C" void Sample__ctor_m1080 (Sample_t290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Reporter/Sample::MemSize()
extern "C" float Sample_MemSize_m1081 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
