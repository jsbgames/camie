﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_SoundManager
struct SCR_SoundManager_t335;
// System.String
struct String_t;

// System.Void SCR_SoundManager::.ctor()
extern "C" void SCR_SoundManager__ctor_m1489 (SCR_SoundManager_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SoundManager::Awake()
extern "C" void SCR_SoundManager_Awake_m1490 (SCR_SoundManager_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SoundManager::Start()
extern "C" void SCR_SoundManager_Start_m1491 (SCR_SoundManager_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SoundManager::OnLevelWasLoaded()
extern "C" void SCR_SoundManager_OnLevelWasLoaded_m1492 (SCR_SoundManager_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SoundManager::FillSoundDictionary()
extern "C" void SCR_SoundManager_FillSoundDictionary_m1493 (SCR_SoundManager_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SoundManager::LevelInit()
extern "C" void SCR_SoundManager_LevelInit_m1494 (SCR_SoundManager_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SoundManager::PlaySound(System.String)
extern "C" void SCR_SoundManager_PlaySound_m1495 (SCR_SoundManager_t335 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SoundManager::StopSound(System.String)
extern "C" void SCR_SoundManager_StopSound_m1496 (SCR_SoundManager_t335 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SoundManager::StopAllSounds()
extern "C" void SCR_SoundManager_StopAllSounds_m1497 (SCR_SoundManager_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_SoundManager::get_SoundOn()
extern "C" bool SCR_SoundManager_get_SoundOn_m1498 (SCR_SoundManager_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SoundManager::set_SoundOn(System.Boolean)
extern "C" void SCR_SoundManager_set_SoundOn_m1499 (SCR_SoundManager_t335 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
