﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ParticlePhysicsExtensions
struct ParticlePhysicsExtensions_t871;
// UnityEngine.ParticleSystem
struct ParticleSystem_t161;
// UnityEngine.GameObject
struct GameObject_t78;
// UnityEngine.ParticleCollisionEvent[]
struct ParticleCollisionEventU5BU5D_t159;

// System.Int32 UnityEngine.ParticlePhysicsExtensions::GetSafeCollisionEventSize(UnityEngine.ParticleSystem)
extern "C" int32_t ParticlePhysicsExtensions_GetSafeCollisionEventSize_m940 (Object_t * __this /* static, unused */, ParticleSystem_t161 * ___ps, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticlePhysicsExtensions::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,UnityEngine.ParticleCollisionEvent[])
extern "C" int32_t ParticlePhysicsExtensions_GetCollisionEvents_m941 (Object_t * __this /* static, unused */, ParticleSystem_t161 * ___ps, GameObject_t78 * ___go, ParticleCollisionEventU5BU5D_t159* ___collisionEvents, const MethodInfo* method) IL2CPP_METHOD_ATTR;
