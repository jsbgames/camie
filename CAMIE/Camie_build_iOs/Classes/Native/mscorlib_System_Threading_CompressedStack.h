﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t1271;
// System.Object
#include "mscorlib_System_Object.h"
// System.Threading.CompressedStack
struct  CompressedStack_t2120  : public Object_t
{
	// System.Collections.ArrayList System.Threading.CompressedStack::_list
	ArrayList_t1271 * ____list_0;
};
