﻿#pragma once
#include <stdint.h>
// UnityEngine.Mesh[]
struct MeshU5BU5D_t113;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.ImageEffects.Triangles
struct  Triangles_t130  : public Object_t
{
};
struct Triangles_t130_StaticFields{
	// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Triangles::meshes
	MeshU5BU5D_t113* ___meshes_0;
	// System.Int32 UnityStandardAssets.ImageEffects.Triangles::currentTris
	int32_t ___currentTris_1;
};
