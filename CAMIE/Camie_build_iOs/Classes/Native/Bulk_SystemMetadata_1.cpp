﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// System.Text.RegularExpressions.FactoryCache
#include "System_System_Text_RegularExpressions_FactoryCache.h"
// Metadata Definition System.Text.RegularExpressions.FactoryCache
extern TypeInfo FactoryCache_t1364_il2cpp_TypeInfo;
// System.Text.RegularExpressions.FactoryCache
#include "System_System_Text_RegularExpressions_FactoryCacheMethodDeclarations.h"
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo FactoryCache_t1364_FactoryCache__ctor_m6003_ParameterInfos[] = 
{
	{"capacity", 0, 134218216, 0, &Int32_t253_0_0_0},
};
extern const Il2CppType Void_t272_0_0_0;
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::.ctor(System.Int32)
extern const MethodInfo FactoryCache__ctor_m6003_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FactoryCache__ctor_m6003/* method */
	, &FactoryCache_t1364_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, FactoryCache_t1364_FactoryCache__ctor_m6003_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType RegexOptions_t1367_0_0_0;
extern const Il2CppType RegexOptions_t1367_0_0_0;
extern const Il2CppType IMachineFactory_t1365_0_0_0;
extern const Il2CppType IMachineFactory_t1365_0_0_0;
static const ParameterInfo FactoryCache_t1364_FactoryCache_Add_m6004_ParameterInfos[] = 
{
	{"pattern", 0, 134218217, 0, &String_t_0_0_0},
	{"options", 1, 134218218, 0, &RegexOptions_t1367_0_0_0},
	{"factory", 2, 134218219, 0, &IMachineFactory_t1365_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::Add(System.String,System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.IMachineFactory)
extern const MethodInfo FactoryCache_Add_m6004_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&FactoryCache_Add_m6004/* method */
	, &FactoryCache_t1364_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t/* invoker_method */
	, FactoryCache_t1364_FactoryCache_Add_m6004_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::Cleanup()
extern const MethodInfo FactoryCache_Cleanup_m6005_MethodInfo = 
{
	"Cleanup"/* name */
	, (methodPointerType)&FactoryCache_Cleanup_m6005/* method */
	, &FactoryCache_t1364_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType RegexOptions_t1367_0_0_0;
static const ParameterInfo FactoryCache_t1364_FactoryCache_Lookup_m6006_ParameterInfos[] = 
{
	{"pattern", 0, 134218220, 0, &String_t_0_0_0},
	{"options", 1, 134218221, 0, &RegexOptions_t1367_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.FactoryCache::Lookup(System.String,System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo FactoryCache_Lookup_m6006_MethodInfo = 
{
	"Lookup"/* name */
	, (methodPointerType)&FactoryCache_Lookup_m6006/* method */
	, &FactoryCache_t1364_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t1365_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t253/* invoker_method */
	, FactoryCache_t1364_FactoryCache_Lookup_m6006_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FactoryCache_t1364_MethodInfos[] =
{
	&FactoryCache__ctor_m6003_MethodInfo,
	&FactoryCache_Add_m6004_MethodInfo,
	&FactoryCache_Cleanup_m6005_MethodInfo,
	&FactoryCache_Lookup_m6006_MethodInfo,
	NULL
};
extern const Il2CppType Key_t1371_0_0_0;
static const Il2CppType* FactoryCache_t1364_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Key_t1371_0_0_0,
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
static const Il2CppMethodReference FactoryCache_t1364_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool FactoryCache_t1364_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FactoryCache_t1364_0_0_0;
extern const Il2CppType FactoryCache_t1364_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct FactoryCache_t1364;
const Il2CppTypeDefinitionMetadata FactoryCache_t1364_DefinitionMetadata = 
{
	NULL/* declaringType */
	, FactoryCache_t1364_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FactoryCache_t1364_VTable/* vtableMethods */
	, FactoryCache_t1364_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 492/* fieldStart */

};
TypeInfo FactoryCache_t1364_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FactoryCache"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, FactoryCache_t1364_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FactoryCache_t1364_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FactoryCache_t1364_0_0_0/* byval_arg */
	, &FactoryCache_t1364_1_0_0/* this_arg */
	, &FactoryCache_t1364_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FactoryCache_t1364)/* instance_size */
	, sizeof (FactoryCache_t1364)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MRUList/Node
#include "System_System_Text_RegularExpressions_MRUList_Node.h"
// Metadata Definition System.Text.RegularExpressions.MRUList/Node
extern TypeInfo Node_t1373_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MRUList/Node
#include "System_System_Text_RegularExpressions_MRUList_NodeMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Node_t1373_Node__ctor_m6007_ParameterInfos[] = 
{
	{"value", 0, 134218226, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList/Node::.ctor(System.Object)
extern const MethodInfo Node__ctor_m6007_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Node__ctor_m6007/* method */
	, &Node_t1373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Node_t1373_Node__ctor_m6007_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Node_t1373_MethodInfos[] =
{
	&Node__ctor_m6007_MethodInfo,
	NULL
};
static const Il2CppMethodReference Node_t1373_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Node_t1373_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Node_t1373_0_0_0;
extern const Il2CppType Node_t1373_1_0_0;
extern TypeInfo MRUList_t1372_il2cpp_TypeInfo;
extern const Il2CppType MRUList_t1372_0_0_0;
struct Node_t1373;
const Il2CppTypeDefinitionMetadata Node_t1373_DefinitionMetadata = 
{
	&MRUList_t1372_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Node_t1373_VTable/* vtableMethods */
	, Node_t1373_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 495/* fieldStart */

};
TypeInfo Node_t1373_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Node"/* name */
	, ""/* namespaze */
	, Node_t1373_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Node_t1373_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Node_t1373_0_0_0/* byval_arg */
	, &Node_t1373_1_0_0/* this_arg */
	, &Node_t1373_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Node_t1373)/* instance_size */
	, sizeof (Node_t1373)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MRUList
#include "System_System_Text_RegularExpressions_MRUList.h"
// Metadata Definition System.Text.RegularExpressions.MRUList
// System.Text.RegularExpressions.MRUList
#include "System_System_Text_RegularExpressions_MRUListMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList::.ctor()
extern const MethodInfo MRUList__ctor_m6008_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MRUList__ctor_m6008/* method */
	, &MRUList_t1372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MRUList_t1372_MRUList_Use_m6009_ParameterInfos[] = 
{
	{"o", 0, 134218225, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList::Use(System.Object)
extern const MethodInfo MRUList_Use_m6009_MethodInfo = 
{
	"Use"/* name */
	, (methodPointerType)&MRUList_Use_m6009/* method */
	, &MRUList_t1372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MRUList_t1372_MRUList_Use_m6009_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.MRUList::Evict()
extern const MethodInfo MRUList_Evict_m6010_MethodInfo = 
{
	"Evict"/* name */
	, (methodPointerType)&MRUList_Evict_m6010/* method */
	, &MRUList_t1372_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MRUList_t1372_MethodInfos[] =
{
	&MRUList__ctor_m6008_MethodInfo,
	&MRUList_Use_m6009_MethodInfo,
	&MRUList_Evict_m6010_MethodInfo,
	NULL
};
static const Il2CppType* MRUList_t1372_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Node_t1373_0_0_0,
};
static const Il2CppMethodReference MRUList_t1372_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool MRUList_t1372_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MRUList_t1372_1_0_0;
struct MRUList_t1372;
const Il2CppTypeDefinitionMetadata MRUList_t1372_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MRUList_t1372_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MRUList_t1372_VTable/* vtableMethods */
	, MRUList_t1372_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 498/* fieldStart */

};
TypeInfo MRUList_t1372_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MRUList"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, MRUList_t1372_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MRUList_t1372_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MRUList_t1372_0_0_0/* byval_arg */
	, &MRUList_t1372_1_0_0/* this_arg */
	, &MRUList_t1372_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MRUList_t1372)/* instance_size */
	, sizeof (MRUList_t1372)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
// Metadata Definition System.Text.RegularExpressions.Category
extern TypeInfo Category_t1374_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_CategoryMethodDeclarations.h"
static const MethodInfo* Category_t1374_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m1051_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m1052_MethodInfo;
extern const MethodInfo Enum_ToString_m1053_MethodInfo;
extern const MethodInfo Enum_ToString_m1054_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m1055_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m1056_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m1057_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m1058_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m1059_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m1060_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m1061_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m1062_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m1063_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m1064_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m1065_MethodInfo;
extern const MethodInfo Enum_ToString_m1066_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m1067_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m1068_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m1069_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m1070_MethodInfo;
extern const MethodInfo Enum_CompareTo_m1071_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m1072_MethodInfo;
static const Il2CppMethodReference Category_t1374_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Category_t1374_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t275_0_0_0;
extern const Il2CppType IConvertible_t276_0_0_0;
extern const Il2CppType IComparable_t277_0_0_0;
static Il2CppInterfaceOffsetPair Category_t1374_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Category_t1374_0_0_0;
extern const Il2CppType Category_t1374_1_0_0;
extern const Il2CppType Enum_t278_0_0_0;
// System.UInt16
#include "mscorlib_System_UInt16.h"
extern TypeInfo UInt16_t684_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Category_t1374_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Category_t1374_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Category_t1374_VTable/* vtableMethods */
	, Category_t1374_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 500/* fieldStart */

};
TypeInfo Category_t1374_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Category"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Category_t1374_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UInt16_t684_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Category_t1374_0_0_0/* byval_arg */
	, &Category_t1374_1_0_0/* this_arg */
	, &Category_t1374_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Category_t1374)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Category_t1374)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 146/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.CategoryUtils
#include "System_System_Text_RegularExpressions_CategoryUtils.h"
// Metadata Definition System.Text.RegularExpressions.CategoryUtils
extern TypeInfo CategoryUtils_t1375_il2cpp_TypeInfo;
// System.Text.RegularExpressions.CategoryUtils
#include "System_System_Text_RegularExpressions_CategoryUtilsMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CategoryUtils_t1375_CategoryUtils_CategoryFromName_m6011_ParameterInfos[] = 
{
	{"name", 0, 134218227, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Category_t1374_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Category System.Text.RegularExpressions.CategoryUtils::CategoryFromName(System.String)
extern const MethodInfo CategoryUtils_CategoryFromName_m6011_MethodInfo = 
{
	"CategoryFromName"/* name */
	, (methodPointerType)&CategoryUtils_CategoryFromName_m6011/* method */
	, &CategoryUtils_t1375_il2cpp_TypeInfo/* declaring_type */
	, &Category_t1374_0_0_0/* return_type */
	, RuntimeInvoker_Category_t1374_Object_t/* invoker_method */
	, CategoryUtils_t1375_CategoryUtils_CategoryFromName_m6011_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1374_0_0_0;
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo CategoryUtils_t1375_CategoryUtils_IsCategory_m6012_ParameterInfos[] = 
{
	{"cat", 0, 134218228, 0, &Category_t1374_0_0_0},
	{"c", 1, 134218229, 0, &Char_t682_0_0_0},
};
extern const Il2CppType Boolean_t273_0_0_0;
extern void* RuntimeInvoker_Boolean_t273_UInt16_t684_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.CategoryUtils::IsCategory(System.Text.RegularExpressions.Category,System.Char)
extern const MethodInfo CategoryUtils_IsCategory_m6012_MethodInfo = 
{
	"IsCategory"/* name */
	, (methodPointerType)&CategoryUtils_IsCategory_m6012/* method */
	, &CategoryUtils_t1375_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_UInt16_t684_Int16_t752/* invoker_method */
	, CategoryUtils_t1375_CategoryUtils_IsCategory_m6012_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnicodeCategory_t1490_0_0_0;
extern const Il2CppType UnicodeCategory_t1490_0_0_0;
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo CategoryUtils_t1375_CategoryUtils_IsCategory_m6013_ParameterInfos[] = 
{
	{"uc", 0, 134218230, 0, &UnicodeCategory_t1490_0_0_0},
	{"c", 1, 134218231, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.CategoryUtils::IsCategory(System.Globalization.UnicodeCategory,System.Char)
extern const MethodInfo CategoryUtils_IsCategory_m6013_MethodInfo = 
{
	"IsCategory"/* name */
	, (methodPointerType)&CategoryUtils_IsCategory_m6013/* method */
	, &CategoryUtils_t1375_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253_Int16_t752/* invoker_method */
	, CategoryUtils_t1375_CategoryUtils_IsCategory_m6013_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CategoryUtils_t1375_MethodInfos[] =
{
	&CategoryUtils_CategoryFromName_m6011_MethodInfo,
	&CategoryUtils_IsCategory_m6012_MethodInfo,
	&CategoryUtils_IsCategory_m6013_MethodInfo,
	NULL
};
static const Il2CppMethodReference CategoryUtils_t1375_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CategoryUtils_t1375_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CategoryUtils_t1375_0_0_0;
extern const Il2CppType CategoryUtils_t1375_1_0_0;
struct CategoryUtils_t1375;
const Il2CppTypeDefinitionMetadata CategoryUtils_t1375_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CategoryUtils_t1375_VTable/* vtableMethods */
	, CategoryUtils_t1375_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CategoryUtils_t1375_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CategoryUtils"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, CategoryUtils_t1375_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CategoryUtils_t1375_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CategoryUtils_t1375_0_0_0/* byval_arg */
	, &CategoryUtils_t1375_1_0_0/* this_arg */
	, &CategoryUtils_t1375_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CategoryUtils_t1375)/* instance_size */
	, sizeof (CategoryUtils_t1375)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRef.h"
// Metadata Definition System.Text.RegularExpressions.LinkRef
extern TypeInfo LinkRef_t1376_il2cpp_TypeInfo;
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRefMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkRef::.ctor()
extern const MethodInfo LinkRef__ctor_m6014_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkRef__ctor_m6014/* method */
	, &LinkRef_t1376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LinkRef_t1376_MethodInfos[] =
{
	&LinkRef__ctor_m6014_MethodInfo,
	NULL
};
static const Il2CppMethodReference LinkRef_t1376_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool LinkRef_t1376_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType LinkRef_t1376_0_0_0;
extern const Il2CppType LinkRef_t1376_1_0_0;
struct LinkRef_t1376;
const Il2CppTypeDefinitionMetadata LinkRef_t1376_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LinkRef_t1376_VTable/* vtableMethods */
	, LinkRef_t1376_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo LinkRef_t1376_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkRef"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, LinkRef_t1376_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LinkRef_t1376_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LinkRef_t1376_0_0_0/* byval_arg */
	, &LinkRef_t1376_1_0_0/* this_arg */
	, &LinkRef_t1376_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkRef_t1376)/* instance_size */
	, sizeof (LinkRef_t1376)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Text.RegularExpressions.ICompiler
extern TypeInfo ICompiler_t1439_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.ICompiler::GetMachineFactory()
extern const MethodInfo ICompiler_GetMachineFactory_m6574_MethodInfo = 
{
	"GetMachineFactory"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t1365_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitFalse()
extern const MethodInfo ICompiler_EmitFalse_m6575_MethodInfo = 
{
	"EmitFalse"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitTrue()
extern const MethodInfo ICompiler_EmitTrue_m6576_MethodInfo = 
{
	"EmitTrue"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitCharacter_m6577_ParameterInfos[] = 
{
	{"c", 0, 134218232, 0, &Char_t682_0_0_0},
	{"negate", 1, 134218233, 0, &Boolean_t273_0_0_0},
	{"ignore", 2, 134218234, 0, &Boolean_t273_0_0_0},
	{"reverse", 3, 134218235, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitCharacter(System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitCharacter_m6577_MethodInfo = 
{
	"EmitCharacter"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752_SByte_t274_SByte_t274_SByte_t274/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitCharacter_m6577_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1374_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitCategory_m6578_ParameterInfos[] = 
{
	{"cat", 0, 134218236, 0, &Category_t1374_0_0_0},
	{"negate", 1, 134218237, 0, &Boolean_t273_0_0_0},
	{"reverse", 2, 134218238, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitCategory_m6578_MethodInfo = 
{
	"EmitCategory"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274_SByte_t274/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitCategory_m6578_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1374_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitNotCategory_m6579_ParameterInfos[] = 
{
	{"cat", 0, 134218239, 0, &Category_t1374_0_0_0},
	{"negate", 1, 134218240, 0, &Boolean_t273_0_0_0},
	{"reverse", 2, 134218241, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitNotCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitNotCategory_m6579_MethodInfo = 
{
	"EmitNotCategory"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274_SByte_t274/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitNotCategory_m6579_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitRange_m6580_ParameterInfos[] = 
{
	{"lo", 0, 134218242, 0, &Char_t682_0_0_0},
	{"hi", 1, 134218243, 0, &Char_t682_0_0_0},
	{"negate", 2, 134218244, 0, &Boolean_t273_0_0_0},
	{"ignore", 3, 134218245, 0, &Boolean_t273_0_0_0},
	{"reverse", 4, 134218246, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752_Int16_t752_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitRange(System.Char,System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitRange_m6580_MethodInfo = 
{
	"EmitRange"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752_Int16_t752_SByte_t274_SByte_t274_SByte_t274/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitRange_m6580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType BitArray_t1412_0_0_0;
extern const Il2CppType BitArray_t1412_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitSet_m6581_ParameterInfos[] = 
{
	{"lo", 0, 134218247, 0, &Char_t682_0_0_0},
	{"set", 1, 134218248, 0, &BitArray_t1412_0_0_0},
	{"negate", 2, 134218249, 0, &Boolean_t273_0_0_0},
	{"ignore", 3, 134218250, 0, &Boolean_t273_0_0_0},
	{"reverse", 4, 134218251, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752_Object_t_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitSet(System.Char,System.Collections.BitArray,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitSet_m6581_MethodInfo = 
{
	"EmitSet"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752_Object_t_SByte_t274_SByte_t274_SByte_t274/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitSet_m6581_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitString_m6582_ParameterInfos[] = 
{
	{"str", 0, 134218252, 0, &String_t_0_0_0},
	{"ignore", 1, 134218253, 0, &Boolean_t273_0_0_0},
	{"reverse", 2, 134218254, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitString(System.String,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitString_m6582_MethodInfo = 
{
	"EmitString"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitString_m6582_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Position_t1370_0_0_0;
extern const Il2CppType Position_t1370_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitPosition_m6583_ParameterInfos[] = 
{
	{"pos", 0, 134218255, 0, &Position_t1370_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitPosition(System.Text.RegularExpressions.Position)
extern const MethodInfo ICompiler_EmitPosition_m6583_MethodInfo = 
{
	"EmitPosition"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitPosition_m6583_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitOpen_m6584_ParameterInfos[] = 
{
	{"gid", 0, 134218256, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitOpen(System.Int32)
extern const MethodInfo ICompiler_EmitOpen_m6584_MethodInfo = 
{
	"EmitOpen"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitOpen_m6584_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitClose_m6585_ParameterInfos[] = 
{
	{"gid", 0, 134218257, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitClose(System.Int32)
extern const MethodInfo ICompiler_EmitClose_m6585_MethodInfo = 
{
	"EmitClose"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitClose_m6585_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitBalanceStart_m6586_ParameterInfos[] = 
{
	{"gid", 0, 134218258, 0, &Int32_t253_0_0_0},
	{"balance", 1, 134218259, 0, &Int32_t253_0_0_0},
	{"capture", 2, 134218260, 0, &Boolean_t273_0_0_0},
	{"tail", 3, 134218261, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBalanceStart(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitBalanceStart_m6586_MethodInfo = 
{
	"EmitBalanceStart"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitBalanceStart_m6586_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBalance()
extern const MethodInfo ICompiler_EmitBalance_m6587_MethodInfo = 
{
	"EmitBalance"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitReference_m6588_ParameterInfos[] = 
{
	{"gid", 0, 134218262, 0, &Int32_t253_0_0_0},
	{"ignore", 1, 134218263, 0, &Boolean_t273_0_0_0},
	{"reverse", 2, 134218264, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitReference(System.Int32,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitReference_m6588_MethodInfo = 
{
	"EmitReference"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_SByte_t274_SByte_t274/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitReference_m6588_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitIfDefined_m6589_ParameterInfos[] = 
{
	{"gid", 0, 134218265, 0, &Int32_t253_0_0_0},
	{"tail", 1, 134218266, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitIfDefined(System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitIfDefined_m6589_MethodInfo = 
{
	"EmitIfDefined"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitIfDefined_m6589_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitSub_m6590_ParameterInfos[] = 
{
	{"tail", 0, 134218267, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitSub(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitSub_m6590_MethodInfo = 
{
	"EmitSub"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitSub_m6590_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitTest_m6591_ParameterInfos[] = 
{
	{"yes", 0, 134218268, 0, &LinkRef_t1376_0_0_0},
	{"tail", 1, 134218269, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitTest(System.Text.RegularExpressions.LinkRef,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitTest_m6591_MethodInfo = 
{
	"EmitTest"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitTest_m6591_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitBranch_m6592_ParameterInfos[] = 
{
	{"next", 0, 134218270, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBranch(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitBranch_m6592_MethodInfo = 
{
	"EmitBranch"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitBranch_m6592_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitJump_m6593_ParameterInfos[] = 
{
	{"target", 0, 134218271, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitJump(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitJump_m6593_MethodInfo = 
{
	"EmitJump"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitJump_m6593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitRepeat_m6594_ParameterInfos[] = 
{
	{"min", 0, 134218272, 0, &Int32_t253_0_0_0},
	{"max", 1, 134218273, 0, &Int32_t253_0_0_0},
	{"lazy", 2, 134218274, 0, &Boolean_t273_0_0_0},
	{"until", 3, 134218275, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitRepeat_m6594_MethodInfo = 
{
	"EmitRepeat"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitRepeat_m6594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitUntil_m6595_ParameterInfos[] = 
{
	{"repeat", 0, 134218276, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitUntil(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitUntil_m6595_MethodInfo = 
{
	"EmitUntil"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitUntil_m6595_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitIn_m6596_ParameterInfos[] = 
{
	{"tail", 0, 134218277, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitIn(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitIn_m6596_MethodInfo = 
{
	"EmitIn"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitIn_m6596_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitInfo_m6597_ParameterInfos[] = 
{
	{"count", 0, 134218278, 0, &Int32_t253_0_0_0},
	{"min", 1, 134218279, 0, &Int32_t253_0_0_0},
	{"max", 2, 134218280, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitInfo(System.Int32,System.Int32,System.Int32)
extern const MethodInfo ICompiler_EmitInfo_m6597_MethodInfo = 
{
	"EmitInfo"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitInfo_m6597_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitFastRepeat_m6598_ParameterInfos[] = 
{
	{"min", 0, 134218281, 0, &Int32_t253_0_0_0},
	{"max", 1, 134218282, 0, &Int32_t253_0_0_0},
	{"lazy", 2, 134218283, 0, &Boolean_t273_0_0_0},
	{"tail", 3, 134218284, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitFastRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitFastRepeat_m6598_MethodInfo = 
{
	"EmitFastRepeat"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitFastRepeat_m6598_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_EmitAnchor_m6599_ParameterInfos[] = 
{
	{"reverse", 0, 134218285, 0, &Boolean_t273_0_0_0},
	{"offset", 1, 134218286, 0, &Int32_t253_0_0_0},
	{"tail", 2, 134218287, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitAnchor(System.Boolean,System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitAnchor_m6599_MethodInfo = 
{
	"EmitAnchor"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274_Int32_t253_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_EmitAnchor_m6599_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBranchEnd()
extern const MethodInfo ICompiler_EmitBranchEnd_m6600_MethodInfo = 
{
	"EmitBranchEnd"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitAlternationEnd()
extern const MethodInfo ICompiler_EmitAlternationEnd_m6601_MethodInfo = 
{
	"EmitAlternationEnd"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink()
extern const MethodInfo ICompiler_NewLink_m6602_MethodInfo = 
{
	"NewLink"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &LinkRef_t1376_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo ICompiler_t1439_ICompiler_ResolveLink_m6603_ParameterInfos[] = 
{
	{"link", 0, 134218288, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_ResolveLink_m6603_MethodInfo = 
{
	"ResolveLink"/* name */
	, NULL/* method */
	, &ICompiler_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ICompiler_t1439_ICompiler_ResolveLink_m6603_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ICompiler_t1439_MethodInfos[] =
{
	&ICompiler_GetMachineFactory_m6574_MethodInfo,
	&ICompiler_EmitFalse_m6575_MethodInfo,
	&ICompiler_EmitTrue_m6576_MethodInfo,
	&ICompiler_EmitCharacter_m6577_MethodInfo,
	&ICompiler_EmitCategory_m6578_MethodInfo,
	&ICompiler_EmitNotCategory_m6579_MethodInfo,
	&ICompiler_EmitRange_m6580_MethodInfo,
	&ICompiler_EmitSet_m6581_MethodInfo,
	&ICompiler_EmitString_m6582_MethodInfo,
	&ICompiler_EmitPosition_m6583_MethodInfo,
	&ICompiler_EmitOpen_m6584_MethodInfo,
	&ICompiler_EmitClose_m6585_MethodInfo,
	&ICompiler_EmitBalanceStart_m6586_MethodInfo,
	&ICompiler_EmitBalance_m6587_MethodInfo,
	&ICompiler_EmitReference_m6588_MethodInfo,
	&ICompiler_EmitIfDefined_m6589_MethodInfo,
	&ICompiler_EmitSub_m6590_MethodInfo,
	&ICompiler_EmitTest_m6591_MethodInfo,
	&ICompiler_EmitBranch_m6592_MethodInfo,
	&ICompiler_EmitJump_m6593_MethodInfo,
	&ICompiler_EmitRepeat_m6594_MethodInfo,
	&ICompiler_EmitUntil_m6595_MethodInfo,
	&ICompiler_EmitIn_m6596_MethodInfo,
	&ICompiler_EmitInfo_m6597_MethodInfo,
	&ICompiler_EmitFastRepeat_m6598_MethodInfo,
	&ICompiler_EmitAnchor_m6599_MethodInfo,
	&ICompiler_EmitBranchEnd_m6600_MethodInfo,
	&ICompiler_EmitAlternationEnd_m6601_MethodInfo,
	&ICompiler_NewLink_m6602_MethodInfo,
	&ICompiler_ResolveLink_m6603_MethodInfo,
	NULL
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType ICompiler_t1439_1_0_0;
struct ICompiler_t1439;
const Il2CppTypeDefinitionMetadata ICompiler_t1439_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ICompiler_t1439_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICompiler"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, ICompiler_t1439_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ICompiler_t1439_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICompiler_t1439_0_0_0/* byval_arg */
	, &ICompiler_t1439_1_0_0/* this_arg */
	, &ICompiler_t1439_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.InterpreterFactory
#include "System_System_Text_RegularExpressions_InterpreterFactory.h"
// Metadata Definition System.Text.RegularExpressions.InterpreterFactory
extern TypeInfo InterpreterFactory_t1377_il2cpp_TypeInfo;
// System.Text.RegularExpressions.InterpreterFactory
#include "System_System_Text_RegularExpressions_InterpreterFactoryMethodDeclarations.h"
extern const Il2CppType UInt16U5BU5D_t1297_0_0_0;
extern const Il2CppType UInt16U5BU5D_t1297_0_0_0;
static const ParameterInfo InterpreterFactory_t1377_InterpreterFactory__ctor_m6015_ParameterInfos[] = 
{
	{"pattern", 0, 134218289, 0, &UInt16U5BU5D_t1297_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::.ctor(System.UInt16[])
extern const MethodInfo InterpreterFactory__ctor_m6015_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InterpreterFactory__ctor_m6015/* method */
	, &InterpreterFactory_t1377_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, InterpreterFactory_t1377_InterpreterFactory__ctor_m6015_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMachine_t1361_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachine System.Text.RegularExpressions.InterpreterFactory::NewInstance()
extern const MethodInfo InterpreterFactory_NewInstance_m6016_MethodInfo = 
{
	"NewInstance"/* name */
	, (methodPointerType)&InterpreterFactory_NewInstance_m6016/* method */
	, &InterpreterFactory_t1377_il2cpp_TypeInfo/* declaring_type */
	, &IMachine_t1361_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.InterpreterFactory::get_GroupCount()
extern const MethodInfo InterpreterFactory_get_GroupCount_m6017_MethodInfo = 
{
	"get_GroupCount"/* name */
	, (methodPointerType)&InterpreterFactory_get_GroupCount_m6017/* method */
	, &InterpreterFactory_t1377_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.InterpreterFactory::get_Gap()
extern const MethodInfo InterpreterFactory_get_Gap_m6018_MethodInfo = 
{
	"get_Gap"/* name */
	, (methodPointerType)&InterpreterFactory_get_Gap_m6018/* method */
	, &InterpreterFactory_t1377_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo InterpreterFactory_t1377_InterpreterFactory_set_Gap_m6019_ParameterInfos[] = 
{
	{"value", 0, 134218290, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_Gap(System.Int32)
extern const MethodInfo InterpreterFactory_set_Gap_m6019_MethodInfo = 
{
	"set_Gap"/* name */
	, (methodPointerType)&InterpreterFactory_set_Gap_m6019/* method */
	, &InterpreterFactory_t1377_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, InterpreterFactory_t1377_InterpreterFactory_set_Gap_m6019_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t444_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Text.RegularExpressions.InterpreterFactory::get_Mapping()
extern const MethodInfo InterpreterFactory_get_Mapping_m6020_MethodInfo = 
{
	"get_Mapping"/* name */
	, (methodPointerType)&InterpreterFactory_get_Mapping_m6020/* method */
	, &InterpreterFactory_t1377_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t444_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t444_0_0_0;
static const ParameterInfo InterpreterFactory_t1377_InterpreterFactory_set_Mapping_m6021_ParameterInfos[] = 
{
	{"value", 0, 134218291, 0, &IDictionary_t444_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_Mapping(System.Collections.IDictionary)
extern const MethodInfo InterpreterFactory_set_Mapping_m6021_MethodInfo = 
{
	"set_Mapping"/* name */
	, (methodPointerType)&InterpreterFactory_set_Mapping_m6021/* method */
	, &InterpreterFactory_t1377_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, InterpreterFactory_t1377_InterpreterFactory_set_Mapping_m6021_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t243_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String[] System.Text.RegularExpressions.InterpreterFactory::get_NamesMapping()
extern const MethodInfo InterpreterFactory_get_NamesMapping_m6022_MethodInfo = 
{
	"get_NamesMapping"/* name */
	, (methodPointerType)&InterpreterFactory_get_NamesMapping_m6022/* method */
	, &InterpreterFactory_t1377_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t243_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t243_0_0_0;
static const ParameterInfo InterpreterFactory_t1377_InterpreterFactory_set_NamesMapping_m6023_ParameterInfos[] = 
{
	{"value", 0, 134218292, 0, &StringU5BU5D_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_NamesMapping(System.String[])
extern const MethodInfo InterpreterFactory_set_NamesMapping_m6023_MethodInfo = 
{
	"set_NamesMapping"/* name */
	, (methodPointerType)&InterpreterFactory_set_NamesMapping_m6023/* method */
	, &InterpreterFactory_t1377_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, InterpreterFactory_t1377_InterpreterFactory_set_NamesMapping_m6023_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InterpreterFactory_t1377_MethodInfos[] =
{
	&InterpreterFactory__ctor_m6015_MethodInfo,
	&InterpreterFactory_NewInstance_m6016_MethodInfo,
	&InterpreterFactory_get_GroupCount_m6017_MethodInfo,
	&InterpreterFactory_get_Gap_m6018_MethodInfo,
	&InterpreterFactory_set_Gap_m6019_MethodInfo,
	&InterpreterFactory_get_Mapping_m6020_MethodInfo,
	&InterpreterFactory_set_Mapping_m6021_MethodInfo,
	&InterpreterFactory_get_NamesMapping_m6022_MethodInfo,
	&InterpreterFactory_set_NamesMapping_m6023_MethodInfo,
	NULL
};
extern const MethodInfo InterpreterFactory_get_GroupCount_m6017_MethodInfo;
static const PropertyInfo InterpreterFactory_t1377____GroupCount_PropertyInfo = 
{
	&InterpreterFactory_t1377_il2cpp_TypeInfo/* parent */
	, "GroupCount"/* name */
	, &InterpreterFactory_get_GroupCount_m6017_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo InterpreterFactory_get_Gap_m6018_MethodInfo;
extern const MethodInfo InterpreterFactory_set_Gap_m6019_MethodInfo;
static const PropertyInfo InterpreterFactory_t1377____Gap_PropertyInfo = 
{
	&InterpreterFactory_t1377_il2cpp_TypeInfo/* parent */
	, "Gap"/* name */
	, &InterpreterFactory_get_Gap_m6018_MethodInfo/* get */
	, &InterpreterFactory_set_Gap_m6019_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo InterpreterFactory_get_Mapping_m6020_MethodInfo;
extern const MethodInfo InterpreterFactory_set_Mapping_m6021_MethodInfo;
static const PropertyInfo InterpreterFactory_t1377____Mapping_PropertyInfo = 
{
	&InterpreterFactory_t1377_il2cpp_TypeInfo/* parent */
	, "Mapping"/* name */
	, &InterpreterFactory_get_Mapping_m6020_MethodInfo/* get */
	, &InterpreterFactory_set_Mapping_m6021_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo InterpreterFactory_get_NamesMapping_m6022_MethodInfo;
extern const MethodInfo InterpreterFactory_set_NamesMapping_m6023_MethodInfo;
static const PropertyInfo InterpreterFactory_t1377____NamesMapping_PropertyInfo = 
{
	&InterpreterFactory_t1377_il2cpp_TypeInfo/* parent */
	, "NamesMapping"/* name */
	, &InterpreterFactory_get_NamesMapping_m6022_MethodInfo/* get */
	, &InterpreterFactory_set_NamesMapping_m6023_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* InterpreterFactory_t1377_PropertyInfos[] =
{
	&InterpreterFactory_t1377____GroupCount_PropertyInfo,
	&InterpreterFactory_t1377____Gap_PropertyInfo,
	&InterpreterFactory_t1377____Mapping_PropertyInfo,
	&InterpreterFactory_t1377____NamesMapping_PropertyInfo,
	NULL
};
extern const MethodInfo InterpreterFactory_NewInstance_m6016_MethodInfo;
static const Il2CppMethodReference InterpreterFactory_t1377_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&InterpreterFactory_NewInstance_m6016_MethodInfo,
	&InterpreterFactory_get_Mapping_m6020_MethodInfo,
	&InterpreterFactory_set_Mapping_m6021_MethodInfo,
	&InterpreterFactory_get_GroupCount_m6017_MethodInfo,
	&InterpreterFactory_get_Gap_m6018_MethodInfo,
	&InterpreterFactory_set_Gap_m6019_MethodInfo,
	&InterpreterFactory_get_NamesMapping_m6022_MethodInfo,
	&InterpreterFactory_set_NamesMapping_m6023_MethodInfo,
};
static bool InterpreterFactory_t1377_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* InterpreterFactory_t1377_InterfacesTypeInfos[] = 
{
	&IMachineFactory_t1365_0_0_0,
};
static Il2CppInterfaceOffsetPair InterpreterFactory_t1377_InterfacesOffsets[] = 
{
	{ &IMachineFactory_t1365_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType InterpreterFactory_t1377_0_0_0;
extern const Il2CppType InterpreterFactory_t1377_1_0_0;
struct InterpreterFactory_t1377;
const Il2CppTypeDefinitionMetadata InterpreterFactory_t1377_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, InterpreterFactory_t1377_InterfacesTypeInfos/* implementedInterfaces */
	, InterpreterFactory_t1377_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InterpreterFactory_t1377_VTable/* vtableMethods */
	, InterpreterFactory_t1377_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 646/* fieldStart */

};
TypeInfo InterpreterFactory_t1377_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "InterpreterFactory"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, InterpreterFactory_t1377_MethodInfos/* methods */
	, InterpreterFactory_t1377_PropertyInfos/* properties */
	, NULL/* events */
	, &InterpreterFactory_t1377_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InterpreterFactory_t1377_0_0_0/* byval_arg */
	, &InterpreterFactory_t1377_1_0_0/* this_arg */
	, &InterpreterFactory_t1377_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InterpreterFactory_t1377)/* instance_size */
	, sizeof (InterpreterFactory_t1377)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
extern TypeInfo Link_t1378_il2cpp_TypeInfo;
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_PatterMethodDeclarations.h"
static const MethodInfo* Link_t1378_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m1076_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m1077_MethodInfo;
extern const MethodInfo ValueType_ToString_m1078_MethodInfo;
static const Il2CppMethodReference Link_t1378_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool Link_t1378_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Link_t1378_0_0_0;
extern const Il2CppType Link_t1378_1_0_0;
extern const Il2CppType ValueType_t285_0_0_0;
extern TypeInfo PatternLinkStack_t1379_il2cpp_TypeInfo;
extern const Il2CppType PatternLinkStack_t1379_0_0_0;
const Il2CppTypeDefinitionMetadata Link_t1378_DefinitionMetadata = 
{
	&PatternLinkStack_t1379_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, Link_t1378_VTable/* vtableMethods */
	, Link_t1378_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 650/* fieldStart */

};
TypeInfo Link_t1378_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link"/* name */
	, ""/* namespaze */
	, Link_t1378_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Link_t1378_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Link_t1378_0_0_0/* byval_arg */
	, &Link_t1378_1_0_0/* this_arg */
	, &Link_t1378_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Link_t1378)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Link_t1378)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Link_t1378 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter_0.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::.ctor()
extern const MethodInfo PatternLinkStack__ctor_m6024_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PatternLinkStack__ctor_m6024/* method */
	, &PatternLinkStack_t1379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo PatternLinkStack_t1379_PatternLinkStack_set_BaseAddress_m6025_ParameterInfos[] = 
{
	{"value", 0, 134218363, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::set_BaseAddress(System.Int32)
extern const MethodInfo PatternLinkStack_set_BaseAddress_m6025_MethodInfo = 
{
	"set_BaseAddress"/* name */
	, (methodPointerType)&PatternLinkStack_set_BaseAddress_m6025/* method */
	, &PatternLinkStack_t1379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, PatternLinkStack_t1379_PatternLinkStack_set_BaseAddress_m6025_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::get_OffsetAddress()
extern const MethodInfo PatternLinkStack_get_OffsetAddress_m6026_MethodInfo = 
{
	"get_OffsetAddress"/* name */
	, (methodPointerType)&PatternLinkStack_get_OffsetAddress_m6026/* method */
	, &PatternLinkStack_t1379_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo PatternLinkStack_t1379_PatternLinkStack_set_OffsetAddress_m6027_ParameterInfos[] = 
{
	{"value", 0, 134218364, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::set_OffsetAddress(System.Int32)
extern const MethodInfo PatternLinkStack_set_OffsetAddress_m6027_MethodInfo = 
{
	"set_OffsetAddress"/* name */
	, (methodPointerType)&PatternLinkStack_set_OffsetAddress_m6027/* method */
	, &PatternLinkStack_t1379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, PatternLinkStack_t1379_PatternLinkStack_set_OffsetAddress_m6027_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo PatternLinkStack_t1379_PatternLinkStack_GetOffset_m6028_ParameterInfos[] = 
{
	{"target_addr", 0, 134218365, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::GetOffset(System.Int32)
extern const MethodInfo PatternLinkStack_GetOffset_m6028_MethodInfo = 
{
	"GetOffset"/* name */
	, (methodPointerType)&PatternLinkStack_GetOffset_m6028/* method */
	, &PatternLinkStack_t1379_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int32_t253/* invoker_method */
	, PatternLinkStack_t1379_PatternLinkStack_GetOffset_m6028_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::GetCurrent()
extern const MethodInfo PatternLinkStack_GetCurrent_m6029_MethodInfo = 
{
	"GetCurrent"/* name */
	, (methodPointerType)&PatternLinkStack_GetCurrent_m6029/* method */
	, &PatternLinkStack_t1379_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PatternLinkStack_t1379_PatternLinkStack_SetCurrent_m6030_ParameterInfos[] = 
{
	{"l", 0, 134218366, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::SetCurrent(System.Object)
extern const MethodInfo PatternLinkStack_SetCurrent_m6030_MethodInfo = 
{
	"SetCurrent"/* name */
	, (methodPointerType)&PatternLinkStack_SetCurrent_m6030/* method */
	, &PatternLinkStack_t1379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, PatternLinkStack_t1379_PatternLinkStack_SetCurrent_m6030_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PatternLinkStack_t1379_MethodInfos[] =
{
	&PatternLinkStack__ctor_m6024_MethodInfo,
	&PatternLinkStack_set_BaseAddress_m6025_MethodInfo,
	&PatternLinkStack_get_OffsetAddress_m6026_MethodInfo,
	&PatternLinkStack_set_OffsetAddress_m6027_MethodInfo,
	&PatternLinkStack_GetOffset_m6028_MethodInfo,
	&PatternLinkStack_GetCurrent_m6029_MethodInfo,
	&PatternLinkStack_SetCurrent_m6030_MethodInfo,
	NULL
};
extern const MethodInfo PatternLinkStack_set_BaseAddress_m6025_MethodInfo;
static const PropertyInfo PatternLinkStack_t1379____BaseAddress_PropertyInfo = 
{
	&PatternLinkStack_t1379_il2cpp_TypeInfo/* parent */
	, "BaseAddress"/* name */
	, NULL/* get */
	, &PatternLinkStack_set_BaseAddress_m6025_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PatternLinkStack_get_OffsetAddress_m6026_MethodInfo;
extern const MethodInfo PatternLinkStack_set_OffsetAddress_m6027_MethodInfo;
static const PropertyInfo PatternLinkStack_t1379____OffsetAddress_PropertyInfo = 
{
	&PatternLinkStack_t1379_il2cpp_TypeInfo/* parent */
	, "OffsetAddress"/* name */
	, &PatternLinkStack_get_OffsetAddress_m6026_MethodInfo/* get */
	, &PatternLinkStack_set_OffsetAddress_m6027_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PatternLinkStack_t1379_PropertyInfos[] =
{
	&PatternLinkStack_t1379____BaseAddress_PropertyInfo,
	&PatternLinkStack_t1379____OffsetAddress_PropertyInfo,
	NULL
};
static const Il2CppType* PatternLinkStack_t1379_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Link_t1378_0_0_0,
};
extern const MethodInfo PatternLinkStack_GetCurrent_m6029_MethodInfo;
extern const MethodInfo PatternLinkStack_SetCurrent_m6030_MethodInfo;
static const Il2CppMethodReference PatternLinkStack_t1379_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&PatternLinkStack_GetCurrent_m6029_MethodInfo,
	&PatternLinkStack_SetCurrent_m6030_MethodInfo,
};
static bool PatternLinkStack_t1379_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PatternLinkStack_t1379_1_0_0;
extern const Il2CppType LinkStack_t1380_0_0_0;
extern TypeInfo PatternCompiler_t1381_il2cpp_TypeInfo;
extern const Il2CppType PatternCompiler_t1381_0_0_0;
struct PatternLinkStack_t1379;
const Il2CppTypeDefinitionMetadata PatternLinkStack_t1379_DefinitionMetadata = 
{
	&PatternCompiler_t1381_0_0_0/* declaringType */
	, PatternLinkStack_t1379_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &LinkStack_t1380_0_0_0/* parent */
	, PatternLinkStack_t1379_VTable/* vtableMethods */
	, PatternLinkStack_t1379_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 652/* fieldStart */

};
TypeInfo PatternLinkStack_t1379_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PatternLinkStack"/* name */
	, ""/* namespaze */
	, PatternLinkStack_t1379_MethodInfos/* methods */
	, PatternLinkStack_t1379_PropertyInfos/* properties */
	, NULL/* events */
	, &PatternLinkStack_t1379_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PatternLinkStack_t1379_0_0_0/* byval_arg */
	, &PatternLinkStack_t1379_1_0_0/* this_arg */
	, &PatternLinkStack_t1379_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PatternLinkStack_t1379)/* instance_size */
	, sizeof (PatternLinkStack_t1379)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler
#include "System_System_Text_RegularExpressions_PatternCompiler.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler
// System.Text.RegularExpressions.PatternCompiler
#include "System_System_Text_RegularExpressions_PatternCompilerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::.ctor()
extern const MethodInfo PatternCompiler__ctor_m6031_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PatternCompiler__ctor_m6031/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType OpCode_t1368_0_0_0;
extern const Il2CppType OpCode_t1368_0_0_0;
extern const Il2CppType OpFlags_t1369_0_0_0;
extern const Il2CppType OpFlags_t1369_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EncodeOp_m6032_ParameterInfos[] = 
{
	{"op", 0, 134218293, 0, &OpCode_t1368_0_0_0},
	{"flags", 1, 134218294, 0, &OpFlags_t1369_0_0_0},
};
extern const Il2CppType UInt16_t684_0_0_0;
extern void* RuntimeInvoker_UInt16_t684_UInt16_t684_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
// System.UInt16 System.Text.RegularExpressions.PatternCompiler::EncodeOp(System.Text.RegularExpressions.OpCode,System.Text.RegularExpressions.OpFlags)
extern const MethodInfo PatternCompiler_EncodeOp_m6032_MethodInfo = 
{
	"EncodeOp"/* name */
	, (methodPointerType)&PatternCompiler_EncodeOp_m6032/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &UInt16_t684_0_0_0/* return_type */
	, RuntimeInvoker_UInt16_t684_UInt16_t684_UInt16_t684/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EncodeOp_m6032_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.PatternCompiler::GetMachineFactory()
extern const MethodInfo PatternCompiler_GetMachineFactory_m6033_MethodInfo = 
{
	"GetMachineFactory"/* name */
	, (methodPointerType)&PatternCompiler_GetMachineFactory_m6033/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t1365_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitFalse()
extern const MethodInfo PatternCompiler_EmitFalse_m6034_MethodInfo = 
{
	"EmitFalse"/* name */
	, (methodPointerType)&PatternCompiler_EmitFalse_m6034/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitTrue()
extern const MethodInfo PatternCompiler_EmitTrue_m6035_MethodInfo = 
{
	"EmitTrue"/* name */
	, (methodPointerType)&PatternCompiler_EmitTrue_m6035/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitCount_m6036_ParameterInfos[] = 
{
	{"count", 0, 134218295, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCount(System.Int32)
extern const MethodInfo PatternCompiler_EmitCount_m6036_MethodInfo = 
{
	"EmitCount"/* name */
	, (methodPointerType)&PatternCompiler_EmitCount_m6036/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitCount_m6036_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitCharacter_m6037_ParameterInfos[] = 
{
	{"c", 0, 134218296, 0, &Char_t682_0_0_0},
	{"negate", 1, 134218297, 0, &Boolean_t273_0_0_0},
	{"ignore", 2, 134218298, 0, &Boolean_t273_0_0_0},
	{"reverse", 3, 134218299, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCharacter(System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitCharacter_m6037_MethodInfo = 
{
	"EmitCharacter"/* name */
	, (methodPointerType)&PatternCompiler_EmitCharacter_m6037/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752_SByte_t274_SByte_t274_SByte_t274/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitCharacter_m6037_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1374_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitCategory_m6038_ParameterInfos[] = 
{
	{"cat", 0, 134218300, 0, &Category_t1374_0_0_0},
	{"negate", 1, 134218301, 0, &Boolean_t273_0_0_0},
	{"reverse", 2, 134218302, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitCategory_m6038_MethodInfo = 
{
	"EmitCategory"/* name */
	, (methodPointerType)&PatternCompiler_EmitCategory_m6038/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274_SByte_t274/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitCategory_m6038_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1374_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitNotCategory_m6039_ParameterInfos[] = 
{
	{"cat", 0, 134218303, 0, &Category_t1374_0_0_0},
	{"negate", 1, 134218304, 0, &Boolean_t273_0_0_0},
	{"reverse", 2, 134218305, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitNotCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitNotCategory_m6039_MethodInfo = 
{
	"EmitNotCategory"/* name */
	, (methodPointerType)&PatternCompiler_EmitNotCategory_m6039/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274_SByte_t274/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitNotCategory_m6039_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitRange_m6040_ParameterInfos[] = 
{
	{"lo", 0, 134218306, 0, &Char_t682_0_0_0},
	{"hi", 1, 134218307, 0, &Char_t682_0_0_0},
	{"negate", 2, 134218308, 0, &Boolean_t273_0_0_0},
	{"ignore", 3, 134218309, 0, &Boolean_t273_0_0_0},
	{"reverse", 4, 134218310, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752_Int16_t752_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitRange(System.Char,System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitRange_m6040_MethodInfo = 
{
	"EmitRange"/* name */
	, (methodPointerType)&PatternCompiler_EmitRange_m6040/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752_Int16_t752_SByte_t274_SByte_t274_SByte_t274/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitRange_m6040_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType BitArray_t1412_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitSet_m6041_ParameterInfos[] = 
{
	{"lo", 0, 134218311, 0, &Char_t682_0_0_0},
	{"set", 1, 134218312, 0, &BitArray_t1412_0_0_0},
	{"negate", 2, 134218313, 0, &Boolean_t273_0_0_0},
	{"ignore", 3, 134218314, 0, &Boolean_t273_0_0_0},
	{"reverse", 4, 134218315, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752_Object_t_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitSet(System.Char,System.Collections.BitArray,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitSet_m6041_MethodInfo = 
{
	"EmitSet"/* name */
	, (methodPointerType)&PatternCompiler_EmitSet_m6041/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752_Object_t_SByte_t274_SByte_t274_SByte_t274/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitSet_m6041_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitString_m6042_ParameterInfos[] = 
{
	{"str", 0, 134218316, 0, &String_t_0_0_0},
	{"ignore", 1, 134218317, 0, &Boolean_t273_0_0_0},
	{"reverse", 2, 134218318, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitString(System.String,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitString_m6042_MethodInfo = 
{
	"EmitString"/* name */
	, (methodPointerType)&PatternCompiler_EmitString_m6042/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitString_m6042_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Position_t1370_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitPosition_m6043_ParameterInfos[] = 
{
	{"pos", 0, 134218319, 0, &Position_t1370_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitPosition(System.Text.RegularExpressions.Position)
extern const MethodInfo PatternCompiler_EmitPosition_m6043_MethodInfo = 
{
	"EmitPosition"/* name */
	, (methodPointerType)&PatternCompiler_EmitPosition_m6043/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitPosition_m6043_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitOpen_m6044_ParameterInfos[] = 
{
	{"gid", 0, 134218320, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitOpen(System.Int32)
extern const MethodInfo PatternCompiler_EmitOpen_m6044_MethodInfo = 
{
	"EmitOpen"/* name */
	, (methodPointerType)&PatternCompiler_EmitOpen_m6044/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitOpen_m6044_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitClose_m6045_ParameterInfos[] = 
{
	{"gid", 0, 134218321, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitClose(System.Int32)
extern const MethodInfo PatternCompiler_EmitClose_m6045_MethodInfo = 
{
	"EmitClose"/* name */
	, (methodPointerType)&PatternCompiler_EmitClose_m6045/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitClose_m6045_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitBalanceStart_m6046_ParameterInfos[] = 
{
	{"gid", 0, 134218322, 0, &Int32_t253_0_0_0},
	{"balance", 1, 134218323, 0, &Int32_t253_0_0_0},
	{"capture", 2, 134218324, 0, &Boolean_t273_0_0_0},
	{"tail", 3, 134218325, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBalanceStart(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitBalanceStart_m6046_MethodInfo = 
{
	"EmitBalanceStart"/* name */
	, (methodPointerType)&PatternCompiler_EmitBalanceStart_m6046/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitBalanceStart_m6046_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBalance()
extern const MethodInfo PatternCompiler_EmitBalance_m6047_MethodInfo = 
{
	"EmitBalance"/* name */
	, (methodPointerType)&PatternCompiler_EmitBalance_m6047/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitReference_m6048_ParameterInfos[] = 
{
	{"gid", 0, 134218326, 0, &Int32_t253_0_0_0},
	{"ignore", 1, 134218327, 0, &Boolean_t273_0_0_0},
	{"reverse", 2, 134218328, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitReference(System.Int32,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitReference_m6048_MethodInfo = 
{
	"EmitReference"/* name */
	, (methodPointerType)&PatternCompiler_EmitReference_m6048/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_SByte_t274_SByte_t274/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitReference_m6048_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitIfDefined_m6049_ParameterInfos[] = 
{
	{"gid", 0, 134218329, 0, &Int32_t253_0_0_0},
	{"tail", 1, 134218330, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitIfDefined(System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitIfDefined_m6049_MethodInfo = 
{
	"EmitIfDefined"/* name */
	, (methodPointerType)&PatternCompiler_EmitIfDefined_m6049/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitIfDefined_m6049_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitSub_m6050_ParameterInfos[] = 
{
	{"tail", 0, 134218331, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitSub(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitSub_m6050_MethodInfo = 
{
	"EmitSub"/* name */
	, (methodPointerType)&PatternCompiler_EmitSub_m6050/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitSub_m6050_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitTest_m6051_ParameterInfos[] = 
{
	{"yes", 0, 134218332, 0, &LinkRef_t1376_0_0_0},
	{"tail", 1, 134218333, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitTest(System.Text.RegularExpressions.LinkRef,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitTest_m6051_MethodInfo = 
{
	"EmitTest"/* name */
	, (methodPointerType)&PatternCompiler_EmitTest_m6051/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitTest_m6051_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitBranch_m6052_ParameterInfos[] = 
{
	{"next", 0, 134218334, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBranch(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitBranch_m6052_MethodInfo = 
{
	"EmitBranch"/* name */
	, (methodPointerType)&PatternCompiler_EmitBranch_m6052/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitBranch_m6052_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitJump_m6053_ParameterInfos[] = 
{
	{"target", 0, 134218335, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitJump(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitJump_m6053_MethodInfo = 
{
	"EmitJump"/* name */
	, (methodPointerType)&PatternCompiler_EmitJump_m6053/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitJump_m6053_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitRepeat_m6054_ParameterInfos[] = 
{
	{"min", 0, 134218336, 0, &Int32_t253_0_0_0},
	{"max", 1, 134218337, 0, &Int32_t253_0_0_0},
	{"lazy", 2, 134218338, 0, &Boolean_t273_0_0_0},
	{"until", 3, 134218339, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitRepeat_m6054_MethodInfo = 
{
	"EmitRepeat"/* name */
	, (methodPointerType)&PatternCompiler_EmitRepeat_m6054/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitRepeat_m6054_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitUntil_m6055_ParameterInfos[] = 
{
	{"repeat", 0, 134218340, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitUntil(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitUntil_m6055_MethodInfo = 
{
	"EmitUntil"/* name */
	, (methodPointerType)&PatternCompiler_EmitUntil_m6055/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitUntil_m6055_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitFastRepeat_m6056_ParameterInfos[] = 
{
	{"min", 0, 134218341, 0, &Int32_t253_0_0_0},
	{"max", 1, 134218342, 0, &Int32_t253_0_0_0},
	{"lazy", 2, 134218343, 0, &Boolean_t273_0_0_0},
	{"tail", 3, 134218344, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitFastRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitFastRepeat_m6056_MethodInfo = 
{
	"EmitFastRepeat"/* name */
	, (methodPointerType)&PatternCompiler_EmitFastRepeat_m6056/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitFastRepeat_m6056_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitIn_m6057_ParameterInfos[] = 
{
	{"tail", 0, 134218345, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitIn(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitIn_m6057_MethodInfo = 
{
	"EmitIn"/* name */
	, (methodPointerType)&PatternCompiler_EmitIn_m6057/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitIn_m6057_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitAnchor_m6058_ParameterInfos[] = 
{
	{"reverse", 0, 134218346, 0, &Boolean_t273_0_0_0},
	{"offset", 1, 134218347, 0, &Int32_t253_0_0_0},
	{"tail", 2, 134218348, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitAnchor(System.Boolean,System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitAnchor_m6058_MethodInfo = 
{
	"EmitAnchor"/* name */
	, (methodPointerType)&PatternCompiler_EmitAnchor_m6058/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274_Int32_t253_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitAnchor_m6058_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitInfo_m6059_ParameterInfos[] = 
{
	{"count", 0, 134218349, 0, &Int32_t253_0_0_0},
	{"min", 1, 134218350, 0, &Int32_t253_0_0_0},
	{"max", 2, 134218351, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitInfo(System.Int32,System.Int32,System.Int32)
extern const MethodInfo PatternCompiler_EmitInfo_m6059_MethodInfo = 
{
	"EmitInfo"/* name */
	, (methodPointerType)&PatternCompiler_EmitInfo_m6059/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitInfo_m6059_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.PatternCompiler::NewLink()
extern const MethodInfo PatternCompiler_NewLink_m6060_MethodInfo = 
{
	"NewLink"/* name */
	, (methodPointerType)&PatternCompiler_NewLink_m6060/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &LinkRef_t1376_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_ResolveLink_m6061_ParameterInfos[] = 
{
	{"lref", 0, 134218352, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::ResolveLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_ResolveLink_m6061_MethodInfo = 
{
	"ResolveLink"/* name */
	, (methodPointerType)&PatternCompiler_ResolveLink_m6061/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_ResolveLink_m6061_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBranchEnd()
extern const MethodInfo PatternCompiler_EmitBranchEnd_m6062_MethodInfo = 
{
	"EmitBranchEnd"/* name */
	, (methodPointerType)&PatternCompiler_EmitBranchEnd_m6062/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitAlternationEnd()
extern const MethodInfo PatternCompiler_EmitAlternationEnd_m6063_MethodInfo = 
{
	"EmitAlternationEnd"/* name */
	, (methodPointerType)&PatternCompiler_EmitAlternationEnd_m6063/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_MakeFlags_m6064_ParameterInfos[] = 
{
	{"negate", 0, 134218353, 0, &Boolean_t273_0_0_0},
	{"ignore", 1, 134218354, 0, &Boolean_t273_0_0_0},
	{"reverse", 2, 134218355, 0, &Boolean_t273_0_0_0},
	{"lazy", 3, 134218356, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_OpFlags_t1369_SByte_t274_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.OpFlags System.Text.RegularExpressions.PatternCompiler::MakeFlags(System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_MakeFlags_m6064_MethodInfo = 
{
	"MakeFlags"/* name */
	, (methodPointerType)&PatternCompiler_MakeFlags_m6064/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &OpFlags_t1369_0_0_0/* return_type */
	, RuntimeInvoker_OpFlags_t1369_SByte_t274_SByte_t274_SByte_t274_SByte_t274/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_MakeFlags_m6064_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType OpCode_t1368_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_Emit_m6065_ParameterInfos[] = 
{
	{"op", 0, 134218357, 0, &OpCode_t1368_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.Text.RegularExpressions.OpCode)
extern const MethodInfo PatternCompiler_Emit_m6065_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m6065/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_Emit_m6065_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType OpCode_t1368_0_0_0;
extern const Il2CppType OpFlags_t1369_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_Emit_m6066_ParameterInfos[] = 
{
	{"op", 0, 134218358, 0, &OpCode_t1368_0_0_0},
	{"flags", 1, 134218359, 0, &OpFlags_t1369_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.Text.RegularExpressions.OpCode,System.Text.RegularExpressions.OpFlags)
extern const MethodInfo PatternCompiler_Emit_m6066_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m6066/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684_UInt16_t684/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_Emit_m6066_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt16_t684_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_Emit_m6067_ParameterInfos[] = 
{
	{"word", 0, 134218360, 0, &UInt16_t684_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.UInt16)
extern const MethodInfo PatternCompiler_Emit_m6067_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m6067/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_Emit_m6067_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler::get_CurrentAddress()
extern const MethodInfo PatternCompiler_get_CurrentAddress_m6068_MethodInfo = 
{
	"get_CurrentAddress"/* name */
	, (methodPointerType)&PatternCompiler_get_CurrentAddress_m6068/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_BeginLink_m6069_ParameterInfos[] = 
{
	{"lref", 0, 134218361, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::BeginLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_BeginLink_m6069_MethodInfo = 
{
	"BeginLink"/* name */
	, (methodPointerType)&PatternCompiler_BeginLink_m6069/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_BeginLink_m6069_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1376_0_0_0;
static const ParameterInfo PatternCompiler_t1381_PatternCompiler_EmitLink_m6070_ParameterInfos[] = 
{
	{"lref", 0, 134218362, 0, &LinkRef_t1376_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitLink_m6070_MethodInfo = 
{
	"EmitLink"/* name */
	, (methodPointerType)&PatternCompiler_EmitLink_m6070/* method */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, PatternCompiler_t1381_PatternCompiler_EmitLink_m6070_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PatternCompiler_t1381_MethodInfos[] =
{
	&PatternCompiler__ctor_m6031_MethodInfo,
	&PatternCompiler_EncodeOp_m6032_MethodInfo,
	&PatternCompiler_GetMachineFactory_m6033_MethodInfo,
	&PatternCompiler_EmitFalse_m6034_MethodInfo,
	&PatternCompiler_EmitTrue_m6035_MethodInfo,
	&PatternCompiler_EmitCount_m6036_MethodInfo,
	&PatternCompiler_EmitCharacter_m6037_MethodInfo,
	&PatternCompiler_EmitCategory_m6038_MethodInfo,
	&PatternCompiler_EmitNotCategory_m6039_MethodInfo,
	&PatternCompiler_EmitRange_m6040_MethodInfo,
	&PatternCompiler_EmitSet_m6041_MethodInfo,
	&PatternCompiler_EmitString_m6042_MethodInfo,
	&PatternCompiler_EmitPosition_m6043_MethodInfo,
	&PatternCompiler_EmitOpen_m6044_MethodInfo,
	&PatternCompiler_EmitClose_m6045_MethodInfo,
	&PatternCompiler_EmitBalanceStart_m6046_MethodInfo,
	&PatternCompiler_EmitBalance_m6047_MethodInfo,
	&PatternCompiler_EmitReference_m6048_MethodInfo,
	&PatternCompiler_EmitIfDefined_m6049_MethodInfo,
	&PatternCompiler_EmitSub_m6050_MethodInfo,
	&PatternCompiler_EmitTest_m6051_MethodInfo,
	&PatternCompiler_EmitBranch_m6052_MethodInfo,
	&PatternCompiler_EmitJump_m6053_MethodInfo,
	&PatternCompiler_EmitRepeat_m6054_MethodInfo,
	&PatternCompiler_EmitUntil_m6055_MethodInfo,
	&PatternCompiler_EmitFastRepeat_m6056_MethodInfo,
	&PatternCompiler_EmitIn_m6057_MethodInfo,
	&PatternCompiler_EmitAnchor_m6058_MethodInfo,
	&PatternCompiler_EmitInfo_m6059_MethodInfo,
	&PatternCompiler_NewLink_m6060_MethodInfo,
	&PatternCompiler_ResolveLink_m6061_MethodInfo,
	&PatternCompiler_EmitBranchEnd_m6062_MethodInfo,
	&PatternCompiler_EmitAlternationEnd_m6063_MethodInfo,
	&PatternCompiler_MakeFlags_m6064_MethodInfo,
	&PatternCompiler_Emit_m6065_MethodInfo,
	&PatternCompiler_Emit_m6066_MethodInfo,
	&PatternCompiler_Emit_m6067_MethodInfo,
	&PatternCompiler_get_CurrentAddress_m6068_MethodInfo,
	&PatternCompiler_BeginLink_m6069_MethodInfo,
	&PatternCompiler_EmitLink_m6070_MethodInfo,
	NULL
};
extern const MethodInfo PatternCompiler_get_CurrentAddress_m6068_MethodInfo;
static const PropertyInfo PatternCompiler_t1381____CurrentAddress_PropertyInfo = 
{
	&PatternCompiler_t1381_il2cpp_TypeInfo/* parent */
	, "CurrentAddress"/* name */
	, &PatternCompiler_get_CurrentAddress_m6068_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PatternCompiler_t1381_PropertyInfos[] =
{
	&PatternCompiler_t1381____CurrentAddress_PropertyInfo,
	NULL
};
static const Il2CppType* PatternCompiler_t1381_il2cpp_TypeInfo__nestedTypes[1] =
{
	&PatternLinkStack_t1379_0_0_0,
};
extern const MethodInfo PatternCompiler_GetMachineFactory_m6033_MethodInfo;
extern const MethodInfo PatternCompiler_EmitFalse_m6034_MethodInfo;
extern const MethodInfo PatternCompiler_EmitTrue_m6035_MethodInfo;
extern const MethodInfo PatternCompiler_EmitCharacter_m6037_MethodInfo;
extern const MethodInfo PatternCompiler_EmitCategory_m6038_MethodInfo;
extern const MethodInfo PatternCompiler_EmitNotCategory_m6039_MethodInfo;
extern const MethodInfo PatternCompiler_EmitRange_m6040_MethodInfo;
extern const MethodInfo PatternCompiler_EmitSet_m6041_MethodInfo;
extern const MethodInfo PatternCompiler_EmitString_m6042_MethodInfo;
extern const MethodInfo PatternCompiler_EmitPosition_m6043_MethodInfo;
extern const MethodInfo PatternCompiler_EmitOpen_m6044_MethodInfo;
extern const MethodInfo PatternCompiler_EmitClose_m6045_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBalanceStart_m6046_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBalance_m6047_MethodInfo;
extern const MethodInfo PatternCompiler_EmitReference_m6048_MethodInfo;
extern const MethodInfo PatternCompiler_EmitIfDefined_m6049_MethodInfo;
extern const MethodInfo PatternCompiler_EmitSub_m6050_MethodInfo;
extern const MethodInfo PatternCompiler_EmitTest_m6051_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBranch_m6052_MethodInfo;
extern const MethodInfo PatternCompiler_EmitJump_m6053_MethodInfo;
extern const MethodInfo PatternCompiler_EmitRepeat_m6054_MethodInfo;
extern const MethodInfo PatternCompiler_EmitUntil_m6055_MethodInfo;
extern const MethodInfo PatternCompiler_EmitIn_m6057_MethodInfo;
extern const MethodInfo PatternCompiler_EmitInfo_m6059_MethodInfo;
extern const MethodInfo PatternCompiler_EmitFastRepeat_m6056_MethodInfo;
extern const MethodInfo PatternCompiler_EmitAnchor_m6058_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBranchEnd_m6062_MethodInfo;
extern const MethodInfo PatternCompiler_EmitAlternationEnd_m6063_MethodInfo;
extern const MethodInfo PatternCompiler_NewLink_m6060_MethodInfo;
extern const MethodInfo PatternCompiler_ResolveLink_m6061_MethodInfo;
static const Il2CppMethodReference PatternCompiler_t1381_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&PatternCompiler_GetMachineFactory_m6033_MethodInfo,
	&PatternCompiler_EmitFalse_m6034_MethodInfo,
	&PatternCompiler_EmitTrue_m6035_MethodInfo,
	&PatternCompiler_EmitCharacter_m6037_MethodInfo,
	&PatternCompiler_EmitCategory_m6038_MethodInfo,
	&PatternCompiler_EmitNotCategory_m6039_MethodInfo,
	&PatternCompiler_EmitRange_m6040_MethodInfo,
	&PatternCompiler_EmitSet_m6041_MethodInfo,
	&PatternCompiler_EmitString_m6042_MethodInfo,
	&PatternCompiler_EmitPosition_m6043_MethodInfo,
	&PatternCompiler_EmitOpen_m6044_MethodInfo,
	&PatternCompiler_EmitClose_m6045_MethodInfo,
	&PatternCompiler_EmitBalanceStart_m6046_MethodInfo,
	&PatternCompiler_EmitBalance_m6047_MethodInfo,
	&PatternCompiler_EmitReference_m6048_MethodInfo,
	&PatternCompiler_EmitIfDefined_m6049_MethodInfo,
	&PatternCompiler_EmitSub_m6050_MethodInfo,
	&PatternCompiler_EmitTest_m6051_MethodInfo,
	&PatternCompiler_EmitBranch_m6052_MethodInfo,
	&PatternCompiler_EmitJump_m6053_MethodInfo,
	&PatternCompiler_EmitRepeat_m6054_MethodInfo,
	&PatternCompiler_EmitUntil_m6055_MethodInfo,
	&PatternCompiler_EmitIn_m6057_MethodInfo,
	&PatternCompiler_EmitInfo_m6059_MethodInfo,
	&PatternCompiler_EmitFastRepeat_m6056_MethodInfo,
	&PatternCompiler_EmitAnchor_m6058_MethodInfo,
	&PatternCompiler_EmitBranchEnd_m6062_MethodInfo,
	&PatternCompiler_EmitAlternationEnd_m6063_MethodInfo,
	&PatternCompiler_NewLink_m6060_MethodInfo,
	&PatternCompiler_ResolveLink_m6061_MethodInfo,
};
static bool PatternCompiler_t1381_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* PatternCompiler_t1381_InterfacesTypeInfos[] = 
{
	&ICompiler_t1439_0_0_0,
};
static Il2CppInterfaceOffsetPair PatternCompiler_t1381_InterfacesOffsets[] = 
{
	{ &ICompiler_t1439_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PatternCompiler_t1381_1_0_0;
struct PatternCompiler_t1381;
const Il2CppTypeDefinitionMetadata PatternCompiler_t1381_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PatternCompiler_t1381_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, PatternCompiler_t1381_InterfacesTypeInfos/* implementedInterfaces */
	, PatternCompiler_t1381_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PatternCompiler_t1381_VTable/* vtableMethods */
	, PatternCompiler_t1381_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 653/* fieldStart */

};
TypeInfo PatternCompiler_t1381_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PatternCompiler"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, PatternCompiler_t1381_MethodInfos/* methods */
	, PatternCompiler_t1381_PropertyInfos/* properties */
	, NULL/* events */
	, &PatternCompiler_t1381_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PatternCompiler_t1381_0_0_0/* byval_arg */
	, &PatternCompiler_t1381_1_0_0/* this_arg */
	, &PatternCompiler_t1381_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PatternCompiler_t1381)/* instance_size */
	, sizeof (PatternCompiler_t1381)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 40/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 34/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStack.h"
// Metadata Definition System.Text.RegularExpressions.LinkStack
extern TypeInfo LinkStack_t1380_il2cpp_TypeInfo;
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStackMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::.ctor()
extern const MethodInfo LinkStack__ctor_m6071_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkStack__ctor_m6071/* method */
	, &LinkStack_t1380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::Push()
extern const MethodInfo LinkStack_Push_m6072_MethodInfo = 
{
	"Push"/* name */
	, (methodPointerType)&LinkStack_Push_m6072/* method */
	, &LinkStack_t1380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.LinkStack::Pop()
extern const MethodInfo LinkStack_Pop_m6073_MethodInfo = 
{
	"Pop"/* name */
	, (methodPointerType)&LinkStack_Pop_m6073/* method */
	, &LinkStack_t1380_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.LinkStack::GetCurrent()
extern const MethodInfo LinkStack_GetCurrent_m6604_MethodInfo = 
{
	"GetCurrent"/* name */
	, NULL/* method */
	, &LinkStack_t1380_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo LinkStack_t1380_LinkStack_SetCurrent_m6605_ParameterInfos[] = 
{
	{"l", 0, 134218367, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::SetCurrent(System.Object)
extern const MethodInfo LinkStack_SetCurrent_m6605_MethodInfo = 
{
	"SetCurrent"/* name */
	, NULL/* method */
	, &LinkStack_t1380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LinkStack_t1380_LinkStack_SetCurrent_m6605_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LinkStack_t1380_MethodInfos[] =
{
	&LinkStack__ctor_m6071_MethodInfo,
	&LinkStack_Push_m6072_MethodInfo,
	&LinkStack_Pop_m6073_MethodInfo,
	&LinkStack_GetCurrent_m6604_MethodInfo,
	&LinkStack_SetCurrent_m6605_MethodInfo,
	NULL
};
static const Il2CppMethodReference LinkStack_t1380_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
	NULL,
};
static bool LinkStack_t1380_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType LinkStack_t1380_1_0_0;
struct LinkStack_t1380;
const Il2CppTypeDefinitionMetadata LinkStack_t1380_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &LinkRef_t1376_0_0_0/* parent */
	, LinkStack_t1380_VTable/* vtableMethods */
	, LinkStack_t1380_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 654/* fieldStart */

};
TypeInfo LinkStack_t1380_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkStack"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, LinkStack_t1380_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LinkStack_t1380_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LinkStack_t1380_0_0_0/* byval_arg */
	, &LinkStack_t1380_1_0_0/* this_arg */
	, &LinkStack_t1380_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkStack_t1380)/* instance_size */
	, sizeof (LinkStack_t1380)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
// Metadata Definition System.Text.RegularExpressions.Mark
extern TypeInfo Mark_t1382_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_MarkMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Mark::get_IsDefined()
extern const MethodInfo Mark_get_IsDefined_m6074_MethodInfo = 
{
	"get_IsDefined"/* name */
	, (methodPointerType)&Mark_get_IsDefined_m6074/* method */
	, &Mark_t1382_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Mark::get_Index()
extern const MethodInfo Mark_get_Index_m6075_MethodInfo = 
{
	"get_Index"/* name */
	, (methodPointerType)&Mark_get_Index_m6075/* method */
	, &Mark_t1382_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Mark::get_Length()
extern const MethodInfo Mark_get_Length_m6076_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&Mark_get_Length_m6076/* method */
	, &Mark_t1382_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Mark_t1382_MethodInfos[] =
{
	&Mark_get_IsDefined_m6074_MethodInfo,
	&Mark_get_Index_m6075_MethodInfo,
	&Mark_get_Length_m6076_MethodInfo,
	NULL
};
extern const MethodInfo Mark_get_IsDefined_m6074_MethodInfo;
static const PropertyInfo Mark_t1382____IsDefined_PropertyInfo = 
{
	&Mark_t1382_il2cpp_TypeInfo/* parent */
	, "IsDefined"/* name */
	, &Mark_get_IsDefined_m6074_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mark_get_Index_m6075_MethodInfo;
static const PropertyInfo Mark_t1382____Index_PropertyInfo = 
{
	&Mark_t1382_il2cpp_TypeInfo/* parent */
	, "Index"/* name */
	, &Mark_get_Index_m6075_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mark_get_Length_m6076_MethodInfo;
static const PropertyInfo Mark_t1382____Length_PropertyInfo = 
{
	&Mark_t1382_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &Mark_get_Length_m6076_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Mark_t1382_PropertyInfos[] =
{
	&Mark_t1382____IsDefined_PropertyInfo,
	&Mark_t1382____Index_PropertyInfo,
	&Mark_t1382____Length_PropertyInfo,
	NULL
};
static const Il2CppMethodReference Mark_t1382_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool Mark_t1382_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Mark_t1382_0_0_0;
extern const Il2CppType Mark_t1382_1_0_0;
const Il2CppTypeDefinitionMetadata Mark_t1382_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, Mark_t1382_VTable/* vtableMethods */
	, Mark_t1382_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 655/* fieldStart */

};
TypeInfo Mark_t1382_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mark"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Mark_t1382_MethodInfos/* methods */
	, Mark_t1382_PropertyInfos/* properties */
	, NULL/* events */
	, &Mark_t1382_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mark_t1382_0_0_0/* byval_arg */
	, &Mark_t1382_1_0_0/* this_arg */
	, &Mark_t1382_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mark_t1382)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mark_t1382)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Mark_t1382 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/IntStack
#include "System_System_Text_RegularExpressions_Interpreter_IntStack.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/IntStack
extern TypeInfo IntStack_t1383_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/IntStack
#include "System_System_Text_RegularExpressions_Interpreter_IntStackMethodDeclarations.h"
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/IntStack::Pop()
extern const MethodInfo IntStack_Pop_m6077_MethodInfo = 
{
	"Pop"/* name */
	, (methodPointerType)&IntStack_Pop_m6077/* method */
	, &IntStack_t1383_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 692/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo IntStack_t1383_IntStack_Push_m6078_ParameterInfos[] = 
{
	{"value", 0, 134218405, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/IntStack::Push(System.Int32)
extern const MethodInfo IntStack_Push_m6078_MethodInfo = 
{
	"Push"/* name */
	, (methodPointerType)&IntStack_Push_m6078/* method */
	, &IntStack_t1383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, IntStack_t1383_IntStack_Push_m6078_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 693/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/IntStack::get_Count()
extern const MethodInfo IntStack_get_Count_m6079_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&IntStack_get_Count_m6079/* method */
	, &IntStack_t1383_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo IntStack_t1383_IntStack_set_Count_m6080_ParameterInfos[] = 
{
	{"value", 0, 134218406, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/IntStack::set_Count(System.Int32)
extern const MethodInfo IntStack_set_Count_m6080_MethodInfo = 
{
	"set_Count"/* name */
	, (methodPointerType)&IntStack_set_Count_m6080/* method */
	, &IntStack_t1383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, IntStack_t1383_IntStack_set_Count_m6080_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IntStack_t1383_MethodInfos[] =
{
	&IntStack_Pop_m6077_MethodInfo,
	&IntStack_Push_m6078_MethodInfo,
	&IntStack_get_Count_m6079_MethodInfo,
	&IntStack_set_Count_m6080_MethodInfo,
	NULL
};
extern const MethodInfo IntStack_get_Count_m6079_MethodInfo;
extern const MethodInfo IntStack_set_Count_m6080_MethodInfo;
static const PropertyInfo IntStack_t1383____Count_PropertyInfo = 
{
	&IntStack_t1383_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IntStack_get_Count_m6079_MethodInfo/* get */
	, &IntStack_set_Count_m6080_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IntStack_t1383_PropertyInfos[] =
{
	&IntStack_t1383____Count_PropertyInfo,
	NULL
};
static const Il2CppMethodReference IntStack_t1383_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool IntStack_t1383_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IntStack_t1383_0_0_0;
extern const Il2CppType IntStack_t1383_1_0_0;
extern TypeInfo Interpreter_t1388_il2cpp_TypeInfo;
extern const Il2CppType Interpreter_t1388_0_0_0;
const Il2CppTypeDefinitionMetadata IntStack_t1383_DefinitionMetadata = 
{
	&Interpreter_t1388_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, IntStack_t1383_VTable/* vtableMethods */
	, IntStack_t1383_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 658/* fieldStart */

};
TypeInfo IntStack_t1383_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntStack"/* name */
	, ""/* namespaze */
	, IntStack_t1383_MethodInfos/* methods */
	, IntStack_t1383_PropertyInfos/* properties */
	, NULL/* events */
	, &IntStack_t1383_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IntStack_t1383_0_0_0/* byval_arg */
	, &IntStack_t1383_1_0_0/* this_arg */
	, &IntStack_t1383_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)IntStack_t1383_marshal/* marshal_to_native_func */
	, (methodPointerType)IntStack_t1383_marshal_back/* marshal_from_native_func */
	, (methodPointerType)IntStack_t1383_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (IntStack_t1383)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (IntStack_t1383)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(IntStack_t1383_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/RepeatContext
#include "System_System_Text_RegularExpressions_Interpreter_RepeatCont.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/RepeatContext
extern TypeInfo RepeatContext_t1384_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/RepeatContext
#include "System_System_Text_RegularExpressions_Interpreter_RepeatContMethodDeclarations.h"
extern const Il2CppType RepeatContext_t1384_0_0_0;
extern const Il2CppType RepeatContext_t1384_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo RepeatContext_t1384_RepeatContext__ctor_m6081_ParameterInfos[] = 
{
	{"previous", 0, 134218407, 0, &RepeatContext_t1384_0_0_0},
	{"min", 1, 134218408, 0, &Int32_t253_0_0_0},
	{"max", 2, 134218409, 0, &Int32_t253_0_0_0},
	{"lazy", 3, 134218410, 0, &Boolean_t273_0_0_0},
	{"expr_pc", 4, 134218411, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::.ctor(System.Text.RegularExpressions.Interpreter/RepeatContext,System.Int32,System.Int32,System.Boolean,System.Int32)
extern const MethodInfo RepeatContext__ctor_m6081_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RepeatContext__ctor_m6081/* method */
	, &RepeatContext_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_SByte_t274_Int32_t253/* invoker_method */
	, RepeatContext_t1384_RepeatContext__ctor_m6081_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Count()
extern const MethodInfo RepeatContext_get_Count_m6082_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&RepeatContext_get_Count_m6082/* method */
	, &RepeatContext_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo RepeatContext_t1384_RepeatContext_set_Count_m6083_ParameterInfos[] = 
{
	{"value", 0, 134218412, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::set_Count(System.Int32)
extern const MethodInfo RepeatContext_set_Count_m6083_MethodInfo = 
{
	"set_Count"/* name */
	, (methodPointerType)&RepeatContext_set_Count_m6083/* method */
	, &RepeatContext_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, RepeatContext_t1384_RepeatContext_set_Count_m6083_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Start()
extern const MethodInfo RepeatContext_get_Start_m6084_MethodInfo = 
{
	"get_Start"/* name */
	, (methodPointerType)&RepeatContext_get_Start_m6084/* method */
	, &RepeatContext_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo RepeatContext_t1384_RepeatContext_set_Start_m6085_ParameterInfos[] = 
{
	{"value", 0, 134218413, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::set_Start(System.Int32)
extern const MethodInfo RepeatContext_set_Start_m6085_MethodInfo = 
{
	"set_Start"/* name */
	, (methodPointerType)&RepeatContext_set_Start_m6085/* method */
	, &RepeatContext_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, RepeatContext_t1384_RepeatContext_set_Start_m6085_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsMinimum()
extern const MethodInfo RepeatContext_get_IsMinimum_m6086_MethodInfo = 
{
	"get_IsMinimum"/* name */
	, (methodPointerType)&RepeatContext_get_IsMinimum_m6086/* method */
	, &RepeatContext_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsMaximum()
extern const MethodInfo RepeatContext_get_IsMaximum_m6087_MethodInfo = 
{
	"get_IsMaximum"/* name */
	, (methodPointerType)&RepeatContext_get_IsMaximum_m6087/* method */
	, &RepeatContext_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsLazy()
extern const MethodInfo RepeatContext_get_IsLazy_m6088_MethodInfo = 
{
	"get_IsLazy"/* name */
	, (methodPointerType)&RepeatContext_get_IsLazy_m6088/* method */
	, &RepeatContext_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Expression()
extern const MethodInfo RepeatContext_get_Expression_m6089_MethodInfo = 
{
	"get_Expression"/* name */
	, (methodPointerType)&RepeatContext_get_Expression_m6089/* method */
	, &RepeatContext_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interpreter/RepeatContext System.Text.RegularExpressions.Interpreter/RepeatContext::get_Previous()
extern const MethodInfo RepeatContext_get_Previous_m6090_MethodInfo = 
{
	"get_Previous"/* name */
	, (methodPointerType)&RepeatContext_get_Previous_m6090/* method */
	, &RepeatContext_t1384_il2cpp_TypeInfo/* declaring_type */
	, &RepeatContext_t1384_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RepeatContext_t1384_MethodInfos[] =
{
	&RepeatContext__ctor_m6081_MethodInfo,
	&RepeatContext_get_Count_m6082_MethodInfo,
	&RepeatContext_set_Count_m6083_MethodInfo,
	&RepeatContext_get_Start_m6084_MethodInfo,
	&RepeatContext_set_Start_m6085_MethodInfo,
	&RepeatContext_get_IsMinimum_m6086_MethodInfo,
	&RepeatContext_get_IsMaximum_m6087_MethodInfo,
	&RepeatContext_get_IsLazy_m6088_MethodInfo,
	&RepeatContext_get_Expression_m6089_MethodInfo,
	&RepeatContext_get_Previous_m6090_MethodInfo,
	NULL
};
extern const MethodInfo RepeatContext_get_Count_m6082_MethodInfo;
extern const MethodInfo RepeatContext_set_Count_m6083_MethodInfo;
static const PropertyInfo RepeatContext_t1384____Count_PropertyInfo = 
{
	&RepeatContext_t1384_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &RepeatContext_get_Count_m6082_MethodInfo/* get */
	, &RepeatContext_set_Count_m6083_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_Start_m6084_MethodInfo;
extern const MethodInfo RepeatContext_set_Start_m6085_MethodInfo;
static const PropertyInfo RepeatContext_t1384____Start_PropertyInfo = 
{
	&RepeatContext_t1384_il2cpp_TypeInfo/* parent */
	, "Start"/* name */
	, &RepeatContext_get_Start_m6084_MethodInfo/* get */
	, &RepeatContext_set_Start_m6085_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_IsMinimum_m6086_MethodInfo;
static const PropertyInfo RepeatContext_t1384____IsMinimum_PropertyInfo = 
{
	&RepeatContext_t1384_il2cpp_TypeInfo/* parent */
	, "IsMinimum"/* name */
	, &RepeatContext_get_IsMinimum_m6086_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_IsMaximum_m6087_MethodInfo;
static const PropertyInfo RepeatContext_t1384____IsMaximum_PropertyInfo = 
{
	&RepeatContext_t1384_il2cpp_TypeInfo/* parent */
	, "IsMaximum"/* name */
	, &RepeatContext_get_IsMaximum_m6087_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_IsLazy_m6088_MethodInfo;
static const PropertyInfo RepeatContext_t1384____IsLazy_PropertyInfo = 
{
	&RepeatContext_t1384_il2cpp_TypeInfo/* parent */
	, "IsLazy"/* name */
	, &RepeatContext_get_IsLazy_m6088_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_Expression_m6089_MethodInfo;
static const PropertyInfo RepeatContext_t1384____Expression_PropertyInfo = 
{
	&RepeatContext_t1384_il2cpp_TypeInfo/* parent */
	, "Expression"/* name */
	, &RepeatContext_get_Expression_m6089_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_Previous_m6090_MethodInfo;
static const PropertyInfo RepeatContext_t1384____Previous_PropertyInfo = 
{
	&RepeatContext_t1384_il2cpp_TypeInfo/* parent */
	, "Previous"/* name */
	, &RepeatContext_get_Previous_m6090_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RepeatContext_t1384_PropertyInfos[] =
{
	&RepeatContext_t1384____Count_PropertyInfo,
	&RepeatContext_t1384____Start_PropertyInfo,
	&RepeatContext_t1384____IsMinimum_PropertyInfo,
	&RepeatContext_t1384____IsMaximum_PropertyInfo,
	&RepeatContext_t1384____IsLazy_PropertyInfo,
	&RepeatContext_t1384____Expression_PropertyInfo,
	&RepeatContext_t1384____Previous_PropertyInfo,
	NULL
};
static const Il2CppMethodReference RepeatContext_t1384_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool RepeatContext_t1384_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RepeatContext_t1384_1_0_0;
struct RepeatContext_t1384;
const Il2CppTypeDefinitionMetadata RepeatContext_t1384_DefinitionMetadata = 
{
	&Interpreter_t1388_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RepeatContext_t1384_VTable/* vtableMethods */
	, RepeatContext_t1384_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 660/* fieldStart */

};
TypeInfo RepeatContext_t1384_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RepeatContext"/* name */
	, ""/* namespaze */
	, RepeatContext_t1384_MethodInfos/* methods */
	, RepeatContext_t1384_PropertyInfos/* properties */
	, NULL/* events */
	, &RepeatContext_t1384_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RepeatContext_t1384_0_0_0/* byval_arg */
	, &RepeatContext_t1384_1_0_0/* this_arg */
	, &RepeatContext_t1384_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RepeatContext_t1384)/* instance_size */
	, sizeof (RepeatContext_t1384)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/Mode
#include "System_System_Text_RegularExpressions_Interpreter_Mode.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/Mode
extern TypeInfo Mode_t1385_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/Mode
#include "System_System_Text_RegularExpressions_Interpreter_ModeMethodDeclarations.h"
static const MethodInfo* Mode_t1385_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Mode_t1385_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Mode_t1385_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Mode_t1385_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Mode_t1385_0_0_0;
extern const Il2CppType Mode_t1385_1_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t253_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Mode_t1385_DefinitionMetadata = 
{
	&Interpreter_t1388_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Mode_t1385_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Mode_t1385_VTable/* vtableMethods */
	, Mode_t1385_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 667/* fieldStart */

};
TypeInfo Mode_t1385_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mode"/* name */
	, ""/* namespaze */
	, Mode_t1385_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mode_t1385_0_0_0/* byval_arg */
	, &Mode_t1385_1_0_0/* this_arg */
	, &Mode_t1385_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mode_t1385)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mode_t1385)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter
#include "System_System_Text_RegularExpressions_Interpreter.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter
// System.Text.RegularExpressions.Interpreter
#include "System_System_Text_RegularExpressions_InterpreterMethodDeclarations.h"
extern const Il2CppType UInt16U5BU5D_t1297_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter__ctor_m6091_ParameterInfos[] = 
{
	{"program", 0, 134218368, 0, &UInt16U5BU5D_t1297_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::.ctor(System.UInt16[])
extern const MethodInfo Interpreter__ctor_m6091_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Interpreter__ctor_m6091/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Interpreter_t1388_Interpreter__ctor_m6091_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_ReadProgramCount_m6092_ParameterInfos[] = 
{
	{"ptr", 0, 134218369, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::ReadProgramCount(System.Int32)
extern const MethodInfo Interpreter_ReadProgramCount_m6092_MethodInfo = 
{
	"ReadProgramCount"/* name */
	, (methodPointerType)&Interpreter_ReadProgramCount_m6092/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_ReadProgramCount_m6092_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Regex_t421_0_0_0;
extern const Il2CppType Regex_t421_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_Scan_m6093_ParameterInfos[] = 
{
	{"regex", 0, 134218370, 0, &Regex_t421_0_0_0},
	{"text", 1, 134218371, 0, &String_t_0_0_0},
	{"start", 2, 134218372, 0, &Int32_t253_0_0_0},
	{"end", 3, 134218373, 0, &Int32_t253_0_0_0},
};
extern const Il2CppType Match_t422_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Interpreter::Scan(System.Text.RegularExpressions.Regex,System.String,System.Int32,System.Int32)
extern const MethodInfo Interpreter_Scan_m6093_MethodInfo = 
{
	"Scan"/* name */
	, (methodPointerType)&Interpreter_Scan_m6093/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Match_t422_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_Scan_m6093_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Reset()
extern const MethodInfo Interpreter_Reset_m6094_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Interpreter_Reset_m6094/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Mode_t1385_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_Eval_m6095_ParameterInfos[] = 
{
	{"mode", 0, 134218374, 0, &Mode_t1385_0_0_0},
	{"ref_ptr", 1, 134218375, 0, &Int32_t253_1_0_0},
	{"pc", 2, 134218376, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::Eval(System.Text.RegularExpressions.Interpreter/Mode,System.Int32&,System.Int32)
extern const MethodInfo Interpreter_Eval_m6095_MethodInfo = 
{
	"Eval"/* name */
	, (methodPointerType)&Interpreter_Eval_m6095/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253_Int32U26_t753_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_Eval_m6095_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Mode_t1385_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_EvalChar_m6096_ParameterInfos[] = 
{
	{"mode", 0, 134218377, 0, &Mode_t1385_0_0_0},
	{"ptr", 1, 134218378, 0, &Int32_t253_1_0_0},
	{"pc", 2, 134218379, 0, &Int32_t253_1_0_0},
	{"multi", 3, 134218380, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253_Int32U26_t753_Int32U26_t753_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::EvalChar(System.Text.RegularExpressions.Interpreter/Mode,System.Int32&,System.Int32&,System.Boolean)
extern const MethodInfo Interpreter_EvalChar_m6096_MethodInfo = 
{
	"EvalChar"/* name */
	, (methodPointerType)&Interpreter_EvalChar_m6096/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253_Int32U26_t753_Int32U26_t753_SByte_t274/* invoker_method */
	, Interpreter_t1388_Interpreter_EvalChar_m6096_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_TryMatch_m6097_ParameterInfos[] = 
{
	{"ref_ptr", 0, 134218381, 0, &Int32_t253_1_0_0},
	{"pc", 1, 134218382, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::TryMatch(System.Int32&,System.Int32)
extern const MethodInfo Interpreter_TryMatch_m6097_MethodInfo = 
{
	"TryMatch"/* name */
	, (methodPointerType)&Interpreter_TryMatch_m6097/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32U26_t753_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_TryMatch_m6097_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Position_t1370_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_IsPosition_m6098_ParameterInfos[] = 
{
	{"pos", 0, 134218383, 0, &Position_t1370_0_0_0},
	{"ptr", 1, 134218384, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_UInt16_t684_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::IsPosition(System.Text.RegularExpressions.Position,System.Int32)
extern const MethodInfo Interpreter_IsPosition_m6098_MethodInfo = 
{
	"IsPosition"/* name */
	, (methodPointerType)&Interpreter_IsPosition_m6098/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_UInt16_t684_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_IsPosition_m6098_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_IsWordChar_m6099_ParameterInfos[] = 
{
	{"c", 0, 134218385, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::IsWordChar(System.Char)
extern const MethodInfo Interpreter_IsWordChar_m6099_MethodInfo = 
{
	"IsWordChar"/* name */
	, (methodPointerType)&Interpreter_IsWordChar_m6099/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int16_t752/* invoker_method */
	, Interpreter_t1388_Interpreter_IsWordChar_m6099_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_GetString_m6100_ParameterInfos[] = 
{
	{"pc", 0, 134218386, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Interpreter::GetString(System.Int32)
extern const MethodInfo Interpreter_GetString_m6100_MethodInfo = 
{
	"GetString"/* name */
	, (methodPointerType)&Interpreter_GetString_m6100/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_GetString_m6100_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_Open_m6101_ParameterInfos[] = 
{
	{"gid", 0, 134218387, 0, &Int32_t253_0_0_0},
	{"ptr", 1, 134218388, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Open(System.Int32,System.Int32)
extern const MethodInfo Interpreter_Open_m6101_MethodInfo = 
{
	"Open"/* name */
	, (methodPointerType)&Interpreter_Open_m6101/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_Open_m6101_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_Close_m6102_ParameterInfos[] = 
{
	{"gid", 0, 134218389, 0, &Int32_t253_0_0_0},
	{"ptr", 1, 134218390, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Close(System.Int32,System.Int32)
extern const MethodInfo Interpreter_Close_m6102_MethodInfo = 
{
	"Close"/* name */
	, (methodPointerType)&Interpreter_Close_m6102/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_Close_m6102_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_Balance_m6103_ParameterInfos[] = 
{
	{"gid", 0, 134218391, 0, &Int32_t253_0_0_0},
	{"balance_gid", 1, 134218392, 0, &Int32_t253_0_0_0},
	{"capture", 2, 134218393, 0, &Boolean_t273_0_0_0},
	{"ptr", 3, 134218394, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253_Int32_t253_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::Balance(System.Int32,System.Int32,System.Boolean,System.Int32)
extern const MethodInfo Interpreter_Balance_m6103_MethodInfo = 
{
	"Balance"/* name */
	, (methodPointerType)&Interpreter_Balance_m6103/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253_Int32_t253_SByte_t274_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_Balance_m6103_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 683/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::Checkpoint()
extern const MethodInfo Interpreter_Checkpoint_m6104_MethodInfo = 
{
	"Checkpoint"/* name */
	, (methodPointerType)&Interpreter_Checkpoint_m6104/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 684/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_Backtrack_m6105_ParameterInfos[] = 
{
	{"cp", 0, 134218395, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Backtrack(System.Int32)
extern const MethodInfo Interpreter_Backtrack_m6105_MethodInfo = 
{
	"Backtrack"/* name */
	, (methodPointerType)&Interpreter_Backtrack_m6105/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_Backtrack_m6105_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 685/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::ResetGroups()
extern const MethodInfo Interpreter_ResetGroups_m6106_MethodInfo = 
{
	"ResetGroups"/* name */
	, (methodPointerType)&Interpreter_ResetGroups_m6106/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 686/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_GetLastDefined_m6107_ParameterInfos[] = 
{
	{"gid", 0, 134218396, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::GetLastDefined(System.Int32)
extern const MethodInfo Interpreter_GetLastDefined_m6107_MethodInfo = 
{
	"GetLastDefined"/* name */
	, (methodPointerType)&Interpreter_GetLastDefined_m6107/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_GetLastDefined_m6107_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 687/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_CreateMark_m6108_ParameterInfos[] = 
{
	{"previous", 0, 134218397, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::CreateMark(System.Int32)
extern const MethodInfo Interpreter_CreateMark_m6108_MethodInfo = 
{
	"CreateMark"/* name */
	, (methodPointerType)&Interpreter_CreateMark_m6108/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_CreateMark_m6108_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 688/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
static const ParameterInfo Interpreter_t1388_Interpreter_GetGroupInfo_m6109_ParameterInfos[] = 
{
	{"gid", 0, 134218398, 0, &Int32_t253_0_0_0},
	{"first_mark_index", 1, 134218399, 0, &Int32_t253_1_0_2},
	{"n_caps", 2, 134218400, 0, &Int32_t253_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::GetGroupInfo(System.Int32,System.Int32&,System.Int32&)
extern const MethodInfo Interpreter_GetGroupInfo_m6109_MethodInfo = 
{
	"GetGroupInfo"/* name */
	, (methodPointerType)&Interpreter_GetGroupInfo_m6109/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32U26_t753_Int32U26_t753/* invoker_method */
	, Interpreter_t1388_Interpreter_GetGroupInfo_m6109_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 689/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Group_t1358_0_0_0;
extern const Il2CppType Group_t1358_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_PopulateGroup_m6110_ParameterInfos[] = 
{
	{"g", 0, 134218401, 0, &Group_t1358_0_0_0},
	{"first_mark_index", 1, 134218402, 0, &Int32_t253_0_0_0},
	{"n_caps", 2, 134218403, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::PopulateGroup(System.Text.RegularExpressions.Group,System.Int32,System.Int32)
extern const MethodInfo Interpreter_PopulateGroup_m6110_MethodInfo = 
{
	"PopulateGroup"/* name */
	, (methodPointerType)&Interpreter_PopulateGroup_m6110/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, Interpreter_t1388_Interpreter_PopulateGroup_m6110_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 690/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Regex_t421_0_0_0;
static const ParameterInfo Interpreter_t1388_Interpreter_GenerateMatch_m6111_ParameterInfos[] = 
{
	{"regex", 0, 134218404, 0, &Regex_t421_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Interpreter::GenerateMatch(System.Text.RegularExpressions.Regex)
extern const MethodInfo Interpreter_GenerateMatch_m6111_MethodInfo = 
{
	"GenerateMatch"/* name */
	, (methodPointerType)&Interpreter_GenerateMatch_m6111/* method */
	, &Interpreter_t1388_il2cpp_TypeInfo/* declaring_type */
	, &Match_t422_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Interpreter_t1388_Interpreter_GenerateMatch_m6111_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 691/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Interpreter_t1388_MethodInfos[] =
{
	&Interpreter__ctor_m6091_MethodInfo,
	&Interpreter_ReadProgramCount_m6092_MethodInfo,
	&Interpreter_Scan_m6093_MethodInfo,
	&Interpreter_Reset_m6094_MethodInfo,
	&Interpreter_Eval_m6095_MethodInfo,
	&Interpreter_EvalChar_m6096_MethodInfo,
	&Interpreter_TryMatch_m6097_MethodInfo,
	&Interpreter_IsPosition_m6098_MethodInfo,
	&Interpreter_IsWordChar_m6099_MethodInfo,
	&Interpreter_GetString_m6100_MethodInfo,
	&Interpreter_Open_m6101_MethodInfo,
	&Interpreter_Close_m6102_MethodInfo,
	&Interpreter_Balance_m6103_MethodInfo,
	&Interpreter_Checkpoint_m6104_MethodInfo,
	&Interpreter_Backtrack_m6105_MethodInfo,
	&Interpreter_ResetGroups_m6106_MethodInfo,
	&Interpreter_GetLastDefined_m6107_MethodInfo,
	&Interpreter_CreateMark_m6108_MethodInfo,
	&Interpreter_GetGroupInfo_m6109_MethodInfo,
	&Interpreter_PopulateGroup_m6110_MethodInfo,
	&Interpreter_GenerateMatch_m6111_MethodInfo,
	NULL
};
static const Il2CppType* Interpreter_t1388_il2cpp_TypeInfo__nestedTypes[3] =
{
	&IntStack_t1383_0_0_0,
	&RepeatContext_t1384_0_0_0,
	&Mode_t1385_0_0_0,
};
extern const MethodInfo Interpreter_Scan_m6093_MethodInfo;
extern const MethodInfo BaseMachine_Replace_m5917_MethodInfo;
static const Il2CppMethodReference Interpreter_t1388_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Interpreter_Scan_m6093_MethodInfo,
	&BaseMachine_Replace_m5917_MethodInfo,
	&BaseMachine_Replace_m5917_MethodInfo,
	&Interpreter_Scan_m6093_MethodInfo,
};
static bool Interpreter_t1388_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Interpreter_t1388_InterfacesOffsets[] = 
{
	{ &IMachine_t1361_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Interpreter_t1388_1_0_0;
extern const Il2CppType BaseMachine_t1354_0_0_0;
struct Interpreter_t1388;
const Il2CppTypeDefinitionMetadata Interpreter_t1388_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Interpreter_t1388_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Interpreter_t1388_InterfacesOffsets/* interfaceOffsets */
	, &BaseMachine_t1354_0_0_0/* parent */
	, Interpreter_t1388_VTable/* vtableMethods */
	, Interpreter_t1388_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 671/* fieldStart */

};
TypeInfo Interpreter_t1388_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interpreter"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Interpreter_t1388_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Interpreter_t1388_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interpreter_t1388_0_0_0/* byval_arg */
	, &Interpreter_t1388_1_0_0/* this_arg */
	, &Interpreter_t1388_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Interpreter_t1388)/* instance_size */
	, sizeof (Interpreter_t1388)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
// Metadata Definition System.Text.RegularExpressions.Interval
extern TypeInfo Interval_t1389_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_IntervalMethodDeclarations.h"
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interval_t1389_Interval__ctor_m6112_ParameterInfos[] = 
{
	{"low", 0, 134218414, 0, &Int32_t253_0_0_0},
	{"high", 1, 134218415, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interval::.ctor(System.Int32,System.Int32)
extern const MethodInfo Interval__ctor_m6112_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Interval__ctor_m6112/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253/* invoker_method */
	, Interval_t1389_Interval__ctor_m6112_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1389_0_0_0;
extern void* RuntimeInvoker_Interval_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.Interval::get_Empty()
extern const MethodInfo Interval_get_Empty_m6113_MethodInfo = 
{
	"get_Empty"/* name */
	, (methodPointerType)&Interval_get_Empty_m6113/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t1389_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t1389/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsDiscontiguous()
extern const MethodInfo Interval_get_IsDiscontiguous_m6114_MethodInfo = 
{
	"get_IsDiscontiguous"/* name */
	, (methodPointerType)&Interval_get_IsDiscontiguous_m6114/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsSingleton()
extern const MethodInfo Interval_get_IsSingleton_m6115_MethodInfo = 
{
	"get_IsSingleton"/* name */
	, (methodPointerType)&Interval_get_IsSingleton_m6115/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsEmpty()
extern const MethodInfo Interval_get_IsEmpty_m6116_MethodInfo = 
{
	"get_IsEmpty"/* name */
	, (methodPointerType)&Interval_get_IsEmpty_m6116/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interval::get_Size()
extern const MethodInfo Interval_get_Size_m6117_MethodInfo = 
{
	"get_Size"/* name */
	, (methodPointerType)&Interval_get_Size_m6117/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1389_0_0_0;
static const ParameterInfo Interval_t1389_Interval_IsDisjoint_m6118_ParameterInfos[] = 
{
	{"i", 0, 134218416, 0, &Interval_t1389_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Interval_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::IsDisjoint(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_IsDisjoint_m6118_MethodInfo = 
{
	"IsDisjoint"/* name */
	, (methodPointerType)&Interval_IsDisjoint_m6118/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Interval_t1389/* invoker_method */
	, Interval_t1389_Interval_IsDisjoint_m6118_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1389_0_0_0;
static const ParameterInfo Interval_t1389_Interval_IsAdjacent_m6119_ParameterInfos[] = 
{
	{"i", 0, 134218417, 0, &Interval_t1389_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Interval_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::IsAdjacent(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_IsAdjacent_m6119_MethodInfo = 
{
	"IsAdjacent"/* name */
	, (methodPointerType)&Interval_IsAdjacent_m6119/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Interval_t1389/* invoker_method */
	, Interval_t1389_Interval_IsAdjacent_m6119_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1389_0_0_0;
static const ParameterInfo Interval_t1389_Interval_Contains_m6120_ParameterInfos[] = 
{
	{"i", 0, 134218418, 0, &Interval_t1389_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Interval_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Contains(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_Contains_m6120_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&Interval_Contains_m6120/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Interval_t1389/* invoker_method */
	, Interval_t1389_Interval_Contains_m6120_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Interval_t1389_Interval_Contains_m6121_ParameterInfos[] = 
{
	{"i", 0, 134218419, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Contains(System.Int32)
extern const MethodInfo Interval_Contains_m6121_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&Interval_Contains_m6121/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253/* invoker_method */
	, Interval_t1389_Interval_Contains_m6121_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1389_0_0_0;
static const ParameterInfo Interval_t1389_Interval_Intersects_m6122_ParameterInfos[] = 
{
	{"i", 0, 134218420, 0, &Interval_t1389_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Interval_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Intersects(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_Intersects_m6122_MethodInfo = 
{
	"Intersects"/* name */
	, (methodPointerType)&Interval_Intersects_m6122/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Interval_t1389/* invoker_method */
	, Interval_t1389_Interval_Intersects_m6122_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1389_0_0_0;
static const ParameterInfo Interval_t1389_Interval_Merge_m6123_ParameterInfos[] = 
{
	{"i", 0, 134218421, 0, &Interval_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Interval_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interval::Merge(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_Merge_m6123_MethodInfo = 
{
	"Merge"/* name */
	, (methodPointerType)&Interval_Merge_m6123/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Interval_t1389/* invoker_method */
	, Interval_t1389_Interval_Merge_m6123_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Interval_t1389_Interval_CompareTo_m6124_ParameterInfos[] = 
{
	{"o", 0, 134218422, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interval::CompareTo(System.Object)
extern const MethodInfo Interval_CompareTo_m6124_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Interval_CompareTo_m6124/* method */
	, &Interval_t1389_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, Interval_t1389_Interval_CompareTo_m6124_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Interval_t1389_MethodInfos[] =
{
	&Interval__ctor_m6112_MethodInfo,
	&Interval_get_Empty_m6113_MethodInfo,
	&Interval_get_IsDiscontiguous_m6114_MethodInfo,
	&Interval_get_IsSingleton_m6115_MethodInfo,
	&Interval_get_IsEmpty_m6116_MethodInfo,
	&Interval_get_Size_m6117_MethodInfo,
	&Interval_IsDisjoint_m6118_MethodInfo,
	&Interval_IsAdjacent_m6119_MethodInfo,
	&Interval_Contains_m6120_MethodInfo,
	&Interval_Contains_m6121_MethodInfo,
	&Interval_Intersects_m6122_MethodInfo,
	&Interval_Merge_m6123_MethodInfo,
	&Interval_CompareTo_m6124_MethodInfo,
	NULL
};
extern const MethodInfo Interval_get_Empty_m6113_MethodInfo;
static const PropertyInfo Interval_t1389____Empty_PropertyInfo = 
{
	&Interval_t1389_il2cpp_TypeInfo/* parent */
	, "Empty"/* name */
	, &Interval_get_Empty_m6113_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_IsDiscontiguous_m6114_MethodInfo;
static const PropertyInfo Interval_t1389____IsDiscontiguous_PropertyInfo = 
{
	&Interval_t1389_il2cpp_TypeInfo/* parent */
	, "IsDiscontiguous"/* name */
	, &Interval_get_IsDiscontiguous_m6114_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_IsSingleton_m6115_MethodInfo;
static const PropertyInfo Interval_t1389____IsSingleton_PropertyInfo = 
{
	&Interval_t1389_il2cpp_TypeInfo/* parent */
	, "IsSingleton"/* name */
	, &Interval_get_IsSingleton_m6115_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_IsEmpty_m6116_MethodInfo;
static const PropertyInfo Interval_t1389____IsEmpty_PropertyInfo = 
{
	&Interval_t1389_il2cpp_TypeInfo/* parent */
	, "IsEmpty"/* name */
	, &Interval_get_IsEmpty_m6116_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_Size_m6117_MethodInfo;
static const PropertyInfo Interval_t1389____Size_PropertyInfo = 
{
	&Interval_t1389_il2cpp_TypeInfo/* parent */
	, "Size"/* name */
	, &Interval_get_Size_m6117_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Interval_t1389_PropertyInfos[] =
{
	&Interval_t1389____Empty_PropertyInfo,
	&Interval_t1389____IsDiscontiguous_PropertyInfo,
	&Interval_t1389____IsSingleton_PropertyInfo,
	&Interval_t1389____IsEmpty_PropertyInfo,
	&Interval_t1389____Size_PropertyInfo,
	NULL
};
extern const MethodInfo Interval_CompareTo_m6124_MethodInfo;
static const Il2CppMethodReference Interval_t1389_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
	&Interval_CompareTo_m6124_MethodInfo,
};
static bool Interval_t1389_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Interval_t1389_InterfacesTypeInfos[] = 
{
	&IComparable_t277_0_0_0,
};
static Il2CppInterfaceOffsetPair Interval_t1389_InterfacesOffsets[] = 
{
	{ &IComparable_t277_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Interval_t1389_1_0_0;
const Il2CppTypeDefinitionMetadata Interval_t1389_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Interval_t1389_InterfacesTypeInfos/* implementedInterfaces */
	, Interval_t1389_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, Interval_t1389_VTable/* vtableMethods */
	, Interval_t1389_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 687/* fieldStart */

};
TypeInfo Interval_t1389_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interval"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Interval_t1389_MethodInfos/* methods */
	, Interval_t1389_PropertyInfos/* properties */
	, NULL/* events */
	, &Interval_t1389_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interval_t1389_0_0_0/* byval_arg */
	, &Interval_t1389_1_0_0/* this_arg */
	, &Interval_t1389_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Interval_t1389_marshal/* marshal_to_native_func */
	, (methodPointerType)Interval_t1389_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Interval_t1389_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Interval_t1389)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Interval_t1389)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Interval_t1389_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection/Enumerator
#include "System_System_Text_RegularExpressions_IntervalCollection_Enu.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/Enumerator
extern TypeInfo Enumerator_t1390_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection/Enumerator
#include "System_System_Text_RegularExpressions_IntervalCollection_EnuMethodDeclarations.h"
extern const Il2CppType IList_t1195_0_0_0;
extern const Il2CppType IList_t1195_0_0_0;
static const ParameterInfo Enumerator_t1390_Enumerator__ctor_m6125_ParameterInfos[] = 
{
	{"list", 0, 134218432, 0, &IList_t1195_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::.ctor(System.Collections.IList)
extern const MethodInfo Enumerator__ctor_m6125_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Enumerator__ctor_m6125/* method */
	, &Enumerator_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Enumerator_t1390_Enumerator__ctor_m6125_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 730/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.IntervalCollection/Enumerator::get_Current()
extern const MethodInfo Enumerator_get_Current_m6126_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&Enumerator_get_Current_m6126/* method */
	, &Enumerator_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 731/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.IntervalCollection/Enumerator::MoveNext()
extern const MethodInfo Enumerator_MoveNext_m6127_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&Enumerator_MoveNext_m6127/* method */
	, &Enumerator_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 732/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::Reset()
extern const MethodInfo Enumerator_Reset_m6128_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Enumerator_Reset_m6128/* method */
	, &Enumerator_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 733/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Enumerator_t1390_MethodInfos[] =
{
	&Enumerator__ctor_m6125_MethodInfo,
	&Enumerator_get_Current_m6126_MethodInfo,
	&Enumerator_MoveNext_m6127_MethodInfo,
	&Enumerator_Reset_m6128_MethodInfo,
	NULL
};
extern const MethodInfo Enumerator_get_Current_m6126_MethodInfo;
static const PropertyInfo Enumerator_t1390____Current_PropertyInfo = 
{
	&Enumerator_t1390_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m6126_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Enumerator_t1390_PropertyInfos[] =
{
	&Enumerator_t1390____Current_PropertyInfo,
	NULL
};
extern const MethodInfo Enumerator_MoveNext_m6127_MethodInfo;
extern const MethodInfo Enumerator_Reset_m6128_MethodInfo;
static const Il2CppMethodReference Enumerator_t1390_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Enumerator_get_Current_m6126_MethodInfo,
	&Enumerator_MoveNext_m6127_MethodInfo,
	&Enumerator_Reset_m6128_MethodInfo,
};
static bool Enumerator_t1390_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_t217_0_0_0;
static const Il2CppType* Enumerator_t1390_InterfacesTypeInfos[] = 
{
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t1390_InterfacesOffsets[] = 
{
	{ &IEnumerator_t217_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Enumerator_t1390_0_0_0;
extern const Il2CppType Enumerator_t1390_1_0_0;
extern TypeInfo IntervalCollection_t1392_il2cpp_TypeInfo;
extern const Il2CppType IntervalCollection_t1392_0_0_0;
struct Enumerator_t1390;
const Il2CppTypeDefinitionMetadata Enumerator_t1390_DefinitionMetadata = 
{
	&IntervalCollection_t1392_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t1390_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t1390_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t1390_VTable/* vtableMethods */
	, Enumerator_t1390_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 690/* fieldStart */

};
TypeInfo Enumerator_t1390_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t1390_MethodInfos/* methods */
	, Enumerator_t1390_PropertyInfos/* properties */
	, NULL/* events */
	, &Enumerator_t1390_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t1390_0_0_0/* byval_arg */
	, &Enumerator_t1390_1_0_0/* this_arg */
	, &Enumerator_t1390_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t1390)/* instance_size */
	, sizeof (Enumerator_t1390)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
#include "System_System_Text_RegularExpressions_IntervalCollection_Cos.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/CostDelegate
extern TypeInfo CostDelegate_t1391_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
#include "System_System_Text_RegularExpressions_IntervalCollection_CosMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CostDelegate_t1391_CostDelegate__ctor_m6129_ParameterInfos[] = 
{
	{"object", 0, 134218433, 0, &Object_t_0_0_0},
	{"method", 1, 134218434, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/CostDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CostDelegate__ctor_m6129_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CostDelegate__ctor_m6129/* method */
	, &CostDelegate_t1391_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, CostDelegate_t1391_CostDelegate__ctor_m6129_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 734/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1389_0_0_0;
static const ParameterInfo CostDelegate_t1391_CostDelegate_Invoke_m6130_ParameterInfos[] = 
{
	{"i", 0, 134218435, 0, &Interval_t1389_0_0_0},
};
extern const Il2CppType Double_t1070_0_0_0;
extern void* RuntimeInvoker_Double_t1070_Interval_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.IntervalCollection/CostDelegate::Invoke(System.Text.RegularExpressions.Interval)
extern const MethodInfo CostDelegate_Invoke_m6130_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CostDelegate_Invoke_m6130/* method */
	, &CostDelegate_t1391_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1070_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1070_Interval_t1389/* invoker_method */
	, CostDelegate_t1391_CostDelegate_Invoke_m6130_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 735/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1389_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CostDelegate_t1391_CostDelegate_BeginInvoke_m6131_ParameterInfos[] = 
{
	{"i", 0, 134218436, 0, &Interval_t1389_0_0_0},
	{"callback", 1, 134218437, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134218438, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t546_0_0_0;
extern void* RuntimeInvoker_Object_t_Interval_t1389_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Text.RegularExpressions.IntervalCollection/CostDelegate::BeginInvoke(System.Text.RegularExpressions.Interval,System.AsyncCallback,System.Object)
extern const MethodInfo CostDelegate_BeginInvoke_m6131_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CostDelegate_BeginInvoke_m6131/* method */
	, &CostDelegate_t1391_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Interval_t1389_Object_t_Object_t/* invoker_method */
	, CostDelegate_t1391_CostDelegate_BeginInvoke_m6131_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 736/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo CostDelegate_t1391_CostDelegate_EndInvoke_m6132_ParameterInfos[] = 
{
	{"result", 0, 134218439, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Double_t1070_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.IntervalCollection/CostDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo CostDelegate_EndInvoke_m6132_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CostDelegate_EndInvoke_m6132/* method */
	, &CostDelegate_t1391_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1070_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1070_Object_t/* invoker_method */
	, CostDelegate_t1391_CostDelegate_EndInvoke_m6132_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 737/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CostDelegate_t1391_MethodInfos[] =
{
	&CostDelegate__ctor_m6129_MethodInfo,
	&CostDelegate_Invoke_m6130_MethodInfo,
	&CostDelegate_BeginInvoke_m6131_MethodInfo,
	&CostDelegate_EndInvoke_m6132_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m3586_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m3587_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m3588_MethodInfo;
extern const MethodInfo Delegate_Clone_m3589_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m3590_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m3591_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m3592_MethodInfo;
extern const MethodInfo CostDelegate_Invoke_m6130_MethodInfo;
extern const MethodInfo CostDelegate_BeginInvoke_m6131_MethodInfo;
extern const MethodInfo CostDelegate_EndInvoke_m6132_MethodInfo;
static const Il2CppMethodReference CostDelegate_t1391_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&CostDelegate_Invoke_m6130_MethodInfo,
	&CostDelegate_BeginInvoke_m6131_MethodInfo,
	&CostDelegate_EndInvoke_m6132_MethodInfo,
};
static bool CostDelegate_t1391_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t733_0_0_0;
extern const Il2CppType ISerializable_t439_0_0_0;
static Il2CppInterfaceOffsetPair CostDelegate_t1391_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CostDelegate_t1391_0_0_0;
extern const Il2CppType CostDelegate_t1391_1_0_0;
extern const Il2CppType MulticastDelegate_t549_0_0_0;
struct CostDelegate_t1391;
const Il2CppTypeDefinitionMetadata CostDelegate_t1391_DefinitionMetadata = 
{
	&IntervalCollection_t1392_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CostDelegate_t1391_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, CostDelegate_t1391_VTable/* vtableMethods */
	, CostDelegate_t1391_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CostDelegate_t1391_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CostDelegate"/* name */
	, ""/* namespaze */
	, CostDelegate_t1391_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CostDelegate_t1391_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CostDelegate_t1391_0_0_0/* byval_arg */
	, &CostDelegate_t1391_1_0_0/* this_arg */
	, &CostDelegate_t1391_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CostDelegate_t1391/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CostDelegate_t1391)/* instance_size */
	, sizeof (CostDelegate_t1391)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection
#include "System_System_Text_RegularExpressions_IntervalCollection.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection
// System.Text.RegularExpressions.IntervalCollection
#include "System_System_Text_RegularExpressions_IntervalCollectionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::.ctor()
extern const MethodInfo IntervalCollection__ctor_m6133_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IntervalCollection__ctor_m6133/* method */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo IntervalCollection_t1392_IntervalCollection_get_Item_m6134_ParameterInfos[] = 
{
	{"i", 0, 134218423, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Interval_t1389_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.IntervalCollection::get_Item(System.Int32)
extern const MethodInfo IntervalCollection_get_Item_m6134_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&IntervalCollection_get_Item_m6134/* method */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t1389_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t1389_Int32_t253/* invoker_method */
	, IntervalCollection_t1392_IntervalCollection_get_Item_m6134_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1389_0_0_0;
static const ParameterInfo IntervalCollection_t1392_IntervalCollection_Add_m6135_ParameterInfos[] = 
{
	{"i", 0, 134218424, 0, &Interval_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Interval_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Add(System.Text.RegularExpressions.Interval)
extern const MethodInfo IntervalCollection_Add_m6135_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&IntervalCollection_Add_m6135/* method */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Interval_t1389/* invoker_method */
	, IntervalCollection_t1392_IntervalCollection_Add_m6135_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Normalize()
extern const MethodInfo IntervalCollection_Normalize_m6136_MethodInfo = 
{
	"Normalize"/* name */
	, (methodPointerType)&IntervalCollection_Normalize_m6136/* method */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CostDelegate_t1391_0_0_0;
static const ParameterInfo IntervalCollection_t1392_IntervalCollection_GetMetaCollection_m6137_ParameterInfos[] = 
{
	{"cost_del", 0, 134218425, 0, &CostDelegate_t1391_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IntervalCollection System.Text.RegularExpressions.IntervalCollection::GetMetaCollection(System.Text.RegularExpressions.IntervalCollection/CostDelegate)
extern const MethodInfo IntervalCollection_GetMetaCollection_m6137_MethodInfo = 
{
	"GetMetaCollection"/* name */
	, (methodPointerType)&IntervalCollection_GetMetaCollection_m6137/* method */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* declaring_type */
	, &IntervalCollection_t1392_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IntervalCollection_t1392_IntervalCollection_GetMetaCollection_m6137_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 723/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType IntervalCollection_t1392_0_0_0;
extern const Il2CppType CostDelegate_t1391_0_0_0;
static const ParameterInfo IntervalCollection_t1392_IntervalCollection_Optimize_m6138_ParameterInfos[] = 
{
	{"begin", 0, 134218426, 0, &Int32_t253_0_0_0},
	{"end", 1, 134218427, 0, &Int32_t253_0_0_0},
	{"meta", 2, 134218428, 0, &IntervalCollection_t1392_0_0_0},
	{"cost_del", 3, 134218429, 0, &CostDelegate_t1391_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Optimize(System.Int32,System.Int32,System.Text.RegularExpressions.IntervalCollection,System.Text.RegularExpressions.IntervalCollection/CostDelegate)
extern const MethodInfo IntervalCollection_Optimize_m6138_MethodInfo = 
{
	"Optimize"/* name */
	, (methodPointerType)&IntervalCollection_Optimize_m6138/* method */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Object_t_Object_t/* invoker_method */
	, IntervalCollection_t1392_IntervalCollection_Optimize_m6138_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 724/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.IntervalCollection::get_Count()
extern const MethodInfo IntervalCollection_get_Count_m6139_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&IntervalCollection_get_Count_m6139/* method */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 725/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.IntervalCollection::get_IsSynchronized()
extern const MethodInfo IntervalCollection_get_IsSynchronized_m6140_MethodInfo = 
{
	"get_IsSynchronized"/* name */
	, (methodPointerType)&IntervalCollection_get_IsSynchronized_m6140/* method */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 726/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.IntervalCollection::get_SyncRoot()
extern const MethodInfo IntervalCollection_get_SyncRoot_m6141_MethodInfo = 
{
	"get_SyncRoot"/* name */
	, (methodPointerType)&IntervalCollection_get_SyncRoot_m6141/* method */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 727/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo IntervalCollection_t1392_IntervalCollection_CopyTo_m6142_ParameterInfos[] = 
{
	{"array", 0, 134218430, 0, &Array_t_0_0_0},
	{"index", 1, 134218431, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::CopyTo(System.Array,System.Int32)
extern const MethodInfo IntervalCollection_CopyTo_m6142_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&IntervalCollection_CopyTo_m6142/* method */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, IntervalCollection_t1392_IntervalCollection_CopyTo_m6142_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 728/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator System.Text.RegularExpressions.IntervalCollection::GetEnumerator()
extern const MethodInfo IntervalCollection_GetEnumerator_m6143_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&IntervalCollection_GetEnumerator_m6143/* method */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 729/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IntervalCollection_t1392_MethodInfos[] =
{
	&IntervalCollection__ctor_m6133_MethodInfo,
	&IntervalCollection_get_Item_m6134_MethodInfo,
	&IntervalCollection_Add_m6135_MethodInfo,
	&IntervalCollection_Normalize_m6136_MethodInfo,
	&IntervalCollection_GetMetaCollection_m6137_MethodInfo,
	&IntervalCollection_Optimize_m6138_MethodInfo,
	&IntervalCollection_get_Count_m6139_MethodInfo,
	&IntervalCollection_get_IsSynchronized_m6140_MethodInfo,
	&IntervalCollection_get_SyncRoot_m6141_MethodInfo,
	&IntervalCollection_CopyTo_m6142_MethodInfo,
	&IntervalCollection_GetEnumerator_m6143_MethodInfo,
	NULL
};
extern const MethodInfo IntervalCollection_get_Item_m6134_MethodInfo;
static const PropertyInfo IntervalCollection_t1392____Item_PropertyInfo = 
{
	&IntervalCollection_t1392_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IntervalCollection_get_Item_m6134_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IntervalCollection_get_Count_m6139_MethodInfo;
static const PropertyInfo IntervalCollection_t1392____Count_PropertyInfo = 
{
	&IntervalCollection_t1392_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IntervalCollection_get_Count_m6139_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IntervalCollection_get_IsSynchronized_m6140_MethodInfo;
static const PropertyInfo IntervalCollection_t1392____IsSynchronized_PropertyInfo = 
{
	&IntervalCollection_t1392_il2cpp_TypeInfo/* parent */
	, "IsSynchronized"/* name */
	, &IntervalCollection_get_IsSynchronized_m6140_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IntervalCollection_get_SyncRoot_m6141_MethodInfo;
static const PropertyInfo IntervalCollection_t1392____SyncRoot_PropertyInfo = 
{
	&IntervalCollection_t1392_il2cpp_TypeInfo/* parent */
	, "SyncRoot"/* name */
	, &IntervalCollection_get_SyncRoot_m6141_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IntervalCollection_t1392_PropertyInfos[] =
{
	&IntervalCollection_t1392____Item_PropertyInfo,
	&IntervalCollection_t1392____Count_PropertyInfo,
	&IntervalCollection_t1392____IsSynchronized_PropertyInfo,
	&IntervalCollection_t1392____SyncRoot_PropertyInfo,
	NULL
};
static const Il2CppType* IntervalCollection_t1392_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Enumerator_t1390_0_0_0,
	&CostDelegate_t1391_0_0_0,
};
extern const MethodInfo IntervalCollection_CopyTo_m6142_MethodInfo;
extern const MethodInfo IntervalCollection_GetEnumerator_m6143_MethodInfo;
static const Il2CppMethodReference IntervalCollection_t1392_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&IntervalCollection_get_Count_m6139_MethodInfo,
	&IntervalCollection_get_IsSynchronized_m6140_MethodInfo,
	&IntervalCollection_get_SyncRoot_m6141_MethodInfo,
	&IntervalCollection_CopyTo_m6142_MethodInfo,
	&IntervalCollection_GetEnumerator_m6143_MethodInfo,
};
static bool IntervalCollection_t1392_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICollection_t440_0_0_0;
extern const Il2CppType IEnumerable_t438_0_0_0;
static const Il2CppType* IntervalCollection_t1392_InterfacesTypeInfos[] = 
{
	&ICollection_t440_0_0_0,
	&IEnumerable_t438_0_0_0,
};
static Il2CppInterfaceOffsetPair IntervalCollection_t1392_InterfacesOffsets[] = 
{
	{ &ICollection_t440_0_0_0, 4},
	{ &IEnumerable_t438_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IntervalCollection_t1392_1_0_0;
struct IntervalCollection_t1392;
const Il2CppTypeDefinitionMetadata IntervalCollection_t1392_DefinitionMetadata = 
{
	NULL/* declaringType */
	, IntervalCollection_t1392_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, IntervalCollection_t1392_InterfacesTypeInfos/* implementedInterfaces */
	, IntervalCollection_t1392_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IntervalCollection_t1392_VTable/* vtableMethods */
	, IntervalCollection_t1392_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 692/* fieldStart */

};
TypeInfo IntervalCollection_t1392_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntervalCollection"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, IntervalCollection_t1392_MethodInfos/* methods */
	, IntervalCollection_t1392_PropertyInfos/* properties */
	, NULL/* events */
	, &IntervalCollection_t1392_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 62/* custom_attributes_cache */
	, &IntervalCollection_t1392_0_0_0/* byval_arg */
	, &IntervalCollection_t1392_1_0_0/* this_arg */
	, &IntervalCollection_t1392_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IntervalCollection_t1392)/* instance_size */
	, sizeof (IntervalCollection_t1392)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Parser
#include "System_System_Text_RegularExpressions_Syntax_Parser.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Parser
extern TypeInfo Parser_t1393_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Parser
#include "System_System_Text_RegularExpressions_Syntax_ParserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::.ctor()
extern const MethodInfo Parser__ctor_m6144_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Parser__ctor_m6144/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 738/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseDecimal_m6145_ParameterInfos[] = 
{
	{"str", 0, 134218440, 0, &String_t_0_0_0},
	{"ptr", 1, 134218441, 0, &Int32_t253_1_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseDecimal(System.String,System.Int32&)
extern const MethodInfo Parser_ParseDecimal_m6145_MethodInfo = 
{
	"ParseDecimal"/* name */
	, (methodPointerType)&Parser_ParseDecimal_m6145/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753/* invoker_method */
	, Parser_t1393_Parser_ParseDecimal_m6145_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 739/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseOctal_m6146_ParameterInfos[] = 
{
	{"str", 0, 134218442, 0, &String_t_0_0_0},
	{"ptr", 1, 134218443, 0, &Int32_t253_1_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseOctal(System.String,System.Int32&)
extern const MethodInfo Parser_ParseOctal_m6146_MethodInfo = 
{
	"ParseOctal"/* name */
	, (methodPointerType)&Parser_ParseOctal_m6146/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753/* invoker_method */
	, Parser_t1393_Parser_ParseOctal_m6146_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 740/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseHex_m6147_ParameterInfos[] = 
{
	{"str", 0, 134218444, 0, &String_t_0_0_0},
	{"ptr", 1, 134218445, 0, &Int32_t253_1_0_0},
	{"digits", 2, 134218446, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseHex(System.String,System.Int32&,System.Int32)
extern const MethodInfo Parser_ParseHex_m6147_MethodInfo = 
{
	"ParseHex"/* name */
	, (methodPointerType)&Parser_ParseHex_m6147/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_ParseHex_m6147_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 741/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseNumber_m6148_ParameterInfos[] = 
{
	{"str", 0, 134218447, 0, &String_t_0_0_0},
	{"ptr", 1, 134218448, 0, &Int32_t253_1_0_0},
	{"b", 2, 134218449, 0, &Int32_t253_0_0_0},
	{"min", 3, 134218450, 0, &Int32_t253_0_0_0},
	{"max", 4, 134218451, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseNumber(System.String,System.Int32&,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Parser_ParseNumber_m6148_MethodInfo = 
{
	"ParseNumber"/* name */
	, (methodPointerType)&Parser_ParseNumber_m6148/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_ParseNumber_m6148_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 742/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseName_m6149_ParameterInfos[] = 
{
	{"str", 0, 134218452, 0, &String_t_0_0_0},
	{"ptr", 1, 134218453, 0, &Int32_t253_1_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.Parser::ParseName(System.String,System.Int32&)
extern const MethodInfo Parser_ParseName_m6149_MethodInfo = 
{
	"ParseName"/* name */
	, (methodPointerType)&Parser_ParseName_m6149/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32U26_t753/* invoker_method */
	, Parser_t1393_Parser_ParseName_m6149_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 743/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType RegexOptions_t1367_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseRegularExpression_m6150_ParameterInfos[] = 
{
	{"pattern", 0, 134218454, 0, &String_t_0_0_0},
	{"options", 1, 134218455, 0, &RegexOptions_t1367_0_0_0},
};
extern const Il2CppType RegularExpression_t1399_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.RegularExpression System.Text.RegularExpressions.Syntax.Parser::ParseRegularExpression(System.String,System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseRegularExpression_m6150_MethodInfo = 
{
	"ParseRegularExpression"/* name */
	, (methodPointerType)&Parser_ParseRegularExpression_m6150/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &RegularExpression_t1399_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_ParseRegularExpression_m6150_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 744/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Hashtable_t1262_0_0_0;
extern const Il2CppType Hashtable_t1262_0_0_0;
static const ParameterInfo Parser_t1393_Parser_GetMapping_m6151_ParameterInfos[] = 
{
	{"mapping", 0, 134218456, 0, &Hashtable_t1262_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::GetMapping(System.Collections.Hashtable)
extern const MethodInfo Parser_GetMapping_m6151_MethodInfo = 
{
	"GetMapping"/* name */
	, (methodPointerType)&Parser_GetMapping_m6151/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, Parser_t1393_Parser_GetMapping_m6151_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 745/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Group_t1398_0_0_0;
extern const Il2CppType Group_t1398_0_0_0;
extern const Il2CppType RegexOptions_t1367_0_0_0;
extern const Il2CppType Assertion_t1404_0_0_0;
extern const Il2CppType Assertion_t1404_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseGroup_m6152_ParameterInfos[] = 
{
	{"group", 0, 134218457, 0, &Group_t1398_0_0_0},
	{"options", 1, 134218458, 0, &RegexOptions_t1367_0_0_0},
	{"assertion", 2, 134218459, 0, &Assertion_t1404_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ParseGroup(System.Text.RegularExpressions.Syntax.Group,System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.Syntax.Assertion)
extern const MethodInfo Parser_ParseGroup_m6152_MethodInfo = 
{
	"ParseGroup"/* name */
	, (methodPointerType)&Parser_ParseGroup_m6152/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t/* invoker_method */
	, Parser_t1393_Parser_ParseGroup_m6152_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 746/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1367_1_0_0;
extern const Il2CppType RegexOptions_t1367_1_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseGroupingConstruct_m6153_ParameterInfos[] = 
{
	{"options", 0, 134218460, 0, &RegexOptions_t1367_1_0_0},
};
extern const Il2CppType Expression_t1396_0_0_0;
extern void* RuntimeInvoker_Object_t_RegexOptionsU26_t1491 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseGroupingConstruct(System.Text.RegularExpressions.RegexOptions&)
extern const MethodInfo Parser_ParseGroupingConstruct_m6153_MethodInfo = 
{
	"ParseGroupingConstruct"/* name */
	, (methodPointerType)&Parser_ParseGroupingConstruct_m6153/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_RegexOptionsU26_t1491/* invoker_method */
	, Parser_t1393_Parser_ParseGroupingConstruct_m6153_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 747/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ExpressionAssertion_t1405_0_0_0;
extern const Il2CppType ExpressionAssertion_t1405_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseAssertionType_m6154_ParameterInfos[] = 
{
	{"assertion", 0, 134218461, 0, &ExpressionAssertion_t1405_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::ParseAssertionType(System.Text.RegularExpressions.Syntax.ExpressionAssertion)
extern const MethodInfo Parser_ParseAssertionType_m6154_MethodInfo = 
{
	"ParseAssertionType"/* name */
	, (methodPointerType)&Parser_ParseAssertionType_m6154/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Parser_t1393_Parser_ParseAssertionType_m6154_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 748/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1367_1_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseOptions_m6155_ParameterInfos[] = 
{
	{"options", 0, 134218462, 0, &RegexOptions_t1367_1_0_0},
	{"negate", 1, 134218463, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_RegexOptionsU26_t1491_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ParseOptions(System.Text.RegularExpressions.RegexOptions&,System.Boolean)
extern const MethodInfo Parser_ParseOptions_m6155_MethodInfo = 
{
	"ParseOptions"/* name */
	, (methodPointerType)&Parser_ParseOptions_m6155/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_RegexOptionsU26_t1491_SByte_t274/* invoker_method */
	, Parser_t1393_Parser_ParseOptions_m6155_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 749/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1367_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseCharacterClass_m6156_ParameterInfos[] = 
{
	{"options", 0, 134218464, 0, &RegexOptions_t1367_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseCharacterClass(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseCharacterClass_m6156_MethodInfo = 
{
	"ParseCharacterClass"/* name */
	, (methodPointerType)&Parser_ParseCharacterClass_m6156/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_ParseCharacterClass_m6156_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 750/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType RegexOptions_t1367_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseRepetitionBounds_m6157_ParameterInfos[] = 
{
	{"min", 0, 134218465, 0, &Int32_t253_1_0_2},
	{"max", 1, 134218466, 0, &Int32_t253_1_0_2},
	{"options", 2, 134218467, 0, &RegexOptions_t1367_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32U26_t753_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::ParseRepetitionBounds(System.Int32&,System.Int32&,System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseRepetitionBounds_m6157_MethodInfo = 
{
	"ParseRepetitionBounds"/* name */
	, (methodPointerType)&Parser_ParseRepetitionBounds_m6157/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32U26_t753_Int32U26_t753_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_ParseRepetitionBounds_m6157_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 751/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Category_t1374 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Category System.Text.RegularExpressions.Syntax.Parser::ParseUnicodeCategory()
extern const MethodInfo Parser_ParseUnicodeCategory_m6158_MethodInfo = 
{
	"ParseUnicodeCategory"/* name */
	, (methodPointerType)&Parser_ParseUnicodeCategory_m6158/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Category_t1374_0_0_0/* return_type */
	, RuntimeInvoker_Category_t1374/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 752/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1367_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseSpecial_m6159_ParameterInfos[] = 
{
	{"options", 0, 134218468, 0, &RegexOptions_t1367_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseSpecial(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseSpecial_m6159_MethodInfo = 
{
	"ParseSpecial"/* name */
	, (methodPointerType)&Parser_ParseSpecial_m6159/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_ParseSpecial_m6159_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 753/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseEscape()
extern const MethodInfo Parser_ParseEscape_m6160_MethodInfo = 
{
	"ParseEscape"/* name */
	, (methodPointerType)&Parser_ParseEscape_m6160/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 754/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.Parser::ParseName()
extern const MethodInfo Parser_ParseName_m6161_MethodInfo = 
{
	"ParseName"/* name */
	, (methodPointerType)&Parser_ParseName_m6161/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 755/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo Parser_t1393_Parser_IsNameChar_m6162_ParameterInfos[] = 
{
	{"c", 0, 134218469, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsNameChar(System.Char)
extern const MethodInfo Parser_IsNameChar_m6162_MethodInfo = 
{
	"IsNameChar"/* name */
	, (methodPointerType)&Parser_IsNameChar_m6162/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int16_t752/* invoker_method */
	, Parser_t1393_Parser_IsNameChar_m6162_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 756/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseNumber_m6163_ParameterInfos[] = 
{
	{"b", 0, 134218470, 0, &Int32_t253_0_0_0},
	{"min", 1, 134218471, 0, &Int32_t253_0_0_0},
	{"max", 2, 134218472, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseNumber(System.Int32,System.Int32,System.Int32)
extern const MethodInfo Parser_ParseNumber_m6163_MethodInfo = 
{
	"ParseNumber"/* name */
	, (methodPointerType)&Parser_ParseNumber_m6163/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_ParseNumber_m6163_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 757/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ParseDigit_m6164_ParameterInfos[] = 
{
	{"c", 0, 134218473, 0, &Char_t682_0_0_0},
	{"b", 1, 134218474, 0, &Int32_t253_0_0_0},
	{"n", 2, 134218475, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int16_t752_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseDigit(System.Char,System.Int32,System.Int32)
extern const MethodInfo Parser_ParseDigit_m6164_MethodInfo = 
{
	"ParseDigit"/* name */
	, (methodPointerType)&Parser_ParseDigit_m6164/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int16_t752_Int32_t253_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_ParseDigit_m6164_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 758/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Parser_t1393_Parser_ConsumeWhitespace_m6165_ParameterInfos[] = 
{
	{"ignore", 0, 134218476, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ConsumeWhitespace(System.Boolean)
extern const MethodInfo Parser_ConsumeWhitespace_m6165_MethodInfo = 
{
	"ConsumeWhitespace"/* name */
	, (methodPointerType)&Parser_ConsumeWhitespace_m6165/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Parser_t1393_Parser_ConsumeWhitespace_m6165_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 759/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ResolveReferences()
extern const MethodInfo Parser_ResolveReferences_m6166_MethodInfo = 
{
	"ResolveReferences"/* name */
	, (methodPointerType)&Parser_ResolveReferences_m6166/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 760/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ArrayList_t1271_0_0_0;
extern const Il2CppType ArrayList_t1271_0_0_0;
static const ParameterInfo Parser_t1393_Parser_HandleExplicitNumericGroups_m6167_ParameterInfos[] = 
{
	{"explicit_numeric_groups", 0, 134218477, 0, &ArrayList_t1271_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::HandleExplicitNumericGroups(System.Collections.ArrayList)
extern const MethodInfo Parser_HandleExplicitNumericGroups_m6167_MethodInfo = 
{
	"HandleExplicitNumericGroups"/* name */
	, (methodPointerType)&Parser_HandleExplicitNumericGroups_m6167/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Parser_t1393_Parser_HandleExplicitNumericGroups_m6167_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 761/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1367_0_0_0;
static const ParameterInfo Parser_t1393_Parser_IsIgnoreCase_m6168_ParameterInfos[] = 
{
	{"options", 0, 134218478, 0, &RegexOptions_t1367_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsIgnoreCase(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsIgnoreCase_m6168_MethodInfo = 
{
	"IsIgnoreCase"/* name */
	, (methodPointerType)&Parser_IsIgnoreCase_m6168/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_IsIgnoreCase_m6168_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 762/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1367_0_0_0;
static const ParameterInfo Parser_t1393_Parser_IsMultiline_m6169_ParameterInfos[] = 
{
	{"options", 0, 134218479, 0, &RegexOptions_t1367_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsMultiline(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsMultiline_m6169_MethodInfo = 
{
	"IsMultiline"/* name */
	, (methodPointerType)&Parser_IsMultiline_m6169/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_IsMultiline_m6169_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 763/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1367_0_0_0;
static const ParameterInfo Parser_t1393_Parser_IsExplicitCapture_m6170_ParameterInfos[] = 
{
	{"options", 0, 134218480, 0, &RegexOptions_t1367_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsExplicitCapture(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsExplicitCapture_m6170_MethodInfo = 
{
	"IsExplicitCapture"/* name */
	, (methodPointerType)&Parser_IsExplicitCapture_m6170/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_IsExplicitCapture_m6170_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1367_0_0_0;
static const ParameterInfo Parser_t1393_Parser_IsSingleline_m6171_ParameterInfos[] = 
{
	{"options", 0, 134218481, 0, &RegexOptions_t1367_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsSingleline(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsSingleline_m6171_MethodInfo = 
{
	"IsSingleline"/* name */
	, (methodPointerType)&Parser_IsSingleline_m6171/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_IsSingleline_m6171_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1367_0_0_0;
static const ParameterInfo Parser_t1393_Parser_IsIgnorePatternWhitespace_m6172_ParameterInfos[] = 
{
	{"options", 0, 134218482, 0, &RegexOptions_t1367_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsIgnorePatternWhitespace(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsIgnorePatternWhitespace_m6172_MethodInfo = 
{
	"IsIgnorePatternWhitespace"/* name */
	, (methodPointerType)&Parser_IsIgnorePatternWhitespace_m6172/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_IsIgnorePatternWhitespace_m6172_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1367_0_0_0;
static const ParameterInfo Parser_t1393_Parser_IsECMAScript_m6173_ParameterInfos[] = 
{
	{"options", 0, 134218483, 0, &RegexOptions_t1367_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsECMAScript(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsECMAScript_m6173_MethodInfo = 
{
	"IsECMAScript"/* name */
	, (methodPointerType)&Parser_IsECMAScript_m6173/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253/* invoker_method */
	, Parser_t1393_Parser_IsECMAScript_m6173_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Parser_t1393_Parser_NewParseException_m6174_ParameterInfos[] = 
{
	{"msg", 0, 134218484, 0, &String_t_0_0_0},
};
extern const Il2CppType ArgumentException_t700_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.ArgumentException System.Text.RegularExpressions.Syntax.Parser::NewParseException(System.String)
extern const MethodInfo Parser_NewParseException_m6174_MethodInfo = 
{
	"NewParseException"/* name */
	, (methodPointerType)&Parser_NewParseException_m6174/* method */
	, &Parser_t1393_il2cpp_TypeInfo/* declaring_type */
	, &ArgumentException_t700_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Parser_t1393_Parser_NewParseException_m6174_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Parser_t1393_MethodInfos[] =
{
	&Parser__ctor_m6144_MethodInfo,
	&Parser_ParseDecimal_m6145_MethodInfo,
	&Parser_ParseOctal_m6146_MethodInfo,
	&Parser_ParseHex_m6147_MethodInfo,
	&Parser_ParseNumber_m6148_MethodInfo,
	&Parser_ParseName_m6149_MethodInfo,
	&Parser_ParseRegularExpression_m6150_MethodInfo,
	&Parser_GetMapping_m6151_MethodInfo,
	&Parser_ParseGroup_m6152_MethodInfo,
	&Parser_ParseGroupingConstruct_m6153_MethodInfo,
	&Parser_ParseAssertionType_m6154_MethodInfo,
	&Parser_ParseOptions_m6155_MethodInfo,
	&Parser_ParseCharacterClass_m6156_MethodInfo,
	&Parser_ParseRepetitionBounds_m6157_MethodInfo,
	&Parser_ParseUnicodeCategory_m6158_MethodInfo,
	&Parser_ParseSpecial_m6159_MethodInfo,
	&Parser_ParseEscape_m6160_MethodInfo,
	&Parser_ParseName_m6161_MethodInfo,
	&Parser_IsNameChar_m6162_MethodInfo,
	&Parser_ParseNumber_m6163_MethodInfo,
	&Parser_ParseDigit_m6164_MethodInfo,
	&Parser_ConsumeWhitespace_m6165_MethodInfo,
	&Parser_ResolveReferences_m6166_MethodInfo,
	&Parser_HandleExplicitNumericGroups_m6167_MethodInfo,
	&Parser_IsIgnoreCase_m6168_MethodInfo,
	&Parser_IsMultiline_m6169_MethodInfo,
	&Parser_IsExplicitCapture_m6170_MethodInfo,
	&Parser_IsSingleline_m6171_MethodInfo,
	&Parser_IsIgnorePatternWhitespace_m6172_MethodInfo,
	&Parser_IsECMAScript_m6173_MethodInfo,
	&Parser_NewParseException_m6174_MethodInfo,
	NULL
};
static const Il2CppMethodReference Parser_t1393_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Parser_t1393_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Parser_t1393_0_0_0;
extern const Il2CppType Parser_t1393_1_0_0;
struct Parser_t1393;
const Il2CppTypeDefinitionMetadata Parser_t1393_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Parser_t1393_VTable/* vtableMethods */
	, Parser_t1393_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 693/* fieldStart */

};
TypeInfo Parser_t1393_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Parser"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Parser_t1393_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Parser_t1393_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Parser_t1393_0_0_0/* byval_arg */
	, &Parser_t1393_1_0_0/* this_arg */
	, &Parser_t1393_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Parser_t1393)/* instance_size */
	, sizeof (Parser_t1393)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.QuickSearch
#include "System_System_Text_RegularExpressions_QuickSearch.h"
// Metadata Definition System.Text.RegularExpressions.QuickSearch
extern TypeInfo QuickSearch_t1386_il2cpp_TypeInfo;
// System.Text.RegularExpressions.QuickSearch
#include "System_System_Text_RegularExpressions_QuickSearchMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo QuickSearch_t1386_QuickSearch__ctor_m6175_ParameterInfos[] = 
{
	{"str", 0, 134218485, 0, &String_t_0_0_0},
	{"ignore", 1, 134218486, 0, &Boolean_t273_0_0_0},
	{"reverse", 2, 134218487, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::.ctor(System.String,System.Boolean,System.Boolean)
extern const MethodInfo QuickSearch__ctor_m6175_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QuickSearch__ctor_m6175/* method */
	, &QuickSearch_t1386_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274/* invoker_method */
	, QuickSearch_t1386_QuickSearch__ctor_m6175_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::.cctor()
extern const MethodInfo QuickSearch__cctor_m6176_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&QuickSearch__cctor_m6176/* method */
	, &QuickSearch_t1386_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::get_Length()
extern const MethodInfo QuickSearch_get_Length_m6177_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&QuickSearch_get_Length_m6177/* method */
	, &QuickSearch_t1386_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo QuickSearch_t1386_QuickSearch_Search_m6178_ParameterInfos[] = 
{
	{"text", 0, 134218488, 0, &String_t_0_0_0},
	{"start", 1, 134218489, 0, &Int32_t253_0_0_0},
	{"end", 2, 134218490, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::Search(System.String,System.Int32,System.Int32)
extern const MethodInfo QuickSearch_Search_m6178_MethodInfo = 
{
	"Search"/* name */
	, (methodPointerType)&QuickSearch_Search_m6178/* method */
	, &QuickSearch_t1386_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, QuickSearch_t1386_QuickSearch_Search_m6178_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::SetupShiftTable()
extern const MethodInfo QuickSearch_SetupShiftTable_m6179_MethodInfo = 
{
	"SetupShiftTable"/* name */
	, (methodPointerType)&QuickSearch_SetupShiftTable_m6179/* method */
	, &QuickSearch_t1386_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo QuickSearch_t1386_QuickSearch_GetShiftDistance_m6180_ParameterInfos[] = 
{
	{"c", 0, 134218491, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::GetShiftDistance(System.Char)
extern const MethodInfo QuickSearch_GetShiftDistance_m6180_MethodInfo = 
{
	"GetShiftDistance"/* name */
	, (methodPointerType)&QuickSearch_GetShiftDistance_m6180/* method */
	, &QuickSearch_t1386_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int16_t752/* invoker_method */
	, QuickSearch_t1386_QuickSearch_GetShiftDistance_m6180_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo QuickSearch_t1386_QuickSearch_GetChar_m6181_ParameterInfos[] = 
{
	{"c", 0, 134218492, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Char_t682_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Char System.Text.RegularExpressions.QuickSearch::GetChar(System.Char)
extern const MethodInfo QuickSearch_GetChar_m6181_MethodInfo = 
{
	"GetChar"/* name */
	, (methodPointerType)&QuickSearch_GetChar_m6181/* method */
	, &QuickSearch_t1386_il2cpp_TypeInfo/* declaring_type */
	, &Char_t682_0_0_0/* return_type */
	, RuntimeInvoker_Char_t682_Int16_t752/* invoker_method */
	, QuickSearch_t1386_QuickSearch_GetChar_m6181_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* QuickSearch_t1386_MethodInfos[] =
{
	&QuickSearch__ctor_m6175_MethodInfo,
	&QuickSearch__cctor_m6176_MethodInfo,
	&QuickSearch_get_Length_m6177_MethodInfo,
	&QuickSearch_Search_m6178_MethodInfo,
	&QuickSearch_SetupShiftTable_m6179_MethodInfo,
	&QuickSearch_GetShiftDistance_m6180_MethodInfo,
	&QuickSearch_GetChar_m6181_MethodInfo,
	NULL
};
extern const MethodInfo QuickSearch_get_Length_m6177_MethodInfo;
static const PropertyInfo QuickSearch_t1386____Length_PropertyInfo = 
{
	&QuickSearch_t1386_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &QuickSearch_get_Length_m6177_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* QuickSearch_t1386_PropertyInfos[] =
{
	&QuickSearch_t1386____Length_PropertyInfo,
	NULL
};
static const Il2CppMethodReference QuickSearch_t1386_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool QuickSearch_t1386_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType QuickSearch_t1386_0_0_0;
extern const Il2CppType QuickSearch_t1386_1_0_0;
struct QuickSearch_t1386;
const Il2CppTypeDefinitionMetadata QuickSearch_t1386_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, QuickSearch_t1386_VTable/* vtableMethods */
	, QuickSearch_t1386_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 699/* fieldStart */

};
TypeInfo QuickSearch_t1386_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "QuickSearch"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, QuickSearch_t1386_MethodInfos/* methods */
	, QuickSearch_t1386_PropertyInfos/* properties */
	, NULL/* events */
	, &QuickSearch_t1386_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QuickSearch_t1386_0_0_0/* byval_arg */
	, &QuickSearch_t1386_1_0_0/* this_arg */
	, &QuickSearch_t1386_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QuickSearch_t1386)/* instance_size */
	, sizeof (QuickSearch_t1386)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(QuickSearch_t1386_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.ReplacementEvaluator
#include "System_System_Text_RegularExpressions_ReplacementEvaluator.h"
// Metadata Definition System.Text.RegularExpressions.ReplacementEvaluator
extern TypeInfo ReplacementEvaluator_t1394_il2cpp_TypeInfo;
// System.Text.RegularExpressions.ReplacementEvaluator
#include "System_System_Text_RegularExpressions_ReplacementEvaluatorMethodDeclarations.h"
extern const Il2CppType Regex_t421_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1394_ReplacementEvaluator__ctor_m6182_ParameterInfos[] = 
{
	{"regex", 0, 134218493, 0, &Regex_t421_0_0_0},
	{"replacement", 1, 134218494, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::.ctor(System.Text.RegularExpressions.Regex,System.String)
extern const MethodInfo ReplacementEvaluator__ctor_m6182_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReplacementEvaluator__ctor_m6182/* method */
	, &ReplacementEvaluator_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ReplacementEvaluator_t1394_ReplacementEvaluator__ctor_m6182_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t422_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1394_ReplacementEvaluator_Evaluate_m6183_ParameterInfos[] = 
{
	{"match", 0, 134218495, 0, &Match_t422_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.ReplacementEvaluator::Evaluate(System.Text.RegularExpressions.Match)
extern const MethodInfo ReplacementEvaluator_Evaluate_m6183_MethodInfo = 
{
	"Evaluate"/* name */
	, (methodPointerType)&ReplacementEvaluator_Evaluate_m6183/* method */
	, &ReplacementEvaluator_t1394_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReplacementEvaluator_t1394_ReplacementEvaluator_Evaluate_m6183_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t422_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1394_ReplacementEvaluator_EvaluateAppend_m6184_ParameterInfos[] = 
{
	{"match", 0, 134218496, 0, &Match_t422_0_0_0},
	{"sb", 1, 134218497, 0, &StringBuilder_t657_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::EvaluateAppend(System.Text.RegularExpressions.Match,System.Text.StringBuilder)
extern const MethodInfo ReplacementEvaluator_EvaluateAppend_m6184_MethodInfo = 
{
	"EvaluateAppend"/* name */
	, (methodPointerType)&ReplacementEvaluator_EvaluateAppend_m6184/* method */
	, &ReplacementEvaluator_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ReplacementEvaluator_t1394_ReplacementEvaluator_EvaluateAppend_m6184_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.ReplacementEvaluator::get_NeedsGroupsOrCaptures()
extern const MethodInfo ReplacementEvaluator_get_NeedsGroupsOrCaptures_m6185_MethodInfo = 
{
	"get_NeedsGroupsOrCaptures"/* name */
	, (methodPointerType)&ReplacementEvaluator_get_NeedsGroupsOrCaptures_m6185/* method */
	, &ReplacementEvaluator_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1394_ReplacementEvaluator_Ensure_m6186_ParameterInfos[] = 
{
	{"size", 0, 134218498, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::Ensure(System.Int32)
extern const MethodInfo ReplacementEvaluator_Ensure_m6186_MethodInfo = 
{
	"Ensure"/* name */
	, (methodPointerType)&ReplacementEvaluator_Ensure_m6186/* method */
	, &ReplacementEvaluator_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ReplacementEvaluator_t1394_ReplacementEvaluator_Ensure_m6186_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1394_ReplacementEvaluator_AddFromReplacement_m6187_ParameterInfos[] = 
{
	{"start", 0, 134218499, 0, &Int32_t253_0_0_0},
	{"end", 1, 134218500, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::AddFromReplacement(System.Int32,System.Int32)
extern const MethodInfo ReplacementEvaluator_AddFromReplacement_m6187_MethodInfo = 
{
	"AddFromReplacement"/* name */
	, (methodPointerType)&ReplacementEvaluator_AddFromReplacement_m6187/* method */
	, &ReplacementEvaluator_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253/* invoker_method */
	, ReplacementEvaluator_t1394_ReplacementEvaluator_AddFromReplacement_m6187_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1394_ReplacementEvaluator_AddInt_m6188_ParameterInfos[] = 
{
	{"i", 0, 134218501, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::AddInt(System.Int32)
extern const MethodInfo ReplacementEvaluator_AddInt_m6188_MethodInfo = 
{
	"AddInt"/* name */
	, (methodPointerType)&ReplacementEvaluator_AddInt_m6188/* method */
	, &ReplacementEvaluator_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ReplacementEvaluator_t1394_ReplacementEvaluator_AddInt_m6188_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::Compile()
extern const MethodInfo ReplacementEvaluator_Compile_m6189_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&ReplacementEvaluator_Compile_m6189/* method */
	, &ReplacementEvaluator_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_0;
static const ParameterInfo ReplacementEvaluator_t1394_ReplacementEvaluator_CompileTerm_m6190_ParameterInfos[] = 
{
	{"ptr", 0, 134218502, 0, &Int32_t253_1_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.ReplacementEvaluator::CompileTerm(System.Int32&)
extern const MethodInfo ReplacementEvaluator_CompileTerm_m6190_MethodInfo = 
{
	"CompileTerm"/* name */
	, (methodPointerType)&ReplacementEvaluator_CompileTerm_m6190/* method */
	, &ReplacementEvaluator_t1394_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int32U26_t753/* invoker_method */
	, ReplacementEvaluator_t1394_ReplacementEvaluator_CompileTerm_m6190_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReplacementEvaluator_t1394_MethodInfos[] =
{
	&ReplacementEvaluator__ctor_m6182_MethodInfo,
	&ReplacementEvaluator_Evaluate_m6183_MethodInfo,
	&ReplacementEvaluator_EvaluateAppend_m6184_MethodInfo,
	&ReplacementEvaluator_get_NeedsGroupsOrCaptures_m6185_MethodInfo,
	&ReplacementEvaluator_Ensure_m6186_MethodInfo,
	&ReplacementEvaluator_AddFromReplacement_m6187_MethodInfo,
	&ReplacementEvaluator_AddInt_m6188_MethodInfo,
	&ReplacementEvaluator_Compile_m6189_MethodInfo,
	&ReplacementEvaluator_CompileTerm_m6190_MethodInfo,
	NULL
};
extern const MethodInfo ReplacementEvaluator_get_NeedsGroupsOrCaptures_m6185_MethodInfo;
static const PropertyInfo ReplacementEvaluator_t1394____NeedsGroupsOrCaptures_PropertyInfo = 
{
	&ReplacementEvaluator_t1394_il2cpp_TypeInfo/* parent */
	, "NeedsGroupsOrCaptures"/* name */
	, &ReplacementEvaluator_get_NeedsGroupsOrCaptures_m6185_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ReplacementEvaluator_t1394_PropertyInfos[] =
{
	&ReplacementEvaluator_t1394____NeedsGroupsOrCaptures_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ReplacementEvaluator_t1394_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ReplacementEvaluator_t1394_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ReplacementEvaluator_t1394_0_0_0;
extern const Il2CppType ReplacementEvaluator_t1394_1_0_0;
struct ReplacementEvaluator_t1394;
const Il2CppTypeDefinitionMetadata ReplacementEvaluator_t1394_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReplacementEvaluator_t1394_VTable/* vtableMethods */
	, ReplacementEvaluator_t1394_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 706/* fieldStart */

};
TypeInfo ReplacementEvaluator_t1394_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReplacementEvaluator"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, ReplacementEvaluator_t1394_MethodInfos/* methods */
	, ReplacementEvaluator_t1394_PropertyInfos/* properties */
	, NULL/* events */
	, &ReplacementEvaluator_t1394_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReplacementEvaluator_t1394_0_0_0/* byval_arg */
	, &ReplacementEvaluator_t1394_1_0_0/* this_arg */
	, &ReplacementEvaluator_t1394_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReplacementEvaluator_t1394)/* instance_size */
	, sizeof (ReplacementEvaluator_t1394)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.ExpressionCollection
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColle.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionCollection
extern TypeInfo ExpressionCollection_t1395_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.ExpressionCollection
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::.ctor()
extern const MethodInfo ExpressionCollection__ctor_m6191_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExpressionCollection__ctor_m6191/* method */
	, &ExpressionCollection_t1395_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1396_0_0_0;
static const ParameterInfo ExpressionCollection_t1395_ExpressionCollection_Add_m6192_ParameterInfos[] = 
{
	{"e", 0, 134218503, 0, &Expression_t1396_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::Add(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo ExpressionCollection_Add_m6192_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&ExpressionCollection_Add_m6192/* method */
	, &ExpressionCollection_t1395_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ExpressionCollection_t1395_ExpressionCollection_Add_m6192_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ExpressionCollection_t1395_ExpressionCollection_get_Item_m6193_ParameterInfos[] = 
{
	{"i", 0, 134218504, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionCollection::get_Item(System.Int32)
extern const MethodInfo ExpressionCollection_get_Item_m6193_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&ExpressionCollection_get_Item_m6193/* method */
	, &ExpressionCollection_t1395_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, ExpressionCollection_t1395_ExpressionCollection_get_Item_m6193_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Expression_t1396_0_0_0;
static const ParameterInfo ExpressionCollection_t1395_ExpressionCollection_set_Item_m6194_ParameterInfos[] = 
{
	{"i", 0, 134218505, 0, &Int32_t253_0_0_0},
	{"value", 1, 134218506, 0, &Expression_t1396_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::set_Item(System.Int32,System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo ExpressionCollection_set_Item_m6194_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&ExpressionCollection_set_Item_m6194/* method */
	, &ExpressionCollection_t1395_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Object_t/* invoker_method */
	, ExpressionCollection_t1395_ExpressionCollection_set_Item_m6194_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ExpressionCollection_t1395_ExpressionCollection_OnValidate_m6195_ParameterInfos[] = 
{
	{"o", 0, 134218507, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::OnValidate(System.Object)
extern const MethodInfo ExpressionCollection_OnValidate_m6195_MethodInfo = 
{
	"OnValidate"/* name */
	, (methodPointerType)&ExpressionCollection_OnValidate_m6195/* method */
	, &ExpressionCollection_t1395_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ExpressionCollection_t1395_ExpressionCollection_OnValidate_m6195_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExpressionCollection_t1395_MethodInfos[] =
{
	&ExpressionCollection__ctor_m6191_MethodInfo,
	&ExpressionCollection_Add_m6192_MethodInfo,
	&ExpressionCollection_get_Item_m6193_MethodInfo,
	&ExpressionCollection_set_Item_m6194_MethodInfo,
	&ExpressionCollection_OnValidate_m6195_MethodInfo,
	NULL
};
extern const MethodInfo ExpressionCollection_get_Item_m6193_MethodInfo;
extern const MethodInfo ExpressionCollection_set_Item_m6194_MethodInfo;
static const PropertyInfo ExpressionCollection_t1395____Item_PropertyInfo = 
{
	&ExpressionCollection_t1395_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &ExpressionCollection_get_Item_m6193_MethodInfo/* get */
	, &ExpressionCollection_set_Item_m6194_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ExpressionCollection_t1395_PropertyInfos[] =
{
	&ExpressionCollection_t1395____Item_PropertyInfo,
	NULL
};
extern const MethodInfo CollectionBase_GetEnumerator_m6628_MethodInfo;
extern const MethodInfo CollectionBase_get_Count_m6629_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_ICollection_get_IsSynchronized_m6630_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_ICollection_get_SyncRoot_m6631_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_ICollection_CopyTo_m6632_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_get_IsFixedSize_m6633_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_get_IsReadOnly_m6634_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_get_Item_m6635_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_set_Item_m6636_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Add_m6637_MethodInfo;
extern const MethodInfo CollectionBase_Clear_m6638_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Contains_m6639_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_IndexOf_m6640_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Insert_m6641_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Remove_m6642_MethodInfo;
extern const MethodInfo CollectionBase_RemoveAt_m6643_MethodInfo;
extern const MethodInfo CollectionBase_OnClear_m6644_MethodInfo;
extern const MethodInfo CollectionBase_OnClearComplete_m6645_MethodInfo;
extern const MethodInfo CollectionBase_OnInsert_m6646_MethodInfo;
extern const MethodInfo CollectionBase_OnInsertComplete_m6647_MethodInfo;
extern const MethodInfo CollectionBase_OnRemove_m6648_MethodInfo;
extern const MethodInfo CollectionBase_OnRemoveComplete_m6649_MethodInfo;
extern const MethodInfo CollectionBase_OnSet_m6650_MethodInfo;
extern const MethodInfo CollectionBase_OnSetComplete_m6651_MethodInfo;
extern const MethodInfo ExpressionCollection_OnValidate_m6195_MethodInfo;
static const Il2CppMethodReference ExpressionCollection_t1395_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&CollectionBase_GetEnumerator_m6628_MethodInfo,
	&CollectionBase_get_Count_m6629_MethodInfo,
	&CollectionBase_System_Collections_ICollection_get_IsSynchronized_m6630_MethodInfo,
	&CollectionBase_System_Collections_ICollection_get_SyncRoot_m6631_MethodInfo,
	&CollectionBase_System_Collections_ICollection_CopyTo_m6632_MethodInfo,
	&CollectionBase_System_Collections_IList_get_IsFixedSize_m6633_MethodInfo,
	&CollectionBase_System_Collections_IList_get_IsReadOnly_m6634_MethodInfo,
	&CollectionBase_System_Collections_IList_get_Item_m6635_MethodInfo,
	&CollectionBase_System_Collections_IList_set_Item_m6636_MethodInfo,
	&CollectionBase_System_Collections_IList_Add_m6637_MethodInfo,
	&CollectionBase_Clear_m6638_MethodInfo,
	&CollectionBase_System_Collections_IList_Contains_m6639_MethodInfo,
	&CollectionBase_System_Collections_IList_IndexOf_m6640_MethodInfo,
	&CollectionBase_System_Collections_IList_Insert_m6641_MethodInfo,
	&CollectionBase_System_Collections_IList_Remove_m6642_MethodInfo,
	&CollectionBase_RemoveAt_m6643_MethodInfo,
	&CollectionBase_OnClear_m6644_MethodInfo,
	&CollectionBase_OnClearComplete_m6645_MethodInfo,
	&CollectionBase_OnInsert_m6646_MethodInfo,
	&CollectionBase_OnInsertComplete_m6647_MethodInfo,
	&CollectionBase_OnRemove_m6648_MethodInfo,
	&CollectionBase_OnRemoveComplete_m6649_MethodInfo,
	&CollectionBase_OnSet_m6650_MethodInfo,
	&CollectionBase_OnSetComplete_m6651_MethodInfo,
	&ExpressionCollection_OnValidate_m6195_MethodInfo,
};
static bool ExpressionCollection_t1395_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExpressionCollection_t1395_InterfacesOffsets[] = 
{
	{ &IEnumerable_t438_0_0_0, 4},
	{ &ICollection_t440_0_0_0, 5},
	{ &IList_t1195_0_0_0, 9},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ExpressionCollection_t1395_0_0_0;
extern const Il2CppType ExpressionCollection_t1395_1_0_0;
extern const Il2CppType CollectionBase_t1327_0_0_0;
struct ExpressionCollection_t1395;
const Il2CppTypeDefinitionMetadata ExpressionCollection_t1395_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExpressionCollection_t1395_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t1327_0_0_0/* parent */
	, ExpressionCollection_t1395_VTable/* vtableMethods */
	, ExpressionCollection_t1395_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExpressionCollection_t1395_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExpressionCollection"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, ExpressionCollection_t1395_MethodInfos/* methods */
	, ExpressionCollection_t1395_PropertyInfos/* properties */
	, NULL/* events */
	, &ExpressionCollection_t1395_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 63/* custom_attributes_cache */
	, &ExpressionCollection_t1395_0_0_0/* byval_arg */
	, &ExpressionCollection_t1395_1_0_0/* this_arg */
	, &ExpressionCollection_t1395_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExpressionCollection_t1395)/* instance_size */
	, sizeof (ExpressionCollection_t1395)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_Expression.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Expression
extern TypeInfo Expression_t1396_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_ExpressionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::.ctor()
extern const MethodInfo Expression__ctor_m6196_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Expression__ctor_m6196/* method */
	, &Expression_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Expression_t1396_Expression_Compile_m6606_ParameterInfos[] = 
{
	{"cmp", 0, 134218508, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218509, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Expression_Compile_m6606_MethodInfo = 
{
	"Compile"/* name */
	, NULL/* method */
	, &Expression_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, Expression_t1396_Expression_Compile_m6606_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
static const ParameterInfo Expression_t1396_Expression_GetWidth_m6607_ParameterInfos[] = 
{
	{"min", 0, 134218510, 0, &Int32_t253_1_0_2},
	{"max", 1, 134218511, 0, &Int32_t253_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Expression_GetWidth_m6607_MethodInfo = 
{
	"GetWidth"/* name */
	, NULL/* method */
	, &Expression_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753/* invoker_method */
	, Expression_t1396_Expression_GetWidth_m6607_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Expression::GetFixedWidth()
extern const MethodInfo Expression_GetFixedWidth_m6197_MethodInfo = 
{
	"GetFixedWidth"/* name */
	, (methodPointerType)&Expression_GetFixedWidth_m6197/* method */
	, &Expression_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Expression_t1396_Expression_GetAnchorInfo_m6198_ParameterInfos[] = 
{
	{"reverse", 0, 134218512, 0, &Boolean_t273_0_0_0},
};
extern const Il2CppType AnchorInfo_t1414_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Expression::GetAnchorInfo(System.Boolean)
extern const MethodInfo Expression_GetAnchorInfo_m6198_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Expression_GetAnchorInfo_m6198/* method */
	, &Expression_t1396_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1414_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t274/* invoker_method */
	, Expression_t1396_Expression_GetAnchorInfo_m6198_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Expression::IsComplex()
extern const MethodInfo Expression_IsComplex_m6608_MethodInfo = 
{
	"IsComplex"/* name */
	, NULL/* method */
	, &Expression_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Expression_t1396_MethodInfos[] =
{
	&Expression__ctor_m6196_MethodInfo,
	&Expression_Compile_m6606_MethodInfo,
	&Expression_GetWidth_m6607_MethodInfo,
	&Expression_GetFixedWidth_m6197_MethodInfo,
	&Expression_GetAnchorInfo_m6198_MethodInfo,
	&Expression_IsComplex_m6608_MethodInfo,
	NULL
};
extern const MethodInfo Expression_GetAnchorInfo_m6198_MethodInfo;
static const Il2CppMethodReference Expression_t1396_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
	NULL,
	&Expression_GetAnchorInfo_m6198_MethodInfo,
	NULL,
};
static bool Expression_t1396_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Expression_t1396_1_0_0;
struct Expression_t1396;
const Il2CppTypeDefinitionMetadata Expression_t1396_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Expression_t1396_VTable/* vtableMethods */
	, Expression_t1396_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Expression_t1396_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Expression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Expression_t1396_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Expression_t1396_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Expression_t1396_0_0_0/* byval_arg */
	, &Expression_t1396_1_0_0/* this_arg */
	, &Expression_t1396_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Expression_t1396)/* instance_size */
	, sizeof (Expression_t1396)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CompositeExpression
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpres.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CompositeExpression
extern TypeInfo CompositeExpression_t1397_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CompositeExpression
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpresMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CompositeExpression::.ctor()
extern const MethodInfo CompositeExpression__ctor_m6199_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CompositeExpression__ctor_m6199/* method */
	, &CompositeExpression_t1397_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.CompositeExpression::get_Expressions()
extern const MethodInfo CompositeExpression_get_Expressions_m6200_MethodInfo = 
{
	"get_Expressions"/* name */
	, (methodPointerType)&CompositeExpression_get_Expressions_m6200/* method */
	, &CompositeExpression_t1397_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionCollection_t1395_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo CompositeExpression_t1397_CompositeExpression_GetWidth_m6201_ParameterInfos[] = 
{
	{"min", 0, 134218513, 0, &Int32_t253_1_0_2},
	{"max", 1, 134218514, 0, &Int32_t253_1_0_2},
	{"count", 2, 134218515, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CompositeExpression::GetWidth(System.Int32&,System.Int32&,System.Int32)
extern const MethodInfo CompositeExpression_GetWidth_m6201_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&CompositeExpression_GetWidth_m6201/* method */
	, &CompositeExpression_t1397_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753_Int32_t253/* invoker_method */
	, CompositeExpression_t1397_CompositeExpression_GetWidth_m6201_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CompositeExpression::IsComplex()
extern const MethodInfo CompositeExpression_IsComplex_m6202_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CompositeExpression_IsComplex_m6202/* method */
	, &CompositeExpression_t1397_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CompositeExpression_t1397_MethodInfos[] =
{
	&CompositeExpression__ctor_m6199_MethodInfo,
	&CompositeExpression_get_Expressions_m6200_MethodInfo,
	&CompositeExpression_GetWidth_m6201_MethodInfo,
	&CompositeExpression_IsComplex_m6202_MethodInfo,
	NULL
};
extern const MethodInfo CompositeExpression_get_Expressions_m6200_MethodInfo;
static const PropertyInfo CompositeExpression_t1397____Expressions_PropertyInfo = 
{
	&CompositeExpression_t1397_il2cpp_TypeInfo/* parent */
	, "Expressions"/* name */
	, &CompositeExpression_get_Expressions_m6200_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CompositeExpression_t1397_PropertyInfos[] =
{
	&CompositeExpression_t1397____Expressions_PropertyInfo,
	NULL
};
extern const MethodInfo CompositeExpression_IsComplex_m6202_MethodInfo;
static const Il2CppMethodReference CompositeExpression_t1397_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
	NULL,
	&Expression_GetAnchorInfo_m6198_MethodInfo,
	&CompositeExpression_IsComplex_m6202_MethodInfo,
};
static bool CompositeExpression_t1397_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CompositeExpression_t1397_0_0_0;
extern const Il2CppType CompositeExpression_t1397_1_0_0;
struct CompositeExpression_t1397;
const Il2CppTypeDefinitionMetadata CompositeExpression_t1397_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1396_0_0_0/* parent */
	, CompositeExpression_t1397_VTable/* vtableMethods */
	, CompositeExpression_t1397_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 710/* fieldStart */

};
TypeInfo CompositeExpression_t1397_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompositeExpression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CompositeExpression_t1397_MethodInfos/* methods */
	, CompositeExpression_t1397_PropertyInfos/* properties */
	, NULL/* events */
	, &CompositeExpression_t1397_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CompositeExpression_t1397_0_0_0/* byval_arg */
	, &CompositeExpression_t1397_1_0_0/* this_arg */
	, &CompositeExpression_t1397_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompositeExpression_t1397)/* instance_size */
	, sizeof (CompositeExpression_t1397)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Group
#include "System_System_Text_RegularExpressions_Syntax_Group.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Group
extern TypeInfo Group_t1398_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Group
#include "System_System_Text_RegularExpressions_Syntax_GroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::.ctor()
extern const MethodInfo Group__ctor_m6203_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Group__ctor_m6203/* method */
	, &Group_t1398_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1396_0_0_0;
static const ParameterInfo Group_t1398_Group_AppendExpression_m6204_ParameterInfos[] = 
{
	{"e", 0, 134218516, 0, &Expression_t1396_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::AppendExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Group_AppendExpression_m6204_MethodInfo = 
{
	"AppendExpression"/* name */
	, (methodPointerType)&Group_AppendExpression_m6204/* method */
	, &Group_t1398_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Group_t1398_Group_AppendExpression_m6204_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Group_t1398_Group_Compile_m6205_ParameterInfos[] = 
{
	{"cmp", 0, 134218517, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218518, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Group_Compile_m6205_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Group_Compile_m6205/* method */
	, &Group_t1398_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, Group_t1398_Group_Compile_m6205_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
static const ParameterInfo Group_t1398_Group_GetWidth_m6206_ParameterInfos[] = 
{
	{"min", 0, 134218519, 0, &Int32_t253_1_0_2},
	{"max", 1, 134218520, 0, &Int32_t253_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Group_GetWidth_m6206_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Group_GetWidth_m6206/* method */
	, &Group_t1398_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753/* invoker_method */
	, Group_t1398_Group_GetWidth_m6206_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Group_t1398_Group_GetAnchorInfo_m6207_ParameterInfos[] = 
{
	{"reverse", 0, 134218521, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Group::GetAnchorInfo(System.Boolean)
extern const MethodInfo Group_GetAnchorInfo_m6207_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Group_GetAnchorInfo_m6207/* method */
	, &Group_t1398_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1414_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t274/* invoker_method */
	, Group_t1398_Group_GetAnchorInfo_m6207_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Group_t1398_MethodInfos[] =
{
	&Group__ctor_m6203_MethodInfo,
	&Group_AppendExpression_m6204_MethodInfo,
	&Group_Compile_m6205_MethodInfo,
	&Group_GetWidth_m6206_MethodInfo,
	&Group_GetAnchorInfo_m6207_MethodInfo,
	NULL
};
extern const MethodInfo Group_Compile_m6205_MethodInfo;
extern const MethodInfo Group_GetWidth_m6206_MethodInfo;
extern const MethodInfo Group_GetAnchorInfo_m6207_MethodInfo;
static const Il2CppMethodReference Group_t1398_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Group_Compile_m6205_MethodInfo,
	&Group_GetWidth_m6206_MethodInfo,
	&Group_GetAnchorInfo_m6207_MethodInfo,
	&CompositeExpression_IsComplex_m6202_MethodInfo,
};
static bool Group_t1398_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Group_t1398_1_0_0;
struct Group_t1398;
const Il2CppTypeDefinitionMetadata Group_t1398_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1397_0_0_0/* parent */
	, Group_t1398_VTable/* vtableMethods */
	, Group_t1398_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Group_t1398_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Group"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Group_t1398_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Group_t1398_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Group_t1398_0_0_0/* byval_arg */
	, &Group_t1398_1_0_0/* this_arg */
	, &Group_t1398_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Group_t1398)/* instance_size */
	, sizeof (Group_t1398)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.RegularExpression
#include "System_System_Text_RegularExpressions_Syntax_RegularExpressi.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.RegularExpression
extern TypeInfo RegularExpression_t1399_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.RegularExpression
#include "System_System_Text_RegularExpressions_Syntax_RegularExpressiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::.ctor()
extern const MethodInfo RegularExpression__ctor_m6208_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RegularExpression__ctor_m6208/* method */
	, &RegularExpression_t1399_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo RegularExpression_t1399_RegularExpression_set_GroupCount_m6209_ParameterInfos[] = 
{
	{"value", 0, 134218522, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::set_GroupCount(System.Int32)
extern const MethodInfo RegularExpression_set_GroupCount_m6209_MethodInfo = 
{
	"set_GroupCount"/* name */
	, (methodPointerType)&RegularExpression_set_GroupCount_m6209/* method */
	, &RegularExpression_t1399_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, RegularExpression_t1399_RegularExpression_set_GroupCount_m6209_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo RegularExpression_t1399_RegularExpression_Compile_m6210_ParameterInfos[] = 
{
	{"cmp", 0, 134218523, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218524, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo RegularExpression_Compile_m6210_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&RegularExpression_Compile_m6210/* method */
	, &RegularExpression_t1399_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, RegularExpression_t1399_RegularExpression_Compile_m6210_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RegularExpression_t1399_MethodInfos[] =
{
	&RegularExpression__ctor_m6208_MethodInfo,
	&RegularExpression_set_GroupCount_m6209_MethodInfo,
	&RegularExpression_Compile_m6210_MethodInfo,
	NULL
};
extern const MethodInfo RegularExpression_set_GroupCount_m6209_MethodInfo;
static const PropertyInfo RegularExpression_t1399____GroupCount_PropertyInfo = 
{
	&RegularExpression_t1399_il2cpp_TypeInfo/* parent */
	, "GroupCount"/* name */
	, NULL/* get */
	, &RegularExpression_set_GroupCount_m6209_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RegularExpression_t1399_PropertyInfos[] =
{
	&RegularExpression_t1399____GroupCount_PropertyInfo,
	NULL
};
extern const MethodInfo RegularExpression_Compile_m6210_MethodInfo;
static const Il2CppMethodReference RegularExpression_t1399_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&RegularExpression_Compile_m6210_MethodInfo,
	&Group_GetWidth_m6206_MethodInfo,
	&Group_GetAnchorInfo_m6207_MethodInfo,
	&CompositeExpression_IsComplex_m6202_MethodInfo,
};
static bool RegularExpression_t1399_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RegularExpression_t1399_1_0_0;
struct RegularExpression_t1399;
const Il2CppTypeDefinitionMetadata RegularExpression_t1399_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t1398_0_0_0/* parent */
	, RegularExpression_t1399_VTable/* vtableMethods */
	, RegularExpression_t1399_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 711/* fieldStart */

};
TypeInfo RegularExpression_t1399_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RegularExpression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, RegularExpression_t1399_MethodInfos/* methods */
	, RegularExpression_t1399_PropertyInfos/* properties */
	, NULL/* events */
	, &RegularExpression_t1399_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RegularExpression_t1399_0_0_0/* byval_arg */
	, &RegularExpression_t1399_1_0_0/* this_arg */
	, &RegularExpression_t1399_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RegularExpression_t1399)/* instance_size */
	, sizeof (RegularExpression_t1399)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CapturingGroup
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroup.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CapturingGroup
extern TypeInfo CapturingGroup_t1400_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CapturingGroup
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::.ctor()
extern const MethodInfo CapturingGroup__ctor_m6211_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CapturingGroup__ctor_m6211/* method */
	, &CapturingGroup_t1400_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.CapturingGroup::get_Index()
extern const MethodInfo CapturingGroup_get_Index_m6212_MethodInfo = 
{
	"get_Index"/* name */
	, (methodPointerType)&CapturingGroup_get_Index_m6212/* method */
	, &CapturingGroup_t1400_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo CapturingGroup_t1400_CapturingGroup_set_Index_m6213_ParameterInfos[] = 
{
	{"value", 0, 134218525, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::set_Index(System.Int32)
extern const MethodInfo CapturingGroup_set_Index_m6213_MethodInfo = 
{
	"set_Index"/* name */
	, (methodPointerType)&CapturingGroup_set_Index_m6213/* method */
	, &CapturingGroup_t1400_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, CapturingGroup_t1400_CapturingGroup_set_Index_m6213_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.CapturingGroup::get_Name()
extern const MethodInfo CapturingGroup_get_Name_m6214_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&CapturingGroup_get_Name_m6214/* method */
	, &CapturingGroup_t1400_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CapturingGroup_t1400_CapturingGroup_set_Name_m6215_ParameterInfos[] = 
{
	{"value", 0, 134218526, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::set_Name(System.String)
extern const MethodInfo CapturingGroup_set_Name_m6215_MethodInfo = 
{
	"set_Name"/* name */
	, (methodPointerType)&CapturingGroup_set_Name_m6215/* method */
	, &CapturingGroup_t1400_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CapturingGroup_t1400_CapturingGroup_set_Name_m6215_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CapturingGroup::get_IsNamed()
extern const MethodInfo CapturingGroup_get_IsNamed_m6216_MethodInfo = 
{
	"get_IsNamed"/* name */
	, (methodPointerType)&CapturingGroup_get_IsNamed_m6216/* method */
	, &CapturingGroup_t1400_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo CapturingGroup_t1400_CapturingGroup_Compile_m6217_ParameterInfos[] = 
{
	{"cmp", 0, 134218527, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218528, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo CapturingGroup_Compile_m6217_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CapturingGroup_Compile_m6217/* method */
	, &CapturingGroup_t1400_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, CapturingGroup_t1400_CapturingGroup_Compile_m6217_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CapturingGroup::IsComplex()
extern const MethodInfo CapturingGroup_IsComplex_m6218_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CapturingGroup_IsComplex_m6218/* method */
	, &CapturingGroup_t1400_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CapturingGroup_t1400_CapturingGroup_CompareTo_m6219_ParameterInfos[] = 
{
	{"other", 0, 134218529, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.CapturingGroup::CompareTo(System.Object)
extern const MethodInfo CapturingGroup_CompareTo_m6219_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&CapturingGroup_CompareTo_m6219/* method */
	, &CapturingGroup_t1400_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, CapturingGroup_t1400_CapturingGroup_CompareTo_m6219_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CapturingGroup_t1400_MethodInfos[] =
{
	&CapturingGroup__ctor_m6211_MethodInfo,
	&CapturingGroup_get_Index_m6212_MethodInfo,
	&CapturingGroup_set_Index_m6213_MethodInfo,
	&CapturingGroup_get_Name_m6214_MethodInfo,
	&CapturingGroup_set_Name_m6215_MethodInfo,
	&CapturingGroup_get_IsNamed_m6216_MethodInfo,
	&CapturingGroup_Compile_m6217_MethodInfo,
	&CapturingGroup_IsComplex_m6218_MethodInfo,
	&CapturingGroup_CompareTo_m6219_MethodInfo,
	NULL
};
extern const MethodInfo CapturingGroup_get_Index_m6212_MethodInfo;
extern const MethodInfo CapturingGroup_set_Index_m6213_MethodInfo;
static const PropertyInfo CapturingGroup_t1400____Index_PropertyInfo = 
{
	&CapturingGroup_t1400_il2cpp_TypeInfo/* parent */
	, "Index"/* name */
	, &CapturingGroup_get_Index_m6212_MethodInfo/* get */
	, &CapturingGroup_set_Index_m6213_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CapturingGroup_get_Name_m6214_MethodInfo;
extern const MethodInfo CapturingGroup_set_Name_m6215_MethodInfo;
static const PropertyInfo CapturingGroup_t1400____Name_PropertyInfo = 
{
	&CapturingGroup_t1400_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &CapturingGroup_get_Name_m6214_MethodInfo/* get */
	, &CapturingGroup_set_Name_m6215_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CapturingGroup_get_IsNamed_m6216_MethodInfo;
static const PropertyInfo CapturingGroup_t1400____IsNamed_PropertyInfo = 
{
	&CapturingGroup_t1400_il2cpp_TypeInfo/* parent */
	, "IsNamed"/* name */
	, &CapturingGroup_get_IsNamed_m6216_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CapturingGroup_t1400_PropertyInfos[] =
{
	&CapturingGroup_t1400____Index_PropertyInfo,
	&CapturingGroup_t1400____Name_PropertyInfo,
	&CapturingGroup_t1400____IsNamed_PropertyInfo,
	NULL
};
extern const MethodInfo CapturingGroup_Compile_m6217_MethodInfo;
extern const MethodInfo CapturingGroup_IsComplex_m6218_MethodInfo;
extern const MethodInfo CapturingGroup_CompareTo_m6219_MethodInfo;
static const Il2CppMethodReference CapturingGroup_t1400_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&CapturingGroup_Compile_m6217_MethodInfo,
	&Group_GetWidth_m6206_MethodInfo,
	&Group_GetAnchorInfo_m6207_MethodInfo,
	&CapturingGroup_IsComplex_m6218_MethodInfo,
	&CapturingGroup_CompareTo_m6219_MethodInfo,
};
static bool CapturingGroup_t1400_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* CapturingGroup_t1400_InterfacesTypeInfos[] = 
{
	&IComparable_t277_0_0_0,
};
static Il2CppInterfaceOffsetPair CapturingGroup_t1400_InterfacesOffsets[] = 
{
	{ &IComparable_t277_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CapturingGroup_t1400_0_0_0;
extern const Il2CppType CapturingGroup_t1400_1_0_0;
struct CapturingGroup_t1400;
const Il2CppTypeDefinitionMetadata CapturingGroup_t1400_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CapturingGroup_t1400_InterfacesTypeInfos/* implementedInterfaces */
	, CapturingGroup_t1400_InterfacesOffsets/* interfaceOffsets */
	, &Group_t1398_0_0_0/* parent */
	, CapturingGroup_t1400_VTable/* vtableMethods */
	, CapturingGroup_t1400_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 712/* fieldStart */

};
TypeInfo CapturingGroup_t1400_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CapturingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CapturingGroup_t1400_MethodInfos/* methods */
	, CapturingGroup_t1400_PropertyInfos/* properties */
	, NULL/* events */
	, &CapturingGroup_t1400_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CapturingGroup_t1400_0_0_0/* byval_arg */
	, &CapturingGroup_t1400_1_0_0/* this_arg */
	, &CapturingGroup_t1400_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CapturingGroup_t1400)/* instance_size */
	, sizeof (CapturingGroup_t1400)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.BalancingGroup
#include "System_System_Text_RegularExpressions_Syntax_BalancingGroup.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.BalancingGroup
extern TypeInfo BalancingGroup_t1401_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.BalancingGroup
#include "System_System_Text_RegularExpressions_Syntax_BalancingGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::.ctor()
extern const MethodInfo BalancingGroup__ctor_m6220_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BalancingGroup__ctor_m6220/* method */
	, &BalancingGroup_t1401_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CapturingGroup_t1400_0_0_0;
static const ParameterInfo BalancingGroup_t1401_BalancingGroup_set_Balance_m6221_ParameterInfos[] = 
{
	{"value", 0, 134218530, 0, &CapturingGroup_t1400_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::set_Balance(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern const MethodInfo BalancingGroup_set_Balance_m6221_MethodInfo = 
{
	"set_Balance"/* name */
	, (methodPointerType)&BalancingGroup_set_Balance_m6221/* method */
	, &BalancingGroup_t1401_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, BalancingGroup_t1401_BalancingGroup_set_Balance_m6221_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo BalancingGroup_t1401_BalancingGroup_Compile_m6222_ParameterInfos[] = 
{
	{"cmp", 0, 134218531, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218532, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo BalancingGroup_Compile_m6222_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&BalancingGroup_Compile_m6222/* method */
	, &BalancingGroup_t1401_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, BalancingGroup_t1401_BalancingGroup_Compile_m6222_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BalancingGroup_t1401_MethodInfos[] =
{
	&BalancingGroup__ctor_m6220_MethodInfo,
	&BalancingGroup_set_Balance_m6221_MethodInfo,
	&BalancingGroup_Compile_m6222_MethodInfo,
	NULL
};
extern const MethodInfo BalancingGroup_set_Balance_m6221_MethodInfo;
static const PropertyInfo BalancingGroup_t1401____Balance_PropertyInfo = 
{
	&BalancingGroup_t1401_il2cpp_TypeInfo/* parent */
	, "Balance"/* name */
	, NULL/* get */
	, &BalancingGroup_set_Balance_m6221_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* BalancingGroup_t1401_PropertyInfos[] =
{
	&BalancingGroup_t1401____Balance_PropertyInfo,
	NULL
};
extern const MethodInfo BalancingGroup_Compile_m6222_MethodInfo;
static const Il2CppMethodReference BalancingGroup_t1401_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&BalancingGroup_Compile_m6222_MethodInfo,
	&Group_GetWidth_m6206_MethodInfo,
	&Group_GetAnchorInfo_m6207_MethodInfo,
	&CapturingGroup_IsComplex_m6218_MethodInfo,
	&CapturingGroup_CompareTo_m6219_MethodInfo,
};
static bool BalancingGroup_t1401_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BalancingGroup_t1401_InterfacesOffsets[] = 
{
	{ &IComparable_t277_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType BalancingGroup_t1401_0_0_0;
extern const Il2CppType BalancingGroup_t1401_1_0_0;
struct BalancingGroup_t1401;
const Il2CppTypeDefinitionMetadata BalancingGroup_t1401_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BalancingGroup_t1401_InterfacesOffsets/* interfaceOffsets */
	, &CapturingGroup_t1400_0_0_0/* parent */
	, BalancingGroup_t1401_VTable/* vtableMethods */
	, BalancingGroup_t1401_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 714/* fieldStart */

};
TypeInfo BalancingGroup_t1401_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BalancingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, BalancingGroup_t1401_MethodInfos/* methods */
	, BalancingGroup_t1401_PropertyInfos/* properties */
	, NULL/* events */
	, &BalancingGroup_t1401_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BalancingGroup_t1401_0_0_0/* byval_arg */
	, &BalancingGroup_t1401_1_0_0/* this_arg */
	, &BalancingGroup_t1401_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BalancingGroup_t1401)/* instance_size */
	, sizeof (BalancingGroup_t1401)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
#include "System_System_Text_RegularExpressions_Syntax_NonBacktracking.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
extern TypeInfo NonBacktrackingGroup_t1402_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
#include "System_System_Text_RegularExpressions_Syntax_NonBacktrackingMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::.ctor()
extern const MethodInfo NonBacktrackingGroup__ctor_m6223_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NonBacktrackingGroup__ctor_m6223/* method */
	, &NonBacktrackingGroup_t1402_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo NonBacktrackingGroup_t1402_NonBacktrackingGroup_Compile_m6224_ParameterInfos[] = 
{
	{"cmp", 0, 134218533, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218534, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo NonBacktrackingGroup_Compile_m6224_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&NonBacktrackingGroup_Compile_m6224/* method */
	, &NonBacktrackingGroup_t1402_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, NonBacktrackingGroup_t1402_NonBacktrackingGroup_Compile_m6224_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::IsComplex()
extern const MethodInfo NonBacktrackingGroup_IsComplex_m6225_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&NonBacktrackingGroup_IsComplex_m6225/* method */
	, &NonBacktrackingGroup_t1402_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NonBacktrackingGroup_t1402_MethodInfos[] =
{
	&NonBacktrackingGroup__ctor_m6223_MethodInfo,
	&NonBacktrackingGroup_Compile_m6224_MethodInfo,
	&NonBacktrackingGroup_IsComplex_m6225_MethodInfo,
	NULL
};
extern const MethodInfo NonBacktrackingGroup_Compile_m6224_MethodInfo;
extern const MethodInfo NonBacktrackingGroup_IsComplex_m6225_MethodInfo;
static const Il2CppMethodReference NonBacktrackingGroup_t1402_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&NonBacktrackingGroup_Compile_m6224_MethodInfo,
	&Group_GetWidth_m6206_MethodInfo,
	&Group_GetAnchorInfo_m6207_MethodInfo,
	&NonBacktrackingGroup_IsComplex_m6225_MethodInfo,
};
static bool NonBacktrackingGroup_t1402_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType NonBacktrackingGroup_t1402_0_0_0;
extern const Il2CppType NonBacktrackingGroup_t1402_1_0_0;
struct NonBacktrackingGroup_t1402;
const Il2CppTypeDefinitionMetadata NonBacktrackingGroup_t1402_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t1398_0_0_0/* parent */
	, NonBacktrackingGroup_t1402_VTable/* vtableMethods */
	, NonBacktrackingGroup_t1402_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo NonBacktrackingGroup_t1402_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "NonBacktrackingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NonBacktrackingGroup_t1402_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NonBacktrackingGroup_t1402_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NonBacktrackingGroup_t1402_0_0_0/* byval_arg */
	, &NonBacktrackingGroup_t1402_1_0_0/* this_arg */
	, &NonBacktrackingGroup_t1402_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NonBacktrackingGroup_t1402)/* instance_size */
	, sizeof (NonBacktrackingGroup_t1402)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Repetition
#include "System_System_Text_RegularExpressions_Syntax_Repetition.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Repetition
extern TypeInfo Repetition_t1403_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Repetition
#include "System_System_Text_RegularExpressions_Syntax_RepetitionMethodDeclarations.h"
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Repetition_t1403_Repetition__ctor_m6226_ParameterInfos[] = 
{
	{"min", 0, 134218535, 0, &Int32_t253_0_0_0},
	{"max", 1, 134218536, 0, &Int32_t253_0_0_0},
	{"lazy", 2, 134218537, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::.ctor(System.Int32,System.Int32,System.Boolean)
extern const MethodInfo Repetition__ctor_m6226_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Repetition__ctor_m6226/* method */
	, &Repetition_t1403_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274/* invoker_method */
	, Repetition_t1403_Repetition__ctor_m6226_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Repetition::get_Expression()
extern const MethodInfo Repetition_get_Expression_m6227_MethodInfo = 
{
	"get_Expression"/* name */
	, (methodPointerType)&Repetition_get_Expression_m6227/* method */
	, &Repetition_t1403_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1396_0_0_0;
static const ParameterInfo Repetition_t1403_Repetition_set_Expression_m6228_ParameterInfos[] = 
{
	{"value", 0, 134218538, 0, &Expression_t1396_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::set_Expression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Repetition_set_Expression_m6228_MethodInfo = 
{
	"set_Expression"/* name */
	, (methodPointerType)&Repetition_set_Expression_m6228/* method */
	, &Repetition_t1403_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Repetition_t1403_Repetition_set_Expression_m6228_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Repetition::get_Minimum()
extern const MethodInfo Repetition_get_Minimum_m6229_MethodInfo = 
{
	"get_Minimum"/* name */
	, (methodPointerType)&Repetition_get_Minimum_m6229/* method */
	, &Repetition_t1403_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Repetition_t1403_Repetition_Compile_m6230_ParameterInfos[] = 
{
	{"cmp", 0, 134218539, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218540, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Repetition_Compile_m6230_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Repetition_Compile_m6230/* method */
	, &Repetition_t1403_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, Repetition_t1403_Repetition_Compile_m6230_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
static const ParameterInfo Repetition_t1403_Repetition_GetWidth_m6231_ParameterInfos[] = 
{
	{"min", 0, 134218541, 0, &Int32_t253_1_0_2},
	{"max", 1, 134218542, 0, &Int32_t253_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Repetition_GetWidth_m6231_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Repetition_GetWidth_m6231/* method */
	, &Repetition_t1403_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753/* invoker_method */
	, Repetition_t1403_Repetition_GetWidth_m6231_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Repetition_t1403_Repetition_GetAnchorInfo_m6232_ParameterInfos[] = 
{
	{"reverse", 0, 134218543, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Repetition::GetAnchorInfo(System.Boolean)
extern const MethodInfo Repetition_GetAnchorInfo_m6232_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Repetition_GetAnchorInfo_m6232/* method */
	, &Repetition_t1403_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1414_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t274/* invoker_method */
	, Repetition_t1403_Repetition_GetAnchorInfo_m6232_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Repetition_t1403_MethodInfos[] =
{
	&Repetition__ctor_m6226_MethodInfo,
	&Repetition_get_Expression_m6227_MethodInfo,
	&Repetition_set_Expression_m6228_MethodInfo,
	&Repetition_get_Minimum_m6229_MethodInfo,
	&Repetition_Compile_m6230_MethodInfo,
	&Repetition_GetWidth_m6231_MethodInfo,
	&Repetition_GetAnchorInfo_m6232_MethodInfo,
	NULL
};
extern const MethodInfo Repetition_get_Expression_m6227_MethodInfo;
extern const MethodInfo Repetition_set_Expression_m6228_MethodInfo;
static const PropertyInfo Repetition_t1403____Expression_PropertyInfo = 
{
	&Repetition_t1403_il2cpp_TypeInfo/* parent */
	, "Expression"/* name */
	, &Repetition_get_Expression_m6227_MethodInfo/* get */
	, &Repetition_set_Expression_m6228_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Repetition_get_Minimum_m6229_MethodInfo;
static const PropertyInfo Repetition_t1403____Minimum_PropertyInfo = 
{
	&Repetition_t1403_il2cpp_TypeInfo/* parent */
	, "Minimum"/* name */
	, &Repetition_get_Minimum_m6229_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Repetition_t1403_PropertyInfos[] =
{
	&Repetition_t1403____Expression_PropertyInfo,
	&Repetition_t1403____Minimum_PropertyInfo,
	NULL
};
extern const MethodInfo Repetition_Compile_m6230_MethodInfo;
extern const MethodInfo Repetition_GetWidth_m6231_MethodInfo;
extern const MethodInfo Repetition_GetAnchorInfo_m6232_MethodInfo;
static const Il2CppMethodReference Repetition_t1403_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Repetition_Compile_m6230_MethodInfo,
	&Repetition_GetWidth_m6231_MethodInfo,
	&Repetition_GetAnchorInfo_m6232_MethodInfo,
	&CompositeExpression_IsComplex_m6202_MethodInfo,
};
static bool Repetition_t1403_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Repetition_t1403_0_0_0;
extern const Il2CppType Repetition_t1403_1_0_0;
struct Repetition_t1403;
const Il2CppTypeDefinitionMetadata Repetition_t1403_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1397_0_0_0/* parent */
	, Repetition_t1403_VTable/* vtableMethods */
	, Repetition_t1403_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 715/* fieldStart */

};
TypeInfo Repetition_t1403_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Repetition"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Repetition_t1403_MethodInfos/* methods */
	, Repetition_t1403_PropertyInfos/* properties */
	, NULL/* events */
	, &Repetition_t1403_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Repetition_t1403_0_0_0/* byval_arg */
	, &Repetition_t1403_1_0_0/* this_arg */
	, &Repetition_t1403_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Repetition_t1403)/* instance_size */
	, sizeof (Repetition_t1403)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Assertion
#include "System_System_Text_RegularExpressions_Syntax_Assertion.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Assertion
extern TypeInfo Assertion_t1404_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Assertion
#include "System_System_Text_RegularExpressions_Syntax_AssertionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::.ctor()
extern const MethodInfo Assertion__ctor_m6233_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Assertion__ctor_m6233/* method */
	, &Assertion_t1404_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_TrueExpression()
extern const MethodInfo Assertion_get_TrueExpression_m6234_MethodInfo = 
{
	"get_TrueExpression"/* name */
	, (methodPointerType)&Assertion_get_TrueExpression_m6234/* method */
	, &Assertion_t1404_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1396_0_0_0;
static const ParameterInfo Assertion_t1404_Assertion_set_TrueExpression_m6235_ParameterInfos[] = 
{
	{"value", 0, 134218544, 0, &Expression_t1396_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_TrueExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Assertion_set_TrueExpression_m6235_MethodInfo = 
{
	"set_TrueExpression"/* name */
	, (methodPointerType)&Assertion_set_TrueExpression_m6235/* method */
	, &Assertion_t1404_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Assertion_t1404_Assertion_set_TrueExpression_m6235_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_FalseExpression()
extern const MethodInfo Assertion_get_FalseExpression_m6236_MethodInfo = 
{
	"get_FalseExpression"/* name */
	, (methodPointerType)&Assertion_get_FalseExpression_m6236/* method */
	, &Assertion_t1404_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1396_0_0_0;
static const ParameterInfo Assertion_t1404_Assertion_set_FalseExpression_m6237_ParameterInfos[] = 
{
	{"value", 0, 134218545, 0, &Expression_t1396_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_FalseExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Assertion_set_FalseExpression_m6237_MethodInfo = 
{
	"set_FalseExpression"/* name */
	, (methodPointerType)&Assertion_set_FalseExpression_m6237/* method */
	, &Assertion_t1404_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Assertion_t1404_Assertion_set_FalseExpression_m6237_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
static const ParameterInfo Assertion_t1404_Assertion_GetWidth_m6238_ParameterInfos[] = 
{
	{"min", 0, 134218546, 0, &Int32_t253_1_0_2},
	{"max", 1, 134218547, 0, &Int32_t253_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Assertion_GetWidth_m6238_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Assertion_GetWidth_m6238/* method */
	, &Assertion_t1404_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753/* invoker_method */
	, Assertion_t1404_Assertion_GetWidth_m6238_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Assertion_t1404_MethodInfos[] =
{
	&Assertion__ctor_m6233_MethodInfo,
	&Assertion_get_TrueExpression_m6234_MethodInfo,
	&Assertion_set_TrueExpression_m6235_MethodInfo,
	&Assertion_get_FalseExpression_m6236_MethodInfo,
	&Assertion_set_FalseExpression_m6237_MethodInfo,
	&Assertion_GetWidth_m6238_MethodInfo,
	NULL
};
extern const MethodInfo Assertion_get_TrueExpression_m6234_MethodInfo;
extern const MethodInfo Assertion_set_TrueExpression_m6235_MethodInfo;
static const PropertyInfo Assertion_t1404____TrueExpression_PropertyInfo = 
{
	&Assertion_t1404_il2cpp_TypeInfo/* parent */
	, "TrueExpression"/* name */
	, &Assertion_get_TrueExpression_m6234_MethodInfo/* get */
	, &Assertion_set_TrueExpression_m6235_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Assertion_get_FalseExpression_m6236_MethodInfo;
extern const MethodInfo Assertion_set_FalseExpression_m6237_MethodInfo;
static const PropertyInfo Assertion_t1404____FalseExpression_PropertyInfo = 
{
	&Assertion_t1404_il2cpp_TypeInfo/* parent */
	, "FalseExpression"/* name */
	, &Assertion_get_FalseExpression_m6236_MethodInfo/* get */
	, &Assertion_set_FalseExpression_m6237_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Assertion_t1404_PropertyInfos[] =
{
	&Assertion_t1404____TrueExpression_PropertyInfo,
	&Assertion_t1404____FalseExpression_PropertyInfo,
	NULL
};
extern const MethodInfo Assertion_GetWidth_m6238_MethodInfo;
static const Il2CppMethodReference Assertion_t1404_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
	&Assertion_GetWidth_m6238_MethodInfo,
	&Expression_GetAnchorInfo_m6198_MethodInfo,
	&CompositeExpression_IsComplex_m6202_MethodInfo,
};
static bool Assertion_t1404_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Assertion_t1404_1_0_0;
struct Assertion_t1404;
const Il2CppTypeDefinitionMetadata Assertion_t1404_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1397_0_0_0/* parent */
	, Assertion_t1404_VTable/* vtableMethods */
	, Assertion_t1404_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Assertion_t1404_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Assertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Assertion_t1404_MethodInfos/* methods */
	, Assertion_t1404_PropertyInfos/* properties */
	, NULL/* events */
	, &Assertion_t1404_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Assertion_t1404_0_0_0/* byval_arg */
	, &Assertion_t1404_1_0_0/* this_arg */
	, &Assertion_t1404_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Assertion_t1404)/* instance_size */
	, sizeof (Assertion_t1404)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CaptureAssertion
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertio.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CaptureAssertion
extern TypeInfo CaptureAssertion_t1407_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CaptureAssertion
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertioMethodDeclarations.h"
extern const Il2CppType Literal_t1406_0_0_0;
extern const Il2CppType Literal_t1406_0_0_0;
static const ParameterInfo CaptureAssertion_t1407_CaptureAssertion__ctor_m6239_ParameterInfos[] = 
{
	{"l", 0, 134218548, 0, &Literal_t1406_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::.ctor(System.Text.RegularExpressions.Syntax.Literal)
extern const MethodInfo CaptureAssertion__ctor_m6239_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CaptureAssertion__ctor_m6239/* method */
	, &CaptureAssertion_t1407_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CaptureAssertion_t1407_CaptureAssertion__ctor_m6239_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CapturingGroup_t1400_0_0_0;
static const ParameterInfo CaptureAssertion_t1407_CaptureAssertion_set_CapturingGroup_m6240_ParameterInfos[] = 
{
	{"value", 0, 134218549, 0, &CapturingGroup_t1400_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::set_CapturingGroup(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern const MethodInfo CaptureAssertion_set_CapturingGroup_m6240_MethodInfo = 
{
	"set_CapturingGroup"/* name */
	, (methodPointerType)&CaptureAssertion_set_CapturingGroup_m6240/* method */
	, &CaptureAssertion_t1407_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CaptureAssertion_t1407_CaptureAssertion_set_CapturingGroup_m6240_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo CaptureAssertion_t1407_CaptureAssertion_Compile_m6241_ParameterInfos[] = 
{
	{"cmp", 0, 134218550, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218551, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo CaptureAssertion_Compile_m6241_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CaptureAssertion_Compile_m6241/* method */
	, &CaptureAssertion_t1407_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, CaptureAssertion_t1407_CaptureAssertion_Compile_m6241_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CaptureAssertion::IsComplex()
extern const MethodInfo CaptureAssertion_IsComplex_m6242_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CaptureAssertion_IsComplex_m6242/* method */
	, &CaptureAssertion_t1407_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionAssertion System.Text.RegularExpressions.Syntax.CaptureAssertion::get_Alternate()
extern const MethodInfo CaptureAssertion_get_Alternate_m6243_MethodInfo = 
{
	"get_Alternate"/* name */
	, (methodPointerType)&CaptureAssertion_get_Alternate_m6243/* method */
	, &CaptureAssertion_t1407_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionAssertion_t1405_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CaptureAssertion_t1407_MethodInfos[] =
{
	&CaptureAssertion__ctor_m6239_MethodInfo,
	&CaptureAssertion_set_CapturingGroup_m6240_MethodInfo,
	&CaptureAssertion_Compile_m6241_MethodInfo,
	&CaptureAssertion_IsComplex_m6242_MethodInfo,
	&CaptureAssertion_get_Alternate_m6243_MethodInfo,
	NULL
};
extern const MethodInfo CaptureAssertion_set_CapturingGroup_m6240_MethodInfo;
static const PropertyInfo CaptureAssertion_t1407____CapturingGroup_PropertyInfo = 
{
	&CaptureAssertion_t1407_il2cpp_TypeInfo/* parent */
	, "CapturingGroup"/* name */
	, NULL/* get */
	, &CaptureAssertion_set_CapturingGroup_m6240_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CaptureAssertion_get_Alternate_m6243_MethodInfo;
static const PropertyInfo CaptureAssertion_t1407____Alternate_PropertyInfo = 
{
	&CaptureAssertion_t1407_il2cpp_TypeInfo/* parent */
	, "Alternate"/* name */
	, &CaptureAssertion_get_Alternate_m6243_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CaptureAssertion_t1407_PropertyInfos[] =
{
	&CaptureAssertion_t1407____CapturingGroup_PropertyInfo,
	&CaptureAssertion_t1407____Alternate_PropertyInfo,
	NULL
};
extern const MethodInfo CaptureAssertion_Compile_m6241_MethodInfo;
extern const MethodInfo CaptureAssertion_IsComplex_m6242_MethodInfo;
static const Il2CppMethodReference CaptureAssertion_t1407_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&CaptureAssertion_Compile_m6241_MethodInfo,
	&Assertion_GetWidth_m6238_MethodInfo,
	&Expression_GetAnchorInfo_m6198_MethodInfo,
	&CaptureAssertion_IsComplex_m6242_MethodInfo,
};
static bool CaptureAssertion_t1407_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CaptureAssertion_t1407_0_0_0;
extern const Il2CppType CaptureAssertion_t1407_1_0_0;
struct CaptureAssertion_t1407;
const Il2CppTypeDefinitionMetadata CaptureAssertion_t1407_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Assertion_t1404_0_0_0/* parent */
	, CaptureAssertion_t1407_VTable/* vtableMethods */
	, CaptureAssertion_t1407_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 718/* fieldStart */

};
TypeInfo CaptureAssertion_t1407_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CaptureAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CaptureAssertion_t1407_MethodInfos/* methods */
	, CaptureAssertion_t1407_PropertyInfos/* properties */
	, NULL/* events */
	, &CaptureAssertion_t1407_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CaptureAssertion_t1407_0_0_0/* byval_arg */
	, &CaptureAssertion_t1407_1_0_0/* this_arg */
	, &CaptureAssertion_t1407_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CaptureAssertion_t1407)/* instance_size */
	, sizeof (CaptureAssertion_t1407)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsser.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionAssertion
extern TypeInfo ExpressionAssertion_t1405_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::.ctor()
extern const MethodInfo ExpressionAssertion__ctor_m6244_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExpressionAssertion__ctor_m6244/* method */
	, &ExpressionAssertion_t1405_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ExpressionAssertion_t1405_ExpressionAssertion_set_Reverse_m6245_ParameterInfos[] = 
{
	{"value", 0, 134218552, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Reverse(System.Boolean)
extern const MethodInfo ExpressionAssertion_set_Reverse_m6245_MethodInfo = 
{
	"set_Reverse"/* name */
	, (methodPointerType)&ExpressionAssertion_set_Reverse_m6245/* method */
	, &ExpressionAssertion_t1405_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, ExpressionAssertion_t1405_ExpressionAssertion_set_Reverse_m6245_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ExpressionAssertion_t1405_ExpressionAssertion_set_Negate_m6246_ParameterInfos[] = 
{
	{"value", 0, 134218553, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Negate(System.Boolean)
extern const MethodInfo ExpressionAssertion_set_Negate_m6246_MethodInfo = 
{
	"set_Negate"/* name */
	, (methodPointerType)&ExpressionAssertion_set_Negate_m6246/* method */
	, &ExpressionAssertion_t1405_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, ExpressionAssertion_t1405_ExpressionAssertion_set_Negate_m6246_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionAssertion::get_TestExpression()
extern const MethodInfo ExpressionAssertion_get_TestExpression_m6247_MethodInfo = 
{
	"get_TestExpression"/* name */
	, (methodPointerType)&ExpressionAssertion_get_TestExpression_m6247/* method */
	, &ExpressionAssertion_t1405_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1396_0_0_0;
static const ParameterInfo ExpressionAssertion_t1405_ExpressionAssertion_set_TestExpression_m6248_ParameterInfos[] = 
{
	{"value", 0, 134218554, 0, &Expression_t1396_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_TestExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo ExpressionAssertion_set_TestExpression_m6248_MethodInfo = 
{
	"set_TestExpression"/* name */
	, (methodPointerType)&ExpressionAssertion_set_TestExpression_m6248/* method */
	, &ExpressionAssertion_t1405_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ExpressionAssertion_t1405_ExpressionAssertion_set_TestExpression_m6248_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ExpressionAssertion_t1405_ExpressionAssertion_Compile_m6249_ParameterInfos[] = 
{
	{"cmp", 0, 134218555, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218556, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo ExpressionAssertion_Compile_m6249_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&ExpressionAssertion_Compile_m6249/* method */
	, &ExpressionAssertion_t1405_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, ExpressionAssertion_t1405_ExpressionAssertion_Compile_m6249_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.ExpressionAssertion::IsComplex()
extern const MethodInfo ExpressionAssertion_IsComplex_m6250_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&ExpressionAssertion_IsComplex_m6250/* method */
	, &ExpressionAssertion_t1405_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExpressionAssertion_t1405_MethodInfos[] =
{
	&ExpressionAssertion__ctor_m6244_MethodInfo,
	&ExpressionAssertion_set_Reverse_m6245_MethodInfo,
	&ExpressionAssertion_set_Negate_m6246_MethodInfo,
	&ExpressionAssertion_get_TestExpression_m6247_MethodInfo,
	&ExpressionAssertion_set_TestExpression_m6248_MethodInfo,
	&ExpressionAssertion_Compile_m6249_MethodInfo,
	&ExpressionAssertion_IsComplex_m6250_MethodInfo,
	NULL
};
extern const MethodInfo ExpressionAssertion_set_Reverse_m6245_MethodInfo;
static const PropertyInfo ExpressionAssertion_t1405____Reverse_PropertyInfo = 
{
	&ExpressionAssertion_t1405_il2cpp_TypeInfo/* parent */
	, "Reverse"/* name */
	, NULL/* get */
	, &ExpressionAssertion_set_Reverse_m6245_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ExpressionAssertion_set_Negate_m6246_MethodInfo;
static const PropertyInfo ExpressionAssertion_t1405____Negate_PropertyInfo = 
{
	&ExpressionAssertion_t1405_il2cpp_TypeInfo/* parent */
	, "Negate"/* name */
	, NULL/* get */
	, &ExpressionAssertion_set_Negate_m6246_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ExpressionAssertion_get_TestExpression_m6247_MethodInfo;
extern const MethodInfo ExpressionAssertion_set_TestExpression_m6248_MethodInfo;
static const PropertyInfo ExpressionAssertion_t1405____TestExpression_PropertyInfo = 
{
	&ExpressionAssertion_t1405_il2cpp_TypeInfo/* parent */
	, "TestExpression"/* name */
	, &ExpressionAssertion_get_TestExpression_m6247_MethodInfo/* get */
	, &ExpressionAssertion_set_TestExpression_m6248_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ExpressionAssertion_t1405_PropertyInfos[] =
{
	&ExpressionAssertion_t1405____Reverse_PropertyInfo,
	&ExpressionAssertion_t1405____Negate_PropertyInfo,
	&ExpressionAssertion_t1405____TestExpression_PropertyInfo,
	NULL
};
extern const MethodInfo ExpressionAssertion_Compile_m6249_MethodInfo;
extern const MethodInfo ExpressionAssertion_IsComplex_m6250_MethodInfo;
static const Il2CppMethodReference ExpressionAssertion_t1405_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ExpressionAssertion_Compile_m6249_MethodInfo,
	&Assertion_GetWidth_m6238_MethodInfo,
	&Expression_GetAnchorInfo_m6198_MethodInfo,
	&ExpressionAssertion_IsComplex_m6250_MethodInfo,
};
static bool ExpressionAssertion_t1405_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ExpressionAssertion_t1405_1_0_0;
struct ExpressionAssertion_t1405;
const Il2CppTypeDefinitionMetadata ExpressionAssertion_t1405_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Assertion_t1404_0_0_0/* parent */
	, ExpressionAssertion_t1405_VTable/* vtableMethods */
	, ExpressionAssertion_t1405_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 721/* fieldStart */

};
TypeInfo ExpressionAssertion_t1405_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExpressionAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, ExpressionAssertion_t1405_MethodInfos/* methods */
	, ExpressionAssertion_t1405_PropertyInfos/* properties */
	, NULL/* events */
	, &ExpressionAssertion_t1405_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExpressionAssertion_t1405_0_0_0/* byval_arg */
	, &ExpressionAssertion_t1405_1_0_0/* this_arg */
	, &ExpressionAssertion_t1405_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExpressionAssertion_t1405)/* instance_size */
	, sizeof (ExpressionAssertion_t1405)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Alternation
#include "System_System_Text_RegularExpressions_Syntax_Alternation.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Alternation
extern TypeInfo Alternation_t1408_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Alternation
#include "System_System_Text_RegularExpressions_Syntax_AlternationMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::.ctor()
extern const MethodInfo Alternation__ctor_m6251_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Alternation__ctor_m6251/* method */
	, &Alternation_t1408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.Alternation::get_Alternatives()
extern const MethodInfo Alternation_get_Alternatives_m6252_MethodInfo = 
{
	"get_Alternatives"/* name */
	, (methodPointerType)&Alternation_get_Alternatives_m6252/* method */
	, &Alternation_t1408_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionCollection_t1395_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1396_0_0_0;
static const ParameterInfo Alternation_t1408_Alternation_AddAlternative_m6253_ParameterInfos[] = 
{
	{"e", 0, 134218557, 0, &Expression_t1396_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::AddAlternative(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Alternation_AddAlternative_m6253_MethodInfo = 
{
	"AddAlternative"/* name */
	, (methodPointerType)&Alternation_AddAlternative_m6253/* method */
	, &Alternation_t1408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Alternation_t1408_Alternation_AddAlternative_m6253_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Alternation_t1408_Alternation_Compile_m6254_ParameterInfos[] = 
{
	{"cmp", 0, 134218558, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218559, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Alternation_Compile_m6254_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Alternation_Compile_m6254/* method */
	, &Alternation_t1408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, Alternation_t1408_Alternation_Compile_m6254_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
static const ParameterInfo Alternation_t1408_Alternation_GetWidth_m6255_ParameterInfos[] = 
{
	{"min", 0, 134218560, 0, &Int32_t253_1_0_2},
	{"max", 1, 134218561, 0, &Int32_t253_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Alternation_GetWidth_m6255_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Alternation_GetWidth_m6255/* method */
	, &Alternation_t1408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753/* invoker_method */
	, Alternation_t1408_Alternation_GetWidth_m6255_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Alternation_t1408_MethodInfos[] =
{
	&Alternation__ctor_m6251_MethodInfo,
	&Alternation_get_Alternatives_m6252_MethodInfo,
	&Alternation_AddAlternative_m6253_MethodInfo,
	&Alternation_Compile_m6254_MethodInfo,
	&Alternation_GetWidth_m6255_MethodInfo,
	NULL
};
extern const MethodInfo Alternation_get_Alternatives_m6252_MethodInfo;
static const PropertyInfo Alternation_t1408____Alternatives_PropertyInfo = 
{
	&Alternation_t1408_il2cpp_TypeInfo/* parent */
	, "Alternatives"/* name */
	, &Alternation_get_Alternatives_m6252_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Alternation_t1408_PropertyInfos[] =
{
	&Alternation_t1408____Alternatives_PropertyInfo,
	NULL
};
extern const MethodInfo Alternation_Compile_m6254_MethodInfo;
extern const MethodInfo Alternation_GetWidth_m6255_MethodInfo;
static const Il2CppMethodReference Alternation_t1408_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Alternation_Compile_m6254_MethodInfo,
	&Alternation_GetWidth_m6255_MethodInfo,
	&Expression_GetAnchorInfo_m6198_MethodInfo,
	&CompositeExpression_IsComplex_m6202_MethodInfo,
};
static bool Alternation_t1408_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Alternation_t1408_0_0_0;
extern const Il2CppType Alternation_t1408_1_0_0;
struct Alternation_t1408;
const Il2CppTypeDefinitionMetadata Alternation_t1408_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1397_0_0_0/* parent */
	, Alternation_t1408_VTable/* vtableMethods */
	, Alternation_t1408_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Alternation_t1408_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Alternation"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Alternation_t1408_MethodInfos/* methods */
	, Alternation_t1408_PropertyInfos/* properties */
	, NULL/* events */
	, &Alternation_t1408_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Alternation_t1408_0_0_0/* byval_arg */
	, &Alternation_t1408_1_0_0/* this_arg */
	, &Alternation_t1408_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Alternation_t1408)/* instance_size */
	, sizeof (Alternation_t1408)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Literal
#include "System_System_Text_RegularExpressions_Syntax_Literal.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Literal
extern TypeInfo Literal_t1406_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Literal
#include "System_System_Text_RegularExpressions_Syntax_LiteralMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Literal_t1406_Literal__ctor_m6256_ParameterInfos[] = 
{
	{"str", 0, 134218562, 0, &String_t_0_0_0},
	{"ignore", 1, 134218563, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::.ctor(System.String,System.Boolean)
extern const MethodInfo Literal__ctor_m6256_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Literal__ctor_m6256/* method */
	, &Literal_t1406_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, Literal_t1406_Literal__ctor_m6256_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Literal_t1406_Literal_CompileLiteral_m6257_ParameterInfos[] = 
{
	{"str", 0, 134218564, 0, &String_t_0_0_0},
	{"cmp", 1, 134218565, 0, &ICompiler_t1439_0_0_0},
	{"ignore", 2, 134218566, 0, &Boolean_t273_0_0_0},
	{"reverse", 3, 134218567, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::CompileLiteral(System.String,System.Text.RegularExpressions.ICompiler,System.Boolean,System.Boolean)
extern const MethodInfo Literal_CompileLiteral_m6257_MethodInfo = 
{
	"CompileLiteral"/* name */
	, (methodPointerType)&Literal_CompileLiteral_m6257/* method */
	, &Literal_t1406_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274_SByte_t274/* invoker_method */
	, Literal_t1406_Literal_CompileLiteral_m6257_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Literal_t1406_Literal_Compile_m6258_ParameterInfos[] = 
{
	{"cmp", 0, 134218568, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218569, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Literal_Compile_m6258_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Literal_Compile_m6258/* method */
	, &Literal_t1406_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, Literal_t1406_Literal_Compile_m6258_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
static const ParameterInfo Literal_t1406_Literal_GetWidth_m6259_ParameterInfos[] = 
{
	{"min", 0, 134218570, 0, &Int32_t253_1_0_2},
	{"max", 1, 134218571, 0, &Int32_t253_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Literal_GetWidth_m6259_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Literal_GetWidth_m6259/* method */
	, &Literal_t1406_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753/* invoker_method */
	, Literal_t1406_Literal_GetWidth_m6259_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Literal_t1406_Literal_GetAnchorInfo_m6260_ParameterInfos[] = 
{
	{"reverse", 0, 134218572, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Literal::GetAnchorInfo(System.Boolean)
extern const MethodInfo Literal_GetAnchorInfo_m6260_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Literal_GetAnchorInfo_m6260/* method */
	, &Literal_t1406_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1414_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t274/* invoker_method */
	, Literal_t1406_Literal_GetAnchorInfo_m6260_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Literal::IsComplex()
extern const MethodInfo Literal_IsComplex_m6261_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&Literal_IsComplex_m6261/* method */
	, &Literal_t1406_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Literal_t1406_MethodInfos[] =
{
	&Literal__ctor_m6256_MethodInfo,
	&Literal_CompileLiteral_m6257_MethodInfo,
	&Literal_Compile_m6258_MethodInfo,
	&Literal_GetWidth_m6259_MethodInfo,
	&Literal_GetAnchorInfo_m6260_MethodInfo,
	&Literal_IsComplex_m6261_MethodInfo,
	NULL
};
extern const MethodInfo Literal_Compile_m6258_MethodInfo;
extern const MethodInfo Literal_GetWidth_m6259_MethodInfo;
extern const MethodInfo Literal_GetAnchorInfo_m6260_MethodInfo;
extern const MethodInfo Literal_IsComplex_m6261_MethodInfo;
static const Il2CppMethodReference Literal_t1406_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Literal_Compile_m6258_MethodInfo,
	&Literal_GetWidth_m6259_MethodInfo,
	&Literal_GetAnchorInfo_m6260_MethodInfo,
	&Literal_IsComplex_m6261_MethodInfo,
};
static bool Literal_t1406_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Literal_t1406_1_0_0;
struct Literal_t1406;
const Il2CppTypeDefinitionMetadata Literal_t1406_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1396_0_0_0/* parent */
	, Literal_t1406_VTable/* vtableMethods */
	, Literal_t1406_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 723/* fieldStart */

};
TypeInfo Literal_t1406_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Literal"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Literal_t1406_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Literal_t1406_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Literal_t1406_0_0_0/* byval_arg */
	, &Literal_t1406_1_0_0/* this_arg */
	, &Literal_t1406_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Literal_t1406)/* instance_size */
	, sizeof (Literal_t1406)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.PositionAssertion
#include "System_System_Text_RegularExpressions_Syntax_PositionAsserti.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.PositionAssertion
extern TypeInfo PositionAssertion_t1409_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.PositionAssertion
#include "System_System_Text_RegularExpressions_Syntax_PositionAssertiMethodDeclarations.h"
extern const Il2CppType Position_t1370_0_0_0;
static const ParameterInfo PositionAssertion_t1409_PositionAssertion__ctor_m6262_ParameterInfos[] = 
{
	{"pos", 0, 134218573, 0, &Position_t1370_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::.ctor(System.Text.RegularExpressions.Position)
extern const MethodInfo PositionAssertion__ctor_m6262_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PositionAssertion__ctor_m6262/* method */
	, &PositionAssertion_t1409_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684/* invoker_method */
	, PositionAssertion_t1409_PositionAssertion__ctor_m6262_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PositionAssertion_t1409_PositionAssertion_Compile_m6263_ParameterInfos[] = 
{
	{"cmp", 0, 134218574, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218575, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo PositionAssertion_Compile_m6263_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&PositionAssertion_Compile_m6263/* method */
	, &PositionAssertion_t1409_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, PositionAssertion_t1409_PositionAssertion_Compile_m6263_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
static const ParameterInfo PositionAssertion_t1409_PositionAssertion_GetWidth_m6264_ParameterInfos[] = 
{
	{"min", 0, 134218576, 0, &Int32_t253_1_0_2},
	{"max", 1, 134218577, 0, &Int32_t253_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo PositionAssertion_GetWidth_m6264_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&PositionAssertion_GetWidth_m6264/* method */
	, &PositionAssertion_t1409_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753/* invoker_method */
	, PositionAssertion_t1409_PositionAssertion_GetWidth_m6264_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.PositionAssertion::IsComplex()
extern const MethodInfo PositionAssertion_IsComplex_m6265_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&PositionAssertion_IsComplex_m6265/* method */
	, &PositionAssertion_t1409_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo PositionAssertion_t1409_PositionAssertion_GetAnchorInfo_m6266_ParameterInfos[] = 
{
	{"revers", 0, 134218578, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.PositionAssertion::GetAnchorInfo(System.Boolean)
extern const MethodInfo PositionAssertion_GetAnchorInfo_m6266_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&PositionAssertion_GetAnchorInfo_m6266/* method */
	, &PositionAssertion_t1409_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1414_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t274/* invoker_method */
	, PositionAssertion_t1409_PositionAssertion_GetAnchorInfo_m6266_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PositionAssertion_t1409_MethodInfos[] =
{
	&PositionAssertion__ctor_m6262_MethodInfo,
	&PositionAssertion_Compile_m6263_MethodInfo,
	&PositionAssertion_GetWidth_m6264_MethodInfo,
	&PositionAssertion_IsComplex_m6265_MethodInfo,
	&PositionAssertion_GetAnchorInfo_m6266_MethodInfo,
	NULL
};
extern const MethodInfo PositionAssertion_Compile_m6263_MethodInfo;
extern const MethodInfo PositionAssertion_GetWidth_m6264_MethodInfo;
extern const MethodInfo PositionAssertion_GetAnchorInfo_m6266_MethodInfo;
extern const MethodInfo PositionAssertion_IsComplex_m6265_MethodInfo;
static const Il2CppMethodReference PositionAssertion_t1409_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&PositionAssertion_Compile_m6263_MethodInfo,
	&PositionAssertion_GetWidth_m6264_MethodInfo,
	&PositionAssertion_GetAnchorInfo_m6266_MethodInfo,
	&PositionAssertion_IsComplex_m6265_MethodInfo,
};
static bool PositionAssertion_t1409_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PositionAssertion_t1409_0_0_0;
extern const Il2CppType PositionAssertion_t1409_1_0_0;
struct PositionAssertion_t1409;
const Il2CppTypeDefinitionMetadata PositionAssertion_t1409_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1396_0_0_0/* parent */
	, PositionAssertion_t1409_VTable/* vtableMethods */
	, PositionAssertion_t1409_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 725/* fieldStart */

};
TypeInfo PositionAssertion_t1409_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PositionAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, PositionAssertion_t1409_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PositionAssertion_t1409_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PositionAssertion_t1409_0_0_0/* byval_arg */
	, &PositionAssertion_t1409_1_0_0/* this_arg */
	, &PositionAssertion_t1409_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PositionAssertion_t1409)/* instance_size */
	, sizeof (PositionAssertion_t1409)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Reference
#include "System_System_Text_RegularExpressions_Syntax_Reference.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Reference
extern TypeInfo Reference_t1410_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Reference
#include "System_System_Text_RegularExpressions_Syntax_ReferenceMethodDeclarations.h"
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Reference_t1410_Reference__ctor_m6267_ParameterInfos[] = 
{
	{"ignore", 0, 134218579, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::.ctor(System.Boolean)
extern const MethodInfo Reference__ctor_m6267_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Reference__ctor_m6267/* method */
	, &Reference_t1410_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Reference_t1410_Reference__ctor_m6267_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.CapturingGroup System.Text.RegularExpressions.Syntax.Reference::get_CapturingGroup()
extern const MethodInfo Reference_get_CapturingGroup_m6268_MethodInfo = 
{
	"get_CapturingGroup"/* name */
	, (methodPointerType)&Reference_get_CapturingGroup_m6268/* method */
	, &Reference_t1410_il2cpp_TypeInfo/* declaring_type */
	, &CapturingGroup_t1400_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CapturingGroup_t1400_0_0_0;
static const ParameterInfo Reference_t1410_Reference_set_CapturingGroup_m6269_ParameterInfos[] = 
{
	{"value", 0, 134218580, 0, &CapturingGroup_t1400_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::set_CapturingGroup(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern const MethodInfo Reference_set_CapturingGroup_m6269_MethodInfo = 
{
	"set_CapturingGroup"/* name */
	, (methodPointerType)&Reference_set_CapturingGroup_m6269/* method */
	, &Reference_t1410_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Reference_t1410_Reference_set_CapturingGroup_m6269_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Reference::get_IgnoreCase()
extern const MethodInfo Reference_get_IgnoreCase_m6270_MethodInfo = 
{
	"get_IgnoreCase"/* name */
	, (methodPointerType)&Reference_get_IgnoreCase_m6270/* method */
	, &Reference_t1410_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Reference_t1410_Reference_Compile_m6271_ParameterInfos[] = 
{
	{"cmp", 0, 134218581, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218582, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Reference_Compile_m6271_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Reference_Compile_m6271/* method */
	, &Reference_t1410_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, Reference_t1410_Reference_Compile_m6271_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
static const ParameterInfo Reference_t1410_Reference_GetWidth_m6272_ParameterInfos[] = 
{
	{"min", 0, 134218583, 0, &Int32_t253_1_0_2},
	{"max", 1, 134218584, 0, &Int32_t253_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Reference_GetWidth_m6272_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Reference_GetWidth_m6272/* method */
	, &Reference_t1410_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753/* invoker_method */
	, Reference_t1410_Reference_GetWidth_m6272_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Reference::IsComplex()
extern const MethodInfo Reference_IsComplex_m6273_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&Reference_IsComplex_m6273/* method */
	, &Reference_t1410_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Reference_t1410_MethodInfos[] =
{
	&Reference__ctor_m6267_MethodInfo,
	&Reference_get_CapturingGroup_m6268_MethodInfo,
	&Reference_set_CapturingGroup_m6269_MethodInfo,
	&Reference_get_IgnoreCase_m6270_MethodInfo,
	&Reference_Compile_m6271_MethodInfo,
	&Reference_GetWidth_m6272_MethodInfo,
	&Reference_IsComplex_m6273_MethodInfo,
	NULL
};
extern const MethodInfo Reference_get_CapturingGroup_m6268_MethodInfo;
extern const MethodInfo Reference_set_CapturingGroup_m6269_MethodInfo;
static const PropertyInfo Reference_t1410____CapturingGroup_PropertyInfo = 
{
	&Reference_t1410_il2cpp_TypeInfo/* parent */
	, "CapturingGroup"/* name */
	, &Reference_get_CapturingGroup_m6268_MethodInfo/* get */
	, &Reference_set_CapturingGroup_m6269_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Reference_get_IgnoreCase_m6270_MethodInfo;
static const PropertyInfo Reference_t1410____IgnoreCase_PropertyInfo = 
{
	&Reference_t1410_il2cpp_TypeInfo/* parent */
	, "IgnoreCase"/* name */
	, &Reference_get_IgnoreCase_m6270_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Reference_t1410_PropertyInfos[] =
{
	&Reference_t1410____CapturingGroup_PropertyInfo,
	&Reference_t1410____IgnoreCase_PropertyInfo,
	NULL
};
extern const MethodInfo Reference_Compile_m6271_MethodInfo;
extern const MethodInfo Reference_GetWidth_m6272_MethodInfo;
extern const MethodInfo Reference_IsComplex_m6273_MethodInfo;
static const Il2CppMethodReference Reference_t1410_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Reference_Compile_m6271_MethodInfo,
	&Reference_GetWidth_m6272_MethodInfo,
	&Expression_GetAnchorInfo_m6198_MethodInfo,
	&Reference_IsComplex_m6273_MethodInfo,
};
static bool Reference_t1410_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Reference_t1410_0_0_0;
extern const Il2CppType Reference_t1410_1_0_0;
struct Reference_t1410;
const Il2CppTypeDefinitionMetadata Reference_t1410_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1396_0_0_0/* parent */
	, Reference_t1410_VTable/* vtableMethods */
	, Reference_t1410_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 726/* fieldStart */

};
TypeInfo Reference_t1410_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Reference"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Reference_t1410_MethodInfos/* methods */
	, Reference_t1410_PropertyInfos/* properties */
	, NULL/* events */
	, &Reference_t1410_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Reference_t1410_0_0_0/* byval_arg */
	, &Reference_t1410_1_0_0/* this_arg */
	, &Reference_t1410_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Reference_t1410)/* instance_size */
	, sizeof (Reference_t1410)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.BackslashNumber
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumber.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.BackslashNumber
extern TypeInfo BackslashNumber_t1411_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.BackslashNumber
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumberMethodDeclarations.h"
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo BackslashNumber_t1411_BackslashNumber__ctor_m6274_ParameterInfos[] = 
{
	{"ignore", 0, 134218585, 0, &Boolean_t273_0_0_0},
	{"ecma", 1, 134218586, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::.ctor(System.Boolean,System.Boolean)
extern const MethodInfo BackslashNumber__ctor_m6274_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BackslashNumber__ctor_m6274/* method */
	, &BackslashNumber_t1411_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274_SByte_t274/* invoker_method */
	, BackslashNumber_t1411_BackslashNumber__ctor_m6274_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Hashtable_t1262_0_0_0;
static const ParameterInfo BackslashNumber_t1411_BackslashNumber_ResolveReference_m6275_ParameterInfos[] = 
{
	{"num_str", 0, 134218587, 0, &String_t_0_0_0},
	{"groups", 1, 134218588, 0, &Hashtable_t1262_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.BackslashNumber::ResolveReference(System.String,System.Collections.Hashtable)
extern const MethodInfo BackslashNumber_ResolveReference_m6275_MethodInfo = 
{
	"ResolveReference"/* name */
	, (methodPointerType)&BackslashNumber_ResolveReference_m6275/* method */
	, &BackslashNumber_t1411_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, BackslashNumber_t1411_BackslashNumber_ResolveReference_m6275_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo BackslashNumber_t1411_BackslashNumber_Compile_m6276_ParameterInfos[] = 
{
	{"cmp", 0, 134218589, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218590, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo BackslashNumber_Compile_m6276_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&BackslashNumber_Compile_m6276/* method */
	, &BackslashNumber_t1411_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, BackslashNumber_t1411_BackslashNumber_Compile_m6276_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BackslashNumber_t1411_MethodInfos[] =
{
	&BackslashNumber__ctor_m6274_MethodInfo,
	&BackslashNumber_ResolveReference_m6275_MethodInfo,
	&BackslashNumber_Compile_m6276_MethodInfo,
	NULL
};
extern const MethodInfo BackslashNumber_Compile_m6276_MethodInfo;
static const Il2CppMethodReference BackslashNumber_t1411_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&BackslashNumber_Compile_m6276_MethodInfo,
	&Reference_GetWidth_m6272_MethodInfo,
	&Expression_GetAnchorInfo_m6198_MethodInfo,
	&Reference_IsComplex_m6273_MethodInfo,
};
static bool BackslashNumber_t1411_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType BackslashNumber_t1411_0_0_0;
extern const Il2CppType BackslashNumber_t1411_1_0_0;
struct BackslashNumber_t1411;
const Il2CppTypeDefinitionMetadata BackslashNumber_t1411_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Reference_t1410_0_0_0/* parent */
	, BackslashNumber_t1411_VTable/* vtableMethods */
	, BackslashNumber_t1411_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 728/* fieldStart */

};
TypeInfo BackslashNumber_t1411_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BackslashNumber"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, BackslashNumber_t1411_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BackslashNumber_t1411_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BackslashNumber_t1411_0_0_0/* byval_arg */
	, &BackslashNumber_t1411_1_0_0/* this_arg */
	, &BackslashNumber_t1411_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BackslashNumber_t1411)/* instance_size */
	, sizeof (BackslashNumber_t1411)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CharacterClass
#include "System_System_Text_RegularExpressions_Syntax_CharacterClass.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CharacterClass
extern TypeInfo CharacterClass_t1413_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CharacterClass
#include "System_System_Text_RegularExpressions_Syntax_CharacterClassMethodDeclarations.h"
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo CharacterClass_t1413_CharacterClass__ctor_m6277_ParameterInfos[] = 
{
	{"negate", 0, 134218591, 0, &Boolean_t273_0_0_0},
	{"ignore", 1, 134218592, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.ctor(System.Boolean,System.Boolean)
extern const MethodInfo CharacterClass__ctor_m6277_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CharacterClass__ctor_m6277/* method */
	, &CharacterClass_t1413_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274_SByte_t274/* invoker_method */
	, CharacterClass_t1413_CharacterClass__ctor_m6277_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1374_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo CharacterClass_t1413_CharacterClass__ctor_m6278_ParameterInfos[] = 
{
	{"cat", 0, 134218593, 0, &Category_t1374_0_0_0},
	{"negate", 1, 134218594, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.ctor(System.Text.RegularExpressions.Category,System.Boolean)
extern const MethodInfo CharacterClass__ctor_m6278_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CharacterClass__ctor_m6278/* method */
	, &CharacterClass_t1413_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274/* invoker_method */
	, CharacterClass_t1413_CharacterClass__ctor_m6278_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.cctor()
extern const MethodInfo CharacterClass__cctor_m6279_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CharacterClass__cctor_m6279/* method */
	, &CharacterClass_t1413_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1374_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo CharacterClass_t1413_CharacterClass_AddCategory_m6280_ParameterInfos[] = 
{
	{"cat", 0, 134218595, 0, &Category_t1374_0_0_0},
	{"negate", 1, 134218596, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddCategory(System.Text.RegularExpressions.Category,System.Boolean)
extern const MethodInfo CharacterClass_AddCategory_m6280_MethodInfo = 
{
	"AddCategory"/* name */
	, (methodPointerType)&CharacterClass_AddCategory_m6280/* method */
	, &CharacterClass_t1413_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274/* invoker_method */
	, CharacterClass_t1413_CharacterClass_AddCategory_m6280_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo CharacterClass_t1413_CharacterClass_AddCharacter_m6281_ParameterInfos[] = 
{
	{"c", 0, 134218597, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddCharacter(System.Char)
extern const MethodInfo CharacterClass_AddCharacter_m6281_MethodInfo = 
{
	"AddCharacter"/* name */
	, (methodPointerType)&CharacterClass_AddCharacter_m6281/* method */
	, &CharacterClass_t1413_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752/* invoker_method */
	, CharacterClass_t1413_CharacterClass_AddCharacter_m6281_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo CharacterClass_t1413_CharacterClass_AddRange_m6282_ParameterInfos[] = 
{
	{"lo", 0, 134218598, 0, &Char_t682_0_0_0},
	{"hi", 1, 134218599, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddRange(System.Char,System.Char)
extern const MethodInfo CharacterClass_AddRange_m6282_MethodInfo = 
{
	"AddRange"/* name */
	, (methodPointerType)&CharacterClass_AddRange_m6282/* method */
	, &CharacterClass_t1413_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752_Int16_t752/* invoker_method */
	, CharacterClass_t1413_CharacterClass_AddRange_m6282_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1439_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo CharacterClass_t1413_CharacterClass_Compile_m6283_ParameterInfos[] = 
{
	{"cmp", 0, 134218600, 0, &ICompiler_t1439_0_0_0},
	{"reverse", 1, 134218601, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo CharacterClass_Compile_m6283_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CharacterClass_Compile_m6283/* method */
	, &CharacterClass_t1413_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, CharacterClass_t1413_CharacterClass_Compile_m6283_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_1_0_2;
extern const Il2CppType Int32_t253_1_0_2;
static const ParameterInfo CharacterClass_t1413_CharacterClass_GetWidth_m6284_ParameterInfos[] = 
{
	{"min", 0, 134218602, 0, &Int32_t253_1_0_2},
	{"max", 1, 134218603, 0, &Int32_t253_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo CharacterClass_GetWidth_m6284_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&CharacterClass_GetWidth_m6284/* method */
	, &CharacterClass_t1413_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753/* invoker_method */
	, CharacterClass_t1413_CharacterClass_GetWidth_m6284_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CharacterClass::IsComplex()
extern const MethodInfo CharacterClass_IsComplex_m6285_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CharacterClass_IsComplex_m6285/* method */
	, &CharacterClass_t1413_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1389_0_0_0;
static const ParameterInfo CharacterClass_t1413_CharacterClass_GetIntervalCost_m6286_ParameterInfos[] = 
{
	{"i", 0, 134218604, 0, &Interval_t1389_0_0_0},
};
extern void* RuntimeInvoker_Double_t1070_Interval_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.Syntax.CharacterClass::GetIntervalCost(System.Text.RegularExpressions.Interval)
extern const MethodInfo CharacterClass_GetIntervalCost_m6286_MethodInfo = 
{
	"GetIntervalCost"/* name */
	, (methodPointerType)&CharacterClass_GetIntervalCost_m6286/* method */
	, &CharacterClass_t1413_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1070_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1070_Interval_t1389/* invoker_method */
	, CharacterClass_t1413_CharacterClass_GetIntervalCost_m6286_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CharacterClass_t1413_MethodInfos[] =
{
	&CharacterClass__ctor_m6277_MethodInfo,
	&CharacterClass__ctor_m6278_MethodInfo,
	&CharacterClass__cctor_m6279_MethodInfo,
	&CharacterClass_AddCategory_m6280_MethodInfo,
	&CharacterClass_AddCharacter_m6281_MethodInfo,
	&CharacterClass_AddRange_m6282_MethodInfo,
	&CharacterClass_Compile_m6283_MethodInfo,
	&CharacterClass_GetWidth_m6284_MethodInfo,
	&CharacterClass_IsComplex_m6285_MethodInfo,
	&CharacterClass_GetIntervalCost_m6286_MethodInfo,
	NULL
};
extern const MethodInfo CharacterClass_Compile_m6283_MethodInfo;
extern const MethodInfo CharacterClass_GetWidth_m6284_MethodInfo;
extern const MethodInfo CharacterClass_IsComplex_m6285_MethodInfo;
static const Il2CppMethodReference CharacterClass_t1413_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&CharacterClass_Compile_m6283_MethodInfo,
	&CharacterClass_GetWidth_m6284_MethodInfo,
	&Expression_GetAnchorInfo_m6198_MethodInfo,
	&CharacterClass_IsComplex_m6285_MethodInfo,
};
static bool CharacterClass_t1413_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CharacterClass_t1413_0_0_0;
extern const Il2CppType CharacterClass_t1413_1_0_0;
struct CharacterClass_t1413;
const Il2CppTypeDefinitionMetadata CharacterClass_t1413_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1396_0_0_0/* parent */
	, CharacterClass_t1413_VTable/* vtableMethods */
	, CharacterClass_t1413_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 730/* fieldStart */

};
TypeInfo CharacterClass_t1413_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharacterClass"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CharacterClass_t1413_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CharacterClass_t1413_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CharacterClass_t1413_0_0_0/* byval_arg */
	, &CharacterClass_t1413_1_0_0/* this_arg */
	, &CharacterClass_t1413_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CharacterClass_t1413)/* instance_size */
	, sizeof (CharacterClass_t1413)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CharacterClass_t1413_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.AnchorInfo
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfo.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.AnchorInfo
extern TypeInfo AnchorInfo_t1414_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.AnchorInfo
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfoMethodDeclarations.h"
extern const Il2CppType Expression_t1396_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo AnchorInfo_t1414_AnchorInfo__ctor_m6287_ParameterInfos[] = 
{
	{"expr", 0, 134218605, 0, &Expression_t1396_0_0_0},
	{"width", 1, 134218606, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32)
extern const MethodInfo AnchorInfo__ctor_m6287_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m6287/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, AnchorInfo_t1414_AnchorInfo__ctor_m6287_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1396_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo AnchorInfo_t1414_AnchorInfo__ctor_m6288_ParameterInfos[] = 
{
	{"expr", 0, 134218607, 0, &Expression_t1396_0_0_0},
	{"offset", 1, 134218608, 0, &Int32_t253_0_0_0},
	{"width", 2, 134218609, 0, &Int32_t253_0_0_0},
	{"str", 3, 134218610, 0, &String_t_0_0_0},
	{"ignore", 4, 134218611, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32,System.Int32,System.String,System.Boolean)
extern const MethodInfo AnchorInfo__ctor_m6288_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m6288/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Object_t_SByte_t274/* invoker_method */
	, AnchorInfo_t1414_AnchorInfo__ctor_m6288_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1396_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Position_t1370_0_0_0;
static const ParameterInfo AnchorInfo_t1414_AnchorInfo__ctor_m6289_ParameterInfos[] = 
{
	{"expr", 0, 134218612, 0, &Expression_t1396_0_0_0},
	{"offset", 1, 134218613, 0, &Int32_t253_0_0_0},
	{"width", 2, 134218614, 0, &Int32_t253_0_0_0},
	{"pos", 3, 134218615, 0, &Position_t1370_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32,System.Int32,System.Text.RegularExpressions.Position)
extern const MethodInfo AnchorInfo__ctor_m6289_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m6289/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_UInt16_t684/* invoker_method */
	, AnchorInfo_t1414_AnchorInfo__ctor_m6289_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Offset()
extern const MethodInfo AnchorInfo_get_Offset_m6290_MethodInfo = 
{
	"get_Offset"/* name */
	, (methodPointerType)&AnchorInfo_get_Offset_m6290/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Width()
extern const MethodInfo AnchorInfo_get_Width_m6291_MethodInfo = 
{
	"get_Width"/* name */
	, (methodPointerType)&AnchorInfo_get_Width_m6291/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Length()
extern const MethodInfo AnchorInfo_get_Length_m6292_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&AnchorInfo_get_Length_m6292/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsUnknownWidth()
extern const MethodInfo AnchorInfo_get_IsUnknownWidth_m6293_MethodInfo = 
{
	"get_IsUnknownWidth"/* name */
	, (methodPointerType)&AnchorInfo_get_IsUnknownWidth_m6293/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsComplete()
extern const MethodInfo AnchorInfo_get_IsComplete_m6294_MethodInfo = 
{
	"get_IsComplete"/* name */
	, (methodPointerType)&AnchorInfo_get_IsComplete_m6294/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.AnchorInfo::get_Substring()
extern const MethodInfo AnchorInfo_get_Substring_m6295_MethodInfo = 
{
	"get_Substring"/* name */
	, (methodPointerType)&AnchorInfo_get_Substring_m6295/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IgnoreCase()
extern const MethodInfo AnchorInfo_get_IgnoreCase_m6296_MethodInfo = 
{
	"get_IgnoreCase"/* name */
	, (methodPointerType)&AnchorInfo_get_IgnoreCase_m6296/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Position_t1370 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Position System.Text.RegularExpressions.Syntax.AnchorInfo::get_Position()
extern const MethodInfo AnchorInfo_get_Position_m6297_MethodInfo = 
{
	"get_Position"/* name */
	, (methodPointerType)&AnchorInfo_get_Position_m6297/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Position_t1370_0_0_0/* return_type */
	, RuntimeInvoker_Position_t1370/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsSubstring()
extern const MethodInfo AnchorInfo_get_IsSubstring_m6298_MethodInfo = 
{
	"get_IsSubstring"/* name */
	, (methodPointerType)&AnchorInfo_get_IsSubstring_m6298/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsPosition()
extern const MethodInfo AnchorInfo_get_IsPosition_m6299_MethodInfo = 
{
	"get_IsPosition"/* name */
	, (methodPointerType)&AnchorInfo_get_IsPosition_m6299/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo AnchorInfo_t1414_AnchorInfo_GetInterval_m6300_ParameterInfos[] = 
{
	{"start", 0, 134218616, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Interval_t1389_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.Syntax.AnchorInfo::GetInterval(System.Int32)
extern const MethodInfo AnchorInfo_GetInterval_m6300_MethodInfo = 
{
	"GetInterval"/* name */
	, (methodPointerType)&AnchorInfo_GetInterval_m6300/* method */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t1389_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t1389_Int32_t253/* invoker_method */
	, AnchorInfo_t1414_AnchorInfo_GetInterval_m6300_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AnchorInfo_t1414_MethodInfos[] =
{
	&AnchorInfo__ctor_m6287_MethodInfo,
	&AnchorInfo__ctor_m6288_MethodInfo,
	&AnchorInfo__ctor_m6289_MethodInfo,
	&AnchorInfo_get_Offset_m6290_MethodInfo,
	&AnchorInfo_get_Width_m6291_MethodInfo,
	&AnchorInfo_get_Length_m6292_MethodInfo,
	&AnchorInfo_get_IsUnknownWidth_m6293_MethodInfo,
	&AnchorInfo_get_IsComplete_m6294_MethodInfo,
	&AnchorInfo_get_Substring_m6295_MethodInfo,
	&AnchorInfo_get_IgnoreCase_m6296_MethodInfo,
	&AnchorInfo_get_Position_m6297_MethodInfo,
	&AnchorInfo_get_IsSubstring_m6298_MethodInfo,
	&AnchorInfo_get_IsPosition_m6299_MethodInfo,
	&AnchorInfo_GetInterval_m6300_MethodInfo,
	NULL
};
extern const MethodInfo AnchorInfo_get_Offset_m6290_MethodInfo;
static const PropertyInfo AnchorInfo_t1414____Offset_PropertyInfo = 
{
	&AnchorInfo_t1414_il2cpp_TypeInfo/* parent */
	, "Offset"/* name */
	, &AnchorInfo_get_Offset_m6290_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Width_m6291_MethodInfo;
static const PropertyInfo AnchorInfo_t1414____Width_PropertyInfo = 
{
	&AnchorInfo_t1414_il2cpp_TypeInfo/* parent */
	, "Width"/* name */
	, &AnchorInfo_get_Width_m6291_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Length_m6292_MethodInfo;
static const PropertyInfo AnchorInfo_t1414____Length_PropertyInfo = 
{
	&AnchorInfo_t1414_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &AnchorInfo_get_Length_m6292_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsUnknownWidth_m6293_MethodInfo;
static const PropertyInfo AnchorInfo_t1414____IsUnknownWidth_PropertyInfo = 
{
	&AnchorInfo_t1414_il2cpp_TypeInfo/* parent */
	, "IsUnknownWidth"/* name */
	, &AnchorInfo_get_IsUnknownWidth_m6293_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsComplete_m6294_MethodInfo;
static const PropertyInfo AnchorInfo_t1414____IsComplete_PropertyInfo = 
{
	&AnchorInfo_t1414_il2cpp_TypeInfo/* parent */
	, "IsComplete"/* name */
	, &AnchorInfo_get_IsComplete_m6294_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Substring_m6295_MethodInfo;
static const PropertyInfo AnchorInfo_t1414____Substring_PropertyInfo = 
{
	&AnchorInfo_t1414_il2cpp_TypeInfo/* parent */
	, "Substring"/* name */
	, &AnchorInfo_get_Substring_m6295_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IgnoreCase_m6296_MethodInfo;
static const PropertyInfo AnchorInfo_t1414____IgnoreCase_PropertyInfo = 
{
	&AnchorInfo_t1414_il2cpp_TypeInfo/* parent */
	, "IgnoreCase"/* name */
	, &AnchorInfo_get_IgnoreCase_m6296_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Position_m6297_MethodInfo;
static const PropertyInfo AnchorInfo_t1414____Position_PropertyInfo = 
{
	&AnchorInfo_t1414_il2cpp_TypeInfo/* parent */
	, "Position"/* name */
	, &AnchorInfo_get_Position_m6297_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsSubstring_m6298_MethodInfo;
static const PropertyInfo AnchorInfo_t1414____IsSubstring_PropertyInfo = 
{
	&AnchorInfo_t1414_il2cpp_TypeInfo/* parent */
	, "IsSubstring"/* name */
	, &AnchorInfo_get_IsSubstring_m6298_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsPosition_m6299_MethodInfo;
static const PropertyInfo AnchorInfo_t1414____IsPosition_PropertyInfo = 
{
	&AnchorInfo_t1414_il2cpp_TypeInfo/* parent */
	, "IsPosition"/* name */
	, &AnchorInfo_get_IsPosition_m6299_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AnchorInfo_t1414_PropertyInfos[] =
{
	&AnchorInfo_t1414____Offset_PropertyInfo,
	&AnchorInfo_t1414____Width_PropertyInfo,
	&AnchorInfo_t1414____Length_PropertyInfo,
	&AnchorInfo_t1414____IsUnknownWidth_PropertyInfo,
	&AnchorInfo_t1414____IsComplete_PropertyInfo,
	&AnchorInfo_t1414____Substring_PropertyInfo,
	&AnchorInfo_t1414____IgnoreCase_PropertyInfo,
	&AnchorInfo_t1414____Position_PropertyInfo,
	&AnchorInfo_t1414____IsSubstring_PropertyInfo,
	&AnchorInfo_t1414____IsPosition_PropertyInfo,
	NULL
};
static const Il2CppMethodReference AnchorInfo_t1414_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool AnchorInfo_t1414_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AnchorInfo_t1414_1_0_0;
struct AnchorInfo_t1414;
const Il2CppTypeDefinitionMetadata AnchorInfo_t1414_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AnchorInfo_t1414_VTable/* vtableMethods */
	, AnchorInfo_t1414_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 736/* fieldStart */

};
TypeInfo AnchorInfo_t1414_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnchorInfo"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, AnchorInfo_t1414_MethodInfos/* methods */
	, AnchorInfo_t1414_PropertyInfos/* properties */
	, NULL/* events */
	, &AnchorInfo_t1414_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnchorInfo_t1414_0_0_0/* byval_arg */
	, &AnchorInfo_t1414_1_0_0/* this_arg */
	, &AnchorInfo_t1414_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnchorInfo_t1414)/* instance_size */
	, sizeof (AnchorInfo_t1414)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 10/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DefaultUriParser
#include "System_System_DefaultUriParser.h"
// Metadata Definition System.DefaultUriParser
extern TypeInfo DefaultUriParser_t1415_il2cpp_TypeInfo;
// System.DefaultUriParser
#include "System_System_DefaultUriParserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.DefaultUriParser::.ctor()
extern const MethodInfo DefaultUriParser__ctor_m6301_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultUriParser__ctor_m6301/* method */
	, &DefaultUriParser_t1415_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DefaultUriParser_t1415_DefaultUriParser__ctor_m6302_ParameterInfos[] = 
{
	{"scheme", 0, 134218617, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.DefaultUriParser::.ctor(System.String)
extern const MethodInfo DefaultUriParser__ctor_m6302_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultUriParser__ctor_m6302/* method */
	, &DefaultUriParser_t1415_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DefaultUriParser_t1415_DefaultUriParser__ctor_m6302_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultUriParser_t1415_MethodInfos[] =
{
	&DefaultUriParser__ctor_m6301_MethodInfo,
	&DefaultUriParser__ctor_m6302_MethodInfo,
	NULL
};
extern const MethodInfo UriParser_InitializeAndValidate_m6358_MethodInfo;
extern const MethodInfo UriParser_OnRegister_m6359_MethodInfo;
static const Il2CppMethodReference DefaultUriParser_t1415_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&UriParser_InitializeAndValidate_m6358_MethodInfo,
	&UriParser_OnRegister_m6359_MethodInfo,
};
static bool DefaultUriParser_t1415_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DefaultUriParser_t1415_0_0_0;
extern const Il2CppType DefaultUriParser_t1415_1_0_0;
extern const Il2CppType UriParser_t1416_0_0_0;
struct DefaultUriParser_t1415;
const Il2CppTypeDefinitionMetadata DefaultUriParser_t1415_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UriParser_t1416_0_0_0/* parent */
	, DefaultUriParser_t1415_VTable/* vtableMethods */
	, DefaultUriParser_t1415_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DefaultUriParser_t1415_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultUriParser"/* name */
	, "System"/* namespaze */
	, DefaultUriParser_t1415_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefaultUriParser_t1415_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultUriParser_t1415_0_0_0/* byval_arg */
	, &DefaultUriParser_t1415_1_0_0/* this_arg */
	, &DefaultUriParser_t1415_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultUriParser_t1415)/* instance_size */
	, sizeof (DefaultUriParser_t1415)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.GenericUriParser
#include "System_System_GenericUriParser.h"
// Metadata Definition System.GenericUriParser
extern TypeInfo GenericUriParser_t1417_il2cpp_TypeInfo;
// System.GenericUriParser
#include "System_System_GenericUriParserMethodDeclarations.h"
static const MethodInfo* GenericUriParser_t1417_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference GenericUriParser_t1417_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&UriParser_InitializeAndValidate_m6358_MethodInfo,
	&UriParser_OnRegister_m6359_MethodInfo,
};
static bool GenericUriParser_t1417_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType GenericUriParser_t1417_0_0_0;
extern const Il2CppType GenericUriParser_t1417_1_0_0;
struct GenericUriParser_t1417;
const Il2CppTypeDefinitionMetadata GenericUriParser_t1417_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UriParser_t1416_0_0_0/* parent */
	, GenericUriParser_t1417_VTable/* vtableMethods */
	, GenericUriParser_t1417_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GenericUriParser_t1417_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericUriParser"/* name */
	, "System"/* namespaze */
	, GenericUriParser_t1417_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GenericUriParser_t1417_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericUriParser_t1417_0_0_0/* byval_arg */
	, &GenericUriParser_t1417_1_0_0/* this_arg */
	, &GenericUriParser_t1417_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericUriParser_t1417)/* instance_size */
	, sizeof (GenericUriParser_t1417)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
// Metadata Definition System.Uri/UriScheme
extern TypeInfo UriScheme_t1418_il2cpp_TypeInfo;
// System.Uri/UriScheme
#include "System_System_Uri_UriSchemeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo UriScheme_t1418_UriScheme__ctor_m6303_ParameterInfos[] = 
{
	{"s", 0, 134218670, 0, &String_t_0_0_0},
	{"d", 1, 134218671, 0, &String_t_0_0_0},
	{"p", 2, 134218672, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri/UriScheme::.ctor(System.String,System.String,System.Int32)
extern const MethodInfo UriScheme__ctor_m6303_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriScheme__ctor_m6303/* method */
	, &UriScheme_t1418_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253/* invoker_method */
	, UriScheme_t1418_UriScheme__ctor_m6303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UriScheme_t1418_MethodInfos[] =
{
	&UriScheme__ctor_m6303_MethodInfo,
	NULL
};
static const Il2CppMethodReference UriScheme_t1418_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool UriScheme_t1418_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriScheme_t1418_0_0_0;
extern const Il2CppType UriScheme_t1418_1_0_0;
extern TypeInfo Uri_t926_il2cpp_TypeInfo;
extern const Il2CppType Uri_t926_0_0_0;
const Il2CppTypeDefinitionMetadata UriScheme_t1418_DefinitionMetadata = 
{
	&Uri_t926_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, UriScheme_t1418_VTable/* vtableMethods */
	, UriScheme_t1418_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 742/* fieldStart */

};
TypeInfo UriScheme_t1418_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriScheme"/* name */
	, ""/* namespaze */
	, UriScheme_t1418_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UriScheme_t1418_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriScheme_t1418_0_0_0/* byval_arg */
	, &UriScheme_t1418_1_0_0/* this_arg */
	, &UriScheme_t1418_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)UriScheme_t1418_marshal/* marshal_to_native_func */
	, (methodPointerType)UriScheme_t1418_marshal_back/* marshal_from_native_func */
	, (methodPointerType)UriScheme_t1418_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (UriScheme_t1418)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriScheme_t1418)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UriScheme_t1418_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Uri
#include "System_System_Uri.h"
// Metadata Definition System.Uri
// System.Uri
#include "System_System_UriMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri__ctor_m5165_ParameterInfos[] = 
{
	{"uriString", 0, 134218618, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.String)
extern const MethodInfo Uri__ctor_m5165_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m5165/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Uri_t926_Uri__ctor_m5165_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo Uri_t926_Uri__ctor_m6304_ParameterInfos[] = 
{
	{"serializationInfo", 0, 134218619, 0, &SerializationInfo_t1043_0_0_0},
	{"streamingContext", 1, 134218620, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Uri__ctor_m6304_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m6304/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, Uri_t926_Uri__ctor_m6304_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Uri_t926_Uri__ctor_m6305_ParameterInfos[] = 
{
	{"uriString", 0, 134218621, 0, &String_t_0_0_0},
	{"dontEscape", 1, 134218622, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.String,System.Boolean)
extern const MethodInfo Uri__ctor_m6305_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m6305/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, Uri_t926_Uri__ctor_m6305_ParameterInfos/* parameters */
	, 70/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t926_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri__ctor_m5167_ParameterInfos[] = 
{
	{"baseUri", 0, 134218623, 0, &Uri_t926_0_0_0},
	{"relativeUri", 1, 134218624, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.Uri,System.String)
extern const MethodInfo Uri__ctor_m5167_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m5167/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Uri_t926_Uri__ctor_m5167_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.cctor()
extern const MethodInfo Uri__cctor_m6306_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Uri__cctor_m6306/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo Uri_t926_Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m6307_ParameterInfos[] = 
{
	{"info", 0, 134218625, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134218626, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m6307_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m6307/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, Uri_t926_Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m6307_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t926_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_Merge_m6308_ParameterInfos[] = 
{
	{"baseUri", 0, 134218627, 0, &Uri_t926_0_0_0},
	{"relativeUri", 1, 134218628, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::Merge(System.Uri,System.String)
extern const MethodInfo Uri_Merge_m6308_MethodInfo = 
{
	"Merge"/* name */
	, (methodPointerType)&Uri_Merge_m6308/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Uri_t926_Uri_Merge_m6308_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_AbsoluteUri()
extern const MethodInfo Uri_get_AbsoluteUri_m6309_MethodInfo = 
{
	"get_AbsoluteUri"/* name */
	, (methodPointerType)&Uri_get_AbsoluteUri_m6309/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Authority()
extern const MethodInfo Uri_get_Authority_m6310_MethodInfo = 
{
	"get_Authority"/* name */
	, (methodPointerType)&Uri_get_Authority_m6310/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Host()
extern const MethodInfo Uri_get_Host_m6311_MethodInfo = 
{
	"get_Host"/* name */
	, (methodPointerType)&Uri_get_Host_m6311/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsFile()
extern const MethodInfo Uri_get_IsFile_m6312_MethodInfo = 
{
	"get_IsFile"/* name */
	, (methodPointerType)&Uri_get_IsFile_m6312/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsLoopback()
extern const MethodInfo Uri_get_IsLoopback_m6313_MethodInfo = 
{
	"get_IsLoopback"/* name */
	, (methodPointerType)&Uri_get_IsLoopback_m6313/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsUnc()
extern const MethodInfo Uri_get_IsUnc_m6314_MethodInfo = 
{
	"get_IsUnc"/* name */
	, (methodPointerType)&Uri_get_IsUnc_m6314/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Scheme()
extern const MethodInfo Uri_get_Scheme_m6315_MethodInfo = 
{
	"get_Scheme"/* name */
	, (methodPointerType)&Uri_get_Scheme_m6315/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsAbsoluteUri()
extern const MethodInfo Uri_get_IsAbsoluteUri_m6316_MethodInfo = 
{
	"get_IsAbsoluteUri"/* name */
	, (methodPointerType)&Uri_get_IsAbsoluteUri_m6316/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_CheckHostName_m6317_ParameterInfos[] = 
{
	{"name", 0, 134218629, 0, &String_t_0_0_0},
};
extern const Il2CppType UriHostNameType_t1421_0_0_0;
extern void* RuntimeInvoker_UriHostNameType_t1421_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UriHostNameType System.Uri::CheckHostName(System.String)
extern const MethodInfo Uri_CheckHostName_m6317_MethodInfo = 
{
	"CheckHostName"/* name */
	, (methodPointerType)&Uri_CheckHostName_m6317/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &UriHostNameType_t1421_0_0_0/* return_type */
	, RuntimeInvoker_UriHostNameType_t1421_Object_t/* invoker_method */
	, Uri_t926_Uri_CheckHostName_m6317_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_IsIPv4Address_m6318_ParameterInfos[] = 
{
	{"name", 0, 134218630, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsIPv4Address(System.String)
extern const MethodInfo Uri_IsIPv4Address_m6318_MethodInfo = 
{
	"IsIPv4Address"/* name */
	, (methodPointerType)&Uri_IsIPv4Address_m6318/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Uri_t926_Uri_IsIPv4Address_m6318_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_IsDomainAddress_m6319_ParameterInfos[] = 
{
	{"name", 0, 134218631, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsDomainAddress(System.String)
extern const MethodInfo Uri_IsDomainAddress_m6319_MethodInfo = 
{
	"IsDomainAddress"/* name */
	, (methodPointerType)&Uri_IsDomainAddress_m6319/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Uri_t926_Uri_IsDomainAddress_m6319_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_CheckSchemeName_m6320_ParameterInfos[] = 
{
	{"schemeName", 0, 134218632, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::CheckSchemeName(System.String)
extern const MethodInfo Uri_CheckSchemeName_m6320_MethodInfo = 
{
	"CheckSchemeName"/* name */
	, (methodPointerType)&Uri_CheckSchemeName_m6320/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Uri_t926_Uri_CheckSchemeName_m6320_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo Uri_t926_Uri_IsAlpha_m6321_ParameterInfos[] = 
{
	{"c", 0, 134218633, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsAlpha(System.Char)
extern const MethodInfo Uri_IsAlpha_m6321_MethodInfo = 
{
	"IsAlpha"/* name */
	, (methodPointerType)&Uri_IsAlpha_m6321/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int16_t752/* invoker_method */
	, Uri_t926_Uri_IsAlpha_m6321_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_Equals_m6322_ParameterInfos[] = 
{
	{"comparant", 0, 134218634, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::Equals(System.Object)
extern const MethodInfo Uri_Equals_m6322_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Uri_Equals_m6322/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Uri_t926_Uri_Equals_m6322_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t926_0_0_0;
static const ParameterInfo Uri_t926_Uri_InternalEquals_m6323_ParameterInfos[] = 
{
	{"uri", 0, 134218635, 0, &Uri_t926_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::InternalEquals(System.Uri)
extern const MethodInfo Uri_InternalEquals_m6323_MethodInfo = 
{
	"InternalEquals"/* name */
	, (methodPointerType)&Uri_InternalEquals_m6323/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Uri_t926_Uri_InternalEquals_m6323_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::GetHashCode()
extern const MethodInfo Uri_GetHashCode_m6324_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Uri_GetHashCode_m6324/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriPartial_t1423_0_0_0;
extern const Il2CppType UriPartial_t1423_0_0_0;
static const ParameterInfo Uri_t926_Uri_GetLeftPart_m6325_ParameterInfos[] = 
{
	{"part", 0, 134218636, 0, &UriPartial_t1423_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetLeftPart(System.UriPartial)
extern const MethodInfo Uri_GetLeftPart_m6325_MethodInfo = 
{
	"GetLeftPart"/* name */
	, (methodPointerType)&Uri_GetLeftPart_m6325/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, Uri_t926_Uri_GetLeftPart_m6325_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo Uri_t926_Uri_FromHex_m6326_ParameterInfos[] = 
{
	{"digit", 0, 134218637, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::FromHex(System.Char)
extern const MethodInfo Uri_FromHex_m6326_MethodInfo = 
{
	"FromHex"/* name */
	, (methodPointerType)&Uri_FromHex_m6326/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int16_t752/* invoker_method */
	, Uri_t926_Uri_FromHex_m6326_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo Uri_t926_Uri_HexEscape_m6327_ParameterInfos[] = 
{
	{"character", 0, 134218638, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::HexEscape(System.Char)
extern const MethodInfo Uri_HexEscape_m6327_MethodInfo = 
{
	"HexEscape"/* name */
	, (methodPointerType)&Uri_HexEscape_m6327/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int16_t752/* invoker_method */
	, Uri_t926_Uri_HexEscape_m6327_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo Uri_t926_Uri_IsHexDigit_m6328_ParameterInfos[] = 
{
	{"digit", 0, 134218639, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsHexDigit(System.Char)
extern const MethodInfo Uri_IsHexDigit_m6328_MethodInfo = 
{
	"IsHexDigit"/* name */
	, (methodPointerType)&Uri_IsHexDigit_m6328/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int16_t752/* invoker_method */
	, Uri_t926_Uri_IsHexDigit_m6328_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Uri_t926_Uri_IsHexEncoding_m6329_ParameterInfos[] = 
{
	{"pattern", 0, 134218640, 0, &String_t_0_0_0},
	{"index", 1, 134218641, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsHexEncoding(System.String,System.Int32)
extern const MethodInfo Uri_IsHexEncoding_m6329_MethodInfo = 
{
	"IsHexEncoding"/* name */
	, (methodPointerType)&Uri_IsHexEncoding_m6329/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Int32_t253/* invoker_method */
	, Uri_t926_Uri_IsHexEncoding_m6329_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_1_0_0;
extern const Il2CppType String_t_1_0_0;
static const ParameterInfo Uri_t926_Uri_AppendQueryAndFragment_m6330_ParameterInfos[] = 
{
	{"result", 0, 134218642, 0, &String_t_1_0_0},
};
extern void* RuntimeInvoker_Void_t272_StringU26_t1216 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::AppendQueryAndFragment(System.String&)
extern const MethodInfo Uri_AppendQueryAndFragment_m6330_MethodInfo = 
{
	"AppendQueryAndFragment"/* name */
	, (methodPointerType)&Uri_AppendQueryAndFragment_m6330/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_StringU26_t1216/* invoker_method */
	, Uri_t926_Uri_AppendQueryAndFragment_m6330_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ToString()
extern const MethodInfo Uri_ToString_m6331_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Uri_ToString_m6331/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_EscapeString_m6332_ParameterInfos[] = 
{
	{"str", 0, 134218643, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::EscapeString(System.String)
extern const MethodInfo Uri_EscapeString_m6332_MethodInfo = 
{
	"EscapeString"/* name */
	, (methodPointerType)&Uri_EscapeString_m6332/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t926_Uri_EscapeString_m6332_ParameterInfos/* parameters */
	, 71/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Uri_t926_Uri_EscapeString_m6333_ParameterInfos[] = 
{
	{"str", 0, 134218644, 0, &String_t_0_0_0},
	{"escapeReserved", 1, 134218645, 0, &Boolean_t273_0_0_0},
	{"escapeHex", 2, 134218646, 0, &Boolean_t273_0_0_0},
	{"escapeBrackets", 3, 134218647, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::EscapeString(System.String,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo Uri_EscapeString_m6333_MethodInfo = 
{
	"EscapeString"/* name */
	, (methodPointerType)&Uri_EscapeString_m6333/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274_SByte_t274_SByte_t274/* invoker_method */
	, Uri_t926_Uri_EscapeString_m6333_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriKind_t1422_0_0_0;
extern const Il2CppType UriKind_t1422_0_0_0;
static const ParameterInfo Uri_t926_Uri_ParseUri_m6334_ParameterInfos[] = 
{
	{"kind", 0, 134218648, 0, &UriKind_t1422_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseUri(System.UriKind)
extern const MethodInfo Uri_ParseUri_m6334_MethodInfo = 
{
	"ParseUri"/* name */
	, (methodPointerType)&Uri_ParseUri_m6334/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Uri_t926_Uri_ParseUri_m6334_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_Unescape_m6335_ParameterInfos[] = 
{
	{"str", 0, 134218649, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Unescape(System.String)
extern const MethodInfo Uri_Unescape_m6335_MethodInfo = 
{
	"Unescape"/* name */
	, (methodPointerType)&Uri_Unescape_m6335/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t926_Uri_Unescape_m6335_ParameterInfos/* parameters */
	, 72/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Uri_t926_Uri_Unescape_m6336_ParameterInfos[] = 
{
	{"str", 0, 134218650, 0, &String_t_0_0_0},
	{"excludeSpecial", 1, 134218651, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Unescape(System.String,System.Boolean)
extern const MethodInfo Uri_Unescape_m6336_MethodInfo = 
{
	"Unescape"/* name */
	, (methodPointerType)&Uri_Unescape_m6336/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274/* invoker_method */
	, Uri_t926_Uri_Unescape_m6336_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_ParseAsWindowsUNC_m6337_ParameterInfos[] = 
{
	{"uriString", 0, 134218652, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseAsWindowsUNC(System.String)
extern const MethodInfo Uri_ParseAsWindowsUNC_m6337_MethodInfo = 
{
	"ParseAsWindowsUNC"/* name */
	, (methodPointerType)&Uri_ParseAsWindowsUNC_m6337/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Uri_t926_Uri_ParseAsWindowsUNC_m6337_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_ParseAsWindowsAbsoluteFilePath_m6338_ParameterInfos[] = 
{
	{"uriString", 0, 134218653, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ParseAsWindowsAbsoluteFilePath(System.String)
extern const MethodInfo Uri_ParseAsWindowsAbsoluteFilePath_m6338_MethodInfo = 
{
	"ParseAsWindowsAbsoluteFilePath"/* name */
	, (methodPointerType)&Uri_ParseAsWindowsAbsoluteFilePath_m6338/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t926_Uri_ParseAsWindowsAbsoluteFilePath_m6338_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_ParseAsUnixAbsoluteFilePath_m6339_ParameterInfos[] = 
{
	{"uriString", 0, 134218654, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseAsUnixAbsoluteFilePath(System.String)
extern const MethodInfo Uri_ParseAsUnixAbsoluteFilePath_m6339_MethodInfo = 
{
	"ParseAsUnixAbsoluteFilePath"/* name */
	, (methodPointerType)&Uri_ParseAsUnixAbsoluteFilePath_m6339/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Uri_t926_Uri_ParseAsUnixAbsoluteFilePath_m6339_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriKind_t1422_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_Parse_m6340_ParameterInfos[] = 
{
	{"kind", 0, 134218655, 0, &UriKind_t1422_0_0_0},
	{"uriString", 1, 134218656, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::Parse(System.UriKind,System.String)
extern const MethodInfo Uri_Parse_m6340_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&Uri_Parse_m6340/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Object_t/* invoker_method */
	, Uri_t926_Uri_Parse_m6340_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriKind_t1422_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_ParseNoExceptions_m6341_ParameterInfos[] = 
{
	{"kind", 0, 134218657, 0, &UriKind_t1422_0_0_0},
	{"uriString", 1, 134218658, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ParseNoExceptions(System.UriKind,System.String)
extern const MethodInfo Uri_ParseNoExceptions_m6341_MethodInfo = 
{
	"ParseNoExceptions"/* name */
	, (methodPointerType)&Uri_ParseNoExceptions_m6341/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t/* invoker_method */
	, Uri_t926_Uri_ParseNoExceptions_m6341_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_CompactEscaped_m6342_ParameterInfos[] = 
{
	{"scheme", 0, 134218659, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::CompactEscaped(System.String)
extern const MethodInfo Uri_CompactEscaped_m6342_MethodInfo = 
{
	"CompactEscaped"/* name */
	, (methodPointerType)&Uri_CompactEscaped_m6342/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Uri_t926_Uri_CompactEscaped_m6342_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Uri_t926_Uri_Reduce_m6343_ParameterInfos[] = 
{
	{"path", 0, 134218660, 0, &String_t_0_0_0},
	{"compact_escaped", 1, 134218661, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Reduce(System.String,System.Boolean)
extern const MethodInfo Uri_Reduce_m6343_MethodInfo = 
{
	"Reduce"/* name */
	, (methodPointerType)&Uri_Reduce_m6343/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274/* invoker_method */
	, Uri_t926_Uri_Reduce_m6343_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Char_t682_1_0_2;
extern const Il2CppType Char_t682_1_0_0;
static const ParameterInfo Uri_t926_Uri_HexUnescapeMultiByte_m6344_ParameterInfos[] = 
{
	{"pattern", 0, 134218662, 0, &String_t_0_0_0},
	{"index", 1, 134218663, 0, &Int32_t253_1_0_0},
	{"surrogate", 2, 134218664, 0, &Char_t682_1_0_2},
};
extern void* RuntimeInvoker_Char_t682_Object_t_Int32U26_t753_CharU26_t1492 (const MethodInfo* method, void* obj, void** args);
// System.Char System.Uri::HexUnescapeMultiByte(System.String,System.Int32&,System.Char&)
extern const MethodInfo Uri_HexUnescapeMultiByte_m6344_MethodInfo = 
{
	"HexUnescapeMultiByte"/* name */
	, (methodPointerType)&Uri_HexUnescapeMultiByte_m6344/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Char_t682_0_0_0/* return_type */
	, RuntimeInvoker_Char_t682_Object_t_Int32U26_t753_CharU26_t1492/* invoker_method */
	, Uri_t926_Uri_HexUnescapeMultiByte_m6344_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_GetSchemeDelimiter_m6345_ParameterInfos[] = 
{
	{"scheme", 0, 134218665, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetSchemeDelimiter(System.String)
extern const MethodInfo Uri_GetSchemeDelimiter_m6345_MethodInfo = 
{
	"GetSchemeDelimiter"/* name */
	, (methodPointerType)&Uri_GetSchemeDelimiter_m6345/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t926_Uri_GetSchemeDelimiter_m6345_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_GetDefaultPort_m6346_ParameterInfos[] = 
{
	{"scheme", 0, 134218666, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::GetDefaultPort(System.String)
extern const MethodInfo Uri_GetDefaultPort_m6346_MethodInfo = 
{
	"GetDefaultPort"/* name */
	, (methodPointerType)&Uri_GetDefaultPort_m6346/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, Uri_t926_Uri_GetDefaultPort_m6346_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetOpaqueWiseSchemeDelimiter()
extern const MethodInfo Uri_GetOpaqueWiseSchemeDelimiter_m6347_MethodInfo = 
{
	"GetOpaqueWiseSchemeDelimiter"/* name */
	, (methodPointerType)&Uri_GetOpaqueWiseSchemeDelimiter_m6347/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t926_Uri_IsPredefinedScheme_m6348_ParameterInfos[] = 
{
	{"scheme", 0, 134218667, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsPredefinedScheme(System.String)
extern const MethodInfo Uri_IsPredefinedScheme_m6348_MethodInfo = 
{
	"IsPredefinedScheme"/* name */
	, (methodPointerType)&Uri_IsPredefinedScheme_m6348/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Uri_t926_Uri_IsPredefinedScheme_m6348_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UriParser System.Uri::get_Parser()
extern const MethodInfo Uri_get_Parser_m6349_MethodInfo = 
{
	"get_Parser"/* name */
	, (methodPointerType)&Uri_get_Parser_m6349/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &UriParser_t1416_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::EnsureAbsoluteUri()
extern const MethodInfo Uri_EnsureAbsoluteUri_m6350_MethodInfo = 
{
	"EnsureAbsoluteUri"/* name */
	, (methodPointerType)&Uri_EnsureAbsoluteUri_m6350/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t926_0_0_0;
extern const Il2CppType Uri_t926_0_0_0;
static const ParameterInfo Uri_t926_Uri_op_Equality_m6351_ParameterInfos[] = 
{
	{"u1", 0, 134218668, 0, &Uri_t926_0_0_0},
	{"u2", 1, 134218669, 0, &Uri_t926_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::op_Equality(System.Uri,System.Uri)
extern const MethodInfo Uri_op_Equality_m6351_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&Uri_op_Equality_m6351/* method */
	, &Uri_t926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, Uri_t926_Uri_op_Equality_m6351_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Uri_t926_MethodInfos[] =
{
	&Uri__ctor_m5165_MethodInfo,
	&Uri__ctor_m6304_MethodInfo,
	&Uri__ctor_m6305_MethodInfo,
	&Uri__ctor_m5167_MethodInfo,
	&Uri__cctor_m6306_MethodInfo,
	&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m6307_MethodInfo,
	&Uri_Merge_m6308_MethodInfo,
	&Uri_get_AbsoluteUri_m6309_MethodInfo,
	&Uri_get_Authority_m6310_MethodInfo,
	&Uri_get_Host_m6311_MethodInfo,
	&Uri_get_IsFile_m6312_MethodInfo,
	&Uri_get_IsLoopback_m6313_MethodInfo,
	&Uri_get_IsUnc_m6314_MethodInfo,
	&Uri_get_Scheme_m6315_MethodInfo,
	&Uri_get_IsAbsoluteUri_m6316_MethodInfo,
	&Uri_CheckHostName_m6317_MethodInfo,
	&Uri_IsIPv4Address_m6318_MethodInfo,
	&Uri_IsDomainAddress_m6319_MethodInfo,
	&Uri_CheckSchemeName_m6320_MethodInfo,
	&Uri_IsAlpha_m6321_MethodInfo,
	&Uri_Equals_m6322_MethodInfo,
	&Uri_InternalEquals_m6323_MethodInfo,
	&Uri_GetHashCode_m6324_MethodInfo,
	&Uri_GetLeftPart_m6325_MethodInfo,
	&Uri_FromHex_m6326_MethodInfo,
	&Uri_HexEscape_m6327_MethodInfo,
	&Uri_IsHexDigit_m6328_MethodInfo,
	&Uri_IsHexEncoding_m6329_MethodInfo,
	&Uri_AppendQueryAndFragment_m6330_MethodInfo,
	&Uri_ToString_m6331_MethodInfo,
	&Uri_EscapeString_m6332_MethodInfo,
	&Uri_EscapeString_m6333_MethodInfo,
	&Uri_ParseUri_m6334_MethodInfo,
	&Uri_Unescape_m6335_MethodInfo,
	&Uri_Unescape_m6336_MethodInfo,
	&Uri_ParseAsWindowsUNC_m6337_MethodInfo,
	&Uri_ParseAsWindowsAbsoluteFilePath_m6338_MethodInfo,
	&Uri_ParseAsUnixAbsoluteFilePath_m6339_MethodInfo,
	&Uri_Parse_m6340_MethodInfo,
	&Uri_ParseNoExceptions_m6341_MethodInfo,
	&Uri_CompactEscaped_m6342_MethodInfo,
	&Uri_Reduce_m6343_MethodInfo,
	&Uri_HexUnescapeMultiByte_m6344_MethodInfo,
	&Uri_GetSchemeDelimiter_m6345_MethodInfo,
	&Uri_GetDefaultPort_m6346_MethodInfo,
	&Uri_GetOpaqueWiseSchemeDelimiter_m6347_MethodInfo,
	&Uri_IsPredefinedScheme_m6348_MethodInfo,
	&Uri_get_Parser_m6349_MethodInfo,
	&Uri_EnsureAbsoluteUri_m6350_MethodInfo,
	&Uri_op_Equality_m6351_MethodInfo,
	NULL
};
extern const MethodInfo Uri_get_AbsoluteUri_m6309_MethodInfo;
static const PropertyInfo Uri_t926____AbsoluteUri_PropertyInfo = 
{
	&Uri_t926_il2cpp_TypeInfo/* parent */
	, "AbsoluteUri"/* name */
	, &Uri_get_AbsoluteUri_m6309_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Authority_m6310_MethodInfo;
static const PropertyInfo Uri_t926____Authority_PropertyInfo = 
{
	&Uri_t926_il2cpp_TypeInfo/* parent */
	, "Authority"/* name */
	, &Uri_get_Authority_m6310_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Host_m6311_MethodInfo;
static const PropertyInfo Uri_t926____Host_PropertyInfo = 
{
	&Uri_t926_il2cpp_TypeInfo/* parent */
	, "Host"/* name */
	, &Uri_get_Host_m6311_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsFile_m6312_MethodInfo;
static const PropertyInfo Uri_t926____IsFile_PropertyInfo = 
{
	&Uri_t926_il2cpp_TypeInfo/* parent */
	, "IsFile"/* name */
	, &Uri_get_IsFile_m6312_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsLoopback_m6313_MethodInfo;
static const PropertyInfo Uri_t926____IsLoopback_PropertyInfo = 
{
	&Uri_t926_il2cpp_TypeInfo/* parent */
	, "IsLoopback"/* name */
	, &Uri_get_IsLoopback_m6313_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsUnc_m6314_MethodInfo;
static const PropertyInfo Uri_t926____IsUnc_PropertyInfo = 
{
	&Uri_t926_il2cpp_TypeInfo/* parent */
	, "IsUnc"/* name */
	, &Uri_get_IsUnc_m6314_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Scheme_m6315_MethodInfo;
static const PropertyInfo Uri_t926____Scheme_PropertyInfo = 
{
	&Uri_t926_il2cpp_TypeInfo/* parent */
	, "Scheme"/* name */
	, &Uri_get_Scheme_m6315_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsAbsoluteUri_m6316_MethodInfo;
static const PropertyInfo Uri_t926____IsAbsoluteUri_PropertyInfo = 
{
	&Uri_t926_il2cpp_TypeInfo/* parent */
	, "IsAbsoluteUri"/* name */
	, &Uri_get_IsAbsoluteUri_m6316_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Parser_m6349_MethodInfo;
static const PropertyInfo Uri_t926____Parser_PropertyInfo = 
{
	&Uri_t926_il2cpp_TypeInfo/* parent */
	, "Parser"/* name */
	, &Uri_get_Parser_m6349_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Uri_t926_PropertyInfos[] =
{
	&Uri_t926____AbsoluteUri_PropertyInfo,
	&Uri_t926____Authority_PropertyInfo,
	&Uri_t926____Host_PropertyInfo,
	&Uri_t926____IsFile_PropertyInfo,
	&Uri_t926____IsLoopback_PropertyInfo,
	&Uri_t926____IsUnc_PropertyInfo,
	&Uri_t926____Scheme_PropertyInfo,
	&Uri_t926____IsAbsoluteUri_PropertyInfo,
	&Uri_t926____Parser_PropertyInfo,
	NULL
};
static const Il2CppType* Uri_t926_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UriScheme_t1418_0_0_0,
};
extern const MethodInfo Uri_Equals_m6322_MethodInfo;
extern const MethodInfo Uri_GetHashCode_m6324_MethodInfo;
extern const MethodInfo Uri_ToString_m6331_MethodInfo;
extern const MethodInfo Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m6307_MethodInfo;
extern const MethodInfo Uri_Unescape_m6335_MethodInfo;
static const Il2CppMethodReference Uri_t926_VTable[] =
{
	&Uri_Equals_m6322_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Uri_GetHashCode_m6324_MethodInfo,
	&Uri_ToString_m6331_MethodInfo,
	&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m6307_MethodInfo,
	&Uri_Unescape_m6335_MethodInfo,
};
static bool Uri_t926_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Uri_t926_InterfacesTypeInfos[] = 
{
	&ISerializable_t439_0_0_0,
};
static Il2CppInterfaceOffsetPair Uri_t926_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Uri_t926_1_0_0;
struct Uri_t926;
const Il2CppTypeDefinitionMetadata Uri_t926_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Uri_t926_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Uri_t926_InterfacesTypeInfos/* implementedInterfaces */
	, Uri_t926_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Uri_t926_VTable/* vtableMethods */
	, Uri_t926_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 745/* fieldStart */

};
TypeInfo Uri_t926_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Uri"/* name */
	, "System"/* namespaze */
	, Uri_t926_MethodInfos/* methods */
	, Uri_t926_PropertyInfos/* properties */
	, NULL/* events */
	, &Uri_t926_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 64/* custom_attributes_cache */
	, &Uri_t926_0_0_0/* byval_arg */
	, &Uri_t926_1_0_0/* this_arg */
	, &Uri_t926_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Uri_t926)/* instance_size */
	, sizeof (Uri_t926)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Uri_t926_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 50/* method_count */
	, 9/* property_count */
	, 38/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.UriFormatException
#include "System_System_UriFormatException.h"
// Metadata Definition System.UriFormatException
extern TypeInfo UriFormatException_t1420_il2cpp_TypeInfo;
// System.UriFormatException
#include "System_System_UriFormatExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor()
extern const MethodInfo UriFormatException__ctor_m6352_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m6352/* method */
	, &UriFormatException_t1420_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UriFormatException_t1420_UriFormatException__ctor_m6353_ParameterInfos[] = 
{
	{"message", 0, 134218673, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor(System.String)
extern const MethodInfo UriFormatException__ctor_m6353_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m6353/* method */
	, &UriFormatException_t1420_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UriFormatException_t1420_UriFormatException__ctor_m6353_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo UriFormatException_t1420_UriFormatException__ctor_m6354_ParameterInfos[] = 
{
	{"info", 0, 134218674, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134218675, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UriFormatException__ctor_m6354_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m6354/* method */
	, &UriFormatException_t1420_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, UriFormatException_t1420_UriFormatException__ctor_m6354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo UriFormatException_t1420_UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m6355_ParameterInfos[] = 
{
	{"info", 0, 134218676, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134218677, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m6355_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m6355/* method */
	, &UriFormatException_t1420_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, UriFormatException_t1420_UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m6355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UriFormatException_t1420_MethodInfos[] =
{
	&UriFormatException__ctor_m6352_MethodInfo,
	&UriFormatException__ctor_m6353_MethodInfo,
	&UriFormatException__ctor_m6354_MethodInfo,
	&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m6355_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m5385_MethodInfo;
extern const MethodInfo UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m6355_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m5387_MethodInfo;
extern const MethodInfo Exception_get_Message_m5388_MethodInfo;
extern const MethodInfo Exception_get_Source_m5389_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m5390_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m5386_MethodInfo;
extern const MethodInfo Exception_GetType_m5391_MethodInfo;
static const Il2CppMethodReference UriFormatException_t1420_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m6355_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool UriFormatException_t1420_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UriFormatException_t1420_InterfacesTypeInfos[] = 
{
	&ISerializable_t439_0_0_0,
};
extern const Il2CppType _Exception_t1147_0_0_0;
static Il2CppInterfaceOffsetPair UriFormatException_t1420_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriFormatException_t1420_0_0_0;
extern const Il2CppType UriFormatException_t1420_1_0_0;
extern const Il2CppType FormatException_t1062_0_0_0;
struct UriFormatException_t1420;
const Il2CppTypeDefinitionMetadata UriFormatException_t1420_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UriFormatException_t1420_InterfacesTypeInfos/* implementedInterfaces */
	, UriFormatException_t1420_InterfacesOffsets/* interfaceOffsets */
	, &FormatException_t1062_0_0_0/* parent */
	, UriFormatException_t1420_VTable/* vtableMethods */
	, UriFormatException_t1420_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UriFormatException_t1420_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriFormatException"/* name */
	, "System"/* namespaze */
	, UriFormatException_t1420_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UriFormatException_t1420_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriFormatException_t1420_0_0_0/* byval_arg */
	, &UriFormatException_t1420_1_0_0/* this_arg */
	, &UriFormatException_t1420_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriFormatException_t1420)/* instance_size */
	, sizeof (UriFormatException_t1420)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
// Metadata Definition System.UriHostNameType
extern TypeInfo UriHostNameType_t1421_il2cpp_TypeInfo;
// System.UriHostNameType
#include "System_System_UriHostNameTypeMethodDeclarations.h"
static const MethodInfo* UriHostNameType_t1421_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriHostNameType_t1421_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool UriHostNameType_t1421_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriHostNameType_t1421_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriHostNameType_t1421_1_0_0;
const Il2CppTypeDefinitionMetadata UriHostNameType_t1421_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriHostNameType_t1421_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, UriHostNameType_t1421_VTable/* vtableMethods */
	, UriHostNameType_t1421_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 783/* fieldStart */

};
TypeInfo UriHostNameType_t1421_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriHostNameType"/* name */
	, "System"/* namespaze */
	, UriHostNameType_t1421_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriHostNameType_t1421_0_0_0/* byval_arg */
	, &UriHostNameType_t1421_1_0_0/* this_arg */
	, &UriHostNameType_t1421_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriHostNameType_t1421)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriHostNameType_t1421)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriKind
#include "System_System_UriKind.h"
// Metadata Definition System.UriKind
extern TypeInfo UriKind_t1422_il2cpp_TypeInfo;
// System.UriKind
#include "System_System_UriKindMethodDeclarations.h"
static const MethodInfo* UriKind_t1422_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriKind_t1422_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool UriKind_t1422_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriKind_t1422_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriKind_t1422_1_0_0;
const Il2CppTypeDefinitionMetadata UriKind_t1422_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriKind_t1422_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, UriKind_t1422_VTable/* vtableMethods */
	, UriKind_t1422_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 789/* fieldStart */

};
TypeInfo UriKind_t1422_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriKind"/* name */
	, "System"/* namespaze */
	, UriKind_t1422_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriKind_t1422_0_0_0/* byval_arg */
	, &UriKind_t1422_1_0_0/* this_arg */
	, &UriKind_t1422_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriKind_t1422)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriKind_t1422)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriParser
#include "System_System_UriParser.h"
// Metadata Definition System.UriParser
extern TypeInfo UriParser_t1416_il2cpp_TypeInfo;
// System.UriParser
#include "System_System_UriParserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::.ctor()
extern const MethodInfo UriParser__ctor_m6356_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriParser__ctor_m6356/* method */
	, &UriParser_t1416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 955/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::.cctor()
extern const MethodInfo UriParser__cctor_m6357_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&UriParser__cctor_m6357/* method */
	, &UriParser_t1416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 956/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t926_0_0_0;
extern const Il2CppType UriFormatException_t1420_1_0_2;
static const ParameterInfo UriParser_t1416_UriParser_InitializeAndValidate_m6358_ParameterInfos[] = 
{
	{"uri", 0, 134218678, 0, &Uri_t926_0_0_0},
	{"parsingError", 1, 134218679, 0, &UriFormatException_t1420_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_UriFormatExceptionU26_t1493 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::InitializeAndValidate(System.Uri,System.UriFormatException&)
extern const MethodInfo UriParser_InitializeAndValidate_m6358_MethodInfo = 
{
	"InitializeAndValidate"/* name */
	, (methodPointerType)&UriParser_InitializeAndValidate_m6358/* method */
	, &UriParser_t1416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_UriFormatExceptionU26_t1493/* invoker_method */
	, UriParser_t1416_UriParser_InitializeAndValidate_m6358_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 453/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 957/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo UriParser_t1416_UriParser_OnRegister_m6359_ParameterInfos[] = 
{
	{"schemeName", 0, 134218680, 0, &String_t_0_0_0},
	{"defaultPort", 1, 134218681, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::OnRegister(System.String,System.Int32)
extern const MethodInfo UriParser_OnRegister_m6359_MethodInfo = 
{
	"OnRegister"/* name */
	, (methodPointerType)&UriParser_OnRegister_m6359/* method */
	, &UriParser_t1416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, UriParser_t1416_UriParser_OnRegister_m6359_ParameterInfos/* parameters */
	, 73/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 958/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UriParser_t1416_UriParser_set_SchemeName_m6360_ParameterInfos[] = 
{
	{"value", 0, 134218682, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::set_SchemeName(System.String)
extern const MethodInfo UriParser_set_SchemeName_m6360_MethodInfo = 
{
	"set_SchemeName"/* name */
	, (methodPointerType)&UriParser_set_SchemeName_m6360/* method */
	, &UriParser_t1416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UriParser_t1416_UriParser_set_SchemeName_m6360_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 959/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.UriParser::get_DefaultPort()
extern const MethodInfo UriParser_get_DefaultPort_m6361_MethodInfo = 
{
	"get_DefaultPort"/* name */
	, (methodPointerType)&UriParser_get_DefaultPort_m6361/* method */
	, &UriParser_t1416_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 960/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo UriParser_t1416_UriParser_set_DefaultPort_m6362_ParameterInfos[] = 
{
	{"value", 0, 134218683, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::set_DefaultPort(System.Int32)
extern const MethodInfo UriParser_set_DefaultPort_m6362_MethodInfo = 
{
	"set_DefaultPort"/* name */
	, (methodPointerType)&UriParser_set_DefaultPort_m6362/* method */
	, &UriParser_t1416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, UriParser_t1416_UriParser_set_DefaultPort_m6362_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 961/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::CreateDefaults()
extern const MethodInfo UriParser_CreateDefaults_m6363_MethodInfo = 
{
	"CreateDefaults"/* name */
	, (methodPointerType)&UriParser_CreateDefaults_m6363/* method */
	, &UriParser_t1416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 962/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Hashtable_t1262_0_0_0;
extern const Il2CppType UriParser_t1416_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo UriParser_t1416_UriParser_InternalRegister_m6364_ParameterInfos[] = 
{
	{"table", 0, 134218684, 0, &Hashtable_t1262_0_0_0},
	{"uriParser", 1, 134218685, 0, &UriParser_t1416_0_0_0},
	{"schemeName", 2, 134218686, 0, &String_t_0_0_0},
	{"defaultPort", 3, 134218687, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::InternalRegister(System.Collections.Hashtable,System.UriParser,System.String,System.Int32)
extern const MethodInfo UriParser_InternalRegister_m6364_MethodInfo = 
{
	"InternalRegister"/* name */
	, (methodPointerType)&UriParser_InternalRegister_m6364/* method */
	, &UriParser_t1416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Int32_t253/* invoker_method */
	, UriParser_t1416_UriParser_InternalRegister_m6364_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 963/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UriParser_t1416_UriParser_GetParser_m6365_ParameterInfos[] = 
{
	{"schemeName", 0, 134218688, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UriParser System.UriParser::GetParser(System.String)
extern const MethodInfo UriParser_GetParser_m6365_MethodInfo = 
{
	"GetParser"/* name */
	, (methodPointerType)&UriParser_GetParser_m6365/* method */
	, &UriParser_t1416_il2cpp_TypeInfo/* declaring_type */
	, &UriParser_t1416_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UriParser_t1416_UriParser_GetParser_m6365_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 964/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UriParser_t1416_MethodInfos[] =
{
	&UriParser__ctor_m6356_MethodInfo,
	&UriParser__cctor_m6357_MethodInfo,
	&UriParser_InitializeAndValidate_m6358_MethodInfo,
	&UriParser_OnRegister_m6359_MethodInfo,
	&UriParser_set_SchemeName_m6360_MethodInfo,
	&UriParser_get_DefaultPort_m6361_MethodInfo,
	&UriParser_set_DefaultPort_m6362_MethodInfo,
	&UriParser_CreateDefaults_m6363_MethodInfo,
	&UriParser_InternalRegister_m6364_MethodInfo,
	&UriParser_GetParser_m6365_MethodInfo,
	NULL
};
extern const MethodInfo UriParser_set_SchemeName_m6360_MethodInfo;
static const PropertyInfo UriParser_t1416____SchemeName_PropertyInfo = 
{
	&UriParser_t1416_il2cpp_TypeInfo/* parent */
	, "SchemeName"/* name */
	, NULL/* get */
	, &UriParser_set_SchemeName_m6360_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo UriParser_get_DefaultPort_m6361_MethodInfo;
extern const MethodInfo UriParser_set_DefaultPort_m6362_MethodInfo;
static const PropertyInfo UriParser_t1416____DefaultPort_PropertyInfo = 
{
	&UriParser_t1416_il2cpp_TypeInfo/* parent */
	, "DefaultPort"/* name */
	, &UriParser_get_DefaultPort_m6361_MethodInfo/* get */
	, &UriParser_set_DefaultPort_m6362_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UriParser_t1416_PropertyInfos[] =
{
	&UriParser_t1416____SchemeName_PropertyInfo,
	&UriParser_t1416____DefaultPort_PropertyInfo,
	NULL
};
static const Il2CppMethodReference UriParser_t1416_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&UriParser_InitializeAndValidate_m6358_MethodInfo,
	&UriParser_OnRegister_m6359_MethodInfo,
};
static bool UriParser_t1416_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriParser_t1416_1_0_0;
struct UriParser_t1416;
const Il2CppTypeDefinitionMetadata UriParser_t1416_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UriParser_t1416_VTable/* vtableMethods */
	, UriParser_t1416_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 793/* fieldStart */

};
TypeInfo UriParser_t1416_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriParser"/* name */
	, "System"/* namespaze */
	, UriParser_t1416_MethodInfos/* methods */
	, UriParser_t1416_PropertyInfos/* properties */
	, NULL/* events */
	, &UriParser_t1416_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriParser_t1416_0_0_0/* byval_arg */
	, &UriParser_t1416_1_0_0/* this_arg */
	, &UriParser_t1416_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriParser_t1416)/* instance_size */
	, sizeof (UriParser_t1416)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(UriParser_t1416_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UriPartial
#include "System_System_UriPartial.h"
// Metadata Definition System.UriPartial
extern TypeInfo UriPartial_t1423_il2cpp_TypeInfo;
// System.UriPartial
#include "System_System_UriPartialMethodDeclarations.h"
static const MethodInfo* UriPartial_t1423_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriPartial_t1423_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool UriPartial_t1423_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriPartial_t1423_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriPartial_t1423_1_0_0;
const Il2CppTypeDefinitionMetadata UriPartial_t1423_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriPartial_t1423_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, UriPartial_t1423_VTable/* vtableMethods */
	, UriPartial_t1423_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 799/* fieldStart */

};
TypeInfo UriPartial_t1423_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriPartial"/* name */
	, "System"/* namespaze */
	, UriPartial_t1423_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriPartial_t1423_0_0_0/* byval_arg */
	, &UriPartial_t1423_1_0_0/* this_arg */
	, &UriPartial_t1423_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriPartial_t1423)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriPartial_t1423)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriTypeConverter
#include "System_System_UriTypeConverter.h"
// Metadata Definition System.UriTypeConverter
extern TypeInfo UriTypeConverter_t1424_il2cpp_TypeInfo;
// System.UriTypeConverter
#include "System_System_UriTypeConverterMethodDeclarations.h"
static const MethodInfo* UriTypeConverter_t1424_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriTypeConverter_t1424_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool UriTypeConverter_t1424_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriTypeConverter_t1424_0_0_0;
extern const Il2CppType UriTypeConverter_t1424_1_0_0;
extern const Il2CppType TypeConverter_t1276_0_0_0;
struct UriTypeConverter_t1424;
const Il2CppTypeDefinitionMetadata UriTypeConverter_t1424_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeConverter_t1276_0_0_0/* parent */
	, UriTypeConverter_t1424_VTable/* vtableMethods */
	, UriTypeConverter_t1424_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UriTypeConverter_t1424_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriTypeConverter"/* name */
	, "System"/* namespaze */
	, UriTypeConverter_t1424_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UriTypeConverter_t1424_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriTypeConverter_t1424_0_0_0/* byval_arg */
	, &UriTypeConverter_t1424_1_0_0/* this_arg */
	, &UriTypeConverter_t1424_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriTypeConverter_t1424)/* instance_size */
	, sizeof (UriTypeConverter_t1424)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.Security.RemoteCertificateValidationCallback
#include "System_System_Net_Security_RemoteCertificateValidationCallba.h"
// Metadata Definition System.Net.Security.RemoteCertificateValidationCallback
extern TypeInfo RemoteCertificateValidationCallback_t1288_il2cpp_TypeInfo;
// System.Net.Security.RemoteCertificateValidationCallback
#include "System_System_Net_Security_RemoteCertificateValidationCallbaMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1288_RemoteCertificateValidationCallback__ctor_m6366_ParameterInfos[] = 
{
	{"object", 0, 134218689, 0, &Object_t_0_0_0},
	{"method", 1, 134218690, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Net.Security.RemoteCertificateValidationCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo RemoteCertificateValidationCallback__ctor_m6366_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback__ctor_m6366/* method */
	, &RemoteCertificateValidationCallback_t1288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1288_RemoteCertificateValidationCallback__ctor_m6366_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 965/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType X509Certificate_t1323_0_0_0;
extern const Il2CppType X509Certificate_t1323_0_0_0;
extern const Il2CppType X509Chain_t1334_0_0_0;
extern const Il2CppType X509Chain_t1334_0_0_0;
extern const Il2CppType SslPolicyErrors_t1279_0_0_0;
extern const Il2CppType SslPolicyErrors_t1279_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1288_RemoteCertificateValidationCallback_Invoke_m6367_ParameterInfos[] = 
{
	{"sender", 0, 134218691, 0, &Object_t_0_0_0},
	{"certificate", 1, 134218692, 0, &X509Certificate_t1323_0_0_0},
	{"chain", 2, 134218693, 0, &X509Chain_t1334_0_0_0},
	{"sslPolicyErrors", 3, 134218694, 0, &SslPolicyErrors_t1279_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Net.Security.RemoteCertificateValidationCallback::Invoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern const MethodInfo RemoteCertificateValidationCallback_Invoke_m6367_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_Invoke_m6367/* method */
	, &RemoteCertificateValidationCallback_t1288_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t_Int32_t253/* invoker_method */
	, RemoteCertificateValidationCallback_t1288_RemoteCertificateValidationCallback_Invoke_m6367_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 966/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType X509Certificate_t1323_0_0_0;
extern const Il2CppType X509Chain_t1334_0_0_0;
extern const Il2CppType SslPolicyErrors_t1279_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1288_RemoteCertificateValidationCallback_BeginInvoke_m6368_ParameterInfos[] = 
{
	{"sender", 0, 134218695, 0, &Object_t_0_0_0},
	{"certificate", 1, 134218696, 0, &X509Certificate_t1323_0_0_0},
	{"chain", 2, 134218697, 0, &X509Chain_t1334_0_0_0},
	{"sslPolicyErrors", 3, 134218698, 0, &SslPolicyErrors_t1279_0_0_0},
	{"callback", 4, 134218699, 0, &AsyncCallback_t547_0_0_0},
	{"object", 5, 134218700, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Net.Security.RemoteCertificateValidationCallback::BeginInvoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors,System.AsyncCallback,System.Object)
extern const MethodInfo RemoteCertificateValidationCallback_BeginInvoke_m6368_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_BeginInvoke_m6368/* method */
	, &RemoteCertificateValidationCallback_t1288_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t253_Object_t_Object_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1288_RemoteCertificateValidationCallback_BeginInvoke_m6368_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 967/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1288_RemoteCertificateValidationCallback_EndInvoke_m6369_ParameterInfos[] = 
{
	{"result", 0, 134218701, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Net.Security.RemoteCertificateValidationCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo RemoteCertificateValidationCallback_EndInvoke_m6369_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_EndInvoke_m6369/* method */
	, &RemoteCertificateValidationCallback_t1288_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1288_RemoteCertificateValidationCallback_EndInvoke_m6369_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 968/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemoteCertificateValidationCallback_t1288_MethodInfos[] =
{
	&RemoteCertificateValidationCallback__ctor_m6366_MethodInfo,
	&RemoteCertificateValidationCallback_Invoke_m6367_MethodInfo,
	&RemoteCertificateValidationCallback_BeginInvoke_m6368_MethodInfo,
	&RemoteCertificateValidationCallback_EndInvoke_m6369_MethodInfo,
	NULL
};
extern const MethodInfo RemoteCertificateValidationCallback_Invoke_m6367_MethodInfo;
extern const MethodInfo RemoteCertificateValidationCallback_BeginInvoke_m6368_MethodInfo;
extern const MethodInfo RemoteCertificateValidationCallback_EndInvoke_m6369_MethodInfo;
static const Il2CppMethodReference RemoteCertificateValidationCallback_t1288_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&RemoteCertificateValidationCallback_Invoke_m6367_MethodInfo,
	&RemoteCertificateValidationCallback_BeginInvoke_m6368_MethodInfo,
	&RemoteCertificateValidationCallback_EndInvoke_m6369_MethodInfo,
};
static bool RemoteCertificateValidationCallback_t1288_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RemoteCertificateValidationCallback_t1288_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RemoteCertificateValidationCallback_t1288_0_0_0;
extern const Il2CppType RemoteCertificateValidationCallback_t1288_1_0_0;
struct RemoteCertificateValidationCallback_t1288;
const Il2CppTypeDefinitionMetadata RemoteCertificateValidationCallback_t1288_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RemoteCertificateValidationCallback_t1288_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, RemoteCertificateValidationCallback_t1288_VTable/* vtableMethods */
	, RemoteCertificateValidationCallback_t1288_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemoteCertificateValidationCallback_t1288_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemoteCertificateValidationCallback"/* name */
	, "System.Net.Security"/* namespaze */
	, RemoteCertificateValidationCallback_t1288_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemoteCertificateValidationCallback_t1288_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemoteCertificateValidationCallback_t1288_0_0_0/* byval_arg */
	, &RemoteCertificateValidationCallback_t1288_1_0_0/* this_arg */
	, &RemoteCertificateValidationCallback_t1288_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t1288/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemoteCertificateValidationCallback_t1288)/* instance_size */
	, sizeof (RemoteCertificateValidationCallback_t1288)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.MatchEvaluator
#include "System_System_Text_RegularExpressions_MatchEvaluator.h"
// Metadata Definition System.Text.RegularExpressions.MatchEvaluator
extern TypeInfo MatchEvaluator_t1425_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MatchEvaluator
#include "System_System_Text_RegularExpressions_MatchEvaluatorMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MatchEvaluator_t1425_MatchEvaluator__ctor_m6370_ParameterInfos[] = 
{
	{"object", 0, 134218702, 0, &Object_t_0_0_0},
	{"method", 1, 134218703, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MatchEvaluator::.ctor(System.Object,System.IntPtr)
extern const MethodInfo MatchEvaluator__ctor_m6370_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MatchEvaluator__ctor_m6370/* method */
	, &MatchEvaluator_t1425_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, MatchEvaluator_t1425_MatchEvaluator__ctor_m6370_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 969/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t422_0_0_0;
static const ParameterInfo MatchEvaluator_t1425_MatchEvaluator_Invoke_m6371_ParameterInfos[] = 
{
	{"match", 0, 134218704, 0, &Match_t422_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.MatchEvaluator::Invoke(System.Text.RegularExpressions.Match)
extern const MethodInfo MatchEvaluator_Invoke_m6371_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MatchEvaluator_Invoke_m6371/* method */
	, &MatchEvaluator_t1425_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MatchEvaluator_t1425_MatchEvaluator_Invoke_m6371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 970/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t422_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MatchEvaluator_t1425_MatchEvaluator_BeginInvoke_m6372_ParameterInfos[] = 
{
	{"match", 0, 134218705, 0, &Match_t422_0_0_0},
	{"callback", 1, 134218706, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134218707, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Text.RegularExpressions.MatchEvaluator::BeginInvoke(System.Text.RegularExpressions.Match,System.AsyncCallback,System.Object)
extern const MethodInfo MatchEvaluator_BeginInvoke_m6372_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&MatchEvaluator_BeginInvoke_m6372/* method */
	, &MatchEvaluator_t1425_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MatchEvaluator_t1425_MatchEvaluator_BeginInvoke_m6372_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 971/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo MatchEvaluator_t1425_MatchEvaluator_EndInvoke_m6373_ParameterInfos[] = 
{
	{"result", 0, 134218708, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.MatchEvaluator::EndInvoke(System.IAsyncResult)
extern const MethodInfo MatchEvaluator_EndInvoke_m6373_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&MatchEvaluator_EndInvoke_m6373/* method */
	, &MatchEvaluator_t1425_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MatchEvaluator_t1425_MatchEvaluator_EndInvoke_m6373_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 972/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MatchEvaluator_t1425_MethodInfos[] =
{
	&MatchEvaluator__ctor_m6370_MethodInfo,
	&MatchEvaluator_Invoke_m6371_MethodInfo,
	&MatchEvaluator_BeginInvoke_m6372_MethodInfo,
	&MatchEvaluator_EndInvoke_m6373_MethodInfo,
	NULL
};
extern const MethodInfo MatchEvaluator_Invoke_m6371_MethodInfo;
extern const MethodInfo MatchEvaluator_BeginInvoke_m6372_MethodInfo;
extern const MethodInfo MatchEvaluator_EndInvoke_m6373_MethodInfo;
static const Il2CppMethodReference MatchEvaluator_t1425_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&MatchEvaluator_Invoke_m6371_MethodInfo,
	&MatchEvaluator_BeginInvoke_m6372_MethodInfo,
	&MatchEvaluator_EndInvoke_m6373_MethodInfo,
};
static bool MatchEvaluator_t1425_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MatchEvaluator_t1425_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MatchEvaluator_t1425_0_0_0;
extern const Il2CppType MatchEvaluator_t1425_1_0_0;
struct MatchEvaluator_t1425;
const Il2CppTypeDefinitionMetadata MatchEvaluator_t1425_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MatchEvaluator_t1425_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, MatchEvaluator_t1425_VTable/* vtableMethods */
	, MatchEvaluator_t1425_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MatchEvaluator_t1425_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchEvaluator"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, MatchEvaluator_t1425_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MatchEvaluator_t1425_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchEvaluator_t1425_0_0_0/* byval_arg */
	, &MatchEvaluator_t1425_1_0_0/* this_arg */
	, &MatchEvaluator_t1425_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MatchEvaluator_t1425/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchEvaluator_t1425)/* instance_size */
	, sizeof (MatchEvaluator_t1425)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
extern TypeInfo U24ArrayTypeU24128_t1426_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24128_t1426_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24128_t1426_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU24128_t1426_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U24ArrayTypeU24128_t1426_0_0_0;
extern const Il2CppType U24ArrayTypeU24128_t1426_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1428_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1428_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24128_t1426_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1428_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU24128_t1426_VTable/* vtableMethods */
	, U24ArrayTypeU24128_t1426_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24128_t1426_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$128"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24128_t1426_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24128_t1426_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24128_t1426_0_0_0/* byval_arg */
	, &U24ArrayTypeU24128_t1426_1_0_0/* this_arg */
	, &U24ArrayTypeU24128_t1426_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24128_t1426_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t1426_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t1426_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24128_t1426)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24128_t1426)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24128_t1426_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t1427_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2412_t1427_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2412_t1427_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2412_t1427_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t1427_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t1427_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t1427_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1428_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2412_t1427_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t1427_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2412_t1427_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t1427_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2412_t1427_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t1427_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t1427_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t1427_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t1427_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1427_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1427_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t1427)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t1427)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t1427_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "System_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "System_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t1428_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1428_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U24ArrayTypeU24128_t1426_0_0_0,
	&U24ArrayTypeU2412_t1427_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t1428_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t1428_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1428_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1428;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1428_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1428_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1428_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t1428_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 804/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1428_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1428_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t1428_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 74/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1428_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1428_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1428_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1428)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1428)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1428_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
