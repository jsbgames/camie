﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Rigidbody>
struct List_1_t142;
// System.Object
struct Object_t;
// UnityEngine.Rigidbody
struct Rigidbody_t14;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Rigidbody>
struct IEnumerable_1_t3531;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Rigidbody>
struct IEnumerator_1_t3532;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>
struct ICollection_1_t3533;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>
struct ReadOnlyCollection_1_t2867;
// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t2865;
// System.Predicate`1<UnityEngine.Rigidbody>
struct Predicate_1_t2868;
// System.Comparison`1<UnityEngine.Rigidbody>
struct Comparison_1_t2869;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m906(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m14163(__this, ___collection, method) (( void (*) (List_1_t142 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::.ctor(System.Int32)
#define List_1__ctor_m14164(__this, ___capacity, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::.cctor()
#define List_1__cctor_m14165(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14166(__this, method) (( Object_t* (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m14167(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t142 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m14168(__this, method) (( Object_t * (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m14169(__this, ___item, method) (( int32_t (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m14170(__this, ___item, method) (( bool (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m14171(__this, ___item, method) (( int32_t (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m14172(__this, ___index, ___item, method) (( void (*) (List_1_t142 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m14173(__this, ___item, method) (( void (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14174(__this, method) (( bool (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14175(__this, method) (( bool (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m14176(__this, method) (( Object_t * (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m14177(__this, method) (( bool (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m14178(__this, method) (( bool (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m14179(__this, ___index, method) (( Object_t * (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m14180(__this, ___index, ___value, method) (( void (*) (List_1_t142 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Add(T)
#define List_1_Add_m14181(__this, ___item, method) (( void (*) (List_1_t142 *, Rigidbody_t14 *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m14182(__this, ___newCount, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m14183(__this, ___collection, method) (( void (*) (List_1_t142 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m14184(__this, ___enumerable, method) (( void (*) (List_1_t142 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m14185(__this, ___collection, method) (( void (*) (List_1_t142 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody>::AsReadOnly()
#define List_1_AsReadOnly_m14186(__this, method) (( ReadOnlyCollection_1_t2867 * (*) (List_1_t142 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Clear()
#define List_1_Clear_m14187(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Contains(T)
#define List_1_Contains_m14188(__this, ___item, method) (( bool (*) (List_1_t142 *, Rigidbody_t14 *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m14189(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t142 *, RigidbodyU5BU5D_t2865*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Find(System.Predicate`1<T>)
#define List_1_Find_m14190(__this, ___match, method) (( Rigidbody_t14 * (*) (List_1_t142 *, Predicate_1_t2868 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m14191(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2868 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m14192(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t142 *, int32_t, int32_t, Predicate_1_t2868 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody>::GetEnumerator()
#define List_1_GetEnumerator_m907(__this, method) (( Enumerator_t145  (*) (List_1_t142 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody>::IndexOf(T)
#define List_1_IndexOf_m14193(__this, ___item, method) (( int32_t (*) (List_1_t142 *, Rigidbody_t14 *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m14194(__this, ___start, ___delta, method) (( void (*) (List_1_t142 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m14195(__this, ___index, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Insert(System.Int32,T)
#define List_1_Insert_m14196(__this, ___index, ___item, method) (( void (*) (List_1_t142 *, int32_t, Rigidbody_t14 *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m14197(__this, ___collection, method) (( void (*) (List_1_t142 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Remove(T)
#define List_1_Remove_m14198(__this, ___item, method) (( bool (*) (List_1_t142 *, Rigidbody_t14 *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m14199(__this, ___match, method) (( int32_t (*) (List_1_t142 *, Predicate_1_t2868 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m14200(__this, ___index, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Reverse()
#define List_1_Reverse_m14201(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Sort()
#define List_1_Sort_m14202(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m14203(__this, ___comparison, method) (( void (*) (List_1_t142 *, Comparison_1_t2869 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Rigidbody>::ToArray()
#define List_1_ToArray_m14204(__this, method) (( RigidbodyU5BU5D_t2865* (*) (List_1_t142 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::TrimExcess()
#define List_1_TrimExcess_m14205(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody>::get_Capacity()
#define List_1_get_Capacity_m14206(__this, method) (( int32_t (*) (List_1_t142 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m14207(__this, ___value, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody>::get_Count()
#define List_1_get_Count_m14208(__this, method) (( int32_t (*) (List_1_t142 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Rigidbody>::get_Item(System.Int32)
#define List_1_get_Item_m14209(__this, ___index, method) (( Rigidbody_t14 * (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::set_Item(System.Int32,T)
#define List_1_set_Item_m14210(__this, ___index, ___value, method) (( void (*) (List_1_t142 *, int32_t, Rigidbody_t14 *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
