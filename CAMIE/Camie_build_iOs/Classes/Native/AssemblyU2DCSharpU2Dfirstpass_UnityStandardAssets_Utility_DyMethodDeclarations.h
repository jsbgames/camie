﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.DynamicShadowSettings
struct DynamicShadowSettings_t177;

// System.Void UnityStandardAssets.Utility.DynamicShadowSettings::.ctor()
extern "C" void DynamicShadowSettings__ctor_m470 (DynamicShadowSettings_t177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Start()
extern "C" void DynamicShadowSettings_Start_m471 (DynamicShadowSettings_t177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Update()
extern "C" void DynamicShadowSettings_Update_m472 (DynamicShadowSettings_t177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
