﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Gradient
struct Gradient_t811;
struct Gradient_t811_marshaled;

// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m3814 (Gradient_t811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m3815 (Gradient_t811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m3816 (Gradient_t811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m3817 (Gradient_t811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void Gradient_t811_marshal(const Gradient_t811& unmarshaled, Gradient_t811_marshaled& marshaled);
void Gradient_t811_marshal_back(const Gradient_t811_marshaled& marshaled, Gradient_t811& unmarshaled);
void Gradient_t811_marshal_cleanup(Gradient_t811_marshaled& marshaled);
