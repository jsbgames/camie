﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t224;
// UnityEngine.Events.InvokableCall`1<System.Int32>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_4.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct  CachedInvokableCall_1_t1104  : public InvokableCall_1_t3386
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<System.Int32>::m_Arg1
	ObjectU5BU5D_t224* ___m_Arg1_1;
};
