﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t418;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.Graphic>
struct  Comparison_1_t527  : public MulticastDelegate_t549
{
};
