﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t3027;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<System.Object>
struct  InvokableCall_1_t3049  : public BaseInvokableCall_t1002
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.Object>::Delegate
	UnityAction_1_t3027 * ___Delegate_0;
};
