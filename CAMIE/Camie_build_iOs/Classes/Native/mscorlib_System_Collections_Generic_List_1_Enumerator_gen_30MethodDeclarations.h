﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>
struct Enumerator_t3226;
// System.Object
struct Object_t;
// UnityEngine.ParticleSystem
struct ParticleSystem_t161;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t1024;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m19466(__this, ___l, method) (( void (*) (Enumerator_t3226 *, List_1_t1024 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19467(__this, method) (( Object_t * (*) (Enumerator_t3226 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::Dispose()
#define Enumerator_Dispose_m19468(__this, method) (( void (*) (Enumerator_t3226 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::VerifyState()
#define Enumerator_VerifyState_m19469(__this, method) (( void (*) (Enumerator_t3226 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::MoveNext()
#define Enumerator_MoveNext_m19470(__this, method) (( bool (*) (Enumerator_t3226 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::get_Current()
#define Enumerator_get_Current_m19471(__this, method) (( ParticleSystem_t161 * (*) (Enumerator_t3226 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
