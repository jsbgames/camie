﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Enumerator_t3343;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t3340;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21144_gshared (Enumerator_t3343 * __this, Dictionary_2_t3340 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m21144(__this, ___dictionary, method) (( void (*) (Enumerator_t3343 *, Dictionary_2_t3340 *, const MethodInfo*))Enumerator__ctor_m21144_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21145_gshared (Enumerator_t3343 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21145(__this, method) (( Object_t * (*) (Enumerator_t3343 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21145_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1430  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21146_gshared (Enumerator_t3343 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21146(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3343 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21146_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21147_gshared (Enumerator_t3343 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21147(__this, method) (( Object_t * (*) (Enumerator_t3343 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21147_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21148_gshared (Enumerator_t3343 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21148(__this, method) (( Object_t * (*) (Enumerator_t3343 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21148_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21149_gshared (Enumerator_t3343 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21149(__this, method) (( bool (*) (Enumerator_t3343 *, const MethodInfo*))Enumerator_MoveNext_m21149_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t3314  Enumerator_get_Current_m21150_gshared (Enumerator_t3343 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21150(__this, method) (( KeyValuePair_2_t3314  (*) (Enumerator_t3343 *, const MethodInfo*))Enumerator_get_Current_m21150_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m21151_gshared (Enumerator_t3343 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m21151(__this, method) (( Object_t * (*) (Enumerator_t3343 *, const MethodInfo*))Enumerator_get_CurrentKey_m21151_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentValue()
extern "C" KeyValuePair_2_t2815  Enumerator_get_CurrentValue_m21152_gshared (Enumerator_t3343 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m21152(__this, method) (( KeyValuePair_2_t2815  (*) (Enumerator_t3343 *, const MethodInfo*))Enumerator_get_CurrentValue_m21152_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern "C" void Enumerator_VerifyState_m21153_gshared (Enumerator_t3343 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m21153(__this, method) (( void (*) (Enumerator_t3343 *, const MethodInfo*))Enumerator_VerifyState_m21153_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m21154_gshared (Enumerator_t3343 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m21154(__this, method) (( void (*) (Enumerator_t3343 *, const MethodInfo*))Enumerator_VerifyCurrent_m21154_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void Enumerator_Dispose_m21155_gshared (Enumerator_t3343 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21155(__this, method) (( void (*) (Enumerator_t3343 *, const MethodInfo*))Enumerator_Dispose_m21155_gshared)(__this, method)
