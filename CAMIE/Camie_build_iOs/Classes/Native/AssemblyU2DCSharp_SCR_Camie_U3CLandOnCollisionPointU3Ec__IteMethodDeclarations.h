﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Camie/<LandOnCollisionPoint>c__Iterator5
struct U3CLandOnCollisionPointU3Ec__Iterator5_t340;
// System.Object
struct Object_t;

// System.Void SCR_Camie/<LandOnCollisionPoint>c__Iterator5::.ctor()
extern "C" void U3CLandOnCollisionPointU3Ec__Iterator5__ctor_m1230 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Camie/<LandOnCollisionPoint>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1231 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Camie/<LandOnCollisionPoint>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1232 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Camie/<LandOnCollisionPoint>c__Iterator5::MoveNext()
extern "C" bool U3CLandOnCollisionPointU3Ec__Iterator5_MoveNext_m1233 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie/<LandOnCollisionPoint>c__Iterator5::Dispose()
extern "C" void U3CLandOnCollisionPointU3Ec__Iterator5_Dispose_m1234 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie/<LandOnCollisionPoint>c__Iterator5::Reset()
extern "C" void U3CLandOnCollisionPointU3Ec__Iterator5_Reset_m1235 (U3CLandOnCollisionPointU3Ec__Iterator5_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
