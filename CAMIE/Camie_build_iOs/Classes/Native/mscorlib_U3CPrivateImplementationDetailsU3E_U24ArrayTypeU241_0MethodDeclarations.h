﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$120
struct U24ArrayTypeU24120_t2257;
struct U24ArrayTypeU24120_t2257_marshaled;

void U24ArrayTypeU24120_t2257_marshal(const U24ArrayTypeU24120_t2257& unmarshaled, U24ArrayTypeU24120_t2257_marshaled& marshaled);
void U24ArrayTypeU24120_t2257_marshal_back(const U24ArrayTypeU24120_t2257_marshaled& marshaled, U24ArrayTypeU24120_t2257& unmarshaled);
void U24ArrayTypeU24120_t2257_marshal_cleanup(U24ArrayTypeU24120_t2257_marshaled& marshaled);
