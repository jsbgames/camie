﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Camie/<Jump>c__Iterator6
struct U3CJumpU3Ec__Iterator6_t341;
// System.Object
struct Object_t;

// System.Void SCR_Camie/<Jump>c__Iterator6::.ctor()
extern "C" void U3CJumpU3Ec__Iterator6__ctor_m1236 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Camie/<Jump>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CJumpU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1237 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Camie/<Jump>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CJumpU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1238 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Camie/<Jump>c__Iterator6::MoveNext()
extern "C" bool U3CJumpU3Ec__Iterator6_MoveNext_m1239 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie/<Jump>c__Iterator6::Dispose()
extern "C" void U3CJumpU3Ec__Iterator6_Dispose_m1240 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie/<Jump>c__Iterator6::Reset()
extern "C" void U3CJumpU3Ec__Iterator6_Reset_m1241 (U3CJumpU3Ec__Iterator6_t341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
