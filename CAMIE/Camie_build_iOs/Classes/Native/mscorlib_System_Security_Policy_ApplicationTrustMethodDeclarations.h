﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Policy.ApplicationTrust
struct ApplicationTrust_t2113;

// System.Void System.Security.Policy.ApplicationTrust::.ctor()
extern "C" void ApplicationTrust__ctor_m11323 (ApplicationTrust_t2113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
