﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Wings
struct SCR_Wings_t351;

// System.Void SCR_Wings::.ctor()
extern "C" void SCR_Wings__ctor_m1285 (SCR_Wings_t351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Wings::Awake()
extern "C" void SCR_Wings_Awake_m1286 (SCR_Wings_t351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Wings::SetVisible(System.Boolean)
extern "C" void SCR_Wings_SetVisible_m1287 (SCR_Wings_t351 * __this, bool ___isVisible, const MethodInfo* method) IL2CPP_METHOD_ATTR;
