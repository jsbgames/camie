﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Enumerator_t3342;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t3340;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21139_gshared (Enumerator_t3342 * __this, Dictionary_2_t3340 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m21139(__this, ___host, method) (( void (*) (Enumerator_t3342 *, Dictionary_2_t3340 *, const MethodInfo*))Enumerator__ctor_m21139_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21140_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21140(__this, method) (( Object_t * (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21140_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void Enumerator_Dispose_m21141_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21141(__this, method) (( void (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_Dispose_m21141_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21142_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21142(__this, method) (( bool (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_MoveNext_m21142_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m21143_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21143(__this, method) (( Object_t * (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_get_Current_m21143_gshared)(__this, method)
