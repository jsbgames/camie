﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
struct ReceiveRecordAsyncResult_t1592;
// System.IO.Stream
struct Stream_t426;
// System.Byte[]
struct ByteU5BU5D_t850;
// System.Object
struct Object_t;
// System.Exception
struct Exception_t232;
// System.Threading.WaitHandle
struct WaitHandle_t1637;
// System.AsyncCallback
struct AsyncCallback_t547;

// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::.ctor(System.AsyncCallback,System.Object,System.Byte[],System.IO.Stream)
extern "C" void ReceiveRecordAsyncResult__ctor_m7168 (ReceiveRecordAsyncResult_t1592 * __this, AsyncCallback_t547 * ___userCallback, Object_t * ___userState, ByteU5BU5D_t850* ___initialBuffer, Stream_t426 * ___record, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_Record()
extern "C" Stream_t426 * ReceiveRecordAsyncResult_get_Record_m7169 (ReceiveRecordAsyncResult_t1592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_ResultingBuffer()
extern "C" ByteU5BU5D_t850* ReceiveRecordAsyncResult_get_ResultingBuffer_m7170 (ReceiveRecordAsyncResult_t1592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_InitialBuffer()
extern "C" ByteU5BU5D_t850* ReceiveRecordAsyncResult_get_InitialBuffer_m7171 (ReceiveRecordAsyncResult_t1592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncState()
extern "C" Object_t * ReceiveRecordAsyncResult_get_AsyncState_m7172 (ReceiveRecordAsyncResult_t1592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncException()
extern "C" Exception_t232 * ReceiveRecordAsyncResult_get_AsyncException_m7173 (ReceiveRecordAsyncResult_t1592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_CompletedWithError()
extern "C" bool ReceiveRecordAsyncResult_get_CompletedWithError_m7174 (ReceiveRecordAsyncResult_t1592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncWaitHandle()
extern "C" WaitHandle_t1637 * ReceiveRecordAsyncResult_get_AsyncWaitHandle_m7175 (ReceiveRecordAsyncResult_t1592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_IsCompleted()
extern "C" bool ReceiveRecordAsyncResult_get_IsCompleted_m7176 (ReceiveRecordAsyncResult_t1592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception,System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m7177 (ReceiveRecordAsyncResult_t1592 * __this, Exception_t232 * ___ex, ByteU5BU5D_t850* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception)
extern "C" void ReceiveRecordAsyncResult_SetComplete_m7178 (ReceiveRecordAsyncResult_t1592 * __this, Exception_t232 * ___ex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m7179 (ReceiveRecordAsyncResult_t1592 * __this, ByteU5BU5D_t850* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
