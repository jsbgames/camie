﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA
struct U3CDeactivateU3Ec__IteratorA_t203;
// System.Object
struct Object_t;

// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::.ctor()
extern "C" void U3CDeactivateU3Ec__IteratorA__ctor_m547 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDeactivateU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m548 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDeactivateU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m549 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::MoveNext()
extern "C" bool U3CDeactivateU3Ec__IteratorA_MoveNext_m550 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::Dispose()
extern "C" void U3CDeactivateU3Ec__IteratorA_Dispose_m551 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__IteratorA::Reset()
extern "C" void U3CDeactivateU3Ec__IteratorA_Reset_m552 (U3CDeactivateU3Ec__IteratorA_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
