﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$64
struct U24ArrayTypeU2464_t1631;
struct U24ArrayTypeU2464_t1631_marshaled;

void U24ArrayTypeU2464_t1631_marshal(const U24ArrayTypeU2464_t1631& unmarshaled, U24ArrayTypeU2464_t1631_marshaled& marshaled);
void U24ArrayTypeU2464_t1631_marshal_back(const U24ArrayTypeU2464_t1631_marshaled& marshaled, U24ArrayTypeU2464_t1631& unmarshaled);
void U24ArrayTypeU2464_t1631_marshal_cleanup(U24ArrayTypeU2464_t1631_marshaled& marshaled);
