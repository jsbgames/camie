﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.FOVKick
struct FOVKick_t178;
// UnityEngine.Camera
struct Camera_t27;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void UnityStandardAssets.Utility.FOVKick::.ctor()
extern "C" void FOVKick__ctor_m485 (FOVKick_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FOVKick::Setup(UnityEngine.Camera)
extern "C" void FOVKick_Setup_m486 (FOVKick_t178 * __this, Camera_t27 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FOVKick::CheckStatus(UnityEngine.Camera)
extern "C" void FOVKick_CheckStatus_m487 (FOVKick_t178 * __this, Camera_t27 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FOVKick::ChangeCamera(UnityEngine.Camera)
extern "C" void FOVKick_ChangeCamera_m488 (FOVKick_t178 * __this, Camera_t27 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickUp()
extern "C" Object_t * FOVKick_FOVKickUp_m489 (FOVKick_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickDown()
extern "C" Object_t * FOVKick_FOVKickDown_m490 (FOVKick_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
