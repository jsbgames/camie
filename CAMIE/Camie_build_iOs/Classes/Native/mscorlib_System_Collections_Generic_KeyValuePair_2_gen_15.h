﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t247;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>
struct  KeyValuePair_2_t3010 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.AudioSource>::value
	AudioSource_t247 * ___value_1;
};
