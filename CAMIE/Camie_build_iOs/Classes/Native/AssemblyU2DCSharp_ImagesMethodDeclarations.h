﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Images
struct Images_t288;

// System.Void Images::.ctor()
extern "C" void Images__ctor_m1079 (Images_t288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
