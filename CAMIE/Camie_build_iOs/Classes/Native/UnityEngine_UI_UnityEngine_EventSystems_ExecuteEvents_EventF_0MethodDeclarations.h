﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_t474;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.ISelectHandler
struct ISelectHandler_t638;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t453;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>::.ctor(System.Object,System.IntPtr)
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_16MethodDeclarations.h"
#define EventFunction_1__ctor_m3083(__this, ___object, ___method, method) (( void (*) (EventFunction_1_t474 *, Object_t *, IntPtr_t, const MethodInfo*))EventFunction_1__ctor_m16355_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
#define EventFunction_1_Invoke_m16595(__this, ___handler, ___eventData, method) (( void (*) (EventFunction_1_t474 *, Object_t *, BaseEventData_t453 *, const MethodInfo*))EventFunction_1_Invoke_m16357_gshared)(__this, ___handler, ___eventData, method)
// System.IAsyncResult UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>::BeginInvoke(T1,UnityEngine.EventSystems.BaseEventData,System.AsyncCallback,System.Object)
#define EventFunction_1_BeginInvoke_m16596(__this, ___handler, ___eventData, ___callback, ___object, method) (( Object_t * (*) (EventFunction_1_t474 *, Object_t *, BaseEventData_t453 *, AsyncCallback_t547 *, Object_t *, const MethodInfo*))EventFunction_1_BeginInvoke_m16359_gshared)(__this, ___handler, ___eventData, ___callback, ___object, method)
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>::EndInvoke(System.IAsyncResult)
#define EventFunction_1_EndInvoke_m16597(__this, ___result, method) (( void (*) (EventFunction_1_t474 *, Object_t *, const MethodInfo*))EventFunction_1_EndInvoke_m16361_gshared)(__this, ___result, method)
