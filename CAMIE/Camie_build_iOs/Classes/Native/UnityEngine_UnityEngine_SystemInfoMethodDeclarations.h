﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SystemInfo
struct SystemInfo_t782;
// System.String
struct String_t;
// UnityEngine.DeviceType
#include "UnityEngine_UnityEngine_DeviceType.h"
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"

// System.String UnityEngine.SystemInfo::get_operatingSystem()
extern "C" String_t* SystemInfo_get_operatingSystem_m1659 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_systemMemorySize()
extern "C" int32_t SystemInfo_get_systemMemorySize_m1617 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_graphicsMemorySize()
extern "C" int32_t SystemInfo_get_graphicsMemorySize_m1614 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_graphicsDeviceName()
extern "C" String_t* SystemInfo_get_graphicsDeviceName_m1657 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
extern "C" int32_t SystemInfo_get_graphicsShaderLevel_m862 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsRenderTextures()
extern "C" bool SystemInfo_get_supportsRenderTextures_m854 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
extern "C" bool SystemInfo_get_supportsImageEffects_m765 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supports3DTextures()
extern "C" bool SystemInfo_get_supports3DTextures_m811 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
extern "C" bool SystemInfo_get_supportsComputeShaders_m863 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
extern "C" bool SystemInfo_SupportsRenderTextureFormat_m781 (Object_t * __this /* static, unused */, int32_t ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
extern "C" String_t* SystemInfo_get_deviceUniqueIdentifier_m3656 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_deviceName()
extern "C" String_t* SystemInfo_get_deviceName_m1613 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_deviceModel()
extern "C" String_t* SystemInfo_get_deviceModel_m1611 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.DeviceType UnityEngine.SystemInfo::get_deviceType()
extern "C" int32_t SystemInfo_get_deviceType_m1612 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_maxTextureSize()
extern "C" int32_t SystemInfo_get_maxTextureSize_m1616 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
