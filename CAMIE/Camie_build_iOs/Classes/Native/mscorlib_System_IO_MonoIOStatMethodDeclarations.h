﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.MonoIOStat
struct MonoIOStat_t1821;
struct MonoIOStat_t1821_marshaled;

void MonoIOStat_t1821_marshal(const MonoIOStat_t1821& unmarshaled, MonoIOStat_t1821_marshaled& marshaled);
void MonoIOStat_t1821_marshal_back(const MonoIOStat_t1821_marshaled& marshaled, MonoIOStat_t1821& unmarshaled);
void MonoIOStat_t1821_marshal_cleanup(MonoIOStat_t1821_marshaled& marshaled);
