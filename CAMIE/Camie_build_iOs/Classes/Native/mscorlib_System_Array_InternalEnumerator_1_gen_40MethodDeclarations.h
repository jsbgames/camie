﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>>
struct InternalEnumerator_1_t2985;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15913_gshared (InternalEnumerator_1_t2985 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15913(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2985 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15913_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15914_gshared (InternalEnumerator_1_t2985 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15914(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2985 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15914_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15915_gshared (InternalEnumerator_1_t2985 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15915(__this, method) (( void (*) (InternalEnumerator_1_t2985 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15915_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15916_gshared (InternalEnumerator_1_t2985 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15916(__this, method) (( bool (*) (InternalEnumerator_1_t2985 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15916_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>>::get_Current()
extern "C" KeyValuePair_2_t2984  InternalEnumerator_1_get_Current_m15917_gshared (InternalEnumerator_1_t2985 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15917(__this, method) (( KeyValuePair_2_t2984  (*) (InternalEnumerator_1_t2985 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15917_gshared)(__this, method)
