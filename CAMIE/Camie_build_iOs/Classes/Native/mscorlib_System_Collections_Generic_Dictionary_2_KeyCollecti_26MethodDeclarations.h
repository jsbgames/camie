﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>
struct Enumerator_t3259;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t3254;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m19985_gshared (Enumerator_t3259 * __this, Dictionary_2_t3254 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m19985(__this, ___host, method) (( void (*) (Enumerator_t3259 *, Dictionary_2_t3254 *, const MethodInfo*))Enumerator__ctor_m19985_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m19986_gshared (Enumerator_t3259 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m19986(__this, method) (( Object_t * (*) (Enumerator_t3259 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19986_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::Dispose()
extern "C" void Enumerator_Dispose_m19987_gshared (Enumerator_t3259 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m19987(__this, method) (( void (*) (Enumerator_t3259 *, const MethodInfo*))Enumerator_Dispose_m19987_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool Enumerator_MoveNext_m19988_gshared (Enumerator_t3259 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m19988(__this, method) (( bool (*) (Enumerator_t3259 *, const MethodInfo*))Enumerator_MoveNext_m19988_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m19989_gshared (Enumerator_t3259 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m19989(__this, method) (( Object_t * (*) (Enumerator_t3259 *, const MethodInfo*))Enumerator_get_Current_m19989_gshared)(__this, method)
