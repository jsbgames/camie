﻿#pragma once
#include <stdint.h>
// System.Version
struct Version_t1292;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Resources.SatelliteContractVersionAttribute
struct  SatelliteContractVersionAttribute_t1470  : public Attribute_t805
{
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t1292 * ___ver_0;
};
