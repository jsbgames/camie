﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t878;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.AudioSettings
struct  AudioSettings_t879  : public Object_t
{
};
struct AudioSettings_t879_StaticFields{
	// UnityEngine.AudioSettings/AudioConfigurationChangeHandler UnityEngine.AudioSettings::OnAudioConfigurationChanged
	AudioConfigurationChangeHandler_t878 * ___OnAudioConfigurationChanged_0;
};
