﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.ActivateTrigger
struct ActivateTrigger_t165;
// UnityEngine.Collider
struct Collider_t138;

// System.Void UnityStandardAssets.Utility.ActivateTrigger::.ctor()
extern "C" void ActivateTrigger__ctor_m441 (ActivateTrigger_t165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.ActivateTrigger::DoActivateTrigger()
extern "C" void ActivateTrigger_DoActivateTrigger_m442 (ActivateTrigger_t165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.ActivateTrigger::OnTriggerEnter(UnityEngine.Collider)
extern "C" void ActivateTrigger_OnTriggerEnter_m443 (ActivateTrigger_t165 * __this, Collider_t138 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
