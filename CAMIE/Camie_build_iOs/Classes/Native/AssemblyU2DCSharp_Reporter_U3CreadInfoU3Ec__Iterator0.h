﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t294;
// System.Object
struct Object_t;
// Reporter
struct Reporter_t295;
// System.Object
#include "mscorlib_System_Object.h"
// Reporter/<readInfo>c__Iterator0
struct  U3CreadInfoU3Ec__Iterator0_t296  : public Object_t
{
	// System.String Reporter/<readInfo>c__Iterator0::<prefFile>__0
	String_t* ___U3CprefFileU3E__0_0;
	// System.String Reporter/<readInfo>c__Iterator0::<url>__1
	String_t* ___U3CurlU3E__1_1;
	// System.String Reporter/<readInfo>c__Iterator0::<streamingAssetsPath>__2
	String_t* ___U3CstreamingAssetsPathU3E__2_2;
	// UnityEngine.WWW Reporter/<readInfo>c__Iterator0::<www>__3
	WWW_t294 * ___U3CwwwU3E__3_3;
	// System.Int32 Reporter/<readInfo>c__Iterator0::$PC
	int32_t ___U24PC_4;
	// System.Object Reporter/<readInfo>c__Iterator0::$current
	Object_t * ___U24current_5;
	// Reporter Reporter/<readInfo>c__Iterator0::<>f__this
	Reporter_t295 * ___U3CU3Ef__this_6;
};
