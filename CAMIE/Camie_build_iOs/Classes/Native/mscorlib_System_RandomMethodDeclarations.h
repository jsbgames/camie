﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Random
struct Random_t923;

// System.Void System.Random::.ctor()
extern "C" void Random__ctor_m12454 (Random_t923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Random::.ctor(System.Int32)
extern "C" void Random__ctor_m5158 (Random_t923 * __this, int32_t ___Seed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
