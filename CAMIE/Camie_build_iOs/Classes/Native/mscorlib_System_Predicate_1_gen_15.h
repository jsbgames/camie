﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IEventSystemHandler
struct IEventSystemHandler_t281;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.IEventSystemHandler>
struct  Predicate_1_t3022  : public MulticastDelegate_t549
{
};
