﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t54;
// UnityEngine.Material
struct Material_t55;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityStandardAssets.ImageEffects.ImageEffectBase
struct  ImageEffectBase_t88  : public MonoBehaviour_t3
{
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ImageEffectBase::shader
	Shader_t54 * ___shader_2;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::m_Material
	Material_t55 * ___m_Material_3;
};
