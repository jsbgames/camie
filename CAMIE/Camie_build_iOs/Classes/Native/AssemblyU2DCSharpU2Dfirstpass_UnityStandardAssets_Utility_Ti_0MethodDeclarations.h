﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_t199;

// System.Void UnityStandardAssets.Utility.TimedObjectActivator/Entry::.ctor()
extern "C" void Entry__ctor_m539 (Entry_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
