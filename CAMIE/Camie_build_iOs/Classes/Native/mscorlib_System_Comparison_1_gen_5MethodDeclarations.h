﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.Rigidbody>
struct Comparison_1_t2869;
// System.Object
struct Object_t;
// UnityEngine.Rigidbody
struct Rigidbody_t14;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<UnityEngine.Rigidbody>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m14249(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2869 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m14080_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.Rigidbody>::Invoke(T,T)
#define Comparison_1_Invoke_m14250(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2869 *, Rigidbody_t14 *, Rigidbody_t14 *, const MethodInfo*))Comparison_1_Invoke_m14081_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Rigidbody>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m14251(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2869 *, Rigidbody_t14 *, Rigidbody_t14 *, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m14082_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.Rigidbody>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m14252(__this, ___result, method) (( int32_t (*) (Comparison_1_t2869 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m14083_gshared)(__this, ___result, method)
