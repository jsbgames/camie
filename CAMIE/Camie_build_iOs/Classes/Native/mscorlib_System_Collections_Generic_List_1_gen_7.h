﻿#pragma once
#include <stdint.h>
// SCR_Puceron[]
struct SCR_PuceronU5BU5D_t2971;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<SCR_Puceron>
struct  List_1_t387  : public Object_t
{
	// T[] System.Collections.Generic.List`1<SCR_Puceron>::_items
	SCR_PuceronU5BU5D_t2971* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<SCR_Puceron>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<SCR_Puceron>::_version
	int32_t ____version_3;
};
struct List_1_t387_StaticFields{
	// T[] System.Collections.Generic.List`1<SCR_Puceron>::EmptyArray
	SCR_PuceronU5BU5D_t2971* ___EmptyArray_4;
};
