﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>
struct InternalEnumerator_1_t3361;
// System.Object
struct Object_t;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t262;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m21280(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3361 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21281(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3361 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>::Dispose()
#define InternalEnumerator_1_Dispose_m21282(__this, method) (( void (*) (InternalEnumerator_1_t3361 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>::MoveNext()
#define InternalEnumerator_1_MoveNext_m21283(__this, method) (( bool (*) (InternalEnumerator_1_t3361 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>::get_Current()
#define InternalEnumerator_1_get_Current_m21284(__this, method) (( ExecuteInEditMode_t262 * (*) (InternalEnumerator_1_t3361 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
