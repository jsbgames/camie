﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIContent
struct GUIContent_t301;
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
// UnityEngine.GUIWordWrapSizer
struct  GUIWordWrapSizer_t822  : public GUILayoutEntry_t819
{
	// UnityEngine.GUIContent UnityEngine.GUIWordWrapSizer::content
	GUIContent_t301 * ___content_10;
	// System.Single UnityEngine.GUIWordWrapSizer::forcedMinHeight
	float ___forcedMinHeight_11;
	// System.Single UnityEngine.GUIWordWrapSizer::forcedMaxHeight
	float ___forcedMaxHeight_12;
};
