﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.PropertyAttribute
struct PropertyAttribute_t990;

// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C" void PropertyAttribute__ctor_m4963 (PropertyAttribute_t990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
