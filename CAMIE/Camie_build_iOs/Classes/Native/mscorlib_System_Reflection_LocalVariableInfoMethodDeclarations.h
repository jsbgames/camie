﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.LocalVariableInfo
struct LocalVariableInfo_t1866;
// System.String
struct String_t;

// System.Void System.Reflection.LocalVariableInfo::.ctor()
extern "C" void LocalVariableInfo__ctor_m10174 (LocalVariableInfo_t1866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.LocalVariableInfo::ToString()
extern "C" String_t* LocalVariableInfo_ToString_m10175 (LocalVariableInfo_t1866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
