﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_FollowAvatar/<ZoomInOn>c__Iterator7
struct U3CZoomInOnU3Ec__Iterator7_t353;
// System.Object
struct Object_t;

// System.Void SCR_FollowAvatar/<ZoomInOn>c__Iterator7::.ctor()
extern "C" void U3CZoomInOnU3Ec__Iterator7__ctor_m1288 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_FollowAvatar/<ZoomInOn>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CZoomInOnU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1289 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_FollowAvatar/<ZoomInOn>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CZoomInOnU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1290 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_FollowAvatar/<ZoomInOn>c__Iterator7::MoveNext()
extern "C" bool U3CZoomInOnU3Ec__Iterator7_MoveNext_m1291 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FollowAvatar/<ZoomInOn>c__Iterator7::Dispose()
extern "C" void U3CZoomInOnU3Ec__Iterator7_Dispose_m1292 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FollowAvatar/<ZoomInOn>c__Iterator7::Reset()
extern "C" void U3CZoomInOnU3Ec__Iterator7_Reset_m1293 (U3CZoomInOnU3Ec__Iterator7_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
