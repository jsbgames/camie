﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Cameras.PivotBasedCameraRig
struct PivotBasedCameraRig_t17;

// System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::.ctor()
extern "C" void PivotBasedCameraRig__ctor_m41 (PivotBasedCameraRig_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::Awake()
extern "C" void PivotBasedCameraRig_Awake_m42 (PivotBasedCameraRig_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
