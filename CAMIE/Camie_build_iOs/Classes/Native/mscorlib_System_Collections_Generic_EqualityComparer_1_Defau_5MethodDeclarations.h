﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t3238;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m19695_gshared (DefaultComparer_t3238 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m19695(__this, method) (( void (*) (DefaultComparer_t3238 *, const MethodInfo*))DefaultComparer__ctor_m19695_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m19696_gshared (DefaultComparer_t3238 * __this, UICharInfo_t689  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m19696(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3238 *, UICharInfo_t689 , const MethodInfo*))DefaultComparer_GetHashCode_m19696_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m19697_gshared (DefaultComparer_t3238 * __this, UICharInfo_t689  ___x, UICharInfo_t689  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m19697(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3238 *, UICharInfo_t689 , UICharInfo_t689 , const MethodInfo*))DefaultComparer_Equals_m19697_gshared)(__this, ___x, ___y, method)
