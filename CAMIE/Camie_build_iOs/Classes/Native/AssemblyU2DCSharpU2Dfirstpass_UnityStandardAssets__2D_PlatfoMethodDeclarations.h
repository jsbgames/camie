﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets._2D.Platformer2DUserControl
struct Platformer2DUserControl_t8;

// System.Void UnityStandardAssets._2D.Platformer2DUserControl::.ctor()
extern "C" void Platformer2DUserControl__ctor_m9 (Platformer2DUserControl_t8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets._2D.Platformer2DUserControl::Awake()
extern "C" void Platformer2DUserControl_Awake_m10 (Platformer2DUserControl_t8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets._2D.Platformer2DUserControl::Update()
extern "C" void Platformer2DUserControl_Update_m11 (Platformer2DUserControl_t8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets._2D.Platformer2DUserControl::FixedUpdate()
extern "C" void Platformer2DUserControl_FixedUpdate_m12 (Platformer2DUserControl_t8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
