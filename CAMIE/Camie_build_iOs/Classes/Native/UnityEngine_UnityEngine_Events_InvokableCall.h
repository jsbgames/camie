﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction
struct UnityAction_t416;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall
struct  InvokableCall_t1003  : public BaseInvokableCall_t1002
{
	// UnityEngine.Events.UnityAction UnityEngine.Events.InvokableCall::Delegate
	UnityAction_t416 * ___Delegate_0;
};
