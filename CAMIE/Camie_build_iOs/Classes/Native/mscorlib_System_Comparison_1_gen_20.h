﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t316;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.Text>
struct  Comparison_1_t3092  : public MulticastDelegate_t549
{
};
