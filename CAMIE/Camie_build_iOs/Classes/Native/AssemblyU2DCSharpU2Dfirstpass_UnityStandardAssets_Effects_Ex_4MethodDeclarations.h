﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.Explosive
struct Explosive_t147;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.Collision
struct Collision_t146;

// System.Void UnityStandardAssets.Effects.Explosive::.ctor()
extern "C" void Explosive__ctor_m421 (Explosive_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.Explosive::Start()
extern "C" void Explosive_Start_m422 (Explosive_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityStandardAssets.Effects.Explosive::OnCollisionEnter(UnityEngine.Collision)
extern "C" Object_t * Explosive_OnCollisionEnter_m423 (Explosive_t147 * __this, Collision_t146 * ___col, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.Explosive::Reset()
extern "C" void Explosive_Reset_m424 (Explosive_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
