﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.SmoothFollow
struct SmoothFollow_t197;

// System.Void UnityStandardAssets.Utility.SmoothFollow::.ctor()
extern "C" void SmoothFollow__ctor_m536 (SmoothFollow_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.SmoothFollow::Start()
extern "C" void SmoothFollow_Start_m537 (SmoothFollow_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.SmoothFollow::LateUpdate()
extern "C" void SmoothFollow_LateUpdate_m538 (SmoothFollow_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
