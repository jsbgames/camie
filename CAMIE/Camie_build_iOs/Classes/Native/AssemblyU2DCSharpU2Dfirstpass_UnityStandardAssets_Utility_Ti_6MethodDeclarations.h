﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.TimedObjectDestructor
struct TimedObjectDestructor_t206;

// System.Void UnityStandardAssets.Utility.TimedObjectDestructor::.ctor()
extern "C" void TimedObjectDestructor__ctor_m564 (TimedObjectDestructor_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.TimedObjectDestructor::Awake()
extern "C" void TimedObjectDestructor_Awake_m565 (TimedObjectDestructor_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.TimedObjectDestructor::DestroyNow()
extern "C" void TimedObjectDestructor_DestroyNow_m566 (TimedObjectDestructor_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
