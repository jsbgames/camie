﻿#pragma once
#include <stdint.h>
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_t34;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
struct  CrossPlatformInputManager_t35  : public Object_t
{
};
struct CrossPlatformInputManager_t35_StaticFields{
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::activeInput
	VirtualInput_t34 * ___activeInput_0;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_TouchInput
	VirtualInput_t34 * ___s_TouchInput_1;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_HardwareInput
	VirtualInput_t34 * ___s_HardwareInput_2;
};
