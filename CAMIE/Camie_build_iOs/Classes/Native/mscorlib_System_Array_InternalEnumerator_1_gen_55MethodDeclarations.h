﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>
struct InternalEnumerator_1_t3195;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18960_gshared (InternalEnumerator_1_t3195 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18960(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3195 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18960_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18961_gshared (InternalEnumerator_1_t3195 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18961(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3195 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18961_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18962_gshared (InternalEnumerator_1_t3195 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18962(__this, method) (( void (*) (InternalEnumerator_1_t3195 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18962_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18963_gshared (InternalEnumerator_1_t3195 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18963(__this, method) (( bool (*) (InternalEnumerator_1_t3195 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18963_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern "C" GcAchievementData_t958  InternalEnumerator_1_get_Current_m18964_gshared (InternalEnumerator_1_t3195 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18964(__this, method) (( GcAchievementData_t958  (*) (InternalEnumerator_1_t3195 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18964_gshared)(__this, method)
