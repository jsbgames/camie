﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t899;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>
struct IEnumerable_1_t3737;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t3738;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>
struct ICollection_1_t691;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t3244;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1026;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t3248;
// System.Comparison`1<UnityEngine.UILineInfo>
struct Comparison_1_t3251;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_33.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void List_1__ctor_m19712_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1__ctor_m19712(__this, method) (( void (*) (List_1_t899 *, const MethodInfo*))List_1__ctor_m19712_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m19713_gshared (List_1_t899 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m19713(__this, ___collection, method) (( void (*) (List_1_t899 *, Object_t*, const MethodInfo*))List_1__ctor_m19713_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m5146_gshared (List_1_t899 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m5146(__this, ___capacity, method) (( void (*) (List_1_t899 *, int32_t, const MethodInfo*))List_1__ctor_m5146_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void List_1__cctor_m19714_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m19714(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m19714_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19715_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19715(__this, method) (( Object_t* (*) (List_1_t899 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19715_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m19716_gshared (List_1_t899 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m19716(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t899 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m19716_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m19717_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19717(__this, method) (( Object_t * (*) (List_1_t899 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m19717_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m19718_gshared (List_1_t899 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m19718(__this, ___item, method) (( int32_t (*) (List_1_t899 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m19718_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m19719_gshared (List_1_t899 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m19719(__this, ___item, method) (( bool (*) (List_1_t899 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m19719_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m19720_gshared (List_1_t899 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m19720(__this, ___item, method) (( int32_t (*) (List_1_t899 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m19720_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m19721_gshared (List_1_t899 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m19721(__this, ___index, ___item, method) (( void (*) (List_1_t899 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m19721_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m19722_gshared (List_1_t899 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m19722(__this, ___item, method) (( void (*) (List_1_t899 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m19722_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19723_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19723(__this, method) (( bool (*) (List_1_t899 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19723_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m19724_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19724(__this, method) (( bool (*) (List_1_t899 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m19724_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m19725_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m19725(__this, method) (( Object_t * (*) (List_1_t899 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m19725_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m19726_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m19726(__this, method) (( bool (*) (List_1_t899 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m19726_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m19727_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m19727(__this, method) (( bool (*) (List_1_t899 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m19727_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m19728_gshared (List_1_t899 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m19728(__this, ___index, method) (( Object_t * (*) (List_1_t899 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m19728_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m19729_gshared (List_1_t899 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m19729(__this, ___index, ___value, method) (( void (*) (List_1_t899 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m19729_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void List_1_Add_m19730_gshared (List_1_t899 * __this, UILineInfo_t687  ___item, const MethodInfo* method);
#define List_1_Add_m19730(__this, ___item, method) (( void (*) (List_1_t899 *, UILineInfo_t687 , const MethodInfo*))List_1_Add_m19730_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m19731_gshared (List_1_t899 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m19731(__this, ___newCount, method) (( void (*) (List_1_t899 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m19731_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m19732_gshared (List_1_t899 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m19732(__this, ___collection, method) (( void (*) (List_1_t899 *, Object_t*, const MethodInfo*))List_1_AddCollection_m19732_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m19733_gshared (List_1_t899 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m19733(__this, ___enumerable, method) (( void (*) (List_1_t899 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m19733_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m19734_gshared (List_1_t899 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m19734(__this, ___collection, method) (( void (*) (List_1_t899 *, Object_t*, const MethodInfo*))List_1_AddRange_m19734_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3244 * List_1_AsReadOnly_m19735_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m19735(__this, method) (( ReadOnlyCollection_1_t3244 * (*) (List_1_t899 *, const MethodInfo*))List_1_AsReadOnly_m19735_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Clear()
extern "C" void List_1_Clear_m19736_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_Clear_m19736(__this, method) (( void (*) (List_1_t899 *, const MethodInfo*))List_1_Clear_m19736_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool List_1_Contains_m19737_gshared (List_1_t899 * __this, UILineInfo_t687  ___item, const MethodInfo* method);
#define List_1_Contains_m19737(__this, ___item, method) (( bool (*) (List_1_t899 *, UILineInfo_t687 , const MethodInfo*))List_1_Contains_m19737_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m19738_gshared (List_1_t899 * __this, UILineInfoU5BU5D_t1026* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m19738(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t899 *, UILineInfoU5BU5D_t1026*, int32_t, const MethodInfo*))List_1_CopyTo_m19738_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Find(System.Predicate`1<T>)
extern "C" UILineInfo_t687  List_1_Find_m19739_gshared (List_1_t899 * __this, Predicate_1_t3248 * ___match, const MethodInfo* method);
#define List_1_Find_m19739(__this, ___match, method) (( UILineInfo_t687  (*) (List_1_t899 *, Predicate_1_t3248 *, const MethodInfo*))List_1_Find_m19739_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m19740_gshared (Object_t * __this /* static, unused */, Predicate_1_t3248 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m19740(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3248 *, const MethodInfo*))List_1_CheckMatch_m19740_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m19741_gshared (List_1_t899 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3248 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m19741(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t899 *, int32_t, int32_t, Predicate_1_t3248 *, const MethodInfo*))List_1_GetIndex_m19741_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Enumerator_t3243  List_1_GetEnumerator_m19742_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m19742(__this, method) (( Enumerator_t3243  (*) (List_1_t899 *, const MethodInfo*))List_1_GetEnumerator_m19742_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m19743_gshared (List_1_t899 * __this, UILineInfo_t687  ___item, const MethodInfo* method);
#define List_1_IndexOf_m19743(__this, ___item, method) (( int32_t (*) (List_1_t899 *, UILineInfo_t687 , const MethodInfo*))List_1_IndexOf_m19743_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m19744_gshared (List_1_t899 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m19744(__this, ___start, ___delta, method) (( void (*) (List_1_t899 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m19744_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m19745_gshared (List_1_t899 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m19745(__this, ___index, method) (( void (*) (List_1_t899 *, int32_t, const MethodInfo*))List_1_CheckIndex_m19745_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m19746_gshared (List_1_t899 * __this, int32_t ___index, UILineInfo_t687  ___item, const MethodInfo* method);
#define List_1_Insert_m19746(__this, ___index, ___item, method) (( void (*) (List_1_t899 *, int32_t, UILineInfo_t687 , const MethodInfo*))List_1_Insert_m19746_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m19747_gshared (List_1_t899 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m19747(__this, ___collection, method) (( void (*) (List_1_t899 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m19747_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool List_1_Remove_m19748_gshared (List_1_t899 * __this, UILineInfo_t687  ___item, const MethodInfo* method);
#define List_1_Remove_m19748(__this, ___item, method) (( bool (*) (List_1_t899 *, UILineInfo_t687 , const MethodInfo*))List_1_Remove_m19748_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m19749_gshared (List_1_t899 * __this, Predicate_1_t3248 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m19749(__this, ___match, method) (( int32_t (*) (List_1_t899 *, Predicate_1_t3248 *, const MethodInfo*))List_1_RemoveAll_m19749_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m19750_gshared (List_1_t899 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m19750(__this, ___index, method) (( void (*) (List_1_t899 *, int32_t, const MethodInfo*))List_1_RemoveAt_m19750_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Reverse()
extern "C" void List_1_Reverse_m19751_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_Reverse_m19751(__this, method) (( void (*) (List_1_t899 *, const MethodInfo*))List_1_Reverse_m19751_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort()
extern "C" void List_1_Sort_m19752_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_Sort_m19752(__this, method) (( void (*) (List_1_t899 *, const MethodInfo*))List_1_Sort_m19752_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m19753_gshared (List_1_t899 * __this, Comparison_1_t3251 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m19753(__this, ___comparison, method) (( void (*) (List_1_t899 *, Comparison_1_t3251 *, const MethodInfo*))List_1_Sort_m19753_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UILineInfo>::ToArray()
extern "C" UILineInfoU5BU5D_t1026* List_1_ToArray_m19754_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_ToArray_m19754(__this, method) (( UILineInfoU5BU5D_t1026* (*) (List_1_t899 *, const MethodInfo*))List_1_ToArray_m19754_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m19755_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m19755(__this, method) (( void (*) (List_1_t899 *, const MethodInfo*))List_1_TrimExcess_m19755_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m19756_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m19756(__this, method) (( int32_t (*) (List_1_t899 *, const MethodInfo*))List_1_get_Capacity_m19756_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m19757_gshared (List_1_t899 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m19757(__this, ___value, method) (( void (*) (List_1_t899 *, int32_t, const MethodInfo*))List_1_set_Capacity_m19757_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m19758_gshared (List_1_t899 * __this, const MethodInfo* method);
#define List_1_get_Count_m19758(__this, method) (( int32_t (*) (List_1_t899 *, const MethodInfo*))List_1_get_Count_m19758_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t687  List_1_get_Item_m19759_gshared (List_1_t899 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m19759(__this, ___index, method) (( UILineInfo_t687  (*) (List_1_t899 *, int32_t, const MethodInfo*))List_1_get_Item_m19759_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m19760_gshared (List_1_t899 * __this, int32_t ___index, UILineInfo_t687  ___value, const MethodInfo* method);
#define List_1_set_Item_m19760(__this, ___index, ___value, method) (( void (*) (List_1_t899 *, int32_t, UILineInfo_t687 , const MethodInfo*))List_1_set_Item_m19760_gshared)(__this, ___index, ___value, method)
