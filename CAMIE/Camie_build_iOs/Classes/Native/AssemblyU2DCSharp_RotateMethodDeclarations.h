﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Rotate
struct Rotate_t307;

// System.Void Rotate::.ctor()
extern "C" void Rotate__ctor_m1133 (Rotate_t307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Rotate::Start()
extern "C" void Rotate_Start_m1134 (Rotate_t307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Rotate::Update()
extern "C" void Rotate_Update_m1135 (Rotate_t307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
