﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct Comparer_1_t3501;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct  Comparer_1_t3501  : public Object_t
{
};
struct Comparer_1_t3501_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.DateTimeOffset>::_default
	Comparer_1_t3501 * ____default_0;
};
