﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t586;

// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
extern "C" void MatEntry__ctor_m2745 (MatEntry_t586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
