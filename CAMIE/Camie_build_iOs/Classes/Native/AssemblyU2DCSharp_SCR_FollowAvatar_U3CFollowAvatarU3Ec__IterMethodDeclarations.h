﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_FollowAvatar/<FollowAvatar>c__Iterator8
struct U3CFollowAvatarU3Ec__Iterator8_t354;
// System.Object
struct Object_t;

// System.Void SCR_FollowAvatar/<FollowAvatar>c__Iterator8::.ctor()
extern "C" void U3CFollowAvatarU3Ec__Iterator8__ctor_m1294 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_FollowAvatar/<FollowAvatar>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFollowAvatarU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1295 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_FollowAvatar/<FollowAvatar>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFollowAvatarU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1296 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_FollowAvatar/<FollowAvatar>c__Iterator8::MoveNext()
extern "C" bool U3CFollowAvatarU3Ec__Iterator8_MoveNext_m1297 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FollowAvatar/<FollowAvatar>c__Iterator8::Dispose()
extern "C" void U3CFollowAvatarU3Ec__Iterator8_Dispose_m1298 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FollowAvatar/<FollowAvatar>c__Iterator8::Reset()
extern "C" void U3CFollowAvatarU3Ec__Iterator8_Reset_m1299 (U3CFollowAvatarU3Ec__Iterator8_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
