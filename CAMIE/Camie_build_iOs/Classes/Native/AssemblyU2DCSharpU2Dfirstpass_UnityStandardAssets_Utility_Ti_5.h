﻿#pragma once
#include <stdint.h>
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct Entries_t201;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityStandardAssets.Utility.TimedObjectActivator
struct  TimedObjectActivator_t205  : public MonoBehaviour_t3
{
	// UnityStandardAssets.Utility.TimedObjectActivator/Entries UnityStandardAssets.Utility.TimedObjectActivator::entries
	Entries_t201 * ___entries_2;
};
