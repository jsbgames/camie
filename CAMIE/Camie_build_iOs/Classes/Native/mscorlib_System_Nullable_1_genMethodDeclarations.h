﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<System.TimeSpan>
struct Nullable_1_t2292;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen.h"

// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C" void Nullable_1__ctor_m12642_gshared (Nullable_1_t2292 * __this, TimeSpan_t1337  ___value, const MethodInfo* method);
#define Nullable_1__ctor_m12642(__this, ___value, method) (( void (*) (Nullable_1_t2292 *, TimeSpan_t1337 , const MethodInfo*))Nullable_1__ctor_m12642_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m12643_gshared (Nullable_1_t2292 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m12643(__this, method) (( bool (*) (Nullable_1_t2292 *, const MethodInfo*))Nullable_1_get_HasValue_m12643_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C" TimeSpan_t1337  Nullable_1_get_Value_m12644_gshared (Nullable_1_t2292 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m12644(__this, method) (( TimeSpan_t1337  (*) (Nullable_1_t2292 *, const MethodInfo*))Nullable_1_get_Value_m12644_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m22507_gshared (Nullable_1_t2292 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m22507(__this, ___other, method) (( bool (*) (Nullable_1_t2292 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m22507_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m22508_gshared (Nullable_1_t2292 * __this, Nullable_1_t2292  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m22508(__this, ___other, method) (( bool (*) (Nullable_1_t2292 *, Nullable_1_t2292 , const MethodInfo*))Nullable_1_Equals_m22508_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m22509_gshared (Nullable_1_t2292 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m22509(__this, method) (( int32_t (*) (Nullable_1_t2292 *, const MethodInfo*))Nullable_1_GetHashCode_m22509_gshared)(__this, method)
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C" String_t* Nullable_1_ToString_m22510_gshared (Nullable_1_t2292 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m22510(__this, method) (( String_t* (*) (Nullable_1_t2292 *, const MethodInfo*))Nullable_1_ToString_m22510_gshared)(__this, method)
