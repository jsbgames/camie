﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>
struct Comparer_1_t3240;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>
struct  Comparer_1_t3240  : public Object_t
{
};
struct Comparer_1_t3240_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::_default
	Comparer_1_t3240 * ____default_0;
};
