﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t2313;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
extern "C" void GenericComparer_1__ctor_m12638_gshared (GenericComparer_1_t2313 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m12638(__this, method) (( void (*) (GenericComparer_1_t2313 *, const MethodInfo*))GenericComparer_1__ctor_m12638_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m22473_gshared (GenericComparer_1_t2313 * __this, DateTime_t406  ___x, DateTime_t406  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m22473(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2313 *, DateTime_t406 , DateTime_t406 , const MethodInfo*))GenericComparer_1_Compare_m22473_gshared)(__this, ___x, ___y, method)
