﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<SCR_Menu>
struct IList_1_t3005;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<SCR_Menu>
struct  ReadOnlyCollection_1_t3006  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SCR_Menu>::list
	Object_t* ___list_0;
};
