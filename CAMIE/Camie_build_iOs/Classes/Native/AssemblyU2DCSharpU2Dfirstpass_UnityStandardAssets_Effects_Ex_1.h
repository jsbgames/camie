﻿#pragma once
#include <stdint.h>
// UnityEngine.Collider[]
struct ColliderU5BU5D_t135;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody>
struct List_1_t142;
// UnityEngine.Collider
struct Collider_t138;
// UnityEngine.Rigidbody
struct Rigidbody_t14;
// System.Object
struct Object_t;
// UnityStandardAssets.Effects.ExplosionPhysicsForce
struct ExplosionPhysicsForce_t143;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
// UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1
struct  U3CStartU3Ec__Iterator1_t144  : public Object_t
{
	// System.Single UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::<multiplier>__0
	float ___U3CmultiplierU3E__0_0;
	// System.Single UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::<r>__1
	float ___U3CrU3E__1_1;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::<cols>__2
	ColliderU5BU5D_t135* ___U3CcolsU3E__2_2;
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody> UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::<rigidbodies>__3
	List_1_t142 * ___U3CrigidbodiesU3E__3_3;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::<$s_10>__4
	ColliderU5BU5D_t135* ___U3CU24s_10U3E__4_4;
	// System.Int32 UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::<$s_11>__5
	int32_t ___U3CU24s_11U3E__5_5;
	// UnityEngine.Collider UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::<col>__6
	Collider_t138 * ___U3CcolU3E__6_6;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody> UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::<$s_12>__7
	Enumerator_t145  ___U3CU24s_12U3E__7_7;
	// UnityEngine.Rigidbody UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::<rb>__8
	Rigidbody_t14 * ___U3CrbU3E__8_8;
	// System.Int32 UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::$PC
	int32_t ___U24PC_9;
	// System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::$current
	Object_t * ___U24current_10;
	// UnityStandardAssets.Effects.ExplosionPhysicsForce UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>c__Iterator1::<>f__this
	ExplosionPhysicsForce_t143 * ___U3CU3Ef__this_11;
};
