﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AudioListener
struct AudioListener_t882;

// System.Single UnityEngine.AudioListener::get_volume()
extern "C" float AudioListener_get_volume_m1717 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::set_volume(System.Single)
extern "C" void AudioListener_set_volume_m1718 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
