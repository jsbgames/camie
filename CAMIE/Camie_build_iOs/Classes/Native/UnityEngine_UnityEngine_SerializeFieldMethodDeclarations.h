﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SerializeField
struct SerializeField_t260;

// System.Void UnityEngine.SerializeField::.ctor()
extern "C" void SerializeField__ctor_m1024 (SerializeField_t260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
