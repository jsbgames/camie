﻿#pragma once
#include <stdint.h>
// UnityEngine.LineRenderer
struct LineRenderer_t397;
// UnityEngine.ParticleSystem
struct ParticleSystem_t161;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// SCR_Spider
struct  SCR_Spider_t384  : public MonoBehaviour_t3
{
	// UnityEngine.Vector3 SCR_Spider::startPosition
	Vector3_t4  ___startPosition_2;
	// UnityEngine.LineRenderer SCR_Spider::line
	LineRenderer_t397 * ___line_3;
	// System.Boolean SCR_Spider::isActive
	bool ___isActive_4;
	// UnityEngine.ParticleSystem SCR_Spider::leaves
	ParticleSystem_t161 * ___leaves_5;
	// System.Single SCR_Spider::totalLeafPlayTime
	float ___totalLeafPlayTime_6;
	// System.Single SCR_Spider::currentLeafPlayTime
	float ___currentLeafPlayTime_7;
	// System.Boolean SCR_Spider::dropLeaves
	bool ___dropLeaves_8;
};
