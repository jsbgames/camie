﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t251;
// UnityEngine.Material
struct Material_t55;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>
struct  Enumerator_t2877 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>::l
	List_1_t251 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>::current
	Material_t55 * ___current_3;
};
