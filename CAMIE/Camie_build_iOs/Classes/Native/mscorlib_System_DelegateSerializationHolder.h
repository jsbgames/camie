﻿#pragma once
#include <stdint.h>
// System.Delegate
struct Delegate_t675;
// System.Object
#include "mscorlib_System_Object.h"
// System.DelegateSerializationHolder
struct  DelegateSerializationHolder_t2199  : public Object_t
{
	// System.Delegate System.DelegateSerializationHolder::_delegate
	Delegate_t675 * ____delegate_0;
};
