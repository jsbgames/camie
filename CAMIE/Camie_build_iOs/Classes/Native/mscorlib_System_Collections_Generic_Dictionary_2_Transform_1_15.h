﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Byte,System.Byte>
struct  Transform_1_t2993  : public MulticastDelegate_t549
{
};
