﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte>
struct Dictionary_2_t2983;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>
struct  KeyCollection_t2987  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::dictionary
	Dictionary_2_t2983 * ___dictionary_0;
};
