﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Button
struct Button_t324;
// UnityEngine.UI.Image
struct Image_t48;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_FlyButton
struct  SCR_FlyButton_t345  : public MonoBehaviour_t3
{
	// System.Boolean SCR_FlyButton::isPressed
	bool ___isPressed_2;
	// System.Boolean SCR_FlyButton::shown
	bool ___shown_3;
	// UnityEngine.UI.Button SCR_FlyButton::button
	Button_t324 * ___button_4;
	// UnityEngine.UI.Image SCR_FlyButton::image
	Image_t48 * ___image_5;
};
