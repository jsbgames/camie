﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.SimpleActivatorMenu
struct SimpleActivatorMenu_t195;

// System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::.ctor()
extern "C" void SimpleActivatorMenu__ctor_m530 (SimpleActivatorMenu_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::OnEnable()
extern "C" void SimpleActivatorMenu_OnEnable_m531 (SimpleActivatorMenu_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.SimpleActivatorMenu::NextCamera()
extern "C" void SimpleActivatorMenu_NextCamera_m532 (SimpleActivatorMenu_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
