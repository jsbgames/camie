﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.TimedObjectActivator/Entry>
struct InternalEnumerator_1_t2885;
// System.Object
struct Object_t;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_t199;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.TimedObjectActivator/Entry>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14467(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2885 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.TimedObjectActivator/Entry>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14468(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2885 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.TimedObjectActivator/Entry>::Dispose()
#define InternalEnumerator_1_Dispose_m14469(__this, method) (( void (*) (InternalEnumerator_1_t2885 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.TimedObjectActivator/Entry>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14470(__this, method) (( bool (*) (InternalEnumerator_1_t2885 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityStandardAssets.Utility.TimedObjectActivator/Entry>::get_Current()
#define InternalEnumerator_1_get_Current_m14471(__this, method) (( Entry_t199 * (*) (InternalEnumerator_1_t2885 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
