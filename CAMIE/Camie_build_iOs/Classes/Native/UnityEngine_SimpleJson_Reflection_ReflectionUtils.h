﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t224;
// System.Object
#include "mscorlib_System_Object.h"
// SimpleJson.Reflection.ReflectionUtils
struct  ReflectionUtils_t946  : public Object_t
{
};
struct ReflectionUtils_t946_StaticFields{
	// System.Object[] SimpleJson.Reflection.ReflectionUtils::EmptyObjects
	ObjectU5BU5D_t224* ___EmptyObjects_0;
};
