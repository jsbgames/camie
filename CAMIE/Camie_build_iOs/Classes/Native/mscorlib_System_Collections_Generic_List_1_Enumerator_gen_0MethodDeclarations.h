﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<SCR_Menu>
struct Enumerator_t423;
// System.Object
struct Object_t;
// SCR_Menu
struct SCR_Menu_t336;
// System.Collections.Generic.List`1<SCR_Menu>
struct List_1_t377;

// System.Void System.Collections.Generic.List`1/Enumerator<SCR_Menu>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m16149(__this, ___l, method) (( void (*) (Enumerator_t423 *, List_1_t377 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SCR_Menu>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16150(__this, method) (( Object_t * (*) (Enumerator_t423 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SCR_Menu>::Dispose()
#define Enumerator_Dispose_m16151(__this, method) (( void (*) (Enumerator_t423 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SCR_Menu>::VerifyState()
#define Enumerator_VerifyState_m16152(__this, method) (( void (*) (Enumerator_t423 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SCR_Menu>::MoveNext()
#define Enumerator_MoveNext_m1799(__this, method) (( bool (*) (Enumerator_t423 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SCR_Menu>::get_Current()
#define Enumerator_get_Current_m1798(__this, method) (( SCR_Menu_t336 * (*) (Enumerator_t423 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
