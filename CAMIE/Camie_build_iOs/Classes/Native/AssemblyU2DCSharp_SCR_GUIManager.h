﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t376;
// System.Collections.Generic.List`1<SCR_Menu>
struct List_1_t377;
// SCR_PauseMenu
struct SCR_PauseMenu_t374;
// SCR_MainMenu
struct SCR_MainMenu_t372;
// SCR_LevelEndMenu
struct SCR_LevelEndMenu_t367;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_GUIManager
struct  SCR_GUIManager_t378  : public MonoBehaviour_t3
{
	// UnityEngine.Canvas[] SCR_GUIManager::gameCanvases
	CanvasU5BU5D_t376* ___gameCanvases_2;
	// System.Collections.Generic.List`1<SCR_Menu> SCR_GUIManager::menus
	List_1_t377 * ___menus_3;
	// SCR_PauseMenu SCR_GUIManager::pauseMenu
	SCR_PauseMenu_t374 * ___pauseMenu_4;
	// SCR_MainMenu SCR_GUIManager::mainMenu
	SCR_MainMenu_t372 * ___mainMenu_5;
	// SCR_LevelEndMenu SCR_GUIManager::lvlEndMenu
	SCR_LevelEndMenu_t367 * ___lvlEndMenu_6;
	// System.Boolean SCR_GUIManager::isFrench
	bool ___isFrench_7;
};
