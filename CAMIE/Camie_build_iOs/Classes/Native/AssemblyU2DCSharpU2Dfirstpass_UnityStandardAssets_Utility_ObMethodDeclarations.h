﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7
struct U3CResetCoroutineU3Ec__Iterator7_t187;
// System.Object
struct Object_t;

// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::.ctor()
extern "C" void U3CResetCoroutineU3Ec__Iterator7__ctor_m507 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CResetCoroutineU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m508 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CResetCoroutineU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m509 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::MoveNext()
extern "C" bool U3CResetCoroutineU3Ec__Iterator7_MoveNext_m510 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::Dispose()
extern "C" void U3CResetCoroutineU3Ec__Iterator7_Dispose_m511 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator7::Reset()
extern "C" void U3CResetCoroutineU3Ec__Iterator7_Reset_m512 (U3CResetCoroutineU3Ec__Iterator7_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
