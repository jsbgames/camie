﻿#pragma once
#include <stdint.h>
// SCR_Camie
struct SCR_Camie_t338;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t400;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t210;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_CamieDirections
struct  SCR_CamieDirections_t348  : public MonoBehaviour_t3
{
	// SCR_Camie SCR_CamieDirections::avatar
	SCR_Camie_t338 * ___avatar_2;
	// UnityEngine.Quaternion[] SCR_CamieDirections::angles
	QuaternionU5BU5D_t400* ___angles_3;
	// UnityEngine.Vector3[] SCR_CamieDirections::normals
	Vector3U5BU5D_t210* ___normals_4;
};
