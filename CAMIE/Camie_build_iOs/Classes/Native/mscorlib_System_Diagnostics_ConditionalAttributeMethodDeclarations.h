﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.ConditionalAttribute
struct ConditionalAttribute_t1673;
// System.String
struct String_t;

// System.Void System.Diagnostics.ConditionalAttribute::.ctor(System.String)
extern "C" void ConditionalAttribute__ctor_m8408 (ConditionalAttribute_t1673 * __this, String_t* ___conditionString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
