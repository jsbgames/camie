﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ReporterGUI
struct ReporterGUI_t305;

// System.Void ReporterGUI::.ctor()
extern "C" void ReporterGUI__ctor_m1124 (ReporterGUI_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterGUI::Awake()
extern "C" void ReporterGUI_Awake_m1125 (ReporterGUI_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReporterGUI::OnGUI()
extern "C" void ReporterGUI_OnGUI_m1126 (ReporterGUI_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
