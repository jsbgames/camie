﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_PointManager
struct SCR_PointManager_t381;
// System.Int32[]
struct Int32U5BU5D_t242;
// System.Collections.Generic.List`1<SCR_Puceron>
struct List_1_t387;
// SCR_PointManager/CollectibleType
#include "AssemblyU2DCSharp_SCR_PointManager_CollectibleType.h"

// System.Void SCR_PointManager::.ctor()
extern "C" void SCR_PointManager__ctor_m1457 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::Awake()
extern "C" void SCR_PointManager_Awake_m1458 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::Start()
extern "C" void SCR_PointManager_Start_m1459 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::OnLevelWasLoaded()
extern "C" void SCR_PointManager_OnLevelWasLoaded_m1460 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::Update()
extern "C" void SCR_PointManager_Update_m1461 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::LevelInit()
extern "C" void SCR_PointManager_LevelInit_m1462 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::StartScoring()
extern "C" void SCR_PointManager_StartScoring_m1463 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::IncrementMovingPucerons()
extern "C" void SCR_PointManager_IncrementMovingPucerons_m1464 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::ValidateScores()
extern "C" void SCR_PointManager_ValidateScores_m1465 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::AcquireCollectible(SCR_PointManager/CollectibleType)
extern "C" void SCR_PointManager_AcquireCollectible_m1466 (SCR_PointManager_t381 * __this, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::LosePucerons()
extern "C" void SCR_PointManager_LosePucerons_m1467 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_PointManager::CompareHighscore(System.Int32)
extern "C" bool SCR_PointManager_CompareHighscore_m1468 (SCR_PointManager_t381 * __this, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::SetNewHighScore(System.Int32)
extern "C" void SCR_PointManager_SetNewHighScore_m1469 (SCR_PointManager_t381 * __this, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SCR_PointManager::get_LadybugsCurrentLevel()
extern "C" int32_t SCR_PointManager_get_LadybugsCurrentLevel_m1470 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SCR_PointManager::get_PuceronsCurrentLevel()
extern "C" int32_t SCR_PointManager_get_PuceronsCurrentLevel_m1471 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SCR_PointManager::get_TotalScore()
extern "C" int32_t SCR_PointManager_get_TotalScore_m1472 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] SCR_PointManager::get_HighScores()
extern "C" Int32U5BU5D_t242* SCR_PointManager_get_HighScores_m1473 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::set_HighScores(System.Int32[])
extern "C" void SCR_PointManager_set_HighScores_m1474 (SCR_PointManager_t381 * __this, Int32U5BU5D_t242* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SCR_PointManager::get_ScoreCumul()
extern "C" int32_t SCR_PointManager_get_ScoreCumul_m1475 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] SCR_PointManager::get_PointsForLevelAccess()
extern "C" Int32U5BU5D_t242* SCR_PointManager_get_PointsForLevelAccess_m1476 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_PointManager::get_IsScoreCounting()
extern "C" bool SCR_PointManager_get_IsScoreCounting_m1477 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<SCR_Puceron> SCR_PointManager::get_PuceronsCollected()
extern "C" List_1_t387 * SCR_PointManager_get_PuceronsCollected_m1478 (SCR_PointManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PointManager::set_PuceronsCollected(System.Collections.Generic.List`1<SCR_Puceron>)
extern "C" void SCR_PointManager_set_PuceronsCollected_m1479 (SCR_PointManager_t381 * __this, List_1_t387 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
