﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_PuceronMove
struct SCR_PuceronMove_t359;
// SCR_Waypoint[]
struct SCR_WaypointU5BU5D_t361;
// SCR_Waypoint
struct SCR_Waypoint_t358;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void SCR_PuceronMove::.ctor()
extern "C" void SCR_PuceronMove__ctor_m1321 (SCR_PuceronMove_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PuceronMove::Start()
extern "C" void SCR_PuceronMove_Start_m1322 (SCR_PuceronMove_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PuceronMove::Update()
extern "C" void SCR_PuceronMove_Update_m1323 (SCR_PuceronMove_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PuceronMove::Collect()
extern "C" void SCR_PuceronMove_Collect_m1324 (SCR_PuceronMove_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PuceronMove::ReturnToGame()
extern "C" void SCR_PuceronMove_ReturnToGame_m1325 (SCR_PuceronMove_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_Waypoint[] SCR_PuceronMove::GetWaypoints()
extern "C" SCR_WaypointU5BU5D_t361* SCR_PuceronMove_GetWaypoints_m1326 (SCR_PuceronMove_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_Waypoint[] SCR_PuceronMove::SortWaypoints(SCR_Waypoint[],System.Boolean)
extern "C" SCR_WaypointU5BU5D_t361* SCR_PuceronMove_SortWaypoints_m1327 (SCR_PuceronMove_t359 * __this, SCR_WaypointU5BU5D_t361* ___unorderedArray, bool ___isInit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PuceronMove::UnChildWaypoints()
extern "C" void SCR_PuceronMove_UnChildWaypoints_m1328 (SCR_PuceronMove_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_Waypoint SCR_PuceronMove::GetNextWaypoint()
extern "C" SCR_Waypoint_t358 * SCR_PuceronMove_GetNextWaypoint_m1329 (SCR_PuceronMove_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_PuceronMove::MoveToWaypoint(SCR_Waypoint)
extern "C" Object_t * SCR_PuceronMove_MoveToWaypoint_m1330 (SCR_PuceronMove_t359 * __this, SCR_Waypoint_t358 * ___wp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PuceronMove::StartMove()
extern "C" void SCR_PuceronMove_StartMove_m1331 (SCR_PuceronMove_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PuceronMove::StopMove()
extern "C" void SCR_PuceronMove_StopMove_m1332 (SCR_PuceronMove_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
