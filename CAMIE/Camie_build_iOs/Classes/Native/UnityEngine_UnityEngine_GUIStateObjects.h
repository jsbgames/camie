﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t973;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.GUIStateObjects
struct  GUIStateObjects_t974  : public Object_t
{
};
struct GUIStateObjects_t974_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Object> UnityEngine.GUIStateObjects::s_StateCache
	Dictionary_2_t973 * ___s_StateCache_0;
};
