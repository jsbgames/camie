﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t262;

// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern "C" void ExecuteInEditMode__ctor_m1027 (ExecuteInEditMode_t262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
