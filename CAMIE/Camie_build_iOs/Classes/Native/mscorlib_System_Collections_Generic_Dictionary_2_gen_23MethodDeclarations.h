﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>
struct Dictionary_2_t2887;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1033;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.Dictionary`2<System.Object,System.Object>>
struct ICollection_1_t3539;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2814;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>
struct KeyCollection_t2891;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>
struct ValueCollection_t2892;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2812;
// System.Collections.Generic.IDictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>
struct IDictionary_2_t3540;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1043;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>[]
struct KeyValuePair_2U5BU5D_t3541;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>>
struct IEnumerator_1_t3542;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1429;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_22MethodDeclarations.h"
#define Dictionary_2__ctor_m14475(__this, method) (( void (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2__ctor_m13547_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m14476(__this, ___comparer, method) (( void (*) (Dictionary_2_t2887 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13549_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m14477(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2887 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13551_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m14478(__this, ___capacity, method) (( void (*) (Dictionary_2_t2887 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m13553_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m14479(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2887 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13555_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m14480(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2887 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2__ctor_m13557_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14481(__this, method) (( Object_t* (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13559_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14482(__this, method) (( Object_t* (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13561_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m14483(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2887 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m13563_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m14484(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2887 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m13565_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m14485(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2887 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m13567_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m14486(__this, ___key, method) (( bool (*) (Dictionary_2_t2887 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m13569_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m14487(__this, ___key, method) (( void (*) (Dictionary_2_t2887 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m13571_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14488(__this, method) (( bool (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13573_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14489(__this, method) (( Object_t * (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13575_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14490(__this, method) (( bool (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14491(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2887 *, KeyValuePair_2_t2890 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13579_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14492(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2887 *, KeyValuePair_2_t2890 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13581_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14493(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2887 *, KeyValuePair_2U5BU5D_t3541*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13583_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14494(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2887 *, KeyValuePair_2_t2890 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13585_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m14495(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2887 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m13587_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14496(__this, method) (( Object_t * (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13589_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14497(__this, method) (( Object_t* (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13591_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14498(__this, method) (( Object_t * (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13593_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_Count()
#define Dictionary_2_get_Count_m14499(__this, method) (( int32_t (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_get_Count_m13595_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_Item(TKey)
#define Dictionary_2_get_Item_m14500(__this, ___key, method) (( Dictionary_2_t2814 * (*) (Dictionary_2_t2887 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m13597_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m14501(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2887 *, Object_t *, Dictionary_2_t2814 *, const MethodInfo*))Dictionary_2_set_Item_m13599_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m14502(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2887 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m13601_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m14503(__this, ___size, method) (( void (*) (Dictionary_2_t2887 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m13603_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m14504(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2887 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m13605_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m14505(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2890  (*) (Object_t * /* static, unused */, Object_t *, Dictionary_2_t2814 *, const MethodInfo*))Dictionary_2_make_pair_m13607_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m14506(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, Dictionary_2_t2814 *, const MethodInfo*))Dictionary_2_pick_key_m13609_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m14507(__this /* static, unused */, ___key, ___value, method) (( Dictionary_2_t2814 * (*) (Object_t * /* static, unused */, Object_t *, Dictionary_2_t2814 *, const MethodInfo*))Dictionary_2_pick_value_m13611_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m14508(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2887 *, KeyValuePair_2U5BU5D_t3541*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m13613_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::Resize()
#define Dictionary_2_Resize_m14509(__this, method) (( void (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_Resize_m13615_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::Add(TKey,TValue)
#define Dictionary_2_Add_m14510(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2887 *, Object_t *, Dictionary_2_t2814 *, const MethodInfo*))Dictionary_2_Add_m13617_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::Clear()
#define Dictionary_2_Clear_m14511(__this, method) (( void (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_Clear_m13619_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m14512(__this, ___key, method) (( bool (*) (Dictionary_2_t2887 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m13621_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m14513(__this, ___value, method) (( bool (*) (Dictionary_2_t2887 *, Dictionary_2_t2814 *, const MethodInfo*))Dictionary_2_ContainsValue_m13623_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m14514(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2887 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2_GetObjectData_m13625_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m14515(__this, ___sender, method) (( void (*) (Dictionary_2_t2887 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m13627_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::Remove(TKey)
#define Dictionary_2_Remove_m14516(__this, ___key, method) (( bool (*) (Dictionary_2_t2887 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m13629_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m14517(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2887 *, Object_t *, Dictionary_2_t2814 **, const MethodInfo*))Dictionary_2_TryGetValue_m13631_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_Keys()
#define Dictionary_2_get_Keys_m14518(__this, method) (( KeyCollection_t2891 * (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_get_Keys_m13633_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_Values()
#define Dictionary_2_get_Values_m14519(__this, method) (( ValueCollection_t2892 * (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_get_Values_m13635_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m14520(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2887 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m13637_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m14521(__this, ___value, method) (( Dictionary_2_t2814 * (*) (Dictionary_2_t2887 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m13639_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m14522(__this, ___pair, method) (( bool (*) (Dictionary_2_t2887 *, KeyValuePair_2_t2890 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m13641_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m14523(__this, method) (( Enumerator_t2893  (*) (Dictionary_2_t2887 *, const MethodInfo*))Dictionary_2_GetEnumerator_m13643_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m14524(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1430  (*) (Object_t * /* static, unused */, Object_t *, Dictionary_2_t2814 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m13645_gshared)(__this /* static, unused */, ___key, ___value, method)
