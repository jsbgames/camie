﻿#pragma once
#include <stdint.h>
// System.Action`1<UnityEngine.Font>
struct Action_1_t668;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t897;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Font
struct  Font_t517  : public Object_t164
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t897 * ___m_FontTextureRebuildCallback_3;
};
struct Font_t517_StaticFields{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t668 * ___textureRebuilt_2;
};
