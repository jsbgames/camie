﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.AutoMoveAndRotate
struct AutoMoveAndRotate_t171;

// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::.ctor()
extern "C" void AutoMoveAndRotate__ctor_m449 (AutoMoveAndRotate_t171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Start()
extern "C" void AutoMoveAndRotate_Start_m450 (AutoMoveAndRotate_t171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Update()
extern "C" void AutoMoveAndRotate_Update_m451 (AutoMoveAndRotate_t171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
