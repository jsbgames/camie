﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct MatEntryU5BU5D_t3155;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct  List_1_t587  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::_items
	MatEntryU5BU5D_t3155* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::_version
	int32_t ____version_3;
};
struct List_1_t587_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::EmptyArray
	MatEntryU5BU5D_t3155* ___EmptyArray_4;
};
