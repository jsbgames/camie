﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.OutAttribute
struct OutAttribute_t1669;

// System.Void System.Runtime.InteropServices.OutAttribute::.ctor()
extern "C" void OutAttribute__ctor_m8403 (OutAttribute_t1669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
