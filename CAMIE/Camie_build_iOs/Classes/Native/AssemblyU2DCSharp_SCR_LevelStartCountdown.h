﻿#pragma once
#include <stdint.h>
// SCR_LevelStartCountdown
struct SCR_LevelStartCountdown_t370;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t192;
// SCR_Menu
#include "AssemblyU2DCSharp_SCR_Menu.h"
// SCR_LevelStartCountdown
struct  SCR_LevelStartCountdown_t370  : public SCR_Menu_t336
{
	// UnityEngine.GameObject[] SCR_LevelStartCountdown::countdownTexts
	GameObjectU5BU5D_t192* ___countdownTexts_6;
	// System.Int32 SCR_LevelStartCountdown::activeText
	int32_t ___activeText_7;
	// System.Boolean SCR_LevelStartCountdown::getBigger
	bool ___getBigger_8;
};
struct SCR_LevelStartCountdown_t370_StaticFields{
	// SCR_LevelStartCountdown SCR_LevelStartCountdown::instance
	SCR_LevelStartCountdown_t370 * ___instance_5;
};
