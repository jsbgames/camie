﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Reporter/Sample>
struct Predicate_1_t2897;
// System.Object
struct Object_t;
// Reporter/Sample
struct Sample_t290;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<Reporter/Sample>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#define Predicate_1__ctor_m14654(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2897 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m14055_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Reporter/Sample>::Invoke(T)
#define Predicate_1_Invoke_m14655(__this, ___obj, method) (( bool (*) (Predicate_1_t2897 *, Sample_t290 *, const MethodInfo*))Predicate_1_Invoke_m14056_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Reporter/Sample>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m14656(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2897 *, Sample_t290 *, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m14057_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Reporter/Sample>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m14657(__this, ___result, method) (( bool (*) (Predicate_1_t2897 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m14058_gshared)(__this, ___result, method)
