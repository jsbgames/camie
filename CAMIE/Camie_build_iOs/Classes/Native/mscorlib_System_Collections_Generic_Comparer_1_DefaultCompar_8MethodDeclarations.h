﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t3506;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C" void DefaultComparer__ctor_m22516_gshared (DefaultComparer_t3506 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22516(__this, method) (( void (*) (DefaultComparer_t3506 *, const MethodInfo*))DefaultComparer__ctor_m22516_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m22517_gshared (DefaultComparer_t3506 * __this, Guid_t1087  ___x, Guid_t1087  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m22517(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3506 *, Guid_t1087 , Guid_t1087 , const MethodInfo*))DefaultComparer_Compare_m22517_gshared)(__this, ___x, ___y, method)
