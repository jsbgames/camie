﻿#pragma once
#include <stdint.h>
// SCR_Waypoint[]
struct SCR_WaypointU5BU5D_t361;
// SCR_Puceron
#include "AssemblyU2DCSharp_SCR_Puceron.h"
// SCR_PuceronMove/TrajectoryType
#include "AssemblyU2DCSharp_SCR_PuceronMove_TrajectoryType.h"
// SCR_PuceronMove
struct  SCR_PuceronMove_t359  : public SCR_Puceron_t356
{
	// SCR_PuceronMove/TrajectoryType SCR_PuceronMove::trajectoire
	int32_t ___trajectoire_7;
	// System.Single SCR_PuceronMove::speed
	float ___speed_8;
	// SCR_Waypoint[] SCR_PuceronMove::myWaypoints
	SCR_WaypointU5BU5D_t361* ___myWaypoints_9;
	// System.Int32 SCR_PuceronMove::goingToIndex
	int32_t ___goingToIndex_10;
	// System.Boolean SCR_PuceronMove::doMove
	bool ___doMove_11;
	// System.Boolean SCR_PuceronMove::isRotating
	bool ___isRotating_12;
	// System.Boolean SCR_PuceronMove::isStarted
	bool ___isStarted_13;
};
