﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ParticleCollisionEvent
struct ParticleCollisionEvent_t160;
// UnityEngine.Collider
struct Collider_t138;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::get_velocity()
extern "C" Vector3_t4  ParticleCollisionEvent_get_velocity_m943 (ParticleCollisionEvent_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.ParticleCollisionEvent::get_collider()
extern "C" Collider_t138 * ParticleCollisionEvent_get_collider_m942 (ParticleCollisionEvent_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.ParticleCollisionEvent::InstanceIDToCollider(System.Int32)
extern "C" Collider_t138 * ParticleCollisionEvent_InstanceIDToCollider_m4447 (Object_t * __this /* static, unused */, int32_t ___instanceID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
