﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttribute.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttribute.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttributeMethodDeclarations.h"
extern TypeInfo* GuidAttribute_t708_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t709_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyConfigurationAttribute_t710_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t711_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t712_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t713_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t714_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t715_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t716_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTrademarkAttribute_t717_il2cpp_TypeInfo_var;
void g_UnityEngine_UI_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GuidAttribute_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1163);
		AssemblyDescriptionAttribute_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1164);
		AssemblyConfigurationAttribute_t710_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1165);
		AssemblyCompanyAttribute_t711_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1166);
		AssemblyProductAttribute_t712_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1167);
		AssemblyCopyrightAttribute_t713_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1168);
		ComVisibleAttribute_t714_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1169);
		AssemblyTitleAttribute_t715_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1170);
		RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(107);
		AssemblyFileVersionAttribute_t716_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1171);
		AssemblyTrademarkAttribute_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1172);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 11;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t708 * tmp;
		tmp = (GuidAttribute_t708 *)il2cpp_codegen_object_new (GuidAttribute_t708_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m3466(tmp, il2cpp_codegen_string_new_wrapper("d4f464c7-9b15-460d-b4bc-2cacd1c1df73"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t709 * tmp;
		tmp = (AssemblyDescriptionAttribute_t709 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t709_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m3467(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyConfigurationAttribute_t710 * tmp;
		tmp = (AssemblyConfigurationAttribute_t710 *)il2cpp_codegen_object_new (AssemblyConfigurationAttribute_t710_il2cpp_TypeInfo_var);
		AssemblyConfigurationAttribute__ctor_m3468(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t711 * tmp;
		tmp = (AssemblyCompanyAttribute_t711 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t711_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m3469(tmp, il2cpp_codegen_string_new_wrapper("Microsoft"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t712 * tmp;
		tmp = (AssemblyProductAttribute_t712 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t712_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m3470(tmp, il2cpp_codegen_string_new_wrapper("guisystem"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t713 * tmp;
		tmp = (AssemblyCopyrightAttribute_t713 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t713_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m3471(tmp, il2cpp_codegen_string_new_wrapper("Copyright © Microsoft 2013"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t714 * tmp;
		tmp = (ComVisibleAttribute_t714 *)il2cpp_codegen_object_new (ComVisibleAttribute_t714_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m3472(tmp, false, NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t715 * tmp;
		tmp = (AssemblyTitleAttribute_t715 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t715_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m3473(tmp, il2cpp_codegen_string_new_wrapper("guisystem"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t258 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t258 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1021(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1022(tmp, true, NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t716 * tmp;
		tmp = (AssemblyFileVersionAttribute_t716 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t716_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m3474(tmp, il2cpp_codegen_string_new_wrapper("1.0.0.0"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTrademarkAttribute_t717 * tmp;
		tmp = (AssemblyTrademarkAttribute_t717 *)il2cpp_codegen_object_new (AssemblyTrademarkAttribute_t717_il2cpp_TypeInfo_var);
		AssemblyTrademarkAttribute__ctor_m3475(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void EventHandle_t450_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void EventSystem_t326_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Event/Event System"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void EventSystem_t326_CustomAttributesCacheGenerator_m_FirstSelected(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_Selected"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void EventSystem_t326_CustomAttributesCacheGenerator_m_sendNavigationEvents(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void EventSystem_t326_CustomAttributesCacheGenerator_m_DragThreshold(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void EventSystem_t326_CustomAttributesCacheGenerator_U3CcurrentU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void EventSystem_t326_CustomAttributesCacheGenerator_EventSystem_get_current_m1879(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void EventSystem_t326_CustomAttributesCacheGenerator_EventSystem_set_current_m1880(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void EventSystem_t326_CustomAttributesCacheGenerator_EventSystem_t326____lastSelectedGameObject_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("lastSelectedGameObject is no longer supported"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void EventTrigger_t460_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Event/Event Trigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void EventTrigger_t460_CustomAttributesCacheGenerator_m_Delegates(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("delegates"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void EventTrigger_t460_CustomAttributesCacheGenerator_delegates(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m3495(tmp, il2cpp_codegen_string_new_wrapper("Please use triggers instead (UnityUpgradable)"), true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ExecuteEvents_t481_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ExecuteEvents_t481_CustomAttributesCacheGenerator_ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m1964(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void AxisEventData_t487_CustomAttributesCacheGenerator_U3CmoveVectorU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void AxisEventData_t487_CustomAttributesCacheGenerator_U3CmoveDirU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void AxisEventData_t487_CustomAttributesCacheGenerator_AxisEventData_get_moveVector_m1989(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void AxisEventData_t487_CustomAttributesCacheGenerator_AxisEventData_set_moveVector_m1990(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void AxisEventData_t487_CustomAttributesCacheGenerator_AxisEventData_get_moveDir_m1991(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void AxisEventData_t487_CustomAttributesCacheGenerator_AxisEventData_set_moveDir_m1992(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CpointerEnterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3ClastPressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CrawPointerPressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CpointerDragU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CpointerCurrentRaycastU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CpointerPressRaycastU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CeligibleForClickU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CpointerIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CpositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CdeltaU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CpressPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CworldPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CworldNormalU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CclickTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CclickCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CscrollDeltaU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CuseDragThresholdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CdraggingU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_U3CbuttonU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pointerEnter_m2000(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pointerEnter_m2001(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_lastPress_m2002(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_lastPress_m2003(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_rawPointerPress_m2004(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_rawPointerPress_m2005(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pointerDrag_m2006(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pointerDrag_m2007(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pointerCurrentRaycast_m2008(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pointerCurrentRaycast_m2009(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pointerPressRaycast_m2010(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pointerPressRaycast_m2011(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_eligibleForClick_m2012(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_eligibleForClick_m2013(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pointerId_m725(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pointerId_m2014(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_position_m711(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_position_m1732(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_delta_m2015(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_delta_m2016(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pressPosition_m2017(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pressPosition_m1731(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_worldPosition_m2018(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_worldPosition_m2019(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_worldNormal_m2020(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_worldNormal_m2021(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_clickTime_m2022(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_clickTime_m2023(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_clickCount_m2024(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_clickCount_m2025(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_scrollDelta_m2026(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_scrollDelta_m2027(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_useDragThreshold_m2028(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_useDragThreshold_m2029(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_dragging_m2030(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_dragging_m2031(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_button_m2032(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_button_m2033(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_t215____worldPosition_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("Use either pointerCurrentRaycast.worldPosition or pointerPressRaycast.worldPosition"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_t215____worldNormal_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("Use either pointerCurrentRaycast.worldNormal or pointerPressRaycast.worldNormal"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"
extern const Il2CppType* EventSystem_t326_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void BaseInputModule_t452_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventSystem_t326_0_0_0_var = il2cpp_codegen_type_from_index(659);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(EventSystem_t326_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void StandaloneInputModule_t498_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Event/Standalone Input Module"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_HorizontalAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_VerticalAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_SubmitButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_CancelButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_InputActionsPerSecond(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_RepeatDelay(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_AllowActivationOnMobileDevice(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void StandaloneInputModule_t498_CustomAttributesCacheGenerator_StandaloneInputModule_t498____inputMode_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m3495(tmp, il2cpp_codegen_string_new_wrapper("Mode is no longer needed on input module as it handles both mouse and keyboard simultaneously."), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void InputMode_t497_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m3495(tmp, il2cpp_codegen_string_new_wrapper("Mode is no longer needed on input module as it handles both mouse and keyboard simultaneously."), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void TouchInputModule_t499_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Event/Touch Input Module"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void TouchInputModule_t499_CustomAttributesCacheGenerator_m_AllowActivationOnStandalone(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void BaseRaycaster_t485_CustomAttributesCacheGenerator_BaseRaycaster_t485____priority_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m3495(tmp, il2cpp_codegen_string_new_wrapper("Please use sortOrderPriority and renderOrderPriority"), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Physics2DRaycaster_t500_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Event/Physics 2D Raycaster"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t27_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t501_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t27_0_0_0_var = il2cpp_codegen_type_from_index(10);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Camera_t27_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Event/Physics Raycaster"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t501_CustomAttributesCacheGenerator_m_EventMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t501_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t501_CustomAttributesCacheGenerator_PhysicsRaycaster_U3CRaycastU3Em__1_m2142(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void TweenRunner_1_t722_CustomAttributesCacheGenerator_TweenRunner_1_Start_m3515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t723_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t723_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3519(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t723_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3520(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t723_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Dispose_m3522(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t723_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Reset_m3523(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void AnimationTriggers_t507_CustomAttributesCacheGenerator_m_NormalTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("normalTrigger"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AnimationTriggers_t507_CustomAttributesCacheGenerator_m_HighlightedTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("highlightedTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedTrigger"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AnimationTriggers_t507_CustomAttributesCacheGenerator_m_PressedTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("pressedTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AnimationTriggers_t507_CustomAttributesCacheGenerator_m_DisabledTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("disabledTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Button_t324_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Button"), 30, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Button_t324_CustomAttributesCacheGenerator_m_OnClick(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("onClick"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void Button_t324_CustomAttributesCacheGenerator_Button_OnFinishSubmit_m2180(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t509_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t509_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2170(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t509_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2171(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t509_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Dispose_m2173(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t509_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Reset_m2174(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t512_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t512_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t512_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m2197(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t512_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m2198(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void ColorBlock_t516_CustomAttributesCacheGenerator_m_NormalColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("normalColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ColorBlock_t516_CustomAttributesCacheGenerator_m_HighlightedColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedColor"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("highlightedColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ColorBlock_t516_CustomAttributesCacheGenerator_m_PressedColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("pressedColor"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ColorBlock_t516_CustomAttributesCacheGenerator_m_DisabledColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("disabledColor"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ColorBlock_t516_CustomAttributesCacheGenerator_m_ColorMultiplier(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 1.0f, 5.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void ColorBlock_t516_CustomAttributesCacheGenerator_m_FadeDuration(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("fadeDuration"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FontData_t518_CustomAttributesCacheGenerator_m_Font(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("font"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FontData_t518_CustomAttributesCacheGenerator_m_FontSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("fontSize"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FontData_t518_CustomAttributesCacheGenerator_m_FontStyle(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("fontStyle"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FontData_t518_CustomAttributesCacheGenerator_m_BestFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FontData_t518_CustomAttributesCacheGenerator_m_MinSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FontData_t518_CustomAttributesCacheGenerator_m_MaxSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void FontData_t518_CustomAttributesCacheGenerator_m_Alignment(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("alignment"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FontData_t518_CustomAttributesCacheGenerator_m_RichText(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("richText"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FontData_t518_CustomAttributesCacheGenerator_m_HorizontalOverflow(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FontData_t518_CustomAttributesCacheGenerator_m_VerticalOverflow(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void FontData_t518_CustomAttributesCacheGenerator_m_LineSpacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
extern const Il2CppType* CanvasRenderer_t522_0_0_0_var;
extern const Il2CppType* RectTransform_t364_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* DisallowMultipleComponent_t724_il2cpp_TypeInfo_var;
void Graphic_t418_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CanvasRenderer_t522_0_0_0_var = il2cpp_codegen_type_from_index(1068);
		RectTransform_t364_0_0_0_var = il2cpp_codegen_type_from_index(687);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		DisallowMultipleComponent_t724_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1175);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(CanvasRenderer_t522_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(RectTransform_t364_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		DisallowMultipleComponent_t724 * tmp;
		tmp = (DisallowMultipleComponent_t724 *)il2cpp_codegen_object_new (DisallowMultipleComponent_t724_il2cpp_TypeInfo_var);
		DisallowMultipleComponent__ctor_m3528(tmp, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Graphic_t418_CustomAttributesCacheGenerator_m_Material(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_Mat"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Graphic_t418_CustomAttributesCacheGenerator_m_Color(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Graphic_t418_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Graphic_t418_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Graphic_t418_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__4_m2285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Graphic_t418_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__5_m2286(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
extern const Il2CppType* Canvas_t414_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void GraphicRaycaster_t325_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t414_0_0_0_var = il2cpp_codegen_type_from_index(662);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper("Event/Graphic Raycaster"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Canvas_t414_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void GraphicRaycaster_t325_CustomAttributesCacheGenerator_m_IgnoreReversedGraphics(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("ignoreReversedGraphics"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void GraphicRaycaster_t325_CustomAttributesCacheGenerator_m_BlockingObjects(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("blockingObjects"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GraphicRaycaster_t325_CustomAttributesCacheGenerator_m_BlockingMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void GraphicRaycaster_t325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void GraphicRaycaster_t325_CustomAttributesCacheGenerator_GraphicRaycaster_U3CRaycastU3Em__6_m2301(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Image_t48_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Image"), 10, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Image_t48_CustomAttributesCacheGenerator_m_Sprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_Frame"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Image_t48_CustomAttributesCacheGenerator_m_Type(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Image_t48_CustomAttributesCacheGenerator_m_PreserveAspect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Image_t48_CustomAttributesCacheGenerator_m_FillCenter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Image_t48_CustomAttributesCacheGenerator_m_FillMethod(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Image_t48_CustomAttributesCacheGenerator_m_FillAmount(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Image_t48_CustomAttributesCacheGenerator_m_FillClockwise(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Image_t48_CustomAttributesCacheGenerator_m_FillOrigin(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Input Field"), 31, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_TextComponent(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("text"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_Placeholder(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_ContentType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_InputType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("inputType"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_AsteriskChar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("asteriskChar"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_KeyboardType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("keyboardType"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_LineType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_HideMobileInput(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("hideMobileInput"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_CharacterValidation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("validation"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_CharacterLimit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("characterLimit"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_EndEdit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("onSubmit"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_OnSubmit"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_OnValueChange(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("onValueChange"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_OnValidateInput(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("onValidateInput"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_SelectionColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("selectionColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("mValue"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_m_CaretBlinkRate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 4.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_InputField_CaretBlink_m2425(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_InputField_MouseDragOutsideRect_m2442(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void InputField_t550_CustomAttributesCacheGenerator_InputField_t550_InputField_SetToCustomIfContentTypeIsNot_m2493_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t551_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t551_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2362(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t551_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2363(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t551_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Dispose_m2365(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t551_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Reset_m2366(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t552_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t552_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2368(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t552_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m2369(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t552_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m2371(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t552_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m2372(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Navigation_t563_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("mode"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void Navigation_t563_CustomAttributesCacheGenerator_m_SelectOnUp(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("selectOnUp"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Navigation_t563_CustomAttributesCacheGenerator_m_SelectOnDown(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("selectOnDown"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void Navigation_t563_CustomAttributesCacheGenerator_m_SelectOnLeft(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("selectOnLeft"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Navigation_t563_CustomAttributesCacheGenerator_m_SelectOnRight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("selectOnRight"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void Mode_t562_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void RawImage_t564_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Raw Image"), 12, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void RawImage_t564_CustomAttributesCacheGenerator_m_Texture(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_Tex"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void RawImage_t564_CustomAttributesCacheGenerator_m_UVRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t364_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void Scrollbar_t569_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t364_0_0_0_var = il2cpp_codegen_type_from_index(687);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Scrollbar"), 32, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(RectTransform_t364_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Scrollbar_t569_CustomAttributesCacheGenerator_m_HandleRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Scrollbar_t569_CustomAttributesCacheGenerator_m_Direction(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void Scrollbar_t569_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void Scrollbar_t569_CustomAttributesCacheGenerator_m_Size(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Scrollbar_t569_CustomAttributesCacheGenerator_m_NumberOfSteps(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 11.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
extern TypeInfo* SpaceAttribute_t726_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Scrollbar_t569_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SpaceAttribute_t726_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1177);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SpaceAttribute_t726 * tmp;
		tmp = (SpaceAttribute_t726 *)il2cpp_codegen_object_new (SpaceAttribute_t726_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m3533(tmp, 6.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void Scrollbar_t569_CustomAttributesCacheGenerator_Scrollbar_ClickRepeat_m2568(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t570_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t570_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2534(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t570_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m2535(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t570_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Dispose_m2537(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t570_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Reset_m2538(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
extern const Il2CppType* RectTransform_t364_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* SelectionBaseAttribute_t727_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t364_0_0_0_var = il2cpp_codegen_type_from_index(687);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		SelectionBaseAttribute_t727_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1178);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Scroll Rect"), 33, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(RectTransform_t364_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		SelectionBaseAttribute_t727 * tmp;
		tmp = (SelectionBaseAttribute_t727 *)il2cpp_codegen_object_new (SelectionBaseAttribute_t727_il2cpp_TypeInfo_var);
		SelectionBaseAttribute__ctor_m3534(tmp, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator_m_Content(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator_m_Horizontal(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator_m_Vertical(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator_m_MovementType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator_m_Elasticity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator_m_Inertia(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator_m_DecelerationRate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator_m_ScrollSensitivity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator_m_HorizontalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator_m_VerticalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ScrollRect_t575_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DisallowMultipleComponent_t724_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* SelectionBaseAttribute_t727_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisallowMultipleComponent_t724_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1175);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		SelectionBaseAttribute_t727_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1178);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DisallowMultipleComponent_t724 * tmp;
		tmp = (DisallowMultipleComponent_t724 *)il2cpp_codegen_object_new (DisallowMultipleComponent_t724_il2cpp_TypeInfo_var);
		DisallowMultipleComponent__ctor_m3528(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SelectionBaseAttribute_t727 * tmp;
		tmp = (SelectionBaseAttribute_t727 *)il2cpp_codegen_object_new (SelectionBaseAttribute_t727_il2cpp_TypeInfo_var);
		SelectionBaseAttribute__ctor_m3534(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Selectable"), 70, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_m_Navigation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("navigation"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_m_Transition(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("transition"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_m_Colors(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("colors"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_m_SpriteState(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("spriteState"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_m_AnimationTriggers(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("animationTriggers"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_m_Interactable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("Can the Selectable be interacted with?"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_m_TargetGraphic(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_HighlightGraphic"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("highlightGraphic"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_U3CisPointerInsideU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_U3CisPointerDownU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_U3ChasSelectionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_Selectable_get_isPointerInside_m2651(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_Selectable_set_isPointerInside_m2652(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_Selectable_get_isPointerDown_m2653(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_Selectable_set_isPointerDown_m2654(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_Selectable_get_hasSelection_m2655(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_Selectable_set_hasSelection_m2656(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void Selectable_t510_CustomAttributesCacheGenerator_Selectable_IsPressed_m2682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m3495(tmp, il2cpp_codegen_string_new_wrapper("Is Pressed no longer requires eventData"), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t364_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
void Slider_t585_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t364_0_0_0_var = il2cpp_codegen_type_from_index(687);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Slider"), 34, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(RectTransform_t364_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Slider_t585_CustomAttributesCacheGenerator_m_FillRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Slider_t585_CustomAttributesCacheGenerator_m_HandleRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* SpaceAttribute_t726_il2cpp_TypeInfo_var;
void Slider_t585_CustomAttributesCacheGenerator_m_Direction(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		SpaceAttribute_t726_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1177);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SpaceAttribute_t726 * tmp;
		tmp = (SpaceAttribute_t726 *)il2cpp_codegen_object_new (SpaceAttribute_t726_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m3533(tmp, 6.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Slider_t585_CustomAttributesCacheGenerator_m_MinValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Slider_t585_CustomAttributesCacheGenerator_m_MaxValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Slider_t585_CustomAttributesCacheGenerator_m_WholeNumbers(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Slider_t585_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SpaceAttribute_t726_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Slider_t585_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SpaceAttribute_t726_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1177);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SpaceAttribute_t726 * tmp;
		tmp = (SpaceAttribute_t726 *)il2cpp_codegen_object_new (SpaceAttribute_t726_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m3533(tmp, 6.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void SpriteState_t580_CustomAttributesCacheGenerator_m_HighlightedSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("highlightedSprite"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedSprite"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void SpriteState_t580_CustomAttributesCacheGenerator_m_PressedSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("pressedSprite"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void SpriteState_t580_CustomAttributesCacheGenerator_m_DisabledSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("disabledSprite"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Text_t316_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Text"), 11, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Text_t316_CustomAttributesCacheGenerator_m_FontData(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* TextAreaAttribute_t728_il2cpp_TypeInfo_var;
void Text_t316_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		TextAreaAttribute_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1179);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TextAreaAttribute_t728 * tmp;
		tmp = (TextAreaAttribute_t728 *)il2cpp_codegen_object_new (TextAreaAttribute_t728_il2cpp_TypeInfo_var);
		TextAreaAttribute__ctor_m3537(tmp, 3, 10, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t364_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Toggle_t312_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t364_0_0_0_var = il2cpp_codegen_type_from_index(687);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(RectTransform_t364_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Toggle"), 35, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Toggle_t312_CustomAttributesCacheGenerator_m_Group(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void Toggle_t312_CustomAttributesCacheGenerator_m_IsOn(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_IsActive"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("Is the toggle currently on or off?"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void ToggleGroup_t592_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Toggle Group"), 36, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ToggleGroup_t592_CustomAttributesCacheGenerator_m_AllowSwitchOff(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ToggleGroup_t592_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ToggleGroup_t592_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ToggleGroup_t592_CustomAttributesCacheGenerator_ToggleGroup_U3CAnyTogglesOnU3Em__7_m2824(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ToggleGroup_t592_CustomAttributesCacheGenerator_ToggleGroup_U3CActiveTogglesU3Em__8_m2825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t364_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void AspectRatioFitter_t597_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t364_0_0_0_var = il2cpp_codegen_type_from_index(687);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(RectTransform_t364_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("Layout/Aspect Ratio Fitter"), 142, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AspectRatioFitter_t597_CustomAttributesCacheGenerator_m_AspectMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void AspectRatioFitter_t597_CustomAttributesCacheGenerator_m_AspectRatio(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Canvas_t414_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void CanvasScaler_t601_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t414_0_0_0_var = il2cpp_codegen_type_from_index(662);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("Layout/Canvas Scaler"), 101, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(Canvas_t414_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void CanvasScaler_t601_CustomAttributesCacheGenerator_m_UiScaleMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("Determines how UI elements in the Canvas are scaled."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void CanvasScaler_t601_CustomAttributesCacheGenerator_m_ReferencePixelsPerUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("If a sprite has this 'Pixels Per Unit' setting, then one pixel in the sprite will cover one unit in the UI."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void CanvasScaler_t601_CustomAttributesCacheGenerator_m_ScaleFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("Scales all UI elements in the Canvas by this factor."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void CanvasScaler_t601_CustomAttributesCacheGenerator_m_ReferenceResolution(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("The resolution the UI layout is designed for. If the screen resolution is larger, the UI will be scaled up, and if it's smaller, the UI will be scaled down. This is done in accordance with the Screen Match Mode."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void CanvasScaler_t601_CustomAttributesCacheGenerator_m_ScreenMatchMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("A mode used to scale the canvas area if the aspect ratio of the current resolution doesn't fit the reference resolution."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void CanvasScaler_t601_CustomAttributesCacheGenerator_m_MatchWidthOrHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("Determines if the scaling is using the width or height as reference, or a mix in between."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void CanvasScaler_t601_CustomAttributesCacheGenerator_m_PhysicalUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("The physical unit to specify positions and sizes in."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
void CanvasScaler_t601_CustomAttributesCacheGenerator_m_FallbackScreenDPI(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("The DPI to assume if the screen DPI is not known."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void CanvasScaler_t601_CustomAttributesCacheGenerator_m_DefaultSpriteDPI(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("The pixels per inch to use for sprites that have a 'Pixels Per Unit' setting that matches the 'Reference Pixels Per Unit' setting."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t266_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void CanvasScaler_t601_CustomAttributesCacheGenerator_m_DynamicPixelsPerUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t266 * tmp;
		tmp = (TooltipAttribute_t266 *)il2cpp_codegen_object_new (TooltipAttribute_t266_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m1042(tmp, il2cpp_codegen_string_new_wrapper("The amount of pixels per unit to use for dynamically created bitmaps in the UI, such as Text."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t364_0_0_0_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void ContentSizeFitter_t603_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t364_0_0_0_var = il2cpp_codegen_type_from_index(687);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("Layout/Content Size Fitter"), 141, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(RectTransform_t364_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ContentSizeFitter_t603_CustomAttributesCacheGenerator_m_HorizontalFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ContentSizeFitter_t603_CustomAttributesCacheGenerator_m_VerticalFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void GridLayoutGroup_t607_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("Layout/Grid Layout Group"), 152, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_StartCorner(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_StartAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_CellSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_Spacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_Constraint(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_ConstraintCount(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void HorizontalLayoutGroup_t609_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("Layout/Horizontal Layout Group"), 150, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t610_CustomAttributesCacheGenerator_m_Spacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t610_CustomAttributesCacheGenerator_m_ChildForceExpandWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t610_CustomAttributesCacheGenerator_m_ChildForceExpandHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t364_0_0_0_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void LayoutElement_t611_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t364_0_0_0_var = il2cpp_codegen_type_from_index(687);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(RectTransform_t364_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("Layout/Layout Element"), 140, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void LayoutElement_t611_CustomAttributesCacheGenerator_m_IgnoreLayout(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void LayoutElement_t611_CustomAttributesCacheGenerator_m_MinWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void LayoutElement_t611_CustomAttributesCacheGenerator_m_MinHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void LayoutElement_t611_CustomAttributesCacheGenerator_m_PreferredWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void LayoutElement_t611_CustomAttributesCacheGenerator_m_PreferredHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void LayoutElement_t611_CustomAttributesCacheGenerator_m_FlexibleWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void LayoutElement_t611_CustomAttributesCacheGenerator_m_FlexibleHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t364_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t259_il2cpp_TypeInfo_var;
extern TypeInfo* DisallowMultipleComponent_t724_il2cpp_TypeInfo_var;
void LayoutGroup_t608_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t364_0_0_0_var = il2cpp_codegen_type_from_index(687);
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		RequireComponent_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		DisallowMultipleComponent_t724_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1175);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t259 * tmp;
		tmp = (RequireComponent_t259 *)il2cpp_codegen_object_new (RequireComponent_t259_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m1023(tmp, il2cpp_codegen_type_get_object(RectTransform_t364_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DisallowMultipleComponent_t724 * tmp;
		tmp = (DisallowMultipleComponent_t724 *)il2cpp_codegen_object_new (DisallowMultipleComponent_t724_il2cpp_TypeInfo_var);
		DisallowMultipleComponent__ctor_m3528(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void LayoutGroup_t608_CustomAttributesCacheGenerator_m_Padding(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void LayoutGroup_t608_CustomAttributesCacheGenerator_m_ChildAlignment(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_Alignment"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutRebuilder_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutRebuilder_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutRebuilder_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutRebuilder_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutRebuilder_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutRebuilder_t615_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__9_m2985(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutRebuilder_t615_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__A_m2986(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutRebuilder_t615_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__B_m2987(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutRebuilder_t615_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__C_m2988(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutRebuilder_t615_CustomAttributesCacheGenerator_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m2989(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinWidthU3Em__E_m3001(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__F_m3002(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__10_m3003(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m3004(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinHeightU3Em__12_m3005(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__13_m3006(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__14_m3007(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m3008(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void VerticalLayoutGroup_t618_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("Layout/Vertical Layout Group"), 151, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Mask_t619_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Mask"), 13, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void Mask_t619_CustomAttributesCacheGenerator_m_ShowMaskGraphic(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_ShowGraphic"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void IndexedSet_1_t729_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CanvasListPool_t622_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CanvasListPool_t622_CustomAttributesCacheGenerator_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m3030(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ComponentListPool_t625_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ComponentListPool_t625_CustomAttributesCacheGenerator_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m3034(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ObjectPool_1_t730_CustomAttributesCacheGenerator_U3CcountAllU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ObjectPool_1_t730_CustomAttributesCacheGenerator_ObjectPool_1_get_countAll_m3573(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ObjectPool_1_t730_CustomAttributesCacheGenerator_ObjectPool_1_set_countAll_m3574(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void BaseVertexEffect_t626_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Outline_t627_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Outline"), 15, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void PositionAsUV1_t629_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Position As UV1"), 16, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void Shadow_t628_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m3524(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Shadow"), 14, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Shadow_t628_CustomAttributesCacheGenerator_m_EffectColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Shadow_t628_CustomAttributesCacheGenerator_m_EffectDistance(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void Shadow_t628_CustomAttributesCacheGenerator_m_UseGraphicAlpha(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_UnityEngine_UI_Assembly_AttributeGenerators[350] = 
{
	NULL,
	g_UnityEngine_UI_Assembly_CustomAttributesCacheGenerator,
	EventHandle_t450_CustomAttributesCacheGenerator,
	EventSystem_t326_CustomAttributesCacheGenerator,
	EventSystem_t326_CustomAttributesCacheGenerator_m_FirstSelected,
	EventSystem_t326_CustomAttributesCacheGenerator_m_sendNavigationEvents,
	EventSystem_t326_CustomAttributesCacheGenerator_m_DragThreshold,
	EventSystem_t326_CustomAttributesCacheGenerator_U3CcurrentU3Ek__BackingField,
	EventSystem_t326_CustomAttributesCacheGenerator_EventSystem_get_current_m1879,
	EventSystem_t326_CustomAttributesCacheGenerator_EventSystem_set_current_m1880,
	EventSystem_t326_CustomAttributesCacheGenerator_EventSystem_t326____lastSelectedGameObject_PropertyInfo,
	EventTrigger_t460_CustomAttributesCacheGenerator,
	EventTrigger_t460_CustomAttributesCacheGenerator_m_Delegates,
	EventTrigger_t460_CustomAttributesCacheGenerator_delegates,
	ExecuteEvents_t481_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache13,
	ExecuteEvents_t481_CustomAttributesCacheGenerator_ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m1964,
	AxisEventData_t487_CustomAttributesCacheGenerator_U3CmoveVectorU3Ek__BackingField,
	AxisEventData_t487_CustomAttributesCacheGenerator_U3CmoveDirU3Ek__BackingField,
	AxisEventData_t487_CustomAttributesCacheGenerator_AxisEventData_get_moveVector_m1989,
	AxisEventData_t487_CustomAttributesCacheGenerator_AxisEventData_set_moveVector_m1990,
	AxisEventData_t487_CustomAttributesCacheGenerator_AxisEventData_get_moveDir_m1991,
	AxisEventData_t487_CustomAttributesCacheGenerator_AxisEventData_set_moveDir_m1992,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CpointerEnterU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3ClastPressU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CrawPointerPressU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CpointerDragU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CpointerCurrentRaycastU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CpointerPressRaycastU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CeligibleForClickU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CpointerIdU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CpositionU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CdeltaU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CpressPositionU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CworldPositionU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CworldNormalU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CclickTimeU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CclickCountU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CscrollDeltaU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CuseDragThresholdU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CdraggingU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_U3CbuttonU3Ek__BackingField,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pointerEnter_m2000,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pointerEnter_m2001,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_lastPress_m2002,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_lastPress_m2003,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_rawPointerPress_m2004,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_rawPointerPress_m2005,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pointerDrag_m2006,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pointerDrag_m2007,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pointerCurrentRaycast_m2008,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pointerCurrentRaycast_m2009,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pointerPressRaycast_m2010,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pointerPressRaycast_m2011,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_eligibleForClick_m2012,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_eligibleForClick_m2013,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pointerId_m725,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pointerId_m2014,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_position_m711,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_position_m1732,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_delta_m2015,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_delta_m2016,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_pressPosition_m2017,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_pressPosition_m1731,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_worldPosition_m2018,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_worldPosition_m2019,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_worldNormal_m2020,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_worldNormal_m2021,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_clickTime_m2022,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_clickTime_m2023,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_clickCount_m2024,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_clickCount_m2025,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_scrollDelta_m2026,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_scrollDelta_m2027,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_useDragThreshold_m2028,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_useDragThreshold_m2029,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_dragging_m2030,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_dragging_m2031,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_get_button_m2032,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_set_button_m2033,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_t215____worldPosition_PropertyInfo,
	PointerEventData_t215_CustomAttributesCacheGenerator_PointerEventData_t215____worldNormal_PropertyInfo,
	BaseInputModule_t452_CustomAttributesCacheGenerator,
	StandaloneInputModule_t498_CustomAttributesCacheGenerator,
	StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_HorizontalAxis,
	StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_VerticalAxis,
	StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_SubmitButton,
	StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_CancelButton,
	StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_InputActionsPerSecond,
	StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_RepeatDelay,
	StandaloneInputModule_t498_CustomAttributesCacheGenerator_m_AllowActivationOnMobileDevice,
	StandaloneInputModule_t498_CustomAttributesCacheGenerator_StandaloneInputModule_t498____inputMode_PropertyInfo,
	InputMode_t497_CustomAttributesCacheGenerator,
	TouchInputModule_t499_CustomAttributesCacheGenerator,
	TouchInputModule_t499_CustomAttributesCacheGenerator_m_AllowActivationOnStandalone,
	BaseRaycaster_t485_CustomAttributesCacheGenerator_BaseRaycaster_t485____priority_PropertyInfo,
	Physics2DRaycaster_t500_CustomAttributesCacheGenerator,
	PhysicsRaycaster_t501_CustomAttributesCacheGenerator,
	PhysicsRaycaster_t501_CustomAttributesCacheGenerator_m_EventMask,
	PhysicsRaycaster_t501_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	PhysicsRaycaster_t501_CustomAttributesCacheGenerator_PhysicsRaycaster_U3CRaycastU3Em__1_m2142,
	TweenRunner_1_t722_CustomAttributesCacheGenerator_TweenRunner_1_Start_m3515,
	U3CStartU3Ec__Iterator0_t723_CustomAttributesCacheGenerator,
	U3CStartU3Ec__Iterator0_t723_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3519,
	U3CStartU3Ec__Iterator0_t723_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3520,
	U3CStartU3Ec__Iterator0_t723_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Dispose_m3522,
	U3CStartU3Ec__Iterator0_t723_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Reset_m3523,
	AnimationTriggers_t507_CustomAttributesCacheGenerator_m_NormalTrigger,
	AnimationTriggers_t507_CustomAttributesCacheGenerator_m_HighlightedTrigger,
	AnimationTriggers_t507_CustomAttributesCacheGenerator_m_PressedTrigger,
	AnimationTriggers_t507_CustomAttributesCacheGenerator_m_DisabledTrigger,
	Button_t324_CustomAttributesCacheGenerator,
	Button_t324_CustomAttributesCacheGenerator_m_OnClick,
	Button_t324_CustomAttributesCacheGenerator_Button_OnFinishSubmit_m2180,
	U3COnFinishSubmitU3Ec__Iterator1_t509_CustomAttributesCacheGenerator,
	U3COnFinishSubmitU3Ec__Iterator1_t509_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2170,
	U3COnFinishSubmitU3Ec__Iterator1_t509_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2171,
	U3COnFinishSubmitU3Ec__Iterator1_t509_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Dispose_m2173,
	U3COnFinishSubmitU3Ec__Iterator1_t509_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Reset_m2174,
	CanvasUpdateRegistry_t512_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	CanvasUpdateRegistry_t512_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7,
	CanvasUpdateRegistry_t512_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m2197,
	CanvasUpdateRegistry_t512_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m2198,
	ColorBlock_t516_CustomAttributesCacheGenerator_m_NormalColor,
	ColorBlock_t516_CustomAttributesCacheGenerator_m_HighlightedColor,
	ColorBlock_t516_CustomAttributesCacheGenerator_m_PressedColor,
	ColorBlock_t516_CustomAttributesCacheGenerator_m_DisabledColor,
	ColorBlock_t516_CustomAttributesCacheGenerator_m_ColorMultiplier,
	ColorBlock_t516_CustomAttributesCacheGenerator_m_FadeDuration,
	FontData_t518_CustomAttributesCacheGenerator_m_Font,
	FontData_t518_CustomAttributesCacheGenerator_m_FontSize,
	FontData_t518_CustomAttributesCacheGenerator_m_FontStyle,
	FontData_t518_CustomAttributesCacheGenerator_m_BestFit,
	FontData_t518_CustomAttributesCacheGenerator_m_MinSize,
	FontData_t518_CustomAttributesCacheGenerator_m_MaxSize,
	FontData_t518_CustomAttributesCacheGenerator_m_Alignment,
	FontData_t518_CustomAttributesCacheGenerator_m_RichText,
	FontData_t518_CustomAttributesCacheGenerator_m_HorizontalOverflow,
	FontData_t518_CustomAttributesCacheGenerator_m_VerticalOverflow,
	FontData_t518_CustomAttributesCacheGenerator_m_LineSpacing,
	Graphic_t418_CustomAttributesCacheGenerator,
	Graphic_t418_CustomAttributesCacheGenerator_m_Material,
	Graphic_t418_CustomAttributesCacheGenerator_m_Color,
	Graphic_t418_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheE,
	Graphic_t418_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheF,
	Graphic_t418_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__4_m2285,
	Graphic_t418_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__5_m2286,
	GraphicRaycaster_t325_CustomAttributesCacheGenerator,
	GraphicRaycaster_t325_CustomAttributesCacheGenerator_m_IgnoreReversedGraphics,
	GraphicRaycaster_t325_CustomAttributesCacheGenerator_m_BlockingObjects,
	GraphicRaycaster_t325_CustomAttributesCacheGenerator_m_BlockingMask,
	GraphicRaycaster_t325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	GraphicRaycaster_t325_CustomAttributesCacheGenerator_GraphicRaycaster_U3CRaycastU3Em__6_m2301,
	Image_t48_CustomAttributesCacheGenerator,
	Image_t48_CustomAttributesCacheGenerator_m_Sprite,
	Image_t48_CustomAttributesCacheGenerator_m_Type,
	Image_t48_CustomAttributesCacheGenerator_m_PreserveAspect,
	Image_t48_CustomAttributesCacheGenerator_m_FillCenter,
	Image_t48_CustomAttributesCacheGenerator_m_FillMethod,
	Image_t48_CustomAttributesCacheGenerator_m_FillAmount,
	Image_t48_CustomAttributesCacheGenerator_m_FillClockwise,
	Image_t48_CustomAttributesCacheGenerator_m_FillOrigin,
	InputField_t550_CustomAttributesCacheGenerator,
	InputField_t550_CustomAttributesCacheGenerator_m_TextComponent,
	InputField_t550_CustomAttributesCacheGenerator_m_Placeholder,
	InputField_t550_CustomAttributesCacheGenerator_m_ContentType,
	InputField_t550_CustomAttributesCacheGenerator_m_InputType,
	InputField_t550_CustomAttributesCacheGenerator_m_AsteriskChar,
	InputField_t550_CustomAttributesCacheGenerator_m_KeyboardType,
	InputField_t550_CustomAttributesCacheGenerator_m_LineType,
	InputField_t550_CustomAttributesCacheGenerator_m_HideMobileInput,
	InputField_t550_CustomAttributesCacheGenerator_m_CharacterValidation,
	InputField_t550_CustomAttributesCacheGenerator_m_CharacterLimit,
	InputField_t550_CustomAttributesCacheGenerator_m_EndEdit,
	InputField_t550_CustomAttributesCacheGenerator_m_OnValueChange,
	InputField_t550_CustomAttributesCacheGenerator_m_OnValidateInput,
	InputField_t550_CustomAttributesCacheGenerator_m_SelectionColor,
	InputField_t550_CustomAttributesCacheGenerator_m_Text,
	InputField_t550_CustomAttributesCacheGenerator_m_CaretBlinkRate,
	InputField_t550_CustomAttributesCacheGenerator_InputField_CaretBlink_m2425,
	InputField_t550_CustomAttributesCacheGenerator_InputField_MouseDragOutsideRect_m2442,
	InputField_t550_CustomAttributesCacheGenerator_InputField_t550_InputField_SetToCustomIfContentTypeIsNot_m2493_Arg0_ParameterInfo,
	U3CCaretBlinkU3Ec__Iterator2_t551_CustomAttributesCacheGenerator,
	U3CCaretBlinkU3Ec__Iterator2_t551_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2362,
	U3CCaretBlinkU3Ec__Iterator2_t551_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2363,
	U3CCaretBlinkU3Ec__Iterator2_t551_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Dispose_m2365,
	U3CCaretBlinkU3Ec__Iterator2_t551_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Reset_m2366,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t552_CustomAttributesCacheGenerator,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t552_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2368,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t552_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m2369,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t552_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m2371,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t552_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m2372,
	Navigation_t563_CustomAttributesCacheGenerator_m_Mode,
	Navigation_t563_CustomAttributesCacheGenerator_m_SelectOnUp,
	Navigation_t563_CustomAttributesCacheGenerator_m_SelectOnDown,
	Navigation_t563_CustomAttributesCacheGenerator_m_SelectOnLeft,
	Navigation_t563_CustomAttributesCacheGenerator_m_SelectOnRight,
	Mode_t562_CustomAttributesCacheGenerator,
	RawImage_t564_CustomAttributesCacheGenerator,
	RawImage_t564_CustomAttributesCacheGenerator_m_Texture,
	RawImage_t564_CustomAttributesCacheGenerator_m_UVRect,
	Scrollbar_t569_CustomAttributesCacheGenerator,
	Scrollbar_t569_CustomAttributesCacheGenerator_m_HandleRect,
	Scrollbar_t569_CustomAttributesCacheGenerator_m_Direction,
	Scrollbar_t569_CustomAttributesCacheGenerator_m_Value,
	Scrollbar_t569_CustomAttributesCacheGenerator_m_Size,
	Scrollbar_t569_CustomAttributesCacheGenerator_m_NumberOfSteps,
	Scrollbar_t569_CustomAttributesCacheGenerator_m_OnValueChanged,
	Scrollbar_t569_CustomAttributesCacheGenerator_Scrollbar_ClickRepeat_m2568,
	U3CClickRepeatU3Ec__Iterator4_t570_CustomAttributesCacheGenerator,
	U3CClickRepeatU3Ec__Iterator4_t570_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2534,
	U3CClickRepeatU3Ec__Iterator4_t570_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m2535,
	U3CClickRepeatU3Ec__Iterator4_t570_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Dispose_m2537,
	U3CClickRepeatU3Ec__Iterator4_t570_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Reset_m2538,
	ScrollRect_t575_CustomAttributesCacheGenerator,
	ScrollRect_t575_CustomAttributesCacheGenerator_m_Content,
	ScrollRect_t575_CustomAttributesCacheGenerator_m_Horizontal,
	ScrollRect_t575_CustomAttributesCacheGenerator_m_Vertical,
	ScrollRect_t575_CustomAttributesCacheGenerator_m_MovementType,
	ScrollRect_t575_CustomAttributesCacheGenerator_m_Elasticity,
	ScrollRect_t575_CustomAttributesCacheGenerator_m_Inertia,
	ScrollRect_t575_CustomAttributesCacheGenerator_m_DecelerationRate,
	ScrollRect_t575_CustomAttributesCacheGenerator_m_ScrollSensitivity,
	ScrollRect_t575_CustomAttributesCacheGenerator_m_HorizontalScrollbar,
	ScrollRect_t575_CustomAttributesCacheGenerator_m_VerticalScrollbar,
	ScrollRect_t575_CustomAttributesCacheGenerator_m_OnValueChanged,
	Selectable_t510_CustomAttributesCacheGenerator,
	Selectable_t510_CustomAttributesCacheGenerator_m_Navigation,
	Selectable_t510_CustomAttributesCacheGenerator_m_Transition,
	Selectable_t510_CustomAttributesCacheGenerator_m_Colors,
	Selectable_t510_CustomAttributesCacheGenerator_m_SpriteState,
	Selectable_t510_CustomAttributesCacheGenerator_m_AnimationTriggers,
	Selectable_t510_CustomAttributesCacheGenerator_m_Interactable,
	Selectable_t510_CustomAttributesCacheGenerator_m_TargetGraphic,
	Selectable_t510_CustomAttributesCacheGenerator_U3CisPointerInsideU3Ek__BackingField,
	Selectable_t510_CustomAttributesCacheGenerator_U3CisPointerDownU3Ek__BackingField,
	Selectable_t510_CustomAttributesCacheGenerator_U3ChasSelectionU3Ek__BackingField,
	Selectable_t510_CustomAttributesCacheGenerator_Selectable_get_isPointerInside_m2651,
	Selectable_t510_CustomAttributesCacheGenerator_Selectable_set_isPointerInside_m2652,
	Selectable_t510_CustomAttributesCacheGenerator_Selectable_get_isPointerDown_m2653,
	Selectable_t510_CustomAttributesCacheGenerator_Selectable_set_isPointerDown_m2654,
	Selectable_t510_CustomAttributesCacheGenerator_Selectable_get_hasSelection_m2655,
	Selectable_t510_CustomAttributesCacheGenerator_Selectable_set_hasSelection_m2656,
	Selectable_t510_CustomAttributesCacheGenerator_Selectable_IsPressed_m2682,
	Slider_t585_CustomAttributesCacheGenerator,
	Slider_t585_CustomAttributesCacheGenerator_m_FillRect,
	Slider_t585_CustomAttributesCacheGenerator_m_HandleRect,
	Slider_t585_CustomAttributesCacheGenerator_m_Direction,
	Slider_t585_CustomAttributesCacheGenerator_m_MinValue,
	Slider_t585_CustomAttributesCacheGenerator_m_MaxValue,
	Slider_t585_CustomAttributesCacheGenerator_m_WholeNumbers,
	Slider_t585_CustomAttributesCacheGenerator_m_Value,
	Slider_t585_CustomAttributesCacheGenerator_m_OnValueChanged,
	SpriteState_t580_CustomAttributesCacheGenerator_m_HighlightedSprite,
	SpriteState_t580_CustomAttributesCacheGenerator_m_PressedSprite,
	SpriteState_t580_CustomAttributesCacheGenerator_m_DisabledSprite,
	Text_t316_CustomAttributesCacheGenerator,
	Text_t316_CustomAttributesCacheGenerator_m_FontData,
	Text_t316_CustomAttributesCacheGenerator_m_Text,
	Toggle_t312_CustomAttributesCacheGenerator,
	Toggle_t312_CustomAttributesCacheGenerator_m_Group,
	Toggle_t312_CustomAttributesCacheGenerator_m_IsOn,
	ToggleGroup_t592_CustomAttributesCacheGenerator,
	ToggleGroup_t592_CustomAttributesCacheGenerator_m_AllowSwitchOff,
	ToggleGroup_t592_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	ToggleGroup_t592_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	ToggleGroup_t592_CustomAttributesCacheGenerator_ToggleGroup_U3CAnyTogglesOnU3Em__7_m2824,
	ToggleGroup_t592_CustomAttributesCacheGenerator_ToggleGroup_U3CActiveTogglesU3Em__8_m2825,
	AspectRatioFitter_t597_CustomAttributesCacheGenerator,
	AspectRatioFitter_t597_CustomAttributesCacheGenerator_m_AspectMode,
	AspectRatioFitter_t597_CustomAttributesCacheGenerator_m_AspectRatio,
	CanvasScaler_t601_CustomAttributesCacheGenerator,
	CanvasScaler_t601_CustomAttributesCacheGenerator_m_UiScaleMode,
	CanvasScaler_t601_CustomAttributesCacheGenerator_m_ReferencePixelsPerUnit,
	CanvasScaler_t601_CustomAttributesCacheGenerator_m_ScaleFactor,
	CanvasScaler_t601_CustomAttributesCacheGenerator_m_ReferenceResolution,
	CanvasScaler_t601_CustomAttributesCacheGenerator_m_ScreenMatchMode,
	CanvasScaler_t601_CustomAttributesCacheGenerator_m_MatchWidthOrHeight,
	CanvasScaler_t601_CustomAttributesCacheGenerator_m_PhysicalUnit,
	CanvasScaler_t601_CustomAttributesCacheGenerator_m_FallbackScreenDPI,
	CanvasScaler_t601_CustomAttributesCacheGenerator_m_DefaultSpriteDPI,
	CanvasScaler_t601_CustomAttributesCacheGenerator_m_DynamicPixelsPerUnit,
	ContentSizeFitter_t603_CustomAttributesCacheGenerator,
	ContentSizeFitter_t603_CustomAttributesCacheGenerator_m_HorizontalFit,
	ContentSizeFitter_t603_CustomAttributesCacheGenerator_m_VerticalFit,
	GridLayoutGroup_t607_CustomAttributesCacheGenerator,
	GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_StartCorner,
	GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_StartAxis,
	GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_CellSize,
	GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_Spacing,
	GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_Constraint,
	GridLayoutGroup_t607_CustomAttributesCacheGenerator_m_ConstraintCount,
	HorizontalLayoutGroup_t609_CustomAttributesCacheGenerator,
	HorizontalOrVerticalLayoutGroup_t610_CustomAttributesCacheGenerator_m_Spacing,
	HorizontalOrVerticalLayoutGroup_t610_CustomAttributesCacheGenerator_m_ChildForceExpandWidth,
	HorizontalOrVerticalLayoutGroup_t610_CustomAttributesCacheGenerator_m_ChildForceExpandHeight,
	LayoutElement_t611_CustomAttributesCacheGenerator,
	LayoutElement_t611_CustomAttributesCacheGenerator_m_IgnoreLayout,
	LayoutElement_t611_CustomAttributesCacheGenerator_m_MinWidth,
	LayoutElement_t611_CustomAttributesCacheGenerator_m_MinHeight,
	LayoutElement_t611_CustomAttributesCacheGenerator_m_PreferredWidth,
	LayoutElement_t611_CustomAttributesCacheGenerator_m_PreferredHeight,
	LayoutElement_t611_CustomAttributesCacheGenerator_m_FlexibleWidth,
	LayoutElement_t611_CustomAttributesCacheGenerator_m_FlexibleHeight,
	LayoutGroup_t608_CustomAttributesCacheGenerator,
	LayoutGroup_t608_CustomAttributesCacheGenerator_m_Padding,
	LayoutGroup_t608_CustomAttributesCacheGenerator_m_ChildAlignment,
	LayoutRebuilder_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	LayoutRebuilder_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	LayoutRebuilder_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	LayoutRebuilder_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5,
	LayoutRebuilder_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	LayoutRebuilder_t615_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__9_m2985,
	LayoutRebuilder_t615_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__A_m2986,
	LayoutRebuilder_t615_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__B_m2987,
	LayoutRebuilder_t615_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__C_m2988,
	LayoutRebuilder_t615_CustomAttributesCacheGenerator_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m2989,
	LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5,
	LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	LayoutUtility_t617_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7,
	LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinWidthU3Em__E_m3001,
	LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__F_m3002,
	LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__10_m3003,
	LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m3004,
	LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinHeightU3Em__12_m3005,
	LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__13_m3006,
	LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__14_m3007,
	LayoutUtility_t617_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m3008,
	VerticalLayoutGroup_t618_CustomAttributesCacheGenerator,
	Mask_t619_CustomAttributesCacheGenerator,
	Mask_t619_CustomAttributesCacheGenerator_m_ShowMaskGraphic,
	IndexedSet_1_t729_CustomAttributesCacheGenerator,
	CanvasListPool_t622_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	CanvasListPool_t622_CustomAttributesCacheGenerator_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m3030,
	ComponentListPool_t625_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	ComponentListPool_t625_CustomAttributesCacheGenerator_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m3034,
	ObjectPool_1_t730_CustomAttributesCacheGenerator_U3CcountAllU3Ek__BackingField,
	ObjectPool_1_t730_CustomAttributesCacheGenerator_ObjectPool_1_get_countAll_m3573,
	ObjectPool_1_t730_CustomAttributesCacheGenerator_ObjectPool_1_set_countAll_m3574,
	BaseVertexEffect_t626_CustomAttributesCacheGenerator,
	Outline_t627_CustomAttributesCacheGenerator,
	PositionAsUV1_t629_CustomAttributesCacheGenerator,
	Shadow_t628_CustomAttributesCacheGenerator,
	Shadow_t628_CustomAttributesCacheGenerator_m_EffectColor,
	Shadow_t628_CustomAttributesCacheGenerator_m_EffectDistance,
	Shadow_t628_CustomAttributesCacheGenerator_m_UseGraphicAlpha,
};
