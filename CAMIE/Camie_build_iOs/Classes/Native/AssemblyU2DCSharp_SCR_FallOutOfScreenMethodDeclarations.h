﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_FallOutOfScreen
struct SCR_FallOutOfScreen_t394;
// UnityEngine.Collider
struct Collider_t138;

// System.Void SCR_FallOutOfScreen::.ctor()
extern "C" void SCR_FallOutOfScreen__ctor_m1531 (SCR_FallOutOfScreen_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FallOutOfScreen::OnBecameVisible()
extern "C" void SCR_FallOutOfScreen_OnBecameVisible_m1532 (SCR_FallOutOfScreen_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FallOutOfScreen::OnTriggerEnter(UnityEngine.Collider)
extern "C" void SCR_FallOutOfScreen_OnTriggerEnter_m1533 (SCR_FallOutOfScreen_t394 * __this, Collider_t138 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_FallOutOfScreen::CallAvatarRespawn()
extern "C" void SCR_FallOutOfScreen_CallAvatarRespawn_m1534 (SCR_FallOutOfScreen_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
