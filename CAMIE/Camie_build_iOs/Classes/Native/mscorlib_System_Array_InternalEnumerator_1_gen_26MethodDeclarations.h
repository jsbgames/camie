﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.ParticleCollisionEvent>
struct InternalEnumerator_1_t2873;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.ParticleCollisionEvent
#include "UnityEngine_UnityEngine_ParticleCollisionEvent.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.ParticleCollisionEvent>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m14268_gshared (InternalEnumerator_1_t2873 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m14268(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2873 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14268_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ParticleCollisionEvent>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14269_gshared (InternalEnumerator_1_t2873 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14269(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2873 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14269_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ParticleCollisionEvent>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m14270_gshared (InternalEnumerator_1_t2873 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14270(__this, method) (( void (*) (InternalEnumerator_1_t2873 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14270_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ParticleCollisionEvent>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m14271_gshared (InternalEnumerator_1_t2873 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14271(__this, method) (( bool (*) (InternalEnumerator_1_t2873 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14271_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ParticleCollisionEvent>::get_Current()
extern "C" ParticleCollisionEvent_t160  InternalEnumerator_1_get_Current_m14272_gshared (InternalEnumerator_1_t2873 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14272(__this, method) (( ParticleCollisionEvent_t160  (*) (InternalEnumerator_1_t2873 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14272_gshared)(__this, method)
