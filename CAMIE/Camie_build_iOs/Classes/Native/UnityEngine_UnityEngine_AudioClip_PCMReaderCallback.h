﻿#pragma once
#include <stdint.h>
// System.Single[]
struct SingleU5BU5D_t211;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.AudioClip/PCMReaderCallback
struct  PCMReaderCallback_t880  : public MulticastDelegate_t549
{
};
