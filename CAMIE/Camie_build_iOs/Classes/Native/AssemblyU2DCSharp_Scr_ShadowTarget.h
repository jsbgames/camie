﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t1;
// SCR_Bird
struct SCR_Bird_t390;
// UnityEngine.Projector
struct Projector_t395;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Scr_ShadowTarget
struct  Scr_ShadowTarget_t392  : public MonoBehaviour_t3
{
	// UnityEngine.Vector3 Scr_ShadowTarget::camieOffset
	Vector3_t4  ___camieOffset_2;
	// System.Boolean Scr_ShadowTarget::canMove
	bool ___canMove_3;
	// System.Boolean Scr_ShadowTarget::canGrow
	bool ___canGrow_4;
	// UnityEngine.Transform Scr_ShadowTarget::camieTransform
	Transform_t1 * ___camieTransform_5;
	// SCR_Bird Scr_ShadowTarget::attackCount
	SCR_Bird_t390 * ___attackCount_6;
	// UnityEngine.Projector Scr_ShadowTarget::projector
	Projector_t395 * ___projector_7;
	// System.Single Scr_ShadowTarget::growSpeed
	float ___growSpeed_8;
	// System.Single Scr_ShadowTarget::maxSize
	float ___maxSize_9;
};
