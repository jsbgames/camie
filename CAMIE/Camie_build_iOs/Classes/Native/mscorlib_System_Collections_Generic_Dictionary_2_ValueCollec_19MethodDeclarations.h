﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Byte>
struct Enumerator_t2992;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte>
struct Dictionary_2_t2983;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15978_gshared (Enumerator_t2992 * __this, Dictionary_2_t2983 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15978(__this, ___host, method) (( void (*) (Enumerator_t2992 *, Dictionary_2_t2983 *, const MethodInfo*))Enumerator__ctor_m15978_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15979_gshared (Enumerator_t2992 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15979(__this, method) (( Object_t * (*) (Enumerator_t2992 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15979_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m15980_gshared (Enumerator_t2992 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15980(__this, method) (( void (*) (Enumerator_t2992 *, const MethodInfo*))Enumerator_Dispose_m15980_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15981_gshared (Enumerator_t2992 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15981(__this, method) (( bool (*) (Enumerator_t2992 *, const MethodInfo*))Enumerator_MoveNext_m15981_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Byte>::get_Current()
extern "C" uint8_t Enumerator_get_Current_m15982_gshared (Enumerator_t2992 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15982(__this, method) (( uint8_t (*) (Enumerator_t2992 *, const MethodInfo*))Enumerator_get_Current_m15982_gshared)(__this, method)
