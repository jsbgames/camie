﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t140;
// System.Object
struct Object_t;

// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m398 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m399 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m400 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m401 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m402 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m403 (U3CStartU3Ec__Iterator0_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
