﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t432;
// System.String
struct String_t;

// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
extern "C" void DefaultMemberAttribute__ctor_m1832 (DefaultMemberAttribute_t432 * __this, String_t* ___memberName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.DefaultMemberAttribute::get_MemberName()
extern "C" String_t* DefaultMemberAttribute_get_MemberName_m8411 (DefaultMemberAttribute_t432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
