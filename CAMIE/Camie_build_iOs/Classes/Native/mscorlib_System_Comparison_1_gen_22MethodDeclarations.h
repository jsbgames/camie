﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.Canvas>
struct Comparison_1_t3114;
// System.Object
struct Object_t;
// UnityEngine.Canvas
struct Canvas_t414;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<UnityEngine.Canvas>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m17866(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3114 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m14080_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.Canvas>::Invoke(T,T)
#define Comparison_1_Invoke_m17867(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3114 *, Canvas_t414 *, Canvas_t414 *, const MethodInfo*))Comparison_1_Invoke_m14081_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Canvas>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m17868(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3114 *, Canvas_t414 *, Canvas_t414 *, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m14082_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.Canvas>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m17869(__this, ___result, method) (( int32_t (*) (Comparison_1_t3114 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m14083_gshared)(__this, ___result, method)
