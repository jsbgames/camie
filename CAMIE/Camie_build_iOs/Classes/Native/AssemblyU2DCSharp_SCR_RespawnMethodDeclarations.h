﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Respawn
struct SCR_Respawn_t344;

// System.Void SCR_Respawn::.ctor()
extern "C" void SCR_Respawn__ctor_m1279 (SCR_Respawn_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Respawn::Start()
extern "C" void SCR_Respawn_Start_m1280 (SCR_Respawn_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Respawn::StartRespawn()
extern "C" void SCR_Respawn_StartRespawn_m1281 (SCR_Respawn_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Respawn::EndRespawn()
extern "C" void SCR_Respawn_EndRespawn_m1282 (SCR_Respawn_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Respawn::get_IsRespawning()
extern "C" bool SCR_Respawn_get_IsRespawning_m1283 (SCR_Respawn_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Respawn::set_IsRespawning(System.Boolean)
extern "C" void SCR_Respawn_set_IsRespawning_m1284 (SCR_Respawn_t344 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
