﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA256
struct HMACSHA256_t2077;
// System.Byte[]
struct ByteU5BU5D_t850;

// System.Void System.Security.Cryptography.HMACSHA256::.ctor()
extern "C" void HMACSHA256__ctor_m11084 (HMACSHA256_t2077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA256::.ctor(System.Byte[])
extern "C" void HMACSHA256__ctor_m11085 (HMACSHA256_t2077 * __this, ByteU5BU5D_t850* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
