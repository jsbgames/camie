﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>
struct InternalEnumerator_1_t3479;
// System.Object
struct Object_t;
// System.Reflection.Emit.PropertyBuilder
struct PropertyBuilder_t1880;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22317(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3479 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22318(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3479 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>::Dispose()
#define InternalEnumerator_1_Dispose_m22319(__this, method) (( void (*) (InternalEnumerator_1_t3479 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22320(__this, method) (( bool (*) (InternalEnumerator_1_t3479 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>::get_Current()
#define InternalEnumerator_1_get_Current_m22321(__this, method) (( PropertyBuilder_t1880 * (*) (InternalEnumerator_1_t3479 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
