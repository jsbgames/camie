﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct MobileControlRig_t39;

// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::.ctor()
extern "C" void MobileControlRig__ctor_m127 (MobileControlRig_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::OnEnable()
extern "C" void MobileControlRig_OnEnable_m128 (MobileControlRig_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::CheckEnableControlRig()
extern "C" void MobileControlRig_CheckEnableControlRig_m129 (MobileControlRig_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::EnableControlRig(System.Boolean)
extern "C" void MobileControlRig_EnableControlRig_m130 (MobileControlRig_t39 * __this, bool ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
