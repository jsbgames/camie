﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t452;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.EventSystems.BaseInputModule>
struct  Comparison_1_t3019  : public MulticastDelegate_t549
{
};
