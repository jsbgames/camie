﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_LevelStartCountdown/<Rescale>c__IteratorA
struct U3CRescaleU3Ec__IteratorA_t371;
// System.Object
struct Object_t;

// System.Void SCR_LevelStartCountdown/<Rescale>c__IteratorA::.ctor()
extern "C" void U3CRescaleU3Ec__IteratorA__ctor_m1371 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_LevelStartCountdown/<Rescale>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CRescaleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1372 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_LevelStartCountdown/<Rescale>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CRescaleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1373 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_LevelStartCountdown/<Rescale>c__IteratorA::MoveNext()
extern "C" bool U3CRescaleU3Ec__IteratorA_MoveNext_m1374 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelStartCountdown/<Rescale>c__IteratorA::Dispose()
extern "C" void U3CRescaleU3Ec__IteratorA_Dispose_m1375 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelStartCountdown/<Rescale>c__IteratorA::Reset()
extern "C" void U3CRescaleU3Ec__IteratorA_Reset_m1376 (U3CRescaleU3Ec__IteratorA_t371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
