﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// EventSystemChecker/<Start>c__Iterator1
struct U3CStartU3Ec__Iterator1_t310;
// System.Object
struct Object_t;

// System.Void EventSystemChecker/<Start>c__Iterator1::.ctor()
extern "C" void U3CStartU3Ec__Iterator1__ctor_m1141 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EventSystemChecker/<Start>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1142 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EventSystemChecker/<Start>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1143 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventSystemChecker/<Start>c__Iterator1::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator1_MoveNext_m1144 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventSystemChecker/<Start>c__Iterator1::Dispose()
extern "C" void U3CStartU3Ec__Iterator1_Dispose_m1145 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventSystemChecker/<Start>c__Iterator1::Reset()
extern "C" void U3CStartU3Ec__Iterator1_Reset_m1146 (U3CStartU3Ec__Iterator1_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
