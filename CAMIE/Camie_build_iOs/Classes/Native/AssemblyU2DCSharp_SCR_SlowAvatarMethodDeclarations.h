﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_SlowAvatar
struct SCR_SlowAvatar_t337;
// UnityEngine.Collider
struct Collider_t138;

// System.Void SCR_SlowAvatar::.ctor()
extern "C" void SCR_SlowAvatar__ctor_m1221 (SCR_SlowAvatar_t337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SlowAvatar::TriggerEnterEffect()
// System.Void SCR_SlowAvatar::TriggerExitEffet()
// System.Void SCR_SlowAvatar::OnTriggerEnter(UnityEngine.Collider)
extern "C" void SCR_SlowAvatar_OnTriggerEnter_m1222 (SCR_SlowAvatar_t337 * __this, Collider_t138 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SlowAvatar::OnTriggerExit()
extern "C" void SCR_SlowAvatar_OnTriggerExit_m1223 (SCR_SlowAvatar_t337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
