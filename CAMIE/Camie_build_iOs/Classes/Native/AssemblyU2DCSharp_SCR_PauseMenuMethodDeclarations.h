﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_PauseMenu
struct SCR_PauseMenu_t374;

// System.Void SCR_PauseMenu::.ctor()
extern "C" void SCR_PauseMenu__ctor_m1399 (SCR_PauseMenu_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PauseMenu::Awake()
extern "C" void SCR_PauseMenu_Awake_m1400 (SCR_PauseMenu_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PauseMenu::OnEnable()
extern "C" void SCR_PauseMenu_OnEnable_m1401 (SCR_PauseMenu_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PauseMenu::PauseGame(System.Boolean)
extern "C" void SCR_PauseMenu_PauseGame_m1402 (SCR_PauseMenu_t374 * __this, bool ___pause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PauseMenu::ShowLadybugCollectibles(System.Int32)
extern "C" void SCR_PauseMenu_ShowLadybugCollectibles_m1403 (SCR_PauseMenu_t374 * __this, int32_t ___collected, const MethodInfo* method) IL2CPP_METHOD_ATTR;
