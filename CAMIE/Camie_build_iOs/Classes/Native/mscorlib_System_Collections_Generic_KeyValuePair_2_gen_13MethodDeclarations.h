﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>
struct KeyValuePair_2_t2984;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m15918_gshared (KeyValuePair_2_t2984 * __this, int32_t ___key, uint8_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m15918(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2984 *, int32_t, uint8_t, const MethodInfo*))KeyValuePair_2__ctor_m15918_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m15919_gshared (KeyValuePair_2_t2984 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m15919(__this, method) (( int32_t (*) (KeyValuePair_2_t2984 *, const MethodInfo*))KeyValuePair_2_get_Key_m15919_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m15920_gshared (KeyValuePair_2_t2984 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m15920(__this, ___value, method) (( void (*) (KeyValuePair_2_t2984 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m15920_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>::get_Value()
extern "C" uint8_t KeyValuePair_2_get_Value_m15921_gshared (KeyValuePair_2_t2984 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m15921(__this, method) (( uint8_t (*) (KeyValuePair_2_t2984 *, const MethodInfo*))KeyValuePair_2_get_Value_m15921_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m15922_gshared (KeyValuePair_2_t2984 * __this, uint8_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m15922(__this, ___value, method) (( void (*) (KeyValuePair_2_t2984 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Value_m15922_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m15923_gshared (KeyValuePair_2_t2984 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m15923(__this, method) (( String_t* (*) (KeyValuePair_2_t2984 *, const MethodInfo*))KeyValuePair_2_ToString_m15923_gshared)(__this, method)
