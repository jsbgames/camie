﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t506;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t645;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"

// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::get_startColor()
extern "C" Color_t65  ColorTween_get_startColor_m2144 (ColorTween_t506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_startColor(UnityEngine.Color)
extern "C" void ColorTween_set_startColor_m2145 (ColorTween_t506 * __this, Color_t65  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::get_targetColor()
extern "C" Color_t65  ColorTween_get_targetColor_m2146 (ColorTween_t506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_targetColor(UnityEngine.Color)
extern "C" void ColorTween_set_targetColor_m2147 (ColorTween_t506 * __this, Color_t65  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::get_tweenMode()
extern "C" int32_t ColorTween_get_tweenMode_m2148 (ColorTween_t506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_tweenMode(UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode)
extern "C" void ColorTween_set_tweenMode_m2149 (ColorTween_t506 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::get_duration()
extern "C" float ColorTween_get_duration_m2150 (ColorTween_t506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_duration(System.Single)
extern "C" void ColorTween_set_duration_m2151 (ColorTween_t506 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::get_ignoreTimeScale()
extern "C" bool ColorTween_get_ignoreTimeScale_m2152 (ColorTween_t506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_ignoreTimeScale(System.Boolean)
extern "C" void ColorTween_set_ignoreTimeScale_m2153 (ColorTween_t506 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::TweenValue(System.Single)
extern "C" void ColorTween_TweenValue_m2154 (ColorTween_t506 * __this, float ___floatPercentage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::AddOnChangedCallback(UnityEngine.Events.UnityAction`1<UnityEngine.Color>)
extern "C" void ColorTween_AddOnChangedCallback_m2155 (ColorTween_t506 * __this, UnityAction_1_t645 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::GetIgnoreTimescale()
extern "C" bool ColorTween_GetIgnoreTimescale_m2156 (ColorTween_t506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::GetDuration()
extern "C" float ColorTween_GetDuration_m2157 (ColorTween_t506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::ValidTarget()
extern "C" bool ColorTween_ValidTarget_m2158 (ColorTween_t506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
