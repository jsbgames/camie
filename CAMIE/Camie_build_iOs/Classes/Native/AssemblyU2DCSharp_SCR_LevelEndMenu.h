﻿#pragma once
#include <stdint.h>
// SCR_LevelEndMenu
struct SCR_LevelEndMenu_t367;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t368;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t369;
// SCR_Menu
#include "AssemblyU2DCSharp_SCR_Menu.h"
// SCR_LevelEndMenu
struct  SCR_LevelEndMenu_t367  : public SCR_Menu_t336
{
	// UnityEngine.Sprite[] SCR_LevelEndMenu::lockedLevels
	SpriteU5BU5D_t368* ___lockedLevels_6;
	// UnityEngine.Sprite[] SCR_LevelEndMenu::unlockedLevels
	SpriteU5BU5D_t368* ___unlockedLevels_7;
	// UnityEngine.UI.Text[] SCR_LevelEndMenu::newHighScoreText
	TextU5BU5D_t369* ___newHighScoreText_8;
	// System.Boolean SCR_LevelEndMenu::canGoToNext
	bool ___canGoToNext_9;
};
struct SCR_LevelEndMenu_t367_StaticFields{
	// SCR_LevelEndMenu SCR_LevelEndMenu::instance
	SCR_LevelEndMenu_t367 * ___instance_5;
};
