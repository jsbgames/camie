﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t850;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.IO.FileStream/ReadDelegate
struct  ReadDelegate_t1818  : public MulticastDelegate_t549
{
};
