﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SignatureDescription
struct SignatureDescription_t2102;
// System.String
struct String_t;

// System.Void System.Security.Cryptography.SignatureDescription::.ctor()
extern "C" void SignatureDescription__ctor_m11281 (SignatureDescription_t2102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_DeformatterAlgorithm(System.String)
extern "C" void SignatureDescription_set_DeformatterAlgorithm_m11282 (SignatureDescription_t2102 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_DigestAlgorithm(System.String)
extern "C" void SignatureDescription_set_DigestAlgorithm_m11283 (SignatureDescription_t2102 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_FormatterAlgorithm(System.String)
extern "C" void SignatureDescription_set_FormatterAlgorithm_m11284 (SignatureDescription_t2102 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_KeyAlgorithm(System.String)
extern "C" void SignatureDescription_set_KeyAlgorithm_m11285 (SignatureDescription_t2102 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
