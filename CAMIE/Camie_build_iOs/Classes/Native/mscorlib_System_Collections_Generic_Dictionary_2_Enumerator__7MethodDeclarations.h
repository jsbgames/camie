﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>
struct Enumerator_t2926;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>
struct Dictionary_2_t407;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>
struct Dictionary_2_t2906;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"
#define Enumerator__ctor_m15136(__this, ___dictionary, method) (( void (*) (Enumerator_t2926 *, Dictionary_2_t2906 *, const MethodInfo*))Enumerator__ctor_m13691_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15137(__this, method) (( Object_t * (*) (Enumerator_t2926 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13692_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15138(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t2926 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13693_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15139(__this, method) (( Object_t * (*) (Enumerator_t2926 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13694_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15140(__this, method) (( Object_t * (*) (Enumerator_t2926 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13695_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::MoveNext()
#define Enumerator_MoveNext_m15141(__this, method) (( bool (*) (Enumerator_t2926 *, const MethodInfo*))Enumerator_MoveNext_m13696_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::get_Current()
#define Enumerator_get_Current_m15142(__this, method) (( KeyValuePair_2_t2923  (*) (Enumerator_t2926 *, const MethodInfo*))Enumerator_get_Current_m13697_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15143(__this, method) (( String_t* (*) (Enumerator_t2926 *, const MethodInfo*))Enumerator_get_CurrentKey_m13698_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15144(__this, method) (( Dictionary_2_t407 * (*) (Enumerator_t2926 *, const MethodInfo*))Enumerator_get_CurrentValue_m13699_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::VerifyState()
#define Enumerator_VerifyState_m15145(__this, method) (( void (*) (Enumerator_t2926 *, const MethodInfo*))Enumerator_VerifyState_m13700_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15146(__this, method) (( void (*) (Enumerator_t2926 *, const MethodInfo*))Enumerator_VerifyCurrent_m13701_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::Dispose()
#define Enumerator_Dispose_m15147(__this, method) (( void (*) (Enumerator_t2926 *, const MethodInfo*))Enumerator_Dispose_m13702_gshared)(__this, method)
