﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.CLSCompliantAttribute
struct CLSCompliantAttribute_t1468;

// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
extern "C" void CLSCompliantAttribute__ctor_m6537 (CLSCompliantAttribute_t1468 * __this, bool ___isCompliant, const MethodInfo* method) IL2CPP_METHOD_ATTR;
