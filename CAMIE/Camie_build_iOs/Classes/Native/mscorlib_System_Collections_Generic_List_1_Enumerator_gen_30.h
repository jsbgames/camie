﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t1024;
// UnityEngine.ParticleSystem
struct ParticleSystem_t161;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>
struct  Enumerator_t3226 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::l
	List_1_t1024 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::current
	ParticleSystem_t161 * ___current_3;
};
