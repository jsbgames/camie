﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int64>
struct InternalEnumerator_1_t3257;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19966_gshared (InternalEnumerator_1_t3257 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19966(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3257 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19966_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19967_gshared (InternalEnumerator_1_t3257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19967(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3257 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19967_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19968_gshared (InternalEnumerator_1_t3257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19968(__this, method) (( void (*) (InternalEnumerator_1_t3257 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19968_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19969_gshared (InternalEnumerator_1_t3257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19969(__this, method) (( bool (*) (InternalEnumerator_1_t3257 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19969_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C" int64_t InternalEnumerator_1_get_Current_m19970_gshared (InternalEnumerator_1_t3257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19970(__this, method) (( int64_t (*) (InternalEnumerator_1_t3257 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19970_gshared)(__this, method)
