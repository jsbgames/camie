﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
struct Collection_1_t3104;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t555;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t3665;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t695;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Collection_1__ctor_m17719_gshared (Collection_1_t3104 * __this, const MethodInfo* method);
#define Collection_1__ctor_m17719(__this, method) (( void (*) (Collection_1_t3104 *, const MethodInfo*))Collection_1__ctor_m17719_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17720_gshared (Collection_1_t3104 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17720(__this, method) (( bool (*) (Collection_1_t3104 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17720_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17721_gshared (Collection_1_t3104 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m17721(__this, ___array, ___index, method) (( void (*) (Collection_1_t3104 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m17721_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m17722_gshared (Collection_1_t3104 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m17722(__this, method) (( Object_t * (*) (Collection_1_t3104 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m17722_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m17723_gshared (Collection_1_t3104 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m17723(__this, ___value, method) (( int32_t (*) (Collection_1_t3104 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m17723_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m17724_gshared (Collection_1_t3104 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m17724(__this, ___value, method) (( bool (*) (Collection_1_t3104 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m17724_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m17725_gshared (Collection_1_t3104 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m17725(__this, ___value, method) (( int32_t (*) (Collection_1_t3104 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m17725_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m17726_gshared (Collection_1_t3104 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m17726(__this, ___index, ___value, method) (( void (*) (Collection_1_t3104 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m17726_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m17727_gshared (Collection_1_t3104 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m17727(__this, ___value, method) (( void (*) (Collection_1_t3104 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m17727_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m17728_gshared (Collection_1_t3104 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m17728(__this, method) (( bool (*) (Collection_1_t3104 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m17728_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m17729_gshared (Collection_1_t3104 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m17729(__this, method) (( Object_t * (*) (Collection_1_t3104 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m17729_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m17730_gshared (Collection_1_t3104 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m17730(__this, method) (( bool (*) (Collection_1_t3104 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m17730_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m17731_gshared (Collection_1_t3104 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m17731(__this, method) (( bool (*) (Collection_1_t3104 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m17731_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m17732_gshared (Collection_1_t3104 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m17732(__this, ___index, method) (( Object_t * (*) (Collection_1_t3104 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m17732_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m17733_gshared (Collection_1_t3104 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m17733(__this, ___index, ___value, method) (( void (*) (Collection_1_t3104 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m17733_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Add(T)
extern "C" void Collection_1_Add_m17734_gshared (Collection_1_t3104 * __this, UIVertex_t556  ___item, const MethodInfo* method);
#define Collection_1_Add_m17734(__this, ___item, method) (( void (*) (Collection_1_t3104 *, UIVertex_t556 , const MethodInfo*))Collection_1_Add_m17734_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Clear()
extern "C" void Collection_1_Clear_m17735_gshared (Collection_1_t3104 * __this, const MethodInfo* method);
#define Collection_1_Clear_m17735(__this, method) (( void (*) (Collection_1_t3104 *, const MethodInfo*))Collection_1_Clear_m17735_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ClearItems()
extern "C" void Collection_1_ClearItems_m17736_gshared (Collection_1_t3104 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m17736(__this, method) (( void (*) (Collection_1_t3104 *, const MethodInfo*))Collection_1_ClearItems_m17736_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool Collection_1_Contains_m17737_gshared (Collection_1_t3104 * __this, UIVertex_t556  ___item, const MethodInfo* method);
#define Collection_1_Contains_m17737(__this, ___item, method) (( bool (*) (Collection_1_t3104 *, UIVertex_t556 , const MethodInfo*))Collection_1_Contains_m17737_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m17738_gshared (Collection_1_t3104 * __this, UIVertexU5BU5D_t555* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m17738(__this, ___array, ___index, method) (( void (*) (Collection_1_t3104 *, UIVertexU5BU5D_t555*, int32_t, const MethodInfo*))Collection_1_CopyTo_m17738_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m17739_gshared (Collection_1_t3104 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m17739(__this, method) (( Object_t* (*) (Collection_1_t3104 *, const MethodInfo*))Collection_1_GetEnumerator_m17739_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m17740_gshared (Collection_1_t3104 * __this, UIVertex_t556  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m17740(__this, ___item, method) (( int32_t (*) (Collection_1_t3104 *, UIVertex_t556 , const MethodInfo*))Collection_1_IndexOf_m17740_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m17741_gshared (Collection_1_t3104 * __this, int32_t ___index, UIVertex_t556  ___item, const MethodInfo* method);
#define Collection_1_Insert_m17741(__this, ___index, ___item, method) (( void (*) (Collection_1_t3104 *, int32_t, UIVertex_t556 , const MethodInfo*))Collection_1_Insert_m17741_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m17742_gshared (Collection_1_t3104 * __this, int32_t ___index, UIVertex_t556  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m17742(__this, ___index, ___item, method) (( void (*) (Collection_1_t3104 *, int32_t, UIVertex_t556 , const MethodInfo*))Collection_1_InsertItem_m17742_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool Collection_1_Remove_m17743_gshared (Collection_1_t3104 * __this, UIVertex_t556  ___item, const MethodInfo* method);
#define Collection_1_Remove_m17743(__this, ___item, method) (( bool (*) (Collection_1_t3104 *, UIVertex_t556 , const MethodInfo*))Collection_1_Remove_m17743_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m17744_gshared (Collection_1_t3104 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m17744(__this, ___index, method) (( void (*) (Collection_1_t3104 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m17744_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m17745_gshared (Collection_1_t3104 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m17745(__this, ___index, method) (( void (*) (Collection_1_t3104 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m17745_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t Collection_1_get_Count_m17746_gshared (Collection_1_t3104 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m17746(__this, method) (( int32_t (*) (Collection_1_t3104 *, const MethodInfo*))Collection_1_get_Count_m17746_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t556  Collection_1_get_Item_m17747_gshared (Collection_1_t3104 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m17747(__this, ___index, method) (( UIVertex_t556  (*) (Collection_1_t3104 *, int32_t, const MethodInfo*))Collection_1_get_Item_m17747_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m17748_gshared (Collection_1_t3104 * __this, int32_t ___index, UIVertex_t556  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m17748(__this, ___index, ___value, method) (( void (*) (Collection_1_t3104 *, int32_t, UIVertex_t556 , const MethodInfo*))Collection_1_set_Item_m17748_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m17749_gshared (Collection_1_t3104 * __this, int32_t ___index, UIVertex_t556  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m17749(__this, ___index, ___item, method) (( void (*) (Collection_1_t3104 *, int32_t, UIVertex_t556 , const MethodInfo*))Collection_1_SetItem_m17749_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m17750_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m17750(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m17750_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ConvertItem(System.Object)
extern "C" UIVertex_t556  Collection_1_ConvertItem_m17751_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m17751(__this /* static, unused */, ___item, method) (( UIVertex_t556  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m17751_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m17752_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m17752(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m17752_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m17753_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m17753(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m17753_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m17754_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m17754(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m17754_gshared)(__this /* static, unused */, ___list, method)
