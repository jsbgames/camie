﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t3502;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C" void DefaultComparer__ctor_m22495_gshared (DefaultComparer_t3502 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22495(__this, method) (( void (*) (DefaultComparer_t3502 *, const MethodInfo*))DefaultComparer__ctor_m22495_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m22496_gshared (DefaultComparer_t3502 * __this, DateTimeOffset_t1086  ___x, DateTimeOffset_t1086  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m22496(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3502 *, DateTimeOffset_t1086 , DateTimeOffset_t1086 , const MethodInfo*))DefaultComparer_Compare_m22496_gshared)(__this, ___x, ___y, method)
