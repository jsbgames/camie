﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<SCR_Menu>
struct Predicate_1_t3007;
// System.Object
struct Object_t;
// SCR_Menu
struct SCR_Menu_t336;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<SCR_Menu>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#define Predicate_1__ctor_m16145(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3007 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m14055_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<SCR_Menu>::Invoke(T)
#define Predicate_1_Invoke_m16146(__this, ___obj, method) (( bool (*) (Predicate_1_t3007 *, SCR_Menu_t336 *, const MethodInfo*))Predicate_1_Invoke_m14056_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<SCR_Menu>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m16147(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3007 *, SCR_Menu_t336 *, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m14057_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<SCR_Menu>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m16148(__this, ___result, method) (( bool (*) (Predicate_1_t3007 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m14058_gshared)(__this, ___result, method)
