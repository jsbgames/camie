﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t655;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t698;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t284;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1033;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2844;
// System.Object[]
struct ObjectU5BU5D_t224;
// System.Predicate`1<System.Object>
struct Predicate_1_t2846;
// System.Comparison`1<System.Object>
struct Comparison_1_t2852;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m5170_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1__ctor_m5170(__this, method) (( void (*) (List_1_t655 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m13913_gshared (List_1_t655 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m13913(__this, ___collection, method) (( void (*) (List_1_t655 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m13915_gshared (List_1_t655 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m13915(__this, ___capacity, method) (( void (*) (List_1_t655 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m13917_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m13917(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425(__this, method) (( Object_t* (*) (List_1_t655 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m5408_gshared (List_1_t655 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m5408(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t655 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m5404(__this, method) (( Object_t * (*) (List_1_t655 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m5413_gshared (List_1_t655 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m5413(__this, ___item, method) (( int32_t (*) (List_1_t655 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m5415_gshared (List_1_t655 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m5415(__this, ___item, method) (( bool (*) (List_1_t655 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m5416_gshared (List_1_t655 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m5416(__this, ___item, method) (( int32_t (*) (List_1_t655 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m5417_gshared (List_1_t655 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m5417(__this, ___index, ___item, method) (( void (*) (List_1_t655 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m5418_gshared (List_1_t655 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m5418(__this, ___item, method) (( void (*) (List_1_t655 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420(__this, method) (( bool (*) (List_1_t655 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m5406(__this, method) (( bool (*) (List_1_t655 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m5407(__this, method) (( Object_t * (*) (List_1_t655 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m5409(__this, method) (( bool (*) (List_1_t655 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m5410(__this, method) (( bool (*) (List_1_t655 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m5411_gshared (List_1_t655 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m5411(__this, ___index, method) (( Object_t * (*) (List_1_t655 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m5412_gshared (List_1_t655 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m5412(__this, ___index, ___value, method) (( void (*) (List_1_t655 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m5421_gshared (List_1_t655 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m5421(__this, ___item, method) (( void (*) (List_1_t655 *, Object_t *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m13935_gshared (List_1_t655 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m13935(__this, ___newCount, method) (( void (*) (List_1_t655 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m13937_gshared (List_1_t655 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m13937(__this, ___collection, method) (( void (*) (List_1_t655 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m13939_gshared (List_1_t655 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m13939(__this, ___enumerable, method) (( void (*) (List_1_t655 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m13941_gshared (List_1_t655 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m13941(__this, ___collection, method) (( void (*) (List_1_t655 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2844 * List_1_AsReadOnly_m13943_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m13943(__this, method) (( ReadOnlyCollection_1_t2844 * (*) (List_1_t655 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m5414_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_Clear_m5414(__this, method) (( void (*) (List_1_t655 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m5422_gshared (List_1_t655 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m5422(__this, ___item, method) (( bool (*) (List_1_t655 *, Object_t *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m5423_gshared (List_1_t655 * __this, ObjectU5BU5D_t224* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m5423(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t655 *, ObjectU5BU5D_t224*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern "C" Object_t * List_1_Find_m13948_gshared (List_1_t655 * __this, Predicate_1_t2846 * ___match, const MethodInfo* method);
#define List_1_Find_m13948(__this, ___match, method) (( Object_t * (*) (List_1_t655 *, Predicate_1_t2846 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m13950_gshared (Object_t * __this /* static, unused */, Predicate_1_t2846 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m13950(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2846 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m13952_gshared (List_1_t655 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2846 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m13952(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t655 *, int32_t, int32_t, Predicate_1_t2846 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2843  List_1_GetEnumerator_m13954_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m13954(__this, method) (( Enumerator_t2843  (*) (List_1_t655 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m5426_gshared (List_1_t655 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m5426(__this, ___item, method) (( int32_t (*) (List_1_t655 *, Object_t *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m13957_gshared (List_1_t655 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m13957(__this, ___start, ___delta, method) (( void (*) (List_1_t655 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m13959_gshared (List_1_t655 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m13959(__this, ___index, method) (( void (*) (List_1_t655 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m5427_gshared (List_1_t655 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m5427(__this, ___index, ___item, method) (( void (*) (List_1_t655 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m13962_gshared (List_1_t655 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m13962(__this, ___collection, method) (( void (*) (List_1_t655 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m5424_gshared (List_1_t655 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m5424(__this, ___item, method) (( bool (*) (List_1_t655 *, Object_t *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m13965_gshared (List_1_t655 * __this, Predicate_1_t2846 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m13965(__this, ___match, method) (( int32_t (*) (List_1_t655 *, Predicate_1_t2846 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m5419_gshared (List_1_t655 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m5419(__this, ___index, method) (( void (*) (List_1_t655 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C" void List_1_Reverse_m13968_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_Reverse_m13968(__this, method) (( void (*) (List_1_t655 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C" void List_1_Sort_m13970_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_Sort_m13970(__this, method) (( void (*) (List_1_t655 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m13972_gshared (List_1_t655 * __this, Comparison_1_t2852 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m13972(__this, ___comparison, method) (( void (*) (List_1_t655 *, Comparison_1_t2852 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t224* List_1_ToArray_m13973_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_ToArray_m13973(__this, method) (( ObjectU5BU5D_t224* (*) (List_1_t655 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C" void List_1_TrimExcess_m13975_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m13975(__this, method) (( void (*) (List_1_t655 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m13977_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m13977(__this, method) (( int32_t (*) (List_1_t655 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m13979_gshared (List_1_t655 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m13979(__this, ___value, method) (( void (*) (List_1_t655 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m5405_gshared (List_1_t655 * __this, const MethodInfo* method);
#define List_1_get_Count_m5405(__this, method) (( int32_t (*) (List_1_t655 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m5428_gshared (List_1_t655 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m5428(__this, ___index, method) (( Object_t * (*) (List_1_t655 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m5429_gshared (List_1_t655 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m5429(__this, ___index, ___value, method) (( void (*) (List_1_t655 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
