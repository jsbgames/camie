﻿#pragma once
#include <stdint.h>
// System.DBNull
struct DBNull_t2193;
// System.Object
#include "mscorlib_System_Object.h"
// System.DBNull
struct  DBNull_t2193  : public Object_t
{
};
struct DBNull_t2193_StaticFields{
	// System.DBNull System.DBNull::Value
	DBNull_t2193 * ___Value_0;
};
