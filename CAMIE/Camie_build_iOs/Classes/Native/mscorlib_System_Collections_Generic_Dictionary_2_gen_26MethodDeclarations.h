﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t3254;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1033;
// System.Collections.Generic.ICollection`1<System.Int64>
struct ICollection_1_t3739;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>
struct KeyCollection_t3258;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>
struct ValueCollection_t3262;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2812;
// System.Collections.Generic.IDictionary`2<System.Object,System.Int64>
struct IDictionary_2_t3743;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1043;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>[]
struct KeyValuePair_2U5BU5D_t3744;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>
struct IEnumerator_1_t3745;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1429;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__21.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor()
extern "C" void Dictionary_2__ctor_m19856_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m19856(__this, method) (( void (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2__ctor_m19856_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m19858_gshared (Dictionary_2_t3254 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m19858(__this, ___comparer, method) (( void (*) (Dictionary_2_t3254 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m19858_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m19860_gshared (Dictionary_2_t3254 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m19860(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3254 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m19860_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m19862_gshared (Dictionary_2_t3254 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m19862(__this, ___capacity, method) (( void (*) (Dictionary_2_t3254 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m19862_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m19864_gshared (Dictionary_2_t3254 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m19864(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3254 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m19864_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m19866_gshared (Dictionary_2_t3254 * __this, SerializationInfo_t1043 * ___info, StreamingContext_t1044  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m19866(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3254 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2__ctor_m19866_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m19868_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m19868(__this, method) (( Object_t* (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m19868_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m19870_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m19870(__this, method) (( Object_t* (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m19870_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m19872_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m19872(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3254 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m19872_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m19874_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m19874(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3254 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m19874_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m19876_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m19876(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3254 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m19876_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m19878_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m19878(__this, ___key, method) (( bool (*) (Dictionary_2_t3254 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m19878_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m19880_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m19880(__this, ___key, method) (( void (*) (Dictionary_2_t3254 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m19880_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19882_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19882(__this, method) (( bool (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19882_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19884_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19884(__this, method) (( Object_t * (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19884_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19886_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19886(__this, method) (( bool (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19886_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19888_gshared (Dictionary_2_t3254 * __this, KeyValuePair_2_t3255  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19888(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3254 *, KeyValuePair_2_t3255 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19888_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19890_gshared (Dictionary_2_t3254 * __this, KeyValuePair_2_t3255  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19890(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3254 *, KeyValuePair_2_t3255 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19890_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19892_gshared (Dictionary_2_t3254 * __this, KeyValuePair_2U5BU5D_t3744* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19892(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3254 *, KeyValuePair_2U5BU5D_t3744*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19892_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19894_gshared (Dictionary_2_t3254 * __this, KeyValuePair_2_t3255  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19894(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3254 *, KeyValuePair_2_t3255 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19894_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m19896_gshared (Dictionary_2_t3254 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m19896(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3254 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m19896_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19898_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19898(__this, method) (( Object_t * (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19898_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19900_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19900(__this, method) (( Object_t* (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19900_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19902_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19902(__this, method) (( Object_t * (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19902_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m19904_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m19904(__this, method) (( int32_t (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_get_Count_m19904_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Item(TKey)
extern "C" int64_t Dictionary_2_get_Item_m19906_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m19906(__this, ___key, method) (( int64_t (*) (Dictionary_2_t3254 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m19906_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m19908_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m19908(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3254 *, Object_t *, int64_t, const MethodInfo*))Dictionary_2_set_Item_m19908_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m19910_gshared (Dictionary_2_t3254 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m19910(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3254 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m19910_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m19912_gshared (Dictionary_2_t3254 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m19912(__this, ___size, method) (( void (*) (Dictionary_2_t3254 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m19912_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m19914_gshared (Dictionary_2_t3254 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m19914(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3254 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m19914_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3255  Dictionary_2_make_pair_m19916_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m19916(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3255  (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_make_pair_m19916_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m19918_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m19918(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_pick_key_m19918_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::pick_value(TKey,TValue)
extern "C" int64_t Dictionary_2_pick_value_m19920_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m19920(__this /* static, unused */, ___key, ___value, method) (( int64_t (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_pick_value_m19920_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m19922_gshared (Dictionary_2_t3254 * __this, KeyValuePair_2U5BU5D_t3744* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m19922(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3254 *, KeyValuePair_2U5BU5D_t3744*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m19922_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Resize()
extern "C" void Dictionary_2_Resize_m19924_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m19924(__this, method) (( void (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_Resize_m19924_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m19926_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m19926(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3254 *, Object_t *, int64_t, const MethodInfo*))Dictionary_2_Add_m19926_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Clear()
extern "C" void Dictionary_2_Clear_m19928_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m19928(__this, method) (( void (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_Clear_m19928_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m19930_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m19930(__this, ___key, method) (( bool (*) (Dictionary_2_t3254 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m19930_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m19932_gshared (Dictionary_2_t3254 * __this, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m19932(__this, ___value, method) (( bool (*) (Dictionary_2_t3254 *, int64_t, const MethodInfo*))Dictionary_2_ContainsValue_m19932_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m19934_gshared (Dictionary_2_t3254 * __this, SerializationInfo_t1043 * ___info, StreamingContext_t1044  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m19934(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3254 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2_GetObjectData_m19934_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m19936_gshared (Dictionary_2_t3254 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m19936(__this, ___sender, method) (( void (*) (Dictionary_2_t3254 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m19936_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m19938_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m19938(__this, ___key, method) (( bool (*) (Dictionary_2_t3254 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m19938_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m19940_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, int64_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m19940(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3254 *, Object_t *, int64_t*, const MethodInfo*))Dictionary_2_TryGetValue_m19940_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Keys()
extern "C" KeyCollection_t3258 * Dictionary_2_get_Keys_m19942_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m19942(__this, method) (( KeyCollection_t3258 * (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_get_Keys_m19942_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Values()
extern "C" ValueCollection_t3262 * Dictionary_2_get_Values_m19944_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m19944(__this, method) (( ValueCollection_t3262 * (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_get_Values_m19944_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m19946_gshared (Dictionary_2_t3254 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m19946(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3254 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m19946_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ToTValue(System.Object)
extern "C" int64_t Dictionary_2_ToTValue_m19948_gshared (Dictionary_2_t3254 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m19948(__this, ___value, method) (( int64_t (*) (Dictionary_2_t3254 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m19948_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m19950_gshared (Dictionary_2_t3254 * __this, KeyValuePair_2_t3255  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m19950(__this, ___pair, method) (( bool (*) (Dictionary_2_t3254 *, KeyValuePair_2_t3255 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m19950_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::GetEnumerator()
extern "C" Enumerator_t3260  Dictionary_2_GetEnumerator_m19952_gshared (Dictionary_2_t3254 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m19952(__this, method) (( Enumerator_t3260  (*) (Dictionary_2_t3254 *, const MethodInfo*))Dictionary_2_GetEnumerator_m19952_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1430  Dictionary_2_U3CCopyToU3Em__0_m19954_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m19954(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1430  (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m19954_gshared)(__this /* static, unused */, ___key, ___value, method)
