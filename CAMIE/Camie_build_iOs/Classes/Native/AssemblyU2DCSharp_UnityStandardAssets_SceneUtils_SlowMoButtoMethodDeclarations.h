﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.SceneUtils.SlowMoButton
struct SlowMoButton_t330;

// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::.ctor()
extern "C" void SlowMoButton__ctor_m1184 (SlowMoButton_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::Start()
extern "C" void SlowMoButton_Start_m1185 (SlowMoButton_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::OnDestroy()
extern "C" void SlowMoButton_OnDestroy_m1186 (SlowMoButton_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.SceneUtils.SlowMoButton::ChangeSpeed()
extern "C" void SlowMoButton_ChangeSpeed_m1187 (SlowMoButton_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
