﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct EqualityComparer_1_t2996;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct  EqualityComparer_1_t2996  : public Object_t
{
};
struct EqualityComparer_1_t2996_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Byte>::_default
	EqualityComparer_1_t2996 * ____default_0;
};
