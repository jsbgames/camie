﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>
struct IndexedSet_1_t513;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t646;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ICanvasElement>
struct IEnumerator_1_t3645;
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t3081;
// System.Predicate`1<UnityEngine.UI.ICanvasElement>
struct Predicate_1_t515;
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct Comparison_1_t514;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m3156(__this, method) (( void (*) (IndexedSet_1_t513 *, const MethodInfo*))IndexedSet_1__ctor_m17276_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17277(__this, method) (( Object_t * (*) (IndexedSet_1_t513 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17278_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m17279(__this, ___item, method) (( void (*) (IndexedSet_1_t513 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m17280_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m17281(__this, ___item, method) (( bool (*) (IndexedSet_1_t513 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m17282_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m17283(__this, method) (( Object_t* (*) (IndexedSet_1_t513 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m17284_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m17285(__this, method) (( void (*) (IndexedSet_1_t513 *, const MethodInfo*))IndexedSet_1_Clear_m17286_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m17287(__this, ___item, method) (( bool (*) (IndexedSet_1_t513 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m17288_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m17289(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t513 *, ICanvasElementU5BU5D_t3081*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m17290_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m17291(__this, method) (( int32_t (*) (IndexedSet_1_t513 *, const MethodInfo*))IndexedSet_1_get_Count_m17292_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m17293(__this, method) (( bool (*) (IndexedSet_1_t513 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m17294_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m17295(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t513 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m17296_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m17297(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t513 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m17298_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m17299(__this, ___index, method) (( void (*) (IndexedSet_1_t513 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m17300_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m17301(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t513 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m17302_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m17303(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t513 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m17304_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m3161(__this, ___match, method) (( void (*) (IndexedSet_1_t513 *, Predicate_1_t515 *, const MethodInfo*))IndexedSet_1_RemoveAll_m17305_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m3162(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t513 *, Comparison_1_t514 *, const MethodInfo*))IndexedSet_1_Sort_m17306_gshared)(__this, ___sortLayoutFunction, method)
