﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RijndaelManagedTransform
struct RijndaelManagedTransform_t2091;
// System.Security.Cryptography.Rijndael
struct Rijndael_t1652;
// System.Byte[]
struct ByteU5BU5D_t850;

// System.Void System.Security.Cryptography.RijndaelManagedTransform::.ctor(System.Security.Cryptography.Rijndael,System.Boolean,System.Byte[],System.Byte[])
extern "C" void RijndaelManagedTransform__ctor_m11214 (RijndaelManagedTransform_t2091 * __this, Rijndael_t1652 * ___algo, bool ___encryption, ByteU5BU5D_t850* ___key, ByteU5BU5D_t850* ___iv, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelManagedTransform::System.IDisposable.Dispose()
extern "C" void RijndaelManagedTransform_System_IDisposable_Dispose_m11215 (RijndaelManagedTransform_t2091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RijndaelManagedTransform::get_CanReuseTransform()
extern "C" bool RijndaelManagedTransform_get_CanReuseTransform_m11216 (RijndaelManagedTransform_t2091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t RijndaelManagedTransform_TransformBlock_m11217 (RijndaelManagedTransform_t2091 * __this, ByteU5BU5D_t850* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, ByteU5BU5D_t850* ___outputBuffer, int32_t ___outputOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RijndaelManagedTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t850* RijndaelManagedTransform_TransformFinalBlock_m11218 (RijndaelManagedTransform_t2091 * __this, ByteU5BU5D_t850* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
