﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t3340;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1033;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t3798;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct KeyCollection_t3341;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ValueCollection_t3345;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2812;
// System.Collections.Generic.IDictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IDictionary_2_t3799;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1043;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct KeyValuePair_2U5BU5D_t3800;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct IEnumerator_1_t3801;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1429;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__26.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void Dictionary_2__ctor_m21026_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m21026(__this, method) (( void (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2__ctor_m21026_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21028_gshared (Dictionary_2_t3340 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21028(__this, ___comparer, method) (( void (*) (Dictionary_2_t3340 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21028_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m21030_gshared (Dictionary_2_t3340 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m21030(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3340 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21030_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m21032_gshared (Dictionary_2_t3340 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m21032(__this, ___capacity, method) (( void (*) (Dictionary_2_t3340 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m21032_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21034_gshared (Dictionary_2_t3340 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21034(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3340 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21034_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m21036_gshared (Dictionary_2_t3340 * __this, SerializationInfo_t1043 * ___info, StreamingContext_t1044  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m21036(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3340 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2__ctor_m21036_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21038_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21038(__this, method) (( Object_t* (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21038_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21040_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21040(__this, method) (( Object_t* (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21040_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m21042_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m21042(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3340 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m21042_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m21044_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m21044(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3340 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m21044_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m21046_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m21046(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3340 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m21046_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m21048_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m21048(__this, ___key, method) (( bool (*) (Dictionary_2_t3340 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m21048_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m21050_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m21050(__this, ___key, method) (( void (*) (Dictionary_2_t3340 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m21050_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21052_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21052(__this, method) (( bool (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21052_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21054_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21054(__this, method) (( Object_t * (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21054_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21056_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21056(__this, method) (( bool (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21056_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21058_gshared (Dictionary_2_t3340 * __this, KeyValuePair_2_t3314  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21058(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3340 *, KeyValuePair_2_t3314 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21058_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21060_gshared (Dictionary_2_t3340 * __this, KeyValuePair_2_t3314  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21060(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3340 *, KeyValuePair_2_t3314 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21060_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21062_gshared (Dictionary_2_t3340 * __this, KeyValuePair_2U5BU5D_t3800* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21062(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3340 *, KeyValuePair_2U5BU5D_t3800*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21062_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21064_gshared (Dictionary_2_t3340 * __this, KeyValuePair_2_t3314  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21064(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3340 *, KeyValuePair_2_t3314 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21064_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m21066_gshared (Dictionary_2_t3340 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m21066(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3340 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m21066_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21068_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21068(__this, method) (( Object_t * (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21068_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21070_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21070(__this, method) (( Object_t* (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21070_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21072_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21072(__this, method) (( Object_t * (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21072_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m21074_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m21074(__this, method) (( int32_t (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_get_Count_m21074_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(TKey)
extern "C" KeyValuePair_2_t2815  Dictionary_2_get_Item_m21076_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m21076(__this, ___key, method) (( KeyValuePair_2_t2815  (*) (Dictionary_2_t3340 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m21076_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m21078_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, KeyValuePair_2_t2815  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m21078(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3340 *, Object_t *, KeyValuePair_2_t2815 , const MethodInfo*))Dictionary_2_set_Item_m21078_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m21080_gshared (Dictionary_2_t3340 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m21080(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3340 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m21080_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m21082_gshared (Dictionary_2_t3340 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m21082(__this, ___size, method) (( void (*) (Dictionary_2_t3340 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m21082_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m21084_gshared (Dictionary_2_t3340 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m21084(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3340 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m21084_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3314  Dictionary_2_make_pair_m21086_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t2815  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m21086(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3314  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t2815 , const MethodInfo*))Dictionary_2_make_pair_m21086_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m21088_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t2815  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m21088(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t2815 , const MethodInfo*))Dictionary_2_pick_key_m21088_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::pick_value(TKey,TValue)
extern "C" KeyValuePair_2_t2815  Dictionary_2_pick_value_m21090_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t2815  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m21090(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2815  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t2815 , const MethodInfo*))Dictionary_2_pick_value_m21090_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m21092_gshared (Dictionary_2_t3340 * __this, KeyValuePair_2U5BU5D_t3800* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m21092(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3340 *, KeyValuePair_2U5BU5D_t3800*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m21092_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Resize()
extern "C" void Dictionary_2_Resize_m21094_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m21094(__this, method) (( void (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_Resize_m21094_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m21096_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, KeyValuePair_2_t2815  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m21096(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3340 *, Object_t *, KeyValuePair_2_t2815 , const MethodInfo*))Dictionary_2_Add_m21096_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C" void Dictionary_2_Clear_m21098_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m21098(__this, method) (( void (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_Clear_m21098_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m21100_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m21100(__this, ___key, method) (( bool (*) (Dictionary_2_t3340 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m21100_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m21102_gshared (Dictionary_2_t3340 * __this, KeyValuePair_2_t2815  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m21102(__this, ___value, method) (( bool (*) (Dictionary_2_t3340 *, KeyValuePair_2_t2815 , const MethodInfo*))Dictionary_2_ContainsValue_m21102_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m21104_gshared (Dictionary_2_t3340 * __this, SerializationInfo_t1043 * ___info, StreamingContext_t1044  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m21104(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3340 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2_GetObjectData_m21104_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m21106_gshared (Dictionary_2_t3340 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m21106(__this, ___sender, method) (( void (*) (Dictionary_2_t3340 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m21106_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m21108_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m21108(__this, ___key, method) (( bool (*) (Dictionary_2_t3340 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m21108_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m21110_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, KeyValuePair_2_t2815 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m21110(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3340 *, Object_t *, KeyValuePair_2_t2815 *, const MethodInfo*))Dictionary_2_TryGetValue_m21110_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Keys()
extern "C" KeyCollection_t3341 * Dictionary_2_get_Keys_m21112_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m21112(__this, method) (( KeyCollection_t3341 * (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_get_Keys_m21112_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Values()
extern "C" ValueCollection_t3345 * Dictionary_2_get_Values_m21114_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m21114(__this, method) (( ValueCollection_t3345 * (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_get_Values_m21114_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m21116_gshared (Dictionary_2_t3340 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m21116(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3340 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m21116_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToTValue(System.Object)
extern "C" KeyValuePair_2_t2815  Dictionary_2_ToTValue_m21118_gshared (Dictionary_2_t3340 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m21118(__this, ___value, method) (( KeyValuePair_2_t2815  (*) (Dictionary_2_t3340 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m21118_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m21120_gshared (Dictionary_2_t3340 * __this, KeyValuePair_2_t3314  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m21120(__this, ___pair, method) (( bool (*) (Dictionary_2_t3340 *, KeyValuePair_2_t3314 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m21120_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C" Enumerator_t3343  Dictionary_2_GetEnumerator_m21122_gshared (Dictionary_2_t3340 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m21122(__this, method) (( Enumerator_t3343  (*) (Dictionary_2_t3340 *, const MethodInfo*))Dictionary_2_GetEnumerator_m21122_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1430  Dictionary_2_U3CCopyToU3Em__0_m21124_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t2815  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m21124(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1430  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t2815 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m21124_gshared)(__this /* static, unused */, ___key, ___value, method)
