﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// <PrivateImplementationDetails>/$ArrayType$16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU2416_t1633 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2416_t1633__padding[16];
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: <PrivateImplementationDetails>/$ArrayType$16
#pragma pack(push, tp, 1)
struct U24ArrayTypeU2416_t1633_marshaled
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2416_t1633__padding[16];
	};
};
#pragma pack(pop, tp)
