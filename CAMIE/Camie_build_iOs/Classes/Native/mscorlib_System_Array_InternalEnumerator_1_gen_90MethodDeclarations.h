﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int16>
struct InternalEnumerator_1_t3448;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22167_gshared (InternalEnumerator_1_t3448 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22167(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3448 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22167_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22168_gshared (InternalEnumerator_1_t3448 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22168(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3448 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22168_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22169_gshared (InternalEnumerator_1_t3448 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22169(__this, method) (( void (*) (InternalEnumerator_1_t3448 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22169_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22170_gshared (InternalEnumerator_1_t3448 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22170(__this, method) (( bool (*) (InternalEnumerator_1_t3448 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22170_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern "C" int16_t InternalEnumerator_1_get_Current_m22171_gshared (InternalEnumerator_1_t3448 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22171(__this, method) (( int16_t (*) (InternalEnumerator_1_t3448 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22171_gshared)(__this, method)
