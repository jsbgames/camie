﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t554;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Enum
struct  Enum_t278  : public ValueType_t285
{
};
struct Enum_t278_StaticFields{
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t554* ___split_char_0;
};
