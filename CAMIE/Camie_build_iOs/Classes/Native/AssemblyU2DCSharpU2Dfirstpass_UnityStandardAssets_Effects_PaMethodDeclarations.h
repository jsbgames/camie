﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.ParticleSystemMultiplier
struct ParticleSystemMultiplier_t156;

// System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::.ctor()
extern "C" void ParticleSystemMultiplier__ctor_m434 (ParticleSystemMultiplier_t156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::Start()
extern "C" void ParticleSystemMultiplier_Start_m435 (ParticleSystemMultiplier_t156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
