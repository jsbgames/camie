﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t2940;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m15338_gshared (DefaultComparer_t2940 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m15338(__this, method) (( void (*) (DefaultComparer_t2940 *, const MethodInfo*))DefaultComparer__ctor_m15338_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m15339_gshared (DefaultComparer_t2940 * __this, RaycastResult_t486  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m15339(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2940 *, RaycastResult_t486 , const MethodInfo*))DefaultComparer_GetHashCode_m15339_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m15340_gshared (DefaultComparer_t2940 * __this, RaycastResult_t486  ___x, RaycastResult_t486  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m15340(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2940 *, RaycastResult_t486 , RaycastResult_t486 , const MethodInfo*))DefaultComparer_Equals_m15340_gshared)(__this, ___x, ___y, method)
