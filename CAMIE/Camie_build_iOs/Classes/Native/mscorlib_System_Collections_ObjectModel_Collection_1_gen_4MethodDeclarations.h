﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t3245;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1026;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t3738;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t688;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m19797_gshared (Collection_1_t3245 * __this, const MethodInfo* method);
#define Collection_1__ctor_m19797(__this, method) (( void (*) (Collection_1_t3245 *, const MethodInfo*))Collection_1__ctor_m19797_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19798_gshared (Collection_1_t3245 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19798(__this, method) (( bool (*) (Collection_1_t3245 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19798_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m19799_gshared (Collection_1_t3245 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m19799(__this, ___array, ___index, method) (( void (*) (Collection_1_t3245 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m19799_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m19800_gshared (Collection_1_t3245 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m19800(__this, method) (( Object_t * (*) (Collection_1_t3245 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m19800_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m19801_gshared (Collection_1_t3245 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m19801(__this, ___value, method) (( int32_t (*) (Collection_1_t3245 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m19801_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m19802_gshared (Collection_1_t3245 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m19802(__this, ___value, method) (( bool (*) (Collection_1_t3245 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m19802_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m19803_gshared (Collection_1_t3245 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m19803(__this, ___value, method) (( int32_t (*) (Collection_1_t3245 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m19803_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m19804_gshared (Collection_1_t3245 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m19804(__this, ___index, ___value, method) (( void (*) (Collection_1_t3245 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m19804_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m19805_gshared (Collection_1_t3245 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m19805(__this, ___value, method) (( void (*) (Collection_1_t3245 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m19805_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m19806_gshared (Collection_1_t3245 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m19806(__this, method) (( bool (*) (Collection_1_t3245 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m19806_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m19807_gshared (Collection_1_t3245 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m19807(__this, method) (( Object_t * (*) (Collection_1_t3245 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m19807_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m19808_gshared (Collection_1_t3245 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m19808(__this, method) (( bool (*) (Collection_1_t3245 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m19808_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m19809_gshared (Collection_1_t3245 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m19809(__this, method) (( bool (*) (Collection_1_t3245 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m19809_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m19810_gshared (Collection_1_t3245 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m19810(__this, ___index, method) (( Object_t * (*) (Collection_1_t3245 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m19810_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m19811_gshared (Collection_1_t3245 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m19811(__this, ___index, ___value, method) (( void (*) (Collection_1_t3245 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m19811_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m19812_gshared (Collection_1_t3245 * __this, UILineInfo_t687  ___item, const MethodInfo* method);
#define Collection_1_Add_m19812(__this, ___item, method) (( void (*) (Collection_1_t3245 *, UILineInfo_t687 , const MethodInfo*))Collection_1_Add_m19812_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m19813_gshared (Collection_1_t3245 * __this, const MethodInfo* method);
#define Collection_1_Clear_m19813(__this, method) (( void (*) (Collection_1_t3245 *, const MethodInfo*))Collection_1_Clear_m19813_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m19814_gshared (Collection_1_t3245 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m19814(__this, method) (( void (*) (Collection_1_t3245 *, const MethodInfo*))Collection_1_ClearItems_m19814_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m19815_gshared (Collection_1_t3245 * __this, UILineInfo_t687  ___item, const MethodInfo* method);
#define Collection_1_Contains_m19815(__this, ___item, method) (( bool (*) (Collection_1_t3245 *, UILineInfo_t687 , const MethodInfo*))Collection_1_Contains_m19815_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m19816_gshared (Collection_1_t3245 * __this, UILineInfoU5BU5D_t1026* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m19816(__this, ___array, ___index, method) (( void (*) (Collection_1_t3245 *, UILineInfoU5BU5D_t1026*, int32_t, const MethodInfo*))Collection_1_CopyTo_m19816_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m19817_gshared (Collection_1_t3245 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m19817(__this, method) (( Object_t* (*) (Collection_1_t3245 *, const MethodInfo*))Collection_1_GetEnumerator_m19817_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m19818_gshared (Collection_1_t3245 * __this, UILineInfo_t687  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m19818(__this, ___item, method) (( int32_t (*) (Collection_1_t3245 *, UILineInfo_t687 , const MethodInfo*))Collection_1_IndexOf_m19818_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m19819_gshared (Collection_1_t3245 * __this, int32_t ___index, UILineInfo_t687  ___item, const MethodInfo* method);
#define Collection_1_Insert_m19819(__this, ___index, ___item, method) (( void (*) (Collection_1_t3245 *, int32_t, UILineInfo_t687 , const MethodInfo*))Collection_1_Insert_m19819_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m19820_gshared (Collection_1_t3245 * __this, int32_t ___index, UILineInfo_t687  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m19820(__this, ___index, ___item, method) (( void (*) (Collection_1_t3245 *, int32_t, UILineInfo_t687 , const MethodInfo*))Collection_1_InsertItem_m19820_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m19821_gshared (Collection_1_t3245 * __this, UILineInfo_t687  ___item, const MethodInfo* method);
#define Collection_1_Remove_m19821(__this, ___item, method) (( bool (*) (Collection_1_t3245 *, UILineInfo_t687 , const MethodInfo*))Collection_1_Remove_m19821_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m19822_gshared (Collection_1_t3245 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m19822(__this, ___index, method) (( void (*) (Collection_1_t3245 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m19822_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m19823_gshared (Collection_1_t3245 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m19823(__this, ___index, method) (( void (*) (Collection_1_t3245 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m19823_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m19824_gshared (Collection_1_t3245 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m19824(__this, method) (( int32_t (*) (Collection_1_t3245 *, const MethodInfo*))Collection_1_get_Count_m19824_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t687  Collection_1_get_Item_m19825_gshared (Collection_1_t3245 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m19825(__this, ___index, method) (( UILineInfo_t687  (*) (Collection_1_t3245 *, int32_t, const MethodInfo*))Collection_1_get_Item_m19825_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m19826_gshared (Collection_1_t3245 * __this, int32_t ___index, UILineInfo_t687  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m19826(__this, ___index, ___value, method) (( void (*) (Collection_1_t3245 *, int32_t, UILineInfo_t687 , const MethodInfo*))Collection_1_set_Item_m19826_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m19827_gshared (Collection_1_t3245 * __this, int32_t ___index, UILineInfo_t687  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m19827(__this, ___index, ___item, method) (( void (*) (Collection_1_t3245 *, int32_t, UILineInfo_t687 , const MethodInfo*))Collection_1_SetItem_m19827_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m19828_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m19828(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m19828_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t687  Collection_1_ConvertItem_m19829_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m19829(__this /* static, unused */, ___item, method) (( UILineInfo_t687  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m19829_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m19830_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m19830(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m19830_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m19831_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m19831(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m19831_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m19832_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m19832(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m19832_gshared)(__this /* static, unused */, ___list, method)
