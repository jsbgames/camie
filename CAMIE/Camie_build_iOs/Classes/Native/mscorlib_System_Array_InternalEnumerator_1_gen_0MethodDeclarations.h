﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Object>
struct InternalEnumerator_1_t2803;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13513_gshared (InternalEnumerator_1_t2803 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13513(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2803 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared (InternalEnumerator_1_t2803 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2803 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13517_gshared (InternalEnumerator_1_t2803 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13517(__this, method) (( void (*) (InternalEnumerator_1_t2803 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13519_gshared (InternalEnumerator_1_t2803 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13519(__this, method) (( bool (*) (InternalEnumerator_1_t2803 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern "C" Object_t * InternalEnumerator_1_get_Current_m13521_gshared (InternalEnumerator_1_t2803 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13521(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2803 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
