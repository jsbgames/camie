﻿#pragma once
#include <stdint.h>
// System.Collections.IEnumerator
struct IEnumerator_t217;
// SCR_Wings[]
struct SCR_WingsU5BU5D_t342;
// UnityEngine.Rigidbody
struct Rigidbody_t14;
// UnityEngine.BoxCollider
struct BoxCollider_t343;
// UnityEngine.Animator
struct Animator_t9;
// SCR_Respawn
struct SCR_Respawn_t344;
// SCR_FlyButton
struct SCR_FlyButton_t345;
// SCR_Joystick
struct SCR_Joystick_t346;
// SCR_Goutte
struct SCR_Goutte_t347;
// UnityEngine.Transform
struct Transform_t1;
// SCR_CamieDirections
struct SCR_CamieDirections_t348;
// UnityEngine.Collider
struct Collider_t138;
// UnityEngine.SphereCollider
struct SphereCollider_t136;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// SCR_Camie
struct  SCR_Camie_t338  : public MonoBehaviour_t3
{
	// System.Single SCR_Camie::moveSpeed
	float ___moveSpeed_2;
	// System.Single SCR_Camie::lerpSpeed
	float ___lerpSpeed_3;
	// System.Single SCR_Camie::gravity
	float ___gravity_4;
	// System.Boolean SCR_Camie::isGrounded
	bool ___isGrounded_5;
	// System.Single SCR_Camie::jumpSpeed
	float ___jumpSpeed_6;
	// UnityEngine.Vector3 SCR_Camie::surfaceNormal
	Vector3_t4  ___surfaceNormal_7;
	// UnityEngine.Vector3 SCR_Camie::myNormal
	Vector3_t4  ___myNormal_8;
	// System.Single SCR_Camie::distGround
	float ___distGround_9;
	// System.Boolean SCR_Camie::onCorner
	bool ___onCorner_10;
	// System.Boolean SCR_Camie::goingRight
	bool ___goingRight_11;
	// System.Single SCR_Camie::speedModifier
	float ___speedModifier_12;
	// System.Single SCR_Camie::maxFlyTime
	float ___maxFlyTime_13;
	// System.Single SCR_Camie::currFlyTime
	float ___currFlyTime_14;
	// System.Boolean SCR_Camie::isFlyTimeCounting
	bool ___isFlyTimeCounting_15;
	// System.Boolean SCR_Camie::canFly
	bool ___canFly_16;
	// System.Boolean SCR_Camie::isJumping
	bool ___isJumping_17;
	// System.Collections.IEnumerator SCR_Camie::jumpRoutine
	Object_t * ___jumpRoutine_18;
	// SCR_Wings[] SCR_Camie::wings
	SCR_WingsU5BU5D_t342* ___wings_19;
	// UnityEngine.LayerMask SCR_Camie::walkable
	LayerMask_t11  ___walkable_20;
	// UnityEngine.Rigidbody SCR_Camie::rb
	Rigidbody_t14 * ___rb_21;
	// UnityEngine.BoxCollider SCR_Camie::myColl
	BoxCollider_t343 * ___myColl_22;
	// UnityEngine.Animator SCR_Camie::anim
	Animator_t9 * ___anim_23;
	// SCR_Respawn SCR_Camie::respawner
	SCR_Respawn_t344 * ___respawner_24;
	// SCR_FlyButton SCR_Camie::flyButton
	SCR_FlyButton_t345 * ___flyButton_25;
	// SCR_Joystick SCR_Camie::joystick
	SCR_Joystick_t346 * ___joystick_26;
	// System.Boolean SCR_Camie::isKnockbacked
	bool ___isKnockbacked_27;
	// System.Boolean SCR_Camie::isLerping
	bool ___isLerping_28;
	// System.Boolean SCR_Camie::isFlyLerping
	bool ___isFlyLerping_29;
	// UnityEngine.Animator SCR_Camie::puceronsPerdus
	Animator_t9 * ___puceronsPerdus_30;
	// SCR_Goutte SCR_Camie::currentGoutte
	SCR_Goutte_t347 * ___currentGoutte_31;
	// System.Boolean SCR_Camie::isSlowed
	bool ___isSlowed_32;
	// UnityEngine.Transform SCR_Camie::trans
	Transform_t1 * ___trans_33;
	// SCR_CamieDirections SCR_Camie::directionChecker
	SCR_CamieDirections_t348 * ___directionChecker_34;
	// UnityEngine.Transform SCR_Camie::frontRayTarget
	Transform_t1 * ___frontRayTarget_35;
	// UnityEngine.Transform SCR_Camie::backRayTarget
	Transform_t1 * ___backRayTarget_36;
	// UnityEngine.Collider SCR_Camie::frontColliderChecker
	Collider_t138 * ___frontColliderChecker_37;
	// UnityEngine.SphereCollider SCR_Camie::flyCollider
	SphereCollider_t136 * ___flyCollider_38;
};
