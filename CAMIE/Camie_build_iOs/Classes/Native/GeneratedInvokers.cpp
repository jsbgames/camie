﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Void
#include "mscorlib_System_Void.h"
void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

struct Object_t;
// System.Boolean
#include "mscorlib_System_Boolean.h"
void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Char
#include "mscorlib_System_Char.h"
void* RuntimeInvoker_Char_t682 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.SByte
#include "mscorlib_System_SByte.h"
void* RuntimeInvoker_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Byte
#include "mscorlib_System_Byte.h"
void* RuntimeInvoker_Byte_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int16
#include "mscorlib_System_Int16.h"
void* RuntimeInvoker_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt16
#include "mscorlib_System_UInt16.h"
void* RuntimeInvoker_UInt16_t684 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt32
#include "mscorlib_System_UInt32.h"
void* RuntimeInvoker_UInt32_t1063 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int64
#include "mscorlib_System_Int64.h"
void* RuntimeInvoker_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt64
#include "mscorlib_System_UInt64.h"
void* RuntimeInvoker_UInt64_t1074 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Single
#include "mscorlib_System_Single.h"
void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Double
#include "mscorlib_System_Double.h"
void* RuntimeInvoker_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
void* RuntimeInvoker_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Utility_Wa_0.h"
void* RuntimeInvoker_RoutePoint_t209 (const MethodInfo* method, void* obj, void** args)
{
	typedef RoutePoint_t209  (*Func)(void* obj, const MethodInfo* method);
	RoutePoint_t209  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
void* RuntimeInvoker_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// SCR_Joystick/Direction
#include "AssemblyU2DCSharp_SCR_Joystick_Direction.h"
void* RuntimeInvoker_Direction_t363 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.MoveDirection
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
void* RuntimeInvoker_MoveDirection_t482 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
void* RuntimeInvoker_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t486  (*Func)(void* obj, const MethodInfo* method);
	RaycastResult_t486  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.PointerEventData/InputButton
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
void* RuntimeInvoker_InputButton_t488 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.StandaloneInputModule/InputMode
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
void* RuntimeInvoker_InputMode_t497 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
void* RuntimeInvoker_LayerMask_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t11  (*Func)(void* obj, const MethodInfo* method);
	LayerMask_t11  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
void* RuntimeInvoker_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t65  (*Func)(void* obj, const MethodInfo* method);
	Color_t65  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
void* RuntimeInvoker_ColorTweenMode_t503 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
void* RuntimeInvoker_ColorBlock_t516 (const MethodInfo* method, void* obj, void** args)
{
	typedef ColorBlock_t516  (*Func)(void* obj, const MethodInfo* method);
	ColorBlock_t516  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
void* RuntimeInvoker_FontStyle_t749 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
void* RuntimeInvoker_TextAnchor_t701 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
void* RuntimeInvoker_HorizontalWrapMode_t750 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
void* RuntimeInvoker_VerticalWrapMode_t751 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
void* RuntimeInvoker_Rect_t304 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t304  (*Func)(void* obj, const MethodInfo* method);
	Rect_t304  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GraphicRaycaster/BlockingObjects
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
void* RuntimeInvoker_BlockingObjects_t525 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Image/Type
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
void* RuntimeInvoker_Type_t530 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Image/FillMethod
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
void* RuntimeInvoker_FillMethod_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
void* RuntimeInvoker_ContentType_t538 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/LineType
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
void* RuntimeInvoker_LineType_t541 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/InputType
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
void* RuntimeInvoker_InputType_t539 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
void* RuntimeInvoker_TouchScreenKeyboardType_t683 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/CharacterValidation
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
void* RuntimeInvoker_CharacterValidation_t540 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
void* RuntimeInvoker_Mode_t562 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
void* RuntimeInvoker_Navigation_t563 (const MethodInfo* method, void* obj, void** args)
{
	typedef Navigation_t563  (*Func)(void* obj, const MethodInfo* method);
	Navigation_t563  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Scrollbar/Direction
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
void* RuntimeInvoker_Direction_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Scrollbar/Axis
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
void* RuntimeInvoker_Axis_t568 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ScrollRect/MovementType
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
void* RuntimeInvoker_MovementType_t572 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
void* RuntimeInvoker_Bounds_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef Bounds_t225  (*Func)(void* obj, const MethodInfo* method);
	Bounds_t225  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
void* RuntimeInvoker_Transition_t576 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
void* RuntimeInvoker_SpriteState_t580 (const MethodInfo* method, void* obj, void** args)
{
	typedef SpriteState_t580  (*Func)(void* obj, const MethodInfo* method);
	SpriteState_t580  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
void* RuntimeInvoker_SelectionState_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
void* RuntimeInvoker_Direction_t582 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
void* RuntimeInvoker_Axis_t584 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
void* RuntimeInvoker_AspectMode_t596 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
void* RuntimeInvoker_ScaleMode_t598 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
void* RuntimeInvoker_ScreenMatchMode_t599 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
void* RuntimeInvoker_Unit_t600 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
void* RuntimeInvoker_FitMode_t602 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
void* RuntimeInvoker_Corner_t604 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
void* RuntimeInvoker_Axis_t605 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
void* RuntimeInvoker_Constraint_t606 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.DeviceType
#include "UnityEngine_UnityEngine_DeviceType.h"
void* RuntimeInvoker_DeviceType_t403 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.FogMode
#include "UnityEngine_UnityEngine_FogMode.h"
void* RuntimeInvoker_FogMode_t962 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.ColorSpace
#include "UnityEngine_UnityEngine_ColorSpace.h"
void* RuntimeInvoker_ColorSpace_t966 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
void* RuntimeInvoker_RenderTextureFormat_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
void* RuntimeInvoker_RenderBuffer_t961 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t961  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t961  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"
void* RuntimeInvoker_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.ImagePosition
#include "UnityEngine_UnityEngine_ImagePosition.h"
void* RuntimeInvoker_ImagePosition_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
void* RuntimeInvoker_EventType_t837 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
void* RuntimeInvoker_EventModifiers_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
void* RuntimeInvoker_KeyCode_t836 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
void* RuntimeInvoker_Quaternion_t19 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
void* RuntimeInvoker_Matrix4x4_t80 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t80  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t80  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
void* RuntimeInvoker_Vector4_t236 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
void* RuntimeInvoker_RuntimePlatform_t780 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SystemLanguage
#include "UnityEngine_UnityEngine_SystemLanguage.h"
void* RuntimeInvoker_SystemLanguage_t781 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
void* RuntimeInvoker_CameraClearFlags_t963 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.DepthTextureMode
#include "UnityEngine_UnityEngine_DepthTextureMode.h"
void* RuntimeInvoker_DepthTextureMode_t964 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
void* RuntimeInvoker_TouchPhase_t862 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
void* RuntimeInvoker_SendMessageOptions_t778 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
void* RuntimeInvoker_AnimatorStateInfo_t887 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t887  (*Func)(void* obj, const MethodInfo* method);
	AnimatorStateInfo_t887  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
void* RuntimeInvoker_AnimatorClipInfo_t888 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t888  (*Func)(void* obj, const MethodInfo* method);
	AnimatorClipInfo_t888  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
void* RuntimeInvoker_RenderMode_t900 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
void* RuntimeInvoker_SourceID_t919 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
void* RuntimeInvoker_AppID_t918 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
void* RuntimeInvoker_NetworkID_t920 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
void* RuntimeInvoker_NodeID_t921 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
void* RuntimeInvoker_UserState_t987 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
void* RuntimeInvoker_UserScope_t988 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
void* RuntimeInvoker_Range_t982 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t982  (*Func)(void* obj, const MethodInfo* method);
	Range_t982  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
void* RuntimeInvoker_TimeScope_t989 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
void* RuntimeInvoker_PersistentListenerMode_t1000 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
void* RuntimeInvoker_DictionaryEntry_t1430 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"
void* RuntimeInvoker_EditorBrowsableState_t1275 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamily.h"
void* RuntimeInvoker_AddressFamily_t1280 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolType.h"
void* RuntimeInvoker_SecurityProtocolType_t1300 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
void* RuntimeInvoker_X509ChainStatusFlags_t1338 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationFlag
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
void* RuntimeInvoker_X509RevocationFlag_t1345 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationMode
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
void* RuntimeInvoker_X509RevocationMode_t1346 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509VerificationFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
void* RuntimeInvoker_X509VerificationFlags_t1350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
void* RuntimeInvoker_X509KeyUsageFlags_t1343 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.RegexOptions
#include "System_System_Text_RegularExpressions_RegexOptions.h"
void* RuntimeInvoker_RegexOptions_t1367 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
void* RuntimeInvoker_Interval_t1389 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1389  (*Func)(void* obj, const MethodInfo* method);
	Interval_t1389  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
void* RuntimeInvoker_Category_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Position
#include "System_System_Text_RegularExpressions_Position.h"
void* RuntimeInvoker_Position_t1370 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t1525 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.X509.X509ChainStatusFlags
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
void* RuntimeInvoker_X509ChainStatusFlags_t1552 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertLevel
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
void* RuntimeInvoker_AlertLevel_t1565 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
void* RuntimeInvoker_AlertDescription_t1566 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
void* RuntimeInvoker_CipherAlgorithmType_t1568 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HashAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
void* RuntimeInvoker_HashAlgorithmType_t1587 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
void* RuntimeInvoker_ExchangeAlgorithmType_t1585 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
void* RuntimeInvoker_CipherMode_t1659 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
void* RuntimeInvoker_SecurityProtocolType_t1600 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityCompressionType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
void* RuntimeInvoker_SecurityCompressionType_t1599 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
void* RuntimeInvoker_HandshakeType_t1613 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
void* RuntimeInvoker_HandshakeState_t1586 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.RSAParameters
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
void* RuntimeInvoker_RSAParameters_t1451 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1451  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t1451  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.ContentType
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
void* RuntimeInvoker_ContentType_t1580 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
void* RuntimeInvoker_TypeCode_t2244 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
void* RuntimeInvoker_TypeAttributes_t1922 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"
void* RuntimeInvoker_MemberTypes_t1900 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
void* RuntimeInvoker_RuntimeTypeHandle_t1667 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1667  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t1667  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t1713 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
void* RuntimeInvoker_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1337  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t1337  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Decimal
#include "mscorlib_System_Decimal.h"
void* RuntimeInvoker_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"
void* RuntimeInvoker_CallingConventions_t1895 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
void* RuntimeInvoker_RuntimeMethodHandle_t1850 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t1850  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t1850  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributes.h"
void* RuntimeInvoker_MethodAttributes_t1901 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.Emit.MethodToken
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
void* RuntimeInvoker_MethodToken_t1868 (const MethodInfo* method, void* obj, void** args)
{
	typedef MethodToken_t1868  (*Func)(void* obj, const MethodInfo* method);
	MethodToken_t1868  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"
void* RuntimeInvoker_FieldAttributes_t1898 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
void* RuntimeInvoker_RuntimeFieldHandle_t1668 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t1668  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t1668  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.Emit.Label
#include "mscorlib_System_Reflection_Emit_Label.h"
void* RuntimeInvoker_Label_t1864 (const MethodInfo* method, void* obj, void** args)
{
	typedef Label_t1864  (*Func)(void* obj, const MethodInfo* method);
	Label_t1864  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.Emit.OperandType
#include "mscorlib_System_Reflection_Emit_OperandType.h"
void* RuntimeInvoker_OperandType_t1876 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.Emit.StackBehaviour
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
void* RuntimeInvoker_StackBehaviour_t1881 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"
void* RuntimeInvoker_PropertyAttributes_t1917 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
void* RuntimeInvoker_AssemblyNameFlags_t1892 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributes.h"
void* RuntimeInvoker_EventAttributes_t1896 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributes.h"
void* RuntimeInvoker_ParameterAttributes_t1913 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Remoting.Lifetime.LeaseState
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseState.h"
void* RuntimeInvoker_LeaseState_t1971 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Remoting.Messaging.CallType
#include "mscorlib_System_Runtime_Remoting_Messaging_CallType.h"
void* RuntimeInvoker_CallType_t1992 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
void* RuntimeInvoker_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t1044  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t1044  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
void* RuntimeInvoker_TypeFilterLevel_t2045 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
void* RuntimeInvoker_SerializationEntry_t2063 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t2063  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t2063  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
void* RuntimeInvoker_StreamingContextStates_t2067 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
void* RuntimeInvoker_CspProviderFlags_t2070 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
void* RuntimeInvoker_PaddingMode_t2082 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"
void* RuntimeInvoker_DayOfWeek_t2197 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
void* RuntimeInvoker_DateTimeKind_t2195 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"
void* RuntimeInvoker_DateTimeOffset_t1086 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTimeOffset_t1086  (*Func)(void* obj, const MethodInfo* method);
	DateTimeOffset_t1086  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
void* RuntimeInvoker_PlatformID_t2234 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Guid
#include "mscorlib_System_Guid.h"
void* RuntimeInvoker_Guid_t1087 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t1087  (*Func)(void* obj, const MethodInfo* method);
	Guid_t1087  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
void* RuntimeInvoker_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t24  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit_t24  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_Link.h"
void* RuntimeInvoker_Link_t1757 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1757  (*Func)(void* obj, const MethodInfo* method);
	Link_t1757  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
void* RuntimeInvoker_Touch_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t235  (*Func)(void* obj, const MethodInfo* method);
	Touch_t235  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
void* RuntimeInvoker_Keyframe_t240 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t240  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t240  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPoint.h"
void* RuntimeInvoker_ContactPoint_t246 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t246  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint_t246  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.ParticleCollisionEvent
#include "UnityEngine_UnityEngine_ParticleCollisionEvent.h"
void* RuntimeInvoker_ParticleCollisionEvent_t160 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParticleCollisionEvent_t160  (*Func)(void* obj, const MethodInfo* method);
	ParticleCollisionEvent_t160  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
void* RuntimeInvoker_RaycastHit2D_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t665  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit2D_t665  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
void* RuntimeInvoker_UIVertex_t556 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t556  (*Func)(void* obj, const MethodInfo* method);
	UIVertex_t556  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
void* RuntimeInvoker_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t687  (*Func)(void* obj, const MethodInfo* method);
	UILineInfo_t687  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
void* RuntimeInvoker_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t689  (*Func)(void* obj, const MethodInfo* method);
	UICharInfo_t689  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
void* RuntimeInvoker_GcAchievementData_t958 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t958  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t958  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
void* RuntimeInvoker_GcScoreData_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t959  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t959  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
void* RuntimeInvoker_ParameterModifier_t1914 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1914  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t1914  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
void* RuntimeInvoker_HitInfo_t983 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t983  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t983  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
void* RuntimeInvoker_X509ChainStatus_t1331 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1331  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t1331  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
void* RuntimeInvoker_Mark_t1382 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1382  (*Func)(void* obj, const MethodInfo* method);
	Mark_t1382  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
void* RuntimeInvoker_UriScheme_t1418 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1418  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t1418  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
void* RuntimeInvoker_TableRange_t1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1690  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t1690  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"
void* RuntimeInvoker_Slot_t1767 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1767  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1767  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"
void* RuntimeInvoker_Slot_t1776 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1776  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1776  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.Emit.MonoResource
#include "mscorlib_System_Reflection_Emit_MonoResource.h"
void* RuntimeInvoker_MonoResource_t1838 (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoResource_t1838  (*Func)(void* obj, const MethodInfo* method);
	MonoResource_t1838  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.Emit.ILTokenInfo
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
void* RuntimeInvoker_ILTokenInfo_t1856 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1856  (*Func)(void* obj, const MethodInfo* method);
	ILTokenInfo_t1856  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.Emit.ILGenerator/LabelData
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
void* RuntimeInvoker_LabelData_t1858 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1858  (*Func)(void* obj, const MethodInfo* method);
	LabelData_t1858  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
void* RuntimeInvoker_LabelFixup_t1857 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1857  (*Func)(void* obj, const MethodInfo* method);
	LabelFixup_t1857  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
void* RuntimeInvoker_Enumerator_t3029 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3029  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3029  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
void* RuntimeInvoker_Enumerator_t2822 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2822  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2822  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
void* RuntimeInvoker_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2815  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2815  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_1.h"
void* RuntimeInvoker_Enumerator_t2821 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2821  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2821  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7.h"
void* RuntimeInvoker_Enumerator_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2825  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2825  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"
void* RuntimeInvoker_Enumerator_t2843 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2843  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2843  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_22.h"
void* RuntimeInvoker_Enumerator_t3066 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3066  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3066  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
void* RuntimeInvoker_Enumerator_t3063 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3063  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3063  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
void* RuntimeInvoker_KeyValuePair_2_t3059 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3059  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3059  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"
void* RuntimeInvoker_Enumerator_t2913 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2913  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2913  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"
void* RuntimeInvoker_Enumerator_t2935 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2935  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2935  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9.h"
void* RuntimeInvoker_Enumerator_t2957 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2957  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2957  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"
void* RuntimeInvoker_KeyValuePair_2_t2953 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2953  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2953  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_10.h"
void* RuntimeInvoker_Enumerator_t2956 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2956  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2956  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_16.h"
void* RuntimeInvoker_Enumerator_t2960 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2960  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2960  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__11.h"
void* RuntimeInvoker_Enumerator_t2989 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2989  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2989  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"
void* RuntimeInvoker_KeyValuePair_2_t2984 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2984  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2984  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_13.h"
void* RuntimeInvoker_Enumerator_t2988 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2988  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2988  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_19.h"
void* RuntimeInvoker_Enumerator_t2992 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2992  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2992  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_17.h"
void* RuntimeInvoker_Enumerator_t3062 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3062  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3062  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"
void* RuntimeInvoker_Enumerator_t3101 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3101  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3101  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_32.h"
void* RuntimeInvoker_Enumerator_t3234 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3234  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3234  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_33.h"
void* RuntimeInvoker_Enumerator_t3243 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3243  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3243  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__21.h"
void* RuntimeInvoker_Enumerator_t3260 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3260  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3260  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23.h"
void* RuntimeInvoker_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3255  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3255  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_26.h"
void* RuntimeInvoker_Enumerator_t3259 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3259  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3259  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_29.h"
void* RuntimeInvoker_Enumerator_t3263 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3263  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3263  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__23.h"
void* RuntimeInvoker_Enumerator_t3298 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3298  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3298  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"
void* RuntimeInvoker_KeyValuePair_2_t3293 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3293  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3293  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_29.h"
void* RuntimeInvoker_Enumerator_t3297 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3297  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3297  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32.h"
void* RuntimeInvoker_Enumerator_t3301 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3301  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3301  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
void* RuntimeInvoker_KeyValuePair_2_t3314 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3314  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3314  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__26.h"
void* RuntimeInvoker_Enumerator_t3343 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3343  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3343  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_33.h"
void* RuntimeInvoker_Enumerator_t3342 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3342  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3342  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_36.h"
void* RuntimeInvoker_Enumerator_t3346 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3346  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3346  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__29.h"
void* RuntimeInvoker_Enumerator_t3411 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3411  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3411  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_33.h"
void* RuntimeInvoker_KeyValuePair_2_t3407 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3407  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3407  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_37.h"
void* RuntimeInvoker_Enumerator_t3410 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3410  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3410  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_40.h"
void* RuntimeInvoker_Enumerator_t3414 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3414  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3414  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__31.h"
void* RuntimeInvoker_Enumerator_t3431 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3431  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3431  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_35.h"
void* RuntimeInvoker_KeyValuePair_2_t3427 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3427  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3427  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_40.h"
void* RuntimeInvoker_Enumerator_t3430 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3430  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3430  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_43.h"
void* RuntimeInvoker_Enumerator_t3434 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3434  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3434  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Byte_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Char_t682_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_UInt16_t684 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Char_t682_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t680_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t752_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t274_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t680_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t682_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t684_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_UInt64_t1074 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int16_t752_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt16_t684_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t680_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t682_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1063_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t752_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1071_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t274_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t680_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t682_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t684_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1074_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t752_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1071_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t274_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Matrix4x4U26_t1143 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t80 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4_t80 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RenderBufferU26_t1144 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RenderBuffer_t961 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (RenderBuffer_t961 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_ColorU26_t754 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t65 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t65 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector2U26_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t6 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t6 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RectU26_t1146 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t304 *)args[0], method);
	return NULL;
}

struct Object_t;
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
void* RuntimeInvoker_Void_t272_SphericalHarmonicsL2U26_t1157 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t847 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t847 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t4 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_QuaternionU26_t1152 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t19 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t19 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_BoundsU26_t1153 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t225 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Bounds_t225 *)args[0], method);
	return NULL;
}

struct Object_t;
struct String_t;
// System.String
#include "mscorlib_System_String.h"
void* RuntimeInvoker_Void_t272_StringU26_t1216 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t680_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t1070_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t274_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t684_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t4 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RoutePoint_t209 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RoutePoint_t209  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RoutePoint_t209 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t6  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t6 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResult_t486  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastResult_t486 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_LayerMask_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LayerMask_t11  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LayerMask_t11 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t65  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t65 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Rect_t304 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t304 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Navigation_t563 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Navigation_t563  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Navigation_t563 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_ColorBlock_t516 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorBlock_t516  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorBlock_t516 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SpriteState_t580 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpriteState_t580  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SpriteState_t580 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_GcScoreData_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t959  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t959 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Matrix4x4_t80 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t80  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t80 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t406  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t406 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Bounds_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t225  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t225 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Quaternion_t19 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t19  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t19 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Range_t982 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t982  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t982 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Interval_t1389 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t1389  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t1389 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RSAParameters_t1451 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t1451  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t1451 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1071_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
void* RuntimeInvoker_Void_t272_DSAParameters_t1453 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t1453  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t1453 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.OpCode
#include "mscorlib_System_Reflection_Emit_OpCode.h"
void* RuntimeInvoker_Void_t272_OpCode_t1873 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1873  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1873 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Label_t1864 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Label_t1864  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Label_t1864 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RuntimeMethodHandle_t1850 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RuntimeMethodHandle_t1850  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RuntimeMethodHandle_t1850 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t1337  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t1337 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t1044  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t1044 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t680_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t752_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.MonoEnumInfo
#include "mscorlib_System_MonoEnumInfo.h"
void* RuntimeInvoker_Void_t272_MonoEnumInfo_t2208 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t2208  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t2208 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
void* RuntimeInvoker_Void_t272_ColorTween_t506 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorTween_t506  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorTween_t506 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit_t24  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit_t24 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Link_t1757 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t1757  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t1757 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_DictionaryEntry_t1430 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t1430  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t1430 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Touch_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Touch_t235  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Touch_t235 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Keyframe_t240 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t240  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t240 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_ContactPoint_t246 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint_t246  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint_t246 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_ParticleCollisionEvent_t160 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParticleCollisionEvent_t160  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParticleCollisionEvent_t160 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_AnimatorClipInfo_t888 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, AnimatorClipInfo_t888  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((AnimatorClipInfo_t888 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RaycastHit2D_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit2D_t665  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit2D_t665 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t687  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t687 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t689  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t689 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_GcAchievementData_t958 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t958  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t958 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_ParameterModifier_t1914 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t1914  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t1914 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_HitInfo_t983 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t983  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t983 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_X509ChainStatus_t1331 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t1331  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t1331 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Mark_t1382 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t1382  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t1382 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_UriScheme_t1418 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t1418  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t1418 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_TableRange_t1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t1690  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t1690 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Slot_t1767 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1767  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1767 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Slot_t1776 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1776  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1776 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_MonoResource_t1838 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoResource_t1838  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoResource_t1838 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_ILTokenInfo_t1856 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t1856  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ILTokenInfo_t1856 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_LabelData_t1858 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t1858  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelData_t1858 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_LabelFixup_t1857 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t1857  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelFixup_t1857 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_UIVertex_t556 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t556  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t556 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
void* RuntimeInvoker_Boolean_t273_LayoutRebuilder_t615 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LayoutRebuilder_t615  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LayoutRebuilder_t615 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t6  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t4  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Bounds_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t225  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t225 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
void* RuntimeInvoker_Boolean_t273_Ray_t26 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t26  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t26 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_HitInfo_t983 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t983  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t983 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
void* RuntimeInvoker_Boolean_t273_TextGenerationSettings_t649 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t649  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t649 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Interval_t1389 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t1389  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t1389 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t406  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t1070_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t752_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t684_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_DateTimeOffset_t1086 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1086  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1086 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Guid_t1087 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t1087  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t1087 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t1337  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t1337 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit_t24  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit_t24 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Link_t1757 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t1757  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t1757 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_DictionaryEntry_t1430 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t1430  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t1430 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Touch_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Touch_t235  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Touch_t235 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Keyframe_t240 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t240  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t65  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t65 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_ContactPoint_t246 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint_t246  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint_t246 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_ParticleCollisionEvent_t160 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParticleCollisionEvent_t160  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParticleCollisionEvent_t160 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t486  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t486 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Quaternion_t19 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t19  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_AnimatorClipInfo_t888 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, AnimatorClipInfo_t888  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((AnimatorClipInfo_t888 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_RaycastHit2D_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit2D_t665  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit2D_t665 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t687  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t687 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t689  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t689 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_GcAchievementData_t958 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t958  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t958 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_GcScoreData_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t959  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t959 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_ParameterModifier_t1914 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t1914  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t1914 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_X509ChainStatus_t1331 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t1331  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t1331 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Mark_t1382 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t1382  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t1382 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_UriScheme_t1418 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t1418  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t1418 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_TableRange_t1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t1690  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t1690 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Slot_t1767 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1767  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1767 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Slot_t1776 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1776  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1776 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_MonoResource_t1838 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, MonoResource_t1838  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((MonoResource_t1838 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_ILTokenInfo_t1856 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t1856  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ILTokenInfo_t1856 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_LabelData_t1858 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t1858  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelData_t1858 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_LabelFixup_t1857 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t1857  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelFixup_t1857 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_UIVertex_t556 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t556  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t556 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t684_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t236_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RSAParameters_t1451_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1451  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t1451  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t274_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1073_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t1453_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1453  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t1453  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t406_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t1070_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t1035 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1035  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1035 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t680_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1071_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2815  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2815 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t2953 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2953  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2953 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t2984 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2984  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2984 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3059 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3059  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3059 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3255  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3255 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3293 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3293  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3293 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3314 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3314  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3314 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3407 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3407  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3427 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3427  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3427 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t1035 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1035  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1035 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SecurityProtocolType_t1600_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategory.h"
void* RuntimeInvoker_UnicodeCategory_t1490_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t752_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1073_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1071_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t406_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t1070_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2815  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2815 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2953 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2953  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2953 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2984 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2984  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2984 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3059 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3059  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3059 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3255  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3255 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3293 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3293  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3293 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3314 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3314  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3314 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3407 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3407  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3427 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3427  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3427 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen.h"
void* RuntimeInvoker_Boolean_t273_Nullable_1_t2292 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t2292  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t2292 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t684_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Direction_t363_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.PointerEventData/FramePressState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
void* RuntimeInvoker_FramePressState_t489_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t6  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t6_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_LayerMask_t11 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t11  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t11 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LayerMask_t11_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t11  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t11  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t304_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t304  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Rect_t304  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_EventType_t837_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector4_t236_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Touch_t235_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t235  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Touch_t235  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Keyframe_t240_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t240  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t240  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509KeyUsageFlags_t1343_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Interval_t1389_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1389  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t1389  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1073_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
void* RuntimeInvoker_ExtenderType_t1704_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t406  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t2197_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_OpCode_t1873 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, OpCode_t1873  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((OpCode_t1873 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t406_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t1070_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_DateTimeOffset_t1086 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1086  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1086 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Guid_t1087 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t1087  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t1087 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t1337  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t1337 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit_t24_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t24  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit_t24  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t24  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t24 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Link_t1757_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1757  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t1757  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Link_t1757 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t1757  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t1757 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1430_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_DictionaryEntry_t1430 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t1430  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t1430 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Touch_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Touch_t235  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Touch_t235 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Keyframe_t240 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t240  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t65_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t65  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Color_t65  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color_t65  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color_t65 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t4  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ContactPoint_t246_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t246  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint_t246  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_ContactPoint_t246 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint_t246  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint_t246 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ParticleCollisionEvent_t160_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParticleCollisionEvent_t160  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParticleCollisionEvent_t160  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_ParticleCollisionEvent_t160 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParticleCollisionEvent_t160  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParticleCollisionEvent_t160 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastResult_t486_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t486  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastResult_t486  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t486  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t486 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Quaternion_t19 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Quaternion_t19  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Quaternion_t19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_AnimatorClipInfo_t888_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t888  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	AnimatorClipInfo_t888  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_AnimatorClipInfo_t888 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, AnimatorClipInfo_t888  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((AnimatorClipInfo_t888 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t665_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t665  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit2D_t665  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_RaycastHit2D_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit2D_t665  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit2D_t665 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UILineInfo_t687_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t687  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UILineInfo_t687  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t687  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t687 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UICharInfo_t689_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t689  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UICharInfo_t689  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t689  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t689 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_GcAchievementData_t958_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t958  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t958  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_GcAchievementData_t958 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t958  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t958 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_GcScoreData_t959_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t959  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t959  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_GcScoreData_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t959  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t959 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ParameterModifier_t1914_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1914  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t1914  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_ParameterModifier_t1914 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t1914  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t1914 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_HitInfo_t983_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t983  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t983  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_HitInfo_t983 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t983  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t983 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509ChainStatus_t1331_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1331  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t1331  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_X509ChainStatus_t1331 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t1331  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t1331 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Mark_t1382_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1382  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t1382  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Mark_t1382 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t1382  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t1382 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UriScheme_t1418_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1418  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t1418  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_UriScheme_t1418 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t1418  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t1418 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TableRange_t1690_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1690  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t1690  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_TableRange_t1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t1690  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t1690 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t1767_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1767  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1767  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Slot_t1767 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1767  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1767 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t1776_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1776  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1776  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Slot_t1776 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1776  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1776 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MonoResource_t1838_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoResource_t1838  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	MonoResource_t1838  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_MonoResource_t1838 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MonoResource_t1838  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((MonoResource_t1838 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ILTokenInfo_t1856_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1856  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ILTokenInfo_t1856  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_ILTokenInfo_t1856 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t1856  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ILTokenInfo_t1856 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LabelData_t1858_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1858  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelData_t1858  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_LabelData_t1858 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t1858  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelData_t1858 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LabelFixup_t1857_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1857  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelFixup_t1857  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_LabelFixup_t1857 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t1857  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelFixup_t1857 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1337_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1337  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t1337  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_UIVertex_t556 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t556  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t556 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIVertex_t556_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t556  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIVertex_t556  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t1070_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1071_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1073_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t406_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, float p1, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RoutePoint_t209_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef RoutePoint_t209  (*Func)(void* obj, float p1, const MethodInfo* method);
	RoutePoint_t209  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t65_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t65  (*Func)(void* obj, float p1, const MethodInfo* method);
	Color_t65  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t6  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t4  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Vector4_t236 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t236  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t236 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t1070_DecimalU26_t2444 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t1073 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t1073 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1073_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t406_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2815_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2815  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2815  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2815  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2815 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2953_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2953  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2953  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t2953 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2953  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2953 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2984_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2984  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2984  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t2984 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2984  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2984 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3059_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3059  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3059  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3059 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3059  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3059 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3255_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3255  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3255  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3255  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3255 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3293_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3293  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3293  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3293 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3293  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3293 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3314_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3314  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3314  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3314 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3314  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3314 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3407_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3407  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3407  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3407 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3407  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3427_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3427  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3427  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3427 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3427  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3427 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t1070_Interval_t1389 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t1389  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t1389 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1073_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t1070_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t406_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1337_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1337  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t1337  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t682_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_QuaternionU26_t1152 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, Quaternion_t19 * p1, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, (Quaternion_t19 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_QuaternionU26_t1152 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Quaternion_t19 * p1, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, (Quaternion_t19 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, Vector3_t4 * p1, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, (Vector3_t4 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t80_Matrix4x4U26_t1143 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t80  (*Func)(void* obj, Matrix4x4_t80 * p1, const MethodInfo* method);
	Matrix4x4_t80  ret = ((Func)method->method)(obj, (Matrix4x4_t80 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t1453_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1453  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t1453  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t680_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1063_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
void* RuntimeInvoker_UIntPtr_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Vector3_t4  p1, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, Vector2_t6  p1, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextGenerationSettings_t649_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t649  (*Func)(void* obj, Vector2_t6  p1, const MethodInfo* method);
	TextGenerationSettings_t649  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t304_Rect_t304 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t304  (*Func)(void* obj, Rect_t304  p1, const MethodInfo* method);
	Rect_t304  ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t6_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, Vector3_t4  p1, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Vector2_t6  p1, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t236_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, Color_t65  p1, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, *((Color_t65 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
void* RuntimeInvoker_Color32_t654_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t654  (*Func)(void* obj, Color_t65  p1, const MethodInfo* method);
	Color32_t654  ret = ((Func)method->method)(obj, *((Color_t65 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t65_Color32_t654 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t65  (*Func)(void* obj, Color32_t654  p1, const MethodInfo* method);
	Color_t65  ret = ((Func)method->method)(obj, *((Color32_t654 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, Vector3_t4  p1, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_Quaternion_t19 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, Quaternion_t19  p1, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, *((Quaternion_t19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Quaternion_t19 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Quaternion_t19  p1, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((Quaternion_t19 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t80_Matrix4x4_t80 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t80  (*Func)(void* obj, Matrix4x4_t80  p1, const MethodInfo* method);
	Matrix4x4_t80  ret = ((Func)method->method)(obj, *((Matrix4x4_t80 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t80_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t80  (*Func)(void* obj, Vector3_t4  p1, const MethodInfo* method);
	Matrix4x4_t80  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t236_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, Vector3_t4  p1, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t236_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, Vector2_t6  p1, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Ray_t26_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t26  (*Func)(void* obj, Vector3_t4  p1, const MethodInfo* method);
	Ray_t26  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextGenerationSettings_t649_TextGenerationSettings_t649 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t649  (*Func)(void* obj, TextGenerationSettings_t649  p1, const MethodInfo* method);
	TextGenerationSettings_t649  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t649 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t752_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1073_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t2197_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t406  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1337  (*Func)(void* obj, TimeSpan_t1337  p1, const MethodInfo* method);
	TimeSpan_t1337  ret = ((Func)method->method)(obj, *((TimeSpan_t1337 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t406_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, TimeSpan_t1337  p1, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, *((TimeSpan_t1337 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1337_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1337  (*Func)(void* obj, DateTime_t406  p1, const MethodInfo* method);
	TimeSpan_t1337  ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t406_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, DateTime_t406  p1, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t684_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1074_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t1074 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t1070_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

struct Object_t;
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
void* RuntimeInvoker_MonoMethodInfo_t1909_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t1909  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t1909  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MethodAttributes_t1901_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_CallingConventions_t1895_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RegexOptionsU26_t1491 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"
void* RuntimeInvoker_Object_t_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t4  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_RaycastResult_t486_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t486  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RaycastResult_t486  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/EditState
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
struct Object_t;
void* RuntimeInvoker_EditState_t545_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector4_t236_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.AsnDecodeStatus
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
struct Object_t;
void* RuntimeInvoker_AsnDecodeStatus_t1351_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1338_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Category_t1374_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
struct Object_t;
void* RuntimeInvoker_UriHostNameType_t1421_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t406_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t1073_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t1073  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeCode_t2244_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t1667 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t1667  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t1667 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_RuntimeTypeHandle_t1667_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1667  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t1667  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t1668 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t1668  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t1668 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1044  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1044 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t1850 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t1850  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t1850 *)args[0]), method);
	return ret;
}

struct Object_t;
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"
struct Object_t;
void* RuntimeInvoker_MonoEventInfo_t1906_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t1906  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t1906  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TimeSpan_t1337_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1337  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TimeSpan_t1337  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
struct Object_t;
void* RuntimeInvoker_TypeTag_t2024_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeAttributes_t1922_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIVertex_t556_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t556  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIVertex_t556  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_ColorTween_t506 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ColorTween_t506  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ColorTween_t506 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UICharInfo_t689_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t689  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UICharInfo_t689  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UILineInfo_t687_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t687  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UILineInfo_t687  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2815_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2815  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2815  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2953_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2953  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2953  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2984_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2984  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2984  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3059_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3059  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3059  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3255_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3255  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3255  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3293_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3293  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3293  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3314_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3314  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3314  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3407_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3407  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3407  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3427_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3427  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3427  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Byte_t680_Byte_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int16_t752_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int16_t752_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_UInt16_t684_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_UInt16_t684_UInt16_t684 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int16_t752_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int16_t752_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t680_Int16_t752_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_UInt16_t684_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t680_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int64_t1071_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int16_t752_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt16_t684_UInt16_t684_UInt16_t684 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RegexOptionsU26_t1491_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t680_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_ByteU26_t1656_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Color_t65_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t65  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t65 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int16_t752_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_OpCode_t1873_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1873  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1873 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t1337  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t1337 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_ByteU26_t1656_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t* p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint8_t*)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_UInt16U26_t2421_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t* p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint16_t*)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct UserProfileU5BU5D_t793;
#include "UnityEngine_ArrayTypes.h"
void* RuntimeInvoker_Void_t272_UserProfileU5BU5DU26_t1141_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t793** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t793**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_RectU26_t1146 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t304 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t304 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_DecimalU26_t2444_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t1073 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t1073 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct StringBuilder_t657;
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
void* RuntimeInvoker_Void_t272_StringBuilderU26_t2763_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t657 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t657 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

struct Object_t;
struct ObjectU5BU5D_t224;
#include "mscorlib_ArrayTypes.h"
void* RuntimeInvoker_Void_t272_ObjectU5BU5DU26_t2398_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t224** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t224**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Vector2U5BU5D_t237;
void* RuntimeInvoker_Void_t272_Vector2U5BU5DU26_t3957_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2U5BU5D_t237** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2U5BU5D_t237**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct RaycastResultU5BU5D_t2933;
#include "UnityEngine.UI_ArrayTypes.h"
void* RuntimeInvoker_Void_t272_RaycastResultU5BU5DU26_t3958_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2933** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2933**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t555;
void* RuntimeInvoker_Void_t272_UIVertexU5BU5DU26_t3959_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t555** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t555**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t1025;
void* RuntimeInvoker_Void_t272_UICharInfoU5BU5DU26_t3960_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t1025** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t1025**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UILineInfoU5BU5D_t1026;
void* RuntimeInvoker_Void_t272_UILineInfoU5BU5DU26_t3961_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t1026** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t1026**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
void* RuntimeInvoker_Void_t272_GcAchievementDescriptionData_t957_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t957  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t957 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
void* RuntimeInvoker_Void_t272_GcUserProfileData_t956_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t956  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t956 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector3_t4  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector3_t4 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Rect_t304 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t304  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t304 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Vector4_t236 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t236  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t236 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color_t65  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color_t65 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Matrix4x4_t80 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Matrix4x4_t80  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Matrix4x4_t80 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3_t4_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_HitInfo_t983 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t983  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t983 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_OpCode_t1873_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1873  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1873 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit_t24  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit_t24 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Link_t1757 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t1757  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t1757 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_DictionaryEntry_t1430 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t1430  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t1430 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Touch_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Touch_t235  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Touch_t235 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector2_t6  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector2_t6 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Keyframe_t240 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t240  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t240 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_ContactPoint_t246 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint_t246  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint_t246 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_ParticleCollisionEvent_t160 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParticleCollisionEvent_t160  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParticleCollisionEvent_t160 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastResult_t486  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastResult_t486 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Quaternion_t19 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Quaternion_t19  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Quaternion_t19 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_AnimatorClipInfo_t888 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, AnimatorClipInfo_t888  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((AnimatorClipInfo_t888 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_RaycastHit2D_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit2D_t665  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit2D_t665 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t687  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t687 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t689  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t689 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_GcAchievementData_t958 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t958  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t958 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_GcScoreData_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t959  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t959 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_ParameterModifier_t1914 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t1914  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t1914 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_X509ChainStatus_t1331 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t1331  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t1331 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Mark_t1382 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t1382  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t1382 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_UriScheme_t1418 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t1418  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t1418 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_TableRange_t1690 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t1690  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t1690 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Slot_t1767 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1767  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1767 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Slot_t1776 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1776  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1776 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_MonoResource_t1838 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, MonoResource_t1838  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((MonoResource_t1838 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_ILTokenInfo_t1856 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t1856  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t1856 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_LabelData_t1858 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t1858  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelData_t1858 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_LabelFixup_t1857 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t1857  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelFixup_t1857 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t406  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t406 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t1073  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t1073 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t1337  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t1337 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_ByteU26_t1656 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_UIVertex_t556 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t556  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t556 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1071_Int64_t1071_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int64_t1071_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t1337  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t1337 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int64_t1071_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3U26_t1142_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4 * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t4 *)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Int64_t1071_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SingleU26_t1150_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1430_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Color_t65_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t65  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t65 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3_t4_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_SingleU26_t1150_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2815  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2815 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t2953 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2953  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2953 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t2984 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2984  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2984 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3059 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3059  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3059 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3255  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3255 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3293 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3293  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3293 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3314 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3314  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3314 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3407 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3407  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3407 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3427 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3427  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3427 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Int32_t253_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_DecimalU26_t2444_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1073 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1073 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253_Rect_t304 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Rect_t304  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t304 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3U26_t1142_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4 * p1, Vector3_t4 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t4 *)args[0], (Vector3_t4 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_ColorU26_t754_SphericalHarmonicsL2U26_t1157 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t65 * p1, SphericalHarmonicsL2_t847 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t65 *)args[0], (SphericalHarmonicsL2_t847 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2984_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2984  (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t2984  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1430_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Matrix4x4U26_t1143_Matrix4x4U26_t1143 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t80 * p1, Matrix4x4_t80 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t80 *)args[0], (Matrix4x4_t80 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_BoundsU26_t1153_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t225 * p1, Vector3_t4 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t225 *)args[0], (Vector3_t4 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Color_t65_SphericalHarmonicsL2U26_t1157 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t65  p1, SphericalHarmonicsL2_t847 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t65 *)args[0]), (SphericalHarmonicsL2_t847 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Byte_t680_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Byte_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector2U26_t1148_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t6 * p1, Vector2_t6  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t6 *)args[0], *((Vector2_t6 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_ColorU26_t754_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t65 * p1, Color_t65  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Color_t65 *)args[0], *((Color_t65 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Matrix4x4_t80_Matrix4x4U26_t1143 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t80  p1, Matrix4x4_t80 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t80 *)args[0]), (Matrix4x4_t80 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Ray_t26_SingleU26_t1150 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t26  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t26 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Ray_t26_RaycastHitU26_t1186 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t26  p1, RaycastHit_t24 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t26 *)args[0]), (RaycastHit_t24 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_OpCode_t1873_Label_t1864 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1873  p1, Label_t1864  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1873 *)args[0]), *((Label_t1864 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Byte_t680_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_DateTime_t406_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t406  p1, TimeSpan_t1337  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t406 *)args[0]), *((TimeSpan_t1337 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_NavigationU26_t3954_Navigation_t563 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Navigation_t563 * p1, Navigation_t563  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Navigation_t563 *)args[0], *((Navigation_t563 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_ColorBlockU26_t3955_ColorBlock_t516 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ColorBlock_t516 * p1, ColorBlock_t516  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (ColorBlock_t516 *)args[0], *((ColorBlock_t516 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_SpriteStateU26_t3956_SpriteState_t580 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SpriteState_t580 * p1, SpriteState_t580  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (SpriteState_t580 *)args[0], *((SpriteState_t580 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
void* RuntimeInvoker_Boolean_t273_BoneWeight_t801_BoneWeight_t801 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t801  p1, BoneWeight_t801  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t801 *)args[0]), *((BoneWeight_t801 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t6  p1, Vector2_t6  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Vector2_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Quaternion_t19_Quaternion_t19 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t19  p1, Quaternion_t19  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t19 *)args[0]), *((Quaternion_t19 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Rect_t304_Rect_t304 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t304  p1, Rect_t304  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), *((Rect_t304 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Matrix4x4_t80_Matrix4x4_t80 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t80  p1, Matrix4x4_t80  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t80 *)args[0]), *((Matrix4x4_t80 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Bounds_t225_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t225  p1, Vector3_t4  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t225 *)args[0]), *((Vector3_t4 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Bounds_t225_Bounds_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t225  p1, Bounds_t225  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t225 *)args[0]), *((Bounds_t225 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Vector4_t236_Vector4_t236 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t236  p1, Vector4_t236  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t236 *)args[0]), *((Vector4_t236 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_SphericalHarmonicsL2_t847_SphericalHarmonicsL2_t847 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t847  p1, SphericalHarmonicsL2_t847  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t847 *)args[0]), *((SphericalHarmonicsL2_t847 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_HitInfo_t983_HitInfo_t983 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t983  p1, HitInfo_t983  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t983 *)args[0]), *((HitInfo_t983 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Color_t65_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t65  p1, Color_t65  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t65 *)args[0]), *((Color_t65 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Decimal_t1073_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t1073  p1, Decimal_t1073  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), *((Decimal_t1073 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_OpCode_t1873_OpCode_t1873 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, OpCode_t1873  p1, OpCode_t1873  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((OpCode_t1873 *)args[0]), *((OpCode_t1873 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_DateTime_t406_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t406  p1, DateTime_t406  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), *((DateTime_t406 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t1337  p1, TimeSpan_t1337  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t1337 *)args[0]), *((TimeSpan_t1337 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_RaycastResult_t486_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t486  p1, RaycastResult_t486  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t486 *)args[0]), *((RaycastResult_t486 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_UIVertex_t556_UIVertex_t556 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t556  p1, UIVertex_t556  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t556 *)args[0]), *((UIVertex_t556 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_UICharInfo_t689_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t689  p1, UICharInfo_t689  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t689 *)args[0]), *((UICharInfo_t689 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_UILineInfo_t687_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t687  p1, UILineInfo_t687  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t687 *)args[0]), *((UILineInfo_t687 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_DateTimeOffset_t1086_DateTimeOffset_t1086 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1086  p1, DateTimeOffset_t1086  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1086 *)args[0]), *((DateTimeOffset_t1086 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Guid_t1087_Guid_t1087 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t1087  p1, Guid_t1087  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t1087 *)args[0]), *((Guid_t1087 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t80_Matrix4x4U26_t1143_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t80  (*Func)(void* obj, Matrix4x4_t80 * p1, int8_t p2, const MethodInfo* method);
	Matrix4x4_t80  ret = ((Func)method->method)(obj, (Matrix4x4_t80 *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t682_Int16_t752_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t680_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3427_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3427  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t3427  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t80_Matrix4x4_t80_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t80  (*Func)(void* obj, Matrix4x4_t80  p1, int8_t p2, const MethodInfo* method);
	Matrix4x4_t80  ret = ((Func)method->method)(obj, *((Matrix4x4_t80 *)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t752_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Double_t1070_Double_t1070_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t752_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_DecimalU26_t2444_UInt64U26_t2410 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1073 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1073 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_DecimalU26_t2444_Int64U26_t2404 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1073 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1073 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_DecimalU26_t2444_DecimalU26_t2444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1073 * p1, Decimal_t1073 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1073 *)args[0], (Decimal_t1073 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1337_Double_t1070_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1337  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t1337  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_MoveDirection_t482_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t65_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t65  (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	Color_t65  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_MonoMethodInfoU26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t1909 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t1909 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_RaycastResult_t486_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t486  p1, RaycastResult_t486  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t486 *)args[0]), *((RaycastResult_t486 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_RaycastHit_t24_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t24  p1, RaycastHit_t24  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t24 *)args[0]), *((RaycastHit_t24 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Double_t1070 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t752_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Decimal_t1073_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1073  p1, Decimal_t1073  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), *((Decimal_t1073 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t752_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_IntPtr_t_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_DateTime_t406_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t406  p1, DateTime_t406  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), *((DateTime_t406 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t406_DateTime_t406_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, DateTime_t406  p1, int32_t p2, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t1337  p1, TimeSpan_t1337  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t1337 *)args[0]), *((TimeSpan_t1337 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t6  p1, Vector2_t6  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Vector2_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_UIVertex_t556_UIVertex_t556 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t556  p1, UIVertex_t556  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t556 *)args[0]), *((UIVertex_t556 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_UICharInfo_t689_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t689  p1, UICharInfo_t689  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t689 *)args[0]), *((UICharInfo_t689 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_UILineInfo_t687_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t687  p1, UILineInfo_t687  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t687 *)args[0]), *((UILineInfo_t687 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_DateTimeOffset_t1086_DateTimeOffset_t1086 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1086  p1, DateTimeOffset_t1086  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1086 *)args[0]), *((DateTimeOffset_t1086 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Guid_t1087_Guid_t1087 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t1087  p1, Guid_t1087  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t1087 *)args[0]), *((Guid_t1087 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_BoundsU26_t1153_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t225 * p1, Vector3_t4 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t225 *)args[0], (Vector3_t4 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2815_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2815  p1, KeyValuePair_2_t2815  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2815 *)args[0]), *((KeyValuePair_2_t2815 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// UnityEngine.Internal_DrawArguments
#include "UnityEngine_UnityEngine_Internal_DrawArguments.h"
void* RuntimeInvoker_Void_t272_Object_t_Internal_DrawArgumentsU26_t1149 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Internal_DrawArguments_t832 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Internal_DrawArguments_t832 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_Single_t254_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, float p1, Vector3_t4 * p2, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, *((float*)args[0]), (Vector3_t4 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Vector2U26_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t6 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t6 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_ColorU26_t754 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t65 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Color_t65 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct UriFormatException_t1420;
// System.UriFormatException
#include "System_System_UriFormatException.h"
void* RuntimeInvoker_Void_t272_Object_t_UriFormatExceptionU26_t1493 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t1420 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t1420 **)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct ObjectU5BU5D_t224;
struct Object_t;
void* RuntimeInvoker_Void_t272_ObjectU5BU5DU26_t2398_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t224** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t224**)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_MonoEventInfoU26_t2748 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t1906 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t1906 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_MonoEnumInfoU26_t2778 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t2208 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t2208 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_ObjectU26_t1197_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t6  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t6 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Matrix4x4_t80 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Matrix4x4_t80  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Matrix4x4_t80 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Rect_t304_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t304 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t6  p1, Vector2_t6  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Vector2_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t6_Vector2_t6_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, Vector2_t6  p1, float p2, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Vector3_t4_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Vector3_t4  p1, float p2, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Single_t254_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, float p1, Vector3_t4  p2, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t4 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t65_Color_t65_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t65  (*Func)(void* obj, Color_t65  p1, float p2, const MethodInfo* method);
	Color_t65  ret = ((Func)method->method)(obj, *((Color_t65 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t65_Single_t254_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t65  (*Func)(void* obj, float p1, Color_t65  p2, const MethodInfo* method);
	Color_t65  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Color_t65 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Quaternion_t19_Quaternion_t19 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Quaternion_t19  p1, Quaternion_t19  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Quaternion_t19 *)args[0]), *((Quaternion_t19 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_Single_t254_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, float p1, Vector3_t4  p2, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t4 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Bounds_t225_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t225  p1, Vector3_t4  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t225 *)args[0]), *((Vector3_t4 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Vector4_t236_Vector4_t236 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t236  p1, Vector4_t236  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t236 *)args[0]), *((Vector4_t236 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t236_Vector4_t236_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, Vector4_t236  p1, float p2, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, *((Vector4_t236 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t236_Single_t254_Vector4_t236 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, float p1, Vector4_t236  p2, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector4_t236 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t65  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t65 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Vector4_t236 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector4_t236  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector4_t236 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t847_SphericalHarmonicsL2_t847_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t847  (*Func)(void* obj, SphericalHarmonicsL2_t847  p1, float p2, const MethodInfo* method);
	SphericalHarmonicsL2_t847  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t847 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t847_Single_t254_SphericalHarmonicsL2_t847 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t847  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t847  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t847  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t847 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t4  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t4 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t1044  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1044 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct IPAddress_t1298;
// System.Net.IPAddress
#include "System_System_Net_IPAddress.h"
void* RuntimeInvoker_Boolean_t273_Object_t_IPAddressU26_t1487 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t1298 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t1298 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct IPv6Address_t1299;
// System.Net.IPv6Address
#include "System_System_Net_IPv6Address.h"
void* RuntimeInvoker_Boolean_t273_Object_t_IPv6AddressU26_t1488 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t1299 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t1299 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int64U26_t2404 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_UInt32U26_t2407 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_UInt64U26_t2410 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_ByteU26_t1656 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_SByteU26_t2415 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int16U26_t2418 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_UInt16U26_t2421 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_DoubleU26_t2441 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_RuntimeFieldHandle_t1668 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t1668  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t1668 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_OpCode_t1873_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1873  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1873 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t406  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t406 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t1071_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t1073  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t1073 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_ObjectU26_t1197_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_KeyValuePair_2U26_t3962 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2815 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (KeyValuePair_2_t2815 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t6  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Rect_t304_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t304  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_TextGenerationSettings_t649 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t649  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t649 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_DateTime_t406_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t406  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t1071_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_Vector3U26_t1142_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, Vector3_t4 * p1, Vector3_t4 * p2, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, (Vector3_t4 *)args[0], (Vector3_t4 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_BoundsU26_t1153_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Bounds_t225 * p1, Vector3_t4 * p2, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, (Bounds_t225 *)args[0], (Vector3_t4 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t1070_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1071_IntPtr_t_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2815  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2815 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector4_t236_Vector4_t236_Rect_t304 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, Vector4_t236  p1, Rect_t304  p2, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, *((Vector4_t236 *)args[0]), *((Rect_t304 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t6_Vector2_t6_Rect_t304 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, Vector2_t6  p1, Rect_t304  p2, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Rect_t304 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t6_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, Vector2_t6  p1, Vector2_t6  p2, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Vector2_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_Quaternion_t19_Quaternion_t19 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, Quaternion_t19  p1, Quaternion_t19  p2, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, *((Quaternion_t19 *)args[0]), *((Quaternion_t19 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Quaternion_t19_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Quaternion_t19  p1, Vector3_t4  p2, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((Quaternion_t19 *)args[0]), *((Vector3_t4 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t80_Matrix4x4_t80_Matrix4x4_t80 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t80  (*Func)(void* obj, Matrix4x4_t80  p1, Matrix4x4_t80  p2, const MethodInfo* method);
	Matrix4x4_t80  ret = ((Func)method->method)(obj, *((Matrix4x4_t80 *)args[0]), *((Matrix4x4_t80 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t236_Matrix4x4_t80_Vector4_t236 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, Matrix4x4_t80  p1, Vector4_t236  p2, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, *((Matrix4x4_t80 *)args[0]), *((Vector4_t236 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t236_Vector4_t236_Vector4_t236 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t236  (*Func)(void* obj, Vector4_t236  p1, Vector4_t236  p2, const MethodInfo* method);
	Vector4_t236  ret = ((Func)method->method)(obj, *((Vector4_t236 *)args[0]), *((Vector4_t236 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t847_SphericalHarmonicsL2_t847_SphericalHarmonicsL2_t847 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t847  (*Func)(void* obj, SphericalHarmonicsL2_t847  p1, SphericalHarmonicsL2_t847  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t847  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t847 *)args[0]), *((SphericalHarmonicsL2_t847 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1073_Decimal_t1073_Decimal_t1073 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, Decimal_t1073  p1, Decimal_t1073  p2, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, *((Decimal_t1073 *)args[0]), *((Decimal_t1073 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t406_DateTime_t406_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, DateTime_t406  p1, TimeSpan_t1337  p2, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), *((TimeSpan_t1337 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1337_DateTime_t406_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1337  (*Func)(void* obj, DateTime_t406  p1, DateTime_t406  p2, const MethodInfo* method);
	TimeSpan_t1337  ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), *((DateTime_t406 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1337  (*Func)(void* obj, TimeSpan_t1337  p1, TimeSpan_t1337  p2, const MethodInfo* method);
	TimeSpan_t1337  ret = ((Func)method->method)(obj, *((TimeSpan_t1337 *)args[0]), *((TimeSpan_t1337 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1337_DateTime_t406_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1337  (*Func)(void* obj, DateTime_t406  p1, TimeSpan_t1337  p2, const MethodInfo* method);
	TimeSpan_t1337  ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), *((TimeSpan_t1337 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t224;
void* RuntimeInvoker_Int32_t253_Object_t_ObjectU5BU5DU26_t2398 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t224** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t224**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t6  p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
struct Object_t;
void* RuntimeInvoker_GCHandle_t1941_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t1941  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t1941  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1430_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3407_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3407  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t3407  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t1071_Object_t_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1430_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Ray_t26_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t26  p1, float p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t26 *)args[0]), *((float*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t4_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t4  p1, float p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((float*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Object_t_TextGenerationSettings_t649 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t649  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t649 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileType.h"
void* RuntimeInvoker_MonoFileType_t1822_IntPtr_t_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2953_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2953  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2953  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3059_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3059  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t3059  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t682_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3255_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3255  (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	KeyValuePair_2_t3255  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3293_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3293  (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t3293  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t304_Object_t_RectU26_t1146 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t304  (*Func)(void* obj, Object_t * p1, Rect_t304 * p2, const MethodInfo* method);
	Rect_t304  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Rect_t304 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t4_Object_t_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Object_t * p1, Vector3_t4 * p2, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t4 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Ray_t26_Object_t_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t26  (*Func)(void* obj, Object_t * p1, Vector3_t4 * p2, const MethodInfo* method);
	Ray_t26  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t4 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t680_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t680_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"
struct Object_t;
void* RuntimeInvoker_FileAttributes_t1813_Object_t_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Byte_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t4_Object_t_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Object_t * p1, Vector2_t6  p2, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t6_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, Vector2_t6  p1, Object_t * p2, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Ray_t26_Object_t_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t26  (*Func)(void* obj, Object_t * p1, Vector2_t6  p2, const MethodInfo* method);
	Ray_t26  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t752_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t684_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t1071_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2815  p2, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2815 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t1074_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t1070_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t1070_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3314_Object_t_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3314  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2815  p2, const MethodInfo* method);
	KeyValuePair_2_t3314  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2815 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2815_Object_t_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2815  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2815  p2, const MethodInfo* method);
	KeyValuePair_2_t2815  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2815 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t4 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t4 *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DictionaryNode_t1265;
// System.Collections.Specialized.ListDictionary/DictionaryNode
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t1486 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t1265 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t1265 **)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t304_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t304  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Rect_t304  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t1519_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t1453 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t1453  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t1453 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t1073_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t1715_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t1044  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1044 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t406_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1430  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1430  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2815_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2815  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2815  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2815  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2815 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_UInt16_t684_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int16_t752_Int16_t752_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int16_t752_Int16_t752_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t65  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t65 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int64_t1071_Int64_t1071_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int64_t1071_Int32_t253_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct PointerEventData_t215;
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
void* RuntimeInvoker_Boolean_t273_Int32_t253_PointerEventDataU26_t744_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, PointerEventData_t215 ** p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (PointerEventData_t215 **)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int16_t752_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Exception_t232;
// System.Exception
#include "mscorlib_System_Exception.h"
void* RuntimeInvoker_Boolean_t273_Int32_t253_SByte_t274_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t232 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t232 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct ObjectU5BU5D_t224;
void* RuntimeInvoker_Void_t272_ObjectU5BU5DU26_t2398_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t224** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t224**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Vector2U5BU5D_t237;
void* RuntimeInvoker_Void_t272_Vector2U5BU5DU26_t3957_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2U5BU5D_t237** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2U5BU5D_t237**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct RaycastResultU5BU5D_t2933;
void* RuntimeInvoker_Void_t272_RaycastResultU5BU5DU26_t3958_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2933** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2933**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t555;
void* RuntimeInvoker_Void_t272_UIVertexU5BU5DU26_t3959_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t555** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t555**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t1025;
void* RuntimeInvoker_Void_t272_UICharInfoU5BU5DU26_t3960_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t1025** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t1025**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UILineInfoU5BU5D_t1026;
void* RuntimeInvoker_Void_t272_UILineInfoU5BU5DU26_t3961_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t1026** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t1026**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Color_t65  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((Color_t65 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct ByteU5BU5D_t850;
void* RuntimeInvoker_Void_t272_SByte_t274_ByteU5BU5DU26_t1657_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t850** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t850**)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_SByte_t274_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_IntPtr_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Byte_t680_Byte_t680_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Byte_t680_Byte_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253_RectU26_t1146 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Rect_t304 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Rect_t304 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32U26_t753_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253_Rect_t304 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Rect_t304  p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((Rect_t304 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Int64_t1071_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32U26_t753_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Int64U5BU5D_t2278;
struct StringU5BU5D_t243;
void* RuntimeInvoker_Boolean_t273_Int32_t253_Int64U5BU5DU26_t2792_StringU5BU5DU26_t2793 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t2278** p2, StringU5BU5D_t243** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t2278**)args[1], (StringU5BU5D_t243**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t682_Object_t_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3_t4_Color_t65_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4  p1, Color_t65  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Color_t65 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Ray_t26_RaycastHitU26_t1186_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t26  p1, RaycastHit_t24 * p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t26 *)args[0]), (RaycastHit_t24 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3U26_t1142_ColorU26_t754_SphericalHarmonicsL2U26_t1157 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4 * p1, Color_t65 * p2, SphericalHarmonicsL2_t847 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t4 *)args[0], (Color_t65 *)args[1], (SphericalHarmonicsL2_t847 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_RayU26_t1154_BoundsU26_t1153_SingleU26_t1150 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t26 * p1, Bounds_t225 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t26 *)args[0], (Bounds_t225 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Double_t1070_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3_t4_Color_t65_SphericalHarmonicsL2U26_t1157 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4  p1, Color_t65  p2, SphericalHarmonicsL2_t847 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Color_t65 *)args[1]), (SphericalHarmonicsL2_t847 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3_t4_Quaternion_t19_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4  p1, Quaternion_t19  p2, Vector3_t4  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Quaternion_t19 *)args[1]), *((Vector3_t4 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3_t4_Vector3_t4_Color_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, Color_t65  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), *((Color_t65 *)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_DateTime_t406_DateTime_t406_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t406  p1, DateTime_t406  p2, TimeSpan_t1337  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t406 *)args[0]), *((DateTime_t406 *)args[1]), *((TimeSpan_t1337 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_IntPtr_t_Int64_t1071_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MoveDirection_t482_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct UserProfileU5BU5D_t793;
struct Object_t;
void* RuntimeInvoker_Void_t272_UserProfileU5BU5DU26_t1141_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t793** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t793**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Vector3_t4 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Vector3_t4 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Matrix4x4U26_t1143_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Matrix4x4_t80 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Matrix4x4_t80 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_ColorU26_t754 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Color_t65 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Color_t65 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Matrix4x4U26_t1143 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Matrix4x4_t80 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Matrix4x4_t80 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Vector3U26_t1142_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t4 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t4 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Vector2U26_t1148_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t6 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t6 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
void* RuntimeInvoker_Void_t272_Object_t_MonoPropertyInfoU26_t2750_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t1910 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t1910 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Matrix4x4_t80_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Matrix4x4_t80  p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Matrix4x4_t80 *)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Rect_t304_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t304 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_AnimatorStateInfo_t887_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, AnimatorStateInfo_t887  p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((AnimatorStateInfo_t887 *)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_OpCode_t1873_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1873  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1873 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_Rect_t304_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Rect_t304  p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t304 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_DecimalU26_t2444_DecimalU26_t2444_DecimalU26_t2444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1073 * p1, Decimal_t1073 * p2, Decimal_t1073 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1073 *)args[0], (Decimal_t1073 *)args[1], (Decimal_t1073 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_RenderBufferU26_t1144_RenderBufferU26_t1144 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t961 * p2, RenderBuffer_t961 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t961 *)args[1], (RenderBuffer_t961 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1338_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_TimeSpan_t1337_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, TimeSpan_t1337  p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TimeSpan_t1337 *)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t1071_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_SingleU26_t1150_SingleU26_t1150 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float* p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (float*)args[1], (float*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Vector3U26_t1142_Vector3U26_t1142_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Vector3_t4 * p1, Vector3_t4 * p2, float p3, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, (Vector3_t4 *)args[0], (Vector3_t4 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_QuaternionU26_t1152_QuaternionU26_t1152_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, Quaternion_t19 * p1, Quaternion_t19 * p2, float p3, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, (Quaternion_t19 *)args[0], (Quaternion_t19 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Vector3U26_t1142_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t4 * p2, Vector3_t4 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t4 *)args[1], (Vector3_t4 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct String_t;
struct String_t;
void* RuntimeInvoker_Void_t272_Object_t_StringU26_t1216_StringU26_t1216 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t850;
struct ByteU5BU5D_t850;
void* RuntimeInvoker_Void_t272_Object_t_ByteU5BU5DU26_t1657_ByteU5BU5DU26_t1657 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t850** p2, ByteU5BU5D_t850** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t850**)args[1], (ByteU5BU5D_t850**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct TypeMetadata_t2035;
// System.Runtime.Serialization.Formatters.Binary.TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type_0.h"
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_TypeMetadataU26_t2762_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, TypeMetadata_t2035 ** p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (TypeMetadata_t2035 **)args[1], (Object_t **)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Byte_t680_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOStat
#include "mscorlib_System_IO_MonoIOStat.h"
void* RuntimeInvoker_Boolean_t273_Object_t_MonoIOStatU26_t2746_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t1821 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t1821 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Vector3_t4_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t4  p2, Vector3_t4  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t4 *)args[1]), *((Vector3_t4 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Vector3_t4_Vector3_t4_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, float p3, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t65_Color_t65_Color_t65_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t65  (*Func)(void* obj, Color_t65  p1, Color_t65  p2, float p3, const MethodInfo* method);
	Color_t65  ret = ((Func)method->method)(obj, *((Color_t65 *)args[0]), *((Color_t65 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t19_Quaternion_t19_Quaternion_t19_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t19  (*Func)(void* obj, Quaternion_t19  p1, Quaternion_t19  p2, float p3, const MethodInfo* method);
	Quaternion_t19  ret = ((Func)method->method)(obj, *((Quaternion_t19 *)args[0]), *((Quaternion_t19 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t682_Object_t_Int32U26_t753_CharU26_t1492 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3U26_t1142_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t4 * p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t4 *)args[0], *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t80_Vector3U26_t1142_QuaternionU26_t1152_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t80  (*Func)(void* obj, Vector3_t4 * p1, Quaternion_t19 * p2, Vector3_t4 * p3, const MethodInfo* method);
	Matrix4x4_t80  ret = ((Func)method->method)(obj, (Vector3_t4 *)args[0], (Quaternion_t19 *)args[1], (Vector3_t4 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Ray_t26_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t26  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t26 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t6_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t6  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t80_Vector3_t4_Quaternion_t19_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t80  (*Func)(void* obj, Vector3_t4  p1, Quaternion_t19  p2, Vector3_t4  p3, const MethodInfo* method);
	Matrix4x4_t80  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Quaternion_t19 *)args[1]), *((Vector3_t4 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t274_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_RectU26_t1146_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t304 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Object_t_Vector2U26_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Vector2_t6 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Vector2_t6 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Ray_t26_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t26  p1, float p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t26 *)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t680_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Double_t1070_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Rect_t304_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t304 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_RectU26_t1146_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t304 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t304 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t6_Rect_t304_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, Rect_t304  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t752_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Vector2_t6_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t6  p1, Vector2_t6  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Vector2_t6 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_RaycastResult_t486_RaycastResult_t486_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t486  p1, RaycastResult_t486  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t486 *)args[0]), *((RaycastResult_t486 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_UIVertex_t556_UIVertex_t556_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t556  p1, UIVertex_t556  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t556 *)args[0]), *((UIVertex_t556 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_UICharInfo_t689_UICharInfo_t689_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t689  p1, UICharInfo_t689  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t689 *)args[0]), *((UICharInfo_t689 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_UILineInfo_t687_UILineInfo_t687_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t687  p1, UILineInfo_t687  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t687 *)args[0]), *((UILineInfo_t687 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Rect_t304_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t304  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t684_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t274_Object_t_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t1151_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TouchScreenKeyboard_InternalConstructorHelperArguments_t835 * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (TouchScreenKeyboard_InternalConstructorHelperArguments_t835 *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t1063_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t224;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_ObjectU5BU5DU26_t2398_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t224** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t224**)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Color_t65_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t65  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t65 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Vector3_t4 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector3_t4  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector3_t4 *)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Rect_t304_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t304 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Vector2U26_t1148_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t6 * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t6 *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t1074_UInt16_t684_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, uint16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), *((uint16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t224;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_ObjectU5BU5DU26_t2398 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t224** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t224**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32U26_t753_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t1071_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_OpCode_t1873_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1873  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1873 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1044  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1044 *)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Rect_t304_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t304  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t6  p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t6 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1044  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1044 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_IntPtr_t_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Object_t_Object_t_SByte_t274_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t232 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t232 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Touch_t235_BooleanU26_t745_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Touch_t235  p1, bool* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Touch_t235 *)args[0]), (bool*)args[1], (bool*)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1338_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t1070_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Object_t_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t1073_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1073  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t1073  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t406_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Module_t1848;
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t753_ModuleU26_t2747 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t1848 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t1848 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t1142_QuaternionU26_t1152 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t4 * p2, Quaternion_t19 * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t4 *)args[1], (Quaternion_t19 *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32U26_t753_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (bool*)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t680_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ISurrogateSelector_t1996;
void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044_ISurrogateSelectorU26_t2759 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t1044  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1044 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector3_t4_Quaternion_t19 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t4  p2, Quaternion_t19  p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t4 *)args[1]), *((Quaternion_t19 *)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t6_Vector2_t6_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, Vector2_t6  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t752_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t684_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t1074_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t1074_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Double_t1070_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t1216 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct MulticastDelegate_t549;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t2450 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t549 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t549 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t232 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t232 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Rect_t304_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Rect_t304  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t304_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t304  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Rect_t304  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Interval_t1389_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t1389  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t1389 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t1044_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1044  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1044 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t406_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t406  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Decimal_t1073_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t1073  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t1073 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t6_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t6  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastResult_t486_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t486  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t486 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Color_t65_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t65  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t65 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UIVertex_t556_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t556  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t556 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UICharInfo_t689_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t689  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t689 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UILineInfo_t687_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t687  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t687 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int16_t752_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_Int32_t253_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Text.RegularExpressions.OpFlags
#include "System_System_Text_RegularExpressions_OpFlags.h"
void* RuntimeInvoker_OpFlags_t1369_SByte_t274_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274_ColorU26_t754_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t65 * p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Color_t65 *)args[2], *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Color_t65_Single_t254_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t65  p1, float p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t65 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274_Color_t65_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t65  p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t65 *)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Single_t254_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_Int32U26_t753_Int32U26_t753_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RenderBufferU26_t1144_RenderBufferU26_t1144_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RenderBuffer_t961 * p1, RenderBuffer_t961 * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (RenderBuffer_t961 *)args[0], (RenderBuffer_t961 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RenderBuffer_t961_RenderBuffer_t961_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RenderBuffer_t961  p1, RenderBuffer_t961  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((RenderBuffer_t961 *)args[0]), *((RenderBuffer_t961 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Vector2_t6_Vector2_t6_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t6  p1, Vector2_t6  p2, float p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Vector2_t6 *)args[1]), *((float*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_UInt16_t684 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_RectU26_t1146_Vector2U26_t1148_Vector2U26_t1148_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304 * p1, Vector2_t6 * p2, Vector2_t6 * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t304 *)args[0], (Vector2_t6 *)args[1], (Vector2_t6 *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Ray_t26_RaycastHitU26_t1186_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t26  p1, RaycastHit_t24 * p2, float p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t26 *)args[0]), (RaycastHit_t24 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Rect_t304_Vector2_t6_Vector2_t6_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304  p1, Vector2_t6  p2, Vector2_t6  p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t304 *)args[0]), *((Vector2_t6 *)args[1]), *((Vector2_t6 *)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32U26_t753_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Rect_t304_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304  p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t304 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_SByte_t274_Object_t_Int32_t253_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t232 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t232 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_ColorU26_t754 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Color_t65 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Color_t65 *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32U26_t753_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t1044  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1044 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Single_t254_Single_t254_SingleU26_t1150_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Vector3_t4_Vector3_t4_RaycastHitU26_t1186_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, RaycastHit_t24 * p3, float p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), (RaycastHit_t24 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_CharU26_t1492_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t304_Single_t254_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t304  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Rect_t304  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t80_Single_t254_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t80  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t80  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t2251;
void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_ObjectU26_t1197_HeaderU5BU5DU26_t2760 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t2251** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t2251**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_SByte_t274_Int32U26_t753_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t232 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t232 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_SByte_t274_Int64U26_t2404_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t232 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t232 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_SByte_t274_UInt32U26_t2407_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t232 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t232 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_SByte_t274_SByteU26_t2415_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t232 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t232 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_SByte_t274_Int16U26_t2418_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t232 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t232 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_DecimalU26_t2444_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1073 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1073 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1071_IntPtr_t_Int64_t1071_Int32_t253_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Vector3U26_t1142_Vector3U26_t1142_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t4 * p2, Vector3_t4 * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t4 *)args[1], (Vector3_t4 *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Vector2_t6_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector2_t6  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t6 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_RaycastResult_t486_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, RaycastResult_t486  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RaycastResult_t486 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_UIVertex_t556_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t556  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t556 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_UICharInfo_t689_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t689  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t689 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_UILineInfo_t687_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t687  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t687 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t665_Vector2_t6_Vector2_t6_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t665  (*Func)(void* obj, Vector2_t6  p1, Vector2_t6  p2, float p3, int32_t p4, const MethodInfo* method);
	RaycastHit2D_t665  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Vector2_t6 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_RayU26_t1154_RaycastHitU26_t1186_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Ray_t26 * p2, RaycastHit_t24 * p3, float p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t26 *)args[1], (RaycastHit_t24 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Ray_t26_RaycastHitU26_t1186_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Ray_t26  p2, RaycastHit_t24 * p3, float p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Ray_t26 *)args[1]), (RaycastHit_t24 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_IntPtr_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t274_SByte_t274_Object_t_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Ray_t26_Single_t254_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t26  p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t26 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t680_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1043;
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
void* RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197_SerializationInfoU26_t2761 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t1043 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t1043 **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Vector3_t4_Vector3_t4_Vector3U26_t1142_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, Vector3_t4 * p3, float p4, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), (Vector3_t4 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3U26_t1142_Vector3U26_t1142_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t4 * p1, Vector3_t4 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t4 *)args[0], (Vector3_t4 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t4_Vector3_t4_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t6_Vector2_t6_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t6  p1, Vector2_t6  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Vector2_t6 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_Int64U26_t2404 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_UInt32U26_t2407 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_ByteU26_t1656 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_UInt16U26_t2421 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_DoubleU26_t2441 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, double* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (double*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Rect_t304_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t304  p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Object_t_SingleU26_t1150_SingleU26_t1150 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float* p3, float* p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (float*)args[2], (float*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Vector2U26_t1148_Object_t_Object_t_Vector2U26_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t6 * p1, Object_t * p2, Object_t * p3, Vector2_t6 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t6 *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Vector2_t6 *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32U26_t753_Object_t_Object_t_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ContractionU5BU5D_t1707;
struct Level2MapU5BU5D_t1708;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_ContractionU5BU5DU26_t2608_Level2MapU5BU5DU26_t2609 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t1707** p3, Level2MapU5BU5D_t1708** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t1707**)args[2], (Level2MapU5BU5D_t1708**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int64U26_t2404_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Vector2_t6_Object_t_Object_t_Vector2U26_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t6  p1, Object_t * p2, Object_t * p3, Vector2_t6 * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t6 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Vector2_t6 *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Vector2_t6_Object_t_Vector3U26_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t6  p2, Object_t * p3, Vector3_t4 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t6 *)args[1]), (Object_t *)args[2], (Vector3_t4 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Vector2_t6_Object_t_Vector2U26_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t6  p2, Object_t * p3, Vector2_t6 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t6 *)args[1]), (Object_t *)args[2], (Vector2_t6 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, TimeSpan_t1337  p3, TimeSpan_t1337  p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((TimeSpan_t1337 *)args[2]), *((TimeSpan_t1337 *)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_RayU26_t1154_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t26 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t26 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t274_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t752_Object_t_BooleanU26_t745_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ILayoutElement_t652;
void* RuntimeInvoker_Single_t254_Object_t_Object_t_Single_t254_ILayoutElementU26_t759 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, Object_t ** p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), (Object_t **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t304_Single_t254_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t304  (*Func)(void* obj, float p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Rect_t304  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t6  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector2_t6 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t406_Object_t_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t406  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t406  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t680_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t6_Vector2_t6_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t6  p1, Vector2_t6  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Vector2_t6 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastResult_t486_RaycastResult_t486_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t486  p1, RaycastResult_t486  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t486 *)args[0]), *((RaycastResult_t486 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastHit_t24_RaycastHit_t24_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastHit_t24  p1, RaycastHit_t24  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastHit_t24 *)args[0]), *((RaycastHit_t24 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UIVertex_t556_UIVertex_t556_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t556  p1, UIVertex_t556  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t556 *)args[0]), *((UIVertex_t556 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UICharInfo_t689_UICharInfo_t689_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t689  p1, UICharInfo_t689  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t689 *)args[0]), *((UICharInfo_t689 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UILineInfo_t687_UILineInfo_t687_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t687  p1, UILineInfo_t687  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t687 *)args[0]), *((UILineInfo_t687 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t406_Nullable_1_t2292_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t406  p1, Nullable_1_t2292  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t406 *)args[0]), *((Nullable_1_t2292 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t1071_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t1071_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1044_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1044  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1044 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2815_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2815  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2815 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int16_t752_Int16_t752_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Rect_t304_SByte_t274_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304  p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t304 *)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Color_t65_Single_t254_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t65  p1, float p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t65 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int16_t752_Object_t_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int64_t1071_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Vector3_t4_Single_t254_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, Vector3_t4  p2, float p3, float p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t4 *)args[1]), *((float*)args[2]), *((float*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Single_t254_Single_t254_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3U26_t1142_Vector3U26_t1142_ColorU26_t754_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4 * p1, Vector3_t4 * p2, Color_t65 * p3, float p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t4 *)args[0], (Vector3_t4 *)args[1], (Color_t65 *)args[2], *((float*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Double_t1070_SByte_t274_SByte_t274_DateTime_t406 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t406  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t406 *)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector3_t4_Vector3_t4_Color_t65_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, Color_t65  p3, float p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), *((Color_t65 *)args[2]), *((float*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Vector3U26_t1142_Vector3U26_t1142_RaycastHitU26_t1186_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t4 * p1, Vector3_t4 * p2, RaycastHit_t24 * p3, float p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t4 *)args[0], (Vector3_t4 *)args[1], (RaycastHit_t24 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Int32U26_t753_Object_t_SByte_t274_SByte_t274_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t232 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t232 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t2251;
void* RuntimeInvoker_Void_t272_Byte_t680_Object_t_SByte_t274_ObjectU26_t1197_HeaderU5BU5DU26_t2760 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t2251** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t2251**)args[4], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t273_Vector3_t4_Vector3_t4_RaycastHitU26_t1186_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, RaycastHit_t24 * p3, float p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), (RaycastHit_t24 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_CharU26_t1492_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32U26_t753_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Single_t254_Single_t254_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_RectU26_t1146_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t304 * p2, Object_t * p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t304 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_BooleanU26_t745_SByte_t274_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Single_t254_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Rect_t304_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t304  p2, Object_t * p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t304 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Int32U26_t753_Object_t_Int32U26_t753_SByte_t274_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t232 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t232 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1043;
void* RuntimeInvoker_Void_t272_Byte_t680_Object_t_Int64U26_t2404_ObjectU26_t1197_SerializationInfoU26_t2761 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t1043 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t1043 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, float p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_DecimalU26_t2444_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t1073 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t1073 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_SByte_t274_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2U26_t1148_Single_t254_Int32_t253_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t6 * p1, float p2, int32_t p3, float p4, float p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector2_t6 *)args[0], *((float*)args[1]), *((int32_t*)args[2]), *((float*)args[3]), *((float*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Int32_t253_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_IntPtr_t_Object_t_Int32_t253_Int32_t253_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_RectU26_t1146_Object_t_Int32_t253_Vector2U26_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t304 * p2, Object_t * p3, int32_t p4, Vector2_t6 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t304 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t6 *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t850;
void* RuntimeInvoker_Void_t272_Object_t_Int32U26_t753_ByteU26_t1656_Int32U26_t753_ByteU5BU5DU26_t1657 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t850** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t850**)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Rect_t304_Object_t_Int32_t253_Vector2U26_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t304  p2, Object_t * p3, int32_t p4, Vector2_t6 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t304 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t6 *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t274_SByte_t274_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_Int32_t253_Int32_t253_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Vector3_t4_Vector3_t4_Vector3_t4_Vector3_t4_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, Vector3_t4  p3, Vector3_t4  p4, float p5, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), *((Vector3_t4 *)args[2]), *((Vector3_t4 *)args[3]), *((float*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t752_Object_t_BooleanU26_t745_BooleanU26_t745_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1043;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int64_t1071_ObjectU26_t1197_SerializationInfoU26_t2761 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t1043 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t1043 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32U26_t753_Object_t_Object_t_BooleanU26_t745_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_TimeSpan_t1337_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, TimeSpan_t1337  p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((TimeSpan_t1337 *)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t253_Int32_t253_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t680_Object_t_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Object_t_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Int16_t752_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int64_t1071_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int64_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_TimeSpan_t1337_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, TimeSpan_t1337  p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((TimeSpan_t1337 *)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Int32_t253_SByte_t274_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1043;
void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274_Int64U26_t2404_ObjectU26_t1197_SerializationInfoU26_t2761 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t1043 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t1043 **)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Color32_t654_Int32_t253_Int32_t253_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color32_t654  p2, int32_t p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t654 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t254_Single_t254_Single_t254_SingleU26_t1150_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t80_Single_t254_Single_t254_Single_t254_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t80  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t80  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Single_t254_Vector3U26_t1142_Single_t254_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Vector3_t4 * p3, float p4, float p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Vector3_t4 *)args[2], *((float*)args[3]), *((float*)args[4]), *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/Context
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Int32_t253_Int32_t253_SByte_t274_ContextU26_t2611 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t1701 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t1701 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t665_Vector2_t6_Vector2_t6_Single_t254_Int32_t253_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t665  (*Func)(void* obj, Vector2_t6  p1, Vector2_t6  p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	RaycastHit2D_t665  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Vector2_t6 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_UInt64U2AU26_t2784_Int32U2AU26_t2785_CharU2AU26_t2786_CharU2AU26_t2786_Int64U2AU26_t2787_Int32U2AU26_t2785 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t274_SByte_t274_SByte_t274_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253_MonoIOErrorU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Object_t_Int32_t253_CharU26_t1492_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_SByte_t274_Int32U26_t753_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t232 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t232 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_SByte_t274_Int64U26_t2404_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t232 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t232 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_SByte_t274_UInt32U26_t2407_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t232 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t232 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_SByte_t274_UInt64U26_t2410_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t232 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t232 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Object_t_SByte_t274_DoubleU26_t2441_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t232 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t232 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t4_Vector3_t4_Vector3_t4_Vector3U26_t1142_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t4  (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, Vector3_t4 * p3, float p4, float p5, float p6, const MethodInfo* method);
	Vector3_t4  ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), (Vector3_t4 *)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2U26_t1148_Vector2U26_t1148_Single_t254_Int32_t253_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t6 * p1, Vector2_t6 * p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector2_t6 *)args[0], (Vector2_t6 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2134;
// System.Text.DecoderFallbackBuffer
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_DecoderFallbackBufferU26_t2766 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t2134 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t2134 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Rect_t304_Single_t254_Single_t254_Single_t254_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Rect_t304  p1, float p2, float p3, float p4, float p5, Object_t * p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Single_t254_Single_t254_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3U26_t1142_Vector3U26_t1142_Single_t254_Vector3U26_t1142_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t4 * p1, Vector3_t4 * p2, float p3, Vector3_t4 * p4, float p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t4 *)args[0], (Vector3_t4 *)args[1], *((float*)args[2]), (Vector3_t4 *)args[3], *((float*)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32U26_t753_Int32U26_t753_Int32U26_t753_BooleanU26_t745_StringU26_t1216 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct CodePointIndexer_t1692;
// Mono.Globalization.Unicode.CodePointIndexer
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
struct CodePointIndexer_t1692;
void* RuntimeInvoker_Void_t272_Object_t_CodePointIndexerU26_t2610_ByteU2AU26_t2427_ByteU2AU26_t2427_CodePointIndexerU26_t2610_ByteU2AU26_t2427 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t1692 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t1692 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t1692 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t1692 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t4_Vector3_t4_Single_t254_Vector3_t4_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t4  p1, Vector3_t4  p2, float p3, Vector3_t4  p4, float p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t4 *)args[0]), *((Vector3_t4 *)args[1]), *((float*)args[2]), *((Vector3_t4 *)args[3]), *((float*)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_UIVertex_t556_Vector2_t6_Vector2_t6_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UIVertex_t556  p2, Vector2_t6  p3, Vector2_t6  p4, Vector2_t6  p5, Vector2_t6  p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t556 *)args[1]), *((Vector2_t6 *)args[2]), *((Vector2_t6 *)args[3]), *((Vector2_t6 *)args[4]), *((Vector2_t6 *)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2134;
struct ByteU5BU5D_t850;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_DecoderFallbackBufferU26_t2766_ByteU5BU5DU26_t1657_Object_t_Int64_t1071_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t2134 ** p2, ByteU5BU5D_t850** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t2134 **)args[1], (ByteU5BU5D_t850**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t_DateTime_t406_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t406  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t406 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Object_t_Object_t_SByte_t274_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Rect_t304_Single_t254_Single_t254_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Rect_t304  p1, float p2, float p3, float p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t304_Single_t254_Single_t254_Single_t254_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t304  (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Rect_t304  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_Int32_t253_Int32_t253_Object_t_ContextU26_t2611 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t1701 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t1701 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Single_t254_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, float p4, Vector2_t6  p5, Vector2_t6  p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((float*)args[3]), *((Vector2_t6 *)args[4]), *((Vector2_t6 *)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Single_t254_Single_t254_Single_t254_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t253_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Object_t_Int64_t1071_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t_Int64_t1071_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t680_Object_t_SByte_t274_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_SByte_t274_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_UInt32U26_t2407_Int32_t253_UInt32U26_t2407_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_SByte_t274_SByte_t274_SByte_t274_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32_t253_SByte_t274_SByte_t274_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Int32_t253_SByte_t274_SByte_t274_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_IntPtr_t_Rect_t304_Object_t_SByte_t274_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t304  p2, Object_t * p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t304 *)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector2U26_t1148_Vector2U26_t1148_Single_t254_Int32_t253_Single_t254_Single_t254_RaycastHit2DU26_t1187 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t6 * p1, Vector2_t6 * p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t665 * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t6 *)args[0], (Vector2_t6 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t665 *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Vector2_t6_Vector2_t6_Single_t254_Int32_t253_Single_t254_Single_t254_RaycastHit2DU26_t1187 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t6  p1, Vector2_t6  p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t665 * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((Vector2_t6 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t665 *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_SByte_t274_SByte_t274_SByte_t274_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_CharU26_t1492_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Int32_t253_Int32_t253_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32U26_t753_Int32_t253_Int32_t253_Object_t_SByte_t274_ContextU26_t2611 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t1701 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t1701 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253_Int32_t253_Object_t_SByte_t274_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t272_ByteU2AU26_t2427_ByteU2AU26_t2427_DoubleU2AU26_t2428_UInt16U2AU26_t2429_UInt16U2AU26_t2429_UInt16U2AU26_t2429_UInt16U2AU26_t2429 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Int32_t253_DateTimeU26_t2768_DateTimeOffsetU26_t2769_SByte_t274_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t406 * p4, DateTimeOffset_t1086 * p5, int8_t p6, Exception_t232 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t406 *)args[3], (DateTimeOffset_t1086 *)args[4], *((int8_t*)args[5]), (Exception_t232 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Object_t_Int32_t253_Int32_t253_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct EncoderFallbackBuffer_t2143;
// System.Text.EncoderFallbackBuffer
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
struct CharU5BU5D_t554;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_EncoderFallbackBufferU26_t2764_CharU5BU5DU26_t2765 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t2143 ** p6, CharU5BU5D_t554** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t2143 **)args[5], (CharU5BU5D_t554**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t_Int32_t253_Single_t254_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Single_t254_Single_t254_Single_t254_Single_t254_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_StreamingContext_t1044_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, StreamingContext_t1044  p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((StreamingContext_t1044 *)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t6_Vector2_t6_SByte_t274_SByte_t274_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, Vector2_t6  p1, int8_t p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, *((Vector2_t6 *)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int64_t1071_Int64_t1071_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t224;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t253_Object_t_ObjectU5BU5DU26_t2398_Object_t_Object_t_Object_t_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t224** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t224**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274_SByte_t274_SByte_t274_SByte_t274_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct MethodBase_t1102;
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
struct String_t;
void* RuntimeInvoker_Boolean_t273_Int32_t253_SByte_t274_MethodBaseU26_t2744_Int32U26_t753_Int32U26_t753_StringU26_t1216_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t1102 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t1102 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int16_t752_Int32_t253_SByte_t274_ContextU26_t2611 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t1701 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1701 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32_t253_Object_t_Int32_t253_SByte_t274_ContextU26_t2611 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t1701 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1701 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32U26_t753_Int32_t253_Int32_t253_Int32_t253_Object_t_SByte_t274_ContextU26_t2611 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t1701 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t1701 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2134;
struct ByteU5BU5D_t850;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32_t253_Object_t_DecoderFallbackBufferU26_t2766_ByteU5BU5DU26_t1657_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t2134 ** p6, ByteU5BU5D_t850** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t2134 **)args[5], (ByteU5BU5D_t850**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Rect_t304_Int32_t253_Object_t_SByte_t274_Int32_t253_Object_t_Object_t_Int16_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304  p1, int32_t p2, Object_t * p3, int8_t p4, int32_t p5, Object_t * p6, Object_t * p7, int16_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t304 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], *((int16_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2134;
struct ByteU5BU5D_t850;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_DecoderFallbackBufferU26_t2766_ByteU5BU5DU26_t1657_Object_t_Int64_t1071_Int32_t253_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t2134 ** p2, ByteU5BU5D_t850** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t2134 **)args[1], (ByteU5BU5D_t850**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t6_Rect_t304_Vector2_t6_Rect_t304_SByte_t274_SByte_t274_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t6  (*Func)(void* obj, Rect_t304  p1, Vector2_t6  p2, Rect_t304  p3, int8_t p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Vector2_t6  ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), *((Vector2_t6 *)args[1]), *((Rect_t304 *)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Int32U26_t753_BooleanU26_t745_BooleanU26_t745_Int32U26_t753_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Object_t_Object_t_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, int32_t p6, int32_t p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], *((int32_t*)args[5]), *((int32_t*)args[6]), (Object_t *)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Int32_t253_Object_t_SByte_t274_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int64_t1071_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_SByte_t274_Int32U26_t753_BooleanU26_t745_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_Int32_t253_BooleanU26_t745_BooleanU26_t745_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2134;
struct ByteU5BU5D_t850;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Object_t_DecoderFallbackBufferU26_t2766_ByteU5BU5DU26_t1657_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t2134 ** p7, ByteU5BU5D_t850** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t2134 **)args[6], (ByteU5BU5D_t850**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Rect_t304_Single_t254_Single_t254_Single_t254_Single_t254_Object_t_Object_t_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304  p1, float p2, float p3, float p4, float p5, Object_t * p6, Object_t * p7, int8_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t304 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), (Object_t *)args[5], (Object_t *)args[6], *((int8_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t1694;
// Mono.Globalization.Unicode.Contraction
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
void* RuntimeInvoker_Boolean_t273_Object_t_Int32U26_t753_Int32_t253_Int32_t253_Object_t_SByte_t274_Int32_t253_ContractionU26_t2612_ContextU26_t2611 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t1694 ** p8, Context_t1701 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t1694 **)args[7], (Context_t1701 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Rect_t304_Single_t254_Single_t254_Single_t254_Single_t254_Object_t_Object_t_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Rect_t304  p1, float p2, float p3, float p4, float p5, Object_t * p6, Object_t * p7, int8_t p8, int32_t p9, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), (Object_t *)args[5], (Object_t *)args[6], *((int8_t*)args[7]), *((int32_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t232;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t_Int32_t253_DateTimeU26_t2768_SByte_t274_BooleanU26_t745_SByte_t274_ExceptionU26_t2399 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t406 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t232 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t406 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t232 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Rect_t304_Int32_t253_Object_t_SByte_t274_Int32_t253_Object_t_Object_t_Int16_t752_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t304  p1, int32_t p2, Object_t * p3, int8_t p4, int32_t p5, Object_t * p6, Object_t * p7, int16_t p8, Object_t * p9, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t304 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], *((int16_t*)args[7]), (Object_t *)args[8], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Int32_t253_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, Object_t * p9, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], (Object_t *)args[8], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t1694;
void* RuntimeInvoker_Boolean_t273_Object_t_Int32U26_t753_Int32_t253_Int32_t253_Int32_t253_Object_t_SByte_t274_Int32_t253_ContractionU26_t2612_ContextU26_t2611 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t1694 ** p9, Context_t1701 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t1694 **)args[8], (Context_t1701 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2134;
struct ByteU5BU5D_t850;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Object_t_Int32_t253_UInt32U26_t2407_UInt32U26_t2407_Object_t_DecoderFallbackBufferU26_t2766_ByteU5BU5DU26_t1657_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t2134 ** p8, ByteU5BU5D_t850** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t2134 **)args[7], (ByteU5BU5D_t850**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t254_Rect_t304_Single_t254_Single_t254_Single_t254_Single_t254_Object_t_Object_t_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Rect_t304  p1, float p2, float p3, float p4, float p5, Object_t * p6, Object_t * p7, Object_t * p8, Object_t * p9, int8_t p10, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Rect_t304 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], (Object_t *)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, Object_t * p9, Object_t * p10, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], (Object_t *)args[8], (Object_t *)args[9], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Int16_t752_Int16_t752_SByte_t274_SByte_t274_SByte_t274_SByte_t274_SByte_t274_SByte_t274_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_Int32_t253_BooleanU26_t745_BooleanU26_t745_SByte_t274_SByte_t274_ContextU26_t2611 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t1701 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t1701 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2134;
struct ByteU5BU5D_t850;
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Int32_t253_UInt32U26_t2407_UInt32U26_t2407_Object_t_DecoderFallbackBufferU26_t2766_ByteU5BU5DU26_t1657_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t2134 ** p9, ByteU5BU5D_t850** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t2134 **)args[8], (ByteU5BU5D_t850**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t_SByte_t274_DateTimeU26_t2768_DateTimeOffsetU26_t2769_Object_t_Int32_t253_SByte_t274_BooleanU26_t745_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t406 * p5, DateTimeOffset_t1086 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t406 *)args[4], (DateTimeOffset_t1086 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int16_t752_Object_t_Int32_t253_Int32_t253_Int32_t253_SByte_t274_SByte_t274_SByte_t274_SByte_t274_Int16_t752_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t752_Object_t_Int32_t253_Int32_t253_Int32_t253_SByte_t274_SByte_t274_SByte_t274_SByte_t274_Int16_t752_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t272_Int32_t253_Object_t_Object_t_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Color_t65_Int32_t253_Single_t254_Single_t254_Int32_t253_SByte_t274_SByte_t274_Int32_t253_Int32_t253_Int32_t253_Int32_t253_SByte_t274_Int32_t253_Vector2_t6_Vector2_t6_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t65  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, Vector2_t6  p16, Vector2_t6  p17, int8_t p18, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t65 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((Vector2_t6 *)args[15]), *((Vector2_t6 *)args[16]), *((int8_t*)args[17]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Color_t65_Int32_t253_Single_t254_Single_t254_Int32_t253_SByte_t274_SByte_t274_Int32_t253_Int32_t253_Int32_t253_Int32_t253_SByte_t274_Int32_t253_Single_t254_Single_t254_Single_t254_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t65  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t65 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t_ColorU26_t754_Int32_t253_Single_t254_Single_t254_Int32_t253_SByte_t274_SByte_t274_Int32_t253_Int32_t253_Int32_t253_Int32_t253_SByte_t274_Int32_t253_Single_t254_Single_t254_Single_t254_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t65 * p4, int32_t p5, float p6, float p7, int32_t p8, int8_t p9, int8_t p10, int32_t p11, int32_t p12, int32_t p13, int32_t p14, int8_t p15, int32_t p16, float p17, float p18, float p19, float p20, int8_t p21, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t65 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((float*)args[6]), *((int32_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int32_t*)args[13]), *((int8_t*)args[14]), *((int32_t*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((float*)args[19]), *((int8_t*)args[20]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

