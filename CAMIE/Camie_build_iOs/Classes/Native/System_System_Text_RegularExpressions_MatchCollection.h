﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Match
struct Match_t422;
// System.Collections.ArrayList
struct ArrayList_t1271;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.MatchCollection
struct  MatchCollection_t1362  : public Object_t
{
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.MatchCollection::current
	Match_t422 * ___current_0;
	// System.Collections.ArrayList System.Text.RegularExpressions.MatchCollection::list
	ArrayList_t1271 * ___list_1;
};
