﻿#pragma once
#include <stdint.h>
// System.TimeZone
struct TimeZone_t2242;
// System.Object
#include "mscorlib_System_Object.h"
// System.TimeZone
struct  TimeZone_t2242  : public Object_t
{
};
struct TimeZone_t2242_StaticFields{
	// System.TimeZone System.TimeZone::currentTimeZone
	TimeZone_t2242 * ___currentTimeZone_0;
};
