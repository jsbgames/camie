﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyInformationalVersionAttribute
struct  AssemblyInformationalVersionAttribute_t1469  : public Attribute_t805
{
	// System.String System.Reflection.AssemblyInformationalVersionAttribute::name
	String_t* ___name_0;
};
