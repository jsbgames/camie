﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t646;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct  Comparison_1_t514  : public MulticastDelegate_t549
{
};
