﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.UI.InputField/OnValidateInput
struct  OnValidateInput_t548  : public MulticastDelegate_t549
{
};
