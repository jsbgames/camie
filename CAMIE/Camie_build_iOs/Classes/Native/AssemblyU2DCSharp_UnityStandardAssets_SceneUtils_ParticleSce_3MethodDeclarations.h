﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.SceneUtils.ParticleSceneControls
struct ParticleSceneControls_t327;

// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.ctor()
extern "C" void ParticleSceneControls__ctor_m1173 (ParticleSceneControls_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.cctor()
extern "C" void ParticleSceneControls__cctor_m1174 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Awake()
extern "C" void ParticleSceneControls_Awake_m1175 (ParticleSceneControls_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::OnDisable()
extern "C" void ParticleSceneControls_OnDisable_m1176 (ParticleSceneControls_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Previous()
extern "C" void ParticleSceneControls_Previous_m1177 (ParticleSceneControls_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Next()
extern "C" void ParticleSceneControls_Next_m1178 (ParticleSceneControls_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Update()
extern "C" void ParticleSceneControls_Update_m1179 (ParticleSceneControls_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.SceneUtils.ParticleSceneControls::CheckForGuiCollision()
extern "C" bool ParticleSceneControls_CheckForGuiCollision_m1180 (ParticleSceneControls_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Select(System.Int32)
extern "C" void ParticleSceneControls_Select_m1181 (ParticleSceneControls_t327 * __this, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
