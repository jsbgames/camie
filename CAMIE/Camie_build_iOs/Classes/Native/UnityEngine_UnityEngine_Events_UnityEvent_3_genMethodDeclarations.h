﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct UnityEvent_3_t3402;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1002;

// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
extern "C" void UnityEvent_3__ctor_m21733_gshared (UnityEvent_3_t3402 * __this, const MethodInfo* method);
#define UnityEvent_3__ctor_m21733(__this, method) (( void (*) (UnityEvent_3_t3402 *, const MethodInfo*))UnityEvent_3__ctor_m21733_gshared)(__this, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C" MethodInfo_t * UnityEvent_3_FindMethod_Impl_m21734_gshared (UnityEvent_3_t3402 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method);
#define UnityEvent_3_FindMethod_Impl_m21734(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_3_t3402 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_3_FindMethod_Impl_m21734_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t1002 * UnityEvent_3_GetDelegate_m21735_gshared (UnityEvent_3_t3402 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define UnityEvent_3_GetDelegate_m21735(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t1002 * (*) (UnityEvent_3_t3402 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_3_GetDelegate_m21735_gshared)(__this, ___target, ___theFunction, method)
