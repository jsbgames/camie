﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Color>
struct InternalEnumerator_1_t2860;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m14138_gshared (InternalEnumerator_1_t2860 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m14138(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2860 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14138_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14139_gshared (InternalEnumerator_1_t2860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14139(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2860 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14139_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m14140_gshared (InternalEnumerator_1_t2860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14140(__this, method) (( void (*) (InternalEnumerator_1_t2860 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14140_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m14141_gshared (InternalEnumerator_1_t2860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14141(__this, method) (( bool (*) (InternalEnumerator_1_t2860 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14141_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern "C" Color_t65  InternalEnumerator_1_get_Current_m14142_gshared (InternalEnumerator_1_t2860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14142(__this, method) (( Color_t65  (*) (InternalEnumerator_1_t2860 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14142_gshared)(__this, method)
