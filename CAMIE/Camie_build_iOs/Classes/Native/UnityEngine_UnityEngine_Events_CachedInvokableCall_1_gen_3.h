﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t224;
// UnityEngine.Events.InvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct  CachedInvokableCall_1_t3385  : public InvokableCall_1_t3049
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<System.Object>::m_Arg1
	ObjectU5BU5D_t224* ___m_Arg1_1;
};
