﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t1;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Transform>
struct  Comparison_1_t2883  : public MulticastDelegate_t549
{
};
