﻿#pragma once
#include <stdint.h>
// Reporter/Log[]
struct LogU5BU5D_t2900;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Reporter/Log>
struct  List_1_t298  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Reporter/Log>::_items
	LogU5BU5D_t2900* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::_version
	int32_t ____version_3;
};
struct List_1_t298_StaticFields{
	// T[] System.Collections.Generic.List`1<Reporter/Log>::EmptyArray
	LogU5BU5D_t2900* ___EmptyArray_4;
};
