﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct Vector3andSpace_t170;

// System.Void UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::.ctor()
extern "C" void Vector3andSpace__ctor_m448 (Vector3andSpace_t170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
