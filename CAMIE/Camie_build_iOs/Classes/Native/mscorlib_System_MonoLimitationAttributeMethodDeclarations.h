﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoLimitationAttribute
struct MonoLimitationAttribute_t1686;
// System.String
struct String_t;

// System.Void System.MonoLimitationAttribute::.ctor(System.String)
extern "C" void MonoLimitationAttribute__ctor_m8430 (MonoLimitationAttribute_t1686 * __this, String_t* ___comment, const MethodInfo* method) IL2CPP_METHOD_ATTR;
