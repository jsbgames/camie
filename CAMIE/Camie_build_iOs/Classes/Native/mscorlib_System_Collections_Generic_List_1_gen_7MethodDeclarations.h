﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<SCR_Puceron>
struct List_1_t387;
// System.Object
struct Object_t;
// SCR_Puceron
struct SCR_Puceron_t356;
// System.Collections.Generic.IEnumerable`1<SCR_Puceron>
struct IEnumerable_1_t3591;
// System.Collections.Generic.IEnumerator`1<SCR_Puceron>
struct IEnumerator_1_t3592;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<SCR_Puceron>
struct ICollection_1_t3593;
// System.Collections.ObjectModel.ReadOnlyCollection`1<SCR_Puceron>
struct ReadOnlyCollection_1_t2973;
// SCR_Puceron[]
struct SCR_PuceronU5BU5D_t2971;
// System.Predicate`1<SCR_Puceron>
struct Predicate_1_t2974;
// System.Comparison`1<SCR_Puceron>
struct Comparison_1_t2976;
// System.Collections.Generic.List`1/Enumerator<SCR_Puceron>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_12.h"

// System.Void System.Collections.Generic.List`1<SCR_Puceron>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m1803(__this, method) (( void (*) (List_1_t387 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m15707(__this, ___collection, method) (( void (*) (List_1_t387 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::.ctor(System.Int32)
#define List_1__ctor_m15708(__this, ___capacity, method) (( void (*) (List_1_t387 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::.cctor()
#define List_1__cctor_m15709(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15710(__this, method) (( Object_t* (*) (List_1_t387 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m15711(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t387 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15712(__this, method) (( Object_t * (*) (List_1_t387 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m15713(__this, ___item, method) (( int32_t (*) (List_1_t387 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m15714(__this, ___item, method) (( bool (*) (List_1_t387 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m15715(__this, ___item, method) (( int32_t (*) (List_1_t387 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m15716(__this, ___index, ___item, method) (( void (*) (List_1_t387 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m15717(__this, ___item, method) (( void (*) (List_1_t387 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15718(__this, method) (( bool (*) (List_1_t387 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15719(__this, method) (( bool (*) (List_1_t387 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m15720(__this, method) (( Object_t * (*) (List_1_t387 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m15721(__this, method) (( bool (*) (List_1_t387 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m15722(__this, method) (( bool (*) (List_1_t387 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m15723(__this, ___index, method) (( Object_t * (*) (List_1_t387 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m15724(__this, ___index, ___value, method) (( void (*) (List_1_t387 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::Add(T)
#define List_1_Add_m15725(__this, ___item, method) (( void (*) (List_1_t387 *, SCR_Puceron_t356 *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m15726(__this, ___newCount, method) (( void (*) (List_1_t387 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m15727(__this, ___collection, method) (( void (*) (List_1_t387 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m15728(__this, ___enumerable, method) (( void (*) (List_1_t387 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m15729(__this, ___collection, method) (( void (*) (List_1_t387 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<SCR_Puceron>::AsReadOnly()
#define List_1_AsReadOnly_m15730(__this, method) (( ReadOnlyCollection_1_t2973 * (*) (List_1_t387 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::Clear()
#define List_1_Clear_m15731(__this, method) (( void (*) (List_1_t387 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Puceron>::Contains(T)
#define List_1_Contains_m15732(__this, ___item, method) (( bool (*) (List_1_t387 *, SCR_Puceron_t356 *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m15733(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t387 *, SCR_PuceronU5BU5D_t2971*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<SCR_Puceron>::Find(System.Predicate`1<T>)
#define List_1_Find_m15734(__this, ___match, method) (( SCR_Puceron_t356 * (*) (List_1_t387 *, Predicate_1_t2974 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m15735(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2974 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Puceron>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m15736(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t387 *, int32_t, int32_t, Predicate_1_t2974 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<SCR_Puceron>::GetEnumerator()
#define List_1_GetEnumerator_m15737(__this, method) (( Enumerator_t2975  (*) (List_1_t387 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Puceron>::IndexOf(T)
#define List_1_IndexOf_m15738(__this, ___item, method) (( int32_t (*) (List_1_t387 *, SCR_Puceron_t356 *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m15739(__this, ___start, ___delta, method) (( void (*) (List_1_t387 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m15740(__this, ___index, method) (( void (*) (List_1_t387 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::Insert(System.Int32,T)
#define List_1_Insert_m15741(__this, ___index, ___item, method) (( void (*) (List_1_t387 *, int32_t, SCR_Puceron_t356 *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m15742(__this, ___collection, method) (( void (*) (List_1_t387 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<SCR_Puceron>::Remove(T)
#define List_1_Remove_m15743(__this, ___item, method) (( bool (*) (List_1_t387 *, SCR_Puceron_t356 *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Puceron>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m15744(__this, ___match, method) (( int32_t (*) (List_1_t387 *, Predicate_1_t2974 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m15745(__this, ___index, method) (( void (*) (List_1_t387 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::Reverse()
#define List_1_Reverse_m15746(__this, method) (( void (*) (List_1_t387 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::Sort()
#define List_1_Sort_m15747(__this, method) (( void (*) (List_1_t387 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m15748(__this, ___comparison, method) (( void (*) (List_1_t387 *, Comparison_1_t2976 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<SCR_Puceron>::ToArray()
#define List_1_ToArray_m15749(__this, method) (( SCR_PuceronU5BU5D_t2971* (*) (List_1_t387 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::TrimExcess()
#define List_1_TrimExcess_m1804(__this, method) (( void (*) (List_1_t387 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Puceron>::get_Capacity()
#define List_1_get_Capacity_m15750(__this, method) (( int32_t (*) (List_1_t387 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m15751(__this, ___value, method) (( void (*) (List_1_t387 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<SCR_Puceron>::get_Count()
#define List_1_get_Count_m15752(__this, method) (( int32_t (*) (List_1_t387 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<SCR_Puceron>::get_Item(System.Int32)
#define List_1_get_Item_m15753(__this, ___index, method) (( SCR_Puceron_t356 * (*) (List_1_t387 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<SCR_Puceron>::set_Item(System.Int32,T)
#define List_1_set_Item_m15754(__this, ___index, ___value, method) (( void (*) (List_1_t387 *, int32_t, SCR_Puceron_t356 *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
