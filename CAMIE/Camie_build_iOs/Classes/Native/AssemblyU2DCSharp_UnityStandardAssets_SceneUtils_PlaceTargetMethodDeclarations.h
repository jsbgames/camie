﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.SceneUtils.PlaceTargetWithMouse
struct PlaceTargetWithMouse_t328;

// System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::.ctor()
extern "C" void PlaceTargetWithMouse__ctor_m1182 (PlaceTargetWithMouse_t328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::Update()
extern "C" void PlaceTargetWithMouse_Update_m1183 (PlaceTargetWithMouse_t328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
