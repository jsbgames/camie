﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t788;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct IAchievementDescriptionU5BU5D_t1046;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_5MethodDeclarations.h"
#define Action_1__ctor_m18824(__this, ___object, ___method, method) (( void (*) (Action_1_t788 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m17605_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>::Invoke(T)
#define Action_1_Invoke_m18825(__this, ___obj, method) (( void (*) (Action_1_t788 *, IAchievementDescriptionU5BU5D_t1046*, const MethodInfo*))Action_1_Invoke_m17607_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m18826(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t788 *, IAchievementDescriptionU5BU5D_t1046*, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m17609_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m18827(__this, ___result, method) (( void (*) (Action_1_t788 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m17611_gshared)(__this, ___result, method)
