﻿#pragma once
#include <stdint.h>
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1872;
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.Emit.ModuleBuilderTokenGenerator
struct  ModuleBuilderTokenGenerator_t1870  : public Object_t
{
	// System.Reflection.Emit.ModuleBuilder System.Reflection.Emit.ModuleBuilderTokenGenerator::mb
	ModuleBuilder_t1872 * ___mb_0;
};
