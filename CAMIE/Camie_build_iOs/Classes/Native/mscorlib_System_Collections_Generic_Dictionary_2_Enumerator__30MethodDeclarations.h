﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>
struct Enumerator_t3421;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1304;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_34.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__29MethodDeclarations.h"
#define Enumerator__ctor_m21959(__this, ___dictionary, method) (( void (*) (Enumerator_t3421 *, Dictionary_2_t1304 *, const MethodInfo*))Enumerator__ctor_m21868_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21960(__this, method) (( Object_t * (*) (Enumerator_t3421 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21869_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21961(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3421 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21870_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21962(__this, method) (( Object_t * (*) (Enumerator_t3421 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21871_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21963(__this, method) (( Object_t * (*) (Enumerator_t3421 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21872_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m21964(__this, method) (( bool (*) (Enumerator_t3421 *, const MethodInfo*))Enumerator_MoveNext_m21873_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m21965(__this, method) (( KeyValuePair_2_t3418  (*) (Enumerator_t3421 *, const MethodInfo*))Enumerator_get_Current_m21874_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m21966(__this, method) (( String_t* (*) (Enumerator_t3421 *, const MethodInfo*))Enumerator_get_CurrentKey_m21875_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m21967(__this, method) (( bool (*) (Enumerator_t3421 *, const MethodInfo*))Enumerator_get_CurrentValue_m21876_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m21968(__this, method) (( void (*) (Enumerator_t3421 *, const MethodInfo*))Enumerator_VerifyState_m21877_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m21969(__this, method) (( void (*) (Enumerator_t3421 *, const MethodInfo*))Enumerator_VerifyCurrent_m21878_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m21970(__this, method) (( void (*) (Enumerator_t3421 *, const MethodInfo*))Enumerator_Dispose_m21879_gshared)(__this, method)
