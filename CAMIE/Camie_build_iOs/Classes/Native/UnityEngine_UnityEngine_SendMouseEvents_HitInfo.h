﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t78;
// UnityEngine.Camera
struct Camera_t27;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t983 
{
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t78 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t27 * ___camera_1;
};
