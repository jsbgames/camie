﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SceneAndURLLoader
struct SceneAndURLLoader_t315;
// System.String
struct String_t;

// System.Void SceneAndURLLoader::.ctor()
extern "C" void SceneAndURLLoader__ctor_m1161 (SceneAndURLLoader_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneAndURLLoader::Awake()
extern "C" void SceneAndURLLoader_Awake_m1162 (SceneAndURLLoader_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneAndURLLoader::SceneLoad(System.String)
extern "C" void SceneAndURLLoader_SceneLoad_m1163 (SceneAndURLLoader_t315 * __this, String_t* ___sceneName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneAndURLLoader::LoadURL(System.String)
extern "C" void SceneAndURLLoader_LoadURL_m1164 (SceneAndURLLoader_t315 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
