﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIText
struct GUIText_t181;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t192;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityStandardAssets.Utility.SimpleActivatorMenu
struct  SimpleActivatorMenu_t195  : public MonoBehaviour_t3
{
	// UnityEngine.GUIText UnityStandardAssets.Utility.SimpleActivatorMenu::camSwitchButton
	GUIText_t181 * ___camSwitchButton_2;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.SimpleActivatorMenu::objects
	GameObjectU5BU5D_t192* ___objects_3;
	// System.Int32 UnityStandardAssets.Utility.SimpleActivatorMenu::m_CurrentActiveObject
	int32_t ___m_CurrentActiveObject_4;
};
