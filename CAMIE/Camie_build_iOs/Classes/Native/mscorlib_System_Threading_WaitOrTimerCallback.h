﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Threading.WaitOrTimerCallback
struct  WaitOrTimerCallback_t2163  : public MulticastDelegate_t549
{
};
