﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5
struct U3CFOVKickDownU3Ec__Iterator5_t180;
// System.Object
struct Object_t;

// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::.ctor()
extern "C" void U3CFOVKickDownU3Ec__Iterator5__ctor_m479 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFOVKickDownU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m480 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFOVKickDownU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m481 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::MoveNext()
extern "C" bool U3CFOVKickDownU3Ec__Iterator5_MoveNext_m482 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::Dispose()
extern "C" void U3CFOVKickDownU3Ec__Iterator5_Dispose_m483 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator5::Reset()
extern "C" void U3CFOVKickDownU3Ec__Iterator5_Reset_m484 (U3CFOVKickDownU3Ec__Iterator5_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
