﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>
struct Dictionary_2_t407;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Reporter/Log>
struct  ValueCollection_t2929  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Reporter/Log>::dictionary
	Dictionary_2_t407 * ___dictionary_0;
};
