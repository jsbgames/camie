﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Keyframe
struct Keyframe_t240;

// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
extern "C" void Keyframe__ctor_m803 (Keyframe_t240 * __this, float ___time, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Keyframe::get_time()
extern "C" float Keyframe_get_time_m880 (Keyframe_t240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
