﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_MainMenu
struct SCR_MainMenu_t372;

// System.Void SCR_MainMenu::.ctor()
extern "C" void SCR_MainMenu__ctor_m1386 (SCR_MainMenu_t372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_MainMenu::.cctor()
extern "C" void SCR_MainMenu__cctor_m1387 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_MainMenu::Awake()
extern "C" void SCR_MainMenu_Awake_m1388 (SCR_MainMenu_t372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_MainMenu::OnEnable()
extern "C" void SCR_MainMenu_OnEnable_m1389 (SCR_MainMenu_t372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_MainMenu::Update()
extern "C" void SCR_MainMenu_Update_m1390 (SCR_MainMenu_t372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_MainMenu::OpenMenu()
extern "C" void SCR_MainMenu_OpenMenu_m1391 (SCR_MainMenu_t372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_MainMenu::LevelSetsInit()
extern "C" void SCR_MainMenu_LevelSetsInit_m1392 (SCR_MainMenu_t372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_MainMenu::ShowTutorials()
extern "C" void SCR_MainMenu_ShowTutorials_m1393 (SCR_MainMenu_t372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_MainMenu::ShowLevelInfo(System.Int32)
extern "C" void SCR_MainMenu_ShowLevelInfo_m1394 (SCR_MainMenu_t372 * __this, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_MainMenu::StartLevel()
extern "C" void SCR_MainMenu_StartLevel_m1395 (SCR_MainMenu_t372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_MainMenu::ScrollLevelSets(System.Boolean)
extern "C" void SCR_MainMenu_ScrollLevelSets_m1396 (SCR_MainMenu_t372 * __this, bool ___goRight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_MainMenu::get_IsFromSplashScreen()
extern "C" bool SCR_MainMenu_get_IsFromSplashScreen_m1397 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_MainMenu::set_IsFromSplashScreen(System.Boolean)
extern "C" void SCR_MainMenu_set_IsFromSplashScreen_m1398 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
