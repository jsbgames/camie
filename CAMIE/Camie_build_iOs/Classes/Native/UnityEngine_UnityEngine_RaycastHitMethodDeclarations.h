﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RaycastHit
struct RaycastHit_t24;
// UnityEngine.Collider
struct Collider_t138;
// UnityEngine.Rigidbody
struct Rigidbody_t14;
// UnityEngine.Transform
struct Transform_t1;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C" Vector3_t4  RaycastHit_get_point_m693 (RaycastHit_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C" Vector3_t4  RaycastHit_get_normal_m902 (RaycastHit_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C" float RaycastHit_get_distance_m678 (RaycastHit_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C" Collider_t138 * RaycastHit_get_collider_m692 (RaycastHit_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C" Rigidbody_t14 * RaycastHit_get_rigidbody_m980 (RaycastHit_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C" Transform_t1 * RaycastHit_get_transform_m1729 (RaycastHit_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
