﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ReturnToMainMenu
struct ReturnToMainMenu_t314;

// System.Void ReturnToMainMenu::.ctor()
extern "C" void ReturnToMainMenu__ctor_m1156 (ReturnToMainMenu_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReturnToMainMenu::Start()
extern "C" void ReturnToMainMenu_Start_m1157 (ReturnToMainMenu_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReturnToMainMenu::OnLevelWasLoaded(System.Int32)
extern "C" void ReturnToMainMenu_OnLevelWasLoaded_m1158 (ReturnToMainMenu_t314 * __this, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReturnToMainMenu::Update()
extern "C" void ReturnToMainMenu_Update_m1159 (ReturnToMainMenu_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReturnToMainMenu::GoBackToMainMenu()
extern "C" void ReturnToMainMenu_GoBackToMainMenu_m1160 (ReturnToMainMenu_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
