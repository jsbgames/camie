﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Puceron
struct SCR_Puceron_t356;
// UnityEngine.Collider
struct Collider_t138;

// System.Void SCR_Puceron::.ctor()
extern "C" void SCR_Puceron__ctor_m1307 (SCR_Puceron_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Puceron::Start()
extern "C" void SCR_Puceron_Start_m1308 (SCR_Puceron_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Puceron::UnRotate()
extern "C" void SCR_Puceron_UnRotate_m1309 (SCR_Puceron_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Puceron::OnTriggerEnter(UnityEngine.Collider)
extern "C" void SCR_Puceron_OnTriggerEnter_m1310 (SCR_Puceron_t356 * __this, Collider_t138 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Puceron::Update()
extern "C" void SCR_Puceron_Update_m1311 (SCR_Puceron_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Puceron::Collect()
extern "C" void SCR_Puceron_Collect_m1312 (SCR_Puceron_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Puceron::Pool()
extern "C" void SCR_Puceron_Pool_m1313 (SCR_Puceron_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Puceron::ReturnToGame()
extern "C" void SCR_Puceron_ReturnToGame_m1314 (SCR_Puceron_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
