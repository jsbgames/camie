﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// SCR_Camie
struct SCR_Camie_t338;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// SCR_Camie/<LandOnCollisionPoint>c__Iterator5
struct  U3CLandOnCollisionPointU3Ec__Iterator5_t340  : public Object_t
{
	// UnityEngine.Vector3 SCR_Camie/<LandOnCollisionPoint>c__Iterator5::point
	Vector3_t4  ___point_0;
	// UnityEngine.Vector3 SCR_Camie/<LandOnCollisionPoint>c__Iterator5::normal
	Vector3_t4  ___normal_1;
	// System.Int32 SCR_Camie/<LandOnCollisionPoint>c__Iterator5::$PC
	int32_t ___U24PC_2;
	// System.Object SCR_Camie/<LandOnCollisionPoint>c__Iterator5::$current
	Object_t * ___U24current_3;
	// UnityEngine.Vector3 SCR_Camie/<LandOnCollisionPoint>c__Iterator5::<$>point
	Vector3_t4  ___U3CU24U3Epoint_4;
	// UnityEngine.Vector3 SCR_Camie/<LandOnCollisionPoint>c__Iterator5::<$>normal
	Vector3_t4  ___U3CU24U3Enormal_5;
	// SCR_Camie SCR_Camie/<LandOnCollisionPoint>c__Iterator5::<>f__this
	SCR_Camie_t338 * ___U3CU3Ef__this_6;
};
