﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.FollowTarget
struct FollowTarget_t183;

// System.Void UnityStandardAssets.Utility.FollowTarget::.ctor()
extern "C" void FollowTarget__ctor_m494 (FollowTarget_t183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.FollowTarget::LateUpdate()
extern "C" void FollowTarget_LateUpdate_m495 (FollowTarget_t183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
