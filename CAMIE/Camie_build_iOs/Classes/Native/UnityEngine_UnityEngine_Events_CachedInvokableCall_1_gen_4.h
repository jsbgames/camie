﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t224;
// UnityEngine.Events.InvokableCall`1<System.Byte>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_3.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Byte>
struct  CachedInvokableCall_1_t3390  : public InvokableCall_1_t3163
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<System.Byte>::m_Arg1
	ObjectU5BU5D_t224* ___m_Arg1_1;
};
