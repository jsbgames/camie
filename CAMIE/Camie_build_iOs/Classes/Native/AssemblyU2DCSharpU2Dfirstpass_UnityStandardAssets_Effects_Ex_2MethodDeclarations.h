﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.ExplosionPhysicsForce
struct ExplosionPhysicsForce_t143;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce::.ctor()
extern "C" void ExplosionPhysicsForce__ctor_m413 (ExplosionPhysicsForce_t143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionPhysicsForce::Start()
extern "C" Object_t * ExplosionPhysicsForce_Start_m414 (ExplosionPhysicsForce_t143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
