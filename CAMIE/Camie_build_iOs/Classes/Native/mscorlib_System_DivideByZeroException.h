﻿#pragma once
#include <stdint.h>
// System.ArithmeticException
#include "mscorlib_System_ArithmeticException.h"
// System.DivideByZeroException
struct  DivideByZeroException_t2200  : public ArithmeticException_t1638
{
};
