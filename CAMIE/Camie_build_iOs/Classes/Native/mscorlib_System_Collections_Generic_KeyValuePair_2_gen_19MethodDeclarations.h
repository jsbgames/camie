﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>
struct KeyValuePair_2_t3128;
// UnityEngine.UI.Graphic
struct Graphic_t418;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11MethodDeclarations.h"
#define KeyValuePair_2__ctor_m18130(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3128 *, Graphic_t418 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m15568_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m18131(__this, method) (( Graphic_t418 * (*) (KeyValuePair_2_t3128 *, const MethodInfo*))KeyValuePair_2_get_Key_m15569_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18132(__this, ___value, method) (( void (*) (KeyValuePair_2_t3128 *, Graphic_t418 *, const MethodInfo*))KeyValuePair_2_set_Key_m15570_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m18133(__this, method) (( int32_t (*) (KeyValuePair_2_t3128 *, const MethodInfo*))KeyValuePair_2_get_Value_m15571_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18134(__this, ___value, method) (( void (*) (KeyValuePair_2_t3128 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m15572_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m18135(__this, method) (( String_t* (*) (KeyValuePair_2_t3128 *, const MethodInfo*))KeyValuePair_2_ToString_m15573_gshared)(__this, method)
