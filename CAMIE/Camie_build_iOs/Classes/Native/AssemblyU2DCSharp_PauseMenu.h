﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t312;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// PauseMenu
struct  PauseMenu_t313  : public MonoBehaviour_t3
{
	// UnityEngine.UI.Toggle PauseMenu::m_MenuToggle
	Toggle_t312 * ___m_MenuToggle_2;
	// System.Single PauseMenu::m_TimeScaleRef
	float ___m_TimeScaleRef_3;
	// System.Single PauseMenu::m_VolumeRef
	float ___m_VolumeRef_4;
	// System.Boolean PauseMenu::m_Paused
	bool ___m_Paused_5;
};
