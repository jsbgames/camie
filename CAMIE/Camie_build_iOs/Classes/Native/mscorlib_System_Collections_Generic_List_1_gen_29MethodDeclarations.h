﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t1024;
// System.Object
struct Object_t;
// UnityEngine.ParticleSystem
struct ParticleSystem_t161;
// System.Collections.Generic.IEnumerable`1<UnityEngine.ParticleSystem>
struct IEnumerable_1_t3728;
// System.Collections.Generic.IEnumerator`1<UnityEngine.ParticleSystem>
struct IEnumerator_1_t3729;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<UnityEngine.ParticleSystem>
struct ICollection_1_t3730;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.ParticleSystem>
struct ReadOnlyCollection_1_t3224;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t150;
// System.Predicate`1<UnityEngine.ParticleSystem>
struct Predicate_1_t3225;
// System.Comparison`1<UnityEngine.ParticleSystem>
struct Comparison_1_t3227;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m5141(__this, method) (( void (*) (List_1_t1024 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19384(__this, ___collection, method) (( void (*) (List_1_t1024 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::.ctor(System.Int32)
#define List_1__ctor_m19385(__this, ___capacity, method) (( void (*) (List_1_t1024 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::.cctor()
#define List_1__cctor_m19386(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19387(__this, method) (( Object_t* (*) (List_1_t1024 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19388(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1024 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19389(__this, method) (( Object_t * (*) (List_1_t1024 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19390(__this, ___item, method) (( int32_t (*) (List_1_t1024 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19391(__this, ___item, method) (( bool (*) (List_1_t1024 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19392(__this, ___item, method) (( int32_t (*) (List_1_t1024 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19393(__this, ___index, ___item, method) (( void (*) (List_1_t1024 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19394(__this, ___item, method) (( void (*) (List_1_t1024 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19395(__this, method) (( bool (*) (List_1_t1024 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19396(__this, method) (( bool (*) (List_1_t1024 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19397(__this, method) (( Object_t * (*) (List_1_t1024 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19398(__this, method) (( bool (*) (List_1_t1024 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19399(__this, method) (( bool (*) (List_1_t1024 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19400(__this, ___index, method) (( Object_t * (*) (List_1_t1024 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19401(__this, ___index, ___value, method) (( void (*) (List_1_t1024 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Add(T)
#define List_1_Add_m19402(__this, ___item, method) (( void (*) (List_1_t1024 *, ParticleSystem_t161 *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19403(__this, ___newCount, method) (( void (*) (List_1_t1024 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19404(__this, ___collection, method) (( void (*) (List_1_t1024 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19405(__this, ___enumerable, method) (( void (*) (List_1_t1024 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19406(__this, ___collection, method) (( void (*) (List_1_t1024 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::AsReadOnly()
#define List_1_AsReadOnly_m19407(__this, method) (( ReadOnlyCollection_1_t3224 * (*) (List_1_t1024 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Clear()
#define List_1_Clear_m19408(__this, method) (( void (*) (List_1_t1024 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Contains(T)
#define List_1_Contains_m19409(__this, ___item, method) (( bool (*) (List_1_t1024 *, ParticleSystem_t161 *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19410(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1024 *, ParticleSystemU5BU5D_t150*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Find(System.Predicate`1<T>)
#define List_1_Find_m19411(__this, ___match, method) (( ParticleSystem_t161 * (*) (List_1_t1024 *, Predicate_1_t3225 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19412(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3225 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19413(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1024 *, int32_t, int32_t, Predicate_1_t3225 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::GetEnumerator()
#define List_1_GetEnumerator_m19414(__this, method) (( Enumerator_t3226  (*) (List_1_t1024 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::IndexOf(T)
#define List_1_IndexOf_m19415(__this, ___item, method) (( int32_t (*) (List_1_t1024 *, ParticleSystem_t161 *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19416(__this, ___start, ___delta, method) (( void (*) (List_1_t1024 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19417(__this, ___index, method) (( void (*) (List_1_t1024 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Insert(System.Int32,T)
#define List_1_Insert_m19418(__this, ___index, ___item, method) (( void (*) (List_1_t1024 *, int32_t, ParticleSystem_t161 *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19419(__this, ___collection, method) (( void (*) (List_1_t1024 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Remove(T)
#define List_1_Remove_m19420(__this, ___item, method) (( bool (*) (List_1_t1024 *, ParticleSystem_t161 *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19421(__this, ___match, method) (( int32_t (*) (List_1_t1024 *, Predicate_1_t3225 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19422(__this, ___index, method) (( void (*) (List_1_t1024 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Reverse()
#define List_1_Reverse_m19423(__this, method) (( void (*) (List_1_t1024 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Sort()
#define List_1_Sort_m19424(__this, method) (( void (*) (List_1_t1024 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19425(__this, ___comparison, method) (( void (*) (List_1_t1024 *, Comparison_1_t3227 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::ToArray()
#define List_1_ToArray_m5142(__this, method) (( ParticleSystemU5BU5D_t150* (*) (List_1_t1024 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::TrimExcess()
#define List_1_TrimExcess_m19426(__this, method) (( void (*) (List_1_t1024 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::get_Capacity()
#define List_1_get_Capacity_m19427(__this, method) (( int32_t (*) (List_1_t1024 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19428(__this, ___value, method) (( void (*) (List_1_t1024 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::get_Count()
#define List_1_get_Count_m19429(__this, method) (( int32_t (*) (List_1_t1024 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::get_Item(System.Int32)
#define List_1_get_Item_m19430(__this, ___index, method) (( ParticleSystem_t161 * (*) (List_1_t1024 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystem>::set_Item(System.Int32,T)
#define List_1_set_Item_m19431(__this, ___index, ___value, method) (( void (*) (List_1_t1024 *, int32_t, ParticleSystem_t161 *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
