﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t1;
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce.h"
// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_0.h"
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
struct  DemoParticleSystem_t321  : public Object_t
{
	// UnityEngine.Transform UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::transform
	Transform_t1 * ___transform_0;
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::mode
	int32_t ___mode_1;
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::align
	int32_t ___align_2;
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::maxCount
	int32_t ___maxCount_3;
	// System.Single UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::minDist
	float ___minDist_4;
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::camOffset
	int32_t ___camOffset_5;
	// System.String UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::instructionText
	String_t* ___instructionText_6;
};
