﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.GC
struct GC_t2215;
// System.Object
struct Object_t;

// System.Int32 System.GC::get_MaxGeneration()
extern "C" int32_t GC_get_MaxGeneration_m12198 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::InternalCollect(System.Int32)
extern "C" void GC_InternalCollect_m12199 (Object_t * __this /* static, unused */, int32_t ___generation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::Collect()
extern "C" void GC_Collect_m1642 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.GC::GetTotalMemory(System.Boolean)
extern "C" int64_t GC_GetTotalMemory_m1692 (Object_t * __this /* static, unused */, bool ___forceFullCollection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C" void GC_SuppressFinalize_m5136 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
