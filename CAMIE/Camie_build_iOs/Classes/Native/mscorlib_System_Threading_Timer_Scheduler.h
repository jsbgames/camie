﻿#pragma once
#include <stdint.h>
// System.Threading.Timer/Scheduler
struct Scheduler_t2173;
// System.Collections.SortedList
struct SortedList_t1446;
// System.Object
#include "mscorlib_System_Object.h"
// System.Threading.Timer/Scheduler
struct  Scheduler_t2173  : public Object_t
{
	// System.Collections.SortedList System.Threading.Timer/Scheduler::list
	SortedList_t1446 * ___list_1;
};
struct Scheduler_t2173_StaticFields{
	// System.Threading.Timer/Scheduler System.Threading.Timer/Scheduler::instance
	Scheduler_t2173 * ___instance_0;
};
