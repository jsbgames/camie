﻿#pragma once
#include <stdint.h>
// SCR_Waypoint
struct SCR_Waypoint_t358;
// System.Object
struct Object_t;
// SCR_PuceronMove
struct SCR_PuceronMove_t359;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// SCR_PuceronMove/<MoveToWaypoint>c__Iterator9
struct  U3CMoveToWaypointU3Ec__Iterator9_t360  : public Object_t
{
	// SCR_Waypoint SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::wp
	SCR_Waypoint_t358 * ___wp_0;
	// UnityEngine.Vector3 SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::<target>__0
	Vector3_t4  ___U3CtargetU3E__0_1;
	// System.Int32 SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::$PC
	int32_t ___U24PC_2;
	// System.Object SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::$current
	Object_t * ___U24current_3;
	// SCR_Waypoint SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::<$>wp
	SCR_Waypoint_t358 * ___U3CU24U3Ewp_4;
	// SCR_PuceronMove SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::<>f__this
	SCR_PuceronMove_t359 * ___U3CU3Ef__this_5;
};
