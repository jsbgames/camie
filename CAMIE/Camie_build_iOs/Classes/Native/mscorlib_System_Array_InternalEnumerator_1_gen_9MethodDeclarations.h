﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.Link>
struct InternalEnumerator_1_t2819;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_Link.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13667_gshared (InternalEnumerator_1_t2819 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13667(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2819 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13667_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13668_gshared (InternalEnumerator_1_t2819 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13668(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2819 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13668_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13669_gshared (InternalEnumerator_1_t2819 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13669(__this, method) (( void (*) (InternalEnumerator_1_t2819 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13669_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13670_gshared (InternalEnumerator_1_t2819 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13670(__this, method) (( bool (*) (InternalEnumerator_1_t2819 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13670_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern "C" Link_t1757  InternalEnumerator_1_get_Current_m13671_gshared (InternalEnumerator_1_t2819 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13671(__this, method) (( Link_t1757  (*) (InternalEnumerator_1_t2819 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13671_gshared)(__this, method)
