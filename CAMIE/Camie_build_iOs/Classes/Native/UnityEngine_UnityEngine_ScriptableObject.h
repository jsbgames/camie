﻿#pragma once
#include <stdint.h>
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.ScriptableObject
struct  ScriptableObject_t784  : public Object_t164
{
};
// Native definition for marshalling of: UnityEngine.ScriptableObject
struct ScriptableObject_t784_marshaled
{
};
