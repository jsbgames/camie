﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Misc
struct Misc_t561;
// UnityEngine.Object
struct Object_t164;
struct Object_t164_marshaled;

// System.Void UnityEngine.UI.Misc::Destroy(UnityEngine.Object)
extern "C" void Misc_Destroy_m2511 (Object_t * __this /* static, unused */, Object_t164 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Misc::DestroyImmediate(UnityEngine.Object)
extern "C" void Misc_DestroyImmediate_m2512 (Object_t * __this /* static, unused */, Object_t164 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
