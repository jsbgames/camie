﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t2919;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Predicate`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m15084_gshared (Predicate_1_t2919 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m15084(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2919 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15084_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m15085_gshared (Predicate_1_t2919 * __this, Vector2_t6  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m15085(__this, ___obj, method) (( bool (*) (Predicate_1_t2919 *, Vector2_t6 , const MethodInfo*))Predicate_1_Invoke_m15085_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m15086_gshared (Predicate_1_t2919 * __this, Vector2_t6  ___obj, AsyncCallback_t547 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m15086(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2919 *, Vector2_t6 , AsyncCallback_t547 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15086_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m15087_gshared (Predicate_1_t2919 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m15087(__this, ___result, method) (( bool (*) (Predicate_1_t2919 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15087_gshared)(__this, ___result, method)
