﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Scr_ShadowTarget
struct Scr_ShadowTarget_t392;

// System.Void Scr_ShadowTarget::.ctor()
extern "C" void Scr_ShadowTarget__ctor_m1542 (Scr_ShadowTarget_t392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Scr_ShadowTarget::Start()
extern "C" void Scr_ShadowTarget_Start_m1543 (Scr_ShadowTarget_t392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Scr_ShadowTarget::Update()
extern "C" void Scr_ShadowTarget_Update_m1544 (Scr_ShadowTarget_t392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
