﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
struct ThreadSafeDictionary_2_t3318;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1033;
// System.Object
struct Object_t;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t3316;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3338;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3518;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2__ctor_m20682_gshared (ThreadSafeDictionary_2_t3318 * __this, ThreadSafeDictionaryValueFactory_2_t3316 * ___valueFactory, const MethodInfo* method);
#define ThreadSafeDictionary_2__ctor_m20682(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t3318 *, ThreadSafeDictionaryValueFactory_2_t3316 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m20682_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m20684_gshared (ThreadSafeDictionary_2_t3318 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m20684(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3318 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m20684_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Get(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_Get_m20686_gshared (ThreadSafeDictionary_2_t3318 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_Get_m20686(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3318 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m20686_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::AddValue(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_AddValue_m20688_gshared (ThreadSafeDictionary_2_t3318 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_AddValue_m20688(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3318 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m20688_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_Add_m20690_gshared (ThreadSafeDictionary_2_t3318 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m20690(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t3318 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Add_m20690_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C" Object_t* ThreadSafeDictionary_2_get_Keys_m20692_gshared (ThreadSafeDictionary_2_t3318 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Keys_m20692(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t3318 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m20692_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C" bool ThreadSafeDictionary_2_Remove_m20694_gshared (ThreadSafeDictionary_2_t3318 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m20694(__this, ___key, method) (( bool (*) (ThreadSafeDictionary_2_t3318 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m20694_gshared)(__this, ___key, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool ThreadSafeDictionary_2_TryGetValue_m20696_gshared (ThreadSafeDictionary_2_t3318 * __this, Object_t * ___key, Object_t ** ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_TryGetValue_m20696(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t3318 *, Object_t *, Object_t **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m20696_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C" Object_t* ThreadSafeDictionary_2_get_Values_m20698_gshared (ThreadSafeDictionary_2_t3318 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Values_m20698(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t3318 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m20698_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_get_Item_m20700_gshared (ThreadSafeDictionary_2_t3318 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Item_m20700(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3318 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m20700_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_set_Item_m20702_gshared (ThreadSafeDictionary_2_t3318 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_set_Item_m20702(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t3318 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m20702_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2_Add_m20704_gshared (ThreadSafeDictionary_2_t3318 * __this, KeyValuePair_2_t2815  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m20704(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t3318 *, KeyValuePair_2_t2815 , const MethodInfo*))ThreadSafeDictionary_2_Add_m20704_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Clear()
extern "C" void ThreadSafeDictionary_2_Clear_m20706_gshared (ThreadSafeDictionary_2_t3318 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_Clear_m20706(__this, method) (( void (*) (ThreadSafeDictionary_2_t3318 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m20706_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Contains_m20708_gshared (ThreadSafeDictionary_2_t3318 * __this, KeyValuePair_2_t2815  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Contains_m20708(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t3318 *, KeyValuePair_2_t2815 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m20708_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void ThreadSafeDictionary_2_CopyTo_m20710_gshared (ThreadSafeDictionary_2_t3318 * __this, KeyValuePair_2U5BU5D_t3338* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ThreadSafeDictionary_2_CopyTo_m20710(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t3318 *, KeyValuePair_2U5BU5D_t3338*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m20710_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C" int32_t ThreadSafeDictionary_2_get_Count_m20712_gshared (ThreadSafeDictionary_2_t3318 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Count_m20712(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t3318 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m20712_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C" bool ThreadSafeDictionary_2_get_IsReadOnly_m20714_gshared (ThreadSafeDictionary_2_t3318 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_IsReadOnly_m20714(__this, method) (( bool (*) (ThreadSafeDictionary_2_t3318 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m20714_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Remove_m20716_gshared (ThreadSafeDictionary_2_t3318 * __this, KeyValuePair_2_t2815  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m20716(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t3318 *, KeyValuePair_2_t2815 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m20716_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" Object_t* ThreadSafeDictionary_2_GetEnumerator_m20718_gshared (ThreadSafeDictionary_2_t3318 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_GetEnumerator_m20718(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t3318 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m20718_gshared)(__this, method)
