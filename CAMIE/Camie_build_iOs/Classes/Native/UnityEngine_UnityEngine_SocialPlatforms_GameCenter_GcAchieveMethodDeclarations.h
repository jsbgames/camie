﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct GcAchievementDescriptionData_t957;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t978;

// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern "C" AchievementDescription_t978 * GcAchievementDescriptionData_ToAchievementDescription_m4886 (GcAchievementDescriptionData_t957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
