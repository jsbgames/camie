﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform
struct RectTransform_t364;
// UnityEngine.Transform
struct Transform_t1;
// SCR_Camie
struct SCR_Camie_t338;
// UnityEngine.UI.Image
struct Image_t48;
// SCR_JoystickDirection[]
struct SCR_JoystickDirectionU5BU5D_t365;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// SCR_Joystick/Direction
#include "AssemblyU2DCSharp_SCR_Joystick_Direction.h"
// SCR_Joystick
struct  SCR_Joystick_t346  : public MonoBehaviour_t3
{
	// System.Boolean SCR_Joystick::isDragging
	bool ___isDragging_2;
	// UnityEngine.Touch SCR_Joystick::currTouch
	Touch_t235  ___currTouch_3;
	// System.Int32 SCR_Joystick::touchId
	int32_t ___touchId_4;
	// UnityEngine.Vector2 SCR_Joystick::currTouchPosition
	Vector2_t6  ___currTouchPosition_5;
	// System.Single SCR_Joystick::radius
	float ___radius_6;
	// UnityEngine.RectTransform SCR_Joystick::rectTransform
	RectTransform_t364 * ___rectTransform_7;
	// UnityEngine.Transform SCR_Joystick::center
	Transform_t1 * ___center_8;
	// UnityEngine.Vector3 SCR_Joystick::axis
	Vector3_t4  ___axis_9;
	// SCR_Camie SCR_Joystick::avatar
	SCR_Camie_t338 * ___avatar_10;
	// UnityEngine.UI.Image SCR_Joystick::img
	Image_t48 * ___img_11;
	// SCR_JoystickDirection[] SCR_Joystick::directionPoints
	SCR_JoystickDirectionU5BU5D_t365* ___directionPoints_12;
	// SCR_Joystick/Direction SCR_Joystick::pointingTo
	int32_t ___pointingTo_13;
	// SCR_Joystick/Direction SCR_Joystick::activeDirection
	int32_t ___activeDirection_14;
};
