﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>
struct KeyValuePair_2_t2999;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13MethodDeclarations.h"
#define KeyValuePair_2__ctor_m16016(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2999 *, int32_t, bool, const MethodInfo*))KeyValuePair_2__ctor_m15918_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m16017(__this, method) (( int32_t (*) (KeyValuePair_2_t2999 *, const MethodInfo*))KeyValuePair_2_get_Key_m15919_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16018(__this, ___value, method) (( void (*) (KeyValuePair_2_t2999 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m15920_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m16019(__this, method) (( bool (*) (KeyValuePair_2_t2999 *, const MethodInfo*))KeyValuePair_2_get_Value_m15921_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16020(__this, ___value, method) (( void (*) (KeyValuePair_2_t2999 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m15922_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m16021(__this, method) (( String_t* (*) (KeyValuePair_2_t2999 *, const MethodInfo*))KeyValuePair_2_ToString_m15923_gshared)(__this, method)
