﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets._2D.Restarter
struct Restarter_t12;
// UnityEngine.Collider2D
struct Collider2D_t214;

// System.Void UnityStandardAssets._2D.Restarter::.ctor()
extern "C" void Restarter__ctor_m18 (Restarter_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets._2D.Restarter::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" void Restarter_OnTriggerEnter2D_m19 (Restarter_t12 * __this, Collider2D_t214 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
