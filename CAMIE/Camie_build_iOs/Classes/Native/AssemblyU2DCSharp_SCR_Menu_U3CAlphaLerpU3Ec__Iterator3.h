﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Image
struct Image_t48;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// SCR_Menu/<AlphaLerp>c__Iterator3
struct  U3CAlphaLerpU3Ec__Iterator3_t333  : public Object_t
{
	// System.Single SCR_Menu/<AlphaLerp>c__Iterator3::<alphaValue>__0
	float ___U3CalphaValueU3E__0_0;
	// System.Single SCR_Menu/<AlphaLerp>c__Iterator3::<modifier>__1
	float ___U3CmodifierU3E__1_1;
	// UnityEngine.UI.Image SCR_Menu/<AlphaLerp>c__Iterator3::image
	Image_t48 * ___image_2;
	// System.Single SCR_Menu/<AlphaLerp>c__Iterator3::speed
	float ___speed_3;
	// System.Boolean SCR_Menu/<AlphaLerp>c__Iterator3::isRepeating
	bool ___isRepeating_4;
	// UnityEngine.Color SCR_Menu/<AlphaLerp>c__Iterator3::<newColor>__2
	Color_t65  ___U3CnewColorU3E__2_5;
	// System.Int32 SCR_Menu/<AlphaLerp>c__Iterator3::$PC
	int32_t ___U24PC_6;
	// System.Object SCR_Menu/<AlphaLerp>c__Iterator3::$current
	Object_t * ___U24current_7;
	// UnityEngine.UI.Image SCR_Menu/<AlphaLerp>c__Iterator3::<$>image
	Image_t48 * ___U3CU24U3Eimage_8;
	// System.Single SCR_Menu/<AlphaLerp>c__Iterator3::<$>speed
	float ___U3CU24U3Espeed_9;
	// System.Boolean SCR_Menu/<AlphaLerp>c__Iterator3::<$>isRepeating
	bool ___U3CU24U3EisRepeating_10;
};
