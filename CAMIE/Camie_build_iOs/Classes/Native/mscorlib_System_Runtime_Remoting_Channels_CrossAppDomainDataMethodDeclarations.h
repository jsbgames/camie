﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Channels.CrossAppDomainData
struct CrossAppDomainData_t1960;

// System.Void System.Runtime.Remoting.Channels.CrossAppDomainData::.ctor(System.Int32)
extern "C" void CrossAppDomainData__ctor_m10469 (CrossAppDomainData_t1960 * __this, int32_t ___domainId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
