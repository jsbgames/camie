﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_t714;

// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
extern "C" void ComVisibleAttribute__ctor_m3472 (ComVisibleAttribute_t714 * __this, bool ___visibility, const MethodInfo* method) IL2CPP_METHOD_ATTR;
