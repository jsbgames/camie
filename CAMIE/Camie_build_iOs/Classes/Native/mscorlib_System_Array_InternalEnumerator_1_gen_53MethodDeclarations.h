﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>
struct InternalEnumerator_1_t3188;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t978;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m18860(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3188 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18861(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3188 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>::Dispose()
#define InternalEnumerator_1_Dispose_m18862(__this, method) (( void (*) (InternalEnumerator_1_t3188 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>::MoveNext()
#define InternalEnumerator_1_MoveNext_m18863(__this, method) (( bool (*) (InternalEnumerator_1_t3188 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.AchievementDescription>::get_Current()
#define InternalEnumerator_1_get_Current_m18864(__this, method) (( AchievementDescription_t978 * (*) (InternalEnumerator_1_t3188 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
