﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Text.RegularExpressions.Match
struct Match_t422;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Text.RegularExpressions.MatchEvaluator
struct  MatchEvaluator_t1425  : public MulticastDelegate_t549
{
};
