﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t3451;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t224;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t284;
// System.Exception
struct Exception_t232;

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m22181_gshared (ArrayReadOnlyList_1_t3451 * __this, ObjectU5BU5D_t224* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m22181(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t3451 *, ObjectU5BU5D_t224*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m22181_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m22182_gshared (ArrayReadOnlyList_1_t3451 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m22182(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3451 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m22182_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m22183_gshared (ArrayReadOnlyList_1_t3451 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m22183(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3451 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m22183_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m22184_gshared (ArrayReadOnlyList_1_t3451 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m22184(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t3451 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m22184_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m22185_gshared (ArrayReadOnlyList_1_t3451 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m22185(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t3451 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m22185_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m22186_gshared (ArrayReadOnlyList_1_t3451 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m22186(__this, method) (( bool (*) (ArrayReadOnlyList_1_t3451 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m22186_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m22187_gshared (ArrayReadOnlyList_1_t3451 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m22187(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3451 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m22187_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m22188_gshared (ArrayReadOnlyList_1_t3451 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m22188(__this, method) (( void (*) (ArrayReadOnlyList_1_t3451 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m22188_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m22189_gshared (ArrayReadOnlyList_1_t3451 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m22189(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3451 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m22189_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m22190_gshared (ArrayReadOnlyList_1_t3451 * __this, ObjectU5BU5D_t224* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m22190(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3451 *, ObjectU5BU5D_t224*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m22190_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m22191_gshared (ArrayReadOnlyList_1_t3451 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m22191(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t3451 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m22191_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m22192_gshared (ArrayReadOnlyList_1_t3451 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m22192(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t3451 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m22192_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m22193_gshared (ArrayReadOnlyList_1_t3451 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m22193(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3451 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m22193_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m22194_gshared (ArrayReadOnlyList_1_t3451 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m22194(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3451 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m22194_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m22195_gshared (ArrayReadOnlyList_1_t3451 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m22195(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3451 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m22195_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t232 * ArrayReadOnlyList_1_ReadOnlyError_m22196_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m22196(__this /* static, unused */, method) (( Exception_t232 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m22196_gshared)(__this /* static, unused */, method)
