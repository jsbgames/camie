﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.MatchCollection/Enumerator
struct Enumerator_t1363;
// System.Object
struct Object_t;
// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_t1362;

// System.Void System.Text.RegularExpressions.MatchCollection/Enumerator::.ctor(System.Text.RegularExpressions.MatchCollection)
extern "C" void Enumerator__ctor_m5956 (Enumerator_t1363 * __this, MatchCollection_t1362 * ___coll, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.RegularExpressions.MatchCollection/Enumerator::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m5957 (Enumerator_t1363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.MatchCollection/Enumerator::System.Collections.IEnumerator.MoveNext()
extern "C" bool Enumerator_System_Collections_IEnumerator_MoveNext_m5958 (Enumerator_t1363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
