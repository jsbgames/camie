﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.AssemblyLoadEventArgs
struct AssemblyLoadEventArgs_t2188;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.AssemblyLoadEventHandler
struct  AssemblyLoadEventHandler_t2180  : public MulticastDelegate_t549
{
};
