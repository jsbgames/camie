﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>
struct ValueCollection_t2991;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte>
struct Dictionary_2_t2983;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t3602;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Byte[]
struct ByteU5BU5D_t850;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_19.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m15964_gshared (ValueCollection_t2991 * __this, Dictionary_2_t2983 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m15964(__this, ___dictionary, method) (( void (*) (ValueCollection_t2991 *, Dictionary_2_t2983 *, const MethodInfo*))ValueCollection__ctor_m15964_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15965_gshared (ValueCollection_t2991 * __this, uint8_t ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15965(__this, ___item, method) (( void (*) (ValueCollection_t2991 *, uint8_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15965_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15966_gshared (ValueCollection_t2991 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15966(__this, method) (( void (*) (ValueCollection_t2991 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15966_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15967_gshared (ValueCollection_t2991 * __this, uint8_t ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15967(__this, ___item, method) (( bool (*) (ValueCollection_t2991 *, uint8_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15967_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15968_gshared (ValueCollection_t2991 * __this, uint8_t ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15968(__this, ___item, method) (( bool (*) (ValueCollection_t2991 *, uint8_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15968_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15969_gshared (ValueCollection_t2991 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15969(__this, method) (( Object_t* (*) (ValueCollection_t2991 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15969_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m15970_gshared (ValueCollection_t2991 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m15970(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2991 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m15970_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15971_gshared (ValueCollection_t2991 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15971(__this, method) (( Object_t * (*) (ValueCollection_t2991 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15972_gshared (ValueCollection_t2991 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15972(__this, method) (( bool (*) (ValueCollection_t2991 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15972_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15973_gshared (ValueCollection_t2991 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15973(__this, method) (( bool (*) (ValueCollection_t2991 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15973_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m15974_gshared (ValueCollection_t2991 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m15974(__this, method) (( Object_t * (*) (ValueCollection_t2991 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m15974_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m15975_gshared (ValueCollection_t2991 * __this, ByteU5BU5D_t850* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m15975(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2991 *, ByteU5BU5D_t850*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m15975_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::GetEnumerator()
extern "C" Enumerator_t2992  ValueCollection_GetEnumerator_m15976_gshared (ValueCollection_t2991 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m15976(__this, method) (( Enumerator_t2992  (*) (ValueCollection_t2991 *, const MethodInfo*))ValueCollection_GetEnumerator_m15976_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Byte>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m15977_gshared (ValueCollection_t2991 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m15977(__this, method) (( int32_t (*) (ValueCollection_t2991 *, const MethodInfo*))ValueCollection_get_Count_m15977_gshared)(__this, method)
