﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator
struct _KeysEnumerator_t1269;
// System.Object
struct Object_t;
// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t1268;

// System.Void System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::.ctor(System.Collections.Specialized.NameObjectCollectionBase)
extern "C" void _KeysEnumerator__ctor_m5521 (_KeysEnumerator_t1269 * __this, NameObjectCollectionBase_t1268 * ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::get_Current()
extern "C" Object_t * _KeysEnumerator_get_Current_m5522 (_KeysEnumerator_t1269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::MoveNext()
extern "C" bool _KeysEnumerator_MoveNext_m5523 (_KeysEnumerator_t1269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::Reset()
extern "C" void _KeysEnumerator_Reset_m5524 (_KeysEnumerator_t1269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
