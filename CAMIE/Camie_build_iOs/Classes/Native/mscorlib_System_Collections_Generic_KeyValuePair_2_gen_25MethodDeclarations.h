﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
struct KeyValuePair_2_t3293;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m20396_gshared (KeyValuePair_2_t3293 * __this, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m20396(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3293 *, uint64_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m20396_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::get_Key()
extern "C" uint64_t KeyValuePair_2_get_Key_m20397_gshared (KeyValuePair_2_t3293 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m20397(__this, method) (( uint64_t (*) (KeyValuePair_2_t3293 *, const MethodInfo*))KeyValuePair_2_get_Key_m20397_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m20398_gshared (KeyValuePair_2_t3293 * __this, uint64_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m20398(__this, ___value, method) (( void (*) (KeyValuePair_2_t3293 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Key_m20398_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m20399_gshared (KeyValuePair_2_t3293 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m20399(__this, method) (( Object_t * (*) (KeyValuePair_2_t3293 *, const MethodInfo*))KeyValuePair_2_get_Value_m20399_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m20400_gshared (KeyValuePair_2_t3293 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m20400(__this, ___value, method) (( void (*) (KeyValuePair_2_t3293 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m20400_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m20401_gshared (KeyValuePair_2_t3293 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m20401(__this, method) (( String_t* (*) (KeyValuePair_2_t3293 *, const MethodInfo*))KeyValuePair_2_ToString_m20401_gshared)(__this, method)
