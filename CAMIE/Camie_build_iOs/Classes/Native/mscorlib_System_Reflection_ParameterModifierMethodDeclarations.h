﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.ParameterModifier
struct ParameterModifier_t1914;
struct ParameterModifier_t1914_marshaled;

void ParameterModifier_t1914_marshal(const ParameterModifier_t1914& unmarshaled, ParameterModifier_t1914_marshaled& marshaled);
void ParameterModifier_t1914_marshal_back(const ParameterModifier_t1914_marshaled& marshaled, ParameterModifier_t1914& unmarshaled);
void ParameterModifier_t1914_marshal_cleanup(ParameterModifier_t1914_marshaled& marshaled);
