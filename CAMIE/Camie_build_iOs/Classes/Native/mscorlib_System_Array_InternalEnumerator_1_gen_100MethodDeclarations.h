﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>
struct InternalEnumerator_1_t3465;
// System.Object
struct Object_t;
// System.Diagnostics.StackFrame
struct StackFrame_t1101;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22247(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3465 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22248(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3465 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::Dispose()
#define InternalEnumerator_1_Dispose_m22249(__this, method) (( void (*) (InternalEnumerator_1_t3465 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22250(__this, method) (( bool (*) (InternalEnumerator_1_t3465 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::get_Current()
#define InternalEnumerator_1_get_Current_m22251(__this, method) (( StackFrame_t1101 * (*) (InternalEnumerator_1_t3465 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
