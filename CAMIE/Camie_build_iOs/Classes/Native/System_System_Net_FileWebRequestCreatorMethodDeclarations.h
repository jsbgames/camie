﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FileWebRequestCreator
struct FileWebRequestCreator_t1286;
// System.Net.WebRequest
struct WebRequest_t1285;
// System.Uri
struct Uri_t926;

// System.Void System.Net.FileWebRequestCreator::.ctor()
extern "C" void FileWebRequestCreator__ctor_m5573 (FileWebRequestCreator_t1286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FileWebRequestCreator::Create(System.Uri)
extern "C" WebRequest_t1285 * FileWebRequestCreator_Create_m5574 (FileWebRequestCreator_t1286 * __this, Uri_t926 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
