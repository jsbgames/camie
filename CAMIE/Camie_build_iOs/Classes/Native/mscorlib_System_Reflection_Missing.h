﻿#pragma once
#include <stdint.h>
// System.Reflection.Missing
struct Missing_t1903;
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.Missing
struct  Missing_t1903  : public Object_t
{
};
struct Missing_t1903_StaticFields{
	// System.Reflection.Missing System.Reflection.Missing::Value
	Missing_t1903 * ___Value_0;
};
