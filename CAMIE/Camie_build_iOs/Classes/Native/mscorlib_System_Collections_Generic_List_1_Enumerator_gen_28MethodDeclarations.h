﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>
struct Enumerator_t3176;
// System.Object
struct Object_t;
// UnityEngine.RectTransform
struct RectTransform_t364;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t612;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m18760(__this, ___l, method) (( void (*) (Enumerator_t3176 *, List_1_t612 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18761(__this, method) (( Object_t * (*) (Enumerator_t3176 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::Dispose()
#define Enumerator_Dispose_m18762(__this, method) (( void (*) (Enumerator_t3176 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::VerifyState()
#define Enumerator_VerifyState_m18763(__this, method) (( void (*) (Enumerator_t3176 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::MoveNext()
#define Enumerator_MoveNext_m18764(__this, method) (( bool (*) (Enumerator_t3176 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::get_Current()
#define Enumerator_get_Current_m18765(__this, method) (( RectTransform_t364 * (*) (Enumerator_t3176 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
