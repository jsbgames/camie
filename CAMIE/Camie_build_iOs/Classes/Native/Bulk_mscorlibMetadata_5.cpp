﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Metadata Definition System.Runtime.Remoting.Services.ITrackingHandler
extern TypeInfo ITrackingHandler_t2305_il2cpp_TypeInfo;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ITrackingHandler_t2305_ITrackingHandler_DisconnectedObject_m13220_ParameterInfos[] = 
{
	{"obj", 0, 134222438, 0, &Object_t_0_0_0},
};
extern const Il2CppType Void_t272_0_0_0;
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.ITrackingHandler::DisconnectedObject(System.Object)
extern const MethodInfo ITrackingHandler_DisconnectedObject_m13220_MethodInfo = 
{
	"DisconnectedObject"/* name */
	, NULL/* method */
	, &ITrackingHandler_t2305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ITrackingHandler_t2305_ITrackingHandler_DisconnectedObject_m13220_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjRef_t2008_0_0_0;
extern const Il2CppType ObjRef_t2008_0_0_0;
static const ParameterInfo ITrackingHandler_t2305_ITrackingHandler_MarshaledObject_m13221_ParameterInfos[] = 
{
	{"obj", 0, 134222439, 0, &Object_t_0_0_0},
	{"or", 1, 134222440, 0, &ObjRef_t2008_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.ITrackingHandler::MarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
extern const MethodInfo ITrackingHandler_MarshaledObject_m13221_MethodInfo = 
{
	"MarshaledObject"/* name */
	, NULL/* method */
	, &ITrackingHandler_t2305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ITrackingHandler_t2305_ITrackingHandler_MarshaledObject_m13221_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjRef_t2008_0_0_0;
static const ParameterInfo ITrackingHandler_t2305_ITrackingHandler_UnmarshaledObject_m13222_ParameterInfos[] = 
{
	{"obj", 0, 134222441, 0, &Object_t_0_0_0},
	{"or", 1, 134222442, 0, &ObjRef_t2008_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.ITrackingHandler::UnmarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
extern const MethodInfo ITrackingHandler_UnmarshaledObject_m13222_MethodInfo = 
{
	"UnmarshaledObject"/* name */
	, NULL/* method */
	, &ITrackingHandler_t2305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ITrackingHandler_t2305_ITrackingHandler_UnmarshaledObject_m13222_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ITrackingHandler_t2305_MethodInfos[] =
{
	&ITrackingHandler_DisconnectedObject_m13220_MethodInfo,
	&ITrackingHandler_MarshaledObject_m13221_MethodInfo,
	&ITrackingHandler_UnmarshaledObject_m13222_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ITrackingHandler_t2305_0_0_0;
extern const Il2CppType ITrackingHandler_t2305_1_0_0;
struct ITrackingHandler_t2305;
const Il2CppTypeDefinitionMetadata ITrackingHandler_t2305_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ITrackingHandler_t2305_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ITrackingHandler"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, ITrackingHandler_t2305_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ITrackingHandler_t2305_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 593/* custom_attributes_cache */
	, &ITrackingHandler_t2305_0_0_0/* byval_arg */
	, &ITrackingHandler_t2305_1_0_0/* this_arg */
	, &ITrackingHandler_t2305_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServices.h"
// Metadata Definition System.Runtime.Remoting.Services.TrackingServices
extern TypeInfo TrackingServices_t2004_il2cpp_TypeInfo;
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServicesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.TrackingServices::.cctor()
extern const MethodInfo TrackingServices__cctor_m10692_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TrackingServices__cctor_m10692/* method */
	, &TrackingServices_t2004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjRef_t2008_0_0_0;
static const ParameterInfo TrackingServices_t2004_TrackingServices_NotifyMarshaledObject_m10693_ParameterInfos[] = 
{
	{"obj", 0, 134222443, 0, &Object_t_0_0_0},
	{"or", 1, 134222444, 0, &ObjRef_t2008_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.TrackingServices::NotifyMarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
extern const MethodInfo TrackingServices_NotifyMarshaledObject_m10693_MethodInfo = 
{
	"NotifyMarshaledObject"/* name */
	, (methodPointerType)&TrackingServices_NotifyMarshaledObject_m10693/* method */
	, &TrackingServices_t2004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, TrackingServices_t2004_TrackingServices_NotifyMarshaledObject_m10693_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjRef_t2008_0_0_0;
static const ParameterInfo TrackingServices_t2004_TrackingServices_NotifyUnmarshaledObject_m10694_ParameterInfos[] = 
{
	{"obj", 0, 134222445, 0, &Object_t_0_0_0},
	{"or", 1, 134222446, 0, &ObjRef_t2008_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.TrackingServices::NotifyUnmarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
extern const MethodInfo TrackingServices_NotifyUnmarshaledObject_m10694_MethodInfo = 
{
	"NotifyUnmarshaledObject"/* name */
	, (methodPointerType)&TrackingServices_NotifyUnmarshaledObject_m10694/* method */
	, &TrackingServices_t2004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, TrackingServices_t2004_TrackingServices_NotifyUnmarshaledObject_m10694_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TrackingServices_t2004_TrackingServices_NotifyDisconnectedObject_m10695_ParameterInfos[] = 
{
	{"obj", 0, 134222447, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.TrackingServices::NotifyDisconnectedObject(System.Object)
extern const MethodInfo TrackingServices_NotifyDisconnectedObject_m10695_MethodInfo = 
{
	"NotifyDisconnectedObject"/* name */
	, (methodPointerType)&TrackingServices_NotifyDisconnectedObject_m10695/* method */
	, &TrackingServices_t2004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TrackingServices_t2004_TrackingServices_NotifyDisconnectedObject_m10695_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TrackingServices_t2004_MethodInfos[] =
{
	&TrackingServices__cctor_m10692_MethodInfo,
	&TrackingServices_NotifyMarshaledObject_m10693_MethodInfo,
	&TrackingServices_NotifyUnmarshaledObject_m10694_MethodInfo,
	&TrackingServices_NotifyDisconnectedObject_m10695_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
static const Il2CppMethodReference TrackingServices_t2004_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool TrackingServices_t2004_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TrackingServices_t2004_0_0_0;
extern const Il2CppType TrackingServices_t2004_1_0_0;
struct TrackingServices_t2004;
const Il2CppTypeDefinitionMetadata TrackingServices_t2004_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackingServices_t2004_VTable/* vtableMethods */
	, TrackingServices_t2004_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2055/* fieldStart */

};
TypeInfo TrackingServices_t2004_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackingServices"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, TrackingServices_t2004_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TrackingServices_t2004_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 594/* custom_attributes_cache */
	, &TrackingServices_t2004_0_0_0/* byval_arg */
	, &TrackingServices_t2004_1_0_0/* this_arg */
	, &TrackingServices_t2004_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TrackingServices_t2004)/* instance_size */
	, sizeof (TrackingServices_t2004)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TrackingServices_t2004_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.ActivatedClientTypeEntry
extern TypeInfo ActivatedClientTypeEntry_t2005_il2cpp_TypeInfo;
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntryMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ActivatedClientTypeEntry::get_ApplicationUrl()
extern const MethodInfo ActivatedClientTypeEntry_get_ApplicationUrl_m10696_MethodInfo = 
{
	"get_ApplicationUrl"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ApplicationUrl_m10696/* method */
	, &ActivatedClientTypeEntry_t2005_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IContextAttributeU5BU5D_t2290_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.IContextAttribute[] System.Runtime.Remoting.ActivatedClientTypeEntry::get_ContextAttributes()
extern const MethodInfo ActivatedClientTypeEntry_get_ContextAttributes_m10697_MethodInfo = 
{
	"get_ContextAttributes"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ContextAttributes_m10697/* method */
	, &ActivatedClientTypeEntry_t2005_il2cpp_TypeInfo/* declaring_type */
	, &IContextAttributeU5BU5D_t2290_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ActivatedClientTypeEntry::get_ObjectType()
extern const MethodInfo ActivatedClientTypeEntry_get_ObjectType_m10698_MethodInfo = 
{
	"get_ObjectType"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ObjectType_m10698/* method */
	, &ActivatedClientTypeEntry_t2005_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ActivatedClientTypeEntry::ToString()
extern const MethodInfo ActivatedClientTypeEntry_ToString_m10699_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_ToString_m10699/* method */
	, &ActivatedClientTypeEntry_t2005_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ActivatedClientTypeEntry_t2005_MethodInfos[] =
{
	&ActivatedClientTypeEntry_get_ApplicationUrl_m10696_MethodInfo,
	&ActivatedClientTypeEntry_get_ContextAttributes_m10697_MethodInfo,
	&ActivatedClientTypeEntry_get_ObjectType_m10698_MethodInfo,
	&ActivatedClientTypeEntry_ToString_m10699_MethodInfo,
	NULL
};
extern const MethodInfo ActivatedClientTypeEntry_get_ApplicationUrl_m10696_MethodInfo;
static const PropertyInfo ActivatedClientTypeEntry_t2005____ApplicationUrl_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t2005_il2cpp_TypeInfo/* parent */
	, "ApplicationUrl"/* name */
	, &ActivatedClientTypeEntry_get_ApplicationUrl_m10696_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ActivatedClientTypeEntry_get_ContextAttributes_m10697_MethodInfo;
static const PropertyInfo ActivatedClientTypeEntry_t2005____ContextAttributes_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t2005_il2cpp_TypeInfo/* parent */
	, "ContextAttributes"/* name */
	, &ActivatedClientTypeEntry_get_ContextAttributes_m10697_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ActivatedClientTypeEntry_get_ObjectType_m10698_MethodInfo;
static const PropertyInfo ActivatedClientTypeEntry_t2005____ObjectType_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t2005_il2cpp_TypeInfo/* parent */
	, "ObjectType"/* name */
	, &ActivatedClientTypeEntry_get_ObjectType_m10698_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ActivatedClientTypeEntry_t2005_PropertyInfos[] =
{
	&ActivatedClientTypeEntry_t2005____ApplicationUrl_PropertyInfo,
	&ActivatedClientTypeEntry_t2005____ContextAttributes_PropertyInfo,
	&ActivatedClientTypeEntry_t2005____ObjectType_PropertyInfo,
	NULL
};
extern const MethodInfo ActivatedClientTypeEntry_ToString_m10699_MethodInfo;
static const Il2CppMethodReference ActivatedClientTypeEntry_t2005_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&ActivatedClientTypeEntry_ToString_m10699_MethodInfo,
};
static bool ActivatedClientTypeEntry_t2005_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivatedClientTypeEntry_t2005_0_0_0;
extern const Il2CppType ActivatedClientTypeEntry_t2005_1_0_0;
extern const Il2CppType TypeEntry_t2006_0_0_0;
struct ActivatedClientTypeEntry_t2005;
const Il2CppTypeDefinitionMetadata ActivatedClientTypeEntry_t2005_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t2006_0_0_0/* parent */
	, ActivatedClientTypeEntry_t2005_VTable/* vtableMethods */
	, ActivatedClientTypeEntry_t2005_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2056/* fieldStart */

};
TypeInfo ActivatedClientTypeEntry_t2005_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivatedClientTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ActivatedClientTypeEntry_t2005_MethodInfos/* methods */
	, ActivatedClientTypeEntry_t2005_PropertyInfos/* properties */
	, NULL/* events */
	, &ActivatedClientTypeEntry_t2005_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 595/* custom_attributes_cache */
	, &ActivatedClientTypeEntry_t2005_0_0_0/* byval_arg */
	, &ActivatedClientTypeEntry_t2005_1_0_0/* this_arg */
	, &ActivatedClientTypeEntry_t2005_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivatedClientTypeEntry_t2005)/* instance_size */
	, sizeof (ActivatedClientTypeEntry_t2005)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfo.h"
// Metadata Definition System.Runtime.Remoting.EnvoyInfo
extern TypeInfo EnvoyInfo_t2007_il2cpp_TypeInfo;
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfoMethodDeclarations.h"
extern const Il2CppType IMessageSink_t1516_0_0_0;
extern const Il2CppType IMessageSink_t1516_0_0_0;
static const ParameterInfo EnvoyInfo_t2007_EnvoyInfo__ctor_m10700_ParameterInfos[] = 
{
	{"sinks", 0, 134222448, 0, &IMessageSink_t1516_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.EnvoyInfo::.ctor(System.Runtime.Remoting.Messaging.IMessageSink)
extern const MethodInfo EnvoyInfo__ctor_m10700_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EnvoyInfo__ctor_m10700/* method */
	, &EnvoyInfo_t2007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, EnvoyInfo_t2007_EnvoyInfo__ctor_m10700_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3955/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.EnvoyInfo::get_EnvoySinks()
extern const MethodInfo EnvoyInfo_get_EnvoySinks_m10701_MethodInfo = 
{
	"get_EnvoySinks"/* name */
	, (methodPointerType)&EnvoyInfo_get_EnvoySinks_m10701/* method */
	, &EnvoyInfo_t2007_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1516_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3956/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EnvoyInfo_t2007_MethodInfos[] =
{
	&EnvoyInfo__ctor_m10700_MethodInfo,
	&EnvoyInfo_get_EnvoySinks_m10701_MethodInfo,
	NULL
};
extern const MethodInfo EnvoyInfo_get_EnvoySinks_m10701_MethodInfo;
static const PropertyInfo EnvoyInfo_t2007____EnvoySinks_PropertyInfo = 
{
	&EnvoyInfo_t2007_il2cpp_TypeInfo/* parent */
	, "EnvoySinks"/* name */
	, &EnvoyInfo_get_EnvoySinks_m10701_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* EnvoyInfo_t2007_PropertyInfos[] =
{
	&EnvoyInfo_t2007____EnvoySinks_PropertyInfo,
	NULL
};
static const Il2CppMethodReference EnvoyInfo_t2007_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&EnvoyInfo_get_EnvoySinks_m10701_MethodInfo,
};
static bool EnvoyInfo_t2007_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnvoyInfo_t2013_0_0_0;
static const Il2CppType* EnvoyInfo_t2007_InterfacesTypeInfos[] = 
{
	&IEnvoyInfo_t2013_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyInfo_t2007_InterfacesOffsets[] = 
{
	{ &IEnvoyInfo_t2013_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnvoyInfo_t2007_0_0_0;
extern const Il2CppType EnvoyInfo_t2007_1_0_0;
struct EnvoyInfo_t2007;
const Il2CppTypeDefinitionMetadata EnvoyInfo_t2007_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyInfo_t2007_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyInfo_t2007_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyInfo_t2007_VTable/* vtableMethods */
	, EnvoyInfo_t2007_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2058/* fieldStart */

};
TypeInfo EnvoyInfo_t2007_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, EnvoyInfo_t2007_MethodInfos/* methods */
	, EnvoyInfo_t2007_PropertyInfos/* properties */
	, NULL/* events */
	, &EnvoyInfo_t2007_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyInfo_t2007_0_0_0/* byval_arg */
	, &EnvoyInfo_t2007_1_0_0/* this_arg */
	, &EnvoyInfo_t2007_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyInfo_t2007)/* instance_size */
	, sizeof (EnvoyInfo_t2007)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IChannelInfo
extern TypeInfo IChannelInfo_t2011_il2cpp_TypeInfo;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.IChannelInfo::get_ChannelData()
extern const MethodInfo IChannelInfo_get_ChannelData_m13223_MethodInfo = 
{
	"get_ChannelData"/* name */
	, NULL/* method */
	, &IChannelInfo_t2011_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3957/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IChannelInfo_t2011_MethodInfos[] =
{
	&IChannelInfo_get_ChannelData_m13223_MethodInfo,
	NULL
};
extern const MethodInfo IChannelInfo_get_ChannelData_m13223_MethodInfo;
static const PropertyInfo IChannelInfo_t2011____ChannelData_PropertyInfo = 
{
	&IChannelInfo_t2011_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &IChannelInfo_get_ChannelData_m13223_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IChannelInfo_t2011_PropertyInfos[] =
{
	&IChannelInfo_t2011____ChannelData_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelInfo_t2011_0_0_0;
extern const Il2CppType IChannelInfo_t2011_1_0_0;
struct IChannelInfo_t2011;
const Il2CppTypeDefinitionMetadata IChannelInfo_t2011_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannelInfo_t2011_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IChannelInfo_t2011_MethodInfos/* methods */
	, IChannelInfo_t2011_PropertyInfos/* properties */
	, NULL/* events */
	, &IChannelInfo_t2011_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 596/* custom_attributes_cache */
	, &IChannelInfo_t2011_0_0_0/* byval_arg */
	, &IChannelInfo_t2011_1_0_0/* this_arg */
	, &IChannelInfo_t2011_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IEnvoyInfo
extern TypeInfo IEnvoyInfo_t2013_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.IEnvoyInfo::get_EnvoySinks()
extern const MethodInfo IEnvoyInfo_get_EnvoySinks_m13224_MethodInfo = 
{
	"get_EnvoySinks"/* name */
	, NULL/* method */
	, &IEnvoyInfo_t2013_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1516_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3958/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IEnvoyInfo_t2013_MethodInfos[] =
{
	&IEnvoyInfo_get_EnvoySinks_m13224_MethodInfo,
	NULL
};
extern const MethodInfo IEnvoyInfo_get_EnvoySinks_m13224_MethodInfo;
static const PropertyInfo IEnvoyInfo_t2013____EnvoySinks_PropertyInfo = 
{
	&IEnvoyInfo_t2013_il2cpp_TypeInfo/* parent */
	, "EnvoySinks"/* name */
	, &IEnvoyInfo_get_EnvoySinks_m13224_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IEnvoyInfo_t2013_PropertyInfos[] =
{
	&IEnvoyInfo_t2013____EnvoySinks_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnvoyInfo_t2013_1_0_0;
struct IEnvoyInfo_t2013;
const Il2CppTypeDefinitionMetadata IEnvoyInfo_t2013_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IEnvoyInfo_t2013_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IEnvoyInfo_t2013_MethodInfos/* methods */
	, IEnvoyInfo_t2013_PropertyInfos/* properties */
	, NULL/* events */
	, &IEnvoyInfo_t2013_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 597/* custom_attributes_cache */
	, &IEnvoyInfo_t2013_0_0_0/* byval_arg */
	, &IEnvoyInfo_t2013_1_0_0/* this_arg */
	, &IEnvoyInfo_t2013_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IRemotingTypeInfo
extern TypeInfo IRemotingTypeInfo_t2012_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.IRemotingTypeInfo::get_TypeName()
extern const MethodInfo IRemotingTypeInfo_get_TypeName_m13225_MethodInfo = 
{
	"get_TypeName"/* name */
	, NULL/* method */
	, &IRemotingTypeInfo_t2012_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3959/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IRemotingTypeInfo_t2012_MethodInfos[] =
{
	&IRemotingTypeInfo_get_TypeName_m13225_MethodInfo,
	NULL
};
extern const MethodInfo IRemotingTypeInfo_get_TypeName_m13225_MethodInfo;
static const PropertyInfo IRemotingTypeInfo_t2012____TypeName_PropertyInfo = 
{
	&IRemotingTypeInfo_t2012_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &IRemotingTypeInfo_get_TypeName_m13225_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IRemotingTypeInfo_t2012_PropertyInfos[] =
{
	&IRemotingTypeInfo_t2012____TypeName_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IRemotingTypeInfo_t2012_0_0_0;
extern const Il2CppType IRemotingTypeInfo_t2012_1_0_0;
struct IRemotingTypeInfo_t2012;
const Il2CppTypeDefinitionMetadata IRemotingTypeInfo_t2012_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IRemotingTypeInfo_t2012_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingTypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IRemotingTypeInfo_t2012_MethodInfos/* methods */
	, IRemotingTypeInfo_t2012_PropertyInfos/* properties */
	, NULL/* events */
	, &IRemotingTypeInfo_t2012_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 598/* custom_attributes_cache */
	, &IRemotingTypeInfo_t2012_0_0_0/* byval_arg */
	, &IRemotingTypeInfo_t2012_1_0_0/* this_arg */
	, &IRemotingTypeInfo_t2012_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// Metadata Definition System.Runtime.Remoting.Identity
extern TypeInfo Identity_t2002_il2cpp_TypeInfo;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_IdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Identity_t2002_Identity__ctor_m10702_ParameterInfos[] = 
{
	{"objectUri", 0, 134222449, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::.ctor(System.String)
extern const MethodInfo Identity__ctor_m10702_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Identity__ctor_m10702/* method */
	, &Identity_t2002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Identity_t2002_Identity__ctor_m10702_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3960/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Identity_t2002_Identity_CreateObjRef_m13226_ParameterInfos[] = 
{
	{"requestedType", 0, 134222450, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.Identity::CreateObjRef(System.Type)
extern const MethodInfo Identity_CreateObjRef_m13226_MethodInfo = 
{
	"CreateObjRef"/* name */
	, NULL/* method */
	, &Identity_t2002_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t2008_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Identity_t2002_Identity_CreateObjRef_m13226_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3961/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Identity::get_ChannelSink()
extern const MethodInfo Identity_get_ChannelSink_m10703_MethodInfo = 
{
	"get_ChannelSink"/* name */
	, (methodPointerType)&Identity_get_ChannelSink_m10703/* method */
	, &Identity_t2002_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1516_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3962/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessageSink_t1516_0_0_0;
static const ParameterInfo Identity_t2002_Identity_set_ChannelSink_m10704_ParameterInfos[] = 
{
	{"value", 0, 134222451, 0, &IMessageSink_t1516_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::set_ChannelSink(System.Runtime.Remoting.Messaging.IMessageSink)
extern const MethodInfo Identity_set_ChannelSink_m10704_MethodInfo = 
{
	"set_ChannelSink"/* name */
	, (methodPointerType)&Identity_set_ChannelSink_m10704/* method */
	, &Identity_t2002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Identity_t2002_Identity_set_ChannelSink_m10704_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3963/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Identity::get_ObjectUri()
extern const MethodInfo Identity_get_ObjectUri_m10705_MethodInfo = 
{
	"get_ObjectUri"/* name */
	, (methodPointerType)&Identity_get_ObjectUri_m10705/* method */
	, &Identity_t2002_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3964/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Identity_t2002_Identity_set_ObjectUri_m10706_ParameterInfos[] = 
{
	{"value", 0, 134222452, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::set_ObjectUri(System.String)
extern const MethodInfo Identity_set_ObjectUri_m10706_MethodInfo = 
{
	"set_ObjectUri"/* name */
	, (methodPointerType)&Identity_set_ObjectUri_m10706/* method */
	, &Identity_t2002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Identity_t2002_Identity_set_ObjectUri_m10706_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3965/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Identity::get_IsConnected()
extern const MethodInfo Identity_get_IsConnected_m10707_MethodInfo = 
{
	"get_IsConnected"/* name */
	, (methodPointerType)&Identity_get_IsConnected_m10707/* method */
	, &Identity_t2002_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3966/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Identity::get_Disposed()
extern const MethodInfo Identity_get_Disposed_m10708_MethodInfo = 
{
	"get_Disposed"/* name */
	, (methodPointerType)&Identity_get_Disposed_m10708/* method */
	, &Identity_t2002_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3967/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Identity_t2002_Identity_set_Disposed_m10709_ParameterInfos[] = 
{
	{"value", 0, 134222453, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::set_Disposed(System.Boolean)
extern const MethodInfo Identity_set_Disposed_m10709_MethodInfo = 
{
	"set_Disposed"/* name */
	, (methodPointerType)&Identity_set_Disposed_m10709/* method */
	, &Identity_t2002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Identity_t2002_Identity_set_Disposed_m10709_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3968/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Identity_t2002_MethodInfos[] =
{
	&Identity__ctor_m10702_MethodInfo,
	&Identity_CreateObjRef_m13226_MethodInfo,
	&Identity_get_ChannelSink_m10703_MethodInfo,
	&Identity_set_ChannelSink_m10704_MethodInfo,
	&Identity_get_ObjectUri_m10705_MethodInfo,
	&Identity_set_ObjectUri_m10706_MethodInfo,
	&Identity_get_IsConnected_m10707_MethodInfo,
	&Identity_get_Disposed_m10708_MethodInfo,
	&Identity_set_Disposed_m10709_MethodInfo,
	NULL
};
extern const MethodInfo Identity_get_ChannelSink_m10703_MethodInfo;
extern const MethodInfo Identity_set_ChannelSink_m10704_MethodInfo;
static const PropertyInfo Identity_t2002____ChannelSink_PropertyInfo = 
{
	&Identity_t2002_il2cpp_TypeInfo/* parent */
	, "ChannelSink"/* name */
	, &Identity_get_ChannelSink_m10703_MethodInfo/* get */
	, &Identity_set_ChannelSink_m10704_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Identity_get_ObjectUri_m10705_MethodInfo;
extern const MethodInfo Identity_set_ObjectUri_m10706_MethodInfo;
static const PropertyInfo Identity_t2002____ObjectUri_PropertyInfo = 
{
	&Identity_t2002_il2cpp_TypeInfo/* parent */
	, "ObjectUri"/* name */
	, &Identity_get_ObjectUri_m10705_MethodInfo/* get */
	, &Identity_set_ObjectUri_m10706_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Identity_get_IsConnected_m10707_MethodInfo;
static const PropertyInfo Identity_t2002____IsConnected_PropertyInfo = 
{
	&Identity_t2002_il2cpp_TypeInfo/* parent */
	, "IsConnected"/* name */
	, &Identity_get_IsConnected_m10707_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Identity_get_Disposed_m10708_MethodInfo;
extern const MethodInfo Identity_set_Disposed_m10709_MethodInfo;
static const PropertyInfo Identity_t2002____Disposed_PropertyInfo = 
{
	&Identity_t2002_il2cpp_TypeInfo/* parent */
	, "Disposed"/* name */
	, &Identity_get_Disposed_m10708_MethodInfo/* get */
	, &Identity_set_Disposed_m10709_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Identity_t2002_PropertyInfos[] =
{
	&Identity_t2002____ChannelSink_PropertyInfo,
	&Identity_t2002____ObjectUri_PropertyInfo,
	&Identity_t2002____IsConnected_PropertyInfo,
	&Identity_t2002____Disposed_PropertyInfo,
	NULL
};
static const Il2CppMethodReference Identity_t2002_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
};
static bool Identity_t2002_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Identity_t2002_0_0_0;
extern const Il2CppType Identity_t2002_1_0_0;
struct Identity_t2002;
const Il2CppTypeDefinitionMetadata Identity_t2002_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Identity_t2002_VTable/* vtableMethods */
	, Identity_t2002_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2059/* fieldStart */

};
TypeInfo Identity_t2002_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Identity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, Identity_t2002_MethodInfos/* methods */
	, Identity_t2002_PropertyInfos/* properties */
	, NULL/* events */
	, &Identity_t2002_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Identity_t2002_0_0_0/* byval_arg */
	, &Identity_t2002_1_0_0/* this_arg */
	, &Identity_t2002_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Identity_t2002)/* instance_size */
	, sizeof (Identity_t2002)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientIdentity
extern TypeInfo ClientIdentity_t2010_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjRef_t2008_0_0_0;
static const ParameterInfo ClientIdentity_t2010_ClientIdentity__ctor_m10710_ParameterInfos[] = 
{
	{"objectUri", 0, 134222454, 0, &String_t_0_0_0},
	{"objRef", 1, 134222455, 0, &ObjRef_t2008_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ClientIdentity::.ctor(System.String,System.Runtime.Remoting.ObjRef)
extern const MethodInfo ClientIdentity__ctor_m10710_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ClientIdentity__ctor_m10710/* method */
	, &ClientIdentity_t2010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ClientIdentity_t2010_ClientIdentity__ctor_m10710_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3969/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MarshalByRefObject_t1308_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.ClientIdentity::get_ClientProxy()
extern const MethodInfo ClientIdentity_get_ClientProxy_m10711_MethodInfo = 
{
	"get_ClientProxy"/* name */
	, (methodPointerType)&ClientIdentity_get_ClientProxy_m10711/* method */
	, &ClientIdentity_t2010_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t1308_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3970/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MarshalByRefObject_t1308_0_0_0;
static const ParameterInfo ClientIdentity_t2010_ClientIdentity_set_ClientProxy_m10712_ParameterInfos[] = 
{
	{"value", 0, 134222456, 0, &MarshalByRefObject_t1308_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ClientIdentity::set_ClientProxy(System.MarshalByRefObject)
extern const MethodInfo ClientIdentity_set_ClientProxy_m10712_MethodInfo = 
{
	"set_ClientProxy"/* name */
	, (methodPointerType)&ClientIdentity_set_ClientProxy_m10712/* method */
	, &ClientIdentity_t2010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ClientIdentity_t2010_ClientIdentity_set_ClientProxy_m10712_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3971/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ClientIdentity_t2010_ClientIdentity_CreateObjRef_m10713_ParameterInfos[] = 
{
	{"requestedType", 0, 134222457, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.ClientIdentity::CreateObjRef(System.Type)
extern const MethodInfo ClientIdentity_CreateObjRef_m10713_MethodInfo = 
{
	"CreateObjRef"/* name */
	, (methodPointerType)&ClientIdentity_CreateObjRef_m10713/* method */
	, &ClientIdentity_t2010_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t2008_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ClientIdentity_t2010_ClientIdentity_CreateObjRef_m10713_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3972/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ClientIdentity::get_TargetUri()
extern const MethodInfo ClientIdentity_get_TargetUri_m10714_MethodInfo = 
{
	"get_TargetUri"/* name */
	, (methodPointerType)&ClientIdentity_get_TargetUri_m10714/* method */
	, &ClientIdentity_t2010_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3973/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ClientIdentity_t2010_MethodInfos[] =
{
	&ClientIdentity__ctor_m10710_MethodInfo,
	&ClientIdentity_get_ClientProxy_m10711_MethodInfo,
	&ClientIdentity_set_ClientProxy_m10712_MethodInfo,
	&ClientIdentity_CreateObjRef_m10713_MethodInfo,
	&ClientIdentity_get_TargetUri_m10714_MethodInfo,
	NULL
};
extern const MethodInfo ClientIdentity_get_ClientProxy_m10711_MethodInfo;
extern const MethodInfo ClientIdentity_set_ClientProxy_m10712_MethodInfo;
static const PropertyInfo ClientIdentity_t2010____ClientProxy_PropertyInfo = 
{
	&ClientIdentity_t2010_il2cpp_TypeInfo/* parent */
	, "ClientProxy"/* name */
	, &ClientIdentity_get_ClientProxy_m10711_MethodInfo/* get */
	, &ClientIdentity_set_ClientProxy_m10712_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ClientIdentity_get_TargetUri_m10714_MethodInfo;
static const PropertyInfo ClientIdentity_t2010____TargetUri_PropertyInfo = 
{
	&ClientIdentity_t2010_il2cpp_TypeInfo/* parent */
	, "TargetUri"/* name */
	, &ClientIdentity_get_TargetUri_m10714_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ClientIdentity_t2010_PropertyInfos[] =
{
	&ClientIdentity_t2010____ClientProxy_PropertyInfo,
	&ClientIdentity_t2010____TargetUri_PropertyInfo,
	NULL
};
extern const MethodInfo ClientIdentity_CreateObjRef_m10713_MethodInfo;
static const Il2CppMethodReference ClientIdentity_t2010_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ClientIdentity_CreateObjRef_m10713_MethodInfo,
};
static bool ClientIdentity_t2010_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClientIdentity_t2010_0_0_0;
extern const Il2CppType ClientIdentity_t2010_1_0_0;
struct ClientIdentity_t2010;
const Il2CppTypeDefinitionMetadata ClientIdentity_t2010_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t2002_0_0_0/* parent */
	, ClientIdentity_t2010_VTable/* vtableMethods */
	, ClientIdentity_t2010_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2064/* fieldStart */

};
TypeInfo ClientIdentity_t2010_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ClientIdentity_t2010_MethodInfos/* methods */
	, ClientIdentity_t2010_PropertyInfos/* properties */
	, NULL/* events */
	, &ClientIdentity_t2010_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientIdentity_t2010_0_0_0/* byval_arg */
	, &ClientIdentity_t2010_1_0_0/* this_arg */
	, &ClientIdentity_t2010_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientIdentity_t2010)/* instance_size */
	, sizeof (ClientIdentity_t2010)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRef.h"
// Metadata Definition System.Runtime.Remoting.ObjRef
extern TypeInfo ObjRef_t2008_il2cpp_TypeInfo;
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRefMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.ctor()
extern const MethodInfo ObjRef__ctor_m10715_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRef__ctor_m10715/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3974/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo ObjRef_t2008_ObjRef__ctor_m10716_ParameterInfos[] = 
{
	{"info", 0, 134222458, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222459, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjRef__ctor_m10716_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRef__ctor_m10716/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, ObjRef_t2008_ObjRef__ctor_m10716_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3975/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.cctor()
extern const MethodInfo ObjRef__cctor_m10717_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ObjRef__cctor_m10717/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3976/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.ObjRef::get_IsReferenceToWellKnow()
extern const MethodInfo ObjRef_get_IsReferenceToWellKnow_m10718_MethodInfo = 
{
	"get_IsReferenceToWellKnow"/* name */
	, (methodPointerType)&ObjRef_get_IsReferenceToWellKnow_m10718/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3977/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IChannelInfo System.Runtime.Remoting.ObjRef::get_ChannelInfo()
extern const MethodInfo ObjRef_get_ChannelInfo_m10719_MethodInfo = 
{
	"get_ChannelInfo"/* name */
	, (methodPointerType)&ObjRef_get_ChannelInfo_m10719/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &IChannelInfo_t2011_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 601/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3978/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IEnvoyInfo System.Runtime.Remoting.ObjRef::get_EnvoyInfo()
extern const MethodInfo ObjRef_get_EnvoyInfo_m10720_MethodInfo = 
{
	"get_EnvoyInfo"/* name */
	, (methodPointerType)&ObjRef_get_EnvoyInfo_m10720/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &IEnvoyInfo_t2013_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3979/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnvoyInfo_t2013_0_0_0;
static const ParameterInfo ObjRef_t2008_ObjRef_set_EnvoyInfo_m10721_ParameterInfos[] = 
{
	{"value", 0, 134222460, 0, &IEnvoyInfo_t2013_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_EnvoyInfo(System.Runtime.Remoting.IEnvoyInfo)
extern const MethodInfo ObjRef_set_EnvoyInfo_m10721_MethodInfo = 
{
	"set_EnvoyInfo"/* name */
	, (methodPointerType)&ObjRef_set_EnvoyInfo_m10721/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjRef_t2008_ObjRef_set_EnvoyInfo_m10721_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3980/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IRemotingTypeInfo System.Runtime.Remoting.ObjRef::get_TypeInfo()
extern const MethodInfo ObjRef_get_TypeInfo_m10722_MethodInfo = 
{
	"get_TypeInfo"/* name */
	, (methodPointerType)&ObjRef_get_TypeInfo_m10722/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &IRemotingTypeInfo_t2012_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3981/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IRemotingTypeInfo_t2012_0_0_0;
static const ParameterInfo ObjRef_t2008_ObjRef_set_TypeInfo_m10723_ParameterInfos[] = 
{
	{"value", 0, 134222461, 0, &IRemotingTypeInfo_t2012_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_TypeInfo(System.Runtime.Remoting.IRemotingTypeInfo)
extern const MethodInfo ObjRef_set_TypeInfo_m10723_MethodInfo = 
{
	"set_TypeInfo"/* name */
	, (methodPointerType)&ObjRef_set_TypeInfo_m10723/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjRef_t2008_ObjRef_set_TypeInfo_m10723_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3982/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ObjRef::get_URI()
extern const MethodInfo ObjRef_get_URI_m10724_MethodInfo = 
{
	"get_URI"/* name */
	, (methodPointerType)&ObjRef_get_URI_m10724/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3983/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjRef_t2008_ObjRef_set_URI_m10725_ParameterInfos[] = 
{
	{"value", 0, 134222462, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_URI(System.String)
extern const MethodInfo ObjRef_set_URI_m10725_MethodInfo = 
{
	"set_URI"/* name */
	, (methodPointerType)&ObjRef_set_URI_m10725/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjRef_t2008_ObjRef_set_URI_m10725_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3984/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo ObjRef_t2008_ObjRef_GetObjectData_m10726_ParameterInfos[] = 
{
	{"info", 0, 134222463, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222464, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjRef_GetObjectData_m10726_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ObjRef_GetObjectData_m10726/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, ObjRef_t2008_ObjRef_GetObjectData_m10726_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3985/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo ObjRef_t2008_ObjRef_GetRealObject_m10727_ParameterInfos[] = 
{
	{"context", 0, 134222465, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.ObjRef::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjRef_GetRealObject_m10727_MethodInfo = 
{
	"GetRealObject"/* name */
	, (methodPointerType)&ObjRef_GetRealObject_m10727/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t1044/* invoker_method */
	, ObjRef_t2008_ObjRef_GetRealObject_m10727_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3986/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::UpdateChannelInfo()
extern const MethodInfo ObjRef_UpdateChannelInfo_m10728_MethodInfo = 
{
	"UpdateChannelInfo"/* name */
	, (methodPointerType)&ObjRef_UpdateChannelInfo_m10728/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3987/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ObjRef::get_ServerType()
extern const MethodInfo ObjRef_get_ServerType_m10729_MethodInfo = 
{
	"get_ServerType"/* name */
	, (methodPointerType)&ObjRef_get_ServerType_m10729/* method */
	, &ObjRef_t2008_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3988/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjRef_t2008_MethodInfos[] =
{
	&ObjRef__ctor_m10715_MethodInfo,
	&ObjRef__ctor_m10716_MethodInfo,
	&ObjRef__cctor_m10717_MethodInfo,
	&ObjRef_get_IsReferenceToWellKnow_m10718_MethodInfo,
	&ObjRef_get_ChannelInfo_m10719_MethodInfo,
	&ObjRef_get_EnvoyInfo_m10720_MethodInfo,
	&ObjRef_set_EnvoyInfo_m10721_MethodInfo,
	&ObjRef_get_TypeInfo_m10722_MethodInfo,
	&ObjRef_set_TypeInfo_m10723_MethodInfo,
	&ObjRef_get_URI_m10724_MethodInfo,
	&ObjRef_set_URI_m10725_MethodInfo,
	&ObjRef_GetObjectData_m10726_MethodInfo,
	&ObjRef_GetRealObject_m10727_MethodInfo,
	&ObjRef_UpdateChannelInfo_m10728_MethodInfo,
	&ObjRef_get_ServerType_m10729_MethodInfo,
	NULL
};
extern const MethodInfo ObjRef_get_IsReferenceToWellKnow_m10718_MethodInfo;
static const PropertyInfo ObjRef_t2008____IsReferenceToWellKnow_PropertyInfo = 
{
	&ObjRef_t2008_il2cpp_TypeInfo/* parent */
	, "IsReferenceToWellKnow"/* name */
	, &ObjRef_get_IsReferenceToWellKnow_m10718_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_ChannelInfo_m10719_MethodInfo;
static const PropertyInfo ObjRef_t2008____ChannelInfo_PropertyInfo = 
{
	&ObjRef_t2008_il2cpp_TypeInfo/* parent */
	, "ChannelInfo"/* name */
	, &ObjRef_get_ChannelInfo_m10719_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_EnvoyInfo_m10720_MethodInfo;
extern const MethodInfo ObjRef_set_EnvoyInfo_m10721_MethodInfo;
static const PropertyInfo ObjRef_t2008____EnvoyInfo_PropertyInfo = 
{
	&ObjRef_t2008_il2cpp_TypeInfo/* parent */
	, "EnvoyInfo"/* name */
	, &ObjRef_get_EnvoyInfo_m10720_MethodInfo/* get */
	, &ObjRef_set_EnvoyInfo_m10721_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_TypeInfo_m10722_MethodInfo;
extern const MethodInfo ObjRef_set_TypeInfo_m10723_MethodInfo;
static const PropertyInfo ObjRef_t2008____TypeInfo_PropertyInfo = 
{
	&ObjRef_t2008_il2cpp_TypeInfo/* parent */
	, "TypeInfo"/* name */
	, &ObjRef_get_TypeInfo_m10722_MethodInfo/* get */
	, &ObjRef_set_TypeInfo_m10723_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_URI_m10724_MethodInfo;
extern const MethodInfo ObjRef_set_URI_m10725_MethodInfo;
static const PropertyInfo ObjRef_t2008____URI_PropertyInfo = 
{
	&ObjRef_t2008_il2cpp_TypeInfo/* parent */
	, "URI"/* name */
	, &ObjRef_get_URI_m10724_MethodInfo/* get */
	, &ObjRef_set_URI_m10725_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_ServerType_m10729_MethodInfo;
static const PropertyInfo ObjRef_t2008____ServerType_PropertyInfo = 
{
	&ObjRef_t2008_il2cpp_TypeInfo/* parent */
	, "ServerType"/* name */
	, &ObjRef_get_ServerType_m10729_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjRef_t2008_PropertyInfos[] =
{
	&ObjRef_t2008____IsReferenceToWellKnow_PropertyInfo,
	&ObjRef_t2008____ChannelInfo_PropertyInfo,
	&ObjRef_t2008____EnvoyInfo_PropertyInfo,
	&ObjRef_t2008____TypeInfo_PropertyInfo,
	&ObjRef_t2008____URI_PropertyInfo,
	&ObjRef_t2008____ServerType_PropertyInfo,
	NULL
};
extern const MethodInfo ObjRef_GetObjectData_m10726_MethodInfo;
extern const MethodInfo ObjRef_GetRealObject_m10727_MethodInfo;
static const Il2CppMethodReference ObjRef_t2008_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ObjRef_GetObjectData_m10726_MethodInfo,
	&ObjRef_GetRealObject_m10727_MethodInfo,
	&ObjRef_get_ChannelInfo_m10719_MethodInfo,
	&ObjRef_get_EnvoyInfo_m10720_MethodInfo,
	&ObjRef_set_EnvoyInfo_m10721_MethodInfo,
	&ObjRef_get_TypeInfo_m10722_MethodInfo,
	&ObjRef_set_TypeInfo_m10723_MethodInfo,
	&ObjRef_get_URI_m10724_MethodInfo,
	&ObjRef_set_URI_m10725_MethodInfo,
	&ObjRef_GetObjectData_m10726_MethodInfo,
	&ObjRef_GetRealObject_m10727_MethodInfo,
};
static bool ObjRef_t2008_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializable_t439_0_0_0;
extern const Il2CppType IObjectReference_t2310_0_0_0;
static const Il2CppType* ObjRef_t2008_InterfacesTypeInfos[] = 
{
	&ISerializable_t439_0_0_0,
	&IObjectReference_t2310_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRef_t2008_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &IObjectReference_t2310_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjRef_t2008_1_0_0;
struct ObjRef_t2008;
const Il2CppTypeDefinitionMetadata ObjRef_t2008_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRef_t2008_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRef_t2008_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRef_t2008_VTable/* vtableMethods */
	, ObjRef_t2008_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2065/* fieldStart */

};
TypeInfo ObjRef_t2008_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRef"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ObjRef_t2008_MethodInfos/* methods */
	, ObjRef_t2008_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjRef_t2008_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 599/* custom_attributes_cache */
	, &ObjRef_t2008_0_0_0/* byval_arg */
	, &ObjRef_t2008_1_0_0/* this_arg */
	, &ObjRef_t2008_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRef_t2008)/* instance_size */
	, sizeof (ObjRef_t2008)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ObjRef_t2008_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfiguration.h"
// Metadata Definition System.Runtime.Remoting.RemotingConfiguration
extern TypeInfo RemotingConfiguration_t2014_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfigurationMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingConfiguration::.cctor()
extern const MethodInfo RemotingConfiguration__cctor_m10730_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingConfiguration__cctor_m10730/* method */
	, &RemotingConfiguration_t2014_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3989/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingConfiguration::get_ApplicationName()
extern const MethodInfo RemotingConfiguration_get_ApplicationName_m10731_MethodInfo = 
{
	"get_ApplicationName"/* name */
	, (methodPointerType)&RemotingConfiguration_get_ApplicationName_m10731/* method */
	, &RemotingConfiguration_t2014_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3990/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingConfiguration::get_ProcessId()
extern const MethodInfo RemotingConfiguration_get_ProcessId_m10732_MethodInfo = 
{
	"get_ProcessId"/* name */
	, (methodPointerType)&RemotingConfiguration_get_ProcessId_m10732/* method */
	, &RemotingConfiguration_t2014_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3991/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RemotingConfiguration_t2014_RemotingConfiguration_IsRemotelyActivatedClientType_m10733_ParameterInfos[] = 
{
	{"svrType", 0, 134222466, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ActivatedClientTypeEntry System.Runtime.Remoting.RemotingConfiguration::IsRemotelyActivatedClientType(System.Type)
extern const MethodInfo RemotingConfiguration_IsRemotelyActivatedClientType_m10733_MethodInfo = 
{
	"IsRemotelyActivatedClientType"/* name */
	, (methodPointerType)&RemotingConfiguration_IsRemotelyActivatedClientType_m10733/* method */
	, &RemotingConfiguration_t2014_il2cpp_TypeInfo/* declaring_type */
	, &ActivatedClientTypeEntry_t2005_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingConfiguration_t2014_RemotingConfiguration_IsRemotelyActivatedClientType_m10733_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3992/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingConfiguration_t2014_MethodInfos[] =
{
	&RemotingConfiguration__cctor_m10730_MethodInfo,
	&RemotingConfiguration_get_ApplicationName_m10731_MethodInfo,
	&RemotingConfiguration_get_ProcessId_m10732_MethodInfo,
	&RemotingConfiguration_IsRemotelyActivatedClientType_m10733_MethodInfo,
	NULL
};
extern const MethodInfo RemotingConfiguration_get_ApplicationName_m10731_MethodInfo;
static const PropertyInfo RemotingConfiguration_t2014____ApplicationName_PropertyInfo = 
{
	&RemotingConfiguration_t2014_il2cpp_TypeInfo/* parent */
	, "ApplicationName"/* name */
	, &RemotingConfiguration_get_ApplicationName_m10731_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RemotingConfiguration_get_ProcessId_m10732_MethodInfo;
static const PropertyInfo RemotingConfiguration_t2014____ProcessId_PropertyInfo = 
{
	&RemotingConfiguration_t2014_il2cpp_TypeInfo/* parent */
	, "ProcessId"/* name */
	, &RemotingConfiguration_get_ProcessId_m10732_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RemotingConfiguration_t2014_PropertyInfos[] =
{
	&RemotingConfiguration_t2014____ApplicationName_PropertyInfo,
	&RemotingConfiguration_t2014____ProcessId_PropertyInfo,
	NULL
};
static const Il2CppMethodReference RemotingConfiguration_t2014_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool RemotingConfiguration_t2014_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingConfiguration_t2014_0_0_0;
extern const Il2CppType RemotingConfiguration_t2014_1_0_0;
struct RemotingConfiguration_t2014;
const Il2CppTypeDefinitionMetadata RemotingConfiguration_t2014_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingConfiguration_t2014_VTable/* vtableMethods */
	, RemotingConfiguration_t2014_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2074/* fieldStart */

};
TypeInfo RemotingConfiguration_t2014_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingConfiguration"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingConfiguration_t2014_MethodInfos/* methods */
	, RemotingConfiguration_t2014_PropertyInfos/* properties */
	, NULL/* events */
	, &RemotingConfiguration_t2014_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 602/* custom_attributes_cache */
	, &RemotingConfiguration_t2014_0_0_0/* byval_arg */
	, &RemotingConfiguration_t2014_1_0_0/* this_arg */
	, &RemotingConfiguration_t2014_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingConfiguration_t2014)/* instance_size */
	, sizeof (RemotingConfiguration_t2014)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingConfiguration_t2014_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingException.h"
// Metadata Definition System.Runtime.Remoting.RemotingException
extern TypeInfo RemotingException_t2015_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor()
extern const MethodInfo RemotingException__ctor_m10734_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m10734/* method */
	, &RemotingException_t2015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3993/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingException_t2015_RemotingException__ctor_m10735_ParameterInfos[] = 
{
	{"message", 0, 134222467, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor(System.String)
extern const MethodInfo RemotingException__ctor_m10735_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m10735/* method */
	, &RemotingException_t2015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, RemotingException_t2015_RemotingException__ctor_m10735_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3994/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo RemotingException_t2015_RemotingException__ctor_m10736_ParameterInfos[] = 
{
	{"info", 0, 134222468, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222469, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RemotingException__ctor_m10736_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m10736/* method */
	, &RemotingException_t2015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, RemotingException_t2015_RemotingException__ctor_m10736_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3995/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingException_t2015_MethodInfos[] =
{
	&RemotingException__ctor_m10734_MethodInfo,
	&RemotingException__ctor_m10735_MethodInfo,
	&RemotingException__ctor_m10736_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m5385_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m5386_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m5387_MethodInfo;
extern const MethodInfo Exception_get_Message_m5388_MethodInfo;
extern const MethodInfo Exception_get_Source_m5389_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m5390_MethodInfo;
extern const MethodInfo Exception_GetType_m5391_MethodInfo;
static const Il2CppMethodReference RemotingException_t2015_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool RemotingException_t2015_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Exception_t1147_0_0_0;
static Il2CppInterfaceOffsetPair RemotingException_t2015_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingException_t2015_0_0_0;
extern const Il2CppType RemotingException_t2015_1_0_0;
extern const Il2CppType SystemException_t1464_0_0_0;
struct RemotingException_t2015;
const Il2CppTypeDefinitionMetadata RemotingException_t2015_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RemotingException_t2015_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1464_0_0_0/* parent */
	, RemotingException_t2015_VTable/* vtableMethods */
	, RemotingException_t2015_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemotingException_t2015_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingException"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingException_t2015_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingException_t2015_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 603/* custom_attributes_cache */
	, &RemotingException_t2015_0_0_0/* byval_arg */
	, &RemotingException_t2015_1_0_0/* this_arg */
	, &RemotingException_t2015_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingException_t2015)/* instance_size */
	, sizeof (RemotingException_t2015)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServices.h"
// Metadata Definition System.Runtime.Remoting.RemotingServices
extern TypeInfo RemotingServices_t2016_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServicesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::.cctor()
extern const MethodInfo RemotingServices__cctor_m10737_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingServices__cctor_m10737/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3996/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType MethodBase_t1102_0_0_0;
extern const Il2CppType MethodBase_t1102_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_GetVirtualMethod_m10738_ParameterInfos[] = 
{
	{"type", 0, 134222470, 0, &Type_t_0_0_0},
	{"method", 1, 134222471, 0, &MethodBase_t1102_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetVirtualMethod(System.Type,System.Reflection.MethodBase)
extern const MethodInfo RemotingServices_GetVirtualMethod_m10738_MethodInfo = 
{
	"GetVirtualMethod"/* name */
	, (methodPointerType)&RemotingServices_GetVirtualMethod_m10738/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1102_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_GetVirtualMethod_m10738_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3997/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_IsTransparentProxy_m10739_ParameterInfos[] = 
{
	{"proxy", 0, 134222472, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.RemotingServices::IsTransparentProxy(System.Object)
extern const MethodInfo RemotingServices_IsTransparentProxy_m10739_MethodInfo = 
{
	"IsTransparentProxy"/* name */
	, (methodPointerType)&RemotingServices_IsTransparentProxy_m10739/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_IsTransparentProxy_m10739_ParameterInfos/* parameters */
	, 605/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3998/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_GetServerTypeForUri_m10740_ParameterInfos[] = 
{
	{"URI", 0, 134222473, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.RemotingServices::GetServerTypeForUri(System.String)
extern const MethodInfo RemotingServices_GetServerTypeForUri_m10740_MethodInfo = 
{
	"GetServerTypeForUri"/* name */
	, (methodPointerType)&RemotingServices_GetServerTypeForUri_m10740/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_GetServerTypeForUri_m10740_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3999/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2008_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_Unmarshal_m10741_ParameterInfos[] = 
{
	{"objectRef", 0, 134222474, 0, &ObjRef_t2008_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::Unmarshal(System.Runtime.Remoting.ObjRef)
extern const MethodInfo RemotingServices_Unmarshal_m10741_MethodInfo = 
{
	"Unmarshal"/* name */
	, (methodPointerType)&RemotingServices_Unmarshal_m10741/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_Unmarshal_m10741_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4000/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2008_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_Unmarshal_m10742_ParameterInfos[] = 
{
	{"objectRef", 0, 134222475, 0, &ObjRef_t2008_0_0_0},
	{"fRefine", 1, 134222476, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::Unmarshal(System.Runtime.Remoting.ObjRef,System.Boolean)
extern const MethodInfo RemotingServices_Unmarshal_m10742_MethodInfo = 
{
	"Unmarshal"/* name */
	, (methodPointerType)&RemotingServices_Unmarshal_m10742/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274/* invoker_method */
	, RemotingServices_t2016_RemotingServices_Unmarshal_m10742_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4001/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MarshalByRefObject_t1308_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_Marshal_m10743_ParameterInfos[] = 
{
	{"Obj", 0, 134222477, 0, &MarshalByRefObject_t1308_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.RemotingServices::Marshal(System.MarshalByRefObject)
extern const MethodInfo RemotingServices_Marshal_m10743_MethodInfo = 
{
	"Marshal"/* name */
	, (methodPointerType)&RemotingServices_Marshal_m10743/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t2008_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_Marshal_m10743_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4002/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MarshalByRefObject_t1308_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_Marshal_m10744_ParameterInfos[] = 
{
	{"Obj", 0, 134222478, 0, &MarshalByRefObject_t1308_0_0_0},
	{"ObjURI", 1, 134222479, 0, &String_t_0_0_0},
	{"RequestedType", 2, 134222480, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.RemotingServices::Marshal(System.MarshalByRefObject,System.String,System.Type)
extern const MethodInfo RemotingServices_Marshal_m10744_MethodInfo = 
{
	"Marshal"/* name */
	, (methodPointerType)&RemotingServices_Marshal_m10744/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t2008_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_Marshal_m10744_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4003/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingServices::NewUri()
extern const MethodInfo RemotingServices_NewUri_m10745_MethodInfo = 
{
	"NewUri"/* name */
	, (methodPointerType)&RemotingServices_NewUri_m10745/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4004/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_GetRealProxy_m10746_ParameterInfos[] = 
{
	{"proxy", 0, 134222481, 0, &Object_t_0_0_0},
};
extern const Il2CppType RealProxy_t2000_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.RemotingServices::GetRealProxy(System.Object)
extern const MethodInfo RemotingServices_GetRealProxy_m10746_MethodInfo = 
{
	"GetRealProxy"/* name */
	, (methodPointerType)&RemotingServices_GetRealProxy_m10746/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &RealProxy_t2000_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_GetRealProxy_m10746_ParameterInfos/* parameters */
	, 606/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4005/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMethodMessage_t1990_0_0_0;
extern const Il2CppType IMethodMessage_t1990_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_GetMethodBaseFromMethodMessage_m10747_ParameterInfos[] = 
{
	{"msg", 0, 134222482, 0, &IMethodMessage_t1990_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetMethodBaseFromMethodMessage(System.Runtime.Remoting.Messaging.IMethodMessage)
extern const MethodInfo RemotingServices_GetMethodBaseFromMethodMessage_m10747_MethodInfo = 
{
	"GetMethodBaseFromMethodMessage"/* name */
	, (methodPointerType)&RemotingServices_GetMethodBaseFromMethodMessage_m10747/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1102_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_GetMethodBaseFromMethodMessage_m10747_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4006/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t238_0_0_0;
extern const Il2CppType TypeU5BU5D_t238_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_GetMethodBaseFromName_m10748_ParameterInfos[] = 
{
	{"type", 0, 134222483, 0, &Type_t_0_0_0},
	{"methodName", 1, 134222484, 0, &String_t_0_0_0},
	{"signature", 2, 134222485, 0, &TypeU5BU5D_t238_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetMethodBaseFromName(System.Type,System.String,System.Type[])
extern const MethodInfo RemotingServices_GetMethodBaseFromName_m10748_MethodInfo = 
{
	"GetMethodBaseFromName"/* name */
	, (methodPointerType)&RemotingServices_GetMethodBaseFromName_m10748/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1102_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_GetMethodBaseFromName_m10748_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4007/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t238_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_FindInterfaceMethod_m10749_ParameterInfos[] = 
{
	{"type", 0, 134222486, 0, &Type_t_0_0_0},
	{"methodName", 1, 134222487, 0, &String_t_0_0_0},
	{"signature", 2, 134222488, 0, &TypeU5BU5D_t238_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::FindInterfaceMethod(System.Type,System.String,System.Type[])
extern const MethodInfo RemotingServices_FindInterfaceMethod_m10749_MethodInfo = 
{
	"FindInterfaceMethod"/* name */
	, (methodPointerType)&RemotingServices_FindInterfaceMethod_m10749/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1102_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_FindInterfaceMethod_m10749_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4008/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_GetObjectData_m10750_ParameterInfos[] = 
{
	{"obj", 0, 134222489, 0, &Object_t_0_0_0},
	{"info", 1, 134222490, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 2, 134222491, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::GetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RemotingServices_GetObjectData_m10750_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&RemotingServices_GetObjectData_m10750/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, RemotingServices_t2016_RemotingServices_GetObjectData_m10750_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4009/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMethodMessage_t1990_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_IsMethodOverloaded_m10751_ParameterInfos[] = 
{
	{"msg", 0, 134222492, 0, &IMethodMessage_t1990_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.RemotingServices::IsMethodOverloaded(System.Runtime.Remoting.Messaging.IMethodMessage)
extern const MethodInfo RemotingServices_IsMethodOverloaded_m10751_MethodInfo = 
{
	"IsMethodOverloaded"/* name */
	, (methodPointerType)&RemotingServices_IsMethodOverloaded_m10751/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_IsMethodOverloaded_m10751_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4010/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MethodBase_t1102_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_IsOneWay_m10752_ParameterInfos[] = 
{
	{"method", 0, 134222493, 0, &MethodBase_t1102_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.RemotingServices::IsOneWay(System.Reflection.MethodBase)
extern const MethodInfo RemotingServices_IsOneWay_m10752_MethodInfo = 
{
	"IsOneWay"/* name */
	, (methodPointerType)&RemotingServices_IsOneWay_m10752/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_IsOneWay_m10752_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4011/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ActivatedClientTypeEntry_t2005_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_CreateClientProxy_m10753_ParameterInfos[] = 
{
	{"entry", 0, 134222494, 0, &ActivatedClientTypeEntry_t2005_0_0_0},
	{"activationAttributes", 1, 134222495, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxy(System.Runtime.Remoting.ActivatedClientTypeEntry,System.Object[])
extern const MethodInfo RemotingServices_CreateClientProxy_m10753_MethodInfo = 
{
	"CreateClientProxy"/* name */
	, (methodPointerType)&RemotingServices_CreateClientProxy_m10753/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_CreateClientProxy_m10753_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4012/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_CreateClientProxyForContextBound_m10754_ParameterInfos[] = 
{
	{"type", 0, 134222496, 0, &Type_t_0_0_0},
	{"activationAttributes", 1, 134222497, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxyForContextBound(System.Type,System.Object[])
extern const MethodInfo RemotingServices_CreateClientProxyForContextBound_m10754_MethodInfo = 
{
	"CreateClientProxyForContextBound"/* name */
	, (methodPointerType)&RemotingServices_CreateClientProxyForContextBound_m10754/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_CreateClientProxyForContextBound_m10754_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4013/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_GetIdentityForUri_m10755_ParameterInfos[] = 
{
	{"uri", 0, 134222498, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Identity System.Runtime.Remoting.RemotingServices::GetIdentityForUri(System.String)
extern const MethodInfo RemotingServices_GetIdentityForUri_m10755_MethodInfo = 
{
	"GetIdentityForUri"/* name */
	, (methodPointerType)&RemotingServices_GetIdentityForUri_m10755/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Identity_t2002_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_GetIdentityForUri_m10755_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4014/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_RemoveAppNameFromUri_m10756_ParameterInfos[] = 
{
	{"uri", 0, 134222499, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingServices::RemoveAppNameFromUri(System.String)
extern const MethodInfo RemotingServices_RemoveAppNameFromUri_m10756_MethodInfo = 
{
	"RemoveAppNameFromUri"/* name */
	, (methodPointerType)&RemotingServices_RemoveAppNameFromUri_m10756/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_RemoveAppNameFromUri_m10756_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4015/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2008_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType Object_t_1_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_GetOrCreateClientIdentity_m10757_ParameterInfos[] = 
{
	{"objRef", 0, 134222500, 0, &ObjRef_t2008_0_0_0},
	{"proxyType", 1, 134222501, 0, &Type_t_0_0_0},
	{"clientProxy", 2, 134222502, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ClientIdentity System.Runtime.Remoting.RemotingServices::GetOrCreateClientIdentity(System.Runtime.Remoting.ObjRef,System.Type,System.Object&)
extern const MethodInfo RemotingServices_GetOrCreateClientIdentity_m10757_MethodInfo = 
{
	"GetOrCreateClientIdentity"/* name */
	, (methodPointerType)&RemotingServices_GetOrCreateClientIdentity_m10757/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &ClientIdentity_t2010_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t1197/* invoker_method */
	, RemotingServices_t2016_RemotingServices_GetOrCreateClientIdentity_m10757_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4016/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MarshalByRefObject_t1308_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_CreateClientActivatedServerIdentity_m10758_ParameterInfos[] = 
{
	{"realObject", 0, 134222503, 0, &MarshalByRefObject_t1308_0_0_0},
	{"objectType", 1, 134222504, 0, &Type_t_0_0_0},
	{"objectUri", 2, 134222505, 0, &String_t_0_0_0},
};
extern const Il2CppType ClientActivatedIdentity_t2017_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ClientActivatedIdentity System.Runtime.Remoting.RemotingServices::CreateClientActivatedServerIdentity(System.MarshalByRefObject,System.Type,System.String)
extern const MethodInfo RemotingServices_CreateClientActivatedServerIdentity_m10758_MethodInfo = 
{
	"CreateClientActivatedServerIdentity"/* name */
	, (methodPointerType)&RemotingServices_CreateClientActivatedServerIdentity_m10758/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &ClientActivatedIdentity_t2017_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_CreateClientActivatedServerIdentity_m10758_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4017/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType WellKnownObjectMode_t2021_0_0_0;
extern const Il2CppType WellKnownObjectMode_t2021_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_CreateWellKnownServerIdentity_m10759_ParameterInfos[] = 
{
	{"objectType", 0, 134222506, 0, &Type_t_0_0_0},
	{"objectUri", 1, 134222507, 0, &String_t_0_0_0},
	{"mode", 2, 134222508, 0, &WellKnownObjectMode_t2021_0_0_0},
};
extern const Il2CppType ServerIdentity_t1681_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ServerIdentity System.Runtime.Remoting.RemotingServices::CreateWellKnownServerIdentity(System.Type,System.String,System.Runtime.Remoting.WellKnownObjectMode)
extern const MethodInfo RemotingServices_CreateWellKnownServerIdentity_m10759_MethodInfo = 
{
	"CreateWellKnownServerIdentity"/* name */
	, (methodPointerType)&RemotingServices_CreateWellKnownServerIdentity_m10759/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &ServerIdentity_t1681_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t253/* invoker_method */
	, RemotingServices_t2016_RemotingServices_CreateWellKnownServerIdentity_m10759_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4018/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ServerIdentity_t1681_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_RegisterServerIdentity_m10760_ParameterInfos[] = 
{
	{"identity", 0, 134222509, 0, &ServerIdentity_t1681_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::RegisterServerIdentity(System.Runtime.Remoting.ServerIdentity)
extern const MethodInfo RemotingServices_RegisterServerIdentity_m10760_MethodInfo = 
{
	"RegisterServerIdentity"/* name */
	, (methodPointerType)&RemotingServices_RegisterServerIdentity_m10760/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_RegisterServerIdentity_m10760_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4019/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2008_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_GetProxyForRemoteObject_m10761_ParameterInfos[] = 
{
	{"objref", 0, 134222510, 0, &ObjRef_t2008_0_0_0},
	{"classToProxy", 1, 134222511, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::GetProxyForRemoteObject(System.Runtime.Remoting.ObjRef,System.Type)
extern const MethodInfo RemotingServices_GetProxyForRemoteObject_m10761_MethodInfo = 
{
	"GetProxyForRemoteObject"/* name */
	, (methodPointerType)&RemotingServices_GetProxyForRemoteObject_m10761/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_GetProxyForRemoteObject_m10761_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4020/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t2008_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_GetRemoteObject_m10762_ParameterInfos[] = 
{
	{"objRef", 0, 134222512, 0, &ObjRef_t2008_0_0_0},
	{"proxyType", 1, 134222513, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::GetRemoteObject(System.Runtime.Remoting.ObjRef,System.Type)
extern const MethodInfo RemotingServices_GetRemoteObject_m10762_MethodInfo = 
{
	"GetRemoteObject"/* name */
	, (methodPointerType)&RemotingServices_GetRemoteObject_m10762/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_GetRemoteObject_m10762_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4021/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::RegisterInternalChannels()
extern const MethodInfo RemotingServices_RegisterInternalChannels_m10763_MethodInfo = 
{
	"RegisterInternalChannels"/* name */
	, (methodPointerType)&RemotingServices_RegisterInternalChannels_m10763/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4022/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Identity_t2002_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_DisposeIdentity_m10764_ParameterInfos[] = 
{
	{"ident", 0, 134222514, 0, &Identity_t2002_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::DisposeIdentity(System.Runtime.Remoting.Identity)
extern const MethodInfo RemotingServices_DisposeIdentity_m10764_MethodInfo = 
{
	"DisposeIdentity"/* name */
	, (methodPointerType)&RemotingServices_DisposeIdentity_m10764/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_DisposeIdentity_m10764_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4023/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t2016_RemotingServices_GetNormalizedUri_m10765_ParameterInfos[] = 
{
	{"uri", 0, 134222515, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingServices::GetNormalizedUri(System.String)
extern const MethodInfo RemotingServices_GetNormalizedUri_m10765_MethodInfo = 
{
	"GetNormalizedUri"/* name */
	, (methodPointerType)&RemotingServices_GetNormalizedUri_m10765/* method */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2016_RemotingServices_GetNormalizedUri_m10765_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4024/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingServices_t2016_MethodInfos[] =
{
	&RemotingServices__cctor_m10737_MethodInfo,
	&RemotingServices_GetVirtualMethod_m10738_MethodInfo,
	&RemotingServices_IsTransparentProxy_m10739_MethodInfo,
	&RemotingServices_GetServerTypeForUri_m10740_MethodInfo,
	&RemotingServices_Unmarshal_m10741_MethodInfo,
	&RemotingServices_Unmarshal_m10742_MethodInfo,
	&RemotingServices_Marshal_m10743_MethodInfo,
	&RemotingServices_Marshal_m10744_MethodInfo,
	&RemotingServices_NewUri_m10745_MethodInfo,
	&RemotingServices_GetRealProxy_m10746_MethodInfo,
	&RemotingServices_GetMethodBaseFromMethodMessage_m10747_MethodInfo,
	&RemotingServices_GetMethodBaseFromName_m10748_MethodInfo,
	&RemotingServices_FindInterfaceMethod_m10749_MethodInfo,
	&RemotingServices_GetObjectData_m10750_MethodInfo,
	&RemotingServices_IsMethodOverloaded_m10751_MethodInfo,
	&RemotingServices_IsOneWay_m10752_MethodInfo,
	&RemotingServices_CreateClientProxy_m10753_MethodInfo,
	&RemotingServices_CreateClientProxyForContextBound_m10754_MethodInfo,
	&RemotingServices_GetIdentityForUri_m10755_MethodInfo,
	&RemotingServices_RemoveAppNameFromUri_m10756_MethodInfo,
	&RemotingServices_GetOrCreateClientIdentity_m10757_MethodInfo,
	&RemotingServices_CreateClientActivatedServerIdentity_m10758_MethodInfo,
	&RemotingServices_CreateWellKnownServerIdentity_m10759_MethodInfo,
	&RemotingServices_RegisterServerIdentity_m10760_MethodInfo,
	&RemotingServices_GetProxyForRemoteObject_m10761_MethodInfo,
	&RemotingServices_GetRemoteObject_m10762_MethodInfo,
	&RemotingServices_RegisterInternalChannels_m10763_MethodInfo,
	&RemotingServices_DisposeIdentity_m10764_MethodInfo,
	&RemotingServices_GetNormalizedUri_m10765_MethodInfo,
	NULL
};
static const Il2CppMethodReference RemotingServices_t2016_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool RemotingServices_t2016_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingServices_t2016_0_0_0;
extern const Il2CppType RemotingServices_t2016_1_0_0;
struct RemotingServices_t2016;
const Il2CppTypeDefinitionMetadata RemotingServices_t2016_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingServices_t2016_VTable/* vtableMethods */
	, RemotingServices_t2016_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2086/* fieldStart */

};
TypeInfo RemotingServices_t2016_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingServices"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingServices_t2016_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingServices_t2016_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 604/* custom_attributes_cache */
	, &RemotingServices_t2016_0_0_0/* byval_arg */
	, &RemotingServices_t2016_1_0_0/* this_arg */
	, &RemotingServices_t2016_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingServices_t2016)/* instance_size */
	, sizeof (RemotingServices_t2016)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingServices_t2016_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 29/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentity.h"
// Metadata Definition System.Runtime.Remoting.ServerIdentity
extern TypeInfo ServerIdentity_t1681_il2cpp_TypeInfo;
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Context_t1963_0_0_0;
extern const Il2CppType Context_t1963_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ServerIdentity_t1681_ServerIdentity__ctor_m10766_ParameterInfos[] = 
{
	{"objectUri", 0, 134222516, 0, &String_t_0_0_0},
	{"context", 1, 134222517, 0, &Context_t1963_0_0_0},
	{"objectType", 2, 134222518, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ServerIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern const MethodInfo ServerIdentity__ctor_m10766_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ServerIdentity__ctor_m10766/* method */
	, &ServerIdentity_t1681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, ServerIdentity_t1681_ServerIdentity__ctor_m10766_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4025/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ServerIdentity::get_ObjectType()
extern const MethodInfo ServerIdentity_get_ObjectType_m10767_MethodInfo = 
{
	"get_ObjectType"/* name */
	, (methodPointerType)&ServerIdentity_get_ObjectType_m10767/* method */
	, &ServerIdentity_t1681_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4026/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILease_t1966_0_0_0;
extern const Il2CppType ILease_t1966_0_0_0;
static const ParameterInfo ServerIdentity_t1681_ServerIdentity_StartTrackingLifetime_m10768_ParameterInfos[] = 
{
	{"lease", 0, 134222519, 0, &ILease_t1966_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ServerIdentity::StartTrackingLifetime(System.Runtime.Remoting.Lifetime.ILease)
extern const MethodInfo ServerIdentity_StartTrackingLifetime_m10768_MethodInfo = 
{
	"StartTrackingLifetime"/* name */
	, (methodPointerType)&ServerIdentity_StartTrackingLifetime_m10768/* method */
	, &ServerIdentity_t1681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ServerIdentity_t1681_ServerIdentity_StartTrackingLifetime_m10768_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4027/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ServerIdentity::OnLifetimeExpired()
extern const MethodInfo ServerIdentity_OnLifetimeExpired_m10769_MethodInfo = 
{
	"OnLifetimeExpired"/* name */
	, (methodPointerType)&ServerIdentity_OnLifetimeExpired_m10769/* method */
	, &ServerIdentity_t1681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4028/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ServerIdentity_t1681_ServerIdentity_CreateObjRef_m10770_ParameterInfos[] = 
{
	{"requestedType", 0, 134222520, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.ServerIdentity::CreateObjRef(System.Type)
extern const MethodInfo ServerIdentity_CreateObjRef_m10770_MethodInfo = 
{
	"CreateObjRef"/* name */
	, (methodPointerType)&ServerIdentity_CreateObjRef_m10770/* method */
	, &ServerIdentity_t1681_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t2008_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ServerIdentity_t1681_ServerIdentity_CreateObjRef_m10770_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4029/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MarshalByRefObject_t1308_0_0_0;
extern const Il2CppType Context_t1963_0_0_0;
static const ParameterInfo ServerIdentity_t1681_ServerIdentity_AttachServerObject_m10771_ParameterInfos[] = 
{
	{"serverObject", 0, 134222521, 0, &MarshalByRefObject_t1308_0_0_0},
	{"context", 1, 134222522, 0, &Context_t1963_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ServerIdentity::AttachServerObject(System.MarshalByRefObject,System.Runtime.Remoting.Contexts.Context)
extern const MethodInfo ServerIdentity_AttachServerObject_m10771_MethodInfo = 
{
	"AttachServerObject"/* name */
	, (methodPointerType)&ServerIdentity_AttachServerObject_m10771/* method */
	, &ServerIdentity_t1681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ServerIdentity_t1681_ServerIdentity_AttachServerObject_m10771_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4030/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Lease_t1968_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Lifetime.Lease System.Runtime.Remoting.ServerIdentity::get_Lease()
extern const MethodInfo ServerIdentity_get_Lease_m10772_MethodInfo = 
{
	"get_Lease"/* name */
	, (methodPointerType)&ServerIdentity_get_Lease_m10772/* method */
	, &ServerIdentity_t1681_il2cpp_TypeInfo/* declaring_type */
	, &Lease_t1968_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4031/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ServerIdentity::DisposeServerObject()
extern const MethodInfo ServerIdentity_DisposeServerObject_m10773_MethodInfo = 
{
	"DisposeServerObject"/* name */
	, (methodPointerType)&ServerIdentity_DisposeServerObject_m10773/* method */
	, &ServerIdentity_t1681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4032/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ServerIdentity_t1681_MethodInfos[] =
{
	&ServerIdentity__ctor_m10766_MethodInfo,
	&ServerIdentity_get_ObjectType_m10767_MethodInfo,
	&ServerIdentity_StartTrackingLifetime_m10768_MethodInfo,
	&ServerIdentity_OnLifetimeExpired_m10769_MethodInfo,
	&ServerIdentity_CreateObjRef_m10770_MethodInfo,
	&ServerIdentity_AttachServerObject_m10771_MethodInfo,
	&ServerIdentity_get_Lease_m10772_MethodInfo,
	&ServerIdentity_DisposeServerObject_m10773_MethodInfo,
	NULL
};
extern const MethodInfo ServerIdentity_get_ObjectType_m10767_MethodInfo;
static const PropertyInfo ServerIdentity_t1681____ObjectType_PropertyInfo = 
{
	&ServerIdentity_t1681_il2cpp_TypeInfo/* parent */
	, "ObjectType"/* name */
	, &ServerIdentity_get_ObjectType_m10767_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ServerIdentity_get_Lease_m10772_MethodInfo;
static const PropertyInfo ServerIdentity_t1681____Lease_PropertyInfo = 
{
	&ServerIdentity_t1681_il2cpp_TypeInfo/* parent */
	, "Lease"/* name */
	, &ServerIdentity_get_Lease_m10772_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ServerIdentity_t1681_PropertyInfos[] =
{
	&ServerIdentity_t1681____ObjectType_PropertyInfo,
	&ServerIdentity_t1681____Lease_PropertyInfo,
	NULL
};
extern const MethodInfo ServerIdentity_CreateObjRef_m10770_MethodInfo;
extern const MethodInfo ServerIdentity_OnLifetimeExpired_m10769_MethodInfo;
static const Il2CppMethodReference ServerIdentity_t1681_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ServerIdentity_CreateObjRef_m10770_MethodInfo,
	&ServerIdentity_OnLifetimeExpired_m10769_MethodInfo,
};
static bool ServerIdentity_t1681_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ServerIdentity_t1681_1_0_0;
struct ServerIdentity_t1681;
const Il2CppTypeDefinitionMetadata ServerIdentity_t1681_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t2002_0_0_0/* parent */
	, ServerIdentity_t1681_VTable/* vtableMethods */
	, ServerIdentity_t1681_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2094/* fieldStart */

};
TypeInfo ServerIdentity_t1681_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ServerIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ServerIdentity_t1681_MethodInfos/* methods */
	, ServerIdentity_t1681_PropertyInfos/* properties */
	, NULL/* events */
	, &ServerIdentity_t1681_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ServerIdentity_t1681_0_0_0/* byval_arg */
	, &ServerIdentity_t1681_1_0_0/* this_arg */
	, &ServerIdentity_t1681_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ServerIdentity_t1681)/* instance_size */
	, sizeof (ServerIdentity_t1681)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientActivatedIdentity
extern TypeInfo ClientActivatedIdentity_t2017_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ClientActivatedIdentity_t2017_ClientActivatedIdentity__ctor_m10774_ParameterInfos[] = 
{
	{"objectUri", 0, 134222523, 0, &String_t_0_0_0},
	{"objectType", 1, 134222524, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ClientActivatedIdentity::.ctor(System.String,System.Type)
extern const MethodInfo ClientActivatedIdentity__ctor_m10774_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ClientActivatedIdentity__ctor_m10774/* method */
	, &ClientActivatedIdentity_t2017_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ClientActivatedIdentity_t2017_ClientActivatedIdentity__ctor_m10774_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4033/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
extern const MethodInfo ClientActivatedIdentity_GetServerObject_m10775_MethodInfo = 
{
	"GetServerObject"/* name */
	, (methodPointerType)&ClientActivatedIdentity_GetServerObject_m10775/* method */
	, &ClientActivatedIdentity_t2017_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t1308_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4034/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ClientActivatedIdentity::OnLifetimeExpired()
extern const MethodInfo ClientActivatedIdentity_OnLifetimeExpired_m10776_MethodInfo = 
{
	"OnLifetimeExpired"/* name */
	, (methodPointerType)&ClientActivatedIdentity_OnLifetimeExpired_m10776/* method */
	, &ClientActivatedIdentity_t2017_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4035/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ClientActivatedIdentity_t2017_MethodInfos[] =
{
	&ClientActivatedIdentity__ctor_m10774_MethodInfo,
	&ClientActivatedIdentity_GetServerObject_m10775_MethodInfo,
	&ClientActivatedIdentity_OnLifetimeExpired_m10776_MethodInfo,
	NULL
};
extern const MethodInfo ClientActivatedIdentity_OnLifetimeExpired_m10776_MethodInfo;
static const Il2CppMethodReference ClientActivatedIdentity_t2017_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ServerIdentity_CreateObjRef_m10770_MethodInfo,
	&ClientActivatedIdentity_OnLifetimeExpired_m10776_MethodInfo,
};
static bool ClientActivatedIdentity_t2017_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClientActivatedIdentity_t2017_1_0_0;
struct ClientActivatedIdentity_t2017;
const Il2CppTypeDefinitionMetadata ClientActivatedIdentity_t2017_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t1681_0_0_0/* parent */
	, ClientActivatedIdentity_t2017_VTable/* vtableMethods */
	, ClientActivatedIdentity_t2017_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ClientActivatedIdentity_t2017_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientActivatedIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ClientActivatedIdentity_t2017_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ClientActivatedIdentity_t2017_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientActivatedIdentity_t2017_0_0_0/* byval_arg */
	, &ClientActivatedIdentity_t2017_1_0_0/* this_arg */
	, &ClientActivatedIdentity_t2017_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientActivatedIdentity_t2017)/* instance_size */
	, sizeof (ClientActivatedIdentity_t2017)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingletonIdentity
extern TypeInfo SingletonIdentity_t2018_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Context_t1963_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SingletonIdentity_t2018_SingletonIdentity__ctor_m10777_ParameterInfos[] = 
{
	{"objectUri", 0, 134222525, 0, &String_t_0_0_0},
	{"context", 1, 134222526, 0, &Context_t1963_0_0_0},
	{"objectType", 2, 134222527, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.SingletonIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern const MethodInfo SingletonIdentity__ctor_m10777_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SingletonIdentity__ctor_m10777/* method */
	, &SingletonIdentity_t2018_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, SingletonIdentity_t2018_SingletonIdentity__ctor_m10777_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4036/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SingletonIdentity_t2018_MethodInfos[] =
{
	&SingletonIdentity__ctor_m10777_MethodInfo,
	NULL
};
static const Il2CppMethodReference SingletonIdentity_t2018_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ServerIdentity_CreateObjRef_m10770_MethodInfo,
	&ServerIdentity_OnLifetimeExpired_m10769_MethodInfo,
};
static bool SingletonIdentity_t2018_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SingletonIdentity_t2018_0_0_0;
extern const Il2CppType SingletonIdentity_t2018_1_0_0;
struct SingletonIdentity_t2018;
const Il2CppTypeDefinitionMetadata SingletonIdentity_t2018_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t1681_0_0_0/* parent */
	, SingletonIdentity_t2018_VTable/* vtableMethods */
	, SingletonIdentity_t2018_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SingletonIdentity_t2018_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingletonIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, SingletonIdentity_t2018_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SingletonIdentity_t2018_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingletonIdentity_t2018_0_0_0/* byval_arg */
	, &SingletonIdentity_t2018_1_0_0/* this_arg */
	, &SingletonIdentity_t2018_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingletonIdentity_t2018)/* instance_size */
	, sizeof (SingletonIdentity_t2018)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingleCallIdentity
extern TypeInfo SingleCallIdentity_t2019_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Context_t1963_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SingleCallIdentity_t2019_SingleCallIdentity__ctor_m10778_ParameterInfos[] = 
{
	{"objectUri", 0, 134222528, 0, &String_t_0_0_0},
	{"context", 1, 134222529, 0, &Context_t1963_0_0_0},
	{"objectType", 2, 134222530, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.SingleCallIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern const MethodInfo SingleCallIdentity__ctor_m10778_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SingleCallIdentity__ctor_m10778/* method */
	, &SingleCallIdentity_t2019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, SingleCallIdentity_t2019_SingleCallIdentity__ctor_m10778_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4037/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SingleCallIdentity_t2019_MethodInfos[] =
{
	&SingleCallIdentity__ctor_m10778_MethodInfo,
	NULL
};
static const Il2CppMethodReference SingleCallIdentity_t2019_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ServerIdentity_CreateObjRef_m10770_MethodInfo,
	&ServerIdentity_OnLifetimeExpired_m10769_MethodInfo,
};
static bool SingleCallIdentity_t2019_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SingleCallIdentity_t2019_0_0_0;
extern const Il2CppType SingleCallIdentity_t2019_1_0_0;
struct SingleCallIdentity_t2019;
const Il2CppTypeDefinitionMetadata SingleCallIdentity_t2019_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t1681_0_0_0/* parent */
	, SingleCallIdentity_t2019_VTable/* vtableMethods */
	, SingleCallIdentity_t2019_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SingleCallIdentity_t2019_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingleCallIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, SingleCallIdentity_t2019_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SingleCallIdentity_t2019_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingleCallIdentity_t2019_0_0_0/* byval_arg */
	, &SingleCallIdentity_t2019_1_0_0/* this_arg */
	, &SingleCallIdentity_t2019_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingleCallIdentity_t2019)/* instance_size */
	, sizeof (SingleCallIdentity_t2019)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntry.h"
// Metadata Definition System.Runtime.Remoting.TypeEntry
extern TypeInfo TypeEntry_t2006_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntryMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeEntry::get_AssemblyName()
extern const MethodInfo TypeEntry_get_AssemblyName_m10779_MethodInfo = 
{
	"get_AssemblyName"/* name */
	, (methodPointerType)&TypeEntry_get_AssemblyName_m10779/* method */
	, &TypeEntry_t2006_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4038/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeEntry::get_TypeName()
extern const MethodInfo TypeEntry_get_TypeName_m10780_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&TypeEntry_get_TypeName_m10780/* method */
	, &TypeEntry_t2006_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4039/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeEntry_t2006_MethodInfos[] =
{
	&TypeEntry_get_AssemblyName_m10779_MethodInfo,
	&TypeEntry_get_TypeName_m10780_MethodInfo,
	NULL
};
extern const MethodInfo TypeEntry_get_AssemblyName_m10779_MethodInfo;
static const PropertyInfo TypeEntry_t2006____AssemblyName_PropertyInfo = 
{
	&TypeEntry_t2006_il2cpp_TypeInfo/* parent */
	, "AssemblyName"/* name */
	, &TypeEntry_get_AssemblyName_m10779_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TypeEntry_get_TypeName_m10780_MethodInfo;
static const PropertyInfo TypeEntry_t2006____TypeName_PropertyInfo = 
{
	&TypeEntry_t2006_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &TypeEntry_get_TypeName_m10780_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TypeEntry_t2006_PropertyInfos[] =
{
	&TypeEntry_t2006____AssemblyName_PropertyInfo,
	&TypeEntry_t2006____TypeName_PropertyInfo,
	NULL
};
static const Il2CppMethodReference TypeEntry_t2006_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool TypeEntry_t2006_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeEntry_t2006_1_0_0;
struct TypeEntry_t2006;
const Il2CppTypeDefinitionMetadata TypeEntry_t2006_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeEntry_t2006_VTable/* vtableMethods */
	, TypeEntry_t2006_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2099/* fieldStart */

};
TypeInfo TypeEntry_t2006_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, TypeEntry_t2006_MethodInfos/* methods */
	, TypeEntry_t2006_PropertyInfos/* properties */
	, NULL/* events */
	, &TypeEntry_t2006_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 607/* custom_attributes_cache */
	, &TypeEntry_t2006_0_0_0/* byval_arg */
	, &TypeEntry_t2006_1_0_0/* this_arg */
	, &TypeEntry_t2006_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeEntry_t2006)/* instance_size */
	, sizeof (TypeEntry_t2006)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfo.h"
// Metadata Definition System.Runtime.Remoting.TypeInfo
extern TypeInfo TypeInfo_t2020_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfoMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo TypeInfo_t2020_TypeInfo__ctor_m10781_ParameterInfos[] = 
{
	{"type", 0, 134222531, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.TypeInfo::.ctor(System.Type)
extern const MethodInfo TypeInfo__ctor_m10781_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInfo__ctor_m10781/* method */
	, &TypeInfo_t2020_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TypeInfo_t2020_TypeInfo__ctor_m10781_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4040/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeInfo::get_TypeName()
extern const MethodInfo TypeInfo_get_TypeName_m10782_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&TypeInfo_get_TypeName_m10782/* method */
	, &TypeInfo_t2020_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4041/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeInfo_t2020_MethodInfos[] =
{
	&TypeInfo__ctor_m10781_MethodInfo,
	&TypeInfo_get_TypeName_m10782_MethodInfo,
	NULL
};
extern const MethodInfo TypeInfo_get_TypeName_m10782_MethodInfo;
static const PropertyInfo TypeInfo_t2020____TypeName_PropertyInfo = 
{
	&TypeInfo_t2020_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &TypeInfo_get_TypeName_m10782_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TypeInfo_t2020_PropertyInfos[] =
{
	&TypeInfo_t2020____TypeName_PropertyInfo,
	NULL
};
static const Il2CppMethodReference TypeInfo_t2020_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&TypeInfo_get_TypeName_m10782_MethodInfo,
};
static bool TypeInfo_t2020_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* TypeInfo_t2020_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t2012_0_0_0,
};
static Il2CppInterfaceOffsetPair TypeInfo_t2020_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t2012_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInfo_t2020_0_0_0;
extern const Il2CppType TypeInfo_t2020_1_0_0;
struct TypeInfo_t2020;
const Il2CppTypeDefinitionMetadata TypeInfo_t2020_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TypeInfo_t2020_InterfacesTypeInfos/* implementedInterfaces */
	, TypeInfo_t2020_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeInfo_t2020_VTable/* vtableMethods */
	, TypeInfo_t2020_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2101/* fieldStart */

};
TypeInfo TypeInfo_t2020_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, TypeInfo_t2020_MethodInfos/* methods */
	, TypeInfo_t2020_PropertyInfos/* properties */
	, NULL/* events */
	, &TypeInfo_t2020_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInfo_t2020_0_0_0/* byval_arg */
	, &TypeInfo_t2020_1_0_0/* this_arg */
	, &TypeInfo_t2020_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInfo_t2020)/* instance_size */
	, sizeof (TypeInfo_t2020)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
// Metadata Definition System.Runtime.Remoting.WellKnownObjectMode
extern TypeInfo WellKnownObjectMode_t2021_il2cpp_TypeInfo;
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectModeMethodDeclarations.h"
static const MethodInfo* WellKnownObjectMode_t2021_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m1051_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m1052_MethodInfo;
extern const MethodInfo Enum_ToString_m1053_MethodInfo;
extern const MethodInfo Enum_ToString_m1054_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m1055_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m1056_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m1057_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m1058_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m1059_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m1060_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m1061_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m1062_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m1063_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m1064_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m1065_MethodInfo;
extern const MethodInfo Enum_ToString_m1066_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m1067_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m1068_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m1069_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m1070_MethodInfo;
extern const MethodInfo Enum_CompareTo_m1071_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m1072_MethodInfo;
static const Il2CppMethodReference WellKnownObjectMode_t2021_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool WellKnownObjectMode_t2021_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t275_0_0_0;
extern const Il2CppType IConvertible_t276_0_0_0;
extern const Il2CppType IComparable_t277_0_0_0;
static Il2CppInterfaceOffsetPair WellKnownObjectMode_t2021_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WellKnownObjectMode_t2021_1_0_0;
extern const Il2CppType Enum_t278_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t253_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata WellKnownObjectMode_t2021_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WellKnownObjectMode_t2021_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, WellKnownObjectMode_t2021_VTable/* vtableMethods */
	, WellKnownObjectMode_t2021_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2104/* fieldStart */

};
TypeInfo WellKnownObjectMode_t2021_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WellKnownObjectMode"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, WellKnownObjectMode_t2021_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 608/* custom_attributes_cache */
	, &WellKnownObjectMode_t2021_0_0_0/* byval_arg */
	, &WellKnownObjectMode_t2021_1_0_0/* this_arg */
	, &WellKnownObjectMode_t2021_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WellKnownObjectMode_t2021)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WellKnownObjectMode_t2021)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryCommon
extern TypeInfo BinaryCommon_t2022_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryCommon::.cctor()
extern const MethodInfo BinaryCommon__cctor_m10783_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&BinaryCommon__cctor_m10783/* method */
	, &BinaryCommon_t2022_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4042/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo BinaryCommon_t2022_BinaryCommon_IsPrimitive_m10784_ParameterInfos[] = 
{
	{"type", 0, 134222532, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.BinaryCommon::IsPrimitive(System.Type)
extern const MethodInfo BinaryCommon_IsPrimitive_m10784_MethodInfo = 
{
	"IsPrimitive"/* name */
	, (methodPointerType)&BinaryCommon_IsPrimitive_m10784/* method */
	, &BinaryCommon_t2022_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, BinaryCommon_t2022_BinaryCommon_IsPrimitive_m10784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4043/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo BinaryCommon_t2022_BinaryCommon_GetTypeCode_m10785_ParameterInfos[] = 
{
	{"type", 0, 134222533, 0, &Type_t_0_0_0},
};
extern const Il2CppType Byte_t680_0_0_0;
extern void* RuntimeInvoker_Byte_t680_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte System.Runtime.Serialization.Formatters.Binary.BinaryCommon::GetTypeCode(System.Type)
extern const MethodInfo BinaryCommon_GetTypeCode_m10785_MethodInfo = 
{
	"GetTypeCode"/* name */
	, (methodPointerType)&BinaryCommon_GetTypeCode_m10785/* method */
	, &BinaryCommon_t2022_il2cpp_TypeInfo/* declaring_type */
	, &Byte_t680_0_0_0/* return_type */
	, RuntimeInvoker_Byte_t680_Object_t/* invoker_method */
	, BinaryCommon_t2022_BinaryCommon_GetTypeCode_m10785_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4044/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo BinaryCommon_t2022_BinaryCommon_GetTypeFromCode_m10786_ParameterInfos[] = 
{
	{"code", 0, 134222534, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.BinaryCommon::GetTypeFromCode(System.Int32)
extern const MethodInfo BinaryCommon_GetTypeFromCode_m10786_MethodInfo = 
{
	"GetTypeFromCode"/* name */
	, (methodPointerType)&BinaryCommon_GetTypeFromCode_m10786/* method */
	, &BinaryCommon_t2022_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, BinaryCommon_t2022_BinaryCommon_GetTypeFromCode_m10786_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4045/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo BinaryCommon_t2022_BinaryCommon_CheckSerializable_m10787_ParameterInfos[] = 
{
	{"type", 0, 134222535, 0, &Type_t_0_0_0},
	{"selector", 1, 134222536, 0, &ISurrogateSelector_t1996_0_0_0},
	{"context", 2, 134222537, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryCommon::CheckSerializable(System.Type,System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo BinaryCommon_CheckSerializable_m10787_MethodInfo = 
{
	"CheckSerializable"/* name */
	, (methodPointerType)&BinaryCommon_CheckSerializable_m10787/* method */
	, &BinaryCommon_t2022_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, BinaryCommon_t2022_BinaryCommon_CheckSerializable_m10787_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4046/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo BinaryCommon_t2022_BinaryCommon_SwapBytes_m10788_ParameterInfos[] = 
{
	{"byteArray", 0, 134222538, 0, &ByteU5BU5D_t850_0_0_0},
	{"size", 1, 134222539, 0, &Int32_t253_0_0_0},
	{"dataSize", 2, 134222540, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryCommon::SwapBytes(System.Byte[],System.Int32,System.Int32)
extern const MethodInfo BinaryCommon_SwapBytes_m10788_MethodInfo = 
{
	"SwapBytes"/* name */
	, (methodPointerType)&BinaryCommon_SwapBytes_m10788/* method */
	, &BinaryCommon_t2022_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, BinaryCommon_t2022_BinaryCommon_SwapBytes_m10788_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4047/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BinaryCommon_t2022_MethodInfos[] =
{
	&BinaryCommon__cctor_m10783_MethodInfo,
	&BinaryCommon_IsPrimitive_m10784_MethodInfo,
	&BinaryCommon_GetTypeCode_m10785_MethodInfo,
	&BinaryCommon_GetTypeFromCode_m10786_MethodInfo,
	&BinaryCommon_CheckSerializable_m10787_MethodInfo,
	&BinaryCommon_SwapBytes_m10788_MethodInfo,
	NULL
};
static const Il2CppMethodReference BinaryCommon_t2022_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool BinaryCommon_t2022_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryCommon_t2022_0_0_0;
extern const Il2CppType BinaryCommon_t2022_1_0_0;
struct BinaryCommon_t2022;
const Il2CppTypeDefinitionMetadata BinaryCommon_t2022_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryCommon_t2022_VTable/* vtableMethods */
	, BinaryCommon_t2022_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2107/* fieldStart */

};
TypeInfo BinaryCommon_t2022_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryCommon"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryCommon_t2022_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BinaryCommon_t2022_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryCommon_t2022_0_0_0/* byval_arg */
	, &BinaryCommon_t2022_1_0_0/* this_arg */
	, &BinaryCommon_t2022_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryCommon_t2022)/* instance_size */
	, sizeof (BinaryCommon_t2022)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryCommon_t2022_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryElement
extern TypeInfo BinaryElement_t2023_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1MethodDeclarations.h"
static const MethodInfo* BinaryElement_t2023_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BinaryElement_t2023_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool BinaryElement_t2023_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BinaryElement_t2023_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryElement_t2023_0_0_0;
extern const Il2CppType BinaryElement_t2023_1_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t680_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata BinaryElement_t2023_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BinaryElement_t2023_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, BinaryElement_t2023_VTable/* vtableMethods */
	, BinaryElement_t2023_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2111/* fieldStart */

};
TypeInfo BinaryElement_t2023_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryElement"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryElement_t2023_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryElement_t2023_0_0_0/* byval_arg */
	, &BinaryElement_t2023_1_0_0/* this_arg */
	, &BinaryElement_t2023_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryElement_t2023)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BinaryElement_t2023)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.TypeTag
extern TypeInfo TypeTag_t2024_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_TypeMethodDeclarations.h"
static const MethodInfo* TypeTag_t2024_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeTag_t2024_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TypeTag_t2024_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeTag_t2024_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeTag_t2024_0_0_0;
extern const Il2CppType TypeTag_t2024_1_0_0;
const Il2CppTypeDefinitionMetadata TypeTag_t2024_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeTag_t2024_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TypeTag_t2024_VTable/* vtableMethods */
	, TypeTag_t2024_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2135/* fieldStart */

};
TypeInfo TypeTag_t2024_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, TypeTag_t2024_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeTag_t2024_0_0_0/* byval_arg */
	, &TypeTag_t2024_1_0_0/* this_arg */
	, &TypeTag_t2024_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeTag_t2024)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeTag_t2024)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Meth.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MethodFlags
extern TypeInfo MethodFlags_t2025_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MethMethodDeclarations.h"
static const MethodInfo* MethodFlags_t2025_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MethodFlags_t2025_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool MethodFlags_t2025_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodFlags_t2025_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodFlags_t2025_0_0_0;
extern const Il2CppType MethodFlags_t2025_1_0_0;
const Il2CppTypeDefinitionMetadata MethodFlags_t2025_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodFlags_t2025_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, MethodFlags_t2025_VTable/* vtableMethods */
	, MethodFlags_t2025_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2144/* fieldStart */

};
TypeInfo MethodFlags_t2025_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodFlags"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, MethodFlags_t2025_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodFlags_t2025_0_0_0/* byval_arg */
	, &MethodFlags_t2025_1_0_0/* this_arg */
	, &MethodFlags_t2025_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodFlags_t2025)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodFlags_t2025)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Retu.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
extern TypeInfo ReturnTypeTag_t2026_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_RetuMethodDeclarations.h"
static const MethodInfo* ReturnTypeTag_t2026_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ReturnTypeTag_t2026_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ReturnTypeTag_t2026_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ReturnTypeTag_t2026_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReturnTypeTag_t2026_0_0_0;
extern const Il2CppType ReturnTypeTag_t2026_1_0_0;
const Il2CppTypeDefinitionMetadata ReturnTypeTag_t2026_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReturnTypeTag_t2026_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ReturnTypeTag_t2026_VTable/* vtableMethods */
	, ReturnTypeTag_t2026_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2155/* fieldStart */

};
TypeInfo ReturnTypeTag_t2026_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnTypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, ReturnTypeTag_t2026_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReturnTypeTag_t2026_0_0_0/* byval_arg */
	, &ReturnTypeTag_t2026_1_0_0/* this_arg */
	, &ReturnTypeTag_t2026_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnTypeTag_t2026)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReturnTypeTag_t2026)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
extern TypeInfo BinaryFormatter_t424_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_BinaMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor()
extern const MethodInfo BinaryFormatter__ctor_m1805_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BinaryFormatter__ctor_m1805/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4048/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo BinaryFormatter_t424_BinaryFormatter__ctor_m10789_ParameterInfos[] = 
{
	{"selector", 0, 134222541, 0, &ISurrogateSelector_t1996_0_0_0},
	{"context", 1, 134222542, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor(System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo BinaryFormatter__ctor_m10789_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BinaryFormatter__ctor_m10789/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, BinaryFormatter_t424_BinaryFormatter__ctor_m10789_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4049/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_DefaultSurrogateSelector()
extern const MethodInfo BinaryFormatter_get_DefaultSurrogateSelector_m10790_MethodInfo = 
{
	"get_DefaultSurrogateSelector"/* name */
	, (methodPointerType)&BinaryFormatter_get_DefaultSurrogateSelector_m10790/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &ISurrogateSelector_t1996_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 611/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4050/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FormatterAssemblyStyle_t2043_0_0_0;
extern const Il2CppType FormatterAssemblyStyle_t2043_0_0_0;
static const ParameterInfo BinaryFormatter_t424_BinaryFormatter_set_AssemblyFormat_m10791_ParameterInfos[] = 
{
	{"value", 0, 134222543, 0, &FormatterAssemblyStyle_t2043_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::set_AssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern const MethodInfo BinaryFormatter_set_AssemblyFormat_m10791_MethodInfo = 
{
	"set_AssemblyFormat"/* name */
	, (methodPointerType)&BinaryFormatter_set_AssemblyFormat_m10791/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, BinaryFormatter_t424_BinaryFormatter_set_AssemblyFormat_m10791_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4051/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationBinder_t2027_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.SerializationBinder System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_Binder()
extern const MethodInfo BinaryFormatter_get_Binder_m10792_MethodInfo = 
{
	"get_Binder"/* name */
	, (methodPointerType)&BinaryFormatter_get_Binder_m10792/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &SerializationBinder_t2027_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4052/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_Context()
extern const MethodInfo BinaryFormatter_get_Context_m10793_MethodInfo = 
{
	"get_Context"/* name */
	, (methodPointerType)&BinaryFormatter_get_Context_m10793/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &StreamingContext_t1044_0_0_0/* return_type */
	, RuntimeInvoker_StreamingContext_t1044/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4053/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_SurrogateSelector()
extern const MethodInfo BinaryFormatter_get_SurrogateSelector_m10794_MethodInfo = 
{
	"get_SurrogateSelector"/* name */
	, (methodPointerType)&BinaryFormatter_get_SurrogateSelector_m10794/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &ISurrogateSelector_t1996_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4054/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeFilterLevel_t2045_0_0_0;
extern void* RuntimeInvoker_TypeFilterLevel_t2045 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.Formatters.TypeFilterLevel System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_FilterLevel()
extern const MethodInfo BinaryFormatter_get_FilterLevel_m10795_MethodInfo = 
{
	"get_FilterLevel"/* name */
	, (methodPointerType)&BinaryFormatter_get_FilterLevel_m10795/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &TypeFilterLevel_t2045_0_0_0/* return_type */
	, RuntimeInvoker_TypeFilterLevel_t2045/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4055/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Stream_t426_0_0_0;
extern const Il2CppType Stream_t426_0_0_0;
static const ParameterInfo BinaryFormatter_t424_BinaryFormatter_Deserialize_m10796_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222544, 0, &Stream_t426_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Deserialize(System.IO.Stream)
extern const MethodInfo BinaryFormatter_Deserialize_m10796_MethodInfo = 
{
	"Deserialize"/* name */
	, (methodPointerType)&BinaryFormatter_Deserialize_m10796/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, BinaryFormatter_t424_BinaryFormatter_Deserialize_m10796_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4056/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Stream_t426_0_0_0;
extern const Il2CppType HeaderHandler_t2252_0_0_0;
extern const Il2CppType HeaderHandler_t2252_0_0_0;
static const ParameterInfo BinaryFormatter_t424_BinaryFormatter_NoCheckDeserialize_m10797_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222545, 0, &Stream_t426_0_0_0},
	{"handler", 1, 134222546, 0, &HeaderHandler_t2252_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::NoCheckDeserialize(System.IO.Stream,System.Runtime.Remoting.Messaging.HeaderHandler)
extern const MethodInfo BinaryFormatter_NoCheckDeserialize_m10797_MethodInfo = 
{
	"NoCheckDeserialize"/* name */
	, (methodPointerType)&BinaryFormatter_NoCheckDeserialize_m10797/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, BinaryFormatter_t424_BinaryFormatter_NoCheckDeserialize_m10797_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4057/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Stream_t426_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo BinaryFormatter_t424_BinaryFormatter_Serialize_m10798_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222547, 0, &Stream_t426_0_0_0},
	{"graph", 1, 134222548, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Serialize(System.IO.Stream,System.Object)
extern const MethodInfo BinaryFormatter_Serialize_m10798_MethodInfo = 
{
	"Serialize"/* name */
	, (methodPointerType)&BinaryFormatter_Serialize_m10798/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, BinaryFormatter_t424_BinaryFormatter_Serialize_m10798_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4058/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Stream_t426_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType HeaderU5BU5D_t2251_0_0_0;
extern const Il2CppType HeaderU5BU5D_t2251_0_0_0;
static const ParameterInfo BinaryFormatter_t424_BinaryFormatter_Serialize_m10799_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222549, 0, &Stream_t426_0_0_0},
	{"graph", 1, 134222550, 0, &Object_t_0_0_0},
	{"headers", 2, 134222551, 0, &HeaderU5BU5D_t2251_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Serialize(System.IO.Stream,System.Object,System.Runtime.Remoting.Messaging.Header[])
extern const MethodInfo BinaryFormatter_Serialize_m10799_MethodInfo = 
{
	"Serialize"/* name */
	, (methodPointerType)&BinaryFormatter_Serialize_m10799/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, BinaryFormatter_t424_BinaryFormatter_Serialize_m10799_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4059/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo BinaryFormatter_t424_BinaryFormatter_WriteBinaryHeader_m10800_ParameterInfos[] = 
{
	{"writer", 0, 134222552, 0, &BinaryWriter_t1806_0_0_0},
	{"hasHeaders", 1, 134222553, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::WriteBinaryHeader(System.IO.BinaryWriter,System.Boolean)
extern const MethodInfo BinaryFormatter_WriteBinaryHeader_m10800_MethodInfo = 
{
	"WriteBinaryHeader"/* name */
	, (methodPointerType)&BinaryFormatter_WriteBinaryHeader_m10800/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, BinaryFormatter_t424_BinaryFormatter_WriteBinaryHeader_m10800_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4060/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Boolean_t273_1_0_2;
extern const Il2CppType Boolean_t273_1_0_0;
static const ParameterInfo BinaryFormatter_t424_BinaryFormatter_ReadBinaryHeader_m10801_ParameterInfos[] = 
{
	{"reader", 0, 134222554, 0, &BinaryReader_t1805_0_0_0},
	{"hasHeaders", 1, 134222555, 0, &Boolean_t273_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::ReadBinaryHeader(System.IO.BinaryReader,System.Boolean&)
extern const MethodInfo BinaryFormatter_ReadBinaryHeader_m10801_MethodInfo = 
{
	"ReadBinaryHeader"/* name */
	, (methodPointerType)&BinaryFormatter_ReadBinaryHeader_m10801/* method */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_BooleanU26_t745/* invoker_method */
	, BinaryFormatter_t424_BinaryFormatter_ReadBinaryHeader_m10801_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4061/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BinaryFormatter_t424_MethodInfos[] =
{
	&BinaryFormatter__ctor_m1805_MethodInfo,
	&BinaryFormatter__ctor_m10789_MethodInfo,
	&BinaryFormatter_get_DefaultSurrogateSelector_m10790_MethodInfo,
	&BinaryFormatter_set_AssemblyFormat_m10791_MethodInfo,
	&BinaryFormatter_get_Binder_m10792_MethodInfo,
	&BinaryFormatter_get_Context_m10793_MethodInfo,
	&BinaryFormatter_get_SurrogateSelector_m10794_MethodInfo,
	&BinaryFormatter_get_FilterLevel_m10795_MethodInfo,
	&BinaryFormatter_Deserialize_m10796_MethodInfo,
	&BinaryFormatter_NoCheckDeserialize_m10797_MethodInfo,
	&BinaryFormatter_Serialize_m10798_MethodInfo,
	&BinaryFormatter_Serialize_m10799_MethodInfo,
	&BinaryFormatter_WriteBinaryHeader_m10800_MethodInfo,
	&BinaryFormatter_ReadBinaryHeader_m10801_MethodInfo,
	NULL
};
extern const MethodInfo BinaryFormatter_get_DefaultSurrogateSelector_m10790_MethodInfo;
static const PropertyInfo BinaryFormatter_t424____DefaultSurrogateSelector_PropertyInfo = 
{
	&BinaryFormatter_t424_il2cpp_TypeInfo/* parent */
	, "DefaultSurrogateSelector"/* name */
	, &BinaryFormatter_get_DefaultSurrogateSelector_m10790_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_set_AssemblyFormat_m10791_MethodInfo;
static const PropertyInfo BinaryFormatter_t424____AssemblyFormat_PropertyInfo = 
{
	&BinaryFormatter_t424_il2cpp_TypeInfo/* parent */
	, "AssemblyFormat"/* name */
	, NULL/* get */
	, &BinaryFormatter_set_AssemblyFormat_m10791_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_Binder_m10792_MethodInfo;
static const PropertyInfo BinaryFormatter_t424____Binder_PropertyInfo = 
{
	&BinaryFormatter_t424_il2cpp_TypeInfo/* parent */
	, "Binder"/* name */
	, &BinaryFormatter_get_Binder_m10792_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_Context_m10793_MethodInfo;
static const PropertyInfo BinaryFormatter_t424____Context_PropertyInfo = 
{
	&BinaryFormatter_t424_il2cpp_TypeInfo/* parent */
	, "Context"/* name */
	, &BinaryFormatter_get_Context_m10793_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_SurrogateSelector_m10794_MethodInfo;
static const PropertyInfo BinaryFormatter_t424____SurrogateSelector_PropertyInfo = 
{
	&BinaryFormatter_t424_il2cpp_TypeInfo/* parent */
	, "SurrogateSelector"/* name */
	, &BinaryFormatter_get_SurrogateSelector_m10794_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_FilterLevel_m10795_MethodInfo;
static const PropertyInfo BinaryFormatter_t424____FilterLevel_PropertyInfo = 
{
	&BinaryFormatter_t424_il2cpp_TypeInfo/* parent */
	, "FilterLevel"/* name */
	, &BinaryFormatter_get_FilterLevel_m10795_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* BinaryFormatter_t424_PropertyInfos[] =
{
	&BinaryFormatter_t424____DefaultSurrogateSelector_PropertyInfo,
	&BinaryFormatter_t424____AssemblyFormat_PropertyInfo,
	&BinaryFormatter_t424____Binder_PropertyInfo,
	&BinaryFormatter_t424____Context_PropertyInfo,
	&BinaryFormatter_t424____SurrogateSelector_PropertyInfo,
	&BinaryFormatter_t424____FilterLevel_PropertyInfo,
	NULL
};
extern const MethodInfo BinaryFormatter_Deserialize_m10796_MethodInfo;
extern const MethodInfo BinaryFormatter_Serialize_m10798_MethodInfo;
extern const MethodInfo BinaryFormatter_Serialize_m10799_MethodInfo;
static const Il2CppMethodReference BinaryFormatter_t424_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&BinaryFormatter_get_Binder_m10792_MethodInfo,
	&BinaryFormatter_get_Context_m10793_MethodInfo,
	&BinaryFormatter_get_SurrogateSelector_m10794_MethodInfo,
	&BinaryFormatter_Deserialize_m10796_MethodInfo,
	&BinaryFormatter_Serialize_m10798_MethodInfo,
	&BinaryFormatter_Serialize_m10799_MethodInfo,
};
static bool BinaryFormatter_t424_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IRemotingFormatter_t2383_0_0_0;
extern const Il2CppType IFormatter_t2385_0_0_0;
static const Il2CppType* BinaryFormatter_t424_InterfacesTypeInfos[] = 
{
	&IRemotingFormatter_t2383_0_0_0,
	&IFormatter_t2385_0_0_0,
};
static Il2CppInterfaceOffsetPair BinaryFormatter_t424_InterfacesOffsets[] = 
{
	{ &IRemotingFormatter_t2383_0_0_0, 4},
	{ &IFormatter_t2385_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryFormatter_t424_0_0_0;
extern const Il2CppType BinaryFormatter_t424_1_0_0;
struct BinaryFormatter_t424;
const Il2CppTypeDefinitionMetadata BinaryFormatter_t424_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BinaryFormatter_t424_InterfacesTypeInfos/* implementedInterfaces */
	, BinaryFormatter_t424_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryFormatter_t424_VTable/* vtableMethods */
	, BinaryFormatter_t424_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2160/* fieldStart */

};
TypeInfo BinaryFormatter_t424_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryFormatter_t424_MethodInfos/* methods */
	, BinaryFormatter_t424_PropertyInfos/* properties */
	, NULL/* events */
	, &BinaryFormatter_t424_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 609/* custom_attributes_cache */
	, &BinaryFormatter_t424_0_0_0/* byval_arg */
	, &BinaryFormatter_t424_1_0_0/* this_arg */
	, &BinaryFormatter_t424_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryFormatter_t424)/* instance_size */
	, sizeof (BinaryFormatter_t424)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryFormatter_t424_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.CodeGenerator
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Code.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.CodeGenerator
extern TypeInfo CodeGenerator_t2028_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.CodeGenerator
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_CodeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.CodeGenerator::.cctor()
extern const MethodInfo CodeGenerator__cctor_m10802_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CodeGenerator__cctor_m10802/* method */
	, &CodeGenerator_t2028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4062/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo CodeGenerator_t2028_CodeGenerator_GenerateMetadataType_m10803_ParameterInfos[] = 
{
	{"type", 0, 134222556, 0, &Type_t_0_0_0},
	{"context", 1, 134222557, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.CodeGenerator::GenerateMetadataType(System.Type,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo CodeGenerator_GenerateMetadataType_m10803_MethodInfo = 
{
	"GenerateMetadataType"/* name */
	, (methodPointerType)&CodeGenerator_GenerateMetadataType_m10803/* method */
	, &CodeGenerator_t2028_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, CodeGenerator_t2028_CodeGenerator_GenerateMetadataType_m10803_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4063/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo CodeGenerator_t2028_CodeGenerator_GenerateMetadataTypeInternal_m10804_ParameterInfos[] = 
{
	{"type", 0, 134222558, 0, &Type_t_0_0_0},
	{"context", 1, 134222559, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.CodeGenerator::GenerateMetadataTypeInternal(System.Type,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo CodeGenerator_GenerateMetadataTypeInternal_m10804_MethodInfo = 
{
	"GenerateMetadataTypeInternal"/* name */
	, (methodPointerType)&CodeGenerator_GenerateMetadataTypeInternal_m10804/* method */
	, &CodeGenerator_t2028_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, CodeGenerator_t2028_CodeGenerator_GenerateMetadataTypeInternal_m10804_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4064/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILGenerator_t1843_0_0_0;
extern const Il2CppType ILGenerator_t1843_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo CodeGenerator_t2028_CodeGenerator_LoadFromPtr_m10805_ParameterInfos[] = 
{
	{"ig", 0, 134222560, 0, &ILGenerator_t1843_0_0_0},
	{"t", 1, 134222561, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.CodeGenerator::LoadFromPtr(System.Reflection.Emit.ILGenerator,System.Type)
extern const MethodInfo CodeGenerator_LoadFromPtr_m10805_MethodInfo = 
{
	"LoadFromPtr"/* name */
	, (methodPointerType)&CodeGenerator_LoadFromPtr_m10805/* method */
	, &CodeGenerator_t2028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, CodeGenerator_t2028_CodeGenerator_LoadFromPtr_m10805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4065/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILGenerator_t1843_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CodeGenerator_t2028_CodeGenerator_EmitWriteTypeSpec_m10806_ParameterInfos[] = 
{
	{"gen", 0, 134222562, 0, &ILGenerator_t1843_0_0_0},
	{"type", 1, 134222563, 0, &Type_t_0_0_0},
	{"member", 2, 134222564, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.CodeGenerator::EmitWriteTypeSpec(System.Reflection.Emit.ILGenerator,System.Type,System.String)
extern const MethodInfo CodeGenerator_EmitWriteTypeSpec_m10806_MethodInfo = 
{
	"EmitWriteTypeSpec"/* name */
	, (methodPointerType)&CodeGenerator_EmitWriteTypeSpec_m10806/* method */
	, &CodeGenerator_t2028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, CodeGenerator_t2028_CodeGenerator_EmitWriteTypeSpec_m10806_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4066/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILGenerator_t1843_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CodeGenerator_t2028_CodeGenerator_EmitLoadTypeAssembly_m10807_ParameterInfos[] = 
{
	{"gen", 0, 134222565, 0, &ILGenerator_t1843_0_0_0},
	{"type", 1, 134222566, 0, &Type_t_0_0_0},
	{"member", 2, 134222567, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.CodeGenerator::EmitLoadTypeAssembly(System.Reflection.Emit.ILGenerator,System.Type,System.String)
extern const MethodInfo CodeGenerator_EmitLoadTypeAssembly_m10807_MethodInfo = 
{
	"EmitLoadTypeAssembly"/* name */
	, (methodPointerType)&CodeGenerator_EmitLoadTypeAssembly_m10807/* method */
	, &CodeGenerator_t2028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, CodeGenerator_t2028_CodeGenerator_EmitLoadTypeAssembly_m10807_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4067/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILGenerator_t1843_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo CodeGenerator_t2028_CodeGenerator_EmitWrite_m10808_ParameterInfos[] = 
{
	{"gen", 0, 134222568, 0, &ILGenerator_t1843_0_0_0},
	{"type", 1, 134222569, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.CodeGenerator::EmitWrite(System.Reflection.Emit.ILGenerator,System.Type)
extern const MethodInfo CodeGenerator_EmitWrite_m10808_MethodInfo = 
{
	"EmitWrite"/* name */
	, (methodPointerType)&CodeGenerator_EmitWrite_m10808/* method */
	, &CodeGenerator_t2028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, CodeGenerator_t2028_CodeGenerator_EmitWrite_m10808_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4068/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILGenerator_t1843_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo CodeGenerator_t2028_CodeGenerator_EmitWritePrimitiveValue_m10809_ParameterInfos[] = 
{
	{"gen", 0, 134222570, 0, &ILGenerator_t1843_0_0_0},
	{"type", 1, 134222571, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.CodeGenerator::EmitWritePrimitiveValue(System.Reflection.Emit.ILGenerator,System.Type)
extern const MethodInfo CodeGenerator_EmitWritePrimitiveValue_m10809_MethodInfo = 
{
	"EmitWritePrimitiveValue"/* name */
	, (methodPointerType)&CodeGenerator_EmitWritePrimitiveValue_m10809/* method */
	, &CodeGenerator_t2028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, CodeGenerator_t2028_CodeGenerator_EmitWritePrimitiveValue_m10809_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4069/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo CodeGenerator_t2028_CodeGenerator_EnumToUnderlying_m10810_ParameterInfos[] = 
{
	{"t", 0, 134222572, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.CodeGenerator::EnumToUnderlying(System.Type)
extern const MethodInfo CodeGenerator_EnumToUnderlying_m10810_MethodInfo = 
{
	"EnumToUnderlying"/* name */
	, (methodPointerType)&CodeGenerator_EnumToUnderlying_m10810/* method */
	, &CodeGenerator_t2028_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CodeGenerator_t2028_CodeGenerator_EnumToUnderlying_m10810_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4070/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CodeGenerator_t2028_MethodInfos[] =
{
	&CodeGenerator__cctor_m10802_MethodInfo,
	&CodeGenerator_GenerateMetadataType_m10803_MethodInfo,
	&CodeGenerator_GenerateMetadataTypeInternal_m10804_MethodInfo,
	&CodeGenerator_LoadFromPtr_m10805_MethodInfo,
	&CodeGenerator_EmitWriteTypeSpec_m10806_MethodInfo,
	&CodeGenerator_EmitLoadTypeAssembly_m10807_MethodInfo,
	&CodeGenerator_EmitWrite_m10808_MethodInfo,
	&CodeGenerator_EmitWritePrimitiveValue_m10809_MethodInfo,
	&CodeGenerator_EnumToUnderlying_m10810_MethodInfo,
	NULL
};
static const Il2CppMethodReference CodeGenerator_t2028_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CodeGenerator_t2028_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CodeGenerator_t2028_0_0_0;
extern const Il2CppType CodeGenerator_t2028_1_0_0;
struct CodeGenerator_t2028;
const Il2CppTypeDefinitionMetadata CodeGenerator_t2028_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CodeGenerator_t2028_VTable/* vtableMethods */
	, CodeGenerator_t2028_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2167/* fieldStart */

};
TypeInfo CodeGenerator_t2028_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CodeGenerator"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, CodeGenerator_t2028_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CodeGenerator_t2028_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CodeGenerator_t2028_0_0_0/* byval_arg */
	, &CodeGenerator_t2028_1_0_0/* this_arg */
	, &CodeGenerator_t2028_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CodeGenerator_t2028)/* instance_size */
	, sizeof (CodeGenerator_t2028)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CodeGenerator_t2028_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Mess.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MessageFormatter
extern TypeInfo MessageFormatter_t2029_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MessMethodDeclarations.h"
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType HeaderU5BU5D_t2251_0_0_0;
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType FormatterAssemblyStyle_t2043_0_0_0;
extern const Il2CppType FormatterTypeStyle_t2044_0_0_0;
extern const Il2CppType FormatterTypeStyle_t2044_0_0_0;
static const ParameterInfo MessageFormatter_t2029_MessageFormatter_WriteMethodCall_m10811_ParameterInfos[] = 
{
	{"writer", 0, 134222573, 0, &BinaryWriter_t1806_0_0_0},
	{"obj", 1, 134222574, 0, &Object_t_0_0_0},
	{"headers", 2, 134222575, 0, &HeaderU5BU5D_t2251_0_0_0},
	{"surrogateSelector", 3, 134222576, 0, &ISurrogateSelector_t1996_0_0_0},
	{"context", 4, 134222577, 0, &StreamingContext_t1044_0_0_0},
	{"assemblyFormat", 5, 134222578, 0, &FormatterAssemblyStyle_t2043_0_0_0},
	{"typeFormat", 6, 134222579, 0, &FormatterTypeStyle_t2044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_StreamingContext_t1044_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.MessageFormatter::WriteMethodCall(System.IO.BinaryWriter,System.Object,System.Runtime.Remoting.Messaging.Header[],System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle,System.Runtime.Serialization.Formatters.FormatterTypeStyle)
extern const MethodInfo MessageFormatter_WriteMethodCall_m10811_MethodInfo = 
{
	"WriteMethodCall"/* name */
	, (methodPointerType)&MessageFormatter_WriteMethodCall_m10811/* method */
	, &MessageFormatter_t2029_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_StreamingContext_t1044_Int32_t253_Int32_t253/* invoker_method */
	, MessageFormatter_t2029_MessageFormatter_WriteMethodCall_m10811_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4071/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType HeaderU5BU5D_t2251_0_0_0;
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType FormatterAssemblyStyle_t2043_0_0_0;
extern const Il2CppType FormatterTypeStyle_t2044_0_0_0;
static const ParameterInfo MessageFormatter_t2029_MessageFormatter_WriteMethodResponse_m10812_ParameterInfos[] = 
{
	{"writer", 0, 134222580, 0, &BinaryWriter_t1806_0_0_0},
	{"obj", 1, 134222581, 0, &Object_t_0_0_0},
	{"headers", 2, 134222582, 0, &HeaderU5BU5D_t2251_0_0_0},
	{"surrogateSelector", 3, 134222583, 0, &ISurrogateSelector_t1996_0_0_0},
	{"context", 4, 134222584, 0, &StreamingContext_t1044_0_0_0},
	{"assemblyFormat", 5, 134222585, 0, &FormatterAssemblyStyle_t2043_0_0_0},
	{"typeFormat", 6, 134222586, 0, &FormatterTypeStyle_t2044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_StreamingContext_t1044_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.MessageFormatter::WriteMethodResponse(System.IO.BinaryWriter,System.Object,System.Runtime.Remoting.Messaging.Header[],System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle,System.Runtime.Serialization.Formatters.FormatterTypeStyle)
extern const MethodInfo MessageFormatter_WriteMethodResponse_m10812_MethodInfo = 
{
	"WriteMethodResponse"/* name */
	, (methodPointerType)&MessageFormatter_WriteMethodResponse_m10812/* method */
	, &MessageFormatter_t2029_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_StreamingContext_t1044_Int32_t253_Int32_t253/* invoker_method */
	, MessageFormatter_t2029_MessageFormatter_WriteMethodResponse_m10812_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4072/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t2023_0_0_0;
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType HeaderHandler_t2252_0_0_0;
extern const Il2CppType BinaryFormatter_t424_0_0_0;
static const ParameterInfo MessageFormatter_t2029_MessageFormatter_ReadMethodCall_m10813_ParameterInfos[] = 
{
	{"elem", 0, 134222587, 0, &BinaryElement_t2023_0_0_0},
	{"reader", 1, 134222588, 0, &BinaryReader_t1805_0_0_0},
	{"hasHeaders", 2, 134222589, 0, &Boolean_t273_0_0_0},
	{"headerHandler", 3, 134222590, 0, &HeaderHandler_t2252_0_0_0},
	{"formatter", 4, 134222591, 0, &BinaryFormatter_t424_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Byte_t680_Object_t_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodCall(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern const MethodInfo MessageFormatter_ReadMethodCall_m10813_MethodInfo = 
{
	"ReadMethodCall"/* name */
	, (methodPointerType)&MessageFormatter_ReadMethodCall_m10813/* method */
	, &MessageFormatter_t2029_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Byte_t680_Object_t_SByte_t274_Object_t_Object_t/* invoker_method */
	, MessageFormatter_t2029_MessageFormatter_ReadMethodCall_m10813_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4073/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t2023_0_0_0;
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType HeaderHandler_t2252_0_0_0;
extern const Il2CppType IMethodCallMessage_t2289_0_0_0;
extern const Il2CppType IMethodCallMessage_t2289_0_0_0;
extern const Il2CppType BinaryFormatter_t424_0_0_0;
static const ParameterInfo MessageFormatter_t2029_MessageFormatter_ReadMethodResponse_m10814_ParameterInfos[] = 
{
	{"elem", 0, 134222592, 0, &BinaryElement_t2023_0_0_0},
	{"reader", 1, 134222593, 0, &BinaryReader_t1805_0_0_0},
	{"hasHeaders", 2, 134222594, 0, &Boolean_t273_0_0_0},
	{"headerHandler", 3, 134222595, 0, &HeaderHandler_t2252_0_0_0},
	{"methodCallMessage", 4, 134222596, 0, &IMethodCallMessage_t2289_0_0_0},
	{"formatter", 5, 134222597, 0, &BinaryFormatter_t424_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Byte_t680_Object_t_SByte_t274_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodResponse(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Remoting.Messaging.IMethodCallMessage,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern const MethodInfo MessageFormatter_ReadMethodResponse_m10814_MethodInfo = 
{
	"ReadMethodResponse"/* name */
	, (methodPointerType)&MessageFormatter_ReadMethodResponse_m10814/* method */
	, &MessageFormatter_t2029_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Byte_t680_Object_t_SByte_t274_Object_t_Object_t_Object_t/* invoker_method */
	, MessageFormatter_t2029_MessageFormatter_ReadMethodResponse_m10814_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4074/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo MessageFormatter_t2029_MessageFormatter_AllTypesArePrimitive_m10815_ParameterInfos[] = 
{
	{"objects", 0, 134222598, 0, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.MessageFormatter::AllTypesArePrimitive(System.Object[])
extern const MethodInfo MessageFormatter_AllTypesArePrimitive_m10815_MethodInfo = 
{
	"AllTypesArePrimitive"/* name */
	, (methodPointerType)&MessageFormatter_AllTypesArePrimitive_m10815/* method */
	, &MessageFormatter_t2029_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, MessageFormatter_t2029_MessageFormatter_AllTypesArePrimitive_m10815_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4075/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MessageFormatter_t2029_MessageFormatter_IsMethodPrimitive_m10816_ParameterInfos[] = 
{
	{"type", 0, 134222599, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.MessageFormatter::IsMethodPrimitive(System.Type)
extern const MethodInfo MessageFormatter_IsMethodPrimitive_m10816_MethodInfo = 
{
	"IsMethodPrimitive"/* name */
	, (methodPointerType)&MessageFormatter_IsMethodPrimitive_m10816/* method */
	, &MessageFormatter_t2029_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, MessageFormatter_t2029_MessageFormatter_IsMethodPrimitive_m10816_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4076/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t444_0_0_0;
extern const Il2CppType IDictionary_t444_0_0_0;
extern const Il2CppType StringU5BU5D_t243_0_0_0;
extern const Il2CppType StringU5BU5D_t243_0_0_0;
static const ParameterInfo MessageFormatter_t2029_MessageFormatter_GetExtraProperties_m10817_ParameterInfos[] = 
{
	{"properties", 0, 134222600, 0, &IDictionary_t444_0_0_0},
	{"internalKeys", 1, 134222601, 0, &StringU5BU5D_t243_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Serialization.Formatters.Binary.MessageFormatter::GetExtraProperties(System.Collections.IDictionary,System.String[])
extern const MethodInfo MessageFormatter_GetExtraProperties_m10817_MethodInfo = 
{
	"GetExtraProperties"/* name */
	, (methodPointerType)&MessageFormatter_GetExtraProperties_m10817/* method */
	, &MessageFormatter_t2029_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MessageFormatter_t2029_MessageFormatter_GetExtraProperties_m10817_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4077/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType StringU5BU5D_t243_0_0_0;
static const ParameterInfo MessageFormatter_t2029_MessageFormatter_IsInternalKey_m10818_ParameterInfos[] = 
{
	{"key", 0, 134222602, 0, &String_t_0_0_0},
	{"internalKeys", 1, 134222603, 0, &StringU5BU5D_t243_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.MessageFormatter::IsInternalKey(System.String,System.String[])
extern const MethodInfo MessageFormatter_IsInternalKey_m10818_MethodInfo = 
{
	"IsInternalKey"/* name */
	, (methodPointerType)&MessageFormatter_IsInternalKey_m10818/* method */
	, &MessageFormatter_t2029_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, MessageFormatter_t2029_MessageFormatter_IsInternalKey_m10818_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4078/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MessageFormatter_t2029_MethodInfos[] =
{
	&MessageFormatter_WriteMethodCall_m10811_MethodInfo,
	&MessageFormatter_WriteMethodResponse_m10812_MethodInfo,
	&MessageFormatter_ReadMethodCall_m10813_MethodInfo,
	&MessageFormatter_ReadMethodResponse_m10814_MethodInfo,
	&MessageFormatter_AllTypesArePrimitive_m10815_MethodInfo,
	&MessageFormatter_IsMethodPrimitive_m10816_MethodInfo,
	&MessageFormatter_GetExtraProperties_m10817_MethodInfo,
	&MessageFormatter_IsInternalKey_m10818_MethodInfo,
	NULL
};
static const Il2CppMethodReference MessageFormatter_t2029_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool MessageFormatter_t2029_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MessageFormatter_t2029_0_0_0;
extern const Il2CppType MessageFormatter_t2029_1_0_0;
struct MessageFormatter_t2029;
const Il2CppTypeDefinitionMetadata MessageFormatter_t2029_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MessageFormatter_t2029_VTable/* vtableMethods */
	, MessageFormatter_t2029_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MessageFormatter_t2029_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MessageFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, MessageFormatter_t2029_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MessageFormatter_t2029_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MessageFormatter_t2029_0_0_0/* byval_arg */
	, &MessageFormatter_t2029_1_0_0/* this_arg */
	, &MessageFormatter_t2029_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MessageFormatter_t2029)/* instance_size */
	, sizeof (MessageFormatter_t2029)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
extern TypeInfo TypeMetadata_t2031_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_ObjeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata::.ctor()
extern const MethodInfo TypeMetadata__ctor_m10819_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeMetadata__ctor_m10819/* method */
	, &TypeMetadata_t2031_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeMetadata_t2031_MethodInfos[] =
{
	&TypeMetadata__ctor_m10819_MethodInfo,
	NULL
};
static const Il2CppMethodReference TypeMetadata_t2031_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool TypeMetadata_t2031_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeMetadata_t2031_0_0_0;
extern const Il2CppType TypeMetadata_t2031_1_0_0;
extern TypeInfo ObjectReader_t2034_il2cpp_TypeInfo;
extern const Il2CppType ObjectReader_t2034_0_0_0;
struct TypeMetadata_t2031;
const Il2CppTypeDefinitionMetadata TypeMetadata_t2031_DefinitionMetadata = 
{
	&ObjectReader_t2034_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeMetadata_t2031_VTable/* vtableMethods */
	, TypeMetadata_t2031_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2169/* fieldStart */

};
TypeInfo TypeMetadata_t2031_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeMetadata"/* name */
	, ""/* namespaze */
	, TypeMetadata_t2031_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeMetadata_t2031_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeMetadata_t2031_0_0_0/* byval_arg */
	, &TypeMetadata_t2031_1_0_0/* this_arg */
	, &TypeMetadata_t2031_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeMetadata_t2031)/* instance_size */
	, sizeof (TypeMetadata_t2031)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
extern TypeInfo ArrayNullFiller_t2032_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0MethodDeclarations.h"
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ArrayNullFiller_t2032_ArrayNullFiller__ctor_m10820_ParameterInfos[] = 
{
	{"count", 0, 134222698, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller::.ctor(System.Int32)
extern const MethodInfo ArrayNullFiller__ctor_m10820_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArrayNullFiller__ctor_m10820/* method */
	, &ArrayNullFiller_t2032_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ArrayNullFiller_t2032_ArrayNullFiller__ctor_m10820_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ArrayNullFiller_t2032_MethodInfos[] =
{
	&ArrayNullFiller__ctor_m10820_MethodInfo,
	NULL
};
static const Il2CppMethodReference ArrayNullFiller_t2032_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ArrayNullFiller_t2032_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayNullFiller_t2032_0_0_0;
extern const Il2CppType ArrayNullFiller_t2032_1_0_0;
struct ArrayNullFiller_t2032;
const Il2CppTypeDefinitionMetadata ArrayNullFiller_t2032_DefinitionMetadata = 
{
	&ObjectReader_t2034_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArrayNullFiller_t2032_VTable/* vtableMethods */
	, ArrayNullFiller_t2032_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2175/* fieldStart */

};
TypeInfo ArrayNullFiller_t2032_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayNullFiller"/* name */
	, ""/* namespaze */
	, ArrayNullFiller_t2032_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ArrayNullFiller_t2032_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArrayNullFiller_t2032_0_0_0/* byval_arg */
	, &ArrayNullFiller_t2032_1_0_0/* this_arg */
	, &ArrayNullFiller_t2032_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayNullFiller_t2032)/* instance_size */
	, sizeof (ArrayNullFiller_t2032)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1MethodDeclarations.h"
extern const Il2CppType BinaryFormatter_t424_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader__ctor_m10821_ParameterInfos[] = 
{
	{"formatter", 0, 134222604, 0, &BinaryFormatter_t424_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::.ctor(System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern const MethodInfo ObjectReader__ctor_m10821_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectReader__ctor_m10821/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjectReader_t2034_ObjectReader__ctor_m10821_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4079/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType HeaderU5BU5D_t2251_1_0_2;
extern const Il2CppType HeaderU5BU5D_t2251_1_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadObjectGraph_m10822_ParameterInfos[] = 
{
	{"reader", 0, 134222605, 0, &BinaryReader_t1805_0_0_0},
	{"readHeaders", 1, 134222606, 0, &Boolean_t273_0_0_0},
	{"result", 2, 134222607, 0, &Object_t_1_0_2},
	{"headers", 3, 134222608, 0, &HeaderU5BU5D_t2251_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_ObjectU26_t1197_HeaderU5BU5DU26_t2760 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectGraph(System.IO.BinaryReader,System.Boolean,System.Object&,System.Runtime.Remoting.Messaging.Header[]&)
extern const MethodInfo ObjectReader_ReadObjectGraph_m10822_MethodInfo = 
{
	"ReadObjectGraph"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectGraph_m10822/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274_ObjectU26_t1197_HeaderU5BU5DU26_t2760/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadObjectGraph_m10822_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4080/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t2023_0_0_0;
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType HeaderU5BU5D_t2251_1_0_2;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadObjectGraph_m10823_ParameterInfos[] = 
{
	{"elem", 0, 134222609, 0, &BinaryElement_t2023_0_0_0},
	{"reader", 1, 134222610, 0, &BinaryReader_t1805_0_0_0},
	{"readHeaders", 2, 134222611, 0, &Boolean_t273_0_0_0},
	{"result", 3, 134222612, 0, &Object_t_1_0_2},
	{"headers", 4, 134222613, 0, &HeaderU5BU5D_t2251_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Byte_t680_Object_t_SByte_t274_ObjectU26_t1197_HeaderU5BU5DU26_t2760 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectGraph(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Object&,System.Runtime.Remoting.Messaging.Header[]&)
extern const MethodInfo ObjectReader_ReadObjectGraph_m10823_MethodInfo = 
{
	"ReadObjectGraph"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectGraph_m10823/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Byte_t680_Object_t_SByte_t274_ObjectU26_t1197_HeaderU5BU5DU26_t2760/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadObjectGraph_m10823_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4081/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t2023_0_0_0;
extern const Il2CppType BinaryReader_t1805_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadNextObject_m10824_ParameterInfos[] = 
{
	{"element", 0, 134222614, 0, &BinaryElement_t2023_0_0_0},
	{"reader", 1, 134222615, 0, &BinaryReader_t1805_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Byte_t680_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadNextObject(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadNextObject_m10824_MethodInfo = 
{
	"ReadNextObject"/* name */
	, (methodPointerType)&ObjectReader_ReadNextObject_m10824/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Byte_t680_Object_t/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadNextObject_m10824_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4082/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadNextObject_m10825_ParameterInfos[] = 
{
	{"reader", 0, 134222616, 0, &BinaryReader_t1805_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadNextObject(System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadNextObject_m10825_MethodInfo = 
{
	"ReadNextObject"/* name */
	, (methodPointerType)&ObjectReader_ReadNextObject_m10825/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadNextObject_m10825_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4083/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::get_CurrentObject()
extern const MethodInfo ObjectReader_get_CurrentObject_m10826_MethodInfo = 
{
	"get_CurrentObject"/* name */
	, (methodPointerType)&ObjectReader_get_CurrentObject_m10826/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4084/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t2023_0_0_0;
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Int64_t1071_1_0_2;
extern const Il2CppType Int64_t1071_1_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t1043_1_0_2;
extern const Il2CppType SerializationInfo_t1043_1_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadObject_m10827_ParameterInfos[] = 
{
	{"element", 0, 134222617, 0, &BinaryElement_t2023_0_0_0},
	{"reader", 1, 134222618, 0, &BinaryReader_t1805_0_0_0},
	{"objectId", 2, 134222619, 0, &Int64_t1071_1_0_2},
	{"value", 3, 134222620, 0, &Object_t_1_0_2},
	{"info", 4, 134222621, 0, &SerializationInfo_t1043_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Byte_t680_Object_t_Int64U26_t2404_ObjectU26_t1197_SerializationInfoU26_t2761 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObject(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadObject_m10827_MethodInfo = 
{
	"ReadObject"/* name */
	, (methodPointerType)&ObjectReader_ReadObject_m10827/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Byte_t680_Object_t_Int64U26_t2404_ObjectU26_t1197_SerializationInfoU26_t2761/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadObject_m10827_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4085/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadAssembly_m10828_ParameterInfos[] = 
{
	{"reader", 0, 134222622, 0, &BinaryReader_t1805_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadAssembly(System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadAssembly_m10828_MethodInfo = 
{
	"ReadAssembly"/* name */
	, (methodPointerType)&ObjectReader_ReadAssembly_m10828/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadAssembly_m10828_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4086/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Int64_t1071_1_0_2;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t1043_1_0_2;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadObjectInstance_m10829_ParameterInfos[] = 
{
	{"reader", 0, 134222623, 0, &BinaryReader_t1805_0_0_0},
	{"isRuntimeObject", 1, 134222624, 0, &Boolean_t273_0_0_0},
	{"hasTypeInfo", 2, 134222625, 0, &Boolean_t273_0_0_0},
	{"objectId", 3, 134222626, 0, &Int64_t1071_1_0_2},
	{"value", 4, 134222627, 0, &Object_t_1_0_2},
	{"info", 5, 134222628, 0, &SerializationInfo_t1043_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274_Int64U26_t2404_ObjectU26_t1197_SerializationInfoU26_t2761 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectInstance(System.IO.BinaryReader,System.Boolean,System.Boolean,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadObjectInstance_m10829_MethodInfo = 
{
	"ReadObjectInstance"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectInstance_m10829/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274_SByte_t274_Int64U26_t2404_ObjectU26_t1197_SerializationInfoU26_t2761/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadObjectInstance_m10829_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4087/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Int64_t1071_1_0_2;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t1043_1_0_2;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadRefTypeObjectInstance_m10830_ParameterInfos[] = 
{
	{"reader", 0, 134222629, 0, &BinaryReader_t1805_0_0_0},
	{"objectId", 1, 134222630, 0, &Int64_t1071_1_0_2},
	{"value", 2, 134222631, 0, &Object_t_1_0_2},
	{"info", 3, 134222632, 0, &SerializationInfo_t1043_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197_SerializationInfoU26_t2761 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadRefTypeObjectInstance(System.IO.BinaryReader,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadRefTypeObjectInstance_m10830_MethodInfo = 
{
	"ReadRefTypeObjectInstance"/* name */
	, (methodPointerType)&ObjectReader_ReadRefTypeObjectInstance_m10830/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197_SerializationInfoU26_t2761/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadRefTypeObjectInstance_m10830_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4088/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType TypeMetadata_t2031_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t1043_1_0_2;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadObjectContent_m10831_ParameterInfos[] = 
{
	{"reader", 0, 134222633, 0, &BinaryReader_t1805_0_0_0},
	{"metadata", 1, 134222634, 0, &TypeMetadata_t2031_0_0_0},
	{"objectId", 2, 134222635, 0, &Int64_t1071_0_0_0},
	{"objectInstance", 3, 134222636, 0, &Object_t_1_0_2},
	{"info", 4, 134222637, 0, &SerializationInfo_t1043_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int64_t1071_ObjectU26_t1197_SerializationInfoU26_t2761 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectContent(System.IO.BinaryReader,System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata,System.Int64,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadObjectContent_m10831_MethodInfo = 
{
	"ReadObjectContent"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectContent_m10831/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int64_t1071_ObjectU26_t1197_SerializationInfoU26_t2761/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadObjectContent_m10831_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4089/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_RegisterObject_m10832_ParameterInfos[] = 
{
	{"objectId", 0, 134222638, 0, &Int64_t1071_0_0_0},
	{"objectInstance", 1, 134222639, 0, &Object_t_0_0_0},
	{"info", 2, 134222640, 0, &SerializationInfo_t1043_0_0_0},
	{"parentObjectId", 3, 134222641, 0, &Int64_t1071_0_0_0},
	{"parentObjectMemeber", 4, 134222642, 0, &MemberInfo_t_0_0_0},
	{"indices", 5, 134222643, 0, &Int32U5BU5D_t242_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Object_t_Int64_t1071_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::RegisterObject(System.Int64,System.Object,System.Runtime.Serialization.SerializationInfo,System.Int64,System.Reflection.MemberInfo,System.Int32[])
extern const MethodInfo ObjectReader_RegisterObject_m10832_MethodInfo = 
{
	"RegisterObject"/* name */
	, (methodPointerType)&ObjectReader_RegisterObject_m10832/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Object_t_Int64_t1071_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2034_ObjectReader_RegisterObject_m10832_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4090/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Int64_t1071_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadStringIntance_m10833_ParameterInfos[] = 
{
	{"reader", 0, 134222644, 0, &BinaryReader_t1805_0_0_0},
	{"objectId", 1, 134222645, 0, &Int64_t1071_1_0_2},
	{"value", 2, 134222646, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadStringIntance(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadStringIntance_m10833_MethodInfo = 
{
	"ReadStringIntance"/* name */
	, (methodPointerType)&ObjectReader_ReadStringIntance_m10833/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadStringIntance_m10833_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4091/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Int64_t1071_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadGenericArray_m10834_ParameterInfos[] = 
{
	{"reader", 0, 134222647, 0, &BinaryReader_t1805_0_0_0},
	{"objectId", 1, 134222648, 0, &Int64_t1071_1_0_2},
	{"val", 2, 134222649, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadGenericArray(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadGenericArray_m10834_MethodInfo = 
{
	"ReadGenericArray"/* name */
	, (methodPointerType)&ObjectReader_ReadGenericArray_m10834/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadGenericArray_m10834_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4092/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadBoxedPrimitiveTypeValue_m10835_ParameterInfos[] = 
{
	{"reader", 0, 134222650, 0, &BinaryReader_t1805_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadBoxedPrimitiveTypeValue(System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadBoxedPrimitiveTypeValue_m10835_MethodInfo = 
{
	"ReadBoxedPrimitiveTypeValue"/* name */
	, (methodPointerType)&ObjectReader_ReadBoxedPrimitiveTypeValue_m10835/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadBoxedPrimitiveTypeValue_m10835_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4093/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Int64_t1071_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadArrayOfPrimitiveType_m10836_ParameterInfos[] = 
{
	{"reader", 0, 134222651, 0, &BinaryReader_t1805_0_0_0},
	{"objectId", 1, 134222652, 0, &Int64_t1071_1_0_2},
	{"val", 2, 134222653, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfPrimitiveType(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadArrayOfPrimitiveType_m10836_MethodInfo = 
{
	"ReadArrayOfPrimitiveType"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfPrimitiveType_m10836/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadArrayOfPrimitiveType_m10836_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4094/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_BlockRead_m10837_ParameterInfos[] = 
{
	{"reader", 0, 134222654, 0, &BinaryReader_t1805_0_0_0},
	{"array", 1, 134222655, 0, &Array_t_0_0_0},
	{"dataSize", 2, 134222656, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::BlockRead(System.IO.BinaryReader,System.Array,System.Int32)
extern const MethodInfo ObjectReader_BlockRead_m10837_MethodInfo = 
{
	"BlockRead"/* name */
	, (methodPointerType)&ObjectReader_BlockRead_m10837/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253/* invoker_method */
	, ObjectReader_t2034_ObjectReader_BlockRead_m10837_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4095/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Int64_t1071_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadArrayOfObject_m10838_ParameterInfos[] = 
{
	{"reader", 0, 134222657, 0, &BinaryReader_t1805_0_0_0},
	{"objectId", 1, 134222658, 0, &Int64_t1071_1_0_2},
	{"array", 2, 134222659, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfObject(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadArrayOfObject_m10838_MethodInfo = 
{
	"ReadArrayOfObject"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfObject_m10838/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadArrayOfObject_m10838_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4096/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Int64_t1071_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadArrayOfString_m10839_ParameterInfos[] = 
{
	{"reader", 0, 134222660, 0, &BinaryReader_t1805_0_0_0},
	{"objectId", 1, 134222661, 0, &Int64_t1071_1_0_2},
	{"array", 2, 134222662, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfString(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadArrayOfString_m10839_MethodInfo = 
{
	"ReadArrayOfString"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfString_m10839/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64U26_t2404_ObjectU26_t1197/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadArrayOfString_m10839_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4097/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Int64_t1071_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadSimpleArray_m10840_ParameterInfos[] = 
{
	{"reader", 0, 134222663, 0, &BinaryReader_t1805_0_0_0},
	{"elementType", 1, 134222664, 0, &Type_t_0_0_0},
	{"objectId", 2, 134222665, 0, &Int64_t1071_1_0_2},
	{"val", 3, 134222666, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int64U26_t2404_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadSimpleArray(System.IO.BinaryReader,System.Type,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadSimpleArray_m10840_MethodInfo = 
{
	"ReadSimpleArray"/* name */
	, (methodPointerType)&ObjectReader_ReadSimpleArray_m10840/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int64U26_t2404_ObjectU26_t1197/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadSimpleArray_m10840_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4098/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadTypeMetadata_m10841_ParameterInfos[] = 
{
	{"reader", 0, 134222667, 0, &BinaryReader_t1805_0_0_0},
	{"isRuntimeObject", 1, 134222668, 0, &Boolean_t273_0_0_0},
	{"hasTypeInfo", 2, 134222669, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadTypeMetadata(System.IO.BinaryReader,System.Boolean,System.Boolean)
extern const MethodInfo ObjectReader_ReadTypeMetadata_m10841_MethodInfo = 
{
	"ReadTypeMetadata"/* name */
	, (methodPointerType)&ObjectReader_ReadTypeMetadata_m10841/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &TypeMetadata_t2031_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274_SByte_t274/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadTypeMetadata_m10841_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4099/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadValue_m10842_ParameterInfos[] = 
{
	{"reader", 0, 134222670, 0, &BinaryReader_t1805_0_0_0},
	{"parentObject", 1, 134222671, 0, &Object_t_0_0_0},
	{"parentObjectId", 2, 134222672, 0, &Int64_t1071_0_0_0},
	{"info", 3, 134222673, 0, &SerializationInfo_t1043_0_0_0},
	{"valueType", 4, 134222674, 0, &Type_t_0_0_0},
	{"fieldName", 5, 134222675, 0, &String_t_0_0_0},
	{"memberInfo", 6, 134222676, 0, &MemberInfo_t_0_0_0},
	{"indices", 7, 134222677, 0, &Int32U5BU5D_t242_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int64_t1071_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadValue(System.IO.BinaryReader,System.Object,System.Int64,System.Runtime.Serialization.SerializationInfo,System.Type,System.String,System.Reflection.MemberInfo,System.Int32[])
extern const MethodInfo ObjectReader_ReadValue_m10842_MethodInfo = 
{
	"ReadValue"/* name */
	, (methodPointerType)&ObjectReader_ReadValue_m10842/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int64_t1071_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadValue_m10842_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 8/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_SetObjectValue_m10843_ParameterInfos[] = 
{
	{"parentObject", 0, 134222678, 0, &Object_t_0_0_0},
	{"fieldName", 1, 134222679, 0, &String_t_0_0_0},
	{"memberInfo", 2, 134222680, 0, &MemberInfo_t_0_0_0},
	{"info", 3, 134222681, 0, &SerializationInfo_t1043_0_0_0},
	{"value", 4, 134222682, 0, &Object_t_0_0_0},
	{"valueType", 5, 134222683, 0, &Type_t_0_0_0},
	{"indices", 6, 134222684, 0, &Int32U5BU5D_t242_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::SetObjectValue(System.Object,System.String,System.Reflection.MemberInfo,System.Runtime.Serialization.SerializationInfo,System.Object,System.Type,System.Int32[])
extern const MethodInfo ObjectReader_SetObjectValue_m10843_MethodInfo = 
{
	"SetObjectValue"/* name */
	, (methodPointerType)&ObjectReader_SetObjectValue_m10843/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2034_ObjectReader_SetObjectValue_m10843_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_RecordFixup_m10844_ParameterInfos[] = 
{
	{"parentObjectId", 0, 134222685, 0, &Int64_t1071_0_0_0},
	{"childObjectId", 1, 134222686, 0, &Int64_t1071_0_0_0},
	{"parentObject", 2, 134222687, 0, &Object_t_0_0_0},
	{"info", 3, 134222688, 0, &SerializationInfo_t1043_0_0_0},
	{"fieldName", 4, 134222689, 0, &String_t_0_0_0},
	{"memberInfo", 5, 134222690, 0, &MemberInfo_t_0_0_0},
	{"indices", 6, 134222691, 0, &Int32U5BU5D_t242_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int64_t1071_Int64_t1071_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::RecordFixup(System.Int64,System.Int64,System.Object,System.Runtime.Serialization.SerializationInfo,System.String,System.Reflection.MemberInfo,System.Int32[])
extern const MethodInfo ObjectReader_RecordFixup_m10844_MethodInfo = 
{
	"RecordFixup"/* name */
	, (methodPointerType)&ObjectReader_RecordFixup_m10844/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int64_t1071_Int64_t1071_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2034_ObjectReader_RecordFixup_m10844_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_GetDeserializationType_m10845_ParameterInfos[] = 
{
	{"assemblyId", 0, 134222692, 0, &Int64_t1071_0_0_0},
	{"className", 1, 134222693, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectReader::GetDeserializationType(System.Int64,System.String)
extern const MethodInfo ObjectReader_GetDeserializationType_m10845_MethodInfo = 
{
	"GetDeserializationType"/* name */
	, (methodPointerType)&ObjectReader_GetDeserializationType_m10845/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t1071_Object_t/* invoker_method */
	, ObjectReader_t2034_ObjectReader_GetDeserializationType_m10845_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType TypeTag_t2024_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadType_m10846_ParameterInfos[] = 
{
	{"reader", 0, 134222694, 0, &BinaryReader_t1805_0_0_0},
	{"code", 1, 134222695, 0, &TypeTag_t2024_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Byte_t680 (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadType(System.IO.BinaryReader,System.Runtime.Serialization.Formatters.Binary.TypeTag)
extern const MethodInfo ObjectReader_ReadType_m10846_MethodInfo = 
{
	"ReadType"/* name */
	, (methodPointerType)&ObjectReader_ReadType_m10846/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Byte_t680/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadType_m10846_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1805_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ObjectReader_t2034_ObjectReader_ReadPrimitiveTypeValue_m10847_ParameterInfos[] = 
{
	{"reader", 0, 134222696, 0, &BinaryReader_t1805_0_0_0},
	{"type", 1, 134222697, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadPrimitiveTypeValue(System.IO.BinaryReader,System.Type)
extern const MethodInfo ObjectReader_ReadPrimitiveTypeValue_m10847_MethodInfo = 
{
	"ReadPrimitiveTypeValue"/* name */
	, (methodPointerType)&ObjectReader_ReadPrimitiveTypeValue_m10847/* method */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2034_ObjectReader_ReadPrimitiveTypeValue_m10847_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectReader_t2034_MethodInfos[] =
{
	&ObjectReader__ctor_m10821_MethodInfo,
	&ObjectReader_ReadObjectGraph_m10822_MethodInfo,
	&ObjectReader_ReadObjectGraph_m10823_MethodInfo,
	&ObjectReader_ReadNextObject_m10824_MethodInfo,
	&ObjectReader_ReadNextObject_m10825_MethodInfo,
	&ObjectReader_get_CurrentObject_m10826_MethodInfo,
	&ObjectReader_ReadObject_m10827_MethodInfo,
	&ObjectReader_ReadAssembly_m10828_MethodInfo,
	&ObjectReader_ReadObjectInstance_m10829_MethodInfo,
	&ObjectReader_ReadRefTypeObjectInstance_m10830_MethodInfo,
	&ObjectReader_ReadObjectContent_m10831_MethodInfo,
	&ObjectReader_RegisterObject_m10832_MethodInfo,
	&ObjectReader_ReadStringIntance_m10833_MethodInfo,
	&ObjectReader_ReadGenericArray_m10834_MethodInfo,
	&ObjectReader_ReadBoxedPrimitiveTypeValue_m10835_MethodInfo,
	&ObjectReader_ReadArrayOfPrimitiveType_m10836_MethodInfo,
	&ObjectReader_BlockRead_m10837_MethodInfo,
	&ObjectReader_ReadArrayOfObject_m10838_MethodInfo,
	&ObjectReader_ReadArrayOfString_m10839_MethodInfo,
	&ObjectReader_ReadSimpleArray_m10840_MethodInfo,
	&ObjectReader_ReadTypeMetadata_m10841_MethodInfo,
	&ObjectReader_ReadValue_m10842_MethodInfo,
	&ObjectReader_SetObjectValue_m10843_MethodInfo,
	&ObjectReader_RecordFixup_m10844_MethodInfo,
	&ObjectReader_GetDeserializationType_m10845_MethodInfo,
	&ObjectReader_ReadType_m10846_MethodInfo,
	&ObjectReader_ReadPrimitiveTypeValue_m10847_MethodInfo,
	NULL
};
extern const MethodInfo ObjectReader_get_CurrentObject_m10826_MethodInfo;
static const PropertyInfo ObjectReader_t2034____CurrentObject_PropertyInfo = 
{
	&ObjectReader_t2034_il2cpp_TypeInfo/* parent */
	, "CurrentObject"/* name */
	, &ObjectReader_get_CurrentObject_m10826_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectReader_t2034_PropertyInfos[] =
{
	&ObjectReader_t2034____CurrentObject_PropertyInfo,
	NULL
};
static const Il2CppType* ObjectReader_t2034_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TypeMetadata_t2031_0_0_0,
	&ArrayNullFiller_t2032_0_0_0,
};
static const Il2CppMethodReference ObjectReader_t2034_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ObjectReader_t2034_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectReader_t2034_1_0_0;
struct ObjectReader_t2034;
const Il2CppTypeDefinitionMetadata ObjectReader_t2034_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ObjectReader_t2034_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectReader_t2034_VTable/* vtableMethods */
	, ObjectReader_t2034_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2176/* fieldStart */

};
TypeInfo ObjectReader_t2034_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectReader"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, ObjectReader_t2034_MethodInfos/* methods */
	, ObjectReader_t2034_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectReader_t2034_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectReader_t2034_0_0_0/* byval_arg */
	, &ObjectReader_t2034_1_0_0/* this_arg */
	, &ObjectReader_t2034_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectReader_t2034)/* instance_size */
	, sizeof (ObjectReader_t2034)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.TypeMetadata
extern TypeInfo TypeMetadata_t2035_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.TypeMetadata::.ctor()
extern const MethodInfo TypeMetadata__ctor_m10848_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeMetadata__ctor_m10848/* method */
	, &TypeMetadata_t2035_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectWriter_t2042_0_0_0;
extern const Il2CppType ObjectWriter_t2042_0_0_0;
extern const Il2CppType BinaryWriter_t1806_0_0_0;
static const ParameterInfo TypeMetadata_t2035_TypeMetadata_WriteAssemblies_m13227_ParameterInfos[] = 
{
	{"ow", 0, 134222699, 0, &ObjectWriter_t2042_0_0_0},
	{"writer", 1, 134222700, 0, &BinaryWriter_t1806_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.TypeMetadata::WriteAssemblies(System.Runtime.Serialization.Formatters.Binary.ObjectWriter,System.IO.BinaryWriter)
extern const MethodInfo TypeMetadata_WriteAssemblies_m13227_MethodInfo = 
{
	"WriteAssemblies"/* name */
	, NULL/* method */
	, &TypeMetadata_t2035_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, TypeMetadata_t2035_TypeMetadata_WriteAssemblies_m13227_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectWriter_t2042_0_0_0;
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo TypeMetadata_t2035_TypeMetadata_WriteTypeData_m13228_ParameterInfos[] = 
{
	{"ow", 0, 134222701, 0, &ObjectWriter_t2042_0_0_0},
	{"writer", 1, 134222702, 0, &BinaryWriter_t1806_0_0_0},
	{"writeTypes", 2, 134222703, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.TypeMetadata::WriteTypeData(System.Runtime.Serialization.Formatters.Binary.ObjectWriter,System.IO.BinaryWriter,System.Boolean)
extern const MethodInfo TypeMetadata_WriteTypeData_m13228_MethodInfo = 
{
	"WriteTypeData"/* name */
	, NULL/* method */
	, &TypeMetadata_t2035_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274/* invoker_method */
	, TypeMetadata_t2035_TypeMetadata_WriteTypeData_m13228_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectWriter_t2042_0_0_0;
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TypeMetadata_t2035_TypeMetadata_WriteObjectData_m13229_ParameterInfos[] = 
{
	{"ow", 0, 134222704, 0, &ObjectWriter_t2042_0_0_0},
	{"writer", 1, 134222705, 0, &BinaryWriter_t1806_0_0_0},
	{"data", 2, 134222706, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.TypeMetadata::WriteObjectData(System.Runtime.Serialization.Formatters.Binary.ObjectWriter,System.IO.BinaryWriter,System.Object)
extern const MethodInfo TypeMetadata_WriteObjectData_m13229_MethodInfo = 
{
	"WriteObjectData"/* name */
	, NULL/* method */
	, &TypeMetadata_t2035_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, TypeMetadata_t2035_TypeMetadata_WriteObjectData_m13229_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeMetadata_t2035_0_0_0;
extern const Il2CppType TypeMetadata_t2035_0_0_0;
static const ParameterInfo TypeMetadata_t2035_TypeMetadata_IsCompatible_m10849_ParameterInfos[] = 
{
	{"other", 0, 134222707, 0, &TypeMetadata_t2035_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.TypeMetadata::IsCompatible(System.Runtime.Serialization.Formatters.Binary.TypeMetadata)
extern const MethodInfo TypeMetadata_IsCompatible_m10849_MethodInfo = 
{
	"IsCompatible"/* name */
	, (methodPointerType)&TypeMetadata_IsCompatible_m10849/* method */
	, &TypeMetadata_t2035_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, TypeMetadata_t2035_TypeMetadata_IsCompatible_m10849_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.TypeMetadata::get_RequiresTypes()
extern const MethodInfo TypeMetadata_get_RequiresTypes_m13230_MethodInfo = 
{
	"get_RequiresTypes"/* name */
	, NULL/* method */
	, &TypeMetadata_t2035_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeMetadata_t2035_MethodInfos[] =
{
	&TypeMetadata__ctor_m10848_MethodInfo,
	&TypeMetadata_WriteAssemblies_m13227_MethodInfo,
	&TypeMetadata_WriteTypeData_m13228_MethodInfo,
	&TypeMetadata_WriteObjectData_m13229_MethodInfo,
	&TypeMetadata_IsCompatible_m10849_MethodInfo,
	&TypeMetadata_get_RequiresTypes_m13230_MethodInfo,
	NULL
};
extern const MethodInfo TypeMetadata_get_RequiresTypes_m13230_MethodInfo;
static const PropertyInfo TypeMetadata_t2035____RequiresTypes_PropertyInfo = 
{
	&TypeMetadata_t2035_il2cpp_TypeInfo/* parent */
	, "RequiresTypes"/* name */
	, &TypeMetadata_get_RequiresTypes_m13230_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TypeMetadata_t2035_PropertyInfos[] =
{
	&TypeMetadata_t2035____RequiresTypes_PropertyInfo,
	NULL
};
extern const MethodInfo TypeMetadata_IsCompatible_m10849_MethodInfo;
static const Il2CppMethodReference TypeMetadata_t2035_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
	NULL,
	NULL,
	&TypeMetadata_IsCompatible_m10849_MethodInfo,
	NULL,
};
static bool TypeMetadata_t2035_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeMetadata_t2035_1_0_0;
struct TypeMetadata_t2035;
const Il2CppTypeDefinitionMetadata TypeMetadata_t2035_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeMetadata_t2035_VTable/* vtableMethods */
	, TypeMetadata_t2035_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2188/* fieldStart */

};
TypeInfo TypeMetadata_t2035_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeMetadata"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, TypeMetadata_t2035_MethodInfos/* methods */
	, TypeMetadata_t2035_PropertyInfos/* properties */
	, NULL/* events */
	, &TypeMetadata_t2035_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeMetadata_t2035_0_0_0/* byval_arg */
	, &TypeMetadata_t2035_1_0_0/* this_arg */
	, &TypeMetadata_t2035_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeMetadata_t2035)/* instance_size */
	, sizeof (TypeMetadata_t2035)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_ClrT.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata
extern TypeInfo ClrTypeMetadata_t2036_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_ClrTMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ClrTypeMetadata_t2036_ClrTypeMetadata__ctor_m10850_ParameterInfos[] = 
{
	{"instanceType", 0, 134222708, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata::.ctor(System.Type)
extern const MethodInfo ClrTypeMetadata__ctor_m10850_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ClrTypeMetadata__ctor_m10850/* method */
	, &ClrTypeMetadata_t2036_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ClrTypeMetadata_t2036_ClrTypeMetadata__ctor_m10850_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata::get_RequiresTypes()
extern const MethodInfo ClrTypeMetadata_get_RequiresTypes_m10851_MethodInfo = 
{
	"get_RequiresTypes"/* name */
	, (methodPointerType)&ClrTypeMetadata_get_RequiresTypes_m10851/* method */
	, &ClrTypeMetadata_t2036_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ClrTypeMetadata_t2036_MethodInfos[] =
{
	&ClrTypeMetadata__ctor_m10850_MethodInfo,
	&ClrTypeMetadata_get_RequiresTypes_m10851_MethodInfo,
	NULL
};
extern const MethodInfo ClrTypeMetadata_get_RequiresTypes_m10851_MethodInfo;
static const PropertyInfo ClrTypeMetadata_t2036____RequiresTypes_PropertyInfo = 
{
	&ClrTypeMetadata_t2036_il2cpp_TypeInfo/* parent */
	, "RequiresTypes"/* name */
	, &ClrTypeMetadata_get_RequiresTypes_m10851_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ClrTypeMetadata_t2036_PropertyInfos[] =
{
	&ClrTypeMetadata_t2036____RequiresTypes_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ClrTypeMetadata_t2036_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
	NULL,
	NULL,
	&TypeMetadata_IsCompatible_m10849_MethodInfo,
	&ClrTypeMetadata_get_RequiresTypes_m10851_MethodInfo,
};
static bool ClrTypeMetadata_t2036_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClrTypeMetadata_t2036_0_0_0;
extern const Il2CppType ClrTypeMetadata_t2036_1_0_0;
struct ClrTypeMetadata_t2036;
const Il2CppTypeDefinitionMetadata ClrTypeMetadata_t2036_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeMetadata_t2035_0_0_0/* parent */
	, ClrTypeMetadata_t2036_VTable/* vtableMethods */
	, ClrTypeMetadata_t2036_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2190/* fieldStart */

};
TypeInfo ClrTypeMetadata_t2036_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClrTypeMetadata"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, ClrTypeMetadata_t2036_MethodInfos/* methods */
	, ClrTypeMetadata_t2036_PropertyInfos/* properties */
	, NULL/* events */
	, &ClrTypeMetadata_t2036_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClrTypeMetadata_t2036_0_0_0/* byval_arg */
	, &ClrTypeMetadata_t2036_1_0_0/* this_arg */
	, &ClrTypeMetadata_t2036_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClrTypeMetadata_t2036)/* instance_size */
	, sizeof (ClrTypeMetadata_t2036)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Seri.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata
extern TypeInfo SerializableTypeMetadata_t2037_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_SeriMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
static const ParameterInfo SerializableTypeMetadata_t2037_SerializableTypeMetadata__ctor_m10852_ParameterInfos[] = 
{
	{"itype", 0, 134222709, 0, &Type_t_0_0_0},
	{"info", 1, 134222710, 0, &SerializationInfo_t1043_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata::.ctor(System.Type,System.Runtime.Serialization.SerializationInfo)
extern const MethodInfo SerializableTypeMetadata__ctor_m10852_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SerializableTypeMetadata__ctor_m10852/* method */
	, &SerializableTypeMetadata_t2037_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, SerializableTypeMetadata_t2037_SerializableTypeMetadata__ctor_m10852_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeMetadata_t2035_0_0_0;
static const ParameterInfo SerializableTypeMetadata_t2037_SerializableTypeMetadata_IsCompatible_m10853_ParameterInfos[] = 
{
	{"other", 0, 134222711, 0, &TypeMetadata_t2035_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata::IsCompatible(System.Runtime.Serialization.Formatters.Binary.TypeMetadata)
extern const MethodInfo SerializableTypeMetadata_IsCompatible_m10853_MethodInfo = 
{
	"IsCompatible"/* name */
	, (methodPointerType)&SerializableTypeMetadata_IsCompatible_m10853/* method */
	, &SerializableTypeMetadata_t2037_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, SerializableTypeMetadata_t2037_SerializableTypeMetadata_IsCompatible_m10853_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectWriter_t2042_0_0_0;
extern const Il2CppType BinaryWriter_t1806_0_0_0;
static const ParameterInfo SerializableTypeMetadata_t2037_SerializableTypeMetadata_WriteAssemblies_m10854_ParameterInfos[] = 
{
	{"ow", 0, 134222712, 0, &ObjectWriter_t2042_0_0_0},
	{"writer", 1, 134222713, 0, &BinaryWriter_t1806_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata::WriteAssemblies(System.Runtime.Serialization.Formatters.Binary.ObjectWriter,System.IO.BinaryWriter)
extern const MethodInfo SerializableTypeMetadata_WriteAssemblies_m10854_MethodInfo = 
{
	"WriteAssemblies"/* name */
	, (methodPointerType)&SerializableTypeMetadata_WriteAssemblies_m10854/* method */
	, &SerializableTypeMetadata_t2037_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, SerializableTypeMetadata_t2037_SerializableTypeMetadata_WriteAssemblies_m10854_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectWriter_t2042_0_0_0;
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SerializableTypeMetadata_t2037_SerializableTypeMetadata_WriteTypeData_m10855_ParameterInfos[] = 
{
	{"ow", 0, 134222714, 0, &ObjectWriter_t2042_0_0_0},
	{"writer", 1, 134222715, 0, &BinaryWriter_t1806_0_0_0},
	{"writeTypes", 2, 134222716, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata::WriteTypeData(System.Runtime.Serialization.Formatters.Binary.ObjectWriter,System.IO.BinaryWriter,System.Boolean)
extern const MethodInfo SerializableTypeMetadata_WriteTypeData_m10855_MethodInfo = 
{
	"WriteTypeData"/* name */
	, (methodPointerType)&SerializableTypeMetadata_WriteTypeData_m10855/* method */
	, &SerializableTypeMetadata_t2037_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274/* invoker_method */
	, SerializableTypeMetadata_t2037_SerializableTypeMetadata_WriteTypeData_m10855_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectWriter_t2042_0_0_0;
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SerializableTypeMetadata_t2037_SerializableTypeMetadata_WriteObjectData_m10856_ParameterInfos[] = 
{
	{"ow", 0, 134222717, 0, &ObjectWriter_t2042_0_0_0},
	{"writer", 1, 134222718, 0, &BinaryWriter_t1806_0_0_0},
	{"data", 2, 134222719, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata::WriteObjectData(System.Runtime.Serialization.Formatters.Binary.ObjectWriter,System.IO.BinaryWriter,System.Object)
extern const MethodInfo SerializableTypeMetadata_WriteObjectData_m10856_MethodInfo = 
{
	"WriteObjectData"/* name */
	, (methodPointerType)&SerializableTypeMetadata_WriteObjectData_m10856/* method */
	, &SerializableTypeMetadata_t2037_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, SerializableTypeMetadata_t2037_SerializableTypeMetadata_WriteObjectData_m10856_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata::get_RequiresTypes()
extern const MethodInfo SerializableTypeMetadata_get_RequiresTypes_m10857_MethodInfo = 
{
	"get_RequiresTypes"/* name */
	, (methodPointerType)&SerializableTypeMetadata_get_RequiresTypes_m10857/* method */
	, &SerializableTypeMetadata_t2037_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SerializableTypeMetadata_t2037_MethodInfos[] =
{
	&SerializableTypeMetadata__ctor_m10852_MethodInfo,
	&SerializableTypeMetadata_IsCompatible_m10853_MethodInfo,
	&SerializableTypeMetadata_WriteAssemblies_m10854_MethodInfo,
	&SerializableTypeMetadata_WriteTypeData_m10855_MethodInfo,
	&SerializableTypeMetadata_WriteObjectData_m10856_MethodInfo,
	&SerializableTypeMetadata_get_RequiresTypes_m10857_MethodInfo,
	NULL
};
extern const MethodInfo SerializableTypeMetadata_get_RequiresTypes_m10857_MethodInfo;
static const PropertyInfo SerializableTypeMetadata_t2037____RequiresTypes_PropertyInfo = 
{
	&SerializableTypeMetadata_t2037_il2cpp_TypeInfo/* parent */
	, "RequiresTypes"/* name */
	, &SerializableTypeMetadata_get_RequiresTypes_m10857_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SerializableTypeMetadata_t2037_PropertyInfos[] =
{
	&SerializableTypeMetadata_t2037____RequiresTypes_PropertyInfo,
	NULL
};
extern const MethodInfo SerializableTypeMetadata_WriteAssemblies_m10854_MethodInfo;
extern const MethodInfo SerializableTypeMetadata_WriteTypeData_m10855_MethodInfo;
extern const MethodInfo SerializableTypeMetadata_WriteObjectData_m10856_MethodInfo;
extern const MethodInfo SerializableTypeMetadata_IsCompatible_m10853_MethodInfo;
static const Il2CppMethodReference SerializableTypeMetadata_t2037_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&SerializableTypeMetadata_WriteAssemblies_m10854_MethodInfo,
	&SerializableTypeMetadata_WriteTypeData_m10855_MethodInfo,
	&SerializableTypeMetadata_WriteObjectData_m10856_MethodInfo,
	&SerializableTypeMetadata_IsCompatible_m10853_MethodInfo,
	&SerializableTypeMetadata_get_RequiresTypes_m10857_MethodInfo,
};
static bool SerializableTypeMetadata_t2037_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializableTypeMetadata_t2037_0_0_0;
extern const Il2CppType SerializableTypeMetadata_t2037_1_0_0;
struct SerializableTypeMetadata_t2037;
const Il2CppTypeDefinitionMetadata SerializableTypeMetadata_t2037_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeMetadata_t2035_0_0_0/* parent */
	, SerializableTypeMetadata_t2037_VTable/* vtableMethods */
	, SerializableTypeMetadata_t2037_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2191/* fieldStart */

};
TypeInfo SerializableTypeMetadata_t2037_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializableTypeMetadata"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, SerializableTypeMetadata_t2037_MethodInfos/* methods */
	, SerializableTypeMetadata_t2037_PropertyInfos/* properties */
	, NULL/* events */
	, &SerializableTypeMetadata_t2037_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SerializableTypeMetadata_t2037_0_0_0/* byval_arg */
	, &SerializableTypeMetadata_t2037_1_0_0/* this_arg */
	, &SerializableTypeMetadata_t2037_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializableTypeMetadata_t2037)/* instance_size */
	, sizeof (SerializableTypeMetadata_t2037)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MemberTypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Memb.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MemberTypeMetadata
extern TypeInfo MemberTypeMetadata_t2038_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MemberTypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MembMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo MemberTypeMetadata_t2038_MemberTypeMetadata__ctor_m10858_ParameterInfos[] = 
{
	{"type", 0, 134222720, 0, &Type_t_0_0_0},
	{"context", 1, 134222721, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.MemberTypeMetadata::.ctor(System.Type,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MemberTypeMetadata__ctor_m10858_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberTypeMetadata__ctor_m10858/* method */
	, &MemberTypeMetadata_t2038_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, MemberTypeMetadata_t2038_MemberTypeMetadata__ctor_m10858_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectWriter_t2042_0_0_0;
extern const Il2CppType BinaryWriter_t1806_0_0_0;
static const ParameterInfo MemberTypeMetadata_t2038_MemberTypeMetadata_WriteAssemblies_m10859_ParameterInfos[] = 
{
	{"ow", 0, 134222722, 0, &ObjectWriter_t2042_0_0_0},
	{"writer", 1, 134222723, 0, &BinaryWriter_t1806_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.MemberTypeMetadata::WriteAssemblies(System.Runtime.Serialization.Formatters.Binary.ObjectWriter,System.IO.BinaryWriter)
extern const MethodInfo MemberTypeMetadata_WriteAssemblies_m10859_MethodInfo = 
{
	"WriteAssemblies"/* name */
	, (methodPointerType)&MemberTypeMetadata_WriteAssemblies_m10859/* method */
	, &MemberTypeMetadata_t2038_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, MemberTypeMetadata_t2038_MemberTypeMetadata_WriteAssemblies_m10859_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectWriter_t2042_0_0_0;
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo MemberTypeMetadata_t2038_MemberTypeMetadata_WriteTypeData_m10860_ParameterInfos[] = 
{
	{"ow", 0, 134222724, 0, &ObjectWriter_t2042_0_0_0},
	{"writer", 1, 134222725, 0, &BinaryWriter_t1806_0_0_0},
	{"writeTypes", 2, 134222726, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.MemberTypeMetadata::WriteTypeData(System.Runtime.Serialization.Formatters.Binary.ObjectWriter,System.IO.BinaryWriter,System.Boolean)
extern const MethodInfo MemberTypeMetadata_WriteTypeData_m10860_MethodInfo = 
{
	"WriteTypeData"/* name */
	, (methodPointerType)&MemberTypeMetadata_WriteTypeData_m10860/* method */
	, &MemberTypeMetadata_t2038_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274/* invoker_method */
	, MemberTypeMetadata_t2038_MemberTypeMetadata_WriteTypeData_m10860_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectWriter_t2042_0_0_0;
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MemberTypeMetadata_t2038_MemberTypeMetadata_WriteObjectData_m10861_ParameterInfos[] = 
{
	{"ow", 0, 134222727, 0, &ObjectWriter_t2042_0_0_0},
	{"writer", 1, 134222728, 0, &BinaryWriter_t1806_0_0_0},
	{"data", 2, 134222729, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.MemberTypeMetadata::WriteObjectData(System.Runtime.Serialization.Formatters.Binary.ObjectWriter,System.IO.BinaryWriter,System.Object)
extern const MethodInfo MemberTypeMetadata_WriteObjectData_m10861_MethodInfo = 
{
	"WriteObjectData"/* name */
	, (methodPointerType)&MemberTypeMetadata_WriteObjectData_m10861/* method */
	, &MemberTypeMetadata_t2038_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, MemberTypeMetadata_t2038_MemberTypeMetadata_WriteObjectData_m10861_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MemberTypeMetadata_t2038_MethodInfos[] =
{
	&MemberTypeMetadata__ctor_m10858_MethodInfo,
	&MemberTypeMetadata_WriteAssemblies_m10859_MethodInfo,
	&MemberTypeMetadata_WriteTypeData_m10860_MethodInfo,
	&MemberTypeMetadata_WriteObjectData_m10861_MethodInfo,
	NULL
};
extern const MethodInfo MemberTypeMetadata_WriteAssemblies_m10859_MethodInfo;
extern const MethodInfo MemberTypeMetadata_WriteTypeData_m10860_MethodInfo;
extern const MethodInfo MemberTypeMetadata_WriteObjectData_m10861_MethodInfo;
static const Il2CppMethodReference MemberTypeMetadata_t2038_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MemberTypeMetadata_WriteAssemblies_m10859_MethodInfo,
	&MemberTypeMetadata_WriteTypeData_m10860_MethodInfo,
	&MemberTypeMetadata_WriteObjectData_m10861_MethodInfo,
	&TypeMetadata_IsCompatible_m10849_MethodInfo,
	&ClrTypeMetadata_get_RequiresTypes_m10851_MethodInfo,
};
static bool MemberTypeMetadata_t2038_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberTypeMetadata_t2038_0_0_0;
extern const Il2CppType MemberTypeMetadata_t2038_1_0_0;
struct MemberTypeMetadata_t2038;
const Il2CppTypeDefinitionMetadata MemberTypeMetadata_t2038_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ClrTypeMetadata_t2036_0_0_0/* parent */
	, MemberTypeMetadata_t2038_VTable/* vtableMethods */
	, MemberTypeMetadata_t2038_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2193/* fieldStart */

};
TypeInfo MemberTypeMetadata_t2038_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberTypeMetadata"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, MemberTypeMetadata_t2038_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MemberTypeMetadata_t2038_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MemberTypeMetadata_t2038_0_0_0/* byval_arg */
	, &MemberTypeMetadata_t2038_1_0_0/* this_arg */
	, &MemberTypeMetadata_t2038_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberTypeMetadata_t2038)/* instance_size */
	, sizeof (MemberTypeMetadata_t2038)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectWriter/MetadataReference
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_2.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectWriter/MetadataReference
extern TypeInfo MetadataReference_t2039_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectWriter/MetadataReference
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_2MethodDeclarations.h"
extern const Il2CppType TypeMetadata_t2035_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo MetadataReference_t2039_MetadataReference__ctor_m10862_ParameterInfos[] = 
{
	{"metadata", 0, 134222796, 0, &TypeMetadata_t2035_0_0_0},
	{"id", 1, 134222797, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter/MetadataReference::.ctor(System.Runtime.Serialization.Formatters.Binary.TypeMetadata,System.Int64)
extern const MethodInfo MetadataReference__ctor_m10862_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MetadataReference__ctor_m10862/* method */
	, &MetadataReference_t2039_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071/* invoker_method */
	, MetadataReference_t2039_MetadataReference__ctor_m10862_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MetadataReference_t2039_MethodInfos[] =
{
	&MetadataReference__ctor_m10862_MethodInfo,
	NULL
};
static const Il2CppMethodReference MetadataReference_t2039_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool MetadataReference_t2039_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MetadataReference_t2039_0_0_0;
extern const Il2CppType MetadataReference_t2039_1_0_0;
extern TypeInfo ObjectWriter_t2042_il2cpp_TypeInfo;
struct MetadataReference_t2039;
const Il2CppTypeDefinitionMetadata MetadataReference_t2039_DefinitionMetadata = 
{
	&ObjectWriter_t2042_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MetadataReference_t2039_VTable/* vtableMethods */
	, MetadataReference_t2039_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2194/* fieldStart */

};
TypeInfo MetadataReference_t2039_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MetadataReference"/* name */
	, ""/* namespaze */
	, MetadataReference_t2039_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MetadataReference_t2039_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MetadataReference_t2039_0_0_0/* byval_arg */
	, &MetadataReference_t2039_1_0_0/* this_arg */
	, &MetadataReference_t2039_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MetadataReference_t2039)/* instance_size */
	, sizeof (MetadataReference_t2039)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectWriter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_3.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectWriter
// System.Runtime.Serialization.Formatters.Binary.ObjectWriter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_3MethodDeclarations.h"
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType FormatterAssemblyStyle_t2043_0_0_0;
extern const Il2CppType FormatterTypeStyle_t2044_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter__ctor_m10863_ParameterInfos[] = 
{
	{"surrogateSelector", 0, 134222730, 0, &ISurrogateSelector_t1996_0_0_0},
	{"context", 1, 134222731, 0, &StreamingContext_t1044_0_0_0},
	{"assemblyFormat", 2, 134222732, 0, &FormatterAssemblyStyle_t2043_0_0_0},
	{"typeFormat", 3, 134222733, 0, &FormatterTypeStyle_t2044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::.ctor(System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle,System.Runtime.Serialization.Formatters.FormatterTypeStyle)
extern const MethodInfo ObjectWriter__ctor_m10863_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectWriter__ctor_m10863/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044_Int32_t253_Int32_t253/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter__ctor_m10863_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::.cctor()
extern const MethodInfo ObjectWriter__cctor_m10864_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ObjectWriter__cctor_m10864/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType HeaderU5BU5D_t2251_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteObjectGraph_m10865_ParameterInfos[] = 
{
	{"writer", 0, 134222734, 0, &BinaryWriter_t1806_0_0_0},
	{"obj", 1, 134222735, 0, &Object_t_0_0_0},
	{"headers", 2, 134222736, 0, &HeaderU5BU5D_t2251_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteObjectGraph(System.IO.BinaryWriter,System.Object,System.Runtime.Remoting.Messaging.Header[])
extern const MethodInfo ObjectWriter_WriteObjectGraph_m10865_MethodInfo = 
{
	"WriteObjectGraph"/* name */
	, (methodPointerType)&ObjectWriter_WriteObjectGraph_m10865/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteObjectGraph_m10865_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_QueueObject_m10866_ParameterInfos[] = 
{
	{"obj", 0, 134222737, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::QueueObject(System.Object)
extern const MethodInfo ObjectWriter_QueueObject_m10866_MethodInfo = 
{
	"QueueObject"/* name */
	, (methodPointerType)&ObjectWriter_QueueObject_m10866/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_QueueObject_m10866_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteQueuedObjects_m10867_ParameterInfos[] = 
{
	{"writer", 0, 134222738, 0, &BinaryWriter_t1806_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteQueuedObjects(System.IO.BinaryWriter)
extern const MethodInfo ObjectWriter_WriteQueuedObjects_m10867_MethodInfo = 
{
	"WriteQueuedObjects"/* name */
	, (methodPointerType)&ObjectWriter_WriteQueuedObjects_m10867/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteQueuedObjects_m10867_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteObjectInstance_m10868_ParameterInfos[] = 
{
	{"writer", 0, 134222739, 0, &BinaryWriter_t1806_0_0_0},
	{"obj", 1, 134222740, 0, &Object_t_0_0_0},
	{"isValueObject", 2, 134222741, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteObjectInstance(System.IO.BinaryWriter,System.Object,System.Boolean)
extern const MethodInfo ObjectWriter_WriteObjectInstance_m10868_MethodInfo = 
{
	"WriteObjectInstance"/* name */
	, (methodPointerType)&ObjectWriter_WriteObjectInstance_m10868/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteObjectInstance_m10868_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteSerializationEnd_m10869_ParameterInfos[] = 
{
	{"writer", 0, 134222742, 0, &BinaryWriter_t1806_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteSerializationEnd(System.IO.BinaryWriter)
extern const MethodInfo ObjectWriter_WriteSerializationEnd_m10869_MethodInfo = 
{
	"WriteSerializationEnd"/* name */
	, (methodPointerType)&ObjectWriter_WriteSerializationEnd_m10869/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteSerializationEnd_m10869_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteObject_m10870_ParameterInfos[] = 
{
	{"writer", 0, 134222743, 0, &BinaryWriter_t1806_0_0_0},
	{"id", 1, 134222744, 0, &Int64_t1071_0_0_0},
	{"obj", 2, 134222745, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteObject(System.IO.BinaryWriter,System.Int64,System.Object)
extern const MethodInfo ObjectWriter_WriteObject_m10870_MethodInfo = 
{
	"WriteObject"/* name */
	, (methodPointerType)&ObjectWriter_WriteObject_m10870/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteObject_m10870_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType TypeMetadata_t2035_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_GetObjectData_m10871_ParameterInfos[] = 
{
	{"obj", 0, 134222746, 0, &Object_t_0_0_0},
	{"metadata", 1, 134222747, 0, &TypeMetadata_t2035_1_0_2},
	{"data", 2, 134222748, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_Object_t_TypeMetadataU26_t2762_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::GetObjectData(System.Object,System.Runtime.Serialization.Formatters.Binary.TypeMetadata&,System.Object&)
extern const MethodInfo ObjectWriter_GetObjectData_m10871_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ObjectWriter_GetObjectData_m10871/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_TypeMetadataU26_t2762_ObjectU26_t1197/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_GetObjectData_m10871_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_CreateMemberTypeMetadata_m10872_ParameterInfos[] = 
{
	{"type", 0, 134222749, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.Formatters.Binary.TypeMetadata System.Runtime.Serialization.Formatters.Binary.ObjectWriter::CreateMemberTypeMetadata(System.Type)
extern const MethodInfo ObjectWriter_CreateMemberTypeMetadata_m10872_MethodInfo = 
{
	"CreateMemberTypeMetadata"/* name */
	, (methodPointerType)&ObjectWriter_CreateMemberTypeMetadata_m10872/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &TypeMetadata_t2035_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_CreateMemberTypeMetadata_m10872_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Array_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteArray_m10873_ParameterInfos[] = 
{
	{"writer", 0, 134222750, 0, &BinaryWriter_t1806_0_0_0},
	{"id", 1, 134222751, 0, &Int64_t1071_0_0_0},
	{"array", 2, 134222752, 0, &Array_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteArray(System.IO.BinaryWriter,System.Int64,System.Array)
extern const MethodInfo ObjectWriter_WriteArray_m10873_MethodInfo = 
{
	"WriteArray"/* name */
	, (methodPointerType)&ObjectWriter_WriteArray_m10873/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteArray_m10873_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Array_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteGenericArray_m10874_ParameterInfos[] = 
{
	{"writer", 0, 134222753, 0, &BinaryWriter_t1806_0_0_0},
	{"id", 1, 134222754, 0, &Int64_t1071_0_0_0},
	{"array", 2, 134222755, 0, &Array_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteGenericArray(System.IO.BinaryWriter,System.Int64,System.Array)
extern const MethodInfo ObjectWriter_WriteGenericArray_m10874_MethodInfo = 
{
	"WriteGenericArray"/* name */
	, (methodPointerType)&ObjectWriter_WriteGenericArray_m10874/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteGenericArray_m10874_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Array_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteObjectArray_m10875_ParameterInfos[] = 
{
	{"writer", 0, 134222756, 0, &BinaryWriter_t1806_0_0_0},
	{"id", 1, 134222757, 0, &Int64_t1071_0_0_0},
	{"array", 2, 134222758, 0, &Array_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteObjectArray(System.IO.BinaryWriter,System.Int64,System.Array)
extern const MethodInfo ObjectWriter_WriteObjectArray_m10875_MethodInfo = 
{
	"WriteObjectArray"/* name */
	, (methodPointerType)&ObjectWriter_WriteObjectArray_m10875/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteObjectArray_m10875_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Array_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteStringArray_m10876_ParameterInfos[] = 
{
	{"writer", 0, 134222759, 0, &BinaryWriter_t1806_0_0_0},
	{"id", 1, 134222760, 0, &Int64_t1071_0_0_0},
	{"array", 2, 134222761, 0, &Array_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteStringArray(System.IO.BinaryWriter,System.Int64,System.Array)
extern const MethodInfo ObjectWriter_WriteStringArray_m10876_MethodInfo = 
{
	"WriteStringArray"/* name */
	, (methodPointerType)&ObjectWriter_WriteStringArray_m10876/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteStringArray_m10876_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Array_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WritePrimitiveTypeArray_m10877_ParameterInfos[] = 
{
	{"writer", 0, 134222762, 0, &BinaryWriter_t1806_0_0_0},
	{"id", 1, 134222763, 0, &Int64_t1071_0_0_0},
	{"array", 2, 134222764, 0, &Array_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WritePrimitiveTypeArray(System.IO.BinaryWriter,System.Int64,System.Array)
extern const MethodInfo ObjectWriter_WritePrimitiveTypeArray_m10877_MethodInfo = 
{
	"WritePrimitiveTypeArray"/* name */
	, (methodPointerType)&ObjectWriter_WritePrimitiveTypeArray_m10877/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WritePrimitiveTypeArray_m10877_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_BlockWrite_m10878_ParameterInfos[] = 
{
	{"writer", 0, 134222765, 0, &BinaryWriter_t1806_0_0_0},
	{"array", 1, 134222766, 0, &Array_t_0_0_0},
	{"dataSize", 2, 134222767, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::BlockWrite(System.IO.BinaryWriter,System.Array,System.Int32)
extern const MethodInfo ObjectWriter_BlockWrite_m10878_MethodInfo = 
{
	"BlockWrite"/* name */
	, (methodPointerType)&ObjectWriter_BlockWrite_m10878/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_BlockWrite_m10878_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteSingleDimensionArrayElements_m10879_ParameterInfos[] = 
{
	{"writer", 0, 134222768, 0, &BinaryWriter_t1806_0_0_0},
	{"array", 1, 134222769, 0, &Array_t_0_0_0},
	{"elementType", 2, 134222770, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteSingleDimensionArrayElements(System.IO.BinaryWriter,System.Array,System.Type)
extern const MethodInfo ObjectWriter_WriteSingleDimensionArrayElements_m10879_MethodInfo = 
{
	"WriteSingleDimensionArrayElements"/* name */
	, (methodPointerType)&ObjectWriter_WriteSingleDimensionArrayElements_m10879/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteSingleDimensionArrayElements_m10879_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteNullFiller_m10880_ParameterInfos[] = 
{
	{"writer", 0, 134222771, 0, &BinaryWriter_t1806_0_0_0},
	{"numNulls", 1, 134222772, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteNullFiller(System.IO.BinaryWriter,System.Int32)
extern const MethodInfo ObjectWriter_WriteNullFiller_m10880_MethodInfo = 
{
	"WriteNullFiller"/* name */
	, (methodPointerType)&ObjectWriter_WriteNullFiller_m10880/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteNullFiller_m10880_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteObjectReference_m10881_ParameterInfos[] = 
{
	{"writer", 0, 134222773, 0, &BinaryWriter_t1806_0_0_0},
	{"id", 1, 134222774, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteObjectReference(System.IO.BinaryWriter,System.Int64)
extern const MethodInfo ObjectWriter_WriteObjectReference_m10881_MethodInfo = 
{
	"WriteObjectReference"/* name */
	, (methodPointerType)&ObjectWriter_WriteObjectReference_m10881/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteObjectReference_m10881_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteValue_m10882_ParameterInfos[] = 
{
	{"writer", 0, 134222775, 0, &BinaryWriter_t1806_0_0_0},
	{"valueType", 1, 134222776, 0, &Type_t_0_0_0},
	{"val", 2, 134222777, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteValue(System.IO.BinaryWriter,System.Type,System.Object)
extern const MethodInfo ObjectWriter_WriteValue_m10882_MethodInfo = 
{
	"WriteValue"/* name */
	, (methodPointerType)&ObjectWriter_WriteValue_m10882/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteValue_m10882_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteString_m10883_ParameterInfos[] = 
{
	{"writer", 0, 134222778, 0, &BinaryWriter_t1806_0_0_0},
	{"id", 1, 134222779, 0, &Int64_t1071_0_0_0},
	{"str", 2, 134222780, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteString(System.IO.BinaryWriter,System.Int64,System.String)
extern const MethodInfo ObjectWriter_WriteString_m10883_MethodInfo = 
{
	"WriteString"/* name */
	, (methodPointerType)&ObjectWriter_WriteString_m10883/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteString_m10883_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Assembly_t1447_0_0_0;
extern const Il2CppType Assembly_t1447_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteAssembly_m10884_ParameterInfos[] = 
{
	{"writer", 0, 134222781, 0, &BinaryWriter_t1806_0_0_0},
	{"assembly", 1, 134222782, 0, &Assembly_t1447_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteAssembly(System.IO.BinaryWriter,System.Reflection.Assembly)
extern const MethodInfo ObjectWriter_WriteAssembly_m10884_MethodInfo = 
{
	"WriteAssembly"/* name */
	, (methodPointerType)&ObjectWriter_WriteAssembly_m10884/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteAssembly_m10884_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteAssemblyName_m10885_ParameterInfos[] = 
{
	{"writer", 0, 134222783, 0, &BinaryWriter_t1806_0_0_0},
	{"assembly", 1, 134222784, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteAssemblyName(System.IO.BinaryWriter,System.String)
extern const MethodInfo ObjectWriter_WriteAssemblyName_m10885_MethodInfo = 
{
	"WriteAssemblyName"/* name */
	, (methodPointerType)&ObjectWriter_WriteAssemblyName_m10885/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteAssemblyName_m10885_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Assembly_t1447_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_GetAssemblyId_m10886_ParameterInfos[] = 
{
	{"assembly", 0, 134222785, 0, &Assembly_t1447_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectWriter::GetAssemblyId(System.Reflection.Assembly)
extern const MethodInfo ObjectWriter_GetAssemblyId_m10886_MethodInfo = 
{
	"GetAssemblyId"/* name */
	, (methodPointerType)&ObjectWriter_GetAssemblyId_m10886/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_GetAssemblyId_m10886_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_GetAssemblyNameId_m10887_ParameterInfos[] = 
{
	{"assembly", 0, 134222786, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectWriter::GetAssemblyNameId(System.String)
extern const MethodInfo ObjectWriter_GetAssemblyNameId_m10887_MethodInfo = 
{
	"GetAssemblyNameId"/* name */
	, (methodPointerType)&ObjectWriter_GetAssemblyNameId_m10887/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_GetAssemblyNameId_m10887_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_1_0_2;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_RegisterAssembly_m10888_ParameterInfos[] = 
{
	{"assembly", 0, 134222787, 0, &String_t_0_0_0},
	{"firstTime", 1, 134222788, 0, &Boolean_t273_1_0_2},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.Formatters.Binary.ObjectWriter::RegisterAssembly(System.String,System.Boolean&)
extern const MethodInfo ObjectWriter_RegisterAssembly_m10888_MethodInfo = 
{
	"RegisterAssembly"/* name */
	, (methodPointerType)&ObjectWriter_RegisterAssembly_m10888/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_BooleanU26_t745/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_RegisterAssembly_m10888_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WritePrimitiveValue_m10889_ParameterInfos[] = 
{
	{"writer", 0, 134222789, 0, &BinaryWriter_t1806_0_0_0},
	{"value", 1, 134222790, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WritePrimitiveValue(System.IO.BinaryWriter,System.Object)
extern const MethodInfo ObjectWriter_WritePrimitiveValue_m10889_MethodInfo = 
{
	"WritePrimitiveValue"/* name */
	, (methodPointerType)&ObjectWriter_WritePrimitiveValue_m10889/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WritePrimitiveValue_m10889_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteTypeCode_m10890_ParameterInfos[] = 
{
	{"writer", 0, 134222791, 0, &BinaryWriter_t1806_0_0_0},
	{"type", 1, 134222792, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteTypeCode(System.IO.BinaryWriter,System.Type)
extern const MethodInfo ObjectWriter_WriteTypeCode_m10890_MethodInfo = 
{
	"WriteTypeCode"/* name */
	, (methodPointerType)&ObjectWriter_WriteTypeCode_m10890/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteTypeCode_m10890_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_GetTypeTag_m10891_ParameterInfos[] = 
{
	{"type", 0, 134222793, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_TypeTag_t2024_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.Formatters.Binary.TypeTag System.Runtime.Serialization.Formatters.Binary.ObjectWriter::GetTypeTag(System.Type)
extern const MethodInfo ObjectWriter_GetTypeTag_m10891_MethodInfo = 
{
	"GetTypeTag"/* name */
	, (methodPointerType)&ObjectWriter_GetTypeTag_m10891/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &TypeTag_t2024_0_0_0/* return_type */
	, RuntimeInvoker_TypeTag_t2024_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_GetTypeTag_m10891_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryWriter_t1806_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ObjectWriter_t2042_ObjectWriter_WriteTypeSpec_m10892_ParameterInfos[] = 
{
	{"writer", 0, 134222794, 0, &BinaryWriter_t1806_0_0_0},
	{"type", 1, 134222795, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter::WriteTypeSpec(System.IO.BinaryWriter,System.Type)
extern const MethodInfo ObjectWriter_WriteTypeSpec_m10892_MethodInfo = 
{
	"WriteTypeSpec"/* name */
	, (methodPointerType)&ObjectWriter_WriteTypeSpec_m10892/* method */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ObjectWriter_t2042_ObjectWriter_WriteTypeSpec_m10892_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectWriter_t2042_MethodInfos[] =
{
	&ObjectWriter__ctor_m10863_MethodInfo,
	&ObjectWriter__cctor_m10864_MethodInfo,
	&ObjectWriter_WriteObjectGraph_m10865_MethodInfo,
	&ObjectWriter_QueueObject_m10866_MethodInfo,
	&ObjectWriter_WriteQueuedObjects_m10867_MethodInfo,
	&ObjectWriter_WriteObjectInstance_m10868_MethodInfo,
	&ObjectWriter_WriteSerializationEnd_m10869_MethodInfo,
	&ObjectWriter_WriteObject_m10870_MethodInfo,
	&ObjectWriter_GetObjectData_m10871_MethodInfo,
	&ObjectWriter_CreateMemberTypeMetadata_m10872_MethodInfo,
	&ObjectWriter_WriteArray_m10873_MethodInfo,
	&ObjectWriter_WriteGenericArray_m10874_MethodInfo,
	&ObjectWriter_WriteObjectArray_m10875_MethodInfo,
	&ObjectWriter_WriteStringArray_m10876_MethodInfo,
	&ObjectWriter_WritePrimitiveTypeArray_m10877_MethodInfo,
	&ObjectWriter_BlockWrite_m10878_MethodInfo,
	&ObjectWriter_WriteSingleDimensionArrayElements_m10879_MethodInfo,
	&ObjectWriter_WriteNullFiller_m10880_MethodInfo,
	&ObjectWriter_WriteObjectReference_m10881_MethodInfo,
	&ObjectWriter_WriteValue_m10882_MethodInfo,
	&ObjectWriter_WriteString_m10883_MethodInfo,
	&ObjectWriter_WriteAssembly_m10884_MethodInfo,
	&ObjectWriter_WriteAssemblyName_m10885_MethodInfo,
	&ObjectWriter_GetAssemblyId_m10886_MethodInfo,
	&ObjectWriter_GetAssemblyNameId_m10887_MethodInfo,
	&ObjectWriter_RegisterAssembly_m10888_MethodInfo,
	&ObjectWriter_WritePrimitiveValue_m10889_MethodInfo,
	&ObjectWriter_WriteTypeCode_m10890_MethodInfo,
	&ObjectWriter_GetTypeTag_m10891_MethodInfo,
	&ObjectWriter_WriteTypeSpec_m10892_MethodInfo,
	NULL
};
static const Il2CppType* ObjectWriter_t2042_il2cpp_TypeInfo__nestedTypes[1] =
{
	&MetadataReference_t2039_0_0_0,
};
static const Il2CppMethodReference ObjectWriter_t2042_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ObjectWriter_t2042_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectWriter_t2042_1_0_0;
struct ObjectWriter_t2042;
const Il2CppTypeDefinitionMetadata ObjectWriter_t2042_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ObjectWriter_t2042_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectWriter_t2042_VTable/* vtableMethods */
	, ObjectWriter_t2042_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2196/* fieldStart */

};
TypeInfo ObjectWriter_t2042_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectWriter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, ObjectWriter_t2042_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ObjectWriter_t2042_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectWriter_t2042_0_0_0/* byval_arg */
	, &ObjectWriter_t2042_1_0_0/* this_arg */
	, &ObjectWriter_t2042_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectWriter_t2042)/* instance_size */
	, sizeof (ObjectWriter_t2042)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ObjectWriter_t2042_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAs.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
extern TypeInfo FormatterAssemblyStyle_t2043_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAsMethodDeclarations.h"
static const MethodInfo* FormatterAssemblyStyle_t2043_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FormatterAssemblyStyle_t2043_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool FormatterAssemblyStyle_t2043_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormatterAssemblyStyle_t2043_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterAssemblyStyle_t2043_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterAssemblyStyle_t2043_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterAssemblyStyle_t2043_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, FormatterAssemblyStyle_t2043_VTable/* vtableMethods */
	, FormatterAssemblyStyle_t2043_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2210/* fieldStart */

};
TypeInfo FormatterAssemblyStyle_t2043_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterAssemblyStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, FormatterAssemblyStyle_t2043_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 612/* custom_attributes_cache */
	, &FormatterAssemblyStyle_t2043_0_0_0/* byval_arg */
	, &FormatterAssemblyStyle_t2043_1_0_0/* this_arg */
	, &FormatterAssemblyStyle_t2043_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterAssemblyStyle_t2043)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterAssemblyStyle_t2043)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTy.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterTypeStyle
extern TypeInfo FormatterTypeStyle_t2044_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTyMethodDeclarations.h"
static const MethodInfo* FormatterTypeStyle_t2044_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FormatterTypeStyle_t2044_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool FormatterTypeStyle_t2044_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormatterTypeStyle_t2044_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterTypeStyle_t2044_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterTypeStyle_t2044_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterTypeStyle_t2044_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, FormatterTypeStyle_t2044_VTable/* vtableMethods */
	, FormatterTypeStyle_t2044_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2213/* fieldStart */

};
TypeInfo FormatterTypeStyle_t2044_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterTypeStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, FormatterTypeStyle_t2044_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 613/* custom_attributes_cache */
	, &FormatterTypeStyle_t2044_0_0_0/* byval_arg */
	, &FormatterTypeStyle_t2044_1_0_0/* this_arg */
	, &FormatterTypeStyle_t2044_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterTypeStyle_t2044)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterTypeStyle_t2044)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
// Metadata Definition System.Runtime.Serialization.Formatters.TypeFilterLevel
extern TypeInfo TypeFilterLevel_t2045_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterLMethodDeclarations.h"
static const MethodInfo* TypeFilterLevel_t2045_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeFilterLevel_t2045_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TypeFilterLevel_t2045_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeFilterLevel_t2045_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeFilterLevel_t2045_1_0_0;
const Il2CppTypeDefinitionMetadata TypeFilterLevel_t2045_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeFilterLevel_t2045_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TypeFilterLevel_t2045_VTable/* vtableMethods */
	, TypeFilterLevel_t2045_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2217/* fieldStart */

};
TypeInfo TypeFilterLevel_t2045_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeFilterLevel"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, TypeFilterLevel_t2045_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 614/* custom_attributes_cache */
	, &TypeFilterLevel_t2045_0_0_0/* byval_arg */
	, &TypeFilterLevel_t2045_1_0_0/* this_arg */
	, &TypeFilterLevel_t2045_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeFilterLevel_t2045)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeFilterLevel_t2045)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverter.h"
// Metadata Definition System.Runtime.Serialization.FormatterConverter
extern TypeInfo FormatterConverter_t2046_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.FormatterConverter::.ctor()
extern const MethodInfo FormatterConverter__ctor_m10893_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatterConverter__ctor_m10893/* method */
	, &FormatterConverter_t2046_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo FormatterConverter_t2046_FormatterConverter_Convert_m10894_ParameterInfos[] = 
{
	{"value", 0, 134222798, 0, &Object_t_0_0_0},
	{"type", 1, 134222799, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterConverter::Convert(System.Object,System.Type)
extern const MethodInfo FormatterConverter_Convert_m10894_MethodInfo = 
{
	"Convert"/* name */
	, (methodPointerType)&FormatterConverter_Convert_m10894/* method */
	, &FormatterConverter_t2046_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, FormatterConverter_t2046_FormatterConverter_Convert_m10894_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t2046_FormatterConverter_ToBoolean_m10895_ParameterInfos[] = 
{
	{"value", 0, 134222800, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.FormatterConverter::ToBoolean(System.Object)
extern const MethodInfo FormatterConverter_ToBoolean_m10895_MethodInfo = 
{
	"ToBoolean"/* name */
	, (methodPointerType)&FormatterConverter_ToBoolean_m10895/* method */
	, &FormatterConverter_t2046_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, FormatterConverter_t2046_FormatterConverter_ToBoolean_m10895_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t2046_FormatterConverter_ToInt16_m10896_ParameterInfos[] = 
{
	{"value", 0, 134222801, 0, &Object_t_0_0_0},
};
extern const Il2CppType Int16_t752_0_0_0;
extern void* RuntimeInvoker_Int16_t752_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int16 System.Runtime.Serialization.FormatterConverter::ToInt16(System.Object)
extern const MethodInfo FormatterConverter_ToInt16_m10896_MethodInfo = 
{
	"ToInt16"/* name */
	, (methodPointerType)&FormatterConverter_ToInt16_m10896/* method */
	, &FormatterConverter_t2046_il2cpp_TypeInfo/* declaring_type */
	, &Int16_t752_0_0_0/* return_type */
	, RuntimeInvoker_Int16_t752_Object_t/* invoker_method */
	, FormatterConverter_t2046_FormatterConverter_ToInt16_m10896_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t2046_FormatterConverter_ToInt32_m10897_ParameterInfos[] = 
{
	{"value", 0, 134222802, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.FormatterConverter::ToInt32(System.Object)
extern const MethodInfo FormatterConverter_ToInt32_m10897_MethodInfo = 
{
	"ToInt32"/* name */
	, (methodPointerType)&FormatterConverter_ToInt32_m10897/* method */
	, &FormatterConverter_t2046_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, FormatterConverter_t2046_FormatterConverter_ToInt32_m10897_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t2046_FormatterConverter_ToInt64_m10898_ParameterInfos[] = 
{
	{"value", 0, 134222803, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Runtime.Serialization.FormatterConverter::ToInt64(System.Object)
extern const MethodInfo FormatterConverter_ToInt64_m10898_MethodInfo = 
{
	"ToInt64"/* name */
	, (methodPointerType)&FormatterConverter_ToInt64_m10898/* method */
	, &FormatterConverter_t2046_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1071_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1071_Object_t/* invoker_method */
	, FormatterConverter_t2046_FormatterConverter_ToInt64_m10898_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t2046_FormatterConverter_ToString_m10899_ParameterInfos[] = 
{
	{"value", 0, 134222804, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.FormatterConverter::ToString(System.Object)
extern const MethodInfo FormatterConverter_ToString_m10899_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&FormatterConverter_ToString_m10899/* method */
	, &FormatterConverter_t2046_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterConverter_t2046_FormatterConverter_ToString_m10899_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormatterConverter_t2046_MethodInfos[] =
{
	&FormatterConverter__ctor_m10893_MethodInfo,
	&FormatterConverter_Convert_m10894_MethodInfo,
	&FormatterConverter_ToBoolean_m10895_MethodInfo,
	&FormatterConverter_ToInt16_m10896_MethodInfo,
	&FormatterConverter_ToInt32_m10897_MethodInfo,
	&FormatterConverter_ToInt64_m10898_MethodInfo,
	&FormatterConverter_ToString_m10899_MethodInfo,
	NULL
};
extern const MethodInfo FormatterConverter_Convert_m10894_MethodInfo;
extern const MethodInfo FormatterConverter_ToBoolean_m10895_MethodInfo;
extern const MethodInfo FormatterConverter_ToInt16_m10896_MethodInfo;
extern const MethodInfo FormatterConverter_ToInt32_m10897_MethodInfo;
extern const MethodInfo FormatterConverter_ToInt64_m10898_MethodInfo;
extern const MethodInfo FormatterConverter_ToString_m10899_MethodInfo;
static const Il2CppMethodReference FormatterConverter_t2046_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&FormatterConverter_Convert_m10894_MethodInfo,
	&FormatterConverter_ToBoolean_m10895_MethodInfo,
	&FormatterConverter_ToInt16_m10896_MethodInfo,
	&FormatterConverter_ToInt32_m10897_MethodInfo,
	&FormatterConverter_ToInt64_m10898_MethodInfo,
	&FormatterConverter_ToString_m10899_MethodInfo,
};
static bool FormatterConverter_t2046_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormatterConverter_t2064_0_0_0;
static const Il2CppType* FormatterConverter_t2046_InterfacesTypeInfos[] = 
{
	&IFormatterConverter_t2064_0_0_0,
};
static Il2CppInterfaceOffsetPair FormatterConverter_t2046_InterfacesOffsets[] = 
{
	{ &IFormatterConverter_t2064_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterConverter_t2046_0_0_0;
extern const Il2CppType FormatterConverter_t2046_1_0_0;
struct FormatterConverter_t2046;
const Il2CppTypeDefinitionMetadata FormatterConverter_t2046_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FormatterConverter_t2046_InterfacesTypeInfos/* implementedInterfaces */
	, FormatterConverter_t2046_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterConverter_t2046_VTable/* vtableMethods */
	, FormatterConverter_t2046_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FormatterConverter_t2046_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, FormatterConverter_t2046_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormatterConverter_t2046_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 615/* custom_attributes_cache */
	, &FormatterConverter_t2046_0_0_0/* byval_arg */
	, &FormatterConverter_t2046_1_0_0/* this_arg */
	, &FormatterConverter_t2046_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterConverter_t2046)/* instance_size */
	, sizeof (FormatterConverter_t2046)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServices.h"
// Metadata Definition System.Runtime.Serialization.FormatterServices
extern TypeInfo FormatterServices_t2047_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServicesMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MemberInfoU5BU5D_t2030_0_0_0;
extern const Il2CppType MemberInfoU5BU5D_t2030_0_0_0;
static const ParameterInfo FormatterServices_t2047_FormatterServices_GetObjectData_m10900_ParameterInfos[] = 
{
	{"obj", 0, 134222805, 0, &Object_t_0_0_0},
	{"members", 1, 134222806, 0, &MemberInfoU5BU5D_t2030_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Serialization.FormatterServices::GetObjectData(System.Object,System.Reflection.MemberInfo[])
extern const MethodInfo FormatterServices_GetObjectData_m10900_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&FormatterServices_GetObjectData_m10900/* method */
	, &FormatterServices_t2047_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, FormatterServices_t2047_FormatterServices_GetObjectData_m10900_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo FormatterServices_t2047_FormatterServices_GetSerializableMembers_m10901_ParameterInfos[] = 
{
	{"type", 0, 134222807, 0, &Type_t_0_0_0},
	{"context", 1, 134222808, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberInfo[] System.Runtime.Serialization.FormatterServices::GetSerializableMembers(System.Type,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo FormatterServices_GetSerializableMembers_m10901_MethodInfo = 
{
	"GetSerializableMembers"/* name */
	, (methodPointerType)&FormatterServices_GetSerializableMembers_m10901/* method */
	, &FormatterServices_t2047_il2cpp_TypeInfo/* declaring_type */
	, &MemberInfoU5BU5D_t2030_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, FormatterServices_t2047_FormatterServices_GetSerializableMembers_m10901_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ArrayList_t1271_0_0_0;
extern const Il2CppType ArrayList_t1271_0_0_0;
static const ParameterInfo FormatterServices_t2047_FormatterServices_GetFields_m10902_ParameterInfos[] = 
{
	{"reflectedType", 0, 134222809, 0, &Type_t_0_0_0},
	{"type", 1, 134222810, 0, &Type_t_0_0_0},
	{"fields", 2, 134222811, 0, &ArrayList_t1271_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.FormatterServices::GetFields(System.Type,System.Type,System.Collections.ArrayList)
extern const MethodInfo FormatterServices_GetFields_m10902_MethodInfo = 
{
	"GetFields"/* name */
	, (methodPointerType)&FormatterServices_GetFields_m10902/* method */
	, &FormatterServices_t2047_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, FormatterServices_t2047_FormatterServices_GetFields_m10902_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo FormatterServices_t2047_FormatterServices_GetUninitializedObject_m10903_ParameterInfos[] = 
{
	{"type", 0, 134222812, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterServices::GetUninitializedObject(System.Type)
extern const MethodInfo FormatterServices_GetUninitializedObject_m10903_MethodInfo = 
{
	"GetUninitializedObject"/* name */
	, (methodPointerType)&FormatterServices_GetUninitializedObject_m10903/* method */
	, &FormatterServices_t2047_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterServices_t2047_FormatterServices_GetUninitializedObject_m10903_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo FormatterServices_t2047_FormatterServices_GetSafeUninitializedObject_m10904_ParameterInfos[] = 
{
	{"type", 0, 134222813, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterServices::GetSafeUninitializedObject(System.Type)
extern const MethodInfo FormatterServices_GetSafeUninitializedObject_m10904_MethodInfo = 
{
	"GetSafeUninitializedObject"/* name */
	, (methodPointerType)&FormatterServices_GetSafeUninitializedObject_m10904/* method */
	, &FormatterServices_t2047_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterServices_t2047_FormatterServices_GetSafeUninitializedObject_m10904_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormatterServices_t2047_MethodInfos[] =
{
	&FormatterServices_GetObjectData_m10900_MethodInfo,
	&FormatterServices_GetSerializableMembers_m10901_MethodInfo,
	&FormatterServices_GetFields_m10902_MethodInfo,
	&FormatterServices_GetUninitializedObject_m10903_MethodInfo,
	&FormatterServices_GetSafeUninitializedObject_m10904_MethodInfo,
	NULL
};
static const Il2CppMethodReference FormatterServices_t2047_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool FormatterServices_t2047_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterServices_t2047_0_0_0;
extern const Il2CppType FormatterServices_t2047_1_0_0;
struct FormatterServices_t2047;
const Il2CppTypeDefinitionMetadata FormatterServices_t2047_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterServices_t2047_VTable/* vtableMethods */
	, FormatterServices_t2047_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FormatterServices_t2047_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterServices"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, FormatterServices_t2047_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormatterServices_t2047_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 616/* custom_attributes_cache */
	, &FormatterServices_t2047_0_0_0/* byval_arg */
	, &FormatterServices_t2047_1_0_0/* this_arg */
	, &FormatterServices_t2047_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterServices_t2047)/* instance_size */
	, sizeof (FormatterServices_t2047)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IDeserializationCallback
extern TypeInfo IDeserializationCallback_t445_il2cpp_TypeInfo;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IDeserializationCallback_t445_IDeserializationCallback_OnDeserialization_m13231_ParameterInfos[] = 
{
	{"sender", 0, 134222814, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.IDeserializationCallback::OnDeserialization(System.Object)
extern const MethodInfo IDeserializationCallback_OnDeserialization_m13231_MethodInfo = 
{
	"OnDeserialization"/* name */
	, NULL/* method */
	, &IDeserializationCallback_t445_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, IDeserializationCallback_t445_IDeserializationCallback_OnDeserialization_m13231_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IDeserializationCallback_t445_MethodInfos[] =
{
	&IDeserializationCallback_OnDeserialization_m13231_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDeserializationCallback_t445_0_0_0;
extern const Il2CppType IDeserializationCallback_t445_1_0_0;
struct IDeserializationCallback_t445;
const Il2CppTypeDefinitionMetadata IDeserializationCallback_t445_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IDeserializationCallback_t445_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDeserializationCallback"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IDeserializationCallback_t445_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IDeserializationCallback_t445_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 617/* custom_attributes_cache */
	, &IDeserializationCallback_t445_0_0_0/* byval_arg */
	, &IDeserializationCallback_t445_1_0_0/* this_arg */
	, &IDeserializationCallback_t445_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatter
extern TypeInfo IFormatter_t2385_il2cpp_TypeInfo;
static const MethodInfo* IFormatter_t2385_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatter_t2385_1_0_0;
struct IFormatter_t2385;
const Il2CppTypeDefinitionMetadata IFormatter_t2385_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IFormatter_t2385_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IFormatter_t2385_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IFormatter_t2385_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 618/* custom_attributes_cache */
	, &IFormatter_t2385_0_0_0/* byval_arg */
	, &IFormatter_t2385_1_0_0/* this_arg */
	, &IFormatter_t2385_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatterConverter
extern TypeInfo IFormatterConverter_t2064_il2cpp_TypeInfo;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2064_IFormatterConverter_Convert_m13232_ParameterInfos[] = 
{
	{"value", 0, 134222815, 0, &Object_t_0_0_0},
	{"type", 1, 134222816, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.IFormatterConverter::Convert(System.Object,System.Type)
extern const MethodInfo IFormatterConverter_Convert_m13232_MethodInfo = 
{
	"Convert"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2064_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, IFormatterConverter_t2064_IFormatterConverter_Convert_m13232_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2064_IFormatterConverter_ToBoolean_m13233_ParameterInfos[] = 
{
	{"value", 0, 134222817, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.IFormatterConverter::ToBoolean(System.Object)
extern const MethodInfo IFormatterConverter_ToBoolean_m13233_MethodInfo = 
{
	"ToBoolean"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2064_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, IFormatterConverter_t2064_IFormatterConverter_ToBoolean_m13233_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2064_IFormatterConverter_ToInt16_m13234_ParameterInfos[] = 
{
	{"value", 0, 134222818, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int16_t752_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int16 System.Runtime.Serialization.IFormatterConverter::ToInt16(System.Object)
extern const MethodInfo IFormatterConverter_ToInt16_m13234_MethodInfo = 
{
	"ToInt16"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2064_il2cpp_TypeInfo/* declaring_type */
	, &Int16_t752_0_0_0/* return_type */
	, RuntimeInvoker_Int16_t752_Object_t/* invoker_method */
	, IFormatterConverter_t2064_IFormatterConverter_ToInt16_m13234_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2064_IFormatterConverter_ToInt32_m13235_ParameterInfos[] = 
{
	{"value", 0, 134222819, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.IFormatterConverter::ToInt32(System.Object)
extern const MethodInfo IFormatterConverter_ToInt32_m13235_MethodInfo = 
{
	"ToInt32"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2064_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, IFormatterConverter_t2064_IFormatterConverter_ToInt32_m13235_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2064_IFormatterConverter_ToInt64_m13236_ParameterInfos[] = 
{
	{"value", 0, 134222820, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Runtime.Serialization.IFormatterConverter::ToInt64(System.Object)
extern const MethodInfo IFormatterConverter_ToInt64_m13236_MethodInfo = 
{
	"ToInt64"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2064_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1071_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1071_Object_t/* invoker_method */
	, IFormatterConverter_t2064_IFormatterConverter_ToInt64_m13236_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t2064_IFormatterConverter_ToString_m13237_ParameterInfos[] = 
{
	{"value", 0, 134222821, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.IFormatterConverter::ToString(System.Object)
extern const MethodInfo IFormatterConverter_ToString_m13237_MethodInfo = 
{
	"ToString"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2064_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IFormatterConverter_t2064_IFormatterConverter_ToString_m13237_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IFormatterConverter_t2064_MethodInfos[] =
{
	&IFormatterConverter_Convert_m13232_MethodInfo,
	&IFormatterConverter_ToBoolean_m13233_MethodInfo,
	&IFormatterConverter_ToInt16_m13234_MethodInfo,
	&IFormatterConverter_ToInt32_m13235_MethodInfo,
	&IFormatterConverter_ToInt64_m13236_MethodInfo,
	&IFormatterConverter_ToString_m13237_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatterConverter_t2064_1_0_0;
struct IFormatterConverter_t2064;
const Il2CppTypeDefinitionMetadata IFormatterConverter_t2064_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IFormatterConverter_t2064_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IFormatterConverter_t2064_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IFormatterConverter_t2064_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 619/* custom_attributes_cache */
	, &IFormatterConverter_t2064_0_0_0/* byval_arg */
	, &IFormatterConverter_t2064_1_0_0/* this_arg */
	, &IFormatterConverter_t2064_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IObjectReference
extern TypeInfo IObjectReference_t2310_il2cpp_TypeInfo;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo IObjectReference_t2310_IObjectReference_GetRealObject_m13238_ParameterInfos[] = 
{
	{"context", 0, 134222822, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.IObjectReference::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo IObjectReference_GetRealObject_m13238_MethodInfo = 
{
	"GetRealObject"/* name */
	, NULL/* method */
	, &IObjectReference_t2310_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t1044/* invoker_method */
	, IObjectReference_t2310_IObjectReference_GetRealObject_m13238_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IObjectReference_t2310_MethodInfos[] =
{
	&IObjectReference_GetRealObject_m13238_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IObjectReference_t2310_1_0_0;
struct IObjectReference_t2310;
const Il2CppTypeDefinitionMetadata IObjectReference_t2310_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IObjectReference_t2310_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IObjectReference"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IObjectReference_t2310_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IObjectReference_t2310_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 620/* custom_attributes_cache */
	, &IObjectReference_t2310_0_0_0/* byval_arg */
	, &IObjectReference_t2310_1_0_0/* this_arg */
	, &IObjectReference_t2310_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.ISerializationSurrogate
extern TypeInfo ISerializationSurrogate_t2056_il2cpp_TypeInfo;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo ISerializationSurrogate_t2056_ISerializationSurrogate_GetObjectData_m13239_ParameterInfos[] = 
{
	{"obj", 0, 134222823, 0, &Object_t_0_0_0},
	{"info", 1, 134222824, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 2, 134222825, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ISerializationSurrogate::GetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ISerializationSurrogate_GetObjectData_m13239_MethodInfo = 
{
	"GetObjectData"/* name */
	, NULL/* method */
	, &ISerializationSurrogate_t2056_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, ISerializationSurrogate_t2056_ISerializationSurrogate_GetObjectData_m13239_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
static const ParameterInfo ISerializationSurrogate_t2056_ISerializationSurrogate_SetObjectData_m13240_ParameterInfos[] = 
{
	{"obj", 0, 134222826, 0, &Object_t_0_0_0},
	{"info", 1, 134222827, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 2, 134222828, 0, &StreamingContext_t1044_0_0_0},
	{"selector", 3, 134222829, 0, &ISurrogateSelector_t1996_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1044_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.ISerializationSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
extern const MethodInfo ISerializationSurrogate_SetObjectData_m13240_MethodInfo = 
{
	"SetObjectData"/* name */
	, NULL/* method */
	, &ISerializationSurrogate_t2056_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1044_Object_t/* invoker_method */
	, ISerializationSurrogate_t2056_ISerializationSurrogate_SetObjectData_m13240_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ISerializationSurrogate_t2056_MethodInfos[] =
{
	&ISerializationSurrogate_GetObjectData_m13239_MethodInfo,
	&ISerializationSurrogate_SetObjectData_m13240_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISerializationSurrogate_t2056_0_0_0;
extern const Il2CppType ISerializationSurrogate_t2056_1_0_0;
struct ISerializationSurrogate_t2056;
const Il2CppTypeDefinitionMetadata ISerializationSurrogate_t2056_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISerializationSurrogate_t2056_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationSurrogate"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, ISerializationSurrogate_t2056_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ISerializationSurrogate_t2056_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 621/* custom_attributes_cache */
	, &ISerializationSurrogate_t2056_0_0_0/* byval_arg */
	, &ISerializationSurrogate_t2056_1_0_0/* this_arg */
	, &ISerializationSurrogate_t2056_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.ISurrogateSelector
extern TypeInfo ISurrogateSelector_t1996_il2cpp_TypeInfo;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType ISurrogateSelector_t1996_1_0_2;
extern const Il2CppType ISurrogateSelector_t1996_1_0_0;
static const ParameterInfo ISurrogateSelector_t1996_ISurrogateSelector_GetSurrogate_m13241_ParameterInfos[] = 
{
	{"type", 0, 134222830, 0, &Type_t_0_0_0},
	{"context", 1, 134222831, 0, &StreamingContext_t1044_0_0_0},
	{"selector", 2, 134222832, 0, &ISurrogateSelector_t1996_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044_ISurrogateSelectorU26_t2759 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Serialization.ISurrogateSelector::GetSurrogate(System.Type,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector&)
extern const MethodInfo ISurrogateSelector_GetSurrogate_m13241_MethodInfo = 
{
	"GetSurrogate"/* name */
	, NULL/* method */
	, &ISurrogateSelector_t1996_il2cpp_TypeInfo/* declaring_type */
	, &ISerializationSurrogate_t2056_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_StreamingContext_t1044_ISurrogateSelectorU26_t2759/* invoker_method */
	, ISurrogateSelector_t1996_ISurrogateSelector_GetSurrogate_m13241_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ISurrogateSelector_t1996_MethodInfos[] =
{
	&ISurrogateSelector_GetSurrogate_m13241_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
struct ISurrogateSelector_t1996;
const Il2CppTypeDefinitionMetadata ISurrogateSelector_t1996_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISurrogateSelector_t1996_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISurrogateSelector"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, ISurrogateSelector_t1996_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ISurrogateSelector_t1996_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 622/* custom_attributes_cache */
	, &ISurrogateSelector_t1996_0_0_0/* byval_arg */
	, &ISurrogateSelector_t1996_1_0_0/* this_arg */
	, &ISurrogateSelector_t1996_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer
#include "mscorlib_System_Runtime_Serialization_ObjectIDGenerator_Inst.h"
// Metadata Definition System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer
extern TypeInfo InstanceComparer_t2048_il2cpp_TypeInfo;
// System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer
#include "mscorlib_System_Runtime_Serialization_ObjectIDGenerator_InstMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer::.ctor()
extern const MethodInfo InstanceComparer__ctor_m10905_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InstanceComparer__ctor_m10905/* method */
	, &InstanceComparer_t2048_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo InstanceComparer_t2048_InstanceComparer_System_Collections_IComparer_Compare_m10906_ParameterInfos[] = 
{
	{"o1", 0, 134222835, 0, &Object_t_0_0_0},
	{"o2", 1, 134222836, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer::System.Collections.IComparer.Compare(System.Object,System.Object)
extern const MethodInfo InstanceComparer_System_Collections_IComparer_Compare_m10906_MethodInfo = 
{
	"System.Collections.IComparer.Compare"/* name */
	, (methodPointerType)&InstanceComparer_System_Collections_IComparer_Compare_m10906/* method */
	, &InstanceComparer_t2048_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Object_t/* invoker_method */
	, InstanceComparer_t2048_InstanceComparer_System_Collections_IComparer_Compare_m10906_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo InstanceComparer_t2048_InstanceComparer_System_Collections_IHashCodeProvider_GetHashCode_m10907_ParameterInfos[] = 
{
	{"o", 0, 134222837, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer::System.Collections.IHashCodeProvider.GetHashCode(System.Object)
extern const MethodInfo InstanceComparer_System_Collections_IHashCodeProvider_GetHashCode_m10907_MethodInfo = 
{
	"System.Collections.IHashCodeProvider.GetHashCode"/* name */
	, (methodPointerType)&InstanceComparer_System_Collections_IHashCodeProvider_GetHashCode_m10907/* method */
	, &InstanceComparer_t2048_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, InstanceComparer_t2048_InstanceComparer_System_Collections_IHashCodeProvider_GetHashCode_m10907_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InstanceComparer_t2048_MethodInfos[] =
{
	&InstanceComparer__ctor_m10905_MethodInfo,
	&InstanceComparer_System_Collections_IComparer_Compare_m10906_MethodInfo,
	&InstanceComparer_System_Collections_IHashCodeProvider_GetHashCode_m10907_MethodInfo,
	NULL
};
extern const MethodInfo InstanceComparer_System_Collections_IComparer_Compare_m10906_MethodInfo;
extern const MethodInfo InstanceComparer_System_Collections_IHashCodeProvider_GetHashCode_m10907_MethodInfo;
static const Il2CppMethodReference InstanceComparer_t2048_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&InstanceComparer_System_Collections_IComparer_Compare_m10906_MethodInfo,
	&InstanceComparer_System_Collections_IHashCodeProvider_GetHashCode_m10907_MethodInfo,
};
static bool InstanceComparer_t2048_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparer_t279_0_0_0;
extern const Il2CppType IHashCodeProvider_t1272_0_0_0;
static const Il2CppType* InstanceComparer_t2048_InterfacesTypeInfos[] = 
{
	&IComparer_t279_0_0_0,
	&IHashCodeProvider_t1272_0_0_0,
};
static Il2CppInterfaceOffsetPair InstanceComparer_t2048_InterfacesOffsets[] = 
{
	{ &IComparer_t279_0_0_0, 4},
	{ &IHashCodeProvider_t1272_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InstanceComparer_t2048_0_0_0;
extern const Il2CppType InstanceComparer_t2048_1_0_0;
extern TypeInfo ObjectIDGenerator_t2040_il2cpp_TypeInfo;
extern const Il2CppType ObjectIDGenerator_t2040_0_0_0;
struct InstanceComparer_t2048;
const Il2CppTypeDefinitionMetadata InstanceComparer_t2048_DefinitionMetadata = 
{
	&ObjectIDGenerator_t2040_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, InstanceComparer_t2048_InterfacesTypeInfos/* implementedInterfaces */
	, InstanceComparer_t2048_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InstanceComparer_t2048_VTable/* vtableMethods */
	, InstanceComparer_t2048_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo InstanceComparer_t2048_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InstanceComparer"/* name */
	, ""/* namespaze */
	, InstanceComparer_t2048_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InstanceComparer_t2048_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InstanceComparer_t2048_0_0_0/* byval_arg */
	, &InstanceComparer_t2048_1_0_0/* this_arg */
	, &InstanceComparer_t2048_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InstanceComparer_t2048)/* instance_size */
	, sizeof (InstanceComparer_t2048)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.ObjectIDGenerator
#include "mscorlib_System_Runtime_Serialization_ObjectIDGenerator.h"
// Metadata Definition System.Runtime.Serialization.ObjectIDGenerator
// System.Runtime.Serialization.ObjectIDGenerator
#include "mscorlib_System_Runtime_Serialization_ObjectIDGeneratorMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectIDGenerator::.ctor()
extern const MethodInfo ObjectIDGenerator__ctor_m10908_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectIDGenerator__ctor_m10908/* method */
	, &ObjectIDGenerator_t2040_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectIDGenerator::.cctor()
extern const MethodInfo ObjectIDGenerator__cctor_m10909_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ObjectIDGenerator__cctor_m10909/* method */
	, &ObjectIDGenerator_t2040_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t273_1_0_2;
static const ParameterInfo ObjectIDGenerator_t2040_ObjectIDGenerator_GetId_m10910_ParameterInfos[] = 
{
	{"obj", 0, 134222833, 0, &Object_t_0_0_0},
	{"firstTime", 1, 134222834, 0, &Boolean_t273_1_0_2},
};
extern void* RuntimeInvoker_Int64_t1071_Object_t_BooleanU26_t745 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Runtime.Serialization.ObjectIDGenerator::GetId(System.Object,System.Boolean&)
extern const MethodInfo ObjectIDGenerator_GetId_m10910_MethodInfo = 
{
	"GetId"/* name */
	, (methodPointerType)&ObjectIDGenerator_GetId_m10910/* method */
	, &ObjectIDGenerator_t2040_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1071_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1071_Object_t_BooleanU26_t745/* invoker_method */
	, ObjectIDGenerator_t2040_ObjectIDGenerator_GetId_m10910_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Runtime.Serialization.ObjectIDGenerator::get_NextId()
extern const MethodInfo ObjectIDGenerator_get_NextId_m10911_MethodInfo = 
{
	"get_NextId"/* name */
	, (methodPointerType)&ObjectIDGenerator_get_NextId_m10911/* method */
	, &ObjectIDGenerator_t2040_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1071_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1071/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectIDGenerator_t2040_MethodInfos[] =
{
	&ObjectIDGenerator__ctor_m10908_MethodInfo,
	&ObjectIDGenerator__cctor_m10909_MethodInfo,
	&ObjectIDGenerator_GetId_m10910_MethodInfo,
	&ObjectIDGenerator_get_NextId_m10911_MethodInfo,
	NULL
};
extern const MethodInfo ObjectIDGenerator_get_NextId_m10911_MethodInfo;
static const PropertyInfo ObjectIDGenerator_t2040____NextId_PropertyInfo = 
{
	&ObjectIDGenerator_t2040_il2cpp_TypeInfo/* parent */
	, "NextId"/* name */
	, &ObjectIDGenerator_get_NextId_m10911_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectIDGenerator_t2040_PropertyInfos[] =
{
	&ObjectIDGenerator_t2040____NextId_PropertyInfo,
	NULL
};
static const Il2CppType* ObjectIDGenerator_t2040_il2cpp_TypeInfo__nestedTypes[1] =
{
	&InstanceComparer_t2048_0_0_0,
};
extern const MethodInfo ObjectIDGenerator_GetId_m10910_MethodInfo;
static const Il2CppMethodReference ObjectIDGenerator_t2040_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ObjectIDGenerator_GetId_m10910_MethodInfo,
};
static bool ObjectIDGenerator_t2040_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectIDGenerator_t2040_1_0_0;
struct ObjectIDGenerator_t2040;
const Il2CppTypeDefinitionMetadata ObjectIDGenerator_t2040_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ObjectIDGenerator_t2040_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectIDGenerator_t2040_VTable/* vtableMethods */
	, ObjectIDGenerator_t2040_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2220/* fieldStart */

};
TypeInfo ObjectIDGenerator_t2040_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectIDGenerator"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, ObjectIDGenerator_t2040_MethodInfos/* methods */
	, ObjectIDGenerator_t2040_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectIDGenerator_t2040_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 623/* custom_attributes_cache */
	, &ObjectIDGenerator_t2040_0_0_0/* byval_arg */
	, &ObjectIDGenerator_t2040_1_0_0/* this_arg */
	, &ObjectIDGenerator_t2040_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectIDGenerator_t2040)/* instance_size */
	, sizeof (ObjectIDGenerator_t2040)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ObjectIDGenerator_t2040_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.ObjectManager
#include "mscorlib_System_Runtime_Serialization_ObjectManager.h"
// Metadata Definition System.Runtime.Serialization.ObjectManager
extern TypeInfo ObjectManager_t2033_il2cpp_TypeInfo;
// System.Runtime.Serialization.ObjectManager
#include "mscorlib_System_Runtime_Serialization_ObjectManagerMethodDeclarations.h"
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager__ctor_m10912_ParameterInfos[] = 
{
	{"selector", 0, 134222838, 0, &ISurrogateSelector_t1996_0_0_0},
	{"context", 1, 134222839, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::.ctor(System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjectManager__ctor_m10912_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectManager__ctor_m10912/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, ObjectManager_t2033_ObjectManager__ctor_m10912_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::DoFixups()
extern const MethodInfo ObjectManager_DoFixups_m10913_MethodInfo = 
{
	"DoFixups"/* name */
	, (methodPointerType)&ObjectManager_DoFixups_m10913/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager_GetObjectRecord_m10914_ParameterInfos[] = 
{
	{"objectID", 0, 134222840, 0, &Int64_t1071_0_0_0},
};
extern const Il2CppType ObjectRecord_t2049_0_0_0;
extern void* RuntimeInvoker_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ObjectRecord System.Runtime.Serialization.ObjectManager::GetObjectRecord(System.Int64)
extern const MethodInfo ObjectManager_GetObjectRecord_m10914_MethodInfo = 
{
	"GetObjectRecord"/* name */
	, (methodPointerType)&ObjectManager_GetObjectRecord_m10914/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &ObjectRecord_t2049_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t1071/* invoker_method */
	, ObjectManager_t2033_ObjectManager_GetObjectRecord_m10914_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager_GetObject_m10915_ParameterInfos[] = 
{
	{"objectID", 0, 134222841, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.ObjectManager::GetObject(System.Int64)
extern const MethodInfo ObjectManager_GetObject_m10915_MethodInfo = 
{
	"GetObject"/* name */
	, (methodPointerType)&ObjectManager_GetObject_m10915/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t1071/* invoker_method */
	, ObjectManager_t2033_ObjectManager_GetObject_m10915_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::RaiseDeserializationEvent()
extern const MethodInfo ObjectManager_RaiseDeserializationEvent_m10916_MethodInfo = 
{
	"RaiseDeserializationEvent"/* name */
	, (methodPointerType)&ObjectManager_RaiseDeserializationEvent_m10916/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager_RaiseOnDeserializingEvent_m10917_ParameterInfos[] = 
{
	{"obj", 0, 134222842, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::RaiseOnDeserializingEvent(System.Object)
extern const MethodInfo ObjectManager_RaiseOnDeserializingEvent_m10917_MethodInfo = 
{
	"RaiseOnDeserializingEvent"/* name */
	, (methodPointerType)&ObjectManager_RaiseOnDeserializingEvent_m10917/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjectManager_t2033_ObjectManager_RaiseOnDeserializingEvent_m10917_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager_RaiseOnDeserializedEvent_m10918_ParameterInfos[] = 
{
	{"obj", 0, 134222843, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::RaiseOnDeserializedEvent(System.Object)
extern const MethodInfo ObjectManager_RaiseOnDeserializedEvent_m10918_MethodInfo = 
{
	"RaiseOnDeserializedEvent"/* name */
	, (methodPointerType)&ObjectManager_RaiseOnDeserializedEvent_m10918/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjectManager_t2033_ObjectManager_RaiseOnDeserializedEvent_m10918_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseFixupRecord_t2050_0_0_0;
extern const Il2CppType BaseFixupRecord_t2050_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager_AddFixup_m10919_ParameterInfos[] = 
{
	{"record", 0, 134222844, 0, &BaseFixupRecord_t2050_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::AddFixup(System.Runtime.Serialization.BaseFixupRecord)
extern const MethodInfo ObjectManager_AddFixup_m10919_MethodInfo = 
{
	"AddFixup"/* name */
	, (methodPointerType)&ObjectManager_AddFixup_m10919/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjectManager_t2033_ObjectManager_AddFixup_m10919_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager_RecordArrayElementFixup_m10920_ParameterInfos[] = 
{
	{"arrayToBeFixed", 0, 134222845, 0, &Int64_t1071_0_0_0},
	{"index", 1, 134222846, 0, &Int32_t253_0_0_0},
	{"objectRequired", 2, 134222847, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int64_t1071_Int32_t253_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::RecordArrayElementFixup(System.Int64,System.Int32,System.Int64)
extern const MethodInfo ObjectManager_RecordArrayElementFixup_m10920_MethodInfo = 
{
	"RecordArrayElementFixup"/* name */
	, (methodPointerType)&ObjectManager_RecordArrayElementFixup_m10920/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int64_t1071_Int32_t253_Int64_t1071/* invoker_method */
	, ObjectManager_t2033_ObjectManager_RecordArrayElementFixup_m10920_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager_RecordArrayElementFixup_m10921_ParameterInfos[] = 
{
	{"arrayToBeFixed", 0, 134222848, 0, &Int64_t1071_0_0_0},
	{"indices", 1, 134222849, 0, &Int32U5BU5D_t242_0_0_0},
	{"objectRequired", 2, 134222850, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::RecordArrayElementFixup(System.Int64,System.Int32[],System.Int64)
extern const MethodInfo ObjectManager_RecordArrayElementFixup_m10921_MethodInfo = 
{
	"RecordArrayElementFixup"/* name */
	, (methodPointerType)&ObjectManager_RecordArrayElementFixup_m10921/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Int64_t1071/* invoker_method */
	, ObjectManager_t2033_ObjectManager_RecordArrayElementFixup_m10921_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager_RecordDelayedFixup_m10922_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222851, 0, &Int64_t1071_0_0_0},
	{"memberName", 1, 134222852, 0, &String_t_0_0_0},
	{"objectRequired", 2, 134222853, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::RecordDelayedFixup(System.Int64,System.String,System.Int64)
extern const MethodInfo ObjectManager_RecordDelayedFixup_m10922_MethodInfo = 
{
	"RecordDelayedFixup"/* name */
	, (methodPointerType)&ObjectManager_RecordDelayedFixup_m10922/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Int64_t1071/* invoker_method */
	, ObjectManager_t2033_ObjectManager_RecordDelayedFixup_m10922_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager_RecordFixup_m10923_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222854, 0, &Int64_t1071_0_0_0},
	{"member", 1, 134222855, 0, &MemberInfo_t_0_0_0},
	{"objectRequired", 2, 134222856, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::RecordFixup(System.Int64,System.Reflection.MemberInfo,System.Int64)
extern const MethodInfo ObjectManager_RecordFixup_m10923_MethodInfo = 
{
	"RecordFixup"/* name */
	, (methodPointerType)&ObjectManager_RecordFixup_m10923/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int64_t1071_Object_t_Int64_t1071/* invoker_method */
	, ObjectManager_t2033_ObjectManager_RecordFixup_m10923_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectRecord_t2049_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager_RegisterObjectInternal_m10924_ParameterInfos[] = 
{
	{"obj", 0, 134222857, 0, &Object_t_0_0_0},
	{"record", 1, 134222858, 0, &ObjectRecord_t2049_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::RegisterObjectInternal(System.Object,System.Runtime.Serialization.ObjectRecord)
extern const MethodInfo ObjectManager_RegisterObjectInternal_m10924_MethodInfo = 
{
	"RegisterObjectInternal"/* name */
	, (methodPointerType)&ObjectManager_RegisterObjectInternal_m10924/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ObjectManager_t2033_ObjectManager_RegisterObjectInternal_m10924_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
static const ParameterInfo ObjectManager_t2033_ObjectManager_RegisterObject_m10925_ParameterInfos[] = 
{
	{"obj", 0, 134222859, 0, &Object_t_0_0_0},
	{"objectID", 1, 134222860, 0, &Int64_t1071_0_0_0},
	{"info", 2, 134222861, 0, &SerializationInfo_t1043_0_0_0},
	{"idOfContainingObj", 3, 134222862, 0, &Int64_t1071_0_0_0},
	{"member", 4, 134222863, 0, &MemberInfo_t_0_0_0},
	{"arrayIndex", 5, 134222864, 0, &Int32U5BU5D_t242_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t_Int64_t1071_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectManager::RegisterObject(System.Object,System.Int64,System.Runtime.Serialization.SerializationInfo,System.Int64,System.Reflection.MemberInfo,System.Int32[])
extern const MethodInfo ObjectManager_RegisterObject_m10925_MethodInfo = 
{
	"RegisterObject"/* name */
	, (methodPointerType)&ObjectManager_RegisterObject_m10925/* method */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071_Object_t_Int64_t1071_Object_t_Object_t/* invoker_method */
	, ObjectManager_t2033_ObjectManager_RegisterObject_m10925_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectManager_t2033_MethodInfos[] =
{
	&ObjectManager__ctor_m10912_MethodInfo,
	&ObjectManager_DoFixups_m10913_MethodInfo,
	&ObjectManager_GetObjectRecord_m10914_MethodInfo,
	&ObjectManager_GetObject_m10915_MethodInfo,
	&ObjectManager_RaiseDeserializationEvent_m10916_MethodInfo,
	&ObjectManager_RaiseOnDeserializingEvent_m10917_MethodInfo,
	&ObjectManager_RaiseOnDeserializedEvent_m10918_MethodInfo,
	&ObjectManager_AddFixup_m10919_MethodInfo,
	&ObjectManager_RecordArrayElementFixup_m10920_MethodInfo,
	&ObjectManager_RecordArrayElementFixup_m10921_MethodInfo,
	&ObjectManager_RecordDelayedFixup_m10922_MethodInfo,
	&ObjectManager_RecordFixup_m10923_MethodInfo,
	&ObjectManager_RegisterObjectInternal_m10924_MethodInfo,
	&ObjectManager_RegisterObject_m10925_MethodInfo,
	NULL
};
extern const MethodInfo ObjectManager_DoFixups_m10913_MethodInfo;
extern const MethodInfo ObjectManager_GetObject_m10915_MethodInfo;
extern const MethodInfo ObjectManager_RaiseDeserializationEvent_m10916_MethodInfo;
extern const MethodInfo ObjectManager_RecordArrayElementFixup_m10920_MethodInfo;
extern const MethodInfo ObjectManager_RecordArrayElementFixup_m10921_MethodInfo;
extern const MethodInfo ObjectManager_RecordDelayedFixup_m10922_MethodInfo;
extern const MethodInfo ObjectManager_RecordFixup_m10923_MethodInfo;
static const Il2CppMethodReference ObjectManager_t2033_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ObjectManager_DoFixups_m10913_MethodInfo,
	&ObjectManager_GetObject_m10915_MethodInfo,
	&ObjectManager_RaiseDeserializationEvent_m10916_MethodInfo,
	&ObjectManager_RecordArrayElementFixup_m10920_MethodInfo,
	&ObjectManager_RecordArrayElementFixup_m10921_MethodInfo,
	&ObjectManager_RecordDelayedFixup_m10922_MethodInfo,
	&ObjectManager_RecordFixup_m10923_MethodInfo,
};
static bool ObjectManager_t2033_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectManager_t2033_0_0_0;
extern const Il2CppType ObjectManager_t2033_1_0_0;
struct ObjectManager_t2033;
const Il2CppTypeDefinitionMetadata ObjectManager_t2033_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectManager_t2033_VTable/* vtableMethods */
	, ObjectManager_t2033_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2223/* fieldStart */

};
TypeInfo ObjectManager_t2033_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectManager"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, ObjectManager_t2033_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ObjectManager_t2033_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 624/* custom_attributes_cache */
	, &ObjectManager_t2033_0_0_0/* byval_arg */
	, &ObjectManager_t2033_1_0_0/* this_arg */
	, &ObjectManager_t2033_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectManager_t2033)/* instance_size */
	, sizeof (ObjectManager_t2033)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.BaseFixupRecord
extern TypeInfo BaseFixupRecord_t2050_il2cpp_TypeInfo;
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecordMethodDeclarations.h"
extern const Il2CppType ObjectRecord_t2049_0_0_0;
extern const Il2CppType ObjectRecord_t2049_0_0_0;
static const ParameterInfo BaseFixupRecord_t2050_BaseFixupRecord__ctor_m10926_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222865, 0, &ObjectRecord_t2049_0_0_0},
	{"objectRequired", 1, 134222866, 0, &ObjectRecord_t2049_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.BaseFixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.Runtime.Serialization.ObjectRecord)
extern const MethodInfo BaseFixupRecord__ctor_m10926_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseFixupRecord__ctor_m10926/* method */
	, &BaseFixupRecord_t2050_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, BaseFixupRecord_t2050_BaseFixupRecord__ctor_m10926_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectManager_t2033_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo BaseFixupRecord_t2050_BaseFixupRecord_DoFixup_m10927_ParameterInfos[] = 
{
	{"manager", 0, 134222867, 0, &ObjectManager_t2033_0_0_0},
	{"strict", 1, 134222868, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.BaseFixupRecord::DoFixup(System.Runtime.Serialization.ObjectManager,System.Boolean)
extern const MethodInfo BaseFixupRecord_DoFixup_m10927_MethodInfo = 
{
	"DoFixup"/* name */
	, (methodPointerType)&BaseFixupRecord_DoFixup_m10927/* method */
	, &BaseFixupRecord_t2050_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_SByte_t274/* invoker_method */
	, BaseFixupRecord_t2050_BaseFixupRecord_DoFixup_m10927_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectManager_t2033_0_0_0;
static const ParameterInfo BaseFixupRecord_t2050_BaseFixupRecord_FixupImpl_m13242_ParameterInfos[] = 
{
	{"manager", 0, 134222869, 0, &ObjectManager_t2033_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.BaseFixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
extern const MethodInfo BaseFixupRecord_FixupImpl_m13242_MethodInfo = 
{
	"FixupImpl"/* name */
	, NULL/* method */
	, &BaseFixupRecord_t2050_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, BaseFixupRecord_t2050_BaseFixupRecord_FixupImpl_m13242_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BaseFixupRecord_t2050_MethodInfos[] =
{
	&BaseFixupRecord__ctor_m10926_MethodInfo,
	&BaseFixupRecord_DoFixup_m10927_MethodInfo,
	&BaseFixupRecord_FixupImpl_m13242_MethodInfo,
	NULL
};
static const Il2CppMethodReference BaseFixupRecord_t2050_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
};
static bool BaseFixupRecord_t2050_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BaseFixupRecord_t2050_1_0_0;
struct BaseFixupRecord_t2050;
const Il2CppTypeDefinitionMetadata BaseFixupRecord_t2050_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseFixupRecord_t2050_VTable/* vtableMethods */
	, BaseFixupRecord_t2050_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2232/* fieldStart */

};
TypeInfo BaseFixupRecord_t2050_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, BaseFixupRecord_t2050_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BaseFixupRecord_t2050_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseFixupRecord_t2050_0_0_0/* byval_arg */
	, &BaseFixupRecord_t2050_1_0_0/* this_arg */
	, &BaseFixupRecord_t2050_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseFixupRecord_t2050)/* instance_size */
	, sizeof (BaseFixupRecord_t2050)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.ArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_ArrayFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.ArrayFixupRecord
extern TypeInfo ArrayFixupRecord_t2051_il2cpp_TypeInfo;
// System.Runtime.Serialization.ArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_ArrayFixupRecordMethodDeclarations.h"
extern const Il2CppType ObjectRecord_t2049_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType ObjectRecord_t2049_0_0_0;
static const ParameterInfo ArrayFixupRecord_t2051_ArrayFixupRecord__ctor_m10928_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222870, 0, &ObjectRecord_t2049_0_0_0},
	{"index", 1, 134222871, 0, &Int32_t253_0_0_0},
	{"objectRequired", 2, 134222872, 0, &ObjectRecord_t2049_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ArrayFixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.Int32,System.Runtime.Serialization.ObjectRecord)
extern const MethodInfo ArrayFixupRecord__ctor_m10928_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArrayFixupRecord__ctor_m10928/* method */
	, &ArrayFixupRecord_t2051_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t/* invoker_method */
	, ArrayFixupRecord_t2051_ArrayFixupRecord__ctor_m10928_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectManager_t2033_0_0_0;
static const ParameterInfo ArrayFixupRecord_t2051_ArrayFixupRecord_FixupImpl_m10929_ParameterInfos[] = 
{
	{"manager", 0, 134222873, 0, &ObjectManager_t2033_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ArrayFixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
extern const MethodInfo ArrayFixupRecord_FixupImpl_m10929_MethodInfo = 
{
	"FixupImpl"/* name */
	, (methodPointerType)&ArrayFixupRecord_FixupImpl_m10929/* method */
	, &ArrayFixupRecord_t2051_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ArrayFixupRecord_t2051_ArrayFixupRecord_FixupImpl_m10929_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ArrayFixupRecord_t2051_MethodInfos[] =
{
	&ArrayFixupRecord__ctor_m10928_MethodInfo,
	&ArrayFixupRecord_FixupImpl_m10929_MethodInfo,
	NULL
};
extern const MethodInfo ArrayFixupRecord_FixupImpl_m10929_MethodInfo;
static const Il2CppMethodReference ArrayFixupRecord_t2051_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&ArrayFixupRecord_FixupImpl_m10929_MethodInfo,
};
static bool ArrayFixupRecord_t2051_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayFixupRecord_t2051_0_0_0;
extern const Il2CppType ArrayFixupRecord_t2051_1_0_0;
struct ArrayFixupRecord_t2051;
const Il2CppTypeDefinitionMetadata ArrayFixupRecord_t2051_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t2050_0_0_0/* parent */
	, ArrayFixupRecord_t2051_VTable/* vtableMethods */
	, ArrayFixupRecord_t2051_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2236/* fieldStart */

};
TypeInfo ArrayFixupRecord_t2051_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, ArrayFixupRecord_t2051_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ArrayFixupRecord_t2051_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArrayFixupRecord_t2051_0_0_0/* byval_arg */
	, &ArrayFixupRecord_t2051_1_0_0/* this_arg */
	, &ArrayFixupRecord_t2051_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayFixupRecord_t2051)/* instance_size */
	, sizeof (ArrayFixupRecord_t2051)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.MultiArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_MultiArrayFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.MultiArrayFixupRecord
extern TypeInfo MultiArrayFixupRecord_t2052_il2cpp_TypeInfo;
// System.Runtime.Serialization.MultiArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_MultiArrayFixupRecordMethodDeclarations.h"
extern const Il2CppType ObjectRecord_t2049_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
extern const Il2CppType ObjectRecord_t2049_0_0_0;
static const ParameterInfo MultiArrayFixupRecord_t2052_MultiArrayFixupRecord__ctor_m10930_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222874, 0, &ObjectRecord_t2049_0_0_0},
	{"indices", 1, 134222875, 0, &Int32U5BU5D_t242_0_0_0},
	{"objectRequired", 2, 134222876, 0, &ObjectRecord_t2049_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.MultiArrayFixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.Int32[],System.Runtime.Serialization.ObjectRecord)
extern const MethodInfo MultiArrayFixupRecord__ctor_m10930_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MultiArrayFixupRecord__ctor_m10930/* method */
	, &MultiArrayFixupRecord_t2052_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, MultiArrayFixupRecord_t2052_MultiArrayFixupRecord__ctor_m10930_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectManager_t2033_0_0_0;
static const ParameterInfo MultiArrayFixupRecord_t2052_MultiArrayFixupRecord_FixupImpl_m10931_ParameterInfos[] = 
{
	{"manager", 0, 134222877, 0, &ObjectManager_t2033_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.MultiArrayFixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
extern const MethodInfo MultiArrayFixupRecord_FixupImpl_m10931_MethodInfo = 
{
	"FixupImpl"/* name */
	, (methodPointerType)&MultiArrayFixupRecord_FixupImpl_m10931/* method */
	, &MultiArrayFixupRecord_t2052_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, MultiArrayFixupRecord_t2052_MultiArrayFixupRecord_FixupImpl_m10931_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MultiArrayFixupRecord_t2052_MethodInfos[] =
{
	&MultiArrayFixupRecord__ctor_m10930_MethodInfo,
	&MultiArrayFixupRecord_FixupImpl_m10931_MethodInfo,
	NULL
};
extern const MethodInfo MultiArrayFixupRecord_FixupImpl_m10931_MethodInfo;
static const Il2CppMethodReference MultiArrayFixupRecord_t2052_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MultiArrayFixupRecord_FixupImpl_m10931_MethodInfo,
};
static bool MultiArrayFixupRecord_t2052_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MultiArrayFixupRecord_t2052_0_0_0;
extern const Il2CppType MultiArrayFixupRecord_t2052_1_0_0;
struct MultiArrayFixupRecord_t2052;
const Il2CppTypeDefinitionMetadata MultiArrayFixupRecord_t2052_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t2050_0_0_0/* parent */
	, MultiArrayFixupRecord_t2052_VTable/* vtableMethods */
	, MultiArrayFixupRecord_t2052_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2237/* fieldStart */

};
TypeInfo MultiArrayFixupRecord_t2052_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MultiArrayFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, MultiArrayFixupRecord_t2052_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MultiArrayFixupRecord_t2052_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MultiArrayFixupRecord_t2052_0_0_0/* byval_arg */
	, &MultiArrayFixupRecord_t2052_1_0_0/* this_arg */
	, &MultiArrayFixupRecord_t2052_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MultiArrayFixupRecord_t2052)/* instance_size */
	, sizeof (MultiArrayFixupRecord_t2052)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.FixupRecord
#include "mscorlib_System_Runtime_Serialization_FixupRecord.h"
// Metadata Definition System.Runtime.Serialization.FixupRecord
extern TypeInfo FixupRecord_t2053_il2cpp_TypeInfo;
// System.Runtime.Serialization.FixupRecord
#include "mscorlib_System_Runtime_Serialization_FixupRecordMethodDeclarations.h"
extern const Il2CppType ObjectRecord_t2049_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType ObjectRecord_t2049_0_0_0;
static const ParameterInfo FixupRecord_t2053_FixupRecord__ctor_m10932_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222878, 0, &ObjectRecord_t2049_0_0_0},
	{"member", 1, 134222879, 0, &MemberInfo_t_0_0_0},
	{"objectRequired", 2, 134222880, 0, &ObjectRecord_t2049_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.FixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.Reflection.MemberInfo,System.Runtime.Serialization.ObjectRecord)
extern const MethodInfo FixupRecord__ctor_m10932_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FixupRecord__ctor_m10932/* method */
	, &FixupRecord_t2053_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, FixupRecord_t2053_FixupRecord__ctor_m10932_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectManager_t2033_0_0_0;
static const ParameterInfo FixupRecord_t2053_FixupRecord_FixupImpl_m10933_ParameterInfos[] = 
{
	{"manager", 0, 134222881, 0, &ObjectManager_t2033_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.FixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
extern const MethodInfo FixupRecord_FixupImpl_m10933_MethodInfo = 
{
	"FixupImpl"/* name */
	, (methodPointerType)&FixupRecord_FixupImpl_m10933/* method */
	, &FixupRecord_t2053_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, FixupRecord_t2053_FixupRecord_FixupImpl_m10933_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FixupRecord_t2053_MethodInfos[] =
{
	&FixupRecord__ctor_m10932_MethodInfo,
	&FixupRecord_FixupImpl_m10933_MethodInfo,
	NULL
};
extern const MethodInfo FixupRecord_FixupImpl_m10933_MethodInfo;
static const Il2CppMethodReference FixupRecord_t2053_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&FixupRecord_FixupImpl_m10933_MethodInfo,
};
static bool FixupRecord_t2053_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FixupRecord_t2053_0_0_0;
extern const Il2CppType FixupRecord_t2053_1_0_0;
struct FixupRecord_t2053;
const Il2CppTypeDefinitionMetadata FixupRecord_t2053_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t2050_0_0_0/* parent */
	, FixupRecord_t2053_VTable/* vtableMethods */
	, FixupRecord_t2053_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2238/* fieldStart */

};
TypeInfo FixupRecord_t2053_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, FixupRecord_t2053_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FixupRecord_t2053_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FixupRecord_t2053_0_0_0/* byval_arg */
	, &FixupRecord_t2053_1_0_0/* this_arg */
	, &FixupRecord_t2053_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FixupRecord_t2053)/* instance_size */
	, sizeof (FixupRecord_t2053)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.DelayedFixupRecord
#include "mscorlib_System_Runtime_Serialization_DelayedFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.DelayedFixupRecord
extern TypeInfo DelayedFixupRecord_t2054_il2cpp_TypeInfo;
// System.Runtime.Serialization.DelayedFixupRecord
#include "mscorlib_System_Runtime_Serialization_DelayedFixupRecordMethodDeclarations.h"
extern const Il2CppType ObjectRecord_t2049_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjectRecord_t2049_0_0_0;
static const ParameterInfo DelayedFixupRecord_t2054_DelayedFixupRecord__ctor_m10934_ParameterInfos[] = 
{
	{"objectToBeFixed", 0, 134222882, 0, &ObjectRecord_t2049_0_0_0},
	{"memberName", 1, 134222883, 0, &String_t_0_0_0},
	{"objectRequired", 2, 134222884, 0, &ObjectRecord_t2049_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.DelayedFixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.String,System.Runtime.Serialization.ObjectRecord)
extern const MethodInfo DelayedFixupRecord__ctor_m10934_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DelayedFixupRecord__ctor_m10934/* method */
	, &DelayedFixupRecord_t2054_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, DelayedFixupRecord_t2054_DelayedFixupRecord__ctor_m10934_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectManager_t2033_0_0_0;
static const ParameterInfo DelayedFixupRecord_t2054_DelayedFixupRecord_FixupImpl_m10935_ParameterInfos[] = 
{
	{"manager", 0, 134222885, 0, &ObjectManager_t2033_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.DelayedFixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
extern const MethodInfo DelayedFixupRecord_FixupImpl_m10935_MethodInfo = 
{
	"FixupImpl"/* name */
	, (methodPointerType)&DelayedFixupRecord_FixupImpl_m10935/* method */
	, &DelayedFixupRecord_t2054_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DelayedFixupRecord_t2054_DelayedFixupRecord_FixupImpl_m10935_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DelayedFixupRecord_t2054_MethodInfos[] =
{
	&DelayedFixupRecord__ctor_m10934_MethodInfo,
	&DelayedFixupRecord_FixupImpl_m10935_MethodInfo,
	NULL
};
extern const MethodInfo DelayedFixupRecord_FixupImpl_m10935_MethodInfo;
static const Il2CppMethodReference DelayedFixupRecord_t2054_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&DelayedFixupRecord_FixupImpl_m10935_MethodInfo,
};
static bool DelayedFixupRecord_t2054_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DelayedFixupRecord_t2054_0_0_0;
extern const Il2CppType DelayedFixupRecord_t2054_1_0_0;
struct DelayedFixupRecord_t2054;
const Il2CppTypeDefinitionMetadata DelayedFixupRecord_t2054_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t2050_0_0_0/* parent */
	, DelayedFixupRecord_t2054_VTable/* vtableMethods */
	, DelayedFixupRecord_t2054_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2239/* fieldStart */

};
TypeInfo DelayedFixupRecord_t2054_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DelayedFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, DelayedFixupRecord_t2054_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DelayedFixupRecord_t2054_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DelayedFixupRecord_t2054_0_0_0/* byval_arg */
	, &DelayedFixupRecord_t2054_1_0_0/* this_arg */
	, &DelayedFixupRecord_t2054_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DelayedFixupRecord_t2054)/* instance_size */
	, sizeof (DelayedFixupRecord_t2054)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.ObjectRecordStatus
#include "mscorlib_System_Runtime_Serialization_ObjectRecordStatus.h"
// Metadata Definition System.Runtime.Serialization.ObjectRecordStatus
extern TypeInfo ObjectRecordStatus_t2055_il2cpp_TypeInfo;
// System.Runtime.Serialization.ObjectRecordStatus
#include "mscorlib_System_Runtime_Serialization_ObjectRecordStatusMethodDeclarations.h"
static const MethodInfo* ObjectRecordStatus_t2055_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ObjectRecordStatus_t2055_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ObjectRecordStatus_t2055_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ObjectRecordStatus_t2055_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectRecordStatus_t2055_0_0_0;
extern const Il2CppType ObjectRecordStatus_t2055_1_0_0;
const Il2CppTypeDefinitionMetadata ObjectRecordStatus_t2055_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObjectRecordStatus_t2055_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ObjectRecordStatus_t2055_VTable/* vtableMethods */
	, ObjectRecordStatus_t2055_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2240/* fieldStart */

};
TypeInfo ObjectRecordStatus_t2055_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectRecordStatus"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, ObjectRecordStatus_t2055_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectRecordStatus_t2055_0_0_0/* byval_arg */
	, &ObjectRecordStatus_t2055_1_0_0/* this_arg */
	, &ObjectRecordStatus_t2055_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectRecordStatus_t2055)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ObjectRecordStatus_t2055)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.ObjectRecord
#include "mscorlib_System_Runtime_Serialization_ObjectRecord.h"
// Metadata Definition System.Runtime.Serialization.ObjectRecord
extern TypeInfo ObjectRecord_t2049_il2cpp_TypeInfo;
// System.Runtime.Serialization.ObjectRecord
#include "mscorlib_System_Runtime_Serialization_ObjectRecordMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectRecord::.ctor()
extern const MethodInfo ObjectRecord__ctor_m10936_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectRecord__ctor_m10936/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectManager_t2033_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ObjectRecord_t2049_ObjectRecord_SetMemberValue_m10937_ParameterInfos[] = 
{
	{"manager", 0, 134222886, 0, &ObjectManager_t2033_0_0_0},
	{"member", 1, 134222887, 0, &MemberInfo_t_0_0_0},
	{"value", 2, 134222888, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectRecord::SetMemberValue(System.Runtime.Serialization.ObjectManager,System.Reflection.MemberInfo,System.Object)
extern const MethodInfo ObjectRecord_SetMemberValue_m10937_MethodInfo = 
{
	"SetMemberValue"/* name */
	, (methodPointerType)&ObjectRecord_SetMemberValue_m10937/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectRecord_t2049_ObjectRecord_SetMemberValue_m10937_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectManager_t2033_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
static const ParameterInfo ObjectRecord_t2049_ObjectRecord_SetArrayValue_m10938_ParameterInfos[] = 
{
	{"manager", 0, 134222889, 0, &ObjectManager_t2033_0_0_0},
	{"value", 1, 134222890, 0, &Object_t_0_0_0},
	{"indices", 2, 134222891, 0, &Int32U5BU5D_t242_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectRecord::SetArrayValue(System.Runtime.Serialization.ObjectManager,System.Object,System.Int32[])
extern const MethodInfo ObjectRecord_SetArrayValue_m10938_MethodInfo = 
{
	"SetArrayValue"/* name */
	, (methodPointerType)&ObjectRecord_SetArrayValue_m10938/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectRecord_t2049_ObjectRecord_SetArrayValue_m10938_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectManager_t2033_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ObjectRecord_t2049_ObjectRecord_SetMemberValue_m10939_ParameterInfos[] = 
{
	{"manager", 0, 134222892, 0, &ObjectManager_t2033_0_0_0},
	{"memberName", 1, 134222893, 0, &String_t_0_0_0},
	{"value", 2, 134222894, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectRecord::SetMemberValue(System.Runtime.Serialization.ObjectManager,System.String,System.Object)
extern const MethodInfo ObjectRecord_SetMemberValue_m10939_MethodInfo = 
{
	"SetMemberValue"/* name */
	, (methodPointerType)&ObjectRecord_SetMemberValue_m10939/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectRecord_t2049_ObjectRecord_SetMemberValue_m10939_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.ObjectRecord::get_IsInstanceReady()
extern const MethodInfo ObjectRecord_get_IsInstanceReady_m10940_MethodInfo = 
{
	"get_IsInstanceReady"/* name */
	, (methodPointerType)&ObjectRecord_get_IsInstanceReady_m10940/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.ObjectRecord::get_IsUnsolvedObjectReference()
extern const MethodInfo ObjectRecord_get_IsUnsolvedObjectReference_m10941_MethodInfo = 
{
	"get_IsUnsolvedObjectReference"/* name */
	, (methodPointerType)&ObjectRecord_get_IsUnsolvedObjectReference_m10941/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.ObjectRecord::get_IsRegistered()
extern const MethodInfo ObjectRecord_get_IsRegistered_m10942_MethodInfo = 
{
	"get_IsRegistered"/* name */
	, (methodPointerType)&ObjectRecord_get_IsRegistered_m10942/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType ObjectManager_t2033_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ObjectRecord_t2049_ObjectRecord_DoFixups_m10943_ParameterInfos[] = 
{
	{"asContainer", 0, 134222895, 0, &Boolean_t273_0_0_0},
	{"manager", 1, 134222896, 0, &ObjectManager_t2033_0_0_0},
	{"strict", 2, 134222897, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_SByte_t274_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.ObjectRecord::DoFixups(System.Boolean,System.Runtime.Serialization.ObjectManager,System.Boolean)
extern const MethodInfo ObjectRecord_DoFixups_m10943_MethodInfo = 
{
	"DoFixups"/* name */
	, (methodPointerType)&ObjectRecord_DoFixups_m10943/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_SByte_t274_Object_t_SByte_t274/* invoker_method */
	, ObjectRecord_t2049_ObjectRecord_DoFixups_m10943_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseFixupRecord_t2050_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ObjectRecord_t2049_ObjectRecord_RemoveFixup_m10944_ParameterInfos[] = 
{
	{"fixupToRemove", 0, 134222898, 0, &BaseFixupRecord_t2050_0_0_0},
	{"asContainer", 1, 134222899, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectRecord::RemoveFixup(System.Runtime.Serialization.BaseFixupRecord,System.Boolean)
extern const MethodInfo ObjectRecord_RemoveFixup_m10944_MethodInfo = 
{
	"RemoveFixup"/* name */
	, (methodPointerType)&ObjectRecord_RemoveFixup_m10944/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, ObjectRecord_t2049_ObjectRecord_RemoveFixup_m10944_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseFixupRecord_t2050_0_0_0;
extern const Il2CppType BaseFixupRecord_t2050_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ObjectRecord_t2049_ObjectRecord_UnchainFixup_m10945_ParameterInfos[] = 
{
	{"fixup", 0, 134222900, 0, &BaseFixupRecord_t2050_0_0_0},
	{"prevFixup", 1, 134222901, 0, &BaseFixupRecord_t2050_0_0_0},
	{"asContainer", 2, 134222902, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectRecord::UnchainFixup(System.Runtime.Serialization.BaseFixupRecord,System.Runtime.Serialization.BaseFixupRecord,System.Boolean)
extern const MethodInfo ObjectRecord_UnchainFixup_m10945_MethodInfo = 
{
	"UnchainFixup"/* name */
	, (methodPointerType)&ObjectRecord_UnchainFixup_m10945/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274/* invoker_method */
	, ObjectRecord_t2049_ObjectRecord_UnchainFixup_m10945_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseFixupRecord_t2050_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ObjectRecord_t2049_ObjectRecord_ChainFixup_m10946_ParameterInfos[] = 
{
	{"fixup", 0, 134222903, 0, &BaseFixupRecord_t2050_0_0_0},
	{"asContainer", 1, 134222904, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.ObjectRecord::ChainFixup(System.Runtime.Serialization.BaseFixupRecord,System.Boolean)
extern const MethodInfo ObjectRecord_ChainFixup_m10946_MethodInfo = 
{
	"ChainFixup"/* name */
	, (methodPointerType)&ObjectRecord_ChainFixup_m10946/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, ObjectRecord_t2049_ObjectRecord_ChainFixup_m10946_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectManager_t2033_0_0_0;
extern const Il2CppType ISurrogateSelector_t1996_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo ObjectRecord_t2049_ObjectRecord_LoadData_m10947_ParameterInfos[] = 
{
	{"manager", 0, 134222905, 0, &ObjectManager_t2033_0_0_0},
	{"selector", 1, 134222906, 0, &ISurrogateSelector_t1996_0_0_0},
	{"context", 2, 134222907, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.ObjectRecord::LoadData(System.Runtime.Serialization.ObjectManager,System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjectRecord_LoadData_m10947_MethodInfo = 
{
	"LoadData"/* name */
	, (methodPointerType)&ObjectRecord_LoadData_m10947/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, ObjectRecord_t2049_ObjectRecord_LoadData_m10947_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.ObjectRecord::get_HasPendingFixups()
extern const MethodInfo ObjectRecord_get_HasPendingFixups_m10948_MethodInfo = 
{
	"get_HasPendingFixups"/* name */
	, (methodPointerType)&ObjectRecord_get_HasPendingFixups_m10948/* method */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectRecord_t2049_MethodInfos[] =
{
	&ObjectRecord__ctor_m10936_MethodInfo,
	&ObjectRecord_SetMemberValue_m10937_MethodInfo,
	&ObjectRecord_SetArrayValue_m10938_MethodInfo,
	&ObjectRecord_SetMemberValue_m10939_MethodInfo,
	&ObjectRecord_get_IsInstanceReady_m10940_MethodInfo,
	&ObjectRecord_get_IsUnsolvedObjectReference_m10941_MethodInfo,
	&ObjectRecord_get_IsRegistered_m10942_MethodInfo,
	&ObjectRecord_DoFixups_m10943_MethodInfo,
	&ObjectRecord_RemoveFixup_m10944_MethodInfo,
	&ObjectRecord_UnchainFixup_m10945_MethodInfo,
	&ObjectRecord_ChainFixup_m10946_MethodInfo,
	&ObjectRecord_LoadData_m10947_MethodInfo,
	&ObjectRecord_get_HasPendingFixups_m10948_MethodInfo,
	NULL
};
extern const MethodInfo ObjectRecord_get_IsInstanceReady_m10940_MethodInfo;
static const PropertyInfo ObjectRecord_t2049____IsInstanceReady_PropertyInfo = 
{
	&ObjectRecord_t2049_il2cpp_TypeInfo/* parent */
	, "IsInstanceReady"/* name */
	, &ObjectRecord_get_IsInstanceReady_m10940_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjectRecord_get_IsUnsolvedObjectReference_m10941_MethodInfo;
static const PropertyInfo ObjectRecord_t2049____IsUnsolvedObjectReference_PropertyInfo = 
{
	&ObjectRecord_t2049_il2cpp_TypeInfo/* parent */
	, "IsUnsolvedObjectReference"/* name */
	, &ObjectRecord_get_IsUnsolvedObjectReference_m10941_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjectRecord_get_IsRegistered_m10942_MethodInfo;
static const PropertyInfo ObjectRecord_t2049____IsRegistered_PropertyInfo = 
{
	&ObjectRecord_t2049_il2cpp_TypeInfo/* parent */
	, "IsRegistered"/* name */
	, &ObjectRecord_get_IsRegistered_m10942_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjectRecord_get_HasPendingFixups_m10948_MethodInfo;
static const PropertyInfo ObjectRecord_t2049____HasPendingFixups_PropertyInfo = 
{
	&ObjectRecord_t2049_il2cpp_TypeInfo/* parent */
	, "HasPendingFixups"/* name */
	, &ObjectRecord_get_HasPendingFixups_m10948_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectRecord_t2049_PropertyInfos[] =
{
	&ObjectRecord_t2049____IsInstanceReady_PropertyInfo,
	&ObjectRecord_t2049____IsUnsolvedObjectReference_PropertyInfo,
	&ObjectRecord_t2049____IsRegistered_PropertyInfo,
	&ObjectRecord_t2049____HasPendingFixups_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ObjectRecord_t2049_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ObjectRecord_t2049_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectRecord_t2049_1_0_0;
struct ObjectRecord_t2049;
const Il2CppTypeDefinitionMetadata ObjectRecord_t2049_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectRecord_t2049_VTable/* vtableMethods */
	, ObjectRecord_t2049_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2245/* fieldStart */

};
TypeInfo ObjectRecord_t2049_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, ObjectRecord_t2049_MethodInfos/* methods */
	, ObjectRecord_t2049_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectRecord_t2049_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectRecord_t2049_0_0_0/* byval_arg */
	, &ObjectRecord_t2049_1_0_0/* this_arg */
	, &ObjectRecord_t2049_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectRecord_t2049)/* instance_size */
	, sizeof (ObjectRecord_t2049)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 4/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.OnDeserializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializedAttribut.h"
// Metadata Definition System.Runtime.Serialization.OnDeserializedAttribute
extern TypeInfo OnDeserializedAttribute_t2057_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnDeserializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializedAttributMethodDeclarations.h"
static const MethodInfo* OnDeserializedAttribute_t2057_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Attribute_Equals_m5384_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5253_MethodInfo;
static const Il2CppMethodReference OnDeserializedAttribute_t2057_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool OnDeserializedAttribute_t2057_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t1145_0_0_0;
static Il2CppInterfaceOffsetPair OnDeserializedAttribute_t2057_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnDeserializedAttribute_t2057_0_0_0;
extern const Il2CppType OnDeserializedAttribute_t2057_1_0_0;
extern const Il2CppType Attribute_t805_0_0_0;
struct OnDeserializedAttribute_t2057;
const Il2CppTypeDefinitionMetadata OnDeserializedAttribute_t2057_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnDeserializedAttribute_t2057_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, OnDeserializedAttribute_t2057_VTable/* vtableMethods */
	, OnDeserializedAttribute_t2057_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo OnDeserializedAttribute_t2057_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnDeserializedAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, OnDeserializedAttribute_t2057_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OnDeserializedAttribute_t2057_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 625/* custom_attributes_cache */
	, &OnDeserializedAttribute_t2057_0_0_0/* byval_arg */
	, &OnDeserializedAttribute_t2057_1_0_0/* this_arg */
	, &OnDeserializedAttribute_t2057_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnDeserializedAttribute_t2057)/* instance_size */
	, sizeof (OnDeserializedAttribute_t2057)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.OnDeserializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializingAttribu.h"
// Metadata Definition System.Runtime.Serialization.OnDeserializingAttribute
extern TypeInfo OnDeserializingAttribute_t2058_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnDeserializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializingAttribuMethodDeclarations.h"
static const MethodInfo* OnDeserializingAttribute_t2058_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference OnDeserializingAttribute_t2058_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool OnDeserializingAttribute_t2058_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OnDeserializingAttribute_t2058_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnDeserializingAttribute_t2058_0_0_0;
extern const Il2CppType OnDeserializingAttribute_t2058_1_0_0;
struct OnDeserializingAttribute_t2058;
const Il2CppTypeDefinitionMetadata OnDeserializingAttribute_t2058_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnDeserializingAttribute_t2058_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, OnDeserializingAttribute_t2058_VTable/* vtableMethods */
	, OnDeserializingAttribute_t2058_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo OnDeserializingAttribute_t2058_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnDeserializingAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, OnDeserializingAttribute_t2058_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OnDeserializingAttribute_t2058_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 626/* custom_attributes_cache */
	, &OnDeserializingAttribute_t2058_0_0_0/* byval_arg */
	, &OnDeserializingAttribute_t2058_1_0_0/* this_arg */
	, &OnDeserializingAttribute_t2058_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnDeserializingAttribute_t2058)/* instance_size */
	, sizeof (OnDeserializingAttribute_t2058)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.OnSerializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializedAttribute.h"
// Metadata Definition System.Runtime.Serialization.OnSerializedAttribute
extern TypeInfo OnSerializedAttribute_t2059_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnSerializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializedAttributeMethodDeclarations.h"
static const MethodInfo* OnSerializedAttribute_t2059_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference OnSerializedAttribute_t2059_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool OnSerializedAttribute_t2059_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OnSerializedAttribute_t2059_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnSerializedAttribute_t2059_0_0_0;
extern const Il2CppType OnSerializedAttribute_t2059_1_0_0;
struct OnSerializedAttribute_t2059;
const Il2CppTypeDefinitionMetadata OnSerializedAttribute_t2059_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnSerializedAttribute_t2059_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, OnSerializedAttribute_t2059_VTable/* vtableMethods */
	, OnSerializedAttribute_t2059_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo OnSerializedAttribute_t2059_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnSerializedAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, OnSerializedAttribute_t2059_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OnSerializedAttribute_t2059_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 627/* custom_attributes_cache */
	, &OnSerializedAttribute_t2059_0_0_0/* byval_arg */
	, &OnSerializedAttribute_t2059_1_0_0/* this_arg */
	, &OnSerializedAttribute_t2059_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnSerializedAttribute_t2059)/* instance_size */
	, sizeof (OnSerializedAttribute_t2059)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.OnSerializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializingAttribute.h"
// Metadata Definition System.Runtime.Serialization.OnSerializingAttribute
extern TypeInfo OnSerializingAttribute_t2060_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnSerializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializingAttributeMethodDeclarations.h"
static const MethodInfo* OnSerializingAttribute_t2060_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference OnSerializingAttribute_t2060_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool OnSerializingAttribute_t2060_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OnSerializingAttribute_t2060_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnSerializingAttribute_t2060_0_0_0;
extern const Il2CppType OnSerializingAttribute_t2060_1_0_0;
struct OnSerializingAttribute_t2060;
const Il2CppTypeDefinitionMetadata OnSerializingAttribute_t2060_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnSerializingAttribute_t2060_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, OnSerializingAttribute_t2060_VTable/* vtableMethods */
	, OnSerializingAttribute_t2060_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo OnSerializingAttribute_t2060_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnSerializingAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, OnSerializingAttribute_t2060_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OnSerializingAttribute_t2060_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 628/* custom_attributes_cache */
	, &OnSerializingAttribute_t2060_0_0_0/* byval_arg */
	, &OnSerializingAttribute_t2060_1_0_0/* this_arg */
	, &OnSerializingAttribute_t2060_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnSerializingAttribute_t2060)/* instance_size */
	, sizeof (OnSerializingAttribute_t2060)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationBinder
#include "mscorlib_System_Runtime_Serialization_SerializationBinder.h"
// Metadata Definition System.Runtime.Serialization.SerializationBinder
extern TypeInfo SerializationBinder_t2027_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationBinder
#include "mscorlib_System_Runtime_Serialization_SerializationBinderMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationBinder::.ctor()
extern const MethodInfo SerializationBinder__ctor_m10949_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SerializationBinder__ctor_m10949/* method */
	, &SerializationBinder_t2027_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SerializationBinder_t2027_SerializationBinder_BindToType_m13243_ParameterInfos[] = 
{
	{"assemblyName", 0, 134222908, 0, &String_t_0_0_0},
	{"typeName", 1, 134222909, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.SerializationBinder::BindToType(System.String,System.String)
extern const MethodInfo SerializationBinder_BindToType_m13243_MethodInfo = 
{
	"BindToType"/* name */
	, NULL/* method */
	, &SerializationBinder_t2027_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, SerializationBinder_t2027_SerializationBinder_BindToType_m13243_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SerializationBinder_t2027_MethodInfos[] =
{
	&SerializationBinder__ctor_m10949_MethodInfo,
	&SerializationBinder_BindToType_m13243_MethodInfo,
	NULL
};
static const Il2CppMethodReference SerializationBinder_t2027_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
};
static bool SerializationBinder_t2027_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationBinder_t2027_1_0_0;
struct SerializationBinder_t2027;
const Il2CppTypeDefinitionMetadata SerializationBinder_t2027_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationBinder_t2027_VTable/* vtableMethods */
	, SerializationBinder_t2027_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SerializationBinder_t2027_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationBinder"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, SerializationBinder_t2027_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SerializationBinder_t2027_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 629/* custom_attributes_cache */
	, &SerializationBinder_t2027_0_0_0/* byval_arg */
	, &SerializationBinder_t2027_1_0_0/* this_arg */
	, &SerializationBinder_t2027_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationBinder_t2027)/* instance_size */
	, sizeof (SerializationBinder_t2027)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacks.h"
// Metadata Definition System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
extern TypeInfo CallbackHandler_t2061_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacksMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CallbackHandler_t2061_CallbackHandler__ctor_m10950_ParameterInfos[] = 
{
	{"object", 0, 134222925, 0, &Object_t_0_0_0},
	{"method", 1, 134222926, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationCallbacks/CallbackHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CallbackHandler__ctor_m10950_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CallbackHandler__ctor_m10950/* method */
	, &CallbackHandler_t2061_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, CallbackHandler_t2061_CallbackHandler__ctor_m10950_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo CallbackHandler_t2061_CallbackHandler_Invoke_m10951_ParameterInfos[] = 
{
	{"context", 0, 134222927, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationCallbacks/CallbackHandler::Invoke(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo CallbackHandler_Invoke_m10951_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CallbackHandler_Invoke_m10951/* method */
	, &CallbackHandler_t2061_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_StreamingContext_t1044/* invoker_method */
	, CallbackHandler_t2061_CallbackHandler_Invoke_m10951_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CallbackHandler_t2061_CallbackHandler_BeginInvoke_m10952_ParameterInfos[] = 
{
	{"context", 0, 134222928, 0, &StreamingContext_t1044_0_0_0},
	{"callback", 1, 134222929, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134222930, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t546_0_0_0;
extern void* RuntimeInvoker_Object_t_StreamingContext_t1044_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Runtime.Serialization.SerializationCallbacks/CallbackHandler::BeginInvoke(System.Runtime.Serialization.StreamingContext,System.AsyncCallback,System.Object)
extern const MethodInfo CallbackHandler_BeginInvoke_m10952_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CallbackHandler_BeginInvoke_m10952/* method */
	, &CallbackHandler_t2061_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t1044_Object_t_Object_t/* invoker_method */
	, CallbackHandler_t2061_CallbackHandler_BeginInvoke_m10952_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo CallbackHandler_t2061_CallbackHandler_EndInvoke_m10953_ParameterInfos[] = 
{
	{"result", 0, 134222931, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationCallbacks/CallbackHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo CallbackHandler_EndInvoke_m10953_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CallbackHandler_EndInvoke_m10953/* method */
	, &CallbackHandler_t2061_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CallbackHandler_t2061_CallbackHandler_EndInvoke_m10953_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CallbackHandler_t2061_MethodInfos[] =
{
	&CallbackHandler__ctor_m10950_MethodInfo,
	&CallbackHandler_Invoke_m10951_MethodInfo,
	&CallbackHandler_BeginInvoke_m10952_MethodInfo,
	&CallbackHandler_EndInvoke_m10953_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m3586_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m3587_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m3588_MethodInfo;
extern const MethodInfo Delegate_Clone_m3589_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m3590_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m3591_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m3592_MethodInfo;
extern const MethodInfo CallbackHandler_Invoke_m10951_MethodInfo;
extern const MethodInfo CallbackHandler_BeginInvoke_m10952_MethodInfo;
extern const MethodInfo CallbackHandler_EndInvoke_m10953_MethodInfo;
static const Il2CppMethodReference CallbackHandler_t2061_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&CallbackHandler_Invoke_m10951_MethodInfo,
	&CallbackHandler_BeginInvoke_m10952_MethodInfo,
	&CallbackHandler_EndInvoke_m10953_MethodInfo,
};
static bool CallbackHandler_t2061_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t733_0_0_0;
static Il2CppInterfaceOffsetPair CallbackHandler_t2061_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallbackHandler_t2061_0_0_0;
extern const Il2CppType CallbackHandler_t2061_1_0_0;
extern const Il2CppType MulticastDelegate_t549_0_0_0;
extern TypeInfo SerializationCallbacks_t2062_il2cpp_TypeInfo;
extern const Il2CppType SerializationCallbacks_t2062_0_0_0;
struct CallbackHandler_t2061;
const Il2CppTypeDefinitionMetadata CallbackHandler_t2061_DefinitionMetadata = 
{
	&SerializationCallbacks_t2062_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CallbackHandler_t2061_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, CallbackHandler_t2061_VTable/* vtableMethods */
	, CallbackHandler_t2061_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CallbackHandler_t2061_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallbackHandler"/* name */
	, ""/* namespaze */
	, CallbackHandler_t2061_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CallbackHandler_t2061_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CallbackHandler_t2061_0_0_0/* byval_arg */
	, &CallbackHandler_t2061_1_0_0/* this_arg */
	, &CallbackHandler_t2061_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CallbackHandler_t2061/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallbackHandler_t2061)/* instance_size */
	, sizeof (CallbackHandler_t2061)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationCallbacks
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacks_0.h"
// Metadata Definition System.Runtime.Serialization.SerializationCallbacks
// System.Runtime.Serialization.SerializationCallbacks
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacks_0MethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SerializationCallbacks_t2062_SerializationCallbacks__ctor_m10954_ParameterInfos[] = 
{
	{"type", 0, 134222910, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationCallbacks::.ctor(System.Type)
extern const MethodInfo SerializationCallbacks__ctor_m10954_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SerializationCallbacks__ctor_m10954/* method */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SerializationCallbacks_t2062_SerializationCallbacks__ctor_m10954_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationCallbacks::.cctor()
extern const MethodInfo SerializationCallbacks__cctor_m10955_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SerializationCallbacks__cctor_m10955/* method */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.SerializationCallbacks::get_HasSerializedCallbacks()
extern const MethodInfo SerializationCallbacks_get_HasSerializedCallbacks_m10956_MethodInfo = 
{
	"get_HasSerializedCallbacks"/* name */
	, (methodPointerType)&SerializationCallbacks_get_HasSerializedCallbacks_m10956/* method */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.SerializationCallbacks::get_HasDeserializedCallbacks()
extern const MethodInfo SerializationCallbacks_get_HasDeserializedCallbacks_m10957_MethodInfo = 
{
	"get_HasDeserializedCallbacks"/* name */
	, (methodPointerType)&SerializationCallbacks_get_HasDeserializedCallbacks_m10957/* method */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SerializationCallbacks_t2062_SerializationCallbacks_GetMethodsByAttribute_m10958_ParameterInfos[] = 
{
	{"type", 0, 134222911, 0, &Type_t_0_0_0},
	{"attr", 1, 134222912, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.ArrayList System.Runtime.Serialization.SerializationCallbacks::GetMethodsByAttribute(System.Type,System.Type)
extern const MethodInfo SerializationCallbacks_GetMethodsByAttribute_m10958_MethodInfo = 
{
	"GetMethodsByAttribute"/* name */
	, (methodPointerType)&SerializationCallbacks_GetMethodsByAttribute_m10958/* method */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* declaring_type */
	, &ArrayList_t1271_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, SerializationCallbacks_t2062_SerializationCallbacks_GetMethodsByAttribute_m10958_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ArrayList_t1271_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo SerializationCallbacks_t2062_SerializationCallbacks_Invoke_m10959_ParameterInfos[] = 
{
	{"list", 0, 134222913, 0, &ArrayList_t1271_0_0_0},
	{"target", 1, 134222914, 0, &Object_t_0_0_0},
	{"context", 2, 134222915, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationCallbacks::Invoke(System.Collections.ArrayList,System.Object,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo SerializationCallbacks_Invoke_m10959_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&SerializationCallbacks_Invoke_m10959/* method */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, SerializationCallbacks_t2062_SerializationCallbacks_Invoke_m10959_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo SerializationCallbacks_t2062_SerializationCallbacks_RaiseOnSerializing_m10960_ParameterInfos[] = 
{
	{"target", 0, 134222916, 0, &Object_t_0_0_0},
	{"contex", 1, 134222917, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationCallbacks::RaiseOnSerializing(System.Object,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo SerializationCallbacks_RaiseOnSerializing_m10960_MethodInfo = 
{
	"RaiseOnSerializing"/* name */
	, (methodPointerType)&SerializationCallbacks_RaiseOnSerializing_m10960/* method */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, SerializationCallbacks_t2062_SerializationCallbacks_RaiseOnSerializing_m10960_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo SerializationCallbacks_t2062_SerializationCallbacks_RaiseOnSerialized_m10961_ParameterInfos[] = 
{
	{"target", 0, 134222918, 0, &Object_t_0_0_0},
	{"contex", 1, 134222919, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationCallbacks::RaiseOnSerialized(System.Object,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo SerializationCallbacks_RaiseOnSerialized_m10961_MethodInfo = 
{
	"RaiseOnSerialized"/* name */
	, (methodPointerType)&SerializationCallbacks_RaiseOnSerialized_m10961/* method */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, SerializationCallbacks_t2062_SerializationCallbacks_RaiseOnSerialized_m10961_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo SerializationCallbacks_t2062_SerializationCallbacks_RaiseOnDeserializing_m10962_ParameterInfos[] = 
{
	{"target", 0, 134222920, 0, &Object_t_0_0_0},
	{"contex", 1, 134222921, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationCallbacks::RaiseOnDeserializing(System.Object,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo SerializationCallbacks_RaiseOnDeserializing_m10962_MethodInfo = 
{
	"RaiseOnDeserializing"/* name */
	, (methodPointerType)&SerializationCallbacks_RaiseOnDeserializing_m10962/* method */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, SerializationCallbacks_t2062_SerializationCallbacks_RaiseOnDeserializing_m10962_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo SerializationCallbacks_t2062_SerializationCallbacks_RaiseOnDeserialized_m10963_ParameterInfos[] = 
{
	{"target", 0, 134222922, 0, &Object_t_0_0_0},
	{"contex", 1, 134222923, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationCallbacks::RaiseOnDeserialized(System.Object,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo SerializationCallbacks_RaiseOnDeserialized_m10963_MethodInfo = 
{
	"RaiseOnDeserialized"/* name */
	, (methodPointerType)&SerializationCallbacks_RaiseOnDeserialized_m10963/* method */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, SerializationCallbacks_t2062_SerializationCallbacks_RaiseOnDeserialized_m10963_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SerializationCallbacks_t2062_SerializationCallbacks_GetSerializationCallbacks_m10964_ParameterInfos[] = 
{
	{"t", 0, 134222924, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.SerializationCallbacks System.Runtime.Serialization.SerializationCallbacks::GetSerializationCallbacks(System.Type)
extern const MethodInfo SerializationCallbacks_GetSerializationCallbacks_m10964_MethodInfo = 
{
	"GetSerializationCallbacks"/* name */
	, (methodPointerType)&SerializationCallbacks_GetSerializationCallbacks_m10964/* method */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* declaring_type */
	, &SerializationCallbacks_t2062_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, SerializationCallbacks_t2062_SerializationCallbacks_GetSerializationCallbacks_m10964_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SerializationCallbacks_t2062_MethodInfos[] =
{
	&SerializationCallbacks__ctor_m10954_MethodInfo,
	&SerializationCallbacks__cctor_m10955_MethodInfo,
	&SerializationCallbacks_get_HasSerializedCallbacks_m10956_MethodInfo,
	&SerializationCallbacks_get_HasDeserializedCallbacks_m10957_MethodInfo,
	&SerializationCallbacks_GetMethodsByAttribute_m10958_MethodInfo,
	&SerializationCallbacks_Invoke_m10959_MethodInfo,
	&SerializationCallbacks_RaiseOnSerializing_m10960_MethodInfo,
	&SerializationCallbacks_RaiseOnSerialized_m10961_MethodInfo,
	&SerializationCallbacks_RaiseOnDeserializing_m10962_MethodInfo,
	&SerializationCallbacks_RaiseOnDeserialized_m10963_MethodInfo,
	&SerializationCallbacks_GetSerializationCallbacks_m10964_MethodInfo,
	NULL
};
extern const MethodInfo SerializationCallbacks_get_HasSerializedCallbacks_m10956_MethodInfo;
static const PropertyInfo SerializationCallbacks_t2062____HasSerializedCallbacks_PropertyInfo = 
{
	&SerializationCallbacks_t2062_il2cpp_TypeInfo/* parent */
	, "HasSerializedCallbacks"/* name */
	, &SerializationCallbacks_get_HasSerializedCallbacks_m10956_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SerializationCallbacks_get_HasDeserializedCallbacks_m10957_MethodInfo;
static const PropertyInfo SerializationCallbacks_t2062____HasDeserializedCallbacks_PropertyInfo = 
{
	&SerializationCallbacks_t2062_il2cpp_TypeInfo/* parent */
	, "HasDeserializedCallbacks"/* name */
	, &SerializationCallbacks_get_HasDeserializedCallbacks_m10957_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SerializationCallbacks_t2062_PropertyInfos[] =
{
	&SerializationCallbacks_t2062____HasSerializedCallbacks_PropertyInfo,
	&SerializationCallbacks_t2062____HasDeserializedCallbacks_PropertyInfo,
	NULL
};
static const Il2CppType* SerializationCallbacks_t2062_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CallbackHandler_t2061_0_0_0,
};
static const Il2CppMethodReference SerializationCallbacks_t2062_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SerializationCallbacks_t2062_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationCallbacks_t2062_1_0_0;
struct SerializationCallbacks_t2062;
const Il2CppTypeDefinitionMetadata SerializationCallbacks_t2062_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SerializationCallbacks_t2062_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationCallbacks_t2062_VTable/* vtableMethods */
	, SerializationCallbacks_t2062_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2258/* fieldStart */

};
TypeInfo SerializationCallbacks_t2062_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationCallbacks"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, SerializationCallbacks_t2062_MethodInfos/* methods */
	, SerializationCallbacks_t2062_PropertyInfos/* properties */
	, NULL/* events */
	, &SerializationCallbacks_t2062_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SerializationCallbacks_t2062_0_0_0/* byval_arg */
	, &SerializationCallbacks_t2062_1_0_0/* this_arg */
	, &SerializationCallbacks_t2062_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationCallbacks_t2062)/* instance_size */
	, sizeof (SerializationCallbacks_t2062)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SerializationCallbacks_t2062_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
// Metadata Definition System.Runtime.Serialization.SerializationEntry
extern TypeInfo SerializationEntry_t2063_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntryMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SerializationEntry_t2063_SerializationEntry__ctor_m10965_ParameterInfos[] = 
{
	{"name", 0, 134222932, 0, &String_t_0_0_0},
	{"type", 1, 134222933, 0, &Type_t_0_0_0},
	{"value", 2, 134222934, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationEntry::.ctor(System.String,System.Type,System.Object)
extern const MethodInfo SerializationEntry__ctor_m10965_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SerializationEntry__ctor_m10965/* method */
	, &SerializationEntry_t2063_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, SerializationEntry_t2063_SerializationEntry__ctor_m10965_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.SerializationEntry::get_Name()
extern const MethodInfo SerializationEntry_get_Name_m10966_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&SerializationEntry_get_Name_m10966/* method */
	, &SerializationEntry_t2063_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.SerializationEntry::get_ObjectType()
extern const MethodInfo SerializationEntry_get_ObjectType_m10967_MethodInfo = 
{
	"get_ObjectType"/* name */
	, (methodPointerType)&SerializationEntry_get_ObjectType_m10967/* method */
	, &SerializationEntry_t2063_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.SerializationEntry::get_Value()
extern const MethodInfo SerializationEntry_get_Value_m10968_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&SerializationEntry_get_Value_m10968/* method */
	, &SerializationEntry_t2063_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SerializationEntry_t2063_MethodInfos[] =
{
	&SerializationEntry__ctor_m10965_MethodInfo,
	&SerializationEntry_get_Name_m10966_MethodInfo,
	&SerializationEntry_get_ObjectType_m10967_MethodInfo,
	&SerializationEntry_get_Value_m10968_MethodInfo,
	NULL
};
extern const MethodInfo SerializationEntry_get_Name_m10966_MethodInfo;
static const PropertyInfo SerializationEntry_t2063____Name_PropertyInfo = 
{
	&SerializationEntry_t2063_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &SerializationEntry_get_Name_m10966_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SerializationEntry_get_ObjectType_m10967_MethodInfo;
static const PropertyInfo SerializationEntry_t2063____ObjectType_PropertyInfo = 
{
	&SerializationEntry_t2063_il2cpp_TypeInfo/* parent */
	, "ObjectType"/* name */
	, &SerializationEntry_get_ObjectType_m10967_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SerializationEntry_get_Value_m10968_MethodInfo;
static const PropertyInfo SerializationEntry_t2063____Value_PropertyInfo = 
{
	&SerializationEntry_t2063_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &SerializationEntry_get_Value_m10968_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SerializationEntry_t2063_PropertyInfos[] =
{
	&SerializationEntry_t2063____Name_PropertyInfo,
	&SerializationEntry_t2063____ObjectType_PropertyInfo,
	&SerializationEntry_t2063____Value_PropertyInfo,
	NULL
};
extern const MethodInfo ValueType_Equals_m1076_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m1077_MethodInfo;
extern const MethodInfo ValueType_ToString_m1078_MethodInfo;
static const Il2CppMethodReference SerializationEntry_t2063_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool SerializationEntry_t2063_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationEntry_t2063_0_0_0;
extern const Il2CppType SerializationEntry_t2063_1_0_0;
extern const Il2CppType ValueType_t285_0_0_0;
const Il2CppTypeDefinitionMetadata SerializationEntry_t2063_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, SerializationEntry_t2063_VTable/* vtableMethods */
	, SerializationEntry_t2063_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2264/* fieldStart */

};
TypeInfo SerializationEntry_t2063_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationEntry"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, SerializationEntry_t2063_MethodInfos/* methods */
	, SerializationEntry_t2063_PropertyInfos/* properties */
	, NULL/* events */
	, &SerializationEntry_t2063_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 630/* custom_attributes_cache */
	, &SerializationEntry_t2063_0_0_0/* byval_arg */
	, &SerializationEntry_t2063_1_0_0/* this_arg */
	, &SerializationEntry_t2063_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationEntry_t2063)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SerializationEntry_t2063)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationException
#include "mscorlib_System_Runtime_Serialization_SerializationException.h"
// Metadata Definition System.Runtime.Serialization.SerializationException
extern TypeInfo SerializationException_t1443_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationException
#include "mscorlib_System_Runtime_Serialization_SerializationExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationException::.ctor()
extern const MethodInfo SerializationException__ctor_m10969_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SerializationException__ctor_m10969/* method */
	, &SerializationException_t1443_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SerializationException_t1443_SerializationException__ctor_m6393_ParameterInfos[] = 
{
	{"message", 0, 134222935, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationException::.ctor(System.String)
extern const MethodInfo SerializationException__ctor_m6393_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SerializationException__ctor_m6393/* method */
	, &SerializationException_t1443_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SerializationException_t1443_SerializationException__ctor_m6393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo SerializationException_t1443_SerializationException__ctor_m10970_ParameterInfos[] = 
{
	{"info", 0, 134222936, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222937, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo SerializationException__ctor_m10970_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SerializationException__ctor_m10970/* method */
	, &SerializationException_t1443_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, SerializationException_t1443_SerializationException__ctor_m10970_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SerializationException_t1443_MethodInfos[] =
{
	&SerializationException__ctor_m10969_MethodInfo,
	&SerializationException__ctor_m6393_MethodInfo,
	&SerializationException__ctor_m10970_MethodInfo,
	NULL
};
static const Il2CppMethodReference SerializationException_t1443_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool SerializationException_t1443_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SerializationException_t1443_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationException_t1443_0_0_0;
extern const Il2CppType SerializationException_t1443_1_0_0;
struct SerializationException_t1443;
const Il2CppTypeDefinitionMetadata SerializationException_t1443_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SerializationException_t1443_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1464_0_0_0/* parent */
	, SerializationException_t1443_VTable/* vtableMethods */
	, SerializationException_t1443_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SerializationException_t1443_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationException"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, SerializationException_t1443_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SerializationException_t1443_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 631/* custom_attributes_cache */
	, &SerializationException_t1443_0_0_0/* byval_arg */
	, &SerializationException_t1443_1_0_0/* this_arg */
	, &SerializationException_t1443_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationException_t1443)/* instance_size */
	, sizeof (SerializationException_t1443)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// Metadata Definition System.Runtime.Serialization.SerializationInfo
extern TypeInfo SerializationInfo_t1043_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IFormatterConverter_t2064_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo__ctor_m10971_ParameterInfos[] = 
{
	{"type", 0, 134222938, 0, &Type_t_0_0_0},
	{"converter", 1, 134222939, 0, &IFormatterConverter_t2064_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfo::.ctor(System.Type,System.Runtime.Serialization.IFormatterConverter)
extern const MethodInfo SerializationInfo__ctor_m10971_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SerializationInfo__ctor_m10971/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo__ctor_m10971_ParameterInfos/* parameters */
	, 633/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.SerializationInfo::get_AssemblyName()
extern const MethodInfo SerializationInfo_get_AssemblyName_m10972_MethodInfo = 
{
	"get_AssemblyName"/* name */
	, (methodPointerType)&SerializationInfo_get_AssemblyName_m10972/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.SerializationInfo::get_FullTypeName()
extern const MethodInfo SerializationInfo_get_FullTypeName_m10973_MethodInfo = 
{
	"get_FullTypeName"/* name */
	, (methodPointerType)&SerializationInfo_get_FullTypeName_m10973/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.SerializationInfo::get_MemberCount()
extern const MethodInfo SerializationInfo_get_MemberCount_m10974_MethodInfo = 
{
	"get_MemberCount"/* name */
	, (methodPointerType)&SerializationInfo_get_MemberCount_m10974/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_AddValue_m6389_ParameterInfos[] = 
{
	{"name", 0, 134222940, 0, &String_t_0_0_0},
	{"value", 1, 134222941, 0, &Object_t_0_0_0},
	{"type", 2, 134222942, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Object,System.Type)
extern const MethodInfo SerializationInfo_AddValue_m6389_MethodInfo = 
{
	"AddValue"/* name */
	, (methodPointerType)&SerializationInfo_AddValue_m6389/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_AddValue_m6389_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_GetValue_m6392_ParameterInfos[] = 
{
	{"name", 0, 134222943, 0, &String_t_0_0_0},
	{"type", 1, 134222944, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.SerializationInfo::GetValue(System.String,System.Type)
extern const MethodInfo SerializationInfo_GetValue_m6392_MethodInfo = 
{
	"GetValue"/* name */
	, (methodPointerType)&SerializationInfo_GetValue_m6392/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_GetValue_m6392_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_SetType_m10975_ParameterInfos[] = 
{
	{"type", 0, 134222945, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfo::SetType(System.Type)
extern const MethodInfo SerializationInfo_SetType_m10975_MethodInfo = 
{
	"SetType"/* name */
	, (methodPointerType)&SerializationInfo_SetType_m10975/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_SetType_m10975_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfoEnumerator_t2065_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.SerializationInfoEnumerator System.Runtime.Serialization.SerializationInfo::GetEnumerator()
extern const MethodInfo SerializationInfo_GetEnumerator_m10976_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&SerializationInfo_GetEnumerator_m10976/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &SerializationInfoEnumerator_t2065_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int16_t752_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_AddValue_m10977_ParameterInfos[] = 
{
	{"name", 0, 134222946, 0, &String_t_0_0_0},
	{"value", 1, 134222947, 0, &Int16_t752_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Int16)
extern const MethodInfo SerializationInfo_AddValue_m10977_MethodInfo = 
{
	"AddValue"/* name */
	, (methodPointerType)&SerializationInfo_AddValue_m10977/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int16_t752/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_AddValue_m10977_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_AddValue_m6391_ParameterInfos[] = 
{
	{"name", 0, 134222948, 0, &String_t_0_0_0},
	{"value", 1, 134222949, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Int32)
extern const MethodInfo SerializationInfo_AddValue_m6391_MethodInfo = 
{
	"AddValue"/* name */
	, (methodPointerType)&SerializationInfo_AddValue_m6391/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_AddValue_m6391_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_AddValue_m6390_ParameterInfos[] = 
{
	{"name", 0, 134222950, 0, &String_t_0_0_0},
	{"value", 1, 134222951, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Boolean)
extern const MethodInfo SerializationInfo_AddValue_m6390_MethodInfo = 
{
	"AddValue"/* name */
	, (methodPointerType)&SerializationInfo_AddValue_m6390/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_AddValue_m6390_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType DateTime_t406_0_0_0;
extern const Il2CppType DateTime_t406_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_AddValue_m10978_ParameterInfos[] = 
{
	{"name", 0, 134222952, 0, &String_t_0_0_0},
	{"value", 1, 134222953, 0, &DateTime_t406_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.DateTime)
extern const MethodInfo SerializationInfo_AddValue_m10978_MethodInfo = 
{
	"AddValue"/* name */
	, (methodPointerType)&SerializationInfo_AddValue_m10978/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_DateTime_t406/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_AddValue_m10978_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_AddValue_m10979_ParameterInfos[] = 
{
	{"name", 0, 134222954, 0, &String_t_0_0_0},
	{"value", 1, 134222955, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Single)
extern const MethodInfo SerializationInfo_AddValue_m10979_MethodInfo = 
{
	"AddValue"/* name */
	, (methodPointerType)&SerializationInfo_AddValue_m10979/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Single_t254/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_AddValue_m10979_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_AddValue_m6402_ParameterInfos[] = 
{
	{"name", 0, 134222956, 0, &String_t_0_0_0},
	{"value", 1, 134222957, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Int64)
extern const MethodInfo SerializationInfo_AddValue_m6402_MethodInfo = 
{
	"AddValue"/* name */
	, (methodPointerType)&SerializationInfo_AddValue_m6402/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_AddValue_m6402_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt64_t1074_0_0_0;
extern const Il2CppType UInt64_t1074_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_AddValue_m10980_ParameterInfos[] = 
{
	{"name", 0, 134222958, 0, &String_t_0_0_0},
	{"value", 1, 134222959, 0, &UInt64_t1074_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.UInt64)
extern const MethodInfo SerializationInfo_AddValue_m10980_MethodInfo = 
{
	"AddValue"/* name */
	, (methodPointerType)&SerializationInfo_AddValue_m10980/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_AddValue_m10980_ParameterInfos/* parameters */
	, 634/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_AddValue_m6401_ParameterInfos[] = 
{
	{"name", 0, 134222960, 0, &String_t_0_0_0},
	{"value", 1, 134222961, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Object)
extern const MethodInfo SerializationInfo_AddValue_m6401_MethodInfo = 
{
	"AddValue"/* name */
	, (methodPointerType)&SerializationInfo_AddValue_m6401/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_AddValue_m6401_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_GetBoolean_m6394_ParameterInfos[] = 
{
	{"name", 0, 134222962, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.SerializationInfo::GetBoolean(System.String)
extern const MethodInfo SerializationInfo_GetBoolean_m6394_MethodInfo = 
{
	"GetBoolean"/* name */
	, (methodPointerType)&SerializationInfo_GetBoolean_m6394/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_GetBoolean_m6394_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_GetInt16_m10981_ParameterInfos[] = 
{
	{"name", 0, 134222963, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int16_t752_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int16 System.Runtime.Serialization.SerializationInfo::GetInt16(System.String)
extern const MethodInfo SerializationInfo_GetInt16_m10981_MethodInfo = 
{
	"GetInt16"/* name */
	, (methodPointerType)&SerializationInfo_GetInt16_m10981/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Int16_t752_0_0_0/* return_type */
	, RuntimeInvoker_Int16_t752_Object_t/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_GetInt16_m10981_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_GetInt32_m6400_ParameterInfos[] = 
{
	{"name", 0, 134222964, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.SerializationInfo::GetInt32(System.String)
extern const MethodInfo SerializationInfo_GetInt32_m6400_MethodInfo = 
{
	"GetInt32"/* name */
	, (methodPointerType)&SerializationInfo_GetInt32_m6400/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_GetInt32_m6400_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_GetInt64_m6399_ParameterInfos[] = 
{
	{"name", 0, 134222965, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Runtime.Serialization.SerializationInfo::GetInt64(System.String)
extern const MethodInfo SerializationInfo_GetInt64_m6399_MethodInfo = 
{
	"GetInt64"/* name */
	, (methodPointerType)&SerializationInfo_GetInt64_m6399/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1071_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1071_Object_t/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_GetInt64_m6399_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SerializationInfo_t1043_SerializationInfo_GetString_m6398_ParameterInfos[] = 
{
	{"name", 0, 134222966, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.SerializationInfo::GetString(System.String)
extern const MethodInfo SerializationInfo_GetString_m6398_MethodInfo = 
{
	"GetString"/* name */
	, (methodPointerType)&SerializationInfo_GetString_m6398/* method */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, SerializationInfo_t1043_SerializationInfo_GetString_m6398_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SerializationInfo_t1043_MethodInfos[] =
{
	&SerializationInfo__ctor_m10971_MethodInfo,
	&SerializationInfo_get_AssemblyName_m10972_MethodInfo,
	&SerializationInfo_get_FullTypeName_m10973_MethodInfo,
	&SerializationInfo_get_MemberCount_m10974_MethodInfo,
	&SerializationInfo_AddValue_m6389_MethodInfo,
	&SerializationInfo_GetValue_m6392_MethodInfo,
	&SerializationInfo_SetType_m10975_MethodInfo,
	&SerializationInfo_GetEnumerator_m10976_MethodInfo,
	&SerializationInfo_AddValue_m10977_MethodInfo,
	&SerializationInfo_AddValue_m6391_MethodInfo,
	&SerializationInfo_AddValue_m6390_MethodInfo,
	&SerializationInfo_AddValue_m10978_MethodInfo,
	&SerializationInfo_AddValue_m10979_MethodInfo,
	&SerializationInfo_AddValue_m6402_MethodInfo,
	&SerializationInfo_AddValue_m10980_MethodInfo,
	&SerializationInfo_AddValue_m6401_MethodInfo,
	&SerializationInfo_GetBoolean_m6394_MethodInfo,
	&SerializationInfo_GetInt16_m10981_MethodInfo,
	&SerializationInfo_GetInt32_m6400_MethodInfo,
	&SerializationInfo_GetInt64_m6399_MethodInfo,
	&SerializationInfo_GetString_m6398_MethodInfo,
	NULL
};
extern const MethodInfo SerializationInfo_get_AssemblyName_m10972_MethodInfo;
static const PropertyInfo SerializationInfo_t1043____AssemblyName_PropertyInfo = 
{
	&SerializationInfo_t1043_il2cpp_TypeInfo/* parent */
	, "AssemblyName"/* name */
	, &SerializationInfo_get_AssemblyName_m10972_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SerializationInfo_get_FullTypeName_m10973_MethodInfo;
static const PropertyInfo SerializationInfo_t1043____FullTypeName_PropertyInfo = 
{
	&SerializationInfo_t1043_il2cpp_TypeInfo/* parent */
	, "FullTypeName"/* name */
	, &SerializationInfo_get_FullTypeName_m10973_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SerializationInfo_get_MemberCount_m10974_MethodInfo;
static const PropertyInfo SerializationInfo_t1043____MemberCount_PropertyInfo = 
{
	&SerializationInfo_t1043_il2cpp_TypeInfo/* parent */
	, "MemberCount"/* name */
	, &SerializationInfo_get_MemberCount_m10974_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SerializationInfo_t1043_PropertyInfos[] =
{
	&SerializationInfo_t1043____AssemblyName_PropertyInfo,
	&SerializationInfo_t1043____FullTypeName_PropertyInfo,
	&SerializationInfo_t1043____MemberCount_PropertyInfo,
	NULL
};
static const Il2CppMethodReference SerializationInfo_t1043_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SerializationInfo_t1043_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
struct SerializationInfo_t1043;
const Il2CppTypeDefinitionMetadata SerializationInfo_t1043_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationInfo_t1043_VTable/* vtableMethods */
	, SerializationInfo_t1043_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2267/* fieldStart */

};
TypeInfo SerializationInfo_t1043_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationInfo"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, SerializationInfo_t1043_MethodInfos/* methods */
	, SerializationInfo_t1043_PropertyInfos/* properties */
	, NULL/* events */
	, &SerializationInfo_t1043_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 632/* custom_attributes_cache */
	, &SerializationInfo_t1043_0_0_0/* byval_arg */
	, &SerializationInfo_t1043_1_0_0/* this_arg */
	, &SerializationInfo_t1043_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationInfo_t1043)/* instance_size */
	, sizeof (SerializationInfo_t1043)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationInfoEnumerator
#include "mscorlib_System_Runtime_Serialization_SerializationInfoEnume.h"
// Metadata Definition System.Runtime.Serialization.SerializationInfoEnumerator
extern TypeInfo SerializationInfoEnumerator_t2065_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationInfoEnumerator
#include "mscorlib_System_Runtime_Serialization_SerializationInfoEnumeMethodDeclarations.h"
extern const Il2CppType ArrayList_t1271_0_0_0;
static const ParameterInfo SerializationInfoEnumerator_t2065_SerializationInfoEnumerator__ctor_m10982_ParameterInfos[] = 
{
	{"list", 0, 134222967, 0, &ArrayList_t1271_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationInfoEnumerator::.ctor(System.Collections.ArrayList)
extern const MethodInfo SerializationInfoEnumerator__ctor_m10982_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SerializationInfoEnumerator__ctor_m10982/* method */
	, &SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SerializationInfoEnumerator_t2065_SerializationInfoEnumerator__ctor_m10982_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.SerializationInfoEnumerator::System.Collections.IEnumerator.get_Current()
extern const MethodInfo SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m10983_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m10983/* method */
	, &SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_SerializationEntry_t2063 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.SerializationEntry System.Runtime.Serialization.SerializationInfoEnumerator::get_Current()
extern const MethodInfo SerializationInfoEnumerator_get_Current_m10984_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&SerializationInfoEnumerator_get_Current_m10984/* method */
	, &SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* declaring_type */
	, &SerializationEntry_t2063_0_0_0/* return_type */
	, RuntimeInvoker_SerializationEntry_t2063/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.SerializationInfoEnumerator::get_Name()
extern const MethodInfo SerializationInfoEnumerator_get_Name_m10985_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&SerializationInfoEnumerator_get_Name_m10985/* method */
	, &SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.SerializationInfoEnumerator::get_ObjectType()
extern const MethodInfo SerializationInfoEnumerator_get_ObjectType_m10986_MethodInfo = 
{
	"get_ObjectType"/* name */
	, (methodPointerType)&SerializationInfoEnumerator_get_ObjectType_m10986/* method */
	, &SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.SerializationInfoEnumerator::get_Value()
extern const MethodInfo SerializationInfoEnumerator_get_Value_m10987_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&SerializationInfoEnumerator_get_Value_m10987/* method */
	, &SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.SerializationInfoEnumerator::MoveNext()
extern const MethodInfo SerializationInfoEnumerator_MoveNext_m10988_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&SerializationInfoEnumerator_MoveNext_m10988/* method */
	, &SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SerializationInfoEnumerator_t2065_MethodInfos[] =
{
	&SerializationInfoEnumerator__ctor_m10982_MethodInfo,
	&SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m10983_MethodInfo,
	&SerializationInfoEnumerator_get_Current_m10984_MethodInfo,
	&SerializationInfoEnumerator_get_Name_m10985_MethodInfo,
	&SerializationInfoEnumerator_get_ObjectType_m10986_MethodInfo,
	&SerializationInfoEnumerator_get_Value_m10987_MethodInfo,
	&SerializationInfoEnumerator_MoveNext_m10988_MethodInfo,
	NULL
};
extern const MethodInfo SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m10983_MethodInfo;
static const PropertyInfo SerializationInfoEnumerator_t2065____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m10983_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SerializationInfoEnumerator_get_Current_m10984_MethodInfo;
static const PropertyInfo SerializationInfoEnumerator_t2065____Current_PropertyInfo = 
{
	&SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &SerializationInfoEnumerator_get_Current_m10984_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SerializationInfoEnumerator_get_Name_m10985_MethodInfo;
static const PropertyInfo SerializationInfoEnumerator_t2065____Name_PropertyInfo = 
{
	&SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &SerializationInfoEnumerator_get_Name_m10985_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SerializationInfoEnumerator_get_ObjectType_m10986_MethodInfo;
static const PropertyInfo SerializationInfoEnumerator_t2065____ObjectType_PropertyInfo = 
{
	&SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* parent */
	, "ObjectType"/* name */
	, &SerializationInfoEnumerator_get_ObjectType_m10986_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SerializationInfoEnumerator_get_Value_m10987_MethodInfo;
static const PropertyInfo SerializationInfoEnumerator_t2065____Value_PropertyInfo = 
{
	&SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &SerializationInfoEnumerator_get_Value_m10987_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SerializationInfoEnumerator_t2065_PropertyInfos[] =
{
	&SerializationInfoEnumerator_t2065____System_Collections_IEnumerator_Current_PropertyInfo,
	&SerializationInfoEnumerator_t2065____Current_PropertyInfo,
	&SerializationInfoEnumerator_t2065____Name_PropertyInfo,
	&SerializationInfoEnumerator_t2065____ObjectType_PropertyInfo,
	&SerializationInfoEnumerator_t2065____Value_PropertyInfo,
	NULL
};
extern const MethodInfo SerializationInfoEnumerator_MoveNext_m10988_MethodInfo;
static const Il2CppMethodReference SerializationInfoEnumerator_t2065_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m10983_MethodInfo,
	&SerializationInfoEnumerator_MoveNext_m10988_MethodInfo,
};
static bool SerializationInfoEnumerator_t2065_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_t217_0_0_0;
static const Il2CppType* SerializationInfoEnumerator_t2065_InterfacesTypeInfos[] = 
{
	&IEnumerator_t217_0_0_0,
};
static Il2CppInterfaceOffsetPair SerializationInfoEnumerator_t2065_InterfacesOffsets[] = 
{
	{ &IEnumerator_t217_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationInfoEnumerator_t2065_1_0_0;
struct SerializationInfoEnumerator_t2065;
const Il2CppTypeDefinitionMetadata SerializationInfoEnumerator_t2065_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SerializationInfoEnumerator_t2065_InterfacesTypeInfos/* implementedInterfaces */
	, SerializationInfoEnumerator_t2065_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationInfoEnumerator_t2065_VTable/* vtableMethods */
	, SerializationInfoEnumerator_t2065_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2272/* fieldStart */

};
TypeInfo SerializationInfoEnumerator_t2065_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationInfoEnumerator"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, SerializationInfoEnumerator_t2065_MethodInfos/* methods */
	, SerializationInfoEnumerator_t2065_PropertyInfos/* properties */
	, NULL/* events */
	, &SerializationInfoEnumerator_t2065_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 635/* custom_attributes_cache */
	, &SerializationInfoEnumerator_t2065_0_0_0/* byval_arg */
	, &SerializationInfoEnumerator_t2065_1_0_0/* this_arg */
	, &SerializationInfoEnumerator_t2065_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationInfoEnumerator_t2065)/* instance_size */
	, sizeof (SerializationInfoEnumerator_t2065)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 5/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationObjectManager/<RegisterObject>c__AnonStorey3
#include "mscorlib_System_Runtime_Serialization_SerializationObjectMan.h"
// Metadata Definition System.Runtime.Serialization.SerializationObjectManager/<RegisterObject>c__AnonStorey3
extern TypeInfo U3CRegisterObjectU3Ec__AnonStorey3_t2066_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationObjectManager/<RegisterObject>c__AnonStorey3
#include "mscorlib_System_Runtime_Serialization_SerializationObjectManMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationObjectManager/<RegisterObject>c__AnonStorey3::.ctor()
extern const MethodInfo U3CRegisterObjectU3Ec__AnonStorey3__ctor_m10989_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CRegisterObjectU3Ec__AnonStorey3__ctor_m10989/* method */
	, &U3CRegisterObjectU3Ec__AnonStorey3_t2066_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo U3CRegisterObjectU3Ec__AnonStorey3_t2066_U3CRegisterObjectU3Ec__AnonStorey3_U3CU3Em__2_m10990_ParameterInfos[] = 
{
	{"ctx", 0, 134222970, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationObjectManager/<RegisterObject>c__AnonStorey3::<>m__2(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo U3CRegisterObjectU3Ec__AnonStorey3_U3CU3Em__2_m10990_MethodInfo = 
{
	"<>m__2"/* name */
	, (methodPointerType)&U3CRegisterObjectU3Ec__AnonStorey3_U3CU3Em__2_m10990/* method */
	, &U3CRegisterObjectU3Ec__AnonStorey3_t2066_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_StreamingContext_t1044/* invoker_method */
	, U3CRegisterObjectU3Ec__AnonStorey3_t2066_U3CRegisterObjectU3Ec__AnonStorey3_U3CU3Em__2_m10990_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CRegisterObjectU3Ec__AnonStorey3_t2066_MethodInfos[] =
{
	&U3CRegisterObjectU3Ec__AnonStorey3__ctor_m10989_MethodInfo,
	&U3CRegisterObjectU3Ec__AnonStorey3_U3CU3Em__2_m10990_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CRegisterObjectU3Ec__AnonStorey3_t2066_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool U3CRegisterObjectU3Ec__AnonStorey3_t2066_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U3CRegisterObjectU3Ec__AnonStorey3_t2066_0_0_0;
extern const Il2CppType U3CRegisterObjectU3Ec__AnonStorey3_t2066_1_0_0;
extern TypeInfo SerializationObjectManager_t2041_il2cpp_TypeInfo;
extern const Il2CppType SerializationObjectManager_t2041_0_0_0;
struct U3CRegisterObjectU3Ec__AnonStorey3_t2066;
const Il2CppTypeDefinitionMetadata U3CRegisterObjectU3Ec__AnonStorey3_t2066_DefinitionMetadata = 
{
	&SerializationObjectManager_t2041_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CRegisterObjectU3Ec__AnonStorey3_t2066_VTable/* vtableMethods */
	, U3CRegisterObjectU3Ec__AnonStorey3_t2066_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2273/* fieldStart */

};
TypeInfo U3CRegisterObjectU3Ec__AnonStorey3_t2066_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "<RegisterObject>c__AnonStorey3"/* name */
	, ""/* namespaze */
	, U3CRegisterObjectU3Ec__AnonStorey3_t2066_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CRegisterObjectU3Ec__AnonStorey3_t2066_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 636/* custom_attributes_cache */
	, &U3CRegisterObjectU3Ec__AnonStorey3_t2066_0_0_0/* byval_arg */
	, &U3CRegisterObjectU3Ec__AnonStorey3_t2066_1_0_0/* this_arg */
	, &U3CRegisterObjectU3Ec__AnonStorey3_t2066_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CRegisterObjectU3Ec__AnonStorey3_t2066)/* instance_size */
	, sizeof (U3CRegisterObjectU3Ec__AnonStorey3_t2066)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationObjectManager
#include "mscorlib_System_Runtime_Serialization_SerializationObjectMan_0.h"
// Metadata Definition System.Runtime.Serialization.SerializationObjectManager
// System.Runtime.Serialization.SerializationObjectManager
#include "mscorlib_System_Runtime_Serialization_SerializationObjectMan_0MethodDeclarations.h"
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo SerializationObjectManager_t2041_SerializationObjectManager__ctor_m10991_ParameterInfos[] = 
{
	{"context", 0, 134222968, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationObjectManager::.ctor(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo SerializationObjectManager__ctor_m10991_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SerializationObjectManager__ctor_m10991/* method */
	, &SerializationObjectManager_t2041_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_StreamingContext_t1044/* invoker_method */
	, SerializationObjectManager_t2041_SerializationObjectManager__ctor_m10991_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SerializationObjectManager_t2041_SerializationObjectManager_RegisterObject_m10992_ParameterInfos[] = 
{
	{"obj", 0, 134222969, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationObjectManager::RegisterObject(System.Object)
extern const MethodInfo SerializationObjectManager_RegisterObject_m10992_MethodInfo = 
{
	"RegisterObject"/* name */
	, (methodPointerType)&SerializationObjectManager_RegisterObject_m10992/* method */
	, &SerializationObjectManager_t2041_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SerializationObjectManager_t2041_SerializationObjectManager_RegisterObject_m10992_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.SerializationObjectManager::RaiseOnSerializedEvent()
extern const MethodInfo SerializationObjectManager_RaiseOnSerializedEvent_m10993_MethodInfo = 
{
	"RaiseOnSerializedEvent"/* name */
	, (methodPointerType)&SerializationObjectManager_RaiseOnSerializedEvent_m10993/* method */
	, &SerializationObjectManager_t2041_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SerializationObjectManager_t2041_MethodInfos[] =
{
	&SerializationObjectManager__ctor_m10991_MethodInfo,
	&SerializationObjectManager_RegisterObject_m10992_MethodInfo,
	&SerializationObjectManager_RaiseOnSerializedEvent_m10993_MethodInfo,
	NULL
};
static const Il2CppType* SerializationObjectManager_t2041_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CRegisterObjectU3Ec__AnonStorey3_t2066_0_0_0,
};
static const Il2CppMethodReference SerializationObjectManager_t2041_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SerializationObjectManager_t2041_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationObjectManager_t2041_1_0_0;
struct SerializationObjectManager_t2041;
const Il2CppTypeDefinitionMetadata SerializationObjectManager_t2041_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SerializationObjectManager_t2041_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationObjectManager_t2041_VTable/* vtableMethods */
	, SerializationObjectManager_t2041_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2275/* fieldStart */

};
TypeInfo SerializationObjectManager_t2041_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationObjectManager"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, SerializationObjectManager_t2041_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SerializationObjectManager_t2041_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SerializationObjectManager_t2041_0_0_0/* byval_arg */
	, &SerializationObjectManager_t2041_1_0_0/* this_arg */
	, &SerializationObjectManager_t2041_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationObjectManager_t2041)/* instance_size */
	, sizeof (SerializationObjectManager_t2041)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// Metadata Definition System.Runtime.Serialization.StreamingContext
extern TypeInfo StreamingContext_t1044_il2cpp_TypeInfo;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContextMethodDeclarations.h"
extern const Il2CppType StreamingContextStates_t2067_0_0_0;
extern const Il2CppType StreamingContextStates_t2067_0_0_0;
static const ParameterInfo StreamingContext_t1044_StreamingContext__ctor_m10994_ParameterInfos[] = 
{
	{"state", 0, 134222971, 0, &StreamingContextStates_t2067_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.StreamingContext::.ctor(System.Runtime.Serialization.StreamingContextStates)
extern const MethodInfo StreamingContext__ctor_m10994_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StreamingContext__ctor_m10994/* method */
	, &StreamingContext_t1044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, StreamingContext_t1044_StreamingContext__ctor_m10994_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContextStates_t2067_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StreamingContext_t1044_StreamingContext__ctor_m10995_ParameterInfos[] = 
{
	{"state", 0, 134222972, 0, &StreamingContextStates_t2067_0_0_0},
	{"additional", 1, 134222973, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.StreamingContext::.ctor(System.Runtime.Serialization.StreamingContextStates,System.Object)
extern const MethodInfo StreamingContext__ctor_m10995_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StreamingContext__ctor_m10995/* method */
	, &StreamingContext_t1044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Object_t/* invoker_method */
	, StreamingContext_t1044_StreamingContext__ctor_m10995_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.StreamingContext::get_Context()
extern const MethodInfo StreamingContext_get_Context_m10996_MethodInfo = 
{
	"get_Context"/* name */
	, (methodPointerType)&StreamingContext_get_Context_m10996/* method */
	, &StreamingContext_t1044_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_StreamingContextStates_t2067 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::get_State()
extern const MethodInfo StreamingContext_get_State_m10997_MethodInfo = 
{
	"get_State"/* name */
	, (methodPointerType)&StreamingContext_get_State_m10997/* method */
	, &StreamingContext_t1044_il2cpp_TypeInfo/* declaring_type */
	, &StreamingContextStates_t2067_0_0_0/* return_type */
	, RuntimeInvoker_StreamingContextStates_t2067/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4285/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StreamingContext_t1044_StreamingContext_Equals_m10998_ParameterInfos[] = 
{
	{"obj", 0, 134222974, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.StreamingContext::Equals(System.Object)
extern const MethodInfo StreamingContext_Equals_m10998_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&StreamingContext_Equals_m10998/* method */
	, &StreamingContext_t1044_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, StreamingContext_t1044_StreamingContext_Equals_m10998_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4286/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.StreamingContext::GetHashCode()
extern const MethodInfo StreamingContext_GetHashCode_m10999_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&StreamingContext_GetHashCode_m10999/* method */
	, &StreamingContext_t1044_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StreamingContext_t1044_MethodInfos[] =
{
	&StreamingContext__ctor_m10994_MethodInfo,
	&StreamingContext__ctor_m10995_MethodInfo,
	&StreamingContext_get_Context_m10996_MethodInfo,
	&StreamingContext_get_State_m10997_MethodInfo,
	&StreamingContext_Equals_m10998_MethodInfo,
	&StreamingContext_GetHashCode_m10999_MethodInfo,
	NULL
};
extern const MethodInfo StreamingContext_get_Context_m10996_MethodInfo;
static const PropertyInfo StreamingContext_t1044____Context_PropertyInfo = 
{
	&StreamingContext_t1044_il2cpp_TypeInfo/* parent */
	, "Context"/* name */
	, &StreamingContext_get_Context_m10996_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo StreamingContext_get_State_m10997_MethodInfo;
static const PropertyInfo StreamingContext_t1044____State_PropertyInfo = 
{
	&StreamingContext_t1044_il2cpp_TypeInfo/* parent */
	, "State"/* name */
	, &StreamingContext_get_State_m10997_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* StreamingContext_t1044_PropertyInfos[] =
{
	&StreamingContext_t1044____Context_PropertyInfo,
	&StreamingContext_t1044____State_PropertyInfo,
	NULL
};
extern const MethodInfo StreamingContext_Equals_m10998_MethodInfo;
extern const MethodInfo StreamingContext_GetHashCode_m10999_MethodInfo;
static const Il2CppMethodReference StreamingContext_t1044_VTable[] =
{
	&StreamingContext_Equals_m10998_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&StreamingContext_GetHashCode_m10999_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool StreamingContext_t1044_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamingContext_t1044_1_0_0;
const Il2CppTypeDefinitionMetadata StreamingContext_t1044_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, StreamingContext_t1044_VTable/* vtableMethods */
	, StreamingContext_t1044_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2278/* fieldStart */

};
TypeInfo StreamingContext_t1044_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamingContext"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, StreamingContext_t1044_MethodInfos/* methods */
	, StreamingContext_t1044_PropertyInfos/* properties */
	, NULL/* events */
	, &StreamingContext_t1044_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 637/* custom_attributes_cache */
	, &StreamingContext_t1044_0_0_0/* byval_arg */
	, &StreamingContext_t1044_1_0_0/* this_arg */
	, &StreamingContext_t1044_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamingContext_t1044)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StreamingContext_t1044)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
// Metadata Definition System.Runtime.Serialization.StreamingContextStates
extern TypeInfo StreamingContextStates_t2067_il2cpp_TypeInfo;
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStatesMethodDeclarations.h"
static const MethodInfo* StreamingContextStates_t2067_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference StreamingContextStates_t2067_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool StreamingContextStates_t2067_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StreamingContextStates_t2067_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamingContextStates_t2067_1_0_0;
const Il2CppTypeDefinitionMetadata StreamingContextStates_t2067_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StreamingContextStates_t2067_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, StreamingContextStates_t2067_VTable/* vtableMethods */
	, StreamingContextStates_t2067_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2280/* fieldStart */

};
TypeInfo StreamingContextStates_t2067_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamingContextStates"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, StreamingContextStates_t2067_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 638/* custom_attributes_cache */
	, &StreamingContextStates_t2067_0_0_0/* byval_arg */
	, &StreamingContextStates_t2067_1_0_0/* this_arg */
	, &StreamingContextStates_t2067_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamingContextStates_t2067)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StreamingContextStates_t2067)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Certificate
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509C.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Certificate
extern TypeInfo X509Certificate_t1323_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Certificate
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509CMethodDeclarations.h"
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo X509Certificate_t1323_X509Certificate__ctor_m11000_ParameterInfos[] = 
{
	{"data", 0, 134222975, 0, &ByteU5BU5D_t850_0_0_0},
	{"dates", 1, 134222976, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[],System.Boolean)
extern const MethodInfo X509Certificate__ctor_m11000_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&X509Certificate__ctor_m11000/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, X509Certificate_t1323_X509Certificate__ctor_m11000_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4288/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo X509Certificate_t1323_X509Certificate__ctor_m7567_ParameterInfos[] = 
{
	{"data", 0, 134222977, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[])
extern const MethodInfo X509Certificate__ctor_m7567_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&X509Certificate__ctor_m7567/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, X509Certificate_t1323_X509Certificate__ctor_m7567_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4289/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor()
extern const MethodInfo X509Certificate__ctor_m6447_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&X509Certificate__ctor_m6447/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo X509Certificate_t1323_X509Certificate__ctor_m11001_ParameterInfos[] = 
{
	{"info", 0, 134222978, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222979, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo X509Certificate__ctor_m11001_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&X509Certificate__ctor_m11001/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, X509Certificate_t1323_X509Certificate__ctor_m11001_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo X509Certificate_t1323_X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6619_ParameterInfos[] = 
{
	{"sender", 0, 134222980, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern const MethodInfo X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6619_MethodInfo = 
{
	"System.Runtime.Serialization.IDeserializationCallback.OnDeserialization"/* name */
	, (methodPointerType)&X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6619/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, X509Certificate_t1323_X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6619_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo X509Certificate_t1323_X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m6618_ParameterInfos[] = 
{
	{"info", 0, 134222981, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134222982, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m6618_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m6618/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, X509Certificate_t1323_X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m6618_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo X509Certificate_t1323_X509Certificate_tostr_m11002_ParameterInfos[] = 
{
	{"data", 0, 134222983, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::tostr(System.Byte[])
extern const MethodInfo X509Certificate_tostr_m11002_MethodInfo = 
{
	"tostr"/* name */
	, (methodPointerType)&X509Certificate_tostr_m11002/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, X509Certificate_t1323_X509Certificate_tostr_m11002_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1323_0_0_0;
extern const Il2CppType X509Certificate_t1323_0_0_0;
static const ParameterInfo X509Certificate_t1323_X509Certificate_Equals_m6620_ParameterInfos[] = 
{
	{"other", 0, 134222984, 0, &X509Certificate_t1323_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::Equals(System.Security.Cryptography.X509Certificates.X509Certificate)
extern const MethodInfo X509Certificate_Equals_m6620_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&X509Certificate_Equals_m6620/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, X509Certificate_t1323_X509Certificate_Equals_m6620_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetCertHash()
extern const MethodInfo X509Certificate_GetCertHash_m6621_MethodInfo = 
{
	"GetCertHash"/* name */
	, (methodPointerType)&X509Certificate_GetCertHash_m6621/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetCertHashString()
extern const MethodInfo X509Certificate_GetCertHashString_m6452_MethodInfo = 
{
	"GetCertHashString"/* name */
	, (methodPointerType)&X509Certificate_GetCertHashString_m6452/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetEffectiveDateString()
extern const MethodInfo X509Certificate_GetEffectiveDateString_m6622_MethodInfo = 
{
	"GetEffectiveDateString"/* name */
	, (methodPointerType)&X509Certificate_GetEffectiveDateString_m6622/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4298/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetExpirationDateString()
extern const MethodInfo X509Certificate_GetExpirationDateString_m6623_MethodInfo = 
{
	"GetExpirationDateString"/* name */
	, (methodPointerType)&X509Certificate_GetExpirationDateString_m6623/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4299/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Security.Cryptography.X509Certificates.X509Certificate::GetHashCode()
extern const MethodInfo X509Certificate_GetHashCode_m6617_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&X509Certificate_GetHashCode_m6617/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4300/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetIssuerName()
extern const MethodInfo X509Certificate_GetIssuerName_m6624_MethodInfo = 
{
	"GetIssuerName"/* name */
	, (methodPointerType)&X509Certificate_GetIssuerName_m6624/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 640/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4301/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetName()
extern const MethodInfo X509Certificate_GetName_m6625_MethodInfo = 
{
	"GetName"/* name */
	, (methodPointerType)&X509Certificate_GetName_m6625/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 641/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4302/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetPublicKey()
extern const MethodInfo X509Certificate_GetPublicKey_m6626_MethodInfo = 
{
	"GetPublicKey"/* name */
	, (methodPointerType)&X509Certificate_GetPublicKey_m6626/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4303/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetRawCertData()
extern const MethodInfo X509Certificate_GetRawCertData_m6627_MethodInfo = 
{
	"GetRawCertData"/* name */
	, (methodPointerType)&X509Certificate_GetRawCertData_m6627/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4304/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::ToString()
extern const MethodInfo X509Certificate_ToString_m11003_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&X509Certificate_ToString_m11003/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4305/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo X509Certificate_t1323_X509Certificate_ToString_m6465_ParameterInfos[] = 
{
	{"fVerbose", 0, 134222985, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::ToString(System.Boolean)
extern const MethodInfo X509Certificate_ToString_m6465_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&X509Certificate_ToString_m6465/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t274/* invoker_method */
	, X509Certificate_t1323_X509Certificate_ToString_m6465_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4306/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::get_Issuer()
extern const MethodInfo X509Certificate_get_Issuer_m6468_MethodInfo = 
{
	"get_Issuer"/* name */
	, (methodPointerType)&X509Certificate_get_Issuer_m6468/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4307/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::get_Subject()
extern const MethodInfo X509Certificate_get_Subject_m6467_MethodInfo = 
{
	"get_Subject"/* name */
	, (methodPointerType)&X509Certificate_get_Subject_m6467/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4308/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo X509Certificate_t1323_X509Certificate_Equals_m6616_ParameterInfos[] = 
{
	{"obj", 0, 134222986, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::Equals(System.Object)
extern const MethodInfo X509Certificate_Equals_m6616_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&X509Certificate_Equals_m6616/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, X509Certificate_t1323_X509Certificate_Equals_m6616_ParameterInfos/* parameters */
	, 642/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4309/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType X509KeyStorageFlags_t1489_0_0_0;
extern const Il2CppType X509KeyStorageFlags_t1489_0_0_0;
static const ParameterInfo X509Certificate_t1323_X509Certificate_Import_m6462_ParameterInfos[] = 
{
	{"rawData", 0, 134222987, 0, &ByteU5BU5D_t850_0_0_0},
	{"password", 1, 134222988, 0, &String_t_0_0_0},
	{"keyStorageFlags", 2, 134222989, 0, &X509KeyStorageFlags_t1489_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern const MethodInfo X509Certificate_Import_m6462_MethodInfo = 
{
	"Import"/* name */
	, (methodPointerType)&X509Certificate_Import_m6462/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253/* invoker_method */
	, X509Certificate_t1323_X509Certificate_Import_m6462_ParameterInfos/* parameters */
	, 643/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4310/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Reset()
extern const MethodInfo X509Certificate_Reset_m6464_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&X509Certificate_Reset_m6464/* method */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 644/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4311/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* X509Certificate_t1323_MethodInfos[] =
{
	&X509Certificate__ctor_m11000_MethodInfo,
	&X509Certificate__ctor_m7567_MethodInfo,
	&X509Certificate__ctor_m6447_MethodInfo,
	&X509Certificate__ctor_m11001_MethodInfo,
	&X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6619_MethodInfo,
	&X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m6618_MethodInfo,
	&X509Certificate_tostr_m11002_MethodInfo,
	&X509Certificate_Equals_m6620_MethodInfo,
	&X509Certificate_GetCertHash_m6621_MethodInfo,
	&X509Certificate_GetCertHashString_m6452_MethodInfo,
	&X509Certificate_GetEffectiveDateString_m6622_MethodInfo,
	&X509Certificate_GetExpirationDateString_m6623_MethodInfo,
	&X509Certificate_GetHashCode_m6617_MethodInfo,
	&X509Certificate_GetIssuerName_m6624_MethodInfo,
	&X509Certificate_GetName_m6625_MethodInfo,
	&X509Certificate_GetPublicKey_m6626_MethodInfo,
	&X509Certificate_GetRawCertData_m6627_MethodInfo,
	&X509Certificate_ToString_m11003_MethodInfo,
	&X509Certificate_ToString_m6465_MethodInfo,
	&X509Certificate_get_Issuer_m6468_MethodInfo,
	&X509Certificate_get_Subject_m6467_MethodInfo,
	&X509Certificate_Equals_m6616_MethodInfo,
	&X509Certificate_Import_m6462_MethodInfo,
	&X509Certificate_Reset_m6464_MethodInfo,
	NULL
};
extern const MethodInfo X509Certificate_get_Issuer_m6468_MethodInfo;
static const PropertyInfo X509Certificate_t1323____Issuer_PropertyInfo = 
{
	&X509Certificate_t1323_il2cpp_TypeInfo/* parent */
	, "Issuer"/* name */
	, &X509Certificate_get_Issuer_m6468_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo X509Certificate_get_Subject_m6467_MethodInfo;
static const PropertyInfo X509Certificate_t1323____Subject_PropertyInfo = 
{
	&X509Certificate_t1323_il2cpp_TypeInfo/* parent */
	, "Subject"/* name */
	, &X509Certificate_get_Subject_m6467_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* X509Certificate_t1323_PropertyInfos[] =
{
	&X509Certificate_t1323____Issuer_PropertyInfo,
	&X509Certificate_t1323____Subject_PropertyInfo,
	NULL
};
extern const MethodInfo X509Certificate_Equals_m6616_MethodInfo;
extern const MethodInfo X509Certificate_GetHashCode_m6617_MethodInfo;
extern const MethodInfo X509Certificate_ToString_m11003_MethodInfo;
extern const MethodInfo X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m6618_MethodInfo;
extern const MethodInfo X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6619_MethodInfo;
extern const MethodInfo X509Certificate_Equals_m6620_MethodInfo;
extern const MethodInfo X509Certificate_GetCertHash_m6621_MethodInfo;
extern const MethodInfo X509Certificate_GetCertHashString_m6452_MethodInfo;
extern const MethodInfo X509Certificate_GetEffectiveDateString_m6622_MethodInfo;
extern const MethodInfo X509Certificate_GetExpirationDateString_m6623_MethodInfo;
extern const MethodInfo X509Certificate_GetIssuerName_m6624_MethodInfo;
extern const MethodInfo X509Certificate_GetName_m6625_MethodInfo;
extern const MethodInfo X509Certificate_GetPublicKey_m6626_MethodInfo;
extern const MethodInfo X509Certificate_GetRawCertData_m6627_MethodInfo;
extern const MethodInfo X509Certificate_ToString_m6465_MethodInfo;
extern const MethodInfo X509Certificate_Import_m6462_MethodInfo;
extern const MethodInfo X509Certificate_Reset_m6464_MethodInfo;
static const Il2CppMethodReference X509Certificate_t1323_VTable[] =
{
	&X509Certificate_Equals_m6616_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&X509Certificate_GetHashCode_m6617_MethodInfo,
	&X509Certificate_ToString_m11003_MethodInfo,
	&X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m6618_MethodInfo,
	&X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6619_MethodInfo,
	&X509Certificate_Equals_m6620_MethodInfo,
	&X509Certificate_GetCertHash_m6621_MethodInfo,
	&X509Certificate_GetCertHashString_m6452_MethodInfo,
	&X509Certificate_GetEffectiveDateString_m6622_MethodInfo,
	&X509Certificate_GetExpirationDateString_m6623_MethodInfo,
	&X509Certificate_GetIssuerName_m6624_MethodInfo,
	&X509Certificate_GetName_m6625_MethodInfo,
	&X509Certificate_GetPublicKey_m6626_MethodInfo,
	&X509Certificate_GetRawCertData_m6627_MethodInfo,
	&X509Certificate_ToString_m6465_MethodInfo,
	&X509Certificate_Import_m6462_MethodInfo,
	&X509Certificate_Reset_m6464_MethodInfo,
};
static bool X509Certificate_t1323_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* X509Certificate_t1323_InterfacesTypeInfos[] = 
{
	&ISerializable_t439_0_0_0,
	&IDeserializationCallback_t445_0_0_0,
};
static Il2CppInterfaceOffsetPair X509Certificate_t1323_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &IDeserializationCallback_t445_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509Certificate_t1323_1_0_0;
struct X509Certificate_t1323;
const Il2CppTypeDefinitionMetadata X509Certificate_t1323_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509Certificate_t1323_InterfacesTypeInfos/* implementedInterfaces */
	, X509Certificate_t1323_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Certificate_t1323_VTable/* vtableMethods */
	, X509Certificate_t1323_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2290/* fieldStart */

};
TypeInfo X509Certificate_t1323_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, X509Certificate_t1323_MethodInfos/* methods */
	, X509Certificate_t1323_PropertyInfos/* properties */
	, NULL/* events */
	, &X509Certificate_t1323_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 639/* custom_attributes_cache */
	, &X509Certificate_t1323_0_0_0/* byval_arg */
	, &X509Certificate_t1323_1_0_0/* this_arg */
	, &X509Certificate_t1323_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate_t1323)/* instance_size */
	, sizeof (X509Certificate_t1323)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509K.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
extern TypeInfo X509KeyStorageFlags_t1489_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509KMethodDeclarations.h"
static const MethodInfo* X509KeyStorageFlags_t1489_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference X509KeyStorageFlags_t1489_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool X509KeyStorageFlags_t1489_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair X509KeyStorageFlags_t1489_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509KeyStorageFlags_t1489_1_0_0;
const Il2CppTypeDefinitionMetadata X509KeyStorageFlags_t1489_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509KeyStorageFlags_t1489_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, X509KeyStorageFlags_t1489_VTable/* vtableMethods */
	, X509KeyStorageFlags_t1489_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2295/* fieldStart */

};
TypeInfo X509KeyStorageFlags_t1489_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509KeyStorageFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, X509KeyStorageFlags_t1489_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 645/* custom_attributes_cache */
	, &X509KeyStorageFlags_t1489_0_0_0/* byval_arg */
	, &X509KeyStorageFlags_t1489_1_0_0/* this_arg */
	, &X509KeyStorageFlags_t1489_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509KeyStorageFlags_t1489)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509KeyStorageFlags_t1489)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_AsymmetricAlgorithm.h"
// Metadata Definition System.Security.Cryptography.AsymmetricAlgorithm
extern TypeInfo AsymmetricAlgorithm_t1310_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_AsymmetricAlgorithmMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::.ctor()
extern const MethodInfo AsymmetricAlgorithm__ctor_m11004_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AsymmetricAlgorithm__ctor_m11004/* method */
	, &AsymmetricAlgorithm_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4312/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::System.IDisposable.Dispose()
extern const MethodInfo AsymmetricAlgorithm_System_IDisposable_Dispose_m7612_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&AsymmetricAlgorithm_System_IDisposable_Dispose_m7612/* method */
	, &AsymmetricAlgorithm_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4313/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Security.Cryptography.AsymmetricAlgorithm::get_KeySize()
extern const MethodInfo AsymmetricAlgorithm_get_KeySize_m7518_MethodInfo = 
{
	"get_KeySize"/* name */
	, (methodPointerType)&AsymmetricAlgorithm_get_KeySize_m7518/* method */
	, &AsymmetricAlgorithm_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4314/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo AsymmetricAlgorithm_t1310_AsymmetricAlgorithm_set_KeySize_m7517_ParameterInfos[] = 
{
	{"value", 0, 134222990, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::set_KeySize(System.Int32)
extern const MethodInfo AsymmetricAlgorithm_set_KeySize_m7517_MethodInfo = 
{
	"set_KeySize"/* name */
	, (methodPointerType)&AsymmetricAlgorithm_set_KeySize_m7517/* method */
	, &AsymmetricAlgorithm_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, AsymmetricAlgorithm_t1310_AsymmetricAlgorithm_set_KeySize_m7517_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4315/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::Clear()
extern const MethodInfo AsymmetricAlgorithm_Clear_m7572_MethodInfo = 
{
	"Clear"/* name */
	, (methodPointerType)&AsymmetricAlgorithm_Clear_m7572/* method */
	, &AsymmetricAlgorithm_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4316/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo AsymmetricAlgorithm_t1310_AsymmetricAlgorithm_Dispose_m13244_ParameterInfos[] = 
{
	{"disposing", 0, 134222991, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::Dispose(System.Boolean)
extern const MethodInfo AsymmetricAlgorithm_Dispose_m13244_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &AsymmetricAlgorithm_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, AsymmetricAlgorithm_t1310_AsymmetricAlgorithm_Dispose_m13244_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4317/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AsymmetricAlgorithm_t1310_AsymmetricAlgorithm_FromXmlString_m13245_ParameterInfos[] = 
{
	{"xmlString", 0, 134222992, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::FromXmlString(System.String)
extern const MethodInfo AsymmetricAlgorithm_FromXmlString_m13245_MethodInfo = 
{
	"FromXmlString"/* name */
	, NULL/* method */
	, &AsymmetricAlgorithm_t1310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AsymmetricAlgorithm_t1310_AsymmetricAlgorithm_FromXmlString_m13245_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4318/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo AsymmetricAlgorithm_t1310_AsymmetricAlgorithm_ToXmlString_m13246_ParameterInfos[] = 
{
	{"includePrivateParameters", 0, 134222993, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.AsymmetricAlgorithm::ToXmlString(System.Boolean)
extern const MethodInfo AsymmetricAlgorithm_ToXmlString_m13246_MethodInfo = 
{
	"ToXmlString"/* name */
	, NULL/* method */
	, &AsymmetricAlgorithm_t1310_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t274/* invoker_method */
	, AsymmetricAlgorithm_t1310_AsymmetricAlgorithm_ToXmlString_m13246_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4319/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AsymmetricAlgorithm_t1310_AsymmetricAlgorithm_GetNamedParam_m11005_ParameterInfos[] = 
{
	{"xml", 0, 134222994, 0, &String_t_0_0_0},
	{"param", 1, 134222995, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.AsymmetricAlgorithm::GetNamedParam(System.String,System.String)
extern const MethodInfo AsymmetricAlgorithm_GetNamedParam_m11005_MethodInfo = 
{
	"GetNamedParam"/* name */
	, (methodPointerType)&AsymmetricAlgorithm_GetNamedParam_m11005/* method */
	, &AsymmetricAlgorithm_t1310_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, AsymmetricAlgorithm_t1310_AsymmetricAlgorithm_GetNamedParam_m11005_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4320/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AsymmetricAlgorithm_t1310_MethodInfos[] =
{
	&AsymmetricAlgorithm__ctor_m11004_MethodInfo,
	&AsymmetricAlgorithm_System_IDisposable_Dispose_m7612_MethodInfo,
	&AsymmetricAlgorithm_get_KeySize_m7518_MethodInfo,
	&AsymmetricAlgorithm_set_KeySize_m7517_MethodInfo,
	&AsymmetricAlgorithm_Clear_m7572_MethodInfo,
	&AsymmetricAlgorithm_Dispose_m13244_MethodInfo,
	&AsymmetricAlgorithm_FromXmlString_m13245_MethodInfo,
	&AsymmetricAlgorithm_ToXmlString_m13246_MethodInfo,
	&AsymmetricAlgorithm_GetNamedParam_m11005_MethodInfo,
	NULL
};
extern const MethodInfo AsymmetricAlgorithm_get_KeySize_m7518_MethodInfo;
extern const MethodInfo AsymmetricAlgorithm_set_KeySize_m7517_MethodInfo;
static const PropertyInfo AsymmetricAlgorithm_t1310____KeySize_PropertyInfo = 
{
	&AsymmetricAlgorithm_t1310_il2cpp_TypeInfo/* parent */
	, "KeySize"/* name */
	, &AsymmetricAlgorithm_get_KeySize_m7518_MethodInfo/* get */
	, &AsymmetricAlgorithm_set_KeySize_m7517_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AsymmetricAlgorithm_t1310_PropertyInfos[] =
{
	&AsymmetricAlgorithm_t1310____KeySize_PropertyInfo,
	NULL
};
extern const MethodInfo AsymmetricAlgorithm_System_IDisposable_Dispose_m7612_MethodInfo;
static const Il2CppMethodReference AsymmetricAlgorithm_t1310_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&AsymmetricAlgorithm_System_IDisposable_Dispose_m7612_MethodInfo,
	&AsymmetricAlgorithm_get_KeySize_m7518_MethodInfo,
	&AsymmetricAlgorithm_set_KeySize_m7517_MethodInfo,
	NULL,
	NULL,
	NULL,
};
static bool AsymmetricAlgorithm_t1310_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t233_0_0_0;
static const Il2CppType* AsymmetricAlgorithm_t1310_InterfacesTypeInfos[] = 
{
	&IDisposable_t233_0_0_0,
};
static Il2CppInterfaceOffsetPair AsymmetricAlgorithm_t1310_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricAlgorithm_t1310_0_0_0;
extern const Il2CppType AsymmetricAlgorithm_t1310_1_0_0;
struct AsymmetricAlgorithm_t1310;
const Il2CppTypeDefinitionMetadata AsymmetricAlgorithm_t1310_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AsymmetricAlgorithm_t1310_InterfacesTypeInfos/* implementedInterfaces */
	, AsymmetricAlgorithm_t1310_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricAlgorithm_t1310_VTable/* vtableMethods */
	, AsymmetricAlgorithm_t1310_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2302/* fieldStart */

};
TypeInfo AsymmetricAlgorithm_t1310_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricAlgorithm"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, AsymmetricAlgorithm_t1310_MethodInfos/* methods */
	, AsymmetricAlgorithm_t1310_PropertyInfos/* properties */
	, NULL/* events */
	, &AsymmetricAlgorithm_t1310_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 646/* custom_attributes_cache */
	, &AsymmetricAlgorithm_t1310_0_0_0/* byval_arg */
	, &AsymmetricAlgorithm_t1310_1_0_0/* this_arg */
	, &AsymmetricAlgorithm_t1310_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricAlgorithm_t1310)/* instance_size */
	, sizeof (AsymmetricAlgorithm_t1310)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeF.h"
// Metadata Definition System.Security.Cryptography.AsymmetricKeyExchangeFormatter
extern TypeInfo AsymmetricKeyExchangeFormatter_t2068_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeFMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricKeyExchangeFormatter::.ctor()
extern const MethodInfo AsymmetricKeyExchangeFormatter__ctor_m11006_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AsymmetricKeyExchangeFormatter__ctor_m11006/* method */
	, &AsymmetricKeyExchangeFormatter_t2068_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4321/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo AsymmetricKeyExchangeFormatter_t2068_AsymmetricKeyExchangeFormatter_CreateKeyExchange_m13247_ParameterInfos[] = 
{
	{"data", 0, 134222996, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.AsymmetricKeyExchangeFormatter::CreateKeyExchange(System.Byte[])
extern const MethodInfo AsymmetricKeyExchangeFormatter_CreateKeyExchange_m13247_MethodInfo = 
{
	"CreateKeyExchange"/* name */
	, NULL/* method */
	, &AsymmetricKeyExchangeFormatter_t2068_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AsymmetricKeyExchangeFormatter_t2068_AsymmetricKeyExchangeFormatter_CreateKeyExchange_m13247_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4322/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AsymmetricKeyExchangeFormatter_t2068_MethodInfos[] =
{
	&AsymmetricKeyExchangeFormatter__ctor_m11006_MethodInfo,
	&AsymmetricKeyExchangeFormatter_CreateKeyExchange_m13247_MethodInfo,
	NULL
};
static const Il2CppMethodReference AsymmetricKeyExchangeFormatter_t2068_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
};
static bool AsymmetricKeyExchangeFormatter_t2068_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricKeyExchangeFormatter_t2068_0_0_0;
extern const Il2CppType AsymmetricKeyExchangeFormatter_t2068_1_0_0;
struct AsymmetricKeyExchangeFormatter_t2068;
const Il2CppTypeDefinitionMetadata AsymmetricKeyExchangeFormatter_t2068_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricKeyExchangeFormatter_t2068_VTable/* vtableMethods */
	, AsymmetricKeyExchangeFormatter_t2068_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AsymmetricKeyExchangeFormatter_t2068_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricKeyExchangeFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, AsymmetricKeyExchangeFormatter_t2068_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AsymmetricKeyExchangeFormatter_t2068_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 647/* custom_attributes_cache */
	, &AsymmetricKeyExchangeFormatter_t2068_0_0_0/* byval_arg */
	, &AsymmetricKeyExchangeFormatter_t2068_1_0_0/* this_arg */
	, &AsymmetricKeyExchangeFormatter_t2068_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricKeyExchangeFormatter_t2068)/* instance_size */
	, sizeof (AsymmetricKeyExchangeFormatter_t2068)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricSignatureDeformatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDef.h"
// Metadata Definition System.Security.Cryptography.AsymmetricSignatureDeformatter
extern TypeInfo AsymmetricSignatureDeformatter_t1596_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricSignatureDeformatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDefMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricSignatureDeformatter::.ctor()
extern const MethodInfo AsymmetricSignatureDeformatter__ctor_m7562_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AsymmetricSignatureDeformatter__ctor_m7562/* method */
	, &AsymmetricSignatureDeformatter_t1596_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4323/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AsymmetricSignatureDeformatter_t1596_AsymmetricSignatureDeformatter_SetHashAlgorithm_m13248_ParameterInfos[] = 
{
	{"strName", 0, 134222997, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricSignatureDeformatter::SetHashAlgorithm(System.String)
extern const MethodInfo AsymmetricSignatureDeformatter_SetHashAlgorithm_m13248_MethodInfo = 
{
	"SetHashAlgorithm"/* name */
	, NULL/* method */
	, &AsymmetricSignatureDeformatter_t1596_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AsymmetricSignatureDeformatter_t1596_AsymmetricSignatureDeformatter_SetHashAlgorithm_m13248_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4324/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsymmetricAlgorithm_t1310_0_0_0;
static const ParameterInfo AsymmetricSignatureDeformatter_t1596_AsymmetricSignatureDeformatter_SetKey_m13249_ParameterInfos[] = 
{
	{"key", 0, 134222998, 0, &AsymmetricAlgorithm_t1310_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricSignatureDeformatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern const MethodInfo AsymmetricSignatureDeformatter_SetKey_m13249_MethodInfo = 
{
	"SetKey"/* name */
	, NULL/* method */
	, &AsymmetricSignatureDeformatter_t1596_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AsymmetricSignatureDeformatter_t1596_AsymmetricSignatureDeformatter_SetKey_m13249_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4325/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo AsymmetricSignatureDeformatter_t1596_AsymmetricSignatureDeformatter_VerifySignature_m13250_ParameterInfos[] = 
{
	{"rgbHash", 0, 134222999, 0, &ByteU5BU5D_t850_0_0_0},
	{"rgbSignature", 1, 134223000, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Security.Cryptography.AsymmetricSignatureDeformatter::VerifySignature(System.Byte[],System.Byte[])
extern const MethodInfo AsymmetricSignatureDeformatter_VerifySignature_m13250_MethodInfo = 
{
	"VerifySignature"/* name */
	, NULL/* method */
	, &AsymmetricSignatureDeformatter_t1596_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, AsymmetricSignatureDeformatter_t1596_AsymmetricSignatureDeformatter_VerifySignature_m13250_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4326/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AsymmetricSignatureDeformatter_t1596_MethodInfos[] =
{
	&AsymmetricSignatureDeformatter__ctor_m7562_MethodInfo,
	&AsymmetricSignatureDeformatter_SetHashAlgorithm_m13248_MethodInfo,
	&AsymmetricSignatureDeformatter_SetKey_m13249_MethodInfo,
	&AsymmetricSignatureDeformatter_VerifySignature_m13250_MethodInfo,
	NULL
};
static const Il2CppMethodReference AsymmetricSignatureDeformatter_t1596_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
	NULL,
	NULL,
};
static bool AsymmetricSignatureDeformatter_t1596_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricSignatureDeformatter_t1596_0_0_0;
extern const Il2CppType AsymmetricSignatureDeformatter_t1596_1_0_0;
struct AsymmetricSignatureDeformatter_t1596;
const Il2CppTypeDefinitionMetadata AsymmetricSignatureDeformatter_t1596_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricSignatureDeformatter_t1596_VTable/* vtableMethods */
	, AsymmetricSignatureDeformatter_t1596_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AsymmetricSignatureDeformatter_t1596_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricSignatureDeformatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, AsymmetricSignatureDeformatter_t1596_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AsymmetricSignatureDeformatter_t1596_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 648/* custom_attributes_cache */
	, &AsymmetricSignatureDeformatter_t1596_0_0_0/* byval_arg */
	, &AsymmetricSignatureDeformatter_t1596_1_0_0/* this_arg */
	, &AsymmetricSignatureDeformatter_t1596_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricSignatureDeformatter_t1596)/* instance_size */
	, sizeof (AsymmetricSignatureDeformatter_t1596)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
// Metadata Definition System.Security.Cryptography.AsymmetricSignatureFormatter
extern TypeInfo AsymmetricSignatureFormatter_t1598_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureForMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricSignatureFormatter::.ctor()
extern const MethodInfo AsymmetricSignatureFormatter__ctor_m7563_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AsymmetricSignatureFormatter__ctor_m7563/* method */
	, &AsymmetricSignatureFormatter_t1598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4327/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AsymmetricSignatureFormatter_t1598_AsymmetricSignatureFormatter_SetHashAlgorithm_m13251_ParameterInfos[] = 
{
	{"strName", 0, 134223001, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricSignatureFormatter::SetHashAlgorithm(System.String)
extern const MethodInfo AsymmetricSignatureFormatter_SetHashAlgorithm_m13251_MethodInfo = 
{
	"SetHashAlgorithm"/* name */
	, NULL/* method */
	, &AsymmetricSignatureFormatter_t1598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AsymmetricSignatureFormatter_t1598_AsymmetricSignatureFormatter_SetHashAlgorithm_m13251_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4328/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsymmetricAlgorithm_t1310_0_0_0;
static const ParameterInfo AsymmetricSignatureFormatter_t1598_AsymmetricSignatureFormatter_SetKey_m13252_ParameterInfos[] = 
{
	{"key", 0, 134223002, 0, &AsymmetricAlgorithm_t1310_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.AsymmetricSignatureFormatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern const MethodInfo AsymmetricSignatureFormatter_SetKey_m13252_MethodInfo = 
{
	"SetKey"/* name */
	, NULL/* method */
	, &AsymmetricSignatureFormatter_t1598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AsymmetricSignatureFormatter_t1598_AsymmetricSignatureFormatter_SetKey_m13252_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4329/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo AsymmetricSignatureFormatter_t1598_AsymmetricSignatureFormatter_CreateSignature_m13253_ParameterInfos[] = 
{
	{"rgbHash", 0, 134223003, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.AsymmetricSignatureFormatter::CreateSignature(System.Byte[])
extern const MethodInfo AsymmetricSignatureFormatter_CreateSignature_m13253_MethodInfo = 
{
	"CreateSignature"/* name */
	, NULL/* method */
	, &AsymmetricSignatureFormatter_t1598_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AsymmetricSignatureFormatter_t1598_AsymmetricSignatureFormatter_CreateSignature_m13253_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4330/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AsymmetricSignatureFormatter_t1598_MethodInfos[] =
{
	&AsymmetricSignatureFormatter__ctor_m7563_MethodInfo,
	&AsymmetricSignatureFormatter_SetHashAlgorithm_m13251_MethodInfo,
	&AsymmetricSignatureFormatter_SetKey_m13252_MethodInfo,
	&AsymmetricSignatureFormatter_CreateSignature_m13253_MethodInfo,
	NULL
};
static const Il2CppMethodReference AsymmetricSignatureFormatter_t1598_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
	NULL,
	NULL,
};
static bool AsymmetricSignatureFormatter_t1598_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricSignatureFormatter_t1598_0_0_0;
extern const Il2CppType AsymmetricSignatureFormatter_t1598_1_0_0;
struct AsymmetricSignatureFormatter_t1598;
const Il2CppTypeDefinitionMetadata AsymmetricSignatureFormatter_t1598_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricSignatureFormatter_t1598_VTable/* vtableMethods */
	, AsymmetricSignatureFormatter_t1598_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AsymmetricSignatureFormatter_t1598_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricSignatureFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, AsymmetricSignatureFormatter_t1598_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AsymmetricSignatureFormatter_t1598_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 649/* custom_attributes_cache */
	, &AsymmetricSignatureFormatter_t1598_0_0_0/* byval_arg */
	, &AsymmetricSignatureFormatter_t1598_1_0_0/* this_arg */
	, &AsymmetricSignatureFormatter_t1598_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricSignatureFormatter_t1598)/* instance_size */
	, sizeof (AsymmetricSignatureFormatter_t1598)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.Base64Constants
#include "mscorlib_System_Security_Cryptography_Base64Constants.h"
// Metadata Definition System.Security.Cryptography.Base64Constants
extern TypeInfo Base64Constants_t2069_il2cpp_TypeInfo;
// System.Security.Cryptography.Base64Constants
#include "mscorlib_System_Security_Cryptography_Base64ConstantsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.Base64Constants::.cctor()
extern const MethodInfo Base64Constants__cctor_m11007_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Base64Constants__cctor_m11007/* method */
	, &Base64Constants_t2069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4331/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Base64Constants_t2069_MethodInfos[] =
{
	&Base64Constants__cctor_m11007_MethodInfo,
	NULL
};
static const Il2CppMethodReference Base64Constants_t2069_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Base64Constants_t2069_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Base64Constants_t2069_0_0_0;
extern const Il2CppType Base64Constants_t2069_1_0_0;
struct Base64Constants_t2069;
const Il2CppTypeDefinitionMetadata Base64Constants_t2069_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Base64Constants_t2069_VTable/* vtableMethods */
	, Base64Constants_t2069_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2304/* fieldStart */

};
TypeInfo Base64Constants_t2069_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Base64Constants"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, Base64Constants_t2069_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Base64Constants_t2069_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Base64Constants_t2069_0_0_0/* byval_arg */
	, &Base64Constants_t2069_1_0_0/* this_arg */
	, &Base64Constants_t2069_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Base64Constants_t2069)/* instance_size */
	, sizeof (Base64Constants_t2069)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Base64Constants_t2069_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
// Metadata Definition System.Security.Cryptography.CipherMode
extern TypeInfo CipherMode_t1659_il2cpp_TypeInfo;
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherModeMethodDeclarations.h"
static const MethodInfo* CipherMode_t1659_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CipherMode_t1659_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool CipherMode_t1659_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CipherMode_t1659_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CipherMode_t1659_0_0_0;
extern const Il2CppType CipherMode_t1659_1_0_0;
const Il2CppTypeDefinitionMetadata CipherMode_t1659_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CipherMode_t1659_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, CipherMode_t1659_VTable/* vtableMethods */
	, CipherMode_t1659_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2306/* fieldStart */

};
TypeInfo CipherMode_t1659_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CipherMode"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, CipherMode_t1659_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 650/* custom_attributes_cache */
	, &CipherMode_t1659_0_0_0/* byval_arg */
	, &CipherMode_t1659_1_0_0/* this_arg */
	, &CipherMode_t1659_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CipherMode_t1659)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CipherMode_t1659)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.CryptoConfig
#include "mscorlib_System_Security_Cryptography_CryptoConfig.h"
// Metadata Definition System.Security.Cryptography.CryptoConfig
extern TypeInfo CryptoConfig_t1458_il2cpp_TypeInfo;
// System.Security.Cryptography.CryptoConfig
#include "mscorlib_System_Security_Cryptography_CryptoConfigMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CryptoConfig::.cctor()
extern const MethodInfo CryptoConfig__cctor_m11008_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CryptoConfig__cctor_m11008/* method */
	, &CryptoConfig_t1458_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4332/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CryptoConfig::Initialize()
extern const MethodInfo CryptoConfig_Initialize_m11009_MethodInfo = 
{
	"Initialize"/* name */
	, (methodPointerType)&CryptoConfig_Initialize_m11009/* method */
	, &CryptoConfig_t1458_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4333/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CryptoConfig_t1458_CryptoConfig_CreateFromName_m6470_ParameterInfos[] = 
{
	{"name", 0, 134223004, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Security.Cryptography.CryptoConfig::CreateFromName(System.String)
extern const MethodInfo CryptoConfig_CreateFromName_m6470_MethodInfo = 
{
	"CreateFromName"/* name */
	, (methodPointerType)&CryptoConfig_CreateFromName_m6470/* method */
	, &CryptoConfig_t1458_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CryptoConfig_t1458_CryptoConfig_CreateFromName_m6470_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4334/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t224_0_0_0;
static const ParameterInfo CryptoConfig_t1458_CryptoConfig_CreateFromName_m6495_ParameterInfos[] = 
{
	{"name", 0, 134223005, 0, &String_t_0_0_0},
	{"args", 1, 134223006, 652, &ObjectU5BU5D_t224_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Security.Cryptography.CryptoConfig::CreateFromName(System.String,System.Object[])
extern const MethodInfo CryptoConfig_CreateFromName_m6495_MethodInfo = 
{
	"CreateFromName"/* name */
	, (methodPointerType)&CryptoConfig_CreateFromName_m6495/* method */
	, &CryptoConfig_t1458_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, CryptoConfig_t1458_CryptoConfig_CreateFromName_m6495_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4335/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CryptoConfig_t1458_CryptoConfig_MapNameToOID_m7509_ParameterInfos[] = 
{
	{"name", 0, 134223007, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.CryptoConfig::MapNameToOID(System.String)
extern const MethodInfo CryptoConfig_MapNameToOID_m7509_MethodInfo = 
{
	"MapNameToOID"/* name */
	, (methodPointerType)&CryptoConfig_MapNameToOID_m7509/* method */
	, &CryptoConfig_t1458_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CryptoConfig_t1458_CryptoConfig_MapNameToOID_m7509_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4336/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CryptoConfig_t1458_CryptoConfig_EncodeOID_m6472_ParameterInfos[] = 
{
	{"str", 0, 134223008, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.CryptoConfig::EncodeOID(System.String)
extern const MethodInfo CryptoConfig_EncodeOID_m6472_MethodInfo = 
{
	"EncodeOID"/* name */
	, (methodPointerType)&CryptoConfig_EncodeOID_m6472/* method */
	, &CryptoConfig_t1458_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CryptoConfig_t1458_CryptoConfig_EncodeOID_m6472_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4337/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo CryptoConfig_t1458_CryptoConfig_EncodeLongNumber_m11010_ParameterInfos[] = 
{
	{"x", 0, 134223009, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.CryptoConfig::EncodeLongNumber(System.Int64)
extern const MethodInfo CryptoConfig_EncodeLongNumber_m11010_MethodInfo = 
{
	"EncodeLongNumber"/* name */
	, (methodPointerType)&CryptoConfig_EncodeLongNumber_m11010/* method */
	, &CryptoConfig_t1458_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t1071/* invoker_method */
	, CryptoConfig_t1458_CryptoConfig_EncodeLongNumber_m11010_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4338/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CryptoConfig_t1458_MethodInfos[] =
{
	&CryptoConfig__cctor_m11008_MethodInfo,
	&CryptoConfig_Initialize_m11009_MethodInfo,
	&CryptoConfig_CreateFromName_m6470_MethodInfo,
	&CryptoConfig_CreateFromName_m6495_MethodInfo,
	&CryptoConfig_MapNameToOID_m7509_MethodInfo,
	&CryptoConfig_EncodeOID_m6472_MethodInfo,
	&CryptoConfig_EncodeLongNumber_m11010_MethodInfo,
	NULL
};
static const Il2CppMethodReference CryptoConfig_t1458_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CryptoConfig_t1458_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CryptoConfig_t1458_0_0_0;
extern const Il2CppType CryptoConfig_t1458_1_0_0;
struct CryptoConfig_t1458;
const Il2CppTypeDefinitionMetadata CryptoConfig_t1458_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CryptoConfig_t1458_VTable/* vtableMethods */
	, CryptoConfig_t1458_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2312/* fieldStart */

};
TypeInfo CryptoConfig_t1458_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptoConfig"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, CryptoConfig_t1458_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CryptoConfig_t1458_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 651/* custom_attributes_cache */
	, &CryptoConfig_t1458_0_0_0/* byval_arg */
	, &CryptoConfig_t1458_1_0_0/* this_arg */
	, &CryptoConfig_t1458_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptoConfig_t1458)/* instance_size */
	, sizeof (CryptoConfig_t1458)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CryptoConfig_t1458_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.CryptographicException
#include "mscorlib_System_Security_Cryptography_CryptographicException.h"
// Metadata Definition System.Security.Cryptography.CryptographicException
extern TypeInfo CryptographicException_t1454_il2cpp_TypeInfo;
// System.Security.Cryptography.CryptographicException
#include "mscorlib_System_Security_Cryptography_CryptographicExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CryptographicException::.ctor()
extern const MethodInfo CryptographicException__ctor_m11011_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CryptographicException__ctor_m11011/* method */
	, &CryptographicException_t1454_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4339/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CryptographicException_t1454_CryptographicException__ctor_m6428_ParameterInfos[] = 
{
	{"message", 0, 134223010, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.String)
extern const MethodInfo CryptographicException__ctor_m6428_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CryptographicException__ctor_m6428/* method */
	, &CryptographicException_t1454_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CryptographicException_t1454_CryptographicException__ctor_m6428_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t232_0_0_0;
extern const Il2CppType Exception_t232_0_0_0;
static const ParameterInfo CryptographicException_t1454_CryptographicException__ctor_m6432_ParameterInfos[] = 
{
	{"message", 0, 134223011, 0, &String_t_0_0_0},
	{"inner", 1, 134223012, 0, &Exception_t232_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.String,System.Exception)
extern const MethodInfo CryptographicException__ctor_m6432_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CryptographicException__ctor_m6432/* method */
	, &CryptographicException_t1454_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, CryptographicException_t1454_CryptographicException__ctor_m6432_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CryptographicException_t1454_CryptographicException__ctor_m11012_ParameterInfos[] = 
{
	{"format", 0, 134223013, 0, &String_t_0_0_0},
	{"insert", 1, 134223014, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.String,System.String)
extern const MethodInfo CryptographicException__ctor_m11012_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CryptographicException__ctor_m11012/* method */
	, &CryptographicException_t1454_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, CryptographicException_t1454_CryptographicException__ctor_m11012_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo CryptographicException_t1454_CryptographicException__ctor_m11013_ParameterInfos[] = 
{
	{"info", 0, 134223015, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134223016, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo CryptographicException__ctor_m11013_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CryptographicException__ctor_m11013/* method */
	, &CryptographicException_t1454_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, CryptographicException_t1454_CryptographicException__ctor_m11013_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CryptographicException_t1454_MethodInfos[] =
{
	&CryptographicException__ctor_m11011_MethodInfo,
	&CryptographicException__ctor_m6428_MethodInfo,
	&CryptographicException__ctor_m6432_MethodInfo,
	&CryptographicException__ctor_m11012_MethodInfo,
	&CryptographicException__ctor_m11013_MethodInfo,
	NULL
};
static const Il2CppMethodReference CryptographicException_t1454_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool CryptographicException_t1454_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* CryptographicException_t1454_InterfacesTypeInfos[] = 
{
	&_Exception_t1147_0_0_0,
};
static Il2CppInterfaceOffsetPair CryptographicException_t1454_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CryptographicException_t1454_0_0_0;
extern const Il2CppType CryptographicException_t1454_1_0_0;
struct CryptographicException_t1454;
const Il2CppTypeDefinitionMetadata CryptographicException_t1454_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CryptographicException_t1454_InterfacesTypeInfos/* implementedInterfaces */
	, CryptographicException_t1454_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1464_0_0_0/* parent */
	, CryptographicException_t1454_VTable/* vtableMethods */
	, CryptographicException_t1454_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CryptographicException_t1454_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptographicException"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, CryptographicException_t1454_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CryptographicException_t1454_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 653/* custom_attributes_cache */
	, &CryptographicException_t1454_0_0_0/* byval_arg */
	, &CryptographicException_t1454_1_0_0/* this_arg */
	, &CryptographicException_t1454_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptographicException_t1454)/* instance_size */
	, sizeof (CryptographicException_t1454)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.CryptographicUnexpectedOperationException
#include "mscorlib_System_Security_Cryptography_CryptographicUnexpecte.h"
// Metadata Definition System.Security.Cryptography.CryptographicUnexpectedOperationException
extern TypeInfo CryptographicUnexpectedOperationException_t1459_il2cpp_TypeInfo;
// System.Security.Cryptography.CryptographicUnexpectedOperationException
#include "mscorlib_System_Security_Cryptography_CryptographicUnexpecteMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CryptographicUnexpectedOperationException::.ctor()
extern const MethodInfo CryptographicUnexpectedOperationException__ctor_m11014_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CryptographicUnexpectedOperationException__ctor_m11014/* method */
	, &CryptographicUnexpectedOperationException_t1459_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CryptographicUnexpectedOperationException_t1459_CryptographicUnexpectedOperationException__ctor_m7546_ParameterInfos[] = 
{
	{"message", 0, 134223017, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CryptographicUnexpectedOperationException::.ctor(System.String)
extern const MethodInfo CryptographicUnexpectedOperationException__ctor_m7546_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CryptographicUnexpectedOperationException__ctor_m7546/* method */
	, &CryptographicUnexpectedOperationException_t1459_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CryptographicUnexpectedOperationException_t1459_CryptographicUnexpectedOperationException__ctor_m7546_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo CryptographicUnexpectedOperationException_t1459_CryptographicUnexpectedOperationException__ctor_m11015_ParameterInfos[] = 
{
	{"info", 0, 134223018, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134223019, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CryptographicUnexpectedOperationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo CryptographicUnexpectedOperationException__ctor_m11015_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CryptographicUnexpectedOperationException__ctor_m11015/* method */
	, &CryptographicUnexpectedOperationException_t1459_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, CryptographicUnexpectedOperationException_t1459_CryptographicUnexpectedOperationException__ctor_m11015_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CryptographicUnexpectedOperationException_t1459_MethodInfos[] =
{
	&CryptographicUnexpectedOperationException__ctor_m11014_MethodInfo,
	&CryptographicUnexpectedOperationException__ctor_m7546_MethodInfo,
	&CryptographicUnexpectedOperationException__ctor_m11015_MethodInfo,
	NULL
};
static const Il2CppMethodReference CryptographicUnexpectedOperationException_t1459_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool CryptographicUnexpectedOperationException_t1459_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CryptographicUnexpectedOperationException_t1459_InterfacesOffsets[] = 
{
	{ &_Exception_t1147_0_0_0, 5},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CryptographicUnexpectedOperationException_t1459_0_0_0;
extern const Il2CppType CryptographicUnexpectedOperationException_t1459_1_0_0;
struct CryptographicUnexpectedOperationException_t1459;
const Il2CppTypeDefinitionMetadata CryptographicUnexpectedOperationException_t1459_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CryptographicUnexpectedOperationException_t1459_InterfacesOffsets/* interfaceOffsets */
	, &CryptographicException_t1454_0_0_0/* parent */
	, CryptographicUnexpectedOperationException_t1459_VTable/* vtableMethods */
	, CryptographicUnexpectedOperationException_t1459_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CryptographicUnexpectedOperationException_t1459_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptographicUnexpectedOperationException"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, CryptographicUnexpectedOperationException_t1459_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CryptographicUnexpectedOperationException_t1459_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 654/* custom_attributes_cache */
	, &CryptographicUnexpectedOperationException_t1459_0_0_0/* byval_arg */
	, &CryptographicUnexpectedOperationException_t1459_1_0_0/* this_arg */
	, &CryptographicUnexpectedOperationException_t1459_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptographicUnexpectedOperationException_t1459)/* instance_size */
	, sizeof (CryptographicUnexpectedOperationException_t1459)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.CspParameters
#include "mscorlib_System_Security_Cryptography_CspParameters.h"
// Metadata Definition System.Security.Cryptography.CspParameters
extern TypeInfo CspParameters_t1639_il2cpp_TypeInfo;
// System.Security.Cryptography.CspParameters
#include "mscorlib_System_Security_Cryptography_CspParametersMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CspParameters::.ctor()
extern const MethodInfo CspParameters__ctor_m7511_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CspParameters__ctor_m7511/* method */
	, &CspParameters_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo CspParameters_t1639_CspParameters__ctor_m11016_ParameterInfos[] = 
{
	{"dwTypeIn", 0, 134223020, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32)
extern const MethodInfo CspParameters__ctor_m11016_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CspParameters__ctor_m11016/* method */
	, &CspParameters_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, CspParameters_t1639_CspParameters__ctor_m11016_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CspParameters_t1639_CspParameters__ctor_m11017_ParameterInfos[] = 
{
	{"dwTypeIn", 0, 134223021, 0, &Int32_t253_0_0_0},
	{"strProviderNameIn", 1, 134223022, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32,System.String)
extern const MethodInfo CspParameters__ctor_m11017_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CspParameters__ctor_m11017/* method */
	, &CspParameters_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Object_t/* invoker_method */
	, CspParameters_t1639_CspParameters__ctor_m11017_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CspParameters_t1639_CspParameters__ctor_m11018_ParameterInfos[] = 
{
	{"dwTypeIn", 0, 134223023, 0, &Int32_t253_0_0_0},
	{"strProviderNameIn", 1, 134223024, 0, &String_t_0_0_0},
	{"strContainerNameIn", 2, 134223025, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32,System.String,System.String)
extern const MethodInfo CspParameters__ctor_m11018_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CspParameters__ctor_m11018/* method */
	, &CspParameters_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Object_t_Object_t/* invoker_method */
	, CspParameters_t1639_CspParameters__ctor_m11018_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CspProviderFlags_t2070_0_0_0;
extern void* RuntimeInvoker_CspProviderFlags_t2070 (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.CspProviderFlags System.Security.Cryptography.CspParameters::get_Flags()
extern const MethodInfo CspParameters_get_Flags_m11019_MethodInfo = 
{
	"get_Flags"/* name */
	, (methodPointerType)&CspParameters_get_Flags_m11019/* method */
	, &CspParameters_t1639_il2cpp_TypeInfo/* declaring_type */
	, &CspProviderFlags_t2070_0_0_0/* return_type */
	, RuntimeInvoker_CspProviderFlags_t2070/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CspProviderFlags_t2070_0_0_0;
static const ParameterInfo CspParameters_t1639_CspParameters_set_Flags_m7512_ParameterInfos[] = 
{
	{"value", 0, 134223026, 0, &CspProviderFlags_t2070_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.CspParameters::set_Flags(System.Security.Cryptography.CspProviderFlags)
extern const MethodInfo CspParameters_set_Flags_m7512_MethodInfo = 
{
	"set_Flags"/* name */
	, (methodPointerType)&CspParameters_set_Flags_m7512/* method */
	, &CspParameters_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, CspParameters_t1639_CspParameters_set_Flags_m7512_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CspParameters_t1639_MethodInfos[] =
{
	&CspParameters__ctor_m7511_MethodInfo,
	&CspParameters__ctor_m11016_MethodInfo,
	&CspParameters__ctor_m11017_MethodInfo,
	&CspParameters__ctor_m11018_MethodInfo,
	&CspParameters_get_Flags_m11019_MethodInfo,
	&CspParameters_set_Flags_m7512_MethodInfo,
	NULL
};
extern const MethodInfo CspParameters_get_Flags_m11019_MethodInfo;
extern const MethodInfo CspParameters_set_Flags_m7512_MethodInfo;
static const PropertyInfo CspParameters_t1639____Flags_PropertyInfo = 
{
	&CspParameters_t1639_il2cpp_TypeInfo/* parent */
	, "Flags"/* name */
	, &CspParameters_get_Flags_m11019_MethodInfo/* get */
	, &CspParameters_set_Flags_m7512_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CspParameters_t1639_PropertyInfos[] =
{
	&CspParameters_t1639____Flags_PropertyInfo,
	NULL
};
static const Il2CppMethodReference CspParameters_t1639_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CspParameters_t1639_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CspParameters_t1639_0_0_0;
extern const Il2CppType CspParameters_t1639_1_0_0;
struct CspParameters_t1639;
const Il2CppTypeDefinitionMetadata CspParameters_t1639_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CspParameters_t1639_VTable/* vtableMethods */
	, CspParameters_t1639_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2315/* fieldStart */

};
TypeInfo CspParameters_t1639_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CspParameters"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, CspParameters_t1639_MethodInfos/* methods */
	, CspParameters_t1639_PropertyInfos/* properties */
	, NULL/* events */
	, &CspParameters_t1639_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 655/* custom_attributes_cache */
	, &CspParameters_t1639_0_0_0/* byval_arg */
	, &CspParameters_t1639_1_0_0/* this_arg */
	, &CspParameters_t1639_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CspParameters_t1639)/* instance_size */
	, sizeof (CspParameters_t1639)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
// Metadata Definition System.Security.Cryptography.CspProviderFlags
extern TypeInfo CspProviderFlags_t2070_il2cpp_TypeInfo;
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlagsMethodDeclarations.h"
static const MethodInfo* CspProviderFlags_t2070_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CspProviderFlags_t2070_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool CspProviderFlags_t2070_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CspProviderFlags_t2070_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CspProviderFlags_t2070_1_0_0;
const Il2CppTypeDefinitionMetadata CspProviderFlags_t2070_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CspProviderFlags_t2070_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, CspProviderFlags_t2070_VTable/* vtableMethods */
	, CspProviderFlags_t2070_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2320/* fieldStart */

};
TypeInfo CspProviderFlags_t2070_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CspProviderFlags"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, CspProviderFlags_t2070_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 656/* custom_attributes_cache */
	, &CspProviderFlags_t2070_0_0_0/* byval_arg */
	, &CspProviderFlags_t2070_1_0_0/* this_arg */
	, &CspProviderFlags_t2070_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CspProviderFlags_t2070)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CspProviderFlags_t2070)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.DES
#include "mscorlib_System_Security_Cryptography_DES.h"
// Metadata Definition System.Security.Cryptography.DES
extern TypeInfo DES_t1649_il2cpp_TypeInfo;
// System.Security.Cryptography.DES
#include "mscorlib_System_Security_Cryptography_DESMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DES::.ctor()
extern const MethodInfo DES__ctor_m11020_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DES__ctor_m11020/* method */
	, &DES_t1649_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DES::.cctor()
extern const MethodInfo DES__cctor_m11021_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&DES__cctor_m11021/* method */
	, &DES_t1649_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DES_t1649_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.DES System.Security.Cryptography.DES::Create()
extern const MethodInfo DES_Create_m7547_MethodInfo = 
{
	"Create"/* name */
	, (methodPointerType)&DES_Create_m7547/* method */
	, &DES_t1649_il2cpp_TypeInfo/* declaring_type */
	, &DES_t1649_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DES_t1649_DES_Create_m11022_ParameterInfos[] = 
{
	{"algName", 0, 134223027, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.DES System.Security.Cryptography.DES::Create(System.String)
extern const MethodInfo DES_Create_m11022_MethodInfo = 
{
	"Create"/* name */
	, (methodPointerType)&DES_Create_m11022/* method */
	, &DES_t1649_il2cpp_TypeInfo/* declaring_type */
	, &DES_t1649_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DES_t1649_DES_Create_m11022_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DES_t1649_DES_IsWeakKey_m11023_ParameterInfos[] = 
{
	{"rgbKey", 0, 134223028, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Security.Cryptography.DES::IsWeakKey(System.Byte[])
extern const MethodInfo DES_IsWeakKey_m11023_MethodInfo = 
{
	"IsWeakKey"/* name */
	, (methodPointerType)&DES_IsWeakKey_m11023/* method */
	, &DES_t1649_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, DES_t1649_DES_IsWeakKey_m11023_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DES_t1649_DES_IsSemiWeakKey_m11024_ParameterInfos[] = 
{
	{"rgbKey", 0, 134223029, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Security.Cryptography.DES::IsSemiWeakKey(System.Byte[])
extern const MethodInfo DES_IsSemiWeakKey_m11024_MethodInfo = 
{
	"IsSemiWeakKey"/* name */
	, (methodPointerType)&DES_IsSemiWeakKey_m11024/* method */
	, &DES_t1649_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, DES_t1649_DES_IsSemiWeakKey_m11024_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.DES::get_Key()
extern const MethodInfo DES_get_Key_m11025_MethodInfo = 
{
	"get_Key"/* name */
	, (methodPointerType)&DES_get_Key_m11025/* method */
	, &DES_t1649_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DES_t1649_DES_set_Key_m11026_ParameterInfos[] = 
{
	{"value", 0, 134223030, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DES::set_Key(System.Byte[])
extern const MethodInfo DES_set_Key_m11026_MethodInfo = 
{
	"set_Key"/* name */
	, (methodPointerType)&DES_set_Key_m11026/* method */
	, &DES_t1649_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DES_t1649_DES_set_Key_m11026_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DES_t1649_MethodInfos[] =
{
	&DES__ctor_m11020_MethodInfo,
	&DES__cctor_m11021_MethodInfo,
	&DES_Create_m7547_MethodInfo,
	&DES_Create_m11022_MethodInfo,
	&DES_IsWeakKey_m11023_MethodInfo,
	&DES_IsSemiWeakKey_m11024_MethodInfo,
	&DES_get_Key_m11025_MethodInfo,
	&DES_set_Key_m11026_MethodInfo,
	NULL
};
extern const MethodInfo DES_get_Key_m11025_MethodInfo;
extern const MethodInfo DES_set_Key_m11026_MethodInfo;
static const PropertyInfo DES_t1649____Key_PropertyInfo = 
{
	&DES_t1649_il2cpp_TypeInfo/* parent */
	, "Key"/* name */
	, &DES_get_Key_m11025_MethodInfo/* get */
	, &DES_set_Key_m11026_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DES_t1649_PropertyInfos[] =
{
	&DES_t1649____Key_PropertyInfo,
	NULL
};
extern const MethodInfo SymmetricAlgorithm_Finalize_m7505_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_System_IDisposable_Dispose_m7589_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_Dispose_m7609_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_get_BlockSize_m7590_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_set_BlockSize_m7591_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_get_FeedbackSize_m7592_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_get_IV_m11288_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_set_IV_m11289_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_get_KeySize_m7593_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_set_KeySize_m7594_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_get_LegalKeySizes_m7595_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_get_Mode_m7596_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_set_Mode_m7597_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_get_Padding_m7598_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_set_Padding_m7599_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_CreateDecryptor_m7600_MethodInfo;
extern const MethodInfo SymmetricAlgorithm_CreateEncryptor_m7601_MethodInfo;
static const Il2CppMethodReference DES_t1649_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&SymmetricAlgorithm_Finalize_m7505_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&SymmetricAlgorithm_System_IDisposable_Dispose_m7589_MethodInfo,
	&SymmetricAlgorithm_Dispose_m7609_MethodInfo,
	&SymmetricAlgorithm_get_BlockSize_m7590_MethodInfo,
	&SymmetricAlgorithm_set_BlockSize_m7591_MethodInfo,
	&SymmetricAlgorithm_get_FeedbackSize_m7592_MethodInfo,
	&SymmetricAlgorithm_get_IV_m11288_MethodInfo,
	&SymmetricAlgorithm_set_IV_m11289_MethodInfo,
	&DES_get_Key_m11025_MethodInfo,
	&DES_set_Key_m11026_MethodInfo,
	&SymmetricAlgorithm_get_KeySize_m7593_MethodInfo,
	&SymmetricAlgorithm_set_KeySize_m7594_MethodInfo,
	&SymmetricAlgorithm_get_LegalKeySizes_m7595_MethodInfo,
	&SymmetricAlgorithm_get_Mode_m7596_MethodInfo,
	&SymmetricAlgorithm_set_Mode_m7597_MethodInfo,
	&SymmetricAlgorithm_get_Padding_m7598_MethodInfo,
	&SymmetricAlgorithm_set_Padding_m7599_MethodInfo,
	&SymmetricAlgorithm_CreateDecryptor_m7600_MethodInfo,
	NULL,
	&SymmetricAlgorithm_CreateEncryptor_m7601_MethodInfo,
	NULL,
	NULL,
	NULL,
};
static bool DES_t1649_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DES_t1649_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DES_t1649_1_0_0;
extern const Il2CppType SymmetricAlgorithm_t1546_0_0_0;
struct DES_t1649;
const Il2CppTypeDefinitionMetadata DES_t1649_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DES_t1649_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t1546_0_0_0/* parent */
	, DES_t1649_VTable/* vtableMethods */
	, DES_t1649_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2329/* fieldStart */

};
TypeInfo DES_t1649_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DES"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, DES_t1649_MethodInfos/* methods */
	, DES_t1649_PropertyInfos/* properties */
	, NULL/* events */
	, &DES_t1649_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 657/* custom_attributes_cache */
	, &DES_t1649_0_0_0/* byval_arg */
	, &DES_t1649_1_0_0/* this_arg */
	, &DES_t1649_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DES_t1649)/* instance_size */
	, sizeof (DES_t1649)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DES_t1649_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.DESTransform
#include "mscorlib_System_Security_Cryptography_DESTransform.h"
// Metadata Definition System.Security.Cryptography.DESTransform
extern TypeInfo DESTransform_t2072_il2cpp_TypeInfo;
// System.Security.Cryptography.DESTransform
#include "mscorlib_System_Security_Cryptography_DESTransformMethodDeclarations.h"
extern const Il2CppType SymmetricAlgorithm_t1546_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DESTransform_t2072_DESTransform__ctor_m11027_ParameterInfos[] = 
{
	{"symmAlgo", 0, 134223031, 0, &SymmetricAlgorithm_t1546_0_0_0},
	{"encryption", 1, 134223032, 0, &Boolean_t273_0_0_0},
	{"key", 2, 134223033, 0, &ByteU5BU5D_t850_0_0_0},
	{"iv", 3, 134223034, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DESTransform::.ctor(System.Security.Cryptography.SymmetricAlgorithm,System.Boolean,System.Byte[],System.Byte[])
extern const MethodInfo DESTransform__ctor_m11027_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DESTransform__ctor_m11027/* method */
	, &DESTransform_t2072_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274_Object_t_Object_t/* invoker_method */
	, DESTransform_t2072_DESTransform__ctor_m11027_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DESTransform::.cctor()
extern const MethodInfo DESTransform__cctor_m11028_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&DESTransform__cctor_m11028/* method */
	, &DESTransform_t2072_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1063_0_0_0;
extern const Il2CppType UInt32_t1063_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo DESTransform_t2072_DESTransform_CipherFunct_m11029_ParameterInfos[] = 
{
	{"r", 0, 134223035, 0, &UInt32_t1063_0_0_0},
	{"n", 1, 134223036, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1063_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.Security.Cryptography.DESTransform::CipherFunct(System.UInt32,System.Int32)
extern const MethodInfo DESTransform_CipherFunct_m11029_MethodInfo = 
{
	"CipherFunct"/* name */
	, (methodPointerType)&DESTransform_CipherFunct_m11029/* method */
	, &DESTransform_t2072_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1063_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1063_Int32_t253_Int32_t253/* invoker_method */
	, DESTransform_t2072_DESTransform_CipherFunct_m11029_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType UInt32U5BU5D_t1523_0_0_0;
extern const Il2CppType UInt32U5BU5D_t1523_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo DESTransform_t2072_DESTransform_Permutation_m11030_ParameterInfos[] = 
{
	{"input", 0, 134223037, 0, &ByteU5BU5D_t850_0_0_0},
	{"output", 1, 134223038, 0, &ByteU5BU5D_t850_0_0_0},
	{"permTab", 2, 134223039, 0, &UInt32U5BU5D_t1523_0_0_0},
	{"preSwap", 3, 134223040, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DESTransform::Permutation(System.Byte[],System.Byte[],System.UInt32[],System.Boolean)
extern const MethodInfo DESTransform_Permutation_m11030_MethodInfo = 
{
	"Permutation"/* name */
	, (methodPointerType)&DESTransform_Permutation_m11030/* method */
	, &DESTransform_t2072_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_SByte_t274/* invoker_method */
	, DESTransform_t2072_DESTransform_Permutation_m11030_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DESTransform_t2072_DESTransform_BSwap_m11031_ParameterInfos[] = 
{
	{"byteBuff", 0, 134223041, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DESTransform::BSwap(System.Byte[])
extern const MethodInfo DESTransform_BSwap_m11031_MethodInfo = 
{
	"BSwap"/* name */
	, (methodPointerType)&DESTransform_BSwap_m11031/* method */
	, &DESTransform_t2072_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DESTransform_t2072_DESTransform_BSwap_m11031_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DESTransform_t2072_DESTransform_SetKey_m11032_ParameterInfos[] = 
{
	{"key", 0, 134223042, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DESTransform::SetKey(System.Byte[])
extern const MethodInfo DESTransform_SetKey_m11032_MethodInfo = 
{
	"SetKey"/* name */
	, (methodPointerType)&DESTransform_SetKey_m11032/* method */
	, &DESTransform_t2072_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DESTransform_t2072_DESTransform_SetKey_m11032_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DESTransform_t2072_DESTransform_ProcessBlock_m11033_ParameterInfos[] = 
{
	{"input", 0, 134223043, 0, &ByteU5BU5D_t850_0_0_0},
	{"output", 1, 134223044, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DESTransform::ProcessBlock(System.Byte[],System.Byte[])
extern const MethodInfo DESTransform_ProcessBlock_m11033_MethodInfo = 
{
	"ProcessBlock"/* name */
	, (methodPointerType)&DESTransform_ProcessBlock_m11033/* method */
	, &DESTransform_t2072_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, DESTransform_t2072_DESTransform_ProcessBlock_m11033_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DESTransform_t2072_DESTransform_ECB_m11034_ParameterInfos[] = 
{
	{"input", 0, 134223045, 0, &ByteU5BU5D_t850_0_0_0},
	{"output", 1, 134223046, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DESTransform::ECB(System.Byte[],System.Byte[])
extern const MethodInfo DESTransform_ECB_m11034_MethodInfo = 
{
	"ECB"/* name */
	, (methodPointerType)&DESTransform_ECB_m11034/* method */
	, &DESTransform_t2072_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, DESTransform_t2072_DESTransform_ECB_m11034_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.DESTransform::GetStrongKey()
extern const MethodInfo DESTransform_GetStrongKey_m11035_MethodInfo = 
{
	"GetStrongKey"/* name */
	, (methodPointerType)&DESTransform_GetStrongKey_m11035/* method */
	, &DESTransform_t2072_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DESTransform_t2072_MethodInfos[] =
{
	&DESTransform__ctor_m11027_MethodInfo,
	&DESTransform__cctor_m11028_MethodInfo,
	&DESTransform_CipherFunct_m11029_MethodInfo,
	&DESTransform_Permutation_m11030_MethodInfo,
	&DESTransform_BSwap_m11031_MethodInfo,
	&DESTransform_SetKey_m11032_MethodInfo,
	&DESTransform_ProcessBlock_m11033_MethodInfo,
	&DESTransform_ECB_m11034_MethodInfo,
	&DESTransform_GetStrongKey_m11035_MethodInfo,
	NULL
};
extern const MethodInfo SymmetricTransform_Finalize_m8747_MethodInfo;
extern const MethodInfo SymmetricTransform_System_IDisposable_Dispose_m8746_MethodInfo;
extern const MethodInfo SymmetricTransform_get_CanReuseTransform_m8749_MethodInfo;
extern const MethodInfo SymmetricTransform_TransformBlock_m8756_MethodInfo;
extern const MethodInfo SymmetricTransform_TransformFinalBlock_m8763_MethodInfo;
extern const MethodInfo SymmetricTransform_Dispose_m8748_MethodInfo;
extern const MethodInfo SymmetricTransform_Transform_m8750_MethodInfo;
extern const MethodInfo DESTransform_ECB_m11034_MethodInfo;
extern const MethodInfo SymmetricTransform_CBC_m8751_MethodInfo;
extern const MethodInfo SymmetricTransform_CFB_m8752_MethodInfo;
extern const MethodInfo SymmetricTransform_OFB_m8753_MethodInfo;
extern const MethodInfo SymmetricTransform_CTS_m8754_MethodInfo;
static const Il2CppMethodReference DESTransform_t2072_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&SymmetricTransform_Finalize_m8747_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&SymmetricTransform_System_IDisposable_Dispose_m8746_MethodInfo,
	&SymmetricTransform_get_CanReuseTransform_m8749_MethodInfo,
	&SymmetricTransform_TransformBlock_m8756_MethodInfo,
	&SymmetricTransform_TransformFinalBlock_m8763_MethodInfo,
	&SymmetricTransform_Dispose_m8748_MethodInfo,
	&SymmetricTransform_get_CanReuseTransform_m8749_MethodInfo,
	&SymmetricTransform_Transform_m8750_MethodInfo,
	&DESTransform_ECB_m11034_MethodInfo,
	&SymmetricTransform_CBC_m8751_MethodInfo,
	&SymmetricTransform_CFB_m8752_MethodInfo,
	&SymmetricTransform_OFB_m8753_MethodInfo,
	&SymmetricTransform_CTS_m8754_MethodInfo,
	&SymmetricTransform_TransformBlock_m8756_MethodInfo,
	&SymmetricTransform_TransformFinalBlock_m8763_MethodInfo,
};
static bool DESTransform_t2072_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICryptoTransform_t1570_0_0_0;
static Il2CppInterfaceOffsetPair DESTransform_t2072_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
	{ &ICryptoTransform_t1570_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DESTransform_t2072_0_0_0;
extern const Il2CppType DESTransform_t2072_1_0_0;
extern const Il2CppType SymmetricTransform_t1732_0_0_0;
struct DESTransform_t2072;
const Il2CppTypeDefinitionMetadata DESTransform_t2072_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DESTransform_t2072_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricTransform_t1732_0_0_0/* parent */
	, DESTransform_t2072_VTable/* vtableMethods */
	, DESTransform_t2072_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2331/* fieldStart */

};
TypeInfo DESTransform_t2072_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DESTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, DESTransform_t2072_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DESTransform_t2072_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DESTransform_t2072_0_0_0/* byval_arg */
	, &DESTransform_t2072_1_0_0/* this_arg */
	, &DESTransform_t2072_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DESTransform_t2072)/* instance_size */
	, sizeof (DESTransform_t2072)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DESTransform_t2072_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.DESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DESCryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.DESCryptoServiceProvider
extern TypeInfo DESCryptoServiceProvider_t2073_il2cpp_TypeInfo;
// System.Security.Cryptography.DESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DESCryptoServiceProvidMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DESCryptoServiceProvider::.ctor()
extern const MethodInfo DESCryptoServiceProvider__ctor_m11036_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DESCryptoServiceProvider__ctor_m11036/* method */
	, &DESCryptoServiceProvider_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DESCryptoServiceProvider_t2073_DESCryptoServiceProvider_CreateDecryptor_m11037_ParameterInfos[] = 
{
	{"rgbKey", 0, 134223047, 0, &ByteU5BU5D_t850_0_0_0},
	{"rgbIV", 1, 134223048, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.DESCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern const MethodInfo DESCryptoServiceProvider_CreateDecryptor_m11037_MethodInfo = 
{
	"CreateDecryptor"/* name */
	, (methodPointerType)&DESCryptoServiceProvider_CreateDecryptor_m11037/* method */
	, &DESCryptoServiceProvider_t2073_il2cpp_TypeInfo/* declaring_type */
	, &ICryptoTransform_t1570_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, DESCryptoServiceProvider_t2073_DESCryptoServiceProvider_CreateDecryptor_m11037_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DESCryptoServiceProvider_t2073_DESCryptoServiceProvider_CreateEncryptor_m11038_ParameterInfos[] = 
{
	{"rgbKey", 0, 134223049, 0, &ByteU5BU5D_t850_0_0_0},
	{"rgbIV", 1, 134223050, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.DESCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern const MethodInfo DESCryptoServiceProvider_CreateEncryptor_m11038_MethodInfo = 
{
	"CreateEncryptor"/* name */
	, (methodPointerType)&DESCryptoServiceProvider_CreateEncryptor_m11038/* method */
	, &DESCryptoServiceProvider_t2073_il2cpp_TypeInfo/* declaring_type */
	, &ICryptoTransform_t1570_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, DESCryptoServiceProvider_t2073_DESCryptoServiceProvider_CreateEncryptor_m11038_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DESCryptoServiceProvider::GenerateIV()
extern const MethodInfo DESCryptoServiceProvider_GenerateIV_m11039_MethodInfo = 
{
	"GenerateIV"/* name */
	, (methodPointerType)&DESCryptoServiceProvider_GenerateIV_m11039/* method */
	, &DESCryptoServiceProvider_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DESCryptoServiceProvider::GenerateKey()
extern const MethodInfo DESCryptoServiceProvider_GenerateKey_m11040_MethodInfo = 
{
	"GenerateKey"/* name */
	, (methodPointerType)&DESCryptoServiceProvider_GenerateKey_m11040/* method */
	, &DESCryptoServiceProvider_t2073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DESCryptoServiceProvider_t2073_MethodInfos[] =
{
	&DESCryptoServiceProvider__ctor_m11036_MethodInfo,
	&DESCryptoServiceProvider_CreateDecryptor_m11037_MethodInfo,
	&DESCryptoServiceProvider_CreateEncryptor_m11038_MethodInfo,
	&DESCryptoServiceProvider_GenerateIV_m11039_MethodInfo,
	&DESCryptoServiceProvider_GenerateKey_m11040_MethodInfo,
	NULL
};
extern const MethodInfo DESCryptoServiceProvider_CreateDecryptor_m11037_MethodInfo;
extern const MethodInfo DESCryptoServiceProvider_CreateEncryptor_m11038_MethodInfo;
extern const MethodInfo DESCryptoServiceProvider_GenerateIV_m11039_MethodInfo;
extern const MethodInfo DESCryptoServiceProvider_GenerateKey_m11040_MethodInfo;
static const Il2CppMethodReference DESCryptoServiceProvider_t2073_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&SymmetricAlgorithm_Finalize_m7505_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&SymmetricAlgorithm_System_IDisposable_Dispose_m7589_MethodInfo,
	&SymmetricAlgorithm_Dispose_m7609_MethodInfo,
	&SymmetricAlgorithm_get_BlockSize_m7590_MethodInfo,
	&SymmetricAlgorithm_set_BlockSize_m7591_MethodInfo,
	&SymmetricAlgorithm_get_FeedbackSize_m7592_MethodInfo,
	&SymmetricAlgorithm_get_IV_m11288_MethodInfo,
	&SymmetricAlgorithm_set_IV_m11289_MethodInfo,
	&DES_get_Key_m11025_MethodInfo,
	&DES_set_Key_m11026_MethodInfo,
	&SymmetricAlgorithm_get_KeySize_m7593_MethodInfo,
	&SymmetricAlgorithm_set_KeySize_m7594_MethodInfo,
	&SymmetricAlgorithm_get_LegalKeySizes_m7595_MethodInfo,
	&SymmetricAlgorithm_get_Mode_m7596_MethodInfo,
	&SymmetricAlgorithm_set_Mode_m7597_MethodInfo,
	&SymmetricAlgorithm_get_Padding_m7598_MethodInfo,
	&SymmetricAlgorithm_set_Padding_m7599_MethodInfo,
	&SymmetricAlgorithm_CreateDecryptor_m7600_MethodInfo,
	&DESCryptoServiceProvider_CreateDecryptor_m11037_MethodInfo,
	&SymmetricAlgorithm_CreateEncryptor_m7601_MethodInfo,
	&DESCryptoServiceProvider_CreateEncryptor_m11038_MethodInfo,
	&DESCryptoServiceProvider_GenerateIV_m11039_MethodInfo,
	&DESCryptoServiceProvider_GenerateKey_m11040_MethodInfo,
};
static bool DESCryptoServiceProvider_t2073_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DESCryptoServiceProvider_t2073_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DESCryptoServiceProvider_t2073_0_0_0;
extern const Il2CppType DESCryptoServiceProvider_t2073_1_0_0;
struct DESCryptoServiceProvider_t2073;
const Il2CppTypeDefinitionMetadata DESCryptoServiceProvider_t2073_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DESCryptoServiceProvider_t2073_InterfacesOffsets/* interfaceOffsets */
	, &DES_t1649_0_0_0/* parent */
	, DESCryptoServiceProvider_t2073_VTable/* vtableMethods */
	, DESCryptoServiceProvider_t2073_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DESCryptoServiceProvider_t2073_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DESCryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, DESCryptoServiceProvider_t2073_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DESCryptoServiceProvider_t2073_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 658/* custom_attributes_cache */
	, &DESCryptoServiceProvider_t2073_0_0_0/* byval_arg */
	, &DESCryptoServiceProvider_t2073_1_0_0/* this_arg */
	, &DESCryptoServiceProvider_t2073_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DESCryptoServiceProvider_t2073)/* instance_size */
	, sizeof (DESCryptoServiceProvider_t2073)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.DSA
#include "mscorlib_System_Security_Cryptography_DSA.h"
// Metadata Definition System.Security.Cryptography.DSA
extern TypeInfo DSA_t1431_il2cpp_TypeInfo;
// System.Security.Cryptography.DSA
#include "mscorlib_System_Security_Cryptography_DSAMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSA::.ctor()
extern const MethodInfo DSA__ctor_m11041_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DSA__ctor_m11041/* method */
	, &DSA_t1431_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DSA_t1431_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.DSA System.Security.Cryptography.DSA::Create()
extern const MethodInfo DSA_Create_m6424_MethodInfo = 
{
	"Create"/* name */
	, (methodPointerType)&DSA_Create_m6424/* method */
	, &DSA_t1431_il2cpp_TypeInfo/* declaring_type */
	, &DSA_t1431_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DSA_t1431_DSA_Create_m11042_ParameterInfos[] = 
{
	{"algName", 0, 134223051, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.DSA System.Security.Cryptography.DSA::Create(System.String)
extern const MethodInfo DSA_Create_m11042_MethodInfo = 
{
	"Create"/* name */
	, (methodPointerType)&DSA_Create_m11042/* method */
	, &DSA_t1431_il2cpp_TypeInfo/* declaring_type */
	, &DSA_t1431_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DSA_t1431_DSA_Create_m11042_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DSA_t1431_DSA_CreateSignature_m13254_ParameterInfos[] = 
{
	{"rgbHash", 0, 134223052, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.DSA::CreateSignature(System.Byte[])
extern const MethodInfo DSA_CreateSignature_m13254_MethodInfo = 
{
	"CreateSignature"/* name */
	, NULL/* method */
	, &DSA_t1431_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DSA_t1431_DSA_CreateSignature_m13254_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo DSA_t1431_DSA_ExportParameters_m13255_ParameterInfos[] = 
{
	{"includePrivateParameters", 0, 134223053, 0, &Boolean_t273_0_0_0},
};
extern const Il2CppType DSAParameters_t1453_0_0_0;
extern void* RuntimeInvoker_DSAParameters_t1453_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.DSAParameters System.Security.Cryptography.DSA::ExportParameters(System.Boolean)
extern const MethodInfo DSA_ExportParameters_m13255_MethodInfo = 
{
	"ExportParameters"/* name */
	, NULL/* method */
	, &DSA_t1431_il2cpp_TypeInfo/* declaring_type */
	, &DSAParameters_t1453_0_0_0/* return_type */
	, RuntimeInvoker_DSAParameters_t1453_SByte_t274/* invoker_method */
	, DSA_t1431_DSA_ExportParameters_m13255_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DSAParameters_t1453_0_0_0;
static const ParameterInfo DSA_t1431_DSA_ZeroizePrivateKey_m11043_ParameterInfos[] = 
{
	{"parameters", 0, 134223054, 0, &DSAParameters_t1453_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_DSAParameters_t1453 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSA::ZeroizePrivateKey(System.Security.Cryptography.DSAParameters)
extern const MethodInfo DSA_ZeroizePrivateKey_m11043_MethodInfo = 
{
	"ZeroizePrivateKey"/* name */
	, (methodPointerType)&DSA_ZeroizePrivateKey_m11043/* method */
	, &DSA_t1431_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_DSAParameters_t1453/* invoker_method */
	, DSA_t1431_DSA_ZeroizePrivateKey_m11043_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DSA_t1431_DSA_FromXmlString_m11044_ParameterInfos[] = 
{
	{"xmlString", 0, 134223055, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSA::FromXmlString(System.String)
extern const MethodInfo DSA_FromXmlString_m11044_MethodInfo = 
{
	"FromXmlString"/* name */
	, (methodPointerType)&DSA_FromXmlString_m11044/* method */
	, &DSA_t1431_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DSA_t1431_DSA_FromXmlString_m11044_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DSAParameters_t1453_0_0_0;
static const ParameterInfo DSA_t1431_DSA_ImportParameters_m13256_ParameterInfos[] = 
{
	{"parameters", 0, 134223056, 0, &DSAParameters_t1453_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_DSAParameters_t1453 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSA::ImportParameters(System.Security.Cryptography.DSAParameters)
extern const MethodInfo DSA_ImportParameters_m13256_MethodInfo = 
{
	"ImportParameters"/* name */
	, NULL/* method */
	, &DSA_t1431_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_DSAParameters_t1453/* invoker_method */
	, DSA_t1431_DSA_ImportParameters_m13256_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo DSA_t1431_DSA_ToXmlString_m11045_ParameterInfos[] = 
{
	{"includePrivateParameters", 0, 134223057, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.String System.Security.Cryptography.DSA::ToXmlString(System.Boolean)
extern const MethodInfo DSA_ToXmlString_m11045_MethodInfo = 
{
	"ToXmlString"/* name */
	, (methodPointerType)&DSA_ToXmlString_m11045/* method */
	, &DSA_t1431_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t274/* invoker_method */
	, DSA_t1431_DSA_ToXmlString_m11045_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DSA_t1431_DSA_VerifySignature_m13257_ParameterInfos[] = 
{
	{"rgbHash", 0, 134223058, 0, &ByteU5BU5D_t850_0_0_0},
	{"rgbSignature", 1, 134223059, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Security.Cryptography.DSA::VerifySignature(System.Byte[],System.Byte[])
extern const MethodInfo DSA_VerifySignature_m13257_MethodInfo = 
{
	"VerifySignature"/* name */
	, NULL/* method */
	, &DSA_t1431_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, DSA_t1431_DSA_VerifySignature_m13257_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DSA_t1431_MethodInfos[] =
{
	&DSA__ctor_m11041_MethodInfo,
	&DSA_Create_m6424_MethodInfo,
	&DSA_Create_m11042_MethodInfo,
	&DSA_CreateSignature_m13254_MethodInfo,
	&DSA_ExportParameters_m13255_MethodInfo,
	&DSA_ZeroizePrivateKey_m11043_MethodInfo,
	&DSA_FromXmlString_m11044_MethodInfo,
	&DSA_ImportParameters_m13256_MethodInfo,
	&DSA_ToXmlString_m11045_MethodInfo,
	&DSA_VerifySignature_m13257_MethodInfo,
	NULL
};
extern const MethodInfo DSA_FromXmlString_m11044_MethodInfo;
extern const MethodInfo DSA_ToXmlString_m11045_MethodInfo;
static const Il2CppMethodReference DSA_t1431_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&AsymmetricAlgorithm_System_IDisposable_Dispose_m7612_MethodInfo,
	&AsymmetricAlgorithm_get_KeySize_m7518_MethodInfo,
	&AsymmetricAlgorithm_set_KeySize_m7517_MethodInfo,
	NULL,
	&DSA_FromXmlString_m11044_MethodInfo,
	&DSA_ToXmlString_m11045_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
};
static bool DSA_t1431_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DSA_t1431_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSA_t1431_1_0_0;
struct DSA_t1431;
const Il2CppTypeDefinitionMetadata DSA_t1431_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DSA_t1431_InterfacesOffsets/* interfaceOffsets */
	, &AsymmetricAlgorithm_t1310_0_0_0/* parent */
	, DSA_t1431_VTable/* vtableMethods */
	, DSA_t1431_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DSA_t1431_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSA"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, DSA_t1431_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DSA_t1431_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 659/* custom_attributes_cache */
	, &DSA_t1431_0_0_0/* byval_arg */
	, &DSA_t1431_1_0_0/* this_arg */
	, &DSA_t1431_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSA_t1431)/* instance_size */
	, sizeof (DSA_t1431)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.DSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DSACryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.DSACryptoServiceProvider
extern TypeInfo DSACryptoServiceProvider_t1452_il2cpp_TypeInfo;
// System.Security.Cryptography.DSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DSACryptoServiceProvidMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.ctor()
extern const MethodInfo DSACryptoServiceProvider__ctor_m11046_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DSACryptoServiceProvider__ctor_m11046/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo DSACryptoServiceProvider_t1452_DSACryptoServiceProvider__ctor_m6433_ParameterInfos[] = 
{
	{"dwKeySize", 0, 134223060, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.ctor(System.Int32)
extern const MethodInfo DSACryptoServiceProvider__ctor_m6433_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DSACryptoServiceProvider__ctor_m6433/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, DSACryptoServiceProvider_t1452_DSACryptoServiceProvider__ctor_m6433_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType CspParameters_t1639_0_0_0;
static const ParameterInfo DSACryptoServiceProvider_t1452_DSACryptoServiceProvider__ctor_m11047_ParameterInfos[] = 
{
	{"dwKeySize", 0, 134223061, 0, &Int32_t253_0_0_0},
	{"parameters", 1, 134223062, 0, &CspParameters_t1639_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.ctor(System.Int32,System.Security.Cryptography.CspParameters)
extern const MethodInfo DSACryptoServiceProvider__ctor_m11047_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DSACryptoServiceProvider__ctor_m11047/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Object_t/* invoker_method */
	, DSACryptoServiceProvider_t1452_DSACryptoServiceProvider__ctor_m11047_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.cctor()
extern const MethodInfo DSACryptoServiceProvider__cctor_m11048_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&DSACryptoServiceProvider__cctor_m11048/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::Finalize()
extern const MethodInfo DSACryptoServiceProvider_Finalize_m11049_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&DSACryptoServiceProvider_Finalize_m11049/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Security.Cryptography.DSACryptoServiceProvider::get_KeySize()
extern const MethodInfo DSACryptoServiceProvider_get_KeySize_m11050_MethodInfo = 
{
	"get_KeySize"/* name */
	, (methodPointerType)&DSACryptoServiceProvider_get_KeySize_m11050/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::get_PublicOnly()
extern const MethodInfo DSACryptoServiceProvider_get_PublicOnly_m6423_MethodInfo = 
{
	"get_PublicOnly"/* name */
	, (methodPointerType)&DSACryptoServiceProvider_get_PublicOnly_m6423/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_ExportParameters_m11051_ParameterInfos[] = 
{
	{"includePrivateParameters", 0, 134223063, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_DSAParameters_t1453_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.DSAParameters System.Security.Cryptography.DSACryptoServiceProvider::ExportParameters(System.Boolean)
extern const MethodInfo DSACryptoServiceProvider_ExportParameters_m11051_MethodInfo = 
{
	"ExportParameters"/* name */
	, (methodPointerType)&DSACryptoServiceProvider_ExportParameters_m11051/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &DSAParameters_t1453_0_0_0/* return_type */
	, RuntimeInvoker_DSAParameters_t1453_SByte_t274/* invoker_method */
	, DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_ExportParameters_m11051_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DSAParameters_t1453_0_0_0;
static const ParameterInfo DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_ImportParameters_m11052_ParameterInfos[] = 
{
	{"parameters", 0, 134223064, 0, &DSAParameters_t1453_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_DSAParameters_t1453 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::ImportParameters(System.Security.Cryptography.DSAParameters)
extern const MethodInfo DSACryptoServiceProvider_ImportParameters_m11052_MethodInfo = 
{
	"ImportParameters"/* name */
	, (methodPointerType)&DSACryptoServiceProvider_ImportParameters_m11052/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_DSAParameters_t1453/* invoker_method */
	, DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_ImportParameters_m11052_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_CreateSignature_m11053_ParameterInfos[] = 
{
	{"rgbHash", 0, 134223065, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::CreateSignature(System.Byte[])
extern const MethodInfo DSACryptoServiceProvider_CreateSignature_m11053_MethodInfo = 
{
	"CreateSignature"/* name */
	, (methodPointerType)&DSACryptoServiceProvider_CreateSignature_m11053/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_CreateSignature_m11053_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_VerifySignature_m11054_ParameterInfos[] = 
{
	{"rgbHash", 0, 134223066, 0, &ByteU5BU5D_t850_0_0_0},
	{"rgbSignature", 1, 134223067, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::VerifySignature(System.Byte[],System.Byte[])
extern const MethodInfo DSACryptoServiceProvider_VerifySignature_m11054_MethodInfo = 
{
	"VerifySignature"/* name */
	, (methodPointerType)&DSACryptoServiceProvider_VerifySignature_m11054/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_VerifySignature_m11054_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_Dispose_m11055_ParameterInfos[] = 
{
	{"disposing", 0, 134223068, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::Dispose(System.Boolean)
extern const MethodInfo DSACryptoServiceProvider_Dispose_m11055_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&DSACryptoServiceProvider_Dispose_m11055/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_Dispose_m11055_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType EventArgs_t1547_0_0_0;
extern const Il2CppType EventArgs_t1547_0_0_0;
static const ParameterInfo DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_OnKeyGenerated_m11056_ParameterInfos[] = 
{
	{"sender", 0, 134223069, 0, &Object_t_0_0_0},
	{"e", 1, 134223070, 0, &EventArgs_t1547_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::OnKeyGenerated(System.Object,System.EventArgs)
extern const MethodInfo DSACryptoServiceProvider_OnKeyGenerated_m11056_MethodInfo = 
{
	"OnKeyGenerated"/* name */
	, (methodPointerType)&DSACryptoServiceProvider_OnKeyGenerated_m11056/* method */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, DSACryptoServiceProvider_t1452_DSACryptoServiceProvider_OnKeyGenerated_m11056_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DSACryptoServiceProvider_t1452_MethodInfos[] =
{
	&DSACryptoServiceProvider__ctor_m11046_MethodInfo,
	&DSACryptoServiceProvider__ctor_m6433_MethodInfo,
	&DSACryptoServiceProvider__ctor_m11047_MethodInfo,
	&DSACryptoServiceProvider__cctor_m11048_MethodInfo,
	&DSACryptoServiceProvider_Finalize_m11049_MethodInfo,
	&DSACryptoServiceProvider_get_KeySize_m11050_MethodInfo,
	&DSACryptoServiceProvider_get_PublicOnly_m6423_MethodInfo,
	&DSACryptoServiceProvider_ExportParameters_m11051_MethodInfo,
	&DSACryptoServiceProvider_ImportParameters_m11052_MethodInfo,
	&DSACryptoServiceProvider_CreateSignature_m11053_MethodInfo,
	&DSACryptoServiceProvider_VerifySignature_m11054_MethodInfo,
	&DSACryptoServiceProvider_Dispose_m11055_MethodInfo,
	&DSACryptoServiceProvider_OnKeyGenerated_m11056_MethodInfo,
	NULL
};
extern const MethodInfo DSACryptoServiceProvider_get_KeySize_m11050_MethodInfo;
static const PropertyInfo DSACryptoServiceProvider_t1452____KeySize_PropertyInfo = 
{
	&DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* parent */
	, "KeySize"/* name */
	, &DSACryptoServiceProvider_get_KeySize_m11050_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo DSACryptoServiceProvider_get_PublicOnly_m6423_MethodInfo;
static const PropertyInfo DSACryptoServiceProvider_t1452____PublicOnly_PropertyInfo = 
{
	&DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* parent */
	, "PublicOnly"/* name */
	, &DSACryptoServiceProvider_get_PublicOnly_m6423_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 661/* custom_attributes_cache */

};
static const PropertyInfo* DSACryptoServiceProvider_t1452_PropertyInfos[] =
{
	&DSACryptoServiceProvider_t1452____KeySize_PropertyInfo,
	&DSACryptoServiceProvider_t1452____PublicOnly_PropertyInfo,
	NULL
};
extern const MethodInfo DSACryptoServiceProvider_Finalize_m11049_MethodInfo;
extern const MethodInfo DSACryptoServiceProvider_Dispose_m11055_MethodInfo;
extern const MethodInfo DSACryptoServiceProvider_CreateSignature_m11053_MethodInfo;
extern const MethodInfo DSACryptoServiceProvider_ExportParameters_m11051_MethodInfo;
extern const MethodInfo DSACryptoServiceProvider_ImportParameters_m11052_MethodInfo;
extern const MethodInfo DSACryptoServiceProvider_VerifySignature_m11054_MethodInfo;
static const Il2CppMethodReference DSACryptoServiceProvider_t1452_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&DSACryptoServiceProvider_Finalize_m11049_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&AsymmetricAlgorithm_System_IDisposable_Dispose_m7612_MethodInfo,
	&DSACryptoServiceProvider_get_KeySize_m11050_MethodInfo,
	&AsymmetricAlgorithm_set_KeySize_m7517_MethodInfo,
	&DSACryptoServiceProvider_Dispose_m11055_MethodInfo,
	&DSA_FromXmlString_m11044_MethodInfo,
	&DSA_ToXmlString_m11045_MethodInfo,
	&DSACryptoServiceProvider_CreateSignature_m11053_MethodInfo,
	&DSACryptoServiceProvider_ExportParameters_m11051_MethodInfo,
	&DSACryptoServiceProvider_ImportParameters_m11052_MethodInfo,
	&DSACryptoServiceProvider_VerifySignature_m11054_MethodInfo,
};
static bool DSACryptoServiceProvider_t1452_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICspAsymmetricAlgorithm_t2386_0_0_0;
static const Il2CppType* DSACryptoServiceProvider_t1452_InterfacesTypeInfos[] = 
{
	&ICspAsymmetricAlgorithm_t2386_0_0_0,
};
static Il2CppInterfaceOffsetPair DSACryptoServiceProvider_t1452_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
	{ &ICspAsymmetricAlgorithm_t2386_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSACryptoServiceProvider_t1452_0_0_0;
extern const Il2CppType DSACryptoServiceProvider_t1452_1_0_0;
struct DSACryptoServiceProvider_t1452;
const Il2CppTypeDefinitionMetadata DSACryptoServiceProvider_t1452_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DSACryptoServiceProvider_t1452_InterfacesTypeInfos/* implementedInterfaces */
	, DSACryptoServiceProvider_t1452_InterfacesOffsets/* interfaceOffsets */
	, &DSA_t1431_0_0_0/* parent */
	, DSACryptoServiceProvider_t1452_VTable/* vtableMethods */
	, DSACryptoServiceProvider_t1452_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2344/* fieldStart */

};
TypeInfo DSACryptoServiceProvider_t1452_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSACryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, DSACryptoServiceProvider_t1452_MethodInfos/* methods */
	, DSACryptoServiceProvider_t1452_PropertyInfos/* properties */
	, NULL/* events */
	, &DSACryptoServiceProvider_t1452_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 660/* custom_attributes_cache */
	, &DSACryptoServiceProvider_t1452_0_0_0/* byval_arg */
	, &DSACryptoServiceProvider_t1452_1_0_0/* this_arg */
	, &DSACryptoServiceProvider_t1452_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSACryptoServiceProvider_t1452)/* instance_size */
	, sizeof (DSACryptoServiceProvider_t1452)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DSACryptoServiceProvider_t1452_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
// Metadata Definition System.Security.Cryptography.DSAParameters
extern TypeInfo DSAParameters_t1453_il2cpp_TypeInfo;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParametersMethodDeclarations.h"
static const MethodInfo* DSAParameters_t1453_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference DSAParameters_t1453_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool DSAParameters_t1453_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSAParameters_t1453_1_0_0;
const Il2CppTypeDefinitionMetadata DSAParameters_t1453_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, DSAParameters_t1453_VTable/* vtableMethods */
	, DSAParameters_t1453_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2351/* fieldStart */

};
TypeInfo DSAParameters_t1453_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSAParameters"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, DSAParameters_t1453_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DSAParameters_t1453_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 662/* custom_attributes_cache */
	, &DSAParameters_t1453_0_0_0/* byval_arg */
	, &DSAParameters_t1453_1_0_0/* this_arg */
	, &DSAParameters_t1453_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)DSAParameters_t1453_marshal/* marshal_to_native_func */
	, (methodPointerType)DSAParameters_t1453_marshal_back/* marshal_from_native_func */
	, (methodPointerType)DSAParameters_t1453_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (DSAParameters_t1453)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DSAParameters_t1453)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(DSAParameters_t1453_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.DSASignatureDeformatter
#include "mscorlib_System_Security_Cryptography_DSASignatureDeformatte.h"
// Metadata Definition System.Security.Cryptography.DSASignatureDeformatter
extern TypeInfo DSASignatureDeformatter_t1646_il2cpp_TypeInfo;
// System.Security.Cryptography.DSASignatureDeformatter
#include "mscorlib_System_Security_Cryptography_DSASignatureDeformatteMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSASignatureDeformatter::.ctor()
extern const MethodInfo DSASignatureDeformatter__ctor_m11057_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DSASignatureDeformatter__ctor_m11057/* method */
	, &DSASignatureDeformatter_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsymmetricAlgorithm_t1310_0_0_0;
static const ParameterInfo DSASignatureDeformatter_t1646_DSASignatureDeformatter__ctor_m7533_ParameterInfos[] = 
{
	{"key", 0, 134223071, 0, &AsymmetricAlgorithm_t1310_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSASignatureDeformatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern const MethodInfo DSASignatureDeformatter__ctor_m7533_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DSASignatureDeformatter__ctor_m7533/* method */
	, &DSASignatureDeformatter_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DSASignatureDeformatter_t1646_DSASignatureDeformatter__ctor_m7533_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DSASignatureDeformatter_t1646_DSASignatureDeformatter_SetHashAlgorithm_m11058_ParameterInfos[] = 
{
	{"strName", 0, 134223072, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSASignatureDeformatter::SetHashAlgorithm(System.String)
extern const MethodInfo DSASignatureDeformatter_SetHashAlgorithm_m11058_MethodInfo = 
{
	"SetHashAlgorithm"/* name */
	, (methodPointerType)&DSASignatureDeformatter_SetHashAlgorithm_m11058/* method */
	, &DSASignatureDeformatter_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DSASignatureDeformatter_t1646_DSASignatureDeformatter_SetHashAlgorithm_m11058_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsymmetricAlgorithm_t1310_0_0_0;
static const ParameterInfo DSASignatureDeformatter_t1646_DSASignatureDeformatter_SetKey_m11059_ParameterInfos[] = 
{
	{"key", 0, 134223073, 0, &AsymmetricAlgorithm_t1310_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSASignatureDeformatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern const MethodInfo DSASignatureDeformatter_SetKey_m11059_MethodInfo = 
{
	"SetKey"/* name */
	, (methodPointerType)&DSASignatureDeformatter_SetKey_m11059/* method */
	, &DSASignatureDeformatter_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DSASignatureDeformatter_t1646_DSASignatureDeformatter_SetKey_m11059_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DSASignatureDeformatter_t1646_DSASignatureDeformatter_VerifySignature_m11060_ParameterInfos[] = 
{
	{"rgbHash", 0, 134223074, 0, &ByteU5BU5D_t850_0_0_0},
	{"rgbSignature", 1, 134223075, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Security.Cryptography.DSASignatureDeformatter::VerifySignature(System.Byte[],System.Byte[])
extern const MethodInfo DSASignatureDeformatter_VerifySignature_m11060_MethodInfo = 
{
	"VerifySignature"/* name */
	, (methodPointerType)&DSASignatureDeformatter_VerifySignature_m11060/* method */
	, &DSASignatureDeformatter_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, DSASignatureDeformatter_t1646_DSASignatureDeformatter_VerifySignature_m11060_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DSASignatureDeformatter_t1646_MethodInfos[] =
{
	&DSASignatureDeformatter__ctor_m11057_MethodInfo,
	&DSASignatureDeformatter__ctor_m7533_MethodInfo,
	&DSASignatureDeformatter_SetHashAlgorithm_m11058_MethodInfo,
	&DSASignatureDeformatter_SetKey_m11059_MethodInfo,
	&DSASignatureDeformatter_VerifySignature_m11060_MethodInfo,
	NULL
};
extern const MethodInfo DSASignatureDeformatter_SetHashAlgorithm_m11058_MethodInfo;
extern const MethodInfo DSASignatureDeformatter_SetKey_m11059_MethodInfo;
extern const MethodInfo DSASignatureDeformatter_VerifySignature_m11060_MethodInfo;
static const Il2CppMethodReference DSASignatureDeformatter_t1646_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&DSASignatureDeformatter_SetHashAlgorithm_m11058_MethodInfo,
	&DSASignatureDeformatter_SetKey_m11059_MethodInfo,
	&DSASignatureDeformatter_VerifySignature_m11060_MethodInfo,
};
static bool DSASignatureDeformatter_t1646_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSASignatureDeformatter_t1646_0_0_0;
extern const Il2CppType DSASignatureDeformatter_t1646_1_0_0;
struct DSASignatureDeformatter_t1646;
const Il2CppTypeDefinitionMetadata DSASignatureDeformatter_t1646_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureDeformatter_t1596_0_0_0/* parent */
	, DSASignatureDeformatter_t1646_VTable/* vtableMethods */
	, DSASignatureDeformatter_t1646_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2359/* fieldStart */

};
TypeInfo DSASignatureDeformatter_t1646_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSASignatureDeformatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, DSASignatureDeformatter_t1646_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DSASignatureDeformatter_t1646_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 663/* custom_attributes_cache */
	, &DSASignatureDeformatter_t1646_0_0_0/* byval_arg */
	, &DSASignatureDeformatter_t1646_1_0_0/* this_arg */
	, &DSASignatureDeformatter_t1646_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSASignatureDeformatter_t1646)/* instance_size */
	, sizeof (DSASignatureDeformatter_t1646)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.DSASignatureFormatter
#include "mscorlib_System_Security_Cryptography_DSASignatureFormatter.h"
// Metadata Definition System.Security.Cryptography.DSASignatureFormatter
extern TypeInfo DSASignatureFormatter_t2074_il2cpp_TypeInfo;
// System.Security.Cryptography.DSASignatureFormatter
#include "mscorlib_System_Security_Cryptography_DSASignatureFormatterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSASignatureFormatter::.ctor()
extern const MethodInfo DSASignatureFormatter__ctor_m11061_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DSASignatureFormatter__ctor_m11061/* method */
	, &DSASignatureFormatter_t2074_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo DSASignatureFormatter_t2074_DSASignatureFormatter_CreateSignature_m11062_ParameterInfos[] = 
{
	{"rgbHash", 0, 134223076, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.DSASignatureFormatter::CreateSignature(System.Byte[])
extern const MethodInfo DSASignatureFormatter_CreateSignature_m11062_MethodInfo = 
{
	"CreateSignature"/* name */
	, (methodPointerType)&DSASignatureFormatter_CreateSignature_m11062/* method */
	, &DSASignatureFormatter_t2074_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DSASignatureFormatter_t2074_DSASignatureFormatter_CreateSignature_m11062_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DSASignatureFormatter_t2074_DSASignatureFormatter_SetHashAlgorithm_m11063_ParameterInfos[] = 
{
	{"strName", 0, 134223077, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSASignatureFormatter::SetHashAlgorithm(System.String)
extern const MethodInfo DSASignatureFormatter_SetHashAlgorithm_m11063_MethodInfo = 
{
	"SetHashAlgorithm"/* name */
	, (methodPointerType)&DSASignatureFormatter_SetHashAlgorithm_m11063/* method */
	, &DSASignatureFormatter_t2074_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DSASignatureFormatter_t2074_DSASignatureFormatter_SetHashAlgorithm_m11063_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsymmetricAlgorithm_t1310_0_0_0;
static const ParameterInfo DSASignatureFormatter_t2074_DSASignatureFormatter_SetKey_m11064_ParameterInfos[] = 
{
	{"key", 0, 134223078, 0, &AsymmetricAlgorithm_t1310_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.DSASignatureFormatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern const MethodInfo DSASignatureFormatter_SetKey_m11064_MethodInfo = 
{
	"SetKey"/* name */
	, (methodPointerType)&DSASignatureFormatter_SetKey_m11064/* method */
	, &DSASignatureFormatter_t2074_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, DSASignatureFormatter_t2074_DSASignatureFormatter_SetKey_m11064_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DSASignatureFormatter_t2074_MethodInfos[] =
{
	&DSASignatureFormatter__ctor_m11061_MethodInfo,
	&DSASignatureFormatter_CreateSignature_m11062_MethodInfo,
	&DSASignatureFormatter_SetHashAlgorithm_m11063_MethodInfo,
	&DSASignatureFormatter_SetKey_m11064_MethodInfo,
	NULL
};
extern const MethodInfo DSASignatureFormatter_SetHashAlgorithm_m11063_MethodInfo;
extern const MethodInfo DSASignatureFormatter_SetKey_m11064_MethodInfo;
extern const MethodInfo DSASignatureFormatter_CreateSignature_m11062_MethodInfo;
static const Il2CppMethodReference DSASignatureFormatter_t2074_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&DSASignatureFormatter_SetHashAlgorithm_m11063_MethodInfo,
	&DSASignatureFormatter_SetKey_m11064_MethodInfo,
	&DSASignatureFormatter_CreateSignature_m11062_MethodInfo,
};
static bool DSASignatureFormatter_t2074_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSASignatureFormatter_t2074_0_0_0;
extern const Il2CppType DSASignatureFormatter_t2074_1_0_0;
struct DSASignatureFormatter_t2074;
const Il2CppTypeDefinitionMetadata DSASignatureFormatter_t2074_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureFormatter_t1598_0_0_0/* parent */
	, DSASignatureFormatter_t2074_VTable/* vtableMethods */
	, DSASignatureFormatter_t2074_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2360/* fieldStart */

};
TypeInfo DSASignatureFormatter_t2074_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSASignatureFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, DSASignatureFormatter_t2074_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DSASignatureFormatter_t2074_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 664/* custom_attributes_cache */
	, &DSASignatureFormatter_t2074_0_0_0/* byval_arg */
	, &DSASignatureFormatter_t2074_1_0_0/* this_arg */
	, &DSASignatureFormatter_t2074_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSASignatureFormatter_t2074)/* instance_size */
	, sizeof (DSASignatureFormatter_t2074)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMAC.h"
// Metadata Definition System.Security.Cryptography.HMAC
extern TypeInfo HMAC_t1643_il2cpp_TypeInfo;
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMACMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMAC::.ctor()
extern const MethodInfo HMAC__ctor_m11065_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HMAC__ctor_m11065/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Security.Cryptography.HMAC::get_BlockSizeValue()
extern const MethodInfo HMAC_get_BlockSizeValue_m11066_MethodInfo = 
{
	"get_BlockSizeValue"/* name */
	, (methodPointerType)&HMAC_get_BlockSizeValue_m11066/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo HMAC_t1643_HMAC_set_BlockSizeValue_m11067_ParameterInfos[] = 
{
	{"value", 0, 134223079, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMAC::set_BlockSizeValue(System.Int32)
extern const MethodInfo HMAC_set_BlockSizeValue_m11067_MethodInfo = 
{
	"set_BlockSizeValue"/* name */
	, (methodPointerType)&HMAC_set_BlockSizeValue_m11067/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, HMAC_t1643_HMAC_set_BlockSizeValue_m11067_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo HMAC_t1643_HMAC_set_HashName_m11068_ParameterInfos[] = 
{
	{"value", 0, 134223080, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMAC::set_HashName(System.String)
extern const MethodInfo HMAC_set_HashName_m11068_MethodInfo = 
{
	"set_HashName"/* name */
	, (methodPointerType)&HMAC_set_HashName_m11068/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, HMAC_t1643_HMAC_set_HashName_m11068_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.HMAC::get_Key()
extern const MethodInfo HMAC_get_Key_m11069_MethodInfo = 
{
	"get_Key"/* name */
	, (methodPointerType)&HMAC_get_Key_m11069/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo HMAC_t1643_HMAC_set_Key_m11070_ParameterInfos[] = 
{
	{"value", 0, 134223081, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMAC::set_Key(System.Byte[])
extern const MethodInfo HMAC_set_Key_m11070_MethodInfo = 
{
	"set_Key"/* name */
	, (methodPointerType)&HMAC_set_Key_m11070/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, HMAC_t1643_HMAC_set_Key_m11070_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BlockProcessor_t1721_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// Mono.Security.Cryptography.BlockProcessor System.Security.Cryptography.HMAC::get_Block()
extern const MethodInfo HMAC_get_Block_m11071_MethodInfo = 
{
	"get_Block"/* name */
	, (methodPointerType)&HMAC_get_Block_m11071/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &BlockProcessor_t1721_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType Byte_t680_0_0_0;
static const ParameterInfo HMAC_t1643_HMAC_KeySetup_m11072_ParameterInfos[] = 
{
	{"key", 0, 134223082, 0, &ByteU5BU5D_t850_0_0_0},
	{"padding", 1, 134223083, 0, &Byte_t680_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.HMAC::KeySetup(System.Byte[],System.Byte)
extern const MethodInfo HMAC_KeySetup_m11072_MethodInfo = 
{
	"KeySetup"/* name */
	, (methodPointerType)&HMAC_KeySetup_m11072/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274/* invoker_method */
	, HMAC_t1643_HMAC_KeySetup_m11072_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo HMAC_t1643_HMAC_Dispose_m11073_ParameterInfos[] = 
{
	{"disposing", 0, 134223084, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMAC::Dispose(System.Boolean)
extern const MethodInfo HMAC_Dispose_m11073_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&HMAC_Dispose_m11073/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, HMAC_t1643_HMAC_Dispose_m11073_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo HMAC_t1643_HMAC_HashCore_m11074_ParameterInfos[] = 
{
	{"rgb", 0, 134223085, 0, &ByteU5BU5D_t850_0_0_0},
	{"ib", 1, 134223086, 0, &Int32_t253_0_0_0},
	{"cb", 2, 134223087, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMAC::HashCore(System.Byte[],System.Int32,System.Int32)
extern const MethodInfo HMAC_HashCore_m11074_MethodInfo = 
{
	"HashCore"/* name */
	, (methodPointerType)&HMAC_HashCore_m11074/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, HMAC_t1643_HMAC_HashCore_m11074_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Security.Cryptography.HMAC::HashFinal()
extern const MethodInfo HMAC_HashFinal_m11075_MethodInfo = 
{
	"HashFinal"/* name */
	, (methodPointerType)&HMAC_HashFinal_m11075/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMAC::Initialize()
extern const MethodInfo HMAC_Initialize_m11076_MethodInfo = 
{
	"Initialize"/* name */
	, (methodPointerType)&HMAC_Initialize_m11076/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HMAC_t1643_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.HMAC System.Security.Cryptography.HMAC::Create()
extern const MethodInfo HMAC_Create_m7526_MethodInfo = 
{
	"Create"/* name */
	, (methodPointerType)&HMAC_Create_m7526/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &HMAC_t1643_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo HMAC_t1643_HMAC_Create_m11077_ParameterInfos[] = 
{
	{"algorithmName", 0, 134223088, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.HMAC System.Security.Cryptography.HMAC::Create(System.String)
extern const MethodInfo HMAC_Create_m11077_MethodInfo = 
{
	"Create"/* name */
	, (methodPointerType)&HMAC_Create_m11077/* method */
	, &HMAC_t1643_il2cpp_TypeInfo/* declaring_type */
	, &HMAC_t1643_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, HMAC_t1643_HMAC_Create_m11077_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HMAC_t1643_MethodInfos[] =
{
	&HMAC__ctor_m11065_MethodInfo,
	&HMAC_get_BlockSizeValue_m11066_MethodInfo,
	&HMAC_set_BlockSizeValue_m11067_MethodInfo,
	&HMAC_set_HashName_m11068_MethodInfo,
	&HMAC_get_Key_m11069_MethodInfo,
	&HMAC_set_Key_m11070_MethodInfo,
	&HMAC_get_Block_m11071_MethodInfo,
	&HMAC_KeySetup_m11072_MethodInfo,
	&HMAC_Dispose_m11073_MethodInfo,
	&HMAC_HashCore_m11074_MethodInfo,
	&HMAC_HashFinal_m11075_MethodInfo,
	&HMAC_Initialize_m11076_MethodInfo,
	&HMAC_Create_m7526_MethodInfo,
	&HMAC_Create_m11077_MethodInfo,
	NULL
};
extern const MethodInfo HMAC_get_BlockSizeValue_m11066_MethodInfo;
extern const MethodInfo HMAC_set_BlockSizeValue_m11067_MethodInfo;
static const PropertyInfo HMAC_t1643____BlockSizeValue_PropertyInfo = 
{
	&HMAC_t1643_il2cpp_TypeInfo/* parent */
	, "BlockSizeValue"/* name */
	, &HMAC_get_BlockSizeValue_m11066_MethodInfo/* get */
	, &HMAC_set_BlockSizeValue_m11067_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HMAC_set_HashName_m11068_MethodInfo;
static const PropertyInfo HMAC_t1643____HashName_PropertyInfo = 
{
	&HMAC_t1643_il2cpp_TypeInfo/* parent */
	, "HashName"/* name */
	, NULL/* get */
	, &HMAC_set_HashName_m11068_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HMAC_get_Key_m11069_MethodInfo;
extern const MethodInfo HMAC_set_Key_m11070_MethodInfo;
static const PropertyInfo HMAC_t1643____Key_PropertyInfo = 
{
	&HMAC_t1643_il2cpp_TypeInfo/* parent */
	, "Key"/* name */
	, &HMAC_get_Key_m11069_MethodInfo/* get */
	, &HMAC_set_Key_m11070_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HMAC_get_Block_m11071_MethodInfo;
static const PropertyInfo HMAC_t1643____Block_PropertyInfo = 
{
	&HMAC_t1643_il2cpp_TypeInfo/* parent */
	, "Block"/* name */
	, &HMAC_get_Block_m11071_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* HMAC_t1643_PropertyInfos[] =
{
	&HMAC_t1643____BlockSizeValue_PropertyInfo,
	&HMAC_t1643____HashName_PropertyInfo,
	&HMAC_t1643____Key_PropertyInfo,
	&HMAC_t1643____Block_PropertyInfo,
	NULL
};
extern const MethodInfo KeyedHashAlgorithm_Finalize_m7614_MethodInfo;
extern const MethodInfo HashAlgorithm_System_IDisposable_Dispose_m7602_MethodInfo;
extern const MethodInfo HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo;
extern const MethodInfo HashAlgorithm_TransformBlock_m7604_MethodInfo;
extern const MethodInfo HashAlgorithm_TransformFinalBlock_m7605_MethodInfo;
extern const MethodInfo HashAlgorithm_get_Hash_m7606_MethodInfo;
extern const MethodInfo HMAC_HashCore_m11074_MethodInfo;
extern const MethodInfo HMAC_HashFinal_m11075_MethodInfo;
extern const MethodInfo HashAlgorithm_get_HashSize_m7607_MethodInfo;
extern const MethodInfo HMAC_Initialize_m11076_MethodInfo;
extern const MethodInfo HMAC_Dispose_m11073_MethodInfo;
static const Il2CppMethodReference HMAC_t1643_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&KeyedHashAlgorithm_Finalize_m7614_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&HashAlgorithm_System_IDisposable_Dispose_m7602_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_TransformBlock_m7604_MethodInfo,
	&HashAlgorithm_TransformFinalBlock_m7605_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_get_Hash_m7606_MethodInfo,
	&HMAC_HashCore_m11074_MethodInfo,
	&HMAC_HashFinal_m11075_MethodInfo,
	&HashAlgorithm_get_HashSize_m7607_MethodInfo,
	&HMAC_Initialize_m11076_MethodInfo,
	&HMAC_Dispose_m11073_MethodInfo,
	&HMAC_get_Key_m11069_MethodInfo,
	&HMAC_set_Key_m11070_MethodInfo,
};
static bool HMAC_t1643_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HMAC_t1643_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
	{ &ICryptoTransform_t1570_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMAC_t1643_1_0_0;
extern const Il2CppType KeyedHashAlgorithm_t1563_0_0_0;
struct HMAC_t1643;
const Il2CppTypeDefinitionMetadata HMAC_t1643_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMAC_t1643_InterfacesOffsets/* interfaceOffsets */
	, &KeyedHashAlgorithm_t1563_0_0_0/* parent */
	, HMAC_t1643_VTable/* vtableMethods */
	, HMAC_t1643_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2361/* fieldStart */

};
TypeInfo HMAC_t1643_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMAC"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, HMAC_t1643_MethodInfos/* methods */
	, HMAC_t1643_PropertyInfos/* properties */
	, NULL/* events */
	, &HMAC_t1643_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 665/* custom_attributes_cache */
	, &HMAC_t1643_0_0_0/* byval_arg */
	, &HMAC_t1643_1_0_0/* this_arg */
	, &HMAC_t1643_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMAC_t1643)/* instance_size */
	, sizeof (HMAC_t1643)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACMD5
#include "mscorlib_System_Security_Cryptography_HMACMD5.h"
// Metadata Definition System.Security.Cryptography.HMACMD5
extern TypeInfo HMACMD5_t2075_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACMD5
#include "mscorlib_System_Security_Cryptography_HMACMD5MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACMD5::.ctor()
extern const MethodInfo HMACMD5__ctor_m11078_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HMACMD5__ctor_m11078/* method */
	, &HMACMD5_t2075_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo HMACMD5_t2075_HMACMD5__ctor_m11079_ParameterInfos[] = 
{
	{"key", 0, 134223089, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACMD5::.ctor(System.Byte[])
extern const MethodInfo HMACMD5__ctor_m11079_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HMACMD5__ctor_m11079/* method */
	, &HMACMD5_t2075_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, HMACMD5_t2075_HMACMD5__ctor_m11079_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HMACMD5_t2075_MethodInfos[] =
{
	&HMACMD5__ctor_m11078_MethodInfo,
	&HMACMD5__ctor_m11079_MethodInfo,
	NULL
};
static const Il2CppMethodReference HMACMD5_t2075_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&KeyedHashAlgorithm_Finalize_m7614_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&HashAlgorithm_System_IDisposable_Dispose_m7602_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_TransformBlock_m7604_MethodInfo,
	&HashAlgorithm_TransformFinalBlock_m7605_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_get_Hash_m7606_MethodInfo,
	&HMAC_HashCore_m11074_MethodInfo,
	&HMAC_HashFinal_m11075_MethodInfo,
	&HashAlgorithm_get_HashSize_m7607_MethodInfo,
	&HMAC_Initialize_m11076_MethodInfo,
	&HMAC_Dispose_m11073_MethodInfo,
	&HMAC_get_Key_m11069_MethodInfo,
	&HMAC_set_Key_m11070_MethodInfo,
};
static bool HMACMD5_t2075_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HMACMD5_t2075_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
	{ &ICryptoTransform_t1570_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACMD5_t2075_0_0_0;
extern const Il2CppType HMACMD5_t2075_1_0_0;
struct HMACMD5_t2075;
const Il2CppTypeDefinitionMetadata HMACMD5_t2075_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACMD5_t2075_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t1643_0_0_0/* parent */
	, HMACMD5_t2075_VTable/* vtableMethods */
	, HMACMD5_t2075_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HMACMD5_t2075_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACMD5"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, HMACMD5_t2075_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HMACMD5_t2075_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 666/* custom_attributes_cache */
	, &HMACMD5_t2075_0_0_0/* byval_arg */
	, &HMACMD5_t2075_1_0_0/* this_arg */
	, &HMACMD5_t2075_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACMD5_t2075)/* instance_size */
	, sizeof (HMACMD5_t2075)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACRIPEMD160
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD160.h"
// Metadata Definition System.Security.Cryptography.HMACRIPEMD160
extern TypeInfo HMACRIPEMD160_t2076_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACRIPEMD160
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD160MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor()
extern const MethodInfo HMACRIPEMD160__ctor_m11080_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HMACRIPEMD160__ctor_m11080/* method */
	, &HMACRIPEMD160_t2076_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo HMACRIPEMD160_t2076_HMACRIPEMD160__ctor_m11081_ParameterInfos[] = 
{
	{"key", 0, 134223090, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor(System.Byte[])
extern const MethodInfo HMACRIPEMD160__ctor_m11081_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HMACRIPEMD160__ctor_m11081/* method */
	, &HMACRIPEMD160_t2076_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, HMACRIPEMD160_t2076_HMACRIPEMD160__ctor_m11081_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HMACRIPEMD160_t2076_MethodInfos[] =
{
	&HMACRIPEMD160__ctor_m11080_MethodInfo,
	&HMACRIPEMD160__ctor_m11081_MethodInfo,
	NULL
};
static const Il2CppMethodReference HMACRIPEMD160_t2076_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&KeyedHashAlgorithm_Finalize_m7614_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&HashAlgorithm_System_IDisposable_Dispose_m7602_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_TransformBlock_m7604_MethodInfo,
	&HashAlgorithm_TransformFinalBlock_m7605_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_get_Hash_m7606_MethodInfo,
	&HMAC_HashCore_m11074_MethodInfo,
	&HMAC_HashFinal_m11075_MethodInfo,
	&HashAlgorithm_get_HashSize_m7607_MethodInfo,
	&HMAC_Initialize_m11076_MethodInfo,
	&HMAC_Dispose_m11073_MethodInfo,
	&HMAC_get_Key_m11069_MethodInfo,
	&HMAC_set_Key_m11070_MethodInfo,
};
static bool HMACRIPEMD160_t2076_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HMACRIPEMD160_t2076_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
	{ &ICryptoTransform_t1570_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACRIPEMD160_t2076_0_0_0;
extern const Il2CppType HMACRIPEMD160_t2076_1_0_0;
struct HMACRIPEMD160_t2076;
const Il2CppTypeDefinitionMetadata HMACRIPEMD160_t2076_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACRIPEMD160_t2076_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t1643_0_0_0/* parent */
	, HMACRIPEMD160_t2076_VTable/* vtableMethods */
	, HMACRIPEMD160_t2076_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HMACRIPEMD160_t2076_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACRIPEMD160"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, HMACRIPEMD160_t2076_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HMACRIPEMD160_t2076_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 667/* custom_attributes_cache */
	, &HMACRIPEMD160_t2076_0_0_0/* byval_arg */
	, &HMACRIPEMD160_t2076_1_0_0/* this_arg */
	, &HMACRIPEMD160_t2076_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACRIPEMD160_t2076)/* instance_size */
	, sizeof (HMACRIPEMD160_t2076)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACSHA1
#include "mscorlib_System_Security_Cryptography_HMACSHA1.h"
// Metadata Definition System.Security.Cryptography.HMACSHA1
extern TypeInfo HMACSHA1_t1642_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACSHA1
#include "mscorlib_System_Security_Cryptography_HMACSHA1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACSHA1::.ctor()
extern const MethodInfo HMACSHA1__ctor_m11082_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HMACSHA1__ctor_m11082/* method */
	, &HMACSHA1_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo HMACSHA1_t1642_HMACSHA1__ctor_m11083_ParameterInfos[] = 
{
	{"key", 0, 134223091, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACSHA1::.ctor(System.Byte[])
extern const MethodInfo HMACSHA1__ctor_m11083_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HMACSHA1__ctor_m11083/* method */
	, &HMACSHA1_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, HMACSHA1_t1642_HMACSHA1__ctor_m11083_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HMACSHA1_t1642_MethodInfos[] =
{
	&HMACSHA1__ctor_m11082_MethodInfo,
	&HMACSHA1__ctor_m11083_MethodInfo,
	NULL
};
static const Il2CppMethodReference HMACSHA1_t1642_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&KeyedHashAlgorithm_Finalize_m7614_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&HashAlgorithm_System_IDisposable_Dispose_m7602_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_TransformBlock_m7604_MethodInfo,
	&HashAlgorithm_TransformFinalBlock_m7605_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_get_Hash_m7606_MethodInfo,
	&HMAC_HashCore_m11074_MethodInfo,
	&HMAC_HashFinal_m11075_MethodInfo,
	&HashAlgorithm_get_HashSize_m7607_MethodInfo,
	&HMAC_Initialize_m11076_MethodInfo,
	&HMAC_Dispose_m11073_MethodInfo,
	&HMAC_get_Key_m11069_MethodInfo,
	&HMAC_set_Key_m11070_MethodInfo,
};
static bool HMACSHA1_t1642_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HMACSHA1_t1642_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
	{ &ICryptoTransform_t1570_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACSHA1_t1642_0_0_0;
extern const Il2CppType HMACSHA1_t1642_1_0_0;
struct HMACSHA1_t1642;
const Il2CppTypeDefinitionMetadata HMACSHA1_t1642_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACSHA1_t1642_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t1643_0_0_0/* parent */
	, HMACSHA1_t1642_VTable/* vtableMethods */
	, HMACSHA1_t1642_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HMACSHA1_t1642_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACSHA1"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, HMACSHA1_t1642_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HMACSHA1_t1642_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 668/* custom_attributes_cache */
	, &HMACSHA1_t1642_0_0_0/* byval_arg */
	, &HMACSHA1_t1642_1_0_0/* this_arg */
	, &HMACSHA1_t1642_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACSHA1_t1642)/* instance_size */
	, sizeof (HMACSHA1_t1642)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACSHA256
#include "mscorlib_System_Security_Cryptography_HMACSHA256.h"
// Metadata Definition System.Security.Cryptography.HMACSHA256
extern TypeInfo HMACSHA256_t2077_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACSHA256
#include "mscorlib_System_Security_Cryptography_HMACSHA256MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACSHA256::.ctor()
extern const MethodInfo HMACSHA256__ctor_m11084_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HMACSHA256__ctor_m11084/* method */
	, &HMACSHA256_t2077_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo HMACSHA256_t2077_HMACSHA256__ctor_m11085_ParameterInfos[] = 
{
	{"key", 0, 134223092, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACSHA256::.ctor(System.Byte[])
extern const MethodInfo HMACSHA256__ctor_m11085_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HMACSHA256__ctor_m11085/* method */
	, &HMACSHA256_t2077_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, HMACSHA256_t2077_HMACSHA256__ctor_m11085_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HMACSHA256_t2077_MethodInfos[] =
{
	&HMACSHA256__ctor_m11084_MethodInfo,
	&HMACSHA256__ctor_m11085_MethodInfo,
	NULL
};
static const Il2CppMethodReference HMACSHA256_t2077_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&KeyedHashAlgorithm_Finalize_m7614_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&HashAlgorithm_System_IDisposable_Dispose_m7602_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_TransformBlock_m7604_MethodInfo,
	&HashAlgorithm_TransformFinalBlock_m7605_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_get_Hash_m7606_MethodInfo,
	&HMAC_HashCore_m11074_MethodInfo,
	&HMAC_HashFinal_m11075_MethodInfo,
	&HashAlgorithm_get_HashSize_m7607_MethodInfo,
	&HMAC_Initialize_m11076_MethodInfo,
	&HMAC_Dispose_m11073_MethodInfo,
	&HMAC_get_Key_m11069_MethodInfo,
	&HMAC_set_Key_m11070_MethodInfo,
};
static bool HMACSHA256_t2077_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HMACSHA256_t2077_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
	{ &ICryptoTransform_t1570_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACSHA256_t2077_0_0_0;
extern const Il2CppType HMACSHA256_t2077_1_0_0;
struct HMACSHA256_t2077;
const Il2CppTypeDefinitionMetadata HMACSHA256_t2077_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACSHA256_t2077_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t1643_0_0_0/* parent */
	, HMACSHA256_t2077_VTable/* vtableMethods */
	, HMACSHA256_t2077_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HMACSHA256_t2077_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACSHA256"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, HMACSHA256_t2077_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HMACSHA256_t2077_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 669/* custom_attributes_cache */
	, &HMACSHA256_t2077_0_0_0/* byval_arg */
	, &HMACSHA256_t2077_1_0_0/* this_arg */
	, &HMACSHA256_t2077_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACSHA256_t2077)/* instance_size */
	, sizeof (HMACSHA256_t2077)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACSHA384
#include "mscorlib_System_Security_Cryptography_HMACSHA384.h"
// Metadata Definition System.Security.Cryptography.HMACSHA384
extern TypeInfo HMACSHA384_t2078_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACSHA384
#include "mscorlib_System_Security_Cryptography_HMACSHA384MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACSHA384::.ctor()
extern const MethodInfo HMACSHA384__ctor_m11086_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HMACSHA384__ctor_m11086/* method */
	, &HMACSHA384_t2078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo HMACSHA384_t2078_HMACSHA384__ctor_m11087_ParameterInfos[] = 
{
	{"key", 0, 134223093, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACSHA384::.ctor(System.Byte[])
extern const MethodInfo HMACSHA384__ctor_m11087_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HMACSHA384__ctor_m11087/* method */
	, &HMACSHA384_t2078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, HMACSHA384_t2078_HMACSHA384__ctor_m11087_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACSHA384::.cctor()
extern const MethodInfo HMACSHA384__cctor_m11088_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&HMACSHA384__cctor_m11088/* method */
	, &HMACSHA384_t2078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo HMACSHA384_t2078_HMACSHA384_set_ProduceLegacyHmacValues_m11089_ParameterInfos[] = 
{
	{"value", 0, 134223094, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Security.Cryptography.HMACSHA384::set_ProduceLegacyHmacValues(System.Boolean)
extern const MethodInfo HMACSHA384_set_ProduceLegacyHmacValues_m11089_MethodInfo = 
{
	"set_ProduceLegacyHmacValues"/* name */
	, (methodPointerType)&HMACSHA384_set_ProduceLegacyHmacValues_m11089/* method */
	, &HMACSHA384_t2078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, HMACSHA384_t2078_HMACSHA384_set_ProduceLegacyHmacValues_m11089_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HMACSHA384_t2078_MethodInfos[] =
{
	&HMACSHA384__ctor_m11086_MethodInfo,
	&HMACSHA384__ctor_m11087_MethodInfo,
	&HMACSHA384__cctor_m11088_MethodInfo,
	&HMACSHA384_set_ProduceLegacyHmacValues_m11089_MethodInfo,
	NULL
};
extern const MethodInfo HMACSHA384_set_ProduceLegacyHmacValues_m11089_MethodInfo;
static const PropertyInfo HMACSHA384_t2078____ProduceLegacyHmacValues_PropertyInfo = 
{
	&HMACSHA384_t2078_il2cpp_TypeInfo/* parent */
	, "ProduceLegacyHmacValues"/* name */
	, NULL/* get */
	, &HMACSHA384_set_ProduceLegacyHmacValues_m11089_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* HMACSHA384_t2078_PropertyInfos[] =
{
	&HMACSHA384_t2078____ProduceLegacyHmacValues_PropertyInfo,
	NULL
};
static const Il2CppMethodReference HMACSHA384_t2078_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&KeyedHashAlgorithm_Finalize_m7614_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&HashAlgorithm_System_IDisposable_Dispose_m7602_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_TransformBlock_m7604_MethodInfo,
	&HashAlgorithm_TransformFinalBlock_m7605_MethodInfo,
	&HashAlgorithm_get_CanReuseTransform_m7603_MethodInfo,
	&HashAlgorithm_get_Hash_m7606_MethodInfo,
	&HMAC_HashCore_m11074_MethodInfo,
	&HMAC_HashFinal_m11075_MethodInfo,
	&HashAlgorithm_get_HashSize_m7607_MethodInfo,
	&HMAC_Initialize_m11076_MethodInfo,
	&HMAC_Dispose_m11073_MethodInfo,
	&HMAC_get_Key_m11069_MethodInfo,
	&HMAC_set_Key_m11070_MethodInfo,
};
static bool HMACSHA384_t2078_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HMACSHA384_t2078_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
	{ &ICryptoTransform_t1570_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACSHA384_t2078_0_0_0;
extern const Il2CppType HMACSHA384_t2078_1_0_0;
struct HMACSHA384_t2078;
const Il2CppTypeDefinitionMetadata HMACSHA384_t2078_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACSHA384_t2078_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t1643_0_0_0/* parent */
	, HMACSHA384_t2078_VTable/* vtableMethods */
	, HMACSHA384_t2078_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2366/* fieldStart */

};
TypeInfo HMACSHA384_t2078_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACSHA384"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, HMACSHA384_t2078_MethodInfos/* methods */
	, HMACSHA384_t2078_PropertyInfos/* properties */
	, NULL/* events */
	, &HMACSHA384_t2078_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 670/* custom_attributes_cache */
	, &HMACSHA384_t2078_0_0_0/* byval_arg */
	, &HMACSHA384_t2078_1_0_0/* this_arg */
	, &HMACSHA384_t2078_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACSHA384_t2078)/* instance_size */
	, sizeof (HMACSHA384_t2078)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(HMACSHA384_t2078_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
