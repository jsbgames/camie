﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct ButtonHandler_t31;

// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::.ctor()
extern "C" void ButtonHandler__ctor_m61 (ButtonHandler_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::OnEnable()
extern "C" void ButtonHandler_OnEnable_m62 (ButtonHandler_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetDownState()
extern "C" void ButtonHandler_SetDownState_m63 (ButtonHandler_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetUpState()
extern "C" void ButtonHandler_SetUpState_m64 (ButtonHandler_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisPositiveState()
extern "C" void ButtonHandler_SetAxisPositiveState_m65 (ButtonHandler_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNeutralState()
extern "C" void ButtonHandler_SetAxisNeutralState_m66 (ButtonHandler_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNegativeState()
extern "C" void ButtonHandler_SetAxisNegativeState_m67 (ButtonHandler_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::Update()
extern "C" void ButtonHandler_Update_m68 (ButtonHandler_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
