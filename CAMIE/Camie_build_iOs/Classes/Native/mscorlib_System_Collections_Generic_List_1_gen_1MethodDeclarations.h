﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t251;
// System.Object
struct Object_t;
// UnityEngine.Material
struct Material_t55;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Material>
struct IEnumerable_1_t3534;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Material>
struct IEnumerator_1_t3535;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<UnityEngine.Material>
struct ICollection_1_t3536;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Material>
struct ReadOnlyCollection_1_t2875;
// UnityEngine.Material[]
struct MaterialU5BU5D_t252;
// System.Predicate`1<UnityEngine.Material>
struct Predicate_1_t2876;
// System.Comparison`1<UnityEngine.Material>
struct Comparison_1_t2878;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m955(__this, method) (( void (*) (List_1_t251 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m14273(__this, ___collection, method) (( void (*) (List_1_t251 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::.ctor(System.Int32)
#define List_1__ctor_m14274(__this, ___capacity, method) (( void (*) (List_1_t251 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::.cctor()
#define List_1__cctor_m14275(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14276(__this, method) (( Object_t* (*) (List_1_t251 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m14277(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t251 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m14278(__this, method) (( Object_t * (*) (List_1_t251 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m14279(__this, ___item, method) (( int32_t (*) (List_1_t251 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m14280(__this, ___item, method) (( bool (*) (List_1_t251 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m14281(__this, ___item, method) (( int32_t (*) (List_1_t251 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m14282(__this, ___index, ___item, method) (( void (*) (List_1_t251 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m14283(__this, ___item, method) (( void (*) (List_1_t251 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14284(__this, method) (( bool (*) (List_1_t251 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14285(__this, method) (( bool (*) (List_1_t251 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m14286(__this, method) (( Object_t * (*) (List_1_t251 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m14287(__this, method) (( bool (*) (List_1_t251 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m14288(__this, method) (( bool (*) (List_1_t251 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m14289(__this, ___index, method) (( Object_t * (*) (List_1_t251 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m14290(__this, ___index, ___value, method) (( void (*) (List_1_t251 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::Add(T)
#define List_1_Add_m14291(__this, ___item, method) (( void (*) (List_1_t251 *, Material_t55 *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m14292(__this, ___newCount, method) (( void (*) (List_1_t251 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m14293(__this, ___collection, method) (( void (*) (List_1_t251 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m14294(__this, ___enumerable, method) (( void (*) (List_1_t251 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m14295(__this, ___collection, method) (( void (*) (List_1_t251 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Material>::AsReadOnly()
#define List_1_AsReadOnly_m14296(__this, method) (( ReadOnlyCollection_1_t2875 * (*) (List_1_t251 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::Clear()
#define List_1_Clear_m14297(__this, method) (( void (*) (List_1_t251 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Material>::Contains(T)
#define List_1_Contains_m14298(__this, ___item, method) (( bool (*) (List_1_t251 *, Material_t55 *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m14299(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t251 *, MaterialU5BU5D_t252*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Material>::Find(System.Predicate`1<T>)
#define List_1_Find_m14300(__this, ___match, method) (( Material_t55 * (*) (List_1_t251 *, Predicate_1_t2876 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m14301(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2876 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m14302(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t251 *, int32_t, int32_t, Predicate_1_t2876 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Material>::GetEnumerator()
#define List_1_GetEnumerator_m14303(__this, method) (( Enumerator_t2877  (*) (List_1_t251 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::IndexOf(T)
#define List_1_IndexOf_m14304(__this, ___item, method) (( int32_t (*) (List_1_t251 *, Material_t55 *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m14305(__this, ___start, ___delta, method) (( void (*) (List_1_t251 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m14306(__this, ___index, method) (( void (*) (List_1_t251 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::Insert(System.Int32,T)
#define List_1_Insert_m14307(__this, ___index, ___item, method) (( void (*) (List_1_t251 *, int32_t, Material_t55 *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m14308(__this, ___collection, method) (( void (*) (List_1_t251 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Material>::Remove(T)
#define List_1_Remove_m14309(__this, ___item, method) (( bool (*) (List_1_t251 *, Material_t55 *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m14310(__this, ___match, method) (( int32_t (*) (List_1_t251 *, Predicate_1_t2876 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m14311(__this, ___index, method) (( void (*) (List_1_t251 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::Reverse()
#define List_1_Reverse_m14312(__this, method) (( void (*) (List_1_t251 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::Sort()
#define List_1_Sort_m14313(__this, method) (( void (*) (List_1_t251 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m14314(__this, ___comparison, method) (( void (*) (List_1_t251 *, Comparison_1_t2878 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Material>::ToArray()
#define List_1_ToArray_m14315(__this, method) (( MaterialU5BU5D_t252* (*) (List_1_t251 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::TrimExcess()
#define List_1_TrimExcess_m14316(__this, method) (( void (*) (List_1_t251 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::get_Capacity()
#define List_1_get_Capacity_m14317(__this, method) (( int32_t (*) (List_1_t251 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m14318(__this, ___value, method) (( void (*) (List_1_t251 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Material>::get_Count()
#define List_1_get_Count_m14319(__this, method) (( int32_t (*) (List_1_t251 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Material>::get_Item(System.Int32)
#define List_1_get_Item_m14320(__this, ___index, method) (( Material_t55 * (*) (List_1_t251 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Material>::set_Item(System.Int32,T)
#define List_1_set_Item_m14321(__this, ___index, ___value, method) (( void (*) (List_1_t251 *, int32_t, Material_t55 *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
