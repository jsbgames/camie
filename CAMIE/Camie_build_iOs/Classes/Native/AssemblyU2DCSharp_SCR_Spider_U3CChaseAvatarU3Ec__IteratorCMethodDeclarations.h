﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Spider/<ChaseAvatar>c__IteratorC
struct U3CChaseAvatarU3Ec__IteratorC_t396;
// System.Object
struct Object_t;

// System.Void SCR_Spider/<ChaseAvatar>c__IteratorC::.ctor()
extern "C" void U3CChaseAvatarU3Ec__IteratorC__ctor_m1545 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Spider/<ChaseAvatar>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CChaseAvatarU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1546 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Spider/<ChaseAvatar>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CChaseAvatarU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1547 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Spider/<ChaseAvatar>c__IteratorC::MoveNext()
extern "C" bool U3CChaseAvatarU3Ec__IteratorC_MoveNext_m1548 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Spider/<ChaseAvatar>c__IteratorC::Dispose()
extern "C" void U3CChaseAvatarU3Ec__IteratorC_Dispose_m1549 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Spider/<ChaseAvatar>c__IteratorC::Reset()
extern "C" void U3CChaseAvatarU3Ec__IteratorC_Reset_m1550 (U3CChaseAvatarU3Ec__IteratorC_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
