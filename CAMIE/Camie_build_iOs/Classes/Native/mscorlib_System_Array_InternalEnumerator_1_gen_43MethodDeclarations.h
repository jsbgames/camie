﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>
struct InternalEnumerator_1_t3013;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16257_gshared (InternalEnumerator_1_t3013 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m16257(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3013 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m16257_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16258_gshared (InternalEnumerator_1_t3013 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16258(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3013 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16258_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16259_gshared (InternalEnumerator_1_t3013 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16259(__this, method) (( void (*) (InternalEnumerator_1_t3013 *, const MethodInfo*))InternalEnumerator_1_Dispose_m16259_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16260_gshared (InternalEnumerator_1_t3013 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16260(__this, method) (( bool (*) (InternalEnumerator_1_t3013 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m16260_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.AnimatorClipInfo>::get_Current()
extern "C" AnimatorClipInfo_t888  InternalEnumerator_1_get_Current_m16261_gshared (InternalEnumerator_1_t3013 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16261(__this, method) (( AnimatorClipInfo_t888  (*) (InternalEnumerator_1_t3013 *, const MethodInfo*))InternalEnumerator_1_get_Current_m16261_gshared)(__this, method)
