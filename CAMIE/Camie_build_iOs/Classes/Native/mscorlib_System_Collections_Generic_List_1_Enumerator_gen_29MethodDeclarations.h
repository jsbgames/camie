﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Byte[]>
struct Enumerator_t3218;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t850;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t849;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m19359(__this, ___l, method) (( void (*) (Enumerator_t3218 *, List_1_t849 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Byte[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19360(__this, method) (( Object_t * (*) (Enumerator_t3218 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::Dispose()
#define Enumerator_Dispose_m19361(__this, method) (( void (*) (Enumerator_t3218 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::VerifyState()
#define Enumerator_VerifyState_m19362(__this, method) (( void (*) (Enumerator_t3218 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Byte[]>::MoveNext()
#define Enumerator_MoveNext_m19363(__this, method) (( bool (*) (Enumerator_t3218 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Byte[]>::get_Current()
#define Enumerator_get_Current_m19364(__this, method) (( ByteU5BU5D_t850* (*) (Enumerator_t3218 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
