﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t1271;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.PatternCompiler
struct  PatternCompiler_t1381  : public Object_t
{
	// System.Collections.ArrayList System.Text.RegularExpressions.PatternCompiler::pgm
	ArrayList_t1271 * ___pgm_0;
};
