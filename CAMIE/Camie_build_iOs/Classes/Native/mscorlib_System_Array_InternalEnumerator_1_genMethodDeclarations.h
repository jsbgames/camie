﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Collider2D>
struct InternalEnumerator_1_t2802;
// System.Object
struct Object_t;
// UnityEngine.Collider2D
struct Collider2D_t214;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider2D>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m13512(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2802 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Collider2D>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13514(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2802 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider2D>::Dispose()
#define InternalEnumerator_1_Dispose_m13516(__this, method) (( void (*) (InternalEnumerator_1_t2802 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Collider2D>::MoveNext()
#define InternalEnumerator_1_MoveNext_m13518(__this, method) (( bool (*) (InternalEnumerator_1_t2802 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Collider2D>::get_Current()
#define InternalEnumerator_1_get_Current_m13520(__this, method) (( Collider2D_t214 * (*) (InternalEnumerator_1_t2802 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
