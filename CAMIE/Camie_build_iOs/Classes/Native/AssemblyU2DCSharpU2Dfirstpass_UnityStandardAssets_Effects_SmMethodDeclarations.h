﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.SmokeParticles
struct SmokeParticles_t158;

// System.Void UnityStandardAssets.Effects.SmokeParticles::.ctor()
extern "C" void SmokeParticles__ctor_m436 (SmokeParticles_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.SmokeParticles::Start()
extern "C" void SmokeParticles_Start_m437 (SmokeParticles_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
