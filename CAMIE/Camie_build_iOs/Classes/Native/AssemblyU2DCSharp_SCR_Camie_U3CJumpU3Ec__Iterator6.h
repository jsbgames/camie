﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// SCR_Camie
struct SCR_Camie_t338;
// System.Object
#include "mscorlib_System_Object.h"
// SCR_Camie/<Jump>c__Iterator6
struct  U3CJumpU3Ec__Iterator6_t341  : public Object_t
{
	// System.Single SCR_Camie/<Jump>c__Iterator6::<t>__0
	float ___U3CtU3E__0_0;
	// System.Int32 SCR_Camie/<Jump>c__Iterator6::$PC
	int32_t ___U24PC_1;
	// System.Object SCR_Camie/<Jump>c__Iterator6::$current
	Object_t * ___U24current_2;
	// SCR_Camie SCR_Camie/<Jump>c__Iterator6::<>f__this
	SCR_Camie_t338 * ___U3CU3Ef__this_3;
};
