﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1323;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t1334;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrors.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Net.Security.RemoteCertificateValidationCallback
struct  RemoteCertificateValidationCallback_t1288  : public MulticastDelegate_t549
{
};
