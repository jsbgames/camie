﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_ResetIdle
struct SCR_ResetIdle_t350;

// System.Void SCR_ResetIdle::.ctor()
extern "C" void SCR_ResetIdle__ctor_m1274 (SCR_ResetIdle_t350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ResetIdle::Start()
extern "C" void SCR_ResetIdle_Start_m1275 (SCR_ResetIdle_t350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ResetIdle::ResetIdle()
extern "C" void SCR_ResetIdle_ResetIdle_m1276 (SCR_ResetIdle_t350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ResetIdle::Land()
extern "C" void SCR_ResetIdle_Land_m1277 (SCR_ResetIdle_t350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_ResetIdle::OutOfBubble()
extern "C" void SCR_ResetIdle_OutOfBubble_m1278 (SCR_ResetIdle_t350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
