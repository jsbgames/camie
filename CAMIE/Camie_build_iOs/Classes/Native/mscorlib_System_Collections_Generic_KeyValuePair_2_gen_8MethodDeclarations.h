﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>
struct KeyValuePair_2_t2890;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2814;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m14529(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2890 *, Object_t *, Dictionary_2_t2814 *, const MethodInfo*))KeyValuePair_2__ctor_m13651_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_Key()
#define KeyValuePair_2_get_Key_m14530(__this, method) (( Object_t * (*) (KeyValuePair_2_t2890 *, const MethodInfo*))KeyValuePair_2_get_Key_m13652_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m14531(__this, ___value, method) (( void (*) (KeyValuePair_2_t2890 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m13653_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_Value()
#define KeyValuePair_2_get_Value_m14532(__this, method) (( Dictionary_2_t2814 * (*) (KeyValuePair_2_t2890 *, const MethodInfo*))KeyValuePair_2_get_Value_m13654_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m14533(__this, ___value, method) (( void (*) (KeyValuePair_2_t2890 *, Dictionary_2_t2814 *, const MethodInfo*))KeyValuePair_2_set_Value_m13655_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::ToString()
#define KeyValuePair_2_ToString_m14534(__this, method) (( String_t* (*) (KeyValuePair_2_t2890 *, const MethodInfo*))KeyValuePair_2_ToString_m13656_gshared)(__this, method)
