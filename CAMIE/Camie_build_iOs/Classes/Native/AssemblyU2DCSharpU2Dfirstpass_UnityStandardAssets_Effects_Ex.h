﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t1;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t135;
// UnityEngine.Collider
struct Collider_t138;
// System.Object
struct Object_t;
// UnityStandardAssets.Effects.ExplosionFireAndDebris
struct ExplosionFireAndDebris_t139;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t140  : public Object_t
{
	// System.Single UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<multiplier>__0
	float ___U3CmultiplierU3E__0_0;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<n>__1
	int32_t ___U3CnU3E__1_1;
	// UnityEngine.Transform UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<prefab>__2
	Transform_t1 * ___U3CprefabU3E__2_2;
	// UnityEngine.Vector3 UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<pos>__3
	Vector3_t4  ___U3CposU3E__3_3;
	// UnityEngine.Quaternion UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<rot>__4
	Quaternion_t19  ___U3CrotU3E__4_4;
	// System.Single UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<r>__5
	float ___U3CrU3E__5_5;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<cols>__6
	ColliderU5BU5D_t135* ___U3CcolsU3E__6_6;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<$s_8>__7
	ColliderU5BU5D_t135* ___U3CU24s_8U3E__7_7;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<$s_9>__8
	int32_t ___U3CU24s_9U3E__8_8;
	// UnityEngine.Collider UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<col>__9
	Collider_t138 * ___U3CcolU3E__9_9;
	// UnityEngine.Ray UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<fireRay>__10
	Ray_t26  ___U3CfireRayU3E__10_10;
	// UnityEngine.RaycastHit UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<fireHit>__11
	RaycastHit_t24  ___U3CfireHitU3E__11_11;
	// System.Single UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<testR>__12
	float ___U3CtestRU3E__12_12;
	// UnityEngine.Ray UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<fireRay>__13
	Ray_t26  ___U3CfireRayU3E__13_13;
	// UnityEngine.RaycastHit UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<fireHit>__14
	RaycastHit_t24  ___U3CfireHitU3E__14_14;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::$PC
	int32_t ___U24PC_15;
	// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::$current
	Object_t * ___U24current_16;
	// UnityStandardAssets.Effects.ExplosionFireAndDebris UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>c__Iterator0::<>f__this
	ExplosionFireAndDebris_t139 * ___U3CU3Ef__this_17;
};
