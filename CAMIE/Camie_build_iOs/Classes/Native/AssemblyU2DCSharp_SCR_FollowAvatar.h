﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t355;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_FollowAvatar
struct  SCR_FollowAvatar_t352  : public MonoBehaviour_t3
{
	// System.Single SCR_FollowAvatar::followSpeed
	float ___followSpeed_2;
	// System.Single SCR_FollowAvatar::zoomInSpeed
	float ___zoomInSpeed_3;
	// System.Single SCR_FollowAvatar::distanceDuFrame
	float ___distanceDuFrame_4;
	// System.Single SCR_FollowAvatar::plusHautQueCamie
	float ___plusHautQueCamie_5;
	// System.Single SCR_FollowAvatar::minX
	float ___minX_6;
	// System.Single SCR_FollowAvatar::maxX
	float ___maxX_7;
	// System.Single SCR_FollowAvatar::minY
	float ___minY_8;
	// System.Single SCR_FollowAvatar::maxY
	float ___maxY_9;
};
struct SCR_FollowAvatar_t352_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> SCR_FollowAvatar::<>f__switch$map0
	Dictionary_2_t355 * ___U3CU3Ef__switchU24map0_10;
};
