﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode
#include "AssemblyU2DCSharp_UnityStandardAssets_SceneUtils_ParticleSce_0.h"
// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode
struct  AlignMode_t320 
{
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode::value__
	int32_t ___value___1;
};
