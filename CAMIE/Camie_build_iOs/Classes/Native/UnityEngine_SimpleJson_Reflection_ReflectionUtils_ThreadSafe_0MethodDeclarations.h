﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionary_2_t1076;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t3771;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ICollection_1_t3772;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t939;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionaryValueFactory_2_t1075;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct KeyValuePair_2U5BU5D_t3773;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>>
struct IEnumerator_1_t3774;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_6MethodDeclarations.h"
#define ThreadSafeDictionary_2__ctor_m5196(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t1076 *, ThreadSafeDictionaryValueFactory_2_t1075 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m20682_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m20683(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t1076 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m20684_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m20685(__this, ___key, method) (( ConstructorDelegate_t939 * (*) (ThreadSafeDictionary_2_t1076 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m20686_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m20687(__this, ___key, method) (( ConstructorDelegate_t939 * (*) (ThreadSafeDictionary_2_t1076 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m20688_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m20689(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t1076 *, Type_t *, ConstructorDelegate_t939 *, const MethodInfo*))ThreadSafeDictionary_2_Add_m20690_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m20691(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1076 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m20692_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(TKey)
#define ThreadSafeDictionary_2_Remove_m20693(__this, ___key, method) (( bool (*) (ThreadSafeDictionary_2_t1076 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m20694_gshared)(__this, ___key, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m20695(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t1076 *, Type_t *, ConstructorDelegate_t939 **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m20696_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m20697(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1076 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m20698_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m20699(__this, ___key, method) (( ConstructorDelegate_t939 * (*) (ThreadSafeDictionary_2_t1076 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m20700_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m20701(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t1076 *, Type_t *, ConstructorDelegate_t939 *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m20702_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m20703(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t1076 *, KeyValuePair_2_t3322 , const MethodInfo*))ThreadSafeDictionary_2_Add_m20704_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Clear()
#define ThreadSafeDictionary_2_Clear_m20705(__this, method) (( void (*) (ThreadSafeDictionary_2_t1076 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m20706_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m20707(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t1076 *, KeyValuePair_2_t3322 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m20708_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m20709(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t1076 *, KeyValuePair_2U5BU5D_t3773*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m20710_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m20711(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t1076 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m20712_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m20713(__this, method) (( bool (*) (ThreadSafeDictionary_2_t1076 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m20714_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m20715(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t1076 *, KeyValuePair_2_t3322 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m20716_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m20717(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1076 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m20718_gshared)(__this, method)
