﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>
struct Dictionary_2_t389;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_SoundManager
struct  SCR_SoundManager_t335  : public MonoBehaviour_t3
{
	// System.Boolean SCR_SoundManager::soundOn
	bool ___soundOn_2;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource> SCR_SoundManager::sources
	Dictionary_2_t389 * ___sources_3;
};
