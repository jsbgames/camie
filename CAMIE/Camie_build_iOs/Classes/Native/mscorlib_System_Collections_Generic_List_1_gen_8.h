﻿#pragma once
#include <stdint.h>
// SCR_Menu[]
struct SCR_MenuU5BU5D_t3004;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<SCR_Menu>
struct  List_1_t377  : public Object_t
{
	// T[] System.Collections.Generic.List`1<SCR_Menu>::_items
	SCR_MenuU5BU5D_t3004* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<SCR_Menu>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<SCR_Menu>::_version
	int32_t ____version_3;
};
struct List_1_t377_StaticFields{
	// T[] System.Collections.Generic.List`1<SCR_Menu>::EmptyArray
	SCR_MenuU5BU5D_t3004* ___EmptyArray_4;
};
