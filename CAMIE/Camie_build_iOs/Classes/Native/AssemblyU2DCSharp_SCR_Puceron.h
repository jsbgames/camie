﻿#pragma once
#include <stdint.h>
// UnityEngine.BoxCollider
struct BoxCollider_t343;
// SCR_Collectible
#include "AssemblyU2DCSharp_SCR_Collectible.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// SCR_Puceron
struct  SCR_Puceron_t356  : public SCR_Collectible_t331
{
	// UnityEngine.Vector3 SCR_Puceron::originalPosition
	Vector3_t4  ___originalPosition_4;
	// UnityEngine.Quaternion SCR_Puceron::originalRotation
	Quaternion_t19  ___originalRotation_5;
	// UnityEngine.BoxCollider SCR_Puceron::trigger
	BoxCollider_t343 * ___trigger_6;
};
