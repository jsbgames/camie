﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Single>
struct InvokableCall_1_t3140;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t693;
// System.Object[]
struct ObjectU5BU5D_t224;

// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m18252_gshared (InvokableCall_1_t3140 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m18252(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t3140 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m18252_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m18253_gshared (InvokableCall_1_t3140 * __this, UnityAction_1_t693 * ___callback, const MethodInfo* method);
#define InvokableCall_1__ctor_m18253(__this, ___callback, method) (( void (*) (InvokableCall_1_t3140 *, UnityAction_1_t693 *, const MethodInfo*))InvokableCall_1__ctor_m18253_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m18254_gshared (InvokableCall_1_t3140 * __this, ObjectU5BU5D_t224* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m18254(__this, ___args, method) (( void (*) (InvokableCall_1_t3140 *, ObjectU5BU5D_t224*, const MethodInfo*))InvokableCall_1_Invoke_m18254_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Single>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m18255_gshared (InvokableCall_1_t3140 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m18255(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t3140 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m18255_gshared)(__this, ___targetObj, ___method, method)
