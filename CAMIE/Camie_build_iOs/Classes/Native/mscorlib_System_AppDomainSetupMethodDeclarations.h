﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AppDomainSetup
struct AppDomainSetup_t2185;

// System.Void System.AppDomainSetup::.ctor()
extern "C" void AppDomainSetup__ctor_m11794 (AppDomainSetup_t2185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
