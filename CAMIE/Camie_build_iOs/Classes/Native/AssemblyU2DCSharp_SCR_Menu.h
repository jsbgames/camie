﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform[]
struct TransformU5BU5D_t141;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t334;
// SCR_SoundManager
struct SCR_SoundManager_t335;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_Menu
struct  SCR_Menu_t336  : public MonoBehaviour_t3
{
	// UnityEngine.Transform[] SCR_Menu::children
	TransformU5BU5D_t141* ___children_2;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> SCR_Menu::menuItems
	Dictionary_2_t334 * ___menuItems_3;
	// SCR_SoundManager SCR_Menu::sound
	SCR_SoundManager_t335 * ___sound_4;
};
