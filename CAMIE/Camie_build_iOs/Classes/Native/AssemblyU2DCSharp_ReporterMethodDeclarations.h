﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Reporter
struct Reporter_t295;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"

// System.Void Reporter::.ctor()
extern "C" void Reporter__ctor_m1091 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::.cctor()
extern "C" void Reporter__cctor_m1092 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Reporter::get_TotalMemUsage()
extern "C" float Reporter_get_TotalMemUsage_m1093 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::Awake()
extern "C" void Reporter_Awake_m1094 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::OnEnable()
extern "C" void Reporter_OnEnable_m1095 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::OnDisable()
extern "C" void Reporter_OnDisable_m1096 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::addSample()
extern "C" void Reporter_addSample_m1097 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::Initialize()
extern "C" void Reporter_Initialize_m1098 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::initializeStyle()
extern "C" void Reporter_initializeStyle_m1099 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::Start()
extern "C" void Reporter_Start_m1100 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::clear()
extern "C" void Reporter_clear_m1101 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::calculateCurrentLog()
extern "C" void Reporter_calculateCurrentLog_m1102 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::DrawInfo()
extern "C" void Reporter_DrawInfo_m1103 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::drawInfo_enableDisableToolBarButtons()
extern "C" void Reporter_drawInfo_enableDisableToolBarButtons_m1104 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::DrawReport()
extern "C" void Reporter_DrawReport_m1105 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::drawToolBar()
extern "C" void Reporter_drawToolBar_m1106 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::DrawLogs()
extern "C" void Reporter_DrawLogs_m1107 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::drawGraph()
extern "C" void Reporter_drawGraph_m1108 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::drawStack()
extern "C" void Reporter_drawStack_m1109 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::OnGUIDraw()
extern "C" void Reporter_OnGUIDraw_m1110 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Reporter::isGestureDone()
extern "C" bool Reporter_isGestureDone_m1111 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Reporter::isDoubleClickDone()
extern "C" bool Reporter_isDoubleClickDone_m1112 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Reporter::getDownPos()
extern "C" Vector2_t6  Reporter_getDownPos_m1113 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Reporter::getDrag()
extern "C" Vector2_t6  Reporter_getDrag_m1114 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::calculateStartIndex()
extern "C" void Reporter_calculateStartIndex_m1115 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::doShow()
extern "C" void Reporter_doShow_m1116 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::Update()
extern "C" void Reporter_Update_m1117 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::CaptureLog(System.String,System.String,UnityEngine.LogType)
extern "C" void Reporter_CaptureLog_m1118 (Reporter_t295 * __this, String_t* ___condition, String_t* ___stacktrace, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::AddLog(System.String,System.String,UnityEngine.LogType)
extern "C" void Reporter_AddLog_m1119 (Reporter_t295 * __this, String_t* ___condition, String_t* ___stacktrace, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::CaptureLogThread(System.String,System.String,UnityEngine.LogType)
extern "C" void Reporter_CaptureLogThread_m1120 (Reporter_t295 * __this, String_t* ___condition, String_t* ___stacktrace, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::OnLevelWasLoaded()
extern "C" void Reporter_OnLevelWasLoaded_m1121 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reporter::OnApplicationQuit()
extern "C" void Reporter_OnApplicationQuit_m1122 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Reporter::readInfo()
extern "C" Object_t * Reporter_readInfo_m1123 (Reporter_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
