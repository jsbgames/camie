﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t850;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<System.Byte[]>
struct  Comparison_1_t3219  : public MulticastDelegate_t549
{
};
