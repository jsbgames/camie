﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Data
struct Data_t388;

// System.Void Data::.ctor()
extern "C" void Data__ctor_m1488 (Data_t388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
