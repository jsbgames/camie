﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2952;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1033;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3581;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>
struct KeyCollection_t2955;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int32>
struct ValueCollection_t2959;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2812;
// System.Collections.Generic.IDictionary`2<System.Object,System.Int32>
struct IDictionary_2_t3585;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1043;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3586;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t3587;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1429;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m15465_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m15465(__this, method) (( void (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2__ctor_m15465_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m15467_gshared (Dictionary_2_t2952 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m15467(__this, ___comparer, method) (( void (*) (Dictionary_2_t2952 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15467_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m15469_gshared (Dictionary_2_t2952 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m15469(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2952 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15469_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m15470_gshared (Dictionary_2_t2952 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m15470(__this, ___capacity, method) (( void (*) (Dictionary_2_t2952 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m15470_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m15472_gshared (Dictionary_2_t2952 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m15472(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2952 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15472_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m15474_gshared (Dictionary_2_t2952 * __this, SerializationInfo_t1043 * ___info, StreamingContext_t1044  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m15474(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2952 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2__ctor_m15474_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15476_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15476(__this, method) (( Object_t* (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15476_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15478_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15478(__this, method) (( Object_t* (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15478_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m15480_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m15480(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2952 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m15480_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m15482_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m15482(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2952 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m15482_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m15484_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m15484(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2952 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m15484_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m15486_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m15486(__this, ___key, method) (( bool (*) (Dictionary_2_t2952 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m15486_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m15488_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m15488(__this, ___key, method) (( void (*) (Dictionary_2_t2952 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m15488_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15490_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15490(__this, method) (( bool (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15490_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15492_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15492(__this, method) (( Object_t * (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15492_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15494_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15494(__this, method) (( bool (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15494_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15496_gshared (Dictionary_2_t2952 * __this, KeyValuePair_2_t2953  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15496(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2952 *, KeyValuePair_2_t2953 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15496_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15498_gshared (Dictionary_2_t2952 * __this, KeyValuePair_2_t2953  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15498(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2952 *, KeyValuePair_2_t2953 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15498_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15500_gshared (Dictionary_2_t2952 * __this, KeyValuePair_2U5BU5D_t3586* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15500(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2952 *, KeyValuePair_2U5BU5D_t3586*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15500_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15502_gshared (Dictionary_2_t2952 * __this, KeyValuePair_2_t2953  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15502(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2952 *, KeyValuePair_2_t2953 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15502_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m15504_gshared (Dictionary_2_t2952 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m15504(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2952 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m15504_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15506_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15506(__this, method) (( Object_t * (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15506_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15508_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15508(__this, method) (( Object_t* (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15508_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15510_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15510(__this, method) (( Object_t * (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15510_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m15512_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m15512(__this, method) (( int32_t (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_get_Count_m15512_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m15514_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m15514(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2952 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m15514_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m15516_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m15516(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2952 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m15516_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m15518_gshared (Dictionary_2_t2952 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m15518(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2952 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m15518_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m15520_gshared (Dictionary_2_t2952 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m15520(__this, ___size, method) (( void (*) (Dictionary_2_t2952 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m15520_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m15522_gshared (Dictionary_2_t2952 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m15522(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2952 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m15522_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2953  Dictionary_2_make_pair_m15524_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m15524(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2953  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m15524_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m15526_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m15526(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m15526_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m15528_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m15528(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m15528_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m15530_gshared (Dictionary_2_t2952 * __this, KeyValuePair_2U5BU5D_t3586* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m15530(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2952 *, KeyValuePair_2U5BU5D_t3586*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m15530_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m15532_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m15532(__this, method) (( void (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_Resize_m15532_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m15534_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m15534(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2952 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m15534_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m15536_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m15536(__this, method) (( void (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_Clear_m15536_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m15538_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m15538(__this, ___key, method) (( bool (*) (Dictionary_2_t2952 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m15538_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m15540_gshared (Dictionary_2_t2952 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m15540(__this, ___value, method) (( bool (*) (Dictionary_2_t2952 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m15540_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m15542_gshared (Dictionary_2_t2952 * __this, SerializationInfo_t1043 * ___info, StreamingContext_t1044  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m15542(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2952 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2_GetObjectData_m15542_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m15544_gshared (Dictionary_2_t2952 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m15544(__this, ___sender, method) (( void (*) (Dictionary_2_t2952 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m15544_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m15546_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m15546(__this, ___key, method) (( bool (*) (Dictionary_2_t2952 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m15546_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m15548_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m15548(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2952 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m15548_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Keys()
extern "C" KeyCollection_t2955 * Dictionary_2_get_Keys_m15550_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m15550(__this, method) (( KeyCollection_t2955 * (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_get_Keys_m15550_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Values()
extern "C" ValueCollection_t2959 * Dictionary_2_get_Values_m15552_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m15552(__this, method) (( ValueCollection_t2959 * (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_get_Values_m15552_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m15554_gshared (Dictionary_2_t2952 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m15554(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2952 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m15554_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m15556_gshared (Dictionary_2_t2952 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m15556(__this, ___value, method) (( int32_t (*) (Dictionary_2_t2952 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m15556_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m15558_gshared (Dictionary_2_t2952 * __this, KeyValuePair_2_t2953  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m15558(__this, ___pair, method) (( bool (*) (Dictionary_2_t2952 *, KeyValuePair_2_t2953 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m15558_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetEnumerator()
extern "C" Enumerator_t2957  Dictionary_2_GetEnumerator_m15560_gshared (Dictionary_2_t2952 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m15560(__this, method) (( Enumerator_t2957  (*) (Dictionary_2_t2952 *, const MethodInfo*))Dictionary_2_GetEnumerator_m15560_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1430  Dictionary_2_U3CCopyToU3Em__0_m15562_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m15562(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1430  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m15562_gshared)(__this /* static, unused */, ___key, ___value, method)
