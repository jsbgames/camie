﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t2316;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m12641_gshared (GenericEqualityComparer_1_t2316 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m12641(__this, method) (( void (*) (GenericEqualityComparer_1_t2316 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m12641_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m22497_gshared (GenericEqualityComparer_1_t2316 * __this, DateTimeOffset_t1086  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m22497(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2316 *, DateTimeOffset_t1086 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m22497_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m22498_gshared (GenericEqualityComparer_1_t2316 * __this, DateTimeOffset_t1086  ___x, DateTimeOffset_t1086  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m22498(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2316 *, DateTimeOffset_t1086 , DateTimeOffset_t1086 , const MethodInfo*))GenericEqualityComparer_1_Equals_m22498_gshared)(__this, ___x, ___y, method)
