﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Enumerator_t2835;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_t30;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Dictionary_2_t50;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"
#define Enumerator__ctor_m13796(__this, ___dictionary, method) (( void (*) (Enumerator_t2835 *, Dictionary_2_t50 *, const MethodInfo*))Enumerator__ctor_m13691_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m13797(__this, method) (( Object_t * (*) (Enumerator_t2835 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13692_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13798(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t2835 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13693_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13799(__this, method) (( Object_t * (*) (Enumerator_t2835 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13694_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13800(__this, method) (( Object_t * (*) (Enumerator_t2835 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13695_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::MoveNext()
#define Enumerator_MoveNext_m13801(__this, method) (( bool (*) (Enumerator_t2835 *, const MethodInfo*))Enumerator_MoveNext_m13696_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Current()
#define Enumerator_get_Current_m13802(__this, method) (( KeyValuePair_2_t2832  (*) (Enumerator_t2835 *, const MethodInfo*))Enumerator_get_Current_m13697_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m13803(__this, method) (( String_t* (*) (Enumerator_t2835 *, const MethodInfo*))Enumerator_get_CurrentKey_m13698_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m13804(__this, method) (( VirtualAxis_t30 * (*) (Enumerator_t2835 *, const MethodInfo*))Enumerator_get_CurrentValue_m13699_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::VerifyState()
#define Enumerator_VerifyState_m13805(__this, method) (( void (*) (Enumerator_t2835 *, const MethodInfo*))Enumerator_VerifyState_m13700_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m13806(__this, method) (( void (*) (Enumerator_t2835 *, const MethodInfo*))Enumerator_VerifyCurrent_m13701_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::Dispose()
#define Enumerator_Dispose_m13807(__this, method) (( void (*) (Enumerator_t2835 *, const MethodInfo*))Enumerator_Dispose_m13702_gshared)(__this, method)
