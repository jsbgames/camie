﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RijndaelTransform
struct RijndaelTransform_t2090;
// System.Security.Cryptography.Rijndael
struct Rijndael_t1652;
// System.Byte[]
struct ByteU5BU5D_t850;
// System.UInt32[]
struct UInt32U5BU5D_t1523;

// System.Void System.Security.Cryptography.RijndaelTransform::.ctor(System.Security.Cryptography.Rijndael,System.Boolean,System.Byte[],System.Byte[])
extern "C" void RijndaelTransform__ctor_m11203 (RijndaelTransform_t2090 * __this, Rijndael_t1652 * ___algo, bool ___encryption, ByteU5BU5D_t850* ___key, ByteU5BU5D_t850* ___iv, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::.cctor()
extern "C" void RijndaelTransform__cctor_m11204 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Clear()
extern "C" void RijndaelTransform_Clear_m11205 (RijndaelTransform_t2090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::ECB(System.Byte[],System.Byte[])
extern "C" void RijndaelTransform_ECB_m11206 (RijndaelTransform_t2090 * __this, ByteU5BU5D_t850* ___input, ByteU5BU5D_t850* ___output, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Security.Cryptography.RijndaelTransform::SubByte(System.UInt32)
extern "C" uint32_t RijndaelTransform_SubByte_m11207 (RijndaelTransform_t2090 * __this, uint32_t ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern "C" void RijndaelTransform_Encrypt128_m11208 (RijndaelTransform_t2090 * __this, ByteU5BU5D_t850* ___indata, ByteU5BU5D_t850* ___outdata, UInt32U5BU5D_t1523* ___ekey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Encrypt192(System.Byte[],System.Byte[],System.UInt32[])
extern "C" void RijndaelTransform_Encrypt192_m11209 (RijndaelTransform_t2090 * __this, ByteU5BU5D_t850* ___indata, ByteU5BU5D_t850* ___outdata, UInt32U5BU5D_t1523* ___ekey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Encrypt256(System.Byte[],System.Byte[],System.UInt32[])
extern "C" void RijndaelTransform_Encrypt256_m11210 (RijndaelTransform_t2090 * __this, ByteU5BU5D_t850* ___indata, ByteU5BU5D_t850* ___outdata, UInt32U5BU5D_t1523* ___ekey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern "C" void RijndaelTransform_Decrypt128_m11211 (RijndaelTransform_t2090 * __this, ByteU5BU5D_t850* ___indata, ByteU5BU5D_t850* ___outdata, UInt32U5BU5D_t1523* ___ekey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Decrypt192(System.Byte[],System.Byte[],System.UInt32[])
extern "C" void RijndaelTransform_Decrypt192_m11212 (RijndaelTransform_t2090 * __this, ByteU5BU5D_t850* ___indata, ByteU5BU5D_t850* ___outdata, UInt32U5BU5D_t1523* ___ekey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Decrypt256(System.Byte[],System.Byte[],System.UInt32[])
extern "C" void RijndaelTransform_Decrypt256_m11213 (RijndaelTransform_t2090 * __this, ByteU5BU5D_t850* ___indata, ByteU5BU5D_t850* ___outdata, UInt32U5BU5D_t1523* ___ekey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
