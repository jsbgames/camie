﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PauseMenu
struct PauseMenu_t313;

// System.Void PauseMenu::.ctor()
extern "C" void PauseMenu__ctor_m1151 (PauseMenu_t313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenu::Awake()
extern "C" void PauseMenu_Awake_m1152 (PauseMenu_t313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenu::MenuOn()
extern "C" void PauseMenu_MenuOn_m1153 (PauseMenu_t313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenu::MenuOff()
extern "C" void PauseMenu_MenuOff_m1154 (PauseMenu_t313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenu::OnMenuStatusChange()
extern "C" void PauseMenu_OnMenuStatusChange_m1155 (PauseMenu_t313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
