﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>
struct Enumerator_t2970;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t355;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9MethodDeclarations.h"
#define Enumerator__ctor_m15695(__this, ___dictionary, method) (( void (*) (Enumerator_t2970 *, Dictionary_2_t355 *, const MethodInfo*))Enumerator__ctor_m15593_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15696(__this, method) (( Object_t * (*) (Enumerator_t2970 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15594_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15697(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t2970 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15595_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15698(__this, method) (( Object_t * (*) (Enumerator_t2970 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15596_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15699(__this, method) (( Object_t * (*) (Enumerator_t2970 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15597_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m15700(__this, method) (( bool (*) (Enumerator_t2970 *, const MethodInfo*))Enumerator_MoveNext_m15598_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_Current()
#define Enumerator_get_Current_m15701(__this, method) (( KeyValuePair_2_t2967  (*) (Enumerator_t2970 *, const MethodInfo*))Enumerator_get_Current_m15599_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15702(__this, method) (( String_t* (*) (Enumerator_t2970 *, const MethodInfo*))Enumerator_get_CurrentKey_m15600_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15703(__this, method) (( int32_t (*) (Enumerator_t2970 *, const MethodInfo*))Enumerator_get_CurrentValue_m15601_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m15704(__this, method) (( void (*) (Enumerator_t2970 *, const MethodInfo*))Enumerator_VerifyState_m15602_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15705(__this, method) (( void (*) (Enumerator_t2970 *, const MethodInfo*))Enumerator_VerifyCurrent_m15603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Dispose()
#define Enumerator_Dispose_m15706(__this, method) (( void (*) (Enumerator_t2970 *, const MethodInfo*))Enumerator_Dispose_m15604_gshared)(__this, method)
