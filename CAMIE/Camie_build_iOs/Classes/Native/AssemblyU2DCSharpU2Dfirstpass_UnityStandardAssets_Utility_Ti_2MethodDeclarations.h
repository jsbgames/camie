﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9
struct U3CActivateU3Ec__Iterator9_t202;
// System.Object
struct Object_t;

// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::.ctor()
extern "C" void U3CActivateU3Ec__Iterator9__ctor_m541 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CActivateU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m542 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CActivateU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m543 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::MoveNext()
extern "C" bool U3CActivateU3Ec__Iterator9_MoveNext_m544 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::Dispose()
extern "C" void U3CActivateU3Ec__Iterator9_Dispose_m545 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::Reset()
extern "C" void U3CActivateU3Ec__Iterator9_Reset_m546 (U3CActivateU3Ec__Iterator9_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
