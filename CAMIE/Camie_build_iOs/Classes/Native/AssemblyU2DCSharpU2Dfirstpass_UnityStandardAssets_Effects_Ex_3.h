﻿#pragma once
#include <stdint.h>
// UnityEngine.Collision
struct Collision_t146;
// System.Object
struct Object_t;
// UnityStandardAssets.Effects.Explosive
struct Explosive_t147;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2
struct  U3COnCollisionEnterU3Ec__Iterator2_t148  : public Object_t
{
	// UnityEngine.Collision UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::col
	Collision_t146 * ___col_0;
	// System.Single UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::<velocityAlongCollisionNormal>__0
	float ___U3CvelocityAlongCollisionNormalU3E__0_1;
	// System.Int32 UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::$PC
	int32_t ___U24PC_2;
	// System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::$current
	Object_t * ___U24current_3;
	// UnityEngine.Collision UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::<$>col
	Collision_t146 * ___U3CU24U3Ecol_4;
	// UnityStandardAssets.Effects.Explosive UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>c__Iterator2::<>f__this
	Explosive_t147 * ___U3CU3Ef__this_5;
};
