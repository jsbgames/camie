﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
struct Enumerator_t3243;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t899;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m19761_gshared (Enumerator_t3243 * __this, List_1_t899 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m19761(__this, ___l, method) (( void (*) (Enumerator_t3243 *, List_1_t899 *, const MethodInfo*))Enumerator__ctor_m19761_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m19762_gshared (Enumerator_t3243 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m19762(__this, method) (( Object_t * (*) (Enumerator_t3243 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19762_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C" void Enumerator_Dispose_m19763_gshared (Enumerator_t3243 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m19763(__this, method) (( void (*) (Enumerator_t3243 *, const MethodInfo*))Enumerator_Dispose_m19763_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m19764_gshared (Enumerator_t3243 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m19764(__this, method) (( void (*) (Enumerator_t3243 *, const MethodInfo*))Enumerator_VerifyState_m19764_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m19765_gshared (Enumerator_t3243 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m19765(__this, method) (( bool (*) (Enumerator_t3243 *, const MethodInfo*))Enumerator_MoveNext_m19765_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t687  Enumerator_get_Current_m19766_gshared (Enumerator_t3243 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m19766(__this, method) (( UILineInfo_t687  (*) (Enumerator_t3243 *, const MethodInfo*))Enumerator_get_Current_m19766_gshared)(__this, method)
