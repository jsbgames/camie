//Generated on : 2016-01-28 23:19:28
void RegisterAllStrippedInternalCalls()
{
	//Start Registrations for type : UnityEngine.Animation

		//System.Boolean UnityEngine.Animation::PlayDefaultAnimation(UnityEngine.PlayMode)
		void Register_UnityEngine_Animation_PlayDefaultAnimation();
		Register_UnityEngine_Animation_PlayDefaultAnimation();

		//System.Int32 UnityEngine.Animation::GetStateCount()
		void Register_UnityEngine_Animation_GetStateCount();
		Register_UnityEngine_Animation_GetStateCount();

		//UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
		void Register_UnityEngine_Animation_GetStateAtIndex();
		Register_UnityEngine_Animation_GetStateAtIndex();

	//End Registrations for type : UnityEngine.Animation

	//Start Registrations for type : UnityEngine.AnimationCurve

		//System.Int32 UnityEngine.AnimationCurve::get_length()
		void Register_UnityEngine_AnimationCurve_get_length();
		Register_UnityEngine_AnimationCurve_get_length();

		//System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
		void Register_UnityEngine_AnimationCurve_Evaluate();
		Register_UnityEngine_AnimationCurve_Evaluate();

		//System.Void UnityEngine.AnimationCurve::Cleanup()
		void Register_UnityEngine_AnimationCurve_Cleanup();
		Register_UnityEngine_AnimationCurve_Cleanup();

		//System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
		void Register_UnityEngine_AnimationCurve_Init();
		Register_UnityEngine_AnimationCurve_Init();

		//UnityEngine.Keyframe UnityEngine.AnimationCurve::GetKey_Internal(System.Int32)
		void Register_UnityEngine_AnimationCurve_GetKey_Internal();
		Register_UnityEngine_AnimationCurve_GetKey_Internal();

		//UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
		void Register_UnityEngine_AnimationCurve_GetKeys();
		Register_UnityEngine_AnimationCurve_GetKeys();

	//End Registrations for type : UnityEngine.AnimationCurve

	//Start Registrations for type : UnityEngine.Animator

		//System.Boolean UnityEngine.Animator::GetBoolString(System.String)
		void Register_UnityEngine_Animator_GetBoolString();
		Register_UnityEngine_Animator_GetBoolString();

		//System.Int32 UnityEngine.Animator::StringToHash(System.String)
		void Register_UnityEngine_Animator_StringToHash();
		Register_UnityEngine_Animator_StringToHash();

		//System.Void UnityEngine.Animator::ResetTriggerString(System.String)
		void Register_UnityEngine_Animator_ResetTriggerString();
		Register_UnityEngine_Animator_ResetTriggerString();

		//System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
		void Register_UnityEngine_Animator_SetBoolString();
		Register_UnityEngine_Animator_SetBoolString();

		//System.Void UnityEngine.Animator::SetFloatString(System.String,System.Single)
		void Register_UnityEngine_Animator_SetFloatString();
		Register_UnityEngine_Animator_SetFloatString();

		//System.Void UnityEngine.Animator::SetTriggerString(System.String)
		void Register_UnityEngine_Animator_SetTriggerString();
		Register_UnityEngine_Animator_SetTriggerString();

		//UnityEngine.AnimatorClipInfo[] UnityEngine.Animator::GetCurrentAnimatorClipInfo(System.Int32)
		void Register_UnityEngine_Animator_GetCurrentAnimatorClipInfo();
		Register_UnityEngine_Animator_GetCurrentAnimatorClipInfo();

		//UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
		void Register_UnityEngine_Animator_get_runtimeAnimatorController();
		Register_UnityEngine_Animator_get_runtimeAnimatorController();

	//End Registrations for type : UnityEngine.Animator

	//Start Registrations for type : UnityEngine.Application

		//System.Boolean UnityEngine.Application::get_isEditor()
		void Register_UnityEngine_Application_get_isEditor();
		Register_UnityEngine_Application_get_isEditor();

		//System.Boolean UnityEngine.Application::get_isLoadingLevel()
		void Register_UnityEngine_Application_get_isLoadingLevel();
		Register_UnityEngine_Application_get_isLoadingLevel();

		//System.Boolean UnityEngine.Application::get_isPlaying()
		void Register_UnityEngine_Application_get_isPlaying();
		Register_UnityEngine_Application_get_isPlaying();

		//System.Int32 UnityEngine.Application::get_levelCount()
		void Register_UnityEngine_Application_get_levelCount();
		Register_UnityEngine_Application_get_levelCount();

		//System.Int32 UnityEngine.Application::get_loadedLevel()
		void Register_UnityEngine_Application_get_loadedLevel();
		Register_UnityEngine_Application_get_loadedLevel();

		//System.String UnityEngine.Application::get_dataPath()
		void Register_UnityEngine_Application_get_dataPath();
		Register_UnityEngine_Application_get_dataPath();

		//System.String UnityEngine.Application::get_loadedLevelName()
		void Register_UnityEngine_Application_get_loadedLevelName();
		Register_UnityEngine_Application_get_loadedLevelName();

		//System.String UnityEngine.Application::get_persistentDataPath()
		void Register_UnityEngine_Application_get_persistentDataPath();
		Register_UnityEngine_Application_get_persistentDataPath();

		//System.String UnityEngine.Application::get_streamingAssetsPath()
		void Register_UnityEngine_Application_get_streamingAssetsPath();
		Register_UnityEngine_Application_get_streamingAssetsPath();

		//System.String UnityEngine.Application::get_unityVersion()
		void Register_UnityEngine_Application_get_unityVersion();
		Register_UnityEngine_Application_get_unityVersion();

		//System.Void UnityEngine.Application::OpenURL(System.String)
		void Register_UnityEngine_Application_OpenURL();
		Register_UnityEngine_Application_OpenURL();

		//System.Void UnityEngine.Application::SetLogCallbackDefined(System.Boolean,System.Boolean)
		void Register_UnityEngine_Application_SetLogCallbackDefined();
		Register_UnityEngine_Application_SetLogCallbackDefined();

		//System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
		void Register_UnityEngine_Application_set_runInBackground();
		Register_UnityEngine_Application_set_runInBackground();

		//UnityEngine.AsyncOperation UnityEngine.Application::LoadLevelAsync(System.String,System.Int32,System.Boolean,System.Boolean)
		void Register_UnityEngine_Application_LoadLevelAsync();
		Register_UnityEngine_Application_LoadLevelAsync();

		//UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
		void Register_UnityEngine_Application_get_platform();
		Register_UnityEngine_Application_get_platform();

		//UnityEngine.SystemLanguage UnityEngine.Application::get_systemLanguage()
		void Register_UnityEngine_Application_get_systemLanguage();
		Register_UnityEngine_Application_get_systemLanguage();

	//End Registrations for type : UnityEngine.Application

	//Start Registrations for type : UnityEngine.AssetBundle

		//UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
		void Register_UnityEngine_AssetBundle_LoadAsset_Internal();
		Register_UnityEngine_AssetBundle_LoadAsset_Internal();

		//UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
		void Register_UnityEngine_AssetBundle_LoadAssetWithSubAssets_Internal();
		Register_UnityEngine_AssetBundle_LoadAssetWithSubAssets_Internal();

	//End Registrations for type : UnityEngine.AssetBundle

	//Start Registrations for type : UnityEngine.AssetBundleCreateRequest

		//System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
		void Register_UnityEngine_AssetBundleCreateRequest_DisableCompatibilityChecks();
		Register_UnityEngine_AssetBundleCreateRequest_DisableCompatibilityChecks();

		//UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
		void Register_UnityEngine_AssetBundleCreateRequest_get_assetBundle();
		Register_UnityEngine_AssetBundleCreateRequest_get_assetBundle();

	//End Registrations for type : UnityEngine.AssetBundleCreateRequest

	//Start Registrations for type : UnityEngine.AsyncOperation

		//System.Void UnityEngine.AsyncOperation::InternalDestroy()
		void Register_UnityEngine_AsyncOperation_InternalDestroy();
		Register_UnityEngine_AsyncOperation_InternalDestroy();

	//End Registrations for type : UnityEngine.AsyncOperation

	//Start Registrations for type : UnityEngine.AudioListener

		//System.Single UnityEngine.AudioListener::get_volume()
		void Register_UnityEngine_AudioListener_get_volume();
		Register_UnityEngine_AudioListener_get_volume();

		//System.Void UnityEngine.AudioListener::set_volume(System.Single)
		void Register_UnityEngine_AudioListener_set_volume();
		Register_UnityEngine_AudioListener_set_volume();

	//End Registrations for type : UnityEngine.AudioListener

	//Start Registrations for type : UnityEngine.AudioSource

		//System.Boolean UnityEngine.AudioSource::get_isPlaying()
		void Register_UnityEngine_AudioSource_get_isPlaying();
		Register_UnityEngine_AudioSource_get_isPlaying();

		//System.Void UnityEngine.AudioSource::Play(System.UInt64)
		void Register_UnityEngine_AudioSource_Play();
		Register_UnityEngine_AudioSource_Play();

		//System.Void UnityEngine.AudioSource::Stop()
		void Register_UnityEngine_AudioSource_Stop();
		Register_UnityEngine_AudioSource_Stop();

		//System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
		void Register_UnityEngine_AudioSource_set_clip();
		Register_UnityEngine_AudioSource_set_clip();

	//End Registrations for type : UnityEngine.AudioSource

	//Start Registrations for type : UnityEngine.Behaviour

		//System.Boolean UnityEngine.Behaviour::get_enabled()
		void Register_UnityEngine_Behaviour_get_enabled();
		Register_UnityEngine_Behaviour_get_enabled();

		//System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
		void Register_UnityEngine_Behaviour_get_isActiveAndEnabled();
		Register_UnityEngine_Behaviour_get_isActiveAndEnabled();

		//System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
		void Register_UnityEngine_Behaviour_set_enabled();
		Register_UnityEngine_Behaviour_set_enabled();

	//End Registrations for type : UnityEngine.Behaviour

	//Start Registrations for type : UnityEngine.Bounds

		//System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_Contains();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_Contains();

		//System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_IntersectRay();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_IntersectRay();

		//System.Single UnityEngine.Bounds::INTERNAL_CALL_Internal_SqrDistance(UnityEngine.Bounds&,UnityEngine.Vector3&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_SqrDistance();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_SqrDistance();

		//UnityEngine.Vector3 UnityEngine.Bounds::INTERNAL_CALL_Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_GetClosestPoint();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_GetClosestPoint();

	//End Registrations for type : UnityEngine.Bounds

	//Start Registrations for type : UnityEngine.Camera

		//System.Boolean UnityEngine.Camera::get_hdr()
		void Register_UnityEngine_Camera_get_hdr();
		Register_UnityEngine_Camera_get_hdr();

		//System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
		void Register_UnityEngine_Camera_GetAllCameras();
		Register_UnityEngine_Camera_GetAllCameras();

		//System.Int32 UnityEngine.Camera::get_allCamerasCount()
		void Register_UnityEngine_Camera_get_allCamerasCount();
		Register_UnityEngine_Camera_get_allCamerasCount();

		//System.Int32 UnityEngine.Camera::get_cullingMask()
		void Register_UnityEngine_Camera_get_cullingMask();
		Register_UnityEngine_Camera_get_cullingMask();

		//System.Int32 UnityEngine.Camera::get_eventMask()
		void Register_UnityEngine_Camera_get_eventMask();
		Register_UnityEngine_Camera_get_eventMask();

		//System.Single UnityEngine.Camera::get_aspect()
		void Register_UnityEngine_Camera_get_aspect();
		Register_UnityEngine_Camera_get_aspect();

		//System.Single UnityEngine.Camera::get_depth()
		void Register_UnityEngine_Camera_get_depth();
		Register_UnityEngine_Camera_get_depth();

		//System.Single UnityEngine.Camera::get_farClipPlane()
		void Register_UnityEngine_Camera_get_farClipPlane();
		Register_UnityEngine_Camera_get_farClipPlane();

		//System.Single UnityEngine.Camera::get_fieldOfView()
		void Register_UnityEngine_Camera_get_fieldOfView();
		Register_UnityEngine_Camera_get_fieldOfView();

		//System.Single UnityEngine.Camera::get_nearClipPlane()
		void Register_UnityEngine_Camera_get_nearClipPlane();
		Register_UnityEngine_Camera_get_nearClipPlane();

		//System.Void UnityEngine.Camera::CopyFrom(UnityEngine.Camera)
		void Register_UnityEngine_Camera_CopyFrom();
		Register_UnityEngine_Camera_CopyFrom();

		//System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_get_pixelRect();
		Register_UnityEngine_Camera_INTERNAL_get_pixelRect();

		//System.Void UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_INTERNAL_get_projectionMatrix();
		Register_UnityEngine_Camera_INTERNAL_get_projectionMatrix();

		//System.Void UnityEngine.Camera::INTERNAL_get_worldToCameraMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Camera_INTERNAL_get_worldToCameraMatrix();
		Register_UnityEngine_Camera_INTERNAL_get_worldToCameraMatrix();

		//System.Void UnityEngine.Camera::RenderWithShader(UnityEngine.Shader,System.String)
		void Register_UnityEngine_Camera_RenderWithShader();
		Register_UnityEngine_Camera_RenderWithShader();

		//System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
		void Register_UnityEngine_Camera_set_clearFlags();
		Register_UnityEngine_Camera_set_clearFlags();

		//System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
		void Register_UnityEngine_Camera_set_cullingMask();
		Register_UnityEngine_Camera_set_cullingMask();

		//System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
		void Register_UnityEngine_Camera_set_depthTextureMode();
		Register_UnityEngine_Camera_set_depthTextureMode();

		//System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
		void Register_UnityEngine_Camera_set_fieldOfView();
		Register_UnityEngine_Camera_set_fieldOfView();

		//System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
		void Register_UnityEngine_Camera_set_targetTexture();
		Register_UnityEngine_Camera_set_targetTexture();

		//UnityEngine.Camera UnityEngine.Camera::get_main()
		void Register_UnityEngine_Camera_get_main();
		Register_UnityEngine_Camera_get_main();

		//UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
		void Register_UnityEngine_Camera_get_clearFlags();
		Register_UnityEngine_Camera_get_clearFlags();

		//UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
		void Register_UnityEngine_Camera_get_depthTextureMode();
		Register_UnityEngine_Camera_get_depthTextureMode();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();

		//UnityEngine.Ray UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();

		//UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
		void Register_UnityEngine_Camera_get_targetTexture();
		Register_UnityEngine_Camera_get_targetTexture();

		//UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint();

		//UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint();

		//UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_WorldToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_WorldToViewportPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_WorldToViewportPoint();

	//End Registrations for type : UnityEngine.Camera

	//Start Registrations for type : UnityEngine.Canvas

		//System.Boolean UnityEngine.Canvas::get_isRootCanvas()
		void Register_UnityEngine_Canvas_get_isRootCanvas();
		Register_UnityEngine_Canvas_get_isRootCanvas();

		//System.Boolean UnityEngine.Canvas::get_pixelPerfect()
		void Register_UnityEngine_Canvas_get_pixelPerfect();
		Register_UnityEngine_Canvas_get_pixelPerfect();

		//System.Int32 UnityEngine.Canvas::get_cachedSortingLayerValue()
		void Register_UnityEngine_Canvas_get_cachedSortingLayerValue();
		Register_UnityEngine_Canvas_get_cachedSortingLayerValue();

		//System.Int32 UnityEngine.Canvas::get_renderOrder()
		void Register_UnityEngine_Canvas_get_renderOrder();
		Register_UnityEngine_Canvas_get_renderOrder();

		//System.Int32 UnityEngine.Canvas::get_sortingOrder()
		void Register_UnityEngine_Canvas_get_sortingOrder();
		Register_UnityEngine_Canvas_get_sortingOrder();

		//System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
		void Register_UnityEngine_Canvas_get_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_get_referencePixelsPerUnit();

		//System.Single UnityEngine.Canvas::get_scaleFactor()
		void Register_UnityEngine_Canvas_get_scaleFactor();
		Register_UnityEngine_Canvas_get_scaleFactor();

		//System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
		void Register_UnityEngine_Canvas_set_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_set_referencePixelsPerUnit();

		//System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
		void Register_UnityEngine_Canvas_set_scaleFactor();
		Register_UnityEngine_Canvas_set_scaleFactor();

		//UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
		void Register_UnityEngine_Canvas_get_worldCamera();
		Register_UnityEngine_Canvas_get_worldCamera();

		//UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
		void Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();
		Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();

		//UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasTextMaterial()
		void Register_UnityEngine_Canvas_GetDefaultCanvasTextMaterial();
		Register_UnityEngine_Canvas_GetDefaultCanvasTextMaterial();

		//UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
		void Register_UnityEngine_Canvas_get_renderMode();
		Register_UnityEngine_Canvas_get_renderMode();

	//End Registrations for type : UnityEngine.Canvas

	//Start Registrations for type : UnityEngine.CanvasGroup

		//System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
		void Register_UnityEngine_CanvasGroup_get_blocksRaycasts();
		Register_UnityEngine_CanvasGroup_get_blocksRaycasts();

		//System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
		void Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();
		Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();

		//System.Boolean UnityEngine.CanvasGroup::get_interactable()
		void Register_UnityEngine_CanvasGroup_get_interactable();
		Register_UnityEngine_CanvasGroup_get_interactable();

	//End Registrations for type : UnityEngine.CanvasGroup

	//Start Registrations for type : UnityEngine.CanvasRenderer

		//System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
		void Register_UnityEngine_CanvasRenderer_get_absoluteDepth();
		Register_UnityEngine_CanvasRenderer_get_absoluteDepth();

		//System.Void UnityEngine.CanvasRenderer::Clear()
		void Register_UnityEngine_CanvasRenderer_Clear();
		Register_UnityEngine_CanvasRenderer_Clear();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor();

		//System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
		void Register_UnityEngine_CanvasRenderer_SetMaterial();
		Register_UnityEngine_CanvasRenderer_SetMaterial();

		//System.Void UnityEngine.CanvasRenderer::SetVerticesInternal(System.Object)
		void Register_UnityEngine_CanvasRenderer_SetVerticesInternal();
		Register_UnityEngine_CanvasRenderer_SetVerticesInternal();

		//System.Void UnityEngine.CanvasRenderer::SetVerticesInternalArray(UnityEngine.UIVertex[],System.Int32)
		void Register_UnityEngine_CanvasRenderer_SetVerticesInternalArray();
		Register_UnityEngine_CanvasRenderer_SetVerticesInternalArray();

		//System.Void UnityEngine.CanvasRenderer::set_isMask(System.Boolean)
		void Register_UnityEngine_CanvasRenderer_set_isMask();
		Register_UnityEngine_CanvasRenderer_set_isMask();

		//UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
		void Register_UnityEngine_CanvasRenderer_GetColor();
		Register_UnityEngine_CanvasRenderer_GetColor();

	//End Registrations for type : UnityEngine.CanvasRenderer

	//Start Registrations for type : UnityEngine.Collider

		//System.Boolean UnityEngine.Collider::INTERNAL_CALL_Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray&,UnityEngine.RaycastHit&,System.Single)
		void Register_UnityEngine_Collider_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Collider_INTERNAL_CALL_Internal_Raycast();

		//System.Boolean UnityEngine.Collider::get_isTrigger()
		void Register_UnityEngine_Collider_get_isTrigger();
		Register_UnityEngine_Collider_get_isTrigger();

		//System.Void UnityEngine.Collider::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Collider_INTERNAL_get_bounds();
		Register_UnityEngine_Collider_INTERNAL_get_bounds();

		//System.Void UnityEngine.Collider::set_enabled(System.Boolean)
		void Register_UnityEngine_Collider_set_enabled();
		Register_UnityEngine_Collider_set_enabled();

		//UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
		void Register_UnityEngine_Collider_get_attachedRigidbody();
		Register_UnityEngine_Collider_get_attachedRigidbody();

	//End Registrations for type : UnityEngine.Collider

	//Start Registrations for type : UnityEngine.Collider2D

		//UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
		void Register_UnityEngine_Collider2D_get_attachedRigidbody();
		Register_UnityEngine_Collider2D_get_attachedRigidbody();

	//End Registrations for type : UnityEngine.Collider2D

	//Start Registrations for type : UnityEngine.Component

		//System.Boolean UnityEngine.Component::CompareTag(System.String)
		void Register_UnityEngine_Component_CompareTag();
		Register_UnityEngine_Component_CompareTag();

		//System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_Component_BroadcastMessage();
		Register_UnityEngine_Component_BroadcastMessage();

		//System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_Component_GetComponentFastPath();
		Register_UnityEngine_Component_GetComponentFastPath();

		//System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
		void Register_UnityEngine_Component_GetComponentsForListInternal();
		Register_UnityEngine_Component_GetComponentsForListInternal();

		//System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_Component_SendMessage();
		Register_UnityEngine_Component_SendMessage();

		//UnityEngine.GameObject UnityEngine.Component::get_gameObject()
		void Register_UnityEngine_Component_get_gameObject();
		Register_UnityEngine_Component_get_gameObject();

		//UnityEngine.Transform UnityEngine.Component::get_transform()
		void Register_UnityEngine_Component_get_transform();
		Register_UnityEngine_Component_get_transform();

	//End Registrations for type : UnityEngine.Component

	//Start Registrations for type : UnityEngine.ComputeBuffer

		//System.Void UnityEngine.ComputeBuffer::CopyCount(UnityEngine.ComputeBuffer,UnityEngine.ComputeBuffer,System.Int32)
		void Register_UnityEngine_ComputeBuffer_CopyCount();
		Register_UnityEngine_ComputeBuffer_CopyCount();

		//System.Void UnityEngine.ComputeBuffer::DestroyBuffer(UnityEngine.ComputeBuffer)
		void Register_UnityEngine_ComputeBuffer_DestroyBuffer();
		Register_UnityEngine_ComputeBuffer_DestroyBuffer();

		//System.Void UnityEngine.ComputeBuffer::InitBuffer(UnityEngine.ComputeBuffer,System.Int32,System.Int32,UnityEngine.ComputeBufferType)
		void Register_UnityEngine_ComputeBuffer_InitBuffer();
		Register_UnityEngine_ComputeBuffer_InitBuffer();

		//System.Void UnityEngine.ComputeBuffer::InternalSetData(System.Array,System.Int32)
		void Register_UnityEngine_ComputeBuffer_InternalSetData();
		Register_UnityEngine_ComputeBuffer_InternalSetData();

	//End Registrations for type : UnityEngine.ComputeBuffer

	//Start Registrations for type : UnityEngine.Coroutine

		//System.Void UnityEngine.Coroutine::ReleaseCoroutine()
		void Register_UnityEngine_Coroutine_ReleaseCoroutine();
		Register_UnityEngine_Coroutine_ReleaseCoroutine();

	//End Registrations for type : UnityEngine.Coroutine

	//Start Registrations for type : UnityEngine.Cursor

		//System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
		void Register_UnityEngine_Cursor_set_lockState();
		Register_UnityEngine_Cursor_set_lockState();

		//System.Void UnityEngine.Cursor::set_visible(System.Boolean)
		void Register_UnityEngine_Cursor_set_visible();
		Register_UnityEngine_Cursor_set_visible();

	//End Registrations for type : UnityEngine.Cursor

	//Start Registrations for type : UnityEngine.Debug

		//System.Void UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
		void Register_UnityEngine_Debug_INTERNAL_CALL_DrawLine();
		Register_UnityEngine_Debug_INTERNAL_CALL_DrawLine();

		//System.Void UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)
		void Register_UnityEngine_Debug_Internal_Log();
		Register_UnityEngine_Debug_Internal_Log();

		//System.Void UnityEngine.Debug::Internal_LogException(System.Exception,UnityEngine.Object)
		void Register_UnityEngine_Debug_Internal_LogException();
		Register_UnityEngine_Debug_Internal_LogException();

	//End Registrations for type : UnityEngine.Debug

	//Start Registrations for type : UnityEngine.Display

		//System.Boolean UnityEngine.Display::MultiDisplayLicenseImpl()
		void Register_UnityEngine_Display_MultiDisplayLicenseImpl();
		Register_UnityEngine_Display_MultiDisplayLicenseImpl();

		//System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_RelativeMouseAtImpl();
		Register_UnityEngine_Display_RelativeMouseAtImpl();

		//System.Void UnityEngine.Display::ActivateDisplayImpl(System.IntPtr,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Display_ActivateDisplayImpl();
		Register_UnityEngine_Display_ActivateDisplayImpl();

		//System.Void UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)
		void Register_UnityEngine_Display_GetRenderingBuffersImpl();
		Register_UnityEngine_Display_GetRenderingBuffersImpl();

		//System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetRenderingExtImpl();
		Register_UnityEngine_Display_GetRenderingExtImpl();

		//System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetSystemExtImpl();
		Register_UnityEngine_Display_GetSystemExtImpl();

		//System.Void UnityEngine.Display::SetParamsImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Display_SetParamsImpl();
		Register_UnityEngine_Display_SetParamsImpl();

		//System.Void UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)
		void Register_UnityEngine_Display_SetRenderingResolutionImpl();
		Register_UnityEngine_Display_SetRenderingResolutionImpl();

	//End Registrations for type : UnityEngine.Display

	//Start Registrations for type : UnityEngine.Event

		//System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
		void Register_UnityEngine_Event_PopEvent();
		Register_UnityEngine_Event_PopEvent();

		//System.Char UnityEngine.Event::get_character()
		void Register_UnityEngine_Event_get_character();
		Register_UnityEngine_Event_get_character();

		//System.String UnityEngine.Event::get_commandName()
		void Register_UnityEngine_Event_get_commandName();
		Register_UnityEngine_Event_get_commandName();

		//System.Void UnityEngine.Event::Cleanup()
		void Register_UnityEngine_Event_Cleanup();
		Register_UnityEngine_Event_Cleanup();

		//System.Void UnityEngine.Event::Init()
		void Register_UnityEngine_Event_Init();
		Register_UnityEngine_Event_Init();

		//System.Void UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_Event_Internal_GetMouseDelta();
		Register_UnityEngine_Event_Internal_GetMouseDelta();

		//System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
		void Register_UnityEngine_Event_Internal_GetMousePosition();
		Register_UnityEngine_Event_Internal_GetMousePosition();

		//System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
		void Register_UnityEngine_Event_Internal_SetNativeEvent();
		Register_UnityEngine_Event_Internal_SetNativeEvent();

		//System.Void UnityEngine.Event::Use()
		void Register_UnityEngine_Event_Use();
		Register_UnityEngine_Event_Use();

		//UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
		void Register_UnityEngine_Event_get_modifiers();
		Register_UnityEngine_Event_get_modifiers();

		//UnityEngine.EventType UnityEngine.Event::GetTypeForControl(System.Int32)
		void Register_UnityEngine_Event_GetTypeForControl();
		Register_UnityEngine_Event_GetTypeForControl();

		//UnityEngine.EventType UnityEngine.Event::get_rawType()
		void Register_UnityEngine_Event_get_rawType();
		Register_UnityEngine_Event_get_rawType();

		//UnityEngine.EventType UnityEngine.Event::get_type()
		void Register_UnityEngine_Event_get_type();
		Register_UnityEngine_Event_get_type();

		//UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
		void Register_UnityEngine_Event_get_keyCode();
		Register_UnityEngine_Event_get_keyCode();

	//End Registrations for type : UnityEngine.Event

	//Start Registrations for type : UnityEngine.Font

		//System.Boolean UnityEngine.Font::HasCharacter(System.Char)
		void Register_UnityEngine_Font_HasCharacter();
		Register_UnityEngine_Font_HasCharacter();

		//System.Boolean UnityEngine.Font::get_dynamic()
		void Register_UnityEngine_Font_get_dynamic();
		Register_UnityEngine_Font_get_dynamic();

		//System.Int32 UnityEngine.Font::get_fontSize()
		void Register_UnityEngine_Font_get_fontSize();
		Register_UnityEngine_Font_get_fontSize();

		//UnityEngine.Material UnityEngine.Font::get_material()
		void Register_UnityEngine_Font_get_material();
		Register_UnityEngine_Font_get_material();

	//End Registrations for type : UnityEngine.Font

	//Start Registrations for type : UnityEngine.GameObject

		//System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
		void Register_UnityEngine_GameObject_GetComponentsInternal();
		Register_UnityEngine_GameObject_GetComponentsInternal();

		//System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
		void Register_UnityEngine_GameObject_get_activeInHierarchy();
		Register_UnityEngine_GameObject_get_activeInHierarchy();

		//System.Boolean UnityEngine.GameObject::get_activeSelf()
		void Register_UnityEngine_GameObject_get_activeSelf();
		Register_UnityEngine_GameObject_get_activeSelf();

		//System.Int32 UnityEngine.GameObject::get_layer()
		void Register_UnityEngine_GameObject_get_layer();
		Register_UnityEngine_GameObject_get_layer();

		//System.String UnityEngine.GameObject::get_tag()
		void Register_UnityEngine_GameObject_get_tag();
		Register_UnityEngine_GameObject_get_tag();

		//System.Void UnityEngine.GameObject::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_BroadcastMessage();
		Register_UnityEngine_GameObject_BroadcastMessage();

		//System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_GameObject_GetComponentFastPath();
		Register_UnityEngine_GameObject_GetComponentFastPath();

		//System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
		void Register_UnityEngine_GameObject_Internal_CreateGameObject();
		Register_UnityEngine_GameObject_Internal_CreateGameObject();

		//System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_SendMessage();
		Register_UnityEngine_GameObject_SendMessage();

		//System.Void UnityEngine.GameObject::SetActive(System.Boolean)
		void Register_UnityEngine_GameObject_SetActive();
		Register_UnityEngine_GameObject_SetActive();

		//System.Void UnityEngine.GameObject::set_layer(System.Int32)
		void Register_UnityEngine_GameObject_set_layer();
		Register_UnityEngine_GameObject_set_layer();

		//UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
		void Register_UnityEngine_GameObject_GetComponent();
		Register_UnityEngine_GameObject_GetComponent();

		//UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
		void Register_UnityEngine_GameObject_Internal_AddComponentWithType();
		Register_UnityEngine_GameObject_Internal_AddComponentWithType();

		//UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
		void Register_UnityEngine_GameObject_Find();
		Register_UnityEngine_GameObject_Find();

		//UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
		void Register_UnityEngine_GameObject_FindGameObjectWithTag();
		Register_UnityEngine_GameObject_FindGameObjectWithTag();

		//UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
		void Register_UnityEngine_GameObject_FindGameObjectsWithTag();
		Register_UnityEngine_GameObject_FindGameObjectsWithTag();

		//UnityEngine.Transform UnityEngine.GameObject::get_transform()
		void Register_UnityEngine_GameObject_get_transform();
		Register_UnityEngine_GameObject_get_transform();

	//End Registrations for type : UnityEngine.GameObject

	//Start Registrations for type : UnityEngine.Gizmos

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireSphere(UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireSphere();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireSphere();

		//System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_Gizmos_INTERNAL_set_color();
		Register_UnityEngine_Gizmos_INTERNAL_set_color();

	//End Registrations for type : UnityEngine.Gizmos

	//Start Registrations for type : UnityEngine.GL

		//System.Void UnityEngine.GL::Begin(System.Int32)
		void Register_UnityEngine_GL_Begin();
		Register_UnityEngine_GL_Begin();

		//System.Void UnityEngine.GL::ClearWithSkybox(System.Boolean,UnityEngine.Camera)
		void Register_UnityEngine_GL_ClearWithSkybox();
		Register_UnityEngine_GL_ClearWithSkybox();

		//System.Void UnityEngine.GL::End()
		void Register_UnityEngine_GL_End();
		Register_UnityEngine_GL_End();

		//System.Void UnityEngine.GL::INTERNAL_CALL_Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
		void Register_UnityEngine_GL_INTERNAL_CALL_Internal_Clear();
		Register_UnityEngine_GL_INTERNAL_CALL_Internal_Clear();

		//System.Void UnityEngine.GL::INTERNAL_CALL_LoadProjectionMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GL_INTERNAL_CALL_LoadProjectionMatrix();
		Register_UnityEngine_GL_INTERNAL_CALL_LoadProjectionMatrix();

		//System.Void UnityEngine.GL::LoadIdentity()
		void Register_UnityEngine_GL_LoadIdentity();
		Register_UnityEngine_GL_LoadIdentity();

		//System.Void UnityEngine.GL::LoadOrtho()
		void Register_UnityEngine_GL_LoadOrtho();
		Register_UnityEngine_GL_LoadOrtho();

		//System.Void UnityEngine.GL::MultiTexCoord2(System.Int32,System.Single,System.Single)
		void Register_UnityEngine_GL_MultiTexCoord2();
		Register_UnityEngine_GL_MultiTexCoord2();

		//System.Void UnityEngine.GL::PopMatrix()
		void Register_UnityEngine_GL_PopMatrix();
		Register_UnityEngine_GL_PopMatrix();

		//System.Void UnityEngine.GL::PushMatrix()
		void Register_UnityEngine_GL_PushMatrix();
		Register_UnityEngine_GL_PushMatrix();

		//System.Void UnityEngine.GL::TexCoord2(System.Single,System.Single)
		void Register_UnityEngine_GL_TexCoord2();
		Register_UnityEngine_GL_TexCoord2();

		//System.Void UnityEngine.GL::Vertex3(System.Single,System.Single,System.Single)
		void Register_UnityEngine_GL_Vertex3();
		Register_UnityEngine_GL_Vertex3();

		//UnityEngine.Matrix4x4 UnityEngine.GL::INTERNAL_CALL_GetGPUProjectionMatrix(UnityEngine.Matrix4x4&,System.Boolean)
		void Register_UnityEngine_GL_INTERNAL_CALL_GetGPUProjectionMatrix();
		Register_UnityEngine_GL_INTERNAL_CALL_GetGPUProjectionMatrix();

	//End Registrations for type : UnityEngine.GL

	//Start Registrations for type : UnityEngine.Gradient

		//System.Void UnityEngine.Gradient::Cleanup()
		void Register_UnityEngine_Gradient_Cleanup();
		Register_UnityEngine_Gradient_Cleanup();

		//System.Void UnityEngine.Gradient::Init()
		void Register_UnityEngine_Gradient_Init();
		Register_UnityEngine_Gradient_Init();

	//End Registrations for type : UnityEngine.Gradient

	//Start Registrations for type : UnityEngine.Graphics

		//System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
		void Register_UnityEngine_Graphics_Blit();
		Register_UnityEngine_Graphics_Blit();

		//System.Void UnityEngine.Graphics::ClearRandomWriteTargets()
		void Register_UnityEngine_Graphics_ClearRandomWriteTargets();
		Register_UnityEngine_Graphics_ClearRandomWriteTargets();

		//System.Void UnityEngine.Graphics::DrawProceduralIndirect(UnityEngine.MeshTopology,UnityEngine.ComputeBuffer,System.Int32)
		void Register_UnityEngine_Graphics_DrawProceduralIndirect();
		Register_UnityEngine_Graphics_DrawProceduralIndirect();

		//System.Void UnityEngine.Graphics::INTERNAL_CALL_Internal_DrawMeshNow2(UnityEngine.Mesh,UnityEngine.Matrix4x4&,System.Int32)
		void Register_UnityEngine_Graphics_INTERNAL_CALL_Internal_DrawMeshNow2();
		Register_UnityEngine_Graphics_INTERNAL_CALL_Internal_DrawMeshNow2();

		//System.Void UnityEngine.Graphics::Internal_BlitMaterial(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean)
		void Register_UnityEngine_Graphics_Internal_BlitMaterial();
		Register_UnityEngine_Graphics_Internal_BlitMaterial();

		//System.Void UnityEngine.Graphics::Internal_BlitMultiTap(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Vector2[])
		void Register_UnityEngine_Graphics_Internal_BlitMultiTap();
		Register_UnityEngine_Graphics_Internal_BlitMultiTap();

		//System.Void UnityEngine.Graphics::Internal_SetNullRT()
		void Register_UnityEngine_Graphics_Internal_SetNullRT();
		Register_UnityEngine_Graphics_Internal_SetNullRT();

		//System.Void UnityEngine.Graphics::Internal_SetRTSimple(UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&,System.Int32,System.Int32)
		void Register_UnityEngine_Graphics_Internal_SetRTSimple();
		Register_UnityEngine_Graphics_Internal_SetRTSimple();

		//System.Void UnityEngine.Graphics::Internal_SetRandomWriteTargetBuffer(System.Int32,UnityEngine.ComputeBuffer)
		void Register_UnityEngine_Graphics_Internal_SetRandomWriteTargetBuffer();
		Register_UnityEngine_Graphics_Internal_SetRandomWriteTargetBuffer();

	//End Registrations for type : UnityEngine.Graphics

	//Start Registrations for type : UnityEngine.GUI

		//System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoButton();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoButton();

		//System.Boolean UnityEngine.GUI::get_usePageScrollbars()
		void Register_UnityEngine_GUI_get_usePageScrollbars();
		Register_UnityEngine_GUI_get_usePageScrollbars();

		//System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel();

		//System.Void UnityEngine.GUI::InternalRepaintEditorWindow()
		void Register_UnityEngine_GUI_InternalRepaintEditorWindow();
		Register_UnityEngine_GUI_InternalRepaintEditorWindow();

		//System.Void UnityEngine.GUI::set_changed(System.Boolean)
		void Register_UnityEngine_GUI_set_changed();
		Register_UnityEngine_GUI_set_changed();

	//End Registrations for type : UnityEngine.GUI

	//Start Registrations for type : UnityEngine.GUIClip

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_Push(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_Push();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_Push();

		//System.Void UnityEngine.GUIClip::Pop()
		void Register_UnityEngine_GUIClip_Pop();
		Register_UnityEngine_GUIClip_Pop();

	//End Registrations for type : UnityEngine.GUIClip

	//Start Registrations for type : UnityEngine.GUILayer

		//UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
		void Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();
		Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();

	//End Registrations for type : UnityEngine.GUILayer

	//Start Registrations for type : UnityEngine.GUILayoutUtility

		//System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)
		void Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow();
		Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow();

		//UnityEngine.Rect UnityEngine.GUILayoutUtility::Internal_GetWindowRect(System.Int32)
		void Register_UnityEngine_GUILayoutUtility_Internal_GetWindowRect();
		Register_UnityEngine_GUILayoutUtility_Internal_GetWindowRect();

	//End Registrations for type : UnityEngine.GUILayoutUtility

	//Start Registrations for type : UnityEngine.GUIStyle

		//System.Boolean UnityEngine.GUIStyle::get_stretchHeight()
		void Register_UnityEngine_GUIStyle_get_stretchHeight();
		Register_UnityEngine_GUIStyle_get_stretchHeight();

		//System.Boolean UnityEngine.GUIStyle::get_stretchWidth()
		void Register_UnityEngine_GUIStyle_get_stretchWidth();
		Register_UnityEngine_GUIStyle_get_stretchWidth();

		//System.Boolean UnityEngine.GUIStyle::get_wordWrap()
		void Register_UnityEngine_GUIStyle_get_wordWrap();
		Register_UnityEngine_GUIStyle_get_wordWrap();

		//System.IntPtr UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)
		void Register_UnityEngine_GUIStyle_GetRectOffsetPtr();
		Register_UnityEngine_GUIStyle_GetRectOffsetPtr();

		//System.IntPtr UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)
		void Register_UnityEngine_GUIStyle_GetStyleStatePtr();
		Register_UnityEngine_GUIStyle_GetStyleStatePtr();

		//System.Single UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)
		void Register_UnityEngine_GUIStyle_Internal_CalcHeight();
		Register_UnityEngine_GUIStyle_Internal_CalcHeight();

		//System.Single UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)
		void Register_UnityEngine_GUIStyle_Internal_GetLineHeight();
		Register_UnityEngine_GUIStyle_Internal_GetLineHeight();

		//System.Single UnityEngine.GUIStyle::get_fixedHeight()
		void Register_UnityEngine_GUIStyle_get_fixedHeight();
		Register_UnityEngine_GUIStyle_get_fixedHeight();

		//System.Single UnityEngine.GUIStyle::get_fixedWidth()
		void Register_UnityEngine_GUIStyle_get_fixedWidth();
		Register_UnityEngine_GUIStyle_get_fixedWidth();

		//System.String UnityEngine.GUIStyle::get_name()
		void Register_UnityEngine_GUIStyle_get_name();
		Register_UnityEngine_GUIStyle_get_name();

		//System.Void UnityEngine.GUIStyle::AssignRectOffset(System.Int32,System.IntPtr)
		void Register_UnityEngine_GUIStyle_AssignRectOffset();
		Register_UnityEngine_GUIStyle_AssignRectOffset();

		//System.Void UnityEngine.GUIStyle::Cleanup()
		void Register_UnityEngine_GUIStyle_Cleanup();
		Register_UnityEngine_GUIStyle_Cleanup();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_Draw2();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_Draw2();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition();

		//System.Void UnityEngine.GUIStyle::Init()
		void Register_UnityEngine_GUIStyle_Init();
		Register_UnityEngine_GUIStyle_Init();

		//System.Void UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)
		void Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth();
		Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth();

		//System.Void UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_Internal_CalcSize();
		Register_UnityEngine_GUIStyle_Internal_CalcSize();

		//System.Void UnityEngine.GUIStyle::Internal_Draw(UnityEngine.GUIContent,UnityEngine.Internal_DrawArguments&)
		void Register_UnityEngine_GUIStyle_Internal_Draw();
		Register_UnityEngine_GUIStyle_Internal_Draw();

		//System.Void UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)
		void Register_UnityEngine_GUIStyle_SetDefaultFont();
		Register_UnityEngine_GUIStyle_SetDefaultFont();

		//System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
		void Register_UnityEngine_GUIStyle_set_alignment();
		Register_UnityEngine_GUIStyle_set_alignment();

		//System.Void UnityEngine.GUIStyle::set_clipping(UnityEngine.TextClipping)
		void Register_UnityEngine_GUIStyle_set_clipping();
		Register_UnityEngine_GUIStyle_set_clipping();

		//System.Void UnityEngine.GUIStyle::set_fixedHeight(System.Single)
		void Register_UnityEngine_GUIStyle_set_fixedHeight();
		Register_UnityEngine_GUIStyle_set_fixedHeight();

		//System.Void UnityEngine.GUIStyle::set_fixedWidth(System.Single)
		void Register_UnityEngine_GUIStyle_set_fixedWidth();
		Register_UnityEngine_GUIStyle_set_fixedWidth();

		//System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
		void Register_UnityEngine_GUIStyle_set_fontSize();
		Register_UnityEngine_GUIStyle_set_fontSize();

		//System.Void UnityEngine.GUIStyle::set_fontStyle(UnityEngine.FontStyle)
		void Register_UnityEngine_GUIStyle_set_fontStyle();
		Register_UnityEngine_GUIStyle_set_fontStyle();

		//System.Void UnityEngine.GUIStyle::set_imagePosition(UnityEngine.ImagePosition)
		void Register_UnityEngine_GUIStyle_set_imagePosition();
		Register_UnityEngine_GUIStyle_set_imagePosition();

		//System.Void UnityEngine.GUIStyle::set_name(System.String)
		void Register_UnityEngine_GUIStyle_set_name();
		Register_UnityEngine_GUIStyle_set_name();

		//System.Void UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_stretchHeight();
		Register_UnityEngine_GUIStyle_set_stretchHeight();

		//System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_stretchWidth();
		Register_UnityEngine_GUIStyle_set_stretchWidth();

		//System.Void UnityEngine.GUIStyle::set_wordWrap(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_wordWrap();
		Register_UnityEngine_GUIStyle_set_wordWrap();

		//UnityEngine.ImagePosition UnityEngine.GUIStyle::get_imagePosition()
		void Register_UnityEngine_GUIStyle_get_imagePosition();
		Register_UnityEngine_GUIStyle_get_imagePosition();

	//End Registrations for type : UnityEngine.GUIStyle

	//Start Registrations for type : UnityEngine.GUIStyleState

		//System.Void UnityEngine.GUIStyleState::Cleanup()
		void Register_UnityEngine_GUIStyleState_Cleanup();
		Register_UnityEngine_GUIStyleState_Cleanup();

		//System.Void UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)
		void Register_UnityEngine_GUIStyleState_INTERNAL_set_textColor();
		Register_UnityEngine_GUIStyleState_INTERNAL_set_textColor();

		//System.Void UnityEngine.GUIStyleState::Init()
		void Register_UnityEngine_GUIStyleState_Init();
		Register_UnityEngine_GUIStyleState_Init();

		//System.Void UnityEngine.GUIStyleState::SetBackgroundInternal(UnityEngine.Texture2D)
		void Register_UnityEngine_GUIStyleState_SetBackgroundInternal();
		Register_UnityEngine_GUIStyleState_SetBackgroundInternal();

		//UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternal()
		void Register_UnityEngine_GUIStyleState_GetBackgroundInternal();
		Register_UnityEngine_GUIStyleState_GetBackgroundInternal();

	//End Registrations for type : UnityEngine.GUIStyleState

	//Start Registrations for type : UnityEngine.GUIText

		//System.Void UnityEngine.GUIText::set_text(System.String)
		void Register_UnityEngine_GUIText_set_text();
		Register_UnityEngine_GUIText_set_text();

	//End Registrations for type : UnityEngine.GUIText

	//Start Registrations for type : UnityEngine.GUIUtility

		//System.Int32 UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)
		void Register_UnityEngine_GUIUtility_GetControlID();
		Register_UnityEngine_GUIUtility_GetControlID();

		//System.Int32 UnityEngine.GUIUtility::INTERNAL_CALL_Internal_GetNextControlID2(System.Int32,UnityEngine.FocusType,UnityEngine.Rect&)
		void Register_UnityEngine_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2();
		Register_UnityEngine_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2();

		//System.Int32 UnityEngine.GUIUtility::Internal_GetGUIDepth()
		void Register_UnityEngine_GUIUtility_Internal_GetGUIDepth();
		Register_UnityEngine_GUIUtility_Internal_GetGUIDepth();

		//System.Int32 UnityEngine.GUIUtility::Internal_GetHotControl()
		void Register_UnityEngine_GUIUtility_Internal_GetHotControl();
		Register_UnityEngine_GUIUtility_Internal_GetHotControl();

		//System.Int32 UnityEngine.GUIUtility::get_keyboardControl()
		void Register_UnityEngine_GUIUtility_get_keyboardControl();
		Register_UnityEngine_GUIUtility_get_keyboardControl();

		//System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
		void Register_UnityEngine_GUIUtility_get_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_get_systemCopyBuffer();

		//System.Void UnityEngine.GUIUtility::Internal_ExitGUI()
		void Register_UnityEngine_GUIUtility_Internal_ExitGUI();
		Register_UnityEngine_GUIUtility_Internal_ExitGUI();

		//System.Void UnityEngine.GUIUtility::Internal_SetHotControl(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_SetHotControl();
		Register_UnityEngine_GUIUtility_Internal_SetHotControl();

		//System.Void UnityEngine.GUIUtility::set_keyboardControl(System.Int32)
		void Register_UnityEngine_GUIUtility_set_keyboardControl();
		Register_UnityEngine_GUIUtility_set_keyboardControl();

		//System.Void UnityEngine.GUIUtility::set_mouseUsed(System.Boolean)
		void Register_UnityEngine_GUIUtility_set_mouseUsed();
		Register_UnityEngine_GUIUtility_set_mouseUsed();

		//System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
		void Register_UnityEngine_GUIUtility_set_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_set_systemCopyBuffer();

		//UnityEngine.GUISkin UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();
		Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();

	//End Registrations for type : UnityEngine.GUIUtility

	//Start Registrations for type : UnityEngine.Input

		//System.Boolean UnityEngine.Input::GetButton(System.String)
		void Register_UnityEngine_Input_GetButton();
		Register_UnityEngine_Input_GetButton();

		//System.Boolean UnityEngine.Input::GetButtonDown(System.String)
		void Register_UnityEngine_Input_GetButtonDown();
		Register_UnityEngine_Input_GetButtonDown();

		//System.Boolean UnityEngine.Input::GetButtonUp(System.String)
		void Register_UnityEngine_Input_GetButtonUp();
		Register_UnityEngine_Input_GetButtonUp();

		//System.Boolean UnityEngine.Input::GetKeyDownInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyDownInt();
		Register_UnityEngine_Input_GetKeyDownInt();

		//System.Boolean UnityEngine.Input::GetKeyInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyInt();
		Register_UnityEngine_Input_GetKeyInt();

		//System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
		void Register_UnityEngine_Input_GetMouseButton();
		Register_UnityEngine_Input_GetMouseButton();

		//System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonDown();
		Register_UnityEngine_Input_GetMouseButtonDown();

		//System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonUp();
		Register_UnityEngine_Input_GetMouseButtonUp();

		//System.Boolean UnityEngine.Input::get_mousePresent()
		void Register_UnityEngine_Input_get_mousePresent();
		Register_UnityEngine_Input_get_mousePresent();

		//System.Int32 UnityEngine.Input::get_touchCount()
		void Register_UnityEngine_Input_get_touchCount();
		Register_UnityEngine_Input_get_touchCount();

		//System.Single UnityEngine.Input::GetAxis(System.String)
		void Register_UnityEngine_Input_GetAxis();
		Register_UnityEngine_Input_GetAxis();

		//System.Single UnityEngine.Input::GetAxisRaw(System.String)
		void Register_UnityEngine_Input_GetAxisRaw();
		Register_UnityEngine_Input_GetAxisRaw();

		//System.String UnityEngine.Input::get_compositionString()
		void Register_UnityEngine_Input_get_compositionString();
		Register_UnityEngine_Input_get_compositionString();

		//System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();
		Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();

		//System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
		void Register_UnityEngine_Input_set_imeCompositionMode();
		Register_UnityEngine_Input_set_imeCompositionMode();

		//UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
		void Register_UnityEngine_Input_GetTouch();
		Register_UnityEngine_Input_GetTouch();

		//UnityEngine.Vector2 UnityEngine.Input::get_mouseScrollDelta()
		void Register_UnityEngine_Input_get_mouseScrollDelta();
		Register_UnityEngine_Input_get_mouseScrollDelta();

		//UnityEngine.Vector3 UnityEngine.Input::get_acceleration()
		void Register_UnityEngine_Input_get_acceleration();
		Register_UnityEngine_Input_get_acceleration();

		//UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
		void Register_UnityEngine_Input_get_mousePosition();
		Register_UnityEngine_Input_get_mousePosition();

	//End Registrations for type : UnityEngine.Input

	//Start Registrations for type : UnityEngine.Joint

		//System.Void UnityEngine.Joint::INTERNAL_set_anchor(UnityEngine.Vector3&)
		void Register_UnityEngine_Joint_INTERNAL_set_anchor();
		Register_UnityEngine_Joint_INTERNAL_set_anchor();

		//System.Void UnityEngine.Joint::set_connectedBody(UnityEngine.Rigidbody)
		void Register_UnityEngine_Joint_set_connectedBody();
		Register_UnityEngine_Joint_set_connectedBody();

		//UnityEngine.Rigidbody UnityEngine.Joint::get_connectedBody()
		void Register_UnityEngine_Joint_get_connectedBody();
		Register_UnityEngine_Joint_get_connectedBody();

	//End Registrations for type : UnityEngine.Joint

	//Start Registrations for type : UnityEngine.LayerMask

		//System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
		void Register_UnityEngine_LayerMask_NameToLayer();
		Register_UnityEngine_LayerMask_NameToLayer();

		//System.String UnityEngine.LayerMask::LayerToName(System.Int32)
		void Register_UnityEngine_LayerMask_LayerToName();
		Register_UnityEngine_LayerMask_LayerToName();

	//End Registrations for type : UnityEngine.LayerMask

	//Start Registrations for type : UnityEngine.Light

		//System.Single UnityEngine.Light::get_shadowStrength()
		void Register_UnityEngine_Light_get_shadowStrength();
		Register_UnityEngine_Light_get_shadowStrength();

		//System.Void UnityEngine.Light::set_intensity(System.Single)
		void Register_UnityEngine_Light_set_intensity();
		Register_UnityEngine_Light_set_intensity();

		//System.Void UnityEngine.Light::set_shadowBias(System.Single)
		void Register_UnityEngine_Light_set_shadowBias();
		Register_UnityEngine_Light_set_shadowBias();

		//System.Void UnityEngine.Light::set_shadowStrength(System.Single)
		void Register_UnityEngine_Light_set_shadowStrength();
		Register_UnityEngine_Light_set_shadowStrength();

	//End Registrations for type : UnityEngine.Light

	//Start Registrations for type : UnityEngine.LineRenderer

		//System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetPosition(UnityEngine.LineRenderer,System.Int32,UnityEngine.Vector3&)
		void Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetPosition();
		Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetPosition();

		//System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetVertexCount(UnityEngine.LineRenderer,System.Int32)
		void Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetVertexCount();
		Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetVertexCount();

		//System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetWidth(UnityEngine.LineRenderer,System.Single,System.Single)
		void Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetWidth();
		Register_UnityEngine_LineRenderer_INTERNAL_CALL_SetWidth();

	//End Registrations for type : UnityEngine.LineRenderer

	//Start Registrations for type : UnityEngine.Material

		//System.Boolean UnityEngine.Material::HasProperty(System.Int32)
		void Register_UnityEngine_Material_HasProperty();
		Register_UnityEngine_Material_HasProperty();

		//System.Boolean UnityEngine.Material::SetPass(System.Int32)
		void Register_UnityEngine_Material_SetPass();
		Register_UnityEngine_Material_SetPass();

		//System.Int32 UnityEngine.Material::get_passCount()
		void Register_UnityEngine_Material_get_passCount();
		Register_UnityEngine_Material_get_passCount();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetColor();
		Register_UnityEngine_Material_INTERNAL_CALL_SetColor();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetMatrix(UnityEngine.Material,System.Int32,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetMatrix();
		Register_UnityEngine_Material_INTERNAL_CALL_SetMatrix();

		//System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
		void Register_UnityEngine_Material_Internal_CreateWithMaterial();
		Register_UnityEngine_Material_Internal_CreateWithMaterial();

		//System.Void UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
		void Register_UnityEngine_Material_Internal_CreateWithShader();
		Register_UnityEngine_Material_Internal_CreateWithShader();

		//System.Void UnityEngine.Material::SetBuffer(System.String,UnityEngine.ComputeBuffer)
		void Register_UnityEngine_Material_SetBuffer();
		Register_UnityEngine_Material_SetBuffer();

		//System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
		void Register_UnityEngine_Material_SetFloat();
		Register_UnityEngine_Material_SetFloat();

		//System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
		void Register_UnityEngine_Material_SetTexture();
		Register_UnityEngine_Material_SetTexture();

		//System.Void UnityEngine.Material::set_shader(UnityEngine.Shader)
		void Register_UnityEngine_Material_set_shader();
		Register_UnityEngine_Material_set_shader();

		//UnityEngine.Shader UnityEngine.Material::get_shader()
		void Register_UnityEngine_Material_get_shader();
		Register_UnityEngine_Material_get_shader();

		//UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
		void Register_UnityEngine_Material_GetTexture();
		Register_UnityEngine_Material_GetTexture();

	//End Registrations for type : UnityEngine.Material

	//Start Registrations for type : UnityEngine.Mathf

		//System.Single UnityEngine.Mathf::PerlinNoise(System.Single,System.Single)
		void Register_UnityEngine_Mathf_PerlinNoise();
		Register_UnityEngine_Mathf_PerlinNoise();

	//End Registrations for type : UnityEngine.Mathf

	//Start Registrations for type : UnityEngine.Matrix4x4

		//System.Boolean UnityEngine.Matrix4x4::INTERNAL_CALL_Invert(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Invert();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Invert();

		//System.Boolean UnityEngine.Matrix4x4::get_isIdentity()
		void Register_UnityEngine_Matrix4x4_get_isIdentity();
		Register_UnityEngine_Matrix4x4_get_isIdentity();

		//UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Inverse();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Inverse();

		//UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();

		//UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Transpose(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Transpose();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Transpose();

		//UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
		void Register_UnityEngine_Matrix4x4_Ortho();
		Register_UnityEngine_Matrix4x4_Ortho();

		//UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Perspective(System.Single,System.Single,System.Single,System.Single)
		void Register_UnityEngine_Matrix4x4_Perspective();
		Register_UnityEngine_Matrix4x4_Perspective();

	//End Registrations for type : UnityEngine.Matrix4x4

	//Start Registrations for type : UnityEngine.Mesh

		//System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
		void Register_UnityEngine_Mesh_Internal_Create();
		Register_UnityEngine_Mesh_Internal_Create();

		//System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
		void Register_UnityEngine_Mesh_set_triangles();
		Register_UnityEngine_Mesh_set_triangles();

		//System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
		void Register_UnityEngine_Mesh_set_uv();
		Register_UnityEngine_Mesh_set_uv();

		//System.Void UnityEngine.Mesh::set_uv2(UnityEngine.Vector2[])
		void Register_UnityEngine_Mesh_set_uv2();
		Register_UnityEngine_Mesh_set_uv2();

		//System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
		void Register_UnityEngine_Mesh_set_vertices();
		Register_UnityEngine_Mesh_set_vertices();

	//End Registrations for type : UnityEngine.Mesh

	//Start Registrations for type : UnityEngine.MonoBehaviour

		//System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
		void Register_UnityEngine_MonoBehaviour_IsInvoking();
		Register_UnityEngine_MonoBehaviour_IsInvoking();

		//System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
		void Register_UnityEngine_MonoBehaviour_CancelInvoke();
		Register_UnityEngine_MonoBehaviour_CancelInvoke();

		//System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
		void Register_UnityEngine_MonoBehaviour_Invoke();
		Register_UnityEngine_MonoBehaviour_Invoke();

		//System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
		void Register_UnityEngine_MonoBehaviour_StopAllCoroutines();
		Register_UnityEngine_MonoBehaviour_StopAllCoroutines();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
		void Register_UnityEngine_MonoBehaviour_StopCoroutine();
		Register_UnityEngine_MonoBehaviour_StopCoroutine();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto();
		Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
		void Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto();
		Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
		void Register_UnityEngine_MonoBehaviour_StartCoroutine();
		Register_UnityEngine_MonoBehaviour_StartCoroutine();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto();
		Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto();

	//End Registrations for type : UnityEngine.MonoBehaviour

	//Start Registrations for type : UnityEngine.Object

		//System.String UnityEngine.Object::ToString()
		void Register_UnityEngine_Object_ToString();
		Register_UnityEngine_Object_ToString();

		//System.String UnityEngine.Object::get_name()
		void Register_UnityEngine_Object_get_name();
		Register_UnityEngine_Object_get_name();

		//System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
		void Register_UnityEngine_Object_Destroy();
		Register_UnityEngine_Object_Destroy();

		//System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
		void Register_UnityEngine_Object_DestroyImmediate();
		Register_UnityEngine_Object_DestroyImmediate();

		//System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
		void Register_UnityEngine_Object_DestroyObject();
		Register_UnityEngine_Object_DestroyObject();

		//System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
		void Register_UnityEngine_Object_DontDestroyOnLoad();
		Register_UnityEngine_Object_DontDestroyOnLoad();

		//System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
		void Register_UnityEngine_Object_set_hideFlags();
		Register_UnityEngine_Object_set_hideFlags();

		//System.Void UnityEngine.Object::set_name(System.String)
		void Register_UnityEngine_Object_set_name();
		Register_UnityEngine_Object_set_name();

		//UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingle();
		Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingle();

		//UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
		void Register_UnityEngine_Object_Internal_CloneSingle();
		Register_UnityEngine_Object_Internal_CloneSingle();

		//UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
		void Register_UnityEngine_Object_FindObjectsOfType();
		Register_UnityEngine_Object_FindObjectsOfType();

	//End Registrations for type : UnityEngine.Object

	//Start Registrations for type : UnityEngine.ParticleCollisionEvent

		//UnityEngine.Collider UnityEngine.ParticleCollisionEvent::InstanceIDToCollider(System.Int32)
		void Register_UnityEngine_ParticleCollisionEvent_InstanceIDToCollider();
		Register_UnityEngine_ParticleCollisionEvent_InstanceIDToCollider();

	//End Registrations for type : UnityEngine.ParticleCollisionEvent

	//Start Registrations for type : UnityEngine.ParticleSystem

		//System.Boolean UnityEngine.ParticleSystem::get_isStopped()
		void Register_UnityEngine_ParticleSystem_get_isStopped();
		Register_UnityEngine_ParticleSystem_get_isStopped();

		//System.Single UnityEngine.ParticleSystem::get_startLifetime()
		void Register_UnityEngine_ParticleSystem_get_startLifetime();
		Register_UnityEngine_ParticleSystem_get_startLifetime();

		//System.Single UnityEngine.ParticleSystem::get_startSize()
		void Register_UnityEngine_ParticleSystem_get_startSize();
		Register_UnityEngine_ParticleSystem_get_startSize();

		//System.Single UnityEngine.ParticleSystem::get_startSpeed()
		void Register_UnityEngine_ParticleSystem_get_startSpeed();
		Register_UnityEngine_ParticleSystem_get_startSpeed();

		//System.Void UnityEngine.ParticleSystem::INTERNAL_CALL_Emit(UnityEngine.ParticleSystem,System.Int32)
		void Register_UnityEngine_ParticleSystem_INTERNAL_CALL_Emit();
		Register_UnityEngine_ParticleSystem_INTERNAL_CALL_Emit();

		//System.Void UnityEngine.ParticleSystem::Internal_Clear()
		void Register_UnityEngine_ParticleSystem_Internal_Clear();
		Register_UnityEngine_ParticleSystem_Internal_Clear();

		//System.Void UnityEngine.ParticleSystem::Internal_Play()
		void Register_UnityEngine_ParticleSystem_Internal_Play();
		Register_UnityEngine_ParticleSystem_Internal_Play();

		//System.Void UnityEngine.ParticleSystem::Internal_Stop()
		void Register_UnityEngine_ParticleSystem_Internal_Stop();
		Register_UnityEngine_ParticleSystem_Internal_Stop();

		//System.Void UnityEngine.ParticleSystem::set_enableEmission(System.Boolean)
		void Register_UnityEngine_ParticleSystem_set_enableEmission();
		Register_UnityEngine_ParticleSystem_set_enableEmission();

		//System.Void UnityEngine.ParticleSystem::set_startLifetime(System.Single)
		void Register_UnityEngine_ParticleSystem_set_startLifetime();
		Register_UnityEngine_ParticleSystem_set_startLifetime();

		//System.Void UnityEngine.ParticleSystem::set_startSize(System.Single)
		void Register_UnityEngine_ParticleSystem_set_startSize();
		Register_UnityEngine_ParticleSystem_set_startSize();

		//System.Void UnityEngine.ParticleSystem::set_startSpeed(System.Single)
		void Register_UnityEngine_ParticleSystem_set_startSpeed();
		Register_UnityEngine_ParticleSystem_set_startSpeed();

	//End Registrations for type : UnityEngine.ParticleSystem

	//Start Registrations for type : UnityEngine.ParticleSystemExtensionsImpl

		//System.Int32 UnityEngine.ParticleSystemExtensionsImpl::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,UnityEngine.ParticleCollisionEvent[])
		void Register_UnityEngine_ParticleSystemExtensionsImpl_GetCollisionEvents();
		Register_UnityEngine_ParticleSystemExtensionsImpl_GetCollisionEvents();

		//System.Int32 UnityEngine.ParticleSystemExtensionsImpl::GetSafeCollisionEventSize(UnityEngine.ParticleSystem)
		void Register_UnityEngine_ParticleSystemExtensionsImpl_GetSafeCollisionEventSize();
		Register_UnityEngine_ParticleSystemExtensionsImpl_GetSafeCollisionEventSize();

	//End Registrations for type : UnityEngine.ParticleSystemExtensionsImpl

	//Start Registrations for type : UnityEngine.Physics

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast();

		//UnityEngine.Collider[] UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32)
		void Register_UnityEngine_Physics_INTERNAL_CALL_OverlapSphere();
		Register_UnityEngine_Physics_INTERNAL_CALL_OverlapSphere();

		//UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_CapsuleCastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,System.Int32)
		void Register_UnityEngine_Physics_INTERNAL_CALL_CapsuleCastAll();
		Register_UnityEngine_Physics_INTERNAL_CALL_CapsuleCastAll();

		//UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
		void Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll();
		Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll();

	//End Registrations for type : UnityEngine.Physics

	//Start Registrations for type : UnityEngine.Physics2D

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast();

		//UnityEngine.Collider2D UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircle(UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircle();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircle();

		//UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircleAll(UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircleAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_OverlapCircleAll();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_RaycastAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_RaycastAll();

	//End Registrations for type : UnityEngine.Physics2D

	//Start Registrations for type : UnityEngine.PlayerPrefs

		//System.Boolean UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)
		void Register_UnityEngine_PlayerPrefs_TrySetFloat();
		Register_UnityEngine_PlayerPrefs_TrySetFloat();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
		void Register_UnityEngine_PlayerPrefs_TrySetInt();
		Register_UnityEngine_PlayerPrefs_TrySetInt();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
		void Register_UnityEngine_PlayerPrefs_TrySetSetString();
		Register_UnityEngine_PlayerPrefs_TrySetSetString();

		//System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
		void Register_UnityEngine_PlayerPrefs_GetInt();
		Register_UnityEngine_PlayerPrefs_GetInt();

		//System.Single UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)
		void Register_UnityEngine_PlayerPrefs_GetFloat();
		Register_UnityEngine_PlayerPrefs_GetFloat();

		//System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
		void Register_UnityEngine_PlayerPrefs_GetString();
		Register_UnityEngine_PlayerPrefs_GetString();

		//System.Void UnityEngine.PlayerPrefs::Save()
		void Register_UnityEngine_PlayerPrefs_Save();
		Register_UnityEngine_PlayerPrefs_Save();

	//End Registrations for type : UnityEngine.PlayerPrefs

	//Start Registrations for type : UnityEngine.Projector

		//System.Single UnityEngine.Projector::get_fieldOfView()
		void Register_UnityEngine_Projector_get_fieldOfView();
		Register_UnityEngine_Projector_get_fieldOfView();

		//System.Void UnityEngine.Projector::set_fieldOfView(System.Single)
		void Register_UnityEngine_Projector_set_fieldOfView();
		Register_UnityEngine_Projector_set_fieldOfView();

	//End Registrations for type : UnityEngine.Projector

	//Start Registrations for type : UnityEngine.QualitySettings

		//System.Void UnityEngine.QualitySettings::set_shadowDistance(System.Single)
		void Register_UnityEngine_QualitySettings_set_shadowDistance();
		Register_UnityEngine_QualitySettings_set_shadowDistance();

		//UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
		void Register_UnityEngine_QualitySettings_get_activeColorSpace();
		Register_UnityEngine_QualitySettings_get_activeColorSpace();

	//End Registrations for type : UnityEngine.QualitySettings

	//Start Registrations for type : UnityEngine.Quaternion

		//UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_AngleAxis();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_AngleAxis();

		//UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_FromToRotation();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_FromToRotation();

		//UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad();

		//UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse();

		//UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Lerp();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Lerp();

		//UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation();

		//UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Slerp();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Slerp();

		//UnityEngine.Vector3 UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad();

	//End Registrations for type : UnityEngine.Quaternion

	//Start Registrations for type : UnityEngine.Random

		//System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
		void Register_UnityEngine_Random_RandomRangeInt();
		Register_UnityEngine_Random_RandomRangeInt();

		//System.Single UnityEngine.Random::Range(System.Single,System.Single)
		void Register_UnityEngine_Random_Range();
		Register_UnityEngine_Random_Range();

		//System.Single UnityEngine.Random::get_value()
		void Register_UnityEngine_Random_get_value();
		Register_UnityEngine_Random_get_value();

		//UnityEngine.Quaternion UnityEngine.Random::get_rotation()
		void Register_UnityEngine_Random_get_rotation();
		Register_UnityEngine_Random_get_rotation();

		//UnityEngine.Vector3 UnityEngine.Random::get_insideUnitSphere()
		void Register_UnityEngine_Random_get_insideUnitSphere();
		Register_UnityEngine_Random_get_insideUnitSphere();

		//UnityEngine.Vector3 UnityEngine.Random::get_onUnitSphere()
		void Register_UnityEngine_Random_get_onUnitSphere();
		Register_UnityEngine_Random_get_onUnitSphere();

	//End Registrations for type : UnityEngine.Random

	//Start Registrations for type : UnityEngine.RectOffset

		//System.Int32 UnityEngine.RectOffset::get_bottom()
		void Register_UnityEngine_RectOffset_get_bottom();
		Register_UnityEngine_RectOffset_get_bottom();

		//System.Int32 UnityEngine.RectOffset::get_horizontal()
		void Register_UnityEngine_RectOffset_get_horizontal();
		Register_UnityEngine_RectOffset_get_horizontal();

		//System.Int32 UnityEngine.RectOffset::get_left()
		void Register_UnityEngine_RectOffset_get_left();
		Register_UnityEngine_RectOffset_get_left();

		//System.Int32 UnityEngine.RectOffset::get_right()
		void Register_UnityEngine_RectOffset_get_right();
		Register_UnityEngine_RectOffset_get_right();

		//System.Int32 UnityEngine.RectOffset::get_top()
		void Register_UnityEngine_RectOffset_get_top();
		Register_UnityEngine_RectOffset_get_top();

		//System.Int32 UnityEngine.RectOffset::get_vertical()
		void Register_UnityEngine_RectOffset_get_vertical();
		Register_UnityEngine_RectOffset_get_vertical();

		//System.Void UnityEngine.RectOffset::Cleanup()
		void Register_UnityEngine_RectOffset_Cleanup();
		Register_UnityEngine_RectOffset_Cleanup();

		//System.Void UnityEngine.RectOffset::Init()
		void Register_UnityEngine_RectOffset_Init();
		Register_UnityEngine_RectOffset_Init();

		//System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
		void Register_UnityEngine_RectOffset_set_bottom();
		Register_UnityEngine_RectOffset_set_bottom();

		//System.Void UnityEngine.RectOffset::set_left(System.Int32)
		void Register_UnityEngine_RectOffset_set_left();
		Register_UnityEngine_RectOffset_set_left();

		//System.Void UnityEngine.RectOffset::set_right(System.Int32)
		void Register_UnityEngine_RectOffset_set_right();
		Register_UnityEngine_RectOffset_set_right();

		//System.Void UnityEngine.RectOffset::set_top(System.Int32)
		void Register_UnityEngine_RectOffset_set_top();
		Register_UnityEngine_RectOffset_set_top();

		//UnityEngine.Rect UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&)
		void Register_UnityEngine_RectOffset_INTERNAL_CALL_Remove();
		Register_UnityEngine_RectOffset_INTERNAL_CALL_Remove();

	//End Registrations for type : UnityEngine.RectOffset

	//Start Registrations for type : UnityEngine.RectTransform

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_pivot();
		Register_UnityEngine_RectTransform_INTERNAL_get_pivot();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_rect();
		Register_UnityEngine_RectTransform_INTERNAL_get_rect();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta();
		Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_pivot();
		Register_UnityEngine_RectTransform_INTERNAL_set_pivot();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta();
		Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta();

	//End Registrations for type : UnityEngine.RectTransform

	//Start Registrations for type : UnityEngine.RectTransformUtility

		//System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint();

		//System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint();

		//UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
		void Register_UnityEngine_RectTransformUtility_PixelAdjustRect();
		Register_UnityEngine_RectTransformUtility_PixelAdjustRect();

	//End Registrations for type : UnityEngine.RectTransformUtility

	//Start Registrations for type : UnityEngine.Renderer

		//System.Boolean UnityEngine.Renderer::get_enabled()
		void Register_UnityEngine_Renderer_get_enabled();
		Register_UnityEngine_Renderer_get_enabled();

		//System.Int32 UnityEngine.Renderer::get_sortingLayerID()
		void Register_UnityEngine_Renderer_get_sortingLayerID();
		Register_UnityEngine_Renderer_get_sortingLayerID();

		//System.Int32 UnityEngine.Renderer::get_sortingOrder()
		void Register_UnityEngine_Renderer_get_sortingOrder();
		Register_UnityEngine_Renderer_get_sortingOrder();

		//System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
		void Register_UnityEngine_Renderer_set_enabled();
		Register_UnityEngine_Renderer_set_enabled();

		//System.Void UnityEngine.Renderer::set_materials(UnityEngine.Material[])
		void Register_UnityEngine_Renderer_set_materials();
		Register_UnityEngine_Renderer_set_materials();

		//UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
		void Register_UnityEngine_Renderer_get_bounds();
		Register_UnityEngine_Renderer_get_bounds();

		//UnityEngine.Material[] UnityEngine.Renderer::get_materials()
		void Register_UnityEngine_Renderer_get_materials();
		Register_UnityEngine_Renderer_get_materials();

		//UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
		void Register_UnityEngine_Renderer_get_sharedMaterials();
		Register_UnityEngine_Renderer_get_sharedMaterials();

	//End Registrations for type : UnityEngine.Renderer

	//Start Registrations for type : UnityEngine.Rendering.SphericalHarmonicsL2

		//System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddAmbientLightInternal(UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)
		void Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal();
		Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal();

		//System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddDirectionalLightInternal(UnityEngine.Vector3&,UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)
		void Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal();
		Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal();

		//System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_ClearInternal(UnityEngine.Rendering.SphericalHarmonicsL2&)
		void Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal();
		Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal();

	//End Registrations for type : UnityEngine.Rendering.SphericalHarmonicsL2

	//Start Registrations for type : UnityEngine.RenderSettings

		//System.Single UnityEngine.RenderSettings::get_fogDensity()
		void Register_UnityEngine_RenderSettings_get_fogDensity();
		Register_UnityEngine_RenderSettings_get_fogDensity();

		//System.Single UnityEngine.RenderSettings::get_fogEndDistance()
		void Register_UnityEngine_RenderSettings_get_fogEndDistance();
		Register_UnityEngine_RenderSettings_get_fogEndDistance();

		//System.Single UnityEngine.RenderSettings::get_fogStartDistance()
		void Register_UnityEngine_RenderSettings_get_fogStartDistance();
		Register_UnityEngine_RenderSettings_get_fogStartDistance();

		//UnityEngine.FogMode UnityEngine.RenderSettings::get_fogMode()
		void Register_UnityEngine_RenderSettings_get_fogMode();
		Register_UnityEngine_RenderSettings_get_fogMode();

	//End Registrations for type : UnityEngine.RenderSettings

	//Start Registrations for type : UnityEngine.RenderTexture

		//System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_GetHeight();
		Register_UnityEngine_RenderTexture_Internal_GetHeight();

		//System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_GetWidth();
		Register_UnityEngine_RenderTexture_Internal_GetWidth();

		//System.Void UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)
		void Register_UnityEngine_RenderTexture_GetColorBuffer();
		Register_UnityEngine_RenderTexture_GetColorBuffer();

		//System.Void UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)
		void Register_UnityEngine_RenderTexture_GetDepthBuffer();
		Register_UnityEngine_RenderTexture_GetDepthBuffer();

		//System.Void UnityEngine.RenderTexture::INTERNAL_CALL_DiscardContents(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_DiscardContents();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_DiscardContents();

		//System.Void UnityEngine.RenderTexture::INTERNAL_CALL_MarkRestoreExpected(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_INTERNAL_CALL_MarkRestoreExpected();
		Register_UnityEngine_RenderTexture_INTERNAL_CALL_MarkRestoreExpected();

		//System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_CreateRenderTexture();
		Register_UnityEngine_RenderTexture_Internal_CreateRenderTexture();

		//System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
		void Register_UnityEngine_RenderTexture_Internal_SetHeight();
		Register_UnityEngine_RenderTexture_Internal_SetHeight();

		//System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
		void Register_UnityEngine_RenderTexture_Internal_SetSRGBReadWrite();
		Register_UnityEngine_RenderTexture_Internal_SetSRGBReadWrite();

		//System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
		void Register_UnityEngine_RenderTexture_Internal_SetWidth();
		Register_UnityEngine_RenderTexture_Internal_SetWidth();

		//System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_ReleaseTemporary();
		Register_UnityEngine_RenderTexture_ReleaseTemporary();

		//System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_set_active();
		Register_UnityEngine_RenderTexture_set_active();

		//System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
		void Register_UnityEngine_RenderTexture_set_depth();
		Register_UnityEngine_RenderTexture_set_depth();

		//System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
		void Register_UnityEngine_RenderTexture_set_format();
		Register_UnityEngine_RenderTexture_set_format();

		//UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)
		void Register_UnityEngine_RenderTexture_GetTemporary();
		Register_UnityEngine_RenderTexture_GetTemporary();

		//UnityEngine.RenderTextureFormat UnityEngine.RenderTexture::get_format()
		void Register_UnityEngine_RenderTexture_get_format();
		Register_UnityEngine_RenderTexture_get_format();

	//End Registrations for type : UnityEngine.RenderTexture

	//Start Registrations for type : UnityEngine.Resources

		//UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
		void Register_UnityEngine_Resources_Load();
		Register_UnityEngine_Resources_Load();

	//End Registrations for type : UnityEngine.Resources

	//Start Registrations for type : UnityEngine.Rigidbody

		//System.Boolean UnityEngine.Rigidbody::get_isKinematic()
		void Register_UnityEngine_Rigidbody_get_isKinematic();
		Register_UnityEngine_Rigidbody_get_isKinematic();

		//System.Single UnityEngine.Rigidbody::get_angularDrag()
		void Register_UnityEngine_Rigidbody_get_angularDrag();
		Register_UnityEngine_Rigidbody_get_angularDrag();

		//System.Single UnityEngine.Rigidbody::get_drag()
		void Register_UnityEngine_Rigidbody_get_drag();
		Register_UnityEngine_Rigidbody_get_drag();

		//System.Single UnityEngine.Rigidbody::get_mass()
		void Register_UnityEngine_Rigidbody_get_mass();
		Register_UnityEngine_Rigidbody_get_mass();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddExplosionForce();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddExplosionForce();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForce();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForce();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForceAtPosition();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForceAtPosition();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_velocity();
		Register_UnityEngine_Rigidbody_INTERNAL_get_velocity();

		//System.Void UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_set_angularVelocity();
		Register_UnityEngine_Rigidbody_INTERNAL_set_angularVelocity();

		//System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_set_velocity();
		Register_UnityEngine_Rigidbody_INTERNAL_set_velocity();

		//System.Void UnityEngine.Rigidbody::set_angularDrag(System.Single)
		void Register_UnityEngine_Rigidbody_set_angularDrag();
		Register_UnityEngine_Rigidbody_set_angularDrag();

		//System.Void UnityEngine.Rigidbody::set_drag(System.Single)
		void Register_UnityEngine_Rigidbody_set_drag();
		Register_UnityEngine_Rigidbody_set_drag();

		//System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_freezeRotation();
		Register_UnityEngine_Rigidbody_set_freezeRotation();

		//System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_isKinematic();
		Register_UnityEngine_Rigidbody_set_isKinematic();

	//End Registrations for type : UnityEngine.Rigidbody

	//Start Registrations for type : UnityEngine.Rigidbody2D

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForce();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForce();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_get_velocity();
		Register_UnityEngine_Rigidbody2D_INTERNAL_get_velocity();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_set_velocity();
		Register_UnityEngine_Rigidbody2D_INTERNAL_set_velocity();

	//End Registrations for type : UnityEngine.Rigidbody2D

	//Start Registrations for type : UnityEngine.Screen

		//System.Int32 UnityEngine.Screen::get_height()
		void Register_UnityEngine_Screen_get_height();
		Register_UnityEngine_Screen_get_height();

		//System.Int32 UnityEngine.Screen::get_width()
		void Register_UnityEngine_Screen_get_width();
		Register_UnityEngine_Screen_get_width();

		//System.Single UnityEngine.Screen::get_dpi()
		void Register_UnityEngine_Screen_get_dpi();
		Register_UnityEngine_Screen_get_dpi();

	//End Registrations for type : UnityEngine.Screen

	//Start Registrations for type : UnityEngine.ScriptableObject

		//System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
		void Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();
		Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();

		//UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
		void Register_UnityEngine_ScriptableObject_CreateInstance();
		Register_UnityEngine_ScriptableObject_CreateInstance();

		//UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
		void Register_UnityEngine_ScriptableObject_CreateInstanceFromType();
		Register_UnityEngine_ScriptableObject_CreateInstanceFromType();

	//End Registrations for type : UnityEngine.ScriptableObject

	//Start Registrations for type : UnityEngine.Shader

		//System.Boolean UnityEngine.Shader::get_isSupported()
		void Register_UnityEngine_Shader_get_isSupported();
		Register_UnityEngine_Shader_get_isSupported();

		//System.Int32 UnityEngine.Shader::PropertyToID(System.String)
		void Register_UnityEngine_Shader_PropertyToID();
		Register_UnityEngine_Shader_PropertyToID();

	//End Registrations for type : UnityEngine.Shader

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();

		//UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScoresWithUsers();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScoresWithUsers();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

	//Start Registrations for type : UnityEngine.SphereCollider

		//System.Single UnityEngine.SphereCollider::get_radius()
		void Register_UnityEngine_SphereCollider_get_radius();
		Register_UnityEngine_SphereCollider_get_radius();

		//System.Void UnityEngine.SphereCollider::INTERNAL_get_center(UnityEngine.Vector3&)
		void Register_UnityEngine_SphereCollider_INTERNAL_get_center();
		Register_UnityEngine_SphereCollider_INTERNAL_get_center();

		//System.Void UnityEngine.SphereCollider::INTERNAL_set_center(UnityEngine.Vector3&)
		void Register_UnityEngine_SphereCollider_INTERNAL_set_center();
		Register_UnityEngine_SphereCollider_INTERNAL_set_center();

		//System.Void UnityEngine.SphereCollider::set_radius(System.Single)
		void Register_UnityEngine_SphereCollider_set_radius();
		Register_UnityEngine_SphereCollider_set_radius();

	//End Registrations for type : UnityEngine.SphereCollider

	//Start Registrations for type : UnityEngine.SpringJoint

		//System.Void UnityEngine.SpringJoint::set_damper(System.Single)
		void Register_UnityEngine_SpringJoint_set_damper();
		Register_UnityEngine_SpringJoint_set_damper();

		//System.Void UnityEngine.SpringJoint::set_maxDistance(System.Single)
		void Register_UnityEngine_SpringJoint_set_maxDistance();
		Register_UnityEngine_SpringJoint_set_maxDistance();

		//System.Void UnityEngine.SpringJoint::set_spring(System.Single)
		void Register_UnityEngine_SpringJoint_set_spring();
		Register_UnityEngine_SpringJoint_set_spring();

	//End Registrations for type : UnityEngine.SpringJoint

	//Start Registrations for type : UnityEngine.Sprite

		//System.Single UnityEngine.Sprite::get_pixelsPerUnit()
		void Register_UnityEngine_Sprite_get_pixelsPerUnit();
		Register_UnityEngine_Sprite_get_pixelsPerUnit();

		//UnityEngine.Rect UnityEngine.Sprite::get_rect()
		void Register_UnityEngine_Sprite_get_rect();
		Register_UnityEngine_Sprite_get_rect();

		//UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
		void Register_UnityEngine_Sprite_get_textureRect();
		Register_UnityEngine_Sprite_get_textureRect();

		//UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
		void Register_UnityEngine_Sprite_get_texture();
		Register_UnityEngine_Sprite_get_texture();

		//UnityEngine.Vector4 UnityEngine.Sprite::get_border()
		void Register_UnityEngine_Sprite_get_border();
		Register_UnityEngine_Sprite_get_border();

	//End Registrations for type : UnityEngine.Sprite

	//Start Registrations for type : UnityEngine.Sprites.DataUtility

		//System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
		void Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize();
		Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize();

		//UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
		void Register_UnityEngine_Sprites_DataUtility_GetInnerUV();
		Register_UnityEngine_Sprites_DataUtility_GetInnerUV();

		//UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
		void Register_UnityEngine_Sprites_DataUtility_GetOuterUV();
		Register_UnityEngine_Sprites_DataUtility_GetOuterUV();

		//UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
		void Register_UnityEngine_Sprites_DataUtility_GetPadding();
		Register_UnityEngine_Sprites_DataUtility_GetPadding();

	//End Registrations for type : UnityEngine.Sprites.DataUtility

	//Start Registrations for type : UnityEngine.SystemInfo

		//System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
		void Register_UnityEngine_SystemInfo_SupportsRenderTextureFormat();
		Register_UnityEngine_SystemInfo_SupportsRenderTextureFormat();

		//System.Boolean UnityEngine.SystemInfo::get_supports3DTextures()
		void Register_UnityEngine_SystemInfo_get_supports3DTextures();
		Register_UnityEngine_SystemInfo_get_supports3DTextures();

		//System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
		void Register_UnityEngine_SystemInfo_get_supportsComputeShaders();
		Register_UnityEngine_SystemInfo_get_supportsComputeShaders();

		//System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
		void Register_UnityEngine_SystemInfo_get_supportsImageEffects();
		Register_UnityEngine_SystemInfo_get_supportsImageEffects();

		//System.Boolean UnityEngine.SystemInfo::get_supportsRenderTextures()
		void Register_UnityEngine_SystemInfo_get_supportsRenderTextures();
		Register_UnityEngine_SystemInfo_get_supportsRenderTextures();

		//System.Int32 UnityEngine.SystemInfo::get_graphicsMemorySize()
		void Register_UnityEngine_SystemInfo_get_graphicsMemorySize();
		Register_UnityEngine_SystemInfo_get_graphicsMemorySize();

		//System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
		void Register_UnityEngine_SystemInfo_get_graphicsShaderLevel();
		Register_UnityEngine_SystemInfo_get_graphicsShaderLevel();

		//System.Int32 UnityEngine.SystemInfo::get_maxTextureSize()
		void Register_UnityEngine_SystemInfo_get_maxTextureSize();
		Register_UnityEngine_SystemInfo_get_maxTextureSize();

		//System.Int32 UnityEngine.SystemInfo::get_systemMemorySize()
		void Register_UnityEngine_SystemInfo_get_systemMemorySize();
		Register_UnityEngine_SystemInfo_get_systemMemorySize();

		//System.String UnityEngine.SystemInfo::get_deviceModel()
		void Register_UnityEngine_SystemInfo_get_deviceModel();
		Register_UnityEngine_SystemInfo_get_deviceModel();

		//System.String UnityEngine.SystemInfo::get_deviceName()
		void Register_UnityEngine_SystemInfo_get_deviceName();
		Register_UnityEngine_SystemInfo_get_deviceName();

		//System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
		void Register_UnityEngine_SystemInfo_get_deviceUniqueIdentifier();
		Register_UnityEngine_SystemInfo_get_deviceUniqueIdentifier();

		//System.String UnityEngine.SystemInfo::get_graphicsDeviceName()
		void Register_UnityEngine_SystemInfo_get_graphicsDeviceName();
		Register_UnityEngine_SystemInfo_get_graphicsDeviceName();

		//System.String UnityEngine.SystemInfo::get_operatingSystem()
		void Register_UnityEngine_SystemInfo_get_operatingSystem();
		Register_UnityEngine_SystemInfo_get_operatingSystem();

		//UnityEngine.DeviceType UnityEngine.SystemInfo::get_deviceType()
		void Register_UnityEngine_SystemInfo_get_deviceType();
		Register_UnityEngine_SystemInfo_get_deviceType();

	//End Registrations for type : UnityEngine.SystemInfo

	//Start Registrations for type : UnityEngine.TextGenerator

		//System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
		void Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp();
		Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp();

		//System.Int32 UnityEngine.TextGenerator::get_characterCount()
		void Register_UnityEngine_TextGenerator_get_characterCount();
		Register_UnityEngine_TextGenerator_get_characterCount();

		//System.Int32 UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()
		void Register_UnityEngine_TextGenerator_get_fontSizeUsedForBestFit();
		Register_UnityEngine_TextGenerator_get_fontSizeUsedForBestFit();

		//System.Int32 UnityEngine.TextGenerator::get_lineCount()
		void Register_UnityEngine_TextGenerator_get_lineCount();
		Register_UnityEngine_TextGenerator_get_lineCount();

		//System.Int32 UnityEngine.TextGenerator::get_vertexCount()
		void Register_UnityEngine_TextGenerator_get_vertexCount();
		Register_UnityEngine_TextGenerator_get_vertexCount();

		//System.Void UnityEngine.TextGenerator::Dispose_cpp()
		void Register_UnityEngine_TextGenerator_Dispose_cpp();
		Register_UnityEngine_TextGenerator_Dispose_cpp();

		//System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetCharactersInternal();
		Register_UnityEngine_TextGenerator_GetCharactersInternal();

		//System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetLinesInternal();
		Register_UnityEngine_TextGenerator_GetLinesInternal();

		//System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetVerticesInternal();
		Register_UnityEngine_TextGenerator_GetVerticesInternal();

		//System.Void UnityEngine.TextGenerator::Init()
		void Register_UnityEngine_TextGenerator_Init();
		Register_UnityEngine_TextGenerator_Init();

		//UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
		void Register_UnityEngine_TextGenerator_get_rectExtents();
		Register_UnityEngine_TextGenerator_get_rectExtents();

		//UnityEngine.UICharInfo[] UnityEngine.TextGenerator::GetCharactersArray()
		void Register_UnityEngine_TextGenerator_GetCharactersArray();
		Register_UnityEngine_TextGenerator_GetCharactersArray();

		//UnityEngine.UILineInfo[] UnityEngine.TextGenerator::GetLinesArray()
		void Register_UnityEngine_TextGenerator_GetLinesArray();
		Register_UnityEngine_TextGenerator_GetLinesArray();

		//UnityEngine.UIVertex[] UnityEngine.TextGenerator::GetVerticesArray()
		void Register_UnityEngine_TextGenerator_GetVerticesArray();
		Register_UnityEngine_TextGenerator_GetVerticesArray();

	//End Registrations for type : UnityEngine.TextGenerator

	//Start Registrations for type : UnityEngine.Texture

		//System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
		void Register_UnityEngine_Texture_Internal_GetHeight();
		Register_UnityEngine_Texture_Internal_GetHeight();

		//System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
		void Register_UnityEngine_Texture_Internal_GetWidth();
		Register_UnityEngine_Texture_Internal_GetWidth();

		//System.Void UnityEngine.Texture::set_anisoLevel(System.Int32)
		void Register_UnityEngine_Texture_set_anisoLevel();
		Register_UnityEngine_Texture_set_anisoLevel();

		//System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
		void Register_UnityEngine_Texture_set_filterMode();
		Register_UnityEngine_Texture_set_filterMode();

		//System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
		void Register_UnityEngine_Texture_set_wrapMode();
		Register_UnityEngine_Texture_set_wrapMode();

		//UnityEngine.Vector2 UnityEngine.Texture::get_texelSize()
		void Register_UnityEngine_Texture_get_texelSize();
		Register_UnityEngine_Texture_get_texelSize();

	//End Registrations for type : UnityEngine.Texture

	//Start Registrations for type : UnityEngine.Texture2D

		//System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
		void Register_UnityEngine_Texture2D_Apply();
		Register_UnityEngine_Texture2D_Apply();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_SetPixel();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_SetPixel();

		//System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
		void Register_UnityEngine_Texture2D_Internal_Create();
		Register_UnityEngine_Texture2D_Internal_Create();

		//UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
		void Register_UnityEngine_Texture2D_GetPixelBilinear();
		Register_UnityEngine_Texture2D_GetPixelBilinear();

		//UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Texture2D_GetPixels();
		Register_UnityEngine_Texture2D_GetPixels();

		//UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
		void Register_UnityEngine_Texture2D_get_whiteTexture();
		Register_UnityEngine_Texture2D_get_whiteTexture();

	//End Registrations for type : UnityEngine.Texture2D

	//Start Registrations for type : UnityEngine.Texture3D

		//System.Void UnityEngine.Texture3D::Apply(System.Boolean)
		void Register_UnityEngine_Texture3D_Apply();
		Register_UnityEngine_Texture3D_Apply();

		//System.Void UnityEngine.Texture3D::Internal_Create(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
		void Register_UnityEngine_Texture3D_Internal_Create();
		Register_UnityEngine_Texture3D_Internal_Create();

		//System.Void UnityEngine.Texture3D::SetPixels(UnityEngine.Color[],System.Int32)
		void Register_UnityEngine_Texture3D_SetPixels();
		Register_UnityEngine_Texture3D_SetPixels();

	//End Registrations for type : UnityEngine.Texture3D

	//Start Registrations for type : UnityEngine.Time

		//System.Int32 UnityEngine.Time::get_frameCount()
		void Register_UnityEngine_Time_get_frameCount();
		Register_UnityEngine_Time_get_frameCount();

		//System.Single UnityEngine.Time::get_deltaTime()
		void Register_UnityEngine_Time_get_deltaTime();
		Register_UnityEngine_Time_get_deltaTime();

		//System.Single UnityEngine.Time::get_fixedDeltaTime()
		void Register_UnityEngine_Time_get_fixedDeltaTime();
		Register_UnityEngine_Time_get_fixedDeltaTime();

		//System.Single UnityEngine.Time::get_realtimeSinceStartup()
		void Register_UnityEngine_Time_get_realtimeSinceStartup();
		Register_UnityEngine_Time_get_realtimeSinceStartup();

		//System.Single UnityEngine.Time::get_time()
		void Register_UnityEngine_Time_get_time();
		Register_UnityEngine_Time_get_time();

		//System.Single UnityEngine.Time::get_timeScale()
		void Register_UnityEngine_Time_get_timeScale();
		Register_UnityEngine_Time_get_timeScale();

		//System.Single UnityEngine.Time::get_unscaledDeltaTime()
		void Register_UnityEngine_Time_get_unscaledDeltaTime();
		Register_UnityEngine_Time_get_unscaledDeltaTime();

		//System.Single UnityEngine.Time::get_unscaledTime()
		void Register_UnityEngine_Time_get_unscaledTime();
		Register_UnityEngine_Time_get_unscaledTime();

		//System.Void UnityEngine.Time::set_timeScale(System.Single)
		void Register_UnityEngine_Time_set_timeScale();
		Register_UnityEngine_Time_set_timeScale();

	//End Registrations for type : UnityEngine.Time

	//Start Registrations for type : UnityEngine.TouchScreenKeyboard

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
		void Register_UnityEngine_TouchScreenKeyboard_get_active();
		Register_UnityEngine_TouchScreenKeyboard_get_active();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
		void Register_UnityEngine_TouchScreenKeyboard_get_done();
		Register_UnityEngine_TouchScreenKeyboard_get_done();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
		void Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled();
		Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled();

		//System.String UnityEngine.TouchScreenKeyboard::get_text()
		void Register_UnityEngine_TouchScreenKeyboard_get_text();
		Register_UnityEngine_TouchScreenKeyboard_get_text();

		//System.Void UnityEngine.TouchScreenKeyboard::Destroy()
		void Register_UnityEngine_TouchScreenKeyboard_Destroy();
		Register_UnityEngine_TouchScreenKeyboard_Destroy();

		//System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
		void Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();
		Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();

		//System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
		void Register_UnityEngine_TouchScreenKeyboard_set_active();
		Register_UnityEngine_TouchScreenKeyboard_set_active();

		//System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
		void Register_UnityEngine_TouchScreenKeyboard_set_hideInput();
		Register_UnityEngine_TouchScreenKeyboard_set_hideInput();

		//System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
		void Register_UnityEngine_TouchScreenKeyboard_set_text();
		Register_UnityEngine_TouchScreenKeyboard_set_text();

	//End Registrations for type : UnityEngine.TouchScreenKeyboard

	//Start Registrations for type : UnityEngine.Transform

		//System.Int32 UnityEngine.Transform::get_childCount()
		void Register_UnityEngine_Transform_get_childCount();
		Register_UnityEngine_Transform_get_childCount();

		//System.Void UnityEngine.Transform::DetachChildren()
		void Register_UnityEngine_Transform_DetachChildren();
		Register_UnityEngine_Transform_DetachChildren();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_LookAt();
		Register_UnityEngine_Transform_INTERNAL_CALL_LookAt();

		//System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_localPosition();
		Register_UnityEngine_Transform_INTERNAL_get_localPosition();

		//System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_get_localRotation();
		Register_UnityEngine_Transform_INTERNAL_get_localRotation();

		//System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_localScale();
		Register_UnityEngine_Transform_INTERNAL_get_localScale();

		//System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_position();
		Register_UnityEngine_Transform_INTERNAL_get_position();

		//System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_get_rotation();
		Register_UnityEngine_Transform_INTERNAL_get_rotation();

		//System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix();
		Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix();

		//System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_localPosition();
		Register_UnityEngine_Transform_INTERNAL_set_localPosition();

		//System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_set_localRotation();
		Register_UnityEngine_Transform_INTERNAL_set_localRotation();

		//System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_localScale();
		Register_UnityEngine_Transform_INTERNAL_set_localScale();

		//System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_position();
		Register_UnityEngine_Transform_INTERNAL_set_position();

		//System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_set_rotation();
		Register_UnityEngine_Transform_INTERNAL_set_rotation();

		//System.Void UnityEngine.Transform::SetAsFirstSibling()
		void Register_UnityEngine_Transform_SetAsFirstSibling();
		Register_UnityEngine_Transform_SetAsFirstSibling();

		//System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_Transform_SetParent();
		Register_UnityEngine_Transform_SetParent();

		//System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
		void Register_UnityEngine_Transform_set_parentInternal();
		Register_UnityEngine_Transform_set_parentInternal();

		//UnityEngine.Transform UnityEngine.Transform::Find(System.String)
		void Register_UnityEngine_Transform_Find();
		Register_UnityEngine_Transform_Find();

		//UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
		void Register_UnityEngine_Transform_GetChild();
		Register_UnityEngine_Transform_GetChild();

		//UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
		void Register_UnityEngine_Transform_get_parentInternal();
		Register_UnityEngine_Transform_get_parentInternal();

		//UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint();
		Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint();

		//UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection();

		//UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint();

	//End Registrations for type : UnityEngine.Transform

	//Start Registrations for type : UnityEngine.Vector3

		//UnityEngine.Vector3 UnityEngine.Vector3::INTERNAL_CALL_Slerp(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Vector3_INTERNAL_CALL_Slerp();
		Register_UnityEngine_Vector3_INTERNAL_CALL_Slerp();

	//End Registrations for type : UnityEngine.Vector3

	//Start Registrations for type : UnityEngine.WWW

		//System.Boolean UnityEngine.WWW::get_isDone()
		void Register_UnityEngine_WWW_get_isDone();
		Register_UnityEngine_WWW_get_isDone();

		//System.Byte[] UnityEngine.WWW::get_bytes()
		void Register_UnityEngine_WWW_get_bytes();
		Register_UnityEngine_WWW_get_bytes();

		//System.String UnityEngine.WWW::get_error()
		void Register_UnityEngine_WWW_get_error();
		Register_UnityEngine_WWW_get_error();

		//System.String UnityEngine.WWW::get_responseHeadersString()
		void Register_UnityEngine_WWW_get_responseHeadersString();
		Register_UnityEngine_WWW_get_responseHeadersString();

		//System.Void UnityEngine.WWW::DestroyWWW(System.Boolean)
		void Register_UnityEngine_WWW_DestroyWWW();
		Register_UnityEngine_WWW_DestroyWWW();

		//System.Void UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])
		void Register_UnityEngine_WWW_InitWWW();
		Register_UnityEngine_WWW_InitWWW();

	//End Registrations for type : UnityEngine.WWW

}
