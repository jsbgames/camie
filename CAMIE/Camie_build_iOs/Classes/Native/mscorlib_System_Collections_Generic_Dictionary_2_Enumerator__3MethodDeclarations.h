﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct Enumerator_t2822;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2814;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m13691_gshared (Enumerator_t2822 * __this, Dictionary_2_t2814 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m13691(__this, ___dictionary, method) (( void (*) (Enumerator_t2822 *, Dictionary_2_t2814 *, const MethodInfo*))Enumerator__ctor_m13691_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13692_gshared (Enumerator_t2822 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13692(__this, method) (( Object_t * (*) (Enumerator_t2822 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13692_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1430  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13693_gshared (Enumerator_t2822 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13693(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t2822 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13693_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13694_gshared (Enumerator_t2822 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13694(__this, method) (( Object_t * (*) (Enumerator_t2822 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13694_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13695_gshared (Enumerator_t2822 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13695(__this, method) (( Object_t * (*) (Enumerator_t2822 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13695_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13696_gshared (Enumerator_t2822 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13696(__this, method) (( bool (*) (Enumerator_t2822 *, const MethodInfo*))Enumerator_MoveNext_m13696_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2815  Enumerator_get_Current_m13697_gshared (Enumerator_t2822 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13697(__this, method) (( KeyValuePair_2_t2815  (*) (Enumerator_t2822 *, const MethodInfo*))Enumerator_get_Current_m13697_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m13698_gshared (Enumerator_t2822 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m13698(__this, method) (( Object_t * (*) (Enumerator_t2822 *, const MethodInfo*))Enumerator_get_CurrentKey_m13698_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m13699_gshared (Enumerator_t2822 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m13699(__this, method) (( Object_t * (*) (Enumerator_t2822 *, const MethodInfo*))Enumerator_get_CurrentValue_m13699_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m13700_gshared (Enumerator_t2822 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m13700(__this, method) (( void (*) (Enumerator_t2822 *, const MethodInfo*))Enumerator_VerifyState_m13700_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m13701_gshared (Enumerator_t2822 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m13701(__this, method) (( void (*) (Enumerator_t2822 *, const MethodInfo*))Enumerator_VerifyCurrent_m13701_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m13702_gshared (Enumerator_t2822 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13702(__this, method) (( void (*) (Enumerator_t2822 *, const MethodInfo*))Enumerator_Dispose_m13702_gshared)(__this, method)
