﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.Label
struct Label_t1864;
// System.Object
struct Object_t;

// System.Void System.Reflection.Emit.Label::.ctor(System.Int32)
extern "C" void Label__ctor_m9912 (Label_t1864 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.Label::Equals(System.Object)
extern "C" bool Label_Equals_m9913 (Label_t1864 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.Label::GetHashCode()
extern "C" int32_t Label_GetHashCode_m9914 (Label_t1864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
