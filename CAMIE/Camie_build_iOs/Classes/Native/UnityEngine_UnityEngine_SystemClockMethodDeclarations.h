﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SystemClock
struct SystemClock_t996;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void UnityEngine.SystemClock::.cctor()
extern "C" void SystemClock__cctor_m5014 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.SystemClock::get_now()
extern "C" DateTime_t406  SystemClock_get_now_m5015 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
