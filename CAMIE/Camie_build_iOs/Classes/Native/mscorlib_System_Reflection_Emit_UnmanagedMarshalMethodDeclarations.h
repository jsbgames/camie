﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1852;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1671;

// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
extern "C" MarshalAsAttribute_t1671 * UnmanagedMarshal_ToMarshalAsAttribute_m10081 (UnmanagedMarshal_t1852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
