﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB
struct U3CReloadLevelU3Ec__IteratorB_t204;
// System.Object
struct Object_t;

// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::.ctor()
extern "C" void U3CReloadLevelU3Ec__IteratorB__ctor_m553 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CReloadLevelU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m554 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CReloadLevelU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m555 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::MoveNext()
extern "C" bool U3CReloadLevelU3Ec__IteratorB_MoveNext_m556 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::Dispose()
extern "C" void U3CReloadLevelU3Ec__IteratorB_Dispose_m557 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::Reset()
extern "C" void U3CReloadLevelU3Ec__IteratorB_Reset_m558 (U3CReloadLevelU3Ec__IteratorB_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
