﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t1097;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t3805;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Type>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m5212(__this, method) (( void (*) (Stack_1_t1097 *, const MethodInfo*))Stack_1__ctor_m16469_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m21290(__this, method) (( bool (*) (Stack_1_t1097 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m16470_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m21291(__this, method) (( Object_t * (*) (Stack_1_t1097 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m16471_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m21292(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t1097 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m16472_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Type>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21293(__this, method) (( Object_t* (*) (Stack_1_t1097 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16473_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Type>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m21294(__this, method) (( Object_t * (*) (Stack_1_t1097 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m16474_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Type>::Peek()
#define Stack_1_Peek_m21295(__this, method) (( Type_t * (*) (Stack_1_t1097 *, const MethodInfo*))Stack_1_Peek_m16475_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Type>::Pop()
#define Stack_1_Pop_m5214(__this, method) (( Type_t * (*) (Stack_1_t1097 *, const MethodInfo*))Stack_1_Pop_m16476_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(T)
#define Stack_1_Push_m5213(__this, ___t, method) (( void (*) (Stack_1_t1097 *, Type_t *, const MethodInfo*))Stack_1_Push_m16477_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
#define Stack_1_get_Count_m21296(__this, method) (( int32_t (*) (Stack_1_t1097 *, const MethodInfo*))Stack_1_get_Count_m16478_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Type>::GetEnumerator()
#define Stack_1_GetEnumerator_m21297(__this, method) (( Enumerator_t3363  (*) (Stack_1_t1097 *, const MethodInfo*))Stack_1_GetEnumerator_m16479_gshared)(__this, method)
