﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ThreadStaticAttribute
struct ThreadStaticAttribute_t2241;

// System.Void System.ThreadStaticAttribute::.ctor()
extern "C" void ThreadStaticAttribute__ctor_m12480 (ThreadStaticAttribute_t2241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
