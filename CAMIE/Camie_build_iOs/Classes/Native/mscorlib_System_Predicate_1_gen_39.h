﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName
struct StrongName_t2116;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Security.Policy.StrongName>
struct  Predicate_1_t3493  : public MulticastDelegate_t549
{
};
