﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<SCR_Wings>
struct InternalEnumerator_1_t2948;
// System.Object
struct Object_t;
// SCR_Wings
struct SCR_Wings_t351;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<SCR_Wings>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15454(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2948 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<SCR_Wings>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15455(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2948 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SCR_Wings>::Dispose()
#define InternalEnumerator_1_Dispose_m15456(__this, method) (( void (*) (InternalEnumerator_1_t2948 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SCR_Wings>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15457(__this, method) (( bool (*) (InternalEnumerator_1_t2948 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SCR_Wings>::get_Current()
#define InternalEnumerator_1_get_Current_m15458(__this, method) (( SCR_Wings_t351 * (*) (InternalEnumerator_1_t2948 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
