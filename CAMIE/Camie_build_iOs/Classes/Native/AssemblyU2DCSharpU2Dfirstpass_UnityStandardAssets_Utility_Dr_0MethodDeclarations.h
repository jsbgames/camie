﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.DragRigidbody
struct DragRigidbody_t174;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.Camera
struct Camera_t27;

// System.Void UnityStandardAssets.Utility.DragRigidbody::.ctor()
extern "C" void DragRigidbody__ctor_m466 (DragRigidbody_t174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.DragRigidbody::Update()
extern "C" void DragRigidbody_Update_m467 (DragRigidbody_t174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityStandardAssets.Utility.DragRigidbody::DragObject(System.Single)
extern "C" Object_t * DragRigidbody_DragObject_m468 (DragRigidbody_t174 * __this, float ___distance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody::FindCamera()
extern "C" Camera_t27 * DragRigidbody_FindCamera_m469 (DragRigidbody_t174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
