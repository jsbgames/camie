﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Reporter/Sample>
struct Enumerator_t2898;
// System.Object
struct Object_t;
// Reporter/Sample
struct Sample_t290;
// System.Collections.Generic.List`1<Reporter/Sample>
struct List_1_t297;

// System.Void System.Collections.Generic.List`1/Enumerator<Reporter/Sample>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m14658(__this, ___l, method) (( void (*) (Enumerator_t2898 *, List_1_t297 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Reporter/Sample>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14659(__this, method) (( Object_t * (*) (Enumerator_t2898 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Reporter/Sample>::Dispose()
#define Enumerator_Dispose_m14660(__this, method) (( void (*) (Enumerator_t2898 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Reporter/Sample>::VerifyState()
#define Enumerator_VerifyState_m14661(__this, method) (( void (*) (Enumerator_t2898 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Reporter/Sample>::MoveNext()
#define Enumerator_MoveNext_m14662(__this, method) (( bool (*) (Enumerator_t2898 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Reporter/Sample>::get_Current()
#define Enumerator_get_Current_m14663(__this, method) (( Sample_t290 * (*) (Enumerator_t2898 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
