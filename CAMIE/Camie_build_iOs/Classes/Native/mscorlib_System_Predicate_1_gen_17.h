﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.EventTrigger/Entry
struct Entry_t458;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct  Predicate_1_t3045  : public MulticastDelegate_t549
{
};
