﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.AutoMobileShaderSwitch
struct AutoMobileShaderSwitch_t169;

// System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::.ctor()
extern "C" void AutoMobileShaderSwitch__ctor_m446 (AutoMobileShaderSwitch_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::OnEnable()
extern "C" void AutoMobileShaderSwitch_OnEnable_m447 (AutoMobileShaderSwitch_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
