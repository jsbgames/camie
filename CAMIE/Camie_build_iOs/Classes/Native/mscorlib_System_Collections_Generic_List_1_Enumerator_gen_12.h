﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<SCR_Puceron>
struct List_1_t387;
// SCR_Puceron
struct SCR_Puceron_t356;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<SCR_Puceron>
struct  Enumerator_t2975 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<SCR_Puceron>::l
	List_1_t387 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<SCR_Puceron>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<SCR_Puceron>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<SCR_Puceron>::current
	SCR_Puceron_t356 * ___current_3;
};
