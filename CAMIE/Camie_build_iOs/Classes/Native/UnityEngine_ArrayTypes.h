﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.Collider2D[]
// UnityEngine.Collider2D[]
struct  Collider2DU5BU5D_t221  : public Array_t
{
};
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct  BehaviourU5BU5D_t3836  : public Array_t
{
};
// UnityEngine.Component[]
// UnityEngine.Component[]
struct  ComponentU5BU5D_t3031  : public Array_t
{
};
// UnityEngine.Object[]
// UnityEngine.Object[]
struct  ObjectU5BU5D_t230  : public Array_t
{
};
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct  RaycastHitU5BU5D_t23  : public Array_t
{
};
// UnityEngine.Collider[]
// UnityEngine.Collider[]
struct  ColliderU5BU5D_t135  : public Array_t
{
};
// UnityEngine.Renderer[]
// UnityEngine.Renderer[]
struct  RendererU5BU5D_t223  : public Array_t
{
};
// UnityEngine.MonoBehaviour[]
// UnityEngine.MonoBehaviour[]
struct  MonoBehaviourU5BU5D_t193  : public Array_t
{
};
// UnityEngine.Touch[]
// UnityEngine.Touch[]
struct  TouchU5BU5D_t234  : public Array_t
{
};
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct  Vector2U5BU5D_t237  : public Array_t
{
};
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct  KeyframeU5BU5D_t239  : public Array_t
{
};
// UnityEngine.Color[]
// UnityEngine.Color[]
struct  ColorU5BU5D_t241  : public Array_t
{
};
// UnityEngine.RenderTexture[]
// UnityEngine.RenderTexture[]
struct  RenderTextureU5BU5D_t90  : public Array_t
{
};
// UnityEngine.Texture[]
// UnityEngine.Texture[]
struct  TextureU5BU5D_t3837  : public Array_t
{
};
// UnityEngine.Mesh[]
// UnityEngine.Mesh[]
struct  MeshU5BU5D_t113  : public Array_t
{
};
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct  Vector3U5BU5D_t210  : public Array_t
{
};
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct  TransformU5BU5D_t141  : public Array_t
{
};
// UnityEngine.Rigidbody[]
// UnityEngine.Rigidbody[]
struct  RigidbodyU5BU5D_t2865  : public Array_t
{
};
// UnityEngine.ContactPoint[]
// UnityEngine.ContactPoint[]
struct  ContactPointU5BU5D_t245  : public Array_t
{
};
// UnityEngine.ParticleSystem[]
// UnityEngine.ParticleSystem[]
struct  ParticleSystemU5BU5D_t150  : public Array_t
{
};
// UnityEngine.AudioClip[]
// UnityEngine.AudioClip[]
struct  AudioClipU5BU5D_t157  : public Array_t
{
};
// UnityEngine.ParticleCollisionEvent[]
// UnityEngine.ParticleCollisionEvent[]
struct  ParticleCollisionEventU5BU5D_t159  : public Array_t
{
};
// UnityEngine.Material[]
// UnityEngine.Material[]
struct  MaterialU5BU5D_t252  : public Array_t
{
};
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct  GameObjectU5BU5D_t192  : public Array_t
{
};
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct  GUILayoutOptionU5BU5D_t409  : public Array_t
{
};
// UnityEngine.Quaternion[]
// UnityEngine.Quaternion[]
struct  QuaternionU5BU5D_t400  : public Array_t
{
};
// UnityEngine.Sprite[]
// UnityEngine.Sprite[]
struct  SpriteU5BU5D_t368  : public Array_t
{
};
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct  CanvasU5BU5D_t376  : public Array_t
{
};
struct CanvasU5BU5D_t376_StaticFields{
};
// UnityEngine.AudioSource[]
// UnityEngine.AudioSource[]
struct  AudioSourceU5BU5D_t427  : public Array_t
{
};
// UnityEngine.AnimatorClipInfo[]
// UnityEngine.AnimatorClipInfo[]
struct  AnimatorClipInfoU5BU5D_t430  : public Array_t
{
};
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct  RaycastHit2DU5BU5D_t664  : public Array_t
{
};
// UnityEngine.Font[]
// UnityEngine.Font[]
struct  FontU5BU5D_t3084  : public Array_t
{
};
struct FontU5BU5D_t3084_StaticFields{
};
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct  UIVertexU5BU5D_t555  : public Array_t
{
};
struct UIVertexU5BU5D_t555_StaticFields{
};
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct  UILineInfoU5BU5D_t1026  : public Array_t
{
};
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct  UICharInfoU5BU5D_t1025  : public Array_t
{
};
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct  CanvasGroupU5BU5D_t3149  : public Array_t
{
};
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct  RectTransformU5BU5D_t3172  : public Array_t
{
};
struct RectTransformU5BU5D_t3172_StaticFields{
};
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct  IAchievementDescriptionU5BU5D_t1046  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct  IAchievementU5BU5D_t1048  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct  IScoreU5BU5D_t981  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct  IUserProfileU5BU5D_t975  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct  AchievementDescriptionU5BU5D_t792  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct  UserProfileU5BU5D_t793  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct  GcLeaderboardU5BU5D_t3190  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct  GcAchievementDataU5BU5D_t1018  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct  AchievementU5BU5D_t1047  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct  GcScoreDataU5BU5D_t1019  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct  ScoreU5BU5D_t1049  : public Array_t
{
};
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct  LayoutCacheU5BU5D_t3199  : public Array_t
{
};
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct  GUILayoutEntryU5BU5D_t3205  : public Array_t
{
};
struct GUILayoutEntryU5BU5D_t3205_StaticFields{
};
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct  GUIStyleU5BU5D_t830  : public Array_t
{
};
struct GUIStyleU5BU5D_t830_StaticFields{
};
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct  CameraU5BU5D_t985  : public Array_t
{
};
struct CameraU5BU5D_t985_StaticFields{
};
// UnityEngine.Display[]
// UnityEngine.Display[]
struct  DisplayU5BU5D_t860  : public Array_t
{
};
struct DisplayU5BU5D_t860_StaticFields{
};
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct  Rigidbody2DU5BU5D_t3228  : public Array_t
{
};
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
struct  MatchDirectConnectInfoU5BU5D_t3274  : public Array_t
{
};
// UnityEngine.Networking.Match.MatchDesc[]
// UnityEngine.Networking.Match.MatchDesc[]
struct  MatchDescU5BU5D_t3280  : public Array_t
{
};
// UnityEngine.Networking.Types.NetworkID[]
// UnityEngine.Networking.Types.NetworkID[]
struct  NetworkIDU5BU5D_t3286  : public Array_t
{
};
// UnityEngine.Networking.Types.NetworkAccessToken[]
// UnityEngine.Networking.Types.NetworkAccessToken[]
struct  NetworkAccessTokenU5BU5D_t3287  : public Array_t
{
};
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate[]
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate[]
struct  ConstructorDelegateU5BU5D_t3319  : public Array_t
{
};
// SimpleJson.Reflection.ReflectionUtils/GetDelegate[]
// SimpleJson.Reflection.ReflectionUtils/GetDelegate[]
struct  GetDelegateU5BU5D_t3331  : public Array_t
{
};
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct  DisallowMultipleComponentU5BU5D_t949  : public Array_t
{
};
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct  ExecuteInEditModeU5BU5D_t950  : public Array_t
{
};
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct  RequireComponentU5BU5D_t951  : public Array_t
{
};
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct  HitInfoU5BU5D_t984  : public Array_t
{
};
// UnityEngine.Event[]
// UnityEngine.Event[]
struct  EventU5BU5D_t3371  : public Array_t
{
};
struct EventU5BU5D_t3371_StaticFields{
};
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct  TextEditOpU5BU5D_t3372  : public Array_t
{
};
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct  PersistentCallU5BU5D_t3391  : public Array_t
{
};
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct  BaseInvokableCallU5BU5D_t3396  : public Array_t
{
};
