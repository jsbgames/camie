﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Waypoint
struct SCR_Waypoint_t358;

// System.Void SCR_Waypoint::.ctor()
extern "C" void SCR_Waypoint__ctor_m1541 (SCR_Waypoint_t358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
