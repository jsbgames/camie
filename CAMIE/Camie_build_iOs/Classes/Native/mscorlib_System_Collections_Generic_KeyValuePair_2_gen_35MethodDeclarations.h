﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
struct KeyValuePair_2_t3427;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m22046_gshared (KeyValuePair_2_t3427 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m22046(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3427 *, int32_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m22046_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m22047_gshared (KeyValuePair_2_t3427 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m22047(__this, method) (( int32_t (*) (KeyValuePair_2_t3427 *, const MethodInfo*))KeyValuePair_2_get_Key_m22047_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m22048_gshared (KeyValuePair_2_t3427 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m22048(__this, ___value, method) (( void (*) (KeyValuePair_2_t3427 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m22048_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m22049_gshared (KeyValuePair_2_t3427 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m22049(__this, method) (( int32_t (*) (KeyValuePair_2_t3427 *, const MethodInfo*))KeyValuePair_2_get_Value_m22049_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m22050_gshared (KeyValuePair_2_t3427 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m22050(__this, ___value, method) (( void (*) (KeyValuePair_2_t3427 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m22050_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m22051_gshared (KeyValuePair_2_t3427 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m22051(__this, method) (( String_t* (*) (KeyValuePair_2_t3427 *, const MethodInfo*))KeyValuePair_2_ToString_m22051_gshared)(__this, method)
