﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t3106;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m17760_gshared (DefaultComparer_t3106 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m17760(__this, method) (( void (*) (DefaultComparer_t3106 *, const MethodInfo*))DefaultComparer__ctor_m17760_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m17761_gshared (DefaultComparer_t3106 * __this, UIVertex_t556  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m17761(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3106 *, UIVertex_t556 , const MethodInfo*))DefaultComparer_GetHashCode_m17761_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m17762_gshared (DefaultComparer_t3106 * __this, UIVertex_t556  ___x, UIVertex_t556  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m17762(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3106 *, UIVertex_t556 , UIVertex_t556 , const MethodInfo*))DefaultComparer_Equals_m17762_gshared)(__this, ___x, ___y, method)
