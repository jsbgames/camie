﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<SCR_Puceron>
struct IList_1_t2972;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<SCR_Puceron>
struct  ReadOnlyCollection_1_t2973  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SCR_Puceron>::list
	Object_t* ___list_0;
};
