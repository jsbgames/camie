﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t3109;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m17770_gshared (U3CStartU3Ec__Iterator0_t3109 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0__ctor_m17770(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t3109 *, const MethodInfo*))U3CStartU3Ec__Iterator0__ctor_m17770_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17771_gshared (U3CStartU3Ec__Iterator0_t3109 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17771(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t3109 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17771_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17772_gshared (U3CStartU3Ec__Iterator0_t3109 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17772(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t3109 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17772_gshared)(__this, method)
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m17773_gshared (U3CStartU3Ec__Iterator0_t3109 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_MoveNext_m17773(__this, method) (( bool (*) (U3CStartU3Ec__Iterator0_t3109 *, const MethodInfo*))U3CStartU3Ec__Iterator0_MoveNext_m17773_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m17774_gshared (U3CStartU3Ec__Iterator0_t3109 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Dispose_m17774(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t3109 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Dispose_m17774_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m17775_gshared (U3CStartU3Ec__Iterator0_t3109 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Reset_m17775(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t3109 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Reset_m17775_gshared)(__this, method)
