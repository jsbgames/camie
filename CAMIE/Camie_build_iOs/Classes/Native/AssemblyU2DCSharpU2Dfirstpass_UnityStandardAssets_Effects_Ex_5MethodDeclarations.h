﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.ExtinguishableParticleSystem
struct ExtinguishableParticleSystem_t151;

// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::.ctor()
extern "C" void ExtinguishableParticleSystem__ctor_m425 (ExtinguishableParticleSystem_t151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Start()
extern "C" void ExtinguishableParticleSystem_Start_m426 (ExtinguishableParticleSystem_t151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Extinguish()
extern "C" void ExtinguishableParticleSystem_Extinguish_m427 (ExtinguishableParticleSystem_t151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
