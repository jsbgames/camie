﻿#pragma once
#include <stdint.h>
// System.Type[]
struct TypeU5BU5D_t238;
// System.String[]
struct StringU5BU5D_t243;
// System.Runtime.Serialization.Formatters.Binary.TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type_0.h"
// System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata
struct  SerializableTypeMetadata_t2037  : public TypeMetadata_t2035
{
	// System.Type[] System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata::types
	TypeU5BU5D_t238* ___types_2;
	// System.String[] System.Runtime.Serialization.Formatters.Binary.SerializableTypeMetadata::names
	StringU5BU5D_t243* ___names_3;
};
