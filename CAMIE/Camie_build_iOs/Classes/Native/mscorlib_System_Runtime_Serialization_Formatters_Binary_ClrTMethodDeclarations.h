﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata
struct ClrTypeMetadata_t2036;
// System.Type
struct Type_t;

// System.Void System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata::.ctor(System.Type)
extern "C" void ClrTypeMetadata__ctor_m10850 (ClrTypeMetadata_t2036 * __this, Type_t * ___instanceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata::get_RequiresTypes()
extern "C" bool ClrTypeMetadata_get_RequiresTypes_m10851 (ClrTypeMetadata_t2036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
