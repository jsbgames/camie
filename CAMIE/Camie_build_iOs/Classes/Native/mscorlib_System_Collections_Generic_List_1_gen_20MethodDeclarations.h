﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t578;
// System.Object
struct Object_t;
// UnityEngine.UI.Selectable
struct Selectable_t510;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Selectable>
struct IEnumerable_1_t3688;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Selectable>
struct IEnumerator_1_t3689;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<UnityEngine.UI.Selectable>
struct ICollection_1_t3690;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.Selectable>
struct ReadOnlyCollection_1_t3145;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t3143;
// System.Predicate`1<UnityEngine.UI.Selectable>
struct Predicate_1_t3146;
// System.Comparison`1<UnityEngine.UI.Selectable>
struct Comparison_1_t3148;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_24.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m3386(__this, method) (( void (*) (List_1_t578 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18267(__this, ___collection, method) (( void (*) (List_1_t578 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::.ctor(System.Int32)
#define List_1__ctor_m18268(__this, ___capacity, method) (( void (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::.cctor()
#define List_1__cctor_m18269(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18270(__this, method) (( Object_t* (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18271(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t578 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18272(__this, method) (( Object_t * (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18273(__this, ___item, method) (( int32_t (*) (List_1_t578 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18274(__this, ___item, method) (( bool (*) (List_1_t578 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18275(__this, ___item, method) (( int32_t (*) (List_1_t578 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18276(__this, ___index, ___item, method) (( void (*) (List_1_t578 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18277(__this, ___item, method) (( void (*) (List_1_t578 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18278(__this, method) (( bool (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18279(__this, method) (( bool (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18280(__this, method) (( Object_t * (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18281(__this, method) (( bool (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18282(__this, method) (( bool (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18283(__this, ___index, method) (( Object_t * (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18284(__this, ___index, ___value, method) (( void (*) (List_1_t578 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Add(T)
#define List_1_Add_m18285(__this, ___item, method) (( void (*) (List_1_t578 *, Selectable_t510 *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18286(__this, ___newCount, method) (( void (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18287(__this, ___collection, method) (( void (*) (List_1_t578 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18288(__this, ___enumerable, method) (( void (*) (List_1_t578 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18289(__this, ___collection, method) (( void (*) (List_1_t578 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::AsReadOnly()
#define List_1_AsReadOnly_m18290(__this, method) (( ReadOnlyCollection_1_t3145 * (*) (List_1_t578 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Clear()
#define List_1_Clear_m18291(__this, method) (( void (*) (List_1_t578 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Contains(T)
#define List_1_Contains_m18292(__this, ___item, method) (( bool (*) (List_1_t578 *, Selectable_t510 *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18293(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t578 *, SelectableU5BU5D_t3143*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Find(System.Predicate`1<T>)
#define List_1_Find_m18294(__this, ___match, method) (( Selectable_t510 * (*) (List_1_t578 *, Predicate_1_t3146 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18295(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3146 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18296(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t578 *, int32_t, int32_t, Predicate_1_t3146 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::GetEnumerator()
#define List_1_GetEnumerator_m18297(__this, method) (( Enumerator_t3147  (*) (List_1_t578 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::IndexOf(T)
#define List_1_IndexOf_m18298(__this, ___item, method) (( int32_t (*) (List_1_t578 *, Selectable_t510 *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18299(__this, ___start, ___delta, method) (( void (*) (List_1_t578 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18300(__this, ___index, method) (( void (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Insert(System.Int32,T)
#define List_1_Insert_m18301(__this, ___index, ___item, method) (( void (*) (List_1_t578 *, int32_t, Selectable_t510 *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18302(__this, ___collection, method) (( void (*) (List_1_t578 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Remove(T)
#define List_1_Remove_m18303(__this, ___item, method) (( bool (*) (List_1_t578 *, Selectable_t510 *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18304(__this, ___match, method) (( int32_t (*) (List_1_t578 *, Predicate_1_t3146 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18305(__this, ___index, method) (( void (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Reverse()
#define List_1_Reverse_m18306(__this, method) (( void (*) (List_1_t578 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Sort()
#define List_1_Sort_m18307(__this, method) (( void (*) (List_1_t578 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18308(__this, ___comparison, method) (( void (*) (List_1_t578 *, Comparison_1_t3148 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::ToArray()
#define List_1_ToArray_m18309(__this, method) (( SelectableU5BU5D_t3143* (*) (List_1_t578 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::TrimExcess()
#define List_1_TrimExcess_m18310(__this, method) (( void (*) (List_1_t578 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::get_Capacity()
#define List_1_get_Capacity_m18311(__this, method) (( int32_t (*) (List_1_t578 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18312(__this, ___value, method) (( void (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::get_Count()
#define List_1_get_Count_m18313(__this, method) (( int32_t (*) (List_1_t578 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::get_Item(System.Int32)
#define List_1_get_Item_m18314(__this, ___index, method) (( Selectable_t510 * (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Selectable>::set_Item(System.Int32,T)
#define List_1_set_Item_m18315(__this, ___index, ___value, method) (( void (*) (List_1_t578 *, int32_t, Selectable_t510 *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
