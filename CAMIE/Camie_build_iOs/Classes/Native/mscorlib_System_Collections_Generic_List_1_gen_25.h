﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t224;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Object>
struct  List_1_t655  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Object>::_items
	ObjectU5BU5D_t224* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Object>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Object>::_version
	int32_t ____version_3;
};
struct List_1_t655_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Object>::EmptyArray
	ObjectU5BU5D_t224* ___EmptyArray_4;
};
