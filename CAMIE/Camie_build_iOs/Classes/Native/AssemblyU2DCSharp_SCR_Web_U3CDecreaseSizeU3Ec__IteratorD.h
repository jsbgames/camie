﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// SCR_Web
struct SCR_Web_t398;
// System.Object
#include "mscorlib_System_Object.h"
// SCR_Web/<DecreaseSize>c__IteratorD
struct  U3CDecreaseSizeU3Ec__IteratorD_t399  : public Object_t
{
	// System.Int32 SCR_Web/<DecreaseSize>c__IteratorD::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Int32 SCR_Web/<DecreaseSize>c__IteratorD::$PC
	int32_t ___U24PC_1;
	// System.Object SCR_Web/<DecreaseSize>c__IteratorD::$current
	Object_t * ___U24current_2;
	// SCR_Web SCR_Web/<DecreaseSize>c__IteratorD::<>f__this
	SCR_Web_t398 * ___U3CU3Ef__this_3;
};
