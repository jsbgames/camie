﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t256;

// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C" void WaitForFixedUpdate__ctor_m1000 (WaitForFixedUpdate_t256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
