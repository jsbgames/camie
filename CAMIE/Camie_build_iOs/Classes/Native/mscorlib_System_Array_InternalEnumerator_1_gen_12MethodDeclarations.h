﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Touch>
struct InternalEnumerator_1_t2842;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13907_gshared (InternalEnumerator_1_t2842 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13907(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2842 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13907_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13908_gshared (InternalEnumerator_1_t2842 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13908(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2842 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13908_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13909_gshared (InternalEnumerator_1_t2842 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13909(__this, method) (( void (*) (InternalEnumerator_1_t2842 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13909_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Touch>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13910_gshared (InternalEnumerator_1_t2842 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13910(__this, method) (( bool (*) (InternalEnumerator_1_t2842 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13910_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Touch>::get_Current()
extern "C" Touch_t235  InternalEnumerator_1_get_Current_m13911_gshared (InternalEnumerator_1_t2842 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13911(__this, method) (( Touch_t235  (*) (InternalEnumerator_1_t2842 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13911_gshared)(__this, method)
