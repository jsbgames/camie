﻿#pragma once
#include <stdint.h>
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1025;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct  List_1_t898  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::_items
	UICharInfoU5BU5D_t1025* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::_version
	int32_t ____version_3;
};
struct List_1_t898_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::EmptyArray
	UICharInfoU5BU5D_t1025* ___EmptyArray_4;
};
