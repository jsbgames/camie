﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioSource>
struct Dictionary_2_t389;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.AudioSource>
struct  ValueCollection_t429  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.AudioSource>::dictionary
	Dictionary_2_t389 * ___dictionary_0;
};
