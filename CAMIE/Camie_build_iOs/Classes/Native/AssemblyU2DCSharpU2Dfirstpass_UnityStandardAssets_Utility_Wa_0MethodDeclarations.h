﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
struct RoutePoint_t209;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void RoutePoint__ctor_m568 (RoutePoint_t209 * __this, Vector3_t4  ___position, Vector3_t4  ___direction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
