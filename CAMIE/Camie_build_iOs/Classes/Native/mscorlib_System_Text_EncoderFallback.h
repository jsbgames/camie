﻿#pragma once
#include <stdint.h>
// System.Text.EncoderFallback
struct EncoderFallback_t2141;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.EncoderFallback
struct  EncoderFallback_t2141  : public Object_t
{
};
struct EncoderFallback_t2141_StaticFields{
	// System.Text.EncoderFallback System.Text.EncoderFallback::exception_fallback
	EncoderFallback_t2141 * ___exception_fallback_0;
	// System.Text.EncoderFallback System.Text.EncoderFallback::replacement_fallback
	EncoderFallback_t2141 * ___replacement_fallback_1;
	// System.Text.EncoderFallback System.Text.EncoderFallback::standard_safe_fallback
	EncoderFallback_t2141 * ___standard_safe_fallback_2;
};
