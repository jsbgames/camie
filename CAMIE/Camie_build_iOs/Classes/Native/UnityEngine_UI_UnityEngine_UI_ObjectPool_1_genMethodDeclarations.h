﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct ObjectPool_1_t479;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct UnityAction_1_t480;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t644;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m3089(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t479 *, UnityAction_1_t480 *, UnityAction_1_t480 *, const MethodInfo*))ObjectPool_1__ctor_m16456_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countAll()
#define ObjectPool_1_get_countAll_m16457(__this, method) (( int32_t (*) (ObjectPool_1_t479 *, const MethodInfo*))ObjectPool_1_get_countAll_m16458_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m16459(__this, ___value, method) (( void (*) (ObjectPool_1_t479 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m16460_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countActive()
#define ObjectPool_1_get_countActive_m16461(__this, method) (( int32_t (*) (ObjectPool_1_t479 *, const MethodInfo*))ObjectPool_1_get_countActive_m16462_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m16463(__this, method) (( int32_t (*) (ObjectPool_1_t479 *, const MethodInfo*))ObjectPool_1_get_countInactive_m16464_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Get()
#define ObjectPool_1_Get_m16465(__this, method) (( List_1_t644 * (*) (ObjectPool_1_t479 *, const MethodInfo*))ObjectPool_1_Get_m16466_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Release(T)
#define ObjectPool_1_Release_m16467(__this, ___element, method) (( void (*) (ObjectPool_1_t479 *, List_1_t644 *, const MethodInfo*))ObjectPool_1_Release_m16468_gshared)(__this, ___element, method)
