﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>
struct KeyValuePair_2_t2923;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>
struct Dictionary_2_t407;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m15102(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2923 *, String_t*, Dictionary_2_t407 *, const MethodInfo*))KeyValuePair_2__ctor_m13651_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::get_Key()
#define KeyValuePair_2_get_Key_m15103(__this, method) (( String_t* (*) (KeyValuePair_2_t2923 *, const MethodInfo*))KeyValuePair_2_get_Key_m13652_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15104(__this, ___value, method) (( void (*) (KeyValuePair_2_t2923 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m13653_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::get_Value()
#define KeyValuePair_2_get_Value_m15105(__this, method) (( Dictionary_2_t407 * (*) (KeyValuePair_2_t2923 *, const MethodInfo*))KeyValuePair_2_get_Value_m13654_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15106(__this, ___value, method) (( void (*) (KeyValuePair_2_t2923 *, Dictionary_2_t407 *, const MethodInfo*))KeyValuePair_2_set_Value_m13655_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,Reporter/Log>>::ToString()
#define KeyValuePair_2_ToString_m15107(__this, method) (( String_t* (*) (KeyValuePair_2_t2923 *, const MethodInfo*))KeyValuePair_2_ToString_m13656_gshared)(__this, method)
