﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct UnityEvent_1_t505;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t645;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1002;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::.ctor()
extern "C" void UnityEvent_1__ctor_m3149_gshared (UnityEvent_1_t505 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m3149(__this, method) (( void (*) (UnityEvent_1_t505 *, const MethodInfo*))UnityEvent_1__ctor_m3149_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_AddListener_m3152_gshared (UnityEvent_1_t505 * __this, UnityAction_1_t645 * ___call, const MethodInfo* method);
#define UnityEvent_1_AddListener_m3152(__this, ___call, method) (( void (*) (UnityEvent_1_t505 *, UnityAction_1_t645 *, const MethodInfo*))UnityEvent_1_AddListener_m3152_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_RemoveListener_m17267_gshared (UnityEvent_1_t505 * __this, UnityAction_1_t645 * ___call, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m17267(__this, ___call, method) (( void (*) (UnityEvent_1_t505 *, UnityAction_1_t645 *, const MethodInfo*))UnityEvent_1_RemoveListener_m17267_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::FindMethod_Impl(System.String,System.Object)
extern "C" MethodInfo_t * UnityEvent_1_FindMethod_Impl_m3599_gshared (UnityEvent_1_t505 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m3599(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t505 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m3599_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t1002 * UnityEvent_1_GetDelegate_m3600_gshared (UnityEvent_1_t505 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m3600(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t1002 * (*) (UnityEvent_1_t505 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m3600_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C" BaseInvokableCall_t1002 * UnityEvent_1_GetDelegate_m17268_gshared (Object_t * __this /* static, unused */, UnityAction_1_t645 * ___action, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m17268(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t1002 * (*) (Object_t * /* static, unused */, UnityAction_1_t645 *, const MethodInfo*))UnityEvent_1_GetDelegate_m17268_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::Invoke(T0)
extern "C" void UnityEvent_1_Invoke_m3151_gshared (UnityEvent_1_t505 * __this, Color_t65  ___arg0, const MethodInfo* method);
#define UnityEvent_1_Invoke_m3151(__this, ___arg0, method) (( void (*) (UnityEvent_1_t505 *, Color_t65 , const MethodInfo*))UnityEvent_1_Invoke_m3151_gshared)(__this, ___arg0, method)
