﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Uri
struct Uri_t926;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Networking.Match.NetworkMatch
struct  NetworkMatch_t927  : public MonoBehaviour_t3
{
	// System.Uri UnityEngine.Networking.Match.NetworkMatch::m_BaseUri
	Uri_t926 * ___m_BaseUri_3;
};
