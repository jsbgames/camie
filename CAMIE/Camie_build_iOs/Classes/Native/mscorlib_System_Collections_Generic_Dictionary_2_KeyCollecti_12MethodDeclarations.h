﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>
struct KeyCollection_t2987;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte>
struct Dictionary_2_t2983;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3588;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Int32[]
struct Int32U5BU5D_t242;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_13.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void KeyCollection__ctor_m15929_gshared (KeyCollection_t2987 * __this, Dictionary_2_t2983 * ___dictionary, const MethodInfo* method);
#define KeyCollection__ctor_m15929(__this, ___dictionary, method) (( void (*) (KeyCollection_t2987 *, Dictionary_2_t2983 *, const MethodInfo*))KeyCollection__ctor_m15929_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15930_gshared (KeyCollection_t2987 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15930(__this, ___item, method) (( void (*) (KeyCollection_t2987 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15930_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15931_gshared (KeyCollection_t2987 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15931(__this, method) (( void (*) (KeyCollection_t2987 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15931_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15932_gshared (KeyCollection_t2987 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15932(__this, ___item, method) (( bool (*) (KeyCollection_t2987 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15932_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15933_gshared (KeyCollection_t2987 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15933(__this, ___item, method) (( bool (*) (KeyCollection_t2987 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15933_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15934_gshared (KeyCollection_t2987 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15934(__this, method) (( Object_t* (*) (KeyCollection_t2987 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15934_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m15935_gshared (KeyCollection_t2987 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m15935(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2987 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m15935_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15936_gshared (KeyCollection_t2987 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15936(__this, method) (( Object_t * (*) (KeyCollection_t2987 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15936_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15937_gshared (KeyCollection_t2987 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15937(__this, method) (( bool (*) (KeyCollection_t2987 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15937_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15938_gshared (KeyCollection_t2987 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15938(__this, method) (( bool (*) (KeyCollection_t2987 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15938_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m15939_gshared (KeyCollection_t2987 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m15939(__this, method) (( Object_t * (*) (KeyCollection_t2987 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m15939_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m15940_gshared (KeyCollection_t2987 * __this, Int32U5BU5D_t242* ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_CopyTo_m15940(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2987 *, Int32U5BU5D_t242*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m15940_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::GetEnumerator()
extern "C" Enumerator_t2988  KeyCollection_GetEnumerator_m15941_gshared (KeyCollection_t2987 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m15941(__this, method) (( Enumerator_t2988  (*) (KeyCollection_t2987 *, const MethodInfo*))KeyCollection_GetEnumerator_m15941_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Byte>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m15942_gshared (KeyCollection_t2987 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m15942(__this, method) (( int32_t (*) (KeyCollection_t2987 *, const MethodInfo*))KeyCollection_get_Count_m15942_gshared)(__this, method)
