﻿#pragma once
#include <stdint.h>
// SCR_Camie
struct SCR_Camie_t338;
// SCR_Joystick
struct SCR_Joystick_t346;
// UnityEngine.Transform
struct Transform_t1;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_CollisionChecker
struct  SCR_CollisionChecker_t349  : public MonoBehaviour_t3
{
	// SCR_Camie SCR_CollisionChecker::avatar
	SCR_Camie_t338 * ___avatar_2;
	// SCR_Joystick SCR_CollisionChecker::joystick
	SCR_Joystick_t346 * ___joystick_3;
	// UnityEngine.Transform SCR_CollisionChecker::avatarTransform
	Transform_t1 * ___avatarTransform_4;
	// UnityEngine.Transform SCR_CollisionChecker::myTransform
	Transform_t1 * ___myTransform_5;
};
