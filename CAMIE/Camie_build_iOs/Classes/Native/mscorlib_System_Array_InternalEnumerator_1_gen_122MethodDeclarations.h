﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
struct InternalEnumerator_1_t3489;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Byte>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_41MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22365(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3489 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15924_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22366(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3489 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15925_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
#define InternalEnumerator_1_Dispose_m22367(__this, method) (( void (*) (InternalEnumerator_1_t3489 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15926_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22368(__this, method) (( bool (*) (InternalEnumerator_1_t3489 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15927_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
#define InternalEnumerator_1_get_Current_m22369(__this, method) (( uint8_t (*) (InternalEnumerator_1_t3489 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15928_gshared)(__this, method)
