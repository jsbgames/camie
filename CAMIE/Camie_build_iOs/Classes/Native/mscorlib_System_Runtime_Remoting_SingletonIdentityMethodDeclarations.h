﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.SingletonIdentity
struct SingletonIdentity_t2018;
// System.String
struct String_t;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1963;
// System.Type
struct Type_t;

// System.Void System.Runtime.Remoting.SingletonIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern "C" void SingletonIdentity__ctor_m10777 (SingletonIdentity_t2018 * __this, String_t* ___objectUri, Context_t1963 * ___context, Type_t * ___objectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
