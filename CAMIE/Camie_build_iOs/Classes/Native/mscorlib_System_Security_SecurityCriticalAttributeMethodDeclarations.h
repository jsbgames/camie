﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SecurityCriticalAttribute
struct SecurityCriticalAttribute_t1113;

// System.Void System.Security.SecurityCriticalAttribute::.ctor()
extern "C" void SecurityCriticalAttribute__ctor_m5262 (SecurityCriticalAttribute_t1113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
