﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SpaceAttribute
struct SpaceAttribute_t726;

// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern "C" void SpaceAttribute__ctor_m3533 (SpaceAttribute_t726 * __this, float ___height, const MethodInfo* method) IL2CPP_METHOD_ATTR;
