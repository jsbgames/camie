﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$2048
struct U24ArrayTypeU242048_t2268;
struct U24ArrayTypeU242048_t2268_marshaled;

void U24ArrayTypeU242048_t2268_marshal(const U24ArrayTypeU242048_t2268& unmarshaled, U24ArrayTypeU242048_t2268_marshaled& marshaled);
void U24ArrayTypeU242048_t2268_marshal_back(const U24ArrayTypeU242048_t2268_marshaled& marshaled, U24ArrayTypeU242048_t2268& unmarshaled);
void U24ArrayTypeU242048_t2268_marshal_cleanup(U24ArrayTypeU242048_t2268_marshaled& marshaled);
