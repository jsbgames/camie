﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.GenericStack
struct GenericStack_t814;

// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C" void GenericStack__ctor_m5087 (GenericStack_t814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
