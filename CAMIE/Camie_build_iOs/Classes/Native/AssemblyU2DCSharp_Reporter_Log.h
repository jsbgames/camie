﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// Reporter/_LogType
#include "AssemblyU2DCSharp_Reporter__LogType.h"
// Reporter/Log
struct  Log_t291  : public Object_t
{
	// System.Int32 Reporter/Log::count
	int32_t ___count_0;
	// Reporter/_LogType Reporter/Log::logType
	int32_t ___logType_1;
	// System.String Reporter/Log::condition
	String_t* ___condition_2;
	// System.String Reporter/Log::stacktrace
	String_t* ___stacktrace_3;
	// System.Int32 Reporter/Log::sampleId
	int32_t ___sampleId_4;
};
