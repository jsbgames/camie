﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.DSA
struct DSA_t1431;
// System.Security.Cryptography.AsymmetricSignatureDeformatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDef.h"
// System.Security.Cryptography.DSASignatureDeformatter
struct  DSASignatureDeformatter_t1646  : public AsymmetricSignatureDeformatter_t1596
{
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureDeformatter::dsa
	DSA_t1431 * ___dsa_0;
};
