﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityEngine.Material>
struct IList_1_t2874;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Material>
struct  ReadOnlyCollection_1_t2875  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Material>::list
	Object_t* ___list_0;
};
