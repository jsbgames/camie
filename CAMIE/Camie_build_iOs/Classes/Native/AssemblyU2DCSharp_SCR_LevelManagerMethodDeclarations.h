﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_LevelManager
struct SCR_LevelManager_t379;

// System.Void SCR_LevelManager::.ctor()
extern "C" void SCR_LevelManager__ctor_m1416 (SCR_LevelManager_t379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelManager::Awake()
extern "C" void SCR_LevelManager_Awake_m1417 (SCR_LevelManager_t379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelManager::OnLevelWasLoaded()
extern "C" void SCR_LevelManager_OnLevelWasLoaded_m1418 (SCR_LevelManager_t379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelManager::EndLevel()
extern "C" void SCR_LevelManager_EndLevel_m1419 (SCR_LevelManager_t379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_LevelManager::LoadLevel(System.Int32)
extern "C" void SCR_LevelManager_LoadLevel_m1420 (SCR_LevelManager_t379 * __this, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SCR_LevelManager::get_CurrentLevel()
extern "C" int32_t SCR_LevelManager_get_CurrentLevel_m1421 (SCR_LevelManager_t379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SCR_LevelManager::get_GameplayLevel()
extern "C" int32_t SCR_LevelManager_get_GameplayLevel_m1422 (SCR_LevelManager_t379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_LevelManager::get_IsMenu()
extern "C" bool SCR_LevelManager_get_IsMenu_m1423 (SCR_LevelManager_t379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
