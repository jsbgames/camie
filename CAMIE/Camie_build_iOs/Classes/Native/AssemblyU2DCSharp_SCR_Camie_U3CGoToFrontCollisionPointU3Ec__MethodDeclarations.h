﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4
struct U3CGoToFrontCollisionPointU3Ec__Iterator4_t339;
// System.Object
struct Object_t;

// System.Void SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::.ctor()
extern "C" void U3CGoToFrontCollisionPointU3Ec__Iterator4__ctor_m1224 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1225 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1226 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::MoveNext()
extern "C" bool U3CGoToFrontCollisionPointU3Ec__Iterator4_MoveNext_m1227 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::Dispose()
extern "C" void U3CGoToFrontCollisionPointU3Ec__Iterator4_Dispose_m1228 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Camie/<GoToFrontCollisionPoint>c__Iterator4::Reset()
extern "C" void U3CGoToFrontCollisionPointU3Ec__Iterator4_Reset_m1229 (U3CGoToFrontCollisionPointU3Ec__Iterator4_t339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
