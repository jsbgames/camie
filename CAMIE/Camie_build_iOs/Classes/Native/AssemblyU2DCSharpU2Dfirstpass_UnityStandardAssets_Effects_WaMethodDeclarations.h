﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.WaterHoseParticles
struct WaterHoseParticles_t162;
// UnityEngine.GameObject
struct GameObject_t78;

// System.Void UnityStandardAssets.Effects.WaterHoseParticles::.ctor()
extern "C" void WaterHoseParticles__ctor_m438 (WaterHoseParticles_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.WaterHoseParticles::Start()
extern "C" void WaterHoseParticles_Start_m439 (WaterHoseParticles_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.WaterHoseParticles::OnParticleCollision(UnityEngine.GameObject)
extern "C" void WaterHoseParticles_OnParticleCollision_m440 (WaterHoseParticles_t162 * __this, GameObject_t78 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
