﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>
struct Enumerator_t2989;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte>
struct Dictionary_2_t2983;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15948_gshared (Enumerator_t2989 * __this, Dictionary_2_t2983 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m15948(__this, ___dictionary, method) (( void (*) (Enumerator_t2989 *, Dictionary_2_t2983 *, const MethodInfo*))Enumerator__ctor_m15948_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15949_gshared (Enumerator_t2989 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15949(__this, method) (( Object_t * (*) (Enumerator_t2989 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1430  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15950_gshared (Enumerator_t2989 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15950(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t2989 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15950_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15951_gshared (Enumerator_t2989 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15951(__this, method) (( Object_t * (*) (Enumerator_t2989 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15951_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15952_gshared (Enumerator_t2989 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15952(__this, method) (( Object_t * (*) (Enumerator_t2989 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15952_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15953_gshared (Enumerator_t2989 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15953(__this, method) (( bool (*) (Enumerator_t2989 *, const MethodInfo*))Enumerator_MoveNext_m15953_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::get_Current()
extern "C" KeyValuePair_2_t2984  Enumerator_get_Current_m15954_gshared (Enumerator_t2989 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15954(__this, method) (( KeyValuePair_2_t2984  (*) (Enumerator_t2989 *, const MethodInfo*))Enumerator_get_Current_m15954_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m15955_gshared (Enumerator_t2989 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m15955(__this, method) (( int32_t (*) (Enumerator_t2989 *, const MethodInfo*))Enumerator_get_CurrentKey_m15955_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::get_CurrentValue()
extern "C" uint8_t Enumerator_get_CurrentValue_m15956_gshared (Enumerator_t2989 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m15956(__this, method) (( uint8_t (*) (Enumerator_t2989 *, const MethodInfo*))Enumerator_get_CurrentValue_m15956_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::VerifyState()
extern "C" void Enumerator_VerifyState_m15957_gshared (Enumerator_t2989 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15957(__this, method) (( void (*) (Enumerator_t2989 *, const MethodInfo*))Enumerator_VerifyState_m15957_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m15958_gshared (Enumerator_t2989 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m15958(__this, method) (( void (*) (Enumerator_t2989 *, const MethodInfo*))Enumerator_VerifyCurrent_m15958_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m15959_gshared (Enumerator_t2989 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15959(__this, method) (( void (*) (Enumerator_t2989 *, const MethodInfo*))Enumerator_Dispose_m15959_gshared)(__this, method)
