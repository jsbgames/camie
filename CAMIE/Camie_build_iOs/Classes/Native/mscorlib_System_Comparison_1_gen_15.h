﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t219;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Component>
struct  Comparison_1_t3035  : public MulticastDelegate_t549
{
};
