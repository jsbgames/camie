﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
struct KeyValuePair_2_t3407;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m21843_gshared (KeyValuePair_2_t3407 * __this, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m21843(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3407 *, Object_t *, uint8_t, const MethodInfo*))KeyValuePair_2__ctor_m21843_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m21844_gshared (KeyValuePair_2_t3407 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m21844(__this, method) (( Object_t * (*) (KeyValuePair_2_t3407 *, const MethodInfo*))KeyValuePair_2_get_Key_m21844_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m21845_gshared (KeyValuePair_2_t3407 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m21845(__this, ___value, method) (( void (*) (KeyValuePair_2_t3407 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m21845_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>::get_Value()
extern "C" uint8_t KeyValuePair_2_get_Value_m21846_gshared (KeyValuePair_2_t3407 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m21846(__this, method) (( uint8_t (*) (KeyValuePair_2_t3407 *, const MethodInfo*))KeyValuePair_2_get_Value_m21846_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m21847_gshared (KeyValuePair_2_t3407 * __this, uint8_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m21847(__this, ___value, method) (( void (*) (KeyValuePair_2_t3407 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Value_m21847_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m21848_gshared (KeyValuePair_2_t3407 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m21848(__this, method) (( String_t* (*) (KeyValuePair_2_t3407 *, const MethodInfo*))KeyValuePair_2_ToString_m21848_gshared)(__this, method)
