﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>
struct InternalEnumerator_1_t2804;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13522_gshared (InternalEnumerator_1_t2804 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13522(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2804 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13522_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13523_gshared (InternalEnumerator_1_t2804 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13523(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2804 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13523_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13524_gshared (InternalEnumerator_1_t2804 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13524(__this, method) (( void (*) (InternalEnumerator_1_t2804 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13524_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13525_gshared (InternalEnumerator_1_t2804 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13525(__this, method) (( bool (*) (InternalEnumerator_1_t2804 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13525_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
extern "C" RaycastHit_t24  InternalEnumerator_1_get_Current_m13526_gshared (InternalEnumerator_1_t2804 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13526(__this, method) (( RaycastHit_t24  (*) (InternalEnumerator_1_t2804 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13526_gshared)(__this, method)
