﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>
struct Enumerator_t3147;
// System.Object
struct Object_t;
// UnityEngine.UI.Selectable
struct Selectable_t510;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t578;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m18350(__this, ___l, method) (( void (*) (Enumerator_t3147 *, List_1_t578 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18351(__this, method) (( Object_t * (*) (Enumerator_t3147 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::Dispose()
#define Enumerator_Dispose_m18352(__this, method) (( void (*) (Enumerator_t3147 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::VerifyState()
#define Enumerator_VerifyState_m18353(__this, method) (( void (*) (Enumerator_t3147 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::MoveNext()
#define Enumerator_MoveNext_m18354(__this, method) (( bool (*) (Enumerator_t3147 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::get_Current()
#define Enumerator_get_Current_m18355(__this, method) (( Selectable_t510 * (*) (Enumerator_t3147 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
