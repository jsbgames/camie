﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$96
struct U24ArrayTypeU2496_t2267;
struct U24ArrayTypeU2496_t2267_marshaled;

void U24ArrayTypeU2496_t2267_marshal(const U24ArrayTypeU2496_t2267& unmarshaled, U24ArrayTypeU2496_t2267_marshaled& marshaled);
void U24ArrayTypeU2496_t2267_marshal_back(const U24ArrayTypeU2496_t2267_marshaled& marshaled, U24ArrayTypeU2496_t2267& unmarshaled);
void U24ArrayTypeU2496_t2267_marshal_cleanup(U24ArrayTypeU2496_t2267_marshaled& marshaled);
