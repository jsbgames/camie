﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Reporter/Log>
struct IList_1_t2901;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Log>
struct  ReadOnlyCollection_1_t2902  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Log>::list
	Object_t* ___list_0;
};
