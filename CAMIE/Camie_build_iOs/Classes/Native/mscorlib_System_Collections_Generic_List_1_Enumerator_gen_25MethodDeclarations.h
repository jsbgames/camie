﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>
struct Enumerator_t3153;
// System.Object
struct Object_t;
// UnityEngine.CanvasGroup
struct CanvasGroup_t672;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t579;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m18443(__this, ___l, method) (( void (*) (Enumerator_t3153 *, List_1_t579 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18444(__this, method) (( Object_t * (*) (Enumerator_t3153 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::Dispose()
#define Enumerator_Dispose_m18445(__this, method) (( void (*) (Enumerator_t3153 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::VerifyState()
#define Enumerator_VerifyState_m18446(__this, method) (( void (*) (Enumerator_t3153 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::MoveNext()
#define Enumerator_MoveNext_m18447(__this, method) (( bool (*) (Enumerator_t3153 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::get_Current()
#define Enumerator_get_Current_m18448(__this, method) (( CanvasGroup_t672 * (*) (Enumerator_t3153 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
