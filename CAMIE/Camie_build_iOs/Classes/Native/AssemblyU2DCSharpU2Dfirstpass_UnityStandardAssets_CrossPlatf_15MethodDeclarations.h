﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct TiltInput_t45;

// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::.ctor()
extern "C" void TiltInput__ctor_m158 (TiltInput_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnEnable()
extern "C" void TiltInput_OnEnable_m159 (TiltInput_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::Update()
extern "C" void TiltInput_Update_m160 (TiltInput_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnDisable()
extern "C" void TiltInput_OnDisable_m161 (TiltInput_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
