﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t2315;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericComparer_1__ctor_m12640_gshared (GenericComparer_1_t2315 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m12640(__this, method) (( void (*) (GenericComparer_1_t2315 *, const MethodInfo*))GenericComparer_1__ctor_m12640_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m22490_gshared (GenericComparer_1_t2315 * __this, DateTimeOffset_t1086  ___x, DateTimeOffset_t1086  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m22490(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2315 *, DateTimeOffset_t1086 , DateTimeOffset_t1086 , const MethodInfo*))GenericComparer_1_Compare_m22490_gshared)(__this, ___x, ___y, method)
