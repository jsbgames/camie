﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIText
struct GUIText_t181;
// System.String
struct String_t;

// System.Void UnityEngine.GUIText::set_text(System.String)
extern "C" void GUIText_set_text_m998 (GUIText_t181 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
