﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>
struct Enumerator_t3301;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t3292;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20456_gshared (Enumerator_t3301 * __this, Dictionary_2_t3292 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m20456(__this, ___host, method) (( void (*) (Enumerator_t3301 *, Dictionary_2_t3292 *, const MethodInfo*))Enumerator__ctor_m20456_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20457_gshared (Enumerator_t3301 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20457(__this, method) (( Object_t * (*) (Enumerator_t3301 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20457_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m20458_gshared (Enumerator_t3301 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20458(__this, method) (( void (*) (Enumerator_t3301 *, const MethodInfo*))Enumerator_Dispose_m20458_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20459_gshared (Enumerator_t3301 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20459(__this, method) (( bool (*) (Enumerator_t3301 *, const MethodInfo*))Enumerator_MoveNext_m20459_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m20460_gshared (Enumerator_t3301 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20460(__this, method) (( Object_t * (*) (Enumerator_t3301 *, const MethodInfo*))Enumerator_get_Current_m20460_gshared)(__this, method)
