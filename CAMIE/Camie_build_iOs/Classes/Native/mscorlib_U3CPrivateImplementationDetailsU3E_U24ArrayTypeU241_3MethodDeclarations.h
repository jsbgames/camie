﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$124
struct U24ArrayTypeU24124_t2266;
struct U24ArrayTypeU24124_t2266_marshaled;

void U24ArrayTypeU24124_t2266_marshal(const U24ArrayTypeU24124_t2266& unmarshaled, U24ArrayTypeU24124_t2266_marshaled& marshaled);
void U24ArrayTypeU24124_t2266_marshal_back(const U24ArrayTypeU24124_t2266_marshaled& marshaled, U24ArrayTypeU24124_t2266& unmarshaled);
void U24ArrayTypeU24124_t2266_marshal_cleanup(U24ArrayTypeU24124_t2266_marshaled& marshaled);
