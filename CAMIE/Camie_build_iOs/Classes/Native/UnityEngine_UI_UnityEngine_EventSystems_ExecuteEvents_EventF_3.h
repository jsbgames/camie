﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IPointerDownHandler
struct IPointerDownHandler_t280;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t453;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct  EventFunction_1_t464  : public MulticastDelegate_t549
{
};
