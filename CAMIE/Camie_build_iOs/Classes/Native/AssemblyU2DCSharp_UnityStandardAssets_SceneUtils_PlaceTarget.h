﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t78;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityStandardAssets.SceneUtils.PlaceTargetWithMouse
struct  PlaceTargetWithMouse_t328  : public MonoBehaviour_t3
{
	// System.Single UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::surfaceOffset
	float ___surfaceOffset_2;
	// UnityEngine.GameObject UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::setTargetOn
	GameObject_t78 * ___setTargetOn_3;
};
