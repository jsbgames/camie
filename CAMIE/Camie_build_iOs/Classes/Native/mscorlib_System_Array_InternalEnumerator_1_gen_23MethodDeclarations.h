﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>
struct InternalEnumerator_1_t2870;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPoint.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m14253_gshared (InternalEnumerator_1_t2870 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m14253(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2870 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14253_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14254_gshared (InternalEnumerator_1_t2870 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14254(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2870 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14254_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m14255_gshared (InternalEnumerator_1_t2870 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14255(__this, method) (( void (*) (InternalEnumerator_1_t2870 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14255_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m14256_gshared (InternalEnumerator_1_t2870 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14256(__this, method) (( bool (*) (InternalEnumerator_1_t2870 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14256_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
extern "C" ContactPoint_t246  InternalEnumerator_1_get_Current_m14257_gshared (InternalEnumerator_1_t2870 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14257(__this, method) (( ContactPoint_t246  (*) (InternalEnumerator_1_t2870 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14257_gshared)(__this, method)
