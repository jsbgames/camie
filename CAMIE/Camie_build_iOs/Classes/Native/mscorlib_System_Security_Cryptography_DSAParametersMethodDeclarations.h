﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSAParameters
struct DSAParameters_t1453;
struct DSAParameters_t1453_marshaled;

void DSAParameters_t1453_marshal(const DSAParameters_t1453& unmarshaled, DSAParameters_t1453_marshaled& marshaled);
void DSAParameters_t1453_marshal_back(const DSAParameters_t1453_marshaled& marshaled, DSAParameters_t1453& unmarshaled);
void DSAParameters_t1453_marshal_cleanup(DSAParameters_t1453_marshaled& marshaled);
