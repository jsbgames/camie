﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>
struct KeyValuePair_2_t3270;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20058(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3270 *, String_t*, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m19960_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Key()
#define KeyValuePair_2_get_Key_m20059(__this, method) (( String_t* (*) (KeyValuePair_2_t3270 *, const MethodInfo*))KeyValuePair_2_get_Key_m19961_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20060(__this, ___value, method) (( void (*) (KeyValuePair_2_t3270 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m19962_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Value()
#define KeyValuePair_2_get_Value_m20061(__this, method) (( int64_t (*) (KeyValuePair_2_t3270 *, const MethodInfo*))KeyValuePair_2_get_Value_m19963_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20062(__this, ___value, method) (( void (*) (KeyValuePair_2_t3270 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m19964_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::ToString()
#define KeyValuePair_2_ToString_m20063(__this, method) (( String_t* (*) (KeyValuePair_2_t3270 *, const MethodInfo*))KeyValuePair_2_ToString_m19965_gshared)(__this, method)
