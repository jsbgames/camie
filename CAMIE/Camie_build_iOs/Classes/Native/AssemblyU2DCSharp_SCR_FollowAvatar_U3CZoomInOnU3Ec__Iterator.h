﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t1;
// System.Object
struct Object_t;
// SCR_FollowAvatar
struct SCR_FollowAvatar_t352;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// SCR_FollowAvatar/<ZoomInOn>c__Iterator7
struct  U3CZoomInOnU3Ec__Iterator7_t353  : public Object_t
{
	// System.Single SCR_FollowAvatar/<ZoomInOn>c__Iterator7::<time>__0
	float ___U3CtimeU3E__0_0;
	// UnityEngine.Vector3 SCR_FollowAvatar/<ZoomInOn>c__Iterator7::<toVector>__1
	Vector3_t4  ___U3CtoVectorU3E__1_1;
	// System.Boolean SCR_FollowAvatar/<ZoomInOn>c__Iterator7::<zoomIn>__2
	bool ___U3CzoomInU3E__2_2;
	// UnityEngine.Transform SCR_FollowAvatar/<ZoomInOn>c__Iterator7::zoomTo
	Transform_t1 * ___zoomTo_3;
	// System.Int32 SCR_FollowAvatar/<ZoomInOn>c__Iterator7::$PC
	int32_t ___U24PC_4;
	// System.Object SCR_FollowAvatar/<ZoomInOn>c__Iterator7::$current
	Object_t * ___U24current_5;
	// UnityEngine.Transform SCR_FollowAvatar/<ZoomInOn>c__Iterator7::<$>zoomTo
	Transform_t1 * ___U3CU24U3EzoomTo_6;
	// SCR_FollowAvatar SCR_FollowAvatar/<ZoomInOn>c__Iterator7::<>f__this
	SCR_FollowAvatar_t352 * ___U3CU3Ef__this_7;
};
