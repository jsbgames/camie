﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1110;
// System.String
struct String_t;

// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
extern "C" void InternalsVisibleToAttribute__ctor_m5255 (InternalsVisibleToAttribute_t1110 * __this, String_t* ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
