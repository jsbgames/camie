﻿#pragma once
#include <stdint.h>
// System.WeakReference
struct WeakReference_t2009;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// System.Runtime.Remoting.ClientIdentity
struct  ClientIdentity_t2010  : public Identity_t2002
{
	// System.WeakReference System.Runtime.Remoting.ClientIdentity::_proxyReference
	WeakReference_t2009 * ____proxyReference_5;
};
