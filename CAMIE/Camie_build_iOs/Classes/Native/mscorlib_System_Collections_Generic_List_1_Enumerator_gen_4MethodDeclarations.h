﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_t2843;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t655;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m13983_gshared (Enumerator_t2843 * __this, List_1_t655 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m13983(__this, ___l, method) (( void (*) (Enumerator_t2843 *, List_1_t655 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared (Enumerator_t2843 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13984(__this, method) (( Object_t * (*) (Enumerator_t2843 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m13985_gshared (Enumerator_t2843 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13985(__this, method) (( void (*) (Enumerator_t2843 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m13986_gshared (Enumerator_t2843 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m13986(__this, method) (( void (*) (Enumerator_t2843 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13987_gshared (Enumerator_t2843 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13987(__this, method) (( bool (*) (Enumerator_t2843 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m13988_gshared (Enumerator_t2843 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13988(__this, method) (( Object_t * (*) (Enumerator_t2843 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
