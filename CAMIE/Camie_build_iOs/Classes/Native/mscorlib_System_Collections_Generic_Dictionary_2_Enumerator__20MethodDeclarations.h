﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t3214;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t302;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t831;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"
#define Enumerator__ctor_m19259(__this, ___dictionary, method) (( void (*) (Enumerator_t3214 *, Dictionary_2_t831 *, const MethodInfo*))Enumerator__ctor_m13691_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19260(__this, method) (( Object_t * (*) (Enumerator_t3214 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13692_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19261(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3214 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13693_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19262(__this, method) (( Object_t * (*) (Enumerator_t3214 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13694_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19263(__this, method) (( Object_t * (*) (Enumerator_t3214 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13695_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m19264(__this, method) (( bool (*) (Enumerator_t3214 *, const MethodInfo*))Enumerator_MoveNext_m13696_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m19265(__this, method) (( KeyValuePair_2_t3212  (*) (Enumerator_t3214 *, const MethodInfo*))Enumerator_get_Current_m13697_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m19266(__this, method) (( String_t* (*) (Enumerator_t3214 *, const MethodInfo*))Enumerator_get_CurrentKey_m13698_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m19267(__this, method) (( GUIStyle_t302 * (*) (Enumerator_t3214 *, const MethodInfo*))Enumerator_get_CurrentValue_m13699_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyState()
#define Enumerator_VerifyState_m19268(__this, method) (( void (*) (Enumerator_t3214 *, const MethodInfo*))Enumerator_VerifyState_m13700_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m19269(__this, method) (( void (*) (Enumerator_t3214 *, const MethodInfo*))Enumerator_VerifyCurrent_m13701_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m19270(__this, method) (( void (*) (Enumerator_t3214 *, const MethodInfo*))Enumerator_Dispose_m13702_gshared)(__this, method)
