﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>
struct InternalEnumerator_1_t3472;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.Emit.ILTokenInfo
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22282_gshared (InternalEnumerator_1_t3472 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22282(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3472 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22282_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22283_gshared (InternalEnumerator_1_t3472 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22283(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3472 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22283_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22284_gshared (InternalEnumerator_1_t3472 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22284(__this, method) (( void (*) (InternalEnumerator_1_t3472 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22284_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22285_gshared (InternalEnumerator_1_t3472 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22285(__this, method) (( bool (*) (InternalEnumerator_1_t3472 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22285_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern "C" ILTokenInfo_t1856  InternalEnumerator_1_get_Current_m22286_gshared (InternalEnumerator_1_t3472 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22286(__this, method) (( ILTokenInfo_t1856  (*) (InternalEnumerator_1_t3472 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22286_gshared)(__this, method)
