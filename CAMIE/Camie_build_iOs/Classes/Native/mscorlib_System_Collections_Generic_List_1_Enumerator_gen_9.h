﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Reporter/Log>
struct List_1_t298;
// Reporter/Log
struct Log_t291;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Reporter/Log>
struct  Enumerator_t2904 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Reporter/Log>::l
	List_1_t298 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Reporter/Log>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Reporter/Log>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Reporter/Log>::current
	Log_t291 * ___current_3;
};
