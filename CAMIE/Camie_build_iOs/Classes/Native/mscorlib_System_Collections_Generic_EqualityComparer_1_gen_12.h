﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Guid>
struct EqualityComparer_1_t3507;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Guid>
struct  EqualityComparer_1_t3507  : public Object_t
{
};
struct EqualityComparer_1_t3507_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Guid>::_default
	EqualityComparer_1_t3507 * ____default_0;
};
