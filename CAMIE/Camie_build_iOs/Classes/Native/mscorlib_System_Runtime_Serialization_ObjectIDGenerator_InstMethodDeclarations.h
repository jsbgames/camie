﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer
struct InstanceComparer_t2048;
// System.Object
struct Object_t;

// System.Void System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer::.ctor()
extern "C" void InstanceComparer__ctor_m10905 (InstanceComparer_t2048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t InstanceComparer_System_Collections_IComparer_Compare_m10906 (InstanceComparer_t2048 * __this, Object_t * ___o1, Object_t * ___o2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer::System.Collections.IHashCodeProvider.GetHashCode(System.Object)
extern "C" int32_t InstanceComparer_System_Collections_IHashCodeProvider_GetHashCode_m10907 (InstanceComparer_t2048 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
