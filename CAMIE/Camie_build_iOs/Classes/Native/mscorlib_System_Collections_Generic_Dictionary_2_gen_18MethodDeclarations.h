﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct Dictionary_2_t1081;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1032;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct ICollection_1_t3790;
// System.Object
struct Object_t;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t937;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct KeyCollection_t3333;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct ValueCollection_t3334;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t2789;
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct IDictionary_2_t1037;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1043;
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
struct KeyValuePair_2U5BU5D_t3791;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>
struct IEnumerator_1_t1090;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1429;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__25.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_22MethodDeclarations.h"
#define Dictionary_2__ctor_m5201(__this, method) (( void (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2__ctor_m13547_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m20929(__this, ___comparer, method) (( void (*) (Dictionary_2_t1081 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13549_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m20930(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1081 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13551_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Int32)
#define Dictionary_2__ctor_m20931(__this, ___capacity, method) (( void (*) (Dictionary_2_t1081 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m13553_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m20932(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1081 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13555_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m20933(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1081 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2__ctor_m13557_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20934(__this, method) (( Object_t* (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13559_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20935(__this, method) (( Object_t* (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13561_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m20936(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1081 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m13563_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m20937(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1081 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m13565_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m20938(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1081 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m13567_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m20939(__this, ___key, method) (( bool (*) (Dictionary_2_t1081 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m13569_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m20940(__this, ___key, method) (( void (*) (Dictionary_2_t1081 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m13571_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20941(__this, method) (( bool (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13573_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20942(__this, method) (( Object_t * (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13575_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20943(__this, method) (( bool (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20944(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1081 *, KeyValuePair_2_t1088 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13579_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20945(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1081 *, KeyValuePair_2_t1088 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13581_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20946(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1081 *, KeyValuePair_2U5BU5D_t3791*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13583_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20947(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1081 *, KeyValuePair_2_t1088 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13585_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m20948(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1081 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m13587_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20949(__this, method) (( Object_t * (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13589_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20950(__this, method) (( Object_t* (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13591_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20951(__this, method) (( Object_t * (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13593_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Count()
#define Dictionary_2_get_Count_m20952(__this, method) (( int32_t (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_get_Count_m13595_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Item(TKey)
#define Dictionary_2_get_Item_m20953(__this, ___key, method) (( GetDelegate_t937 * (*) (Dictionary_2_t1081 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m13597_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m20954(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1081 *, String_t*, GetDelegate_t937 *, const MethodInfo*))Dictionary_2_set_Item_m13599_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m20955(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1081 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m13601_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m20956(__this, ___size, method) (( void (*) (Dictionary_2_t1081 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m13603_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m20957(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1081 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m13605_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m20958(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1088  (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t937 *, const MethodInfo*))Dictionary_2_make_pair_m13607_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m20959(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t937 *, const MethodInfo*))Dictionary_2_pick_key_m13609_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m20960(__this /* static, unused */, ___key, ___value, method) (( GetDelegate_t937 * (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t937 *, const MethodInfo*))Dictionary_2_pick_value_m13611_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m20961(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1081 *, KeyValuePair_2U5BU5D_t3791*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m13613_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Resize()
#define Dictionary_2_Resize_m20962(__this, method) (( void (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_Resize_m13615_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Add(TKey,TValue)
#define Dictionary_2_Add_m20963(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1081 *, String_t*, GetDelegate_t937 *, const MethodInfo*))Dictionary_2_Add_m13617_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Clear()
#define Dictionary_2_Clear_m20964(__this, method) (( void (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_Clear_m13619_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m20965(__this, ___key, method) (( bool (*) (Dictionary_2_t1081 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m13621_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m20966(__this, ___value, method) (( bool (*) (Dictionary_2_t1081 *, GetDelegate_t937 *, const MethodInfo*))Dictionary_2_ContainsValue_m13623_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m20967(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1081 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2_GetObjectData_m13625_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m20968(__this, ___sender, method) (( void (*) (Dictionary_2_t1081 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m13627_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Remove(TKey)
#define Dictionary_2_Remove_m20969(__this, ___key, method) (( bool (*) (Dictionary_2_t1081 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m13629_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m20970(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1081 *, String_t*, GetDelegate_t937 **, const MethodInfo*))Dictionary_2_TryGetValue_m13631_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Keys()
#define Dictionary_2_get_Keys_m20971(__this, method) (( KeyCollection_t3333 * (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_get_Keys_m13633_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Values()
#define Dictionary_2_get_Values_m20972(__this, method) (( ValueCollection_t3334 * (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_get_Values_m13635_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m20973(__this, ___key, method) (( String_t* (*) (Dictionary_2_t1081 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m13637_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m20974(__this, ___value, method) (( GetDelegate_t937 * (*) (Dictionary_2_t1081 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m13639_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m20975(__this, ___pair, method) (( bool (*) (Dictionary_2_t1081 *, KeyValuePair_2_t1088 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m13641_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m20976(__this, method) (( Enumerator_t3335  (*) (Dictionary_2_t1081 *, const MethodInfo*))Dictionary_2_GetEnumerator_m13643_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m20977(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1430  (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t937 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m13645_gshared)(__this /* static, unused */, ___key, ___value, method)
