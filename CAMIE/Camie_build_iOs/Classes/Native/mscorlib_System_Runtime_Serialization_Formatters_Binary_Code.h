﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1872;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Serialization.Formatters.Binary.CodeGenerator
struct  CodeGenerator_t2028  : public Object_t
{
};
struct CodeGenerator_t2028_StaticFields{
	// System.Object System.Runtime.Serialization.Formatters.Binary.CodeGenerator::monitor
	Object_t * ___monitor_0;
	// System.Reflection.Emit.ModuleBuilder System.Runtime.Serialization.Formatters.Binary.CodeGenerator::_module
	ModuleBuilder_t1872 * ____module_1;
};
