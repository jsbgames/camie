﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct Enumerator_t1050;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t798;
// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct List_1_t795;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m18952(__this, ___l, method) (( void (*) (Enumerator_t1050 *, List_1_t795 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18953(__this, method) (( Object_t * (*) (Enumerator_t1050 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Dispose()
#define Enumerator_Dispose_m18954(__this, method) (( void (*) (Enumerator_t1050 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::VerifyState()
#define Enumerator_VerifyState_m18955(__this, method) (( void (*) (Enumerator_t1050 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::MoveNext()
#define Enumerator_MoveNext_m5095(__this, method) (( bool (*) (Enumerator_t1050 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::get_Current()
#define Enumerator_get_Current_m5094(__this, method) (( GcLeaderboard_t798 * (*) (Enumerator_t1050 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
