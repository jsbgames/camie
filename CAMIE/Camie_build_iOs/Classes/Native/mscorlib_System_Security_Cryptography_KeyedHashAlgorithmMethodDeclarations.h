﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.KeyedHashAlgorithm
struct KeyedHashAlgorithm_t1563;
// System.Byte[]
struct ByteU5BU5D_t850;

// System.Void System.Security.Cryptography.KeyedHashAlgorithm::.ctor()
extern "C" void KeyedHashAlgorithm__ctor_m7545 (KeyedHashAlgorithm_t1563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::Finalize()
extern "C" void KeyedHashAlgorithm_Finalize_m7614 (KeyedHashAlgorithm_t1563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::get_Key()
extern "C" ByteU5BU5D_t850* KeyedHashAlgorithm_get_Key_m11099 (KeyedHashAlgorithm_t1563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::set_Key(System.Byte[])
extern "C" void KeyedHashAlgorithm_set_Key_m11100 (KeyedHashAlgorithm_t1563 * __this, ByteU5BU5D_t850* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::Dispose(System.Boolean)
extern "C" void KeyedHashAlgorithm_Dispose_m7615 (KeyedHashAlgorithm_t1563 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::ZeroizeKey()
extern "C" void KeyedHashAlgorithm_ZeroizeKey_m11101 (KeyedHashAlgorithm_t1563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
