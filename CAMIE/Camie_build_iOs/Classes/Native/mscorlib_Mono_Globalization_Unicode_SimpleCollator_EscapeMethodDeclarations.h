﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/Escape
struct Escape_t1703;
struct Escape_t1703_marshaled;

void Escape_t1703_marshal(const Escape_t1703& unmarshaled, Escape_t1703_marshaled& marshaled);
void Escape_t1703_marshal_back(const Escape_t1703_marshaled& marshaled, Escape_t1703& unmarshaled);
void Escape_t1703_marshal_cleanup(Escape_t1703_marshaled& marshaled);
