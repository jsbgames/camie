﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t3491;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Security.Policy.StrongName>
struct  List_1_t2312  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Security.Policy.StrongName>::_items
	StrongNameU5BU5D_t3491* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::_version
	int32_t ____version_3;
};
struct List_1_t2312_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Security.Policy.StrongName>::EmptyArray
	StrongNameU5BU5D_t3491* ___EmptyArray_4;
};
