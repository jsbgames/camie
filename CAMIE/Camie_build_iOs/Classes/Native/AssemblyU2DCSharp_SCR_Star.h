﻿#pragma once
#include <stdint.h>
// UnityEngine.Animator
struct Animator_t9;
// SCR_Collectible
#include "AssemblyU2DCSharp_SCR_Collectible.h"
// SCR_Star
struct  SCR_Star_t362  : public SCR_Collectible_t331
{
	// UnityEngine.Animator SCR_Star::starAnim
	Animator_t9 * ___starAnim_4;
};
