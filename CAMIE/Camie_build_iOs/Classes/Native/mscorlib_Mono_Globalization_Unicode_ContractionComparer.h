﻿#pragma once
#include <stdint.h>
// Mono.Globalization.Unicode.ContractionComparer
struct ContractionComparer_t1695;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Globalization.Unicode.ContractionComparer
struct  ContractionComparer_t1695  : public Object_t
{
};
struct ContractionComparer_t1695_StaticFields{
	// Mono.Globalization.Unicode.ContractionComparer Mono.Globalization.Unicode.ContractionComparer::Instance
	ContractionComparer_t1695 * ___Instance_0;
};
