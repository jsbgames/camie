﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AsyncOperation
struct AsyncOperation_t775;
struct AsyncOperation_t775_marshaled;

// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C" void AsyncOperation__ctor_m4293 (AsyncOperation_t775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C" void AsyncOperation_InternalDestroy_m4294 (AsyncOperation_t775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C" void AsyncOperation_Finalize_m4295 (AsyncOperation_t775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void AsyncOperation_t775_marshal(const AsyncOperation_t775& unmarshaled, AsyncOperation_t775_marshaled& marshaled);
void AsyncOperation_t775_marshal_back(const AsyncOperation_t775_marshaled& marshaled, AsyncOperation_t775& unmarshaled);
void AsyncOperation_t775_marshal_cleanup(AsyncOperation_t775_marshaled& marshaled);
