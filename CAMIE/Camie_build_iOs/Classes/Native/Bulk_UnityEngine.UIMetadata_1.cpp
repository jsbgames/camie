﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEvent.h"
// Metadata Definition UnityEngine.UI.ScrollRect/ScrollRectEvent
extern TypeInfo ScrollRectEvent_t573_il2cpp_TypeInfo;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEventMethodDeclarations.h"
extern const Il2CppType Void_t272_0_0_0;
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect/ScrollRectEvent::.ctor()
extern const MethodInfo ScrollRectEvent__ctor_m2579_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScrollRectEvent__ctor_m2579/* method */
	, &ScrollRectEvent_t573_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ScrollRectEvent_t573_MethodInfos[] =
{
	&ScrollRectEvent__ctor_m2579_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
extern const MethodInfo UnityEventBase_ToString_m3581_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo;
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m3609_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m3610_GenericMethod;
static const Il2CppMethodReference ScrollRectEvent_t573_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&UnityEventBase_ToString_m3581_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m3609_GenericMethod,
	&UnityEvent_1_GetDelegate_m3610_GenericMethod,
};
static bool ScrollRectEvent_t573_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
extern const Il2CppType ISerializationCallbackReceiver_t731_0_0_0;
static Il2CppInterfaceOffsetPair ScrollRectEvent_t573_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScrollRectEvent_t573_0_0_0;
extern const Il2CppType ScrollRectEvent_t573_1_0_0;
extern const Il2CppType UnityEvent_1_t574_0_0_0;
extern TypeInfo ScrollRect_t575_il2cpp_TypeInfo;
extern const Il2CppType ScrollRect_t575_0_0_0;
struct ScrollRectEvent_t573;
const Il2CppTypeDefinitionMetadata ScrollRectEvent_t573_DefinitionMetadata = 
{
	&ScrollRect_t575_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScrollRectEvent_t573_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t574_0_0_0/* parent */
	, ScrollRectEvent_t573_VTable/* vtableMethods */
	, ScrollRectEvent_t573_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ScrollRectEvent_t573_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRectEvent"/* name */
	, ""/* namespaze */
	, ScrollRectEvent_t573_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ScrollRectEvent_t573_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScrollRectEvent_t573_0_0_0/* byval_arg */
	, &ScrollRectEvent_t573_1_0_0/* this_arg */
	, &ScrollRectEvent_t573_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRectEvent_t573)/* instance_size */
	, sizeof (ScrollRectEvent_t573)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect.h"
// Metadata Definition UnityEngine.UI.ScrollRect
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRectMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::.ctor()
extern const MethodInfo ScrollRect__ctor_m2580_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScrollRect__ctor_m2580/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::get_content()
extern const MethodInfo ScrollRect_get_content_m2581_MethodInfo = 
{
	"get_content"/* name */
	, (methodPointerType)&ScrollRect_get_content_m2581/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t364_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_content_m2582_ParameterInfos[] = 
{
	{"value", 0, 134218190, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_content(UnityEngine.RectTransform)
extern const MethodInfo ScrollRect_set_content_m2582_MethodInfo = 
{
	"set_content"/* name */
	, (methodPointerType)&ScrollRect_set_content_m2582/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_content_m2582_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_horizontal()
extern const MethodInfo ScrollRect_get_horizontal_m2583_MethodInfo = 
{
	"get_horizontal"/* name */
	, (methodPointerType)&ScrollRect_get_horizontal_m2583/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_horizontal_m2584_ParameterInfos[] = 
{
	{"value", 0, 134218191, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontal(System.Boolean)
extern const MethodInfo ScrollRect_set_horizontal_m2584_MethodInfo = 
{
	"set_horizontal"/* name */
	, (methodPointerType)&ScrollRect_set_horizontal_m2584/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_horizontal_m2584_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_vertical()
extern const MethodInfo ScrollRect_get_vertical_m2585_MethodInfo = 
{
	"get_vertical"/* name */
	, (methodPointerType)&ScrollRect_get_vertical_m2585/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_vertical_m2586_ParameterInfos[] = 
{
	{"value", 0, 134218192, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_vertical(System.Boolean)
extern const MethodInfo ScrollRect_set_vertical_m2586_MethodInfo = 
{
	"set_vertical"/* name */
	, (methodPointerType)&ScrollRect_set_vertical_m2586/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_vertical_m2586_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MovementType_t572_0_0_0;
extern void* RuntimeInvoker_MovementType_t572 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::get_movementType()
extern const MethodInfo ScrollRect_get_movementType_m2587_MethodInfo = 
{
	"get_movementType"/* name */
	, (methodPointerType)&ScrollRect_get_movementType_m2587/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &MovementType_t572_0_0_0/* return_type */
	, RuntimeInvoker_MovementType_t572/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MovementType_t572_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_movementType_m2588_ParameterInfos[] = 
{
	{"value", 0, 134218193, 0, &MovementType_t572_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_movementType(UnityEngine.UI.ScrollRect/MovementType)
extern const MethodInfo ScrollRect_set_movementType_m2588_MethodInfo = 
{
	"set_movementType"/* name */
	, (methodPointerType)&ScrollRect_set_movementType_m2588/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_movementType_m2588_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_elasticity()
extern const MethodInfo ScrollRect_get_elasticity_m2589_MethodInfo = 
{
	"get_elasticity"/* name */
	, (methodPointerType)&ScrollRect_get_elasticity_m2589/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_elasticity_m2590_ParameterInfos[] = 
{
	{"value", 0, 134218194, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_elasticity(System.Single)
extern const MethodInfo ScrollRect_set_elasticity_m2590_MethodInfo = 
{
	"set_elasticity"/* name */
	, (methodPointerType)&ScrollRect_set_elasticity_m2590/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_elasticity_m2590_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_inertia()
extern const MethodInfo ScrollRect_get_inertia_m2591_MethodInfo = 
{
	"get_inertia"/* name */
	, (methodPointerType)&ScrollRect_get_inertia_m2591/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_inertia_m2592_ParameterInfos[] = 
{
	{"value", 0, 134218195, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_inertia(System.Boolean)
extern const MethodInfo ScrollRect_set_inertia_m2592_MethodInfo = 
{
	"set_inertia"/* name */
	, (methodPointerType)&ScrollRect_set_inertia_m2592/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_inertia_m2592_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_decelerationRate()
extern const MethodInfo ScrollRect_get_decelerationRate_m2593_MethodInfo = 
{
	"get_decelerationRate"/* name */
	, (methodPointerType)&ScrollRect_get_decelerationRate_m2593/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_decelerationRate_m2594_ParameterInfos[] = 
{
	{"value", 0, 134218196, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_decelerationRate(System.Single)
extern const MethodInfo ScrollRect_set_decelerationRate_m2594_MethodInfo = 
{
	"set_decelerationRate"/* name */
	, (methodPointerType)&ScrollRect_set_decelerationRate_m2594/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_decelerationRate_m2594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_scrollSensitivity()
extern const MethodInfo ScrollRect_get_scrollSensitivity_m2595_MethodInfo = 
{
	"get_scrollSensitivity"/* name */
	, (methodPointerType)&ScrollRect_get_scrollSensitivity_m2595/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_scrollSensitivity_m2596_ParameterInfos[] = 
{
	{"value", 0, 134218197, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_scrollSensitivity(System.Single)
extern const MethodInfo ScrollRect_set_scrollSensitivity_m2596_MethodInfo = 
{
	"set_scrollSensitivity"/* name */
	, (methodPointerType)&ScrollRect_set_scrollSensitivity_m2596/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_scrollSensitivity_m2596_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t569_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::get_horizontalScrollbar()
extern const MethodInfo ScrollRect_get_horizontalScrollbar_m2597_MethodInfo = 
{
	"get_horizontalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_get_horizontalScrollbar_m2597/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t569_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t569_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_horizontalScrollbar_m2598_ParameterInfos[] = 
{
	{"value", 0, 134218198, 0, &Scrollbar_t569_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontalScrollbar(UnityEngine.UI.Scrollbar)
extern const MethodInfo ScrollRect_set_horizontalScrollbar_m2598_MethodInfo = 
{
	"set_horizontalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_set_horizontalScrollbar_m2598/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_horizontalScrollbar_m2598_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::get_verticalScrollbar()
extern const MethodInfo ScrollRect_get_verticalScrollbar_m2599_MethodInfo = 
{
	"get_verticalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_get_verticalScrollbar_m2599/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t569_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t569_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_verticalScrollbar_m2600_ParameterInfos[] = 
{
	{"value", 0, 134218199, 0, &Scrollbar_t569_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_verticalScrollbar(UnityEngine.UI.Scrollbar)
extern const MethodInfo ScrollRect_set_verticalScrollbar_m2600_MethodInfo = 
{
	"set_verticalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_set_verticalScrollbar_m2600/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_verticalScrollbar_m2600_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::get_onValueChanged()
extern const MethodInfo ScrollRect_get_onValueChanged_m2601_MethodInfo = 
{
	"get_onValueChanged"/* name */
	, (methodPointerType)&ScrollRect_get_onValueChanged_m2601/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &ScrollRectEvent_t573_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScrollRectEvent_t573_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_onValueChanged_m2602_ParameterInfos[] = 
{
	{"value", 0, 134218200, 0, &ScrollRectEvent_t573_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_onValueChanged(UnityEngine.UI.ScrollRect/ScrollRectEvent)
extern const MethodInfo ScrollRect_set_onValueChanged_m2602_MethodInfo = 
{
	"set_onValueChanged"/* name */
	, (methodPointerType)&ScrollRect_set_onValueChanged_m2602/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_onValueChanged_m2602_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::get_viewRect()
extern const MethodInfo ScrollRect_get_viewRect_m2603_MethodInfo = 
{
	"get_viewRect"/* name */
	, (methodPointerType)&ScrollRect_get_viewRect_m2603/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t364_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
extern void* RuntimeInvoker_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::get_velocity()
extern const MethodInfo ScrollRect_get_velocity_m2604_MethodInfo = 
{
	"get_velocity"/* name */
	, (methodPointerType)&ScrollRect_get_velocity_m2604/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_velocity_m2605_ParameterInfos[] = 
{
	{"value", 0, 134218201, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_velocity(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_set_velocity_m2605_MethodInfo = 
{
	"set_velocity"/* name */
	, (methodPointerType)&ScrollRect_set_velocity_m2605/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector2_t6/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_velocity_m2605_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t511_0_0_0;
extern const Il2CppType CanvasUpdate_t511_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_Rebuild_m2606_ParameterInfos[] = 
{
	{"executing", 0, 134218202, 0, &CanvasUpdate_t511_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo ScrollRect_Rebuild_m2606_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&ScrollRect_Rebuild_m2606/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ScrollRect_t575_ScrollRect_Rebuild_m2606_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnEnable()
extern const MethodInfo ScrollRect_OnEnable_m2607_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ScrollRect_OnEnable_m2607/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnDisable()
extern const MethodInfo ScrollRect_OnDisable_m2608_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ScrollRect_OnDisable_m2608/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::IsActive()
extern const MethodInfo ScrollRect_IsActive_m2609_MethodInfo = 
{
	"IsActive"/* name */
	, (methodPointerType)&ScrollRect_IsActive_m2609/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::EnsureLayoutHasRebuilt()
extern const MethodInfo ScrollRect_EnsureLayoutHasRebuilt_m2610_MethodInfo = 
{
	"EnsureLayoutHasRebuilt"/* name */
	, (methodPointerType)&ScrollRect_EnsureLayoutHasRebuilt_m2610/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::StopMovement()
extern const MethodInfo ScrollRect_StopMovement_m2611_MethodInfo = 
{
	"StopMovement"/* name */
	, (methodPointerType)&ScrollRect_StopMovement_m2611/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_OnScroll_m2612_ParameterInfos[] = 
{
	{"data", 0, 134218203, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnScroll(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnScroll_m2612_MethodInfo = 
{
	"OnScroll"/* name */
	, (methodPointerType)&ScrollRect_OnScroll_m2612/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ScrollRect_t575_ScrollRect_OnScroll_m2612_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_OnInitializePotentialDrag_m2613_ParameterInfos[] = 
{
	{"eventData", 0, 134218204, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnInitializePotentialDrag_m2613_MethodInfo = 
{
	"OnInitializePotentialDrag"/* name */
	, (methodPointerType)&ScrollRect_OnInitializePotentialDrag_m2613/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ScrollRect_t575_ScrollRect_OnInitializePotentialDrag_m2613_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_OnBeginDrag_m2614_ParameterInfos[] = 
{
	{"eventData", 0, 134218205, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnBeginDrag_m2614_MethodInfo = 
{
	"OnBeginDrag"/* name */
	, (methodPointerType)&ScrollRect_OnBeginDrag_m2614/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ScrollRect_t575_ScrollRect_OnBeginDrag_m2614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_OnEndDrag_m2615_ParameterInfos[] = 
{
	{"eventData", 0, 134218206, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnEndDrag_m2615_MethodInfo = 
{
	"OnEndDrag"/* name */
	, (methodPointerType)&ScrollRect_OnEndDrag_m2615/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ScrollRect_t575_ScrollRect_OnEndDrag_m2615_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_OnDrag_m2616_ParameterInfos[] = 
{
	{"eventData", 0, 134218207, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnDrag_m2616_MethodInfo = 
{
	"OnDrag"/* name */
	, (methodPointerType)&ScrollRect_OnDrag_m2616/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ScrollRect_t575_ScrollRect_OnDrag_m2616_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_SetContentAnchoredPosition_m2617_ParameterInfos[] = 
{
	{"position", 0, 134218208, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetContentAnchoredPosition(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_SetContentAnchoredPosition_m2617_MethodInfo = 
{
	"SetContentAnchoredPosition"/* name */
	, (methodPointerType)&ScrollRect_SetContentAnchoredPosition_m2617/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector2_t6/* invoker_method */
	, ScrollRect_t575_ScrollRect_SetContentAnchoredPosition_m2617_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::LateUpdate()
extern const MethodInfo ScrollRect_LateUpdate_m2618_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&ScrollRect_LateUpdate_m2618/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdatePrevData()
extern const MethodInfo ScrollRect_UpdatePrevData_m2619_MethodInfo = 
{
	"UpdatePrevData"/* name */
	, (methodPointerType)&ScrollRect_UpdatePrevData_m2619/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_UpdateScrollbars_m2620_ParameterInfos[] = 
{
	{"offset", 0, 134218209, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdateScrollbars(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_UpdateScrollbars_m2620_MethodInfo = 
{
	"UpdateScrollbars"/* name */
	, (methodPointerType)&ScrollRect_UpdateScrollbars_m2620/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector2_t6/* invoker_method */
	, ScrollRect_t575_ScrollRect_UpdateScrollbars_m2620_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::get_normalizedPosition()
extern const MethodInfo ScrollRect_get_normalizedPosition_m2621_MethodInfo = 
{
	"get_normalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_normalizedPosition_m2621/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_normalizedPosition_m2622_ParameterInfos[] = 
{
	{"value", 0, 134218210, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_normalizedPosition(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_set_normalizedPosition_m2622_MethodInfo = 
{
	"set_normalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_normalizedPosition_m2622/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector2_t6/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_normalizedPosition_m2622_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_horizontalNormalizedPosition()
extern const MethodInfo ScrollRect_get_horizontalNormalizedPosition_m2623_MethodInfo = 
{
	"get_horizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_horizontalNormalizedPosition_m2623/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_horizontalNormalizedPosition_m2624_ParameterInfos[] = 
{
	{"value", 0, 134218211, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_set_horizontalNormalizedPosition_m2624_MethodInfo = 
{
	"set_horizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_horizontalNormalizedPosition_m2624/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_horizontalNormalizedPosition_m2624_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_verticalNormalizedPosition()
extern const MethodInfo ScrollRect_get_verticalNormalizedPosition_m2625_MethodInfo = 
{
	"get_verticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_verticalNormalizedPosition_m2625/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_set_verticalNormalizedPosition_m2626_ParameterInfos[] = 
{
	{"value", 0, 134218212, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_verticalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_set_verticalNormalizedPosition_m2626_MethodInfo = 
{
	"set_verticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_verticalNormalizedPosition_m2626/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, ScrollRect_t575_ScrollRect_set_verticalNormalizedPosition_m2626_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_SetHorizontalNormalizedPosition_m2627_ParameterInfos[] = 
{
	{"value", 0, 134218213, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetHorizontalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_SetHorizontalNormalizedPosition_m2627_MethodInfo = 
{
	"SetHorizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetHorizontalNormalizedPosition_m2627/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, ScrollRect_t575_ScrollRect_SetHorizontalNormalizedPosition_m2627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_SetVerticalNormalizedPosition_m2628_ParameterInfos[] = 
{
	{"value", 0, 134218214, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetVerticalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_SetVerticalNormalizedPosition_m2628_MethodInfo = 
{
	"SetVerticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetVerticalNormalizedPosition_m2628/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, ScrollRect_t575_ScrollRect_SetVerticalNormalizedPosition_m2628_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_SetNormalizedPosition_m2629_ParameterInfos[] = 
{
	{"value", 0, 134218215, 0, &Single_t254_0_0_0},
	{"axis", 1, 134218216, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetNormalizedPosition(System.Single,System.Int32)
extern const MethodInfo ScrollRect_SetNormalizedPosition_m2629_MethodInfo = 
{
	"SetNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetNormalizedPosition_m2629/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_Int32_t253/* invoker_method */
	, ScrollRect_t575_ScrollRect_SetNormalizedPosition_m2629_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_RubberDelta_m2630_ParameterInfos[] = 
{
	{"overStretching", 0, 134218217, 0, &Single_t254_0_0_0},
	{"viewSize", 1, 134218218, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::RubberDelta(System.Single,System.Single)
extern const MethodInfo ScrollRect_RubberDelta_m2630_MethodInfo = 
{
	"RubberDelta"/* name */
	, (methodPointerType)&ScrollRect_RubberDelta_m2630/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Single_t254_Single_t254/* invoker_method */
	, ScrollRect_t575_ScrollRect_RubberDelta_m2630_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdateBounds()
extern const MethodInfo ScrollRect_UpdateBounds_m2631_MethodInfo = 
{
	"UpdateBounds"/* name */
	, (methodPointerType)&ScrollRect_UpdateBounds_m2631/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Bounds_t225_0_0_0;
extern void* RuntimeInvoker_Bounds_t225 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Bounds UnityEngine.UI.ScrollRect::GetBounds()
extern const MethodInfo ScrollRect_GetBounds_m2632_MethodInfo = 
{
	"GetBounds"/* name */
	, (methodPointerType)&ScrollRect_GetBounds_m2632/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Bounds_t225_0_0_0/* return_type */
	, RuntimeInvoker_Bounds_t225/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo ScrollRect_t575_ScrollRect_CalculateOffset_m2633_ParameterInfos[] = 
{
	{"delta", 0, 134218219, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::CalculateOffset(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_CalculateOffset_m2633_MethodInfo = 
{
	"CalculateOffset"/* name */
	, (methodPointerType)&ScrollRect_CalculateOffset_m2633/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6_Vector2_t6/* invoker_method */
	, ScrollRect_t575_ScrollRect_CalculateOffset_m2633_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m2634_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m2634/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t1_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.ScrollRect::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m2635_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m2635/* method */
	, &ScrollRect_t575_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t1_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ScrollRect_t575_MethodInfos[] =
{
	&ScrollRect__ctor_m2580_MethodInfo,
	&ScrollRect_get_content_m2581_MethodInfo,
	&ScrollRect_set_content_m2582_MethodInfo,
	&ScrollRect_get_horizontal_m2583_MethodInfo,
	&ScrollRect_set_horizontal_m2584_MethodInfo,
	&ScrollRect_get_vertical_m2585_MethodInfo,
	&ScrollRect_set_vertical_m2586_MethodInfo,
	&ScrollRect_get_movementType_m2587_MethodInfo,
	&ScrollRect_set_movementType_m2588_MethodInfo,
	&ScrollRect_get_elasticity_m2589_MethodInfo,
	&ScrollRect_set_elasticity_m2590_MethodInfo,
	&ScrollRect_get_inertia_m2591_MethodInfo,
	&ScrollRect_set_inertia_m2592_MethodInfo,
	&ScrollRect_get_decelerationRate_m2593_MethodInfo,
	&ScrollRect_set_decelerationRate_m2594_MethodInfo,
	&ScrollRect_get_scrollSensitivity_m2595_MethodInfo,
	&ScrollRect_set_scrollSensitivity_m2596_MethodInfo,
	&ScrollRect_get_horizontalScrollbar_m2597_MethodInfo,
	&ScrollRect_set_horizontalScrollbar_m2598_MethodInfo,
	&ScrollRect_get_verticalScrollbar_m2599_MethodInfo,
	&ScrollRect_set_verticalScrollbar_m2600_MethodInfo,
	&ScrollRect_get_onValueChanged_m2601_MethodInfo,
	&ScrollRect_set_onValueChanged_m2602_MethodInfo,
	&ScrollRect_get_viewRect_m2603_MethodInfo,
	&ScrollRect_get_velocity_m2604_MethodInfo,
	&ScrollRect_set_velocity_m2605_MethodInfo,
	&ScrollRect_Rebuild_m2606_MethodInfo,
	&ScrollRect_OnEnable_m2607_MethodInfo,
	&ScrollRect_OnDisable_m2608_MethodInfo,
	&ScrollRect_IsActive_m2609_MethodInfo,
	&ScrollRect_EnsureLayoutHasRebuilt_m2610_MethodInfo,
	&ScrollRect_StopMovement_m2611_MethodInfo,
	&ScrollRect_OnScroll_m2612_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m2613_MethodInfo,
	&ScrollRect_OnBeginDrag_m2614_MethodInfo,
	&ScrollRect_OnEndDrag_m2615_MethodInfo,
	&ScrollRect_OnDrag_m2616_MethodInfo,
	&ScrollRect_SetContentAnchoredPosition_m2617_MethodInfo,
	&ScrollRect_LateUpdate_m2618_MethodInfo,
	&ScrollRect_UpdatePrevData_m2619_MethodInfo,
	&ScrollRect_UpdateScrollbars_m2620_MethodInfo,
	&ScrollRect_get_normalizedPosition_m2621_MethodInfo,
	&ScrollRect_set_normalizedPosition_m2622_MethodInfo,
	&ScrollRect_get_horizontalNormalizedPosition_m2623_MethodInfo,
	&ScrollRect_set_horizontalNormalizedPosition_m2624_MethodInfo,
	&ScrollRect_get_verticalNormalizedPosition_m2625_MethodInfo,
	&ScrollRect_set_verticalNormalizedPosition_m2626_MethodInfo,
	&ScrollRect_SetHorizontalNormalizedPosition_m2627_MethodInfo,
	&ScrollRect_SetVerticalNormalizedPosition_m2628_MethodInfo,
	&ScrollRect_SetNormalizedPosition_m2629_MethodInfo,
	&ScrollRect_RubberDelta_m2630_MethodInfo,
	&ScrollRect_UpdateBounds_m2631_MethodInfo,
	&ScrollRect_GetBounds_m2632_MethodInfo,
	&ScrollRect_CalculateOffset_m2633_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m2634_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m2635_MethodInfo,
	NULL
};
extern const MethodInfo ScrollRect_get_content_m2581_MethodInfo;
extern const MethodInfo ScrollRect_set_content_m2582_MethodInfo;
static const PropertyInfo ScrollRect_t575____content_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "content"/* name */
	, &ScrollRect_get_content_m2581_MethodInfo/* get */
	, &ScrollRect_set_content_m2582_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontal_m2583_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontal_m2584_MethodInfo;
static const PropertyInfo ScrollRect_t575____horizontal_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "horizontal"/* name */
	, &ScrollRect_get_horizontal_m2583_MethodInfo/* get */
	, &ScrollRect_set_horizontal_m2584_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_vertical_m2585_MethodInfo;
extern const MethodInfo ScrollRect_set_vertical_m2586_MethodInfo;
static const PropertyInfo ScrollRect_t575____vertical_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "vertical"/* name */
	, &ScrollRect_get_vertical_m2585_MethodInfo/* get */
	, &ScrollRect_set_vertical_m2586_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_movementType_m2587_MethodInfo;
extern const MethodInfo ScrollRect_set_movementType_m2588_MethodInfo;
static const PropertyInfo ScrollRect_t575____movementType_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "movementType"/* name */
	, &ScrollRect_get_movementType_m2587_MethodInfo/* get */
	, &ScrollRect_set_movementType_m2588_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_elasticity_m2589_MethodInfo;
extern const MethodInfo ScrollRect_set_elasticity_m2590_MethodInfo;
static const PropertyInfo ScrollRect_t575____elasticity_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "elasticity"/* name */
	, &ScrollRect_get_elasticity_m2589_MethodInfo/* get */
	, &ScrollRect_set_elasticity_m2590_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_inertia_m2591_MethodInfo;
extern const MethodInfo ScrollRect_set_inertia_m2592_MethodInfo;
static const PropertyInfo ScrollRect_t575____inertia_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "inertia"/* name */
	, &ScrollRect_get_inertia_m2591_MethodInfo/* get */
	, &ScrollRect_set_inertia_m2592_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_decelerationRate_m2593_MethodInfo;
extern const MethodInfo ScrollRect_set_decelerationRate_m2594_MethodInfo;
static const PropertyInfo ScrollRect_t575____decelerationRate_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "decelerationRate"/* name */
	, &ScrollRect_get_decelerationRate_m2593_MethodInfo/* get */
	, &ScrollRect_set_decelerationRate_m2594_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_scrollSensitivity_m2595_MethodInfo;
extern const MethodInfo ScrollRect_set_scrollSensitivity_m2596_MethodInfo;
static const PropertyInfo ScrollRect_t575____scrollSensitivity_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "scrollSensitivity"/* name */
	, &ScrollRect_get_scrollSensitivity_m2595_MethodInfo/* get */
	, &ScrollRect_set_scrollSensitivity_m2596_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontalScrollbar_m2597_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontalScrollbar_m2598_MethodInfo;
static const PropertyInfo ScrollRect_t575____horizontalScrollbar_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "horizontalScrollbar"/* name */
	, &ScrollRect_get_horizontalScrollbar_m2597_MethodInfo/* get */
	, &ScrollRect_set_horizontalScrollbar_m2598_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_verticalScrollbar_m2599_MethodInfo;
extern const MethodInfo ScrollRect_set_verticalScrollbar_m2600_MethodInfo;
static const PropertyInfo ScrollRect_t575____verticalScrollbar_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "verticalScrollbar"/* name */
	, &ScrollRect_get_verticalScrollbar_m2599_MethodInfo/* get */
	, &ScrollRect_set_verticalScrollbar_m2600_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_onValueChanged_m2601_MethodInfo;
extern const MethodInfo ScrollRect_set_onValueChanged_m2602_MethodInfo;
static const PropertyInfo ScrollRect_t575____onValueChanged_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "onValueChanged"/* name */
	, &ScrollRect_get_onValueChanged_m2601_MethodInfo/* get */
	, &ScrollRect_set_onValueChanged_m2602_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_viewRect_m2603_MethodInfo;
static const PropertyInfo ScrollRect_t575____viewRect_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "viewRect"/* name */
	, &ScrollRect_get_viewRect_m2603_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_velocity_m2604_MethodInfo;
extern const MethodInfo ScrollRect_set_velocity_m2605_MethodInfo;
static const PropertyInfo ScrollRect_t575____velocity_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "velocity"/* name */
	, &ScrollRect_get_velocity_m2604_MethodInfo/* get */
	, &ScrollRect_set_velocity_m2605_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_normalizedPosition_m2621_MethodInfo;
extern const MethodInfo ScrollRect_set_normalizedPosition_m2622_MethodInfo;
static const PropertyInfo ScrollRect_t575____normalizedPosition_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "normalizedPosition"/* name */
	, &ScrollRect_get_normalizedPosition_m2621_MethodInfo/* get */
	, &ScrollRect_set_normalizedPosition_m2622_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontalNormalizedPosition_m2623_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontalNormalizedPosition_m2624_MethodInfo;
static const PropertyInfo ScrollRect_t575____horizontalNormalizedPosition_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "horizontalNormalizedPosition"/* name */
	, &ScrollRect_get_horizontalNormalizedPosition_m2623_MethodInfo/* get */
	, &ScrollRect_set_horizontalNormalizedPosition_m2624_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_verticalNormalizedPosition_m2625_MethodInfo;
extern const MethodInfo ScrollRect_set_verticalNormalizedPosition_m2626_MethodInfo;
static const PropertyInfo ScrollRect_t575____verticalNormalizedPosition_PropertyInfo = 
{
	&ScrollRect_t575_il2cpp_TypeInfo/* parent */
	, "verticalNormalizedPosition"/* name */
	, &ScrollRect_get_verticalNormalizedPosition_m2625_MethodInfo/* get */
	, &ScrollRect_set_verticalNormalizedPosition_m2626_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ScrollRect_t575_PropertyInfos[] =
{
	&ScrollRect_t575____content_PropertyInfo,
	&ScrollRect_t575____horizontal_PropertyInfo,
	&ScrollRect_t575____vertical_PropertyInfo,
	&ScrollRect_t575____movementType_PropertyInfo,
	&ScrollRect_t575____elasticity_PropertyInfo,
	&ScrollRect_t575____inertia_PropertyInfo,
	&ScrollRect_t575____decelerationRate_PropertyInfo,
	&ScrollRect_t575____scrollSensitivity_PropertyInfo,
	&ScrollRect_t575____horizontalScrollbar_PropertyInfo,
	&ScrollRect_t575____verticalScrollbar_PropertyInfo,
	&ScrollRect_t575____onValueChanged_PropertyInfo,
	&ScrollRect_t575____viewRect_PropertyInfo,
	&ScrollRect_t575____velocity_PropertyInfo,
	&ScrollRect_t575____normalizedPosition_PropertyInfo,
	&ScrollRect_t575____horizontalNormalizedPosition_PropertyInfo,
	&ScrollRect_t575____verticalNormalizedPosition_PropertyInfo,
	NULL
};
static const Il2CppType* ScrollRect_t575_il2cpp_TypeInfo__nestedTypes[2] =
{
	&MovementType_t572_0_0_0,
	&ScrollRectEvent_t573_0_0_0,
};
extern const MethodInfo Object_Equals_m1047_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1049_MethodInfo;
extern const MethodInfo Object_ToString_m1050_MethodInfo;
extern const MethodInfo UIBehaviour_Awake_m1975_MethodInfo;
extern const MethodInfo ScrollRect_OnEnable_m2607_MethodInfo;
extern const MethodInfo UIBehaviour_Start_m1977_MethodInfo;
extern const MethodInfo ScrollRect_OnDisable_m2608_MethodInfo;
extern const MethodInfo UIBehaviour_OnDestroy_m1979_MethodInfo;
extern const MethodInfo ScrollRect_IsActive_m2609_MethodInfo;
extern const MethodInfo UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo;
extern const MethodInfo UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo;
extern const MethodInfo UIBehaviour_OnTransformParentChanged_m1983_MethodInfo;
extern const MethodInfo UIBehaviour_OnDidApplyAnimationProperties_m1984_MethodInfo;
extern const MethodInfo UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo;
extern const MethodInfo UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo;
extern const MethodInfo ScrollRect_OnBeginDrag_m2614_MethodInfo;
extern const MethodInfo ScrollRect_OnInitializePotentialDrag_m2613_MethodInfo;
extern const MethodInfo ScrollRect_OnDrag_m2616_MethodInfo;
extern const MethodInfo ScrollRect_OnEndDrag_m2615_MethodInfo;
extern const MethodInfo ScrollRect_OnScroll_m2612_MethodInfo;
extern const MethodInfo ScrollRect_Rebuild_m2606_MethodInfo;
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m2635_MethodInfo;
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m2634_MethodInfo;
extern const MethodInfo ScrollRect_StopMovement_m2611_MethodInfo;
extern const MethodInfo ScrollRect_SetContentAnchoredPosition_m2617_MethodInfo;
extern const MethodInfo ScrollRect_LateUpdate_m2618_MethodInfo;
static const Il2CppMethodReference ScrollRect_t575_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&ScrollRect_OnEnable_m2607_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&ScrollRect_OnDisable_m2608_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&ScrollRect_IsActive_m2609_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m1984_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&ScrollRect_OnBeginDrag_m2614_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m2613_MethodInfo,
	&ScrollRect_OnDrag_m2616_MethodInfo,
	&ScrollRect_OnEndDrag_m2615_MethodInfo,
	&ScrollRect_OnScroll_m2612_MethodInfo,
	&ScrollRect_Rebuild_m2606_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m2635_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m2634_MethodInfo,
	&ScrollRect_Rebuild_m2606_MethodInfo,
	&ScrollRect_StopMovement_m2611_MethodInfo,
	&ScrollRect_OnScroll_m2612_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m2613_MethodInfo,
	&ScrollRect_OnBeginDrag_m2614_MethodInfo,
	&ScrollRect_OnEndDrag_m2615_MethodInfo,
	&ScrollRect_OnDrag_m2616_MethodInfo,
	&ScrollRect_SetContentAnchoredPosition_m2617_MethodInfo,
	&ScrollRect_LateUpdate_m2618_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m2634_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m2635_MethodInfo,
};
static bool ScrollRect_t575_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEventSystemHandler_t281_0_0_0;
extern const Il2CppType IBeginDragHandler_t633_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t632_0_0_0;
extern const Il2CppType IDragHandler_t283_0_0_0;
extern const Il2CppType IEndDragHandler_t634_0_0_0;
extern const Il2CppType IScrollHandler_t636_0_0_0;
extern const Il2CppType ICanvasElement_t646_0_0_0;
static const Il2CppType* ScrollRect_t575_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t281_0_0_0,
	&IBeginDragHandler_t633_0_0_0,
	&IInitializePotentialDragHandler_t632_0_0_0,
	&IDragHandler_t283_0_0_0,
	&IEndDragHandler_t634_0_0_0,
	&IScrollHandler_t636_0_0_0,
	&ICanvasElement_t646_0_0_0,
};
static Il2CppInterfaceOffsetPair ScrollRect_t575_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t281_0_0_0, 16},
	{ &IBeginDragHandler_t633_0_0_0, 16},
	{ &IInitializePotentialDragHandler_t632_0_0_0, 17},
	{ &IDragHandler_t283_0_0_0, 18},
	{ &IEndDragHandler_t634_0_0_0, 19},
	{ &IScrollHandler_t636_0_0_0, 20},
	{ &ICanvasElement_t646_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScrollRect_t575_1_0_0;
extern const Il2CppType UIBehaviour_t455_0_0_0;
struct ScrollRect_t575;
const Il2CppTypeDefinitionMetadata ScrollRect_t575_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ScrollRect_t575_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ScrollRect_t575_InterfacesTypeInfos/* implementedInterfaces */
	, ScrollRect_t575_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t455_0_0_0/* parent */
	, ScrollRect_t575_VTable/* vtableMethods */
	, ScrollRect_t575_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 418/* fieldStart */

};
TypeInfo ScrollRect_t575_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ScrollRect_t575_MethodInfos/* methods */
	, ScrollRect_t575_PropertyInfos/* properties */
	, NULL/* events */
	, &ScrollRect_t575_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 213/* custom_attributes_cache */
	, &ScrollRect_t575_0_0_0/* byval_arg */
	, &ScrollRect_t575_1_0_0/* this_arg */
	, &ScrollRect_t575_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRect_t575)/* instance_size */
	, sizeof (ScrollRect_t575)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 56/* method_count */
	, 16/* property_count */
	, 23/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 35/* vtable_count */
	, 7/* interfaces_count */
	, 7/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
// Metadata Definition UnityEngine.UI.Selectable/Transition
extern TypeInfo Transition_t576_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_TransitionMethodDeclarations.h"
static const MethodInfo* Transition_t576_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m1051_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m1052_MethodInfo;
extern const MethodInfo Enum_ToString_m1053_MethodInfo;
extern const MethodInfo Enum_ToString_m1054_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m1055_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m1056_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m1057_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m1058_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m1059_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m1060_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m1061_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m1062_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m1063_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m1064_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m1065_MethodInfo;
extern const MethodInfo Enum_ToString_m1066_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m1067_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m1068_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m1069_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m1070_MethodInfo;
extern const MethodInfo Enum_CompareTo_m1071_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m1072_MethodInfo;
static const Il2CppMethodReference Transition_t576_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Transition_t576_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t275_0_0_0;
extern const Il2CppType IConvertible_t276_0_0_0;
extern const Il2CppType IComparable_t277_0_0_0;
static Il2CppInterfaceOffsetPair Transition_t576_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Transition_t576_0_0_0;
extern const Il2CppType Transition_t576_1_0_0;
extern const Il2CppType Enum_t278_0_0_0;
extern TypeInfo Selectable_t510_il2cpp_TypeInfo;
extern const Il2CppType Selectable_t510_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t253_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Transition_t576_DefinitionMetadata = 
{
	&Selectable_t510_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Transition_t576_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Transition_t576_VTable/* vtableMethods */
	, Transition_t576_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 441/* fieldStart */

};
TypeInfo Transition_t576_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Transition"/* name */
	, ""/* namespaze */
	, Transition_t576_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Transition_t576_0_0_0/* byval_arg */
	, &Transition_t576_1_0_0/* this_arg */
	, &Transition_t576_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Transition_t576)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Transition_t576)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
// Metadata Definition UnityEngine.UI.Selectable/SelectionState
extern TypeInfo SelectionState_t577_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionStateMethodDeclarations.h"
static const MethodInfo* SelectionState_t577_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference SelectionState_t577_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool SelectionState_t577_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SelectionState_t577_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SelectionState_t577_0_0_0;
extern const Il2CppType SelectionState_t577_1_0_0;
const Il2CppTypeDefinitionMetadata SelectionState_t577_DefinitionMetadata = 
{
	&Selectable_t510_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionState_t577_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, SelectionState_t577_VTable/* vtableMethods */
	, SelectionState_t577_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 446/* fieldStart */

};
TypeInfo SelectionState_t577_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionState"/* name */
	, ""/* namespaze */
	, SelectionState_t577_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SelectionState_t577_0_0_0/* byval_arg */
	, &SelectionState_t577_1_0_0/* this_arg */
	, &SelectionState_t577_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionState_t577)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SelectionState_t577)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 260/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
// Metadata Definition UnityEngine.UI.Selectable
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_SelectableMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::.ctor()
extern const MethodInfo Selectable__ctor_m2636_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Selectable__ctor_m2636/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::.cctor()
extern const MethodInfo Selectable__cctor_m2637_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Selectable__cctor_m2637/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t578_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::get_allSelectables()
extern const MethodInfo Selectable_get_allSelectables_m2638_MethodInfo = 
{
	"get_allSelectables"/* name */
	, (methodPointerType)&Selectable_get_allSelectables_m2638/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t578_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Navigation_t563_0_0_0;
extern void* RuntimeInvoker_Navigation_t563 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::get_navigation()
extern const MethodInfo Selectable_get_navigation_m2639_MethodInfo = 
{
	"get_navigation"/* name */
	, (methodPointerType)&Selectable_get_navigation_m2639/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Navigation_t563_0_0_0/* return_type */
	, RuntimeInvoker_Navigation_t563/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Navigation_t563_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_set_navigation_m2640_ParameterInfos[] = 
{
	{"value", 0, 134218220, 0, &Navigation_t563_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Navigation_t563 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_navigation(UnityEngine.UI.Navigation)
extern const MethodInfo Selectable_set_navigation_m2640_MethodInfo = 
{
	"set_navigation"/* name */
	, (methodPointerType)&Selectable_set_navigation_m2640/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Navigation_t563/* invoker_method */
	, Selectable_t510_Selectable_set_navigation_m2640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Transition_t576 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::get_transition()
extern const MethodInfo Selectable_get_transition_m2641_MethodInfo = 
{
	"get_transition"/* name */
	, (methodPointerType)&Selectable_get_transition_m2641/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Transition_t576_0_0_0/* return_type */
	, RuntimeInvoker_Transition_t576/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transition_t576_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_set_transition_m2642_ParameterInfos[] = 
{
	{"value", 0, 134218221, 0, &Transition_t576_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_transition(UnityEngine.UI.Selectable/Transition)
extern const MethodInfo Selectable_set_transition_m2642_MethodInfo = 
{
	"set_transition"/* name */
	, (methodPointerType)&Selectable_set_transition_m2642/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Selectable_t510_Selectable_set_transition_m2642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorBlock_t516_0_0_0;
extern void* RuntimeInvoker_ColorBlock_t516 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::get_colors()
extern const MethodInfo Selectable_get_colors_m2643_MethodInfo = 
{
	"get_colors"/* name */
	, (methodPointerType)&Selectable_get_colors_m2643/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &ColorBlock_t516_0_0_0/* return_type */
	, RuntimeInvoker_ColorBlock_t516/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorBlock_t516_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_set_colors_m2644_ParameterInfos[] = 
{
	{"value", 0, 134218222, 0, &ColorBlock_t516_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_ColorBlock_t516 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_colors(UnityEngine.UI.ColorBlock)
extern const MethodInfo Selectable_set_colors_m2644_MethodInfo = 
{
	"set_colors"/* name */
	, (methodPointerType)&Selectable_set_colors_m2644/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_ColorBlock_t516/* invoker_method */
	, Selectable_t510_Selectable_set_colors_m2644_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SpriteState_t580_0_0_0;
extern void* RuntimeInvoker_SpriteState_t580 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::get_spriteState()
extern const MethodInfo Selectable_get_spriteState_m2645_MethodInfo = 
{
	"get_spriteState"/* name */
	, (methodPointerType)&Selectable_get_spriteState_m2645/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &SpriteState_t580_0_0_0/* return_type */
	, RuntimeInvoker_SpriteState_t580/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SpriteState_t580_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_set_spriteState_m2646_ParameterInfos[] = 
{
	{"value", 0, 134218223, 0, &SpriteState_t580_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SpriteState_t580 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_spriteState(UnityEngine.UI.SpriteState)
extern const MethodInfo Selectable_set_spriteState_m2646_MethodInfo = 
{
	"set_spriteState"/* name */
	, (methodPointerType)&Selectable_set_spriteState_m2646/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SpriteState_t580/* invoker_method */
	, Selectable_t510_Selectable_set_spriteState_m2646_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AnimationTriggers_t507_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::get_animationTriggers()
extern const MethodInfo Selectable_get_animationTriggers_m2647_MethodInfo = 
{
	"get_animationTriggers"/* name */
	, (methodPointerType)&Selectable_get_animationTriggers_m2647/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &AnimationTriggers_t507_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AnimationTriggers_t507_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_set_animationTriggers_m2648_ParameterInfos[] = 
{
	{"value", 0, 134218224, 0, &AnimationTriggers_t507_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_animationTriggers(UnityEngine.UI.AnimationTriggers)
extern const MethodInfo Selectable_set_animationTriggers_m2648_MethodInfo = 
{
	"set_animationTriggers"/* name */
	, (methodPointerType)&Selectable_set_animationTriggers_m2648/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_set_animationTriggers_m2648_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Graphic_t418_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::get_targetGraphic()
extern const MethodInfo Selectable_get_targetGraphic_m1734_MethodInfo = 
{
	"get_targetGraphic"/* name */
	, (methodPointerType)&Selectable_get_targetGraphic_m1734/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t418_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Graphic_t418_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_set_targetGraphic_m1736_ParameterInfos[] = 
{
	{"value", 0, 134218225, 0, &Graphic_t418_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_targetGraphic(UnityEngine.UI.Graphic)
extern const MethodInfo Selectable_set_targetGraphic_m1736_MethodInfo = 
{
	"set_targetGraphic"/* name */
	, (methodPointerType)&Selectable_set_targetGraphic_m1736/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_set_targetGraphic_m1736_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_interactable()
extern const MethodInfo Selectable_get_interactable_m2649_MethodInfo = 
{
	"get_interactable"/* name */
	, (methodPointerType)&Selectable_get_interactable_m2649/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_set_interactable_m2650_ParameterInfos[] = 
{
	{"value", 0, 134218226, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
extern const MethodInfo Selectable_set_interactable_m2650_MethodInfo = 
{
	"set_interactable"/* name */
	, (methodPointerType)&Selectable_set_interactable_m2650/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Selectable_t510_Selectable_set_interactable_m2650_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_isPointerInside()
extern const MethodInfo Selectable_get_isPointerInside_m2651_MethodInfo = 
{
	"get_isPointerInside"/* name */
	, (methodPointerType)&Selectable_get_isPointerInside_m2651/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 236/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_set_isPointerInside_m2652_ParameterInfos[] = 
{
	{"value", 0, 134218227, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_isPointerInside(System.Boolean)
extern const MethodInfo Selectable_set_isPointerInside_m2652_MethodInfo = 
{
	"set_isPointerInside"/* name */
	, (methodPointerType)&Selectable_set_isPointerInside_m2652/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Selectable_t510_Selectable_set_isPointerInside_m2652_ParameterInfos/* parameters */
	, 237/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_isPointerDown()
extern const MethodInfo Selectable_get_isPointerDown_m2653_MethodInfo = 
{
	"get_isPointerDown"/* name */
	, (methodPointerType)&Selectable_get_isPointerDown_m2653/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 238/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_set_isPointerDown_m2654_ParameterInfos[] = 
{
	{"value", 0, 134218228, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_isPointerDown(System.Boolean)
extern const MethodInfo Selectable_set_isPointerDown_m2654_MethodInfo = 
{
	"set_isPointerDown"/* name */
	, (methodPointerType)&Selectable_set_isPointerDown_m2654/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Selectable_t510_Selectable_set_isPointerDown_m2654_ParameterInfos/* parameters */
	, 239/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_hasSelection()
extern const MethodInfo Selectable_get_hasSelection_m2655_MethodInfo = 
{
	"get_hasSelection"/* name */
	, (methodPointerType)&Selectable_get_hasSelection_m2655/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 240/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_set_hasSelection_m2656_ParameterInfos[] = 
{
	{"value", 0, 134218229, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_hasSelection(System.Boolean)
extern const MethodInfo Selectable_set_hasSelection_m2656_MethodInfo = 
{
	"set_hasSelection"/* name */
	, (methodPointerType)&Selectable_set_hasSelection_m2656/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Selectable_t510_Selectable_set_hasSelection_m2656_ParameterInfos/* parameters */
	, 241/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Image_t48_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Image UnityEngine.UI.Selectable::get_image()
extern const MethodInfo Selectable_get_image_m2657_MethodInfo = 
{
	"get_image"/* name */
	, (methodPointerType)&Selectable_get_image_m2657/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Image_t48_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Image_t48_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_set_image_m2658_ParameterInfos[] = 
{
	{"value", 0, 134218230, 0, &Image_t48_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_image(UnityEngine.UI.Image)
extern const MethodInfo Selectable_set_image_m2658_MethodInfo = 
{
	"set_image"/* name */
	, (methodPointerType)&Selectable_set_image_m2658/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_set_image_m2658_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t9_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Animator UnityEngine.UI.Selectable::get_animator()
extern const MethodInfo Selectable_get_animator_m2659_MethodInfo = 
{
	"get_animator"/* name */
	, (methodPointerType)&Selectable_get_animator_m2659/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Animator_t9_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Awake()
extern const MethodInfo Selectable_Awake_m2660_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&Selectable_Awake_m2660/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnCanvasGroupChanged()
extern const MethodInfo Selectable_OnCanvasGroupChanged_m2661_MethodInfo = 
{
	"OnCanvasGroupChanged"/* name */
	, (methodPointerType)&Selectable_OnCanvasGroupChanged_m2661/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsInteractable()
extern const MethodInfo Selectable_IsInteractable_m2662_MethodInfo = 
{
	"IsInteractable"/* name */
	, (methodPointerType)&Selectable_IsInteractable_m2662/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDidApplyAnimationProperties()
extern const MethodInfo Selectable_OnDidApplyAnimationProperties_m2663_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&Selectable_OnDidApplyAnimationProperties_m2663/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnEnable()
extern const MethodInfo Selectable_OnEnable_m2664_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Selectable_OnEnable_m2664/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnSetProperty()
extern const MethodInfo Selectable_OnSetProperty_m2665_MethodInfo = 
{
	"OnSetProperty"/* name */
	, (methodPointerType)&Selectable_OnSetProperty_m2665/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDisable()
extern const MethodInfo Selectable_OnDisable_m2666_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Selectable_OnDisable_m2666/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_SelectionState_t577 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::get_currentSelectionState()
extern const MethodInfo Selectable_get_currentSelectionState_m2667_MethodInfo = 
{
	"get_currentSelectionState"/* name */
	, (methodPointerType)&Selectable_get_currentSelectionState_m2667/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &SelectionState_t577_0_0_0/* return_type */
	, RuntimeInvoker_SelectionState_t577/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::InstantClearState()
extern const MethodInfo Selectable_InstantClearState_m2668_MethodInfo = 
{
	"InstantClearState"/* name */
	, (methodPointerType)&Selectable_InstantClearState_m2668/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SelectionState_t577_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_DoStateTransition_m2669_ParameterInfos[] = 
{
	{"state", 0, 134218231, 0, &SelectionState_t577_0_0_0},
	{"instant", 1, 134218232, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::DoStateTransition(UnityEngine.UI.Selectable/SelectionState,System.Boolean)
extern const MethodInfo Selectable_DoStateTransition_m2669_MethodInfo = 
{
	"DoStateTransition"/* name */
	, (methodPointerType)&Selectable_DoStateTransition_m2669/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_SByte_t274/* invoker_method */
	, Selectable_t510_Selectable_DoStateTransition_m2669_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t4_0_0_0;
extern const Il2CppType Vector3_t4_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_FindSelectable_m2670_ParameterInfos[] = 
{
	{"dir", 0, 134218233, 0, &Vector3_t4_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectable(UnityEngine.Vector3)
extern const MethodInfo Selectable_FindSelectable_m2670_MethodInfo = 
{
	"FindSelectable"/* name */
	, (methodPointerType)&Selectable_FindSelectable_m2670/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t510_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Vector3_t4/* invoker_method */
	, Selectable_t510_Selectable_FindSelectable_m2670_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_GetPointOnRectEdge_m2671_ParameterInfos[] = 
{
	{"rect", 0, 134218234, 0, &RectTransform_t364_0_0_0},
	{"dir", 1, 134218235, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t4_Object_t_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityEngine.UI.Selectable::GetPointOnRectEdge(UnityEngine.RectTransform,UnityEngine.Vector2)
extern const MethodInfo Selectable_GetPointOnRectEdge_m2671_MethodInfo = 
{
	"GetPointOnRectEdge"/* name */
	, (methodPointerType)&Selectable_GetPointOnRectEdge_m2671/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t4_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t4_Object_t_Vector2_t6/* invoker_method */
	, Selectable_t510_Selectable_GetPointOnRectEdge_m2671_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t487_0_0_0;
extern const Il2CppType AxisEventData_t487_0_0_0;
extern const Il2CppType Selectable_t510_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_Navigate_m2672_ParameterInfos[] = 
{
	{"eventData", 0, 134218236, 0, &AxisEventData_t487_0_0_0},
	{"sel", 1, 134218237, 0, &Selectable_t510_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Navigate(UnityEngine.EventSystems.AxisEventData,UnityEngine.UI.Selectable)
extern const MethodInfo Selectable_Navigate_m2672_MethodInfo = 
{
	"Navigate"/* name */
	, (methodPointerType)&Selectable_Navigate_m2672/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Selectable_t510_Selectable_Navigate_m2672_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnLeft()
extern const MethodInfo Selectable_FindSelectableOnLeft_m2673_MethodInfo = 
{
	"FindSelectableOnLeft"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnLeft_m2673/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t510_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnRight()
extern const MethodInfo Selectable_FindSelectableOnRight_m2674_MethodInfo = 
{
	"FindSelectableOnRight"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnRight_m2674/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t510_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnUp()
extern const MethodInfo Selectable_FindSelectableOnUp_m2675_MethodInfo = 
{
	"FindSelectableOnUp"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnUp_m2675/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t510_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnDown()
extern const MethodInfo Selectable_FindSelectableOnDown_m2676_MethodInfo = 
{
	"FindSelectableOnDown"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnDown_m2676/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t510_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t487_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_OnMove_m2677_ParameterInfos[] = 
{
	{"eventData", 0, 134218238, 0, &AxisEventData_t487_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnMove(UnityEngine.EventSystems.AxisEventData)
extern const MethodInfo Selectable_OnMove_m2677_MethodInfo = 
{
	"OnMove"/* name */
	, (methodPointerType)&Selectable_OnMove_m2677/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_OnMove_m2677_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color_t65_0_0_0;
extern const Il2CppType Color_t65_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_StartColorTween_m2678_ParameterInfos[] = 
{
	{"targetColor", 0, 134218239, 0, &Color_t65_0_0_0},
	{"instant", 1, 134218240, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Color_t65_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::StartColorTween(UnityEngine.Color,System.Boolean)
extern const MethodInfo Selectable_StartColorTween_m2678_MethodInfo = 
{
	"StartColorTween"/* name */
	, (methodPointerType)&Selectable_StartColorTween_m2678/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Color_t65_SByte_t274/* invoker_method */
	, Selectable_t510_Selectable_StartColorTween_m2678_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t329_0_0_0;
extern const Il2CppType Sprite_t329_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_DoSpriteSwap_m2679_ParameterInfos[] = 
{
	{"newSprite", 0, 134218241, 0, &Sprite_t329_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::DoSpriteSwap(UnityEngine.Sprite)
extern const MethodInfo Selectable_DoSpriteSwap_m2679_MethodInfo = 
{
	"DoSpriteSwap"/* name */
	, (methodPointerType)&Selectable_DoSpriteSwap_m2679/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_DoSpriteSwap_m2679_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_TriggerAnimation_m2680_ParameterInfos[] = 
{
	{"triggername", 0, 134218242, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::TriggerAnimation(System.String)
extern const MethodInfo Selectable_TriggerAnimation_m2680_MethodInfo = 
{
	"TriggerAnimation"/* name */
	, (methodPointerType)&Selectable_TriggerAnimation_m2680/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_TriggerAnimation_m2680_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t453_0_0_0;
extern const Il2CppType BaseEventData_t453_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_IsHighlighted_m2681_ParameterInfos[] = 
{
	{"eventData", 0, 134218243, 0, &BaseEventData_t453_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsHighlighted(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_IsHighlighted_m2681_MethodInfo = 
{
	"IsHighlighted"/* name */
	, (methodPointerType)&Selectable_IsHighlighted_m2681/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Selectable_t510_Selectable_IsHighlighted_m2681_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t453_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_IsPressed_m2682_ParameterInfos[] = 
{
	{"eventData", 0, 134218244, 0, &BaseEventData_t453_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsPressed(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_IsPressed_m2682_MethodInfo = 
{
	"IsPressed"/* name */
	, (methodPointerType)&Selectable_IsPressed_m2682/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Selectable_t510_Selectable_IsPressed_m2682_ParameterInfos/* parameters */
	, 242/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsPressed()
extern const MethodInfo Selectable_IsPressed_m2683_MethodInfo = 
{
	"IsPressed"/* name */
	, (methodPointerType)&Selectable_IsPressed_m2683/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t453_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_UpdateSelectionState_m2684_ParameterInfos[] = 
{
	{"eventData", 0, 134218245, 0, &BaseEventData_t453_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::UpdateSelectionState(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_UpdateSelectionState_m2684_MethodInfo = 
{
	"UpdateSelectionState"/* name */
	, (methodPointerType)&Selectable_UpdateSelectionState_m2684/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_UpdateSelectionState_m2684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t453_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_EvaluateAndTransitionToSelectionState_m2685_ParameterInfos[] = 
{
	{"eventData", 0, 134218246, 0, &BaseEventData_t453_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::EvaluateAndTransitionToSelectionState(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_EvaluateAndTransitionToSelectionState_m2685_MethodInfo = 
{
	"EvaluateAndTransitionToSelectionState"/* name */
	, (methodPointerType)&Selectable_EvaluateAndTransitionToSelectionState_m2685/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_EvaluateAndTransitionToSelectionState_m2685_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_InternalEvaluateAndTransitionToSelectionState_m2686_ParameterInfos[] = 
{
	{"instant", 0, 134218247, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::InternalEvaluateAndTransitionToSelectionState(System.Boolean)
extern const MethodInfo Selectable_InternalEvaluateAndTransitionToSelectionState_m2686_MethodInfo = 
{
	"InternalEvaluateAndTransitionToSelectionState"/* name */
	, (methodPointerType)&Selectable_InternalEvaluateAndTransitionToSelectionState_m2686/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Selectable_t510_Selectable_InternalEvaluateAndTransitionToSelectionState_m2686_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_OnPointerDown_m2687_ParameterInfos[] = 
{
	{"eventData", 0, 134218248, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerDown_m2687_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&Selectable_OnPointerDown_m2687/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_OnPointerDown_m2687_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_OnPointerUp_m2688_ParameterInfos[] = 
{
	{"eventData", 0, 134218249, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerUp_m2688_MethodInfo = 
{
	"OnPointerUp"/* name */
	, (methodPointerType)&Selectable_OnPointerUp_m2688/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_OnPointerUp_m2688_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_OnPointerEnter_m2689_ParameterInfos[] = 
{
	{"eventData", 0, 134218250, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerEnter_m2689_MethodInfo = 
{
	"OnPointerEnter"/* name */
	, (methodPointerType)&Selectable_OnPointerEnter_m2689/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_OnPointerEnter_m2689_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_OnPointerExit_m2690_ParameterInfos[] = 
{
	{"eventData", 0, 134218251, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerExit_m2690_MethodInfo = 
{
	"OnPointerExit"/* name */
	, (methodPointerType)&Selectable_OnPointerExit_m2690/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_OnPointerExit_m2690_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t453_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_OnSelect_m2691_ParameterInfos[] = 
{
	{"eventData", 0, 134218252, 0, &BaseEventData_t453_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_OnSelect_m2691_MethodInfo = 
{
	"OnSelect"/* name */
	, (methodPointerType)&Selectable_OnSelect_m2691/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_OnSelect_m2691_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t453_0_0_0;
static const ParameterInfo Selectable_t510_Selectable_OnDeselect_m2692_ParameterInfos[] = 
{
	{"eventData", 0, 134218253, 0, &BaseEventData_t453_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_OnDeselect_m2692_MethodInfo = 
{
	"OnDeselect"/* name */
	, (methodPointerType)&Selectable_OnDeselect_m2692/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Selectable_t510_Selectable_OnDeselect_m2692_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Select()
extern const MethodInfo Selectable_Select_m2693_MethodInfo = 
{
	"Select"/* name */
	, (methodPointerType)&Selectable_Select_m2693/* method */
	, &Selectable_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Selectable_t510_MethodInfos[] =
{
	&Selectable__ctor_m2636_MethodInfo,
	&Selectable__cctor_m2637_MethodInfo,
	&Selectable_get_allSelectables_m2638_MethodInfo,
	&Selectable_get_navigation_m2639_MethodInfo,
	&Selectable_set_navigation_m2640_MethodInfo,
	&Selectable_get_transition_m2641_MethodInfo,
	&Selectable_set_transition_m2642_MethodInfo,
	&Selectable_get_colors_m2643_MethodInfo,
	&Selectable_set_colors_m2644_MethodInfo,
	&Selectable_get_spriteState_m2645_MethodInfo,
	&Selectable_set_spriteState_m2646_MethodInfo,
	&Selectable_get_animationTriggers_m2647_MethodInfo,
	&Selectable_set_animationTriggers_m2648_MethodInfo,
	&Selectable_get_targetGraphic_m1734_MethodInfo,
	&Selectable_set_targetGraphic_m1736_MethodInfo,
	&Selectable_get_interactable_m2649_MethodInfo,
	&Selectable_set_interactable_m2650_MethodInfo,
	&Selectable_get_isPointerInside_m2651_MethodInfo,
	&Selectable_set_isPointerInside_m2652_MethodInfo,
	&Selectable_get_isPointerDown_m2653_MethodInfo,
	&Selectable_set_isPointerDown_m2654_MethodInfo,
	&Selectable_get_hasSelection_m2655_MethodInfo,
	&Selectable_set_hasSelection_m2656_MethodInfo,
	&Selectable_get_image_m2657_MethodInfo,
	&Selectable_set_image_m2658_MethodInfo,
	&Selectable_get_animator_m2659_MethodInfo,
	&Selectable_Awake_m2660_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m2661_MethodInfo,
	&Selectable_IsInteractable_m2662_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m2663_MethodInfo,
	&Selectable_OnEnable_m2664_MethodInfo,
	&Selectable_OnSetProperty_m2665_MethodInfo,
	&Selectable_OnDisable_m2666_MethodInfo,
	&Selectable_get_currentSelectionState_m2667_MethodInfo,
	&Selectable_InstantClearState_m2668_MethodInfo,
	&Selectable_DoStateTransition_m2669_MethodInfo,
	&Selectable_FindSelectable_m2670_MethodInfo,
	&Selectable_GetPointOnRectEdge_m2671_MethodInfo,
	&Selectable_Navigate_m2672_MethodInfo,
	&Selectable_FindSelectableOnLeft_m2673_MethodInfo,
	&Selectable_FindSelectableOnRight_m2674_MethodInfo,
	&Selectable_FindSelectableOnUp_m2675_MethodInfo,
	&Selectable_FindSelectableOnDown_m2676_MethodInfo,
	&Selectable_OnMove_m2677_MethodInfo,
	&Selectable_StartColorTween_m2678_MethodInfo,
	&Selectable_DoSpriteSwap_m2679_MethodInfo,
	&Selectable_TriggerAnimation_m2680_MethodInfo,
	&Selectable_IsHighlighted_m2681_MethodInfo,
	&Selectable_IsPressed_m2682_MethodInfo,
	&Selectable_IsPressed_m2683_MethodInfo,
	&Selectable_UpdateSelectionState_m2684_MethodInfo,
	&Selectable_EvaluateAndTransitionToSelectionState_m2685_MethodInfo,
	&Selectable_InternalEvaluateAndTransitionToSelectionState_m2686_MethodInfo,
	&Selectable_OnPointerDown_m2687_MethodInfo,
	&Selectable_OnPointerUp_m2688_MethodInfo,
	&Selectable_OnPointerEnter_m2689_MethodInfo,
	&Selectable_OnPointerExit_m2690_MethodInfo,
	&Selectable_OnSelect_m2691_MethodInfo,
	&Selectable_OnDeselect_m2692_MethodInfo,
	&Selectable_Select_m2693_MethodInfo,
	NULL
};
extern const MethodInfo Selectable_get_allSelectables_m2638_MethodInfo;
static const PropertyInfo Selectable_t510____allSelectables_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "allSelectables"/* name */
	, &Selectable_get_allSelectables_m2638_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_navigation_m2639_MethodInfo;
extern const MethodInfo Selectable_set_navigation_m2640_MethodInfo;
static const PropertyInfo Selectable_t510____navigation_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "navigation"/* name */
	, &Selectable_get_navigation_m2639_MethodInfo/* get */
	, &Selectable_set_navigation_m2640_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_transition_m2641_MethodInfo;
extern const MethodInfo Selectable_set_transition_m2642_MethodInfo;
static const PropertyInfo Selectable_t510____transition_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "transition"/* name */
	, &Selectable_get_transition_m2641_MethodInfo/* get */
	, &Selectable_set_transition_m2642_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_colors_m2643_MethodInfo;
extern const MethodInfo Selectable_set_colors_m2644_MethodInfo;
static const PropertyInfo Selectable_t510____colors_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "colors"/* name */
	, &Selectable_get_colors_m2643_MethodInfo/* get */
	, &Selectable_set_colors_m2644_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_spriteState_m2645_MethodInfo;
extern const MethodInfo Selectable_set_spriteState_m2646_MethodInfo;
static const PropertyInfo Selectable_t510____spriteState_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "spriteState"/* name */
	, &Selectable_get_spriteState_m2645_MethodInfo/* get */
	, &Selectable_set_spriteState_m2646_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_animationTriggers_m2647_MethodInfo;
extern const MethodInfo Selectable_set_animationTriggers_m2648_MethodInfo;
static const PropertyInfo Selectable_t510____animationTriggers_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "animationTriggers"/* name */
	, &Selectable_get_animationTriggers_m2647_MethodInfo/* get */
	, &Selectable_set_animationTriggers_m2648_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_targetGraphic_m1734_MethodInfo;
extern const MethodInfo Selectable_set_targetGraphic_m1736_MethodInfo;
static const PropertyInfo Selectable_t510____targetGraphic_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "targetGraphic"/* name */
	, &Selectable_get_targetGraphic_m1734_MethodInfo/* get */
	, &Selectable_set_targetGraphic_m1736_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_interactable_m2649_MethodInfo;
extern const MethodInfo Selectable_set_interactable_m2650_MethodInfo;
static const PropertyInfo Selectable_t510____interactable_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "interactable"/* name */
	, &Selectable_get_interactable_m2649_MethodInfo/* get */
	, &Selectable_set_interactable_m2650_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_isPointerInside_m2651_MethodInfo;
extern const MethodInfo Selectable_set_isPointerInside_m2652_MethodInfo;
static const PropertyInfo Selectable_t510____isPointerInside_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "isPointerInside"/* name */
	, &Selectable_get_isPointerInside_m2651_MethodInfo/* get */
	, &Selectable_set_isPointerInside_m2652_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_isPointerDown_m2653_MethodInfo;
extern const MethodInfo Selectable_set_isPointerDown_m2654_MethodInfo;
static const PropertyInfo Selectable_t510____isPointerDown_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "isPointerDown"/* name */
	, &Selectable_get_isPointerDown_m2653_MethodInfo/* get */
	, &Selectable_set_isPointerDown_m2654_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_hasSelection_m2655_MethodInfo;
extern const MethodInfo Selectable_set_hasSelection_m2656_MethodInfo;
static const PropertyInfo Selectable_t510____hasSelection_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "hasSelection"/* name */
	, &Selectable_get_hasSelection_m2655_MethodInfo/* get */
	, &Selectable_set_hasSelection_m2656_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_image_m2657_MethodInfo;
extern const MethodInfo Selectable_set_image_m2658_MethodInfo;
static const PropertyInfo Selectable_t510____image_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "image"/* name */
	, &Selectable_get_image_m2657_MethodInfo/* get */
	, &Selectable_set_image_m2658_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_animator_m2659_MethodInfo;
static const PropertyInfo Selectable_t510____animator_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "animator"/* name */
	, &Selectable_get_animator_m2659_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_currentSelectionState_m2667_MethodInfo;
static const PropertyInfo Selectable_t510____currentSelectionState_PropertyInfo = 
{
	&Selectable_t510_il2cpp_TypeInfo/* parent */
	, "currentSelectionState"/* name */
	, &Selectable_get_currentSelectionState_m2667_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Selectable_t510_PropertyInfos[] =
{
	&Selectable_t510____allSelectables_PropertyInfo,
	&Selectable_t510____navigation_PropertyInfo,
	&Selectable_t510____transition_PropertyInfo,
	&Selectable_t510____colors_PropertyInfo,
	&Selectable_t510____spriteState_PropertyInfo,
	&Selectable_t510____animationTriggers_PropertyInfo,
	&Selectable_t510____targetGraphic_PropertyInfo,
	&Selectable_t510____interactable_PropertyInfo,
	&Selectable_t510____isPointerInside_PropertyInfo,
	&Selectable_t510____isPointerDown_PropertyInfo,
	&Selectable_t510____hasSelection_PropertyInfo,
	&Selectable_t510____image_PropertyInfo,
	&Selectable_t510____animator_PropertyInfo,
	&Selectable_t510____currentSelectionState_PropertyInfo,
	NULL
};
static const Il2CppType* Selectable_t510_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Transition_t576_0_0_0,
	&SelectionState_t577_0_0_0,
};
extern const MethodInfo Selectable_Awake_m2660_MethodInfo;
extern const MethodInfo Selectable_OnEnable_m2664_MethodInfo;
extern const MethodInfo Selectable_OnDisable_m2666_MethodInfo;
extern const MethodInfo UIBehaviour_IsActive_m1980_MethodInfo;
extern const MethodInfo Selectable_OnDidApplyAnimationProperties_m2663_MethodInfo;
extern const MethodInfo Selectable_OnCanvasGroupChanged_m2661_MethodInfo;
extern const MethodInfo Selectable_OnPointerEnter_m2689_MethodInfo;
extern const MethodInfo Selectable_OnPointerExit_m2690_MethodInfo;
extern const MethodInfo Selectable_OnPointerDown_m2687_MethodInfo;
extern const MethodInfo Selectable_OnPointerUp_m2688_MethodInfo;
extern const MethodInfo Selectable_OnSelect_m2691_MethodInfo;
extern const MethodInfo Selectable_OnDeselect_m2692_MethodInfo;
extern const MethodInfo Selectable_OnMove_m2677_MethodInfo;
extern const MethodInfo Selectable_IsInteractable_m2662_MethodInfo;
extern const MethodInfo Selectable_InstantClearState_m2668_MethodInfo;
extern const MethodInfo Selectable_DoStateTransition_m2669_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnLeft_m2673_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnRight_m2674_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnUp_m2675_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnDown_m2676_MethodInfo;
extern const MethodInfo Selectable_Select_m2693_MethodInfo;
static const Il2CppMethodReference Selectable_t510_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&Selectable_Awake_m2660_MethodInfo,
	&Selectable_OnEnable_m2664_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&Selectable_OnDisable_m2666_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m2663_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m2661_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&Selectable_OnPointerEnter_m2689_MethodInfo,
	&Selectable_OnPointerExit_m2690_MethodInfo,
	&Selectable_OnPointerDown_m2687_MethodInfo,
	&Selectable_OnPointerUp_m2688_MethodInfo,
	&Selectable_OnSelect_m2691_MethodInfo,
	&Selectable_OnDeselect_m2692_MethodInfo,
	&Selectable_OnMove_m2677_MethodInfo,
	&Selectable_IsInteractable_m2662_MethodInfo,
	&Selectable_InstantClearState_m2668_MethodInfo,
	&Selectable_DoStateTransition_m2669_MethodInfo,
	&Selectable_FindSelectableOnLeft_m2673_MethodInfo,
	&Selectable_FindSelectableOnRight_m2674_MethodInfo,
	&Selectable_FindSelectableOnUp_m2675_MethodInfo,
	&Selectable_FindSelectableOnDown_m2676_MethodInfo,
	&Selectable_OnMove_m2677_MethodInfo,
	&Selectable_OnPointerDown_m2687_MethodInfo,
	&Selectable_OnPointerUp_m2688_MethodInfo,
	&Selectable_OnPointerEnter_m2689_MethodInfo,
	&Selectable_OnPointerExit_m2690_MethodInfo,
	&Selectable_OnSelect_m2691_MethodInfo,
	&Selectable_OnDeselect_m2692_MethodInfo,
	&Selectable_Select_m2693_MethodInfo,
};
static bool Selectable_t510_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IPointerEnterHandler_t630_0_0_0;
extern const Il2CppType IPointerExitHandler_t631_0_0_0;
extern const Il2CppType IPointerDownHandler_t280_0_0_0;
extern const Il2CppType IPointerUpHandler_t282_0_0_0;
extern const Il2CppType ISelectHandler_t638_0_0_0;
extern const Il2CppType IDeselectHandler_t639_0_0_0;
extern const Il2CppType IMoveHandler_t640_0_0_0;
static const Il2CppType* Selectable_t510_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t281_0_0_0,
	&IPointerEnterHandler_t630_0_0_0,
	&IPointerExitHandler_t631_0_0_0,
	&IPointerDownHandler_t280_0_0_0,
	&IPointerUpHandler_t282_0_0_0,
	&ISelectHandler_t638_0_0_0,
	&IDeselectHandler_t639_0_0_0,
	&IMoveHandler_t640_0_0_0,
};
static Il2CppInterfaceOffsetPair Selectable_t510_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t281_0_0_0, 16},
	{ &IPointerEnterHandler_t630_0_0_0, 16},
	{ &IPointerExitHandler_t631_0_0_0, 17},
	{ &IPointerDownHandler_t280_0_0_0, 18},
	{ &IPointerUpHandler_t282_0_0_0, 19},
	{ &ISelectHandler_t638_0_0_0, 20},
	{ &IDeselectHandler_t639_0_0_0, 21},
	{ &IMoveHandler_t640_0_0_0, 22},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Selectable_t510_1_0_0;
struct Selectable_t510;
const Il2CppTypeDefinitionMetadata Selectable_t510_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Selectable_t510_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Selectable_t510_InterfacesTypeInfos/* implementedInterfaces */
	, Selectable_t510_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t455_0_0_0/* parent */
	, Selectable_t510_VTable/* vtableMethods */
	, Selectable_t510_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 451/* fieldStart */

};
TypeInfo Selectable_t510_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Selectable"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Selectable_t510_MethodInfos/* methods */
	, Selectable_t510_PropertyInfos/* properties */
	, NULL/* events */
	, &Selectable_t510_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 225/* custom_attributes_cache */
	, &Selectable_t510_0_0_0/* byval_arg */
	, &Selectable_t510_1_0_0/* this_arg */
	, &Selectable_t510_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Selectable_t510)/* instance_size */
	, sizeof (Selectable_t510)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Selectable_t510_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 60/* method_count */
	, 14/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 38/* vtable_count */
	, 8/* interfaces_count */
	, 8/* interface_offsets_count */

};
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility.h"
// Metadata Definition UnityEngine.UI.SetPropertyUtility
extern TypeInfo SetPropertyUtility_t581_il2cpp_TypeInfo;
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
extern const Il2CppType Color_t65_1_0_0;
extern const Il2CppType Color_t65_1_0_0;
extern const Il2CppType Color_t65_0_0_0;
static const ParameterInfo SetPropertyUtility_t581_SetPropertyUtility_SetColor_m2694_ParameterInfos[] = 
{
	{"currentValue", 0, 134218254, 0, &Color_t65_1_0_0},
	{"newValue", 1, 134218255, 0, &Color_t65_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_ColorU26_t754_Color_t65 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetColor(UnityEngine.Color&,UnityEngine.Color)
extern const MethodInfo SetPropertyUtility_SetColor_m2694_MethodInfo = 
{
	"SetColor"/* name */
	, (methodPointerType)&SetPropertyUtility_SetColor_m2694/* method */
	, &SetPropertyUtility_t581_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_ColorU26_t754_Color_t65/* invoker_method */
	, SetPropertyUtility_t581_SetPropertyUtility_SetColor_m2694_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SetPropertyUtility_SetStruct_m3535_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m3535_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m3535_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m3535_gp_0_0_0_0;
static const ParameterInfo SetPropertyUtility_t581_SetPropertyUtility_SetStruct_m3535_ParameterInfos[] = 
{
	{"currentValue", 0, 134218256, 0, &SetPropertyUtility_SetStruct_m3535_gp_0_1_0_0},
	{"newValue", 1, 134218257, 0, &SetPropertyUtility_SetStruct_m3535_gp_0_0_0_0},
};
extern const Il2CppGenericContainer SetPropertyUtility_SetStruct_m3535_Il2CppGenericContainer;
extern TypeInfo SetPropertyUtility_SetStruct_m3535_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppType ValueType_t285_0_0_0;
static const Il2CppType* SetPropertyUtility_SetStruct_m3535_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t285_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter SetPropertyUtility_SetStruct_m3535_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &SetPropertyUtility_SetStruct_m3535_Il2CppGenericContainer, SetPropertyUtility_SetStruct_m3535_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 24 };
static const Il2CppGenericParameter* SetPropertyUtility_SetStruct_m3535_Il2CppGenericParametersArray[1] = 
{
	&SetPropertyUtility_SetStruct_m3535_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo SetPropertyUtility_SetStruct_m3535_MethodInfo;
extern const Il2CppGenericContainer SetPropertyUtility_SetStruct_m3535_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&SetPropertyUtility_SetStruct_m3535_MethodInfo, 1, 1, SetPropertyUtility_SetStruct_m3535_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition SetPropertyUtility_SetStruct_m3535_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&SetPropertyUtility_SetStruct_m3535_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct(T&,T)
extern const MethodInfo SetPropertyUtility_SetStruct_m3535_MethodInfo = 
{
	"SetStruct"/* name */
	, NULL/* method */
	, &SetPropertyUtility_t581_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, SetPropertyUtility_t581_SetPropertyUtility_SetStruct_m3535_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, SetPropertyUtility_SetStruct_m3535_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &SetPropertyUtility_SetStruct_m3535_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType SetPropertyUtility_SetClass_m3536_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m3536_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m3536_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m3536_gp_0_0_0_0;
static const ParameterInfo SetPropertyUtility_t581_SetPropertyUtility_SetClass_m3536_ParameterInfos[] = 
{
	{"currentValue", 0, 134218258, 0, &SetPropertyUtility_SetClass_m3536_gp_0_1_0_0},
	{"newValue", 1, 134218259, 0, &SetPropertyUtility_SetClass_m3536_gp_0_0_0_0},
};
extern const Il2CppGenericContainer SetPropertyUtility_SetClass_m3536_Il2CppGenericContainer;
extern TypeInfo SetPropertyUtility_SetClass_m3536_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter SetPropertyUtility_SetClass_m3536_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &SetPropertyUtility_SetClass_m3536_Il2CppGenericContainer, NULL, "T", 0, 4 };
static const Il2CppGenericParameter* SetPropertyUtility_SetClass_m3536_Il2CppGenericParametersArray[1] = 
{
	&SetPropertyUtility_SetClass_m3536_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo SetPropertyUtility_SetClass_m3536_MethodInfo;
extern const Il2CppGenericContainer SetPropertyUtility_SetClass_m3536_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&SetPropertyUtility_SetClass_m3536_MethodInfo, 1, 1, SetPropertyUtility_SetClass_m3536_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition SetPropertyUtility_SetClass_m3536_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&SetPropertyUtility_SetClass_m3536_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetClass(T&,T)
extern const MethodInfo SetPropertyUtility_SetClass_m3536_MethodInfo = 
{
	"SetClass"/* name */
	, NULL/* method */
	, &SetPropertyUtility_t581_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, SetPropertyUtility_t581_SetPropertyUtility_SetClass_m3536_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, SetPropertyUtility_SetClass_m3536_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &SetPropertyUtility_SetClass_m3536_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* SetPropertyUtility_t581_MethodInfos[] =
{
	&SetPropertyUtility_SetColor_m2694_MethodInfo,
	&SetPropertyUtility_SetStruct_m3535_MethodInfo,
	&SetPropertyUtility_SetClass_m3536_MethodInfo,
	NULL
};
extern const MethodInfo Object_ToString_m1075_MethodInfo;
static const Il2CppMethodReference SetPropertyUtility_t581_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool SetPropertyUtility_t581_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SetPropertyUtility_t581_0_0_0;
extern const Il2CppType SetPropertyUtility_t581_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SetPropertyUtility_t581;
const Il2CppTypeDefinitionMetadata SetPropertyUtility_t581_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetPropertyUtility_t581_VTable/* vtableMethods */
	, SetPropertyUtility_t581_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SetPropertyUtility_t581_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetPropertyUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, SetPropertyUtility_t581_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetPropertyUtility_t581_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetPropertyUtility_t581_0_0_0/* byval_arg */
	, &SetPropertyUtility_t581_1_0_0/* this_arg */
	, &SetPropertyUtility_t581_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetPropertyUtility_t581)/* instance_size */
	, sizeof (SetPropertyUtility_t581)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
// Metadata Definition UnityEngine.UI.Slider/Direction
extern TypeInfo Direction_t582_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_DirectionMethodDeclarations.h"
static const MethodInfo* Direction_t582_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Direction_t582_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Direction_t582_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Direction_t582_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Direction_t582_0_0_0;
extern const Il2CppType Direction_t582_1_0_0;
extern TypeInfo Slider_t585_il2cpp_TypeInfo;
extern const Il2CppType Slider_t585_0_0_0;
const Il2CppTypeDefinitionMetadata Direction_t582_DefinitionMetadata = 
{
	&Slider_t585_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Direction_t582_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Direction_t582_VTable/* vtableMethods */
	, Direction_t582_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 465/* fieldStart */

};
TypeInfo Direction_t582_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Direction"/* name */
	, ""/* namespaze */
	, Direction_t582_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Direction_t582_0_0_0/* byval_arg */
	, &Direction_t582_1_0_0/* this_arg */
	, &Direction_t582_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Direction_t582)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Direction_t582)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent.h"
// Metadata Definition UnityEngine.UI.Slider/SliderEvent
extern TypeInfo SliderEvent_t583_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider/SliderEvent::.ctor()
extern const MethodInfo SliderEvent__ctor_m2695_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderEvent__ctor_m2695/* method */
	, &SliderEvent_t583_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SliderEvent_t583_MethodInfos[] =
{
	&SliderEvent__ctor_m2695_MethodInfo,
	NULL
};
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m3607_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m3608_GenericMethod;
static const Il2CppMethodReference SliderEvent_t583_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&UnityEventBase_ToString_m3581_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m3607_GenericMethod,
	&UnityEvent_1_GetDelegate_m3608_GenericMethod,
};
static bool SliderEvent_t583_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
static Il2CppInterfaceOffsetPair SliderEvent_t583_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SliderEvent_t583_0_0_0;
extern const Il2CppType SliderEvent_t583_1_0_0;
extern const Il2CppType UnityEvent_1_t567_0_0_0;
struct SliderEvent_t583;
const Il2CppTypeDefinitionMetadata SliderEvent_t583_DefinitionMetadata = 
{
	&Slider_t585_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SliderEvent_t583_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t567_0_0_0/* parent */
	, SliderEvent_t583_VTable/* vtableMethods */
	, SliderEvent_t583_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SliderEvent_t583_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderEvent"/* name */
	, ""/* namespaze */
	, SliderEvent_t583_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SliderEvent_t583_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderEvent_t583_0_0_0/* byval_arg */
	, &SliderEvent_t583_1_0_0/* this_arg */
	, &SliderEvent_t583_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderEvent_t583)/* instance_size */
	, sizeof (SliderEvent_t583)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
// Metadata Definition UnityEngine.UI.Slider/Axis
extern TypeInfo Axis_t584_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_AxisMethodDeclarations.h"
static const MethodInfo* Axis_t584_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Axis_t584_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Axis_t584_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Axis_t584_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Axis_t584_0_0_0;
extern const Il2CppType Axis_t584_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t584_DefinitionMetadata = 
{
	&Slider_t585_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t584_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Axis_t584_VTable/* vtableMethods */
	, Axis_t584_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 470/* fieldStart */

};
TypeInfo Axis_t584_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, Axis_t584_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t584_0_0_0/* byval_arg */
	, &Axis_t584_1_0_0/* this_arg */
	, &Axis_t584_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t584)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t584)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_Slider.h"
// Metadata Definition UnityEngine.UI.Slider
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_SliderMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::.ctor()
extern const MethodInfo Slider__ctor_m2696_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Slider__ctor_m2696/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Slider::get_fillRect()
extern const MethodInfo Slider_get_fillRect_m2697_MethodInfo = 
{
	"get_fillRect"/* name */
	, (methodPointerType)&Slider_get_fillRect_m2697/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t364_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo Slider_t585_Slider_set_fillRect_m2698_ParameterInfos[] = 
{
	{"value", 0, 134218260, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_fillRect(UnityEngine.RectTransform)
extern const MethodInfo Slider_set_fillRect_m2698_MethodInfo = 
{
	"set_fillRect"/* name */
	, (methodPointerType)&Slider_set_fillRect_m2698/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Slider_t585_Slider_set_fillRect_m2698_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Slider::get_handleRect()
extern const MethodInfo Slider_get_handleRect_m2699_MethodInfo = 
{
	"get_handleRect"/* name */
	, (methodPointerType)&Slider_get_handleRect_m2699/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t364_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo Slider_t585_Slider_set_handleRect_m2700_ParameterInfos[] = 
{
	{"value", 0, 134218261, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_handleRect(UnityEngine.RectTransform)
extern const MethodInfo Slider_set_handleRect_m2700_MethodInfo = 
{
	"set_handleRect"/* name */
	, (methodPointerType)&Slider_set_handleRect_m2700/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Slider_t585_Slider_set_handleRect_m2700_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Direction_t582 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::get_direction()
extern const MethodInfo Slider_get_direction_m2701_MethodInfo = 
{
	"get_direction"/* name */
	, (methodPointerType)&Slider_get_direction_m2701/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Direction_t582_0_0_0/* return_type */
	, RuntimeInvoker_Direction_t582/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Direction_t582_0_0_0;
static const ParameterInfo Slider_t585_Slider_set_direction_m2702_ParameterInfos[] = 
{
	{"value", 0, 134218262, 0, &Direction_t582_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_direction(UnityEngine.UI.Slider/Direction)
extern const MethodInfo Slider_set_direction_m2702_MethodInfo = 
{
	"set_direction"/* name */
	, (methodPointerType)&Slider_set_direction_m2702/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Slider_t585_Slider_set_direction_m2702_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_minValue()
extern const MethodInfo Slider_get_minValue_m2703_MethodInfo = 
{
	"get_minValue"/* name */
	, (methodPointerType)&Slider_get_minValue_m2703/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo Slider_t585_Slider_set_minValue_m2704_ParameterInfos[] = 
{
	{"value", 0, 134218263, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_minValue(System.Single)
extern const MethodInfo Slider_set_minValue_m2704_MethodInfo = 
{
	"set_minValue"/* name */
	, (methodPointerType)&Slider_set_minValue_m2704/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, Slider_t585_Slider_set_minValue_m2704_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_maxValue()
extern const MethodInfo Slider_get_maxValue_m2705_MethodInfo = 
{
	"get_maxValue"/* name */
	, (methodPointerType)&Slider_get_maxValue_m2705/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo Slider_t585_Slider_set_maxValue_m2706_ParameterInfos[] = 
{
	{"value", 0, 134218264, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_maxValue(System.Single)
extern const MethodInfo Slider_set_maxValue_m2706_MethodInfo = 
{
	"set_maxValue"/* name */
	, (methodPointerType)&Slider_set_maxValue_m2706/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, Slider_t585_Slider_set_maxValue_m2706_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::get_wholeNumbers()
extern const MethodInfo Slider_get_wholeNumbers_m2707_MethodInfo = 
{
	"get_wholeNumbers"/* name */
	, (methodPointerType)&Slider_get_wholeNumbers_m2707/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Slider_t585_Slider_set_wholeNumbers_m2708_ParameterInfos[] = 
{
	{"value", 0, 134218265, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_wholeNumbers(System.Boolean)
extern const MethodInfo Slider_set_wholeNumbers_m2708_MethodInfo = 
{
	"set_wholeNumbers"/* name */
	, (methodPointerType)&Slider_set_wholeNumbers_m2708/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Slider_t585_Slider_set_wholeNumbers_m2708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_value()
extern const MethodInfo Slider_get_value_m2709_MethodInfo = 
{
	"get_value"/* name */
	, (methodPointerType)&Slider_get_value_m2709/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo Slider_t585_Slider_set_value_m2710_ParameterInfos[] = 
{
	{"value", 0, 134218266, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_value(System.Single)
extern const MethodInfo Slider_set_value_m2710_MethodInfo = 
{
	"set_value"/* name */
	, (methodPointerType)&Slider_set_value_m2710/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, Slider_t585_Slider_set_value_m2710_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_normalizedValue()
extern const MethodInfo Slider_get_normalizedValue_m2711_MethodInfo = 
{
	"get_normalizedValue"/* name */
	, (methodPointerType)&Slider_get_normalizedValue_m2711/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo Slider_t585_Slider_set_normalizedValue_m2712_ParameterInfos[] = 
{
	{"value", 0, 134218267, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_normalizedValue(System.Single)
extern const MethodInfo Slider_set_normalizedValue_m2712_MethodInfo = 
{
	"set_normalizedValue"/* name */
	, (methodPointerType)&Slider_set_normalizedValue_m2712/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, Slider_t585_Slider_set_normalizedValue_m2712_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::get_onValueChanged()
extern const MethodInfo Slider_get_onValueChanged_m2713_MethodInfo = 
{
	"get_onValueChanged"/* name */
	, (methodPointerType)&Slider_get_onValueChanged_m2713/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &SliderEvent_t583_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SliderEvent_t583_0_0_0;
static const ParameterInfo Slider_t585_Slider_set_onValueChanged_m2714_ParameterInfos[] = 
{
	{"value", 0, 134218268, 0, &SliderEvent_t583_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_onValueChanged(UnityEngine.UI.Slider/SliderEvent)
extern const MethodInfo Slider_set_onValueChanged_m2714_MethodInfo = 
{
	"set_onValueChanged"/* name */
	, (methodPointerType)&Slider_set_onValueChanged_m2714/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Slider_t585_Slider_set_onValueChanged_m2714_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_stepSize()
extern const MethodInfo Slider_get_stepSize_m2715_MethodInfo = 
{
	"get_stepSize"/* name */
	, (methodPointerType)&Slider_get_stepSize_m2715/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t511_0_0_0;
static const ParameterInfo Slider_t585_Slider_Rebuild_m2716_ParameterInfos[] = 
{
	{"executing", 0, 134218269, 0, &CanvasUpdate_t511_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo Slider_Rebuild_m2716_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&Slider_Rebuild_m2716/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Slider_t585_Slider_Rebuild_m2716_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnEnable()
extern const MethodInfo Slider_OnEnable_m2717_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Slider_OnEnable_m2717/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnDisable()
extern const MethodInfo Slider_OnDisable_m2718_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Slider_OnDisable_m2718/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateCachedReferences()
extern const MethodInfo Slider_UpdateCachedReferences_m2719_MethodInfo = 
{
	"UpdateCachedReferences"/* name */
	, (methodPointerType)&Slider_UpdateCachedReferences_m2719/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo Slider_t585_Slider_Set_m2720_ParameterInfos[] = 
{
	{"input", 0, 134218270, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Set(System.Single)
extern const MethodInfo Slider_Set_m2720_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Slider_Set_m2720/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, Slider_t585_Slider_Set_m2720_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Slider_t585_Slider_Set_m2721_ParameterInfos[] = 
{
	{"input", 0, 134218271, 0, &Single_t254_0_0_0},
	{"sendCallback", 1, 134218272, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Set(System.Single,System.Boolean)
extern const MethodInfo Slider_Set_m2721_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Slider_Set_m2721/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_SByte_t274/* invoker_method */
	, Slider_t585_Slider_Set_m2721_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnRectTransformDimensionsChange()
extern const MethodInfo Slider_OnRectTransformDimensionsChange_m2722_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&Slider_OnRectTransformDimensionsChange_m2722/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Axis_t584 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/Axis UnityEngine.UI.Slider::get_axis()
extern const MethodInfo Slider_get_axis_m2723_MethodInfo = 
{
	"get_axis"/* name */
	, (methodPointerType)&Slider_get_axis_m2723/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t584_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t584/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::get_reverseValue()
extern const MethodInfo Slider_get_reverseValue_m2724_MethodInfo = 
{
	"get_reverseValue"/* name */
	, (methodPointerType)&Slider_get_reverseValue_m2724/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateVisuals()
extern const MethodInfo Slider_UpdateVisuals_m2725_MethodInfo = 
{
	"UpdateVisuals"/* name */
	, (methodPointerType)&Slider_UpdateVisuals_m2725/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
extern const Il2CppType Camera_t27_0_0_0;
extern const Il2CppType Camera_t27_0_0_0;
static const ParameterInfo Slider_t585_Slider_UpdateDrag_m2726_ParameterInfos[] = 
{
	{"eventData", 0, 134218273, 0, &PointerEventData_t215_0_0_0},
	{"cam", 1, 134218274, 0, &Camera_t27_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateDrag(UnityEngine.EventSystems.PointerEventData,UnityEngine.Camera)
extern const MethodInfo Slider_UpdateDrag_m2726_MethodInfo = 
{
	"UpdateDrag"/* name */
	, (methodPointerType)&Slider_UpdateDrag_m2726/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Slider_t585_Slider_UpdateDrag_m2726_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Slider_t585_Slider_MayDrag_m2727_ParameterInfos[] = 
{
	{"eventData", 0, 134218275, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::MayDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_MayDrag_m2727_MethodInfo = 
{
	"MayDrag"/* name */
	, (methodPointerType)&Slider_MayDrag_m2727/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Slider_t585_Slider_MayDrag_m2727_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Slider_t585_Slider_OnPointerDown_m2728_ParameterInfos[] = 
{
	{"eventData", 0, 134218276, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnPointerDown_m2728_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&Slider_OnPointerDown_m2728/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Slider_t585_Slider_OnPointerDown_m2728_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Slider_t585_Slider_OnDrag_m2729_ParameterInfos[] = 
{
	{"eventData", 0, 134218277, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnDrag_m2729_MethodInfo = 
{
	"OnDrag"/* name */
	, (methodPointerType)&Slider_OnDrag_m2729/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Slider_t585_Slider_OnDrag_m2729_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 44/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t487_0_0_0;
static const ParameterInfo Slider_t585_Slider_OnMove_m2730_ParameterInfos[] = 
{
	{"eventData", 0, 134218278, 0, &AxisEventData_t487_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnMove(UnityEngine.EventSystems.AxisEventData)
extern const MethodInfo Slider_OnMove_m2730_MethodInfo = 
{
	"OnMove"/* name */
	, (methodPointerType)&Slider_OnMove_m2730/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Slider_t585_Slider_OnMove_m2730_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnLeft()
extern const MethodInfo Slider_FindSelectableOnLeft_m2731_MethodInfo = 
{
	"FindSelectableOnLeft"/* name */
	, (methodPointerType)&Slider_FindSelectableOnLeft_m2731/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t510_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnRight()
extern const MethodInfo Slider_FindSelectableOnRight_m2732_MethodInfo = 
{
	"FindSelectableOnRight"/* name */
	, (methodPointerType)&Slider_FindSelectableOnRight_m2732/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t510_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnUp()
extern const MethodInfo Slider_FindSelectableOnUp_m2733_MethodInfo = 
{
	"FindSelectableOnUp"/* name */
	, (methodPointerType)&Slider_FindSelectableOnUp_m2733/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t510_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnDown()
extern const MethodInfo Slider_FindSelectableOnDown_m2734_MethodInfo = 
{
	"FindSelectableOnDown"/* name */
	, (methodPointerType)&Slider_FindSelectableOnDown_m2734/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t510_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Slider_t585_Slider_OnInitializePotentialDrag_m2735_ParameterInfos[] = 
{
	{"eventData", 0, 134218279, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnInitializePotentialDrag_m2735_MethodInfo = 
{
	"OnInitializePotentialDrag"/* name */
	, (methodPointerType)&Slider_OnInitializePotentialDrag_m2735/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Slider_t585_Slider_OnInitializePotentialDrag_m2735_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Direction_t582_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Slider_t585_Slider_SetDirection_m2736_ParameterInfos[] = 
{
	{"direction", 0, 134218280, 0, &Direction_t582_0_0_0},
	{"includeRectLayouts", 1, 134218281, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::SetDirection(UnityEngine.UI.Slider/Direction,System.Boolean)
extern const MethodInfo Slider_SetDirection_m2736_MethodInfo = 
{
	"SetDirection"/* name */
	, (methodPointerType)&Slider_SetDirection_m2736/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_SByte_t274/* invoker_method */
	, Slider_t585_Slider_SetDirection_m2736_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m2737_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m2737/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 46/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.Slider::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_get_transform_m2738_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&Slider_UnityEngine_UI_ICanvasElement_get_transform_m2738/* method */
	, &Slider_t585_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t1_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Slider_t585_MethodInfos[] =
{
	&Slider__ctor_m2696_MethodInfo,
	&Slider_get_fillRect_m2697_MethodInfo,
	&Slider_set_fillRect_m2698_MethodInfo,
	&Slider_get_handleRect_m2699_MethodInfo,
	&Slider_set_handleRect_m2700_MethodInfo,
	&Slider_get_direction_m2701_MethodInfo,
	&Slider_set_direction_m2702_MethodInfo,
	&Slider_get_minValue_m2703_MethodInfo,
	&Slider_set_minValue_m2704_MethodInfo,
	&Slider_get_maxValue_m2705_MethodInfo,
	&Slider_set_maxValue_m2706_MethodInfo,
	&Slider_get_wholeNumbers_m2707_MethodInfo,
	&Slider_set_wholeNumbers_m2708_MethodInfo,
	&Slider_get_value_m2709_MethodInfo,
	&Slider_set_value_m2710_MethodInfo,
	&Slider_get_normalizedValue_m2711_MethodInfo,
	&Slider_set_normalizedValue_m2712_MethodInfo,
	&Slider_get_onValueChanged_m2713_MethodInfo,
	&Slider_set_onValueChanged_m2714_MethodInfo,
	&Slider_get_stepSize_m2715_MethodInfo,
	&Slider_Rebuild_m2716_MethodInfo,
	&Slider_OnEnable_m2717_MethodInfo,
	&Slider_OnDisable_m2718_MethodInfo,
	&Slider_UpdateCachedReferences_m2719_MethodInfo,
	&Slider_Set_m2720_MethodInfo,
	&Slider_Set_m2721_MethodInfo,
	&Slider_OnRectTransformDimensionsChange_m2722_MethodInfo,
	&Slider_get_axis_m2723_MethodInfo,
	&Slider_get_reverseValue_m2724_MethodInfo,
	&Slider_UpdateVisuals_m2725_MethodInfo,
	&Slider_UpdateDrag_m2726_MethodInfo,
	&Slider_MayDrag_m2727_MethodInfo,
	&Slider_OnPointerDown_m2728_MethodInfo,
	&Slider_OnDrag_m2729_MethodInfo,
	&Slider_OnMove_m2730_MethodInfo,
	&Slider_FindSelectableOnLeft_m2731_MethodInfo,
	&Slider_FindSelectableOnRight_m2732_MethodInfo,
	&Slider_FindSelectableOnUp_m2733_MethodInfo,
	&Slider_FindSelectableOnDown_m2734_MethodInfo,
	&Slider_OnInitializePotentialDrag_m2735_MethodInfo,
	&Slider_SetDirection_m2736_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m2737_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m2738_MethodInfo,
	NULL
};
extern const MethodInfo Slider_get_fillRect_m2697_MethodInfo;
extern const MethodInfo Slider_set_fillRect_m2698_MethodInfo;
static const PropertyInfo Slider_t585____fillRect_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "fillRect"/* name */
	, &Slider_get_fillRect_m2697_MethodInfo/* get */
	, &Slider_set_fillRect_m2698_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_handleRect_m2699_MethodInfo;
extern const MethodInfo Slider_set_handleRect_m2700_MethodInfo;
static const PropertyInfo Slider_t585____handleRect_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "handleRect"/* name */
	, &Slider_get_handleRect_m2699_MethodInfo/* get */
	, &Slider_set_handleRect_m2700_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_direction_m2701_MethodInfo;
extern const MethodInfo Slider_set_direction_m2702_MethodInfo;
static const PropertyInfo Slider_t585____direction_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "direction"/* name */
	, &Slider_get_direction_m2701_MethodInfo/* get */
	, &Slider_set_direction_m2702_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_minValue_m2703_MethodInfo;
extern const MethodInfo Slider_set_minValue_m2704_MethodInfo;
static const PropertyInfo Slider_t585____minValue_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "minValue"/* name */
	, &Slider_get_minValue_m2703_MethodInfo/* get */
	, &Slider_set_minValue_m2704_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_maxValue_m2705_MethodInfo;
extern const MethodInfo Slider_set_maxValue_m2706_MethodInfo;
static const PropertyInfo Slider_t585____maxValue_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "maxValue"/* name */
	, &Slider_get_maxValue_m2705_MethodInfo/* get */
	, &Slider_set_maxValue_m2706_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_wholeNumbers_m2707_MethodInfo;
extern const MethodInfo Slider_set_wholeNumbers_m2708_MethodInfo;
static const PropertyInfo Slider_t585____wholeNumbers_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "wholeNumbers"/* name */
	, &Slider_get_wholeNumbers_m2707_MethodInfo/* get */
	, &Slider_set_wholeNumbers_m2708_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_value_m2709_MethodInfo;
extern const MethodInfo Slider_set_value_m2710_MethodInfo;
static const PropertyInfo Slider_t585____value_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "value"/* name */
	, &Slider_get_value_m2709_MethodInfo/* get */
	, &Slider_set_value_m2710_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_normalizedValue_m2711_MethodInfo;
extern const MethodInfo Slider_set_normalizedValue_m2712_MethodInfo;
static const PropertyInfo Slider_t585____normalizedValue_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "normalizedValue"/* name */
	, &Slider_get_normalizedValue_m2711_MethodInfo/* get */
	, &Slider_set_normalizedValue_m2712_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_onValueChanged_m2713_MethodInfo;
extern const MethodInfo Slider_set_onValueChanged_m2714_MethodInfo;
static const PropertyInfo Slider_t585____onValueChanged_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "onValueChanged"/* name */
	, &Slider_get_onValueChanged_m2713_MethodInfo/* get */
	, &Slider_set_onValueChanged_m2714_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_stepSize_m2715_MethodInfo;
static const PropertyInfo Slider_t585____stepSize_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "stepSize"/* name */
	, &Slider_get_stepSize_m2715_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_axis_m2723_MethodInfo;
static const PropertyInfo Slider_t585____axis_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "axis"/* name */
	, &Slider_get_axis_m2723_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_reverseValue_m2724_MethodInfo;
static const PropertyInfo Slider_t585____reverseValue_PropertyInfo = 
{
	&Slider_t585_il2cpp_TypeInfo/* parent */
	, "reverseValue"/* name */
	, &Slider_get_reverseValue_m2724_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Slider_t585_PropertyInfos[] =
{
	&Slider_t585____fillRect_PropertyInfo,
	&Slider_t585____handleRect_PropertyInfo,
	&Slider_t585____direction_PropertyInfo,
	&Slider_t585____minValue_PropertyInfo,
	&Slider_t585____maxValue_PropertyInfo,
	&Slider_t585____wholeNumbers_PropertyInfo,
	&Slider_t585____value_PropertyInfo,
	&Slider_t585____normalizedValue_PropertyInfo,
	&Slider_t585____onValueChanged_PropertyInfo,
	&Slider_t585____stepSize_PropertyInfo,
	&Slider_t585____axis_PropertyInfo,
	&Slider_t585____reverseValue_PropertyInfo,
	NULL
};
static const Il2CppType* Slider_t585_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Direction_t582_0_0_0,
	&SliderEvent_t583_0_0_0,
	&Axis_t584_0_0_0,
};
extern const MethodInfo Slider_OnEnable_m2717_MethodInfo;
extern const MethodInfo Slider_OnDisable_m2718_MethodInfo;
extern const MethodInfo Slider_OnRectTransformDimensionsChange_m2722_MethodInfo;
extern const MethodInfo Slider_OnPointerDown_m2728_MethodInfo;
extern const MethodInfo Slider_OnMove_m2730_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnLeft_m2731_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnRight_m2732_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnUp_m2733_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnDown_m2734_MethodInfo;
extern const MethodInfo Slider_OnInitializePotentialDrag_m2735_MethodInfo;
extern const MethodInfo Slider_OnDrag_m2729_MethodInfo;
extern const MethodInfo Slider_Rebuild_m2716_MethodInfo;
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_get_transform_m2738_MethodInfo;
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m2737_MethodInfo;
static const Il2CppMethodReference Slider_t585_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&Selectable_Awake_m2660_MethodInfo,
	&Slider_OnEnable_m2717_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&Slider_OnDisable_m2718_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&Slider_OnRectTransformDimensionsChange_m2722_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m2663_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m2661_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&Selectable_OnPointerEnter_m2689_MethodInfo,
	&Selectable_OnPointerExit_m2690_MethodInfo,
	&Slider_OnPointerDown_m2728_MethodInfo,
	&Selectable_OnPointerUp_m2688_MethodInfo,
	&Selectable_OnSelect_m2691_MethodInfo,
	&Selectable_OnDeselect_m2692_MethodInfo,
	&Slider_OnMove_m2730_MethodInfo,
	&Selectable_IsInteractable_m2662_MethodInfo,
	&Selectable_InstantClearState_m2668_MethodInfo,
	&Selectable_DoStateTransition_m2669_MethodInfo,
	&Slider_FindSelectableOnLeft_m2731_MethodInfo,
	&Slider_FindSelectableOnRight_m2732_MethodInfo,
	&Slider_FindSelectableOnUp_m2733_MethodInfo,
	&Slider_FindSelectableOnDown_m2734_MethodInfo,
	&Slider_OnMove_m2730_MethodInfo,
	&Slider_OnPointerDown_m2728_MethodInfo,
	&Selectable_OnPointerUp_m2688_MethodInfo,
	&Selectable_OnPointerEnter_m2689_MethodInfo,
	&Selectable_OnPointerExit_m2690_MethodInfo,
	&Selectable_OnSelect_m2691_MethodInfo,
	&Selectable_OnDeselect_m2692_MethodInfo,
	&Selectable_Select_m2693_MethodInfo,
	&Slider_OnInitializePotentialDrag_m2735_MethodInfo,
	&Slider_OnDrag_m2729_MethodInfo,
	&Slider_Rebuild_m2716_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m2738_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m2737_MethodInfo,
	&Slider_Rebuild_m2716_MethodInfo,
	&Slider_OnDrag_m2729_MethodInfo,
	&Slider_OnInitializePotentialDrag_m2735_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m2737_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m2738_MethodInfo,
};
static bool Slider_t585_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Slider_t585_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t281_0_0_0,
	&IInitializePotentialDragHandler_t632_0_0_0,
	&IDragHandler_t283_0_0_0,
	&ICanvasElement_t646_0_0_0,
};
static Il2CppInterfaceOffsetPair Slider_t585_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t281_0_0_0, 16},
	{ &IPointerEnterHandler_t630_0_0_0, 16},
	{ &IPointerExitHandler_t631_0_0_0, 17},
	{ &IPointerDownHandler_t280_0_0_0, 18},
	{ &IPointerUpHandler_t282_0_0_0, 19},
	{ &ISelectHandler_t638_0_0_0, 20},
	{ &IDeselectHandler_t639_0_0_0, 21},
	{ &IMoveHandler_t640_0_0_0, 22},
	{ &IInitializePotentialDragHandler_t632_0_0_0, 38},
	{ &IDragHandler_t283_0_0_0, 39},
	{ &ICanvasElement_t646_0_0_0, 40},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Slider_t585_1_0_0;
struct Slider_t585;
const Il2CppTypeDefinitionMetadata Slider_t585_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Slider_t585_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Slider_t585_InterfacesTypeInfos/* implementedInterfaces */
	, Slider_t585_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t510_0_0_0/* parent */
	, Slider_t585_VTable/* vtableMethods */
	, Slider_t585_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 473/* fieldStart */

};
TypeInfo Slider_t585_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Slider"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Slider_t585_MethodInfos/* methods */
	, Slider_t585_PropertyInfos/* properties */
	, NULL/* events */
	, &Slider_t585_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 243/* custom_attributes_cache */
	, &Slider_t585_0_0_0/* byval_arg */
	, &Slider_t585_1_0_0/* this_arg */
	, &Slider_t585_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Slider_t585)/* instance_size */
	, sizeof (Slider_t585)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 43/* method_count */
	, 12/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 48/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
// Metadata Definition UnityEngine.UI.SpriteState
extern TypeInfo SpriteState_t580_il2cpp_TypeInfo;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteStateMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
extern const MethodInfo SpriteState_get_highlightedSprite_m2739_MethodInfo = 
{
	"get_highlightedSprite"/* name */
	, (methodPointerType)&SpriteState_get_highlightedSprite_m2739/* method */
	, &SpriteState_t580_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t329_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t329_0_0_0;
static const ParameterInfo SpriteState_t580_SpriteState_set_highlightedSprite_m2740_ParameterInfos[] = 
{
	{"value", 0, 134218282, 0, &Sprite_t329_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_highlightedSprite_m2740_MethodInfo = 
{
	"set_highlightedSprite"/* name */
	, (methodPointerType)&SpriteState_set_highlightedSprite_m2740/* method */
	, &SpriteState_t580_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SpriteState_t580_SpriteState_set_highlightedSprite_m2740_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
extern const MethodInfo SpriteState_get_pressedSprite_m2741_MethodInfo = 
{
	"get_pressedSprite"/* name */
	, (methodPointerType)&SpriteState_get_pressedSprite_m2741/* method */
	, &SpriteState_t580_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t329_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t329_0_0_0;
static const ParameterInfo SpriteState_t580_SpriteState_set_pressedSprite_m2742_ParameterInfos[] = 
{
	{"value", 0, 134218283, 0, &Sprite_t329_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_pressedSprite_m2742_MethodInfo = 
{
	"set_pressedSprite"/* name */
	, (methodPointerType)&SpriteState_set_pressedSprite_m2742/* method */
	, &SpriteState_t580_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SpriteState_t580_SpriteState_set_pressedSprite_m2742_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
extern const MethodInfo SpriteState_get_disabledSprite_m2743_MethodInfo = 
{
	"get_disabledSprite"/* name */
	, (methodPointerType)&SpriteState_get_disabledSprite_m2743/* method */
	, &SpriteState_t580_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t329_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t329_0_0_0;
static const ParameterInfo SpriteState_t580_SpriteState_set_disabledSprite_m2744_ParameterInfos[] = 
{
	{"value", 0, 134218284, 0, &Sprite_t329_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_disabledSprite_m2744_MethodInfo = 
{
	"set_disabledSprite"/* name */
	, (methodPointerType)&SpriteState_set_disabledSprite_m2744/* method */
	, &SpriteState_t580_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SpriteState_t580_SpriteState_set_disabledSprite_m2744_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SpriteState_t580_MethodInfos[] =
{
	&SpriteState_get_highlightedSprite_m2739_MethodInfo,
	&SpriteState_set_highlightedSprite_m2740_MethodInfo,
	&SpriteState_get_pressedSprite_m2741_MethodInfo,
	&SpriteState_set_pressedSprite_m2742_MethodInfo,
	&SpriteState_get_disabledSprite_m2743_MethodInfo,
	&SpriteState_set_disabledSprite_m2744_MethodInfo,
	NULL
};
extern const MethodInfo SpriteState_get_highlightedSprite_m2739_MethodInfo;
extern const MethodInfo SpriteState_set_highlightedSprite_m2740_MethodInfo;
static const PropertyInfo SpriteState_t580____highlightedSprite_PropertyInfo = 
{
	&SpriteState_t580_il2cpp_TypeInfo/* parent */
	, "highlightedSprite"/* name */
	, &SpriteState_get_highlightedSprite_m2739_MethodInfo/* get */
	, &SpriteState_set_highlightedSprite_m2740_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SpriteState_get_pressedSprite_m2741_MethodInfo;
extern const MethodInfo SpriteState_set_pressedSprite_m2742_MethodInfo;
static const PropertyInfo SpriteState_t580____pressedSprite_PropertyInfo = 
{
	&SpriteState_t580_il2cpp_TypeInfo/* parent */
	, "pressedSprite"/* name */
	, &SpriteState_get_pressedSprite_m2741_MethodInfo/* get */
	, &SpriteState_set_pressedSprite_m2742_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SpriteState_get_disabledSprite_m2743_MethodInfo;
extern const MethodInfo SpriteState_set_disabledSprite_m2744_MethodInfo;
static const PropertyInfo SpriteState_t580____disabledSprite_PropertyInfo = 
{
	&SpriteState_t580_il2cpp_TypeInfo/* parent */
	, "disabledSprite"/* name */
	, &SpriteState_get_disabledSprite_m2743_MethodInfo/* get */
	, &SpriteState_set_disabledSprite_m2744_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SpriteState_t580_PropertyInfos[] =
{
	&SpriteState_t580____highlightedSprite_PropertyInfo,
	&SpriteState_t580____pressedSprite_PropertyInfo,
	&SpriteState_t580____disabledSprite_PropertyInfo,
	NULL
};
extern const MethodInfo ValueType_Equals_m1076_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m1077_MethodInfo;
extern const MethodInfo ValueType_ToString_m1078_MethodInfo;
static const Il2CppMethodReference SpriteState_t580_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool SpriteState_t580_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SpriteState_t580_1_0_0;
const Il2CppTypeDefinitionMetadata SpriteState_t580_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, SpriteState_t580_VTable/* vtableMethods */
	, SpriteState_t580_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 488/* fieldStart */

};
TypeInfo SpriteState_t580_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpriteState"/* name */
	, "UnityEngine.UI"/* namespaze */
	, SpriteState_t580_MethodInfos/* methods */
	, SpriteState_t580_PropertyInfos/* properties */
	, NULL/* events */
	, &SpriteState_t580_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SpriteState_t580_0_0_0/* byval_arg */
	, &SpriteState_t580_1_0_0/* this_arg */
	, &SpriteState_t580_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpriteState_t580)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SpriteState_t580)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntry.h"
// Metadata Definition UnityEngine.UI.StencilMaterial/MatEntry
extern TypeInfo MatEntry_t586_il2cpp_TypeInfo;
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntryMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
extern const MethodInfo MatEntry__ctor_m2745_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MatEntry__ctor_m2745/* method */
	, &MatEntry_t586_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MatEntry_t586_MethodInfos[] =
{
	&MatEntry__ctor_m2745_MethodInfo,
	NULL
};
static const Il2CppMethodReference MatEntry_t586_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool MatEntry_t586_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType MatEntry_t586_0_0_0;
extern const Il2CppType MatEntry_t586_1_0_0;
extern TypeInfo StencilMaterial_t588_il2cpp_TypeInfo;
extern const Il2CppType StencilMaterial_t588_0_0_0;
struct MatEntry_t586;
const Il2CppTypeDefinitionMetadata MatEntry_t586_DefinitionMetadata = 
{
	&StencilMaterial_t588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MatEntry_t586_VTable/* vtableMethods */
	, MatEntry_t586_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 491/* fieldStart */

};
TypeInfo MatEntry_t586_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatEntry"/* name */
	, ""/* namespaze */
	, MatEntry_t586_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MatEntry_t586_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatEntry_t586_0_0_0/* byval_arg */
	, &MatEntry_t586_1_0_0/* this_arg */
	, &MatEntry_t586_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatEntry_t586)/* instance_size */
	, sizeof (MatEntry_t586)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial.h"
// Metadata Definition UnityEngine.UI.StencilMaterial
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterialMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial::.cctor()
extern const MethodInfo StencilMaterial__cctor_m2746_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StencilMaterial__cctor_m2746/* method */
	, &StencilMaterial_t588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t55_0_0_0;
extern const Il2CppType Material_t55_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo StencilMaterial_t588_StencilMaterial_Add_m2747_ParameterInfos[] = 
{
	{"baseMat", 0, 134218285, 0, &Material_t55_0_0_0},
	{"stencilID", 1, 134218286, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.StencilMaterial::Add(UnityEngine.Material,System.Int32)
extern const MethodInfo StencilMaterial_Add_m2747_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&StencilMaterial_Add_m2747/* method */
	, &StencilMaterial_t588_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t253/* invoker_method */
	, StencilMaterial_t588_StencilMaterial_Add_m2747_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t55_0_0_0;
static const ParameterInfo StencilMaterial_t588_StencilMaterial_Remove_m2748_ParameterInfos[] = 
{
	{"customMat", 0, 134218287, 0, &Material_t55_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial::Remove(UnityEngine.Material)
extern const MethodInfo StencilMaterial_Remove_m2748_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&StencilMaterial_Remove_m2748/* method */
	, &StencilMaterial_t588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, StencilMaterial_t588_StencilMaterial_Remove_m2748_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StencilMaterial_t588_MethodInfos[] =
{
	&StencilMaterial__cctor_m2746_MethodInfo,
	&StencilMaterial_Add_m2747_MethodInfo,
	&StencilMaterial_Remove_m2748_MethodInfo,
	NULL
};
static const Il2CppType* StencilMaterial_t588_il2cpp_TypeInfo__nestedTypes[1] =
{
	&MatEntry_t586_0_0_0,
};
static const Il2CppMethodReference StencilMaterial_t588_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool StencilMaterial_t588_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType StencilMaterial_t588_1_0_0;
struct StencilMaterial_t588;
const Il2CppTypeDefinitionMetadata StencilMaterial_t588_DefinitionMetadata = 
{
	NULL/* declaringType */
	, StencilMaterial_t588_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StencilMaterial_t588_VTable/* vtableMethods */
	, StencilMaterial_t588_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 495/* fieldStart */

};
TypeInfo StencilMaterial_t588_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "StencilMaterial"/* name */
	, "UnityEngine.UI"/* namespaze */
	, StencilMaterial_t588_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StencilMaterial_t588_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StencilMaterial_t588_0_0_0/* byval_arg */
	, &StencilMaterial_t588_1_0_0/* this_arg */
	, &StencilMaterial_t588_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StencilMaterial_t588)/* instance_size */
	, sizeof (StencilMaterial_t588)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StencilMaterial_t588_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// Metadata Definition UnityEngine.UI.Text
extern TypeInfo Text_t316_il2cpp_TypeInfo;
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::.ctor()
extern const MethodInfo Text__ctor_m2749_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Text__ctor_m2749/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::.cctor()
extern const MethodInfo Text__cctor_m2750_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Text__cctor_m2750/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextGenerator_t557_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGenerator()
extern const MethodInfo Text_get_cachedTextGenerator_m2751_MethodInfo = 
{
	"get_cachedTextGenerator"/* name */
	, (methodPointerType)&Text_get_cachedTextGenerator_m2751/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerator_t557_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGeneratorForLayout()
extern const MethodInfo Text_get_cachedTextGeneratorForLayout_m2752_MethodInfo = 
{
	"get_cachedTextGeneratorForLayout"/* name */
	, (methodPointerType)&Text_get_cachedTextGeneratorForLayout_m2752/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerator_t557_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.Text::get_defaultMaterial()
extern const MethodInfo Text_get_defaultMaterial_m2753_MethodInfo = 
{
	"get_defaultMaterial"/* name */
	, (methodPointerType)&Text_get_defaultMaterial_m2753/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture_t86_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Texture UnityEngine.UI.Text::get_mainTexture()
extern const MethodInfo Text_get_mainTexture_m2754_MethodInfo = 
{
	"get_mainTexture"/* name */
	, (methodPointerType)&Text_get_mainTexture_m2754/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Texture_t86_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::FontTextureChanged()
extern const MethodInfo Text_FontTextureChanged_m2755_MethodInfo = 
{
	"FontTextureChanged"/* name */
	, (methodPointerType)&Text_FontTextureChanged_m2755/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Font_t517_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Font UnityEngine.UI.Text::get_font()
extern const MethodInfo Text_get_font_m2756_MethodInfo = 
{
	"get_font"/* name */
	, (methodPointerType)&Text_get_font_m2756/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Font_t517_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Font_t517_0_0_0;
static const ParameterInfo Text_t316_Text_set_font_m2757_ParameterInfos[] = 
{
	{"value", 0, 134218288, 0, &Font_t517_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_font(UnityEngine.Font)
extern const MethodInfo Text_set_font_m2757_MethodInfo = 
{
	"set_font"/* name */
	, (methodPointerType)&Text_set_font_m2757/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Text_t316_Text_set_font_m2757_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.UI.Text::get_text()
extern const MethodInfo Text_get_text_m2758_MethodInfo = 
{
	"get_text"/* name */
	, (methodPointerType)&Text_get_text_m2758/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Text_t316_Text_set_text_m2759_ParameterInfos[] = 
{
	{"value", 0, 134218289, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_text(System.String)
extern const MethodInfo Text_set_text_m2759_MethodInfo = 
{
	"set_text"/* name */
	, (methodPointerType)&Text_set_text_m2759/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Text_t316_Text_set_text_m2759_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 48/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Text::get_supportRichText()
extern const MethodInfo Text_get_supportRichText_m2760_MethodInfo = 
{
	"get_supportRichText"/* name */
	, (methodPointerType)&Text_get_supportRichText_m2760/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Text_t316_Text_set_supportRichText_m2761_ParameterInfos[] = 
{
	{"value", 0, 134218290, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_supportRichText(System.Boolean)
extern const MethodInfo Text_set_supportRichText_m2761_MethodInfo = 
{
	"set_supportRichText"/* name */
	, (methodPointerType)&Text_set_supportRichText_m2761/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Text_t316_Text_set_supportRichText_m2761_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Text::get_resizeTextForBestFit()
extern const MethodInfo Text_get_resizeTextForBestFit_m2762_MethodInfo = 
{
	"get_resizeTextForBestFit"/* name */
	, (methodPointerType)&Text_get_resizeTextForBestFit_m2762/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Text_t316_Text_set_resizeTextForBestFit_m2763_ParameterInfos[] = 
{
	{"value", 0, 134218291, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextForBestFit(System.Boolean)
extern const MethodInfo Text_set_resizeTextForBestFit_m2763_MethodInfo = 
{
	"set_resizeTextForBestFit"/* name */
	, (methodPointerType)&Text_set_resizeTextForBestFit_m2763/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Text_t316_Text_set_resizeTextForBestFit_m2763_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_resizeTextMinSize()
extern const MethodInfo Text_get_resizeTextMinSize_m2764_MethodInfo = 
{
	"get_resizeTextMinSize"/* name */
	, (methodPointerType)&Text_get_resizeTextMinSize_m2764/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Text_t316_Text_set_resizeTextMinSize_m2765_ParameterInfos[] = 
{
	{"value", 0, 134218292, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextMinSize(System.Int32)
extern const MethodInfo Text_set_resizeTextMinSize_m2765_MethodInfo = 
{
	"set_resizeTextMinSize"/* name */
	, (methodPointerType)&Text_set_resizeTextMinSize_m2765/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Text_t316_Text_set_resizeTextMinSize_m2765_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_resizeTextMaxSize()
extern const MethodInfo Text_get_resizeTextMaxSize_m2766_MethodInfo = 
{
	"get_resizeTextMaxSize"/* name */
	, (methodPointerType)&Text_get_resizeTextMaxSize_m2766/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 955/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Text_t316_Text_set_resizeTextMaxSize_m2767_ParameterInfos[] = 
{
	{"value", 0, 134218293, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextMaxSize(System.Int32)
extern const MethodInfo Text_set_resizeTextMaxSize_m2767_MethodInfo = 
{
	"set_resizeTextMaxSize"/* name */
	, (methodPointerType)&Text_set_resizeTextMaxSize_m2767/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Text_t316_Text_set_resizeTextMaxSize_m2767_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 956/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t701_0_0_0;
extern void* RuntimeInvoker_TextAnchor_t701 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextAnchor UnityEngine.UI.Text::get_alignment()
extern const MethodInfo Text_get_alignment_m2768_MethodInfo = 
{
	"get_alignment"/* name */
	, (methodPointerType)&Text_get_alignment_m2768/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t701_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t701/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 957/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t701_0_0_0;
static const ParameterInfo Text_t316_Text_set_alignment_m2769_ParameterInfos[] = 
{
	{"value", 0, 134218294, 0, &TextAnchor_t701_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_alignment(UnityEngine.TextAnchor)
extern const MethodInfo Text_set_alignment_m2769_MethodInfo = 
{
	"set_alignment"/* name */
	, (methodPointerType)&Text_set_alignment_m2769/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Text_t316_Text_set_alignment_m2769_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 958/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_fontSize()
extern const MethodInfo Text_get_fontSize_m2770_MethodInfo = 
{
	"get_fontSize"/* name */
	, (methodPointerType)&Text_get_fontSize_m2770/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 959/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Text_t316_Text_set_fontSize_m2771_ParameterInfos[] = 
{
	{"value", 0, 134218295, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_fontSize(System.Int32)
extern const MethodInfo Text_set_fontSize_m2771_MethodInfo = 
{
	"set_fontSize"/* name */
	, (methodPointerType)&Text_set_fontSize_m2771/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Text_t316_Text_set_fontSize_m2771_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 960/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HorizontalWrapMode_t750_0_0_0;
extern void* RuntimeInvoker_HorizontalWrapMode_t750 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.HorizontalWrapMode UnityEngine.UI.Text::get_horizontalOverflow()
extern const MethodInfo Text_get_horizontalOverflow_m2772_MethodInfo = 
{
	"get_horizontalOverflow"/* name */
	, (methodPointerType)&Text_get_horizontalOverflow_m2772/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &HorizontalWrapMode_t750_0_0_0/* return_type */
	, RuntimeInvoker_HorizontalWrapMode_t750/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 961/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HorizontalWrapMode_t750_0_0_0;
static const ParameterInfo Text_t316_Text_set_horizontalOverflow_m2773_ParameterInfos[] = 
{
	{"value", 0, 134218296, 0, &HorizontalWrapMode_t750_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_horizontalOverflow(UnityEngine.HorizontalWrapMode)
extern const MethodInfo Text_set_horizontalOverflow_m2773_MethodInfo = 
{
	"set_horizontalOverflow"/* name */
	, (methodPointerType)&Text_set_horizontalOverflow_m2773/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Text_t316_Text_set_horizontalOverflow_m2773_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 962/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VerticalWrapMode_t751_0_0_0;
extern void* RuntimeInvoker_VerticalWrapMode_t751 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.VerticalWrapMode UnityEngine.UI.Text::get_verticalOverflow()
extern const MethodInfo Text_get_verticalOverflow_m2774_MethodInfo = 
{
	"get_verticalOverflow"/* name */
	, (methodPointerType)&Text_get_verticalOverflow_m2774/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &VerticalWrapMode_t751_0_0_0/* return_type */
	, RuntimeInvoker_VerticalWrapMode_t751/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 963/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VerticalWrapMode_t751_0_0_0;
static const ParameterInfo Text_t316_Text_set_verticalOverflow_m2775_ParameterInfos[] = 
{
	{"value", 0, 134218297, 0, &VerticalWrapMode_t751_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_verticalOverflow(UnityEngine.VerticalWrapMode)
extern const MethodInfo Text_set_verticalOverflow_m2775_MethodInfo = 
{
	"set_verticalOverflow"/* name */
	, (methodPointerType)&Text_set_verticalOverflow_m2775/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Text_t316_Text_set_verticalOverflow_m2775_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 964/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_lineSpacing()
extern const MethodInfo Text_get_lineSpacing_m2776_MethodInfo = 
{
	"get_lineSpacing"/* name */
	, (methodPointerType)&Text_get_lineSpacing_m2776/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 965/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo Text_t316_Text_set_lineSpacing_m2777_ParameterInfos[] = 
{
	{"value", 0, 134218298, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_lineSpacing(System.Single)
extern const MethodInfo Text_set_lineSpacing_m2777_MethodInfo = 
{
	"set_lineSpacing"/* name */
	, (methodPointerType)&Text_set_lineSpacing_m2777/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, Text_t316_Text_set_lineSpacing_m2777_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 966/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FontStyle_t749_0_0_0;
extern void* RuntimeInvoker_FontStyle_t749 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.FontStyle UnityEngine.UI.Text::get_fontStyle()
extern const MethodInfo Text_get_fontStyle_m2778_MethodInfo = 
{
	"get_fontStyle"/* name */
	, (methodPointerType)&Text_get_fontStyle_m2778/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &FontStyle_t749_0_0_0/* return_type */
	, RuntimeInvoker_FontStyle_t749/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 967/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FontStyle_t749_0_0_0;
static const ParameterInfo Text_t316_Text_set_fontStyle_m2779_ParameterInfos[] = 
{
	{"value", 0, 134218299, 0, &FontStyle_t749_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_fontStyle(UnityEngine.FontStyle)
extern const MethodInfo Text_set_fontStyle_m2779_MethodInfo = 
{
	"set_fontStyle"/* name */
	, (methodPointerType)&Text_set_fontStyle_m2779/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Text_t316_Text_set_fontStyle_m2779_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 968/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_pixelsPerUnit()
extern const MethodInfo Text_get_pixelsPerUnit_m2780_MethodInfo = 
{
	"get_pixelsPerUnit"/* name */
	, (methodPointerType)&Text_get_pixelsPerUnit_m2780/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 969/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnEnable()
extern const MethodInfo Text_OnEnable_m2781_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Text_OnEnable_m2781/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 970/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnDisable()
extern const MethodInfo Text_OnDisable_m2782_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Text_OnDisable_m2782/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 971/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::UpdateGeometry()
extern const MethodInfo Text_UpdateGeometry_m2783_MethodInfo = 
{
	"UpdateGeometry"/* name */
	, (methodPointerType)&Text_UpdateGeometry_m2783/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 972/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo Text_t316_Text_GetGenerationSettings_m2784_ParameterInfos[] = 
{
	{"extents", 0, 134218300, 0, &Vector2_t6_0_0_0},
};
extern const Il2CppType TextGenerationSettings_t649_0_0_0;
extern void* RuntimeInvoker_TextGenerationSettings_t649_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerationSettings UnityEngine.UI.Text::GetGenerationSettings(UnityEngine.Vector2)
extern const MethodInfo Text_GetGenerationSettings_m2784_MethodInfo = 
{
	"GetGenerationSettings"/* name */
	, (methodPointerType)&Text_GetGenerationSettings_m2784/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerationSettings_t649_0_0_0/* return_type */
	, RuntimeInvoker_TextGenerationSettings_t649_Vector2_t6/* invoker_method */
	, Text_t316_Text_GetGenerationSettings_m2784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 973/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t701_0_0_0;
static const ParameterInfo Text_t316_Text_GetTextAnchorPivot_m2785_ParameterInfos[] = 
{
	{"anchor", 0, 134218301, 0, &TextAnchor_t701_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t6_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.Text::GetTextAnchorPivot(UnityEngine.TextAnchor)
extern const MethodInfo Text_GetTextAnchorPivot_m2785_MethodInfo = 
{
	"GetTextAnchorPivot"/* name */
	, (methodPointerType)&Text_GetTextAnchorPivot_m2785/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6_Int32_t253/* invoker_method */
	, Text_t316_Text_GetTextAnchorPivot_m2785_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 974/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t558_0_0_0;
extern const Il2CppType List_1_t558_0_0_0;
static const ParameterInfo Text_t316_Text_OnFillVBO_m2786_ParameterInfos[] = 
{
	{"vbo", 0, 134218302, 0, &List_1_t558_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Text_OnFillVBO_m2786_MethodInfo = 
{
	"OnFillVBO"/* name */
	, (methodPointerType)&Text_OnFillVBO_m2786/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Text_t316_Text_OnFillVBO_m2786_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 975/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::CalculateLayoutInputHorizontal()
extern const MethodInfo Text_CalculateLayoutInputHorizontal_m2787_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&Text_CalculateLayoutInputHorizontal_m2787/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 49/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 976/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::CalculateLayoutInputVertical()
extern const MethodInfo Text_CalculateLayoutInputVertical_m2788_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&Text_CalculateLayoutInputVertical_m2788/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 50/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 977/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_minWidth()
extern const MethodInfo Text_get_minWidth_m2789_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&Text_get_minWidth_m2789/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 51/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 978/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_preferredWidth()
extern const MethodInfo Text_get_preferredWidth_m2790_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&Text_get_preferredWidth_m2790/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 52/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 979/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_flexibleWidth()
extern const MethodInfo Text_get_flexibleWidth_m2791_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&Text_get_flexibleWidth_m2791/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 53/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 980/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_minHeight()
extern const MethodInfo Text_get_minHeight_m2792_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&Text_get_minHeight_m2792/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 54/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 981/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_preferredHeight()
extern const MethodInfo Text_get_preferredHeight_m2793_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&Text_get_preferredHeight_m2793/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 55/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 982/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_flexibleHeight()
extern const MethodInfo Text_get_flexibleHeight_m2794_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&Text_get_flexibleHeight_m2794/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 56/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 983/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_layoutPriority()
extern const MethodInfo Text_get_layoutPriority_m2795_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&Text_get_layoutPriority_m2795/* method */
	, &Text_t316_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 57/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 984/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Text_t316_MethodInfos[] =
{
	&Text__ctor_m2749_MethodInfo,
	&Text__cctor_m2750_MethodInfo,
	&Text_get_cachedTextGenerator_m2751_MethodInfo,
	&Text_get_cachedTextGeneratorForLayout_m2752_MethodInfo,
	&Text_get_defaultMaterial_m2753_MethodInfo,
	&Text_get_mainTexture_m2754_MethodInfo,
	&Text_FontTextureChanged_m2755_MethodInfo,
	&Text_get_font_m2756_MethodInfo,
	&Text_set_font_m2757_MethodInfo,
	&Text_get_text_m2758_MethodInfo,
	&Text_set_text_m2759_MethodInfo,
	&Text_get_supportRichText_m2760_MethodInfo,
	&Text_set_supportRichText_m2761_MethodInfo,
	&Text_get_resizeTextForBestFit_m2762_MethodInfo,
	&Text_set_resizeTextForBestFit_m2763_MethodInfo,
	&Text_get_resizeTextMinSize_m2764_MethodInfo,
	&Text_set_resizeTextMinSize_m2765_MethodInfo,
	&Text_get_resizeTextMaxSize_m2766_MethodInfo,
	&Text_set_resizeTextMaxSize_m2767_MethodInfo,
	&Text_get_alignment_m2768_MethodInfo,
	&Text_set_alignment_m2769_MethodInfo,
	&Text_get_fontSize_m2770_MethodInfo,
	&Text_set_fontSize_m2771_MethodInfo,
	&Text_get_horizontalOverflow_m2772_MethodInfo,
	&Text_set_horizontalOverflow_m2773_MethodInfo,
	&Text_get_verticalOverflow_m2774_MethodInfo,
	&Text_set_verticalOverflow_m2775_MethodInfo,
	&Text_get_lineSpacing_m2776_MethodInfo,
	&Text_set_lineSpacing_m2777_MethodInfo,
	&Text_get_fontStyle_m2778_MethodInfo,
	&Text_set_fontStyle_m2779_MethodInfo,
	&Text_get_pixelsPerUnit_m2780_MethodInfo,
	&Text_OnEnable_m2781_MethodInfo,
	&Text_OnDisable_m2782_MethodInfo,
	&Text_UpdateGeometry_m2783_MethodInfo,
	&Text_GetGenerationSettings_m2784_MethodInfo,
	&Text_GetTextAnchorPivot_m2785_MethodInfo,
	&Text_OnFillVBO_m2786_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m2787_MethodInfo,
	&Text_CalculateLayoutInputVertical_m2788_MethodInfo,
	&Text_get_minWidth_m2789_MethodInfo,
	&Text_get_preferredWidth_m2790_MethodInfo,
	&Text_get_flexibleWidth_m2791_MethodInfo,
	&Text_get_minHeight_m2792_MethodInfo,
	&Text_get_preferredHeight_m2793_MethodInfo,
	&Text_get_flexibleHeight_m2794_MethodInfo,
	&Text_get_layoutPriority_m2795_MethodInfo,
	NULL
};
extern const MethodInfo Text_get_cachedTextGenerator_m2751_MethodInfo;
static const PropertyInfo Text_t316____cachedTextGenerator_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "cachedTextGenerator"/* name */
	, &Text_get_cachedTextGenerator_m2751_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_cachedTextGeneratorForLayout_m2752_MethodInfo;
static const PropertyInfo Text_t316____cachedTextGeneratorForLayout_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "cachedTextGeneratorForLayout"/* name */
	, &Text_get_cachedTextGeneratorForLayout_m2752_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_defaultMaterial_m2753_MethodInfo;
static const PropertyInfo Text_t316____defaultMaterial_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "defaultMaterial"/* name */
	, &Text_get_defaultMaterial_m2753_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_mainTexture_m2754_MethodInfo;
static const PropertyInfo Text_t316____mainTexture_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "mainTexture"/* name */
	, &Text_get_mainTexture_m2754_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_font_m2756_MethodInfo;
extern const MethodInfo Text_set_font_m2757_MethodInfo;
static const PropertyInfo Text_t316____font_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "font"/* name */
	, &Text_get_font_m2756_MethodInfo/* get */
	, &Text_set_font_m2757_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_text_m2758_MethodInfo;
extern const MethodInfo Text_set_text_m2759_MethodInfo;
static const PropertyInfo Text_t316____text_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "text"/* name */
	, &Text_get_text_m2758_MethodInfo/* get */
	, &Text_set_text_m2759_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_supportRichText_m2760_MethodInfo;
extern const MethodInfo Text_set_supportRichText_m2761_MethodInfo;
static const PropertyInfo Text_t316____supportRichText_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "supportRichText"/* name */
	, &Text_get_supportRichText_m2760_MethodInfo/* get */
	, &Text_set_supportRichText_m2761_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextForBestFit_m2762_MethodInfo;
extern const MethodInfo Text_set_resizeTextForBestFit_m2763_MethodInfo;
static const PropertyInfo Text_t316____resizeTextForBestFit_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "resizeTextForBestFit"/* name */
	, &Text_get_resizeTextForBestFit_m2762_MethodInfo/* get */
	, &Text_set_resizeTextForBestFit_m2763_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextMinSize_m2764_MethodInfo;
extern const MethodInfo Text_set_resizeTextMinSize_m2765_MethodInfo;
static const PropertyInfo Text_t316____resizeTextMinSize_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "resizeTextMinSize"/* name */
	, &Text_get_resizeTextMinSize_m2764_MethodInfo/* get */
	, &Text_set_resizeTextMinSize_m2765_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextMaxSize_m2766_MethodInfo;
extern const MethodInfo Text_set_resizeTextMaxSize_m2767_MethodInfo;
static const PropertyInfo Text_t316____resizeTextMaxSize_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "resizeTextMaxSize"/* name */
	, &Text_get_resizeTextMaxSize_m2766_MethodInfo/* get */
	, &Text_set_resizeTextMaxSize_m2767_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_alignment_m2768_MethodInfo;
extern const MethodInfo Text_set_alignment_m2769_MethodInfo;
static const PropertyInfo Text_t316____alignment_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "alignment"/* name */
	, &Text_get_alignment_m2768_MethodInfo/* get */
	, &Text_set_alignment_m2769_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_fontSize_m2770_MethodInfo;
extern const MethodInfo Text_set_fontSize_m2771_MethodInfo;
static const PropertyInfo Text_t316____fontSize_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "fontSize"/* name */
	, &Text_get_fontSize_m2770_MethodInfo/* get */
	, &Text_set_fontSize_m2771_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_horizontalOverflow_m2772_MethodInfo;
extern const MethodInfo Text_set_horizontalOverflow_m2773_MethodInfo;
static const PropertyInfo Text_t316____horizontalOverflow_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "horizontalOverflow"/* name */
	, &Text_get_horizontalOverflow_m2772_MethodInfo/* get */
	, &Text_set_horizontalOverflow_m2773_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_verticalOverflow_m2774_MethodInfo;
extern const MethodInfo Text_set_verticalOverflow_m2775_MethodInfo;
static const PropertyInfo Text_t316____verticalOverflow_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "verticalOverflow"/* name */
	, &Text_get_verticalOverflow_m2774_MethodInfo/* get */
	, &Text_set_verticalOverflow_m2775_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_lineSpacing_m2776_MethodInfo;
extern const MethodInfo Text_set_lineSpacing_m2777_MethodInfo;
static const PropertyInfo Text_t316____lineSpacing_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "lineSpacing"/* name */
	, &Text_get_lineSpacing_m2776_MethodInfo/* get */
	, &Text_set_lineSpacing_m2777_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_fontStyle_m2778_MethodInfo;
extern const MethodInfo Text_set_fontStyle_m2779_MethodInfo;
static const PropertyInfo Text_t316____fontStyle_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "fontStyle"/* name */
	, &Text_get_fontStyle_m2778_MethodInfo/* get */
	, &Text_set_fontStyle_m2779_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_pixelsPerUnit_m2780_MethodInfo;
static const PropertyInfo Text_t316____pixelsPerUnit_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "pixelsPerUnit"/* name */
	, &Text_get_pixelsPerUnit_m2780_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_minWidth_m2789_MethodInfo;
static const PropertyInfo Text_t316____minWidth_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &Text_get_minWidth_m2789_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_preferredWidth_m2790_MethodInfo;
static const PropertyInfo Text_t316____preferredWidth_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &Text_get_preferredWidth_m2790_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_flexibleWidth_m2791_MethodInfo;
static const PropertyInfo Text_t316____flexibleWidth_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &Text_get_flexibleWidth_m2791_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_minHeight_m2792_MethodInfo;
static const PropertyInfo Text_t316____minHeight_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &Text_get_minHeight_m2792_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_preferredHeight_m2793_MethodInfo;
static const PropertyInfo Text_t316____preferredHeight_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &Text_get_preferredHeight_m2793_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_flexibleHeight_m2794_MethodInfo;
static const PropertyInfo Text_t316____flexibleHeight_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &Text_get_flexibleHeight_m2794_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_layoutPriority_m2795_MethodInfo;
static const PropertyInfo Text_t316____layoutPriority_PropertyInfo = 
{
	&Text_t316_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &Text_get_layoutPriority_m2795_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Text_t316_PropertyInfos[] =
{
	&Text_t316____cachedTextGenerator_PropertyInfo,
	&Text_t316____cachedTextGeneratorForLayout_PropertyInfo,
	&Text_t316____defaultMaterial_PropertyInfo,
	&Text_t316____mainTexture_PropertyInfo,
	&Text_t316____font_PropertyInfo,
	&Text_t316____text_PropertyInfo,
	&Text_t316____supportRichText_PropertyInfo,
	&Text_t316____resizeTextForBestFit_PropertyInfo,
	&Text_t316____resizeTextMinSize_PropertyInfo,
	&Text_t316____resizeTextMaxSize_PropertyInfo,
	&Text_t316____alignment_PropertyInfo,
	&Text_t316____fontSize_PropertyInfo,
	&Text_t316____horizontalOverflow_PropertyInfo,
	&Text_t316____verticalOverflow_PropertyInfo,
	&Text_t316____lineSpacing_PropertyInfo,
	&Text_t316____fontStyle_PropertyInfo,
	&Text_t316____pixelsPerUnit_PropertyInfo,
	&Text_t316____minWidth_PropertyInfo,
	&Text_t316____preferredWidth_PropertyInfo,
	&Text_t316____flexibleWidth_PropertyInfo,
	&Text_t316____minHeight_PropertyInfo,
	&Text_t316____preferredHeight_PropertyInfo,
	&Text_t316____flexibleHeight_PropertyInfo,
	&Text_t316____layoutPriority_PropertyInfo,
	NULL
};
extern const MethodInfo Text_OnEnable_m2781_MethodInfo;
extern const MethodInfo Text_OnDisable_m2782_MethodInfo;
extern const MethodInfo Graphic_OnRectTransformDimensionsChange_m2249_MethodInfo;
extern const MethodInfo Graphic_OnBeforeTransformParentChanged_m2250_MethodInfo;
extern const MethodInfo MaskableGraphic_OnTransformParentChanged_m2506_MethodInfo;
extern const MethodInfo Graphic_OnDidApplyAnimationProperties_m2270_MethodInfo;
extern const MethodInfo Graphic_OnCanvasHierarchyChanged_m2265_MethodInfo;
extern const MethodInfo Graphic_Rebuild_m2266_MethodInfo;
extern const MethodInfo Graphic_UnityEngine_UI_ICanvasElement_get_transform_m2288_MethodInfo;
extern const MethodInfo Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m2287_MethodInfo;
extern const MethodInfo Graphic_SetAllDirty_m2245_MethodInfo;
extern const MethodInfo Graphic_SetLayoutDirty_m2246_MethodInfo;
extern const MethodInfo Graphic_SetVerticesDirty_m2247_MethodInfo;
extern const MethodInfo MaskableGraphic_SetMaterialDirty_m2509_MethodInfo;
extern const MethodInfo MaskableGraphic_get_material_m2501_MethodInfo;
extern const MethodInfo MaskableGraphic_set_material_m2502_MethodInfo;
extern const MethodInfo Graphic_get_materialForRendering_m2260_MethodInfo;
extern const MethodInfo Text_UpdateGeometry_m2783_MethodInfo;
extern const MethodInfo Graphic_UpdateMaterial_m2268_MethodInfo;
extern const MethodInfo Text_OnFillVBO_m2786_MethodInfo;
extern const MethodInfo Graphic_SetNativeSize_m2271_MethodInfo;
extern const MethodInfo Graphic_Raycast_m2272_MethodInfo;
extern const MethodInfo MaskableGraphic_ParentMaskStateChanged_m2507_MethodInfo;
extern const MethodInfo Text_CalculateLayoutInputHorizontal_m2787_MethodInfo;
extern const MethodInfo Text_CalculateLayoutInputVertical_m2788_MethodInfo;
static const Il2CppMethodReference Text_t316_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&Text_OnEnable_m2781_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&Text_OnDisable_m2782_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&Graphic_OnRectTransformDimensionsChange_m2249_MethodInfo,
	&Graphic_OnBeforeTransformParentChanged_m2250_MethodInfo,
	&MaskableGraphic_OnTransformParentChanged_m2506_MethodInfo,
	&Graphic_OnDidApplyAnimationProperties_m2270_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&Graphic_OnCanvasHierarchyChanged_m2265_MethodInfo,
	&Graphic_Rebuild_m2266_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_get_transform_m2288_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m2287_MethodInfo,
	&Graphic_SetAllDirty_m2245_MethodInfo,
	&Graphic_SetLayoutDirty_m2246_MethodInfo,
	&Graphic_SetVerticesDirty_m2247_MethodInfo,
	&MaskableGraphic_SetMaterialDirty_m2509_MethodInfo,
	&Text_get_defaultMaterial_m2753_MethodInfo,
	&MaskableGraphic_get_material_m2501_MethodInfo,
	&MaskableGraphic_set_material_m2502_MethodInfo,
	&Graphic_get_materialForRendering_m2260_MethodInfo,
	&Text_get_mainTexture_m2754_MethodInfo,
	&Graphic_Rebuild_m2266_MethodInfo,
	&Text_UpdateGeometry_m2783_MethodInfo,
	&Graphic_UpdateMaterial_m2268_MethodInfo,
	&Text_OnFillVBO_m2786_MethodInfo,
	&Graphic_SetNativeSize_m2271_MethodInfo,
	&Graphic_Raycast_m2272_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m2287_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_get_transform_m2288_MethodInfo,
	&MaskableGraphic_ParentMaskStateChanged_m2507_MethodInfo,
	&MaskableGraphic_ParentMaskStateChanged_m2507_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m2787_MethodInfo,
	&Text_CalculateLayoutInputVertical_m2788_MethodInfo,
	&Text_get_minWidth_m2789_MethodInfo,
	&Text_get_preferredWidth_m2790_MethodInfo,
	&Text_get_flexibleWidth_m2791_MethodInfo,
	&Text_get_minHeight_m2792_MethodInfo,
	&Text_get_preferredHeight_m2793_MethodInfo,
	&Text_get_flexibleHeight_m2794_MethodInfo,
	&Text_get_layoutPriority_m2795_MethodInfo,
	&Text_get_text_m2758_MethodInfo,
	&Text_set_text_m2759_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m2787_MethodInfo,
	&Text_CalculateLayoutInputVertical_m2788_MethodInfo,
	&Text_get_minWidth_m2789_MethodInfo,
	&Text_get_preferredWidth_m2790_MethodInfo,
	&Text_get_flexibleWidth_m2791_MethodInfo,
	&Text_get_minHeight_m2792_MethodInfo,
	&Text_get_preferredHeight_m2793_MethodInfo,
	&Text_get_flexibleHeight_m2794_MethodInfo,
	&Text_get_layoutPriority_m2795_MethodInfo,
};
static bool Text_t316_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutElement_t652_0_0_0;
static const Il2CppType* Text_t316_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t652_0_0_0,
};
extern const Il2CppType IMaskable_t707_0_0_0;
static Il2CppInterfaceOffsetPair Text_t316_InterfacesOffsets[] = 
{
	{ &IMaskable_t707_0_0_0, 36},
	{ &ICanvasElement_t646_0_0_0, 16},
	{ &ILayoutElement_t652_0_0_0, 38},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Text_t316_0_0_0;
extern const Il2CppType Text_t316_1_0_0;
extern const Il2CppType MaskableGraphic_t537_0_0_0;
struct Text_t316;
const Il2CppTypeDefinitionMetadata Text_t316_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Text_t316_InterfacesTypeInfos/* implementedInterfaces */
	, Text_t316_InterfacesOffsets/* interfaceOffsets */
	, &MaskableGraphic_t537_0_0_0/* parent */
	, Text_t316_VTable/* vtableMethods */
	, Text_t316_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 496/* fieldStart */

};
TypeInfo Text_t316_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Text"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Text_t316_MethodInfos/* methods */
	, Text_t316_PropertyInfos/* properties */
	, NULL/* events */
	, &Text_t316_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 255/* custom_attributes_cache */
	, &Text_t316_0_0_0/* byval_arg */
	, &Text_t316_1_0_0/* this_arg */
	, &Text_t316_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Text_t316)/* instance_size */
	, sizeof (Text_t316)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Text_t316_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 47/* method_count */
	, 24/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 58/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransition.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleTransition
extern TypeInfo ToggleTransition_t589_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransitionMethodDeclarations.h"
static const MethodInfo* ToggleTransition_t589_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ToggleTransition_t589_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ToggleTransition_t589_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ToggleTransition_t589_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleTransition_t589_0_0_0;
extern const Il2CppType ToggleTransition_t589_1_0_0;
extern TypeInfo Toggle_t312_il2cpp_TypeInfo;
extern const Il2CppType Toggle_t312_0_0_0;
const Il2CppTypeDefinitionMetadata ToggleTransition_t589_DefinitionMetadata = 
{
	&Toggle_t312_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleTransition_t589_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ToggleTransition_t589_VTable/* vtableMethods */
	, ToggleTransition_t589_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 502/* fieldStart */

};
TypeInfo ToggleTransition_t589_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleTransition"/* name */
	, ""/* namespaze */
	, ToggleTransition_t589_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleTransition_t589_0_0_0/* byval_arg */
	, &ToggleTransition_t589_1_0_0/* this_arg */
	, &ToggleTransition_t589_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleTransition_t589)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ToggleTransition_t589)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleEvent
extern TypeInfo ToggleEvent_t590_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle/ToggleEvent::.ctor()
extern const MethodInfo ToggleEvent__ctor_m2796_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ToggleEvent__ctor_m2796/* method */
	, &ToggleEvent_t590_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1003/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ToggleEvent_t590_MethodInfos[] =
{
	&ToggleEvent__ctor_m2796_MethodInfo,
	NULL
};
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m3611_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m3612_GenericMethod;
static const Il2CppMethodReference ToggleEvent_t590_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&UnityEventBase_ToString_m3581_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3582_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3583_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m3611_GenericMethod,
	&UnityEvent_1_GetDelegate_m3612_GenericMethod,
};
static bool ToggleEvent_t590_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
static Il2CppInterfaceOffsetPair ToggleEvent_t590_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t731_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleEvent_t590_0_0_0;
extern const Il2CppType ToggleEvent_t590_1_0_0;
extern const Il2CppType UnityEvent_1_t591_0_0_0;
struct ToggleEvent_t590;
const Il2CppTypeDefinitionMetadata ToggleEvent_t590_DefinitionMetadata = 
{
	&Toggle_t312_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleEvent_t590_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t591_0_0_0/* parent */
	, ToggleEvent_t590_VTable/* vtableMethods */
	, ToggleEvent_t590_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ToggleEvent_t590_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleEvent"/* name */
	, ""/* namespaze */
	, ToggleEvent_t590_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ToggleEvent_t590_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleEvent_t590_0_0_0/* byval_arg */
	, &ToggleEvent_t590_1_0_0/* this_arg */
	, &ToggleEvent_t590_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleEvent_t590)/* instance_size */
	, sizeof (ToggleEvent_t590)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
// Metadata Definition UnityEngine.UI.Toggle
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::.ctor()
extern const MethodInfo Toggle__ctor_m2797_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Toggle__ctor_m2797/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 985/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t592_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::get_group()
extern const MethodInfo Toggle_get_group_m2798_MethodInfo = 
{
	"get_group"/* name */
	, (methodPointerType)&Toggle_get_group_m2798/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &ToggleGroup_t592_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 986/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t592_0_0_0;
static const ParameterInfo Toggle_t312_Toggle_set_group_m2799_ParameterInfos[] = 
{
	{"value", 0, 134218303, 0, &ToggleGroup_t592_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::set_group(UnityEngine.UI.ToggleGroup)
extern const MethodInfo Toggle_set_group_m2799_MethodInfo = 
{
	"set_group"/* name */
	, (methodPointerType)&Toggle_set_group_m2799/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Toggle_t312_Toggle_set_group_m2799_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 987/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t511_0_0_0;
static const ParameterInfo Toggle_t312_Toggle_Rebuild_m2800_ParameterInfos[] = 
{
	{"executing", 0, 134218304, 0, &CanvasUpdate_t511_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo Toggle_Rebuild_m2800_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&Toggle_Rebuild_m2800/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Toggle_t312_Toggle_Rebuild_m2800_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 988/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnEnable()
extern const MethodInfo Toggle_OnEnable_m2801_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Toggle_OnEnable_m2801/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 989/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnDisable()
extern const MethodInfo Toggle_OnDisable_m2802_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Toggle_OnDisable_m2802/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 990/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t592_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Toggle_t312_Toggle_SetToggleGroup_m2803_ParameterInfos[] = 
{
	{"newGroup", 0, 134218305, 0, &ToggleGroup_t592_0_0_0},
	{"setMemberValue", 1, 134218306, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::SetToggleGroup(UnityEngine.UI.ToggleGroup,System.Boolean)
extern const MethodInfo Toggle_SetToggleGroup_m2803_MethodInfo = 
{
	"SetToggleGroup"/* name */
	, (methodPointerType)&Toggle_SetToggleGroup_m2803/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, Toggle_t312_Toggle_SetToggleGroup_m2803_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 991/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Toggle::get_isOn()
extern const MethodInfo Toggle_get_isOn_m1719_MethodInfo = 
{
	"get_isOn"/* name */
	, (methodPointerType)&Toggle_get_isOn_m1719/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 992/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Toggle_t312_Toggle_set_isOn_m2804_ParameterInfos[] = 
{
	{"value", 0, 134218307, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::set_isOn(System.Boolean)
extern const MethodInfo Toggle_set_isOn_m2804_MethodInfo = 
{
	"set_isOn"/* name */
	, (methodPointerType)&Toggle_set_isOn_m2804/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Toggle_t312_Toggle_set_isOn_m2804_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 993/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Toggle_t312_Toggle_Set_m2805_ParameterInfos[] = 
{
	{"value", 0, 134218308, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean)
extern const MethodInfo Toggle_Set_m2805_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Toggle_Set_m2805/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Toggle_t312_Toggle_Set_m2805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 994/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Toggle_t312_Toggle_Set_m2806_ParameterInfos[] = 
{
	{"value", 0, 134218309, 0, &Boolean_t273_0_0_0},
	{"sendCallback", 1, 134218310, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean,System.Boolean)
extern const MethodInfo Toggle_Set_m2806_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Toggle_Set_m2806/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274_SByte_t274/* invoker_method */
	, Toggle_t312_Toggle_Set_m2806_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 995/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Toggle_t312_Toggle_PlayEffect_m2807_ParameterInfos[] = 
{
	{"instant", 0, 134218311, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::PlayEffect(System.Boolean)
extern const MethodInfo Toggle_PlayEffect_m2807_MethodInfo = 
{
	"PlayEffect"/* name */
	, (methodPointerType)&Toggle_PlayEffect_m2807/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Toggle_t312_Toggle_PlayEffect_m2807_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 996/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Start()
extern const MethodInfo Toggle_Start_m2808_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Toggle_Start_m2808/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 997/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::InternalToggle()
extern const MethodInfo Toggle_InternalToggle_m2809_MethodInfo = 
{
	"InternalToggle"/* name */
	, (methodPointerType)&Toggle_InternalToggle_m2809/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 998/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t215_0_0_0;
static const ParameterInfo Toggle_t312_Toggle_OnPointerClick_m2810_ParameterInfos[] = 
{
	{"eventData", 0, 134218312, 0, &PointerEventData_t215_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Toggle_OnPointerClick_m2810_MethodInfo = 
{
	"OnPointerClick"/* name */
	, (methodPointerType)&Toggle_OnPointerClick_m2810/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Toggle_t312_Toggle_OnPointerClick_m2810_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 44/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 999/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t453_0_0_0;
static const ParameterInfo Toggle_t312_Toggle_OnSubmit_m2811_ParameterInfos[] = 
{
	{"eventData", 0, 134218313, 0, &BaseEventData_t453_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Toggle_OnSubmit_m2811_MethodInfo = 
{
	"OnSubmit"/* name */
	, (methodPointerType)&Toggle_OnSubmit_m2811/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Toggle_t312_Toggle_OnSubmit_m2811_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1000/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m2812_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m2812/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 46/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1001/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_get_transform_m2813_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m2813/* method */
	, &Toggle_t312_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t1_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1002/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Toggle_t312_MethodInfos[] =
{
	&Toggle__ctor_m2797_MethodInfo,
	&Toggle_get_group_m2798_MethodInfo,
	&Toggle_set_group_m2799_MethodInfo,
	&Toggle_Rebuild_m2800_MethodInfo,
	&Toggle_OnEnable_m2801_MethodInfo,
	&Toggle_OnDisable_m2802_MethodInfo,
	&Toggle_SetToggleGroup_m2803_MethodInfo,
	&Toggle_get_isOn_m1719_MethodInfo,
	&Toggle_set_isOn_m2804_MethodInfo,
	&Toggle_Set_m2805_MethodInfo,
	&Toggle_Set_m2806_MethodInfo,
	&Toggle_PlayEffect_m2807_MethodInfo,
	&Toggle_Start_m2808_MethodInfo,
	&Toggle_InternalToggle_m2809_MethodInfo,
	&Toggle_OnPointerClick_m2810_MethodInfo,
	&Toggle_OnSubmit_m2811_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m2812_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m2813_MethodInfo,
	NULL
};
extern const MethodInfo Toggle_get_group_m2798_MethodInfo;
extern const MethodInfo Toggle_set_group_m2799_MethodInfo;
static const PropertyInfo Toggle_t312____group_PropertyInfo = 
{
	&Toggle_t312_il2cpp_TypeInfo/* parent */
	, "group"/* name */
	, &Toggle_get_group_m2798_MethodInfo/* get */
	, &Toggle_set_group_m2799_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Toggle_get_isOn_m1719_MethodInfo;
extern const MethodInfo Toggle_set_isOn_m2804_MethodInfo;
static const PropertyInfo Toggle_t312____isOn_PropertyInfo = 
{
	&Toggle_t312_il2cpp_TypeInfo/* parent */
	, "isOn"/* name */
	, &Toggle_get_isOn_m1719_MethodInfo/* get */
	, &Toggle_set_isOn_m2804_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Toggle_t312_PropertyInfos[] =
{
	&Toggle_t312____group_PropertyInfo,
	&Toggle_t312____isOn_PropertyInfo,
	NULL
};
static const Il2CppType* Toggle_t312_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ToggleTransition_t589_0_0_0,
	&ToggleEvent_t590_0_0_0,
};
extern const MethodInfo Toggle_OnEnable_m2801_MethodInfo;
extern const MethodInfo Toggle_Start_m2808_MethodInfo;
extern const MethodInfo Toggle_OnDisable_m2802_MethodInfo;
extern const MethodInfo Toggle_OnPointerClick_m2810_MethodInfo;
extern const MethodInfo Toggle_OnSubmit_m2811_MethodInfo;
extern const MethodInfo Toggle_Rebuild_m2800_MethodInfo;
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_get_transform_m2813_MethodInfo;
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m2812_MethodInfo;
static const Il2CppMethodReference Toggle_t312_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&Selectable_Awake_m2660_MethodInfo,
	&Toggle_OnEnable_m2801_MethodInfo,
	&Toggle_Start_m2808_MethodInfo,
	&Toggle_OnDisable_m2802_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m2663_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m2661_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&Selectable_OnPointerEnter_m2689_MethodInfo,
	&Selectable_OnPointerExit_m2690_MethodInfo,
	&Selectable_OnPointerDown_m2687_MethodInfo,
	&Selectable_OnPointerUp_m2688_MethodInfo,
	&Selectable_OnSelect_m2691_MethodInfo,
	&Selectable_OnDeselect_m2692_MethodInfo,
	&Selectable_OnMove_m2677_MethodInfo,
	&Selectable_IsInteractable_m2662_MethodInfo,
	&Selectable_InstantClearState_m2668_MethodInfo,
	&Selectable_DoStateTransition_m2669_MethodInfo,
	&Selectable_FindSelectableOnLeft_m2673_MethodInfo,
	&Selectable_FindSelectableOnRight_m2674_MethodInfo,
	&Selectable_FindSelectableOnUp_m2675_MethodInfo,
	&Selectable_FindSelectableOnDown_m2676_MethodInfo,
	&Selectable_OnMove_m2677_MethodInfo,
	&Selectable_OnPointerDown_m2687_MethodInfo,
	&Selectable_OnPointerUp_m2688_MethodInfo,
	&Selectable_OnPointerEnter_m2689_MethodInfo,
	&Selectable_OnPointerExit_m2690_MethodInfo,
	&Selectable_OnSelect_m2691_MethodInfo,
	&Selectable_OnDeselect_m2692_MethodInfo,
	&Selectable_Select_m2693_MethodInfo,
	&Toggle_OnPointerClick_m2810_MethodInfo,
	&Toggle_OnSubmit_m2811_MethodInfo,
	&Toggle_Rebuild_m2800_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m2813_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m2812_MethodInfo,
	&Toggle_Rebuild_m2800_MethodInfo,
	&Toggle_OnPointerClick_m2810_MethodInfo,
	&Toggle_OnSubmit_m2811_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m2812_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m2813_MethodInfo,
};
static bool Toggle_t312_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IPointerClickHandler_t448_0_0_0;
extern const Il2CppType ISubmitHandler_t641_0_0_0;
static const Il2CppType* Toggle_t312_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t281_0_0_0,
	&IPointerClickHandler_t448_0_0_0,
	&ISubmitHandler_t641_0_0_0,
	&ICanvasElement_t646_0_0_0,
};
static Il2CppInterfaceOffsetPair Toggle_t312_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t281_0_0_0, 16},
	{ &IPointerEnterHandler_t630_0_0_0, 16},
	{ &IPointerExitHandler_t631_0_0_0, 17},
	{ &IPointerDownHandler_t280_0_0_0, 18},
	{ &IPointerUpHandler_t282_0_0_0, 19},
	{ &ISelectHandler_t638_0_0_0, 20},
	{ &IDeselectHandler_t639_0_0_0, 21},
	{ &IMoveHandler_t640_0_0_0, 22},
	{ &IPointerClickHandler_t448_0_0_0, 38},
	{ &ISubmitHandler_t641_0_0_0, 39},
	{ &ICanvasElement_t646_0_0_0, 40},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Toggle_t312_1_0_0;
struct Toggle_t312;
const Il2CppTypeDefinitionMetadata Toggle_t312_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Toggle_t312_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Toggle_t312_InterfacesTypeInfos/* implementedInterfaces */
	, Toggle_t312_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t510_0_0_0/* parent */
	, Toggle_t312_VTable/* vtableMethods */
	, Toggle_t312_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 505/* fieldStart */

};
TypeInfo Toggle_t312_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Toggle"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Toggle_t312_MethodInfos/* methods */
	, Toggle_t312_PropertyInfos/* properties */
	, NULL/* events */
	, &Toggle_t312_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 258/* custom_attributes_cache */
	, &Toggle_t312_0_0_0/* byval_arg */
	, &Toggle_t312_1_0_0/* this_arg */
	, &Toggle_t312_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Toggle_t312)/* instance_size */
	, sizeof (Toggle_t312)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 48/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup.h"
// Metadata Definition UnityEngine.UI.ToggleGroup
extern TypeInfo ToggleGroup_t592_il2cpp_TypeInfo;
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::.ctor()
extern const MethodInfo ToggleGroup__ctor_m2814_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ToggleGroup__ctor_m2814/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1004/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::get_allowSwitchOff()
extern const MethodInfo ToggleGroup_get_allowSwitchOff_m2815_MethodInfo = 
{
	"get_allowSwitchOff"/* name */
	, (methodPointerType)&ToggleGroup_get_allowSwitchOff_m2815/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1005/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo ToggleGroup_t592_ToggleGroup_set_allowSwitchOff_m2816_ParameterInfos[] = 
{
	{"value", 0, 134218314, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::set_allowSwitchOff(System.Boolean)
extern const MethodInfo ToggleGroup_set_allowSwitchOff_m2816_MethodInfo = 
{
	"set_allowSwitchOff"/* name */
	, (methodPointerType)&ToggleGroup_set_allowSwitchOff_m2816/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, ToggleGroup_t592_ToggleGroup_set_allowSwitchOff_m2816_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1006/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t312_0_0_0;
static const ParameterInfo ToggleGroup_t592_ToggleGroup_ValidateToggleIsInGroup_m2817_ParameterInfos[] = 
{
	{"toggle", 0, 134218315, 0, &Toggle_t312_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::ValidateToggleIsInGroup(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_ValidateToggleIsInGroup_m2817_MethodInfo = 
{
	"ValidateToggleIsInGroup"/* name */
	, (methodPointerType)&ToggleGroup_ValidateToggleIsInGroup_m2817/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ToggleGroup_t592_ToggleGroup_ValidateToggleIsInGroup_m2817_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1007/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t312_0_0_0;
static const ParameterInfo ToggleGroup_t592_ToggleGroup_NotifyToggleOn_m2818_ParameterInfos[] = 
{
	{"toggle", 0, 134218316, 0, &Toggle_t312_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::NotifyToggleOn(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_NotifyToggleOn_m2818_MethodInfo = 
{
	"NotifyToggleOn"/* name */
	, (methodPointerType)&ToggleGroup_NotifyToggleOn_m2818/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ToggleGroup_t592_ToggleGroup_NotifyToggleOn_m2818_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1008/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t312_0_0_0;
static const ParameterInfo ToggleGroup_t592_ToggleGroup_UnregisterToggle_m2819_ParameterInfos[] = 
{
	{"toggle", 0, 134218317, 0, &Toggle_t312_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::UnregisterToggle(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_UnregisterToggle_m2819_MethodInfo = 
{
	"UnregisterToggle"/* name */
	, (methodPointerType)&ToggleGroup_UnregisterToggle_m2819/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ToggleGroup_t592_ToggleGroup_UnregisterToggle_m2819_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1009/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t312_0_0_0;
static const ParameterInfo ToggleGroup_t592_ToggleGroup_RegisterToggle_m2820_ParameterInfos[] = 
{
	{"toggle", 0, 134218318, 0, &Toggle_t312_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::RegisterToggle(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_RegisterToggle_m2820_MethodInfo = 
{
	"RegisterToggle"/* name */
	, (methodPointerType)&ToggleGroup_RegisterToggle_m2820/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ToggleGroup_t592_ToggleGroup_RegisterToggle_m2820_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1010/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::AnyTogglesOn()
extern const MethodInfo ToggleGroup_AnyTogglesOn_m2821_MethodInfo = 
{
	"AnyTogglesOn"/* name */
	, (methodPointerType)&ToggleGroup_AnyTogglesOn_m2821/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1011/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerable_1_t650_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::ActiveToggles()
extern const MethodInfo ToggleGroup_ActiveToggles_m2822_MethodInfo = 
{
	"ActiveToggles"/* name */
	, (methodPointerType)&ToggleGroup_ActiveToggles_m2822/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t650_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1012/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::SetAllTogglesOff()
extern const MethodInfo ToggleGroup_SetAllTogglesOff_m2823_MethodInfo = 
{
	"SetAllTogglesOff"/* name */
	, (methodPointerType)&ToggleGroup_SetAllTogglesOff_m2823/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1013/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t312_0_0_0;
static const ParameterInfo ToggleGroup_t592_ToggleGroup_U3CAnyTogglesOnU3Em__7_m2824_ParameterInfos[] = 
{
	{"x", 0, 134218319, 0, &Toggle_t312_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::<AnyTogglesOn>m__7(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_U3CAnyTogglesOnU3Em__7_m2824_MethodInfo = 
{
	"<AnyTogglesOn>m__7"/* name */
	, (methodPointerType)&ToggleGroup_U3CAnyTogglesOnU3Em__7_m2824/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, ToggleGroup_t592_ToggleGroup_U3CAnyTogglesOnU3Em__7_m2824_ParameterInfos/* parameters */
	, 265/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1014/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t312_0_0_0;
static const ParameterInfo ToggleGroup_t592_ToggleGroup_U3CActiveTogglesU3Em__8_m2825_ParameterInfos[] = 
{
	{"x", 0, 134218320, 0, &Toggle_t312_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::<ActiveToggles>m__8(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_U3CActiveTogglesU3Em__8_m2825_MethodInfo = 
{
	"<ActiveToggles>m__8"/* name */
	, (methodPointerType)&ToggleGroup_U3CActiveTogglesU3Em__8_m2825/* method */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, ToggleGroup_t592_ToggleGroup_U3CActiveTogglesU3Em__8_m2825_ParameterInfos/* parameters */
	, 266/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1015/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ToggleGroup_t592_MethodInfos[] =
{
	&ToggleGroup__ctor_m2814_MethodInfo,
	&ToggleGroup_get_allowSwitchOff_m2815_MethodInfo,
	&ToggleGroup_set_allowSwitchOff_m2816_MethodInfo,
	&ToggleGroup_ValidateToggleIsInGroup_m2817_MethodInfo,
	&ToggleGroup_NotifyToggleOn_m2818_MethodInfo,
	&ToggleGroup_UnregisterToggle_m2819_MethodInfo,
	&ToggleGroup_RegisterToggle_m2820_MethodInfo,
	&ToggleGroup_AnyTogglesOn_m2821_MethodInfo,
	&ToggleGroup_ActiveToggles_m2822_MethodInfo,
	&ToggleGroup_SetAllTogglesOff_m2823_MethodInfo,
	&ToggleGroup_U3CAnyTogglesOnU3Em__7_m2824_MethodInfo,
	&ToggleGroup_U3CActiveTogglesU3Em__8_m2825_MethodInfo,
	NULL
};
extern const MethodInfo ToggleGroup_get_allowSwitchOff_m2815_MethodInfo;
extern const MethodInfo ToggleGroup_set_allowSwitchOff_m2816_MethodInfo;
static const PropertyInfo ToggleGroup_t592____allowSwitchOff_PropertyInfo = 
{
	&ToggleGroup_t592_il2cpp_TypeInfo/* parent */
	, "allowSwitchOff"/* name */
	, &ToggleGroup_get_allowSwitchOff_m2815_MethodInfo/* get */
	, &ToggleGroup_set_allowSwitchOff_m2816_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ToggleGroup_t592_PropertyInfos[] =
{
	&ToggleGroup_t592____allowSwitchOff_PropertyInfo,
	NULL
};
extern const MethodInfo UIBehaviour_OnEnable_m1976_MethodInfo;
extern const MethodInfo UIBehaviour_OnDisable_m1978_MethodInfo;
static const Il2CppMethodReference ToggleGroup_t592_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&UIBehaviour_OnEnable_m1976_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&UIBehaviour_OnDisable_m1978_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m1984_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
};
static bool ToggleGroup_t592_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleGroup_t592_1_0_0;
struct ToggleGroup_t592;
const Il2CppTypeDefinitionMetadata ToggleGroup_t592_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t455_0_0_0/* parent */
	, ToggleGroup_t592_VTable/* vtableMethods */
	, ToggleGroup_t592_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 510/* fieldStart */

};
TypeInfo ToggleGroup_t592_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ToggleGroup_t592_MethodInfos/* methods */
	, ToggleGroup_t592_PropertyInfos/* properties */
	, NULL/* events */
	, &ToggleGroup_t592_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 261/* custom_attributes_cache */
	, &ToggleGroup_t592_0_0_0/* byval_arg */
	, &ToggleGroup_t592_1_0_0/* this_arg */
	, &ToggleGroup_t592_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleGroup_t592)/* instance_size */
	, sizeof (ToggleGroup_t592)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ToggleGroup_t592_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter/AspectMode
extern TypeInfo AspectMode_t596_il2cpp_TypeInfo;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectModeMethodDeclarations.h"
static const MethodInfo* AspectMode_t596_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AspectMode_t596_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool AspectMode_t596_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AspectMode_t596_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AspectMode_t596_0_0_0;
extern const Il2CppType AspectMode_t596_1_0_0;
extern TypeInfo AspectRatioFitter_t597_il2cpp_TypeInfo;
extern const Il2CppType AspectRatioFitter_t597_0_0_0;
const Il2CppTypeDefinitionMetadata AspectMode_t596_DefinitionMetadata = 
{
	&AspectRatioFitter_t597_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AspectMode_t596_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, AspectMode_t596_VTable/* vtableMethods */
	, AspectMode_t596_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 514/* fieldStart */

};
TypeInfo AspectMode_t596_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectMode"/* name */
	, ""/* namespaze */
	, AspectMode_t596_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AspectMode_t596_0_0_0/* byval_arg */
	, &AspectMode_t596_1_0_0/* this_arg */
	, &AspectMode_t596_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectMode_t596)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AspectMode_t596)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::.ctor()
extern const MethodInfo AspectRatioFitter__ctor_m2826_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AspectRatioFitter__ctor_m2826/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1016/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_AspectMode_t596 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::get_aspectMode()
extern const MethodInfo AspectRatioFitter_get_aspectMode_m2827_MethodInfo = 
{
	"get_aspectMode"/* name */
	, (methodPointerType)&AspectRatioFitter_get_aspectMode_m2827/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &AspectMode_t596_0_0_0/* return_type */
	, RuntimeInvoker_AspectMode_t596/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1017/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AspectMode_t596_0_0_0;
static const ParameterInfo AspectRatioFitter_t597_AspectRatioFitter_set_aspectMode_m2828_ParameterInfos[] = 
{
	{"value", 0, 134218321, 0, &AspectMode_t596_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectMode(UnityEngine.UI.AspectRatioFitter/AspectMode)
extern const MethodInfo AspectRatioFitter_set_aspectMode_m2828_MethodInfo = 
{
	"set_aspectMode"/* name */
	, (methodPointerType)&AspectRatioFitter_set_aspectMode_m2828/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, AspectRatioFitter_t597_AspectRatioFitter_set_aspectMode_m2828_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1018/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.AspectRatioFitter::get_aspectRatio()
extern const MethodInfo AspectRatioFitter_get_aspectRatio_m2829_MethodInfo = 
{
	"get_aspectRatio"/* name */
	, (methodPointerType)&AspectRatioFitter_get_aspectRatio_m2829/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1019/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo AspectRatioFitter_t597_AspectRatioFitter_set_aspectRatio_m2830_ParameterInfos[] = 
{
	{"value", 0, 134218322, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectRatio(System.Single)
extern const MethodInfo AspectRatioFitter_set_aspectRatio_m2830_MethodInfo = 
{
	"set_aspectRatio"/* name */
	, (methodPointerType)&AspectRatioFitter_set_aspectRatio_m2830/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, AspectRatioFitter_t597_AspectRatioFitter_set_aspectRatio_m2830_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1020/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::get_rectTransform()
extern const MethodInfo AspectRatioFitter_get_rectTransform_m2831_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&AspectRatioFitter_get_rectTransform_m2831/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t364_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1021/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnEnable()
extern const MethodInfo AspectRatioFitter_OnEnable_m2832_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&AspectRatioFitter_OnEnable_m2832/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1022/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnDisable()
extern const MethodInfo AspectRatioFitter_OnDisable_m2833_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&AspectRatioFitter_OnDisable_m2833/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1023/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnRectTransformDimensionsChange()
extern const MethodInfo AspectRatioFitter_OnRectTransformDimensionsChange_m2834_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&AspectRatioFitter_OnRectTransformDimensionsChange_m2834/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1024/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::UpdateRect()
extern const MethodInfo AspectRatioFitter_UpdateRect_m2835_MethodInfo = 
{
	"UpdateRect"/* name */
	, (methodPointerType)&AspectRatioFitter_UpdateRect_m2835/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1025/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo AspectRatioFitter_t597_AspectRatioFitter_GetSizeDeltaToProduceSize_m2836_ParameterInfos[] = 
{
	{"size", 0, 134218323, 0, &Single_t254_0_0_0},
	{"axis", 1, 134218324, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.AspectRatioFitter::GetSizeDeltaToProduceSize(System.Single,System.Int32)
extern const MethodInfo AspectRatioFitter_GetSizeDeltaToProduceSize_m2836_MethodInfo = 
{
	"GetSizeDeltaToProduceSize"/* name */
	, (methodPointerType)&AspectRatioFitter_GetSizeDeltaToProduceSize_m2836/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Single_t254_Int32_t253/* invoker_method */
	, AspectRatioFitter_t597_AspectRatioFitter_GetSizeDeltaToProduceSize_m2836_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1026/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.AspectRatioFitter::GetParentSize()
extern const MethodInfo AspectRatioFitter_GetParentSize_m2837_MethodInfo = 
{
	"GetParentSize"/* name */
	, (methodPointerType)&AspectRatioFitter_GetParentSize_m2837/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1027/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutHorizontal()
extern const MethodInfo AspectRatioFitter_SetLayoutHorizontal_m2838_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&AspectRatioFitter_SetLayoutHorizontal_m2838/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1028/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutVertical()
extern const MethodInfo AspectRatioFitter_SetLayoutVertical_m2839_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&AspectRatioFitter_SetLayoutVertical_m2839/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1029/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetDirty()
extern const MethodInfo AspectRatioFitter_SetDirty_m2840_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&AspectRatioFitter_SetDirty_m2840/* method */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1030/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AspectRatioFitter_t597_MethodInfos[] =
{
	&AspectRatioFitter__ctor_m2826_MethodInfo,
	&AspectRatioFitter_get_aspectMode_m2827_MethodInfo,
	&AspectRatioFitter_set_aspectMode_m2828_MethodInfo,
	&AspectRatioFitter_get_aspectRatio_m2829_MethodInfo,
	&AspectRatioFitter_set_aspectRatio_m2830_MethodInfo,
	&AspectRatioFitter_get_rectTransform_m2831_MethodInfo,
	&AspectRatioFitter_OnEnable_m2832_MethodInfo,
	&AspectRatioFitter_OnDisable_m2833_MethodInfo,
	&AspectRatioFitter_OnRectTransformDimensionsChange_m2834_MethodInfo,
	&AspectRatioFitter_UpdateRect_m2835_MethodInfo,
	&AspectRatioFitter_GetSizeDeltaToProduceSize_m2836_MethodInfo,
	&AspectRatioFitter_GetParentSize_m2837_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m2838_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m2839_MethodInfo,
	&AspectRatioFitter_SetDirty_m2840_MethodInfo,
	NULL
};
extern const MethodInfo AspectRatioFitter_get_aspectMode_m2827_MethodInfo;
extern const MethodInfo AspectRatioFitter_set_aspectMode_m2828_MethodInfo;
static const PropertyInfo AspectRatioFitter_t597____aspectMode_PropertyInfo = 
{
	&AspectRatioFitter_t597_il2cpp_TypeInfo/* parent */
	, "aspectMode"/* name */
	, &AspectRatioFitter_get_aspectMode_m2827_MethodInfo/* get */
	, &AspectRatioFitter_set_aspectMode_m2828_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AspectRatioFitter_get_aspectRatio_m2829_MethodInfo;
extern const MethodInfo AspectRatioFitter_set_aspectRatio_m2830_MethodInfo;
static const PropertyInfo AspectRatioFitter_t597____aspectRatio_PropertyInfo = 
{
	&AspectRatioFitter_t597_il2cpp_TypeInfo/* parent */
	, "aspectRatio"/* name */
	, &AspectRatioFitter_get_aspectRatio_m2829_MethodInfo/* get */
	, &AspectRatioFitter_set_aspectRatio_m2830_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AspectRatioFitter_get_rectTransform_m2831_MethodInfo;
static const PropertyInfo AspectRatioFitter_t597____rectTransform_PropertyInfo = 
{
	&AspectRatioFitter_t597_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &AspectRatioFitter_get_rectTransform_m2831_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AspectRatioFitter_t597_PropertyInfos[] =
{
	&AspectRatioFitter_t597____aspectMode_PropertyInfo,
	&AspectRatioFitter_t597____aspectRatio_PropertyInfo,
	&AspectRatioFitter_t597____rectTransform_PropertyInfo,
	NULL
};
static const Il2CppType* AspectRatioFitter_t597_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AspectMode_t596_0_0_0,
};
extern const MethodInfo AspectRatioFitter_OnEnable_m2832_MethodInfo;
extern const MethodInfo AspectRatioFitter_OnDisable_m2833_MethodInfo;
extern const MethodInfo AspectRatioFitter_OnRectTransformDimensionsChange_m2834_MethodInfo;
extern const MethodInfo AspectRatioFitter_SetLayoutHorizontal_m2838_MethodInfo;
extern const MethodInfo AspectRatioFitter_SetLayoutVertical_m2839_MethodInfo;
static const Il2CppMethodReference AspectRatioFitter_t597_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&AspectRatioFitter_OnEnable_m2832_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&AspectRatioFitter_OnDisable_m2833_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&AspectRatioFitter_OnRectTransformDimensionsChange_m2834_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m1984_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m2838_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m2839_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m2838_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m2839_MethodInfo,
};
static bool AspectRatioFitter_t597_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutController_t705_0_0_0;
extern const Il2CppType ILayoutSelfController_t706_0_0_0;
static const Il2CppType* AspectRatioFitter_t597_InterfacesTypeInfos[] = 
{
	&ILayoutController_t705_0_0_0,
	&ILayoutSelfController_t706_0_0_0,
};
static Il2CppInterfaceOffsetPair AspectRatioFitter_t597_InterfacesOffsets[] = 
{
	{ &ILayoutController_t705_0_0_0, 16},
	{ &ILayoutSelfController_t706_0_0_0, 18},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AspectRatioFitter_t597_1_0_0;
struct AspectRatioFitter_t597;
const Il2CppTypeDefinitionMetadata AspectRatioFitter_t597_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AspectRatioFitter_t597_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, AspectRatioFitter_t597_InterfacesTypeInfos/* implementedInterfaces */
	, AspectRatioFitter_t597_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t455_0_0_0/* parent */
	, AspectRatioFitter_t597_VTable/* vtableMethods */
	, AspectRatioFitter_t597_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 520/* fieldStart */

};
TypeInfo AspectRatioFitter_t597_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectRatioFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, AspectRatioFitter_t597_MethodInfos/* methods */
	, AspectRatioFitter_t597_PropertyInfos/* properties */
	, NULL/* events */
	, &AspectRatioFitter_t597_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 267/* custom_attributes_cache */
	, &AspectRatioFitter_t597_0_0_0/* byval_arg */
	, &AspectRatioFitter_t597_1_0_0/* this_arg */
	, &AspectRatioFitter_t597_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectRatioFitter_t597)/* instance_size */
	, sizeof (AspectRatioFitter_t597)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 20/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScaleMode
extern TypeInfo ScaleMode_t598_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleModeMethodDeclarations.h"
static const MethodInfo* ScaleMode_t598_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScaleMode_t598_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ScaleMode_t598_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScaleMode_t598_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScaleMode_t598_0_0_0;
extern const Il2CppType ScaleMode_t598_1_0_0;
extern TypeInfo CanvasScaler_t601_il2cpp_TypeInfo;
extern const Il2CppType CanvasScaler_t601_0_0_0;
const Il2CppTypeDefinitionMetadata ScaleMode_t598_DefinitionMetadata = 
{
	&CanvasScaler_t601_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScaleMode_t598_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ScaleMode_t598_VTable/* vtableMethods */
	, ScaleMode_t598_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 524/* fieldStart */

};
TypeInfo ScaleMode_t598_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScaleMode"/* name */
	, ""/* namespaze */
	, ScaleMode_t598_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScaleMode_t598_0_0_0/* byval_arg */
	, &ScaleMode_t598_1_0_0/* this_arg */
	, &ScaleMode_t598_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScaleMode_t598)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScaleMode_t598)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScreenMatchMode
extern TypeInfo ScreenMatchMode_t599_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchModeMethodDeclarations.h"
static const MethodInfo* ScreenMatchMode_t599_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScreenMatchMode_t599_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool ScreenMatchMode_t599_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScreenMatchMode_t599_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScreenMatchMode_t599_0_0_0;
extern const Il2CppType ScreenMatchMode_t599_1_0_0;
const Il2CppTypeDefinitionMetadata ScreenMatchMode_t599_DefinitionMetadata = 
{
	&CanvasScaler_t601_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScreenMatchMode_t599_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, ScreenMatchMode_t599_VTable/* vtableMethods */
	, ScreenMatchMode_t599_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 528/* fieldStart */

};
TypeInfo ScreenMatchMode_t599_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenMatchMode"/* name */
	, ""/* namespaze */
	, ScreenMatchMode_t599_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScreenMatchMode_t599_0_0_0/* byval_arg */
	, &ScreenMatchMode_t599_1_0_0/* this_arg */
	, &ScreenMatchMode_t599_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenMatchMode_t599)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScreenMatchMode_t599)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/Unit
extern TypeInfo Unit_t600_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_UnitMethodDeclarations.h"
static const MethodInfo* Unit_t600_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Unit_t600_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Unit_t600_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Unit_t600_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Unit_t600_0_0_0;
extern const Il2CppType Unit_t600_1_0_0;
const Il2CppTypeDefinitionMetadata Unit_t600_DefinitionMetadata = 
{
	&CanvasScaler_t601_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Unit_t600_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Unit_t600_VTable/* vtableMethods */
	, Unit_t600_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 532/* fieldStart */

};
TypeInfo Unit_t600_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Unit"/* name */
	, ""/* namespaze */
	, Unit_t600_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Unit_t600_0_0_0/* byval_arg */
	, &Unit_t600_1_0_0/* this_arg */
	, &Unit_t600_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Unit_t600)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Unit_t600)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler.h"
// Metadata Definition UnityEngine.UI.CanvasScaler
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScalerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::.ctor()
extern const MethodInfo CanvasScaler__ctor_m2841_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CanvasScaler__ctor_m2841/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1031/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_ScaleMode_t598 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::get_uiScaleMode()
extern const MethodInfo CanvasScaler_get_uiScaleMode_m2842_MethodInfo = 
{
	"get_uiScaleMode"/* name */
	, (methodPointerType)&CanvasScaler_get_uiScaleMode_m2842/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &ScaleMode_t598_0_0_0/* return_type */
	, RuntimeInvoker_ScaleMode_t598/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1032/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScaleMode_t598_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_set_uiScaleMode_m2843_ParameterInfos[] = 
{
	{"value", 0, 134218325, 0, &ScaleMode_t598_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_uiScaleMode(UnityEngine.UI.CanvasScaler/ScaleMode)
extern const MethodInfo CanvasScaler_set_uiScaleMode_m2843_MethodInfo = 
{
	"set_uiScaleMode"/* name */
	, (methodPointerType)&CanvasScaler_set_uiScaleMode_m2843/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_set_uiScaleMode_m2843_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1033/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_referencePixelsPerUnit()
extern const MethodInfo CanvasScaler_get_referencePixelsPerUnit_m2844_MethodInfo = 
{
	"get_referencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_referencePixelsPerUnit_m2844/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1034/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_set_referencePixelsPerUnit_m2845_ParameterInfos[] = 
{
	{"value", 0, 134218326, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_referencePixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_set_referencePixelsPerUnit_m2845_MethodInfo = 
{
	"set_referencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_referencePixelsPerUnit_m2845/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_set_referencePixelsPerUnit_m2845_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1035/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_scaleFactor()
extern const MethodInfo CanvasScaler_get_scaleFactor_m2846_MethodInfo = 
{
	"get_scaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_get_scaleFactor_m2846/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1036/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_set_scaleFactor_m2847_ParameterInfos[] = 
{
	{"value", 0, 134218327, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_scaleFactor(System.Single)
extern const MethodInfo CanvasScaler_set_scaleFactor_m2847_MethodInfo = 
{
	"set_scaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_set_scaleFactor_m2847/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_set_scaleFactor_m2847_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1037/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::get_referenceResolution()
extern const MethodInfo CanvasScaler_get_referenceResolution_m2848_MethodInfo = 
{
	"get_referenceResolution"/* name */
	, (methodPointerType)&CanvasScaler_get_referenceResolution_m2848/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1038/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_set_referenceResolution_m2849_ParameterInfos[] = 
{
	{"value", 0, 134218328, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_referenceResolution(UnityEngine.Vector2)
extern const MethodInfo CanvasScaler_set_referenceResolution_m2849_MethodInfo = 
{
	"set_referenceResolution"/* name */
	, (methodPointerType)&CanvasScaler_set_referenceResolution_m2849/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector2_t6/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_set_referenceResolution_m2849_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1039/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_ScreenMatchMode_t599 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::get_screenMatchMode()
extern const MethodInfo CanvasScaler_get_screenMatchMode_m2850_MethodInfo = 
{
	"get_screenMatchMode"/* name */
	, (methodPointerType)&CanvasScaler_get_screenMatchMode_m2850/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &ScreenMatchMode_t599_0_0_0/* return_type */
	, RuntimeInvoker_ScreenMatchMode_t599/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1040/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScreenMatchMode_t599_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_set_screenMatchMode_m2851_ParameterInfos[] = 
{
	{"value", 0, 134218329, 0, &ScreenMatchMode_t599_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_screenMatchMode(UnityEngine.UI.CanvasScaler/ScreenMatchMode)
extern const MethodInfo CanvasScaler_set_screenMatchMode_m2851_MethodInfo = 
{
	"set_screenMatchMode"/* name */
	, (methodPointerType)&CanvasScaler_set_screenMatchMode_m2851/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_set_screenMatchMode_m2851_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1041/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_matchWidthOrHeight()
extern const MethodInfo CanvasScaler_get_matchWidthOrHeight_m2852_MethodInfo = 
{
	"get_matchWidthOrHeight"/* name */
	, (methodPointerType)&CanvasScaler_get_matchWidthOrHeight_m2852/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1042/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_set_matchWidthOrHeight_m2853_ParameterInfos[] = 
{
	{"value", 0, 134218330, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_matchWidthOrHeight(System.Single)
extern const MethodInfo CanvasScaler_set_matchWidthOrHeight_m2853_MethodInfo = 
{
	"set_matchWidthOrHeight"/* name */
	, (methodPointerType)&CanvasScaler_set_matchWidthOrHeight_m2853/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_set_matchWidthOrHeight_m2853_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1043/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Unit_t600 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::get_physicalUnit()
extern const MethodInfo CanvasScaler_get_physicalUnit_m2854_MethodInfo = 
{
	"get_physicalUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_physicalUnit_m2854/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Unit_t600_0_0_0/* return_type */
	, RuntimeInvoker_Unit_t600/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1044/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Unit_t600_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_set_physicalUnit_m2855_ParameterInfos[] = 
{
	{"value", 0, 134218331, 0, &Unit_t600_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_physicalUnit(UnityEngine.UI.CanvasScaler/Unit)
extern const MethodInfo CanvasScaler_set_physicalUnit_m2855_MethodInfo = 
{
	"set_physicalUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_physicalUnit_m2855/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_set_physicalUnit_m2855_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1045/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_fallbackScreenDPI()
extern const MethodInfo CanvasScaler_get_fallbackScreenDPI_m2856_MethodInfo = 
{
	"get_fallbackScreenDPI"/* name */
	, (methodPointerType)&CanvasScaler_get_fallbackScreenDPI_m2856/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1046/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_set_fallbackScreenDPI_m2857_ParameterInfos[] = 
{
	{"value", 0, 134218332, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_fallbackScreenDPI(System.Single)
extern const MethodInfo CanvasScaler_set_fallbackScreenDPI_m2857_MethodInfo = 
{
	"set_fallbackScreenDPI"/* name */
	, (methodPointerType)&CanvasScaler_set_fallbackScreenDPI_m2857/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_set_fallbackScreenDPI_m2857_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1047/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_defaultSpriteDPI()
extern const MethodInfo CanvasScaler_get_defaultSpriteDPI_m2858_MethodInfo = 
{
	"get_defaultSpriteDPI"/* name */
	, (methodPointerType)&CanvasScaler_get_defaultSpriteDPI_m2858/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1048/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_set_defaultSpriteDPI_m2859_ParameterInfos[] = 
{
	{"value", 0, 134218333, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_defaultSpriteDPI(System.Single)
extern const MethodInfo CanvasScaler_set_defaultSpriteDPI_m2859_MethodInfo = 
{
	"set_defaultSpriteDPI"/* name */
	, (methodPointerType)&CanvasScaler_set_defaultSpriteDPI_m2859/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_set_defaultSpriteDPI_m2859_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1049/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_dynamicPixelsPerUnit()
extern const MethodInfo CanvasScaler_get_dynamicPixelsPerUnit_m2860_MethodInfo = 
{
	"get_dynamicPixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_dynamicPixelsPerUnit_m2860/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1050/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_set_dynamicPixelsPerUnit_m2861_ParameterInfos[] = 
{
	{"value", 0, 134218334, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_dynamicPixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_set_dynamicPixelsPerUnit_m2861_MethodInfo = 
{
	"set_dynamicPixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_dynamicPixelsPerUnit_m2861/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_set_dynamicPixelsPerUnit_m2861_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1051/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::OnEnable()
extern const MethodInfo CanvasScaler_OnEnable_m2862_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&CanvasScaler_OnEnable_m2862/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1052/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::OnDisable()
extern const MethodInfo CanvasScaler_OnDisable_m2863_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&CanvasScaler_OnDisable_m2863/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1053/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::Update()
extern const MethodInfo CanvasScaler_Update_m2864_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&CanvasScaler_Update_m2864/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1054/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::Handle()
extern const MethodInfo CanvasScaler_Handle_m2865_MethodInfo = 
{
	"Handle"/* name */
	, (methodPointerType)&CanvasScaler_Handle_m2865/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1055/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleWorldCanvas()
extern const MethodInfo CanvasScaler_HandleWorldCanvas_m2866_MethodInfo = 
{
	"HandleWorldCanvas"/* name */
	, (methodPointerType)&CanvasScaler_HandleWorldCanvas_m2866/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1056/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPixelSize()
extern const MethodInfo CanvasScaler_HandleConstantPixelSize_m2867_MethodInfo = 
{
	"HandleConstantPixelSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleConstantPixelSize_m2867/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1057/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleScaleWithScreenSize()
extern const MethodInfo CanvasScaler_HandleScaleWithScreenSize_m2868_MethodInfo = 
{
	"HandleScaleWithScreenSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleScaleWithScreenSize_m2868/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1058/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPhysicalSize()
extern const MethodInfo CanvasScaler_HandleConstantPhysicalSize_m2869_MethodInfo = 
{
	"HandleConstantPhysicalSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleConstantPhysicalSize_m2869/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1059/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_SetScaleFactor_m2870_ParameterInfos[] = 
{
	{"scaleFactor", 0, 134218335, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::SetScaleFactor(System.Single)
extern const MethodInfo CanvasScaler_SetScaleFactor_m2870_MethodInfo = 
{
	"SetScaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_SetScaleFactor_m2870/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_SetScaleFactor_m2870_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1060/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo CanvasScaler_t601_CanvasScaler_SetReferencePixelsPerUnit_m2871_ParameterInfos[] = 
{
	{"referencePixelsPerUnit", 0, 134218336, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::SetReferencePixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_SetReferencePixelsPerUnit_m2871_MethodInfo = 
{
	"SetReferencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_SetReferencePixelsPerUnit_m2871/* method */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, CanvasScaler_t601_CanvasScaler_SetReferencePixelsPerUnit_m2871_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1061/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CanvasScaler_t601_MethodInfos[] =
{
	&CanvasScaler__ctor_m2841_MethodInfo,
	&CanvasScaler_get_uiScaleMode_m2842_MethodInfo,
	&CanvasScaler_set_uiScaleMode_m2843_MethodInfo,
	&CanvasScaler_get_referencePixelsPerUnit_m2844_MethodInfo,
	&CanvasScaler_set_referencePixelsPerUnit_m2845_MethodInfo,
	&CanvasScaler_get_scaleFactor_m2846_MethodInfo,
	&CanvasScaler_set_scaleFactor_m2847_MethodInfo,
	&CanvasScaler_get_referenceResolution_m2848_MethodInfo,
	&CanvasScaler_set_referenceResolution_m2849_MethodInfo,
	&CanvasScaler_get_screenMatchMode_m2850_MethodInfo,
	&CanvasScaler_set_screenMatchMode_m2851_MethodInfo,
	&CanvasScaler_get_matchWidthOrHeight_m2852_MethodInfo,
	&CanvasScaler_set_matchWidthOrHeight_m2853_MethodInfo,
	&CanvasScaler_get_physicalUnit_m2854_MethodInfo,
	&CanvasScaler_set_physicalUnit_m2855_MethodInfo,
	&CanvasScaler_get_fallbackScreenDPI_m2856_MethodInfo,
	&CanvasScaler_set_fallbackScreenDPI_m2857_MethodInfo,
	&CanvasScaler_get_defaultSpriteDPI_m2858_MethodInfo,
	&CanvasScaler_set_defaultSpriteDPI_m2859_MethodInfo,
	&CanvasScaler_get_dynamicPixelsPerUnit_m2860_MethodInfo,
	&CanvasScaler_set_dynamicPixelsPerUnit_m2861_MethodInfo,
	&CanvasScaler_OnEnable_m2862_MethodInfo,
	&CanvasScaler_OnDisable_m2863_MethodInfo,
	&CanvasScaler_Update_m2864_MethodInfo,
	&CanvasScaler_Handle_m2865_MethodInfo,
	&CanvasScaler_HandleWorldCanvas_m2866_MethodInfo,
	&CanvasScaler_HandleConstantPixelSize_m2867_MethodInfo,
	&CanvasScaler_HandleScaleWithScreenSize_m2868_MethodInfo,
	&CanvasScaler_HandleConstantPhysicalSize_m2869_MethodInfo,
	&CanvasScaler_SetScaleFactor_m2870_MethodInfo,
	&CanvasScaler_SetReferencePixelsPerUnit_m2871_MethodInfo,
	NULL
};
extern const MethodInfo CanvasScaler_get_uiScaleMode_m2842_MethodInfo;
extern const MethodInfo CanvasScaler_set_uiScaleMode_m2843_MethodInfo;
static const PropertyInfo CanvasScaler_t601____uiScaleMode_PropertyInfo = 
{
	&CanvasScaler_t601_il2cpp_TypeInfo/* parent */
	, "uiScaleMode"/* name */
	, &CanvasScaler_get_uiScaleMode_m2842_MethodInfo/* get */
	, &CanvasScaler_set_uiScaleMode_m2843_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_referencePixelsPerUnit_m2844_MethodInfo;
extern const MethodInfo CanvasScaler_set_referencePixelsPerUnit_m2845_MethodInfo;
static const PropertyInfo CanvasScaler_t601____referencePixelsPerUnit_PropertyInfo = 
{
	&CanvasScaler_t601_il2cpp_TypeInfo/* parent */
	, "referencePixelsPerUnit"/* name */
	, &CanvasScaler_get_referencePixelsPerUnit_m2844_MethodInfo/* get */
	, &CanvasScaler_set_referencePixelsPerUnit_m2845_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_scaleFactor_m2846_MethodInfo;
extern const MethodInfo CanvasScaler_set_scaleFactor_m2847_MethodInfo;
static const PropertyInfo CanvasScaler_t601____scaleFactor_PropertyInfo = 
{
	&CanvasScaler_t601_il2cpp_TypeInfo/* parent */
	, "scaleFactor"/* name */
	, &CanvasScaler_get_scaleFactor_m2846_MethodInfo/* get */
	, &CanvasScaler_set_scaleFactor_m2847_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_referenceResolution_m2848_MethodInfo;
extern const MethodInfo CanvasScaler_set_referenceResolution_m2849_MethodInfo;
static const PropertyInfo CanvasScaler_t601____referenceResolution_PropertyInfo = 
{
	&CanvasScaler_t601_il2cpp_TypeInfo/* parent */
	, "referenceResolution"/* name */
	, &CanvasScaler_get_referenceResolution_m2848_MethodInfo/* get */
	, &CanvasScaler_set_referenceResolution_m2849_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_screenMatchMode_m2850_MethodInfo;
extern const MethodInfo CanvasScaler_set_screenMatchMode_m2851_MethodInfo;
static const PropertyInfo CanvasScaler_t601____screenMatchMode_PropertyInfo = 
{
	&CanvasScaler_t601_il2cpp_TypeInfo/* parent */
	, "screenMatchMode"/* name */
	, &CanvasScaler_get_screenMatchMode_m2850_MethodInfo/* get */
	, &CanvasScaler_set_screenMatchMode_m2851_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_matchWidthOrHeight_m2852_MethodInfo;
extern const MethodInfo CanvasScaler_set_matchWidthOrHeight_m2853_MethodInfo;
static const PropertyInfo CanvasScaler_t601____matchWidthOrHeight_PropertyInfo = 
{
	&CanvasScaler_t601_il2cpp_TypeInfo/* parent */
	, "matchWidthOrHeight"/* name */
	, &CanvasScaler_get_matchWidthOrHeight_m2852_MethodInfo/* get */
	, &CanvasScaler_set_matchWidthOrHeight_m2853_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_physicalUnit_m2854_MethodInfo;
extern const MethodInfo CanvasScaler_set_physicalUnit_m2855_MethodInfo;
static const PropertyInfo CanvasScaler_t601____physicalUnit_PropertyInfo = 
{
	&CanvasScaler_t601_il2cpp_TypeInfo/* parent */
	, "physicalUnit"/* name */
	, &CanvasScaler_get_physicalUnit_m2854_MethodInfo/* get */
	, &CanvasScaler_set_physicalUnit_m2855_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_fallbackScreenDPI_m2856_MethodInfo;
extern const MethodInfo CanvasScaler_set_fallbackScreenDPI_m2857_MethodInfo;
static const PropertyInfo CanvasScaler_t601____fallbackScreenDPI_PropertyInfo = 
{
	&CanvasScaler_t601_il2cpp_TypeInfo/* parent */
	, "fallbackScreenDPI"/* name */
	, &CanvasScaler_get_fallbackScreenDPI_m2856_MethodInfo/* get */
	, &CanvasScaler_set_fallbackScreenDPI_m2857_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_defaultSpriteDPI_m2858_MethodInfo;
extern const MethodInfo CanvasScaler_set_defaultSpriteDPI_m2859_MethodInfo;
static const PropertyInfo CanvasScaler_t601____defaultSpriteDPI_PropertyInfo = 
{
	&CanvasScaler_t601_il2cpp_TypeInfo/* parent */
	, "defaultSpriteDPI"/* name */
	, &CanvasScaler_get_defaultSpriteDPI_m2858_MethodInfo/* get */
	, &CanvasScaler_set_defaultSpriteDPI_m2859_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_dynamicPixelsPerUnit_m2860_MethodInfo;
extern const MethodInfo CanvasScaler_set_dynamicPixelsPerUnit_m2861_MethodInfo;
static const PropertyInfo CanvasScaler_t601____dynamicPixelsPerUnit_PropertyInfo = 
{
	&CanvasScaler_t601_il2cpp_TypeInfo/* parent */
	, "dynamicPixelsPerUnit"/* name */
	, &CanvasScaler_get_dynamicPixelsPerUnit_m2860_MethodInfo/* get */
	, &CanvasScaler_set_dynamicPixelsPerUnit_m2861_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CanvasScaler_t601_PropertyInfos[] =
{
	&CanvasScaler_t601____uiScaleMode_PropertyInfo,
	&CanvasScaler_t601____referencePixelsPerUnit_PropertyInfo,
	&CanvasScaler_t601____scaleFactor_PropertyInfo,
	&CanvasScaler_t601____referenceResolution_PropertyInfo,
	&CanvasScaler_t601____screenMatchMode_PropertyInfo,
	&CanvasScaler_t601____matchWidthOrHeight_PropertyInfo,
	&CanvasScaler_t601____physicalUnit_PropertyInfo,
	&CanvasScaler_t601____fallbackScreenDPI_PropertyInfo,
	&CanvasScaler_t601____defaultSpriteDPI_PropertyInfo,
	&CanvasScaler_t601____dynamicPixelsPerUnit_PropertyInfo,
	NULL
};
static const Il2CppType* CanvasScaler_t601_il2cpp_TypeInfo__nestedTypes[3] =
{
	&ScaleMode_t598_0_0_0,
	&ScreenMatchMode_t599_0_0_0,
	&Unit_t600_0_0_0,
};
extern const MethodInfo CanvasScaler_OnEnable_m2862_MethodInfo;
extern const MethodInfo CanvasScaler_OnDisable_m2863_MethodInfo;
extern const MethodInfo CanvasScaler_Update_m2864_MethodInfo;
extern const MethodInfo CanvasScaler_Handle_m2865_MethodInfo;
extern const MethodInfo CanvasScaler_HandleWorldCanvas_m2866_MethodInfo;
extern const MethodInfo CanvasScaler_HandleConstantPixelSize_m2867_MethodInfo;
extern const MethodInfo CanvasScaler_HandleScaleWithScreenSize_m2868_MethodInfo;
extern const MethodInfo CanvasScaler_HandleConstantPhysicalSize_m2869_MethodInfo;
static const Il2CppMethodReference CanvasScaler_t601_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&CanvasScaler_OnEnable_m2862_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&CanvasScaler_OnDisable_m2863_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m1984_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&CanvasScaler_Update_m2864_MethodInfo,
	&CanvasScaler_Handle_m2865_MethodInfo,
	&CanvasScaler_HandleWorldCanvas_m2866_MethodInfo,
	&CanvasScaler_HandleConstantPixelSize_m2867_MethodInfo,
	&CanvasScaler_HandleScaleWithScreenSize_m2868_MethodInfo,
	&CanvasScaler_HandleConstantPhysicalSize_m2869_MethodInfo,
};
static bool CanvasScaler_t601_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasScaler_t601_1_0_0;
struct CanvasScaler_t601;
const Il2CppTypeDefinitionMetadata CanvasScaler_t601_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CanvasScaler_t601_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t455_0_0_0/* parent */
	, CanvasScaler_t601_VTable/* vtableMethods */
	, CanvasScaler_t601_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 538/* fieldStart */

};
TypeInfo CanvasScaler_t601_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasScaler"/* name */
	, "UnityEngine.UI"/* namespaze */
	, CanvasScaler_t601_MethodInfos/* methods */
	, CanvasScaler_t601_PropertyInfos/* properties */
	, NULL/* events */
	, &CanvasScaler_t601_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 270/* custom_attributes_cache */
	, &CanvasScaler_t601_0_0_0/* byval_arg */
	, &CanvasScaler_t601_1_0_0/* this_arg */
	, &CanvasScaler_t601_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasScaler_t601)/* instance_size */
	, sizeof (CanvasScaler_t601)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 10/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 22/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter/FitMode
extern TypeInfo FitMode_t602_il2cpp_TypeInfo;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitModeMethodDeclarations.h"
static const MethodInfo* FitMode_t602_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FitMode_t602_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool FitMode_t602_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FitMode_t602_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType FitMode_t602_0_0_0;
extern const Il2CppType FitMode_t602_1_0_0;
extern TypeInfo ContentSizeFitter_t603_il2cpp_TypeInfo;
extern const Il2CppType ContentSizeFitter_t603_0_0_0;
const Il2CppTypeDefinitionMetadata FitMode_t602_DefinitionMetadata = 
{
	&ContentSizeFitter_t603_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FitMode_t602_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, FitMode_t602_VTable/* vtableMethods */
	, FitMode_t602_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 552/* fieldStart */

};
TypeInfo FitMode_t602_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "FitMode"/* name */
	, ""/* namespaze */
	, FitMode_t602_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FitMode_t602_0_0_0/* byval_arg */
	, &FitMode_t602_1_0_0/* this_arg */
	, &FitMode_t602_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FitMode_t602)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FitMode_t602)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::.ctor()
extern const MethodInfo ContentSizeFitter__ctor_m2872_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContentSizeFitter__ctor_m2872/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1062/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FitMode_t602 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_horizontalFit()
extern const MethodInfo ContentSizeFitter_get_horizontalFit_m2873_MethodInfo = 
{
	"get_horizontalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_get_horizontalFit_m2873/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &FitMode_t602_0_0_0/* return_type */
	, RuntimeInvoker_FitMode_t602/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1063/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FitMode_t602_0_0_0;
static const ParameterInfo ContentSizeFitter_t603_ContentSizeFitter_set_horizontalFit_m2874_ParameterInfos[] = 
{
	{"value", 0, 134218337, 0, &FitMode_t602_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::set_horizontalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern const MethodInfo ContentSizeFitter_set_horizontalFit_m2874_MethodInfo = 
{
	"set_horizontalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_set_horizontalFit_m2874/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ContentSizeFitter_t603_ContentSizeFitter_set_horizontalFit_m2874_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1064/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FitMode_t602 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_verticalFit()
extern const MethodInfo ContentSizeFitter_get_verticalFit_m2875_MethodInfo = 
{
	"get_verticalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_get_verticalFit_m2875/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &FitMode_t602_0_0_0/* return_type */
	, RuntimeInvoker_FitMode_t602/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1065/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FitMode_t602_0_0_0;
static const ParameterInfo ContentSizeFitter_t603_ContentSizeFitter_set_verticalFit_m2876_ParameterInfos[] = 
{
	{"value", 0, 134218338, 0, &FitMode_t602_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::set_verticalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern const MethodInfo ContentSizeFitter_set_verticalFit_m2876_MethodInfo = 
{
	"set_verticalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_set_verticalFit_m2876/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ContentSizeFitter_t603_ContentSizeFitter_set_verticalFit_m2876_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1066/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::get_rectTransform()
extern const MethodInfo ContentSizeFitter_get_rectTransform_m2877_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&ContentSizeFitter_get_rectTransform_m2877/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t364_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1067/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnEnable()
extern const MethodInfo ContentSizeFitter_OnEnable_m2878_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ContentSizeFitter_OnEnable_m2878/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1068/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnDisable()
extern const MethodInfo ContentSizeFitter_OnDisable_m2879_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ContentSizeFitter_OnDisable_m2879/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1069/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnRectTransformDimensionsChange()
extern const MethodInfo ContentSizeFitter_OnRectTransformDimensionsChange_m2880_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&ContentSizeFitter_OnRectTransformDimensionsChange_m2880/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1070/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ContentSizeFitter_t603_ContentSizeFitter_HandleSelfFittingAlongAxis_m2881_ParameterInfos[] = 
{
	{"axis", 0, 134218339, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::HandleSelfFittingAlongAxis(System.Int32)
extern const MethodInfo ContentSizeFitter_HandleSelfFittingAlongAxis_m2881_MethodInfo = 
{
	"HandleSelfFittingAlongAxis"/* name */
	, (methodPointerType)&ContentSizeFitter_HandleSelfFittingAlongAxis_m2881/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, ContentSizeFitter_t603_ContentSizeFitter_HandleSelfFittingAlongAxis_m2881_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1071/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutHorizontal()
extern const MethodInfo ContentSizeFitter_SetLayoutHorizontal_m2882_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&ContentSizeFitter_SetLayoutHorizontal_m2882/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1072/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutVertical()
extern const MethodInfo ContentSizeFitter_SetLayoutVertical_m2883_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&ContentSizeFitter_SetLayoutVertical_m2883/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1073/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetDirty()
extern const MethodInfo ContentSizeFitter_SetDirty_m2884_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&ContentSizeFitter_SetDirty_m2884/* method */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1074/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContentSizeFitter_t603_MethodInfos[] =
{
	&ContentSizeFitter__ctor_m2872_MethodInfo,
	&ContentSizeFitter_get_horizontalFit_m2873_MethodInfo,
	&ContentSizeFitter_set_horizontalFit_m2874_MethodInfo,
	&ContentSizeFitter_get_verticalFit_m2875_MethodInfo,
	&ContentSizeFitter_set_verticalFit_m2876_MethodInfo,
	&ContentSizeFitter_get_rectTransform_m2877_MethodInfo,
	&ContentSizeFitter_OnEnable_m2878_MethodInfo,
	&ContentSizeFitter_OnDisable_m2879_MethodInfo,
	&ContentSizeFitter_OnRectTransformDimensionsChange_m2880_MethodInfo,
	&ContentSizeFitter_HandleSelfFittingAlongAxis_m2881_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m2882_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m2883_MethodInfo,
	&ContentSizeFitter_SetDirty_m2884_MethodInfo,
	NULL
};
extern const MethodInfo ContentSizeFitter_get_horizontalFit_m2873_MethodInfo;
extern const MethodInfo ContentSizeFitter_set_horizontalFit_m2874_MethodInfo;
static const PropertyInfo ContentSizeFitter_t603____horizontalFit_PropertyInfo = 
{
	&ContentSizeFitter_t603_il2cpp_TypeInfo/* parent */
	, "horizontalFit"/* name */
	, &ContentSizeFitter_get_horizontalFit_m2873_MethodInfo/* get */
	, &ContentSizeFitter_set_horizontalFit_m2874_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ContentSizeFitter_get_verticalFit_m2875_MethodInfo;
extern const MethodInfo ContentSizeFitter_set_verticalFit_m2876_MethodInfo;
static const PropertyInfo ContentSizeFitter_t603____verticalFit_PropertyInfo = 
{
	&ContentSizeFitter_t603_il2cpp_TypeInfo/* parent */
	, "verticalFit"/* name */
	, &ContentSizeFitter_get_verticalFit_m2875_MethodInfo/* get */
	, &ContentSizeFitter_set_verticalFit_m2876_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ContentSizeFitter_get_rectTransform_m2877_MethodInfo;
static const PropertyInfo ContentSizeFitter_t603____rectTransform_PropertyInfo = 
{
	&ContentSizeFitter_t603_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &ContentSizeFitter_get_rectTransform_m2877_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ContentSizeFitter_t603_PropertyInfos[] =
{
	&ContentSizeFitter_t603____horizontalFit_PropertyInfo,
	&ContentSizeFitter_t603____verticalFit_PropertyInfo,
	&ContentSizeFitter_t603____rectTransform_PropertyInfo,
	NULL
};
static const Il2CppType* ContentSizeFitter_t603_il2cpp_TypeInfo__nestedTypes[1] =
{
	&FitMode_t602_0_0_0,
};
extern const MethodInfo ContentSizeFitter_OnEnable_m2878_MethodInfo;
extern const MethodInfo ContentSizeFitter_OnDisable_m2879_MethodInfo;
extern const MethodInfo ContentSizeFitter_OnRectTransformDimensionsChange_m2880_MethodInfo;
extern const MethodInfo ContentSizeFitter_SetLayoutHorizontal_m2882_MethodInfo;
extern const MethodInfo ContentSizeFitter_SetLayoutVertical_m2883_MethodInfo;
static const Il2CppMethodReference ContentSizeFitter_t603_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&ContentSizeFitter_OnEnable_m2878_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&ContentSizeFitter_OnDisable_m2879_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&ContentSizeFitter_OnRectTransformDimensionsChange_m2880_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m1984_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m2882_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m2883_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m2882_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m2883_MethodInfo,
};
static bool ContentSizeFitter_t603_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContentSizeFitter_t603_InterfacesTypeInfos[] = 
{
	&ILayoutController_t705_0_0_0,
	&ILayoutSelfController_t706_0_0_0,
};
static Il2CppInterfaceOffsetPair ContentSizeFitter_t603_InterfacesOffsets[] = 
{
	{ &ILayoutController_t705_0_0_0, 16},
	{ &ILayoutSelfController_t706_0_0_0, 18},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ContentSizeFitter_t603_1_0_0;
struct ContentSizeFitter_t603;
const Il2CppTypeDefinitionMetadata ContentSizeFitter_t603_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ContentSizeFitter_t603_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ContentSizeFitter_t603_InterfacesTypeInfos/* implementedInterfaces */
	, ContentSizeFitter_t603_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t455_0_0_0/* parent */
	, ContentSizeFitter_t603_VTable/* vtableMethods */
	, ContentSizeFitter_t603_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 556/* fieldStart */

};
TypeInfo ContentSizeFitter_t603_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContentSizeFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ContentSizeFitter_t603_MethodInfos/* methods */
	, ContentSizeFitter_t603_PropertyInfos/* properties */
	, NULL/* events */
	, &ContentSizeFitter_t603_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 281/* custom_attributes_cache */
	, &ContentSizeFitter_t603_0_0_0/* byval_arg */
	, &ContentSizeFitter_t603_1_0_0/* this_arg */
	, &ContentSizeFitter_t603_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContentSizeFitter_t603)/* instance_size */
	, sizeof (ContentSizeFitter_t603)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 20/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Corner
extern TypeInfo Corner_t604_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_CornerMethodDeclarations.h"
static const MethodInfo* Corner_t604_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Corner_t604_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Corner_t604_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Corner_t604_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Corner_t604_0_0_0;
extern const Il2CppType Corner_t604_1_0_0;
extern TypeInfo GridLayoutGroup_t607_il2cpp_TypeInfo;
extern const Il2CppType GridLayoutGroup_t607_0_0_0;
const Il2CppTypeDefinitionMetadata Corner_t604_DefinitionMetadata = 
{
	&GridLayoutGroup_t607_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Corner_t604_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Corner_t604_VTable/* vtableMethods */
	, Corner_t604_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 560/* fieldStart */

};
TypeInfo Corner_t604_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Corner"/* name */
	, ""/* namespaze */
	, Corner_t604_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Corner_t604_0_0_0/* byval_arg */
	, &Corner_t604_1_0_0/* this_arg */
	, &Corner_t604_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Corner_t604)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Corner_t604)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Axis
extern TypeInfo Axis_t605_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_AxisMethodDeclarations.h"
static const MethodInfo* Axis_t605_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Axis_t605_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Axis_t605_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Axis_t605_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Axis_t605_0_0_0;
extern const Il2CppType Axis_t605_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t605_DefinitionMetadata = 
{
	&GridLayoutGroup_t607_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t605_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Axis_t605_VTable/* vtableMethods */
	, Axis_t605_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 565/* fieldStart */

};
TypeInfo Axis_t605_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, Axis_t605_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t605_0_0_0/* byval_arg */
	, &Axis_t605_1_0_0/* this_arg */
	, &Axis_t605_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t605)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t605)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Constraint
extern TypeInfo Constraint_t606_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_ConstraintMethodDeclarations.h"
static const MethodInfo* Constraint_t606_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Constraint_t606_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool Constraint_t606_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Constraint_t606_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Constraint_t606_0_0_0;
extern const Il2CppType Constraint_t606_1_0_0;
const Il2CppTypeDefinitionMetadata Constraint_t606_DefinitionMetadata = 
{
	&GridLayoutGroup_t607_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Constraint_t606_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, Constraint_t606_VTable/* vtableMethods */
	, Constraint_t606_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 568/* fieldStart */

};
TypeInfo Constraint_t606_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Constraint"/* name */
	, ""/* namespaze */
	, Constraint_t606_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Constraint_t606_0_0_0/* byval_arg */
	, &Constraint_t606_1_0_0/* this_arg */
	, &Constraint_t606_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Constraint_t606)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Constraint_t606)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::.ctor()
extern const MethodInfo GridLayoutGroup__ctor_m2885_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GridLayoutGroup__ctor_m2885/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1075/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Corner_t604 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::get_startCorner()
extern const MethodInfo GridLayoutGroup_get_startCorner_m2886_MethodInfo = 
{
	"get_startCorner"/* name */
	, (methodPointerType)&GridLayoutGroup_get_startCorner_m2886/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Corner_t604_0_0_0/* return_type */
	, RuntimeInvoker_Corner_t604/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1076/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Corner_t604_0_0_0;
static const ParameterInfo GridLayoutGroup_t607_GridLayoutGroup_set_startCorner_m2887_ParameterInfos[] = 
{
	{"value", 0, 134218340, 0, &Corner_t604_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_startCorner(UnityEngine.UI.GridLayoutGroup/Corner)
extern const MethodInfo GridLayoutGroup_set_startCorner_m2887_MethodInfo = 
{
	"set_startCorner"/* name */
	, (methodPointerType)&GridLayoutGroup_set_startCorner_m2887/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, GridLayoutGroup_t607_GridLayoutGroup_set_startCorner_m2887_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1077/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Axis_t605 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::get_startAxis()
extern const MethodInfo GridLayoutGroup_get_startAxis_m2888_MethodInfo = 
{
	"get_startAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_get_startAxis_m2888/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t605_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t605/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1078/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Axis_t605_0_0_0;
static const ParameterInfo GridLayoutGroup_t607_GridLayoutGroup_set_startAxis_m2889_ParameterInfos[] = 
{
	{"value", 0, 134218341, 0, &Axis_t605_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_startAxis(UnityEngine.UI.GridLayoutGroup/Axis)
extern const MethodInfo GridLayoutGroup_set_startAxis_m2889_MethodInfo = 
{
	"set_startAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_set_startAxis_m2889/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, GridLayoutGroup_t607_GridLayoutGroup_set_startAxis_m2889_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1079/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_cellSize()
extern const MethodInfo GridLayoutGroup_get_cellSize_m2890_MethodInfo = 
{
	"get_cellSize"/* name */
	, (methodPointerType)&GridLayoutGroup_get_cellSize_m2890/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1080/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo GridLayoutGroup_t607_GridLayoutGroup_set_cellSize_m2891_ParameterInfos[] = 
{
	{"value", 0, 134218342, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_cellSize(UnityEngine.Vector2)
extern const MethodInfo GridLayoutGroup_set_cellSize_m2891_MethodInfo = 
{
	"set_cellSize"/* name */
	, (methodPointerType)&GridLayoutGroup_set_cellSize_m2891/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector2_t6/* invoker_method */
	, GridLayoutGroup_t607_GridLayoutGroup_set_cellSize_m2891_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1081/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_spacing()
extern const MethodInfo GridLayoutGroup_get_spacing_m2892_MethodInfo = 
{
	"get_spacing"/* name */
	, (methodPointerType)&GridLayoutGroup_get_spacing_m2892/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1082/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo GridLayoutGroup_t607_GridLayoutGroup_set_spacing_m2893_ParameterInfos[] = 
{
	{"value", 0, 134218343, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_spacing(UnityEngine.Vector2)
extern const MethodInfo GridLayoutGroup_set_spacing_m2893_MethodInfo = 
{
	"set_spacing"/* name */
	, (methodPointerType)&GridLayoutGroup_set_spacing_m2893/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector2_t6/* invoker_method */
	, GridLayoutGroup_t607_GridLayoutGroup_set_spacing_m2893_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1083/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Constraint_t606 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::get_constraint()
extern const MethodInfo GridLayoutGroup_get_constraint_m2894_MethodInfo = 
{
	"get_constraint"/* name */
	, (methodPointerType)&GridLayoutGroup_get_constraint_m2894/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Constraint_t606_0_0_0/* return_type */
	, RuntimeInvoker_Constraint_t606/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1084/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Constraint_t606_0_0_0;
static const ParameterInfo GridLayoutGroup_t607_GridLayoutGroup_set_constraint_m2895_ParameterInfos[] = 
{
	{"value", 0, 134218344, 0, &Constraint_t606_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraint(UnityEngine.UI.GridLayoutGroup/Constraint)
extern const MethodInfo GridLayoutGroup_set_constraint_m2895_MethodInfo = 
{
	"set_constraint"/* name */
	, (methodPointerType)&GridLayoutGroup_set_constraint_m2895/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, GridLayoutGroup_t607_GridLayoutGroup_set_constraint_m2895_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1085/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.GridLayoutGroup::get_constraintCount()
extern const MethodInfo GridLayoutGroup_get_constraintCount_m2896_MethodInfo = 
{
	"get_constraintCount"/* name */
	, (methodPointerType)&GridLayoutGroup_get_constraintCount_m2896/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1086/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo GridLayoutGroup_t607_GridLayoutGroup_set_constraintCount_m2897_ParameterInfos[] = 
{
	{"value", 0, 134218345, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraintCount(System.Int32)
extern const MethodInfo GridLayoutGroup_set_constraintCount_m2897_MethodInfo = 
{
	"set_constraintCount"/* name */
	, (methodPointerType)&GridLayoutGroup_set_constraintCount_m2897/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, GridLayoutGroup_t607_GridLayoutGroup_set_constraintCount_m2897_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1087/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputHorizontal_m2898_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&GridLayoutGroup_CalculateLayoutInputHorizontal_m2898/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1088/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputVertical_m2899_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&GridLayoutGroup_CalculateLayoutInputVertical_m2899/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1089/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo GridLayoutGroup_SetLayoutHorizontal_m2900_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&GridLayoutGroup_SetLayoutHorizontal_m2900/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1090/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutVertical()
extern const MethodInfo GridLayoutGroup_SetLayoutVertical_m2901_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&GridLayoutGroup_SetLayoutVertical_m2901/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1091/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo GridLayoutGroup_t607_GridLayoutGroup_SetCellsAlongAxis_m2902_ParameterInfos[] = 
{
	{"axis", 0, 134218346, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetCellsAlongAxis(System.Int32)
extern const MethodInfo GridLayoutGroup_SetCellsAlongAxis_m2902_MethodInfo = 
{
	"SetCellsAlongAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_SetCellsAlongAxis_m2902/* method */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, GridLayoutGroup_t607_GridLayoutGroup_SetCellsAlongAxis_m2902_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1092/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GridLayoutGroup_t607_MethodInfos[] =
{
	&GridLayoutGroup__ctor_m2885_MethodInfo,
	&GridLayoutGroup_get_startCorner_m2886_MethodInfo,
	&GridLayoutGroup_set_startCorner_m2887_MethodInfo,
	&GridLayoutGroup_get_startAxis_m2888_MethodInfo,
	&GridLayoutGroup_set_startAxis_m2889_MethodInfo,
	&GridLayoutGroup_get_cellSize_m2890_MethodInfo,
	&GridLayoutGroup_set_cellSize_m2891_MethodInfo,
	&GridLayoutGroup_get_spacing_m2892_MethodInfo,
	&GridLayoutGroup_set_spacing_m2893_MethodInfo,
	&GridLayoutGroup_get_constraint_m2894_MethodInfo,
	&GridLayoutGroup_set_constraint_m2895_MethodInfo,
	&GridLayoutGroup_get_constraintCount_m2896_MethodInfo,
	&GridLayoutGroup_set_constraintCount_m2897_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m2898_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m2899_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m2900_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m2901_MethodInfo,
	&GridLayoutGroup_SetCellsAlongAxis_m2902_MethodInfo,
	NULL
};
extern const MethodInfo GridLayoutGroup_get_startCorner_m2886_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_startCorner_m2887_MethodInfo;
static const PropertyInfo GridLayoutGroup_t607____startCorner_PropertyInfo = 
{
	&GridLayoutGroup_t607_il2cpp_TypeInfo/* parent */
	, "startCorner"/* name */
	, &GridLayoutGroup_get_startCorner_m2886_MethodInfo/* get */
	, &GridLayoutGroup_set_startCorner_m2887_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_startAxis_m2888_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_startAxis_m2889_MethodInfo;
static const PropertyInfo GridLayoutGroup_t607____startAxis_PropertyInfo = 
{
	&GridLayoutGroup_t607_il2cpp_TypeInfo/* parent */
	, "startAxis"/* name */
	, &GridLayoutGroup_get_startAxis_m2888_MethodInfo/* get */
	, &GridLayoutGroup_set_startAxis_m2889_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_cellSize_m2890_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_cellSize_m2891_MethodInfo;
static const PropertyInfo GridLayoutGroup_t607____cellSize_PropertyInfo = 
{
	&GridLayoutGroup_t607_il2cpp_TypeInfo/* parent */
	, "cellSize"/* name */
	, &GridLayoutGroup_get_cellSize_m2890_MethodInfo/* get */
	, &GridLayoutGroup_set_cellSize_m2891_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_spacing_m2892_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_spacing_m2893_MethodInfo;
static const PropertyInfo GridLayoutGroup_t607____spacing_PropertyInfo = 
{
	&GridLayoutGroup_t607_il2cpp_TypeInfo/* parent */
	, "spacing"/* name */
	, &GridLayoutGroup_get_spacing_m2892_MethodInfo/* get */
	, &GridLayoutGroup_set_spacing_m2893_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_constraint_m2894_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_constraint_m2895_MethodInfo;
static const PropertyInfo GridLayoutGroup_t607____constraint_PropertyInfo = 
{
	&GridLayoutGroup_t607_il2cpp_TypeInfo/* parent */
	, "constraint"/* name */
	, &GridLayoutGroup_get_constraint_m2894_MethodInfo/* get */
	, &GridLayoutGroup_set_constraint_m2895_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_constraintCount_m2896_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_constraintCount_m2897_MethodInfo;
static const PropertyInfo GridLayoutGroup_t607____constraintCount_PropertyInfo = 
{
	&GridLayoutGroup_t607_il2cpp_TypeInfo/* parent */
	, "constraintCount"/* name */
	, &GridLayoutGroup_get_constraintCount_m2896_MethodInfo/* get */
	, &GridLayoutGroup_set_constraintCount_m2897_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* GridLayoutGroup_t607_PropertyInfos[] =
{
	&GridLayoutGroup_t607____startCorner_PropertyInfo,
	&GridLayoutGroup_t607____startAxis_PropertyInfo,
	&GridLayoutGroup_t607____cellSize_PropertyInfo,
	&GridLayoutGroup_t607____spacing_PropertyInfo,
	&GridLayoutGroup_t607____constraint_PropertyInfo,
	&GridLayoutGroup_t607____constraintCount_PropertyInfo,
	NULL
};
static const Il2CppType* GridLayoutGroup_t607_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Corner_t604_0_0_0,
	&Axis_t605_0_0_0,
	&Constraint_t606_0_0_0,
};
extern const MethodInfo LayoutGroup_OnEnable_m2956_MethodInfo;
extern const MethodInfo LayoutGroup_OnDisable_m2957_MethodInfo;
extern const MethodInfo LayoutGroup_OnRectTransformDimensionsChange_m2966_MethodInfo;
extern const MethodInfo LayoutGroup_OnDidApplyAnimationProperties_m2958_MethodInfo;
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputHorizontal_m2898_MethodInfo;
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputVertical_m2899_MethodInfo;
extern const MethodInfo LayoutGroup_get_minWidth_m2949_MethodInfo;
extern const MethodInfo LayoutGroup_get_preferredWidth_m2950_MethodInfo;
extern const MethodInfo LayoutGroup_get_flexibleWidth_m2951_MethodInfo;
extern const MethodInfo LayoutGroup_get_minHeight_m2952_MethodInfo;
extern const MethodInfo LayoutGroup_get_preferredHeight_m2953_MethodInfo;
extern const MethodInfo LayoutGroup_get_flexibleHeight_m2954_MethodInfo;
extern const MethodInfo LayoutGroup_get_layoutPriority_m2955_MethodInfo;
extern const MethodInfo GridLayoutGroup_SetLayoutHorizontal_m2900_MethodInfo;
extern const MethodInfo GridLayoutGroup_SetLayoutVertical_m2901_MethodInfo;
extern const MethodInfo LayoutGroup_OnTransformChildrenChanged_m2967_MethodInfo;
static const Il2CppMethodReference GridLayoutGroup_t607_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&LayoutGroup_OnEnable_m2956_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&LayoutGroup_OnDisable_m2957_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m2966_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m2958_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m2898_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m2899_MethodInfo,
	&LayoutGroup_get_minWidth_m2949_MethodInfo,
	&LayoutGroup_get_preferredWidth_m2950_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m2951_MethodInfo,
	&LayoutGroup_get_minHeight_m2952_MethodInfo,
	&LayoutGroup_get_preferredHeight_m2953_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m2954_MethodInfo,
	&LayoutGroup_get_layoutPriority_m2955_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m2900_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m2901_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m2898_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m2899_MethodInfo,
	&LayoutGroup_get_minWidth_m2949_MethodInfo,
	&LayoutGroup_get_preferredWidth_m2950_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m2951_MethodInfo,
	&LayoutGroup_get_minHeight_m2952_MethodInfo,
	&LayoutGroup_get_preferredHeight_m2953_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m2954_MethodInfo,
	&LayoutGroup_get_layoutPriority_m2955_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m2900_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m2901_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m2967_MethodInfo,
};
static bool GridLayoutGroup_t607_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutGroup_t703_0_0_0;
static Il2CppInterfaceOffsetPair GridLayoutGroup_t607_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t652_0_0_0, 16},
	{ &ILayoutController_t705_0_0_0, 25},
	{ &ILayoutGroup_t703_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType GridLayoutGroup_t607_1_0_0;
extern const Il2CppType LayoutGroup_t608_0_0_0;
struct GridLayoutGroup_t607;
const Il2CppTypeDefinitionMetadata GridLayoutGroup_t607_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GridLayoutGroup_t607_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GridLayoutGroup_t607_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t608_0_0_0/* parent */
	, GridLayoutGroup_t607_VTable/* vtableMethods */
	, GridLayoutGroup_t607_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 572/* fieldStart */

};
TypeInfo GridLayoutGroup_t607_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "GridLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, GridLayoutGroup_t607_MethodInfos/* methods */
	, GridLayoutGroup_t607_PropertyInfos/* properties */
	, NULL/* events */
	, &GridLayoutGroup_t607_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 284/* custom_attributes_cache */
	, &GridLayoutGroup_t607_0_0_0/* byval_arg */
	, &GridLayoutGroup_t607_1_0_0/* this_arg */
	, &GridLayoutGroup_t607_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GridLayoutGroup_t607)/* instance_size */
	, sizeof (GridLayoutGroup_t607)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.HorizontalLayoutGroup
extern TypeInfo HorizontalLayoutGroup_t609_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::.ctor()
extern const MethodInfo HorizontalLayoutGroup__ctor_m2903_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HorizontalLayoutGroup__ctor_m2903/* method */
	, &HorizontalLayoutGroup_t609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1093/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m2904_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m2904/* method */
	, &HorizontalLayoutGroup_t609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1094/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputVertical_m2905_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_CalculateLayoutInputVertical_m2905/* method */
	, &HorizontalLayoutGroup_t609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1095/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo HorizontalLayoutGroup_SetLayoutHorizontal_m2906_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_SetLayoutHorizontal_m2906/* method */
	, &HorizontalLayoutGroup_t609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1096/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutVertical()
extern const MethodInfo HorizontalLayoutGroup_SetLayoutVertical_m2907_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_SetLayoutVertical_m2907/* method */
	, &HorizontalLayoutGroup_t609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1097/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HorizontalLayoutGroup_t609_MethodInfos[] =
{
	&HorizontalLayoutGroup__ctor_m2903_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m2904_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m2905_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m2906_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m2907_MethodInfo,
	NULL
};
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m2904_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputVertical_m2905_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_SetLayoutHorizontal_m2906_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_SetLayoutVertical_m2907_MethodInfo;
static const Il2CppMethodReference HorizontalLayoutGroup_t609_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&LayoutGroup_OnEnable_m2956_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&LayoutGroup_OnDisable_m2957_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m2966_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m2958_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m2904_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m2905_MethodInfo,
	&LayoutGroup_get_minWidth_m2949_MethodInfo,
	&LayoutGroup_get_preferredWidth_m2950_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m2951_MethodInfo,
	&LayoutGroup_get_minHeight_m2952_MethodInfo,
	&LayoutGroup_get_preferredHeight_m2953_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m2954_MethodInfo,
	&LayoutGroup_get_layoutPriority_m2955_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m2906_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m2907_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m2904_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m2905_MethodInfo,
	&LayoutGroup_get_minWidth_m2949_MethodInfo,
	&LayoutGroup_get_preferredWidth_m2950_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m2951_MethodInfo,
	&LayoutGroup_get_minHeight_m2952_MethodInfo,
	&LayoutGroup_get_preferredHeight_m2953_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m2954_MethodInfo,
	&LayoutGroup_get_layoutPriority_m2955_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m2906_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m2907_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m2967_MethodInfo,
};
static bool HorizontalLayoutGroup_t609_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HorizontalLayoutGroup_t609_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t652_0_0_0, 16},
	{ &ILayoutController_t705_0_0_0, 25},
	{ &ILayoutGroup_t703_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType HorizontalLayoutGroup_t609_0_0_0;
extern const Il2CppType HorizontalLayoutGroup_t609_1_0_0;
extern const Il2CppType HorizontalOrVerticalLayoutGroup_t610_0_0_0;
struct HorizontalLayoutGroup_t609;
const Il2CppTypeDefinitionMetadata HorizontalLayoutGroup_t609_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalLayoutGroup_t609_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t610_0_0_0/* parent */
	, HorizontalLayoutGroup_t609_VTable/* vtableMethods */
	, HorizontalLayoutGroup_t609_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HorizontalLayoutGroup_t609_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, HorizontalLayoutGroup_t609_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HorizontalLayoutGroup_t609_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 291/* custom_attributes_cache */
	, &HorizontalLayoutGroup_t609_0_0_0/* byval_arg */
	, &HorizontalLayoutGroup_t609_1_0_0/* this_arg */
	, &HorizontalLayoutGroup_t609_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalLayoutGroup_t609)/* instance_size */
	, sizeof (HorizontalLayoutGroup_t609)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrou.h"
// Metadata Definition UnityEngine.UI.HorizontalOrVerticalLayoutGroup
extern TypeInfo HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
extern const MethodInfo HorizontalOrVerticalLayoutGroup__ctor_m2908_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup__ctor_m2908/* method */
	, &HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1098/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_spacing_m2909_MethodInfo = 
{
	"get_spacing"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_spacing_m2909/* method */
	, &HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1099/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t610_HorizontalOrVerticalLayoutGroup_set_spacing_m2910_ParameterInfos[] = 
{
	{"value", 0, 134218347, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_spacing_m2910_MethodInfo = 
{
	"set_spacing"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_spacing_m2910/* method */
	, &HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t610_HorizontalOrVerticalLayoutGroup_set_spacing_m2910_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m2911_MethodInfo = 
{
	"get_childForceExpandWidth"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m2911/* method */
	, &HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t610_HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m2912_ParameterInfos[] = 
{
	{"value", 0, 134218348, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m2912_MethodInfo = 
{
	"set_childForceExpandWidth"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m2912/* method */
	, &HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t610_HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m2912_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m2913_MethodInfo = 
{
	"get_childForceExpandHeight"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m2913/* method */
	, &HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t610_HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m2914_ParameterInfos[] = 
{
	{"value", 0, 134218349, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m2914_MethodInfo = 
{
	"set_childForceExpandHeight"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m2914/* method */
	, &HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t610_HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m2914_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t610_HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m2915_ParameterInfos[] = 
{
	{"axis", 0, 134218350, 0, &Int32_t253_0_0_0},
	{"isVertical", 1, 134218351, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m2915_MethodInfo = 
{
	"CalcAlongAxis"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m2915/* method */
	, &HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_SByte_t274/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t610_HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m2915_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t610_HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m2916_ParameterInfos[] = 
{
	{"axis", 0, 134218352, 0, &Int32_t253_0_0_0},
	{"isVertical", 1, 134218353, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m2916_MethodInfo = 
{
	"SetChildrenAlongAxis"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m2916/* method */
	, &HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_SByte_t274/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t610_HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m2916_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HorizontalOrVerticalLayoutGroup_t610_MethodInfos[] =
{
	&HorizontalOrVerticalLayoutGroup__ctor_m2908_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_spacing_m2909_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_spacing_m2910_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m2911_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m2912_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m2913_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m2914_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m2915_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m2916_MethodInfo,
	NULL
};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_spacing_m2909_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_spacing_m2910_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t610____spacing_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* parent */
	, "spacing"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_spacing_m2909_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_spacing_m2910_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m2911_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m2912_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t610____childForceExpandWidth_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* parent */
	, "childForceExpandWidth"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m2911_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m2912_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m2913_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m2914_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t610____childForceExpandHeight_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* parent */
	, "childForceExpandHeight"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m2913_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m2914_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* HorizontalOrVerticalLayoutGroup_t610_PropertyInfos[] =
{
	&HorizontalOrVerticalLayoutGroup_t610____spacing_PropertyInfo,
	&HorizontalOrVerticalLayoutGroup_t610____childForceExpandWidth_PropertyInfo,
	&HorizontalOrVerticalLayoutGroup_t610____childForceExpandHeight_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutGroup_CalculateLayoutInputHorizontal_m2948_MethodInfo;
extern const MethodInfo LayoutGroup_CalculateLayoutInputVertical_m3550_MethodInfo;
extern const MethodInfo LayoutGroup_SetLayoutHorizontal_m3551_MethodInfo;
extern const MethodInfo LayoutGroup_SetLayoutVertical_m3552_MethodInfo;
static const Il2CppMethodReference HorizontalOrVerticalLayoutGroup_t610_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&LayoutGroup_OnEnable_m2956_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&LayoutGroup_OnDisable_m2957_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m2966_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m2958_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m2948_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m3550_MethodInfo,
	&LayoutGroup_get_minWidth_m2949_MethodInfo,
	&LayoutGroup_get_preferredWidth_m2950_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m2951_MethodInfo,
	&LayoutGroup_get_minHeight_m2952_MethodInfo,
	&LayoutGroup_get_preferredHeight_m2953_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m2954_MethodInfo,
	&LayoutGroup_get_layoutPriority_m2955_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m3551_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m3552_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m2948_MethodInfo,
	NULL,
	&LayoutGroup_get_minWidth_m2949_MethodInfo,
	&LayoutGroup_get_preferredWidth_m2950_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m2951_MethodInfo,
	&LayoutGroup_get_minHeight_m2952_MethodInfo,
	&LayoutGroup_get_preferredHeight_m2953_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m2954_MethodInfo,
	&LayoutGroup_get_layoutPriority_m2955_MethodInfo,
	NULL,
	NULL,
	&LayoutGroup_OnTransformChildrenChanged_m2967_MethodInfo,
};
static bool HorizontalOrVerticalLayoutGroup_t610_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HorizontalOrVerticalLayoutGroup_t610_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t652_0_0_0, 16},
	{ &ILayoutController_t705_0_0_0, 25},
	{ &ILayoutGroup_t703_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType HorizontalOrVerticalLayoutGroup_t610_1_0_0;
struct HorizontalOrVerticalLayoutGroup_t610;
const Il2CppTypeDefinitionMetadata HorizontalOrVerticalLayoutGroup_t610_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalOrVerticalLayoutGroup_t610_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t608_0_0_0/* parent */
	, HorizontalOrVerticalLayoutGroup_t610_VTable/* vtableMethods */
	, HorizontalOrVerticalLayoutGroup_t610_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 578/* fieldStart */

};
TypeInfo HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalOrVerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, HorizontalOrVerticalLayoutGroup_t610_MethodInfos/* methods */
	, HorizontalOrVerticalLayoutGroup_t610_PropertyInfos/* properties */
	, NULL/* events */
	, &HorizontalOrVerticalLayoutGroup_t610_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HorizontalOrVerticalLayoutGroup_t610_0_0_0/* byval_arg */
	, &HorizontalOrVerticalLayoutGroup_t610_1_0_0/* this_arg */
	, &HorizontalOrVerticalLayoutGroup_t610_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalOrVerticalLayoutGroup_t610)/* instance_size */
	, sizeof (HorizontalOrVerticalLayoutGroup_t610)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutElement
extern TypeInfo ILayoutElement_t652_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputHorizontal()
extern const MethodInfo ILayoutElement_CalculateLayoutInputHorizontal_m3538_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, NULL/* method */
	, &ILayoutElement_t652_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputVertical()
extern const MethodInfo ILayoutElement_CalculateLayoutInputVertical_m3539_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, NULL/* method */
	, &ILayoutElement_t652_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_minWidth()
extern const MethodInfo ILayoutElement_get_minWidth_m3540_MethodInfo = 
{
	"get_minWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t652_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_preferredWidth()
extern const MethodInfo ILayoutElement_get_preferredWidth_m3541_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t652_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_flexibleWidth()
extern const MethodInfo ILayoutElement_get_flexibleWidth_m3542_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t652_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_minHeight()
extern const MethodInfo ILayoutElement_get_minHeight_m3543_MethodInfo = 
{
	"get_minHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t652_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_preferredHeight()
extern const MethodInfo ILayoutElement_get_preferredHeight_m3544_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t652_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_flexibleHeight()
extern const MethodInfo ILayoutElement_get_flexibleHeight_m3545_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t652_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.ILayoutElement::get_layoutPriority()
extern const MethodInfo ILayoutElement_get_layoutPriority_m3546_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, NULL/* method */
	, &ILayoutElement_t652_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutElement_t652_MethodInfos[] =
{
	&ILayoutElement_CalculateLayoutInputHorizontal_m3538_MethodInfo,
	&ILayoutElement_CalculateLayoutInputVertical_m3539_MethodInfo,
	&ILayoutElement_get_minWidth_m3540_MethodInfo,
	&ILayoutElement_get_preferredWidth_m3541_MethodInfo,
	&ILayoutElement_get_flexibleWidth_m3542_MethodInfo,
	&ILayoutElement_get_minHeight_m3543_MethodInfo,
	&ILayoutElement_get_preferredHeight_m3544_MethodInfo,
	&ILayoutElement_get_flexibleHeight_m3545_MethodInfo,
	&ILayoutElement_get_layoutPriority_m3546_MethodInfo,
	NULL
};
extern const MethodInfo ILayoutElement_get_minWidth_m3540_MethodInfo;
static const PropertyInfo ILayoutElement_t652____minWidth_PropertyInfo = 
{
	&ILayoutElement_t652_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &ILayoutElement_get_minWidth_m3540_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_preferredWidth_m3541_MethodInfo;
static const PropertyInfo ILayoutElement_t652____preferredWidth_PropertyInfo = 
{
	&ILayoutElement_t652_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &ILayoutElement_get_preferredWidth_m3541_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_flexibleWidth_m3542_MethodInfo;
static const PropertyInfo ILayoutElement_t652____flexibleWidth_PropertyInfo = 
{
	&ILayoutElement_t652_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &ILayoutElement_get_flexibleWidth_m3542_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_minHeight_m3543_MethodInfo;
static const PropertyInfo ILayoutElement_t652____minHeight_PropertyInfo = 
{
	&ILayoutElement_t652_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &ILayoutElement_get_minHeight_m3543_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_preferredHeight_m3544_MethodInfo;
static const PropertyInfo ILayoutElement_t652____preferredHeight_PropertyInfo = 
{
	&ILayoutElement_t652_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &ILayoutElement_get_preferredHeight_m3544_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_flexibleHeight_m3545_MethodInfo;
static const PropertyInfo ILayoutElement_t652____flexibleHeight_PropertyInfo = 
{
	&ILayoutElement_t652_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &ILayoutElement_get_flexibleHeight_m3545_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_layoutPriority_m3546_MethodInfo;
static const PropertyInfo ILayoutElement_t652____layoutPriority_PropertyInfo = 
{
	&ILayoutElement_t652_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &ILayoutElement_get_layoutPriority_m3546_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILayoutElement_t652_PropertyInfos[] =
{
	&ILayoutElement_t652____minWidth_PropertyInfo,
	&ILayoutElement_t652____preferredWidth_PropertyInfo,
	&ILayoutElement_t652____flexibleWidth_PropertyInfo,
	&ILayoutElement_t652____minHeight_PropertyInfo,
	&ILayoutElement_t652____preferredHeight_PropertyInfo,
	&ILayoutElement_t652____flexibleHeight_PropertyInfo,
	&ILayoutElement_t652____layoutPriority_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutElement_t652_1_0_0;
struct ILayoutElement_t652;
const Il2CppTypeDefinitionMetadata ILayoutElement_t652_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutElement_t652_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutElement_t652_MethodInfos/* methods */
	, ILayoutElement_t652_PropertyInfos/* properties */
	, NULL/* events */
	, &ILayoutElement_t652_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutElement_t652_0_0_0/* byval_arg */
	, &ILayoutElement_t652_1_0_0/* this_arg */
	, &ILayoutElement_t652_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutController
extern TypeInfo ILayoutController_t705_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutController::SetLayoutHorizontal()
extern const MethodInfo ILayoutController_SetLayoutHorizontal_m3547_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, NULL/* method */
	, &ILayoutController_t705_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutController::SetLayoutVertical()
extern const MethodInfo ILayoutController_SetLayoutVertical_m3548_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, NULL/* method */
	, &ILayoutController_t705_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutController_t705_MethodInfos[] =
{
	&ILayoutController_SetLayoutHorizontal_m3547_MethodInfo,
	&ILayoutController_SetLayoutVertical_m3548_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutController_t705_1_0_0;
struct ILayoutController_t705;
const Il2CppTypeDefinitionMetadata ILayoutController_t705_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutController_t705_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutController_t705_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutController_t705_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutController_t705_0_0_0/* byval_arg */
	, &ILayoutController_t705_1_0_0/* this_arg */
	, &ILayoutController_t705_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutGroup
extern TypeInfo ILayoutGroup_t703_il2cpp_TypeInfo;
static const MethodInfo* ILayoutGroup_t703_MethodInfos[] =
{
	NULL
};
static const Il2CppType* ILayoutGroup_t703_InterfacesTypeInfos[] = 
{
	&ILayoutController_t705_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutGroup_t703_1_0_0;
struct ILayoutGroup_t703;
const Il2CppTypeDefinitionMetadata ILayoutGroup_t703_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutGroup_t703_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutGroup_t703_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutGroup_t703_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutGroup_t703_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutGroup_t703_0_0_0/* byval_arg */
	, &ILayoutGroup_t703_1_0_0/* this_arg */
	, &ILayoutGroup_t703_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutSelfController
extern TypeInfo ILayoutSelfController_t706_il2cpp_TypeInfo;
static const MethodInfo* ILayoutSelfController_t706_MethodInfos[] =
{
	NULL
};
static const Il2CppType* ILayoutSelfController_t706_InterfacesTypeInfos[] = 
{
	&ILayoutController_t705_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutSelfController_t706_1_0_0;
struct ILayoutSelfController_t706;
const Il2CppTypeDefinitionMetadata ILayoutSelfController_t706_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutSelfController_t706_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutSelfController_t706_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutSelfController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutSelfController_t706_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutSelfController_t706_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutSelfController_t706_0_0_0/* byval_arg */
	, &ILayoutSelfController_t706_1_0_0/* this_arg */
	, &ILayoutSelfController_t706_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutIgnorer
extern TypeInfo ILayoutIgnorer_t702_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ILayoutIgnorer::get_ignoreLayout()
extern const MethodInfo ILayoutIgnorer_get_ignoreLayout_m3549_MethodInfo = 
{
	"get_ignoreLayout"/* name */
	, NULL/* method */
	, &ILayoutIgnorer_t702_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutIgnorer_t702_MethodInfos[] =
{
	&ILayoutIgnorer_get_ignoreLayout_m3549_MethodInfo,
	NULL
};
extern const MethodInfo ILayoutIgnorer_get_ignoreLayout_m3549_MethodInfo;
static const PropertyInfo ILayoutIgnorer_t702____ignoreLayout_PropertyInfo = 
{
	&ILayoutIgnorer_t702_il2cpp_TypeInfo/* parent */
	, "ignoreLayout"/* name */
	, &ILayoutIgnorer_get_ignoreLayout_m3549_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILayoutIgnorer_t702_PropertyInfos[] =
{
	&ILayoutIgnorer_t702____ignoreLayout_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutIgnorer_t702_0_0_0;
extern const Il2CppType ILayoutIgnorer_t702_1_0_0;
struct ILayoutIgnorer_t702;
const Il2CppTypeDefinitionMetadata ILayoutIgnorer_t702_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutIgnorer_t702_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutIgnorer"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutIgnorer_t702_MethodInfos/* methods */
	, ILayoutIgnorer_t702_PropertyInfos/* properties */
	, NULL/* events */
	, &ILayoutIgnorer_t702_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutIgnorer_t702_0_0_0/* byval_arg */
	, &ILayoutIgnorer_t702_1_0_0/* this_arg */
	, &ILayoutIgnorer_t702_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement.h"
// Metadata Definition UnityEngine.UI.LayoutElement
extern TypeInfo LayoutElement_t611_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElementMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::.ctor()
extern const MethodInfo LayoutElement__ctor_m2917_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutElement__ctor_m2917/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutElement::get_ignoreLayout()
extern const MethodInfo LayoutElement_get_ignoreLayout_m2918_MethodInfo = 
{
	"get_ignoreLayout"/* name */
	, (methodPointerType)&LayoutElement_get_ignoreLayout_m2918/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo LayoutElement_t611_LayoutElement_set_ignoreLayout_m2919_ParameterInfos[] = 
{
	{"value", 0, 134218354, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_ignoreLayout(System.Boolean)
extern const MethodInfo LayoutElement_set_ignoreLayout_m2919_MethodInfo = 
{
	"set_ignoreLayout"/* name */
	, (methodPointerType)&LayoutElement_set_ignoreLayout_m2919/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, LayoutElement_t611_LayoutElement_set_ignoreLayout_m2919_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputHorizontal()
extern const MethodInfo LayoutElement_CalculateLayoutInputHorizontal_m2920_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&LayoutElement_CalculateLayoutInputHorizontal_m2920/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputVertical()
extern const MethodInfo LayoutElement_CalculateLayoutInputVertical_m2921_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&LayoutElement_CalculateLayoutInputVertical_m2921/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_minWidth()
extern const MethodInfo LayoutElement_get_minWidth_m2922_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&LayoutElement_get_minWidth_m2922/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo LayoutElement_t611_LayoutElement_set_minWidth_m2923_ParameterInfos[] = 
{
	{"value", 0, 134218355, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single)
extern const MethodInfo LayoutElement_set_minWidth_m2923_MethodInfo = 
{
	"set_minWidth"/* name */
	, (methodPointerType)&LayoutElement_set_minWidth_m2923/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, LayoutElement_t611_LayoutElement_set_minWidth_m2923_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_minHeight()
extern const MethodInfo LayoutElement_get_minHeight_m2924_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&LayoutElement_get_minHeight_m2924/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo LayoutElement_t611_LayoutElement_set_minHeight_m2925_ParameterInfos[] = 
{
	{"value", 0, 134218356, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single)
extern const MethodInfo LayoutElement_set_minHeight_m2925_MethodInfo = 
{
	"set_minHeight"/* name */
	, (methodPointerType)&LayoutElement_set_minHeight_m2925/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, LayoutElement_t611_LayoutElement_set_minHeight_m2925_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_preferredWidth()
extern const MethodInfo LayoutElement_get_preferredWidth_m2926_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&LayoutElement_get_preferredWidth_m2926/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo LayoutElement_t611_LayoutElement_set_preferredWidth_m2927_ParameterInfos[] = 
{
	{"value", 0, 134218357, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single)
extern const MethodInfo LayoutElement_set_preferredWidth_m2927_MethodInfo = 
{
	"set_preferredWidth"/* name */
	, (methodPointerType)&LayoutElement_set_preferredWidth_m2927/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, LayoutElement_t611_LayoutElement_set_preferredWidth_m2927_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_preferredHeight()
extern const MethodInfo LayoutElement_get_preferredHeight_m2928_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&LayoutElement_get_preferredHeight_m2928/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo LayoutElement_t611_LayoutElement_set_preferredHeight_m2929_ParameterInfos[] = 
{
	{"value", 0, 134218358, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single)
extern const MethodInfo LayoutElement_set_preferredHeight_m2929_MethodInfo = 
{
	"set_preferredHeight"/* name */
	, (methodPointerType)&LayoutElement_set_preferredHeight_m2929/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, LayoutElement_t611_LayoutElement_set_preferredHeight_m2929_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth()
extern const MethodInfo LayoutElement_get_flexibleWidth_m2930_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&LayoutElement_get_flexibleWidth_m2930/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo LayoutElement_t611_LayoutElement_set_flexibleWidth_m2931_ParameterInfos[] = 
{
	{"value", 0, 134218359, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single)
extern const MethodInfo LayoutElement_set_flexibleWidth_m2931_MethodInfo = 
{
	"set_flexibleWidth"/* name */
	, (methodPointerType)&LayoutElement_set_flexibleWidth_m2931/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, LayoutElement_t611_LayoutElement_set_flexibleWidth_m2931_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 39/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight()
extern const MethodInfo LayoutElement_get_flexibleHeight_m2932_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&LayoutElement_get_flexibleHeight_m2932/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 40/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo LayoutElement_t611_LayoutElement_set_flexibleHeight_m2933_ParameterInfos[] = 
{
	{"value", 0, 134218360, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single)
extern const MethodInfo LayoutElement_set_flexibleHeight_m2933_MethodInfo = 
{
	"set_flexibleHeight"/* name */
	, (methodPointerType)&LayoutElement_set_flexibleHeight_m2933/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254/* invoker_method */
	, LayoutElement_t611_LayoutElement_set_flexibleHeight_m2933_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 41/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutElement::get_layoutPriority()
extern const MethodInfo LayoutElement_get_layoutPriority_m2934_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&LayoutElement_get_layoutPriority_m2934/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 42/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnEnable()
extern const MethodInfo LayoutElement_OnEnable_m2935_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&LayoutElement_OnEnable_m2935/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnTransformParentChanged()
extern const MethodInfo LayoutElement_OnTransformParentChanged_m2936_MethodInfo = 
{
	"OnTransformParentChanged"/* name */
	, (methodPointerType)&LayoutElement_OnTransformParentChanged_m2936/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnDisable()
extern const MethodInfo LayoutElement_OnDisable_m2937_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&LayoutElement_OnDisable_m2937/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnDidApplyAnimationProperties()
extern const MethodInfo LayoutElement_OnDidApplyAnimationProperties_m2938_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&LayoutElement_OnDidApplyAnimationProperties_m2938/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnBeforeTransformParentChanged()
extern const MethodInfo LayoutElement_OnBeforeTransformParentChanged_m2939_MethodInfo = 
{
	"OnBeforeTransformParentChanged"/* name */
	, (methodPointerType)&LayoutElement_OnBeforeTransformParentChanged_m2939/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::SetDirty()
extern const MethodInfo LayoutElement_SetDirty_m2940_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&LayoutElement_SetDirty_m2940/* method */
	, &LayoutElement_t611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutElement_t611_MethodInfos[] =
{
	&LayoutElement__ctor_m2917_MethodInfo,
	&LayoutElement_get_ignoreLayout_m2918_MethodInfo,
	&LayoutElement_set_ignoreLayout_m2919_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m2920_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m2921_MethodInfo,
	&LayoutElement_get_minWidth_m2922_MethodInfo,
	&LayoutElement_set_minWidth_m2923_MethodInfo,
	&LayoutElement_get_minHeight_m2924_MethodInfo,
	&LayoutElement_set_minHeight_m2925_MethodInfo,
	&LayoutElement_get_preferredWidth_m2926_MethodInfo,
	&LayoutElement_set_preferredWidth_m2927_MethodInfo,
	&LayoutElement_get_preferredHeight_m2928_MethodInfo,
	&LayoutElement_set_preferredHeight_m2929_MethodInfo,
	&LayoutElement_get_flexibleWidth_m2930_MethodInfo,
	&LayoutElement_set_flexibleWidth_m2931_MethodInfo,
	&LayoutElement_get_flexibleHeight_m2932_MethodInfo,
	&LayoutElement_set_flexibleHeight_m2933_MethodInfo,
	&LayoutElement_get_layoutPriority_m2934_MethodInfo,
	&LayoutElement_OnEnable_m2935_MethodInfo,
	&LayoutElement_OnTransformParentChanged_m2936_MethodInfo,
	&LayoutElement_OnDisable_m2937_MethodInfo,
	&LayoutElement_OnDidApplyAnimationProperties_m2938_MethodInfo,
	&LayoutElement_OnBeforeTransformParentChanged_m2939_MethodInfo,
	&LayoutElement_SetDirty_m2940_MethodInfo,
	NULL
};
extern const MethodInfo LayoutElement_get_ignoreLayout_m2918_MethodInfo;
extern const MethodInfo LayoutElement_set_ignoreLayout_m2919_MethodInfo;
static const PropertyInfo LayoutElement_t611____ignoreLayout_PropertyInfo = 
{
	&LayoutElement_t611_il2cpp_TypeInfo/* parent */
	, "ignoreLayout"/* name */
	, &LayoutElement_get_ignoreLayout_m2918_MethodInfo/* get */
	, &LayoutElement_set_ignoreLayout_m2919_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_minWidth_m2922_MethodInfo;
extern const MethodInfo LayoutElement_set_minWidth_m2923_MethodInfo;
static const PropertyInfo LayoutElement_t611____minWidth_PropertyInfo = 
{
	&LayoutElement_t611_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &LayoutElement_get_minWidth_m2922_MethodInfo/* get */
	, &LayoutElement_set_minWidth_m2923_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_minHeight_m2924_MethodInfo;
extern const MethodInfo LayoutElement_set_minHeight_m2925_MethodInfo;
static const PropertyInfo LayoutElement_t611____minHeight_PropertyInfo = 
{
	&LayoutElement_t611_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &LayoutElement_get_minHeight_m2924_MethodInfo/* get */
	, &LayoutElement_set_minHeight_m2925_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_preferredWidth_m2926_MethodInfo;
extern const MethodInfo LayoutElement_set_preferredWidth_m2927_MethodInfo;
static const PropertyInfo LayoutElement_t611____preferredWidth_PropertyInfo = 
{
	&LayoutElement_t611_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &LayoutElement_get_preferredWidth_m2926_MethodInfo/* get */
	, &LayoutElement_set_preferredWidth_m2927_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_preferredHeight_m2928_MethodInfo;
extern const MethodInfo LayoutElement_set_preferredHeight_m2929_MethodInfo;
static const PropertyInfo LayoutElement_t611____preferredHeight_PropertyInfo = 
{
	&LayoutElement_t611_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &LayoutElement_get_preferredHeight_m2928_MethodInfo/* get */
	, &LayoutElement_set_preferredHeight_m2929_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_flexibleWidth_m2930_MethodInfo;
extern const MethodInfo LayoutElement_set_flexibleWidth_m2931_MethodInfo;
static const PropertyInfo LayoutElement_t611____flexibleWidth_PropertyInfo = 
{
	&LayoutElement_t611_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &LayoutElement_get_flexibleWidth_m2930_MethodInfo/* get */
	, &LayoutElement_set_flexibleWidth_m2931_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_flexibleHeight_m2932_MethodInfo;
extern const MethodInfo LayoutElement_set_flexibleHeight_m2933_MethodInfo;
static const PropertyInfo LayoutElement_t611____flexibleHeight_PropertyInfo = 
{
	&LayoutElement_t611_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &LayoutElement_get_flexibleHeight_m2932_MethodInfo/* get */
	, &LayoutElement_set_flexibleHeight_m2933_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_layoutPriority_m2934_MethodInfo;
static const PropertyInfo LayoutElement_t611____layoutPriority_PropertyInfo = 
{
	&LayoutElement_t611_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &LayoutElement_get_layoutPriority_m2934_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutElement_t611_PropertyInfos[] =
{
	&LayoutElement_t611____ignoreLayout_PropertyInfo,
	&LayoutElement_t611____minWidth_PropertyInfo,
	&LayoutElement_t611____minHeight_PropertyInfo,
	&LayoutElement_t611____preferredWidth_PropertyInfo,
	&LayoutElement_t611____preferredHeight_PropertyInfo,
	&LayoutElement_t611____flexibleWidth_PropertyInfo,
	&LayoutElement_t611____flexibleHeight_PropertyInfo,
	&LayoutElement_t611____layoutPriority_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutElement_OnEnable_m2935_MethodInfo;
extern const MethodInfo LayoutElement_OnDisable_m2937_MethodInfo;
extern const MethodInfo LayoutElement_OnBeforeTransformParentChanged_m2939_MethodInfo;
extern const MethodInfo LayoutElement_OnTransformParentChanged_m2936_MethodInfo;
extern const MethodInfo LayoutElement_OnDidApplyAnimationProperties_m2938_MethodInfo;
extern const MethodInfo LayoutElement_CalculateLayoutInputHorizontal_m2920_MethodInfo;
extern const MethodInfo LayoutElement_CalculateLayoutInputVertical_m2921_MethodInfo;
static const Il2CppMethodReference LayoutElement_t611_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&LayoutElement_OnEnable_m2935_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&LayoutElement_OnDisable_m2937_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo,
	&LayoutElement_OnBeforeTransformParentChanged_m2939_MethodInfo,
	&LayoutElement_OnTransformParentChanged_m2936_MethodInfo,
	&LayoutElement_OnDidApplyAnimationProperties_m2938_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m2920_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m2921_MethodInfo,
	&LayoutElement_get_minWidth_m2922_MethodInfo,
	&LayoutElement_get_preferredWidth_m2926_MethodInfo,
	&LayoutElement_get_flexibleWidth_m2930_MethodInfo,
	&LayoutElement_get_minHeight_m2924_MethodInfo,
	&LayoutElement_get_preferredHeight_m2928_MethodInfo,
	&LayoutElement_get_flexibleHeight_m2932_MethodInfo,
	&LayoutElement_get_layoutPriority_m2934_MethodInfo,
	&LayoutElement_get_ignoreLayout_m2918_MethodInfo,
	&LayoutElement_get_ignoreLayout_m2918_MethodInfo,
	&LayoutElement_set_ignoreLayout_m2919_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m2920_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m2921_MethodInfo,
	&LayoutElement_get_minWidth_m2922_MethodInfo,
	&LayoutElement_set_minWidth_m2923_MethodInfo,
	&LayoutElement_get_minHeight_m2924_MethodInfo,
	&LayoutElement_set_minHeight_m2925_MethodInfo,
	&LayoutElement_get_preferredWidth_m2926_MethodInfo,
	&LayoutElement_set_preferredWidth_m2927_MethodInfo,
	&LayoutElement_get_preferredHeight_m2928_MethodInfo,
	&LayoutElement_set_preferredHeight_m2929_MethodInfo,
	&LayoutElement_get_flexibleWidth_m2930_MethodInfo,
	&LayoutElement_set_flexibleWidth_m2931_MethodInfo,
	&LayoutElement_get_flexibleHeight_m2932_MethodInfo,
	&LayoutElement_set_flexibleHeight_m2933_MethodInfo,
	&LayoutElement_get_layoutPriority_m2934_MethodInfo,
};
static bool LayoutElement_t611_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* LayoutElement_t611_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t652_0_0_0,
	&ILayoutIgnorer_t702_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutElement_t611_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t652_0_0_0, 16},
	{ &ILayoutIgnorer_t702_0_0_0, 25},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutElement_t611_0_0_0;
extern const Il2CppType LayoutElement_t611_1_0_0;
struct LayoutElement_t611;
const Il2CppTypeDefinitionMetadata LayoutElement_t611_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutElement_t611_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutElement_t611_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t455_0_0_0/* parent */
	, LayoutElement_t611_VTable/* vtableMethods */
	, LayoutElement_t611_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 581/* fieldStart */

};
TypeInfo LayoutElement_t611_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutElement_t611_MethodInfos/* methods */
	, LayoutElement_t611_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutElement_t611_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 295/* custom_attributes_cache */
	, &LayoutElement_t611_0_0_0/* byval_arg */
	, &LayoutElement_t611_1_0_0/* this_arg */
	, &LayoutElement_t611_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutElement_t611)/* instance_size */
	, sizeof (LayoutElement_t611)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 8/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 43/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup.h"
// Metadata Definition UnityEngine.UI.LayoutGroup
extern TypeInfo LayoutGroup_t608_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::.ctor()
extern const MethodInfo LayoutGroup__ctor_m2941_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutGroup__ctor_m2941/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOffset_t404_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::get_padding()
extern const MethodInfo LayoutGroup_get_padding_m2942_MethodInfo = 
{
	"get_padding"/* name */
	, (methodPointerType)&LayoutGroup_get_padding_m2942/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &RectOffset_t404_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOffset_t404_0_0_0;
static const ParameterInfo LayoutGroup_t608_LayoutGroup_set_padding_m2943_ParameterInfos[] = 
{
	{"value", 0, 134218361, 0, &RectOffset_t404_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::set_padding(UnityEngine.RectOffset)
extern const MethodInfo LayoutGroup_set_padding_m2943_MethodInfo = 
{
	"set_padding"/* name */
	, (methodPointerType)&LayoutGroup_set_padding_m2943/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LayoutGroup_t608_LayoutGroup_set_padding_m2943_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TextAnchor_t701 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::get_childAlignment()
extern const MethodInfo LayoutGroup_get_childAlignment_m2944_MethodInfo = 
{
	"get_childAlignment"/* name */
	, (methodPointerType)&LayoutGroup_get_childAlignment_m2944/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t701_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t701/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t701_0_0_0;
static const ParameterInfo LayoutGroup_t608_LayoutGroup_set_childAlignment_m2945_ParameterInfos[] = 
{
	{"value", 0, 134218362, 0, &TextAnchor_t701_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::set_childAlignment(UnityEngine.TextAnchor)
extern const MethodInfo LayoutGroup_set_childAlignment_m2945_MethodInfo = 
{
	"set_childAlignment"/* name */
	, (methodPointerType)&LayoutGroup_set_childAlignment_m2945/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, LayoutGroup_t608_LayoutGroup_set_childAlignment_m2945_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::get_rectTransform()
extern const MethodInfo LayoutGroup_get_rectTransform_m2946_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&LayoutGroup_get_rectTransform_m2946/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t364_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t612_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::get_rectChildren()
extern const MethodInfo LayoutGroup_get_rectChildren_m2947_MethodInfo = 
{
	"get_rectChildren"/* name */
	, (methodPointerType)&LayoutGroup_get_rectChildren_m2947/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t612_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo LayoutGroup_CalculateLayoutInputHorizontal_m2948_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&LayoutGroup_CalculateLayoutInputHorizontal_m2948/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo LayoutGroup_CalculateLayoutInputVertical_m3550_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, NULL/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_minWidth()
extern const MethodInfo LayoutGroup_get_minWidth_m2949_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_minWidth_m2949/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_preferredWidth()
extern const MethodInfo LayoutGroup_get_preferredWidth_m2950_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_preferredWidth_m2950/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleWidth()
extern const MethodInfo LayoutGroup_get_flexibleWidth_m2951_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_flexibleWidth_m2951/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_minHeight()
extern const MethodInfo LayoutGroup_get_minHeight_m2952_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_minHeight_m2952/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_preferredHeight()
extern const MethodInfo LayoutGroup_get_preferredHeight_m2953_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_preferredHeight_m2953/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleHeight()
extern const MethodInfo LayoutGroup_get_flexibleHeight_m2954_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_flexibleHeight_m2954/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutGroup::get_layoutPriority()
extern const MethodInfo LayoutGroup_get_layoutPriority_m2955_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&LayoutGroup_get_layoutPriority_m2955/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutHorizontal()
extern const MethodInfo LayoutGroup_SetLayoutHorizontal_m3551_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, NULL/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutVertical()
extern const MethodInfo LayoutGroup_SetLayoutVertical_m3552_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, NULL/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnEnable()
extern const MethodInfo LayoutGroup_OnEnable_m2956_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&LayoutGroup_OnEnable_m2956/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnDisable()
extern const MethodInfo LayoutGroup_OnDisable_m2957_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&LayoutGroup_OnDisable_m2957/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnDidApplyAnimationProperties()
extern const MethodInfo LayoutGroup_OnDidApplyAnimationProperties_m2958_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&LayoutGroup_OnDidApplyAnimationProperties_m2958/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo LayoutGroup_t608_LayoutGroup_GetTotalMinSize_m2959_ParameterInfos[] = 
{
	{"axis", 0, 134218363, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalMinSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalMinSize_m2959_MethodInfo = 
{
	"GetTotalMinSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalMinSize_m2959/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Int32_t253/* invoker_method */
	, LayoutGroup_t608_LayoutGroup_GetTotalMinSize_m2959_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo LayoutGroup_t608_LayoutGroup_GetTotalPreferredSize_m2960_ParameterInfos[] = 
{
	{"axis", 0, 134218364, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalPreferredSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalPreferredSize_m2960_MethodInfo = 
{
	"GetTotalPreferredSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalPreferredSize_m2960/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Int32_t253/* invoker_method */
	, LayoutGroup_t608_LayoutGroup_GetTotalPreferredSize_m2960_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo LayoutGroup_t608_LayoutGroup_GetTotalFlexibleSize_m2961_ParameterInfos[] = 
{
	{"axis", 0, 134218365, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalFlexibleSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalFlexibleSize_m2961_MethodInfo = 
{
	"GetTotalFlexibleSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalFlexibleSize_m2961/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Int32_t253/* invoker_method */
	, LayoutGroup_t608_LayoutGroup_GetTotalFlexibleSize_m2961_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo LayoutGroup_t608_LayoutGroup_GetStartOffset_m2962_ParameterInfos[] = 
{
	{"axis", 0, 134218366, 0, &Int32_t253_0_0_0},
	{"requiredSpaceWithoutPadding", 1, 134218367, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Int32_t253_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetStartOffset(System.Int32,System.Single)
extern const MethodInfo LayoutGroup_GetStartOffset_m2962_MethodInfo = 
{
	"GetStartOffset"/* name */
	, (methodPointerType)&LayoutGroup_GetStartOffset_m2962/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Int32_t253_Single_t254/* invoker_method */
	, LayoutGroup_t608_LayoutGroup_GetStartOffset_m2962_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo LayoutGroup_t608_LayoutGroup_SetLayoutInputForAxis_m2963_ParameterInfos[] = 
{
	{"totalMin", 0, 134218368, 0, &Single_t254_0_0_0},
	{"totalPreferred", 1, 134218369, 0, &Single_t254_0_0_0},
	{"totalFlexible", 2, 134218370, 0, &Single_t254_0_0_0},
	{"axis", 3, 134218371, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Single_t254_Single_t254_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutInputForAxis(System.Single,System.Single,System.Single,System.Int32)
extern const MethodInfo LayoutGroup_SetLayoutInputForAxis_m2963_MethodInfo = 
{
	"SetLayoutInputForAxis"/* name */
	, (methodPointerType)&LayoutGroup_SetLayoutInputForAxis_m2963/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Single_t254_Single_t254_Single_t254_Int32_t253/* invoker_method */
	, LayoutGroup_t608_LayoutGroup_SetLayoutInputForAxis_m2963_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo LayoutGroup_t608_LayoutGroup_SetChildAlongAxis_m2964_ParameterInfos[] = 
{
	{"rect", 0, 134218372, 0, &RectTransform_t364_0_0_0},
	{"axis", 1, 134218373, 0, &Int32_t253_0_0_0},
	{"pos", 2, 134218374, 0, &Single_t254_0_0_0},
	{"size", 3, 134218375, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetChildAlongAxis(UnityEngine.RectTransform,System.Int32,System.Single,System.Single)
extern const MethodInfo LayoutGroup_SetChildAlongAxis_m2964_MethodInfo = 
{
	"SetChildAlongAxis"/* name */
	, (methodPointerType)&LayoutGroup_SetChildAlongAxis_m2964/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Single_t254_Single_t254/* invoker_method */
	, LayoutGroup_t608_LayoutGroup_SetChildAlongAxis_m2964_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutGroup::get_isRootLayoutGroup()
extern const MethodInfo LayoutGroup_get_isRootLayoutGroup_m2965_MethodInfo = 
{
	"get_isRootLayoutGroup"/* name */
	, (methodPointerType)&LayoutGroup_get_isRootLayoutGroup_m2965/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnRectTransformDimensionsChange()
extern const MethodInfo LayoutGroup_OnRectTransformDimensionsChange_m2966_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&LayoutGroup_OnRectTransformDimensionsChange_m2966/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnTransformChildrenChanged()
extern const MethodInfo LayoutGroup_OnTransformChildrenChanged_m2967_MethodInfo = 
{
	"OnTransformChildrenChanged"/* name */
	, (methodPointerType)&LayoutGroup_OnTransformChildrenChanged_m2967/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LayoutGroup_SetProperty_m3553_gp_0_1_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m3553_gp_0_1_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m3553_gp_0_0_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m3553_gp_0_0_0_0;
static const ParameterInfo LayoutGroup_t608_LayoutGroup_SetProperty_m3553_ParameterInfos[] = 
{
	{"currentValue", 0, 134218376, 0, &LayoutGroup_SetProperty_m3553_gp_0_1_0_0},
	{"newValue", 1, 134218377, 0, &LayoutGroup_SetProperty_m3553_gp_0_0_0_0},
};
extern const Il2CppGenericContainer LayoutGroup_SetProperty_m3553_Il2CppGenericContainer;
extern TypeInfo LayoutGroup_SetProperty_m3553_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter LayoutGroup_SetProperty_m3553_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &LayoutGroup_SetProperty_m3553_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* LayoutGroup_SetProperty_m3553_Il2CppGenericParametersArray[1] = 
{
	&LayoutGroup_SetProperty_m3553_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo LayoutGroup_SetProperty_m3553_MethodInfo;
extern const Il2CppGenericContainer LayoutGroup_SetProperty_m3553_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&LayoutGroup_SetProperty_m3553_MethodInfo, 1, 1, LayoutGroup_SetProperty_m3553_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition LayoutGroup_SetProperty_m3553_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&LayoutGroup_SetProperty_m3553_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.UI.LayoutGroup::SetProperty(T&,T)
extern const MethodInfo LayoutGroup_SetProperty_m3553_MethodInfo = 
{
	"SetProperty"/* name */
	, NULL/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, LayoutGroup_t608_LayoutGroup_SetProperty_m3553_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1173/* token */
	, LayoutGroup_SetProperty_m3553_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LayoutGroup_SetProperty_m3553_Il2CppGenericContainer/* genericContainer */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetDirty()
extern const MethodInfo LayoutGroup_SetDirty_m2968_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&LayoutGroup_SetDirty_m2968/* method */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutGroup_t608_MethodInfos[] =
{
	&LayoutGroup__ctor_m2941_MethodInfo,
	&LayoutGroup_get_padding_m2942_MethodInfo,
	&LayoutGroup_set_padding_m2943_MethodInfo,
	&LayoutGroup_get_childAlignment_m2944_MethodInfo,
	&LayoutGroup_set_childAlignment_m2945_MethodInfo,
	&LayoutGroup_get_rectTransform_m2946_MethodInfo,
	&LayoutGroup_get_rectChildren_m2947_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m2948_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m3550_MethodInfo,
	&LayoutGroup_get_minWidth_m2949_MethodInfo,
	&LayoutGroup_get_preferredWidth_m2950_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m2951_MethodInfo,
	&LayoutGroup_get_minHeight_m2952_MethodInfo,
	&LayoutGroup_get_preferredHeight_m2953_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m2954_MethodInfo,
	&LayoutGroup_get_layoutPriority_m2955_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m3551_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m3552_MethodInfo,
	&LayoutGroup_OnEnable_m2956_MethodInfo,
	&LayoutGroup_OnDisable_m2957_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m2958_MethodInfo,
	&LayoutGroup_GetTotalMinSize_m2959_MethodInfo,
	&LayoutGroup_GetTotalPreferredSize_m2960_MethodInfo,
	&LayoutGroup_GetTotalFlexibleSize_m2961_MethodInfo,
	&LayoutGroup_GetStartOffset_m2962_MethodInfo,
	&LayoutGroup_SetLayoutInputForAxis_m2963_MethodInfo,
	&LayoutGroup_SetChildAlongAxis_m2964_MethodInfo,
	&LayoutGroup_get_isRootLayoutGroup_m2965_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m2966_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m2967_MethodInfo,
	&LayoutGroup_SetProperty_m3553_MethodInfo,
	&LayoutGroup_SetDirty_m2968_MethodInfo,
	NULL
};
extern const MethodInfo LayoutGroup_get_padding_m2942_MethodInfo;
extern const MethodInfo LayoutGroup_set_padding_m2943_MethodInfo;
static const PropertyInfo LayoutGroup_t608____padding_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "padding"/* name */
	, &LayoutGroup_get_padding_m2942_MethodInfo/* get */
	, &LayoutGroup_set_padding_m2943_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_childAlignment_m2944_MethodInfo;
extern const MethodInfo LayoutGroup_set_childAlignment_m2945_MethodInfo;
static const PropertyInfo LayoutGroup_t608____childAlignment_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "childAlignment"/* name */
	, &LayoutGroup_get_childAlignment_m2944_MethodInfo/* get */
	, &LayoutGroup_set_childAlignment_m2945_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_rectTransform_m2946_MethodInfo;
static const PropertyInfo LayoutGroup_t608____rectTransform_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &LayoutGroup_get_rectTransform_m2946_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_rectChildren_m2947_MethodInfo;
static const PropertyInfo LayoutGroup_t608____rectChildren_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "rectChildren"/* name */
	, &LayoutGroup_get_rectChildren_m2947_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t608____minWidth_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &LayoutGroup_get_minWidth_m2949_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t608____preferredWidth_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &LayoutGroup_get_preferredWidth_m2950_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t608____flexibleWidth_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &LayoutGroup_get_flexibleWidth_m2951_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t608____minHeight_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &LayoutGroup_get_minHeight_m2952_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t608____preferredHeight_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &LayoutGroup_get_preferredHeight_m2953_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t608____flexibleHeight_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &LayoutGroup_get_flexibleHeight_m2954_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t608____layoutPriority_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &LayoutGroup_get_layoutPriority_m2955_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_isRootLayoutGroup_m2965_MethodInfo;
static const PropertyInfo LayoutGroup_t608____isRootLayoutGroup_PropertyInfo = 
{
	&LayoutGroup_t608_il2cpp_TypeInfo/* parent */
	, "isRootLayoutGroup"/* name */
	, &LayoutGroup_get_isRootLayoutGroup_m2965_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutGroup_t608_PropertyInfos[] =
{
	&LayoutGroup_t608____padding_PropertyInfo,
	&LayoutGroup_t608____childAlignment_PropertyInfo,
	&LayoutGroup_t608____rectTransform_PropertyInfo,
	&LayoutGroup_t608____rectChildren_PropertyInfo,
	&LayoutGroup_t608____minWidth_PropertyInfo,
	&LayoutGroup_t608____preferredWidth_PropertyInfo,
	&LayoutGroup_t608____flexibleWidth_PropertyInfo,
	&LayoutGroup_t608____minHeight_PropertyInfo,
	&LayoutGroup_t608____preferredHeight_PropertyInfo,
	&LayoutGroup_t608____flexibleHeight_PropertyInfo,
	&LayoutGroup_t608____layoutPriority_PropertyInfo,
	&LayoutGroup_t608____isRootLayoutGroup_PropertyInfo,
	NULL
};
static const Il2CppMethodReference LayoutGroup_t608_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&LayoutGroup_OnEnable_m2956_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&LayoutGroup_OnDisable_m2957_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m2966_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m2958_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m2948_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m3550_MethodInfo,
	&LayoutGroup_get_minWidth_m2949_MethodInfo,
	&LayoutGroup_get_preferredWidth_m2950_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m2951_MethodInfo,
	&LayoutGroup_get_minHeight_m2952_MethodInfo,
	&LayoutGroup_get_preferredHeight_m2953_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m2954_MethodInfo,
	&LayoutGroup_get_layoutPriority_m2955_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m3551_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m3552_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m2948_MethodInfo,
	NULL,
	&LayoutGroup_get_minWidth_m2949_MethodInfo,
	&LayoutGroup_get_preferredWidth_m2950_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m2951_MethodInfo,
	&LayoutGroup_get_minHeight_m2952_MethodInfo,
	&LayoutGroup_get_preferredHeight_m2953_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m2954_MethodInfo,
	&LayoutGroup_get_layoutPriority_m2955_MethodInfo,
	NULL,
	NULL,
	&LayoutGroup_OnTransformChildrenChanged_m2967_MethodInfo,
};
static bool LayoutGroup_t608_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* LayoutGroup_t608_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t652_0_0_0,
	&ILayoutController_t705_0_0_0,
	&ILayoutGroup_t703_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutGroup_t608_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t652_0_0_0, 16},
	{ &ILayoutController_t705_0_0_0, 25},
	{ &ILayoutGroup_t703_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutGroup_t608_1_0_0;
struct LayoutGroup_t608;
const Il2CppTypeDefinitionMetadata LayoutGroup_t608_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutGroup_t608_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutGroup_t608_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t455_0_0_0/* parent */
	, LayoutGroup_t608_VTable/* vtableMethods */
	, LayoutGroup_t608_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 588/* fieldStart */

};
TypeInfo LayoutGroup_t608_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutGroup_t608_MethodInfos/* methods */
	, LayoutGroup_t608_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutGroup_t608_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 303/* custom_attributes_cache */
	, &LayoutGroup_t608_0_0_0/* byval_arg */
	, &LayoutGroup_t608_1_0_0/* this_arg */
	, &LayoutGroup_t608_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutGroup_t608)/* instance_size */
	, sizeof (LayoutGroup_t608)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 32/* method_count */
	, 12/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
// Metadata Definition UnityEngine.UI.LayoutRebuilder
extern TypeInfo LayoutRebuilder_t615_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder__ctor_m2969_ParameterInfos[] = 
{
	{"controller", 0, 134218378, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::.ctor(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder__ctor_m2969_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutRebuilder__ctor_m2969/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder__ctor_m2969_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::.cctor()
extern const MethodInfo LayoutRebuilder__cctor_m2970_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&LayoutRebuilder__cctor_m2970/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t511_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m2971_ParameterInfos[] = 
{
	{"executing", 0, 134218379, 0, &CanvasUpdate_t511_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::UnityEngine.UI.ICanvasElement.Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m2971_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.Rebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m2971/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m2971_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_ReapplyDrivenProperties_m2972_ParameterInfos[] = 
{
	{"driven", 0, 134218380, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::ReapplyDrivenProperties(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ReapplyDrivenProperties_m2972_MethodInfo = 
{
	"ReapplyDrivenProperties"/* name */
	, (methodPointerType)&LayoutRebuilder_ReapplyDrivenProperties_m2972/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_ReapplyDrivenProperties_m2972_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.LayoutRebuilder::get_transform()
extern const MethodInfo LayoutRebuilder_get_transform_m2973_MethodInfo = 
{
	"get_transform"/* name */
	, (methodPointerType)&LayoutRebuilder_get_transform_m2973/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t1_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::IsDestroyed()
extern const MethodInfo LayoutRebuilder_IsDestroyed_m2974_MethodInfo = 
{
	"IsDestroyed"/* name */
	, (methodPointerType)&LayoutRebuilder_IsDestroyed_m2974/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t651_0_0_0;
extern const Il2CppType List_1_t651_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_StripDisabledBehavioursFromList_m2975_ParameterInfos[] = 
{
	{"components", 0, 134218381, 0, &List_1_t651_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::StripDisabledBehavioursFromList(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_StripDisabledBehavioursFromList_m2975_MethodInfo = 
{
	"StripDisabledBehavioursFromList"/* name */
	, (methodPointerType)&LayoutRebuilder_StripDisabledBehavioursFromList_m2975/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_StripDisabledBehavioursFromList_m2975_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType UnityAction_1_t613_0_0_0;
extern const Il2CppType UnityAction_1_t613_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_PerformLayoutControl_m2976_ParameterInfos[] = 
{
	{"rect", 0, 134218382, 0, &RectTransform_t364_0_0_0},
	{"action", 1, 134218383, 0, &UnityAction_1_t613_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutControl(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_PerformLayoutControl_m2976_MethodInfo = 
{
	"PerformLayoutControl"/* name */
	, (methodPointerType)&LayoutRebuilder_PerformLayoutControl_m2976/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_PerformLayoutControl_m2976_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType UnityAction_1_t613_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_PerformLayoutCalculation_m2977_ParameterInfos[] = 
{
	{"rect", 0, 134218384, 0, &RectTransform_t364_0_0_0},
	{"action", 1, 134218385, 0, &UnityAction_1_t613_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutCalculation(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_PerformLayoutCalculation_m2977_MethodInfo = 
{
	"PerformLayoutCalculation"/* name */
	, (methodPointerType)&LayoutRebuilder_PerformLayoutCalculation_m2977/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_PerformLayoutCalculation_m2977_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_MarkLayoutForRebuild_m2978_ParameterInfos[] = 
{
	{"rect", 0, 134218386, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutForRebuild(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_MarkLayoutForRebuild_m2978_MethodInfo = 
{
	"MarkLayoutForRebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_MarkLayoutForRebuild_m2978/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_MarkLayoutForRebuild_m2978_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_ValidLayoutGroup_m2979_ParameterInfos[] = 
{
	{"parent", 0, 134218387, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidLayoutGroup(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ValidLayoutGroup_m2979_MethodInfo = 
{
	"ValidLayoutGroup"/* name */
	, (methodPointerType)&LayoutRebuilder_ValidLayoutGroup_m2979/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_ValidLayoutGroup_m2979_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_ValidController_m2980_ParameterInfos[] = 
{
	{"layoutRoot", 0, 134218388, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidController(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ValidController_m2980_MethodInfo = 
{
	"ValidController"/* name */
	, (methodPointerType)&LayoutRebuilder_ValidController_m2980/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_ValidController_m2980_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_MarkLayoutRootForRebuild_m2981_ParameterInfos[] = 
{
	{"controller", 0, 134218389, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutRootForRebuild(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_MarkLayoutRootForRebuild_m2981_MethodInfo = 
{
	"MarkLayoutRootForRebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_MarkLayoutRootForRebuild_m2981/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_MarkLayoutRootForRebuild_m2981_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LayoutRebuilder_t615_0_0_0;
extern const Il2CppType LayoutRebuilder_t615_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_Equals_m2982_ParameterInfos[] = 
{
	{"other", 0, 134218390, 0, &LayoutRebuilder_t615_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_LayoutRebuilder_t615 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::Equals(UnityEngine.UI.LayoutRebuilder)
extern const MethodInfo LayoutRebuilder_Equals_m2982_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&LayoutRebuilder_Equals_m2982/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_LayoutRebuilder_t615/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_Equals_m2982_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutRebuilder::GetHashCode()
extern const MethodInfo LayoutRebuilder_GetHashCode_m2983_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&LayoutRebuilder_GetHashCode_m2983/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.UI.LayoutRebuilder::ToString()
extern const MethodInfo LayoutRebuilder_ToString_m2984_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&LayoutRebuilder_ToString_m2984/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t219_0_0_0;
extern const Il2CppType Component_t219_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_U3CRebuildU3Em__9_m2985_ParameterInfos[] = 
{
	{"e", 0, 134218391, 0, &Component_t219_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__9(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__9_m2985_MethodInfo = 
{
	"<Rebuild>m__9"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__9_m2985/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_U3CRebuildU3Em__9_m2985_ParameterInfos/* parameters */
	, 311/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t219_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_U3CRebuildU3Em__A_m2986_ParameterInfos[] = 
{
	{"e", 0, 134218392, 0, &Component_t219_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__A(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__A_m2986_MethodInfo = 
{
	"<Rebuild>m__A"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__A_m2986/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_U3CRebuildU3Em__A_m2986_ParameterInfos/* parameters */
	, 312/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t219_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_U3CRebuildU3Em__B_m2987_ParameterInfos[] = 
{
	{"e", 0, 134218393, 0, &Component_t219_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__B(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__B_m2987_MethodInfo = 
{
	"<Rebuild>m__B"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__B_m2987/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_U3CRebuildU3Em__B_m2987_ParameterInfos/* parameters */
	, 313/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t219_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_U3CRebuildU3Em__C_m2988_ParameterInfos[] = 
{
	{"e", 0, 134218394, 0, &Component_t219_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__C(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__C_m2988_MethodInfo = 
{
	"<Rebuild>m__C"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__C_m2988/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_U3CRebuildU3Em__C_m2988_ParameterInfos/* parameters */
	, 314/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t219_0_0_0;
static const ParameterInfo LayoutRebuilder_t615_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m2989_ParameterInfos[] = 
{
	{"e", 0, 134218395, 0, &Component_t219_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::<StripDisabledBehavioursFromList>m__D(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m2989_MethodInfo = 
{
	"<StripDisabledBehavioursFromList>m__D"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m2989/* method */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, LayoutRebuilder_t615_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m2989_ParameterInfos/* parameters */
	, 315/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutRebuilder_t615_MethodInfos[] =
{
	&LayoutRebuilder__ctor_m2969_MethodInfo,
	&LayoutRebuilder__cctor_m2970_MethodInfo,
	&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m2971_MethodInfo,
	&LayoutRebuilder_ReapplyDrivenProperties_m2972_MethodInfo,
	&LayoutRebuilder_get_transform_m2973_MethodInfo,
	&LayoutRebuilder_IsDestroyed_m2974_MethodInfo,
	&LayoutRebuilder_StripDisabledBehavioursFromList_m2975_MethodInfo,
	&LayoutRebuilder_PerformLayoutControl_m2976_MethodInfo,
	&LayoutRebuilder_PerformLayoutCalculation_m2977_MethodInfo,
	&LayoutRebuilder_MarkLayoutForRebuild_m2978_MethodInfo,
	&LayoutRebuilder_ValidLayoutGroup_m2979_MethodInfo,
	&LayoutRebuilder_ValidController_m2980_MethodInfo,
	&LayoutRebuilder_MarkLayoutRootForRebuild_m2981_MethodInfo,
	&LayoutRebuilder_Equals_m2982_MethodInfo,
	&LayoutRebuilder_GetHashCode_m2983_MethodInfo,
	&LayoutRebuilder_ToString_m2984_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__9_m2985_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__A_m2986_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__B_m2987_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__C_m2988_MethodInfo,
	&LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m2989_MethodInfo,
	NULL
};
extern const MethodInfo LayoutRebuilder_get_transform_m2973_MethodInfo;
static const PropertyInfo LayoutRebuilder_t615____transform_PropertyInfo = 
{
	&LayoutRebuilder_t615_il2cpp_TypeInfo/* parent */
	, "transform"/* name */
	, &LayoutRebuilder_get_transform_m2973_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutRebuilder_t615_PropertyInfos[] =
{
	&LayoutRebuilder_t615____transform_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutRebuilder_GetHashCode_m2983_MethodInfo;
extern const MethodInfo LayoutRebuilder_ToString_m2984_MethodInfo;
extern const MethodInfo LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m2971_MethodInfo;
extern const MethodInfo LayoutRebuilder_IsDestroyed_m2974_MethodInfo;
extern const MethodInfo LayoutRebuilder_Equals_m2982_MethodInfo;
static const Il2CppMethodReference LayoutRebuilder_t615_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&LayoutRebuilder_GetHashCode_m2983_MethodInfo,
	&LayoutRebuilder_ToString_m2984_MethodInfo,
	&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m2971_MethodInfo,
	&LayoutRebuilder_get_transform_m2973_MethodInfo,
	&LayoutRebuilder_IsDestroyed_m2974_MethodInfo,
	&LayoutRebuilder_Equals_m2982_MethodInfo,
};
static bool LayoutRebuilder_t615_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEquatable_1_t758_0_0_0;
static const Il2CppType* LayoutRebuilder_t615_InterfacesTypeInfos[] = 
{
	&ICanvasElement_t646_0_0_0,
	&IEquatable_1_t758_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutRebuilder_t615_InterfacesOffsets[] = 
{
	{ &ICanvasElement_t646_0_0_0, 4},
	{ &IEquatable_1_t758_0_0_0, 7},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutRebuilder_t615_1_0_0;
const Il2CppTypeDefinitionMetadata LayoutRebuilder_t615_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutRebuilder_t615_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutRebuilder_t615_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, LayoutRebuilder_t615_VTable/* vtableMethods */
	, LayoutRebuilder_t615_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 596/* fieldStart */

};
TypeInfo LayoutRebuilder_t615_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutRebuilder"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutRebuilder_t615_MethodInfos/* methods */
	, LayoutRebuilder_t615_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutRebuilder_t615_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutRebuilder_t615_0_0_0/* byval_arg */
	, &LayoutRebuilder_t615_1_0_0/* this_arg */
	, &LayoutRebuilder_t615_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutRebuilder_t615)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LayoutRebuilder_t615)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutRebuilder_t615_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 265/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility.h"
// Metadata Definition UnityEngine.UI.LayoutUtility
extern TypeInfo LayoutUtility_t617_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_GetMinSize_m2990_ParameterInfos[] = 
{
	{"rect", 0, 134218396, 0, &RectTransform_t364_0_0_0},
	{"axis", 1, 134218397, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetMinSize_m2990_MethodInfo = 
{
	"GetMinSize"/* name */
	, (methodPointerType)&LayoutUtility_GetMinSize_m2990/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t_Int32_t253/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_GetMinSize_m2990_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_GetPreferredSize_m2991_ParameterInfos[] = 
{
	{"rect", 0, 134218398, 0, &RectTransform_t364_0_0_0},
	{"axis", 1, 134218399, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetPreferredSize_m2991_MethodInfo = 
{
	"GetPreferredSize"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredSize_m2991/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t_Int32_t253/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_GetPreferredSize_m2991_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_GetFlexibleSize_m2992_ParameterInfos[] = 
{
	{"rect", 0, 134218400, 0, &RectTransform_t364_0_0_0},
	{"axis", 1, 134218401, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetFlexibleSize_m2992_MethodInfo = 
{
	"GetFlexibleSize"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleSize_m2992/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t_Int32_t253/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_GetFlexibleSize_m2992_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_GetMinWidth_m2993_ParameterInfos[] = 
{
	{"rect", 0, 134218402, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetMinWidth_m2993_MethodInfo = 
{
	"GetMinWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetMinWidth_m2993/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_GetMinWidth_m2993_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_GetPreferredWidth_m2994_ParameterInfos[] = 
{
	{"rect", 0, 134218403, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetPreferredWidth_m2994_MethodInfo = 
{
	"GetPreferredWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredWidth_m2994/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_GetPreferredWidth_m2994_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_GetFlexibleWidth_m2995_ParameterInfos[] = 
{
	{"rect", 0, 134218404, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetFlexibleWidth_m2995_MethodInfo = 
{
	"GetFlexibleWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleWidth_m2995/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_GetFlexibleWidth_m2995_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_GetMinHeight_m2996_ParameterInfos[] = 
{
	{"rect", 0, 134218405, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetMinHeight_m2996_MethodInfo = 
{
	"GetMinHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetMinHeight_m2996/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_GetMinHeight_m2996_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_GetPreferredHeight_m2997_ParameterInfos[] = 
{
	{"rect", 0, 134218406, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetPreferredHeight_m2997_MethodInfo = 
{
	"GetPreferredHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredHeight_m2997/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_GetPreferredHeight_m2997_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_GetFlexibleHeight_m2998_ParameterInfos[] = 
{
	{"rect", 0, 134218407, 0, &RectTransform_t364_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetFlexibleHeight_m2998_MethodInfo = 
{
	"GetFlexibleHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleHeight_m2998/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_GetFlexibleHeight_m2998_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Func_2_t616_0_0_0;
extern const Il2CppType Func_2_t616_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_GetLayoutProperty_m2999_ParameterInfos[] = 
{
	{"rect", 0, 134218408, 0, &RectTransform_t364_0_0_0},
	{"property", 1, 134218409, 0, &Func_2_t616_0_0_0},
	{"defaultValue", 2, 134218410, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single)
extern const MethodInfo LayoutUtility_GetLayoutProperty_m2999_MethodInfo = 
{
	"GetLayoutProperty"/* name */
	, (methodPointerType)&LayoutUtility_GetLayoutProperty_m2999/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t_Object_t_Single_t254/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_GetLayoutProperty_m2999_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t364_0_0_0;
extern const Il2CppType Func_2_t616_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType ILayoutElement_t652_1_0_2;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_GetLayoutProperty_m3000_ParameterInfos[] = 
{
	{"rect", 0, 134218411, 0, &RectTransform_t364_0_0_0},
	{"property", 1, 134218412, 0, &Func_2_t616_0_0_0},
	{"defaultValue", 2, 134218413, 0, &Single_t254_0_0_0},
	{"source", 3, 134218414, 0, &ILayoutElement_t652_1_0_2},
};
extern void* RuntimeInvoker_Single_t254_Object_t_Object_t_Single_t254_ILayoutElementU26_t759 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single,UnityEngine.UI.ILayoutElement&)
extern const MethodInfo LayoutUtility_GetLayoutProperty_m3000_MethodInfo = 
{
	"GetLayoutProperty"/* name */
	, (methodPointerType)&LayoutUtility_GetLayoutProperty_m3000/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t_Object_t_Single_t254_ILayoutElementU26_t759/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_GetLayoutProperty_m3000_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t652_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_U3CGetMinWidthU3Em__E_m3001_ParameterInfos[] = 
{
	{"e", 0, 134218415, 0, &ILayoutElement_t652_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetMinWidth>m__E(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetMinWidthU3Em__E_m3001_MethodInfo = 
{
	"<GetMinWidth>m__E"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetMinWidthU3Em__E_m3001/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_U3CGetMinWidthU3Em__E_m3001_ParameterInfos/* parameters */
	, 324/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t652_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_U3CGetPreferredWidthU3Em__F_m3002_ParameterInfos[] = 
{
	{"e", 0, 134218416, 0, &ILayoutElement_t652_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__F(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredWidthU3Em__F_m3002_MethodInfo = 
{
	"<GetPreferredWidth>m__F"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredWidthU3Em__F_m3002/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_U3CGetPreferredWidthU3Em__F_m3002_ParameterInfos/* parameters */
	, 325/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t652_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_U3CGetPreferredWidthU3Em__10_m3003_ParameterInfos[] = 
{
	{"e", 0, 134218417, 0, &ILayoutElement_t652_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__10(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredWidthU3Em__10_m3003_MethodInfo = 
{
	"<GetPreferredWidth>m__10"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredWidthU3Em__10_m3003/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_U3CGetPreferredWidthU3Em__10_m3003_ParameterInfos/* parameters */
	, 326/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t652_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m3004_ParameterInfos[] = 
{
	{"e", 0, 134218418, 0, &ILayoutElement_t652_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleWidth>m__11(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetFlexibleWidthU3Em__11_m3004_MethodInfo = 
{
	"<GetFlexibleWidth>m__11"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetFlexibleWidthU3Em__11_m3004/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m3004_ParameterInfos/* parameters */
	, 327/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t652_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_U3CGetMinHeightU3Em__12_m3005_ParameterInfos[] = 
{
	{"e", 0, 134218419, 0, &ILayoutElement_t652_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetMinHeight>m__12(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetMinHeightU3Em__12_m3005_MethodInfo = 
{
	"<GetMinHeight>m__12"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetMinHeightU3Em__12_m3005/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_U3CGetMinHeightU3Em__12_m3005_ParameterInfos/* parameters */
	, 328/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t652_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_U3CGetPreferredHeightU3Em__13_m3006_ParameterInfos[] = 
{
	{"e", 0, 134218420, 0, &ILayoutElement_t652_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__13(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredHeightU3Em__13_m3006_MethodInfo = 
{
	"<GetPreferredHeight>m__13"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredHeightU3Em__13_m3006/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_U3CGetPreferredHeightU3Em__13_m3006_ParameterInfos/* parameters */
	, 329/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t652_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_U3CGetPreferredHeightU3Em__14_m3007_ParameterInfos[] = 
{
	{"e", 0, 134218421, 0, &ILayoutElement_t652_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__14(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredHeightU3Em__14_m3007_MethodInfo = 
{
	"<GetPreferredHeight>m__14"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredHeightU3Em__14_m3007/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_U3CGetPreferredHeightU3Em__14_m3007_ParameterInfos/* parameters */
	, 330/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t652_0_0_0;
static const ParameterInfo LayoutUtility_t617_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m3008_ParameterInfos[] = 
{
	{"e", 0, 134218422, 0, &ILayoutElement_t652_0_0_0},
};
extern void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleHeight>m__15(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetFlexibleHeightU3Em__15_m3008_MethodInfo = 
{
	"<GetFlexibleHeight>m__15"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetFlexibleHeightU3Em__15_m3008/* method */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* declaring_type */
	, &Single_t254_0_0_0/* return_type */
	, RuntimeInvoker_Single_t254_Object_t/* invoker_method */
	, LayoutUtility_t617_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m3008_ParameterInfos/* parameters */
	, 331/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutUtility_t617_MethodInfos[] =
{
	&LayoutUtility_GetMinSize_m2990_MethodInfo,
	&LayoutUtility_GetPreferredSize_m2991_MethodInfo,
	&LayoutUtility_GetFlexibleSize_m2992_MethodInfo,
	&LayoutUtility_GetMinWidth_m2993_MethodInfo,
	&LayoutUtility_GetPreferredWidth_m2994_MethodInfo,
	&LayoutUtility_GetFlexibleWidth_m2995_MethodInfo,
	&LayoutUtility_GetMinHeight_m2996_MethodInfo,
	&LayoutUtility_GetPreferredHeight_m2997_MethodInfo,
	&LayoutUtility_GetFlexibleHeight_m2998_MethodInfo,
	&LayoutUtility_GetLayoutProperty_m2999_MethodInfo,
	&LayoutUtility_GetLayoutProperty_m3000_MethodInfo,
	&LayoutUtility_U3CGetMinWidthU3Em__E_m3001_MethodInfo,
	&LayoutUtility_U3CGetPreferredWidthU3Em__F_m3002_MethodInfo,
	&LayoutUtility_U3CGetPreferredWidthU3Em__10_m3003_MethodInfo,
	&LayoutUtility_U3CGetFlexibleWidthU3Em__11_m3004_MethodInfo,
	&LayoutUtility_U3CGetMinHeightU3Em__12_m3005_MethodInfo,
	&LayoutUtility_U3CGetPreferredHeightU3Em__13_m3006_MethodInfo,
	&LayoutUtility_U3CGetPreferredHeightU3Em__14_m3007_MethodInfo,
	&LayoutUtility_U3CGetFlexibleHeightU3Em__15_m3008_MethodInfo,
	NULL
};
static const Il2CppMethodReference LayoutUtility_t617_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool LayoutUtility_t617_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutUtility_t617_0_0_0;
extern const Il2CppType LayoutUtility_t617_1_0_0;
struct LayoutUtility_t617;
const Il2CppTypeDefinitionMetadata LayoutUtility_t617_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LayoutUtility_t617_VTable/* vtableMethods */
	, LayoutUtility_t617_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 603/* fieldStart */

};
TypeInfo LayoutUtility_t617_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutUtility_t617_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LayoutUtility_t617_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutUtility_t617_0_0_0/* byval_arg */
	, &LayoutUtility_t617_1_0_0/* this_arg */
	, &LayoutUtility_t617_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutUtility_t617)/* instance_size */
	, sizeof (LayoutUtility_t617)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutUtility_t617_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.VerticalLayoutGroup
extern TypeInfo VerticalLayoutGroup_t618_il2cpp_TypeInfo;
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
extern const MethodInfo VerticalLayoutGroup__ctor_m3009_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VerticalLayoutGroup__ctor_m3009/* method */
	, &VerticalLayoutGroup_t618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3010_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3010/* method */
	, &VerticalLayoutGroup_t618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputVertical_m3011_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&VerticalLayoutGroup_CalculateLayoutInputVertical_m3011/* method */
	, &VerticalLayoutGroup_t618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo VerticalLayoutGroup_SetLayoutHorizontal_m3012_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&VerticalLayoutGroup_SetLayoutHorizontal_m3012/* method */
	, &VerticalLayoutGroup_t618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
extern const MethodInfo VerticalLayoutGroup_SetLayoutVertical_m3013_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&VerticalLayoutGroup_SetLayoutVertical_m3013/* method */
	, &VerticalLayoutGroup_t618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VerticalLayoutGroup_t618_MethodInfos[] =
{
	&VerticalLayoutGroup__ctor_m3009_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3010_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m3011_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m3012_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m3013_MethodInfo,
	NULL
};
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3010_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputVertical_m3011_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_SetLayoutHorizontal_m3012_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_SetLayoutVertical_m3013_MethodInfo;
static const Il2CppMethodReference VerticalLayoutGroup_t618_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&LayoutGroup_OnEnable_m2956_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&LayoutGroup_OnDisable_m2957_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m2966_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m2958_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3010_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m3011_MethodInfo,
	&LayoutGroup_get_minWidth_m2949_MethodInfo,
	&LayoutGroup_get_preferredWidth_m2950_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m2951_MethodInfo,
	&LayoutGroup_get_minHeight_m2952_MethodInfo,
	&LayoutGroup_get_preferredHeight_m2953_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m2954_MethodInfo,
	&LayoutGroup_get_layoutPriority_m2955_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m3012_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m3013_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3010_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m3011_MethodInfo,
	&LayoutGroup_get_minWidth_m2949_MethodInfo,
	&LayoutGroup_get_preferredWidth_m2950_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m2951_MethodInfo,
	&LayoutGroup_get_minHeight_m2952_MethodInfo,
	&LayoutGroup_get_preferredHeight_m2953_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m2954_MethodInfo,
	&LayoutGroup_get_layoutPriority_m2955_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m3012_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m3013_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m2967_MethodInfo,
};
static bool VerticalLayoutGroup_t618_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair VerticalLayoutGroup_t618_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t652_0_0_0, 16},
	{ &ILayoutController_t705_0_0_0, 25},
	{ &ILayoutGroup_t703_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType VerticalLayoutGroup_t618_0_0_0;
extern const Il2CppType VerticalLayoutGroup_t618_1_0_0;
struct VerticalLayoutGroup_t618;
const Il2CppTypeDefinitionMetadata VerticalLayoutGroup_t618_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VerticalLayoutGroup_t618_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t610_0_0_0/* parent */
	, VerticalLayoutGroup_t618_VTable/* vtableMethods */
	, VerticalLayoutGroup_t618_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VerticalLayoutGroup_t618_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "VerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, VerticalLayoutGroup_t618_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VerticalLayoutGroup_t618_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 332/* custom_attributes_cache */
	, &VerticalLayoutGroup_t618_0_0_0/* byval_arg */
	, &VerticalLayoutGroup_t618_1_0_0/* this_arg */
	, &VerticalLayoutGroup_t618_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VerticalLayoutGroup_t618)/* instance_size */
	, sizeof (VerticalLayoutGroup_t618)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IMaterialModifier
extern TypeInfo IMaterialModifier_t669_il2cpp_TypeInfo;
extern const Il2CppType Material_t55_0_0_0;
static const ParameterInfo IMaterialModifier_t669_IMaterialModifier_GetModifiedMaterial_m3554_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218423, 0, &Material_t55_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.IMaterialModifier::GetModifiedMaterial(UnityEngine.Material)
extern const MethodInfo IMaterialModifier_GetModifiedMaterial_m3554_MethodInfo = 
{
	"GetModifiedMaterial"/* name */
	, NULL/* method */
	, &IMaterialModifier_t669_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IMaterialModifier_t669_IMaterialModifier_GetModifiedMaterial_m3554_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMaterialModifier_t669_MethodInfos[] =
{
	&IMaterialModifier_GetModifiedMaterial_m3554_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IMaterialModifier_t669_0_0_0;
extern const Il2CppType IMaterialModifier_t669_1_0_0;
struct IMaterialModifier_t669;
const Il2CppTypeDefinitionMetadata IMaterialModifier_t669_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMaterialModifier_t669_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMaterialModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, IMaterialModifier_t669_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMaterialModifier_t669_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMaterialModifier_t669_0_0_0/* byval_arg */
	, &IMaterialModifier_t669_1_0_0/* this_arg */
	, &IMaterialModifier_t669_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_Mask.h"
// Metadata Definition UnityEngine.UI.Mask
extern TypeInfo Mask_t619_il2cpp_TypeInfo;
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_MaskMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::.ctor()
extern const MethodInfo Mask__ctor_m3014_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Mask__ctor_m3014/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.Mask::get_graphic()
extern const MethodInfo Mask_get_graphic_m3015_MethodInfo = 
{
	"get_graphic"/* name */
	, (methodPointerType)&Mask_get_graphic_m3015/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t418_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::get_showMaskGraphic()
extern const MethodInfo Mask_get_showMaskGraphic_m3016_MethodInfo = 
{
	"get_showMaskGraphic"/* name */
	, (methodPointerType)&Mask_get_showMaskGraphic_m3016/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Mask_t619_Mask_set_showMaskGraphic_m3017_ParameterInfos[] = 
{
	{"value", 0, 134218424, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::set_showMaskGraphic(System.Boolean)
extern const MethodInfo Mask_set_showMaskGraphic_m3017_MethodInfo = 
{
	"set_showMaskGraphic"/* name */
	, (methodPointerType)&Mask_set_showMaskGraphic_m3017/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Mask_t619_Mask_set_showMaskGraphic_m3017_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Mask::get_rectTransform()
extern const MethodInfo Mask_get_rectTransform_m3018_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&Mask_get_rectTransform_m3018/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t364_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::MaskEnabled()
extern const MethodInfo Mask_MaskEnabled_m3019_MethodInfo = 
{
	"MaskEnabled"/* name */
	, (methodPointerType)&Mask_MaskEnabled_m3019/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnSiblingGraphicEnabledDisabled()
extern const MethodInfo Mask_OnSiblingGraphicEnabledDisabled_m3020_MethodInfo = 
{
	"OnSiblingGraphicEnabledDisabled"/* name */
	, (methodPointerType)&Mask_OnSiblingGraphicEnabledDisabled_m3020/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::NotifyMaskStateChanged()
extern const MethodInfo Mask_NotifyMaskStateChanged_m3021_MethodInfo = 
{
	"NotifyMaskStateChanged"/* name */
	, (methodPointerType)&Mask_NotifyMaskStateChanged_m3021/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::ClearCachedMaterial()
extern const MethodInfo Mask_ClearCachedMaterial_m3022_MethodInfo = 
{
	"ClearCachedMaterial"/* name */
	, (methodPointerType)&Mask_ClearCachedMaterial_m3022/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnEnable()
extern const MethodInfo Mask_OnEnable_m3023_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Mask_OnEnable_m3023/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnDisable()
extern const MethodInfo Mask_OnDisable_m3024_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Mask_OnDisable_m3024/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
extern const Il2CppType Camera_t27_0_0_0;
static const ParameterInfo Mask_t619_Mask_IsRaycastLocationValid_m3025_ParameterInfos[] = 
{
	{"sp", 0, 134218425, 0, &Vector2_t6_0_0_0},
	{"eventCamera", 1, 134218426, 0, &Camera_t27_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern const MethodInfo Mask_IsRaycastLocationValid_m3025_MethodInfo = 
{
	"IsRaycastLocationValid"/* name */
	, (methodPointerType)&Mask_IsRaycastLocationValid_m3025/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Vector2_t6_Object_t/* invoker_method */
	, Mask_t619_Mask_IsRaycastLocationValid_m3025_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t55_0_0_0;
static const ParameterInfo Mask_t619_Mask_GetModifiedMaterial_m3026_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218427, 0, &Material_t55_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.Mask::GetModifiedMaterial(UnityEngine.Material)
extern const MethodInfo Mask_GetModifiedMaterial_m3026_MethodInfo = 
{
	"GetModifiedMaterial"/* name */
	, (methodPointerType)&Mask_GetModifiedMaterial_m3026/* method */
	, &Mask_t619_il2cpp_TypeInfo/* declaring_type */
	, &Material_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mask_t619_Mask_GetModifiedMaterial_m3026_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Mask_t619_MethodInfos[] =
{
	&Mask__ctor_m3014_MethodInfo,
	&Mask_get_graphic_m3015_MethodInfo,
	&Mask_get_showMaskGraphic_m3016_MethodInfo,
	&Mask_set_showMaskGraphic_m3017_MethodInfo,
	&Mask_get_rectTransform_m3018_MethodInfo,
	&Mask_MaskEnabled_m3019_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m3020_MethodInfo,
	&Mask_NotifyMaskStateChanged_m3021_MethodInfo,
	&Mask_ClearCachedMaterial_m3022_MethodInfo,
	&Mask_OnEnable_m3023_MethodInfo,
	&Mask_OnDisable_m3024_MethodInfo,
	&Mask_IsRaycastLocationValid_m3025_MethodInfo,
	&Mask_GetModifiedMaterial_m3026_MethodInfo,
	NULL
};
extern const MethodInfo Mask_get_graphic_m3015_MethodInfo;
static const PropertyInfo Mask_t619____graphic_PropertyInfo = 
{
	&Mask_t619_il2cpp_TypeInfo/* parent */
	, "graphic"/* name */
	, &Mask_get_graphic_m3015_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mask_get_showMaskGraphic_m3016_MethodInfo;
extern const MethodInfo Mask_set_showMaskGraphic_m3017_MethodInfo;
static const PropertyInfo Mask_t619____showMaskGraphic_PropertyInfo = 
{
	&Mask_t619_il2cpp_TypeInfo/* parent */
	, "showMaskGraphic"/* name */
	, &Mask_get_showMaskGraphic_m3016_MethodInfo/* get */
	, &Mask_set_showMaskGraphic_m3017_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mask_get_rectTransform_m3018_MethodInfo;
static const PropertyInfo Mask_t619____rectTransform_PropertyInfo = 
{
	&Mask_t619_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &Mask_get_rectTransform_m3018_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Mask_t619_PropertyInfos[] =
{
	&Mask_t619____graphic_PropertyInfo,
	&Mask_t619____showMaskGraphic_PropertyInfo,
	&Mask_t619____rectTransform_PropertyInfo,
	NULL
};
extern const MethodInfo Mask_OnEnable_m3023_MethodInfo;
extern const MethodInfo Mask_OnDisable_m3024_MethodInfo;
extern const MethodInfo Mask_OnSiblingGraphicEnabledDisabled_m3020_MethodInfo;
extern const MethodInfo Mask_MaskEnabled_m3019_MethodInfo;
extern const MethodInfo Mask_IsRaycastLocationValid_m3025_MethodInfo;
extern const MethodInfo Mask_GetModifiedMaterial_m3026_MethodInfo;
static const Il2CppMethodReference Mask_t619_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&Mask_OnEnable_m3023_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&Mask_OnDisable_m3024_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m1984_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m3020_MethodInfo,
	&Mask_MaskEnabled_m3019_MethodInfo,
	&Mask_IsRaycastLocationValid_m3025_MethodInfo,
	&Mask_GetModifiedMaterial_m3026_MethodInfo,
	&Mask_MaskEnabled_m3019_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m3020_MethodInfo,
	&Mask_IsRaycastLocationValid_m3025_MethodInfo,
	&Mask_GetModifiedMaterial_m3026_MethodInfo,
};
static bool Mask_t619_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IGraphicEnabledDisabled_t670_0_0_0;
extern const Il2CppType IMask_t692_0_0_0;
extern const Il2CppType ICanvasRaycastFilter_t673_0_0_0;
static const Il2CppType* Mask_t619_InterfacesTypeInfos[] = 
{
	&IGraphicEnabledDisabled_t670_0_0_0,
	&IMask_t692_0_0_0,
	&ICanvasRaycastFilter_t673_0_0_0,
	&IMaterialModifier_t669_0_0_0,
};
static Il2CppInterfaceOffsetPair Mask_t619_InterfacesOffsets[] = 
{
	{ &IGraphicEnabledDisabled_t670_0_0_0, 16},
	{ &IMask_t692_0_0_0, 17},
	{ &ICanvasRaycastFilter_t673_0_0_0, 18},
	{ &IMaterialModifier_t669_0_0_0, 19},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Mask_t619_0_0_0;
extern const Il2CppType Mask_t619_1_0_0;
struct Mask_t619;
const Il2CppTypeDefinitionMetadata Mask_t619_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Mask_t619_InterfacesTypeInfos/* implementedInterfaces */
	, Mask_t619_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t455_0_0_0/* parent */
	, Mask_t619_VTable/* vtableMethods */
	, Mask_t619_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 611/* fieldStart */

};
TypeInfo Mask_t619_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mask"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Mask_t619_MethodInfos/* methods */
	, Mask_t619_PropertyInfos/* properties */
	, NULL/* events */
	, &Mask_t619_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 333/* custom_attributes_cache */
	, &Mask_t619_0_0_0/* byval_arg */
	, &Mask_t619_1_0_0/* this_arg */
	, &Mask_t619_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mask_t619)/* instance_size */
	, sizeof (Mask_t619)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.Collections.IndexedSet`1
extern TypeInfo IndexedSet_1_t729_il2cpp_TypeInfo;
extern const Il2CppGenericContainer IndexedSet_1_t729_Il2CppGenericContainer;
extern TypeInfo IndexedSet_1_t729_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter IndexedSet_1_t729_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &IndexedSet_1_t729_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* IndexedSet_1_t729_Il2CppGenericParametersArray[1] = 
{
	&IndexedSet_1_t729_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer IndexedSet_1_t729_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&IndexedSet_1_t729_il2cpp_TypeInfo, 1, 0, IndexedSet_1_t729_Il2CppGenericParametersArray };
// System.Void UnityEngine.UI.Collections.IndexedSet`1::.ctor()
extern const MethodInfo IndexedSet_1__ctor_m3555_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_t217_0_0_0;
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m3556_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t729_gp_0_0_0_0;
extern const Il2CppType IndexedSet_1_t729_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t729_IndexedSet_1_Add_m3557_ParameterInfos[] = 
{
	{"item", 0, 134218428, 0, &IndexedSet_1_t729_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Add(T)
extern const MethodInfo IndexedSet_1_Add_m3557_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t729_IndexedSet_1_Add_m3557_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t729_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t729_IndexedSet_1_Remove_m3558_ParameterInfos[] = 
{
	{"item", 0, 134218429, 0, &IndexedSet_1_t729_gp_0_0_0_0},
};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::Remove(T)
extern const MethodInfo IndexedSet_1_Remove_m3558_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t729_IndexedSet_1_Remove_m3558_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t761_0_0_0;
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1::GetEnumerator()
extern const MethodInfo IndexedSet_1_GetEnumerator_m3559_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t761_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Clear()
extern const MethodInfo IndexedSet_1_Clear_m3560_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t729_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t729_IndexedSet_1_Contains_m3561_ParameterInfos[] = 
{
	{"item", 0, 134218430, 0, &IndexedSet_1_t729_gp_0_0_0_0},
};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::Contains(T)
extern const MethodInfo IndexedSet_1_Contains_m3561_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t729_IndexedSet_1_Contains_m3561_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TU5BU5D_t762_0_0_0;
extern const Il2CppType TU5BU5D_t762_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo IndexedSet_1_t729_IndexedSet_1_CopyTo_m3562_ParameterInfos[] = 
{
	{"array", 0, 134218431, 0, &TU5BU5D_t762_0_0_0},
	{"arrayIndex", 1, 134218432, 0, &Int32_t253_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::CopyTo(T[],System.Int32)
extern const MethodInfo IndexedSet_1_CopyTo_m3562_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t729_IndexedSet_1_CopyTo_m3562_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1::get_Count()
extern const MethodInfo IndexedSet_1_get_Count_m3563_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::get_IsReadOnly()
extern const MethodInfo IndexedSet_1_get_IsReadOnly_m3564_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t729_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t729_IndexedSet_1_IndexOf_m3565_ParameterInfos[] = 
{
	{"item", 0, 134218433, 0, &IndexedSet_1_t729_gp_0_0_0_0},
};
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1::IndexOf(T)
extern const MethodInfo IndexedSet_1_IndexOf_m3565_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t729_IndexedSet_1_IndexOf_m3565_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType IndexedSet_1_t729_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t729_IndexedSet_1_Insert_m3566_ParameterInfos[] = 
{
	{"index", 0, 134218434, 0, &Int32_t253_0_0_0},
	{"item", 1, 134218435, 0, &IndexedSet_1_t729_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Insert(System.Int32,T)
extern const MethodInfo IndexedSet_1_Insert_m3566_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t729_IndexedSet_1_Insert_m3566_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo IndexedSet_1_t729_IndexedSet_1_RemoveAt_m3567_ParameterInfos[] = 
{
	{"index", 0, 134218436, 0, &Int32_t253_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::RemoveAt(System.Int32)
extern const MethodInfo IndexedSet_1_RemoveAt_m3567_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t729_IndexedSet_1_RemoveAt_m3567_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo IndexedSet_1_t729_IndexedSet_1_get_Item_m3568_ParameterInfos[] = 
{
	{"index", 0, 134218437, 0, &Int32_t253_0_0_0},
};
// T UnityEngine.UI.Collections.IndexedSet`1::get_Item(System.Int32)
extern const MethodInfo IndexedSet_1_get_Item_m3568_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &IndexedSet_1_t729_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t729_IndexedSet_1_get_Item_m3568_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType IndexedSet_1_t729_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t729_IndexedSet_1_set_Item_m3569_ParameterInfos[] = 
{
	{"index", 0, 134218438, 0, &Int32_t253_0_0_0},
	{"value", 1, 134218439, 0, &IndexedSet_1_t729_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::set_Item(System.Int32,T)
extern const MethodInfo IndexedSet_1_set_Item_m3569_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t729_IndexedSet_1_set_Item_m3569_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t763_0_0_0;
extern const Il2CppType Predicate_1_t763_0_0_0;
static const ParameterInfo IndexedSet_1_t729_IndexedSet_1_RemoveAll_m3570_ParameterInfos[] = 
{
	{"match", 0, 134218440, 0, &Predicate_1_t763_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::RemoveAll(System.Predicate`1<T>)
extern const MethodInfo IndexedSet_1_RemoveAll_m3570_MethodInfo = 
{
	"RemoveAll"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t729_IndexedSet_1_RemoveAll_m3570_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t764_0_0_0;
extern const Il2CppType Comparison_1_t764_0_0_0;
static const ParameterInfo IndexedSet_1_t729_IndexedSet_1_Sort_m3571_ParameterInfos[] = 
{
	{"sortLayoutFunction", 0, 134218441, 0, &Comparison_1_t764_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Sort(System.Comparison`1<T>)
extern const MethodInfo IndexedSet_1_Sort_m3571_MethodInfo = 
{
	"Sort"/* name */
	, NULL/* method */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t729_IndexedSet_1_Sort_m3571_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IndexedSet_1_t729_MethodInfos[] =
{
	&IndexedSet_1__ctor_m3555_MethodInfo,
	&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m3556_MethodInfo,
	&IndexedSet_1_Add_m3557_MethodInfo,
	&IndexedSet_1_Remove_m3558_MethodInfo,
	&IndexedSet_1_GetEnumerator_m3559_MethodInfo,
	&IndexedSet_1_Clear_m3560_MethodInfo,
	&IndexedSet_1_Contains_m3561_MethodInfo,
	&IndexedSet_1_CopyTo_m3562_MethodInfo,
	&IndexedSet_1_get_Count_m3563_MethodInfo,
	&IndexedSet_1_get_IsReadOnly_m3564_MethodInfo,
	&IndexedSet_1_IndexOf_m3565_MethodInfo,
	&IndexedSet_1_Insert_m3566_MethodInfo,
	&IndexedSet_1_RemoveAt_m3567_MethodInfo,
	&IndexedSet_1_get_Item_m3568_MethodInfo,
	&IndexedSet_1_set_Item_m3569_MethodInfo,
	&IndexedSet_1_RemoveAll_m3570_MethodInfo,
	&IndexedSet_1_Sort_m3571_MethodInfo,
	NULL
};
extern const MethodInfo IndexedSet_1_get_Count_m3563_MethodInfo;
static const PropertyInfo IndexedSet_1_t729____Count_PropertyInfo = 
{
	&IndexedSet_1_t729_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IndexedSet_1_get_Count_m3563_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IndexedSet_1_get_IsReadOnly_m3564_MethodInfo;
static const PropertyInfo IndexedSet_1_t729____IsReadOnly_PropertyInfo = 
{
	&IndexedSet_1_t729_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &IndexedSet_1_get_IsReadOnly_m3564_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IndexedSet_1_get_Item_m3568_MethodInfo;
extern const MethodInfo IndexedSet_1_set_Item_m3569_MethodInfo;
static const PropertyInfo IndexedSet_1_t729____Item_PropertyInfo = 
{
	&IndexedSet_1_t729_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IndexedSet_1_get_Item_m3568_MethodInfo/* get */
	, &IndexedSet_1_set_Item_m3569_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IndexedSet_1_t729_PropertyInfos[] =
{
	&IndexedSet_1_t729____Count_PropertyInfo,
	&IndexedSet_1_t729____IsReadOnly_PropertyInfo,
	&IndexedSet_1_t729____Item_PropertyInfo,
	NULL
};
extern const MethodInfo IndexedSet_1_IndexOf_m3565_MethodInfo;
extern const MethodInfo IndexedSet_1_Insert_m3566_MethodInfo;
extern const MethodInfo IndexedSet_1_RemoveAt_m3567_MethodInfo;
extern const MethodInfo IndexedSet_1_Add_m3557_MethodInfo;
extern const MethodInfo IndexedSet_1_Clear_m3560_MethodInfo;
extern const MethodInfo IndexedSet_1_Contains_m3561_MethodInfo;
extern const MethodInfo IndexedSet_1_CopyTo_m3562_MethodInfo;
extern const MethodInfo IndexedSet_1_Remove_m3558_MethodInfo;
extern const MethodInfo IndexedSet_1_GetEnumerator_m3559_MethodInfo;
extern const MethodInfo IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m3556_MethodInfo;
static const Il2CppMethodReference IndexedSet_1_t729_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&IndexedSet_1_IndexOf_m3565_MethodInfo,
	&IndexedSet_1_Insert_m3566_MethodInfo,
	&IndexedSet_1_RemoveAt_m3567_MethodInfo,
	&IndexedSet_1_get_Item_m3568_MethodInfo,
	&IndexedSet_1_set_Item_m3569_MethodInfo,
	&IndexedSet_1_get_Count_m3563_MethodInfo,
	&IndexedSet_1_get_IsReadOnly_m3564_MethodInfo,
	&IndexedSet_1_Add_m3557_MethodInfo,
	&IndexedSet_1_Clear_m3560_MethodInfo,
	&IndexedSet_1_Contains_m3561_MethodInfo,
	&IndexedSet_1_CopyTo_m3562_MethodInfo,
	&IndexedSet_1_Remove_m3558_MethodInfo,
	&IndexedSet_1_GetEnumerator_m3559_MethodInfo,
	&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m3556_MethodInfo,
};
static bool IndexedSet_1_t729_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IList_1_t765_0_0_0;
extern const Il2CppType ICollection_1_t766_0_0_0;
extern const Il2CppType IEnumerable_1_t767_0_0_0;
extern const Il2CppType IEnumerable_t438_0_0_0;
static const Il2CppType* IndexedSet_1_t729_InterfacesTypeInfos[] = 
{
	&IList_1_t765_0_0_0,
	&ICollection_1_t766_0_0_0,
	&IEnumerable_1_t767_0_0_0,
	&IEnumerable_t438_0_0_0,
};
static Il2CppInterfaceOffsetPair IndexedSet_1_t729_InterfacesOffsets[] = 
{
	{ &IList_1_t765_0_0_0, 4},
	{ &ICollection_1_t766_0_0_0, 9},
	{ &IEnumerable_1_t767_0_0_0, 16},
	{ &IEnumerable_t438_0_0_0, 17},
};
extern const Il2CppType List_1_t768_0_0_0;
extern const Il2CppGenericMethod List_1__ctor_m3613_GenericMethod;
extern const Il2CppType Dictionary_2_t769_0_0_0;
extern const Il2CppGenericMethod Dictionary_2__ctor_m3614_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_GetEnumerator_m3615_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_ContainsKey_m3616_GenericMethod;
extern const Il2CppGenericMethod List_1_Add_m3617_GenericMethod;
extern const Il2CppGenericMethod List_1_get_Count_m3618_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Add_m3619_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_TryGetValue_m3620_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_RemoveAt_m3621_GenericMethod;
extern const Il2CppGenericMethod List_1_Clear_m3622_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Clear_m3623_GenericMethod;
extern const Il2CppGenericMethod List_1_CopyTo_m3624_GenericMethod;
extern const Il2CppGenericMethod List_1_get_Item_m3625_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Remove_m3626_GenericMethod;
extern const Il2CppGenericMethod List_1_RemoveAt_m3627_GenericMethod;
extern const Il2CppGenericMethod List_1_set_Item_m3628_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_set_Item_m3629_GenericMethod;
extern const Il2CppGenericMethod Predicate_1_Invoke_m3630_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_Remove_m3631_GenericMethod;
extern const Il2CppGenericMethod List_1_Sort_m3632_GenericMethod;
static Il2CppRGCTXDefinition IndexedSet_1_t729_RGCTXData[23] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&List_1_t768_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m3613_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Dictionary_2_t769_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m3614_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_GetEnumerator_m3615_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_ContainsKey_m3616_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Add_m3617_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_get_Count_m3618_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Add_m3619_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_TryGetValue_m3620_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_RemoveAt_m3621_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Clear_m3622_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Clear_m3623_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_CopyTo_m3624_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_get_Item_m3625_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Remove_m3626_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_RemoveAt_m3627_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_set_Item_m3628_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_set_Item_m3629_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Predicate_1_Invoke_m3630_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_Remove_m3631_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Sort_m3632_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IndexedSet_1_t729_0_0_0;
extern const Il2CppType IndexedSet_1_t729_1_0_0;
struct IndexedSet_1_t729;
const Il2CppTypeDefinitionMetadata IndexedSet_1_t729_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IndexedSet_1_t729_InterfacesTypeInfos/* implementedInterfaces */
	, IndexedSet_1_t729_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IndexedSet_1_t729_VTable/* vtableMethods */
	, IndexedSet_1_t729_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, IndexedSet_1_t729_RGCTXData/* rgctxDefinition */
	, 615/* fieldStart */

};
TypeInfo IndexedSet_1_t729_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IndexedSet`1"/* name */
	, "UnityEngine.UI.Collections"/* namespaze */
	, IndexedSet_1_t729_MethodInfos/* methods */
	, IndexedSet_1_t729_PropertyInfos/* properties */
	, NULL/* events */
	, &IndexedSet_1_t729_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 335/* custom_attributes_cache */
	, &IndexedSet_1_t729_0_0_0/* byval_arg */
	, &IndexedSet_1_t729_1_0_0/* this_arg */
	, &IndexedSet_1_t729_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &IndexedSet_1_t729_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPool.h"
// Metadata Definition UnityEngine.UI.CanvasListPool
extern TypeInfo CanvasListPool_t622_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPoolMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::.cctor()
extern const MethodInfo CanvasListPool__cctor_m3027_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CanvasListPool__cctor_m3027/* method */
	, &CanvasListPool_t622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t653_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Canvas> UnityEngine.UI.CanvasListPool::Get()
extern const MethodInfo CanvasListPool_Get_m3028_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&CanvasListPool_Get_m3028/* method */
	, &CanvasListPool_t622_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t653_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t653_0_0_0;
static const ParameterInfo CanvasListPool_t622_CanvasListPool_Release_m3029_ParameterInfos[] = 
{
	{"toRelease", 0, 134218442, 0, &List_1_t653_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::Release(System.Collections.Generic.List`1<UnityEngine.Canvas>)
extern const MethodInfo CanvasListPool_Release_m3029_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&CanvasListPool_Release_m3029/* method */
	, &CanvasListPool_t622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CanvasListPool_t622_CanvasListPool_Release_m3029_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t653_0_0_0;
static const ParameterInfo CanvasListPool_t622_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m3030_ParameterInfos[] = 
{
	{"l", 0, 134218443, 0, &List_1_t653_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::<s_CanvasListPool>m__16(System.Collections.Generic.List`1<UnityEngine.Canvas>)
extern const MethodInfo CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m3030_MethodInfo = 
{
	"<s_CanvasListPool>m__16"/* name */
	, (methodPointerType)&CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m3030/* method */
	, &CanvasListPool_t622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CanvasListPool_t622_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m3030_ParameterInfos/* parameters */
	, 337/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CanvasListPool_t622_MethodInfos[] =
{
	&CanvasListPool__cctor_m3027_MethodInfo,
	&CanvasListPool_Get_m3028_MethodInfo,
	&CanvasListPool_Release_m3029_MethodInfo,
	&CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m3030_MethodInfo,
	NULL
};
static const Il2CppMethodReference CanvasListPool_t622_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CanvasListPool_t622_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasListPool_t622_0_0_0;
extern const Il2CppType CanvasListPool_t622_1_0_0;
struct CanvasListPool_t622;
const Il2CppTypeDefinitionMetadata CanvasListPool_t622_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CanvasListPool_t622_VTable/* vtableMethods */
	, CanvasListPool_t622_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 617/* fieldStart */

};
TypeInfo CanvasListPool_t622_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, CanvasListPool_t622_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CanvasListPool_t622_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasListPool_t622_0_0_0/* byval_arg */
	, &CanvasListPool_t622_1_0_0/* this_arg */
	, &CanvasListPool_t622_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasListPool_t622)/* instance_size */
	, sizeof (CanvasListPool_t622)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CanvasListPool_t622_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPool.h"
// Metadata Definition UnityEngine.UI.ComponentListPool
extern TypeInfo ComponentListPool_t625_il2cpp_TypeInfo;
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPoolMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::.cctor()
extern const MethodInfo ComponentListPool__cctor_m3031_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ComponentListPool__cctor_m3031/* method */
	, &ComponentListPool_t625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Component> UnityEngine.UI.ComponentListPool::Get()
extern const MethodInfo ComponentListPool_Get_m3032_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&ComponentListPool_Get_m3032/* method */
	, &ComponentListPool_t625_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t651_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t651_0_0_0;
static const ParameterInfo ComponentListPool_t625_ComponentListPool_Release_m3033_ParameterInfos[] = 
{
	{"toRelease", 0, 134218444, 0, &List_1_t651_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::Release(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo ComponentListPool_Release_m3033_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&ComponentListPool_Release_m3033/* method */
	, &ComponentListPool_t625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ComponentListPool_t625_ComponentListPool_Release_m3033_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t651_0_0_0;
static const ParameterInfo ComponentListPool_t625_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m3034_ParameterInfos[] = 
{
	{"l", 0, 134218445, 0, &List_1_t651_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::<s_ComponentListPool>m__17(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m3034_MethodInfo = 
{
	"<s_ComponentListPool>m__17"/* name */
	, (methodPointerType)&ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m3034/* method */
	, &ComponentListPool_t625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ComponentListPool_t625_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m3034_ParameterInfos/* parameters */
	, 339/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ComponentListPool_t625_MethodInfos[] =
{
	&ComponentListPool__cctor_m3031_MethodInfo,
	&ComponentListPool_Get_m3032_MethodInfo,
	&ComponentListPool_Release_m3033_MethodInfo,
	&ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m3034_MethodInfo,
	NULL
};
static const Il2CppMethodReference ComponentListPool_t625_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ComponentListPool_t625_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ComponentListPool_t625_0_0_0;
extern const Il2CppType ComponentListPool_t625_1_0_0;
struct ComponentListPool_t625;
const Il2CppTypeDefinitionMetadata ComponentListPool_t625_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ComponentListPool_t625_VTable/* vtableMethods */
	, ComponentListPool_t625_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 619/* fieldStart */

};
TypeInfo ComponentListPool_t625_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComponentListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ComponentListPool_t625_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ComponentListPool_t625_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ComponentListPool_t625_0_0_0/* byval_arg */
	, &ComponentListPool_t625_1_0_0/* this_arg */
	, &ComponentListPool_t625_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComponentListPool_t625)/* instance_size */
	, sizeof (ComponentListPool_t625)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ComponentListPool_t625_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ObjectPool`1
extern TypeInfo ObjectPool_1_t730_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ObjectPool_1_t730_Il2CppGenericContainer;
extern TypeInfo ObjectPool_1_t730_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ObjectPool_1_t730_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &ObjectPool_1_t730_Il2CppGenericContainer, NULL, "T", 0, 16 };
static const Il2CppGenericParameter* ObjectPool_1_t730_Il2CppGenericParametersArray[1] = 
{
	&ObjectPool_1_t730_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ObjectPool_1_t730_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ObjectPool_1_t730_il2cpp_TypeInfo, 1, 0, ObjectPool_1_t730_Il2CppGenericParametersArray };
extern const Il2CppType UnityAction_1_t771_0_0_0;
extern const Il2CppType UnityAction_1_t771_0_0_0;
extern const Il2CppType UnityAction_1_t771_0_0_0;
static const ParameterInfo ObjectPool_1_t730_ObjectPool_1__ctor_m3572_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134218446, 0, &UnityAction_1_t771_0_0_0},
	{"actionOnRelease", 1, 134218447, 0, &UnityAction_1_t771_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern const MethodInfo ObjectPool_1__ctor_m3572_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ObjectPool_1_t730_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t730_ObjectPool_1__ctor_m3572_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countAll()
extern const MethodInfo ObjectPool_1_get_countAll_m3573_MethodInfo = 
{
	"get_countAll"/* name */
	, NULL/* method */
	, &ObjectPool_1_t730_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 341/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo ObjectPool_1_t730_ObjectPool_1_set_countAll_m3574_ParameterInfos[] = 
{
	{"value", 0, 134218448, 0, &Int32_t253_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::set_countAll(System.Int32)
extern const MethodInfo ObjectPool_1_set_countAll_m3574_MethodInfo = 
{
	"set_countAll"/* name */
	, NULL/* method */
	, &ObjectPool_1_t730_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t730_ObjectPool_1_set_countAll_m3574_ParameterInfos/* parameters */
	, 342/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countActive()
extern const MethodInfo ObjectPool_1_get_countActive_m3575_MethodInfo = 
{
	"get_countActive"/* name */
	, NULL/* method */
	, &ObjectPool_1_t730_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countInactive()
extern const MethodInfo ObjectPool_1_get_countInactive_m3576_MethodInfo = 
{
	"get_countInactive"/* name */
	, NULL/* method */
	, &ObjectPool_1_t730_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectPool_1_t730_gp_0_0_0_0;
// T UnityEngine.UI.ObjectPool`1::Get()
extern const MethodInfo ObjectPool_1_Get_m3577_MethodInfo = 
{
	"Get"/* name */
	, NULL/* method */
	, &ObjectPool_1_t730_il2cpp_TypeInfo/* declaring_type */
	, &ObjectPool_1_t730_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectPool_1_t730_gp_0_0_0_0;
static const ParameterInfo ObjectPool_1_t730_ObjectPool_1_Release_m3578_ParameterInfos[] = 
{
	{"element", 0, 134218449, 0, &ObjectPool_1_t730_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::Release(T)
extern const MethodInfo ObjectPool_1_Release_m3578_MethodInfo = 
{
	"Release"/* name */
	, NULL/* method */
	, &ObjectPool_1_t730_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t730_ObjectPool_1_Release_m3578_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectPool_1_t730_MethodInfos[] =
{
	&ObjectPool_1__ctor_m3572_MethodInfo,
	&ObjectPool_1_get_countAll_m3573_MethodInfo,
	&ObjectPool_1_set_countAll_m3574_MethodInfo,
	&ObjectPool_1_get_countActive_m3575_MethodInfo,
	&ObjectPool_1_get_countInactive_m3576_MethodInfo,
	&ObjectPool_1_Get_m3577_MethodInfo,
	&ObjectPool_1_Release_m3578_MethodInfo,
	NULL
};
extern const MethodInfo ObjectPool_1_get_countAll_m3573_MethodInfo;
extern const MethodInfo ObjectPool_1_set_countAll_m3574_MethodInfo;
static const PropertyInfo ObjectPool_1_t730____countAll_PropertyInfo = 
{
	&ObjectPool_1_t730_il2cpp_TypeInfo/* parent */
	, "countAll"/* name */
	, &ObjectPool_1_get_countAll_m3573_MethodInfo/* get */
	, &ObjectPool_1_set_countAll_m3574_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjectPool_1_get_countActive_m3575_MethodInfo;
static const PropertyInfo ObjectPool_1_t730____countActive_PropertyInfo = 
{
	&ObjectPool_1_t730_il2cpp_TypeInfo/* parent */
	, "countActive"/* name */
	, &ObjectPool_1_get_countActive_m3575_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjectPool_1_get_countInactive_m3576_MethodInfo;
static const PropertyInfo ObjectPool_1_t730____countInactive_PropertyInfo = 
{
	&ObjectPool_1_t730_il2cpp_TypeInfo/* parent */
	, "countInactive"/* name */
	, &ObjectPool_1_get_countInactive_m3576_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectPool_1_t730_PropertyInfos[] =
{
	&ObjectPool_1_t730____countAll_PropertyInfo,
	&ObjectPool_1_t730____countActive_PropertyInfo,
	&ObjectPool_1_t730____countInactive_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ObjectPool_1_t730_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ObjectPool_1_t730_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType Stack_1_t772_0_0_0;
extern const Il2CppGenericMethod Stack_1__ctor_m3633_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_get_countAll_m3634_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_get_countInactive_m3635_GenericMethod;
extern const Il2CppGenericMethod Stack_1_get_Count_m3636_GenericMethod;
extern const Il2CppGenericMethod Activator_CreateInstance_TisT_t770_m3637_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_set_countAll_m3638_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Pop_m3639_GenericMethod;
extern const Il2CppGenericMethod UnityAction_1_Invoke_m3640_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Peek_m3641_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Push_m3642_GenericMethod;
static Il2CppRGCTXDefinition ObjectPool_1_t730_RGCTXData[13] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Stack_1_t772_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1__ctor_m3633_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_get_countAll_m3634_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_get_countInactive_m3635_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_get_Count_m3636_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ObjectPool_1_t730_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Activator_CreateInstance_TisT_t770_m3637_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_set_countAll_m3638_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Pop_m3639_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_1_Invoke_m3640_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Peek_m3641_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Push_m3642_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ObjectPool_1_t730_0_0_0;
extern const Il2CppType ObjectPool_1_t730_1_0_0;
struct ObjectPool_1_t730;
const Il2CppTypeDefinitionMetadata ObjectPool_1_t730_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectPool_1_t730_VTable/* vtableMethods */
	, ObjectPool_1_t730_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, ObjectPool_1_t730_RGCTXData/* rgctxDefinition */
	, 621/* fieldStart */

};
TypeInfo ObjectPool_1_t730_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectPool`1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ObjectPool_1_t730_MethodInfos/* methods */
	, ObjectPool_1_t730_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectPool_1_t730_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectPool_1_t730_0_0_0/* byval_arg */
	, &ObjectPool_1_t730_1_0_0/* this_arg */
	, &ObjectPool_1_t730_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ObjectPool_1_t730_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"
// Metadata Definition UnityEngine.UI.BaseVertexEffect
extern TypeInfo BaseVertexEffect_t626_il2cpp_TypeInfo;
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffectMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
extern const MethodInfo BaseVertexEffect__ctor_m3035_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseVertexEffect__ctor_m3035/* method */
	, &BaseVertexEffect_t626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::get_graphic()
extern const MethodInfo BaseVertexEffect_get_graphic_m3036_MethodInfo = 
{
	"get_graphic"/* name */
	, (methodPointerType)&BaseVertexEffect_get_graphic_m3036/* method */
	, &BaseVertexEffect_t626_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t418_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::OnEnable()
extern const MethodInfo BaseVertexEffect_OnEnable_m3037_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&BaseVertexEffect_OnEnable_m3037/* method */
	, &BaseVertexEffect_t626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::OnDisable()
extern const MethodInfo BaseVertexEffect_OnDisable_m3038_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&BaseVertexEffect_OnDisable_m3038/* method */
	, &BaseVertexEffect_t626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t558_0_0_0;
static const ParameterInfo BaseVertexEffect_t626_BaseVertexEffect_ModifyVertices_m3579_ParameterInfos[] = 
{
	{"verts", 0, 134218450, 0, &List_1_t558_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo BaseVertexEffect_ModifyVertices_m3579_MethodInfo = 
{
	"ModifyVertices"/* name */
	, NULL/* method */
	, &BaseVertexEffect_t626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, BaseVertexEffect_t626_BaseVertexEffect_ModifyVertices_m3579_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BaseVertexEffect_t626_MethodInfos[] =
{
	&BaseVertexEffect__ctor_m3035_MethodInfo,
	&BaseVertexEffect_get_graphic_m3036_MethodInfo,
	&BaseVertexEffect_OnEnable_m3037_MethodInfo,
	&BaseVertexEffect_OnDisable_m3038_MethodInfo,
	&BaseVertexEffect_ModifyVertices_m3579_MethodInfo,
	NULL
};
extern const MethodInfo BaseVertexEffect_get_graphic_m3036_MethodInfo;
static const PropertyInfo BaseVertexEffect_t626____graphic_PropertyInfo = 
{
	&BaseVertexEffect_t626_il2cpp_TypeInfo/* parent */
	, "graphic"/* name */
	, &BaseVertexEffect_get_graphic_m3036_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* BaseVertexEffect_t626_PropertyInfos[] =
{
	&BaseVertexEffect_t626____graphic_PropertyInfo,
	NULL
};
extern const MethodInfo BaseVertexEffect_OnEnable_m3037_MethodInfo;
extern const MethodInfo BaseVertexEffect_OnDisable_m3038_MethodInfo;
extern const MethodInfo BaseVertexEffect_ModifyVertices_m3579_MethodInfo;
static const Il2CppMethodReference BaseVertexEffect_t626_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&BaseVertexEffect_OnEnable_m3037_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&BaseVertexEffect_OnDisable_m3038_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m1984_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&BaseVertexEffect_ModifyVertices_m3579_MethodInfo,
	NULL,
};
static bool BaseVertexEffect_t626_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IVertexModifier_t671_0_0_0;
static const Il2CppType* BaseVertexEffect_t626_InterfacesTypeInfos[] = 
{
	&IVertexModifier_t671_0_0_0,
};
static Il2CppInterfaceOffsetPair BaseVertexEffect_t626_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t671_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType BaseVertexEffect_t626_0_0_0;
extern const Il2CppType BaseVertexEffect_t626_1_0_0;
struct BaseVertexEffect_t626;
const Il2CppTypeDefinitionMetadata BaseVertexEffect_t626_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BaseVertexEffect_t626_InterfacesTypeInfos/* implementedInterfaces */
	, BaseVertexEffect_t626_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t455_0_0_0/* parent */
	, BaseVertexEffect_t626_VTable/* vtableMethods */
	, BaseVertexEffect_t626_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 625/* fieldStart */

};
TypeInfo BaseVertexEffect_t626_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseVertexEffect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, BaseVertexEffect_t626_MethodInfos/* methods */
	, BaseVertexEffect_t626_PropertyInfos/* properties */
	, NULL/* events */
	, &BaseVertexEffect_t626_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 343/* custom_attributes_cache */
	, &BaseVertexEffect_t626_0_0_0/* byval_arg */
	, &BaseVertexEffect_t626_1_0_0/* this_arg */
	, &BaseVertexEffect_t626_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseVertexEffect_t626)/* instance_size */
	, sizeof (BaseVertexEffect_t626)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IVertexModifier
extern TypeInfo IVertexModifier_t671_il2cpp_TypeInfo;
extern const Il2CppType List_1_t558_0_0_0;
static const ParameterInfo IVertexModifier_t671_IVertexModifier_ModifyVertices_m3580_ParameterInfos[] = 
{
	{"verts", 0, 134218451, 0, &List_1_t558_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.IVertexModifier::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo IVertexModifier_ModifyVertices_m3580_MethodInfo = 
{
	"ModifyVertices"/* name */
	, NULL/* method */
	, &IVertexModifier_t671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, IVertexModifier_t671_IVertexModifier_ModifyVertices_m3580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IVertexModifier_t671_MethodInfos[] =
{
	&IVertexModifier_ModifyVertices_m3580_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IVertexModifier_t671_1_0_0;
struct IVertexModifier_t671;
const Il2CppTypeDefinitionMetadata IVertexModifier_t671_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IVertexModifier_t671_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IVertexModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, IVertexModifier_t671_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IVertexModifier_t671_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IVertexModifier_t671_0_0_0/* byval_arg */
	, &IVertexModifier_t671_1_0_0/* this_arg */
	, &IVertexModifier_t671_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_Outline.h"
// Metadata Definition UnityEngine.UI.Outline
extern TypeInfo Outline_t627_il2cpp_TypeInfo;
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_OutlineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Outline::.ctor()
extern const MethodInfo Outline__ctor_m3039_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Outline__ctor_m3039/* method */
	, &Outline_t627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t558_0_0_0;
static const ParameterInfo Outline_t627_Outline_ModifyVertices_m3040_ParameterInfos[] = 
{
	{"verts", 0, 134218452, 0, &List_1_t558_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Outline_ModifyVertices_m3040_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&Outline_ModifyVertices_m3040/* method */
	, &Outline_t627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Outline_t627_Outline_ModifyVertices_m3040_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Outline_t627_MethodInfos[] =
{
	&Outline__ctor_m3039_MethodInfo,
	&Outline_ModifyVertices_m3040_MethodInfo,
	NULL
};
extern const MethodInfo Outline_ModifyVertices_m3040_MethodInfo;
static const Il2CppMethodReference Outline_t627_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&BaseVertexEffect_OnEnable_m3037_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&BaseVertexEffect_OnDisable_m3038_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m1984_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&Outline_ModifyVertices_m3040_MethodInfo,
	&Outline_ModifyVertices_m3040_MethodInfo,
};
static bool Outline_t627_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Outline_t627_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t671_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Outline_t627_0_0_0;
extern const Il2CppType Outline_t627_1_0_0;
extern const Il2CppType Shadow_t628_0_0_0;
struct Outline_t627;
const Il2CppTypeDefinitionMetadata Outline_t627_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Outline_t627_InterfacesOffsets/* interfaceOffsets */
	, &Shadow_t628_0_0_0/* parent */
	, Outline_t627_VTable/* vtableMethods */
	, Outline_t627_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Outline_t627_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Outline"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Outline_t627_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Outline_t627_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 344/* custom_attributes_cache */
	, &Outline_t627_0_0_0/* byval_arg */
	, &Outline_t627_1_0_0/* this_arg */
	, &Outline_t627_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Outline_t627)/* instance_size */
	, sizeof (Outline_t627)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1.h"
// Metadata Definition UnityEngine.UI.PositionAsUV1
extern TypeInfo PositionAsUV1_t629_il2cpp_TypeInfo;
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
extern const MethodInfo PositionAsUV1__ctor_m3041_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PositionAsUV1__ctor_m3041/* method */
	, &PositionAsUV1_t629_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t558_0_0_0;
static const ParameterInfo PositionAsUV1_t629_PositionAsUV1_ModifyVertices_m3042_ParameterInfos[] = 
{
	{"verts", 0, 134218453, 0, &List_1_t558_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.PositionAsUV1::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo PositionAsUV1_ModifyVertices_m3042_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&PositionAsUV1_ModifyVertices_m3042/* method */
	, &PositionAsUV1_t629_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, PositionAsUV1_t629_PositionAsUV1_ModifyVertices_m3042_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PositionAsUV1_t629_MethodInfos[] =
{
	&PositionAsUV1__ctor_m3041_MethodInfo,
	&PositionAsUV1_ModifyVertices_m3042_MethodInfo,
	NULL
};
extern const MethodInfo PositionAsUV1_ModifyVertices_m3042_MethodInfo;
static const Il2CppMethodReference PositionAsUV1_t629_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&BaseVertexEffect_OnEnable_m3037_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&BaseVertexEffect_OnDisable_m3038_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m1984_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&PositionAsUV1_ModifyVertices_m3042_MethodInfo,
	&PositionAsUV1_ModifyVertices_m3042_MethodInfo,
};
static bool PositionAsUV1_t629_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PositionAsUV1_t629_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t671_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType PositionAsUV1_t629_0_0_0;
extern const Il2CppType PositionAsUV1_t629_1_0_0;
struct PositionAsUV1_t629;
const Il2CppTypeDefinitionMetadata PositionAsUV1_t629_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PositionAsUV1_t629_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t626_0_0_0/* parent */
	, PositionAsUV1_t629_VTable/* vtableMethods */
	, PositionAsUV1_t629_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PositionAsUV1_t629_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "PositionAsUV1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, PositionAsUV1_t629_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PositionAsUV1_t629_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 345/* custom_attributes_cache */
	, &PositionAsUV1_t629_0_0_0/* byval_arg */
	, &PositionAsUV1_t629_1_0_0/* this_arg */
	, &PositionAsUV1_t629_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PositionAsUV1_t629)/* instance_size */
	, sizeof (PositionAsUV1_t629)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_Shadow.h"
// Metadata Definition UnityEngine.UI.Shadow
extern TypeInfo Shadow_t628_il2cpp_TypeInfo;
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_ShadowMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::.ctor()
extern const MethodInfo Shadow__ctor_m3043_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Shadow__ctor_m3043/* method */
	, &Shadow_t628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Color_t65 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Color UnityEngine.UI.Shadow::get_effectColor()
extern const MethodInfo Shadow_get_effectColor_m3044_MethodInfo = 
{
	"get_effectColor"/* name */
	, (methodPointerType)&Shadow_get_effectColor_m3044/* method */
	, &Shadow_t628_il2cpp_TypeInfo/* declaring_type */
	, &Color_t65_0_0_0/* return_type */
	, RuntimeInvoker_Color_t65/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color_t65_0_0_0;
static const ParameterInfo Shadow_t628_Shadow_set_effectColor_m3045_ParameterInfos[] = 
{
	{"value", 0, 134218454, 0, &Color_t65_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Color_t65 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_effectColor(UnityEngine.Color)
extern const MethodInfo Shadow_set_effectColor_m3045_MethodInfo = 
{
	"set_effectColor"/* name */
	, (methodPointerType)&Shadow_set_effectColor_m3045/* method */
	, &Shadow_t628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Color_t65/* invoker_method */
	, Shadow_t628_Shadow_set_effectColor_m3045_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.Shadow::get_effectDistance()
extern const MethodInfo Shadow_get_effectDistance_m3046_MethodInfo = 
{
	"get_effectDistance"/* name */
	, (methodPointerType)&Shadow_get_effectDistance_m3046/* method */
	, &Shadow_t628_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t6_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t6/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t6_0_0_0;
static const ParameterInfo Shadow_t628_Shadow_set_effectDistance_m3047_ParameterInfos[] = 
{
	{"value", 0, 134218455, 0, &Vector2_t6_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_effectDistance(UnityEngine.Vector2)
extern const MethodInfo Shadow_set_effectDistance_m3047_MethodInfo = 
{
	"set_effectDistance"/* name */
	, (methodPointerType)&Shadow_set_effectDistance_m3047/* method */
	, &Shadow_t628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Vector2_t6/* invoker_method */
	, Shadow_t628_Shadow_set_effectDistance_m3047_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Shadow::get_useGraphicAlpha()
extern const MethodInfo Shadow_get_useGraphicAlpha_m3048_MethodInfo = 
{
	"get_useGraphicAlpha"/* name */
	, (methodPointerType)&Shadow_get_useGraphicAlpha_m3048/* method */
	, &Shadow_t628_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo Shadow_t628_Shadow_set_useGraphicAlpha_m3049_ParameterInfos[] = 
{
	{"value", 0, 134218456, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_useGraphicAlpha(System.Boolean)
extern const MethodInfo Shadow_set_useGraphicAlpha_m3049_MethodInfo = 
{
	"set_useGraphicAlpha"/* name */
	, (methodPointerType)&Shadow_set_useGraphicAlpha_m3049/* method */
	, &Shadow_t628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, Shadow_t628_Shadow_set_useGraphicAlpha_m3049_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t558_0_0_0;
extern const Il2CppType Color32_t654_0_0_0;
extern const Il2CppType Color32_t654_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
static const ParameterInfo Shadow_t628_Shadow_ApplyShadow_m3050_ParameterInfos[] = 
{
	{"verts", 0, 134218457, 0, &List_1_t558_0_0_0},
	{"color", 1, 134218458, 0, &Color32_t654_0_0_0},
	{"start", 2, 134218459, 0, &Int32_t253_0_0_0},
	{"end", 3, 134218460, 0, &Int32_t253_0_0_0},
	{"x", 4, 134218461, 0, &Single_t254_0_0_0},
	{"y", 5, 134218462, 0, &Single_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Color32_t654_Int32_t253_Int32_t253_Single_t254_Single_t254 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color32,System.Int32,System.Int32,System.Single,System.Single)
extern const MethodInfo Shadow_ApplyShadow_m3050_MethodInfo = 
{
	"ApplyShadow"/* name */
	, (methodPointerType)&Shadow_ApplyShadow_m3050/* method */
	, &Shadow_t628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Color32_t654_Int32_t253_Int32_t253_Single_t254_Single_t254/* invoker_method */
	, Shadow_t628_Shadow_ApplyShadow_m3050_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t558_0_0_0;
static const ParameterInfo Shadow_t628_Shadow_ModifyVertices_m3051_ParameterInfos[] = 
{
	{"verts", 0, 134218463, 0, &List_1_t558_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Shadow_ModifyVertices_m3051_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&Shadow_ModifyVertices_m3051/* method */
	, &Shadow_t628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Shadow_t628_Shadow_ModifyVertices_m3051_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Shadow_t628_MethodInfos[] =
{
	&Shadow__ctor_m3043_MethodInfo,
	&Shadow_get_effectColor_m3044_MethodInfo,
	&Shadow_set_effectColor_m3045_MethodInfo,
	&Shadow_get_effectDistance_m3046_MethodInfo,
	&Shadow_set_effectDistance_m3047_MethodInfo,
	&Shadow_get_useGraphicAlpha_m3048_MethodInfo,
	&Shadow_set_useGraphicAlpha_m3049_MethodInfo,
	&Shadow_ApplyShadow_m3050_MethodInfo,
	&Shadow_ModifyVertices_m3051_MethodInfo,
	NULL
};
extern const MethodInfo Shadow_get_effectColor_m3044_MethodInfo;
extern const MethodInfo Shadow_set_effectColor_m3045_MethodInfo;
static const PropertyInfo Shadow_t628____effectColor_PropertyInfo = 
{
	&Shadow_t628_il2cpp_TypeInfo/* parent */
	, "effectColor"/* name */
	, &Shadow_get_effectColor_m3044_MethodInfo/* get */
	, &Shadow_set_effectColor_m3045_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Shadow_get_effectDistance_m3046_MethodInfo;
extern const MethodInfo Shadow_set_effectDistance_m3047_MethodInfo;
static const PropertyInfo Shadow_t628____effectDistance_PropertyInfo = 
{
	&Shadow_t628_il2cpp_TypeInfo/* parent */
	, "effectDistance"/* name */
	, &Shadow_get_effectDistance_m3046_MethodInfo/* get */
	, &Shadow_set_effectDistance_m3047_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Shadow_get_useGraphicAlpha_m3048_MethodInfo;
extern const MethodInfo Shadow_set_useGraphicAlpha_m3049_MethodInfo;
static const PropertyInfo Shadow_t628____useGraphicAlpha_PropertyInfo = 
{
	&Shadow_t628_il2cpp_TypeInfo/* parent */
	, "useGraphicAlpha"/* name */
	, &Shadow_get_useGraphicAlpha_m3048_MethodInfo/* get */
	, &Shadow_set_useGraphicAlpha_m3049_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Shadow_t628_PropertyInfos[] =
{
	&Shadow_t628____effectColor_PropertyInfo,
	&Shadow_t628____effectDistance_PropertyInfo,
	&Shadow_t628____useGraphicAlpha_PropertyInfo,
	NULL
};
extern const MethodInfo Shadow_ModifyVertices_m3051_MethodInfo;
static const Il2CppMethodReference Shadow_t628_VTable[] =
{
	&Object_Equals_m1047_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1049_MethodInfo,
	&Object_ToString_m1050_MethodInfo,
	&UIBehaviour_Awake_m1975_MethodInfo,
	&BaseVertexEffect_OnEnable_m3037_MethodInfo,
	&UIBehaviour_Start_m1977_MethodInfo,
	&BaseVertexEffect_OnDisable_m3038_MethodInfo,
	&UIBehaviour_OnDestroy_m1979_MethodInfo,
	&UIBehaviour_IsActive_m1980_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m1981_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m1982_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m1983_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m1984_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m1985_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m1986_MethodInfo,
	&Shadow_ModifyVertices_m3051_MethodInfo,
	&Shadow_ModifyVertices_m3051_MethodInfo,
};
static bool Shadow_t628_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Shadow_t628_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t671_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Shadow_t628_1_0_0;
struct Shadow_t628;
const Il2CppTypeDefinitionMetadata Shadow_t628_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Shadow_t628_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t626_0_0_0/* parent */
	, Shadow_t628_VTable/* vtableMethods */
	, Shadow_t628_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 626/* fieldStart */

};
TypeInfo Shadow_t628_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Shadow"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Shadow_t628_MethodInfos/* methods */
	, Shadow_t628_PropertyInfos/* properties */
	, NULL/* events */
	, &Shadow_t628_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 346/* custom_attributes_cache */
	, &Shadow_t628_0_0_0/* byval_arg */
	, &Shadow_t628_1_0_0/* this_arg */
	, &Shadow_t628_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Shadow_t628)/* instance_size */
	, sizeof (Shadow_t628)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
