﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraSwitch
struct CameraSwitch_t317;

// System.Void CameraSwitch::.ctor()
extern "C" void CameraSwitch__ctor_m1165 (CameraSwitch_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSwitch::OnEnable()
extern "C" void CameraSwitch_OnEnable_m1166 (CameraSwitch_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSwitch::NextCamera()
extern "C" void CameraSwitch_NextCamera_m1167 (CameraSwitch_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
