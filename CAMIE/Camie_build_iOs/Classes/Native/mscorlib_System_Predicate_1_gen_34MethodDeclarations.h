﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t3248;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Predicate`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m19841_gshared (Predicate_1_t3248 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m19841(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3248 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m19841_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m19842_gshared (Predicate_1_t3248 * __this, UILineInfo_t687  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m19842(__this, ___obj, method) (( bool (*) (Predicate_1_t3248 *, UILineInfo_t687 , const MethodInfo*))Predicate_1_Invoke_m19842_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m19843_gshared (Predicate_1_t3248 * __this, UILineInfo_t687  ___obj, AsyncCallback_t547 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m19843(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3248 *, UILineInfo_t687 , AsyncCallback_t547 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m19843_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m19844_gshared (Predicate_1_t3248 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m19844(__this, ___result, method) (( bool (*) (Predicate_1_t3248 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m19844_gshared)(__this, ___result, method)
