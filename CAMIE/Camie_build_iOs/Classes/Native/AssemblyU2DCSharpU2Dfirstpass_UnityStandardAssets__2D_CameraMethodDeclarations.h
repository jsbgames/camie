﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets._2D.Camera2DFollow
struct Camera2DFollow_t2;

// System.Void UnityStandardAssets._2D.Camera2DFollow::.ctor()
extern "C" void Camera2DFollow__ctor_m0 (Camera2DFollow_t2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets._2D.Camera2DFollow::Start()
extern "C" void Camera2DFollow_Start_m1 (Camera2DFollow_t2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets._2D.Camera2DFollow::Update()
extern "C" void Camera2DFollow_Update_m2 (Camera2DFollow_t2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
