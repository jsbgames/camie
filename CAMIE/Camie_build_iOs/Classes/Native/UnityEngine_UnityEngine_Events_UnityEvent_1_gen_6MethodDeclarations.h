﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityEvent`1<System.Byte>
struct UnityEvent_1_t3161;
// UnityEngine.Events.UnityAction`1<System.Byte>
struct UnityAction_1_t3162;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1002;

// System.Void UnityEngine.Events.UnityEvent`1<System.Byte>::.ctor()
extern "C" void UnityEvent_1__ctor_m18546_gshared (UnityEvent_1_t3161 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m18546(__this, method) (( void (*) (UnityEvent_1_t3161 *, const MethodInfo*))UnityEvent_1__ctor_m18546_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Byte>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_AddListener_m18548_gshared (UnityEvent_1_t3161 * __this, UnityAction_1_t3162 * ___call, const MethodInfo* method);
#define UnityEvent_1_AddListener_m18548(__this, ___call, method) (( void (*) (UnityEvent_1_t3161 *, UnityAction_1_t3162 *, const MethodInfo*))UnityEvent_1_AddListener_m18548_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Byte>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_RemoveListener_m18550_gshared (UnityEvent_1_t3161 * __this, UnityAction_1_t3162 * ___call, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m18550(__this, ___call, method) (( void (*) (UnityEvent_1_t3161 *, UnityAction_1_t3162 *, const MethodInfo*))UnityEvent_1_RemoveListener_m18550_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Byte>::FindMethod_Impl(System.String,System.Object)
extern "C" MethodInfo_t * UnityEvent_1_FindMethod_Impl_m18551_gshared (UnityEvent_1_t3161 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m18551(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t3161 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m18551_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Byte>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t1002 * UnityEvent_1_GetDelegate_m18552_gshared (UnityEvent_1_t3161 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m18552(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t1002 * (*) (UnityEvent_1_t3161 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m18552_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Byte>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C" BaseInvokableCall_t1002 * UnityEvent_1_GetDelegate_m18554_gshared (Object_t * __this /* static, unused */, UnityAction_1_t3162 * ___action, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m18554(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t1002 * (*) (Object_t * /* static, unused */, UnityAction_1_t3162 *, const MethodInfo*))UnityEvent_1_GetDelegate_m18554_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Byte>::Invoke(T0)
extern "C" void UnityEvent_1_Invoke_m18555_gshared (UnityEvent_1_t3161 * __this, uint8_t ___arg0, const MethodInfo* method);
#define UnityEvent_1_Invoke_m18555(__this, ___arg0, method) (( void (*) (UnityEvent_1_t3161 *, uint8_t, const MethodInfo*))UnityEvent_1_Invoke_m18555_gshared)(__this, ___arg0, method)
