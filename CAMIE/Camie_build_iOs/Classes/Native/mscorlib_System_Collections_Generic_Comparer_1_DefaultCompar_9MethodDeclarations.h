﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t3510;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C" void DefaultComparer__ctor_m22533_gshared (DefaultComparer_t3510 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22533(__this, method) (( void (*) (DefaultComparer_t3510 *, const MethodInfo*))DefaultComparer__ctor_m22533_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m22534_gshared (DefaultComparer_t3510 * __this, TimeSpan_t1337  ___x, TimeSpan_t1337  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m22534(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3510 *, TimeSpan_t1337 , TimeSpan_t1337 , const MethodInfo*))DefaultComparer_Compare_m22534_gshared)(__this, ___x, ___y, method)
