﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t1524;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Security.Cryptography.KeyBuilder
struct  KeyBuilder_t1537  : public Object_t
{
};
struct KeyBuilder_t1537_StaticFields{
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::rng
	RandomNumberGenerator_t1524 * ___rng_0;
};
