﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>
struct Enumerator_t3168;
// System.Object
struct Object_t;
// UnityEngine.UI.Toggle
struct Toggle_t312;
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_t593;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m18649(__this, ___l, method) (( void (*) (Enumerator_t3168 *, List_1_t593 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18650(__this, method) (( Object_t * (*) (Enumerator_t3168 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::Dispose()
#define Enumerator_Dispose_m18651(__this, method) (( void (*) (Enumerator_t3168 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::VerifyState()
#define Enumerator_VerifyState_m18652(__this, method) (( void (*) (Enumerator_t3168 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::MoveNext()
#define Enumerator_MoveNext_m18653(__this, method) (( bool (*) (Enumerator_t3168 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::get_Current()
#define Enumerator_get_Current_m18654(__this, method) (( Toggle_t312 * (*) (Enumerator_t3168 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
