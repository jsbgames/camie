﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t1270;
// System.Object
struct Object_t;
// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t1268;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::.ctor(System.Collections.Specialized.NameObjectCollectionBase)
extern "C" void KeysCollection__ctor_m5525 (KeysCollection_t1270 * __this, NameObjectCollectionBase_t1268 * ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeysCollection_System_Collections_ICollection_CopyTo_m5526 (KeysCollection_t1270 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeysCollection_System_Collections_ICollection_get_IsSynchronized_m5527 (KeysCollection_t1270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeysCollection_System_Collections_ICollection_get_SyncRoot_m5528 (KeysCollection_t1270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::get_Count()
extern "C" int32_t KeysCollection_get_Count_m5529 (KeysCollection_t1270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::GetEnumerator()
extern "C" Object_t * KeysCollection_GetEnumerator_m5530 (KeysCollection_t1270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
