﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>
struct Enumerator_t3034;
// System.Object
struct Object_t;
// UnityEngine.Component
struct Component_t219;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t651;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m16585(__this, ___l, method) (( void (*) (Enumerator_t3034 *, List_1_t651 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16586(__this, method) (( Object_t * (*) (Enumerator_t3034 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::Dispose()
#define Enumerator_Dispose_m16587(__this, method) (( void (*) (Enumerator_t3034 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::VerifyState()
#define Enumerator_VerifyState_m16588(__this, method) (( void (*) (Enumerator_t3034 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::MoveNext()
#define Enumerator_MoveNext_m16589(__this, method) (( bool (*) (Enumerator_t3034 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::get_Current()
#define Enumerator_get_Current_m16590(__this, method) (( Component_t219 * (*) (Enumerator_t3034 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
