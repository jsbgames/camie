﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Text.DecoderFallback
#include "mscorlib_System_Text_DecoderFallback.h"
// System.Text.DecoderReplacementFallback
struct  DecoderReplacementFallback_t2138  : public DecoderFallback_t2133
{
	// System.String System.Text.DecoderReplacementFallback::replacement
	String_t* ___replacement_3;
};
