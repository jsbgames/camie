﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.LinkStack
struct LinkStack_t1380;
// System.Object
struct Object_t;

// System.Void System.Text.RegularExpressions.LinkStack::.ctor()
extern "C" void LinkStack__ctor_m6071 (LinkStack_t1380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.LinkStack::Push()
extern "C" void LinkStack_Push_m6072 (LinkStack_t1380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.LinkStack::Pop()
extern "C" bool LinkStack_Pop_m6073 (LinkStack_t1380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.RegularExpressions.LinkStack::GetCurrent()
// System.Void System.Text.RegularExpressions.LinkStack::SetCurrent(System.Object)
