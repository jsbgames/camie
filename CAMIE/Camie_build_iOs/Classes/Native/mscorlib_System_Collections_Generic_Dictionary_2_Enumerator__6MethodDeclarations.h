﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>
struct Enumerator_t2893;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2814;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>
struct Dictionary_2_t2887;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"
#define Enumerator__ctor_m14563(__this, ___dictionary, method) (( void (*) (Enumerator_t2893 *, Dictionary_2_t2887 *, const MethodInfo*))Enumerator__ctor_m13691_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14564(__this, method) (( Object_t * (*) (Enumerator_t2893 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13692_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14565(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t2893 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13693_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14566(__this, method) (( Object_t * (*) (Enumerator_t2893 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13694_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14567(__this, method) (( Object_t * (*) (Enumerator_t2893 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13695_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::MoveNext()
#define Enumerator_MoveNext_m14568(__this, method) (( bool (*) (Enumerator_t2893 *, const MethodInfo*))Enumerator_MoveNext_m13696_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_Current()
#define Enumerator_get_Current_m14569(__this, method) (( KeyValuePair_2_t2890  (*) (Enumerator_t2893 *, const MethodInfo*))Enumerator_get_Current_m13697_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m14570(__this, method) (( Object_t * (*) (Enumerator_t2893 *, const MethodInfo*))Enumerator_get_CurrentKey_m13698_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m14571(__this, method) (( Dictionary_2_t2814 * (*) (Enumerator_t2893 *, const MethodInfo*))Enumerator_get_CurrentValue_m13699_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::VerifyState()
#define Enumerator_VerifyState_m14572(__this, method) (( void (*) (Enumerator_t2893 *, const MethodInfo*))Enumerator_VerifyState_m13700_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m14573(__this, method) (( void (*) (Enumerator_t2893 *, const MethodInfo*))Enumerator_VerifyCurrent_m13701_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.Dictionary`2<System.Object,System.Object>>::Dispose()
#define Enumerator_Dispose_m14574(__this, method) (( void (*) (Enumerator_t2893 *, const MethodInfo*))Enumerator_Dispose_m13702_gshared)(__this, method)
