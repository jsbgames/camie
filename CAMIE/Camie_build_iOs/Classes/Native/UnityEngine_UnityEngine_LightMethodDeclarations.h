﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Light
struct Light_t152;

// System.Void UnityEngine.Light::set_intensity(System.Single)
extern "C" void Light_set_intensity_m923 (Light_t152 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_shadowStrength()
extern "C" float Light_get_shadowStrength_m990 (Light_t152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_shadowStrength(System.Single)
extern "C" void Light_set_shadowStrength_m994 (Light_t152 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_shadowBias(System.Single)
extern "C" void Light_set_shadowBias_m993 (Light_t152 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
