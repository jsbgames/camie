﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern "C" void MultiKeyDictionary_3_get_Item_m14473_gshared ();
void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void MultiKeyDictionary_3__ctor_m14472_gshared ();
void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
extern "C" void MultiKeyDictionary_3_ContainsKey_m14474_gshared ();
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_ValidateEventData_TisObject_t_m3069_gshared ();
extern "C" void ExecuteEvents_Execute_TisObject_t_m3055_gshared ();
void* RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_ExecuteHierarchy_TisObject_t_m3121_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_ShouldSendToComponent_TisObject_t_m22854_gshared ();
void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_GetEventList_TisObject_t_m22850_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_CanHandleEvent_TisObject_t_m22856_gshared ();
extern "C" void ExecuteEvents_GetEventHandler_TisObject_t_m3102_gshared ();
extern "C" void EventFunction_1__ctor_m16355_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void EventFunction_1_Invoke_m16357_gshared ();
extern "C" void EventFunction_1_BeginInvoke_m16359_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void EventFunction_1_EndInvoke_m16361_gshared ();
void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetClass_TisObject_t_m3220_gshared ();
void* RuntimeInvoker_Boolean_t273_ObjectU26_t1197_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisObject_t_m3441_gshared ();
void* RuntimeInvoker_Void_t272_ObjectU26_t1197_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_Count_m17292_gshared ();
void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_IsReadOnly_m17294_gshared ();
void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_Item_m17302_gshared ();
void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_set_Item_m17304_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1__ctor_m17276_gshared ();
extern "C" void IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17278_gshared ();
void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_Add_m17280_gshared ();
extern "C" void IndexedSet_1_Remove_m17282_gshared ();
extern "C" void IndexedSet_1_GetEnumerator_m17284_gshared ();
extern "C" void IndexedSet_1_Clear_m17286_gshared ();
extern "C" void IndexedSet_1_Contains_m17288_gshared ();
extern "C" void IndexedSet_1_CopyTo_m17290_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_IndexOf_m17296_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_Insert_m17298_gshared ();
extern "C" void IndexedSet_1_RemoveAt_m17300_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_RemoveAll_m17305_gshared ();
extern "C" void IndexedSet_1_Sort_m17306_gshared ();
extern "C" void ObjectPool_1_get_countAll_m16458_gshared ();
extern "C" void ObjectPool_1_set_countAll_m16460_gshared ();
extern "C" void ObjectPool_1_get_countActive_m16462_gshared ();
extern "C" void ObjectPool_1_get_countInactive_m16464_gshared ();
extern "C" void ObjectPool_1__ctor_m16456_gshared ();
extern "C" void ObjectPool_1_Get_m16466_gshared ();
extern "C" void ObjectPool_1_Release_m16468_gshared ();
extern "C" void ScriptableObject_CreateInstance_TisObject_t_m22932_gshared ();
extern "C" void Resources_ConvertObjects_TisObject_t_m22740_gshared ();
extern "C" void Object_Instantiate_TisObject_t_m953_gshared ();
extern "C" void Object_FindObjectsOfType_TisObject_t_m951_gshared ();
extern "C" void Object_FindObjectOfType_TisObject_t_m1713_gshared ();
extern "C" void Component_GetComponent_TisObject_t_m614_gshared ();
extern "C" void Component_GetComponentInChildren_TisObject_t_m675_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m22567_gshared ();
void* RuntimeInvoker_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Component_GetComponentsInChildren_TisObject_t_m22930_gshared ();
void* RuntimeInvoker_Void_t272_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Component_GetComponentsInChildren_TisObject_t_m698_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m3455_gshared ();
extern "C" void Component_GetComponents_TisObject_t_m3053_gshared ();
extern "C" void GameObject_GetComponent_TisObject_t_m771_gshared ();
extern "C" void GameObject_GetComponents_TisObject_t_m22853_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m22568_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m22931_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m1781_gshared ();
extern "C" void GameObject_GetComponentsInParent_TisObject_t_m3172_gshared ();
extern "C" void GameObject_AddComponent_TisObject_t_m975_gshared ();
extern "C" void ResponseBase_ParseJSONList_TisObject_t_m5154_gshared ();
extern "C" void NetworkMatch_ProcessMatchResponse_TisObject_t_m5161_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ResponseDelegate_1__ctor_m20541_gshared ();
extern "C" void ResponseDelegate_1_Invoke_m20543_gshared ();
extern "C" void ResponseDelegate_1_BeginInvoke_m20545_gshared ();
extern "C" void ResponseDelegate_1_EndInvoke_m20547_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m20549_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m20550_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m20548_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m20551_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m20552_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Keys_m20692_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Values_m20698_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Item_m20700_gshared ();
extern "C" void ThreadSafeDictionary_2_set_Item_m20702_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Count_m20712_gshared ();
extern "C" void ThreadSafeDictionary_2_get_IsReadOnly_m20714_gshared ();
extern "C" void ThreadSafeDictionary_2__ctor_m20682_gshared ();
extern "C" void ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m20684_gshared ();
extern "C" void ThreadSafeDictionary_2_Get_m20686_gshared ();
extern "C" void ThreadSafeDictionary_2_AddValue_m20688_gshared ();
extern "C" void ThreadSafeDictionary_2_Add_m20690_gshared ();
extern "C" void ThreadSafeDictionary_2_Remove_m20694_gshared ();
extern "C" void ThreadSafeDictionary_2_TryGetValue_m20696_gshared ();
void* RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_Add_m20704_gshared ();
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_Clear_m20706_gshared ();
extern "C" void ThreadSafeDictionary_2_Contains_m20708_gshared ();
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_CopyTo_m20710_gshared ();
extern "C" void ThreadSafeDictionary_2_Remove_m20716_gshared ();
extern "C" void ThreadSafeDictionary_2_GetEnumerator_m20718_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2__ctor_m20675_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_Invoke_m20677_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_BeginInvoke_m20679_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_EndInvoke_m20681_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m22855_gshared ();
extern "C" void InvokableCall_1__ctor_m16794_gshared ();
extern "C" void InvokableCall_1__ctor_m16795_gshared ();
extern "C" void InvokableCall_1_Invoke_m16796_gshared ();
extern "C" void InvokableCall_1_Find_m16797_gshared ();
extern "C" void InvokableCall_2__ctor_m21505_gshared ();
extern "C" void InvokableCall_2_Invoke_m21506_gshared ();
extern "C" void InvokableCall_2_Find_m21507_gshared ();
extern "C" void InvokableCall_3__ctor_m21512_gshared ();
extern "C" void InvokableCall_3_Invoke_m21513_gshared ();
extern "C" void InvokableCall_3_Find_m21514_gshared ();
extern "C" void InvokableCall_4__ctor_m21519_gshared ();
extern "C" void InvokableCall_4_Invoke_m21520_gshared ();
extern "C" void InvokableCall_4_Find_m21521_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m21526_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1_Invoke_m21527_gshared ();
extern "C" void UnityEvent_1__ctor_m16784_gshared ();
extern "C" void UnityEvent_1_AddListener_m16786_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m16788_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m16789_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m16790_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m16792_gshared ();
extern "C" void UnityEvent_1_Invoke_m16793_gshared ();
extern "C" void UnityEvent_2__ctor_m21730_gshared ();
extern "C" void UnityEvent_2_FindMethod_Impl_m21731_gshared ();
extern "C" void UnityEvent_2_GetDelegate_m21732_gshared ();
extern "C" void UnityEvent_3__ctor_m21733_gshared ();
extern "C" void UnityEvent_3_FindMethod_Impl_m21734_gshared ();
extern "C" void UnityEvent_3_GetDelegate_m21735_gshared ();
extern "C" void UnityEvent_4__ctor_m21736_gshared ();
extern "C" void UnityEvent_4_FindMethod_Impl_m21737_gshared ();
extern "C" void UnityEvent_4_GetDelegate_m21738_gshared ();
extern "C" void UnityAction_1__ctor_m16485_gshared ();
extern "C" void UnityAction_1_Invoke_m16486_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m16487_gshared ();
extern "C" void UnityAction_1_EndInvoke_m16488_gshared ();
extern "C" void UnityAction_2__ctor_m21508_gshared ();
extern "C" void UnityAction_2_Invoke_m21509_gshared ();
extern "C" void UnityAction_2_BeginInvoke_m21510_gshared ();
extern "C" void UnityAction_2_EndInvoke_m21511_gshared ();
extern "C" void UnityAction_3__ctor_m21515_gshared ();
extern "C" void UnityAction_3_Invoke_m21516_gshared ();
extern "C" void UnityAction_3_BeginInvoke_m21517_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_3_EndInvoke_m21518_gshared ();
extern "C" void UnityAction_4__ctor_m21522_gshared ();
extern "C" void UnityAction_4_Invoke_m21523_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_4_BeginInvoke_m21524_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_4_EndInvoke_m21525_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_IsSynchronized_m16470_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_SyncRoot_m16471_gshared ();
extern "C" void Stack_1_get_Count_m16478_gshared ();
extern "C" void Stack_1__ctor_m16469_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_CopyTo_m16472_gshared ();
extern "C" void Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16473_gshared ();
extern "C" void Stack_1_System_Collections_IEnumerable_GetEnumerator_m16474_gshared ();
extern "C" void Stack_1_Peek_m16475_gshared ();
extern "C" void Stack_1_Pop_m16476_gshared ();
extern "C" void Stack_1_Push_m16477_gshared ();
extern "C" void Stack_1_GetEnumerator_m16479_gshared ();
void* RuntimeInvoker_Enumerator_t3029 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16481_gshared ();
extern "C" void Enumerator_get_Current_m16484_gshared ();
extern "C" void Enumerator__ctor_m16480_gshared ();
extern "C" void Enumerator_Dispose_m16482_gshared ();
extern "C" void Enumerator_MoveNext_m16483_gshared ();
extern "C" void Enumerable_Where_TisObject_t_m3413_gshared ();
extern "C" void Enumerable_CreateWhereIterator_TisObject_t_m22929_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m18671_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m18672_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m18670_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m18673_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m18674_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m18675_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m18676_gshared ();
extern "C" void Func_2__ctor_m22138_gshared ();
extern "C" void Func_2_Invoke_m22139_gshared ();
extern "C" void Func_2_BeginInvoke_m22140_gshared ();
extern "C" void Func_2_EndInvoke_m22141_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m22555_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisObject_t_m22547_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisObject_t_m22550_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisObject_t_m22548_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisObject_t_m22549_gshared ();
extern "C" void Array_InternalArray__Insert_TisObject_t_m22552_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisObject_t_m22551_gshared ();
extern "C" void Array_InternalArray__get_Item_TisObject_t_m22546_gshared ();
extern "C" void Array_InternalArray__set_Item_TisObject_t_m22554_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_get_swapper_TisObject_t_m22645_gshared ();
extern "C" void Array_Sort_TisObject_t_m23200_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m23201_gshared ();
extern "C" void Array_Sort_TisObject_t_m23202_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m23203_gshared ();
extern "C" void Array_Sort_TisObject_t_m12634_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_TisObject_t_m23204_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_m22644_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_TisObject_t_m22643_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_m23205_gshared ();
extern "C" void Array_Sort_TisObject_t_m22672_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_qsort_TisObject_t_TisObject_t_m22646_gshared ();
extern "C" void Array_compare_TisObject_t_m22669_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_qsort_TisObject_t_m22671_gshared ();
extern "C" void Array_swap_TisObject_t_TisObject_t_m22670_gshared ();
extern "C" void Array_swap_TisObject_t_m22673_gshared ();
extern "C" void Array_Resize_TisObject_t_m22642_gshared ();
void* RuntimeInvoker_Void_t272_ObjectU5BU5DU26_t2398_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisObject_t_m22641_gshared ();
void* RuntimeInvoker_Void_t272_ObjectU5BU5DU26_t2398_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_TrueForAll_TisObject_t_m23206_gshared ();
extern "C" void Array_ForEach_TisObject_t_m23207_gshared ();
extern "C" void Array_ConvertAll_TisObject_t_TisObject_t_m23208_gshared ();
extern "C" void Array_FindLastIndex_TisObject_t_m23210_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindLastIndex_TisObject_t_m23211_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindLastIndex_TisObject_t_m23209_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindIndex_TisObject_t_m23213_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m23214_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m23212_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m23216_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m23217_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m23218_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m23215_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisObject_t_m12636_gshared ();
extern "C" void Array_IndexOf_TisObject_t_m23219_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisObject_t_m12633_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_LastIndexOf_TisObject_t_m23221_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m23220_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m23222_gshared ();
extern "C" void Array_FindAll_TisObject_t_m23223_gshared ();
extern "C" void Array_Exists_TisObject_t_m23224_gshared ();
extern "C" void Array_AsReadOnly_TisObject_t_m23225_gshared ();
extern "C" void Array_Find_TisObject_t_m23226_gshared ();
extern "C" void Array_FindLast_TisObject_t_m23227_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13521_gshared ();
extern "C" void InternalEnumerator_1__ctor_m13513_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13517_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13519_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Item_m22183_gshared ();
extern "C" void ArrayReadOnlyList_1_set_Item_m22184_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Count_m22185_gshared ();
extern "C" void ArrayReadOnlyList_1_get_IsReadOnly_m22186_gshared ();
extern "C" void ArrayReadOnlyList_1__ctor_m22181_gshared ();
extern "C" void ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m22182_gshared ();
extern "C" void ArrayReadOnlyList_1_Add_m22187_gshared ();
extern "C" void ArrayReadOnlyList_1_Clear_m22188_gshared ();
extern "C" void ArrayReadOnlyList_1_Contains_m22189_gshared ();
extern "C" void ArrayReadOnlyList_1_CopyTo_m22190_gshared ();
extern "C" void ArrayReadOnlyList_1_GetEnumerator_m22191_gshared ();
extern "C" void ArrayReadOnlyList_1_IndexOf_m22192_gshared ();
extern "C" void ArrayReadOnlyList_1_Insert_m22193_gshared ();
extern "C" void ArrayReadOnlyList_1_Remove_m22194_gshared ();
extern "C" void ArrayReadOnlyList_1_RemoveAt_m22195_gshared ();
extern "C" void ArrayReadOnlyList_1_ReadOnlyError_m22196_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m22198_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m22199_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m22197_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m22200_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m22201_gshared ();
extern "C" void Comparer_1_get_Default_m14062_gshared ();
extern "C" void Comparer_1__ctor_m14059_gshared ();
extern "C" void Comparer_1__cctor_m14060_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m14061_gshared ();
extern "C" void DefaultComparer__ctor_m14063_gshared ();
extern "C" void DefaultComparer_Compare_m14064_gshared ();
extern "C" void GenericComparer_1__ctor_m22232_gshared ();
extern "C" void GenericComparer_1_Compare_m22233_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13559_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13561_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m13563_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m13565_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13573_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13575_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13577_gshared ();
extern "C" void Dictionary_2_get_Count_m13595_gshared ();
extern "C" void Dictionary_2_get_Item_m13597_gshared ();
extern "C" void Dictionary_2_set_Item_m13599_gshared ();
extern "C" void Dictionary_2_get_Keys_m13633_gshared ();
extern "C" void Dictionary_2_get_Values_m13635_gshared ();
extern "C" void Dictionary_2__ctor_m13547_gshared ();
extern "C" void Dictionary_2__ctor_m13549_gshared ();
extern "C" void Dictionary_2__ctor_m13551_gshared ();
extern "C" void Dictionary_2__ctor_m13553_gshared ();
extern "C" void Dictionary_2__ctor_m13555_gshared ();
extern "C" void Dictionary_2__ctor_m13557_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m13567_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m13569_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m13571_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13579_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13581_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13583_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13585_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m13587_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13589_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13591_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13593_gshared ();
extern "C" void Dictionary_2_Init_m13601_gshared ();
extern "C" void Dictionary_2_InitArrays_m13603_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m13605_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m22613_gshared ();
extern "C" void Dictionary_2_make_pair_m13607_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2815_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m13609_gshared ();
extern "C" void Dictionary_2_pick_value_m13611_gshared ();
extern "C" void Dictionary_2_CopyTo_m13613_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m22614_gshared ();
extern "C" void Dictionary_2_Resize_m13615_gshared ();
extern "C" void Dictionary_2_Add_m13617_gshared ();
extern "C" void Dictionary_2_Clear_m13619_gshared ();
extern "C" void Dictionary_2_ContainsKey_m13621_gshared ();
extern "C" void Dictionary_2_ContainsValue_m13623_gshared ();
extern "C" void Dictionary_2_GetObjectData_m13625_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m13627_gshared ();
extern "C" void Dictionary_2_Remove_m13629_gshared ();
extern "C" void Dictionary_2_TryGetValue_m13631_gshared ();
extern "C" void Dictionary_2_ToTKey_m13637_gshared ();
extern "C" void Dictionary_2_ToTValue_m13639_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m13641_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m13643_gshared ();
void* RuntimeInvoker_Enumerator_t2822 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m13645_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator_get_Entry_m13741_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430 (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator_get_Key_m13742_gshared ();
extern "C" void ShimEnumerator_get_Value_m13743_gshared ();
extern "C" void ShimEnumerator_get_Current_m13744_gshared ();
extern "C" void ShimEnumerator__ctor_m13739_gshared ();
extern "C" void ShimEnumerator_MoveNext_m13740_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m13692_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13693_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13694_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13695_gshared ();
extern "C" void Enumerator_get_Current_m13697_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_CurrentKey_m13698_gshared ();
extern "C" void Enumerator_get_CurrentValue_m13699_gshared ();
extern "C" void Enumerator__ctor_m13691_gshared ();
extern "C" void Enumerator_MoveNext_m13696_gshared ();
extern "C" void Enumerator_VerifyState_m13700_gshared ();
extern "C" void Enumerator_VerifyCurrent_m13701_gshared ();
extern "C" void Enumerator_Dispose_m13702_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m13680_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m13681_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m13682_gshared ();
extern "C" void KeyCollection_get_Count_m13685_gshared ();
extern "C" void KeyCollection__ctor_m13672_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m13673_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m13674_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m13675_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m13676_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m13677_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m13678_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m13679_gshared ();
extern "C" void KeyCollection_CopyTo_m13683_gshared ();
extern "C" void KeyCollection_GetEnumerator_m13684_gshared ();
void* RuntimeInvoker_Enumerator_t2821 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m13687_gshared ();
extern "C" void Enumerator_get_Current_m13690_gshared ();
extern "C" void Enumerator__ctor_m13686_gshared ();
extern "C" void Enumerator_Dispose_m13688_gshared ();
extern "C" void Enumerator_MoveNext_m13689_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m13715_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m13716_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m13717_gshared ();
extern "C" void ValueCollection_get_Count_m13720_gshared ();
extern "C" void ValueCollection__ctor_m13707_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m13708_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m13709_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m13710_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m13711_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m13712_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m13713_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m13714_gshared ();
extern "C" void ValueCollection_CopyTo_m13718_gshared ();
extern "C" void ValueCollection_GetEnumerator_m13719_gshared ();
void* RuntimeInvoker_Enumerator_t2825 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m13722_gshared ();
extern "C" void Enumerator_get_Current_m13725_gshared ();
extern "C" void Enumerator__ctor_m13721_gshared ();
extern "C" void Enumerator_Dispose_m13723_gshared ();
extern "C" void Enumerator_MoveNext_m13724_gshared ();
extern "C" void Transform_1__ctor_m13703_gshared ();
extern "C" void Transform_1_Invoke_m13704_gshared ();
extern "C" void Transform_1_BeginInvoke_m13705_gshared ();
extern "C" void Transform_1_EndInvoke_m13706_gshared ();
extern "C" void EqualityComparer_1_get_Default_m13749_gshared ();
extern "C" void EqualityComparer_1__ctor_m13745_gshared ();
extern "C" void EqualityComparer_1__cctor_m13746_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m13747_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m13748_gshared ();
extern "C" void DefaultComparer__ctor_m13755_gshared ();
extern "C" void DefaultComparer_GetHashCode_m13756_gshared ();
extern "C" void DefaultComparer_Equals_m13757_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m22234_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m22235_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m22236_gshared ();
extern "C" void KeyValuePair_2_get_Key_m13652_gshared ();
extern "C" void KeyValuePair_2_set_Key_m13653_gshared ();
extern "C" void KeyValuePair_2_get_Value_m13654_gshared ();
extern "C" void KeyValuePair_2_set_Value_m13655_gshared ();
extern "C" void KeyValuePair_2__ctor_m13651_gshared ();
extern "C" void KeyValuePair_2_ToString_m13656_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m5411_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m5412_gshared ();
extern "C" void List_1_get_Capacity_m13977_gshared ();
extern "C" void List_1_set_Capacity_m13979_gshared ();
extern "C" void List_1_get_Count_m5405_gshared ();
extern "C" void List_1_get_Item_m5428_gshared ();
extern "C" void List_1_set_Item_m5429_gshared ();
extern "C" void List_1__ctor_m5170_gshared ();
extern "C" void List_1__ctor_m13913_gshared ();
extern "C" void List_1__ctor_m13915_gshared ();
extern "C" void List_1__cctor_m13917_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m5408_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m5413_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m5415_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m5416_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m5417_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m5418_gshared ();
extern "C" void List_1_Add_m5421_gshared ();
extern "C" void List_1_GrowIfNeeded_m13935_gshared ();
extern "C" void List_1_AddCollection_m13937_gshared ();
extern "C" void List_1_AddEnumerable_m13939_gshared ();
extern "C" void List_1_AddRange_m13941_gshared ();
extern "C" void List_1_AsReadOnly_m13943_gshared ();
extern "C" void List_1_Clear_m5414_gshared ();
extern "C" void List_1_Contains_m5422_gshared ();
extern "C" void List_1_CopyTo_m5423_gshared ();
extern "C" void List_1_Find_m13948_gshared ();
extern "C" void List_1_CheckMatch_m13950_gshared ();
extern "C" void List_1_GetIndex_m13952_gshared ();
void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_GetEnumerator_m13954_gshared ();
void* RuntimeInvoker_Enumerator_t2843 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m5426_gshared ();
extern "C" void List_1_Shift_m13957_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckIndex_m13959_gshared ();
extern "C" void List_1_Insert_m5427_gshared ();
extern "C" void List_1_CheckCollection_m13962_gshared ();
extern "C" void List_1_Remove_m5424_gshared ();
extern "C" void List_1_RemoveAll_m13965_gshared ();
extern "C" void List_1_RemoveAt_m5419_gshared ();
extern "C" void List_1_Reverse_m13968_gshared ();
extern "C" void List_1_Sort_m13970_gshared ();
extern "C" void List_1_Sort_m13972_gshared ();
extern "C" void List_1_ToArray_m13973_gshared ();
extern "C" void List_1_TrimExcess_m13975_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared ();
extern "C" void Enumerator_get_Current_m13988_gshared ();
extern "C" void Enumerator__ctor_m13983_gshared ();
extern "C" void Enumerator_Dispose_m13985_gshared ();
extern "C" void Enumerator_VerifyState_m13986_gshared ();
extern "C" void Enumerator_MoveNext_m13987_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14020_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m14028_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m14029_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m14030_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m14031_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m14032_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m14033_gshared ();
extern "C" void Collection_1_get_Count_m14046_gshared ();
extern "C" void Collection_1_get_Item_m14047_gshared ();
extern "C" void Collection_1_set_Item_m14048_gshared ();
extern "C" void Collection_1__ctor_m14019_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m14021_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m14022_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m14023_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m14024_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m14025_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m14026_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m14027_gshared ();
extern "C" void Collection_1_Add_m14034_gshared ();
extern "C" void Collection_1_Clear_m14035_gshared ();
extern "C" void Collection_1_ClearItems_m14036_gshared ();
extern "C" void Collection_1_Contains_m14037_gshared ();
extern "C" void Collection_1_CopyTo_m14038_gshared ();
extern "C" void Collection_1_GetEnumerator_m14039_gshared ();
extern "C" void Collection_1_IndexOf_m14040_gshared ();
extern "C" void Collection_1_Insert_m14041_gshared ();
extern "C" void Collection_1_InsertItem_m14042_gshared ();
extern "C" void Collection_1_Remove_m14043_gshared ();
extern "C" void Collection_1_RemoveAt_m14044_gshared ();
extern "C" void Collection_1_RemoveItem_m14045_gshared ();
extern "C" void Collection_1_SetItem_m14049_gshared ();
extern "C" void Collection_1_IsValidItem_m14050_gshared ();
extern "C" void Collection_1_ConvertItem_m14051_gshared ();
extern "C" void Collection_1_CheckWritable_m14052_gshared ();
extern "C" void Collection_1_IsSynchronized_m14053_gshared ();
extern "C" void Collection_1_IsFixedSize_m14054_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m13995_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m13996_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13997_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14007_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14008_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14009_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14010_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m14011_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m14012_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m14017_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m14018_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m13989_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m13990_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m13991_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m13992_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m13993_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m13994_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m13998_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m13999_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m14000_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m14001_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m14002_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14003_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m14004_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m14005_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14006_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m14013_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m14014_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m14015_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m14016_gshared ();
extern "C" void MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m23305_gshared ();
extern "C" void MonoProperty_StaticGetterAdapterFrame_TisObject_t_m23306_gshared ();
extern "C" void Getter_2__ctor_m22327_gshared ();
extern "C" void Getter_2_Invoke_m22328_gshared ();
extern "C" void Getter_2_BeginInvoke_m22329_gshared ();
extern "C" void Getter_2_EndInvoke_m22330_gshared ();
extern "C" void StaticGetter_1__ctor_m22331_gshared ();
extern "C" void StaticGetter_1_Invoke_m22332_gshared ();
extern "C" void StaticGetter_1_BeginInvoke_m22333_gshared ();
extern "C" void StaticGetter_1_EndInvoke_m22334_gshared ();
extern "C" void Activator_CreateInstance_TisObject_t_m22851_gshared ();
extern "C" void Action_1__ctor_m17605_gshared ();
extern "C" void Action_1_Invoke_m17607_gshared ();
extern "C" void Action_1_BeginInvoke_m17609_gshared ();
extern "C" void Action_1_EndInvoke_m17611_gshared ();
extern "C" void Comparison_1__ctor_m14080_gshared ();
extern "C" void Comparison_1_Invoke_m14081_gshared ();
extern "C" void Comparison_1_BeginInvoke_m14082_gshared ();
extern "C" void Comparison_1_EndInvoke_m14083_gshared ();
extern "C" void Converter_2__ctor_m22177_gshared ();
extern "C" void Converter_2_Invoke_m22178_gshared ();
extern "C" void Converter_2_BeginInvoke_m22179_gshared ();
extern "C" void Converter_2_EndInvoke_m22180_gshared ();
extern "C" void Predicate_1__ctor_m14055_gshared ();
extern "C" void Predicate_1_Invoke_m14056_gshared ();
extern "C" void Predicate_1_BeginInvoke_m14057_gshared ();
extern "C" void Predicate_1_EndInvoke_m14058_gshared ();
extern "C" void List_1__ctor_m1597_gshared ();
extern "C" void List_1__ctor_m1733_gshared ();
extern "C" void Dictionary_2__ctor_m15470_gshared ();
extern "C" void Dictionary_2__ctor_m15814_gshared ();
extern "C" void Comparison_1__ctor_m3058_gshared ();
extern "C" void List_1_Sort_m3061_gshared ();
extern "C" void Dictionary_2__ctor_m5219_gshared ();
extern "C" void Dictionary_2_get_Values_m17026_gshared ();
extern "C" void ValueCollection_GetEnumerator_m17094_gshared ();
void* RuntimeInvoker_Enumerator_t3066 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m17100_gshared ();
extern "C" void Enumerator_MoveNext_m17099_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m17033_gshared ();
void* RuntimeInvoker_Enumerator_t3063 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m17072_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3059 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_get_Value_m17044_gshared ();
extern "C" void KeyValuePair_2_get_Key_m17042_gshared ();
extern "C" void Enumerator_MoveNext_m17071_gshared ();
extern "C" void KeyValuePair_2_ToString_m17046_gshared ();
extern "C" void Comparison_1__ctor_m3148_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t24_m3145_gshared ();
extern "C" void UnityEvent_1__ctor_m3149_gshared ();
extern "C" void UnityEvent_1_Invoke_m3151_gshared ();
void* RuntimeInvoker_Void_t272_Color_t65 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_AddListener_m3152_gshared ();
extern "C" void TweenRunner_1__ctor_m3174_gshared ();
extern "C" void TweenRunner_1_Init_m3175_gshared ();
extern "C" void UnityAction_1__ctor_m3197_gshared ();
extern "C" void TweenRunner_1_StartTween_m3198_gshared ();
void* RuntimeInvoker_Void_t272_ColorTween_t506 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_get_Capacity_m3201_gshared ();
extern "C" void List_1_set_Capacity_m3202_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisInt32_t253_m3222_gshared ();
void* RuntimeInvoker_Boolean_t273_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisByte_t680_m3224_gshared ();
void* RuntimeInvoker_Boolean_t273_ByteU26_t1656_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisSingle_t254_m3226_gshared ();
void* RuntimeInvoker_Boolean_t273_SingleU26_t1150_Single_t254 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m3276_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisUInt16_t684_m3272_gshared ();
void* RuntimeInvoker_Boolean_t273_UInt16U26_t2421_Int16_t752 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_ToArray_m3323_gshared ();
extern "C" void UnityEvent_1__ctor_m3346_gshared ();
extern "C" void UnityEvent_1_Invoke_m3351_gshared ();
void* RuntimeInvoker_Void_t272_Single_t254 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1__ctor_m3356_gshared ();
extern "C" void UnityAction_1__ctor_m3357_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m3358_gshared ();
extern "C" void UnityEvent_1_AddListener_m3359_gshared ();
extern "C" void UnityEvent_1_Invoke_m3364_gshared ();
void* RuntimeInvoker_Void_t272_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisNavigation_t563_m3378_gshared ();
void* RuntimeInvoker_Boolean_t273_NavigationU26_t3954_Navigation_t563 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisColorBlock_t516_m3380_gshared ();
void* RuntimeInvoker_Boolean_t273_ColorBlockU26_t3955_ColorBlock_t516 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisSpriteState_t580_m3381_gshared ();
void* RuntimeInvoker_Boolean_t273_SpriteStateU26_t3956_SpriteState_t580 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1__ctor_m18546_gshared ();
extern "C" void UnityEvent_1_Invoke_m18555_gshared ();
void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2__ctor_m18659_gshared ();
extern "C" void LayoutGroup_SetProperty_TisInt32_t253_m3428_gshared ();
void* RuntimeInvoker_Void_t272_Int32U26_t753_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisVector2_t6_m3430_gshared ();
void* RuntimeInvoker_Void_t272_Vector2U26_t1148_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisSingle_t254_m3435_gshared ();
void* RuntimeInvoker_Void_t272_SingleU26_t1150_Single_t254 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisByte_t680_m3437_gshared ();
void* RuntimeInvoker_Void_t272_ByteU26_t1656_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2__ctor_m18773_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m3599_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m3600_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m3607_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m3608_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m3609_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m3610_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m18551_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m18552_gshared ();
extern "C" void List_1__ctor_m5144_gshared ();
extern "C" void List_1__ctor_m5145_gshared ();
extern "C" void List_1__ctor_m5146_gshared ();
extern "C" void Dictionary_2__ctor_m20292_gshared ();
extern "C" void Dictionary_2__ctor_m21026_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m5240_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Single_t254 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1__ctor_m5241_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1__ctor_m21543_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m21741_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t253_m6510_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericComparer_1__ctor_m12638_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m12639_gshared ();
extern "C" void GenericComparer_1__ctor_m12640_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m12641_gshared ();
extern "C" void Nullable_1__ctor_m12642_gshared ();
void* RuntimeInvoker_Void_t272_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_get_HasValue_m12643_gshared ();
extern "C" void Nullable_1_get_Value_m12644_gshared ();
void* RuntimeInvoker_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericComparer_1__ctor_m12645_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m12646_gshared ();
extern "C" void GenericComparer_1__ctor_m12647_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m12648_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit_t24_m22557_gshared ();
void* RuntimeInvoker_RaycastHit_t24_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit_t24_m22558_gshared ();
void* RuntimeInvoker_Void_t272_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit_t24_m22559_gshared ();
void* RuntimeInvoker_Boolean_t273_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t24_m22560_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit_t24_m22561_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit_t24_m22562_gshared ();
void* RuntimeInvoker_Int32_t253_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastHit_t24_m22563_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastHit_t24_m22565_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t24_m22566_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSingle_t254_m22570_gshared ();
void* RuntimeInvoker_Single_t254_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSingle_t254_m22571_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSingle_t254_m22572_gshared ();
void* RuntimeInvoker_Boolean_t273_Single_t254 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSingle_t254_m22573_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSingle_t254_m22574_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSingle_t254_m22575_gshared ();
void* RuntimeInvoker_Int32_t253_Single_t254 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSingle_t254_m22576_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Single_t254 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSingle_t254_m22578_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t254_m22579_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2815_m22581_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2815_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2815_m22582_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2815_m22583_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2815_m22584_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2815_m22585_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2815_m22586_gshared ();
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2815_m22587_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2815_m22589_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2815_m22590_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt32_t253_m22592_gshared ();
void* RuntimeInvoker_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt32_t253_m22593_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt32_t253_m22594_gshared ();
void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt32_t253_m22595_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt32_t253_m22596_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt32_t253_m22597_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt32_t253_m22598_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt32_t253_m22600_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t253_m22601_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLink_t1757_m22603_gshared ();
void* RuntimeInvoker_Link_t1757_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisLink_t1757_m22604_gshared ();
void* RuntimeInvoker_Void_t272_Link_t1757 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisLink_t1757_m22605_gshared ();
void* RuntimeInvoker_Boolean_t273_Link_t1757 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLink_t1757_m22606_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLink_t1757_m22607_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLink_t1757_m22608_gshared ();
void* RuntimeInvoker_Int32_t253_Link_t1757 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisLink_t1757_m22609_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Link_t1757 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisLink_t1757_m22611_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t1757_m22612_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDictionaryEntry_t1430_m22616_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDictionaryEntry_t1430_m22617_gshared ();
void* RuntimeInvoker_Void_t272_DictionaryEntry_t1430 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t1430_m22618_gshared ();
void* RuntimeInvoker_Boolean_t273_DictionaryEntry_t1430 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t1430_m22619_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t1430_m22620_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDictionaryEntry_t1430_m22621_gshared ();
void* RuntimeInvoker_Int32_t253_DictionaryEntry_t1430 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDictionaryEntry_t1430_m22622_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_DictionaryEntry_t1430 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDictionaryEntry_t1430_m22624_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t1430_m22625_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m22626_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2815_m22628_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2815_TisObject_t_m22627_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2815_TisKeyValuePair_2_t2815_m22629_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTouch_t235_m22631_gshared ();
void* RuntimeInvoker_Touch_t235_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTouch_t235_m22632_gshared ();
void* RuntimeInvoker_Void_t272_Touch_t235 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisTouch_t235_m22633_gshared ();
void* RuntimeInvoker_Boolean_t273_Touch_t235 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTouch_t235_m22634_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTouch_t235_m22635_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTouch_t235_m22636_gshared ();
void* RuntimeInvoker_Int32_t253_Touch_t235 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTouch_t235_m22637_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Touch_t235 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTouch_t235_m22639_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTouch_t235_m22640_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDouble_t1070_m22648_gshared ();
void* RuntimeInvoker_Double_t1070_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDouble_t1070_m22649_gshared ();
void* RuntimeInvoker_Void_t272_Double_t1070 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDouble_t1070_m22650_gshared ();
void* RuntimeInvoker_Boolean_t273_Double_t1070 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDouble_t1070_m22651_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDouble_t1070_m22652_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDouble_t1070_m22653_gshared ();
void* RuntimeInvoker_Int32_t253_Double_t1070 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDouble_t1070_m22654_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Double_t1070 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDouble_t1070_m22656_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t1070_m22657_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt16_t684_m22659_gshared ();
void* RuntimeInvoker_UInt16_t684_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt16_t684_m22660_gshared ();
void* RuntimeInvoker_Void_t272_Int16_t752 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt16_t684_m22661_gshared ();
void* RuntimeInvoker_Boolean_t273_Int16_t752 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt16_t684_m22662_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt16_t684_m22663_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt16_t684_m22664_gshared ();
void* RuntimeInvoker_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUInt16_t684_m22665_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Int16_t752 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUInt16_t684_m22667_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t684_m22668_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector2_t6_m22675_gshared ();
void* RuntimeInvoker_Vector2_t6_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVector2_t6_m22676_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisVector2_t6_m22677_gshared ();
void* RuntimeInvoker_Boolean_t273_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector2_t6_m22678_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector2_t6_m22679_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector2_t6_m22680_gshared ();
void* RuntimeInvoker_Int32_t253_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVector2_t6_m22681_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVector2_t6_m22683_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t6_m22684_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyframe_t240_m22686_gshared ();
void* RuntimeInvoker_Keyframe_t240_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyframe_t240_m22687_gshared ();
void* RuntimeInvoker_Void_t272_Keyframe_t240 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyframe_t240_m22688_gshared ();
void* RuntimeInvoker_Boolean_t273_Keyframe_t240 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyframe_t240_m22689_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyframe_t240_m22690_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyframe_t240_m22691_gshared ();
void* RuntimeInvoker_Int32_t253_Keyframe_t240 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyframe_t240_m22692_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Keyframe_t240 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyframe_t240_m22694_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t240_m22695_gshared ();
extern "C" void Array_InternalArray__get_Item_TisColor_t65_m22697_gshared ();
void* RuntimeInvoker_Color_t65_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisColor_t65_m22698_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisColor_t65_m22699_gshared ();
void* RuntimeInvoker_Boolean_t273_Color_t65 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisColor_t65_m22700_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisColor_t65_m22701_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisColor_t65_m22702_gshared ();
void* RuntimeInvoker_Int32_t253_Color_t65 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisColor_t65_m22703_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Color_t65 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisColor_t65_m22705_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisColor_t65_m22706_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector3_t4_m22708_gshared ();
void* RuntimeInvoker_Vector3_t4_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVector3_t4_m22709_gshared ();
void* RuntimeInvoker_Void_t272_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisVector3_t4_m22710_gshared ();
void* RuntimeInvoker_Boolean_t273_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector3_t4_m22711_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector3_t4_m22712_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector3_t4_m22713_gshared ();
void* RuntimeInvoker_Int32_t253_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVector3_t4_m22714_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVector3_t4_m22716_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t4_m22717_gshared ();
extern "C" void Array_InternalArray__get_Item_TisContactPoint_t246_m22719_gshared ();
void* RuntimeInvoker_ContactPoint_t246_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisContactPoint_t246_m22720_gshared ();
void* RuntimeInvoker_Void_t272_ContactPoint_t246 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisContactPoint_t246_m22721_gshared ();
void* RuntimeInvoker_Boolean_t273_ContactPoint_t246 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisContactPoint_t246_m22722_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisContactPoint_t246_m22723_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisContactPoint_t246_m22724_gshared ();
void* RuntimeInvoker_Int32_t253_ContactPoint_t246 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisContactPoint_t246_m22725_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_ContactPoint_t246 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisContactPoint_t246_m22727_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisContactPoint_t246_m22728_gshared ();
extern "C" void Array_InternalArray__get_Item_TisParticleCollisionEvent_t160_m22730_gshared ();
void* RuntimeInvoker_ParticleCollisionEvent_t160_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisParticleCollisionEvent_t160_m22731_gshared ();
void* RuntimeInvoker_Void_t272_ParticleCollisionEvent_t160 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisParticleCollisionEvent_t160_m22732_gshared ();
void* RuntimeInvoker_Boolean_t273_ParticleCollisionEvent_t160 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisParticleCollisionEvent_t160_m22733_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisParticleCollisionEvent_t160_m22734_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisParticleCollisionEvent_t160_m22735_gshared ();
void* RuntimeInvoker_Int32_t253_ParticleCollisionEvent_t160 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisParticleCollisionEvent_t160_m22736_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_ParticleCollisionEvent_t160 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisParticleCollisionEvent_t160_m22738_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisParticleCollisionEvent_t160_m22739_gshared ();
extern "C" void Array_Resize_TisVector2_t6_m22742_gshared ();
void* RuntimeInvoker_Void_t272_Vector2U5BU5DU26_t3957_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisVector2_t6_m22741_gshared ();
void* RuntimeInvoker_Void_t272_Vector2U5BU5DU26_t3957_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisVector2_t6_m22743_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_Vector2_t6_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisVector2_t6_m22745_gshared ();
extern "C" void Array_Sort_TisVector2_t6_TisVector2_t6_m22744_gshared ();
extern "C" void Array_get_swapper_TisVector2_t6_m22746_gshared ();
extern "C" void Array_qsort_TisVector2_t6_TisVector2_t6_m22747_gshared ();
extern "C" void Array_compare_TisVector2_t6_m22748_gshared ();
void* RuntimeInvoker_Int32_t253_Vector2_t6_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisVector2_t6_TisVector2_t6_m22749_gshared ();
extern "C" void Array_Sort_TisVector2_t6_m22751_gshared ();
extern "C" void Array_qsort_TisVector2_t6_m22750_gshared ();
extern "C" void Array_swap_TisVector2_t6_m22752_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastResult_t486_m22754_gshared ();
void* RuntimeInvoker_RaycastResult_t486_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastResult_t486_m22755_gshared ();
void* RuntimeInvoker_Void_t272_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastResult_t486_m22756_gshared ();
void* RuntimeInvoker_Boolean_t273_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t486_m22757_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastResult_t486_m22758_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastResult_t486_m22759_gshared ();
void* RuntimeInvoker_Int32_t253_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastResult_t486_m22760_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastResult_t486_m22762_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t486_m22763_gshared ();
extern "C" void Array_Resize_TisRaycastResult_t486_m22765_gshared ();
void* RuntimeInvoker_Void_t272_RaycastResultU5BU5DU26_t3958_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisRaycastResult_t486_m22764_gshared ();
void* RuntimeInvoker_Void_t272_RaycastResultU5BU5DU26_t3958_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisRaycastResult_t486_m22766_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_RaycastResult_t486_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisRaycastResult_t486_m22768_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t486_TisRaycastResult_t486_m22767_gshared ();
extern "C" void Array_get_swapper_TisRaycastResult_t486_m22769_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t486_TisRaycastResult_t486_m22770_gshared ();
extern "C" void Array_compare_TisRaycastResult_t486_m22771_gshared ();
void* RuntimeInvoker_Int32_t253_RaycastResult_t486_RaycastResult_t486_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisRaycastResult_t486_TisRaycastResult_t486_m22772_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t486_m22774_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t486_m22773_gshared ();
extern "C" void Array_swap_TisRaycastResult_t486_m22775_gshared ();
extern "C" void Array_InternalArray__get_Item_TisQuaternion_t19_m22777_gshared ();
void* RuntimeInvoker_Quaternion_t19_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisQuaternion_t19_m22778_gshared ();
void* RuntimeInvoker_Void_t272_Quaternion_t19 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisQuaternion_t19_m22779_gshared ();
void* RuntimeInvoker_Boolean_t273_Quaternion_t19 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisQuaternion_t19_m22780_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisQuaternion_t19_m22781_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisQuaternion_t19_m22782_gshared ();
void* RuntimeInvoker_Int32_t253_Quaternion_t19 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisQuaternion_t19_m22783_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Quaternion_t19 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisQuaternion_t19_m22785_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisQuaternion_t19_m22786_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2953_m22788_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2953_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2953_m22789_gshared ();
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t2953 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2953_m22790_gshared ();
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2953 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2953_m22791_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2953_m22792_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2953_m22793_gshared ();
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t2953 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2953_m22794_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t2953 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2953_m22796_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2953_m22797_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m22799_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m22798_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t253_m22801_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t253_TisObject_t_m22800_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t253_TisInt32_t253_m22802_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m22803_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2953_m22805_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2953_TisObject_t_m22804_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2953_TisKeyValuePair_2_t2953_m22806_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2984_m22808_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2984_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2984_m22809_gshared ();
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t2984 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2984_m22810_gshared ();
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2984 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2984_m22811_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2984_m22812_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2984_m22813_gshared ();
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t2984 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2984_m22814_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t2984 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2984_m22816_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2984_m22817_gshared ();
extern "C" void Array_InternalArray__get_Item_TisByte_t680_m22819_gshared ();
void* RuntimeInvoker_Byte_t680_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisByte_t680_m22820_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisByte_t680_m22821_gshared ();
void* RuntimeInvoker_Boolean_t273_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisByte_t680_m22822_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisByte_t680_m22823_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisByte_t680_m22824_gshared ();
void* RuntimeInvoker_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisByte_t680_m22825_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisByte_t680_m22827_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t680_m22828_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t253_m22830_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t253_TisObject_t_m22829_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t253_TisInt32_t253_m22831_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisByte_t680_m22833_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisByte_t680_TisObject_t_m22832_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisByte_t680_TisByte_t680_m22834_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m22835_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2984_m22837_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2984_TisObject_t_m22836_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2984_TisKeyValuePair_2_t2984_m22838_gshared ();
extern "C" void Array_InternalArray__get_Item_TisAnimatorClipInfo_t888_m22840_gshared ();
void* RuntimeInvoker_AnimatorClipInfo_t888_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisAnimatorClipInfo_t888_m22841_gshared ();
void* RuntimeInvoker_Void_t272_AnimatorClipInfo_t888 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisAnimatorClipInfo_t888_m22842_gshared ();
void* RuntimeInvoker_Boolean_t273_AnimatorClipInfo_t888 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisAnimatorClipInfo_t888_m22843_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisAnimatorClipInfo_t888_m22844_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisAnimatorClipInfo_t888_m22845_gshared ();
void* RuntimeInvoker_Int32_t253_AnimatorClipInfo_t888 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisAnimatorClipInfo_t888_m22846_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_AnimatorClipInfo_t888 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisAnimatorClipInfo_t888_m22848_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisAnimatorClipInfo_t888_m22849_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3059_m22858_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3059_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3059_m22859_gshared ();
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3059 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3059_m22860_gshared ();
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3059 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3059_m22861_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3059_m22862_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3059_m22863_gshared ();
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3059 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3059_m22864_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3059 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3059_m22866_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3059_m22867_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t253_m22869_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t253_TisObject_t_m22868_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t253_TisInt32_t253_m22870_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m22872_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m22871_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m22873_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3059_m22875_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3059_TisObject_t_m22874_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3059_TisKeyValuePair_2_t3059_m22876_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit2D_t665_m22878_gshared ();
void* RuntimeInvoker_RaycastHit2D_t665_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit2D_t665_m22879_gshared ();
void* RuntimeInvoker_Void_t272_RaycastHit2D_t665 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t665_m22880_gshared ();
void* RuntimeInvoker_Boolean_t273_RaycastHit2D_t665 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t665_m22881_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t665_m22882_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit2D_t665_m22883_gshared ();
void* RuntimeInvoker_Int32_t253_RaycastHit2D_t665 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastHit2D_t665_m22884_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_RaycastHit2D_t665 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastHit2D_t665_m22886_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t665_m22887_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t24_m22888_gshared ();
extern "C" void Array_qsort_TisRaycastHit_t24_m22889_gshared ();
extern "C" void Array_swap_TisRaycastHit_t24_m22890_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisColor_t65_m22891_gshared ();
extern "C" void Array_Resize_TisUIVertex_t556_m22893_gshared ();
void* RuntimeInvoker_Void_t272_UIVertexU5BU5DU26_t3959_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUIVertex_t556_m22892_gshared ();
void* RuntimeInvoker_Void_t272_UIVertexU5BU5DU26_t3959_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUIVertex_t556_m22894_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_UIVertex_t556_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUIVertex_t556_m22896_gshared ();
extern "C" void Array_Sort_TisUIVertex_t556_TisUIVertex_t556_m22895_gshared ();
extern "C" void Array_get_swapper_TisUIVertex_t556_m22897_gshared ();
extern "C" void Array_qsort_TisUIVertex_t556_TisUIVertex_t556_m22898_gshared ();
extern "C" void Array_compare_TisUIVertex_t556_m22899_gshared ();
void* RuntimeInvoker_Int32_t253_UIVertex_t556_UIVertex_t556_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUIVertex_t556_TisUIVertex_t556_m22900_gshared ();
extern "C" void Array_Sort_TisUIVertex_t556_m22902_gshared ();
extern "C" void Array_qsort_TisUIVertex_t556_m22901_gshared ();
extern "C" void Array_swap_TisUIVertex_t556_m22903_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUILineInfo_t687_m22905_gshared ();
void* RuntimeInvoker_UILineInfo_t687_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUILineInfo_t687_m22906_gshared ();
void* RuntimeInvoker_Void_t272_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUILineInfo_t687_m22907_gshared ();
void* RuntimeInvoker_Boolean_t273_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t687_m22908_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUILineInfo_t687_m22909_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUILineInfo_t687_m22910_gshared ();
void* RuntimeInvoker_Int32_t253_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUILineInfo_t687_m22911_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUILineInfo_t687_m22913_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t687_m22914_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUICharInfo_t689_m22916_gshared ();
void* RuntimeInvoker_UICharInfo_t689_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUICharInfo_t689_m22917_gshared ();
void* RuntimeInvoker_Void_t272_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUICharInfo_t689_m22918_gshared ();
void* RuntimeInvoker_Boolean_t273_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t689_m22919_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUICharInfo_t689_m22920_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUICharInfo_t689_m22921_gshared ();
void* RuntimeInvoker_Int32_t253_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUICharInfo_t689_m22922_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUICharInfo_t689_m22924_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t689_m22925_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t254_m22926_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t6_m22927_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisByte_t680_m22928_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcAchievementData_t958_m22934_gshared ();
void* RuntimeInvoker_GcAchievementData_t958_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisGcAchievementData_t958_m22935_gshared ();
void* RuntimeInvoker_Void_t272_GcAchievementData_t958 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisGcAchievementData_t958_m22936_gshared ();
void* RuntimeInvoker_Boolean_t273_GcAchievementData_t958 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t958_m22937_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcAchievementData_t958_m22938_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcAchievementData_t958_m22939_gshared ();
void* RuntimeInvoker_Int32_t253_GcAchievementData_t958 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisGcAchievementData_t958_m22940_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_GcAchievementData_t958 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisGcAchievementData_t958_m22942_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t958_m22943_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcScoreData_t959_m22945_gshared ();
void* RuntimeInvoker_GcScoreData_t959_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisGcScoreData_t959_m22946_gshared ();
void* RuntimeInvoker_Void_t272_GcScoreData_t959 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisGcScoreData_t959_m22947_gshared ();
void* RuntimeInvoker_Boolean_t273_GcScoreData_t959 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t959_m22948_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcScoreData_t959_m22949_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcScoreData_t959_m22950_gshared ();
void* RuntimeInvoker_Int32_t253_GcScoreData_t959 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisGcScoreData_t959_m22951_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_GcScoreData_t959 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisGcScoreData_t959_m22953_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t959_m22954_gshared ();
extern "C" void Array_InternalArray__get_Item_TisIntPtr_t_m22956_gshared ();
void* RuntimeInvoker_IntPtr_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisIntPtr_t_m22957_gshared ();
void* RuntimeInvoker_Void_t272_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisIntPtr_t_m22958_gshared ();
void* RuntimeInvoker_Boolean_t273_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m22959_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisIntPtr_t_m22960_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisIntPtr_t_m22961_gshared ();
void* RuntimeInvoker_Int32_t253_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisIntPtr_t_m22962_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisIntPtr_t_m22964_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m22965_gshared ();
extern "C" void Array_Resize_TisUICharInfo_t689_m22967_gshared ();
void* RuntimeInvoker_Void_t272_UICharInfoU5BU5DU26_t3960_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUICharInfo_t689_m22966_gshared ();
void* RuntimeInvoker_Void_t272_UICharInfoU5BU5DU26_t3960_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUICharInfo_t689_m22968_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_UICharInfo_t689_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUICharInfo_t689_m22970_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t689_TisUICharInfo_t689_m22969_gshared ();
extern "C" void Array_get_swapper_TisUICharInfo_t689_m22971_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t689_TisUICharInfo_t689_m22972_gshared ();
extern "C" void Array_compare_TisUICharInfo_t689_m22973_gshared ();
void* RuntimeInvoker_Int32_t253_UICharInfo_t689_UICharInfo_t689_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUICharInfo_t689_TisUICharInfo_t689_m22974_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t689_m22976_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t689_m22975_gshared ();
extern "C" void Array_swap_TisUICharInfo_t689_m22977_gshared ();
extern "C" void Array_Resize_TisUILineInfo_t687_m22979_gshared ();
void* RuntimeInvoker_Void_t272_UILineInfoU5BU5DU26_t3961_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUILineInfo_t687_m22978_gshared ();
void* RuntimeInvoker_Void_t272_UILineInfoU5BU5DU26_t3961_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUILineInfo_t687_m22980_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_UILineInfo_t687_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUILineInfo_t687_m22982_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t687_TisUILineInfo_t687_m22981_gshared ();
extern "C" void Array_get_swapper_TisUILineInfo_t687_m22983_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t687_TisUILineInfo_t687_m22984_gshared ();
extern "C" void Array_compare_TisUILineInfo_t687_m22985_gshared ();
void* RuntimeInvoker_Int32_t253_UILineInfo_t687_UILineInfo_t687_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUILineInfo_t687_TisUILineInfo_t687_m22986_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t687_m22988_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t687_m22987_gshared ();
extern "C" void Array_swap_TisUILineInfo_t687_m22989_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3255_m22991_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3255_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3255_m22992_gshared ();
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3255_m22993_gshared ();
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3255_m22994_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3255_m22995_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3255_m22996_gshared ();
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3255_m22997_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3255_m22999_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3255_m23000_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt64_t1071_m23002_gshared ();
void* RuntimeInvoker_Int64_t1071_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt64_t1071_m23003_gshared ();
void* RuntimeInvoker_Void_t272_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisInt64_t1071_m23004_gshared ();
void* RuntimeInvoker_Boolean_t273_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt64_t1071_m23005_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt64_t1071_m23006_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt64_t1071_m23007_gshared ();
void* RuntimeInvoker_Int32_t253_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisInt64_t1071_m23008_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisInt64_t1071_m23010_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t1071_m23011_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m23013_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m23012_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt64_t1071_m23015_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt64_t1071_TisObject_t_m23014_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt64_t1071_TisInt64_t1071_m23016_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m23017_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3255_m23019_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3255_TisObject_t_m23018_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3255_TisKeyValuePair_2_t3255_m23020_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3293_m23022_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3293_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3293_m23023_gshared ();
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3293 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3293_m23024_gshared ();
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3293 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3293_m23025_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3293_m23026_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3293_m23027_gshared ();
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3293 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3293_m23028_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3293 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3293_m23030_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3293_m23031_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt64_t1074_m23033_gshared ();
void* RuntimeInvoker_UInt64_t1074_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt64_t1074_m23034_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt64_t1074_m23035_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt64_t1074_m23036_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt64_t1074_m23037_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt64_t1074_m23038_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt64_t1074_m23039_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt64_t1074_m23041_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t1074_m23042_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisUInt64_t1074_m23044_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt64_t1074_TisObject_t_m23043_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt64_t1074_TisUInt64_t1074_m23045_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m23047_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m23046_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m23048_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3293_m23050_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3293_TisObject_t_m23049_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3293_TisKeyValuePair_2_t3293_m23051_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3314_m23053_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3314_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3314_m23054_gshared ();
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3314 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3314_m23055_gshared ();
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3314 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3314_m23056_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3314_m23057_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3314_m23058_gshared ();
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3314 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3314_m23059_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3314 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3314_m23061_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3314_m23062_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m23064_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m23063_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2815_m23066_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2815_TisObject_t_m23065_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2815_TisKeyValuePair_2_t2815_m23067_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m23068_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3314_m23070_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3314_TisObject_t_m23069_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3314_TisKeyValuePair_2_t3314_m23071_gshared ();
extern "C" void Array_InternalArray__get_Item_TisParameterModifier_t1914_m23073_gshared ();
void* RuntimeInvoker_ParameterModifier_t1914_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisParameterModifier_t1914_m23074_gshared ();
void* RuntimeInvoker_Void_t272_ParameterModifier_t1914 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisParameterModifier_t1914_m23075_gshared ();
void* RuntimeInvoker_Boolean_t273_ParameterModifier_t1914 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t1914_m23076_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisParameterModifier_t1914_m23077_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisParameterModifier_t1914_m23078_gshared ();
void* RuntimeInvoker_Int32_t253_ParameterModifier_t1914 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisParameterModifier_t1914_m23079_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_ParameterModifier_t1914 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisParameterModifier_t1914_m23081_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t1914_m23082_gshared ();
extern "C" void Array_InternalArray__get_Item_TisHitInfo_t983_m23084_gshared ();
void* RuntimeInvoker_HitInfo_t983_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisHitInfo_t983_m23085_gshared ();
void* RuntimeInvoker_Void_t272_HitInfo_t983 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisHitInfo_t983_m23086_gshared ();
void* RuntimeInvoker_Boolean_t273_HitInfo_t983 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisHitInfo_t983_m23087_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisHitInfo_t983_m23088_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisHitInfo_t983_m23089_gshared ();
void* RuntimeInvoker_Int32_t253_HitInfo_t983 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisHitInfo_t983_m23090_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_HitInfo_t983 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisHitInfo_t983_m23092_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t983_m23093_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t253_m23094_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3407_m23096_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3407_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3407_m23097_gshared ();
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3407 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3407_m23098_gshared ();
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3407 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3407_m23099_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3407_m23100_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3407_m23101_gshared ();
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3407 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3407_m23102_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3407 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3407_m23104_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3407_m23105_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m23107_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m23106_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisByte_t680_m23109_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisByte_t680_TisObject_t_m23108_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisByte_t680_TisByte_t680_m23110_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m23111_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3407_m23113_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3407_TisObject_t_m23112_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3407_TisKeyValuePair_2_t3407_m23114_gshared ();
extern "C" void Array_InternalArray__get_Item_TisX509ChainStatus_t1331_m23116_gshared ();
void* RuntimeInvoker_X509ChainStatus_t1331_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisX509ChainStatus_t1331_m23117_gshared ();
void* RuntimeInvoker_Void_t272_X509ChainStatus_t1331 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t1331_m23118_gshared ();
void* RuntimeInvoker_Boolean_t273_X509ChainStatus_t1331 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t1331_m23119_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t1331_m23120_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisX509ChainStatus_t1331_m23121_gshared ();
void* RuntimeInvoker_Int32_t253_X509ChainStatus_t1331 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisX509ChainStatus_t1331_m23122_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_X509ChainStatus_t1331 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisX509ChainStatus_t1331_m23124_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t1331_m23125_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3427_m23127_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3427_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3427_m23128_gshared ();
void* RuntimeInvoker_Void_t272_KeyValuePair_2_t3427 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3427_m23129_gshared ();
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3427 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3427_m23130_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3427_m23131_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3427_m23132_gshared ();
void* RuntimeInvoker_Int32_t253_KeyValuePair_2_t3427 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3427_m23133_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3427 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3427_m23135_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3427_m23136_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t253_m23138_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t253_TisObject_t_m23137_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t253_TisInt32_t253_m23139_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m23140_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3427_m23142_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3427_TisObject_t_m23141_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3427_TisKeyValuePair_2_t3427_m23143_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t253_m23144_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__get_Item_TisMark_t1382_m23146_gshared ();
void* RuntimeInvoker_Mark_t1382_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisMark_t1382_m23147_gshared ();
void* RuntimeInvoker_Void_t272_Mark_t1382 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisMark_t1382_m23148_gshared ();
void* RuntimeInvoker_Boolean_t273_Mark_t1382 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisMark_t1382_m23149_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisMark_t1382_m23150_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisMark_t1382_m23151_gshared ();
void* RuntimeInvoker_Int32_t253_Mark_t1382 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisMark_t1382_m23152_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Mark_t1382 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisMark_t1382_m23154_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t1382_m23155_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUriScheme_t1418_m23157_gshared ();
void* RuntimeInvoker_UriScheme_t1418_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUriScheme_t1418_m23158_gshared ();
void* RuntimeInvoker_Void_t272_UriScheme_t1418 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUriScheme_t1418_m23159_gshared ();
void* RuntimeInvoker_Boolean_t273_UriScheme_t1418 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUriScheme_t1418_m23160_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUriScheme_t1418_m23161_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUriScheme_t1418_m23162_gshared ();
void* RuntimeInvoker_Int32_t253_UriScheme_t1418 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUriScheme_t1418_m23163_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_UriScheme_t1418 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUriScheme_t1418_m23165_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t1418_m23166_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt32_t1063_m23168_gshared ();
void* RuntimeInvoker_UInt32_t1063_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt32_t1063_m23169_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt32_t1063_m23170_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt32_t1063_m23171_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt32_t1063_m23172_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt32_t1063_m23173_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt32_t1063_m23174_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt32_t1063_m23176_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t1063_m23177_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt16_t752_m23179_gshared ();
void* RuntimeInvoker_Int16_t752_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt16_t752_m23180_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt16_t752_m23181_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt16_t752_m23182_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt16_t752_m23183_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt16_t752_m23184_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt16_t752_m23185_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt16_t752_m23187_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t752_m23188_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSByte_t274_m23190_gshared ();
void* RuntimeInvoker_SByte_t274_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSByte_t274_m23191_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSByte_t274_m23192_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSByte_t274_m23193_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSByte_t274_m23194_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSByte_t274_m23195_gshared ();
extern "C" void Array_InternalArray__Insert_TisSByte_t274_m23196_gshared ();
extern "C" void Array_InternalArray__set_Item_TisSByte_t274_m23198_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t274_m23199_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTableRange_t1690_m23229_gshared ();
void* RuntimeInvoker_TableRange_t1690_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTableRange_t1690_m23230_gshared ();
void* RuntimeInvoker_Void_t272_TableRange_t1690 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisTableRange_t1690_m23231_gshared ();
void* RuntimeInvoker_Boolean_t273_TableRange_t1690 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t1690_m23232_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTableRange_t1690_m23233_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTableRange_t1690_m23234_gshared ();
void* RuntimeInvoker_Int32_t253_TableRange_t1690 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTableRange_t1690_m23235_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_TableRange_t1690 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTableRange_t1690_m23237_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1690_m23238_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t1767_m23240_gshared ();
void* RuntimeInvoker_Slot_t1767_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1767_m23241_gshared ();
void* RuntimeInvoker_Void_t272_Slot_t1767 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t1767_m23242_gshared ();
void* RuntimeInvoker_Boolean_t273_Slot_t1767 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1767_m23243_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t1767_m23244_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t1767_m23245_gshared ();
void* RuntimeInvoker_Int32_t253_Slot_t1767 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSlot_t1767_m23246_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Slot_t1767 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSlot_t1767_m23248_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1767_m23249_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t1776_m23251_gshared ();
void* RuntimeInvoker_Slot_t1776_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1776_m23252_gshared ();
void* RuntimeInvoker_Void_t272_Slot_t1776 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t1776_m23253_gshared ();
void* RuntimeInvoker_Boolean_t273_Slot_t1776 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1776_m23254_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t1776_m23255_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t1776_m23256_gshared ();
void* RuntimeInvoker_Int32_t253_Slot_t1776 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSlot_t1776_m23257_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Slot_t1776 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSlot_t1776_m23259_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1776_m23260_gshared ();
extern "C" void Array_InternalArray__get_Item_TisMonoResource_t1838_m23262_gshared ();
void* RuntimeInvoker_MonoResource_t1838_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisMonoResource_t1838_m23263_gshared ();
void* RuntimeInvoker_Void_t272_MonoResource_t1838 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisMonoResource_t1838_m23264_gshared ();
void* RuntimeInvoker_Boolean_t273_MonoResource_t1838 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisMonoResource_t1838_m23265_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisMonoResource_t1838_m23266_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisMonoResource_t1838_m23267_gshared ();
void* RuntimeInvoker_Int32_t253_MonoResource_t1838 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisMonoResource_t1838_m23268_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_MonoResource_t1838 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisMonoResource_t1838_m23270_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisMonoResource_t1838_m23271_gshared ();
extern "C" void Array_InternalArray__get_Item_TisILTokenInfo_t1856_m23273_gshared ();
void* RuntimeInvoker_ILTokenInfo_t1856_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisILTokenInfo_t1856_m23274_gshared ();
void* RuntimeInvoker_Void_t272_ILTokenInfo_t1856 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisILTokenInfo_t1856_m23275_gshared ();
void* RuntimeInvoker_Boolean_t273_ILTokenInfo_t1856 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t1856_m23276_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisILTokenInfo_t1856_m23277_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisILTokenInfo_t1856_m23278_gshared ();
void* RuntimeInvoker_Int32_t253_ILTokenInfo_t1856 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisILTokenInfo_t1856_m23279_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_ILTokenInfo_t1856 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisILTokenInfo_t1856_m23281_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t1856_m23282_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLabelData_t1858_m23284_gshared ();
void* RuntimeInvoker_LabelData_t1858_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisLabelData_t1858_m23285_gshared ();
void* RuntimeInvoker_Void_t272_LabelData_t1858 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisLabelData_t1858_m23286_gshared ();
void* RuntimeInvoker_Boolean_t273_LabelData_t1858 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelData_t1858_m23287_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLabelData_t1858_m23288_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLabelData_t1858_m23289_gshared ();
void* RuntimeInvoker_Int32_t253_LabelData_t1858 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisLabelData_t1858_m23290_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_LabelData_t1858 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisLabelData_t1858_m23292_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t1858_m23293_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLabelFixup_t1857_m23295_gshared ();
void* RuntimeInvoker_LabelFixup_t1857_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisLabelFixup_t1857_m23296_gshared ();
void* RuntimeInvoker_Void_t272_LabelFixup_t1857 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisLabelFixup_t1857_m23297_gshared ();
void* RuntimeInvoker_Boolean_t273_LabelFixup_t1857 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t1857_m23298_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLabelFixup_t1857_m23299_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLabelFixup_t1857_m23300_gshared ();
void* RuntimeInvoker_Int32_t253_LabelFixup_t1857 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisLabelFixup_t1857_m23301_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_LabelFixup_t1857 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisLabelFixup_t1857_m23303_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t1857_m23304_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDateTime_t406_m23308_gshared ();
void* RuntimeInvoker_DateTime_t406_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t406_m23309_gshared ();
void* RuntimeInvoker_Void_t272_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDateTime_t406_m23310_gshared ();
void* RuntimeInvoker_Boolean_t273_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t406_m23311_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDateTime_t406_m23312_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDateTime_t406_m23313_gshared ();
void* RuntimeInvoker_Int32_t253_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDateTime_t406_m23314_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDateTime_t406_m23316_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t406_m23317_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDecimal_t1073_m23319_gshared ();
void* RuntimeInvoker_Decimal_t1073_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t1073_m23320_gshared ();
void* RuntimeInvoker_Void_t272_Decimal_t1073 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDecimal_t1073_m23321_gshared ();
void* RuntimeInvoker_Boolean_t273_Decimal_t1073 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t1073_m23322_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDecimal_t1073_m23323_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDecimal_t1073_m23324_gshared ();
void* RuntimeInvoker_Int32_t253_Decimal_t1073 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDecimal_t1073_m23325_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_Decimal_t1073 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDecimal_t1073_m23327_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1073_m23328_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTimeSpan_t1337_m23330_gshared ();
void* RuntimeInvoker_TimeSpan_t1337_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t1337_m23331_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisTimeSpan_t1337_m23332_gshared ();
void* RuntimeInvoker_Boolean_t273_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t1337_m23333_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTimeSpan_t1337_m23334_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTimeSpan_t1337_m23335_gshared ();
void* RuntimeInvoker_Int32_t253_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t1337_m23336_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t1337_m23338_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t1337_m23339_gshared ();
extern "C" void InternalEnumerator_1__ctor_m13522_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13523_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13524_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13525_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13526_gshared ();
void* RuntimeInvoker_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m13537_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13538_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13539_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13540_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13541_gshared ();
void* RuntimeInvoker_Single_t254 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m13646_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13647_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13648_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13649_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13650_gshared ();
extern "C" void InternalEnumerator_1__ctor_m13662_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13663_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13664_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13665_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13666_gshared ();
extern "C" void InternalEnumerator_1__ctor_m13667_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13668_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13669_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13670_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13671_gshared ();
void* RuntimeInvoker_Link_t1757 (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1__ctor_m13726_gshared ();
extern "C" void Transform_1_Invoke_m13727_gshared ();
extern "C" void Transform_1_BeginInvoke_m13728_gshared ();
extern "C" void Transform_1_EndInvoke_m13729_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m13730_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13731_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13732_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13733_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13734_gshared ();
extern "C" void Transform_1__ctor_m13735_gshared ();
extern "C" void Transform_1_Invoke_m13736_gshared ();
extern "C" void Transform_1_BeginInvoke_m13737_gshared ();
extern "C" void Transform_1_EndInvoke_m13738_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2815_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m13907_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13908_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13909_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13910_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13911_gshared ();
void* RuntimeInvoker_Touch_t235 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m14065_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14066_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14067_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14068_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14069_gshared ();
void* RuntimeInvoker_Double_t1070 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m14071_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14073_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14075_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14077_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14079_gshared ();
void* RuntimeInvoker_UInt16_t684 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m14128_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14129_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14130_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14131_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14132_gshared ();
void* RuntimeInvoker_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m14133_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14134_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14135_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14136_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14137_gshared ();
void* RuntimeInvoker_Keyframe_t240 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m14138_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14139_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14140_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14141_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14142_gshared ();
void* RuntimeInvoker_Color_t65 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m14153_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14154_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14155_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14156_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14157_gshared ();
void* RuntimeInvoker_Vector3_t4 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m14253_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14254_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14255_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14256_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14257_gshared ();
void* RuntimeInvoker_ContactPoint_t246 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m14268_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14269_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14270_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14271_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14272_gshared ();
void* RuntimeInvoker_ParticleCollisionEvent_t160 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m14955_gshared ();
extern "C" void List_1__ctor_m14956_gshared ();
extern "C" void List_1__cctor_m14957_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14958_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m14959_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m14960_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m14961_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m14962_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m14963_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m14964_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m14965_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14966_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m14967_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m14968_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m14969_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m14970_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m14971_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m14972_gshared ();
extern "C" void List_1_Add_m14973_gshared ();
extern "C" void List_1_GrowIfNeeded_m14974_gshared ();
extern "C" void List_1_AddCollection_m14975_gshared ();
extern "C" void List_1_AddEnumerable_m14976_gshared ();
extern "C" void List_1_AddRange_m14977_gshared ();
extern "C" void List_1_AsReadOnly_m14978_gshared ();
extern "C" void List_1_Clear_m14979_gshared ();
extern "C" void List_1_Contains_m14980_gshared ();
extern "C" void List_1_CopyTo_m14981_gshared ();
extern "C" void List_1_Find_m14982_gshared ();
void* RuntimeInvoker_Vector2_t6_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m14983_gshared ();
extern "C" void List_1_GetIndex_m14984_gshared ();
extern "C" void List_1_GetEnumerator_m14985_gshared ();
void* RuntimeInvoker_Enumerator_t2913 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m14986_gshared ();
extern "C" void List_1_Shift_m14987_gshared ();
extern "C" void List_1_CheckIndex_m14988_gshared ();
extern "C" void List_1_Insert_m14989_gshared ();
extern "C" void List_1_CheckCollection_m14990_gshared ();
extern "C" void List_1_Remove_m14991_gshared ();
extern "C" void List_1_RemoveAll_m14992_gshared ();
extern "C" void List_1_RemoveAt_m14993_gshared ();
extern "C" void List_1_Reverse_m14994_gshared ();
extern "C" void List_1_Sort_m14995_gshared ();
extern "C" void List_1_Sort_m14996_gshared ();
extern "C" void List_1_ToArray_m14997_gshared ();
extern "C" void List_1_TrimExcess_m14998_gshared ();
extern "C" void List_1_get_Capacity_m14999_gshared ();
extern "C" void List_1_set_Capacity_m15000_gshared ();
extern "C" void List_1_get_Count_m15001_gshared ();
extern "C" void List_1_get_Item_m15002_gshared ();
extern "C" void List_1_set_Item_m15003_gshared ();
extern "C" void Enumerator__ctor_m15004_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15005_gshared ();
extern "C" void Enumerator_Dispose_m15006_gshared ();
extern "C" void Enumerator_VerifyState_m15007_gshared ();
extern "C" void Enumerator_MoveNext_m15008_gshared ();
extern "C" void Enumerator_get_Current_m15009_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m15010_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15011_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15012_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15013_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15014_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15015_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15016_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15017_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15018_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15019_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15020_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m15021_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15022_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m15023_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15024_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15025_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15026_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15027_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15028_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15029_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15030_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15031_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m15032_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15033_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m15034_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m15035_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m15036_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m15037_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m15038_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m15039_gshared ();
extern "C" void Collection_1__ctor_m15040_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15041_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15042_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m15043_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m15044_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m15045_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m15046_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m15047_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m15048_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m15049_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m15050_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m15051_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m15052_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m15053_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m15054_gshared ();
extern "C" void Collection_1_Add_m15055_gshared ();
extern "C" void Collection_1_Clear_m15056_gshared ();
extern "C" void Collection_1_ClearItems_m15057_gshared ();
extern "C" void Collection_1_Contains_m15058_gshared ();
extern "C" void Collection_1_CopyTo_m15059_gshared ();
extern "C" void Collection_1_GetEnumerator_m15060_gshared ();
extern "C" void Collection_1_IndexOf_m15061_gshared ();
extern "C" void Collection_1_Insert_m15062_gshared ();
extern "C" void Collection_1_InsertItem_m15063_gshared ();
extern "C" void Collection_1_Remove_m15064_gshared ();
extern "C" void Collection_1_RemoveAt_m15065_gshared ();
extern "C" void Collection_1_RemoveItem_m15066_gshared ();
extern "C" void Collection_1_get_Count_m15067_gshared ();
extern "C" void Collection_1_get_Item_m15068_gshared ();
extern "C" void Collection_1_set_Item_m15069_gshared ();
extern "C" void Collection_1_SetItem_m15070_gshared ();
extern "C" void Collection_1_IsValidItem_m15071_gshared ();
extern "C" void Collection_1_ConvertItem_m15072_gshared ();
extern "C" void Collection_1_CheckWritable_m15073_gshared ();
extern "C" void Collection_1_IsSynchronized_m15074_gshared ();
extern "C" void Collection_1_IsFixedSize_m15075_gshared ();
extern "C" void EqualityComparer_1__ctor_m15076_gshared ();
extern "C" void EqualityComparer_1__cctor_m15077_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15078_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15079_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15080_gshared ();
extern "C" void DefaultComparer__ctor_m15081_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15082_gshared ();
extern "C" void DefaultComparer_Equals_m15083_gshared ();
void* RuntimeInvoker_Boolean_t273_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m15084_gshared ();
extern "C" void Predicate_1_Invoke_m15085_gshared ();
extern "C" void Predicate_1_BeginInvoke_m15086_gshared ();
void* RuntimeInvoker_Object_t_Vector2_t6_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m15087_gshared ();
extern "C" void Comparer_1__ctor_m15088_gshared ();
extern "C" void Comparer_1__cctor_m15089_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m15090_gshared ();
extern "C" void Comparer_1_get_Default_m15091_gshared ();
extern "C" void DefaultComparer__ctor_m15092_gshared ();
extern "C" void DefaultComparer_Compare_m15093_gshared ();
void* RuntimeInvoker_Int32_t253_Vector2_t6_Vector2_t6 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m15094_gshared ();
extern "C" void Comparison_1_Invoke_m15095_gshared ();
extern "C" void Comparison_1_BeginInvoke_m15096_gshared ();
void* RuntimeInvoker_Object_t_Vector2_t6_Vector2_t6_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m15097_gshared ();
extern "C" void List_1__ctor_m15208_gshared ();
extern "C" void List_1__ctor_m15209_gshared ();
extern "C" void List_1__cctor_m15210_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15211_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15212_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m15213_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m15214_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m15215_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m15216_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m15217_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m15218_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15219_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m15220_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m15221_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m15222_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m15223_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m15224_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m15225_gshared ();
extern "C" void List_1_Add_m15226_gshared ();
extern "C" void List_1_GrowIfNeeded_m15227_gshared ();
extern "C" void List_1_AddCollection_m15228_gshared ();
extern "C" void List_1_AddEnumerable_m15229_gshared ();
extern "C" void List_1_AddRange_m15230_gshared ();
extern "C" void List_1_AsReadOnly_m15231_gshared ();
extern "C" void List_1_Clear_m15232_gshared ();
extern "C" void List_1_Contains_m15233_gshared ();
extern "C" void List_1_CopyTo_m15234_gshared ();
extern "C" void List_1_Find_m15235_gshared ();
void* RuntimeInvoker_RaycastResult_t486_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m15236_gshared ();
extern "C" void List_1_GetIndex_m15237_gshared ();
extern "C" void List_1_GetEnumerator_m15238_gshared ();
void* RuntimeInvoker_Enumerator_t2935 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m15239_gshared ();
extern "C" void List_1_Shift_m15240_gshared ();
extern "C" void List_1_CheckIndex_m15241_gshared ();
extern "C" void List_1_Insert_m15242_gshared ();
extern "C" void List_1_CheckCollection_m15243_gshared ();
extern "C" void List_1_Remove_m15244_gshared ();
extern "C" void List_1_RemoveAll_m15245_gshared ();
extern "C" void List_1_RemoveAt_m15246_gshared ();
extern "C" void List_1_Reverse_m15247_gshared ();
extern "C" void List_1_Sort_m15248_gshared ();
extern "C" void List_1_ToArray_m15249_gshared ();
extern "C" void List_1_TrimExcess_m15250_gshared ();
extern "C" void List_1_get_Capacity_m15251_gshared ();
extern "C" void List_1_set_Capacity_m15252_gshared ();
extern "C" void List_1_get_Count_m15253_gshared ();
extern "C" void List_1_get_Item_m15254_gshared ();
extern "C" void List_1_set_Item_m15255_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15256_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15257_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15258_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15259_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15260_gshared ();
void* RuntimeInvoker_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator__ctor_m15261_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15262_gshared ();
extern "C" void Enumerator_Dispose_m15263_gshared ();
extern "C" void Enumerator_VerifyState_m15264_gshared ();
extern "C" void Enumerator_MoveNext_m15265_gshared ();
extern "C" void Enumerator_get_Current_m15266_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m15267_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15268_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15269_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15270_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15271_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15272_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15273_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15274_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15275_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15276_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15277_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m15278_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15279_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m15280_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15281_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15282_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15283_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15284_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15285_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15286_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15287_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15288_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m15289_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15290_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m15291_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m15292_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m15293_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m15294_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m15295_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m15296_gshared ();
extern "C" void Collection_1__ctor_m15297_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15298_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15299_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m15300_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m15301_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m15302_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m15303_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m15304_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m15305_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m15306_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m15307_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m15308_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m15309_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m15310_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m15311_gshared ();
extern "C" void Collection_1_Add_m15312_gshared ();
extern "C" void Collection_1_Clear_m15313_gshared ();
extern "C" void Collection_1_ClearItems_m15314_gshared ();
extern "C" void Collection_1_Contains_m15315_gshared ();
extern "C" void Collection_1_CopyTo_m15316_gshared ();
extern "C" void Collection_1_GetEnumerator_m15317_gshared ();
extern "C" void Collection_1_IndexOf_m15318_gshared ();
extern "C" void Collection_1_Insert_m15319_gshared ();
extern "C" void Collection_1_InsertItem_m15320_gshared ();
extern "C" void Collection_1_Remove_m15321_gshared ();
extern "C" void Collection_1_RemoveAt_m15322_gshared ();
extern "C" void Collection_1_RemoveItem_m15323_gshared ();
extern "C" void Collection_1_get_Count_m15324_gshared ();
extern "C" void Collection_1_get_Item_m15325_gshared ();
extern "C" void Collection_1_set_Item_m15326_gshared ();
extern "C" void Collection_1_SetItem_m15327_gshared ();
extern "C" void Collection_1_IsValidItem_m15328_gshared ();
extern "C" void Collection_1_ConvertItem_m15329_gshared ();
extern "C" void Collection_1_CheckWritable_m15330_gshared ();
extern "C" void Collection_1_IsSynchronized_m15331_gshared ();
extern "C" void Collection_1_IsFixedSize_m15332_gshared ();
extern "C" void EqualityComparer_1__ctor_m15333_gshared ();
extern "C" void EqualityComparer_1__cctor_m15334_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15335_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15336_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15337_gshared ();
extern "C" void DefaultComparer__ctor_m15338_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15339_gshared ();
extern "C" void DefaultComparer_Equals_m15340_gshared ();
void* RuntimeInvoker_Boolean_t273_RaycastResult_t486_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m15341_gshared ();
extern "C" void Predicate_1_Invoke_m15342_gshared ();
extern "C" void Predicate_1_BeginInvoke_m15343_gshared ();
void* RuntimeInvoker_Object_t_RaycastResult_t486_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m15344_gshared ();
extern "C" void Comparer_1__ctor_m15345_gshared ();
extern "C" void Comparer_1__cctor_m15346_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m15347_gshared ();
extern "C" void Comparer_1_get_Default_m15348_gshared ();
extern "C" void DefaultComparer__ctor_m15349_gshared ();
extern "C" void DefaultComparer_Compare_m15350_gshared ();
void* RuntimeInvoker_Int32_t253_RaycastResult_t486_RaycastResult_t486 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_Invoke_m15351_gshared ();
extern "C" void Comparison_1_BeginInvoke_m15352_gshared ();
void* RuntimeInvoker_Object_t_RaycastResult_t486_RaycastResult_t486_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m15353_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15459_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15461_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15462_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15463_gshared ();
void* RuntimeInvoker_Quaternion_t19 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m15465_gshared ();
extern "C" void Dictionary_2__ctor_m15467_gshared ();
extern "C" void Dictionary_2__ctor_m15469_gshared ();
extern "C" void Dictionary_2__ctor_m15472_gshared ();
extern "C" void Dictionary_2__ctor_m15474_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15476_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15478_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m15480_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m15482_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m15484_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m15486_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m15488_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15490_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15492_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15494_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15496_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15498_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15500_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15502_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m15504_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15506_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15508_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15510_gshared ();
extern "C" void Dictionary_2_get_Count_m15512_gshared ();
extern "C" void Dictionary_2_get_Item_m15514_gshared ();
extern "C" void Dictionary_2_set_Item_m15516_gshared ();
extern "C" void Dictionary_2_Init_m15518_gshared ();
extern "C" void Dictionary_2_InitArrays_m15520_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m15522_gshared ();
extern "C" void Dictionary_2_make_pair_m15524_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2953_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m15526_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m15528_gshared ();
void* RuntimeInvoker_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m15530_gshared ();
extern "C" void Dictionary_2_Resize_m15532_gshared ();
extern "C" void Dictionary_2_Add_m15534_gshared ();
extern "C" void Dictionary_2_Clear_m15536_gshared ();
extern "C" void Dictionary_2_ContainsKey_m15538_gshared ();
extern "C" void Dictionary_2_ContainsValue_m15540_gshared ();
extern "C" void Dictionary_2_GetObjectData_m15542_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m15544_gshared ();
extern "C" void Dictionary_2_Remove_m15546_gshared ();
extern "C" void Dictionary_2_TryGetValue_m15548_gshared ();
void* RuntimeInvoker_Boolean_t273_Object_t_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m15550_gshared ();
extern "C" void Dictionary_2_get_Values_m15552_gshared ();
extern "C" void Dictionary_2_ToTKey_m15554_gshared ();
extern "C" void Dictionary_2_ToTValue_m15556_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m15558_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m15560_gshared ();
void* RuntimeInvoker_Enumerator_t2957 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m15562_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m15563_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15564_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15565_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15566_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15567_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2953 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m15568_gshared ();
extern "C" void KeyValuePair_2_get_Key_m15569_gshared ();
extern "C" void KeyValuePair_2_set_Key_m15570_gshared ();
extern "C" void KeyValuePair_2_get_Value_m15571_gshared ();
extern "C" void KeyValuePair_2_set_Value_m15572_gshared ();
extern "C" void KeyValuePair_2_ToString_m15573_gshared ();
extern "C" void KeyCollection__ctor_m15574_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15575_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15576_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15577_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15578_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15579_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m15580_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15581_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15582_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15583_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m15584_gshared ();
extern "C" void KeyCollection_CopyTo_m15585_gshared ();
extern "C" void KeyCollection_GetEnumerator_m15586_gshared ();
void* RuntimeInvoker_Enumerator_t2956 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m15587_gshared ();
extern "C" void Enumerator__ctor_m15588_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15589_gshared ();
extern "C" void Enumerator_Dispose_m15590_gshared ();
extern "C" void Enumerator_MoveNext_m15591_gshared ();
extern "C" void Enumerator_get_Current_m15592_gshared ();
extern "C" void Enumerator__ctor_m15593_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15594_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15595_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15596_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15597_gshared ();
extern "C" void Enumerator_MoveNext_m15598_gshared ();
extern "C" void Enumerator_get_Current_m15599_gshared ();
extern "C" void Enumerator_get_CurrentKey_m15600_gshared ();
extern "C" void Enumerator_get_CurrentValue_m15601_gshared ();
extern "C" void Enumerator_VerifyState_m15602_gshared ();
extern "C" void Enumerator_VerifyCurrent_m15603_gshared ();
extern "C" void Enumerator_Dispose_m15604_gshared ();
extern "C" void Transform_1__ctor_m15605_gshared ();
extern "C" void Transform_1_Invoke_m15606_gshared ();
extern "C" void Transform_1_BeginInvoke_m15607_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m15608_gshared ();
extern "C" void ValueCollection__ctor_m15609_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15610_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15611_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15612_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15613_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15614_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m15615_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15616_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15617_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15618_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m15619_gshared ();
extern "C" void ValueCollection_CopyTo_m15620_gshared ();
extern "C" void ValueCollection_GetEnumerator_m15621_gshared ();
void* RuntimeInvoker_Enumerator_t2960 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m15622_gshared ();
extern "C" void Enumerator__ctor_m15623_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15624_gshared ();
extern "C" void Enumerator_Dispose_m15625_gshared ();
extern "C" void Enumerator_MoveNext_m15626_gshared ();
extern "C" void Enumerator_get_Current_m15627_gshared ();
extern "C" void Transform_1__ctor_m15628_gshared ();
extern "C" void Transform_1_Invoke_m15629_gshared ();
extern "C" void Transform_1_BeginInvoke_m15630_gshared ();
extern "C" void Transform_1_EndInvoke_m15631_gshared ();
extern "C" void Transform_1__ctor_m15632_gshared ();
extern "C" void Transform_1_Invoke_m15633_gshared ();
extern "C" void Transform_1_BeginInvoke_m15634_gshared ();
extern "C" void Transform_1_EndInvoke_m15635_gshared ();
extern "C" void Transform_1__ctor_m15636_gshared ();
extern "C" void Transform_1_Invoke_m15637_gshared ();
extern "C" void Transform_1_BeginInvoke_m15638_gshared ();
extern "C" void Transform_1_EndInvoke_m15639_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2953_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m15640_gshared ();
extern "C" void ShimEnumerator_MoveNext_m15641_gshared ();
extern "C" void ShimEnumerator_get_Entry_m15642_gshared ();
extern "C" void ShimEnumerator_get_Key_m15643_gshared ();
extern "C" void ShimEnumerator_get_Value_m15644_gshared ();
extern "C" void ShimEnumerator_get_Current_m15645_gshared ();
extern "C" void EqualityComparer_1__ctor_m15646_gshared ();
extern "C" void EqualityComparer_1__cctor_m15647_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15648_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15649_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15650_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m15651_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m15652_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m15653_gshared ();
void* RuntimeInvoker_Boolean_t273_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m15654_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15655_gshared ();
extern "C" void DefaultComparer_Equals_m15656_gshared ();
extern "C" void Dictionary_2__ctor_m15816_gshared ();
extern "C" void Dictionary_2__ctor_m15818_gshared ();
extern "C" void Dictionary_2__ctor_m15820_gshared ();
extern "C" void Dictionary_2__ctor_m15822_gshared ();
extern "C" void Dictionary_2__ctor_m15824_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15826_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15828_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m15830_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m15832_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m15834_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m15836_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m15838_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15840_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15842_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15844_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15846_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15848_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15850_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15852_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m15854_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15856_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15858_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15860_gshared ();
extern "C" void Dictionary_2_get_Count_m15862_gshared ();
extern "C" void Dictionary_2_get_Item_m15864_gshared ();
extern "C" void Dictionary_2_set_Item_m15866_gshared ();
extern "C" void Dictionary_2_Init_m15868_gshared ();
extern "C" void Dictionary_2_InitArrays_m15870_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m15872_gshared ();
extern "C" void Dictionary_2_make_pair_m15874_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2984_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m15876_gshared ();
void* RuntimeInvoker_Int32_t253_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m15878_gshared ();
void* RuntimeInvoker_Byte_t680_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m15880_gshared ();
extern "C" void Dictionary_2_Resize_m15882_gshared ();
extern "C" void Dictionary_2_Add_m15884_gshared ();
extern "C" void Dictionary_2_Clear_m15886_gshared ();
extern "C" void Dictionary_2_ContainsKey_m15888_gshared ();
extern "C" void Dictionary_2_ContainsValue_m15890_gshared ();
extern "C" void Dictionary_2_GetObjectData_m15892_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m15894_gshared ();
extern "C" void Dictionary_2_Remove_m15896_gshared ();
extern "C" void Dictionary_2_TryGetValue_m15898_gshared ();
void* RuntimeInvoker_Boolean_t273_Int32_t253_ByteU26_t1656 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m15900_gshared ();
extern "C" void Dictionary_2_get_Values_m15902_gshared ();
extern "C" void Dictionary_2_ToTKey_m15904_gshared ();
extern "C" void Dictionary_2_ToTValue_m15906_gshared ();
void* RuntimeInvoker_Byte_t680_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_ContainsKeyValuePair_m15908_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m15910_gshared ();
void* RuntimeInvoker_Enumerator_t2989 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m15912_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m15913_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15914_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15915_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15916_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15917_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2984 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m15918_gshared ();
extern "C" void KeyValuePair_2_get_Key_m15919_gshared ();
extern "C" void KeyValuePair_2_set_Key_m15920_gshared ();
extern "C" void KeyValuePair_2_get_Value_m15921_gshared ();
void* RuntimeInvoker_Byte_t680 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_set_Value_m15922_gshared ();
extern "C" void KeyValuePair_2_ToString_m15923_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15924_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15925_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15926_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15927_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15928_gshared ();
extern "C" void KeyCollection__ctor_m15929_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15930_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15931_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15932_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15933_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15934_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m15935_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15936_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15937_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15938_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m15939_gshared ();
extern "C" void KeyCollection_CopyTo_m15940_gshared ();
extern "C" void KeyCollection_GetEnumerator_m15941_gshared ();
void* RuntimeInvoker_Enumerator_t2988 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m15942_gshared ();
extern "C" void Enumerator__ctor_m15943_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15944_gshared ();
extern "C" void Enumerator_Dispose_m15945_gshared ();
extern "C" void Enumerator_MoveNext_m15946_gshared ();
extern "C" void Enumerator_get_Current_m15947_gshared ();
extern "C" void Enumerator__ctor_m15948_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15949_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15950_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15951_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15952_gshared ();
extern "C" void Enumerator_MoveNext_m15953_gshared ();
extern "C" void Enumerator_get_Current_m15954_gshared ();
extern "C" void Enumerator_get_CurrentKey_m15955_gshared ();
extern "C" void Enumerator_get_CurrentValue_m15956_gshared ();
extern "C" void Enumerator_VerifyState_m15957_gshared ();
extern "C" void Enumerator_VerifyCurrent_m15958_gshared ();
extern "C" void Enumerator_Dispose_m15959_gshared ();
extern "C" void Transform_1__ctor_m15960_gshared ();
extern "C" void Transform_1_Invoke_m15961_gshared ();
extern "C" void Transform_1_BeginInvoke_m15962_gshared ();
void* RuntimeInvoker_Object_t_Int32_t253_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m15963_gshared ();
extern "C" void ValueCollection__ctor_m15964_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15965_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15966_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15967_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15968_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15969_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m15970_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15971_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15972_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15973_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m15974_gshared ();
extern "C" void ValueCollection_CopyTo_m15975_gshared ();
extern "C" void ValueCollection_GetEnumerator_m15976_gshared ();
void* RuntimeInvoker_Enumerator_t2992 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m15977_gshared ();
extern "C" void Enumerator__ctor_m15978_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15979_gshared ();
extern "C" void Enumerator_Dispose_m15980_gshared ();
extern "C" void Enumerator_MoveNext_m15981_gshared ();
extern "C" void Enumerator_get_Current_m15982_gshared ();
extern "C" void Transform_1__ctor_m15983_gshared ();
extern "C" void Transform_1_Invoke_m15984_gshared ();
extern "C" void Transform_1_BeginInvoke_m15985_gshared ();
extern "C" void Transform_1_EndInvoke_m15986_gshared ();
extern "C" void Transform_1__ctor_m15987_gshared ();
extern "C" void Transform_1_Invoke_m15988_gshared ();
extern "C" void Transform_1_BeginInvoke_m15989_gshared ();
extern "C" void Transform_1_EndInvoke_m15990_gshared ();
extern "C" void Transform_1__ctor_m15991_gshared ();
extern "C" void Transform_1_Invoke_m15992_gshared ();
extern "C" void Transform_1_BeginInvoke_m15993_gshared ();
extern "C" void Transform_1_EndInvoke_m15994_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2984_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m15995_gshared ();
extern "C" void ShimEnumerator_MoveNext_m15996_gshared ();
extern "C" void ShimEnumerator_get_Entry_m15997_gshared ();
extern "C" void ShimEnumerator_get_Key_m15998_gshared ();
extern "C" void ShimEnumerator_get_Value_m15999_gshared ();
extern "C" void ShimEnumerator_get_Current_m16000_gshared ();
extern "C" void EqualityComparer_1__ctor_m16001_gshared ();
extern "C" void EqualityComparer_1__cctor_m16002_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16003_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16004_gshared ();
extern "C" void EqualityComparer_1_get_Default_m16005_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m16006_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m16007_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m16008_gshared ();
void* RuntimeInvoker_Boolean_t273_SByte_t274_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m16009_gshared ();
extern "C" void DefaultComparer_GetHashCode_m16010_gshared ();
extern "C" void DefaultComparer_Equals_m16011_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16257_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16258_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16259_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16260_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16261_gshared ();
void* RuntimeInvoker_AnimatorClipInfo_t888 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m16941_gshared ();
extern "C" void Dictionary_2__ctor_m16943_gshared ();
extern "C" void Dictionary_2__ctor_m16945_gshared ();
extern "C" void Dictionary_2__ctor_m16947_gshared ();
extern "C" void Dictionary_2__ctor_m16949_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16951_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16953_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m16955_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16957_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16959_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m16961_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16963_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16965_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16967_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16969_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16971_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16973_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16975_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16977_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16979_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16981_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16983_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16985_gshared ();
extern "C" void Dictionary_2_get_Count_m16987_gshared ();
extern "C" void Dictionary_2_get_Item_m16989_gshared ();
extern "C" void Dictionary_2_set_Item_m16991_gshared ();
extern "C" void Dictionary_2_Init_m16993_gshared ();
extern "C" void Dictionary_2_InitArrays_m16995_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m16997_gshared ();
extern "C" void Dictionary_2_make_pair_m16999_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3059_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m17001_gshared ();
void* RuntimeInvoker_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m17003_gshared ();
void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m17005_gshared ();
extern "C" void Dictionary_2_Resize_m17007_gshared ();
extern "C" void Dictionary_2_Add_m17009_gshared ();
extern "C" void Dictionary_2_Clear_m17011_gshared ();
extern "C" void Dictionary_2_ContainsKey_m17013_gshared ();
extern "C" void Dictionary_2_ContainsValue_m17015_gshared ();
extern "C" void Dictionary_2_GetObjectData_m17017_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m17019_gshared ();
extern "C" void Dictionary_2_Remove_m17021_gshared ();
extern "C" void Dictionary_2_TryGetValue_m17023_gshared ();
void* RuntimeInvoker_Boolean_t273_Int32_t253_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m17025_gshared ();
extern "C" void Dictionary_2_ToTKey_m17028_gshared ();
extern "C" void Dictionary_2_ToTValue_m17030_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m17032_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m17035_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m17036_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17037_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17038_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17039_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17040_gshared ();
extern "C" void KeyValuePair_2__ctor_m17041_gshared ();
extern "C" void KeyValuePair_2_set_Key_m17043_gshared ();
extern "C" void KeyValuePair_2_set_Value_m17045_gshared ();
extern "C" void KeyCollection__ctor_m17047_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17048_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17049_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17050_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17051_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17052_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m17053_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17054_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17055_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17056_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m17057_gshared ();
extern "C" void KeyCollection_CopyTo_m17058_gshared ();
extern "C" void KeyCollection_GetEnumerator_m17059_gshared ();
void* RuntimeInvoker_Enumerator_t3062 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m17060_gshared ();
extern "C" void Enumerator__ctor_m17061_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17062_gshared ();
extern "C" void Enumerator_Dispose_m17063_gshared ();
extern "C" void Enumerator_MoveNext_m17064_gshared ();
extern "C" void Enumerator_get_Current_m17065_gshared ();
extern "C" void Enumerator__ctor_m17066_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17067_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17068_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17069_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17070_gshared ();
extern "C" void Enumerator_get_CurrentKey_m17073_gshared ();
extern "C" void Enumerator_get_CurrentValue_m17074_gshared ();
extern "C" void Enumerator_VerifyState_m17075_gshared ();
extern "C" void Enumerator_VerifyCurrent_m17076_gshared ();
extern "C" void Enumerator_Dispose_m17077_gshared ();
extern "C" void Transform_1__ctor_m17078_gshared ();
extern "C" void Transform_1_Invoke_m17079_gshared ();
extern "C" void Transform_1_BeginInvoke_m17080_gshared ();
void* RuntimeInvoker_Object_t_Int32_t253_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m17081_gshared ();
extern "C" void ValueCollection__ctor_m17082_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17083_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17084_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17085_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17086_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17087_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m17088_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17089_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17090_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17091_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m17092_gshared ();
extern "C" void ValueCollection_CopyTo_m17093_gshared ();
extern "C" void ValueCollection_get_Count_m17095_gshared ();
extern "C" void Enumerator__ctor_m17096_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17097_gshared ();
extern "C" void Enumerator_Dispose_m17098_gshared ();
extern "C" void Transform_1__ctor_m17101_gshared ();
extern "C" void Transform_1_Invoke_m17102_gshared ();
extern "C" void Transform_1_BeginInvoke_m17103_gshared ();
extern "C" void Transform_1_EndInvoke_m17104_gshared ();
extern "C" void Transform_1__ctor_m17105_gshared ();
extern "C" void Transform_1_Invoke_m17106_gshared ();
extern "C" void Transform_1_BeginInvoke_m17107_gshared ();
extern "C" void Transform_1_EndInvoke_m17108_gshared ();
extern "C" void Transform_1__ctor_m17109_gshared ();
extern "C" void Transform_1_Invoke_m17110_gshared ();
extern "C" void Transform_1_BeginInvoke_m17111_gshared ();
extern "C" void Transform_1_EndInvoke_m17112_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3059_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m17113_gshared ();
extern "C" void ShimEnumerator_MoveNext_m17114_gshared ();
extern "C" void ShimEnumerator_get_Entry_m17115_gshared ();
extern "C" void ShimEnumerator_get_Key_m17116_gshared ();
extern "C" void ShimEnumerator_get_Value_m17117_gshared ();
extern "C" void ShimEnumerator_get_Current_m17118_gshared ();
extern "C" void InternalEnumerator_1__ctor_m17259_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17260_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17261_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17262_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17263_gshared ();
void* RuntimeInvoker_RaycastHit2D_t665 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_Invoke_m17264_gshared ();
void* RuntimeInvoker_Int32_t253_RaycastHit_t24_RaycastHit_t24 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_BeginInvoke_m17265_gshared ();
void* RuntimeInvoker_Object_t_RaycastHit_t24_RaycastHit_t24_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m17266_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m17267_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m17268_gshared ();
extern "C" void UnityAction_1_Invoke_m17269_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m17270_gshared ();
void* RuntimeInvoker_Object_t_Color_t65_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m17271_gshared ();
extern "C" void InvokableCall_1__ctor_m17272_gshared ();
extern "C" void InvokableCall_1__ctor_m17273_gshared ();
extern "C" void InvokableCall_1_Invoke_m17274_gshared ();
extern "C" void InvokableCall_1_Find_m17275_gshared ();
extern "C" void List_1__ctor_m17671_gshared ();
extern "C" void List_1__cctor_m17672_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17673_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17674_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m17675_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m17676_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m17677_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m17678_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m17679_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m17680_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17681_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m17682_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m17683_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m17684_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m17685_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m17686_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m17687_gshared ();
extern "C" void List_1_Add_m17688_gshared ();
void* RuntimeInvoker_Void_t272_UIVertex_t556 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_GrowIfNeeded_m17689_gshared ();
extern "C" void List_1_AddCollection_m17690_gshared ();
extern "C" void List_1_AddEnumerable_m17691_gshared ();
extern "C" void List_1_AddRange_m17692_gshared ();
extern "C" void List_1_AsReadOnly_m17693_gshared ();
extern "C" void List_1_Clear_m17694_gshared ();
extern "C" void List_1_Contains_m17695_gshared ();
void* RuntimeInvoker_Boolean_t273_UIVertex_t556 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CopyTo_m17696_gshared ();
extern "C" void List_1_Find_m17697_gshared ();
void* RuntimeInvoker_UIVertex_t556_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m17698_gshared ();
extern "C" void List_1_GetIndex_m17699_gshared ();
extern "C" void List_1_GetEnumerator_m17700_gshared ();
void* RuntimeInvoker_Enumerator_t3101 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m17701_gshared ();
void* RuntimeInvoker_Int32_t253_UIVertex_t556 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_Shift_m17702_gshared ();
extern "C" void List_1_CheckIndex_m17703_gshared ();
extern "C" void List_1_Insert_m17704_gshared ();
void* RuntimeInvoker_Void_t272_Int32_t253_UIVertex_t556 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckCollection_m17705_gshared ();
extern "C" void List_1_Remove_m17706_gshared ();
extern "C" void List_1_RemoveAll_m17707_gshared ();
extern "C" void List_1_RemoveAt_m17708_gshared ();
extern "C" void List_1_Reverse_m17709_gshared ();
extern "C" void List_1_Sort_m17710_gshared ();
extern "C" void List_1_Sort_m17711_gshared ();
extern "C" void List_1_TrimExcess_m17712_gshared ();
extern "C" void List_1_get_Count_m17713_gshared ();
extern "C" void List_1_get_Item_m17714_gshared ();
void* RuntimeInvoker_UIVertex_t556_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_set_Item_m17715_gshared ();
extern "C" void Enumerator__ctor_m17650_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17651_gshared ();
extern "C" void Enumerator_Dispose_m17652_gshared ();
extern "C" void Enumerator_VerifyState_m17653_gshared ();
extern "C" void Enumerator_MoveNext_m17654_gshared ();
extern "C" void Enumerator_get_Current_m17655_gshared ();
void* RuntimeInvoker_UIVertex_t556 (const MethodInfo* method, void* obj, void** args);
extern "C" void ReadOnlyCollection_1__ctor_m17616_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17617_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17618_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17619_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17620_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17621_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17622_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17623_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17624_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17625_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17626_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m17627_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m17628_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m17629_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17630_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m17631_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m17632_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17633_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17634_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17635_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17636_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17637_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m17638_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m17639_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m17640_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m17641_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m17642_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m17643_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m17644_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m17645_gshared ();
extern "C" void Collection_1__ctor_m17719_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17720_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17721_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m17722_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m17723_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m17724_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m17725_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m17726_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m17727_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m17728_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m17729_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m17730_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m17731_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m17732_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m17733_gshared ();
extern "C" void Collection_1_Add_m17734_gshared ();
extern "C" void Collection_1_Clear_m17735_gshared ();
extern "C" void Collection_1_ClearItems_m17736_gshared ();
extern "C" void Collection_1_Contains_m17737_gshared ();
extern "C" void Collection_1_CopyTo_m17738_gshared ();
extern "C" void Collection_1_GetEnumerator_m17739_gshared ();
extern "C" void Collection_1_IndexOf_m17740_gshared ();
extern "C" void Collection_1_Insert_m17741_gshared ();
extern "C" void Collection_1_InsertItem_m17742_gshared ();
extern "C" void Collection_1_Remove_m17743_gshared ();
extern "C" void Collection_1_RemoveAt_m17744_gshared ();
extern "C" void Collection_1_RemoveItem_m17745_gshared ();
extern "C" void Collection_1_get_Count_m17746_gshared ();
extern "C" void Collection_1_get_Item_m17747_gshared ();
extern "C" void Collection_1_set_Item_m17748_gshared ();
extern "C" void Collection_1_SetItem_m17749_gshared ();
extern "C" void Collection_1_IsValidItem_m17750_gshared ();
extern "C" void Collection_1_ConvertItem_m17751_gshared ();
extern "C" void Collection_1_CheckWritable_m17752_gshared ();
extern "C" void Collection_1_IsSynchronized_m17753_gshared ();
extern "C" void Collection_1_IsFixedSize_m17754_gshared ();
extern "C" void EqualityComparer_1__ctor_m17755_gshared ();
extern "C" void EqualityComparer_1__cctor_m17756_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17757_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17758_gshared ();
extern "C" void EqualityComparer_1_get_Default_m17759_gshared ();
extern "C" void DefaultComparer__ctor_m17760_gshared ();
extern "C" void DefaultComparer_GetHashCode_m17761_gshared ();
extern "C" void DefaultComparer_Equals_m17762_gshared ();
void* RuntimeInvoker_Boolean_t273_UIVertex_t556_UIVertex_t556 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m17646_gshared ();
extern "C" void Predicate_1_Invoke_m17647_gshared ();
extern "C" void Predicate_1_BeginInvoke_m17648_gshared ();
void* RuntimeInvoker_Object_t_UIVertex_t556_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m17649_gshared ();
extern "C" void Comparer_1__ctor_m17763_gshared ();
extern "C" void Comparer_1__cctor_m17764_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m17765_gshared ();
extern "C" void Comparer_1_get_Default_m17766_gshared ();
extern "C" void DefaultComparer__ctor_m17767_gshared ();
extern "C" void DefaultComparer_Compare_m17768_gshared ();
void* RuntimeInvoker_Int32_t253_UIVertex_t556_UIVertex_t556 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m17656_gshared ();
extern "C" void Comparison_1_Invoke_m17657_gshared ();
extern "C" void Comparison_1_BeginInvoke_m17658_gshared ();
void* RuntimeInvoker_Object_t_UIVertex_t556_UIVertex_t556_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m17659_gshared ();
extern "C" void TweenRunner_1_Start_m17769_gshared ();
void* RuntimeInvoker_Object_t_ColorTween_t506 (const MethodInfo* method, void* obj, void** args);
extern "C" void U3CStartU3Ec__Iterator0__ctor_m17770_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17771_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17772_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_MoveNext_m17773_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m17774_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Reset_m17775_gshared ();
extern "C" void InternalEnumerator_1__ctor_m18231_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18232_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18233_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18234_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18235_gshared ();
void* RuntimeInvoker_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18236_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18237_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18238_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18239_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18240_gshared ();
void* RuntimeInvoker_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_GetDelegate_m18248_gshared ();
extern "C" void UnityAction_1_Invoke_m18249_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m18250_gshared ();
void* RuntimeInvoker_Object_t_Single_t254_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m18251_gshared ();
extern "C" void InvokableCall_1__ctor_m18252_gshared ();
extern "C" void InvokableCall_1__ctor_m18253_gshared ();
extern "C" void InvokableCall_1_Invoke_m18254_gshared ();
extern "C" void InvokableCall_1_Find_m18255_gshared ();
extern "C" void UnityEvent_1_AddListener_m18256_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m18257_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m18258_gshared ();
extern "C" void UnityAction_1__ctor_m18259_gshared ();
extern "C" void UnityAction_1_Invoke_m18260_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m18261_gshared ();
extern "C" void UnityAction_1_EndInvoke_m18262_gshared ();
extern "C" void InvokableCall_1__ctor_m18263_gshared ();
extern "C" void InvokableCall_1__ctor_m18264_gshared ();
extern "C" void InvokableCall_1_Invoke_m18265_gshared ();
extern "C" void InvokableCall_1_Find_m18266_gshared ();
extern "C" void UnityEvent_1_AddListener_m18548_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m18550_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m18554_gshared ();
extern "C" void UnityAction_1__ctor_m18556_gshared ();
extern "C" void UnityAction_1_Invoke_m18557_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m18558_gshared ();
void* RuntimeInvoker_Object_t_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m18559_gshared ();
extern "C" void InvokableCall_1__ctor_m18560_gshared ();
extern "C" void InvokableCall_1__ctor_m18561_gshared ();
extern "C" void InvokableCall_1_Invoke_m18562_gshared ();
extern "C" void InvokableCall_1_Find_m18563_gshared ();
extern "C" void Func_2_Invoke_m18661_gshared ();
extern "C" void Func_2_BeginInvoke_m18663_gshared ();
extern "C" void Func_2_EndInvoke_m18665_gshared ();
extern "C" void Func_2_Invoke_m18775_gshared ();
void* RuntimeInvoker_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_BeginInvoke_m18777_gshared ();
extern "C" void Func_2_EndInvoke_m18779_gshared ();
extern "C" void Action_1__ctor_m18817_gshared ();
extern "C" void Action_1_Invoke_m18819_gshared ();
extern "C" void Action_1_BeginInvoke_m18821_gshared ();
extern "C" void Action_1_EndInvoke_m18823_gshared ();
extern "C" void InternalEnumerator_1__ctor_m18960_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18961_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18962_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18963_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18964_gshared ();
void* RuntimeInvoker_GcAchievementData_t958 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18970_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18971_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18972_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18973_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18974_gshared ();
void* RuntimeInvoker_GcScoreData_t959 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19379_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19380_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19381_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19382_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19383_gshared ();
void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m19569_gshared ();
extern "C" void List_1__ctor_m19570_gshared ();
extern "C" void List_1__cctor_m19571_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19572_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m19573_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m19574_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m19575_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m19576_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m19577_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m19578_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m19579_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19580_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m19581_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m19582_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m19583_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m19584_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m19585_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m19586_gshared ();
extern "C" void List_1_Add_m19587_gshared ();
extern "C" void List_1_GrowIfNeeded_m19588_gshared ();
extern "C" void List_1_AddCollection_m19589_gshared ();
extern "C" void List_1_AddEnumerable_m19590_gshared ();
extern "C" void List_1_AddRange_m19591_gshared ();
extern "C" void List_1_AsReadOnly_m19592_gshared ();
extern "C" void List_1_Clear_m19593_gshared ();
extern "C" void List_1_Contains_m19594_gshared ();
extern "C" void List_1_CopyTo_m19595_gshared ();
extern "C" void List_1_Find_m19596_gshared ();
void* RuntimeInvoker_UICharInfo_t689_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m19597_gshared ();
extern "C" void List_1_GetIndex_m19598_gshared ();
extern "C" void List_1_GetEnumerator_m19599_gshared ();
void* RuntimeInvoker_Enumerator_t3234 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m19600_gshared ();
extern "C" void List_1_Shift_m19601_gshared ();
extern "C" void List_1_CheckIndex_m19602_gshared ();
extern "C" void List_1_Insert_m19603_gshared ();
extern "C" void List_1_CheckCollection_m19604_gshared ();
extern "C" void List_1_Remove_m19605_gshared ();
extern "C" void List_1_RemoveAll_m19606_gshared ();
extern "C" void List_1_RemoveAt_m19607_gshared ();
extern "C" void List_1_Reverse_m19608_gshared ();
extern "C" void List_1_Sort_m19609_gshared ();
extern "C" void List_1_Sort_m19610_gshared ();
extern "C" void List_1_ToArray_m19611_gshared ();
extern "C" void List_1_TrimExcess_m19612_gshared ();
extern "C" void List_1_get_Capacity_m19613_gshared ();
extern "C" void List_1_set_Capacity_m19614_gshared ();
extern "C" void List_1_get_Count_m19615_gshared ();
extern "C" void List_1_get_Item_m19616_gshared ();
extern "C" void List_1_set_Item_m19617_gshared ();
extern "C" void Enumerator__ctor_m19618_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m19619_gshared ();
extern "C" void Enumerator_Dispose_m19620_gshared ();
extern "C" void Enumerator_VerifyState_m19621_gshared ();
extern "C" void Enumerator_MoveNext_m19622_gshared ();
extern "C" void Enumerator_get_Current_m19623_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m19624_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19625_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19626_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19627_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19628_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19629_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19630_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19631_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19632_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19633_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19634_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m19635_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m19636_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m19637_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19638_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m19639_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m19640_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19641_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19642_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19643_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19644_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19645_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m19646_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m19647_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m19648_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m19649_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m19650_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m19651_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m19652_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m19653_gshared ();
extern "C" void Collection_1__ctor_m19654_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19655_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m19656_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m19657_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m19658_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m19659_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m19660_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m19661_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m19662_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m19663_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m19664_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m19665_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m19666_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m19667_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m19668_gshared ();
extern "C" void Collection_1_Add_m19669_gshared ();
extern "C" void Collection_1_Clear_m19670_gshared ();
extern "C" void Collection_1_ClearItems_m19671_gshared ();
extern "C" void Collection_1_Contains_m19672_gshared ();
extern "C" void Collection_1_CopyTo_m19673_gshared ();
extern "C" void Collection_1_GetEnumerator_m19674_gshared ();
extern "C" void Collection_1_IndexOf_m19675_gshared ();
extern "C" void Collection_1_Insert_m19676_gshared ();
extern "C" void Collection_1_InsertItem_m19677_gshared ();
extern "C" void Collection_1_Remove_m19678_gshared ();
extern "C" void Collection_1_RemoveAt_m19679_gshared ();
extern "C" void Collection_1_RemoveItem_m19680_gshared ();
extern "C" void Collection_1_get_Count_m19681_gshared ();
extern "C" void Collection_1_get_Item_m19682_gshared ();
extern "C" void Collection_1_set_Item_m19683_gshared ();
extern "C" void Collection_1_SetItem_m19684_gshared ();
extern "C" void Collection_1_IsValidItem_m19685_gshared ();
extern "C" void Collection_1_ConvertItem_m19686_gshared ();
extern "C" void Collection_1_CheckWritable_m19687_gshared ();
extern "C" void Collection_1_IsSynchronized_m19688_gshared ();
extern "C" void Collection_1_IsFixedSize_m19689_gshared ();
extern "C" void EqualityComparer_1__ctor_m19690_gshared ();
extern "C" void EqualityComparer_1__cctor_m19691_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m19692_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m19693_gshared ();
extern "C" void EqualityComparer_1_get_Default_m19694_gshared ();
extern "C" void DefaultComparer__ctor_m19695_gshared ();
extern "C" void DefaultComparer_GetHashCode_m19696_gshared ();
extern "C" void DefaultComparer_Equals_m19697_gshared ();
void* RuntimeInvoker_Boolean_t273_UICharInfo_t689_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m19698_gshared ();
extern "C" void Predicate_1_Invoke_m19699_gshared ();
extern "C" void Predicate_1_BeginInvoke_m19700_gshared ();
void* RuntimeInvoker_Object_t_UICharInfo_t689_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m19701_gshared ();
extern "C" void Comparer_1__ctor_m19702_gshared ();
extern "C" void Comparer_1__cctor_m19703_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m19704_gshared ();
extern "C" void Comparer_1_get_Default_m19705_gshared ();
extern "C" void DefaultComparer__ctor_m19706_gshared ();
extern "C" void DefaultComparer_Compare_m19707_gshared ();
void* RuntimeInvoker_Int32_t253_UICharInfo_t689_UICharInfo_t689 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m19708_gshared ();
extern "C" void Comparison_1_Invoke_m19709_gshared ();
extern "C" void Comparison_1_BeginInvoke_m19710_gshared ();
void* RuntimeInvoker_Object_t_UICharInfo_t689_UICharInfo_t689_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m19711_gshared ();
extern "C" void List_1__ctor_m19712_gshared ();
extern "C" void List_1__ctor_m19713_gshared ();
extern "C" void List_1__cctor_m19714_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19715_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m19716_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m19717_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m19718_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m19719_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m19720_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m19721_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m19722_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19723_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m19724_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m19725_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m19726_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m19727_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m19728_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m19729_gshared ();
extern "C" void List_1_Add_m19730_gshared ();
extern "C" void List_1_GrowIfNeeded_m19731_gshared ();
extern "C" void List_1_AddCollection_m19732_gshared ();
extern "C" void List_1_AddEnumerable_m19733_gshared ();
extern "C" void List_1_AddRange_m19734_gshared ();
extern "C" void List_1_AsReadOnly_m19735_gshared ();
extern "C" void List_1_Clear_m19736_gshared ();
extern "C" void List_1_Contains_m19737_gshared ();
extern "C" void List_1_CopyTo_m19738_gshared ();
extern "C" void List_1_Find_m19739_gshared ();
void* RuntimeInvoker_UILineInfo_t687_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m19740_gshared ();
extern "C" void List_1_GetIndex_m19741_gshared ();
extern "C" void List_1_GetEnumerator_m19742_gshared ();
void* RuntimeInvoker_Enumerator_t3243 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m19743_gshared ();
extern "C" void List_1_Shift_m19744_gshared ();
extern "C" void List_1_CheckIndex_m19745_gshared ();
extern "C" void List_1_Insert_m19746_gshared ();
extern "C" void List_1_CheckCollection_m19747_gshared ();
extern "C" void List_1_Remove_m19748_gshared ();
extern "C" void List_1_RemoveAll_m19749_gshared ();
extern "C" void List_1_RemoveAt_m19750_gshared ();
extern "C" void List_1_Reverse_m19751_gshared ();
extern "C" void List_1_Sort_m19752_gshared ();
extern "C" void List_1_Sort_m19753_gshared ();
extern "C" void List_1_ToArray_m19754_gshared ();
extern "C" void List_1_TrimExcess_m19755_gshared ();
extern "C" void List_1_get_Capacity_m19756_gshared ();
extern "C" void List_1_set_Capacity_m19757_gshared ();
extern "C" void List_1_get_Count_m19758_gshared ();
extern "C" void List_1_get_Item_m19759_gshared ();
extern "C" void List_1_set_Item_m19760_gshared ();
extern "C" void Enumerator__ctor_m19761_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m19762_gshared ();
extern "C" void Enumerator_Dispose_m19763_gshared ();
extern "C" void Enumerator_VerifyState_m19764_gshared ();
extern "C" void Enumerator_MoveNext_m19765_gshared ();
extern "C" void Enumerator_get_Current_m19766_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m19767_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19768_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19769_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19770_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19771_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19772_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19773_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19774_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19775_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19776_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19777_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m19778_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m19779_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m19780_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19781_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m19782_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m19783_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19784_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19785_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19786_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19787_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19788_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m19789_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m19790_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m19791_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m19792_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m19793_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m19794_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m19795_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m19796_gshared ();
extern "C" void Collection_1__ctor_m19797_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19798_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m19799_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m19800_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m19801_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m19802_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m19803_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m19804_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m19805_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m19806_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m19807_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m19808_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m19809_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m19810_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m19811_gshared ();
extern "C" void Collection_1_Add_m19812_gshared ();
extern "C" void Collection_1_Clear_m19813_gshared ();
extern "C" void Collection_1_ClearItems_m19814_gshared ();
extern "C" void Collection_1_Contains_m19815_gshared ();
extern "C" void Collection_1_CopyTo_m19816_gshared ();
extern "C" void Collection_1_GetEnumerator_m19817_gshared ();
extern "C" void Collection_1_IndexOf_m19818_gshared ();
extern "C" void Collection_1_Insert_m19819_gshared ();
extern "C" void Collection_1_InsertItem_m19820_gshared ();
extern "C" void Collection_1_Remove_m19821_gshared ();
extern "C" void Collection_1_RemoveAt_m19822_gshared ();
extern "C" void Collection_1_RemoveItem_m19823_gshared ();
extern "C" void Collection_1_get_Count_m19824_gshared ();
extern "C" void Collection_1_get_Item_m19825_gshared ();
extern "C" void Collection_1_set_Item_m19826_gshared ();
extern "C" void Collection_1_SetItem_m19827_gshared ();
extern "C" void Collection_1_IsValidItem_m19828_gshared ();
extern "C" void Collection_1_ConvertItem_m19829_gshared ();
extern "C" void Collection_1_CheckWritable_m19830_gshared ();
extern "C" void Collection_1_IsSynchronized_m19831_gshared ();
extern "C" void Collection_1_IsFixedSize_m19832_gshared ();
extern "C" void EqualityComparer_1__ctor_m19833_gshared ();
extern "C" void EqualityComparer_1__cctor_m19834_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m19835_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m19836_gshared ();
extern "C" void EqualityComparer_1_get_Default_m19837_gshared ();
extern "C" void DefaultComparer__ctor_m19838_gshared ();
extern "C" void DefaultComparer_GetHashCode_m19839_gshared ();
extern "C" void DefaultComparer_Equals_m19840_gshared ();
void* RuntimeInvoker_Boolean_t273_UILineInfo_t687_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m19841_gshared ();
extern "C" void Predicate_1_Invoke_m19842_gshared ();
extern "C" void Predicate_1_BeginInvoke_m19843_gshared ();
void* RuntimeInvoker_Object_t_UILineInfo_t687_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m19844_gshared ();
extern "C" void Comparer_1__ctor_m19845_gshared ();
extern "C" void Comparer_1__cctor_m19846_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m19847_gshared ();
extern "C" void Comparer_1_get_Default_m19848_gshared ();
extern "C" void DefaultComparer__ctor_m19849_gshared ();
extern "C" void DefaultComparer_Compare_m19850_gshared ();
void* RuntimeInvoker_Int32_t253_UILineInfo_t687_UILineInfo_t687 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m19851_gshared ();
extern "C" void Comparison_1_Invoke_m19852_gshared ();
extern "C" void Comparison_1_BeginInvoke_m19853_gshared ();
void* RuntimeInvoker_Object_t_UILineInfo_t687_UILineInfo_t687_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m19854_gshared ();
extern "C" void Dictionary_2__ctor_m19856_gshared ();
extern "C" void Dictionary_2__ctor_m19858_gshared ();
extern "C" void Dictionary_2__ctor_m19860_gshared ();
extern "C" void Dictionary_2__ctor_m19862_gshared ();
extern "C" void Dictionary_2__ctor_m19864_gshared ();
extern "C" void Dictionary_2__ctor_m19866_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m19868_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m19870_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m19872_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m19874_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m19876_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m19878_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m19880_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19882_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19884_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19886_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19888_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19890_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19892_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19894_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m19896_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19898_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19900_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19902_gshared ();
extern "C" void Dictionary_2_get_Count_m19904_gshared ();
extern "C" void Dictionary_2_get_Item_m19906_gshared ();
void* RuntimeInvoker_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_set_Item_m19908_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m19910_gshared ();
extern "C" void Dictionary_2_InitArrays_m19912_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m19914_gshared ();
extern "C" void Dictionary_2_make_pair_m19916_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3255_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m19918_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m19920_gshared ();
void* RuntimeInvoker_Int64_t1071_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m19922_gshared ();
extern "C" void Dictionary_2_Resize_m19924_gshared ();
extern "C" void Dictionary_2_Add_m19926_gshared ();
extern "C" void Dictionary_2_Clear_m19928_gshared ();
extern "C" void Dictionary_2_ContainsKey_m19930_gshared ();
extern "C" void Dictionary_2_ContainsValue_m19932_gshared ();
extern "C" void Dictionary_2_GetObjectData_m19934_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m19936_gshared ();
extern "C" void Dictionary_2_Remove_m19938_gshared ();
extern "C" void Dictionary_2_TryGetValue_m19940_gshared ();
void* RuntimeInvoker_Boolean_t273_Object_t_Int64U26_t2404 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m19942_gshared ();
extern "C" void Dictionary_2_get_Values_m19944_gshared ();
extern "C" void Dictionary_2_ToTKey_m19946_gshared ();
extern "C" void Dictionary_2_ToTValue_m19948_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m19950_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m19952_gshared ();
void* RuntimeInvoker_Enumerator_t3260 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m19954_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m19955_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19956_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19957_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19958_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19959_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3255 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m19960_gshared ();
extern "C" void KeyValuePair_2_get_Key_m19961_gshared ();
extern "C" void KeyValuePair_2_set_Key_m19962_gshared ();
extern "C" void KeyValuePair_2_get_Value_m19963_gshared ();
void* RuntimeInvoker_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_set_Value_m19964_gshared ();
extern "C" void KeyValuePair_2_ToString_m19965_gshared ();
extern "C" void InternalEnumerator_1__ctor_m19966_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19967_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19968_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19969_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19970_gshared ();
extern "C" void KeyCollection__ctor_m19971_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m19972_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m19973_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m19974_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m19975_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m19976_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m19977_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m19978_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m19979_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m19980_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m19981_gshared ();
extern "C" void KeyCollection_CopyTo_m19982_gshared ();
extern "C" void KeyCollection_GetEnumerator_m19983_gshared ();
void* RuntimeInvoker_Enumerator_t3259 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m19984_gshared ();
extern "C" void Enumerator__ctor_m19985_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m19986_gshared ();
extern "C" void Enumerator_Dispose_m19987_gshared ();
extern "C" void Enumerator_MoveNext_m19988_gshared ();
extern "C" void Enumerator_get_Current_m19989_gshared ();
extern "C" void Enumerator__ctor_m19990_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m19991_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19992_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19993_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19994_gshared ();
extern "C" void Enumerator_MoveNext_m19995_gshared ();
extern "C" void Enumerator_get_Current_m19996_gshared ();
extern "C" void Enumerator_get_CurrentKey_m19997_gshared ();
extern "C" void Enumerator_get_CurrentValue_m19998_gshared ();
extern "C" void Enumerator_VerifyState_m19999_gshared ();
extern "C" void Enumerator_VerifyCurrent_m20000_gshared ();
extern "C" void Enumerator_Dispose_m20001_gshared ();
extern "C" void Transform_1__ctor_m20002_gshared ();
extern "C" void Transform_1_Invoke_m20003_gshared ();
extern "C" void Transform_1_BeginInvoke_m20004_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int64_t1071_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m20005_gshared ();
extern "C" void ValueCollection__ctor_m20006_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20007_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20008_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20009_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20010_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20011_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m20012_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20013_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20014_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20015_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m20016_gshared ();
extern "C" void ValueCollection_CopyTo_m20017_gshared ();
extern "C" void ValueCollection_GetEnumerator_m20018_gshared ();
void* RuntimeInvoker_Enumerator_t3263 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m20019_gshared ();
extern "C" void Enumerator__ctor_m20020_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20021_gshared ();
extern "C" void Enumerator_Dispose_m20022_gshared ();
extern "C" void Enumerator_MoveNext_m20023_gshared ();
extern "C" void Enumerator_get_Current_m20024_gshared ();
extern "C" void Transform_1__ctor_m20025_gshared ();
extern "C" void Transform_1_Invoke_m20026_gshared ();
extern "C" void Transform_1_BeginInvoke_m20027_gshared ();
extern "C" void Transform_1_EndInvoke_m20028_gshared ();
extern "C" void Transform_1__ctor_m20029_gshared ();
extern "C" void Transform_1_Invoke_m20030_gshared ();
extern "C" void Transform_1_BeginInvoke_m20031_gshared ();
extern "C" void Transform_1_EndInvoke_m20032_gshared ();
extern "C" void Transform_1__ctor_m20033_gshared ();
extern "C" void Transform_1_Invoke_m20034_gshared ();
extern "C" void Transform_1_BeginInvoke_m20035_gshared ();
extern "C" void Transform_1_EndInvoke_m20036_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3255_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m20037_gshared ();
extern "C" void ShimEnumerator_MoveNext_m20038_gshared ();
extern "C" void ShimEnumerator_get_Entry_m20039_gshared ();
extern "C" void ShimEnumerator_get_Key_m20040_gshared ();
extern "C" void ShimEnumerator_get_Value_m20041_gshared ();
extern "C" void ShimEnumerator_get_Current_m20042_gshared ();
extern "C" void EqualityComparer_1__ctor_m20043_gshared ();
extern "C" void EqualityComparer_1__cctor_m20044_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20045_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20046_gshared ();
extern "C" void EqualityComparer_1_get_Default_m20047_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m20048_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m20049_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m20050_gshared ();
void* RuntimeInvoker_Boolean_t273_Int64_t1071_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m20051_gshared ();
extern "C" void DefaultComparer_GetHashCode_m20052_gshared ();
extern "C" void DefaultComparer_Equals_m20053_gshared ();
extern "C" void Dictionary_2__ctor_m20294_gshared ();
extern "C" void Dictionary_2__ctor_m20296_gshared ();
extern "C" void Dictionary_2__ctor_m20298_gshared ();
extern "C" void Dictionary_2__ctor_m20300_gshared ();
extern "C" void Dictionary_2__ctor_m20302_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20304_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20306_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m20308_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m20310_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m20312_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m20314_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m20316_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20318_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20320_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20322_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20324_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20326_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20328_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20330_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m20332_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20334_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20336_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20338_gshared ();
extern "C" void Dictionary_2_get_Count_m20340_gshared ();
extern "C" void Dictionary_2_get_Item_m20342_gshared ();
void* RuntimeInvoker_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_set_Item_m20344_gshared ();
void* RuntimeInvoker_Void_t272_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m20346_gshared ();
extern "C" void Dictionary_2_InitArrays_m20348_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m20350_gshared ();
extern "C" void Dictionary_2_make_pair_m20352_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3293_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m20354_gshared ();
void* RuntimeInvoker_UInt64_t1074_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m20356_gshared ();
void* RuntimeInvoker_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m20358_gshared ();
extern "C" void Dictionary_2_Resize_m20360_gshared ();
extern "C" void Dictionary_2_Add_m20362_gshared ();
extern "C" void Dictionary_2_Clear_m20364_gshared ();
extern "C" void Dictionary_2_ContainsKey_m20366_gshared ();
extern "C" void Dictionary_2_ContainsValue_m20368_gshared ();
extern "C" void Dictionary_2_GetObjectData_m20370_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m20372_gshared ();
extern "C" void Dictionary_2_Remove_m20374_gshared ();
extern "C" void Dictionary_2_TryGetValue_m20376_gshared ();
void* RuntimeInvoker_Boolean_t273_Int64_t1071_ObjectU26_t1197 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m20378_gshared ();
extern "C" void Dictionary_2_get_Values_m20380_gshared ();
extern "C" void Dictionary_2_ToTKey_m20382_gshared ();
void* RuntimeInvoker_UInt64_t1074_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_ToTValue_m20384_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m20386_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m20388_gshared ();
void* RuntimeInvoker_Enumerator_t3298 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m20390_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m20391_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20392_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m20393_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m20394_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m20395_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3293 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m20396_gshared ();
extern "C" void KeyValuePair_2_get_Key_m20397_gshared ();
void* RuntimeInvoker_UInt64_t1074 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_set_Key_m20398_gshared ();
extern "C" void KeyValuePair_2_get_Value_m20399_gshared ();
extern "C" void KeyValuePair_2_set_Value_m20400_gshared ();
extern "C" void KeyValuePair_2_ToString_m20401_gshared ();
extern "C" void InternalEnumerator_1__ctor_m20402_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20403_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m20404_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m20405_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m20406_gshared ();
extern "C" void KeyCollection__ctor_m20407_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m20408_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m20409_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m20410_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m20411_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m20412_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m20413_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m20414_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m20415_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m20416_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m20417_gshared ();
extern "C" void KeyCollection_CopyTo_m20418_gshared ();
extern "C" void KeyCollection_GetEnumerator_m20419_gshared ();
void* RuntimeInvoker_Enumerator_t3297 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m20420_gshared ();
extern "C" void Enumerator__ctor_m20421_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20422_gshared ();
extern "C" void Enumerator_Dispose_m20423_gshared ();
extern "C" void Enumerator_MoveNext_m20424_gshared ();
extern "C" void Enumerator_get_Current_m20425_gshared ();
extern "C" void Enumerator__ctor_m20426_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20427_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20428_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20429_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20430_gshared ();
extern "C" void Enumerator_MoveNext_m20431_gshared ();
extern "C" void Enumerator_get_Current_m20432_gshared ();
extern "C" void Enumerator_get_CurrentKey_m20433_gshared ();
extern "C" void Enumerator_get_CurrentValue_m20434_gshared ();
extern "C" void Enumerator_VerifyState_m20435_gshared ();
extern "C" void Enumerator_VerifyCurrent_m20436_gshared ();
extern "C" void Enumerator_Dispose_m20437_gshared ();
extern "C" void Transform_1__ctor_m20438_gshared ();
extern "C" void Transform_1_Invoke_m20439_gshared ();
extern "C" void Transform_1_BeginInvoke_m20440_gshared ();
void* RuntimeInvoker_Object_t_Int64_t1071_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m20441_gshared ();
extern "C" void ValueCollection__ctor_m20442_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20443_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20444_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20445_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20446_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20447_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m20448_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20449_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20450_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20451_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m20452_gshared ();
extern "C" void ValueCollection_CopyTo_m20453_gshared ();
extern "C" void ValueCollection_GetEnumerator_m20454_gshared ();
void* RuntimeInvoker_Enumerator_t3301 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m20455_gshared ();
extern "C" void Enumerator__ctor_m20456_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20457_gshared ();
extern "C" void Enumerator_Dispose_m20458_gshared ();
extern "C" void Enumerator_MoveNext_m20459_gshared ();
extern "C" void Enumerator_get_Current_m20460_gshared ();
extern "C" void Transform_1__ctor_m20461_gshared ();
extern "C" void Transform_1_Invoke_m20462_gshared ();
extern "C" void Transform_1_BeginInvoke_m20463_gshared ();
extern "C" void Transform_1_EndInvoke_m20464_gshared ();
extern "C" void Transform_1__ctor_m20465_gshared ();
extern "C" void Transform_1_Invoke_m20466_gshared ();
extern "C" void Transform_1_BeginInvoke_m20467_gshared ();
extern "C" void Transform_1_EndInvoke_m20468_gshared ();
extern "C" void Transform_1__ctor_m20469_gshared ();
extern "C" void Transform_1_Invoke_m20470_gshared ();
extern "C" void Transform_1_BeginInvoke_m20471_gshared ();
extern "C" void Transform_1_EndInvoke_m20472_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3293_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m20473_gshared ();
extern "C" void ShimEnumerator_MoveNext_m20474_gshared ();
extern "C" void ShimEnumerator_get_Entry_m20475_gshared ();
extern "C" void ShimEnumerator_get_Key_m20476_gshared ();
extern "C" void ShimEnumerator_get_Value_m20477_gshared ();
extern "C" void ShimEnumerator_get_Current_m20478_gshared ();
extern "C" void EqualityComparer_1__ctor_m20479_gshared ();
extern "C" void EqualityComparer_1__cctor_m20480_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20481_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20482_gshared ();
extern "C" void EqualityComparer_1_get_Default_m20483_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m20484_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m20485_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m20486_gshared ();
extern "C" void DefaultComparer__ctor_m20487_gshared ();
extern "C" void DefaultComparer_GetHashCode_m20488_gshared ();
extern "C" void DefaultComparer_Equals_m20489_gshared ();
extern "C" void InternalEnumerator_1__ctor_m20664_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20665_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m20666_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m20667_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m20668_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3314 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m20669_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_get_Key_m20670_gshared ();
extern "C" void KeyValuePair_2_set_Key_m20671_gshared ();
extern "C" void KeyValuePair_2_get_Value_m20672_gshared ();
extern "C" void KeyValuePair_2_set_Value_m20673_gshared ();
extern "C" void KeyValuePair_2_ToString_m20674_gshared ();
extern "C" void Dictionary_2__ctor_m21028_gshared ();
extern "C" void Dictionary_2__ctor_m21030_gshared ();
extern "C" void Dictionary_2__ctor_m21032_gshared ();
extern "C" void Dictionary_2__ctor_m21034_gshared ();
extern "C" void Dictionary_2__ctor_m21036_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21038_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21040_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m21042_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m21044_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m21046_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m21048_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m21050_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21052_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21054_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21056_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21058_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21060_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21062_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21064_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m21066_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21068_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21070_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21072_gshared ();
extern "C" void Dictionary_2_get_Count_m21074_gshared ();
extern "C" void Dictionary_2_get_Item_m21076_gshared ();
extern "C" void Dictionary_2_set_Item_m21078_gshared ();
extern "C" void Dictionary_2_Init_m21080_gshared ();
extern "C" void Dictionary_2_InitArrays_m21082_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m21084_gshared ();
extern "C" void Dictionary_2_make_pair_m21086_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3314_Object_t_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m21088_gshared ();
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m21090_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2815_Object_t_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m21092_gshared ();
extern "C" void Dictionary_2_Resize_m21094_gshared ();
extern "C" void Dictionary_2_Add_m21096_gshared ();
extern "C" void Dictionary_2_Clear_m21098_gshared ();
extern "C" void Dictionary_2_ContainsKey_m21100_gshared ();
extern "C" void Dictionary_2_ContainsValue_m21102_gshared ();
extern "C" void Dictionary_2_GetObjectData_m21104_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m21106_gshared ();
extern "C" void Dictionary_2_Remove_m21108_gshared ();
extern "C" void Dictionary_2_TryGetValue_m21110_gshared ();
void* RuntimeInvoker_Boolean_t273_Object_t_KeyValuePair_2U26_t3962 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m21112_gshared ();
extern "C" void Dictionary_2_get_Values_m21114_gshared ();
extern "C" void Dictionary_2_ToTKey_m21116_gshared ();
extern "C" void Dictionary_2_ToTValue_m21118_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m21120_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m21122_gshared ();
void* RuntimeInvoker_Enumerator_t3343 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m21124_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection__ctor_m21125_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21126_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21127_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21128_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21129_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21130_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m21131_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21132_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21133_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21134_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m21135_gshared ();
extern "C" void KeyCollection_CopyTo_m21136_gshared ();
extern "C" void KeyCollection_GetEnumerator_m21137_gshared ();
void* RuntimeInvoker_Enumerator_t3342 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m21138_gshared ();
extern "C" void Enumerator__ctor_m21139_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21140_gshared ();
extern "C" void Enumerator_Dispose_m21141_gshared ();
extern "C" void Enumerator_MoveNext_m21142_gshared ();
extern "C" void Enumerator_get_Current_m21143_gshared ();
extern "C" void Enumerator__ctor_m21144_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21145_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21146_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21147_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21148_gshared ();
extern "C" void Enumerator_MoveNext_m21149_gshared ();
extern "C" void Enumerator_get_Current_m21150_gshared ();
extern "C" void Enumerator_get_CurrentKey_m21151_gshared ();
extern "C" void Enumerator_get_CurrentValue_m21152_gshared ();
extern "C" void Enumerator_VerifyState_m21153_gshared ();
extern "C" void Enumerator_VerifyCurrent_m21154_gshared ();
extern "C" void Enumerator_Dispose_m21155_gshared ();
extern "C" void Transform_1__ctor_m21156_gshared ();
extern "C" void Transform_1_Invoke_m21157_gshared ();
extern "C" void Transform_1_BeginInvoke_m21158_gshared ();
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2815_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m21159_gshared ();
extern "C" void ValueCollection__ctor_m21160_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m21161_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m21162_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m21163_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m21164_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m21165_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m21166_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m21167_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m21168_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m21169_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m21170_gshared ();
extern "C" void ValueCollection_CopyTo_m21171_gshared ();
extern "C" void ValueCollection_GetEnumerator_m21172_gshared ();
void* RuntimeInvoker_Enumerator_t3346 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m21173_gshared ();
extern "C" void Enumerator__ctor_m21174_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21175_gshared ();
extern "C" void Enumerator_Dispose_m21176_gshared ();
extern "C" void Enumerator_MoveNext_m21177_gshared ();
extern "C" void Enumerator_get_Current_m21178_gshared ();
extern "C" void Transform_1__ctor_m21179_gshared ();
extern "C" void Transform_1_Invoke_m21180_gshared ();
extern "C" void Transform_1_BeginInvoke_m21181_gshared ();
extern "C" void Transform_1_EndInvoke_m21182_gshared ();
extern "C" void Transform_1__ctor_m21183_gshared ();
extern "C" void Transform_1_Invoke_m21184_gshared ();
extern "C" void Transform_1_BeginInvoke_m21185_gshared ();
extern "C" void Transform_1_EndInvoke_m21186_gshared ();
extern "C" void Transform_1__ctor_m21187_gshared ();
extern "C" void Transform_1_Invoke_m21188_gshared ();
extern "C" void Transform_1_BeginInvoke_m21189_gshared ();
extern "C" void Transform_1_EndInvoke_m21190_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3314_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m21191_gshared ();
extern "C" void ShimEnumerator_MoveNext_m21192_gshared ();
extern "C" void ShimEnumerator_get_Entry_m21193_gshared ();
extern "C" void ShimEnumerator_get_Key_m21194_gshared ();
extern "C" void ShimEnumerator_get_Value_m21195_gshared ();
extern "C" void ShimEnumerator_get_Current_m21196_gshared ();
extern "C" void EqualityComparer_1__ctor_m21197_gshared ();
extern "C" void EqualityComparer_1__cctor_m21198_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21199_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21200_gshared ();
extern "C" void EqualityComparer_1_get_Default_m21201_gshared ();
extern "C" void DefaultComparer__ctor_m21202_gshared ();
extern "C" void DefaultComparer_GetHashCode_m21203_gshared ();
extern "C" void DefaultComparer_Equals_m21204_gshared ();
void* RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2815_KeyValuePair_2_t2815 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m21395_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21396_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21397_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21398_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21399_gshared ();
void* RuntimeInvoker_ParameterModifier_t1914 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m21400_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21401_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21402_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21403_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21404_gshared ();
void* RuntimeInvoker_HitInfo_t983 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1_Invoke_m21528_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m21529_gshared ();
extern "C" void InvokableCall_1__ctor_m21530_gshared ();
extern "C" void InvokableCall_1__ctor_m21531_gshared ();
extern "C" void InvokableCall_1_Invoke_m21532_gshared ();
extern "C" void InvokableCall_1_Find_m21533_gshared ();
extern "C" void UnityAction_1__ctor_m21534_gshared ();
extern "C" void UnityAction_1_Invoke_m21535_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m21536_gshared ();
void* RuntimeInvoker_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m21537_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m21545_gshared ();
extern "C" void Dictionary_2__ctor_m21740_gshared ();
extern "C" void Dictionary_2__ctor_m21743_gshared ();
extern "C" void Dictionary_2__ctor_m21745_gshared ();
extern "C" void Dictionary_2__ctor_m21747_gshared ();
extern "C" void Dictionary_2__ctor_m21749_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21751_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21753_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m21755_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m21757_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m21759_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m21761_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m21763_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21765_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21767_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21769_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21771_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21773_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21775_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21777_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m21779_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21781_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21783_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21785_gshared ();
extern "C" void Dictionary_2_get_Count_m21787_gshared ();
extern "C" void Dictionary_2_get_Item_m21789_gshared ();
extern "C" void Dictionary_2_set_Item_m21791_gshared ();
void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m21793_gshared ();
extern "C" void Dictionary_2_InitArrays_m21795_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m21797_gshared ();
extern "C" void Dictionary_2_make_pair_m21799_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3407_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m21801_gshared ();
void* RuntimeInvoker_Object_t_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m21803_gshared ();
void* RuntimeInvoker_Byte_t680_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m21805_gshared ();
extern "C" void Dictionary_2_Resize_m21807_gshared ();
extern "C" void Dictionary_2_Add_m21809_gshared ();
extern "C" void Dictionary_2_Clear_m21811_gshared ();
extern "C" void Dictionary_2_ContainsKey_m21813_gshared ();
extern "C" void Dictionary_2_ContainsValue_m21815_gshared ();
extern "C" void Dictionary_2_GetObjectData_m21817_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m21819_gshared ();
extern "C" void Dictionary_2_Remove_m21821_gshared ();
extern "C" void Dictionary_2_TryGetValue_m21823_gshared ();
void* RuntimeInvoker_Boolean_t273_Object_t_ByteU26_t1656 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m21825_gshared ();
extern "C" void Dictionary_2_get_Values_m21827_gshared ();
extern "C" void Dictionary_2_ToTKey_m21829_gshared ();
extern "C" void Dictionary_2_ToTValue_m21831_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m21833_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m21835_gshared ();
void* RuntimeInvoker_Enumerator_t3411 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m21837_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m21838_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21839_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21840_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21841_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21842_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3407 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m21843_gshared ();
extern "C" void KeyValuePair_2_get_Key_m21844_gshared ();
extern "C" void KeyValuePair_2_set_Key_m21845_gshared ();
extern "C" void KeyValuePair_2_get_Value_m21846_gshared ();
extern "C" void KeyValuePair_2_set_Value_m21847_gshared ();
extern "C" void KeyValuePair_2_ToString_m21848_gshared ();
extern "C" void KeyCollection__ctor_m21849_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21850_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21851_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21852_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21853_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21854_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m21855_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21856_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21857_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21858_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m21859_gshared ();
extern "C" void KeyCollection_CopyTo_m21860_gshared ();
extern "C" void KeyCollection_GetEnumerator_m21861_gshared ();
void* RuntimeInvoker_Enumerator_t3410 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m21862_gshared ();
extern "C" void Enumerator__ctor_m21863_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21864_gshared ();
extern "C" void Enumerator_Dispose_m21865_gshared ();
extern "C" void Enumerator_MoveNext_m21866_gshared ();
extern "C" void Enumerator_get_Current_m21867_gshared ();
extern "C" void Enumerator__ctor_m21868_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21869_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21870_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21871_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21872_gshared ();
extern "C" void Enumerator_MoveNext_m21873_gshared ();
extern "C" void Enumerator_get_Current_m21874_gshared ();
extern "C" void Enumerator_get_CurrentKey_m21875_gshared ();
extern "C" void Enumerator_get_CurrentValue_m21876_gshared ();
extern "C" void Enumerator_VerifyState_m21877_gshared ();
extern "C" void Enumerator_VerifyCurrent_m21878_gshared ();
extern "C" void Enumerator_Dispose_m21879_gshared ();
extern "C" void Transform_1__ctor_m21880_gshared ();
extern "C" void Transform_1_Invoke_m21881_gshared ();
extern "C" void Transform_1_BeginInvoke_m21882_gshared ();
void* RuntimeInvoker_Object_t_Object_t_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m21883_gshared ();
extern "C" void ValueCollection__ctor_m21884_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m21885_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m21886_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m21887_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m21888_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m21889_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m21890_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m21891_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m21892_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m21893_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m21894_gshared ();
extern "C" void ValueCollection_CopyTo_m21895_gshared ();
extern "C" void ValueCollection_GetEnumerator_m21896_gshared ();
void* RuntimeInvoker_Enumerator_t3414 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m21897_gshared ();
extern "C" void Enumerator__ctor_m21898_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21899_gshared ();
extern "C" void Enumerator_Dispose_m21900_gshared ();
extern "C" void Enumerator_MoveNext_m21901_gshared ();
extern "C" void Enumerator_get_Current_m21902_gshared ();
extern "C" void Transform_1__ctor_m21903_gshared ();
extern "C" void Transform_1_Invoke_m21904_gshared ();
extern "C" void Transform_1_BeginInvoke_m21905_gshared ();
extern "C" void Transform_1_EndInvoke_m21906_gshared ();
extern "C" void Transform_1__ctor_m21907_gshared ();
extern "C" void Transform_1_Invoke_m21908_gshared ();
extern "C" void Transform_1_BeginInvoke_m21909_gshared ();
extern "C" void Transform_1_EndInvoke_m21910_gshared ();
extern "C" void Transform_1__ctor_m21911_gshared ();
extern "C" void Transform_1_Invoke_m21912_gshared ();
extern "C" void Transform_1_BeginInvoke_m21913_gshared ();
extern "C" void Transform_1_EndInvoke_m21914_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3407_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m21915_gshared ();
extern "C" void ShimEnumerator_MoveNext_m21916_gshared ();
extern "C" void ShimEnumerator_get_Entry_m21917_gshared ();
extern "C" void ShimEnumerator_get_Key_m21918_gshared ();
extern "C" void ShimEnumerator_get_Value_m21919_gshared ();
extern "C" void ShimEnumerator_get_Current_m21920_gshared ();
extern "C" void InternalEnumerator_1__ctor_m21976_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21977_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21978_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21979_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21980_gshared ();
void* RuntimeInvoker_X509ChainStatus_t1331 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m21991_gshared ();
extern "C" void Dictionary_2__ctor_m21992_gshared ();
extern "C" void Dictionary_2__ctor_m21993_gshared ();
extern "C" void Dictionary_2__ctor_m21994_gshared ();
extern "C" void Dictionary_2__ctor_m21995_gshared ();
extern "C" void Dictionary_2__ctor_m21996_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21997_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21998_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m21999_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22000_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22001_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m22002_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22003_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22004_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22005_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22006_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22007_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22008_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22009_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22010_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22011_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22012_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22013_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22014_gshared ();
extern "C" void Dictionary_2_get_Count_m22015_gshared ();
extern "C" void Dictionary_2_get_Item_m22016_gshared ();
extern "C" void Dictionary_2_set_Item_m22017_gshared ();
extern "C" void Dictionary_2_Init_m22018_gshared ();
extern "C" void Dictionary_2_InitArrays_m22019_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m22020_gshared ();
extern "C" void Dictionary_2_make_pair_m22021_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3427_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m22022_gshared ();
void* RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m22023_gshared ();
extern "C" void Dictionary_2_CopyTo_m22024_gshared ();
extern "C" void Dictionary_2_Resize_m22025_gshared ();
extern "C" void Dictionary_2_Add_m22026_gshared ();
extern "C" void Dictionary_2_Clear_m22027_gshared ();
extern "C" void Dictionary_2_ContainsKey_m22028_gshared ();
extern "C" void Dictionary_2_ContainsValue_m22029_gshared ();
extern "C" void Dictionary_2_GetObjectData_m22030_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m22031_gshared ();
extern "C" void Dictionary_2_Remove_m22032_gshared ();
extern "C" void Dictionary_2_TryGetValue_m22033_gshared ();
void* RuntimeInvoker_Boolean_t273_Int32_t253_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m22034_gshared ();
extern "C" void Dictionary_2_get_Values_m22035_gshared ();
extern "C" void Dictionary_2_ToTKey_m22036_gshared ();
extern "C" void Dictionary_2_ToTValue_m22037_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m22038_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m22039_gshared ();
void* RuntimeInvoker_Enumerator_t3431 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m22040_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1430_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22041_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22042_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22043_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22044_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22045_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3427 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m22046_gshared ();
extern "C" void KeyValuePair_2_get_Key_m22047_gshared ();
extern "C" void KeyValuePair_2_set_Key_m22048_gshared ();
extern "C" void KeyValuePair_2_get_Value_m22049_gshared ();
extern "C" void KeyValuePair_2_set_Value_m22050_gshared ();
extern "C" void KeyValuePair_2_ToString_m22051_gshared ();
extern "C" void KeyCollection__ctor_m22052_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22053_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22054_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22055_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22056_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22057_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m22058_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22059_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22060_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22061_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m22062_gshared ();
extern "C" void KeyCollection_CopyTo_m22063_gshared ();
extern "C" void KeyCollection_GetEnumerator_m22064_gshared ();
void* RuntimeInvoker_Enumerator_t3430 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m22065_gshared ();
extern "C" void Enumerator__ctor_m22066_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22067_gshared ();
extern "C" void Enumerator_Dispose_m22068_gshared ();
extern "C" void Enumerator_MoveNext_m22069_gshared ();
extern "C" void Enumerator_get_Current_m22070_gshared ();
extern "C" void Enumerator__ctor_m22071_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22072_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22073_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22074_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22075_gshared ();
extern "C" void Enumerator_MoveNext_m22076_gshared ();
extern "C" void Enumerator_get_Current_m22077_gshared ();
extern "C" void Enumerator_get_CurrentKey_m22078_gshared ();
extern "C" void Enumerator_get_CurrentValue_m22079_gshared ();
extern "C" void Enumerator_VerifyState_m22080_gshared ();
extern "C" void Enumerator_VerifyCurrent_m22081_gshared ();
extern "C" void Enumerator_Dispose_m22082_gshared ();
extern "C" void Transform_1__ctor_m22083_gshared ();
extern "C" void Transform_1_Invoke_m22084_gshared ();
extern "C" void Transform_1_BeginInvoke_m22085_gshared ();
void* RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m22086_gshared ();
extern "C" void ValueCollection__ctor_m22087_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22088_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22089_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22090_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22091_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22092_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m22093_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22094_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22095_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22096_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m22097_gshared ();
extern "C" void ValueCollection_CopyTo_m22098_gshared ();
extern "C" void ValueCollection_GetEnumerator_m22099_gshared ();
void* RuntimeInvoker_Enumerator_t3434 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m22100_gshared ();
extern "C" void Enumerator__ctor_m22101_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22102_gshared ();
extern "C" void Enumerator_Dispose_m22103_gshared ();
extern "C" void Enumerator_MoveNext_m22104_gshared ();
extern "C" void Enumerator_get_Current_m22105_gshared ();
extern "C" void Transform_1__ctor_m22106_gshared ();
extern "C" void Transform_1_Invoke_m22107_gshared ();
extern "C" void Transform_1_BeginInvoke_m22108_gshared ();
extern "C" void Transform_1_EndInvoke_m22109_gshared ();
extern "C" void Transform_1__ctor_m22110_gshared ();
extern "C" void Transform_1_Invoke_m22111_gshared ();
extern "C" void Transform_1_BeginInvoke_m22112_gshared ();
extern "C" void Transform_1_EndInvoke_m22113_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3427_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m22114_gshared ();
extern "C" void ShimEnumerator_MoveNext_m22115_gshared ();
extern "C" void ShimEnumerator_get_Entry_m22116_gshared ();
extern "C" void ShimEnumerator_get_Key_m22117_gshared ();
extern "C" void ShimEnumerator_get_Value_m22118_gshared ();
extern "C" void ShimEnumerator_get_Current_m22119_gshared ();
extern "C" void Comparer_1__ctor_m22120_gshared ();
extern "C" void Comparer_1__cctor_m22121_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m22122_gshared ();
extern "C" void Comparer_1_get_Default_m22123_gshared ();
extern "C" void GenericComparer_1__ctor_m22124_gshared ();
extern "C" void GenericComparer_1_Compare_m22125_gshared ();
extern "C" void DefaultComparer__ctor_m22126_gshared ();
extern "C" void DefaultComparer_Compare_m22127_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22128_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22129_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22130_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22131_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22132_gshared ();
void* RuntimeInvoker_Mark_t1382 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22133_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22134_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22135_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22136_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22137_gshared ();
void* RuntimeInvoker_UriScheme_t1418 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22142_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22143_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22144_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22145_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22146_gshared ();
void* RuntimeInvoker_UInt32_t1063 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22167_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22168_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22169_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22170_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22171_gshared ();
void* RuntimeInvoker_Int16_t752 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22172_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22173_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22174_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22175_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22176_gshared ();
void* RuntimeInvoker_SByte_t274 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22207_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22208_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22209_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22210_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22211_gshared ();
void* RuntimeInvoker_TableRange_t1690 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22237_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22238_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22239_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22240_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22241_gshared ();
void* RuntimeInvoker_Slot_t1767 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22242_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22243_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22244_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22245_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22246_gshared ();
void* RuntimeInvoker_Slot_t1776 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22262_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22263_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22264_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22265_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22266_gshared ();
void* RuntimeInvoker_MonoResource_t1838 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22282_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22283_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22284_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22285_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22286_gshared ();
void* RuntimeInvoker_ILTokenInfo_t1856 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22287_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22288_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22289_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22290_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22291_gshared ();
void* RuntimeInvoker_LabelData_t1858 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22292_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22293_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22294_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22295_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22296_gshared ();
void* RuntimeInvoker_LabelFixup_t1857 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22350_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22351_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22352_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22353_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22354_gshared ();
void* RuntimeInvoker_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22355_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22356_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22357_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22358_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22359_gshared ();
void* RuntimeInvoker_Decimal_t1073 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22360_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22361_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22362_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22363_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22364_gshared ();
extern "C" void GenericComparer_1_Compare_m22473_gshared ();
void* RuntimeInvoker_Int32_t253_DateTime_t406_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m22474_gshared ();
extern "C" void Comparer_1__cctor_m22475_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m22476_gshared ();
extern "C" void Comparer_1_get_Default_m22477_gshared ();
extern "C" void DefaultComparer__ctor_m22478_gshared ();
extern "C" void DefaultComparer_Compare_m22479_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m22480_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m22481_gshared ();
void* RuntimeInvoker_Boolean_t273_DateTime_t406_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m22482_gshared ();
extern "C" void EqualityComparer_1__cctor_m22483_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22484_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22485_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22486_gshared ();
extern "C" void DefaultComparer__ctor_m22487_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22488_gshared ();
extern "C" void DefaultComparer_Equals_m22489_gshared ();
extern "C" void GenericComparer_1_Compare_m22490_gshared ();
void* RuntimeInvoker_Int32_t253_DateTimeOffset_t1086_DateTimeOffset_t1086 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m22491_gshared ();
extern "C" void Comparer_1__cctor_m22492_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m22493_gshared ();
extern "C" void Comparer_1_get_Default_m22494_gshared ();
extern "C" void DefaultComparer__ctor_m22495_gshared ();
extern "C" void DefaultComparer_Compare_m22496_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m22497_gshared ();
void* RuntimeInvoker_Int32_t253_DateTimeOffset_t1086 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericEqualityComparer_1_Equals_m22498_gshared ();
void* RuntimeInvoker_Boolean_t273_DateTimeOffset_t1086_DateTimeOffset_t1086 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m22499_gshared ();
extern "C" void EqualityComparer_1__cctor_m22500_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22501_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22502_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22503_gshared ();
extern "C" void DefaultComparer__ctor_m22504_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22505_gshared ();
extern "C" void DefaultComparer_Equals_m22506_gshared ();
extern "C" void Nullable_1_Equals_m22507_gshared ();
extern "C" void Nullable_1_Equals_m22508_gshared ();
void* RuntimeInvoker_Boolean_t273_Nullable_1_t2292 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_GetHashCode_m22509_gshared ();
extern "C" void Nullable_1_ToString_m22510_gshared ();
extern "C" void GenericComparer_1_Compare_m22511_gshared ();
void* RuntimeInvoker_Int32_t253_Guid_t1087_Guid_t1087 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m22512_gshared ();
extern "C" void Comparer_1__cctor_m22513_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m22514_gshared ();
extern "C" void Comparer_1_get_Default_m22515_gshared ();
extern "C" void DefaultComparer__ctor_m22516_gshared ();
extern "C" void DefaultComparer_Compare_m22517_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m22518_gshared ();
void* RuntimeInvoker_Int32_t253_Guid_t1087 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericEqualityComparer_1_Equals_m22519_gshared ();
void* RuntimeInvoker_Boolean_t273_Guid_t1087_Guid_t1087 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m22520_gshared ();
extern "C" void EqualityComparer_1__cctor_m22521_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22522_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22523_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22524_gshared ();
extern "C" void DefaultComparer__ctor_m22525_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22526_gshared ();
extern "C" void DefaultComparer_Equals_m22527_gshared ();
extern "C" void GenericComparer_1_Compare_m22528_gshared ();
void* RuntimeInvoker_Int32_t253_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m22529_gshared ();
extern "C" void Comparer_1__cctor_m22530_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m22531_gshared ();
extern "C" void Comparer_1_get_Default_m22532_gshared ();
extern "C" void DefaultComparer__ctor_m22533_gshared ();
extern "C" void DefaultComparer_Compare_m22534_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m22535_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m22536_gshared ();
void* RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m22537_gshared ();
extern "C" void EqualityComparer_1__cctor_m22538_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22539_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22540_gshared ();
extern "C" void EqualityComparer_1_get_Default_m22541_gshared ();
extern "C" void DefaultComparer__ctor_m22542_gshared ();
extern "C" void DefaultComparer_GetHashCode_m22543_gshared ();
extern "C" void DefaultComparer_Equals_m22544_gshared ();
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_PlatformerCharacter2D_t7_0_0_0;
extern const Il2CppGenericInst GenInst_Animator_t9_0_0_0;
extern const Il2CppGenericInst GenInst_Rigidbody2D_t10_0_0_0;
extern const Il2CppGenericInst GenInst_Rigidbody_t14_0_0_0;
extern const Il2CppGenericInst GenInst_Camera_t27_0_0_0;
extern const Il2CppGenericInst GenInst_Renderer_t154_0_0_0;
extern const Il2CppGenericInst GenInst_Image_t48_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t30_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t33_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0;
extern const Il2CppGenericInst GenInst_Collider_t138_0_0_0;
extern const Il2CppGenericInst GenInst_ParticleSystemMultiplier_t156_0_0_0;
extern const Il2CppGenericInst GenInst_ObjectResetter_t149_0_0_0;
extern const Il2CppGenericInst GenInst_ParticleSystem_t161_0_0_0;
extern const Il2CppGenericInst GenInst_Light_t152_0_0_0;
extern const Il2CppGenericInst GenInst_AudioSource_t247_0_0_0;
extern const Il2CppGenericInst GenInst_Animation_t249_0_0_0;
extern const Il2CppGenericInst GenInst_Material_t55_0_0_0;
extern const Il2CppGenericInst GenInst_SpringJoint_t176_0_0_0;
extern const Il2CppGenericInst GenInst_GUIText_t181_0_0_0;
extern const Il2CppGenericInst GenInst_Transform_t1_0_0_0;
extern const Il2CppGenericInst GenInst_Sample_t290_0_0_0;
extern const Il2CppGenericInst GenInst_Log_t291_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_Log_t291_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0;
extern const Il2CppGenericInst GenInst_Vector2_t6_0_0_0;
extern const Il2CppGenericInst GenInst_GUISkin_t287_0_0_0;
extern const Il2CppGenericInst GenInst_ReporterGUI_t305_0_0_0;
extern const Il2CppGenericInst GenInst_Reporter_t295_0_0_0;
extern const Il2CppGenericInst GenInst_EventSystem_t326_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_t78_0_0_0;
extern const Il2CppGenericInst GenInst_Toggle_t312_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t414_0_0_0;
extern const Il2CppGenericInst GenInst_PauseMenu_t313_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastResult_t486_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t78_0_0_0;
extern const Il2CppGenericInst GenInst_Text_t316_0_0_0;
extern const Il2CppGenericInst GenInst_BoxCollider_t343_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_Wings_t351_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_Respawn_t344_0_0_0;
extern const Il2CppGenericInst GenInst_SphereCollider_t136_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t253_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_Waypoint_t358_0_0_0;
extern const Il2CppGenericInst GenInst_Button_t324_0_0_0;
extern const Il2CppGenericInst GenInst_RectTransform_t364_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_JoystickDirection_t366_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Boolean_t273_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_Menu_t336_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_PauseMenu_t374_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_MainMenu_t372_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_LevelEndMenu_t367_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_Camie_t338_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_ManagerComponents_t380_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_Puceron_t356_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AudioSource_t247_0_0_0;
extern const Il2CppGenericInst GenInst_SCR_Hurt_t393_0_0_0;
extern const Il2CppGenericInst GenInst_LineRenderer_t397_0_0_0;
extern const Il2CppGenericInst GenInst_MultiKeyDictionary_3_t431_gp_1_0_0_0_MultiKeyDictionary_3_t431_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_MultiKeyDictionary_3_t431_gp_0_0_0_0_Dictionary_2_t437_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3963_0_0_0;
extern const Il2CppGenericInst GenInst_BaseInputModule_t452_0_0_0;
extern const Il2CppGenericInst GenInst_IDeselectHandler_t639_0_0_0;
extern const Il2CppGenericInst GenInst_ISelectHandler_t638_0_0_0;
extern const Il2CppGenericInst GenInst_BaseEventData_t453_0_0_0;
extern const Il2CppGenericInst GenInst_Entry_t458_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t630_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t631_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t280_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t282_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t448_0_0_0;
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t632_0_0_0;
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t633_0_0_0;
extern const Il2CppGenericInst GenInst_IDragHandler_t283_0_0_0;
extern const Il2CppGenericInst GenInst_IEndDragHandler_t634_0_0_0;
extern const Il2CppGenericInst GenInst_IDropHandler_t635_0_0_0;
extern const Il2CppGenericInst GenInst_IScrollHandler_t636_0_0_0;
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t637_0_0_0;
extern const Il2CppGenericInst GenInst_IMoveHandler_t640_0_0_0;
extern const Il2CppGenericInst GenInst_ISubmitHandler_t641_0_0_0;
extern const Il2CppGenericInst GenInst_ICancelHandler_t642_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t644_0_0_0;
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t281_0_0_0;
extern const Il2CppGenericInst GenInst_PointerEventData_t215_0_0_0;
extern const Il2CppGenericInst GenInst_AxisEventData_t487_0_0_0;
extern const Il2CppGenericInst GenInst_BaseRaycaster_t485_0_0_0;
extern const Il2CppGenericInst GenInst_ButtonState_t492_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_PointerEventData_t215_0_0_0;
extern const Il2CppGenericInst GenInst_SpriteRenderer_t663_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastHit_t24_0_0_0;
extern const Il2CppGenericInst GenInst_Color_t65_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t646_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t517_0_0_0_List_1_t667_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t517_0_0_0;
extern const Il2CppGenericInst GenInst_ColorTween_t506_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t558_0_0_0;
extern const Il2CppGenericInst GenInst_UIVertex_t556_0_0_0;
extern const Il2CppGenericInst GenInst_CanvasRenderer_t522_0_0_0;
extern const Il2CppGenericInst GenInst_Component_t219_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t418_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t414_0_0_0_IndexedSet_1_t679_0_0_0;
extern const Il2CppGenericInst GenInst_Sprite_t329_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t530_0_0_0;
extern const Il2CppGenericInst GenInst_Boolean_t273_0_0_0;
extern const Il2CppGenericInst GenInst_FillMethod_t531_0_0_0;
extern const Il2CppGenericInst GenInst_Single_t254_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0;
extern const Il2CppGenericInst GenInst_SubmitEvent_t542_0_0_0;
extern const Il2CppGenericInst GenInst_OnChangeEvent_t544_0_0_0;
extern const Il2CppGenericInst GenInst_OnValidateInput_t548_0_0_0;
extern const Il2CppGenericInst GenInst_ContentType_t538_0_0_0;
extern const Il2CppGenericInst GenInst_LineType_t541_0_0_0;
extern const Il2CppGenericInst GenInst_InputType_t539_0_0_0;
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t683_0_0_0;
extern const Il2CppGenericInst GenInst_CharacterValidation_t540_0_0_0;
extern const Il2CppGenericInst GenInst_Char_t682_0_0_0;
extern const Il2CppGenericInst GenInst_UILineInfo_t687_0_0_0;
extern const Il2CppGenericInst GenInst_UICharInfo_t689_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutElement_t611_0_0_0;
extern const Il2CppGenericInst GenInst_Direction_t565_0_0_0;
extern const Il2CppGenericInst GenInst_CanvasGroup_t672_0_0_0;
extern const Il2CppGenericInst GenInst_Selectable_t510_0_0_0;
extern const Il2CppGenericInst GenInst_Navigation_t563_0_0_0;
extern const Il2CppGenericInst GenInst_Transition_t576_0_0_0;
extern const Il2CppGenericInst GenInst_ColorBlock_t516_0_0_0;
extern const Il2CppGenericInst GenInst_SpriteState_t580_0_0_0;
extern const Il2CppGenericInst GenInst_AnimationTriggers_t507_0_0_0;
extern const Il2CppGenericInst GenInst_Direction_t582_0_0_0;
extern const Il2CppGenericInst GenInst_MatEntry_t586_0_0_0;
extern const Il2CppGenericInst GenInst_Toggle_t312_0_0_0_Boolean_t273_0_0_0;
extern const Il2CppGenericInst GenInst_AspectMode_t596_0_0_0;
extern const Il2CppGenericInst GenInst_FitMode_t602_0_0_0;
extern const Il2CppGenericInst GenInst_Corner_t604_0_0_0;
extern const Il2CppGenericInst GenInst_Axis_t605_0_0_0;
extern const Il2CppGenericInst GenInst_Constraint_t606_0_0_0;
extern const Il2CppGenericInst GenInst_RectOffset_t404_0_0_0;
extern const Il2CppGenericInst GenInst_TextAnchor_t701_0_0_0;
extern const Il2CppGenericInst GenInst_ILayoutElement_t652_0_0_0_Single_t254_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t653_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t651_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m3497_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m3498_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m3500_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m3501_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m3502_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_TweenRunner_1_t722_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t615_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t729_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t729_gp_0_0_0_0_Int32_t253_0_0_0;
extern const Il2CppGenericInst GenInst_ObjectPool_1_t730_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GcLeaderboard_t798_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_LayoutCache_t817_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t819_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t302_0_0_0;
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t850_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_MatchDirectConnectInfo_t913_0_0_0;
extern const Il2CppGenericInst GenInst_MatchDesc_t915_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t920_0_0_0_NetworkAccessToken_t922_0_0_0;
extern const Il2CppGenericInst GenInst_CreateMatchResponse_t907_0_0_0;
extern const Il2CppGenericInst GenInst_JoinMatchResponse_t909_0_0_0;
extern const Il2CppGenericInst GenInst_BasicResponse_t904_0_0_0;
extern const Il2CppGenericInst GenInst_ListMatchResponse_t917_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1035_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t939_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1037_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t937_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1038_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t1085_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SetDelegate_t938_0_0_0;
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1088_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorInfo_t940_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayer_t808_0_0_0;
extern const Il2CppGenericInst GenInst_PersistentCall_t1005_0_0_0;
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t1002_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t1046_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1048_0_0_0;
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t981_0_0_0;
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t975_0_0_0;
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m5265_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m5269_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m5270_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m5271_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m5272_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponents_m5273_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m5275_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m5277_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m5278_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m5279_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ResponseBase_ParseJSONList_m5283_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t1071_0_0_0;
extern const Il2CppGenericInst GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkMatch_ProcessMatchResponse_m5285_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t1121_gp_0_0_0_0_ThreadSafeDictionary_2_t1121_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t1121_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t1121_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1205_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t560_0_0_0_TextEditOp_t998_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_1_t1128_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t1129_gp_0_0_0_0_InvokableCall_2_t1129_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t1129_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t1129_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1130_gp_0_0_0_0_InvokableCall_3_t1130_gp_1_0_0_0_InvokableCall_3_t1130_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1130_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1130_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1130_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1131_gp_0_0_0_0_InvokableCall_4_t1131_gp_1_0_0_0_InvokableCall_4_t1131_gp_2_0_0_0_InvokableCall_4_t1131_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1131_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1131_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1131_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t1131_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t1107_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_1_t1132_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_2_t1133_gp_0_0_0_0_UnityEvent_2_t1133_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_3_t1134_gp_0_0_0_0_UnityEvent_3_t1134_gp_1_0_0_0_UnityEvent_3_t1134_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_4_t1135_gp_0_0_0_0_UnityEvent_4_t1135_gp_1_0_0_0_UnityEvent_4_t1135_gp_2_0_0_0_UnityEvent_4_t1135_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t273_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t1477_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Stack_1_t1476_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Int32_t253_0_0_0;
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_gp_0_0_0_0_Boolean_t273_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Where_m6654_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Where_m6654_gp_0_0_0_0_Boolean_t273_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m6655_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m6655_gp_0_0_0_0_Boolean_t273_0_0_0;
extern const Il2CppGenericInst GenInst_StrongName_t2116_0_0_0;
extern const Il2CppGenericInst GenInst_DateTime_t406_0_0_0;
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1086_0_0_0;
extern const Il2CppGenericInst GenInst_TimeSpan_t1337_0_0_0;
extern const Il2CppGenericInst GenInst_Guid_t1087_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t1071_0_0_0;
extern const Il2CppGenericInst GenInst_UInt32_t1063_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1074_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t680_0_0_0;
extern const Il2CppGenericInst GenInst_SByte_t274_0_0_0;
extern const Il2CppGenericInst GenInst_Int16_t752_0_0_0;
extern const Il2CppGenericInst GenInst_UInt16_t684_0_0_0;
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2324_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Double_t1070_0_0_0;
extern const Il2CppGenericInst GenInst_Decimal_t1073_0_0_0;
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t2325_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2327_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2326_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m12676_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12688_gp_0_0_0_0_Array_Sort_m12688_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12689_gp_0_0_0_0_Array_Sort_m12689_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12690_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12690_gp_0_0_0_0_Array_Sort_m12690_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12691_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12691_gp_0_0_0_0_Array_Sort_m12691_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12692_gp_0_0_0_0_Array_Sort_m12692_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12693_gp_0_0_0_0_Array_Sort_m12693_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12694_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12694_gp_0_0_0_0_Array_Sort_m12694_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12695_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12695_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12695_gp_0_0_0_0_Array_Sort_m12695_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12696_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m12697_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m12698_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m12698_gp_0_0_0_0_Array_qsort_m12698_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_compare_m12699_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m12700_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Resize_m12703_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m12705_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_ForEach_m12706_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m12707_gp_0_0_0_0_Array_ConvertAll_m12707_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m12708_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m12709_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m12710_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m12711_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m12712_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m12713_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m12714_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m12715_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m12716_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m12717_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m12718_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m12719_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m12720_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m12721_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m12722_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m12723_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindAll_m12724_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Exists_m12725_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m12726_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Find_m12727_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLast_m12728_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IList_1_t2328_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ICollection_1_t2329_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Nullable_1_t2296_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_DefaultComparer_t2336_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Comparer_1_t2335_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GenericComparer_1_t2337_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ShimEnumerator_t2339_gp_0_0_0_0_ShimEnumerator_t2339_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2340_gp_0_0_0_0_Enumerator_t2340_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2636_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2342_gp_0_0_0_0_Enumerator_t2342_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2342_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2341_gp_0_0_0_0_KeyCollection_t2341_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2341_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2341_gp_0_0_0_0_KeyCollection_t2341_gp_1_0_0_0_KeyCollection_t2341_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t2341_gp_0_0_0_0_KeyCollection_t2341_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2344_gp_0_0_0_0_Enumerator_t2344_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2344_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2343_gp_0_0_0_0_ValueCollection_t2343_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2343_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2343_gp_0_0_0_0_ValueCollection_t2343_gp_1_0_0_0_ValueCollection_t2343_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t2343_gp_1_0_0_0_ValueCollection_t2343_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2338_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2338_gp_0_0_0_0_Dictionary_2_t2338_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2338_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2673_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2338_gp_0_0_0_0_Dictionary_2_t2338_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m12877_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2338_gp_0_0_0_0_Dictionary_2_t2338_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m12882_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m12882_gp_0_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2338_gp_0_0_0_0_Dictionary_2_t2338_gp_1_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1430_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2338_gp_0_0_0_0_Dictionary_2_t2338_gp_1_0_0_0_KeyValuePair_2_t2673_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2673_0_0_0_KeyValuePair_2_t2673_0_0_0;
extern const Il2CppGenericInst GenInst_DefaultComparer_t2347_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2346_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2348_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t2350_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t2350_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3964_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t2350_gp_0_0_0_0_IDictionary_2_t2350_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2352_gp_0_0_0_0_KeyValuePair_2_t2352_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t2354_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t2353_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Collection_1_t2355_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2356_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m13163_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m13163_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m13164_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Version_t1292_0_0_0;
extern const Il2CppGenericInst GenInst_Collider2D_t214_0_0_0;
extern const Il2CppGenericInst GenInst_Behaviour_t250_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t164_0_0_0;
extern const Il2CppGenericInst GenInst_ValueType_t285_0_0_0;
extern const Il2CppGenericInst GenInst_IFormattable_t275_0_0_0;
extern const Il2CppGenericInst GenInst_IConvertible_t276_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_t277_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2439_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2440_0_0_0;
extern const Il2CppGenericInst GenInst_AxisTouchButton_t29_0_0_0;
extern const Il2CppGenericInst GenInst_MonoBehaviour_t3_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2815_0_0_0;
extern const Il2CppGenericInst GenInst_IEnumerable_t438_0_0_0;
extern const Il2CppGenericInst GenInst_ICloneable_t733_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2434_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2435_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2400_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2401_0_0_0;
extern const Il2CppGenericInst GenInst_Link_t1757_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2815_0_0_0;
extern const Il2CppGenericInst GenInst_IReflect_t2332_0_0_0;
extern const Il2CppGenericInst GenInst__Type_t2330_0_0_0;
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t2293_0_0_0;
extern const Il2CppGenericInst GenInst__MemberInfo_t2331_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t30_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualAxis_t30_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2832_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t33_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_VirtualButton_t33_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2838_0_0_0;
extern const Il2CppGenericInst GenInst_Touch_t235_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2442_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2443_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2422_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2423_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2430_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2431_0_0_0;
extern const Il2CppGenericInst GenInst_Keyframe_t240_0_0_0;
extern const Il2CppGenericInst GenInst_RenderTexture_t101_0_0_0;
extern const Il2CppGenericInst GenInst_Texture_t86_0_0_0;
extern const Il2CppGenericInst GenInst_Mesh_t216_0_0_0;
extern const Il2CppGenericInst GenInst_Vector3_t4_0_0_0;
extern const Il2CppGenericInst GenInst_ContactPoint_t246_0_0_0;
extern const Il2CppGenericInst GenInst_AudioClip_t248_0_0_0;
extern const Il2CppGenericInst GenInst_ParticleCollisionEvent_t160_0_0_0;
extern const Il2CppGenericInst GenInst_ReplacementDefinition_t166_0_0_0;
extern const Il2CppGenericInst GenInst_Entry_t199_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Dictionary_2_t2814_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Dictionary_2_t2814_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t2814_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2890_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t407_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Log_t291_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t407_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Log_t291_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1057_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t407_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2923_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2927_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayoutOption_t410_0_0_0;
extern const Il2CppGenericInst GenInst_DemoParticleSystem_t321_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t78_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t419_0_0_0;
extern const Il2CppGenericInst GenInst_Quaternion_t19_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t253_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2953_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t253_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t253_0_0_0_Int32_t253_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t253_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t253_0_0_0_KeyValuePair_2_t2953_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t253_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2967_0_0_0;
extern const Il2CppGenericInst GenInst_ILayoutElement_t652_0_0_0;
extern const Il2CppGenericInst GenInst_MaskableGraphic_t537_0_0_0;
extern const Il2CppGenericInst GenInst_IMaskable_t707_0_0_0;
extern const Il2CppGenericInst GenInst_UIBehaviour_t455_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Byte_t680_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2984_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2413_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2414_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Byte_t680_0_0_0_Int32_t253_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Byte_t680_0_0_0_Byte_t680_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Byte_t680_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Byte_t680_0_0_0_KeyValuePair_2_t2984_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Boolean_t273_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2999_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AudioSource_t247_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3010_0_0_0;
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t888_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3059_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Object_t_0_0_0_Int32_t253_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Object_t_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3059_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_PointerEventData_t215_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t661_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastHit2D_t665_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t646_0_0_0_Int32_t253_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t646_0_0_0_Int32_t253_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t517_0_0_0_List_1_t667_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t667_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3093_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t418_0_0_0_Int32_t253_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t418_0_0_0_Int32_t253_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t414_0_0_0_IndexedSet_1_t679_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t679_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3124_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3128_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132_0_0_0;
extern const Il2CppGenericInst GenInst_Enum_t278_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t680_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t273_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t254_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementDescription_t1127_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievement_t1021_0_0_0;
extern const Il2CppGenericInst GenInst_IScore_t980_0_0_0;
extern const Il2CppGenericInst GenInst_IUserProfile_t1126_0_0_0;
extern const Il2CppGenericInst GenInst_AchievementDescription_t978_0_0_0;
extern const Il2CppGenericInst GenInst_UserProfile_t976_0_0_0;
extern const Il2CppGenericInst GenInst_GcAchievementData_t958_0_0_0;
extern const Il2CppGenericInst GenInst_Achievement_t977_0_0_0;
extern const Il2CppGenericInst GenInst_GcScoreData_t959_0_0_0;
extern const Il2CppGenericInst GenInst_Score_t979_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_LayoutCache_t817_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutCache_t817_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3201_0_0_0;
extern const Il2CppGenericInst GenInst_GUIStyle_t302_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t302_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3212_0_0_0;
extern const Il2CppGenericInst GenInst_Display_t861_0_0_0;
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0;
extern const Il2CppGenericInst GenInst_ISerializable_t439_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1071_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3255_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2405_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2406_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1071_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1071_0_0_0_Int64_t1071_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1071_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1071_0_0_0_KeyValuePair_2_t3255_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t1071_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3270_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1074_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3293_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2411_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2412_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1074_0_0_0_Object_t_0_0_0_UInt64_t1074_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1074_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1074_0_0_0_Object_t_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1074_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3293_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t920_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t920_0_0_0_NetworkAccessToken_t922_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkAccessToken_t922_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3308_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorDelegate_t939_0_0_0;
extern const Il2CppGenericInst GenInst_GetDelegate_t937_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t1037_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1085_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t1038_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2815_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3314_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t939_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3322_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1037_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3326_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1038_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3330_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t937_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2815_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2815_0_0_0_KeyValuePair_2_t2815_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2815_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2815_0_0_0_KeyValuePair_2_t3314_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t1085_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3352_0_0_0;
extern const Il2CppGenericInst GenInst__ConstructorInfo_t2370_0_0_0;
extern const Il2CppGenericInst GenInst_MethodBase_t1102_0_0_0;
extern const Il2CppGenericInst GenInst__MethodBase_t2373_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterInfo_t1093_0_0_0;
extern const Il2CppGenericInst GenInst__ParameterInfo_t2376_0_0_0;
extern const Il2CppGenericInst GenInst__PropertyInfo_t2377_0_0_0;
extern const Il2CppGenericInst GenInst__FieldInfo_t2372_0_0_0;
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t724_0_0_0;
extern const Il2CppGenericInst GenInst_Attribute_t805_0_0_0;
extern const Il2CppGenericInst GenInst__Attribute_t1145_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t262_0_0_0;
extern const Il2CppGenericInst GenInst_RequireComponent_t259_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterModifier_t1914_0_0_0;
extern const Il2CppGenericInst GenInst_HitInfo_t983_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t560_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t560_0_0_0_TextEditOp_t998_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_TextEditOp_t998_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3375_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3407_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t680_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t680_0_0_0_Byte_t680_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t680_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t680_0_0_0_KeyValuePair_2_t3407_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t273_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3418_0_0_0;
extern const Il2CppGenericInst GenInst_X509Certificate_t1323_0_0_0;
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t445_0_0_0;
extern const Il2CppGenericInst GenInst_X509ChainStatus_t1331_0_0_0;
extern const Il2CppGenericInst GenInst_Capture_t1355_0_0_0;
extern const Il2CppGenericInst GenInst_Group_t1358_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3427_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Int32_t253_0_0_0_Int32_t253_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Int32_t253_0_0_0_DictionaryEntry_t1430_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t253_0_0_0_Int32_t253_0_0_0_KeyValuePair_2_t3427_0_0_0;
extern const Il2CppGenericInst GenInst_Mark_t1382_0_0_0;
extern const Il2CppGenericInst GenInst_UriScheme_t1418_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2408_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2409_0_0_0;
extern const Il2CppGenericInst GenInst_BigInteger_t1520_0_0_0;
extern const Il2CppGenericInst GenInst_KeySizes_t1640_0_0_0;
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1612_0_0_0;
extern const Il2CppGenericInst GenInst_Delegate_t675_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2419_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2420_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2416_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2417_0_0_0;
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst__MethodInfo_t2374_0_0_0;
extern const Il2CppGenericInst GenInst_TableRange_t1690_0_0_0;
extern const Il2CppGenericInst GenInst_TailoringInfo_t1693_0_0_0;
extern const Il2CppGenericInst GenInst_Contraction_t1694_0_0_0;
extern const Il2CppGenericInst GenInst_Level2Map_t1696_0_0_0;
extern const Il2CppGenericInst GenInst_BigInteger_t1716_0_0_0;
extern const Il2CppGenericInst GenInst_Slot_t1767_0_0_0;
extern const Il2CppGenericInst GenInst_Slot_t1776_0_0_0;
extern const Il2CppGenericInst GenInst_StackFrame_t1101_0_0_0;
extern const Il2CppGenericInst GenInst_Calendar_t1788_0_0_0;
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1872_0_0_0;
extern const Il2CppGenericInst GenInst__ModuleBuilder_t2364_0_0_0;
extern const Il2CppGenericInst GenInst_Module_t1848_0_0_0;
extern const Il2CppGenericInst GenInst__Module_t2375_0_0_0;
extern const Il2CppGenericInst GenInst_MonoResource_t1838_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1879_0_0_0;
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2365_0_0_0;
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t238_0_0_0;
extern const Il2CppGenericInst GenInst_Array_t_0_0_0;
extern const Il2CppGenericInst GenInst_ICollection_t440_0_0_0;
extern const Il2CppGenericInst GenInst_IList_t1195_0_0_0;
extern const Il2CppGenericInst GenInst_LocalBuilder_t1865_0_0_0;
extern const Il2CppGenericInst GenInst__LocalBuilder_t2362_0_0_0;
extern const Il2CppGenericInst GenInst_LocalVariableInfo_t1866_0_0_0;
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1856_0_0_0;
extern const Il2CppGenericInst GenInst_LabelData_t1858_0_0_0;
extern const Il2CppGenericInst GenInst_LabelFixup_t1857_0_0_0;
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1855_0_0_0;
extern const Il2CppGenericInst GenInst_TypeBuilder_t1844_0_0_0;
extern const Il2CppGenericInst GenInst__TypeBuilder_t2367_0_0_0;
extern const Il2CppGenericInst GenInst_MethodBuilder_t1854_0_0_0;
extern const Il2CppGenericInst GenInst__MethodBuilder_t2363_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1847_0_0_0;
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t2358_0_0_0;
extern const Il2CppGenericInst GenInst_PropertyBuilder_t1880_0_0_0;
extern const Il2CppGenericInst GenInst__PropertyBuilder_t2366_0_0_0;
extern const Il2CppGenericInst GenInst_FieldBuilder_t1853_0_0_0;
extern const Il2CppGenericInst GenInst__FieldBuilder_t2360_0_0_0;
extern const Il2CppGenericInst GenInst_Header_t1985_0_0_0;
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2305_0_0_0;
extern const Il2CppGenericInst GenInst_IContextAttribute_t2299_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2770_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2771_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2445_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2446_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2790_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2791_0_0_0;
extern const Il2CppGenericInst GenInst_TypeTag_t2024_0_0_0;
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0;
extern const Il2CppGenericInst GenInst_WaitHandle_t1637_0_0_0;
extern const Il2CppGenericInst GenInst_IDisposable_t233_0_0_0;
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t1308_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2815_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2815_0_0_0_KeyValuePair_2_t2815_0_0_0;
extern const Il2CppGenericInst GenInst_Vector2_t6_0_0_0_Vector2_t6_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastResult_t486_0_0_0_RaycastResult_t486_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2953_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2953_0_0_0_KeyValuePair_2_t2953_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t680_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t680_0_0_0_Byte_t680_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2984_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2984_0_0_0_KeyValuePair_2_t2984_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3059_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3059_0_0_0_KeyValuePair_2_t3059_0_0_0;
extern const Il2CppGenericInst GenInst_UIVertex_t556_0_0_0_UIVertex_t556_0_0_0;
extern const Il2CppGenericInst GenInst_UICharInfo_t689_0_0_0_UICharInfo_t689_0_0_0;
extern const Il2CppGenericInst GenInst_UILineInfo_t687_0_0_0_UILineInfo_t687_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t1071_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t1071_0_0_0_Int64_t1071_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3255_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3255_0_0_0_KeyValuePair_2_t3255_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t1074_0_0_0_UInt64_t1074_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3293_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3293_0_0_0_KeyValuePair_2_t3293_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3314_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3314_0_0_0_KeyValuePair_2_t3314_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3407_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3407_0_0_0_KeyValuePair_2_t3407_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3427_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3427_0_0_0_KeyValuePair_2_t3427_0_0_0;
const methodPointerType g_Il2CppMethodPointers[3424] = 
{
	NULL/* 0*/,
	(methodPointerType)&MultiKeyDictionary_3_get_Item_m14473_gshared/* 1*/,
	(methodPointerType)&MultiKeyDictionary_3__ctor_m14472_gshared/* 2*/,
	(methodPointerType)&MultiKeyDictionary_3_ContainsKey_m14474_gshared/* 3*/,
	(methodPointerType)&ExecuteEvents_ValidateEventData_TisObject_t_m3069_gshared/* 4*/,
	(methodPointerType)&ExecuteEvents_Execute_TisObject_t_m3055_gshared/* 5*/,
	(methodPointerType)&ExecuteEvents_ExecuteHierarchy_TisObject_t_m3121_gshared/* 6*/,
	(methodPointerType)&ExecuteEvents_ShouldSendToComponent_TisObject_t_m22854_gshared/* 7*/,
	(methodPointerType)&ExecuteEvents_GetEventList_TisObject_t_m22850_gshared/* 8*/,
	(methodPointerType)&ExecuteEvents_CanHandleEvent_TisObject_t_m22856_gshared/* 9*/,
	(methodPointerType)&ExecuteEvents_GetEventHandler_TisObject_t_m3102_gshared/* 10*/,
	(methodPointerType)&EventFunction_1__ctor_m16355_gshared/* 11*/,
	(methodPointerType)&EventFunction_1_Invoke_m16357_gshared/* 12*/,
	(methodPointerType)&EventFunction_1_BeginInvoke_m16359_gshared/* 13*/,
	(methodPointerType)&EventFunction_1_EndInvoke_m16361_gshared/* 14*/,
	(methodPointerType)&SetPropertyUtility_SetClass_TisObject_t_m3220_gshared/* 15*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisObject_t_m3441_gshared/* 16*/,
	(methodPointerType)&IndexedSet_1_get_Count_m17292_gshared/* 17*/,
	(methodPointerType)&IndexedSet_1_get_IsReadOnly_m17294_gshared/* 18*/,
	(methodPointerType)&IndexedSet_1_get_Item_m17302_gshared/* 19*/,
	(methodPointerType)&IndexedSet_1_set_Item_m17304_gshared/* 20*/,
	(methodPointerType)&IndexedSet_1__ctor_m17276_gshared/* 21*/,
	(methodPointerType)&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17278_gshared/* 22*/,
	(methodPointerType)&IndexedSet_1_Add_m17280_gshared/* 23*/,
	(methodPointerType)&IndexedSet_1_Remove_m17282_gshared/* 24*/,
	(methodPointerType)&IndexedSet_1_GetEnumerator_m17284_gshared/* 25*/,
	(methodPointerType)&IndexedSet_1_Clear_m17286_gshared/* 26*/,
	(methodPointerType)&IndexedSet_1_Contains_m17288_gshared/* 27*/,
	(methodPointerType)&IndexedSet_1_CopyTo_m17290_gshared/* 28*/,
	(methodPointerType)&IndexedSet_1_IndexOf_m17296_gshared/* 29*/,
	(methodPointerType)&IndexedSet_1_Insert_m17298_gshared/* 30*/,
	(methodPointerType)&IndexedSet_1_RemoveAt_m17300_gshared/* 31*/,
	(methodPointerType)&IndexedSet_1_RemoveAll_m17305_gshared/* 32*/,
	(methodPointerType)&IndexedSet_1_Sort_m17306_gshared/* 33*/,
	(methodPointerType)&ObjectPool_1_get_countAll_m16458_gshared/* 34*/,
	(methodPointerType)&ObjectPool_1_set_countAll_m16460_gshared/* 35*/,
	(methodPointerType)&ObjectPool_1_get_countActive_m16462_gshared/* 36*/,
	(methodPointerType)&ObjectPool_1_get_countInactive_m16464_gshared/* 37*/,
	(methodPointerType)&ObjectPool_1__ctor_m16456_gshared/* 38*/,
	(methodPointerType)&ObjectPool_1_Get_m16466_gshared/* 39*/,
	(methodPointerType)&ObjectPool_1_Release_m16468_gshared/* 40*/,
	(methodPointerType)&ScriptableObject_CreateInstance_TisObject_t_m22932_gshared/* 41*/,
	(methodPointerType)&Resources_ConvertObjects_TisObject_t_m22740_gshared/* 42*/,
	(methodPointerType)&Object_Instantiate_TisObject_t_m953_gshared/* 43*/,
	(methodPointerType)&Object_FindObjectsOfType_TisObject_t_m951_gshared/* 44*/,
	(methodPointerType)&Object_FindObjectOfType_TisObject_t_m1713_gshared/* 45*/,
	(methodPointerType)&Component_GetComponent_TisObject_t_m614_gshared/* 46*/,
	(methodPointerType)&Component_GetComponentInChildren_TisObject_t_m675_gshared/* 47*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m22567_gshared/* 48*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m22930_gshared/* 49*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m698_gshared/* 50*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m3455_gshared/* 51*/,
	(methodPointerType)&Component_GetComponents_TisObject_t_m3053_gshared/* 52*/,
	(methodPointerType)&GameObject_GetComponent_TisObject_t_m771_gshared/* 53*/,
	(methodPointerType)&GameObject_GetComponents_TisObject_t_m22853_gshared/* 54*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m22568_gshared/* 55*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m22931_gshared/* 56*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m1781_gshared/* 57*/,
	(methodPointerType)&GameObject_GetComponentsInParent_TisObject_t_m3172_gshared/* 58*/,
	(methodPointerType)&GameObject_AddComponent_TisObject_t_m975_gshared/* 59*/,
	(methodPointerType)&ResponseBase_ParseJSONList_TisObject_t_m5154_gshared/* 60*/,
	(methodPointerType)&NetworkMatch_ProcessMatchResponse_TisObject_t_m5161_gshared/* 61*/,
	(methodPointerType)&ResponseDelegate_1__ctor_m20541_gshared/* 62*/,
	(methodPointerType)&ResponseDelegate_1_Invoke_m20543_gshared/* 63*/,
	(methodPointerType)&ResponseDelegate_1_BeginInvoke_m20545_gshared/* 64*/,
	(methodPointerType)&ResponseDelegate_1_EndInvoke_m20547_gshared/* 65*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m20549_gshared/* 66*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m20550_gshared/* 67*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m20548_gshared/* 68*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m20551_gshared/* 69*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m20552_gshared/* 70*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Keys_m20692_gshared/* 71*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Values_m20698_gshared/* 72*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Item_m20700_gshared/* 73*/,
	(methodPointerType)&ThreadSafeDictionary_2_set_Item_m20702_gshared/* 74*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Count_m20712_gshared/* 75*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_IsReadOnly_m20714_gshared/* 76*/,
	(methodPointerType)&ThreadSafeDictionary_2__ctor_m20682_gshared/* 77*/,
	(methodPointerType)&ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m20684_gshared/* 78*/,
	(methodPointerType)&ThreadSafeDictionary_2_Get_m20686_gshared/* 79*/,
	(methodPointerType)&ThreadSafeDictionary_2_AddValue_m20688_gshared/* 80*/,
	(methodPointerType)&ThreadSafeDictionary_2_Add_m20690_gshared/* 81*/,
	(methodPointerType)&ThreadSafeDictionary_2_Remove_m20694_gshared/* 82*/,
	(methodPointerType)&ThreadSafeDictionary_2_TryGetValue_m20696_gshared/* 83*/,
	(methodPointerType)&ThreadSafeDictionary_2_Add_m20704_gshared/* 84*/,
	(methodPointerType)&ThreadSafeDictionary_2_Clear_m20706_gshared/* 85*/,
	(methodPointerType)&ThreadSafeDictionary_2_Contains_m20708_gshared/* 86*/,
	(methodPointerType)&ThreadSafeDictionary_2_CopyTo_m20710_gshared/* 87*/,
	(methodPointerType)&ThreadSafeDictionary_2_Remove_m20716_gshared/* 88*/,
	(methodPointerType)&ThreadSafeDictionary_2_GetEnumerator_m20718_gshared/* 89*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2__ctor_m20675_gshared/* 90*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_Invoke_m20677_gshared/* 91*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_BeginInvoke_m20679_gshared/* 92*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_EndInvoke_m20681_gshared/* 93*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m22855_gshared/* 94*/,
	(methodPointerType)&InvokableCall_1__ctor_m16794_gshared/* 95*/,
	(methodPointerType)&InvokableCall_1__ctor_m16795_gshared/* 96*/,
	(methodPointerType)&InvokableCall_1_Invoke_m16796_gshared/* 97*/,
	(methodPointerType)&InvokableCall_1_Find_m16797_gshared/* 98*/,
	(methodPointerType)&InvokableCall_2__ctor_m21505_gshared/* 99*/,
	(methodPointerType)&InvokableCall_2_Invoke_m21506_gshared/* 100*/,
	(methodPointerType)&InvokableCall_2_Find_m21507_gshared/* 101*/,
	(methodPointerType)&InvokableCall_3__ctor_m21512_gshared/* 102*/,
	(methodPointerType)&InvokableCall_3_Invoke_m21513_gshared/* 103*/,
	(methodPointerType)&InvokableCall_3_Find_m21514_gshared/* 104*/,
	(methodPointerType)&InvokableCall_4__ctor_m21519_gshared/* 105*/,
	(methodPointerType)&InvokableCall_4_Invoke_m21520_gshared/* 106*/,
	(methodPointerType)&InvokableCall_4_Find_m21521_gshared/* 107*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m21526_gshared/* 108*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m21527_gshared/* 109*/,
	(methodPointerType)&UnityEvent_1__ctor_m16784_gshared/* 110*/,
	(methodPointerType)&UnityEvent_1_AddListener_m16786_gshared/* 111*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m16788_gshared/* 112*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m16789_gshared/* 113*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m16790_gshared/* 114*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m16792_gshared/* 115*/,
	(methodPointerType)&UnityEvent_1_Invoke_m16793_gshared/* 116*/,
	(methodPointerType)&UnityEvent_2__ctor_m21730_gshared/* 117*/,
	(methodPointerType)&UnityEvent_2_FindMethod_Impl_m21731_gshared/* 118*/,
	(methodPointerType)&UnityEvent_2_GetDelegate_m21732_gshared/* 119*/,
	(methodPointerType)&UnityEvent_3__ctor_m21733_gshared/* 120*/,
	(methodPointerType)&UnityEvent_3_FindMethod_Impl_m21734_gshared/* 121*/,
	(methodPointerType)&UnityEvent_3_GetDelegate_m21735_gshared/* 122*/,
	(methodPointerType)&UnityEvent_4__ctor_m21736_gshared/* 123*/,
	(methodPointerType)&UnityEvent_4_FindMethod_Impl_m21737_gshared/* 124*/,
	(methodPointerType)&UnityEvent_4_GetDelegate_m21738_gshared/* 125*/,
	(methodPointerType)&UnityAction_1__ctor_m16485_gshared/* 126*/,
	(methodPointerType)&UnityAction_1_Invoke_m16486_gshared/* 127*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m16487_gshared/* 128*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m16488_gshared/* 129*/,
	(methodPointerType)&UnityAction_2__ctor_m21508_gshared/* 130*/,
	(methodPointerType)&UnityAction_2_Invoke_m21509_gshared/* 131*/,
	(methodPointerType)&UnityAction_2_BeginInvoke_m21510_gshared/* 132*/,
	(methodPointerType)&UnityAction_2_EndInvoke_m21511_gshared/* 133*/,
	(methodPointerType)&UnityAction_3__ctor_m21515_gshared/* 134*/,
	(methodPointerType)&UnityAction_3_Invoke_m21516_gshared/* 135*/,
	(methodPointerType)&UnityAction_3_BeginInvoke_m21517_gshared/* 136*/,
	(methodPointerType)&UnityAction_3_EndInvoke_m21518_gshared/* 137*/,
	(methodPointerType)&UnityAction_4__ctor_m21522_gshared/* 138*/,
	(methodPointerType)&UnityAction_4_Invoke_m21523_gshared/* 139*/,
	(methodPointerType)&UnityAction_4_BeginInvoke_m21524_gshared/* 140*/,
	(methodPointerType)&UnityAction_4_EndInvoke_m21525_gshared/* 141*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_IsSynchronized_m16470_gshared/* 142*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_SyncRoot_m16471_gshared/* 143*/,
	(methodPointerType)&Stack_1_get_Count_m16478_gshared/* 144*/,
	(methodPointerType)&Stack_1__ctor_m16469_gshared/* 145*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_CopyTo_m16472_gshared/* 146*/,
	(methodPointerType)&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16473_gshared/* 147*/,
	(methodPointerType)&Stack_1_System_Collections_IEnumerable_GetEnumerator_m16474_gshared/* 148*/,
	(methodPointerType)&Stack_1_Peek_m16475_gshared/* 149*/,
	(methodPointerType)&Stack_1_Pop_m16476_gshared/* 150*/,
	(methodPointerType)&Stack_1_Push_m16477_gshared/* 151*/,
	(methodPointerType)&Stack_1_GetEnumerator_m16479_gshared/* 152*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16481_gshared/* 153*/,
	(methodPointerType)&Enumerator_get_Current_m16484_gshared/* 154*/,
	(methodPointerType)&Enumerator__ctor_m16480_gshared/* 155*/,
	(methodPointerType)&Enumerator_Dispose_m16482_gshared/* 156*/,
	(methodPointerType)&Enumerator_MoveNext_m16483_gshared/* 157*/,
	(methodPointerType)&Enumerable_Where_TisObject_t_m3413_gshared/* 158*/,
	(methodPointerType)&Enumerable_CreateWhereIterator_TisObject_t_m22929_gshared/* 159*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m18671_gshared/* 160*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m18672_gshared/* 161*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m18670_gshared/* 162*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m18673_gshared/* 163*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m18674_gshared/* 164*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m18675_gshared/* 165*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m18676_gshared/* 166*/,
	(methodPointerType)&Func_2__ctor_m22138_gshared/* 167*/,
	(methodPointerType)&Func_2_Invoke_m22139_gshared/* 168*/,
	(methodPointerType)&Func_2_BeginInvoke_m22140_gshared/* 169*/,
	(methodPointerType)&Func_2_EndInvoke_m22141_gshared/* 170*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m22555_gshared/* 171*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisObject_t_m22547_gshared/* 172*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisObject_t_m22550_gshared/* 173*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisObject_t_m22548_gshared/* 174*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisObject_t_m22549_gshared/* 175*/,
	(methodPointerType)&Array_InternalArray__Insert_TisObject_t_m22552_gshared/* 176*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisObject_t_m22551_gshared/* 177*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisObject_t_m22546_gshared/* 178*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisObject_t_m22554_gshared/* 179*/,
	(methodPointerType)&Array_get_swapper_TisObject_t_m22645_gshared/* 180*/,
	(methodPointerType)&Array_Sort_TisObject_t_m23200_gshared/* 181*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m23201_gshared/* 182*/,
	(methodPointerType)&Array_Sort_TisObject_t_m23202_gshared/* 183*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m23203_gshared/* 184*/,
	(methodPointerType)&Array_Sort_TisObject_t_m12634_gshared/* 185*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m23204_gshared/* 186*/,
	(methodPointerType)&Array_Sort_TisObject_t_m22644_gshared/* 187*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m22643_gshared/* 188*/,
	(methodPointerType)&Array_Sort_TisObject_t_m23205_gshared/* 189*/,
	(methodPointerType)&Array_Sort_TisObject_t_m22672_gshared/* 190*/,
	(methodPointerType)&Array_qsort_TisObject_t_TisObject_t_m22646_gshared/* 191*/,
	(methodPointerType)&Array_compare_TisObject_t_m22669_gshared/* 192*/,
	(methodPointerType)&Array_qsort_TisObject_t_m22671_gshared/* 193*/,
	(methodPointerType)&Array_swap_TisObject_t_TisObject_t_m22670_gshared/* 194*/,
	(methodPointerType)&Array_swap_TisObject_t_m22673_gshared/* 195*/,
	(methodPointerType)&Array_Resize_TisObject_t_m22642_gshared/* 196*/,
	(methodPointerType)&Array_Resize_TisObject_t_m22641_gshared/* 197*/,
	(methodPointerType)&Array_TrueForAll_TisObject_t_m23206_gshared/* 198*/,
	(methodPointerType)&Array_ForEach_TisObject_t_m23207_gshared/* 199*/,
	(methodPointerType)&Array_ConvertAll_TisObject_t_TisObject_t_m23208_gshared/* 200*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m23210_gshared/* 201*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m23211_gshared/* 202*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m23209_gshared/* 203*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m23213_gshared/* 204*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m23214_gshared/* 205*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m23212_gshared/* 206*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m23216_gshared/* 207*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m23217_gshared/* 208*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m23218_gshared/* 209*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m23215_gshared/* 210*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m12636_gshared/* 211*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m23219_gshared/* 212*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m12633_gshared/* 213*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m23221_gshared/* 214*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m23220_gshared/* 215*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m23222_gshared/* 216*/,
	(methodPointerType)&Array_FindAll_TisObject_t_m23223_gshared/* 217*/,
	(methodPointerType)&Array_Exists_TisObject_t_m23224_gshared/* 218*/,
	(methodPointerType)&Array_AsReadOnly_TisObject_t_m23225_gshared/* 219*/,
	(methodPointerType)&Array_Find_TisObject_t_m23226_gshared/* 220*/,
	(methodPointerType)&Array_FindLast_TisObject_t_m23227_gshared/* 221*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared/* 222*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13521_gshared/* 223*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13513_gshared/* 224*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13517_gshared/* 225*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13519_gshared/* 226*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Item_m22183_gshared/* 227*/,
	(methodPointerType)&ArrayReadOnlyList_1_set_Item_m22184_gshared/* 228*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Count_m22185_gshared/* 229*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_IsReadOnly_m22186_gshared/* 230*/,
	(methodPointerType)&ArrayReadOnlyList_1__ctor_m22181_gshared/* 231*/,
	(methodPointerType)&ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m22182_gshared/* 232*/,
	(methodPointerType)&ArrayReadOnlyList_1_Add_m22187_gshared/* 233*/,
	(methodPointerType)&ArrayReadOnlyList_1_Clear_m22188_gshared/* 234*/,
	(methodPointerType)&ArrayReadOnlyList_1_Contains_m22189_gshared/* 235*/,
	(methodPointerType)&ArrayReadOnlyList_1_CopyTo_m22190_gshared/* 236*/,
	(methodPointerType)&ArrayReadOnlyList_1_GetEnumerator_m22191_gshared/* 237*/,
	(methodPointerType)&ArrayReadOnlyList_1_IndexOf_m22192_gshared/* 238*/,
	(methodPointerType)&ArrayReadOnlyList_1_Insert_m22193_gshared/* 239*/,
	(methodPointerType)&ArrayReadOnlyList_1_Remove_m22194_gshared/* 240*/,
	(methodPointerType)&ArrayReadOnlyList_1_RemoveAt_m22195_gshared/* 241*/,
	(methodPointerType)&ArrayReadOnlyList_1_ReadOnlyError_m22196_gshared/* 242*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m22198_gshared/* 243*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m22199_gshared/* 244*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0__ctor_m22197_gshared/* 245*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m22200_gshared/* 246*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_Dispose_m22201_gshared/* 247*/,
	(methodPointerType)&Comparer_1_get_Default_m14062_gshared/* 248*/,
	(methodPointerType)&Comparer_1__ctor_m14059_gshared/* 249*/,
	(methodPointerType)&Comparer_1__cctor_m14060_gshared/* 250*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m14061_gshared/* 251*/,
	(methodPointerType)&DefaultComparer__ctor_m14063_gshared/* 252*/,
	(methodPointerType)&DefaultComparer_Compare_m14064_gshared/* 253*/,
	(methodPointerType)&GenericComparer_1__ctor_m22232_gshared/* 254*/,
	(methodPointerType)&GenericComparer_1_Compare_m22233_gshared/* 255*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13559_gshared/* 256*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13561_gshared/* 257*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m13563_gshared/* 258*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m13565_gshared/* 259*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13573_gshared/* 260*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13575_gshared/* 261*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13577_gshared/* 262*/,
	(methodPointerType)&Dictionary_2_get_Count_m13595_gshared/* 263*/,
	(methodPointerType)&Dictionary_2_get_Item_m13597_gshared/* 264*/,
	(methodPointerType)&Dictionary_2_set_Item_m13599_gshared/* 265*/,
	(methodPointerType)&Dictionary_2_get_Keys_m13633_gshared/* 266*/,
	(methodPointerType)&Dictionary_2_get_Values_m13635_gshared/* 267*/,
	(methodPointerType)&Dictionary_2__ctor_m13547_gshared/* 268*/,
	(methodPointerType)&Dictionary_2__ctor_m13549_gshared/* 269*/,
	(methodPointerType)&Dictionary_2__ctor_m13551_gshared/* 270*/,
	(methodPointerType)&Dictionary_2__ctor_m13553_gshared/* 271*/,
	(methodPointerType)&Dictionary_2__ctor_m13555_gshared/* 272*/,
	(methodPointerType)&Dictionary_2__ctor_m13557_gshared/* 273*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m13567_gshared/* 274*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m13569_gshared/* 275*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m13571_gshared/* 276*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13579_gshared/* 277*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13581_gshared/* 278*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13583_gshared/* 279*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13585_gshared/* 280*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m13587_gshared/* 281*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13589_gshared/* 282*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13591_gshared/* 283*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13593_gshared/* 284*/,
	(methodPointerType)&Dictionary_2_Init_m13601_gshared/* 285*/,
	(methodPointerType)&Dictionary_2_InitArrays_m13603_gshared/* 286*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m13605_gshared/* 287*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m22613_gshared/* 288*/,
	(methodPointerType)&Dictionary_2_make_pair_m13607_gshared/* 289*/,
	(methodPointerType)&Dictionary_2_pick_key_m13609_gshared/* 290*/,
	(methodPointerType)&Dictionary_2_pick_value_m13611_gshared/* 291*/,
	(methodPointerType)&Dictionary_2_CopyTo_m13613_gshared/* 292*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m22614_gshared/* 293*/,
	(methodPointerType)&Dictionary_2_Resize_m13615_gshared/* 294*/,
	(methodPointerType)&Dictionary_2_Add_m13617_gshared/* 295*/,
	(methodPointerType)&Dictionary_2_Clear_m13619_gshared/* 296*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m13621_gshared/* 297*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m13623_gshared/* 298*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m13625_gshared/* 299*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m13627_gshared/* 300*/,
	(methodPointerType)&Dictionary_2_Remove_m13629_gshared/* 301*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m13631_gshared/* 302*/,
	(methodPointerType)&Dictionary_2_ToTKey_m13637_gshared/* 303*/,
	(methodPointerType)&Dictionary_2_ToTValue_m13639_gshared/* 304*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m13641_gshared/* 305*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m13643_gshared/* 306*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m13645_gshared/* 307*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m13741_gshared/* 308*/,
	(methodPointerType)&ShimEnumerator_get_Key_m13742_gshared/* 309*/,
	(methodPointerType)&ShimEnumerator_get_Value_m13743_gshared/* 310*/,
	(methodPointerType)&ShimEnumerator_get_Current_m13744_gshared/* 311*/,
	(methodPointerType)&ShimEnumerator__ctor_m13739_gshared/* 312*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m13740_gshared/* 313*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m13692_gshared/* 314*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13693_gshared/* 315*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13694_gshared/* 316*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13695_gshared/* 317*/,
	(methodPointerType)&Enumerator_get_Current_m13697_gshared/* 318*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m13698_gshared/* 319*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m13699_gshared/* 320*/,
	(methodPointerType)&Enumerator__ctor_m13691_gshared/* 321*/,
	(methodPointerType)&Enumerator_MoveNext_m13696_gshared/* 322*/,
	(methodPointerType)&Enumerator_VerifyState_m13700_gshared/* 323*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m13701_gshared/* 324*/,
	(methodPointerType)&Enumerator_Dispose_m13702_gshared/* 325*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m13680_gshared/* 326*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m13681_gshared/* 327*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m13682_gshared/* 328*/,
	(methodPointerType)&KeyCollection_get_Count_m13685_gshared/* 329*/,
	(methodPointerType)&KeyCollection__ctor_m13672_gshared/* 330*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m13673_gshared/* 331*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m13674_gshared/* 332*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m13675_gshared/* 333*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m13676_gshared/* 334*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m13677_gshared/* 335*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m13678_gshared/* 336*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m13679_gshared/* 337*/,
	(methodPointerType)&KeyCollection_CopyTo_m13683_gshared/* 338*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m13684_gshared/* 339*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m13687_gshared/* 340*/,
	(methodPointerType)&Enumerator_get_Current_m13690_gshared/* 341*/,
	(methodPointerType)&Enumerator__ctor_m13686_gshared/* 342*/,
	(methodPointerType)&Enumerator_Dispose_m13688_gshared/* 343*/,
	(methodPointerType)&Enumerator_MoveNext_m13689_gshared/* 344*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m13715_gshared/* 345*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m13716_gshared/* 346*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m13717_gshared/* 347*/,
	(methodPointerType)&ValueCollection_get_Count_m13720_gshared/* 348*/,
	(methodPointerType)&ValueCollection__ctor_m13707_gshared/* 349*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m13708_gshared/* 350*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m13709_gshared/* 351*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m13710_gshared/* 352*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m13711_gshared/* 353*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m13712_gshared/* 354*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m13713_gshared/* 355*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m13714_gshared/* 356*/,
	(methodPointerType)&ValueCollection_CopyTo_m13718_gshared/* 357*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m13719_gshared/* 358*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m13722_gshared/* 359*/,
	(methodPointerType)&Enumerator_get_Current_m13725_gshared/* 360*/,
	(methodPointerType)&Enumerator__ctor_m13721_gshared/* 361*/,
	(methodPointerType)&Enumerator_Dispose_m13723_gshared/* 362*/,
	(methodPointerType)&Enumerator_MoveNext_m13724_gshared/* 363*/,
	(methodPointerType)&Transform_1__ctor_m13703_gshared/* 364*/,
	(methodPointerType)&Transform_1_Invoke_m13704_gshared/* 365*/,
	(methodPointerType)&Transform_1_BeginInvoke_m13705_gshared/* 366*/,
	(methodPointerType)&Transform_1_EndInvoke_m13706_gshared/* 367*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m13749_gshared/* 368*/,
	(methodPointerType)&EqualityComparer_1__ctor_m13745_gshared/* 369*/,
	(methodPointerType)&EqualityComparer_1__cctor_m13746_gshared/* 370*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m13747_gshared/* 371*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m13748_gshared/* 372*/,
	(methodPointerType)&DefaultComparer__ctor_m13755_gshared/* 373*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m13756_gshared/* 374*/,
	(methodPointerType)&DefaultComparer_Equals_m13757_gshared/* 375*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m22234_gshared/* 376*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m22235_gshared/* 377*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m22236_gshared/* 378*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m13652_gshared/* 379*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m13653_gshared/* 380*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m13654_gshared/* 381*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m13655_gshared/* 382*/,
	(methodPointerType)&KeyValuePair_2__ctor_m13651_gshared/* 383*/,
	(methodPointerType)&KeyValuePair_2_ToString_m13656_gshared/* 384*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared/* 385*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared/* 386*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared/* 387*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared/* 388*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared/* 389*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m5411_gshared/* 390*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m5412_gshared/* 391*/,
	(methodPointerType)&List_1_get_Capacity_m13977_gshared/* 392*/,
	(methodPointerType)&List_1_set_Capacity_m13979_gshared/* 393*/,
	(methodPointerType)&List_1_get_Count_m5405_gshared/* 394*/,
	(methodPointerType)&List_1_get_Item_m5428_gshared/* 395*/,
	(methodPointerType)&List_1_set_Item_m5429_gshared/* 396*/,
	(methodPointerType)&List_1__ctor_m5170_gshared/* 397*/,
	(methodPointerType)&List_1__ctor_m13913_gshared/* 398*/,
	(methodPointerType)&List_1__ctor_m13915_gshared/* 399*/,
	(methodPointerType)&List_1__cctor_m13917_gshared/* 400*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared/* 401*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m5408_gshared/* 402*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared/* 403*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m5413_gshared/* 404*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m5415_gshared/* 405*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m5416_gshared/* 406*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m5417_gshared/* 407*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m5418_gshared/* 408*/,
	(methodPointerType)&List_1_Add_m5421_gshared/* 409*/,
	(methodPointerType)&List_1_GrowIfNeeded_m13935_gshared/* 410*/,
	(methodPointerType)&List_1_AddCollection_m13937_gshared/* 411*/,
	(methodPointerType)&List_1_AddEnumerable_m13939_gshared/* 412*/,
	(methodPointerType)&List_1_AddRange_m13941_gshared/* 413*/,
	(methodPointerType)&List_1_AsReadOnly_m13943_gshared/* 414*/,
	(methodPointerType)&List_1_Clear_m5414_gshared/* 415*/,
	(methodPointerType)&List_1_Contains_m5422_gshared/* 416*/,
	(methodPointerType)&List_1_CopyTo_m5423_gshared/* 417*/,
	(methodPointerType)&List_1_Find_m13948_gshared/* 418*/,
	(methodPointerType)&List_1_CheckMatch_m13950_gshared/* 419*/,
	(methodPointerType)&List_1_GetIndex_m13952_gshared/* 420*/,
	(methodPointerType)&List_1_GetEnumerator_m13954_gshared/* 421*/,
	(methodPointerType)&List_1_IndexOf_m5426_gshared/* 422*/,
	(methodPointerType)&List_1_Shift_m13957_gshared/* 423*/,
	(methodPointerType)&List_1_CheckIndex_m13959_gshared/* 424*/,
	(methodPointerType)&List_1_Insert_m5427_gshared/* 425*/,
	(methodPointerType)&List_1_CheckCollection_m13962_gshared/* 426*/,
	(methodPointerType)&List_1_Remove_m5424_gshared/* 427*/,
	(methodPointerType)&List_1_RemoveAll_m13965_gshared/* 428*/,
	(methodPointerType)&List_1_RemoveAt_m5419_gshared/* 429*/,
	(methodPointerType)&List_1_Reverse_m13968_gshared/* 430*/,
	(methodPointerType)&List_1_Sort_m13970_gshared/* 431*/,
	(methodPointerType)&List_1_Sort_m13972_gshared/* 432*/,
	(methodPointerType)&List_1_ToArray_m13973_gshared/* 433*/,
	(methodPointerType)&List_1_TrimExcess_m13975_gshared/* 434*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared/* 435*/,
	(methodPointerType)&Enumerator_get_Current_m13988_gshared/* 436*/,
	(methodPointerType)&Enumerator__ctor_m13983_gshared/* 437*/,
	(methodPointerType)&Enumerator_Dispose_m13985_gshared/* 438*/,
	(methodPointerType)&Enumerator_VerifyState_m13986_gshared/* 439*/,
	(methodPointerType)&Enumerator_MoveNext_m13987_gshared/* 440*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14020_gshared/* 441*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m14028_gshared/* 442*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m14029_gshared/* 443*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m14030_gshared/* 444*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m14031_gshared/* 445*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m14032_gshared/* 446*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m14033_gshared/* 447*/,
	(methodPointerType)&Collection_1_get_Count_m14046_gshared/* 448*/,
	(methodPointerType)&Collection_1_get_Item_m14047_gshared/* 449*/,
	(methodPointerType)&Collection_1_set_Item_m14048_gshared/* 450*/,
	(methodPointerType)&Collection_1__ctor_m14019_gshared/* 451*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m14021_gshared/* 452*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m14022_gshared/* 453*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m14023_gshared/* 454*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m14024_gshared/* 455*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m14025_gshared/* 456*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m14026_gshared/* 457*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m14027_gshared/* 458*/,
	(methodPointerType)&Collection_1_Add_m14034_gshared/* 459*/,
	(methodPointerType)&Collection_1_Clear_m14035_gshared/* 460*/,
	(methodPointerType)&Collection_1_ClearItems_m14036_gshared/* 461*/,
	(methodPointerType)&Collection_1_Contains_m14037_gshared/* 462*/,
	(methodPointerType)&Collection_1_CopyTo_m14038_gshared/* 463*/,
	(methodPointerType)&Collection_1_GetEnumerator_m14039_gshared/* 464*/,
	(methodPointerType)&Collection_1_IndexOf_m14040_gshared/* 465*/,
	(methodPointerType)&Collection_1_Insert_m14041_gshared/* 466*/,
	(methodPointerType)&Collection_1_InsertItem_m14042_gshared/* 467*/,
	(methodPointerType)&Collection_1_Remove_m14043_gshared/* 468*/,
	(methodPointerType)&Collection_1_RemoveAt_m14044_gshared/* 469*/,
	(methodPointerType)&Collection_1_RemoveItem_m14045_gshared/* 470*/,
	(methodPointerType)&Collection_1_SetItem_m14049_gshared/* 471*/,
	(methodPointerType)&Collection_1_IsValidItem_m14050_gshared/* 472*/,
	(methodPointerType)&Collection_1_ConvertItem_m14051_gshared/* 473*/,
	(methodPointerType)&Collection_1_CheckWritable_m14052_gshared/* 474*/,
	(methodPointerType)&Collection_1_IsSynchronized_m14053_gshared/* 475*/,
	(methodPointerType)&Collection_1_IsFixedSize_m14054_gshared/* 476*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m13995_gshared/* 477*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m13996_gshared/* 478*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13997_gshared/* 479*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14007_gshared/* 480*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14008_gshared/* 481*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14009_gshared/* 482*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14010_gshared/* 483*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m14011_gshared/* 484*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m14012_gshared/* 485*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m14017_gshared/* 486*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m14018_gshared/* 487*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m13989_gshared/* 488*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m13990_gshared/* 489*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m13991_gshared/* 490*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m13992_gshared/* 491*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m13993_gshared/* 492*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m13994_gshared/* 493*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m13998_gshared/* 494*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m13999_gshared/* 495*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m14000_gshared/* 496*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m14001_gshared/* 497*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m14002_gshared/* 498*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14003_gshared/* 499*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m14004_gshared/* 500*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m14005_gshared/* 501*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14006_gshared/* 502*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m14013_gshared/* 503*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m14014_gshared/* 504*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m14015_gshared/* 505*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m14016_gshared/* 506*/,
	(methodPointerType)&MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m23305_gshared/* 507*/,
	(methodPointerType)&MonoProperty_StaticGetterAdapterFrame_TisObject_t_m23306_gshared/* 508*/,
	(methodPointerType)&Getter_2__ctor_m22327_gshared/* 509*/,
	(methodPointerType)&Getter_2_Invoke_m22328_gshared/* 510*/,
	(methodPointerType)&Getter_2_BeginInvoke_m22329_gshared/* 511*/,
	(methodPointerType)&Getter_2_EndInvoke_m22330_gshared/* 512*/,
	(methodPointerType)&StaticGetter_1__ctor_m22331_gshared/* 513*/,
	(methodPointerType)&StaticGetter_1_Invoke_m22332_gshared/* 514*/,
	(methodPointerType)&StaticGetter_1_BeginInvoke_m22333_gshared/* 515*/,
	(methodPointerType)&StaticGetter_1_EndInvoke_m22334_gshared/* 516*/,
	(methodPointerType)&Activator_CreateInstance_TisObject_t_m22851_gshared/* 517*/,
	(methodPointerType)&Action_1__ctor_m17605_gshared/* 518*/,
	(methodPointerType)&Action_1_Invoke_m17607_gshared/* 519*/,
	(methodPointerType)&Action_1_BeginInvoke_m17609_gshared/* 520*/,
	(methodPointerType)&Action_1_EndInvoke_m17611_gshared/* 521*/,
	(methodPointerType)&Comparison_1__ctor_m14080_gshared/* 522*/,
	(methodPointerType)&Comparison_1_Invoke_m14081_gshared/* 523*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m14082_gshared/* 524*/,
	(methodPointerType)&Comparison_1_EndInvoke_m14083_gshared/* 525*/,
	(methodPointerType)&Converter_2__ctor_m22177_gshared/* 526*/,
	(methodPointerType)&Converter_2_Invoke_m22178_gshared/* 527*/,
	(methodPointerType)&Converter_2_BeginInvoke_m22179_gshared/* 528*/,
	(methodPointerType)&Converter_2_EndInvoke_m22180_gshared/* 529*/,
	(methodPointerType)&Predicate_1__ctor_m14055_gshared/* 530*/,
	(methodPointerType)&Predicate_1_Invoke_m14056_gshared/* 531*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m14057_gshared/* 532*/,
	(methodPointerType)&Predicate_1_EndInvoke_m14058_gshared/* 533*/,
	(methodPointerType)&List_1__ctor_m1597_gshared/* 534*/,
	(methodPointerType)&List_1__ctor_m1733_gshared/* 535*/,
	(methodPointerType)&Dictionary_2__ctor_m15470_gshared/* 536*/,
	(methodPointerType)&Dictionary_2__ctor_m15814_gshared/* 537*/,
	(methodPointerType)&Comparison_1__ctor_m3058_gshared/* 538*/,
	(methodPointerType)&List_1_Sort_m3061_gshared/* 539*/,
	(methodPointerType)&Dictionary_2__ctor_m5219_gshared/* 540*/,
	(methodPointerType)&Dictionary_2_get_Values_m17026_gshared/* 541*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m17094_gshared/* 542*/,
	(methodPointerType)&Enumerator_get_Current_m17100_gshared/* 543*/,
	(methodPointerType)&Enumerator_MoveNext_m17099_gshared/* 544*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m17033_gshared/* 545*/,
	(methodPointerType)&Enumerator_get_Current_m17072_gshared/* 546*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m17044_gshared/* 547*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m17042_gshared/* 548*/,
	(methodPointerType)&Enumerator_MoveNext_m17071_gshared/* 549*/,
	(methodPointerType)&KeyValuePair_2_ToString_m17046_gshared/* 550*/,
	(methodPointerType)&Comparison_1__ctor_m3148_gshared/* 551*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t24_m3145_gshared/* 552*/,
	(methodPointerType)&UnityEvent_1__ctor_m3149_gshared/* 553*/,
	(methodPointerType)&UnityEvent_1_Invoke_m3151_gshared/* 554*/,
	(methodPointerType)&UnityEvent_1_AddListener_m3152_gshared/* 555*/,
	(methodPointerType)&TweenRunner_1__ctor_m3174_gshared/* 556*/,
	(methodPointerType)&TweenRunner_1_Init_m3175_gshared/* 557*/,
	(methodPointerType)&UnityAction_1__ctor_m3197_gshared/* 558*/,
	(methodPointerType)&TweenRunner_1_StartTween_m3198_gshared/* 559*/,
	(methodPointerType)&List_1_get_Capacity_m3201_gshared/* 560*/,
	(methodPointerType)&List_1_set_Capacity_m3202_gshared/* 561*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisInt32_t253_m3222_gshared/* 562*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisByte_t680_m3224_gshared/* 563*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSingle_t254_m3226_gshared/* 564*/,
	(methodPointerType)&List_1__ctor_m3276_gshared/* 565*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisUInt16_t684_m3272_gshared/* 566*/,
	(methodPointerType)&List_1_ToArray_m3323_gshared/* 567*/,
	(methodPointerType)&UnityEvent_1__ctor_m3346_gshared/* 568*/,
	(methodPointerType)&UnityEvent_1_Invoke_m3351_gshared/* 569*/,
	(methodPointerType)&UnityEvent_1__ctor_m3356_gshared/* 570*/,
	(methodPointerType)&UnityAction_1__ctor_m3357_gshared/* 571*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m3358_gshared/* 572*/,
	(methodPointerType)&UnityEvent_1_AddListener_m3359_gshared/* 573*/,
	(methodPointerType)&UnityEvent_1_Invoke_m3364_gshared/* 574*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisNavigation_t563_m3378_gshared/* 575*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisColorBlock_t516_m3380_gshared/* 576*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSpriteState_t580_m3381_gshared/* 577*/,
	(methodPointerType)&UnityEvent_1__ctor_m18546_gshared/* 578*/,
	(methodPointerType)&UnityEvent_1_Invoke_m18555_gshared/* 579*/,
	(methodPointerType)&Func_2__ctor_m18659_gshared/* 580*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisInt32_t253_m3428_gshared/* 581*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisVector2_t6_m3430_gshared/* 582*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisSingle_t254_m3435_gshared/* 583*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisByte_t680_m3437_gshared/* 584*/,
	(methodPointerType)&Func_2__ctor_m18773_gshared/* 585*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m3599_gshared/* 586*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m3600_gshared/* 587*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m3607_gshared/* 588*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m3608_gshared/* 589*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m3609_gshared/* 590*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m3610_gshared/* 591*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m18551_gshared/* 592*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m18552_gshared/* 593*/,
	(methodPointerType)&List_1__ctor_m5144_gshared/* 594*/,
	(methodPointerType)&List_1__ctor_m5145_gshared/* 595*/,
	(methodPointerType)&List_1__ctor_m5146_gshared/* 596*/,
	(methodPointerType)&Dictionary_2__ctor_m20292_gshared/* 597*/,
	(methodPointerType)&Dictionary_2__ctor_m21026_gshared/* 598*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m5240_gshared/* 599*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m5241_gshared/* 600*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m21543_gshared/* 601*/,
	(methodPointerType)&Dictionary_2__ctor_m21741_gshared/* 602*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t253_m6510_gshared/* 603*/,
	(methodPointerType)&GenericComparer_1__ctor_m12638_gshared/* 604*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m12639_gshared/* 605*/,
	(methodPointerType)&GenericComparer_1__ctor_m12640_gshared/* 606*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m12641_gshared/* 607*/,
	(methodPointerType)&Nullable_1__ctor_m12642_gshared/* 608*/,
	(methodPointerType)&Nullable_1_get_HasValue_m12643_gshared/* 609*/,
	(methodPointerType)&Nullable_1_get_Value_m12644_gshared/* 610*/,
	(methodPointerType)&GenericComparer_1__ctor_m12645_gshared/* 611*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m12646_gshared/* 612*/,
	(methodPointerType)&GenericComparer_1__ctor_m12647_gshared/* 613*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m12648_gshared/* 614*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit_t24_m22557_gshared/* 615*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit_t24_m22558_gshared/* 616*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit_t24_m22559_gshared/* 617*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t24_m22560_gshared/* 618*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit_t24_m22561_gshared/* 619*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit_t24_m22562_gshared/* 620*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit_t24_m22563_gshared/* 621*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit_t24_m22565_gshared/* 622*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t24_m22566_gshared/* 623*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSingle_t254_m22570_gshared/* 624*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSingle_t254_m22571_gshared/* 625*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSingle_t254_m22572_gshared/* 626*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSingle_t254_m22573_gshared/* 627*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSingle_t254_m22574_gshared/* 628*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSingle_t254_m22575_gshared/* 629*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSingle_t254_m22576_gshared/* 630*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSingle_t254_m22578_gshared/* 631*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t254_m22579_gshared/* 632*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2815_m22581_gshared/* 633*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2815_m22582_gshared/* 634*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2815_m22583_gshared/* 635*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2815_m22584_gshared/* 636*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2815_m22585_gshared/* 637*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2815_m22586_gshared/* 638*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2815_m22587_gshared/* 639*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2815_m22589_gshared/* 640*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2815_m22590_gshared/* 641*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt32_t253_m22592_gshared/* 642*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt32_t253_m22593_gshared/* 643*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt32_t253_m22594_gshared/* 644*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt32_t253_m22595_gshared/* 645*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt32_t253_m22596_gshared/* 646*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt32_t253_m22597_gshared/* 647*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt32_t253_m22598_gshared/* 648*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt32_t253_m22600_gshared/* 649*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t253_m22601_gshared/* 650*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLink_t1757_m22603_gshared/* 651*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLink_t1757_m22604_gshared/* 652*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLink_t1757_m22605_gshared/* 653*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLink_t1757_m22606_gshared/* 654*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLink_t1757_m22607_gshared/* 655*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLink_t1757_m22608_gshared/* 656*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLink_t1757_m22609_gshared/* 657*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLink_t1757_m22611_gshared/* 658*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t1757_m22612_gshared/* 659*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDictionaryEntry_t1430_m22616_gshared/* 660*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDictionaryEntry_t1430_m22617_gshared/* 661*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t1430_m22618_gshared/* 662*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t1430_m22619_gshared/* 663*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t1430_m22620_gshared/* 664*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDictionaryEntry_t1430_m22621_gshared/* 665*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDictionaryEntry_t1430_m22622_gshared/* 666*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDictionaryEntry_t1430_m22624_gshared/* 667*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t1430_m22625_gshared/* 668*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m22626_gshared/* 669*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2815_m22628_gshared/* 670*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2815_TisObject_t_m22627_gshared/* 671*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2815_TisKeyValuePair_2_t2815_m22629_gshared/* 672*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTouch_t235_m22631_gshared/* 673*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTouch_t235_m22632_gshared/* 674*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTouch_t235_m22633_gshared/* 675*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTouch_t235_m22634_gshared/* 676*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTouch_t235_m22635_gshared/* 677*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTouch_t235_m22636_gshared/* 678*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTouch_t235_m22637_gshared/* 679*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTouch_t235_m22639_gshared/* 680*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTouch_t235_m22640_gshared/* 681*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDouble_t1070_m22648_gshared/* 682*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDouble_t1070_m22649_gshared/* 683*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDouble_t1070_m22650_gshared/* 684*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDouble_t1070_m22651_gshared/* 685*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDouble_t1070_m22652_gshared/* 686*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDouble_t1070_m22653_gshared/* 687*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDouble_t1070_m22654_gshared/* 688*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDouble_t1070_m22656_gshared/* 689*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t1070_m22657_gshared/* 690*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt16_t684_m22659_gshared/* 691*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt16_t684_m22660_gshared/* 692*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt16_t684_m22661_gshared/* 693*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt16_t684_m22662_gshared/* 694*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt16_t684_m22663_gshared/* 695*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt16_t684_m22664_gshared/* 696*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt16_t684_m22665_gshared/* 697*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt16_t684_m22667_gshared/* 698*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t684_m22668_gshared/* 699*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector2_t6_m22675_gshared/* 700*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector2_t6_m22676_gshared/* 701*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector2_t6_m22677_gshared/* 702*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector2_t6_m22678_gshared/* 703*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector2_t6_m22679_gshared/* 704*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector2_t6_m22680_gshared/* 705*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector2_t6_m22681_gshared/* 706*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector2_t6_m22683_gshared/* 707*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t6_m22684_gshared/* 708*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyframe_t240_m22686_gshared/* 709*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyframe_t240_m22687_gshared/* 710*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyframe_t240_m22688_gshared/* 711*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyframe_t240_m22689_gshared/* 712*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyframe_t240_m22690_gshared/* 713*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyframe_t240_m22691_gshared/* 714*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyframe_t240_m22692_gshared/* 715*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyframe_t240_m22694_gshared/* 716*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t240_m22695_gshared/* 717*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisColor_t65_m22697_gshared/* 718*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisColor_t65_m22698_gshared/* 719*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisColor_t65_m22699_gshared/* 720*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisColor_t65_m22700_gshared/* 721*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisColor_t65_m22701_gshared/* 722*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisColor_t65_m22702_gshared/* 723*/,
	(methodPointerType)&Array_InternalArray__Insert_TisColor_t65_m22703_gshared/* 724*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisColor_t65_m22705_gshared/* 725*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisColor_t65_m22706_gshared/* 726*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector3_t4_m22708_gshared/* 727*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector3_t4_m22709_gshared/* 728*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector3_t4_m22710_gshared/* 729*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector3_t4_m22711_gshared/* 730*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector3_t4_m22712_gshared/* 731*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector3_t4_m22713_gshared/* 732*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector3_t4_m22714_gshared/* 733*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector3_t4_m22716_gshared/* 734*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t4_m22717_gshared/* 735*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisContactPoint_t246_m22719_gshared/* 736*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisContactPoint_t246_m22720_gshared/* 737*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisContactPoint_t246_m22721_gshared/* 738*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisContactPoint_t246_m22722_gshared/* 739*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisContactPoint_t246_m22723_gshared/* 740*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisContactPoint_t246_m22724_gshared/* 741*/,
	(methodPointerType)&Array_InternalArray__Insert_TisContactPoint_t246_m22725_gshared/* 742*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisContactPoint_t246_m22727_gshared/* 743*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisContactPoint_t246_m22728_gshared/* 744*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisParticleCollisionEvent_t160_m22730_gshared/* 745*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisParticleCollisionEvent_t160_m22731_gshared/* 746*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisParticleCollisionEvent_t160_m22732_gshared/* 747*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisParticleCollisionEvent_t160_m22733_gshared/* 748*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisParticleCollisionEvent_t160_m22734_gshared/* 749*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisParticleCollisionEvent_t160_m22735_gshared/* 750*/,
	(methodPointerType)&Array_InternalArray__Insert_TisParticleCollisionEvent_t160_m22736_gshared/* 751*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisParticleCollisionEvent_t160_m22738_gshared/* 752*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisParticleCollisionEvent_t160_m22739_gshared/* 753*/,
	(methodPointerType)&Array_Resize_TisVector2_t6_m22742_gshared/* 754*/,
	(methodPointerType)&Array_Resize_TisVector2_t6_m22741_gshared/* 755*/,
	(methodPointerType)&Array_IndexOf_TisVector2_t6_m22743_gshared/* 756*/,
	(methodPointerType)&Array_Sort_TisVector2_t6_m22745_gshared/* 757*/,
	(methodPointerType)&Array_Sort_TisVector2_t6_TisVector2_t6_m22744_gshared/* 758*/,
	(methodPointerType)&Array_get_swapper_TisVector2_t6_m22746_gshared/* 759*/,
	(methodPointerType)&Array_qsort_TisVector2_t6_TisVector2_t6_m22747_gshared/* 760*/,
	(methodPointerType)&Array_compare_TisVector2_t6_m22748_gshared/* 761*/,
	(methodPointerType)&Array_swap_TisVector2_t6_TisVector2_t6_m22749_gshared/* 762*/,
	(methodPointerType)&Array_Sort_TisVector2_t6_m22751_gshared/* 763*/,
	(methodPointerType)&Array_qsort_TisVector2_t6_m22750_gshared/* 764*/,
	(methodPointerType)&Array_swap_TisVector2_t6_m22752_gshared/* 765*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastResult_t486_m22754_gshared/* 766*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastResult_t486_m22755_gshared/* 767*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastResult_t486_m22756_gshared/* 768*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t486_m22757_gshared/* 769*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastResult_t486_m22758_gshared/* 770*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastResult_t486_m22759_gshared/* 771*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastResult_t486_m22760_gshared/* 772*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastResult_t486_m22762_gshared/* 773*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t486_m22763_gshared/* 774*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t486_m22765_gshared/* 775*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t486_m22764_gshared/* 776*/,
	(methodPointerType)&Array_IndexOf_TisRaycastResult_t486_m22766_gshared/* 777*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t486_m22768_gshared/* 778*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t486_TisRaycastResult_t486_m22767_gshared/* 779*/,
	(methodPointerType)&Array_get_swapper_TisRaycastResult_t486_m22769_gshared/* 780*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t486_TisRaycastResult_t486_m22770_gshared/* 781*/,
	(methodPointerType)&Array_compare_TisRaycastResult_t486_m22771_gshared/* 782*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t486_TisRaycastResult_t486_m22772_gshared/* 783*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t486_m22774_gshared/* 784*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t486_m22773_gshared/* 785*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t486_m22775_gshared/* 786*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisQuaternion_t19_m22777_gshared/* 787*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisQuaternion_t19_m22778_gshared/* 788*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisQuaternion_t19_m22779_gshared/* 789*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisQuaternion_t19_m22780_gshared/* 790*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisQuaternion_t19_m22781_gshared/* 791*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisQuaternion_t19_m22782_gshared/* 792*/,
	(methodPointerType)&Array_InternalArray__Insert_TisQuaternion_t19_m22783_gshared/* 793*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisQuaternion_t19_m22785_gshared/* 794*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisQuaternion_t19_m22786_gshared/* 795*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2953_m22788_gshared/* 796*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2953_m22789_gshared/* 797*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2953_m22790_gshared/* 798*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2953_m22791_gshared/* 799*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2953_m22792_gshared/* 800*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2953_m22793_gshared/* 801*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2953_m22794_gshared/* 802*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2953_m22796_gshared/* 803*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2953_m22797_gshared/* 804*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m22799_gshared/* 805*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m22798_gshared/* 806*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t253_m22801_gshared/* 807*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t253_TisObject_t_m22800_gshared/* 808*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t253_TisInt32_t253_m22802_gshared/* 809*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m22803_gshared/* 810*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2953_m22805_gshared/* 811*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2953_TisObject_t_m22804_gshared/* 812*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2953_TisKeyValuePair_2_t2953_m22806_gshared/* 813*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2984_m22808_gshared/* 814*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2984_m22809_gshared/* 815*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2984_m22810_gshared/* 816*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2984_m22811_gshared/* 817*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2984_m22812_gshared/* 818*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2984_m22813_gshared/* 819*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2984_m22814_gshared/* 820*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2984_m22816_gshared/* 821*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2984_m22817_gshared/* 822*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisByte_t680_m22819_gshared/* 823*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisByte_t680_m22820_gshared/* 824*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisByte_t680_m22821_gshared/* 825*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisByte_t680_m22822_gshared/* 826*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisByte_t680_m22823_gshared/* 827*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisByte_t680_m22824_gshared/* 828*/,
	(methodPointerType)&Array_InternalArray__Insert_TisByte_t680_m22825_gshared/* 829*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisByte_t680_m22827_gshared/* 830*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t680_m22828_gshared/* 831*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t253_m22830_gshared/* 832*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t253_TisObject_t_m22829_gshared/* 833*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t253_TisInt32_t253_m22831_gshared/* 834*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisByte_t680_m22833_gshared/* 835*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisByte_t680_TisObject_t_m22832_gshared/* 836*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisByte_t680_TisByte_t680_m22834_gshared/* 837*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m22835_gshared/* 838*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2984_m22837_gshared/* 839*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2984_TisObject_t_m22836_gshared/* 840*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2984_TisKeyValuePair_2_t2984_m22838_gshared/* 841*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisAnimatorClipInfo_t888_m22840_gshared/* 842*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisAnimatorClipInfo_t888_m22841_gshared/* 843*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisAnimatorClipInfo_t888_m22842_gshared/* 844*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisAnimatorClipInfo_t888_m22843_gshared/* 845*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisAnimatorClipInfo_t888_m22844_gshared/* 846*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisAnimatorClipInfo_t888_m22845_gshared/* 847*/,
	(methodPointerType)&Array_InternalArray__Insert_TisAnimatorClipInfo_t888_m22846_gshared/* 848*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisAnimatorClipInfo_t888_m22848_gshared/* 849*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisAnimatorClipInfo_t888_m22849_gshared/* 850*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3059_m22858_gshared/* 851*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3059_m22859_gshared/* 852*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3059_m22860_gshared/* 853*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3059_m22861_gshared/* 854*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3059_m22862_gshared/* 855*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3059_m22863_gshared/* 856*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3059_m22864_gshared/* 857*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3059_m22866_gshared/* 858*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3059_m22867_gshared/* 859*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t253_m22869_gshared/* 860*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t253_TisObject_t_m22868_gshared/* 861*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t253_TisInt32_t253_m22870_gshared/* 862*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m22872_gshared/* 863*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m22871_gshared/* 864*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m22873_gshared/* 865*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3059_m22875_gshared/* 866*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3059_TisObject_t_m22874_gshared/* 867*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3059_TisKeyValuePair_2_t3059_m22876_gshared/* 868*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit2D_t665_m22878_gshared/* 869*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit2D_t665_m22879_gshared/* 870*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t665_m22880_gshared/* 871*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t665_m22881_gshared/* 872*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t665_m22882_gshared/* 873*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit2D_t665_m22883_gshared/* 874*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit2D_t665_m22884_gshared/* 875*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit2D_t665_m22886_gshared/* 876*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t665_m22887_gshared/* 877*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t24_m22888_gshared/* 878*/,
	(methodPointerType)&Array_qsort_TisRaycastHit_t24_m22889_gshared/* 879*/,
	(methodPointerType)&Array_swap_TisRaycastHit_t24_m22890_gshared/* 880*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisColor_t65_m22891_gshared/* 881*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t556_m22893_gshared/* 882*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t556_m22892_gshared/* 883*/,
	(methodPointerType)&Array_IndexOf_TisUIVertex_t556_m22894_gshared/* 884*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t556_m22896_gshared/* 885*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t556_TisUIVertex_t556_m22895_gshared/* 886*/,
	(methodPointerType)&Array_get_swapper_TisUIVertex_t556_m22897_gshared/* 887*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t556_TisUIVertex_t556_m22898_gshared/* 888*/,
	(methodPointerType)&Array_compare_TisUIVertex_t556_m22899_gshared/* 889*/,
	(methodPointerType)&Array_swap_TisUIVertex_t556_TisUIVertex_t556_m22900_gshared/* 890*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t556_m22902_gshared/* 891*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t556_m22901_gshared/* 892*/,
	(methodPointerType)&Array_swap_TisUIVertex_t556_m22903_gshared/* 893*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUILineInfo_t687_m22905_gshared/* 894*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUILineInfo_t687_m22906_gshared/* 895*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUILineInfo_t687_m22907_gshared/* 896*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t687_m22908_gshared/* 897*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUILineInfo_t687_m22909_gshared/* 898*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUILineInfo_t687_m22910_gshared/* 899*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUILineInfo_t687_m22911_gshared/* 900*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUILineInfo_t687_m22913_gshared/* 901*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t687_m22914_gshared/* 902*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUICharInfo_t689_m22916_gshared/* 903*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUICharInfo_t689_m22917_gshared/* 904*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUICharInfo_t689_m22918_gshared/* 905*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t689_m22919_gshared/* 906*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUICharInfo_t689_m22920_gshared/* 907*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUICharInfo_t689_m22921_gshared/* 908*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUICharInfo_t689_m22922_gshared/* 909*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUICharInfo_t689_m22924_gshared/* 910*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t689_m22925_gshared/* 911*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t254_m22926_gshared/* 912*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t6_m22927_gshared/* 913*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisByte_t680_m22928_gshared/* 914*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcAchievementData_t958_m22934_gshared/* 915*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcAchievementData_t958_m22935_gshared/* 916*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcAchievementData_t958_m22936_gshared/* 917*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t958_m22937_gshared/* 918*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcAchievementData_t958_m22938_gshared/* 919*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcAchievementData_t958_m22939_gshared/* 920*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcAchievementData_t958_m22940_gshared/* 921*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcAchievementData_t958_m22942_gshared/* 922*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t958_m22943_gshared/* 923*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcScoreData_t959_m22945_gshared/* 924*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcScoreData_t959_m22946_gshared/* 925*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcScoreData_t959_m22947_gshared/* 926*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t959_m22948_gshared/* 927*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcScoreData_t959_m22949_gshared/* 928*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcScoreData_t959_m22950_gshared/* 929*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcScoreData_t959_m22951_gshared/* 930*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcScoreData_t959_m22953_gshared/* 931*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t959_m22954_gshared/* 932*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisIntPtr_t_m22956_gshared/* 933*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisIntPtr_t_m22957_gshared/* 934*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisIntPtr_t_m22958_gshared/* 935*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m22959_gshared/* 936*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisIntPtr_t_m22960_gshared/* 937*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisIntPtr_t_m22961_gshared/* 938*/,
	(methodPointerType)&Array_InternalArray__Insert_TisIntPtr_t_m22962_gshared/* 939*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisIntPtr_t_m22964_gshared/* 940*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m22965_gshared/* 941*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t689_m22967_gshared/* 942*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t689_m22966_gshared/* 943*/,
	(methodPointerType)&Array_IndexOf_TisUICharInfo_t689_m22968_gshared/* 944*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t689_m22970_gshared/* 945*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t689_TisUICharInfo_t689_m22969_gshared/* 946*/,
	(methodPointerType)&Array_get_swapper_TisUICharInfo_t689_m22971_gshared/* 947*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t689_TisUICharInfo_t689_m22972_gshared/* 948*/,
	(methodPointerType)&Array_compare_TisUICharInfo_t689_m22973_gshared/* 949*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t689_TisUICharInfo_t689_m22974_gshared/* 950*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t689_m22976_gshared/* 951*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t689_m22975_gshared/* 952*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t689_m22977_gshared/* 953*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t687_m22979_gshared/* 954*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t687_m22978_gshared/* 955*/,
	(methodPointerType)&Array_IndexOf_TisUILineInfo_t687_m22980_gshared/* 956*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t687_m22982_gshared/* 957*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t687_TisUILineInfo_t687_m22981_gshared/* 958*/,
	(methodPointerType)&Array_get_swapper_TisUILineInfo_t687_m22983_gshared/* 959*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t687_TisUILineInfo_t687_m22984_gshared/* 960*/,
	(methodPointerType)&Array_compare_TisUILineInfo_t687_m22985_gshared/* 961*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t687_TisUILineInfo_t687_m22986_gshared/* 962*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t687_m22988_gshared/* 963*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t687_m22987_gshared/* 964*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t687_m22989_gshared/* 965*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3255_m22991_gshared/* 966*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3255_m22992_gshared/* 967*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3255_m22993_gshared/* 968*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3255_m22994_gshared/* 969*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3255_m22995_gshared/* 970*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3255_m22996_gshared/* 971*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3255_m22997_gshared/* 972*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3255_m22999_gshared/* 973*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3255_m23000_gshared/* 974*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt64_t1071_m23002_gshared/* 975*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt64_t1071_m23003_gshared/* 976*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt64_t1071_m23004_gshared/* 977*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt64_t1071_m23005_gshared/* 978*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt64_t1071_m23006_gshared/* 979*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt64_t1071_m23007_gshared/* 980*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt64_t1071_m23008_gshared/* 981*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt64_t1071_m23010_gshared/* 982*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t1071_m23011_gshared/* 983*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m23013_gshared/* 984*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m23012_gshared/* 985*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt64_t1071_m23015_gshared/* 986*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt64_t1071_TisObject_t_m23014_gshared/* 987*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt64_t1071_TisInt64_t1071_m23016_gshared/* 988*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m23017_gshared/* 989*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3255_m23019_gshared/* 990*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3255_TisObject_t_m23018_gshared/* 991*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3255_TisKeyValuePair_2_t3255_m23020_gshared/* 992*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3293_m23022_gshared/* 993*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3293_m23023_gshared/* 994*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3293_m23024_gshared/* 995*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3293_m23025_gshared/* 996*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3293_m23026_gshared/* 997*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3293_m23027_gshared/* 998*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3293_m23028_gshared/* 999*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3293_m23030_gshared/* 1000*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3293_m23031_gshared/* 1001*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt64_t1074_m23033_gshared/* 1002*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt64_t1074_m23034_gshared/* 1003*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt64_t1074_m23035_gshared/* 1004*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt64_t1074_m23036_gshared/* 1005*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt64_t1074_m23037_gshared/* 1006*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt64_t1074_m23038_gshared/* 1007*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt64_t1074_m23039_gshared/* 1008*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt64_t1074_m23041_gshared/* 1009*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t1074_m23042_gshared/* 1010*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisUInt64_t1074_m23044_gshared/* 1011*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt64_t1074_TisObject_t_m23043_gshared/* 1012*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt64_t1074_TisUInt64_t1074_m23045_gshared/* 1013*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m23047_gshared/* 1014*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m23046_gshared/* 1015*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m23048_gshared/* 1016*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3293_m23050_gshared/* 1017*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3293_TisObject_t_m23049_gshared/* 1018*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3293_TisKeyValuePair_2_t3293_m23051_gshared/* 1019*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3314_m23053_gshared/* 1020*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3314_m23054_gshared/* 1021*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3314_m23055_gshared/* 1022*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3314_m23056_gshared/* 1023*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3314_m23057_gshared/* 1024*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3314_m23058_gshared/* 1025*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3314_m23059_gshared/* 1026*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3314_m23061_gshared/* 1027*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3314_m23062_gshared/* 1028*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m23064_gshared/* 1029*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m23063_gshared/* 1030*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2815_m23066_gshared/* 1031*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2815_TisObject_t_m23065_gshared/* 1032*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2815_TisKeyValuePair_2_t2815_m23067_gshared/* 1033*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m23068_gshared/* 1034*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3314_m23070_gshared/* 1035*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3314_TisObject_t_m23069_gshared/* 1036*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3314_TisKeyValuePair_2_t3314_m23071_gshared/* 1037*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisParameterModifier_t1914_m23073_gshared/* 1038*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisParameterModifier_t1914_m23074_gshared/* 1039*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisParameterModifier_t1914_m23075_gshared/* 1040*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t1914_m23076_gshared/* 1041*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisParameterModifier_t1914_m23077_gshared/* 1042*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisParameterModifier_t1914_m23078_gshared/* 1043*/,
	(methodPointerType)&Array_InternalArray__Insert_TisParameterModifier_t1914_m23079_gshared/* 1044*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisParameterModifier_t1914_m23081_gshared/* 1045*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t1914_m23082_gshared/* 1046*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisHitInfo_t983_m23084_gshared/* 1047*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisHitInfo_t983_m23085_gshared/* 1048*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisHitInfo_t983_m23086_gshared/* 1049*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisHitInfo_t983_m23087_gshared/* 1050*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisHitInfo_t983_m23088_gshared/* 1051*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisHitInfo_t983_m23089_gshared/* 1052*/,
	(methodPointerType)&Array_InternalArray__Insert_TisHitInfo_t983_m23090_gshared/* 1053*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisHitInfo_t983_m23092_gshared/* 1054*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t983_m23093_gshared/* 1055*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t253_m23094_gshared/* 1056*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3407_m23096_gshared/* 1057*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3407_m23097_gshared/* 1058*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3407_m23098_gshared/* 1059*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3407_m23099_gshared/* 1060*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3407_m23100_gshared/* 1061*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3407_m23101_gshared/* 1062*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3407_m23102_gshared/* 1063*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3407_m23104_gshared/* 1064*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3407_m23105_gshared/* 1065*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m23107_gshared/* 1066*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m23106_gshared/* 1067*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisByte_t680_m23109_gshared/* 1068*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisByte_t680_TisObject_t_m23108_gshared/* 1069*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisByte_t680_TisByte_t680_m23110_gshared/* 1070*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m23111_gshared/* 1071*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3407_m23113_gshared/* 1072*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3407_TisObject_t_m23112_gshared/* 1073*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3407_TisKeyValuePair_2_t3407_m23114_gshared/* 1074*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisX509ChainStatus_t1331_m23116_gshared/* 1075*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisX509ChainStatus_t1331_m23117_gshared/* 1076*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t1331_m23118_gshared/* 1077*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t1331_m23119_gshared/* 1078*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t1331_m23120_gshared/* 1079*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisX509ChainStatus_t1331_m23121_gshared/* 1080*/,
	(methodPointerType)&Array_InternalArray__Insert_TisX509ChainStatus_t1331_m23122_gshared/* 1081*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisX509ChainStatus_t1331_m23124_gshared/* 1082*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t1331_m23125_gshared/* 1083*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3427_m23127_gshared/* 1084*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3427_m23128_gshared/* 1085*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3427_m23129_gshared/* 1086*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3427_m23130_gshared/* 1087*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3427_m23131_gshared/* 1088*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3427_m23132_gshared/* 1089*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3427_m23133_gshared/* 1090*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3427_m23135_gshared/* 1091*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3427_m23136_gshared/* 1092*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t253_m23138_gshared/* 1093*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t253_TisObject_t_m23137_gshared/* 1094*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t253_TisInt32_t253_m23139_gshared/* 1095*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1430_TisDictionaryEntry_t1430_m23140_gshared/* 1096*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3427_m23142_gshared/* 1097*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3427_TisObject_t_m23141_gshared/* 1098*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3427_TisKeyValuePair_2_t3427_m23143_gshared/* 1099*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t253_m23144_gshared/* 1100*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisMark_t1382_m23146_gshared/* 1101*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisMark_t1382_m23147_gshared/* 1102*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisMark_t1382_m23148_gshared/* 1103*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisMark_t1382_m23149_gshared/* 1104*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisMark_t1382_m23150_gshared/* 1105*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisMark_t1382_m23151_gshared/* 1106*/,
	(methodPointerType)&Array_InternalArray__Insert_TisMark_t1382_m23152_gshared/* 1107*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisMark_t1382_m23154_gshared/* 1108*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t1382_m23155_gshared/* 1109*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUriScheme_t1418_m23157_gshared/* 1110*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUriScheme_t1418_m23158_gshared/* 1111*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUriScheme_t1418_m23159_gshared/* 1112*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUriScheme_t1418_m23160_gshared/* 1113*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUriScheme_t1418_m23161_gshared/* 1114*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUriScheme_t1418_m23162_gshared/* 1115*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUriScheme_t1418_m23163_gshared/* 1116*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUriScheme_t1418_m23165_gshared/* 1117*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t1418_m23166_gshared/* 1118*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt32_t1063_m23168_gshared/* 1119*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt32_t1063_m23169_gshared/* 1120*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt32_t1063_m23170_gshared/* 1121*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt32_t1063_m23171_gshared/* 1122*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt32_t1063_m23172_gshared/* 1123*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt32_t1063_m23173_gshared/* 1124*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt32_t1063_m23174_gshared/* 1125*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt32_t1063_m23176_gshared/* 1126*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t1063_m23177_gshared/* 1127*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt16_t752_m23179_gshared/* 1128*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt16_t752_m23180_gshared/* 1129*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt16_t752_m23181_gshared/* 1130*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt16_t752_m23182_gshared/* 1131*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt16_t752_m23183_gshared/* 1132*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt16_t752_m23184_gshared/* 1133*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt16_t752_m23185_gshared/* 1134*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt16_t752_m23187_gshared/* 1135*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t752_m23188_gshared/* 1136*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSByte_t274_m23190_gshared/* 1137*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSByte_t274_m23191_gshared/* 1138*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSByte_t274_m23192_gshared/* 1139*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSByte_t274_m23193_gshared/* 1140*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSByte_t274_m23194_gshared/* 1141*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSByte_t274_m23195_gshared/* 1142*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSByte_t274_m23196_gshared/* 1143*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSByte_t274_m23198_gshared/* 1144*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t274_m23199_gshared/* 1145*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTableRange_t1690_m23229_gshared/* 1146*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTableRange_t1690_m23230_gshared/* 1147*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTableRange_t1690_m23231_gshared/* 1148*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTableRange_t1690_m23232_gshared/* 1149*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTableRange_t1690_m23233_gshared/* 1150*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTableRange_t1690_m23234_gshared/* 1151*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTableRange_t1690_m23235_gshared/* 1152*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTableRange_t1690_m23237_gshared/* 1153*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1690_m23238_gshared/* 1154*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t1767_m23240_gshared/* 1155*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t1767_m23241_gshared/* 1156*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t1767_m23242_gshared/* 1157*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t1767_m23243_gshared/* 1158*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t1767_m23244_gshared/* 1159*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t1767_m23245_gshared/* 1160*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t1767_m23246_gshared/* 1161*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t1767_m23248_gshared/* 1162*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1767_m23249_gshared/* 1163*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t1776_m23251_gshared/* 1164*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t1776_m23252_gshared/* 1165*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t1776_m23253_gshared/* 1166*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t1776_m23254_gshared/* 1167*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t1776_m23255_gshared/* 1168*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t1776_m23256_gshared/* 1169*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t1776_m23257_gshared/* 1170*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t1776_m23259_gshared/* 1171*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1776_m23260_gshared/* 1172*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisMonoResource_t1838_m23262_gshared/* 1173*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisMonoResource_t1838_m23263_gshared/* 1174*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisMonoResource_t1838_m23264_gshared/* 1175*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisMonoResource_t1838_m23265_gshared/* 1176*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisMonoResource_t1838_m23266_gshared/* 1177*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisMonoResource_t1838_m23267_gshared/* 1178*/,
	(methodPointerType)&Array_InternalArray__Insert_TisMonoResource_t1838_m23268_gshared/* 1179*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisMonoResource_t1838_m23270_gshared/* 1180*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisMonoResource_t1838_m23271_gshared/* 1181*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisILTokenInfo_t1856_m23273_gshared/* 1182*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisILTokenInfo_t1856_m23274_gshared/* 1183*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisILTokenInfo_t1856_m23275_gshared/* 1184*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisILTokenInfo_t1856_m23276_gshared/* 1185*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisILTokenInfo_t1856_m23277_gshared/* 1186*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisILTokenInfo_t1856_m23278_gshared/* 1187*/,
	(methodPointerType)&Array_InternalArray__Insert_TisILTokenInfo_t1856_m23279_gshared/* 1188*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisILTokenInfo_t1856_m23281_gshared/* 1189*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisILTokenInfo_t1856_m23282_gshared/* 1190*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLabelData_t1858_m23284_gshared/* 1191*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLabelData_t1858_m23285_gshared/* 1192*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLabelData_t1858_m23286_gshared/* 1193*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLabelData_t1858_m23287_gshared/* 1194*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLabelData_t1858_m23288_gshared/* 1195*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLabelData_t1858_m23289_gshared/* 1196*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLabelData_t1858_m23290_gshared/* 1197*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLabelData_t1858_m23292_gshared/* 1198*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t1858_m23293_gshared/* 1199*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLabelFixup_t1857_m23295_gshared/* 1200*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLabelFixup_t1857_m23296_gshared/* 1201*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLabelFixup_t1857_m23297_gshared/* 1202*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t1857_m23298_gshared/* 1203*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLabelFixup_t1857_m23299_gshared/* 1204*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLabelFixup_t1857_m23300_gshared/* 1205*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLabelFixup_t1857_m23301_gshared/* 1206*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLabelFixup_t1857_m23303_gshared/* 1207*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t1857_m23304_gshared/* 1208*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDateTime_t406_m23308_gshared/* 1209*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDateTime_t406_m23309_gshared/* 1210*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDateTime_t406_m23310_gshared/* 1211*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDateTime_t406_m23311_gshared/* 1212*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDateTime_t406_m23312_gshared/* 1213*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDateTime_t406_m23313_gshared/* 1214*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDateTime_t406_m23314_gshared/* 1215*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDateTime_t406_m23316_gshared/* 1216*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t406_m23317_gshared/* 1217*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDecimal_t1073_m23319_gshared/* 1218*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDecimal_t1073_m23320_gshared/* 1219*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDecimal_t1073_m23321_gshared/* 1220*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDecimal_t1073_m23322_gshared/* 1221*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDecimal_t1073_m23323_gshared/* 1222*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDecimal_t1073_m23324_gshared/* 1223*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDecimal_t1073_m23325_gshared/* 1224*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDecimal_t1073_m23327_gshared/* 1225*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1073_m23328_gshared/* 1226*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTimeSpan_t1337_m23330_gshared/* 1227*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTimeSpan_t1337_m23331_gshared/* 1228*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTimeSpan_t1337_m23332_gshared/* 1229*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t1337_m23333_gshared/* 1230*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTimeSpan_t1337_m23334_gshared/* 1231*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTimeSpan_t1337_m23335_gshared/* 1232*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTimeSpan_t1337_m23336_gshared/* 1233*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTimeSpan_t1337_m23338_gshared/* 1234*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t1337_m23339_gshared/* 1235*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13522_gshared/* 1236*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13523_gshared/* 1237*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13524_gshared/* 1238*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13525_gshared/* 1239*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13526_gshared/* 1240*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13537_gshared/* 1241*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13538_gshared/* 1242*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13539_gshared/* 1243*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13540_gshared/* 1244*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13541_gshared/* 1245*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13646_gshared/* 1246*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13647_gshared/* 1247*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13648_gshared/* 1248*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13649_gshared/* 1249*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13650_gshared/* 1250*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13662_gshared/* 1251*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13663_gshared/* 1252*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13664_gshared/* 1253*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13665_gshared/* 1254*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13666_gshared/* 1255*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13667_gshared/* 1256*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13668_gshared/* 1257*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13669_gshared/* 1258*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13670_gshared/* 1259*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13671_gshared/* 1260*/,
	(methodPointerType)&Transform_1__ctor_m13726_gshared/* 1261*/,
	(methodPointerType)&Transform_1_Invoke_m13727_gshared/* 1262*/,
	(methodPointerType)&Transform_1_BeginInvoke_m13728_gshared/* 1263*/,
	(methodPointerType)&Transform_1_EndInvoke_m13729_gshared/* 1264*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13730_gshared/* 1265*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13731_gshared/* 1266*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13732_gshared/* 1267*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13733_gshared/* 1268*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13734_gshared/* 1269*/,
	(methodPointerType)&Transform_1__ctor_m13735_gshared/* 1270*/,
	(methodPointerType)&Transform_1_Invoke_m13736_gshared/* 1271*/,
	(methodPointerType)&Transform_1_BeginInvoke_m13737_gshared/* 1272*/,
	(methodPointerType)&Transform_1_EndInvoke_m13738_gshared/* 1273*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13907_gshared/* 1274*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13908_gshared/* 1275*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13909_gshared/* 1276*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13910_gshared/* 1277*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13911_gshared/* 1278*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14065_gshared/* 1279*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14066_gshared/* 1280*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14067_gshared/* 1281*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14068_gshared/* 1282*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14069_gshared/* 1283*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14071_gshared/* 1284*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14073_gshared/* 1285*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14075_gshared/* 1286*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14077_gshared/* 1287*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14079_gshared/* 1288*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14128_gshared/* 1289*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14129_gshared/* 1290*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14130_gshared/* 1291*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14131_gshared/* 1292*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14132_gshared/* 1293*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14133_gshared/* 1294*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14134_gshared/* 1295*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14135_gshared/* 1296*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14136_gshared/* 1297*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14137_gshared/* 1298*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14138_gshared/* 1299*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14139_gshared/* 1300*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14140_gshared/* 1301*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14141_gshared/* 1302*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14142_gshared/* 1303*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14153_gshared/* 1304*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14154_gshared/* 1305*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14155_gshared/* 1306*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14156_gshared/* 1307*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14157_gshared/* 1308*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14253_gshared/* 1309*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14254_gshared/* 1310*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14255_gshared/* 1311*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14256_gshared/* 1312*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14257_gshared/* 1313*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14268_gshared/* 1314*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14269_gshared/* 1315*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14270_gshared/* 1316*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14271_gshared/* 1317*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14272_gshared/* 1318*/,
	(methodPointerType)&List_1__ctor_m14955_gshared/* 1319*/,
	(methodPointerType)&List_1__ctor_m14956_gshared/* 1320*/,
	(methodPointerType)&List_1__cctor_m14957_gshared/* 1321*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14958_gshared/* 1322*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m14959_gshared/* 1323*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m14960_gshared/* 1324*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m14961_gshared/* 1325*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m14962_gshared/* 1326*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m14963_gshared/* 1327*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m14964_gshared/* 1328*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m14965_gshared/* 1329*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14966_gshared/* 1330*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m14967_gshared/* 1331*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m14968_gshared/* 1332*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m14969_gshared/* 1333*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m14970_gshared/* 1334*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m14971_gshared/* 1335*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m14972_gshared/* 1336*/,
	(methodPointerType)&List_1_Add_m14973_gshared/* 1337*/,
	(methodPointerType)&List_1_GrowIfNeeded_m14974_gshared/* 1338*/,
	(methodPointerType)&List_1_AddCollection_m14975_gshared/* 1339*/,
	(methodPointerType)&List_1_AddEnumerable_m14976_gshared/* 1340*/,
	(methodPointerType)&List_1_AddRange_m14977_gshared/* 1341*/,
	(methodPointerType)&List_1_AsReadOnly_m14978_gshared/* 1342*/,
	(methodPointerType)&List_1_Clear_m14979_gshared/* 1343*/,
	(methodPointerType)&List_1_Contains_m14980_gshared/* 1344*/,
	(methodPointerType)&List_1_CopyTo_m14981_gshared/* 1345*/,
	(methodPointerType)&List_1_Find_m14982_gshared/* 1346*/,
	(methodPointerType)&List_1_CheckMatch_m14983_gshared/* 1347*/,
	(methodPointerType)&List_1_GetIndex_m14984_gshared/* 1348*/,
	(methodPointerType)&List_1_GetEnumerator_m14985_gshared/* 1349*/,
	(methodPointerType)&List_1_IndexOf_m14986_gshared/* 1350*/,
	(methodPointerType)&List_1_Shift_m14987_gshared/* 1351*/,
	(methodPointerType)&List_1_CheckIndex_m14988_gshared/* 1352*/,
	(methodPointerType)&List_1_Insert_m14989_gshared/* 1353*/,
	(methodPointerType)&List_1_CheckCollection_m14990_gshared/* 1354*/,
	(methodPointerType)&List_1_Remove_m14991_gshared/* 1355*/,
	(methodPointerType)&List_1_RemoveAll_m14992_gshared/* 1356*/,
	(methodPointerType)&List_1_RemoveAt_m14993_gshared/* 1357*/,
	(methodPointerType)&List_1_Reverse_m14994_gshared/* 1358*/,
	(methodPointerType)&List_1_Sort_m14995_gshared/* 1359*/,
	(methodPointerType)&List_1_Sort_m14996_gshared/* 1360*/,
	(methodPointerType)&List_1_ToArray_m14997_gshared/* 1361*/,
	(methodPointerType)&List_1_TrimExcess_m14998_gshared/* 1362*/,
	(methodPointerType)&List_1_get_Capacity_m14999_gshared/* 1363*/,
	(methodPointerType)&List_1_set_Capacity_m15000_gshared/* 1364*/,
	(methodPointerType)&List_1_get_Count_m15001_gshared/* 1365*/,
	(methodPointerType)&List_1_get_Item_m15002_gshared/* 1366*/,
	(methodPointerType)&List_1_set_Item_m15003_gshared/* 1367*/,
	(methodPointerType)&Enumerator__ctor_m15004_gshared/* 1368*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15005_gshared/* 1369*/,
	(methodPointerType)&Enumerator_Dispose_m15006_gshared/* 1370*/,
	(methodPointerType)&Enumerator_VerifyState_m15007_gshared/* 1371*/,
	(methodPointerType)&Enumerator_MoveNext_m15008_gshared/* 1372*/,
	(methodPointerType)&Enumerator_get_Current_m15009_gshared/* 1373*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m15010_gshared/* 1374*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15011_gshared/* 1375*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15012_gshared/* 1376*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15013_gshared/* 1377*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15014_gshared/* 1378*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15015_gshared/* 1379*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15016_gshared/* 1380*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15017_gshared/* 1381*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15018_gshared/* 1382*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15019_gshared/* 1383*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15020_gshared/* 1384*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m15021_gshared/* 1385*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m15022_gshared/* 1386*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m15023_gshared/* 1387*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15024_gshared/* 1388*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m15025_gshared/* 1389*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m15026_gshared/* 1390*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15027_gshared/* 1391*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15028_gshared/* 1392*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15029_gshared/* 1393*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15030_gshared/* 1394*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15031_gshared/* 1395*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m15032_gshared/* 1396*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m15033_gshared/* 1397*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m15034_gshared/* 1398*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m15035_gshared/* 1399*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m15036_gshared/* 1400*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m15037_gshared/* 1401*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m15038_gshared/* 1402*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m15039_gshared/* 1403*/,
	(methodPointerType)&Collection_1__ctor_m15040_gshared/* 1404*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15041_gshared/* 1405*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m15042_gshared/* 1406*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m15043_gshared/* 1407*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m15044_gshared/* 1408*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m15045_gshared/* 1409*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m15046_gshared/* 1410*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m15047_gshared/* 1411*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m15048_gshared/* 1412*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m15049_gshared/* 1413*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m15050_gshared/* 1414*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m15051_gshared/* 1415*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m15052_gshared/* 1416*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m15053_gshared/* 1417*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m15054_gshared/* 1418*/,
	(methodPointerType)&Collection_1_Add_m15055_gshared/* 1419*/,
	(methodPointerType)&Collection_1_Clear_m15056_gshared/* 1420*/,
	(methodPointerType)&Collection_1_ClearItems_m15057_gshared/* 1421*/,
	(methodPointerType)&Collection_1_Contains_m15058_gshared/* 1422*/,
	(methodPointerType)&Collection_1_CopyTo_m15059_gshared/* 1423*/,
	(methodPointerType)&Collection_1_GetEnumerator_m15060_gshared/* 1424*/,
	(methodPointerType)&Collection_1_IndexOf_m15061_gshared/* 1425*/,
	(methodPointerType)&Collection_1_Insert_m15062_gshared/* 1426*/,
	(methodPointerType)&Collection_1_InsertItem_m15063_gshared/* 1427*/,
	(methodPointerType)&Collection_1_Remove_m15064_gshared/* 1428*/,
	(methodPointerType)&Collection_1_RemoveAt_m15065_gshared/* 1429*/,
	(methodPointerType)&Collection_1_RemoveItem_m15066_gshared/* 1430*/,
	(methodPointerType)&Collection_1_get_Count_m15067_gshared/* 1431*/,
	(methodPointerType)&Collection_1_get_Item_m15068_gshared/* 1432*/,
	(methodPointerType)&Collection_1_set_Item_m15069_gshared/* 1433*/,
	(methodPointerType)&Collection_1_SetItem_m15070_gshared/* 1434*/,
	(methodPointerType)&Collection_1_IsValidItem_m15071_gshared/* 1435*/,
	(methodPointerType)&Collection_1_ConvertItem_m15072_gshared/* 1436*/,
	(methodPointerType)&Collection_1_CheckWritable_m15073_gshared/* 1437*/,
	(methodPointerType)&Collection_1_IsSynchronized_m15074_gshared/* 1438*/,
	(methodPointerType)&Collection_1_IsFixedSize_m15075_gshared/* 1439*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15076_gshared/* 1440*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15077_gshared/* 1441*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15078_gshared/* 1442*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15079_gshared/* 1443*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15080_gshared/* 1444*/,
	(methodPointerType)&DefaultComparer__ctor_m15081_gshared/* 1445*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15082_gshared/* 1446*/,
	(methodPointerType)&DefaultComparer_Equals_m15083_gshared/* 1447*/,
	(methodPointerType)&Predicate_1__ctor_m15084_gshared/* 1448*/,
	(methodPointerType)&Predicate_1_Invoke_m15085_gshared/* 1449*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m15086_gshared/* 1450*/,
	(methodPointerType)&Predicate_1_EndInvoke_m15087_gshared/* 1451*/,
	(methodPointerType)&Comparer_1__ctor_m15088_gshared/* 1452*/,
	(methodPointerType)&Comparer_1__cctor_m15089_gshared/* 1453*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m15090_gshared/* 1454*/,
	(methodPointerType)&Comparer_1_get_Default_m15091_gshared/* 1455*/,
	(methodPointerType)&DefaultComparer__ctor_m15092_gshared/* 1456*/,
	(methodPointerType)&DefaultComparer_Compare_m15093_gshared/* 1457*/,
	(methodPointerType)&Comparison_1__ctor_m15094_gshared/* 1458*/,
	(methodPointerType)&Comparison_1_Invoke_m15095_gshared/* 1459*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m15096_gshared/* 1460*/,
	(methodPointerType)&Comparison_1_EndInvoke_m15097_gshared/* 1461*/,
	(methodPointerType)&List_1__ctor_m15208_gshared/* 1462*/,
	(methodPointerType)&List_1__ctor_m15209_gshared/* 1463*/,
	(methodPointerType)&List_1__cctor_m15210_gshared/* 1464*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15211_gshared/* 1465*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m15212_gshared/* 1466*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m15213_gshared/* 1467*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m15214_gshared/* 1468*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m15215_gshared/* 1469*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m15216_gshared/* 1470*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m15217_gshared/* 1471*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m15218_gshared/* 1472*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15219_gshared/* 1473*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m15220_gshared/* 1474*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m15221_gshared/* 1475*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m15222_gshared/* 1476*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m15223_gshared/* 1477*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m15224_gshared/* 1478*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m15225_gshared/* 1479*/,
	(methodPointerType)&List_1_Add_m15226_gshared/* 1480*/,
	(methodPointerType)&List_1_GrowIfNeeded_m15227_gshared/* 1481*/,
	(methodPointerType)&List_1_AddCollection_m15228_gshared/* 1482*/,
	(methodPointerType)&List_1_AddEnumerable_m15229_gshared/* 1483*/,
	(methodPointerType)&List_1_AddRange_m15230_gshared/* 1484*/,
	(methodPointerType)&List_1_AsReadOnly_m15231_gshared/* 1485*/,
	(methodPointerType)&List_1_Clear_m15232_gshared/* 1486*/,
	(methodPointerType)&List_1_Contains_m15233_gshared/* 1487*/,
	(methodPointerType)&List_1_CopyTo_m15234_gshared/* 1488*/,
	(methodPointerType)&List_1_Find_m15235_gshared/* 1489*/,
	(methodPointerType)&List_1_CheckMatch_m15236_gshared/* 1490*/,
	(methodPointerType)&List_1_GetIndex_m15237_gshared/* 1491*/,
	(methodPointerType)&List_1_GetEnumerator_m15238_gshared/* 1492*/,
	(methodPointerType)&List_1_IndexOf_m15239_gshared/* 1493*/,
	(methodPointerType)&List_1_Shift_m15240_gshared/* 1494*/,
	(methodPointerType)&List_1_CheckIndex_m15241_gshared/* 1495*/,
	(methodPointerType)&List_1_Insert_m15242_gshared/* 1496*/,
	(methodPointerType)&List_1_CheckCollection_m15243_gshared/* 1497*/,
	(methodPointerType)&List_1_Remove_m15244_gshared/* 1498*/,
	(methodPointerType)&List_1_RemoveAll_m15245_gshared/* 1499*/,
	(methodPointerType)&List_1_RemoveAt_m15246_gshared/* 1500*/,
	(methodPointerType)&List_1_Reverse_m15247_gshared/* 1501*/,
	(methodPointerType)&List_1_Sort_m15248_gshared/* 1502*/,
	(methodPointerType)&List_1_ToArray_m15249_gshared/* 1503*/,
	(methodPointerType)&List_1_TrimExcess_m15250_gshared/* 1504*/,
	(methodPointerType)&List_1_get_Capacity_m15251_gshared/* 1505*/,
	(methodPointerType)&List_1_set_Capacity_m15252_gshared/* 1506*/,
	(methodPointerType)&List_1_get_Count_m15253_gshared/* 1507*/,
	(methodPointerType)&List_1_get_Item_m15254_gshared/* 1508*/,
	(methodPointerType)&List_1_set_Item_m15255_gshared/* 1509*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15256_gshared/* 1510*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15257_gshared/* 1511*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15258_gshared/* 1512*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15259_gshared/* 1513*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15260_gshared/* 1514*/,
	(methodPointerType)&Enumerator__ctor_m15261_gshared/* 1515*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15262_gshared/* 1516*/,
	(methodPointerType)&Enumerator_Dispose_m15263_gshared/* 1517*/,
	(methodPointerType)&Enumerator_VerifyState_m15264_gshared/* 1518*/,
	(methodPointerType)&Enumerator_MoveNext_m15265_gshared/* 1519*/,
	(methodPointerType)&Enumerator_get_Current_m15266_gshared/* 1520*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m15267_gshared/* 1521*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15268_gshared/* 1522*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15269_gshared/* 1523*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15270_gshared/* 1524*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15271_gshared/* 1525*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15272_gshared/* 1526*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15273_gshared/* 1527*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15274_gshared/* 1528*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15275_gshared/* 1529*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15276_gshared/* 1530*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15277_gshared/* 1531*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m15278_gshared/* 1532*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m15279_gshared/* 1533*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m15280_gshared/* 1534*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15281_gshared/* 1535*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m15282_gshared/* 1536*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m15283_gshared/* 1537*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15284_gshared/* 1538*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15285_gshared/* 1539*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15286_gshared/* 1540*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15287_gshared/* 1541*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15288_gshared/* 1542*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m15289_gshared/* 1543*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m15290_gshared/* 1544*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m15291_gshared/* 1545*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m15292_gshared/* 1546*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m15293_gshared/* 1547*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m15294_gshared/* 1548*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m15295_gshared/* 1549*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m15296_gshared/* 1550*/,
	(methodPointerType)&Collection_1__ctor_m15297_gshared/* 1551*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15298_gshared/* 1552*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m15299_gshared/* 1553*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m15300_gshared/* 1554*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m15301_gshared/* 1555*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m15302_gshared/* 1556*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m15303_gshared/* 1557*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m15304_gshared/* 1558*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m15305_gshared/* 1559*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m15306_gshared/* 1560*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m15307_gshared/* 1561*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m15308_gshared/* 1562*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m15309_gshared/* 1563*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m15310_gshared/* 1564*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m15311_gshared/* 1565*/,
	(methodPointerType)&Collection_1_Add_m15312_gshared/* 1566*/,
	(methodPointerType)&Collection_1_Clear_m15313_gshared/* 1567*/,
	(methodPointerType)&Collection_1_ClearItems_m15314_gshared/* 1568*/,
	(methodPointerType)&Collection_1_Contains_m15315_gshared/* 1569*/,
	(methodPointerType)&Collection_1_CopyTo_m15316_gshared/* 1570*/,
	(methodPointerType)&Collection_1_GetEnumerator_m15317_gshared/* 1571*/,
	(methodPointerType)&Collection_1_IndexOf_m15318_gshared/* 1572*/,
	(methodPointerType)&Collection_1_Insert_m15319_gshared/* 1573*/,
	(methodPointerType)&Collection_1_InsertItem_m15320_gshared/* 1574*/,
	(methodPointerType)&Collection_1_Remove_m15321_gshared/* 1575*/,
	(methodPointerType)&Collection_1_RemoveAt_m15322_gshared/* 1576*/,
	(methodPointerType)&Collection_1_RemoveItem_m15323_gshared/* 1577*/,
	(methodPointerType)&Collection_1_get_Count_m15324_gshared/* 1578*/,
	(methodPointerType)&Collection_1_get_Item_m15325_gshared/* 1579*/,
	(methodPointerType)&Collection_1_set_Item_m15326_gshared/* 1580*/,
	(methodPointerType)&Collection_1_SetItem_m15327_gshared/* 1581*/,
	(methodPointerType)&Collection_1_IsValidItem_m15328_gshared/* 1582*/,
	(methodPointerType)&Collection_1_ConvertItem_m15329_gshared/* 1583*/,
	(methodPointerType)&Collection_1_CheckWritable_m15330_gshared/* 1584*/,
	(methodPointerType)&Collection_1_IsSynchronized_m15331_gshared/* 1585*/,
	(methodPointerType)&Collection_1_IsFixedSize_m15332_gshared/* 1586*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15333_gshared/* 1587*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15334_gshared/* 1588*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15335_gshared/* 1589*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15336_gshared/* 1590*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15337_gshared/* 1591*/,
	(methodPointerType)&DefaultComparer__ctor_m15338_gshared/* 1592*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15339_gshared/* 1593*/,
	(methodPointerType)&DefaultComparer_Equals_m15340_gshared/* 1594*/,
	(methodPointerType)&Predicate_1__ctor_m15341_gshared/* 1595*/,
	(methodPointerType)&Predicate_1_Invoke_m15342_gshared/* 1596*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m15343_gshared/* 1597*/,
	(methodPointerType)&Predicate_1_EndInvoke_m15344_gshared/* 1598*/,
	(methodPointerType)&Comparer_1__ctor_m15345_gshared/* 1599*/,
	(methodPointerType)&Comparer_1__cctor_m15346_gshared/* 1600*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m15347_gshared/* 1601*/,
	(methodPointerType)&Comparer_1_get_Default_m15348_gshared/* 1602*/,
	(methodPointerType)&DefaultComparer__ctor_m15349_gshared/* 1603*/,
	(methodPointerType)&DefaultComparer_Compare_m15350_gshared/* 1604*/,
	(methodPointerType)&Comparison_1_Invoke_m15351_gshared/* 1605*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m15352_gshared/* 1606*/,
	(methodPointerType)&Comparison_1_EndInvoke_m15353_gshared/* 1607*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15459_gshared/* 1608*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460_gshared/* 1609*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15461_gshared/* 1610*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15462_gshared/* 1611*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15463_gshared/* 1612*/,
	(methodPointerType)&Dictionary_2__ctor_m15465_gshared/* 1613*/,
	(methodPointerType)&Dictionary_2__ctor_m15467_gshared/* 1614*/,
	(methodPointerType)&Dictionary_2__ctor_m15469_gshared/* 1615*/,
	(methodPointerType)&Dictionary_2__ctor_m15472_gshared/* 1616*/,
	(methodPointerType)&Dictionary_2__ctor_m15474_gshared/* 1617*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15476_gshared/* 1618*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15478_gshared/* 1619*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m15480_gshared/* 1620*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m15482_gshared/* 1621*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m15484_gshared/* 1622*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m15486_gshared/* 1623*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m15488_gshared/* 1624*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15490_gshared/* 1625*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15492_gshared/* 1626*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15494_gshared/* 1627*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15496_gshared/* 1628*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15498_gshared/* 1629*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15500_gshared/* 1630*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15502_gshared/* 1631*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m15504_gshared/* 1632*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15506_gshared/* 1633*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15508_gshared/* 1634*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15510_gshared/* 1635*/,
	(methodPointerType)&Dictionary_2_get_Count_m15512_gshared/* 1636*/,
	(methodPointerType)&Dictionary_2_get_Item_m15514_gshared/* 1637*/,
	(methodPointerType)&Dictionary_2_set_Item_m15516_gshared/* 1638*/,
	(methodPointerType)&Dictionary_2_Init_m15518_gshared/* 1639*/,
	(methodPointerType)&Dictionary_2_InitArrays_m15520_gshared/* 1640*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m15522_gshared/* 1641*/,
	(methodPointerType)&Dictionary_2_make_pair_m15524_gshared/* 1642*/,
	(methodPointerType)&Dictionary_2_pick_key_m15526_gshared/* 1643*/,
	(methodPointerType)&Dictionary_2_pick_value_m15528_gshared/* 1644*/,
	(methodPointerType)&Dictionary_2_CopyTo_m15530_gshared/* 1645*/,
	(methodPointerType)&Dictionary_2_Resize_m15532_gshared/* 1646*/,
	(methodPointerType)&Dictionary_2_Add_m15534_gshared/* 1647*/,
	(methodPointerType)&Dictionary_2_Clear_m15536_gshared/* 1648*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m15538_gshared/* 1649*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m15540_gshared/* 1650*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m15542_gshared/* 1651*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m15544_gshared/* 1652*/,
	(methodPointerType)&Dictionary_2_Remove_m15546_gshared/* 1653*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m15548_gshared/* 1654*/,
	(methodPointerType)&Dictionary_2_get_Keys_m15550_gshared/* 1655*/,
	(methodPointerType)&Dictionary_2_get_Values_m15552_gshared/* 1656*/,
	(methodPointerType)&Dictionary_2_ToTKey_m15554_gshared/* 1657*/,
	(methodPointerType)&Dictionary_2_ToTValue_m15556_gshared/* 1658*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m15558_gshared/* 1659*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m15560_gshared/* 1660*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m15562_gshared/* 1661*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15563_gshared/* 1662*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15564_gshared/* 1663*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15565_gshared/* 1664*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15566_gshared/* 1665*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15567_gshared/* 1666*/,
	(methodPointerType)&KeyValuePair_2__ctor_m15568_gshared/* 1667*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m15569_gshared/* 1668*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m15570_gshared/* 1669*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m15571_gshared/* 1670*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m15572_gshared/* 1671*/,
	(methodPointerType)&KeyValuePair_2_ToString_m15573_gshared/* 1672*/,
	(methodPointerType)&KeyCollection__ctor_m15574_gshared/* 1673*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15575_gshared/* 1674*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15576_gshared/* 1675*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15577_gshared/* 1676*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15578_gshared/* 1677*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15579_gshared/* 1678*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m15580_gshared/* 1679*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15581_gshared/* 1680*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15582_gshared/* 1681*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15583_gshared/* 1682*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m15584_gshared/* 1683*/,
	(methodPointerType)&KeyCollection_CopyTo_m15585_gshared/* 1684*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m15586_gshared/* 1685*/,
	(methodPointerType)&KeyCollection_get_Count_m15587_gshared/* 1686*/,
	(methodPointerType)&Enumerator__ctor_m15588_gshared/* 1687*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15589_gshared/* 1688*/,
	(methodPointerType)&Enumerator_Dispose_m15590_gshared/* 1689*/,
	(methodPointerType)&Enumerator_MoveNext_m15591_gshared/* 1690*/,
	(methodPointerType)&Enumerator_get_Current_m15592_gshared/* 1691*/,
	(methodPointerType)&Enumerator__ctor_m15593_gshared/* 1692*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15594_gshared/* 1693*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15595_gshared/* 1694*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15596_gshared/* 1695*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15597_gshared/* 1696*/,
	(methodPointerType)&Enumerator_MoveNext_m15598_gshared/* 1697*/,
	(methodPointerType)&Enumerator_get_Current_m15599_gshared/* 1698*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m15600_gshared/* 1699*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m15601_gshared/* 1700*/,
	(methodPointerType)&Enumerator_VerifyState_m15602_gshared/* 1701*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m15603_gshared/* 1702*/,
	(methodPointerType)&Enumerator_Dispose_m15604_gshared/* 1703*/,
	(methodPointerType)&Transform_1__ctor_m15605_gshared/* 1704*/,
	(methodPointerType)&Transform_1_Invoke_m15606_gshared/* 1705*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15607_gshared/* 1706*/,
	(methodPointerType)&Transform_1_EndInvoke_m15608_gshared/* 1707*/,
	(methodPointerType)&ValueCollection__ctor_m15609_gshared/* 1708*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15610_gshared/* 1709*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15611_gshared/* 1710*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15612_gshared/* 1711*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15613_gshared/* 1712*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15614_gshared/* 1713*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m15615_gshared/* 1714*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15616_gshared/* 1715*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15617_gshared/* 1716*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15618_gshared/* 1717*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m15619_gshared/* 1718*/,
	(methodPointerType)&ValueCollection_CopyTo_m15620_gshared/* 1719*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m15621_gshared/* 1720*/,
	(methodPointerType)&ValueCollection_get_Count_m15622_gshared/* 1721*/,
	(methodPointerType)&Enumerator__ctor_m15623_gshared/* 1722*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15624_gshared/* 1723*/,
	(methodPointerType)&Enumerator_Dispose_m15625_gshared/* 1724*/,
	(methodPointerType)&Enumerator_MoveNext_m15626_gshared/* 1725*/,
	(methodPointerType)&Enumerator_get_Current_m15627_gshared/* 1726*/,
	(methodPointerType)&Transform_1__ctor_m15628_gshared/* 1727*/,
	(methodPointerType)&Transform_1_Invoke_m15629_gshared/* 1728*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15630_gshared/* 1729*/,
	(methodPointerType)&Transform_1_EndInvoke_m15631_gshared/* 1730*/,
	(methodPointerType)&Transform_1__ctor_m15632_gshared/* 1731*/,
	(methodPointerType)&Transform_1_Invoke_m15633_gshared/* 1732*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15634_gshared/* 1733*/,
	(methodPointerType)&Transform_1_EndInvoke_m15635_gshared/* 1734*/,
	(methodPointerType)&Transform_1__ctor_m15636_gshared/* 1735*/,
	(methodPointerType)&Transform_1_Invoke_m15637_gshared/* 1736*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15638_gshared/* 1737*/,
	(methodPointerType)&Transform_1_EndInvoke_m15639_gshared/* 1738*/,
	(methodPointerType)&ShimEnumerator__ctor_m15640_gshared/* 1739*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m15641_gshared/* 1740*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m15642_gshared/* 1741*/,
	(methodPointerType)&ShimEnumerator_get_Key_m15643_gshared/* 1742*/,
	(methodPointerType)&ShimEnumerator_get_Value_m15644_gshared/* 1743*/,
	(methodPointerType)&ShimEnumerator_get_Current_m15645_gshared/* 1744*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15646_gshared/* 1745*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15647_gshared/* 1746*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15648_gshared/* 1747*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15649_gshared/* 1748*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15650_gshared/* 1749*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m15651_gshared/* 1750*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m15652_gshared/* 1751*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m15653_gshared/* 1752*/,
	(methodPointerType)&DefaultComparer__ctor_m15654_gshared/* 1753*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15655_gshared/* 1754*/,
	(methodPointerType)&DefaultComparer_Equals_m15656_gshared/* 1755*/,
	(methodPointerType)&Dictionary_2__ctor_m15816_gshared/* 1756*/,
	(methodPointerType)&Dictionary_2__ctor_m15818_gshared/* 1757*/,
	(methodPointerType)&Dictionary_2__ctor_m15820_gshared/* 1758*/,
	(methodPointerType)&Dictionary_2__ctor_m15822_gshared/* 1759*/,
	(methodPointerType)&Dictionary_2__ctor_m15824_gshared/* 1760*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15826_gshared/* 1761*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15828_gshared/* 1762*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m15830_gshared/* 1763*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m15832_gshared/* 1764*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m15834_gshared/* 1765*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m15836_gshared/* 1766*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m15838_gshared/* 1767*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15840_gshared/* 1768*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15842_gshared/* 1769*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15844_gshared/* 1770*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15846_gshared/* 1771*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15848_gshared/* 1772*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15850_gshared/* 1773*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15852_gshared/* 1774*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m15854_gshared/* 1775*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15856_gshared/* 1776*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15858_gshared/* 1777*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15860_gshared/* 1778*/,
	(methodPointerType)&Dictionary_2_get_Count_m15862_gshared/* 1779*/,
	(methodPointerType)&Dictionary_2_get_Item_m15864_gshared/* 1780*/,
	(methodPointerType)&Dictionary_2_set_Item_m15866_gshared/* 1781*/,
	(methodPointerType)&Dictionary_2_Init_m15868_gshared/* 1782*/,
	(methodPointerType)&Dictionary_2_InitArrays_m15870_gshared/* 1783*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m15872_gshared/* 1784*/,
	(methodPointerType)&Dictionary_2_make_pair_m15874_gshared/* 1785*/,
	(methodPointerType)&Dictionary_2_pick_key_m15876_gshared/* 1786*/,
	(methodPointerType)&Dictionary_2_pick_value_m15878_gshared/* 1787*/,
	(methodPointerType)&Dictionary_2_CopyTo_m15880_gshared/* 1788*/,
	(methodPointerType)&Dictionary_2_Resize_m15882_gshared/* 1789*/,
	(methodPointerType)&Dictionary_2_Add_m15884_gshared/* 1790*/,
	(methodPointerType)&Dictionary_2_Clear_m15886_gshared/* 1791*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m15888_gshared/* 1792*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m15890_gshared/* 1793*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m15892_gshared/* 1794*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m15894_gshared/* 1795*/,
	(methodPointerType)&Dictionary_2_Remove_m15896_gshared/* 1796*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m15898_gshared/* 1797*/,
	(methodPointerType)&Dictionary_2_get_Keys_m15900_gshared/* 1798*/,
	(methodPointerType)&Dictionary_2_get_Values_m15902_gshared/* 1799*/,
	(methodPointerType)&Dictionary_2_ToTKey_m15904_gshared/* 1800*/,
	(methodPointerType)&Dictionary_2_ToTValue_m15906_gshared/* 1801*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m15908_gshared/* 1802*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m15910_gshared/* 1803*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m15912_gshared/* 1804*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15913_gshared/* 1805*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15914_gshared/* 1806*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15915_gshared/* 1807*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15916_gshared/* 1808*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15917_gshared/* 1809*/,
	(methodPointerType)&KeyValuePair_2__ctor_m15918_gshared/* 1810*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m15919_gshared/* 1811*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m15920_gshared/* 1812*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m15921_gshared/* 1813*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m15922_gshared/* 1814*/,
	(methodPointerType)&KeyValuePair_2_ToString_m15923_gshared/* 1815*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15924_gshared/* 1816*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15925_gshared/* 1817*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15926_gshared/* 1818*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15927_gshared/* 1819*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15928_gshared/* 1820*/,
	(methodPointerType)&KeyCollection__ctor_m15929_gshared/* 1821*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15930_gshared/* 1822*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15931_gshared/* 1823*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15932_gshared/* 1824*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15933_gshared/* 1825*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15934_gshared/* 1826*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m15935_gshared/* 1827*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15936_gshared/* 1828*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15937_gshared/* 1829*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15938_gshared/* 1830*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m15939_gshared/* 1831*/,
	(methodPointerType)&KeyCollection_CopyTo_m15940_gshared/* 1832*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m15941_gshared/* 1833*/,
	(methodPointerType)&KeyCollection_get_Count_m15942_gshared/* 1834*/,
	(methodPointerType)&Enumerator__ctor_m15943_gshared/* 1835*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15944_gshared/* 1836*/,
	(methodPointerType)&Enumerator_Dispose_m15945_gshared/* 1837*/,
	(methodPointerType)&Enumerator_MoveNext_m15946_gshared/* 1838*/,
	(methodPointerType)&Enumerator_get_Current_m15947_gshared/* 1839*/,
	(methodPointerType)&Enumerator__ctor_m15948_gshared/* 1840*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15949_gshared/* 1841*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15950_gshared/* 1842*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15951_gshared/* 1843*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15952_gshared/* 1844*/,
	(methodPointerType)&Enumerator_MoveNext_m15953_gshared/* 1845*/,
	(methodPointerType)&Enumerator_get_Current_m15954_gshared/* 1846*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m15955_gshared/* 1847*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m15956_gshared/* 1848*/,
	(methodPointerType)&Enumerator_VerifyState_m15957_gshared/* 1849*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m15958_gshared/* 1850*/,
	(methodPointerType)&Enumerator_Dispose_m15959_gshared/* 1851*/,
	(methodPointerType)&Transform_1__ctor_m15960_gshared/* 1852*/,
	(methodPointerType)&Transform_1_Invoke_m15961_gshared/* 1853*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15962_gshared/* 1854*/,
	(methodPointerType)&Transform_1_EndInvoke_m15963_gshared/* 1855*/,
	(methodPointerType)&ValueCollection__ctor_m15964_gshared/* 1856*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15965_gshared/* 1857*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15966_gshared/* 1858*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15967_gshared/* 1859*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15968_gshared/* 1860*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15969_gshared/* 1861*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m15970_gshared/* 1862*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15971_gshared/* 1863*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15972_gshared/* 1864*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15973_gshared/* 1865*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m15974_gshared/* 1866*/,
	(methodPointerType)&ValueCollection_CopyTo_m15975_gshared/* 1867*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m15976_gshared/* 1868*/,
	(methodPointerType)&ValueCollection_get_Count_m15977_gshared/* 1869*/,
	(methodPointerType)&Enumerator__ctor_m15978_gshared/* 1870*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15979_gshared/* 1871*/,
	(methodPointerType)&Enumerator_Dispose_m15980_gshared/* 1872*/,
	(methodPointerType)&Enumerator_MoveNext_m15981_gshared/* 1873*/,
	(methodPointerType)&Enumerator_get_Current_m15982_gshared/* 1874*/,
	(methodPointerType)&Transform_1__ctor_m15983_gshared/* 1875*/,
	(methodPointerType)&Transform_1_Invoke_m15984_gshared/* 1876*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15985_gshared/* 1877*/,
	(methodPointerType)&Transform_1_EndInvoke_m15986_gshared/* 1878*/,
	(methodPointerType)&Transform_1__ctor_m15987_gshared/* 1879*/,
	(methodPointerType)&Transform_1_Invoke_m15988_gshared/* 1880*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15989_gshared/* 1881*/,
	(methodPointerType)&Transform_1_EndInvoke_m15990_gshared/* 1882*/,
	(methodPointerType)&Transform_1__ctor_m15991_gshared/* 1883*/,
	(methodPointerType)&Transform_1_Invoke_m15992_gshared/* 1884*/,
	(methodPointerType)&Transform_1_BeginInvoke_m15993_gshared/* 1885*/,
	(methodPointerType)&Transform_1_EndInvoke_m15994_gshared/* 1886*/,
	(methodPointerType)&ShimEnumerator__ctor_m15995_gshared/* 1887*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m15996_gshared/* 1888*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m15997_gshared/* 1889*/,
	(methodPointerType)&ShimEnumerator_get_Key_m15998_gshared/* 1890*/,
	(methodPointerType)&ShimEnumerator_get_Value_m15999_gshared/* 1891*/,
	(methodPointerType)&ShimEnumerator_get_Current_m16000_gshared/* 1892*/,
	(methodPointerType)&EqualityComparer_1__ctor_m16001_gshared/* 1893*/,
	(methodPointerType)&EqualityComparer_1__cctor_m16002_gshared/* 1894*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16003_gshared/* 1895*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16004_gshared/* 1896*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m16005_gshared/* 1897*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m16006_gshared/* 1898*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m16007_gshared/* 1899*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m16008_gshared/* 1900*/,
	(methodPointerType)&DefaultComparer__ctor_m16009_gshared/* 1901*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m16010_gshared/* 1902*/,
	(methodPointerType)&DefaultComparer_Equals_m16011_gshared/* 1903*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16257_gshared/* 1904*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16258_gshared/* 1905*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16259_gshared/* 1906*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16260_gshared/* 1907*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16261_gshared/* 1908*/,
	(methodPointerType)&Dictionary_2__ctor_m16941_gshared/* 1909*/,
	(methodPointerType)&Dictionary_2__ctor_m16943_gshared/* 1910*/,
	(methodPointerType)&Dictionary_2__ctor_m16945_gshared/* 1911*/,
	(methodPointerType)&Dictionary_2__ctor_m16947_gshared/* 1912*/,
	(methodPointerType)&Dictionary_2__ctor_m16949_gshared/* 1913*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16951_gshared/* 1914*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16953_gshared/* 1915*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m16955_gshared/* 1916*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m16957_gshared/* 1917*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m16959_gshared/* 1918*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m16961_gshared/* 1919*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m16963_gshared/* 1920*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16965_gshared/* 1921*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16967_gshared/* 1922*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16969_gshared/* 1923*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16971_gshared/* 1924*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16973_gshared/* 1925*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16975_gshared/* 1926*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16977_gshared/* 1927*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m16979_gshared/* 1928*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16981_gshared/* 1929*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16983_gshared/* 1930*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16985_gshared/* 1931*/,
	(methodPointerType)&Dictionary_2_get_Count_m16987_gshared/* 1932*/,
	(methodPointerType)&Dictionary_2_get_Item_m16989_gshared/* 1933*/,
	(methodPointerType)&Dictionary_2_set_Item_m16991_gshared/* 1934*/,
	(methodPointerType)&Dictionary_2_Init_m16993_gshared/* 1935*/,
	(methodPointerType)&Dictionary_2_InitArrays_m16995_gshared/* 1936*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m16997_gshared/* 1937*/,
	(methodPointerType)&Dictionary_2_make_pair_m16999_gshared/* 1938*/,
	(methodPointerType)&Dictionary_2_pick_key_m17001_gshared/* 1939*/,
	(methodPointerType)&Dictionary_2_pick_value_m17003_gshared/* 1940*/,
	(methodPointerType)&Dictionary_2_CopyTo_m17005_gshared/* 1941*/,
	(methodPointerType)&Dictionary_2_Resize_m17007_gshared/* 1942*/,
	(methodPointerType)&Dictionary_2_Add_m17009_gshared/* 1943*/,
	(methodPointerType)&Dictionary_2_Clear_m17011_gshared/* 1944*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m17013_gshared/* 1945*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m17015_gshared/* 1946*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m17017_gshared/* 1947*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m17019_gshared/* 1948*/,
	(methodPointerType)&Dictionary_2_Remove_m17021_gshared/* 1949*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m17023_gshared/* 1950*/,
	(methodPointerType)&Dictionary_2_get_Keys_m17025_gshared/* 1951*/,
	(methodPointerType)&Dictionary_2_ToTKey_m17028_gshared/* 1952*/,
	(methodPointerType)&Dictionary_2_ToTValue_m17030_gshared/* 1953*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m17032_gshared/* 1954*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m17035_gshared/* 1955*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17036_gshared/* 1956*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17037_gshared/* 1957*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17038_gshared/* 1958*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17039_gshared/* 1959*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17040_gshared/* 1960*/,
	(methodPointerType)&KeyValuePair_2__ctor_m17041_gshared/* 1961*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m17043_gshared/* 1962*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m17045_gshared/* 1963*/,
	(methodPointerType)&KeyCollection__ctor_m17047_gshared/* 1964*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17048_gshared/* 1965*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17049_gshared/* 1966*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17050_gshared/* 1967*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17051_gshared/* 1968*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17052_gshared/* 1969*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m17053_gshared/* 1970*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17054_gshared/* 1971*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17055_gshared/* 1972*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17056_gshared/* 1973*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m17057_gshared/* 1974*/,
	(methodPointerType)&KeyCollection_CopyTo_m17058_gshared/* 1975*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m17059_gshared/* 1976*/,
	(methodPointerType)&KeyCollection_get_Count_m17060_gshared/* 1977*/,
	(methodPointerType)&Enumerator__ctor_m17061_gshared/* 1978*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17062_gshared/* 1979*/,
	(methodPointerType)&Enumerator_Dispose_m17063_gshared/* 1980*/,
	(methodPointerType)&Enumerator_MoveNext_m17064_gshared/* 1981*/,
	(methodPointerType)&Enumerator_get_Current_m17065_gshared/* 1982*/,
	(methodPointerType)&Enumerator__ctor_m17066_gshared/* 1983*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17067_gshared/* 1984*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17068_gshared/* 1985*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17069_gshared/* 1986*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17070_gshared/* 1987*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m17073_gshared/* 1988*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m17074_gshared/* 1989*/,
	(methodPointerType)&Enumerator_VerifyState_m17075_gshared/* 1990*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m17076_gshared/* 1991*/,
	(methodPointerType)&Enumerator_Dispose_m17077_gshared/* 1992*/,
	(methodPointerType)&Transform_1__ctor_m17078_gshared/* 1993*/,
	(methodPointerType)&Transform_1_Invoke_m17079_gshared/* 1994*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17080_gshared/* 1995*/,
	(methodPointerType)&Transform_1_EndInvoke_m17081_gshared/* 1996*/,
	(methodPointerType)&ValueCollection__ctor_m17082_gshared/* 1997*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17083_gshared/* 1998*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17084_gshared/* 1999*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17085_gshared/* 2000*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17086_gshared/* 2001*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17087_gshared/* 2002*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m17088_gshared/* 2003*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17089_gshared/* 2004*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17090_gshared/* 2005*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17091_gshared/* 2006*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m17092_gshared/* 2007*/,
	(methodPointerType)&ValueCollection_CopyTo_m17093_gshared/* 2008*/,
	(methodPointerType)&ValueCollection_get_Count_m17095_gshared/* 2009*/,
	(methodPointerType)&Enumerator__ctor_m17096_gshared/* 2010*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17097_gshared/* 2011*/,
	(methodPointerType)&Enumerator_Dispose_m17098_gshared/* 2012*/,
	(methodPointerType)&Transform_1__ctor_m17101_gshared/* 2013*/,
	(methodPointerType)&Transform_1_Invoke_m17102_gshared/* 2014*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17103_gshared/* 2015*/,
	(methodPointerType)&Transform_1_EndInvoke_m17104_gshared/* 2016*/,
	(methodPointerType)&Transform_1__ctor_m17105_gshared/* 2017*/,
	(methodPointerType)&Transform_1_Invoke_m17106_gshared/* 2018*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17107_gshared/* 2019*/,
	(methodPointerType)&Transform_1_EndInvoke_m17108_gshared/* 2020*/,
	(methodPointerType)&Transform_1__ctor_m17109_gshared/* 2021*/,
	(methodPointerType)&Transform_1_Invoke_m17110_gshared/* 2022*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17111_gshared/* 2023*/,
	(methodPointerType)&Transform_1_EndInvoke_m17112_gshared/* 2024*/,
	(methodPointerType)&ShimEnumerator__ctor_m17113_gshared/* 2025*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m17114_gshared/* 2026*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m17115_gshared/* 2027*/,
	(methodPointerType)&ShimEnumerator_get_Key_m17116_gshared/* 2028*/,
	(methodPointerType)&ShimEnumerator_get_Value_m17117_gshared/* 2029*/,
	(methodPointerType)&ShimEnumerator_get_Current_m17118_gshared/* 2030*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17259_gshared/* 2031*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17260_gshared/* 2032*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17261_gshared/* 2033*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17262_gshared/* 2034*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17263_gshared/* 2035*/,
	(methodPointerType)&Comparison_1_Invoke_m17264_gshared/* 2036*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m17265_gshared/* 2037*/,
	(methodPointerType)&Comparison_1_EndInvoke_m17266_gshared/* 2038*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m17267_gshared/* 2039*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m17268_gshared/* 2040*/,
	(methodPointerType)&UnityAction_1_Invoke_m17269_gshared/* 2041*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m17270_gshared/* 2042*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m17271_gshared/* 2043*/,
	(methodPointerType)&InvokableCall_1__ctor_m17272_gshared/* 2044*/,
	(methodPointerType)&InvokableCall_1__ctor_m17273_gshared/* 2045*/,
	(methodPointerType)&InvokableCall_1_Invoke_m17274_gshared/* 2046*/,
	(methodPointerType)&InvokableCall_1_Find_m17275_gshared/* 2047*/,
	(methodPointerType)&List_1__ctor_m17671_gshared/* 2048*/,
	(methodPointerType)&List_1__cctor_m17672_gshared/* 2049*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17673_gshared/* 2050*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m17674_gshared/* 2051*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m17675_gshared/* 2052*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m17676_gshared/* 2053*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m17677_gshared/* 2054*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m17678_gshared/* 2055*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m17679_gshared/* 2056*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m17680_gshared/* 2057*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17681_gshared/* 2058*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m17682_gshared/* 2059*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m17683_gshared/* 2060*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m17684_gshared/* 2061*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m17685_gshared/* 2062*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m17686_gshared/* 2063*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m17687_gshared/* 2064*/,
	(methodPointerType)&List_1_Add_m17688_gshared/* 2065*/,
	(methodPointerType)&List_1_GrowIfNeeded_m17689_gshared/* 2066*/,
	(methodPointerType)&List_1_AddCollection_m17690_gshared/* 2067*/,
	(methodPointerType)&List_1_AddEnumerable_m17691_gshared/* 2068*/,
	(methodPointerType)&List_1_AddRange_m17692_gshared/* 2069*/,
	(methodPointerType)&List_1_AsReadOnly_m17693_gshared/* 2070*/,
	(methodPointerType)&List_1_Clear_m17694_gshared/* 2071*/,
	(methodPointerType)&List_1_Contains_m17695_gshared/* 2072*/,
	(methodPointerType)&List_1_CopyTo_m17696_gshared/* 2073*/,
	(methodPointerType)&List_1_Find_m17697_gshared/* 2074*/,
	(methodPointerType)&List_1_CheckMatch_m17698_gshared/* 2075*/,
	(methodPointerType)&List_1_GetIndex_m17699_gshared/* 2076*/,
	(methodPointerType)&List_1_GetEnumerator_m17700_gshared/* 2077*/,
	(methodPointerType)&List_1_IndexOf_m17701_gshared/* 2078*/,
	(methodPointerType)&List_1_Shift_m17702_gshared/* 2079*/,
	(methodPointerType)&List_1_CheckIndex_m17703_gshared/* 2080*/,
	(methodPointerType)&List_1_Insert_m17704_gshared/* 2081*/,
	(methodPointerType)&List_1_CheckCollection_m17705_gshared/* 2082*/,
	(methodPointerType)&List_1_Remove_m17706_gshared/* 2083*/,
	(methodPointerType)&List_1_RemoveAll_m17707_gshared/* 2084*/,
	(methodPointerType)&List_1_RemoveAt_m17708_gshared/* 2085*/,
	(methodPointerType)&List_1_Reverse_m17709_gshared/* 2086*/,
	(methodPointerType)&List_1_Sort_m17710_gshared/* 2087*/,
	(methodPointerType)&List_1_Sort_m17711_gshared/* 2088*/,
	(methodPointerType)&List_1_TrimExcess_m17712_gshared/* 2089*/,
	(methodPointerType)&List_1_get_Count_m17713_gshared/* 2090*/,
	(methodPointerType)&List_1_get_Item_m17714_gshared/* 2091*/,
	(methodPointerType)&List_1_set_Item_m17715_gshared/* 2092*/,
	(methodPointerType)&Enumerator__ctor_m17650_gshared/* 2093*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17651_gshared/* 2094*/,
	(methodPointerType)&Enumerator_Dispose_m17652_gshared/* 2095*/,
	(methodPointerType)&Enumerator_VerifyState_m17653_gshared/* 2096*/,
	(methodPointerType)&Enumerator_MoveNext_m17654_gshared/* 2097*/,
	(methodPointerType)&Enumerator_get_Current_m17655_gshared/* 2098*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m17616_gshared/* 2099*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17617_gshared/* 2100*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17618_gshared/* 2101*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17619_gshared/* 2102*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17620_gshared/* 2103*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17621_gshared/* 2104*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17622_gshared/* 2105*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17623_gshared/* 2106*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17624_gshared/* 2107*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17625_gshared/* 2108*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17626_gshared/* 2109*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m17627_gshared/* 2110*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m17628_gshared/* 2111*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m17629_gshared/* 2112*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17630_gshared/* 2113*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m17631_gshared/* 2114*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m17632_gshared/* 2115*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17633_gshared/* 2116*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17634_gshared/* 2117*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17635_gshared/* 2118*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17636_gshared/* 2119*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17637_gshared/* 2120*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m17638_gshared/* 2121*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m17639_gshared/* 2122*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m17640_gshared/* 2123*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m17641_gshared/* 2124*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m17642_gshared/* 2125*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m17643_gshared/* 2126*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m17644_gshared/* 2127*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m17645_gshared/* 2128*/,
	(methodPointerType)&Collection_1__ctor_m17719_gshared/* 2129*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17720_gshared/* 2130*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m17721_gshared/* 2131*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m17722_gshared/* 2132*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m17723_gshared/* 2133*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m17724_gshared/* 2134*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m17725_gshared/* 2135*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m17726_gshared/* 2136*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m17727_gshared/* 2137*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m17728_gshared/* 2138*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m17729_gshared/* 2139*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m17730_gshared/* 2140*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m17731_gshared/* 2141*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m17732_gshared/* 2142*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m17733_gshared/* 2143*/,
	(methodPointerType)&Collection_1_Add_m17734_gshared/* 2144*/,
	(methodPointerType)&Collection_1_Clear_m17735_gshared/* 2145*/,
	(methodPointerType)&Collection_1_ClearItems_m17736_gshared/* 2146*/,
	(methodPointerType)&Collection_1_Contains_m17737_gshared/* 2147*/,
	(methodPointerType)&Collection_1_CopyTo_m17738_gshared/* 2148*/,
	(methodPointerType)&Collection_1_GetEnumerator_m17739_gshared/* 2149*/,
	(methodPointerType)&Collection_1_IndexOf_m17740_gshared/* 2150*/,
	(methodPointerType)&Collection_1_Insert_m17741_gshared/* 2151*/,
	(methodPointerType)&Collection_1_InsertItem_m17742_gshared/* 2152*/,
	(methodPointerType)&Collection_1_Remove_m17743_gshared/* 2153*/,
	(methodPointerType)&Collection_1_RemoveAt_m17744_gshared/* 2154*/,
	(methodPointerType)&Collection_1_RemoveItem_m17745_gshared/* 2155*/,
	(methodPointerType)&Collection_1_get_Count_m17746_gshared/* 2156*/,
	(methodPointerType)&Collection_1_get_Item_m17747_gshared/* 2157*/,
	(methodPointerType)&Collection_1_set_Item_m17748_gshared/* 2158*/,
	(methodPointerType)&Collection_1_SetItem_m17749_gshared/* 2159*/,
	(methodPointerType)&Collection_1_IsValidItem_m17750_gshared/* 2160*/,
	(methodPointerType)&Collection_1_ConvertItem_m17751_gshared/* 2161*/,
	(methodPointerType)&Collection_1_CheckWritable_m17752_gshared/* 2162*/,
	(methodPointerType)&Collection_1_IsSynchronized_m17753_gshared/* 2163*/,
	(methodPointerType)&Collection_1_IsFixedSize_m17754_gshared/* 2164*/,
	(methodPointerType)&EqualityComparer_1__ctor_m17755_gshared/* 2165*/,
	(methodPointerType)&EqualityComparer_1__cctor_m17756_gshared/* 2166*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17757_gshared/* 2167*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17758_gshared/* 2168*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m17759_gshared/* 2169*/,
	(methodPointerType)&DefaultComparer__ctor_m17760_gshared/* 2170*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m17761_gshared/* 2171*/,
	(methodPointerType)&DefaultComparer_Equals_m17762_gshared/* 2172*/,
	(methodPointerType)&Predicate_1__ctor_m17646_gshared/* 2173*/,
	(methodPointerType)&Predicate_1_Invoke_m17647_gshared/* 2174*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m17648_gshared/* 2175*/,
	(methodPointerType)&Predicate_1_EndInvoke_m17649_gshared/* 2176*/,
	(methodPointerType)&Comparer_1__ctor_m17763_gshared/* 2177*/,
	(methodPointerType)&Comparer_1__cctor_m17764_gshared/* 2178*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m17765_gshared/* 2179*/,
	(methodPointerType)&Comparer_1_get_Default_m17766_gshared/* 2180*/,
	(methodPointerType)&DefaultComparer__ctor_m17767_gshared/* 2181*/,
	(methodPointerType)&DefaultComparer_Compare_m17768_gshared/* 2182*/,
	(methodPointerType)&Comparison_1__ctor_m17656_gshared/* 2183*/,
	(methodPointerType)&Comparison_1_Invoke_m17657_gshared/* 2184*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m17658_gshared/* 2185*/,
	(methodPointerType)&Comparison_1_EndInvoke_m17659_gshared/* 2186*/,
	(methodPointerType)&TweenRunner_1_Start_m17769_gshared/* 2187*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0__ctor_m17770_gshared/* 2188*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m17771_gshared/* 2189*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m17772_gshared/* 2190*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_MoveNext_m17773_gshared/* 2191*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Dispose_m17774_gshared/* 2192*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Reset_m17775_gshared/* 2193*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18231_gshared/* 2194*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18232_gshared/* 2195*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18233_gshared/* 2196*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18234_gshared/* 2197*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18235_gshared/* 2198*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18236_gshared/* 2199*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18237_gshared/* 2200*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18238_gshared/* 2201*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18239_gshared/* 2202*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18240_gshared/* 2203*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m18248_gshared/* 2204*/,
	(methodPointerType)&UnityAction_1_Invoke_m18249_gshared/* 2205*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m18250_gshared/* 2206*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m18251_gshared/* 2207*/,
	(methodPointerType)&InvokableCall_1__ctor_m18252_gshared/* 2208*/,
	(methodPointerType)&InvokableCall_1__ctor_m18253_gshared/* 2209*/,
	(methodPointerType)&InvokableCall_1_Invoke_m18254_gshared/* 2210*/,
	(methodPointerType)&InvokableCall_1_Find_m18255_gshared/* 2211*/,
	(methodPointerType)&UnityEvent_1_AddListener_m18256_gshared/* 2212*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m18257_gshared/* 2213*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m18258_gshared/* 2214*/,
	(methodPointerType)&UnityAction_1__ctor_m18259_gshared/* 2215*/,
	(methodPointerType)&UnityAction_1_Invoke_m18260_gshared/* 2216*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m18261_gshared/* 2217*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m18262_gshared/* 2218*/,
	(methodPointerType)&InvokableCall_1__ctor_m18263_gshared/* 2219*/,
	(methodPointerType)&InvokableCall_1__ctor_m18264_gshared/* 2220*/,
	(methodPointerType)&InvokableCall_1_Invoke_m18265_gshared/* 2221*/,
	(methodPointerType)&InvokableCall_1_Find_m18266_gshared/* 2222*/,
	(methodPointerType)&UnityEvent_1_AddListener_m18548_gshared/* 2223*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m18550_gshared/* 2224*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m18554_gshared/* 2225*/,
	(methodPointerType)&UnityAction_1__ctor_m18556_gshared/* 2226*/,
	(methodPointerType)&UnityAction_1_Invoke_m18557_gshared/* 2227*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m18558_gshared/* 2228*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m18559_gshared/* 2229*/,
	(methodPointerType)&InvokableCall_1__ctor_m18560_gshared/* 2230*/,
	(methodPointerType)&InvokableCall_1__ctor_m18561_gshared/* 2231*/,
	(methodPointerType)&InvokableCall_1_Invoke_m18562_gshared/* 2232*/,
	(methodPointerType)&InvokableCall_1_Find_m18563_gshared/* 2233*/,
	(methodPointerType)&Func_2_Invoke_m18661_gshared/* 2234*/,
	(methodPointerType)&Func_2_BeginInvoke_m18663_gshared/* 2235*/,
	(methodPointerType)&Func_2_EndInvoke_m18665_gshared/* 2236*/,
	(methodPointerType)&Func_2_Invoke_m18775_gshared/* 2237*/,
	(methodPointerType)&Func_2_BeginInvoke_m18777_gshared/* 2238*/,
	(methodPointerType)&Func_2_EndInvoke_m18779_gshared/* 2239*/,
	(methodPointerType)&Action_1__ctor_m18817_gshared/* 2240*/,
	(methodPointerType)&Action_1_Invoke_m18819_gshared/* 2241*/,
	(methodPointerType)&Action_1_BeginInvoke_m18821_gshared/* 2242*/,
	(methodPointerType)&Action_1_EndInvoke_m18823_gshared/* 2243*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18960_gshared/* 2244*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18961_gshared/* 2245*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18962_gshared/* 2246*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18963_gshared/* 2247*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18964_gshared/* 2248*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18970_gshared/* 2249*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18971_gshared/* 2250*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18972_gshared/* 2251*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18973_gshared/* 2252*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18974_gshared/* 2253*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19379_gshared/* 2254*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19380_gshared/* 2255*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19381_gshared/* 2256*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19382_gshared/* 2257*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19383_gshared/* 2258*/,
	(methodPointerType)&List_1__ctor_m19569_gshared/* 2259*/,
	(methodPointerType)&List_1__ctor_m19570_gshared/* 2260*/,
	(methodPointerType)&List_1__cctor_m19571_gshared/* 2261*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19572_gshared/* 2262*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m19573_gshared/* 2263*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m19574_gshared/* 2264*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m19575_gshared/* 2265*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m19576_gshared/* 2266*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m19577_gshared/* 2267*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m19578_gshared/* 2268*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m19579_gshared/* 2269*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19580_gshared/* 2270*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m19581_gshared/* 2271*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m19582_gshared/* 2272*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m19583_gshared/* 2273*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m19584_gshared/* 2274*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m19585_gshared/* 2275*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m19586_gshared/* 2276*/,
	(methodPointerType)&List_1_Add_m19587_gshared/* 2277*/,
	(methodPointerType)&List_1_GrowIfNeeded_m19588_gshared/* 2278*/,
	(methodPointerType)&List_1_AddCollection_m19589_gshared/* 2279*/,
	(methodPointerType)&List_1_AddEnumerable_m19590_gshared/* 2280*/,
	(methodPointerType)&List_1_AddRange_m19591_gshared/* 2281*/,
	(methodPointerType)&List_1_AsReadOnly_m19592_gshared/* 2282*/,
	(methodPointerType)&List_1_Clear_m19593_gshared/* 2283*/,
	(methodPointerType)&List_1_Contains_m19594_gshared/* 2284*/,
	(methodPointerType)&List_1_CopyTo_m19595_gshared/* 2285*/,
	(methodPointerType)&List_1_Find_m19596_gshared/* 2286*/,
	(methodPointerType)&List_1_CheckMatch_m19597_gshared/* 2287*/,
	(methodPointerType)&List_1_GetIndex_m19598_gshared/* 2288*/,
	(methodPointerType)&List_1_GetEnumerator_m19599_gshared/* 2289*/,
	(methodPointerType)&List_1_IndexOf_m19600_gshared/* 2290*/,
	(methodPointerType)&List_1_Shift_m19601_gshared/* 2291*/,
	(methodPointerType)&List_1_CheckIndex_m19602_gshared/* 2292*/,
	(methodPointerType)&List_1_Insert_m19603_gshared/* 2293*/,
	(methodPointerType)&List_1_CheckCollection_m19604_gshared/* 2294*/,
	(methodPointerType)&List_1_Remove_m19605_gshared/* 2295*/,
	(methodPointerType)&List_1_RemoveAll_m19606_gshared/* 2296*/,
	(methodPointerType)&List_1_RemoveAt_m19607_gshared/* 2297*/,
	(methodPointerType)&List_1_Reverse_m19608_gshared/* 2298*/,
	(methodPointerType)&List_1_Sort_m19609_gshared/* 2299*/,
	(methodPointerType)&List_1_Sort_m19610_gshared/* 2300*/,
	(methodPointerType)&List_1_ToArray_m19611_gshared/* 2301*/,
	(methodPointerType)&List_1_TrimExcess_m19612_gshared/* 2302*/,
	(methodPointerType)&List_1_get_Capacity_m19613_gshared/* 2303*/,
	(methodPointerType)&List_1_set_Capacity_m19614_gshared/* 2304*/,
	(methodPointerType)&List_1_get_Count_m19615_gshared/* 2305*/,
	(methodPointerType)&List_1_get_Item_m19616_gshared/* 2306*/,
	(methodPointerType)&List_1_set_Item_m19617_gshared/* 2307*/,
	(methodPointerType)&Enumerator__ctor_m19618_gshared/* 2308*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m19619_gshared/* 2309*/,
	(methodPointerType)&Enumerator_Dispose_m19620_gshared/* 2310*/,
	(methodPointerType)&Enumerator_VerifyState_m19621_gshared/* 2311*/,
	(methodPointerType)&Enumerator_MoveNext_m19622_gshared/* 2312*/,
	(methodPointerType)&Enumerator_get_Current_m19623_gshared/* 2313*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m19624_gshared/* 2314*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19625_gshared/* 2315*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19626_gshared/* 2316*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19627_gshared/* 2317*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19628_gshared/* 2318*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19629_gshared/* 2319*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19630_gshared/* 2320*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19631_gshared/* 2321*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19632_gshared/* 2322*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19633_gshared/* 2323*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19634_gshared/* 2324*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m19635_gshared/* 2325*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m19636_gshared/* 2326*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m19637_gshared/* 2327*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19638_gshared/* 2328*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m19639_gshared/* 2329*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m19640_gshared/* 2330*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19641_gshared/* 2331*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19642_gshared/* 2332*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19643_gshared/* 2333*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19644_gshared/* 2334*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19645_gshared/* 2335*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m19646_gshared/* 2336*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m19647_gshared/* 2337*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m19648_gshared/* 2338*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m19649_gshared/* 2339*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m19650_gshared/* 2340*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m19651_gshared/* 2341*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m19652_gshared/* 2342*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m19653_gshared/* 2343*/,
	(methodPointerType)&Collection_1__ctor_m19654_gshared/* 2344*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19655_gshared/* 2345*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m19656_gshared/* 2346*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m19657_gshared/* 2347*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m19658_gshared/* 2348*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m19659_gshared/* 2349*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m19660_gshared/* 2350*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m19661_gshared/* 2351*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m19662_gshared/* 2352*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m19663_gshared/* 2353*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m19664_gshared/* 2354*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m19665_gshared/* 2355*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m19666_gshared/* 2356*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m19667_gshared/* 2357*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m19668_gshared/* 2358*/,
	(methodPointerType)&Collection_1_Add_m19669_gshared/* 2359*/,
	(methodPointerType)&Collection_1_Clear_m19670_gshared/* 2360*/,
	(methodPointerType)&Collection_1_ClearItems_m19671_gshared/* 2361*/,
	(methodPointerType)&Collection_1_Contains_m19672_gshared/* 2362*/,
	(methodPointerType)&Collection_1_CopyTo_m19673_gshared/* 2363*/,
	(methodPointerType)&Collection_1_GetEnumerator_m19674_gshared/* 2364*/,
	(methodPointerType)&Collection_1_IndexOf_m19675_gshared/* 2365*/,
	(methodPointerType)&Collection_1_Insert_m19676_gshared/* 2366*/,
	(methodPointerType)&Collection_1_InsertItem_m19677_gshared/* 2367*/,
	(methodPointerType)&Collection_1_Remove_m19678_gshared/* 2368*/,
	(methodPointerType)&Collection_1_RemoveAt_m19679_gshared/* 2369*/,
	(methodPointerType)&Collection_1_RemoveItem_m19680_gshared/* 2370*/,
	(methodPointerType)&Collection_1_get_Count_m19681_gshared/* 2371*/,
	(methodPointerType)&Collection_1_get_Item_m19682_gshared/* 2372*/,
	(methodPointerType)&Collection_1_set_Item_m19683_gshared/* 2373*/,
	(methodPointerType)&Collection_1_SetItem_m19684_gshared/* 2374*/,
	(methodPointerType)&Collection_1_IsValidItem_m19685_gshared/* 2375*/,
	(methodPointerType)&Collection_1_ConvertItem_m19686_gshared/* 2376*/,
	(methodPointerType)&Collection_1_CheckWritable_m19687_gshared/* 2377*/,
	(methodPointerType)&Collection_1_IsSynchronized_m19688_gshared/* 2378*/,
	(methodPointerType)&Collection_1_IsFixedSize_m19689_gshared/* 2379*/,
	(methodPointerType)&EqualityComparer_1__ctor_m19690_gshared/* 2380*/,
	(methodPointerType)&EqualityComparer_1__cctor_m19691_gshared/* 2381*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m19692_gshared/* 2382*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m19693_gshared/* 2383*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m19694_gshared/* 2384*/,
	(methodPointerType)&DefaultComparer__ctor_m19695_gshared/* 2385*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m19696_gshared/* 2386*/,
	(methodPointerType)&DefaultComparer_Equals_m19697_gshared/* 2387*/,
	(methodPointerType)&Predicate_1__ctor_m19698_gshared/* 2388*/,
	(methodPointerType)&Predicate_1_Invoke_m19699_gshared/* 2389*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m19700_gshared/* 2390*/,
	(methodPointerType)&Predicate_1_EndInvoke_m19701_gshared/* 2391*/,
	(methodPointerType)&Comparer_1__ctor_m19702_gshared/* 2392*/,
	(methodPointerType)&Comparer_1__cctor_m19703_gshared/* 2393*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m19704_gshared/* 2394*/,
	(methodPointerType)&Comparer_1_get_Default_m19705_gshared/* 2395*/,
	(methodPointerType)&DefaultComparer__ctor_m19706_gshared/* 2396*/,
	(methodPointerType)&DefaultComparer_Compare_m19707_gshared/* 2397*/,
	(methodPointerType)&Comparison_1__ctor_m19708_gshared/* 2398*/,
	(methodPointerType)&Comparison_1_Invoke_m19709_gshared/* 2399*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m19710_gshared/* 2400*/,
	(methodPointerType)&Comparison_1_EndInvoke_m19711_gshared/* 2401*/,
	(methodPointerType)&List_1__ctor_m19712_gshared/* 2402*/,
	(methodPointerType)&List_1__ctor_m19713_gshared/* 2403*/,
	(methodPointerType)&List_1__cctor_m19714_gshared/* 2404*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19715_gshared/* 2405*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m19716_gshared/* 2406*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m19717_gshared/* 2407*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m19718_gshared/* 2408*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m19719_gshared/* 2409*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m19720_gshared/* 2410*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m19721_gshared/* 2411*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m19722_gshared/* 2412*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19723_gshared/* 2413*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m19724_gshared/* 2414*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m19725_gshared/* 2415*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m19726_gshared/* 2416*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m19727_gshared/* 2417*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m19728_gshared/* 2418*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m19729_gshared/* 2419*/,
	(methodPointerType)&List_1_Add_m19730_gshared/* 2420*/,
	(methodPointerType)&List_1_GrowIfNeeded_m19731_gshared/* 2421*/,
	(methodPointerType)&List_1_AddCollection_m19732_gshared/* 2422*/,
	(methodPointerType)&List_1_AddEnumerable_m19733_gshared/* 2423*/,
	(methodPointerType)&List_1_AddRange_m19734_gshared/* 2424*/,
	(methodPointerType)&List_1_AsReadOnly_m19735_gshared/* 2425*/,
	(methodPointerType)&List_1_Clear_m19736_gshared/* 2426*/,
	(methodPointerType)&List_1_Contains_m19737_gshared/* 2427*/,
	(methodPointerType)&List_1_CopyTo_m19738_gshared/* 2428*/,
	(methodPointerType)&List_1_Find_m19739_gshared/* 2429*/,
	(methodPointerType)&List_1_CheckMatch_m19740_gshared/* 2430*/,
	(methodPointerType)&List_1_GetIndex_m19741_gshared/* 2431*/,
	(methodPointerType)&List_1_GetEnumerator_m19742_gshared/* 2432*/,
	(methodPointerType)&List_1_IndexOf_m19743_gshared/* 2433*/,
	(methodPointerType)&List_1_Shift_m19744_gshared/* 2434*/,
	(methodPointerType)&List_1_CheckIndex_m19745_gshared/* 2435*/,
	(methodPointerType)&List_1_Insert_m19746_gshared/* 2436*/,
	(methodPointerType)&List_1_CheckCollection_m19747_gshared/* 2437*/,
	(methodPointerType)&List_1_Remove_m19748_gshared/* 2438*/,
	(methodPointerType)&List_1_RemoveAll_m19749_gshared/* 2439*/,
	(methodPointerType)&List_1_RemoveAt_m19750_gshared/* 2440*/,
	(methodPointerType)&List_1_Reverse_m19751_gshared/* 2441*/,
	(methodPointerType)&List_1_Sort_m19752_gshared/* 2442*/,
	(methodPointerType)&List_1_Sort_m19753_gshared/* 2443*/,
	(methodPointerType)&List_1_ToArray_m19754_gshared/* 2444*/,
	(methodPointerType)&List_1_TrimExcess_m19755_gshared/* 2445*/,
	(methodPointerType)&List_1_get_Capacity_m19756_gshared/* 2446*/,
	(methodPointerType)&List_1_set_Capacity_m19757_gshared/* 2447*/,
	(methodPointerType)&List_1_get_Count_m19758_gshared/* 2448*/,
	(methodPointerType)&List_1_get_Item_m19759_gshared/* 2449*/,
	(methodPointerType)&List_1_set_Item_m19760_gshared/* 2450*/,
	(methodPointerType)&Enumerator__ctor_m19761_gshared/* 2451*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m19762_gshared/* 2452*/,
	(methodPointerType)&Enumerator_Dispose_m19763_gshared/* 2453*/,
	(methodPointerType)&Enumerator_VerifyState_m19764_gshared/* 2454*/,
	(methodPointerType)&Enumerator_MoveNext_m19765_gshared/* 2455*/,
	(methodPointerType)&Enumerator_get_Current_m19766_gshared/* 2456*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m19767_gshared/* 2457*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19768_gshared/* 2458*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19769_gshared/* 2459*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19770_gshared/* 2460*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19771_gshared/* 2461*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19772_gshared/* 2462*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19773_gshared/* 2463*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19774_gshared/* 2464*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19775_gshared/* 2465*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19776_gshared/* 2466*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19777_gshared/* 2467*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m19778_gshared/* 2468*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m19779_gshared/* 2469*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m19780_gshared/* 2470*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19781_gshared/* 2471*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m19782_gshared/* 2472*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m19783_gshared/* 2473*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19784_gshared/* 2474*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19785_gshared/* 2475*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19786_gshared/* 2476*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19787_gshared/* 2477*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19788_gshared/* 2478*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m19789_gshared/* 2479*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m19790_gshared/* 2480*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m19791_gshared/* 2481*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m19792_gshared/* 2482*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m19793_gshared/* 2483*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m19794_gshared/* 2484*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m19795_gshared/* 2485*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m19796_gshared/* 2486*/,
	(methodPointerType)&Collection_1__ctor_m19797_gshared/* 2487*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19798_gshared/* 2488*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m19799_gshared/* 2489*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m19800_gshared/* 2490*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m19801_gshared/* 2491*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m19802_gshared/* 2492*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m19803_gshared/* 2493*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m19804_gshared/* 2494*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m19805_gshared/* 2495*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m19806_gshared/* 2496*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m19807_gshared/* 2497*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m19808_gshared/* 2498*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m19809_gshared/* 2499*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m19810_gshared/* 2500*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m19811_gshared/* 2501*/,
	(methodPointerType)&Collection_1_Add_m19812_gshared/* 2502*/,
	(methodPointerType)&Collection_1_Clear_m19813_gshared/* 2503*/,
	(methodPointerType)&Collection_1_ClearItems_m19814_gshared/* 2504*/,
	(methodPointerType)&Collection_1_Contains_m19815_gshared/* 2505*/,
	(methodPointerType)&Collection_1_CopyTo_m19816_gshared/* 2506*/,
	(methodPointerType)&Collection_1_GetEnumerator_m19817_gshared/* 2507*/,
	(methodPointerType)&Collection_1_IndexOf_m19818_gshared/* 2508*/,
	(methodPointerType)&Collection_1_Insert_m19819_gshared/* 2509*/,
	(methodPointerType)&Collection_1_InsertItem_m19820_gshared/* 2510*/,
	(methodPointerType)&Collection_1_Remove_m19821_gshared/* 2511*/,
	(methodPointerType)&Collection_1_RemoveAt_m19822_gshared/* 2512*/,
	(methodPointerType)&Collection_1_RemoveItem_m19823_gshared/* 2513*/,
	(methodPointerType)&Collection_1_get_Count_m19824_gshared/* 2514*/,
	(methodPointerType)&Collection_1_get_Item_m19825_gshared/* 2515*/,
	(methodPointerType)&Collection_1_set_Item_m19826_gshared/* 2516*/,
	(methodPointerType)&Collection_1_SetItem_m19827_gshared/* 2517*/,
	(methodPointerType)&Collection_1_IsValidItem_m19828_gshared/* 2518*/,
	(methodPointerType)&Collection_1_ConvertItem_m19829_gshared/* 2519*/,
	(methodPointerType)&Collection_1_CheckWritable_m19830_gshared/* 2520*/,
	(methodPointerType)&Collection_1_IsSynchronized_m19831_gshared/* 2521*/,
	(methodPointerType)&Collection_1_IsFixedSize_m19832_gshared/* 2522*/,
	(methodPointerType)&EqualityComparer_1__ctor_m19833_gshared/* 2523*/,
	(methodPointerType)&EqualityComparer_1__cctor_m19834_gshared/* 2524*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m19835_gshared/* 2525*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m19836_gshared/* 2526*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m19837_gshared/* 2527*/,
	(methodPointerType)&DefaultComparer__ctor_m19838_gshared/* 2528*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m19839_gshared/* 2529*/,
	(methodPointerType)&DefaultComparer_Equals_m19840_gshared/* 2530*/,
	(methodPointerType)&Predicate_1__ctor_m19841_gshared/* 2531*/,
	(methodPointerType)&Predicate_1_Invoke_m19842_gshared/* 2532*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m19843_gshared/* 2533*/,
	(methodPointerType)&Predicate_1_EndInvoke_m19844_gshared/* 2534*/,
	(methodPointerType)&Comparer_1__ctor_m19845_gshared/* 2535*/,
	(methodPointerType)&Comparer_1__cctor_m19846_gshared/* 2536*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m19847_gshared/* 2537*/,
	(methodPointerType)&Comparer_1_get_Default_m19848_gshared/* 2538*/,
	(methodPointerType)&DefaultComparer__ctor_m19849_gshared/* 2539*/,
	(methodPointerType)&DefaultComparer_Compare_m19850_gshared/* 2540*/,
	(methodPointerType)&Comparison_1__ctor_m19851_gshared/* 2541*/,
	(methodPointerType)&Comparison_1_Invoke_m19852_gshared/* 2542*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m19853_gshared/* 2543*/,
	(methodPointerType)&Comparison_1_EndInvoke_m19854_gshared/* 2544*/,
	(methodPointerType)&Dictionary_2__ctor_m19856_gshared/* 2545*/,
	(methodPointerType)&Dictionary_2__ctor_m19858_gshared/* 2546*/,
	(methodPointerType)&Dictionary_2__ctor_m19860_gshared/* 2547*/,
	(methodPointerType)&Dictionary_2__ctor_m19862_gshared/* 2548*/,
	(methodPointerType)&Dictionary_2__ctor_m19864_gshared/* 2549*/,
	(methodPointerType)&Dictionary_2__ctor_m19866_gshared/* 2550*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m19868_gshared/* 2551*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m19870_gshared/* 2552*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m19872_gshared/* 2553*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m19874_gshared/* 2554*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m19876_gshared/* 2555*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m19878_gshared/* 2556*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m19880_gshared/* 2557*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19882_gshared/* 2558*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19884_gshared/* 2559*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19886_gshared/* 2560*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19888_gshared/* 2561*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19890_gshared/* 2562*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19892_gshared/* 2563*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19894_gshared/* 2564*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m19896_gshared/* 2565*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19898_gshared/* 2566*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19900_gshared/* 2567*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19902_gshared/* 2568*/,
	(methodPointerType)&Dictionary_2_get_Count_m19904_gshared/* 2569*/,
	(methodPointerType)&Dictionary_2_get_Item_m19906_gshared/* 2570*/,
	(methodPointerType)&Dictionary_2_set_Item_m19908_gshared/* 2571*/,
	(methodPointerType)&Dictionary_2_Init_m19910_gshared/* 2572*/,
	(methodPointerType)&Dictionary_2_InitArrays_m19912_gshared/* 2573*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m19914_gshared/* 2574*/,
	(methodPointerType)&Dictionary_2_make_pair_m19916_gshared/* 2575*/,
	(methodPointerType)&Dictionary_2_pick_key_m19918_gshared/* 2576*/,
	(methodPointerType)&Dictionary_2_pick_value_m19920_gshared/* 2577*/,
	(methodPointerType)&Dictionary_2_CopyTo_m19922_gshared/* 2578*/,
	(methodPointerType)&Dictionary_2_Resize_m19924_gshared/* 2579*/,
	(methodPointerType)&Dictionary_2_Add_m19926_gshared/* 2580*/,
	(methodPointerType)&Dictionary_2_Clear_m19928_gshared/* 2581*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m19930_gshared/* 2582*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m19932_gshared/* 2583*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m19934_gshared/* 2584*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m19936_gshared/* 2585*/,
	(methodPointerType)&Dictionary_2_Remove_m19938_gshared/* 2586*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m19940_gshared/* 2587*/,
	(methodPointerType)&Dictionary_2_get_Keys_m19942_gshared/* 2588*/,
	(methodPointerType)&Dictionary_2_get_Values_m19944_gshared/* 2589*/,
	(methodPointerType)&Dictionary_2_ToTKey_m19946_gshared/* 2590*/,
	(methodPointerType)&Dictionary_2_ToTValue_m19948_gshared/* 2591*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m19950_gshared/* 2592*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m19952_gshared/* 2593*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m19954_gshared/* 2594*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19955_gshared/* 2595*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19956_gshared/* 2596*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19957_gshared/* 2597*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19958_gshared/* 2598*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19959_gshared/* 2599*/,
	(methodPointerType)&KeyValuePair_2__ctor_m19960_gshared/* 2600*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m19961_gshared/* 2601*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m19962_gshared/* 2602*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m19963_gshared/* 2603*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m19964_gshared/* 2604*/,
	(methodPointerType)&KeyValuePair_2_ToString_m19965_gshared/* 2605*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19966_gshared/* 2606*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19967_gshared/* 2607*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19968_gshared/* 2608*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19969_gshared/* 2609*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19970_gshared/* 2610*/,
	(methodPointerType)&KeyCollection__ctor_m19971_gshared/* 2611*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m19972_gshared/* 2612*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m19973_gshared/* 2613*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m19974_gshared/* 2614*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m19975_gshared/* 2615*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m19976_gshared/* 2616*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m19977_gshared/* 2617*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m19978_gshared/* 2618*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m19979_gshared/* 2619*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m19980_gshared/* 2620*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m19981_gshared/* 2621*/,
	(methodPointerType)&KeyCollection_CopyTo_m19982_gshared/* 2622*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m19983_gshared/* 2623*/,
	(methodPointerType)&KeyCollection_get_Count_m19984_gshared/* 2624*/,
	(methodPointerType)&Enumerator__ctor_m19985_gshared/* 2625*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m19986_gshared/* 2626*/,
	(methodPointerType)&Enumerator_Dispose_m19987_gshared/* 2627*/,
	(methodPointerType)&Enumerator_MoveNext_m19988_gshared/* 2628*/,
	(methodPointerType)&Enumerator_get_Current_m19989_gshared/* 2629*/,
	(methodPointerType)&Enumerator__ctor_m19990_gshared/* 2630*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m19991_gshared/* 2631*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19992_gshared/* 2632*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19993_gshared/* 2633*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19994_gshared/* 2634*/,
	(methodPointerType)&Enumerator_MoveNext_m19995_gshared/* 2635*/,
	(methodPointerType)&Enumerator_get_Current_m19996_gshared/* 2636*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m19997_gshared/* 2637*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m19998_gshared/* 2638*/,
	(methodPointerType)&Enumerator_VerifyState_m19999_gshared/* 2639*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m20000_gshared/* 2640*/,
	(methodPointerType)&Enumerator_Dispose_m20001_gshared/* 2641*/,
	(methodPointerType)&Transform_1__ctor_m20002_gshared/* 2642*/,
	(methodPointerType)&Transform_1_Invoke_m20003_gshared/* 2643*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20004_gshared/* 2644*/,
	(methodPointerType)&Transform_1_EndInvoke_m20005_gshared/* 2645*/,
	(methodPointerType)&ValueCollection__ctor_m20006_gshared/* 2646*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20007_gshared/* 2647*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20008_gshared/* 2648*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20009_gshared/* 2649*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20010_gshared/* 2650*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20011_gshared/* 2651*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m20012_gshared/* 2652*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20013_gshared/* 2653*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20014_gshared/* 2654*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20015_gshared/* 2655*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m20016_gshared/* 2656*/,
	(methodPointerType)&ValueCollection_CopyTo_m20017_gshared/* 2657*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m20018_gshared/* 2658*/,
	(methodPointerType)&ValueCollection_get_Count_m20019_gshared/* 2659*/,
	(methodPointerType)&Enumerator__ctor_m20020_gshared/* 2660*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20021_gshared/* 2661*/,
	(methodPointerType)&Enumerator_Dispose_m20022_gshared/* 2662*/,
	(methodPointerType)&Enumerator_MoveNext_m20023_gshared/* 2663*/,
	(methodPointerType)&Enumerator_get_Current_m20024_gshared/* 2664*/,
	(methodPointerType)&Transform_1__ctor_m20025_gshared/* 2665*/,
	(methodPointerType)&Transform_1_Invoke_m20026_gshared/* 2666*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20027_gshared/* 2667*/,
	(methodPointerType)&Transform_1_EndInvoke_m20028_gshared/* 2668*/,
	(methodPointerType)&Transform_1__ctor_m20029_gshared/* 2669*/,
	(methodPointerType)&Transform_1_Invoke_m20030_gshared/* 2670*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20031_gshared/* 2671*/,
	(methodPointerType)&Transform_1_EndInvoke_m20032_gshared/* 2672*/,
	(methodPointerType)&Transform_1__ctor_m20033_gshared/* 2673*/,
	(methodPointerType)&Transform_1_Invoke_m20034_gshared/* 2674*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20035_gshared/* 2675*/,
	(methodPointerType)&Transform_1_EndInvoke_m20036_gshared/* 2676*/,
	(methodPointerType)&ShimEnumerator__ctor_m20037_gshared/* 2677*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m20038_gshared/* 2678*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m20039_gshared/* 2679*/,
	(methodPointerType)&ShimEnumerator_get_Key_m20040_gshared/* 2680*/,
	(methodPointerType)&ShimEnumerator_get_Value_m20041_gshared/* 2681*/,
	(methodPointerType)&ShimEnumerator_get_Current_m20042_gshared/* 2682*/,
	(methodPointerType)&EqualityComparer_1__ctor_m20043_gshared/* 2683*/,
	(methodPointerType)&EqualityComparer_1__cctor_m20044_gshared/* 2684*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20045_gshared/* 2685*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20046_gshared/* 2686*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m20047_gshared/* 2687*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m20048_gshared/* 2688*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m20049_gshared/* 2689*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m20050_gshared/* 2690*/,
	(methodPointerType)&DefaultComparer__ctor_m20051_gshared/* 2691*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m20052_gshared/* 2692*/,
	(methodPointerType)&DefaultComparer_Equals_m20053_gshared/* 2693*/,
	(methodPointerType)&Dictionary_2__ctor_m20294_gshared/* 2694*/,
	(methodPointerType)&Dictionary_2__ctor_m20296_gshared/* 2695*/,
	(methodPointerType)&Dictionary_2__ctor_m20298_gshared/* 2696*/,
	(methodPointerType)&Dictionary_2__ctor_m20300_gshared/* 2697*/,
	(methodPointerType)&Dictionary_2__ctor_m20302_gshared/* 2698*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20304_gshared/* 2699*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20306_gshared/* 2700*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m20308_gshared/* 2701*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m20310_gshared/* 2702*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m20312_gshared/* 2703*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m20314_gshared/* 2704*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m20316_gshared/* 2705*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20318_gshared/* 2706*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20320_gshared/* 2707*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20322_gshared/* 2708*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20324_gshared/* 2709*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20326_gshared/* 2710*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20328_gshared/* 2711*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20330_gshared/* 2712*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m20332_gshared/* 2713*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20334_gshared/* 2714*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20336_gshared/* 2715*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20338_gshared/* 2716*/,
	(methodPointerType)&Dictionary_2_get_Count_m20340_gshared/* 2717*/,
	(methodPointerType)&Dictionary_2_get_Item_m20342_gshared/* 2718*/,
	(methodPointerType)&Dictionary_2_set_Item_m20344_gshared/* 2719*/,
	(methodPointerType)&Dictionary_2_Init_m20346_gshared/* 2720*/,
	(methodPointerType)&Dictionary_2_InitArrays_m20348_gshared/* 2721*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m20350_gshared/* 2722*/,
	(methodPointerType)&Dictionary_2_make_pair_m20352_gshared/* 2723*/,
	(methodPointerType)&Dictionary_2_pick_key_m20354_gshared/* 2724*/,
	(methodPointerType)&Dictionary_2_pick_value_m20356_gshared/* 2725*/,
	(methodPointerType)&Dictionary_2_CopyTo_m20358_gshared/* 2726*/,
	(methodPointerType)&Dictionary_2_Resize_m20360_gshared/* 2727*/,
	(methodPointerType)&Dictionary_2_Add_m20362_gshared/* 2728*/,
	(methodPointerType)&Dictionary_2_Clear_m20364_gshared/* 2729*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m20366_gshared/* 2730*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m20368_gshared/* 2731*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m20370_gshared/* 2732*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m20372_gshared/* 2733*/,
	(methodPointerType)&Dictionary_2_Remove_m20374_gshared/* 2734*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m20376_gshared/* 2735*/,
	(methodPointerType)&Dictionary_2_get_Keys_m20378_gshared/* 2736*/,
	(methodPointerType)&Dictionary_2_get_Values_m20380_gshared/* 2737*/,
	(methodPointerType)&Dictionary_2_ToTKey_m20382_gshared/* 2738*/,
	(methodPointerType)&Dictionary_2_ToTValue_m20384_gshared/* 2739*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m20386_gshared/* 2740*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m20388_gshared/* 2741*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m20390_gshared/* 2742*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m20391_gshared/* 2743*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20392_gshared/* 2744*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m20393_gshared/* 2745*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m20394_gshared/* 2746*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m20395_gshared/* 2747*/,
	(methodPointerType)&KeyValuePair_2__ctor_m20396_gshared/* 2748*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m20397_gshared/* 2749*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m20398_gshared/* 2750*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m20399_gshared/* 2751*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m20400_gshared/* 2752*/,
	(methodPointerType)&KeyValuePair_2_ToString_m20401_gshared/* 2753*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m20402_gshared/* 2754*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20403_gshared/* 2755*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m20404_gshared/* 2756*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m20405_gshared/* 2757*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m20406_gshared/* 2758*/,
	(methodPointerType)&KeyCollection__ctor_m20407_gshared/* 2759*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m20408_gshared/* 2760*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m20409_gshared/* 2761*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m20410_gshared/* 2762*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m20411_gshared/* 2763*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m20412_gshared/* 2764*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m20413_gshared/* 2765*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m20414_gshared/* 2766*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m20415_gshared/* 2767*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m20416_gshared/* 2768*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m20417_gshared/* 2769*/,
	(methodPointerType)&KeyCollection_CopyTo_m20418_gshared/* 2770*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m20419_gshared/* 2771*/,
	(methodPointerType)&KeyCollection_get_Count_m20420_gshared/* 2772*/,
	(methodPointerType)&Enumerator__ctor_m20421_gshared/* 2773*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20422_gshared/* 2774*/,
	(methodPointerType)&Enumerator_Dispose_m20423_gshared/* 2775*/,
	(methodPointerType)&Enumerator_MoveNext_m20424_gshared/* 2776*/,
	(methodPointerType)&Enumerator_get_Current_m20425_gshared/* 2777*/,
	(methodPointerType)&Enumerator__ctor_m20426_gshared/* 2778*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20427_gshared/* 2779*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20428_gshared/* 2780*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20429_gshared/* 2781*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20430_gshared/* 2782*/,
	(methodPointerType)&Enumerator_MoveNext_m20431_gshared/* 2783*/,
	(methodPointerType)&Enumerator_get_Current_m20432_gshared/* 2784*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m20433_gshared/* 2785*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m20434_gshared/* 2786*/,
	(methodPointerType)&Enumerator_VerifyState_m20435_gshared/* 2787*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m20436_gshared/* 2788*/,
	(methodPointerType)&Enumerator_Dispose_m20437_gshared/* 2789*/,
	(methodPointerType)&Transform_1__ctor_m20438_gshared/* 2790*/,
	(methodPointerType)&Transform_1_Invoke_m20439_gshared/* 2791*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20440_gshared/* 2792*/,
	(methodPointerType)&Transform_1_EndInvoke_m20441_gshared/* 2793*/,
	(methodPointerType)&ValueCollection__ctor_m20442_gshared/* 2794*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20443_gshared/* 2795*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20444_gshared/* 2796*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20445_gshared/* 2797*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20446_gshared/* 2798*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20447_gshared/* 2799*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m20448_gshared/* 2800*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20449_gshared/* 2801*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20450_gshared/* 2802*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20451_gshared/* 2803*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m20452_gshared/* 2804*/,
	(methodPointerType)&ValueCollection_CopyTo_m20453_gshared/* 2805*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m20454_gshared/* 2806*/,
	(methodPointerType)&ValueCollection_get_Count_m20455_gshared/* 2807*/,
	(methodPointerType)&Enumerator__ctor_m20456_gshared/* 2808*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20457_gshared/* 2809*/,
	(methodPointerType)&Enumerator_Dispose_m20458_gshared/* 2810*/,
	(methodPointerType)&Enumerator_MoveNext_m20459_gshared/* 2811*/,
	(methodPointerType)&Enumerator_get_Current_m20460_gshared/* 2812*/,
	(methodPointerType)&Transform_1__ctor_m20461_gshared/* 2813*/,
	(methodPointerType)&Transform_1_Invoke_m20462_gshared/* 2814*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20463_gshared/* 2815*/,
	(methodPointerType)&Transform_1_EndInvoke_m20464_gshared/* 2816*/,
	(methodPointerType)&Transform_1__ctor_m20465_gshared/* 2817*/,
	(methodPointerType)&Transform_1_Invoke_m20466_gshared/* 2818*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20467_gshared/* 2819*/,
	(methodPointerType)&Transform_1_EndInvoke_m20468_gshared/* 2820*/,
	(methodPointerType)&Transform_1__ctor_m20469_gshared/* 2821*/,
	(methodPointerType)&Transform_1_Invoke_m20470_gshared/* 2822*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20471_gshared/* 2823*/,
	(methodPointerType)&Transform_1_EndInvoke_m20472_gshared/* 2824*/,
	(methodPointerType)&ShimEnumerator__ctor_m20473_gshared/* 2825*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m20474_gshared/* 2826*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m20475_gshared/* 2827*/,
	(methodPointerType)&ShimEnumerator_get_Key_m20476_gshared/* 2828*/,
	(methodPointerType)&ShimEnumerator_get_Value_m20477_gshared/* 2829*/,
	(methodPointerType)&ShimEnumerator_get_Current_m20478_gshared/* 2830*/,
	(methodPointerType)&EqualityComparer_1__ctor_m20479_gshared/* 2831*/,
	(methodPointerType)&EqualityComparer_1__cctor_m20480_gshared/* 2832*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20481_gshared/* 2833*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20482_gshared/* 2834*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m20483_gshared/* 2835*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m20484_gshared/* 2836*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m20485_gshared/* 2837*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m20486_gshared/* 2838*/,
	(methodPointerType)&DefaultComparer__ctor_m20487_gshared/* 2839*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m20488_gshared/* 2840*/,
	(methodPointerType)&DefaultComparer_Equals_m20489_gshared/* 2841*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m20664_gshared/* 2842*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20665_gshared/* 2843*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m20666_gshared/* 2844*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m20667_gshared/* 2845*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m20668_gshared/* 2846*/,
	(methodPointerType)&KeyValuePair_2__ctor_m20669_gshared/* 2847*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m20670_gshared/* 2848*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m20671_gshared/* 2849*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m20672_gshared/* 2850*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m20673_gshared/* 2851*/,
	(methodPointerType)&KeyValuePair_2_ToString_m20674_gshared/* 2852*/,
	(methodPointerType)&Dictionary_2__ctor_m21028_gshared/* 2853*/,
	(methodPointerType)&Dictionary_2__ctor_m21030_gshared/* 2854*/,
	(methodPointerType)&Dictionary_2__ctor_m21032_gshared/* 2855*/,
	(methodPointerType)&Dictionary_2__ctor_m21034_gshared/* 2856*/,
	(methodPointerType)&Dictionary_2__ctor_m21036_gshared/* 2857*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21038_gshared/* 2858*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21040_gshared/* 2859*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m21042_gshared/* 2860*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m21044_gshared/* 2861*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m21046_gshared/* 2862*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m21048_gshared/* 2863*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m21050_gshared/* 2864*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21052_gshared/* 2865*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21054_gshared/* 2866*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21056_gshared/* 2867*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21058_gshared/* 2868*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21060_gshared/* 2869*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21062_gshared/* 2870*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21064_gshared/* 2871*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m21066_gshared/* 2872*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21068_gshared/* 2873*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21070_gshared/* 2874*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21072_gshared/* 2875*/,
	(methodPointerType)&Dictionary_2_get_Count_m21074_gshared/* 2876*/,
	(methodPointerType)&Dictionary_2_get_Item_m21076_gshared/* 2877*/,
	(methodPointerType)&Dictionary_2_set_Item_m21078_gshared/* 2878*/,
	(methodPointerType)&Dictionary_2_Init_m21080_gshared/* 2879*/,
	(methodPointerType)&Dictionary_2_InitArrays_m21082_gshared/* 2880*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m21084_gshared/* 2881*/,
	(methodPointerType)&Dictionary_2_make_pair_m21086_gshared/* 2882*/,
	(methodPointerType)&Dictionary_2_pick_key_m21088_gshared/* 2883*/,
	(methodPointerType)&Dictionary_2_pick_value_m21090_gshared/* 2884*/,
	(methodPointerType)&Dictionary_2_CopyTo_m21092_gshared/* 2885*/,
	(methodPointerType)&Dictionary_2_Resize_m21094_gshared/* 2886*/,
	(methodPointerType)&Dictionary_2_Add_m21096_gshared/* 2887*/,
	(methodPointerType)&Dictionary_2_Clear_m21098_gshared/* 2888*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m21100_gshared/* 2889*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m21102_gshared/* 2890*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m21104_gshared/* 2891*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m21106_gshared/* 2892*/,
	(methodPointerType)&Dictionary_2_Remove_m21108_gshared/* 2893*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m21110_gshared/* 2894*/,
	(methodPointerType)&Dictionary_2_get_Keys_m21112_gshared/* 2895*/,
	(methodPointerType)&Dictionary_2_get_Values_m21114_gshared/* 2896*/,
	(methodPointerType)&Dictionary_2_ToTKey_m21116_gshared/* 2897*/,
	(methodPointerType)&Dictionary_2_ToTValue_m21118_gshared/* 2898*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m21120_gshared/* 2899*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m21122_gshared/* 2900*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m21124_gshared/* 2901*/,
	(methodPointerType)&KeyCollection__ctor_m21125_gshared/* 2902*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21126_gshared/* 2903*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21127_gshared/* 2904*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21128_gshared/* 2905*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21129_gshared/* 2906*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21130_gshared/* 2907*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m21131_gshared/* 2908*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21132_gshared/* 2909*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21133_gshared/* 2910*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21134_gshared/* 2911*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m21135_gshared/* 2912*/,
	(methodPointerType)&KeyCollection_CopyTo_m21136_gshared/* 2913*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m21137_gshared/* 2914*/,
	(methodPointerType)&KeyCollection_get_Count_m21138_gshared/* 2915*/,
	(methodPointerType)&Enumerator__ctor_m21139_gshared/* 2916*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21140_gshared/* 2917*/,
	(methodPointerType)&Enumerator_Dispose_m21141_gshared/* 2918*/,
	(methodPointerType)&Enumerator_MoveNext_m21142_gshared/* 2919*/,
	(methodPointerType)&Enumerator_get_Current_m21143_gshared/* 2920*/,
	(methodPointerType)&Enumerator__ctor_m21144_gshared/* 2921*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21145_gshared/* 2922*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21146_gshared/* 2923*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21147_gshared/* 2924*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21148_gshared/* 2925*/,
	(methodPointerType)&Enumerator_MoveNext_m21149_gshared/* 2926*/,
	(methodPointerType)&Enumerator_get_Current_m21150_gshared/* 2927*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m21151_gshared/* 2928*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m21152_gshared/* 2929*/,
	(methodPointerType)&Enumerator_VerifyState_m21153_gshared/* 2930*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m21154_gshared/* 2931*/,
	(methodPointerType)&Enumerator_Dispose_m21155_gshared/* 2932*/,
	(methodPointerType)&Transform_1__ctor_m21156_gshared/* 2933*/,
	(methodPointerType)&Transform_1_Invoke_m21157_gshared/* 2934*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21158_gshared/* 2935*/,
	(methodPointerType)&Transform_1_EndInvoke_m21159_gshared/* 2936*/,
	(methodPointerType)&ValueCollection__ctor_m21160_gshared/* 2937*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m21161_gshared/* 2938*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m21162_gshared/* 2939*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m21163_gshared/* 2940*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m21164_gshared/* 2941*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m21165_gshared/* 2942*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m21166_gshared/* 2943*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m21167_gshared/* 2944*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m21168_gshared/* 2945*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m21169_gshared/* 2946*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m21170_gshared/* 2947*/,
	(methodPointerType)&ValueCollection_CopyTo_m21171_gshared/* 2948*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m21172_gshared/* 2949*/,
	(methodPointerType)&ValueCollection_get_Count_m21173_gshared/* 2950*/,
	(methodPointerType)&Enumerator__ctor_m21174_gshared/* 2951*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21175_gshared/* 2952*/,
	(methodPointerType)&Enumerator_Dispose_m21176_gshared/* 2953*/,
	(methodPointerType)&Enumerator_MoveNext_m21177_gshared/* 2954*/,
	(methodPointerType)&Enumerator_get_Current_m21178_gshared/* 2955*/,
	(methodPointerType)&Transform_1__ctor_m21179_gshared/* 2956*/,
	(methodPointerType)&Transform_1_Invoke_m21180_gshared/* 2957*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21181_gshared/* 2958*/,
	(methodPointerType)&Transform_1_EndInvoke_m21182_gshared/* 2959*/,
	(methodPointerType)&Transform_1__ctor_m21183_gshared/* 2960*/,
	(methodPointerType)&Transform_1_Invoke_m21184_gshared/* 2961*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21185_gshared/* 2962*/,
	(methodPointerType)&Transform_1_EndInvoke_m21186_gshared/* 2963*/,
	(methodPointerType)&Transform_1__ctor_m21187_gshared/* 2964*/,
	(methodPointerType)&Transform_1_Invoke_m21188_gshared/* 2965*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21189_gshared/* 2966*/,
	(methodPointerType)&Transform_1_EndInvoke_m21190_gshared/* 2967*/,
	(methodPointerType)&ShimEnumerator__ctor_m21191_gshared/* 2968*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m21192_gshared/* 2969*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m21193_gshared/* 2970*/,
	(methodPointerType)&ShimEnumerator_get_Key_m21194_gshared/* 2971*/,
	(methodPointerType)&ShimEnumerator_get_Value_m21195_gshared/* 2972*/,
	(methodPointerType)&ShimEnumerator_get_Current_m21196_gshared/* 2973*/,
	(methodPointerType)&EqualityComparer_1__ctor_m21197_gshared/* 2974*/,
	(methodPointerType)&EqualityComparer_1__cctor_m21198_gshared/* 2975*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21199_gshared/* 2976*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21200_gshared/* 2977*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m21201_gshared/* 2978*/,
	(methodPointerType)&DefaultComparer__ctor_m21202_gshared/* 2979*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m21203_gshared/* 2980*/,
	(methodPointerType)&DefaultComparer_Equals_m21204_gshared/* 2981*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21395_gshared/* 2982*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21396_gshared/* 2983*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21397_gshared/* 2984*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21398_gshared/* 2985*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21399_gshared/* 2986*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21400_gshared/* 2987*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21401_gshared/* 2988*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21402_gshared/* 2989*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21403_gshared/* 2990*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21404_gshared/* 2991*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m21528_gshared/* 2992*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m21529_gshared/* 2993*/,
	(methodPointerType)&InvokableCall_1__ctor_m21530_gshared/* 2994*/,
	(methodPointerType)&InvokableCall_1__ctor_m21531_gshared/* 2995*/,
	(methodPointerType)&InvokableCall_1_Invoke_m21532_gshared/* 2996*/,
	(methodPointerType)&InvokableCall_1_Find_m21533_gshared/* 2997*/,
	(methodPointerType)&UnityAction_1__ctor_m21534_gshared/* 2998*/,
	(methodPointerType)&UnityAction_1_Invoke_m21535_gshared/* 2999*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m21536_gshared/* 3000*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m21537_gshared/* 3001*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m21545_gshared/* 3002*/,
	(methodPointerType)&Dictionary_2__ctor_m21740_gshared/* 3003*/,
	(methodPointerType)&Dictionary_2__ctor_m21743_gshared/* 3004*/,
	(methodPointerType)&Dictionary_2__ctor_m21745_gshared/* 3005*/,
	(methodPointerType)&Dictionary_2__ctor_m21747_gshared/* 3006*/,
	(methodPointerType)&Dictionary_2__ctor_m21749_gshared/* 3007*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21751_gshared/* 3008*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21753_gshared/* 3009*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m21755_gshared/* 3010*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m21757_gshared/* 3011*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m21759_gshared/* 3012*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m21761_gshared/* 3013*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m21763_gshared/* 3014*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21765_gshared/* 3015*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21767_gshared/* 3016*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21769_gshared/* 3017*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21771_gshared/* 3018*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21773_gshared/* 3019*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21775_gshared/* 3020*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21777_gshared/* 3021*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m21779_gshared/* 3022*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21781_gshared/* 3023*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21783_gshared/* 3024*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21785_gshared/* 3025*/,
	(methodPointerType)&Dictionary_2_get_Count_m21787_gshared/* 3026*/,
	(methodPointerType)&Dictionary_2_get_Item_m21789_gshared/* 3027*/,
	(methodPointerType)&Dictionary_2_set_Item_m21791_gshared/* 3028*/,
	(methodPointerType)&Dictionary_2_Init_m21793_gshared/* 3029*/,
	(methodPointerType)&Dictionary_2_InitArrays_m21795_gshared/* 3030*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m21797_gshared/* 3031*/,
	(methodPointerType)&Dictionary_2_make_pair_m21799_gshared/* 3032*/,
	(methodPointerType)&Dictionary_2_pick_key_m21801_gshared/* 3033*/,
	(methodPointerType)&Dictionary_2_pick_value_m21803_gshared/* 3034*/,
	(methodPointerType)&Dictionary_2_CopyTo_m21805_gshared/* 3035*/,
	(methodPointerType)&Dictionary_2_Resize_m21807_gshared/* 3036*/,
	(methodPointerType)&Dictionary_2_Add_m21809_gshared/* 3037*/,
	(methodPointerType)&Dictionary_2_Clear_m21811_gshared/* 3038*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m21813_gshared/* 3039*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m21815_gshared/* 3040*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m21817_gshared/* 3041*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m21819_gshared/* 3042*/,
	(methodPointerType)&Dictionary_2_Remove_m21821_gshared/* 3043*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m21823_gshared/* 3044*/,
	(methodPointerType)&Dictionary_2_get_Keys_m21825_gshared/* 3045*/,
	(methodPointerType)&Dictionary_2_get_Values_m21827_gshared/* 3046*/,
	(methodPointerType)&Dictionary_2_ToTKey_m21829_gshared/* 3047*/,
	(methodPointerType)&Dictionary_2_ToTValue_m21831_gshared/* 3048*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m21833_gshared/* 3049*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m21835_gshared/* 3050*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m21837_gshared/* 3051*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21838_gshared/* 3052*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21839_gshared/* 3053*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21840_gshared/* 3054*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21841_gshared/* 3055*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21842_gshared/* 3056*/,
	(methodPointerType)&KeyValuePair_2__ctor_m21843_gshared/* 3057*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m21844_gshared/* 3058*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m21845_gshared/* 3059*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m21846_gshared/* 3060*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m21847_gshared/* 3061*/,
	(methodPointerType)&KeyValuePair_2_ToString_m21848_gshared/* 3062*/,
	(methodPointerType)&KeyCollection__ctor_m21849_gshared/* 3063*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21850_gshared/* 3064*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21851_gshared/* 3065*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21852_gshared/* 3066*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21853_gshared/* 3067*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21854_gshared/* 3068*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m21855_gshared/* 3069*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21856_gshared/* 3070*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21857_gshared/* 3071*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21858_gshared/* 3072*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m21859_gshared/* 3073*/,
	(methodPointerType)&KeyCollection_CopyTo_m21860_gshared/* 3074*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m21861_gshared/* 3075*/,
	(methodPointerType)&KeyCollection_get_Count_m21862_gshared/* 3076*/,
	(methodPointerType)&Enumerator__ctor_m21863_gshared/* 3077*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21864_gshared/* 3078*/,
	(methodPointerType)&Enumerator_Dispose_m21865_gshared/* 3079*/,
	(methodPointerType)&Enumerator_MoveNext_m21866_gshared/* 3080*/,
	(methodPointerType)&Enumerator_get_Current_m21867_gshared/* 3081*/,
	(methodPointerType)&Enumerator__ctor_m21868_gshared/* 3082*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21869_gshared/* 3083*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21870_gshared/* 3084*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21871_gshared/* 3085*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21872_gshared/* 3086*/,
	(methodPointerType)&Enumerator_MoveNext_m21873_gshared/* 3087*/,
	(methodPointerType)&Enumerator_get_Current_m21874_gshared/* 3088*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m21875_gshared/* 3089*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m21876_gshared/* 3090*/,
	(methodPointerType)&Enumerator_VerifyState_m21877_gshared/* 3091*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m21878_gshared/* 3092*/,
	(methodPointerType)&Enumerator_Dispose_m21879_gshared/* 3093*/,
	(methodPointerType)&Transform_1__ctor_m21880_gshared/* 3094*/,
	(methodPointerType)&Transform_1_Invoke_m21881_gshared/* 3095*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21882_gshared/* 3096*/,
	(methodPointerType)&Transform_1_EndInvoke_m21883_gshared/* 3097*/,
	(methodPointerType)&ValueCollection__ctor_m21884_gshared/* 3098*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m21885_gshared/* 3099*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m21886_gshared/* 3100*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m21887_gshared/* 3101*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m21888_gshared/* 3102*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m21889_gshared/* 3103*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m21890_gshared/* 3104*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m21891_gshared/* 3105*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m21892_gshared/* 3106*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m21893_gshared/* 3107*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m21894_gshared/* 3108*/,
	(methodPointerType)&ValueCollection_CopyTo_m21895_gshared/* 3109*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m21896_gshared/* 3110*/,
	(methodPointerType)&ValueCollection_get_Count_m21897_gshared/* 3111*/,
	(methodPointerType)&Enumerator__ctor_m21898_gshared/* 3112*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21899_gshared/* 3113*/,
	(methodPointerType)&Enumerator_Dispose_m21900_gshared/* 3114*/,
	(methodPointerType)&Enumerator_MoveNext_m21901_gshared/* 3115*/,
	(methodPointerType)&Enumerator_get_Current_m21902_gshared/* 3116*/,
	(methodPointerType)&Transform_1__ctor_m21903_gshared/* 3117*/,
	(methodPointerType)&Transform_1_Invoke_m21904_gshared/* 3118*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21905_gshared/* 3119*/,
	(methodPointerType)&Transform_1_EndInvoke_m21906_gshared/* 3120*/,
	(methodPointerType)&Transform_1__ctor_m21907_gshared/* 3121*/,
	(methodPointerType)&Transform_1_Invoke_m21908_gshared/* 3122*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21909_gshared/* 3123*/,
	(methodPointerType)&Transform_1_EndInvoke_m21910_gshared/* 3124*/,
	(methodPointerType)&Transform_1__ctor_m21911_gshared/* 3125*/,
	(methodPointerType)&Transform_1_Invoke_m21912_gshared/* 3126*/,
	(methodPointerType)&Transform_1_BeginInvoke_m21913_gshared/* 3127*/,
	(methodPointerType)&Transform_1_EndInvoke_m21914_gshared/* 3128*/,
	(methodPointerType)&ShimEnumerator__ctor_m21915_gshared/* 3129*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m21916_gshared/* 3130*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m21917_gshared/* 3131*/,
	(methodPointerType)&ShimEnumerator_get_Key_m21918_gshared/* 3132*/,
	(methodPointerType)&ShimEnumerator_get_Value_m21919_gshared/* 3133*/,
	(methodPointerType)&ShimEnumerator_get_Current_m21920_gshared/* 3134*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21976_gshared/* 3135*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21977_gshared/* 3136*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21978_gshared/* 3137*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21979_gshared/* 3138*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21980_gshared/* 3139*/,
	(methodPointerType)&Dictionary_2__ctor_m21991_gshared/* 3140*/,
	(methodPointerType)&Dictionary_2__ctor_m21992_gshared/* 3141*/,
	(methodPointerType)&Dictionary_2__ctor_m21993_gshared/* 3142*/,
	(methodPointerType)&Dictionary_2__ctor_m21994_gshared/* 3143*/,
	(methodPointerType)&Dictionary_2__ctor_m21995_gshared/* 3144*/,
	(methodPointerType)&Dictionary_2__ctor_m21996_gshared/* 3145*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21997_gshared/* 3146*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21998_gshared/* 3147*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m21999_gshared/* 3148*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m22000_gshared/* 3149*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m22001_gshared/* 3150*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m22002_gshared/* 3151*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m22003_gshared/* 3152*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22004_gshared/* 3153*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22005_gshared/* 3154*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22006_gshared/* 3155*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22007_gshared/* 3156*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22008_gshared/* 3157*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22009_gshared/* 3158*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22010_gshared/* 3159*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m22011_gshared/* 3160*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22012_gshared/* 3161*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22013_gshared/* 3162*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22014_gshared/* 3163*/,
	(methodPointerType)&Dictionary_2_get_Count_m22015_gshared/* 3164*/,
	(methodPointerType)&Dictionary_2_get_Item_m22016_gshared/* 3165*/,
	(methodPointerType)&Dictionary_2_set_Item_m22017_gshared/* 3166*/,
	(methodPointerType)&Dictionary_2_Init_m22018_gshared/* 3167*/,
	(methodPointerType)&Dictionary_2_InitArrays_m22019_gshared/* 3168*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m22020_gshared/* 3169*/,
	(methodPointerType)&Dictionary_2_make_pair_m22021_gshared/* 3170*/,
	(methodPointerType)&Dictionary_2_pick_key_m22022_gshared/* 3171*/,
	(methodPointerType)&Dictionary_2_pick_value_m22023_gshared/* 3172*/,
	(methodPointerType)&Dictionary_2_CopyTo_m22024_gshared/* 3173*/,
	(methodPointerType)&Dictionary_2_Resize_m22025_gshared/* 3174*/,
	(methodPointerType)&Dictionary_2_Add_m22026_gshared/* 3175*/,
	(methodPointerType)&Dictionary_2_Clear_m22027_gshared/* 3176*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m22028_gshared/* 3177*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m22029_gshared/* 3178*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m22030_gshared/* 3179*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m22031_gshared/* 3180*/,
	(methodPointerType)&Dictionary_2_Remove_m22032_gshared/* 3181*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m22033_gshared/* 3182*/,
	(methodPointerType)&Dictionary_2_get_Keys_m22034_gshared/* 3183*/,
	(methodPointerType)&Dictionary_2_get_Values_m22035_gshared/* 3184*/,
	(methodPointerType)&Dictionary_2_ToTKey_m22036_gshared/* 3185*/,
	(methodPointerType)&Dictionary_2_ToTValue_m22037_gshared/* 3186*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m22038_gshared/* 3187*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m22039_gshared/* 3188*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m22040_gshared/* 3189*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22041_gshared/* 3190*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22042_gshared/* 3191*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22043_gshared/* 3192*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22044_gshared/* 3193*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22045_gshared/* 3194*/,
	(methodPointerType)&KeyValuePair_2__ctor_m22046_gshared/* 3195*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m22047_gshared/* 3196*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m22048_gshared/* 3197*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m22049_gshared/* 3198*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m22050_gshared/* 3199*/,
	(methodPointerType)&KeyValuePair_2_ToString_m22051_gshared/* 3200*/,
	(methodPointerType)&KeyCollection__ctor_m22052_gshared/* 3201*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22053_gshared/* 3202*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22054_gshared/* 3203*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22055_gshared/* 3204*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22056_gshared/* 3205*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22057_gshared/* 3206*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m22058_gshared/* 3207*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22059_gshared/* 3208*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22060_gshared/* 3209*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22061_gshared/* 3210*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m22062_gshared/* 3211*/,
	(methodPointerType)&KeyCollection_CopyTo_m22063_gshared/* 3212*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m22064_gshared/* 3213*/,
	(methodPointerType)&KeyCollection_get_Count_m22065_gshared/* 3214*/,
	(methodPointerType)&Enumerator__ctor_m22066_gshared/* 3215*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22067_gshared/* 3216*/,
	(methodPointerType)&Enumerator_Dispose_m22068_gshared/* 3217*/,
	(methodPointerType)&Enumerator_MoveNext_m22069_gshared/* 3218*/,
	(methodPointerType)&Enumerator_get_Current_m22070_gshared/* 3219*/,
	(methodPointerType)&Enumerator__ctor_m22071_gshared/* 3220*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22072_gshared/* 3221*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22073_gshared/* 3222*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22074_gshared/* 3223*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22075_gshared/* 3224*/,
	(methodPointerType)&Enumerator_MoveNext_m22076_gshared/* 3225*/,
	(methodPointerType)&Enumerator_get_Current_m22077_gshared/* 3226*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m22078_gshared/* 3227*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m22079_gshared/* 3228*/,
	(methodPointerType)&Enumerator_VerifyState_m22080_gshared/* 3229*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m22081_gshared/* 3230*/,
	(methodPointerType)&Enumerator_Dispose_m22082_gshared/* 3231*/,
	(methodPointerType)&Transform_1__ctor_m22083_gshared/* 3232*/,
	(methodPointerType)&Transform_1_Invoke_m22084_gshared/* 3233*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22085_gshared/* 3234*/,
	(methodPointerType)&Transform_1_EndInvoke_m22086_gshared/* 3235*/,
	(methodPointerType)&ValueCollection__ctor_m22087_gshared/* 3236*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22088_gshared/* 3237*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22089_gshared/* 3238*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22090_gshared/* 3239*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22091_gshared/* 3240*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22092_gshared/* 3241*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m22093_gshared/* 3242*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22094_gshared/* 3243*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22095_gshared/* 3244*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22096_gshared/* 3245*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m22097_gshared/* 3246*/,
	(methodPointerType)&ValueCollection_CopyTo_m22098_gshared/* 3247*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m22099_gshared/* 3248*/,
	(methodPointerType)&ValueCollection_get_Count_m22100_gshared/* 3249*/,
	(methodPointerType)&Enumerator__ctor_m22101_gshared/* 3250*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22102_gshared/* 3251*/,
	(methodPointerType)&Enumerator_Dispose_m22103_gshared/* 3252*/,
	(methodPointerType)&Enumerator_MoveNext_m22104_gshared/* 3253*/,
	(methodPointerType)&Enumerator_get_Current_m22105_gshared/* 3254*/,
	(methodPointerType)&Transform_1__ctor_m22106_gshared/* 3255*/,
	(methodPointerType)&Transform_1_Invoke_m22107_gshared/* 3256*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22108_gshared/* 3257*/,
	(methodPointerType)&Transform_1_EndInvoke_m22109_gshared/* 3258*/,
	(methodPointerType)&Transform_1__ctor_m22110_gshared/* 3259*/,
	(methodPointerType)&Transform_1_Invoke_m22111_gshared/* 3260*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22112_gshared/* 3261*/,
	(methodPointerType)&Transform_1_EndInvoke_m22113_gshared/* 3262*/,
	(methodPointerType)&ShimEnumerator__ctor_m22114_gshared/* 3263*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m22115_gshared/* 3264*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m22116_gshared/* 3265*/,
	(methodPointerType)&ShimEnumerator_get_Key_m22117_gshared/* 3266*/,
	(methodPointerType)&ShimEnumerator_get_Value_m22118_gshared/* 3267*/,
	(methodPointerType)&ShimEnumerator_get_Current_m22119_gshared/* 3268*/,
	(methodPointerType)&Comparer_1__ctor_m22120_gshared/* 3269*/,
	(methodPointerType)&Comparer_1__cctor_m22121_gshared/* 3270*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m22122_gshared/* 3271*/,
	(methodPointerType)&Comparer_1_get_Default_m22123_gshared/* 3272*/,
	(methodPointerType)&GenericComparer_1__ctor_m22124_gshared/* 3273*/,
	(methodPointerType)&GenericComparer_1_Compare_m22125_gshared/* 3274*/,
	(methodPointerType)&DefaultComparer__ctor_m22126_gshared/* 3275*/,
	(methodPointerType)&DefaultComparer_Compare_m22127_gshared/* 3276*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22128_gshared/* 3277*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22129_gshared/* 3278*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22130_gshared/* 3279*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22131_gshared/* 3280*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22132_gshared/* 3281*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22133_gshared/* 3282*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22134_gshared/* 3283*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22135_gshared/* 3284*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22136_gshared/* 3285*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22137_gshared/* 3286*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22142_gshared/* 3287*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22143_gshared/* 3288*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22144_gshared/* 3289*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22145_gshared/* 3290*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22146_gshared/* 3291*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22167_gshared/* 3292*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22168_gshared/* 3293*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22169_gshared/* 3294*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22170_gshared/* 3295*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22171_gshared/* 3296*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22172_gshared/* 3297*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22173_gshared/* 3298*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22174_gshared/* 3299*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22175_gshared/* 3300*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22176_gshared/* 3301*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22207_gshared/* 3302*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22208_gshared/* 3303*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22209_gshared/* 3304*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22210_gshared/* 3305*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22211_gshared/* 3306*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22237_gshared/* 3307*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22238_gshared/* 3308*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22239_gshared/* 3309*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22240_gshared/* 3310*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22241_gshared/* 3311*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22242_gshared/* 3312*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22243_gshared/* 3313*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22244_gshared/* 3314*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22245_gshared/* 3315*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22246_gshared/* 3316*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22262_gshared/* 3317*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22263_gshared/* 3318*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22264_gshared/* 3319*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22265_gshared/* 3320*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22266_gshared/* 3321*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22282_gshared/* 3322*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22283_gshared/* 3323*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22284_gshared/* 3324*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22285_gshared/* 3325*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22286_gshared/* 3326*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22287_gshared/* 3327*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22288_gshared/* 3328*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22289_gshared/* 3329*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22290_gshared/* 3330*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22291_gshared/* 3331*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22292_gshared/* 3332*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22293_gshared/* 3333*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22294_gshared/* 3334*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22295_gshared/* 3335*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22296_gshared/* 3336*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22350_gshared/* 3337*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22351_gshared/* 3338*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22352_gshared/* 3339*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22353_gshared/* 3340*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22354_gshared/* 3341*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22355_gshared/* 3342*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22356_gshared/* 3343*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22357_gshared/* 3344*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22358_gshared/* 3345*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22359_gshared/* 3346*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22360_gshared/* 3347*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22361_gshared/* 3348*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22362_gshared/* 3349*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22363_gshared/* 3350*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22364_gshared/* 3351*/,
	(methodPointerType)&GenericComparer_1_Compare_m22473_gshared/* 3352*/,
	(methodPointerType)&Comparer_1__ctor_m22474_gshared/* 3353*/,
	(methodPointerType)&Comparer_1__cctor_m22475_gshared/* 3354*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m22476_gshared/* 3355*/,
	(methodPointerType)&Comparer_1_get_Default_m22477_gshared/* 3356*/,
	(methodPointerType)&DefaultComparer__ctor_m22478_gshared/* 3357*/,
	(methodPointerType)&DefaultComparer_Compare_m22479_gshared/* 3358*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m22480_gshared/* 3359*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m22481_gshared/* 3360*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22482_gshared/* 3361*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22483_gshared/* 3362*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22484_gshared/* 3363*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22485_gshared/* 3364*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22486_gshared/* 3365*/,
	(methodPointerType)&DefaultComparer__ctor_m22487_gshared/* 3366*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22488_gshared/* 3367*/,
	(methodPointerType)&DefaultComparer_Equals_m22489_gshared/* 3368*/,
	(methodPointerType)&GenericComparer_1_Compare_m22490_gshared/* 3369*/,
	(methodPointerType)&Comparer_1__ctor_m22491_gshared/* 3370*/,
	(methodPointerType)&Comparer_1__cctor_m22492_gshared/* 3371*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m22493_gshared/* 3372*/,
	(methodPointerType)&Comparer_1_get_Default_m22494_gshared/* 3373*/,
	(methodPointerType)&DefaultComparer__ctor_m22495_gshared/* 3374*/,
	(methodPointerType)&DefaultComparer_Compare_m22496_gshared/* 3375*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m22497_gshared/* 3376*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m22498_gshared/* 3377*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22499_gshared/* 3378*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22500_gshared/* 3379*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22501_gshared/* 3380*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22502_gshared/* 3381*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22503_gshared/* 3382*/,
	(methodPointerType)&DefaultComparer__ctor_m22504_gshared/* 3383*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22505_gshared/* 3384*/,
	(methodPointerType)&DefaultComparer_Equals_m22506_gshared/* 3385*/,
	(methodPointerType)&Nullable_1_Equals_m22507_gshared/* 3386*/,
	(methodPointerType)&Nullable_1_Equals_m22508_gshared/* 3387*/,
	(methodPointerType)&Nullable_1_GetHashCode_m22509_gshared/* 3388*/,
	(methodPointerType)&Nullable_1_ToString_m22510_gshared/* 3389*/,
	(methodPointerType)&GenericComparer_1_Compare_m22511_gshared/* 3390*/,
	(methodPointerType)&Comparer_1__ctor_m22512_gshared/* 3391*/,
	(methodPointerType)&Comparer_1__cctor_m22513_gshared/* 3392*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m22514_gshared/* 3393*/,
	(methodPointerType)&Comparer_1_get_Default_m22515_gshared/* 3394*/,
	(methodPointerType)&DefaultComparer__ctor_m22516_gshared/* 3395*/,
	(methodPointerType)&DefaultComparer_Compare_m22517_gshared/* 3396*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m22518_gshared/* 3397*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m22519_gshared/* 3398*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22520_gshared/* 3399*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22521_gshared/* 3400*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22522_gshared/* 3401*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22523_gshared/* 3402*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22524_gshared/* 3403*/,
	(methodPointerType)&DefaultComparer__ctor_m22525_gshared/* 3404*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22526_gshared/* 3405*/,
	(methodPointerType)&DefaultComparer_Equals_m22527_gshared/* 3406*/,
	(methodPointerType)&GenericComparer_1_Compare_m22528_gshared/* 3407*/,
	(methodPointerType)&Comparer_1__ctor_m22529_gshared/* 3408*/,
	(methodPointerType)&Comparer_1__cctor_m22530_gshared/* 3409*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m22531_gshared/* 3410*/,
	(methodPointerType)&Comparer_1_get_Default_m22532_gshared/* 3411*/,
	(methodPointerType)&DefaultComparer__ctor_m22533_gshared/* 3412*/,
	(methodPointerType)&DefaultComparer_Compare_m22534_gshared/* 3413*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m22535_gshared/* 3414*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m22536_gshared/* 3415*/,
	(methodPointerType)&EqualityComparer_1__ctor_m22537_gshared/* 3416*/,
	(methodPointerType)&EqualityComparer_1__cctor_m22538_gshared/* 3417*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22539_gshared/* 3418*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22540_gshared/* 3419*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m22541_gshared/* 3420*/,
	(methodPointerType)&DefaultComparer__ctor_m22542_gshared/* 3421*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m22543_gshared/* 3422*/,
	(methodPointerType)&DefaultComparer_Equals_m22544_gshared/* 3423*/,
};
const InvokerMethod g_Il2CppInvokerPointers[537] = 
{
	NULL/* 0*/,
	RuntimeInvoker_Object_t_Object_t/* 1*/,
	RuntimeInvoker_Void_t272/* 2*/,
	RuntimeInvoker_Boolean_t273_Object_t_Object_t/* 3*/,
	RuntimeInvoker_Boolean_t273_Object_t_Object_t_Object_t/* 4*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* 5*/,
	RuntimeInvoker_Boolean_t273_Object_t/* 6*/,
	RuntimeInvoker_Void_t272_Object_t_Object_t/* 7*/,
	RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* 8*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* 9*/,
	RuntimeInvoker_Void_t272_Object_t/* 10*/,
	RuntimeInvoker_Boolean_t273_ObjectU26_t1197_Object_t/* 11*/,
	RuntimeInvoker_Void_t272_ObjectU26_t1197_Object_t/* 12*/,
	RuntimeInvoker_Int32_t253/* 13*/,
	RuntimeInvoker_Boolean_t273/* 14*/,
	RuntimeInvoker_Object_t_Int32_t253/* 15*/,
	RuntimeInvoker_Void_t272_Int32_t253_Object_t/* 16*/,
	RuntimeInvoker_Object_t/* 17*/,
	RuntimeInvoker_Void_t272_Object_t_Int32_t253/* 18*/,
	RuntimeInvoker_Int32_t253_Object_t/* 19*/,
	RuntimeInvoker_Void_t272_Int32_t253/* 20*/,
	RuntimeInvoker_Object_t_SByte_t274/* 21*/,
	RuntimeInvoker_Void_t272_SByte_t274_Object_t/* 22*/,
	RuntimeInvoker_Object_t_Object_t_Object_t/* 23*/,
	RuntimeInvoker_Boolean_t273_Object_t_ObjectU26_t1197/* 24*/,
	RuntimeInvoker_Void_t272_KeyValuePair_2_t2815/* 25*/,
	RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2815/* 26*/,
	RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t/* 27*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* 28*/,
	RuntimeInvoker_Void_t272_Object_t_Object_t_Object_t_Object_t/* 29*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* 30*/,
	RuntimeInvoker_Enumerator_t3029/* 31*/,
	RuntimeInvoker_Void_t272_Int32_t253_ObjectU26_t1197/* 32*/,
	RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253/* 33*/,
	RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253/* 34*/,
	RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253_Object_t/* 35*/,
	RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253_Int32_t253_Object_t/* 36*/,
	RuntimeInvoker_Void_t272_Object_t_Int32_t253_Object_t/* 37*/,
	RuntimeInvoker_Int32_t253_Object_t_Object_t_Object_t/* 38*/,
	RuntimeInvoker_Void_t272_ObjectU5BU5DU26_t2398_Int32_t253/* 39*/,
	RuntimeInvoker_Void_t272_ObjectU5BU5DU26_t2398_Int32_t253_Int32_t253/* 40*/,
	RuntimeInvoker_Int32_t253_Object_t_Object_t/* 41*/,
	RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Object_t/* 42*/,
	RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t/* 43*/,
	RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Object_t_Object_t/* 44*/,
	RuntimeInvoker_Int32_t253_Object_t_Object_t_Int32_t253/* 45*/,
	RuntimeInvoker_Int32_t253_Object_t_Object_t_Int32_t253_Int32_t253/* 46*/,
	RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* 47*/,
	RuntimeInvoker_KeyValuePair_2_t2815_Object_t_Object_t/* 48*/,
	RuntimeInvoker_Enumerator_t2822/* 49*/,
	RuntimeInvoker_DictionaryEntry_t1430_Object_t_Object_t/* 50*/,
	RuntimeInvoker_DictionaryEntry_t1430/* 51*/,
	RuntimeInvoker_KeyValuePair_2_t2815/* 52*/,
	RuntimeInvoker_Enumerator_t2821/* 53*/,
	RuntimeInvoker_Enumerator_t2825/* 54*/,
	RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253_Object_t/* 55*/,
	RuntimeInvoker_Enumerator_t2843/* 56*/,
	RuntimeInvoker_Void_t272_Int32_t253_Int32_t253/* 57*/,
	RuntimeInvoker_Enumerator_t3066/* 58*/,
	RuntimeInvoker_Enumerator_t3063/* 59*/,
	RuntimeInvoker_KeyValuePair_2_t3059/* 60*/,
	RuntimeInvoker_Void_t272_Color_t65/* 61*/,
	RuntimeInvoker_Void_t272_ColorTween_t506/* 62*/,
	RuntimeInvoker_Boolean_t273_Int32U26_t753_Int32_t253/* 63*/,
	RuntimeInvoker_Boolean_t273_ByteU26_t1656_SByte_t274/* 64*/,
	RuntimeInvoker_Boolean_t273_SingleU26_t1150_Single_t254/* 65*/,
	RuntimeInvoker_Boolean_t273_UInt16U26_t2421_Int16_t752/* 66*/,
	RuntimeInvoker_Void_t272_Single_t254/* 67*/,
	RuntimeInvoker_Void_t272_Vector2_t6/* 68*/,
	RuntimeInvoker_Boolean_t273_NavigationU26_t3954_Navigation_t563/* 69*/,
	RuntimeInvoker_Boolean_t273_ColorBlockU26_t3955_ColorBlock_t516/* 70*/,
	RuntimeInvoker_Boolean_t273_SpriteStateU26_t3956_SpriteState_t580/* 71*/,
	RuntimeInvoker_Void_t272_SByte_t274/* 72*/,
	RuntimeInvoker_Void_t272_Int32U26_t753_Int32_t253/* 73*/,
	RuntimeInvoker_Void_t272_Vector2U26_t1148_Vector2_t6/* 74*/,
	RuntimeInvoker_Void_t272_SingleU26_t1150_Single_t254/* 75*/,
	RuntimeInvoker_Void_t272_ByteU26_t1656_SByte_t274/* 76*/,
	RuntimeInvoker_Void_t272_Object_t_Object_t_Single_t254/* 77*/,
	RuntimeInvoker_Void_t272_Object_t_Object_t_Int32_t253/* 78*/,
	RuntimeInvoker_Void_t272_Object_t_Object_t_SByte_t274/* 79*/,
	RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32_t253/* 80*/,
	RuntimeInvoker_Void_t272_TimeSpan_t1337/* 81*/,
	RuntimeInvoker_TimeSpan_t1337/* 82*/,
	RuntimeInvoker_RaycastHit_t24_Int32_t253/* 83*/,
	RuntimeInvoker_Void_t272_RaycastHit_t24/* 84*/,
	RuntimeInvoker_Boolean_t273_RaycastHit_t24/* 85*/,
	RuntimeInvoker_Int32_t253_RaycastHit_t24/* 86*/,
	RuntimeInvoker_Void_t272_Int32_t253_RaycastHit_t24/* 87*/,
	RuntimeInvoker_Single_t254_Int32_t253/* 88*/,
	RuntimeInvoker_Boolean_t273_Single_t254/* 89*/,
	RuntimeInvoker_Int32_t253_Single_t254/* 90*/,
	RuntimeInvoker_Void_t272_Int32_t253_Single_t254/* 91*/,
	RuntimeInvoker_KeyValuePair_2_t2815_Int32_t253/* 92*/,
	RuntimeInvoker_Int32_t253_KeyValuePair_2_t2815/* 93*/,
	RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t2815/* 94*/,
	RuntimeInvoker_Int32_t253_Int32_t253/* 95*/,
	RuntimeInvoker_Boolean_t273_Int32_t253/* 96*/,
	RuntimeInvoker_Link_t1757_Int32_t253/* 97*/,
	RuntimeInvoker_Void_t272_Link_t1757/* 98*/,
	RuntimeInvoker_Boolean_t273_Link_t1757/* 99*/,
	RuntimeInvoker_Int32_t253_Link_t1757/* 100*/,
	RuntimeInvoker_Void_t272_Int32_t253_Link_t1757/* 101*/,
	RuntimeInvoker_DictionaryEntry_t1430_Int32_t253/* 102*/,
	RuntimeInvoker_Void_t272_DictionaryEntry_t1430/* 103*/,
	RuntimeInvoker_Boolean_t273_DictionaryEntry_t1430/* 104*/,
	RuntimeInvoker_Int32_t253_DictionaryEntry_t1430/* 105*/,
	RuntimeInvoker_Void_t272_Int32_t253_DictionaryEntry_t1430/* 106*/,
	RuntimeInvoker_Touch_t235_Int32_t253/* 107*/,
	RuntimeInvoker_Void_t272_Touch_t235/* 108*/,
	RuntimeInvoker_Boolean_t273_Touch_t235/* 109*/,
	RuntimeInvoker_Int32_t253_Touch_t235/* 110*/,
	RuntimeInvoker_Void_t272_Int32_t253_Touch_t235/* 111*/,
	RuntimeInvoker_Double_t1070_Int32_t253/* 112*/,
	RuntimeInvoker_Void_t272_Double_t1070/* 113*/,
	RuntimeInvoker_Boolean_t273_Double_t1070/* 114*/,
	RuntimeInvoker_Int32_t253_Double_t1070/* 115*/,
	RuntimeInvoker_Void_t272_Int32_t253_Double_t1070/* 116*/,
	RuntimeInvoker_UInt16_t684_Int32_t253/* 117*/,
	RuntimeInvoker_Void_t272_Int16_t752/* 118*/,
	RuntimeInvoker_Boolean_t273_Int16_t752/* 119*/,
	RuntimeInvoker_Int32_t253_Int16_t752/* 120*/,
	RuntimeInvoker_Void_t272_Int32_t253_Int16_t752/* 121*/,
	RuntimeInvoker_Vector2_t6_Int32_t253/* 122*/,
	RuntimeInvoker_Boolean_t273_Vector2_t6/* 123*/,
	RuntimeInvoker_Int32_t253_Vector2_t6/* 124*/,
	RuntimeInvoker_Void_t272_Int32_t253_Vector2_t6/* 125*/,
	RuntimeInvoker_Keyframe_t240_Int32_t253/* 126*/,
	RuntimeInvoker_Void_t272_Keyframe_t240/* 127*/,
	RuntimeInvoker_Boolean_t273_Keyframe_t240/* 128*/,
	RuntimeInvoker_Int32_t253_Keyframe_t240/* 129*/,
	RuntimeInvoker_Void_t272_Int32_t253_Keyframe_t240/* 130*/,
	RuntimeInvoker_Color_t65_Int32_t253/* 131*/,
	RuntimeInvoker_Boolean_t273_Color_t65/* 132*/,
	RuntimeInvoker_Int32_t253_Color_t65/* 133*/,
	RuntimeInvoker_Void_t272_Int32_t253_Color_t65/* 134*/,
	RuntimeInvoker_Vector3_t4_Int32_t253/* 135*/,
	RuntimeInvoker_Void_t272_Vector3_t4/* 136*/,
	RuntimeInvoker_Boolean_t273_Vector3_t4/* 137*/,
	RuntimeInvoker_Int32_t253_Vector3_t4/* 138*/,
	RuntimeInvoker_Void_t272_Int32_t253_Vector3_t4/* 139*/,
	RuntimeInvoker_ContactPoint_t246_Int32_t253/* 140*/,
	RuntimeInvoker_Void_t272_ContactPoint_t246/* 141*/,
	RuntimeInvoker_Boolean_t273_ContactPoint_t246/* 142*/,
	RuntimeInvoker_Int32_t253_ContactPoint_t246/* 143*/,
	RuntimeInvoker_Void_t272_Int32_t253_ContactPoint_t246/* 144*/,
	RuntimeInvoker_ParticleCollisionEvent_t160_Int32_t253/* 145*/,
	RuntimeInvoker_Void_t272_ParticleCollisionEvent_t160/* 146*/,
	RuntimeInvoker_Boolean_t273_ParticleCollisionEvent_t160/* 147*/,
	RuntimeInvoker_Int32_t253_ParticleCollisionEvent_t160/* 148*/,
	RuntimeInvoker_Void_t272_Int32_t253_ParticleCollisionEvent_t160/* 149*/,
	RuntimeInvoker_Void_t272_Vector2U5BU5DU26_t3957_Int32_t253/* 150*/,
	RuntimeInvoker_Void_t272_Vector2U5BU5DU26_t3957_Int32_t253_Int32_t253/* 151*/,
	RuntimeInvoker_Int32_t253_Object_t_Vector2_t6_Int32_t253_Int32_t253/* 152*/,
	RuntimeInvoker_Int32_t253_Vector2_t6_Vector2_t6_Object_t/* 153*/,
	RuntimeInvoker_RaycastResult_t486_Int32_t253/* 154*/,
	RuntimeInvoker_Void_t272_RaycastResult_t486/* 155*/,
	RuntimeInvoker_Boolean_t273_RaycastResult_t486/* 156*/,
	RuntimeInvoker_Int32_t253_RaycastResult_t486/* 157*/,
	RuntimeInvoker_Void_t272_Int32_t253_RaycastResult_t486/* 158*/,
	RuntimeInvoker_Void_t272_RaycastResultU5BU5DU26_t3958_Int32_t253/* 159*/,
	RuntimeInvoker_Void_t272_RaycastResultU5BU5DU26_t3958_Int32_t253_Int32_t253/* 160*/,
	RuntimeInvoker_Int32_t253_Object_t_RaycastResult_t486_Int32_t253_Int32_t253/* 161*/,
	RuntimeInvoker_Int32_t253_RaycastResult_t486_RaycastResult_t486_Object_t/* 162*/,
	RuntimeInvoker_Quaternion_t19_Int32_t253/* 163*/,
	RuntimeInvoker_Void_t272_Quaternion_t19/* 164*/,
	RuntimeInvoker_Boolean_t273_Quaternion_t19/* 165*/,
	RuntimeInvoker_Int32_t253_Quaternion_t19/* 166*/,
	RuntimeInvoker_Void_t272_Int32_t253_Quaternion_t19/* 167*/,
	RuntimeInvoker_KeyValuePair_2_t2953_Int32_t253/* 168*/,
	RuntimeInvoker_Void_t272_KeyValuePair_2_t2953/* 169*/,
	RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2953/* 170*/,
	RuntimeInvoker_Int32_t253_KeyValuePair_2_t2953/* 171*/,
	RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t2953/* 172*/,
	RuntimeInvoker_KeyValuePair_2_t2984_Int32_t253/* 173*/,
	RuntimeInvoker_Void_t272_KeyValuePair_2_t2984/* 174*/,
	RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2984/* 175*/,
	RuntimeInvoker_Int32_t253_KeyValuePair_2_t2984/* 176*/,
	RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t2984/* 177*/,
	RuntimeInvoker_Byte_t680_Int32_t253/* 178*/,
	RuntimeInvoker_Boolean_t273_SByte_t274/* 179*/,
	RuntimeInvoker_Int32_t253_SByte_t274/* 180*/,
	RuntimeInvoker_Void_t272_Int32_t253_SByte_t274/* 181*/,
	RuntimeInvoker_AnimatorClipInfo_t888_Int32_t253/* 182*/,
	RuntimeInvoker_Void_t272_AnimatorClipInfo_t888/* 183*/,
	RuntimeInvoker_Boolean_t273_AnimatorClipInfo_t888/* 184*/,
	RuntimeInvoker_Int32_t253_AnimatorClipInfo_t888/* 185*/,
	RuntimeInvoker_Void_t272_Int32_t253_AnimatorClipInfo_t888/* 186*/,
	RuntimeInvoker_KeyValuePair_2_t3059_Int32_t253/* 187*/,
	RuntimeInvoker_Void_t272_KeyValuePair_2_t3059/* 188*/,
	RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3059/* 189*/,
	RuntimeInvoker_Int32_t253_KeyValuePair_2_t3059/* 190*/,
	RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3059/* 191*/,
	RuntimeInvoker_RaycastHit2D_t665_Int32_t253/* 192*/,
	RuntimeInvoker_Void_t272_RaycastHit2D_t665/* 193*/,
	RuntimeInvoker_Boolean_t273_RaycastHit2D_t665/* 194*/,
	RuntimeInvoker_Int32_t253_RaycastHit2D_t665/* 195*/,
	RuntimeInvoker_Void_t272_Int32_t253_RaycastHit2D_t665/* 196*/,
	RuntimeInvoker_Void_t272_UIVertexU5BU5DU26_t3959_Int32_t253/* 197*/,
	RuntimeInvoker_Void_t272_UIVertexU5BU5DU26_t3959_Int32_t253_Int32_t253/* 198*/,
	RuntimeInvoker_Int32_t253_Object_t_UIVertex_t556_Int32_t253_Int32_t253/* 199*/,
	RuntimeInvoker_Int32_t253_UIVertex_t556_UIVertex_t556_Object_t/* 200*/,
	RuntimeInvoker_UILineInfo_t687_Int32_t253/* 201*/,
	RuntimeInvoker_Void_t272_UILineInfo_t687/* 202*/,
	RuntimeInvoker_Boolean_t273_UILineInfo_t687/* 203*/,
	RuntimeInvoker_Int32_t253_UILineInfo_t687/* 204*/,
	RuntimeInvoker_Void_t272_Int32_t253_UILineInfo_t687/* 205*/,
	RuntimeInvoker_UICharInfo_t689_Int32_t253/* 206*/,
	RuntimeInvoker_Void_t272_UICharInfo_t689/* 207*/,
	RuntimeInvoker_Boolean_t273_UICharInfo_t689/* 208*/,
	RuntimeInvoker_Int32_t253_UICharInfo_t689/* 209*/,
	RuntimeInvoker_Void_t272_Int32_t253_UICharInfo_t689/* 210*/,
	RuntimeInvoker_GcAchievementData_t958_Int32_t253/* 211*/,
	RuntimeInvoker_Void_t272_GcAchievementData_t958/* 212*/,
	RuntimeInvoker_Boolean_t273_GcAchievementData_t958/* 213*/,
	RuntimeInvoker_Int32_t253_GcAchievementData_t958/* 214*/,
	RuntimeInvoker_Void_t272_Int32_t253_GcAchievementData_t958/* 215*/,
	RuntimeInvoker_GcScoreData_t959_Int32_t253/* 216*/,
	RuntimeInvoker_Void_t272_GcScoreData_t959/* 217*/,
	RuntimeInvoker_Boolean_t273_GcScoreData_t959/* 218*/,
	RuntimeInvoker_Int32_t253_GcScoreData_t959/* 219*/,
	RuntimeInvoker_Void_t272_Int32_t253_GcScoreData_t959/* 220*/,
	RuntimeInvoker_IntPtr_t_Int32_t253/* 221*/,
	RuntimeInvoker_Void_t272_IntPtr_t/* 222*/,
	RuntimeInvoker_Boolean_t273_IntPtr_t/* 223*/,
	RuntimeInvoker_Int32_t253_IntPtr_t/* 224*/,
	RuntimeInvoker_Void_t272_Int32_t253_IntPtr_t/* 225*/,
	RuntimeInvoker_Void_t272_UICharInfoU5BU5DU26_t3960_Int32_t253/* 226*/,
	RuntimeInvoker_Void_t272_UICharInfoU5BU5DU26_t3960_Int32_t253_Int32_t253/* 227*/,
	RuntimeInvoker_Int32_t253_Object_t_UICharInfo_t689_Int32_t253_Int32_t253/* 228*/,
	RuntimeInvoker_Int32_t253_UICharInfo_t689_UICharInfo_t689_Object_t/* 229*/,
	RuntimeInvoker_Void_t272_UILineInfoU5BU5DU26_t3961_Int32_t253/* 230*/,
	RuntimeInvoker_Void_t272_UILineInfoU5BU5DU26_t3961_Int32_t253_Int32_t253/* 231*/,
	RuntimeInvoker_Int32_t253_Object_t_UILineInfo_t687_Int32_t253_Int32_t253/* 232*/,
	RuntimeInvoker_Int32_t253_UILineInfo_t687_UILineInfo_t687_Object_t/* 233*/,
	RuntimeInvoker_KeyValuePair_2_t3255_Int32_t253/* 234*/,
	RuntimeInvoker_Void_t272_KeyValuePair_2_t3255/* 235*/,
	RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3255/* 236*/,
	RuntimeInvoker_Int32_t253_KeyValuePair_2_t3255/* 237*/,
	RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3255/* 238*/,
	RuntimeInvoker_Int64_t1071_Int32_t253/* 239*/,
	RuntimeInvoker_Void_t272_Int64_t1071/* 240*/,
	RuntimeInvoker_Boolean_t273_Int64_t1071/* 241*/,
	RuntimeInvoker_Int32_t253_Int64_t1071/* 242*/,
	RuntimeInvoker_Void_t272_Int32_t253_Int64_t1071/* 243*/,
	RuntimeInvoker_KeyValuePair_2_t3293_Int32_t253/* 244*/,
	RuntimeInvoker_Void_t272_KeyValuePair_2_t3293/* 245*/,
	RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3293/* 246*/,
	RuntimeInvoker_Int32_t253_KeyValuePair_2_t3293/* 247*/,
	RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3293/* 248*/,
	RuntimeInvoker_UInt64_t1074_Int32_t253/* 249*/,
	RuntimeInvoker_KeyValuePair_2_t3314_Int32_t253/* 250*/,
	RuntimeInvoker_Void_t272_KeyValuePair_2_t3314/* 251*/,
	RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3314/* 252*/,
	RuntimeInvoker_Int32_t253_KeyValuePair_2_t3314/* 253*/,
	RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3314/* 254*/,
	RuntimeInvoker_ParameterModifier_t1914_Int32_t253/* 255*/,
	RuntimeInvoker_Void_t272_ParameterModifier_t1914/* 256*/,
	RuntimeInvoker_Boolean_t273_ParameterModifier_t1914/* 257*/,
	RuntimeInvoker_Int32_t253_ParameterModifier_t1914/* 258*/,
	RuntimeInvoker_Void_t272_Int32_t253_ParameterModifier_t1914/* 259*/,
	RuntimeInvoker_HitInfo_t983_Int32_t253/* 260*/,
	RuntimeInvoker_Void_t272_HitInfo_t983/* 261*/,
	RuntimeInvoker_Boolean_t273_HitInfo_t983/* 262*/,
	RuntimeInvoker_Int32_t253_HitInfo_t983/* 263*/,
	RuntimeInvoker_Void_t272_Int32_t253_HitInfo_t983/* 264*/,
	RuntimeInvoker_KeyValuePair_2_t3407_Int32_t253/* 265*/,
	RuntimeInvoker_Void_t272_KeyValuePair_2_t3407/* 266*/,
	RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3407/* 267*/,
	RuntimeInvoker_Int32_t253_KeyValuePair_2_t3407/* 268*/,
	RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3407/* 269*/,
	RuntimeInvoker_X509ChainStatus_t1331_Int32_t253/* 270*/,
	RuntimeInvoker_Void_t272_X509ChainStatus_t1331/* 271*/,
	RuntimeInvoker_Boolean_t273_X509ChainStatus_t1331/* 272*/,
	RuntimeInvoker_Int32_t253_X509ChainStatus_t1331/* 273*/,
	RuntimeInvoker_Void_t272_Int32_t253_X509ChainStatus_t1331/* 274*/,
	RuntimeInvoker_KeyValuePair_2_t3427_Int32_t253/* 275*/,
	RuntimeInvoker_Void_t272_KeyValuePair_2_t3427/* 276*/,
	RuntimeInvoker_Boolean_t273_KeyValuePair_2_t3427/* 277*/,
	RuntimeInvoker_Int32_t253_KeyValuePair_2_t3427/* 278*/,
	RuntimeInvoker_Void_t272_Int32_t253_KeyValuePair_2_t3427/* 279*/,
	RuntimeInvoker_Int32_t253_Object_t_Int32_t253_Int32_t253_Int32_t253_Object_t/* 280*/,
	RuntimeInvoker_Mark_t1382_Int32_t253/* 281*/,
	RuntimeInvoker_Void_t272_Mark_t1382/* 282*/,
	RuntimeInvoker_Boolean_t273_Mark_t1382/* 283*/,
	RuntimeInvoker_Int32_t253_Mark_t1382/* 284*/,
	RuntimeInvoker_Void_t272_Int32_t253_Mark_t1382/* 285*/,
	RuntimeInvoker_UriScheme_t1418_Int32_t253/* 286*/,
	RuntimeInvoker_Void_t272_UriScheme_t1418/* 287*/,
	RuntimeInvoker_Boolean_t273_UriScheme_t1418/* 288*/,
	RuntimeInvoker_Int32_t253_UriScheme_t1418/* 289*/,
	RuntimeInvoker_Void_t272_Int32_t253_UriScheme_t1418/* 290*/,
	RuntimeInvoker_UInt32_t1063_Int32_t253/* 291*/,
	RuntimeInvoker_Int16_t752_Int32_t253/* 292*/,
	RuntimeInvoker_SByte_t274_Int32_t253/* 293*/,
	RuntimeInvoker_TableRange_t1690_Int32_t253/* 294*/,
	RuntimeInvoker_Void_t272_TableRange_t1690/* 295*/,
	RuntimeInvoker_Boolean_t273_TableRange_t1690/* 296*/,
	RuntimeInvoker_Int32_t253_TableRange_t1690/* 297*/,
	RuntimeInvoker_Void_t272_Int32_t253_TableRange_t1690/* 298*/,
	RuntimeInvoker_Slot_t1767_Int32_t253/* 299*/,
	RuntimeInvoker_Void_t272_Slot_t1767/* 300*/,
	RuntimeInvoker_Boolean_t273_Slot_t1767/* 301*/,
	RuntimeInvoker_Int32_t253_Slot_t1767/* 302*/,
	RuntimeInvoker_Void_t272_Int32_t253_Slot_t1767/* 303*/,
	RuntimeInvoker_Slot_t1776_Int32_t253/* 304*/,
	RuntimeInvoker_Void_t272_Slot_t1776/* 305*/,
	RuntimeInvoker_Boolean_t273_Slot_t1776/* 306*/,
	RuntimeInvoker_Int32_t253_Slot_t1776/* 307*/,
	RuntimeInvoker_Void_t272_Int32_t253_Slot_t1776/* 308*/,
	RuntimeInvoker_MonoResource_t1838_Int32_t253/* 309*/,
	RuntimeInvoker_Void_t272_MonoResource_t1838/* 310*/,
	RuntimeInvoker_Boolean_t273_MonoResource_t1838/* 311*/,
	RuntimeInvoker_Int32_t253_MonoResource_t1838/* 312*/,
	RuntimeInvoker_Void_t272_Int32_t253_MonoResource_t1838/* 313*/,
	RuntimeInvoker_ILTokenInfo_t1856_Int32_t253/* 314*/,
	RuntimeInvoker_Void_t272_ILTokenInfo_t1856/* 315*/,
	RuntimeInvoker_Boolean_t273_ILTokenInfo_t1856/* 316*/,
	RuntimeInvoker_Int32_t253_ILTokenInfo_t1856/* 317*/,
	RuntimeInvoker_Void_t272_Int32_t253_ILTokenInfo_t1856/* 318*/,
	RuntimeInvoker_LabelData_t1858_Int32_t253/* 319*/,
	RuntimeInvoker_Void_t272_LabelData_t1858/* 320*/,
	RuntimeInvoker_Boolean_t273_LabelData_t1858/* 321*/,
	RuntimeInvoker_Int32_t253_LabelData_t1858/* 322*/,
	RuntimeInvoker_Void_t272_Int32_t253_LabelData_t1858/* 323*/,
	RuntimeInvoker_LabelFixup_t1857_Int32_t253/* 324*/,
	RuntimeInvoker_Void_t272_LabelFixup_t1857/* 325*/,
	RuntimeInvoker_Boolean_t273_LabelFixup_t1857/* 326*/,
	RuntimeInvoker_Int32_t253_LabelFixup_t1857/* 327*/,
	RuntimeInvoker_Void_t272_Int32_t253_LabelFixup_t1857/* 328*/,
	RuntimeInvoker_DateTime_t406_Int32_t253/* 329*/,
	RuntimeInvoker_Void_t272_DateTime_t406/* 330*/,
	RuntimeInvoker_Boolean_t273_DateTime_t406/* 331*/,
	RuntimeInvoker_Int32_t253_DateTime_t406/* 332*/,
	RuntimeInvoker_Void_t272_Int32_t253_DateTime_t406/* 333*/,
	RuntimeInvoker_Decimal_t1073_Int32_t253/* 334*/,
	RuntimeInvoker_Void_t272_Decimal_t1073/* 335*/,
	RuntimeInvoker_Boolean_t273_Decimal_t1073/* 336*/,
	RuntimeInvoker_Int32_t253_Decimal_t1073/* 337*/,
	RuntimeInvoker_Void_t272_Int32_t253_Decimal_t1073/* 338*/,
	RuntimeInvoker_TimeSpan_t1337_Int32_t253/* 339*/,
	RuntimeInvoker_Boolean_t273_TimeSpan_t1337/* 340*/,
	RuntimeInvoker_Int32_t253_TimeSpan_t1337/* 341*/,
	RuntimeInvoker_Void_t272_Int32_t253_TimeSpan_t1337/* 342*/,
	RuntimeInvoker_RaycastHit_t24/* 343*/,
	RuntimeInvoker_Single_t254/* 344*/,
	RuntimeInvoker_Link_t1757/* 345*/,
	RuntimeInvoker_DictionaryEntry_t1430_Object_t/* 346*/,
	RuntimeInvoker_KeyValuePair_2_t2815_Object_t/* 347*/,
	RuntimeInvoker_Touch_t235/* 348*/,
	RuntimeInvoker_Double_t1070/* 349*/,
	RuntimeInvoker_UInt16_t684/* 350*/,
	RuntimeInvoker_Vector2_t6/* 351*/,
	RuntimeInvoker_Keyframe_t240/* 352*/,
	RuntimeInvoker_Color_t65/* 353*/,
	RuntimeInvoker_Vector3_t4/* 354*/,
	RuntimeInvoker_ContactPoint_t246/* 355*/,
	RuntimeInvoker_ParticleCollisionEvent_t160/* 356*/,
	RuntimeInvoker_Vector2_t6_Object_t/* 357*/,
	RuntimeInvoker_Enumerator_t2913/* 358*/,
	RuntimeInvoker_Boolean_t273_Vector2_t6_Vector2_t6/* 359*/,
	RuntimeInvoker_Object_t_Vector2_t6_Object_t_Object_t/* 360*/,
	RuntimeInvoker_Int32_t253_Vector2_t6_Vector2_t6/* 361*/,
	RuntimeInvoker_Object_t_Vector2_t6_Vector2_t6_Object_t_Object_t/* 362*/,
	RuntimeInvoker_RaycastResult_t486_Object_t/* 363*/,
	RuntimeInvoker_Enumerator_t2935/* 364*/,
	RuntimeInvoker_RaycastResult_t486/* 365*/,
	RuntimeInvoker_Boolean_t273_RaycastResult_t486_RaycastResult_t486/* 366*/,
	RuntimeInvoker_Object_t_RaycastResult_t486_Object_t_Object_t/* 367*/,
	RuntimeInvoker_Int32_t253_RaycastResult_t486_RaycastResult_t486/* 368*/,
	RuntimeInvoker_Object_t_RaycastResult_t486_RaycastResult_t486_Object_t_Object_t/* 369*/,
	RuntimeInvoker_Quaternion_t19/* 370*/,
	RuntimeInvoker_KeyValuePair_2_t2953_Object_t_Int32_t253/* 371*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t253/* 372*/,
	RuntimeInvoker_Int32_t253_Object_t_Int32_t253/* 373*/,
	RuntimeInvoker_Boolean_t273_Object_t_Int32U26_t753/* 374*/,
	RuntimeInvoker_Enumerator_t2957/* 375*/,
	RuntimeInvoker_DictionaryEntry_t1430_Object_t_Int32_t253/* 376*/,
	RuntimeInvoker_KeyValuePair_2_t2953/* 377*/,
	RuntimeInvoker_Enumerator_t2956/* 378*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Object_t/* 379*/,
	RuntimeInvoker_Enumerator_t2960/* 380*/,
	RuntimeInvoker_KeyValuePair_2_t2953_Object_t/* 381*/,
	RuntimeInvoker_Boolean_t273_Int32_t253_Int32_t253/* 382*/,
	RuntimeInvoker_KeyValuePair_2_t2984_Int32_t253_SByte_t274/* 383*/,
	RuntimeInvoker_Int32_t253_Int32_t253_SByte_t274/* 384*/,
	RuntimeInvoker_Byte_t680_Int32_t253_SByte_t274/* 385*/,
	RuntimeInvoker_Boolean_t273_Int32_t253_ByteU26_t1656/* 386*/,
	RuntimeInvoker_Byte_t680_Object_t/* 387*/,
	RuntimeInvoker_Enumerator_t2989/* 388*/,
	RuntimeInvoker_DictionaryEntry_t1430_Int32_t253_SByte_t274/* 389*/,
	RuntimeInvoker_KeyValuePair_2_t2984/* 390*/,
	RuntimeInvoker_Byte_t680/* 391*/,
	RuntimeInvoker_Enumerator_t2988/* 392*/,
	RuntimeInvoker_Object_t_Int32_t253_SByte_t274_Object_t_Object_t/* 393*/,
	RuntimeInvoker_Enumerator_t2992/* 394*/,
	RuntimeInvoker_KeyValuePair_2_t2984_Object_t/* 395*/,
	RuntimeInvoker_Boolean_t273_SByte_t274_SByte_t274/* 396*/,
	RuntimeInvoker_AnimatorClipInfo_t888/* 397*/,
	RuntimeInvoker_KeyValuePair_2_t3059_Int32_t253_Object_t/* 398*/,
	RuntimeInvoker_Int32_t253_Int32_t253_Object_t/* 399*/,
	RuntimeInvoker_Object_t_Int32_t253_Object_t/* 400*/,
	RuntimeInvoker_Boolean_t273_Int32_t253_ObjectU26_t1197/* 401*/,
	RuntimeInvoker_DictionaryEntry_t1430_Int32_t253_Object_t/* 402*/,
	RuntimeInvoker_Enumerator_t3062/* 403*/,
	RuntimeInvoker_Object_t_Int32_t253_Object_t_Object_t_Object_t/* 404*/,
	RuntimeInvoker_KeyValuePair_2_t3059_Object_t/* 405*/,
	RuntimeInvoker_RaycastHit2D_t665/* 406*/,
	RuntimeInvoker_Int32_t253_RaycastHit_t24_RaycastHit_t24/* 407*/,
	RuntimeInvoker_Object_t_RaycastHit_t24_RaycastHit_t24_Object_t_Object_t/* 408*/,
	RuntimeInvoker_Object_t_Color_t65_Object_t_Object_t/* 409*/,
	RuntimeInvoker_Void_t272_UIVertex_t556/* 410*/,
	RuntimeInvoker_Boolean_t273_UIVertex_t556/* 411*/,
	RuntimeInvoker_UIVertex_t556_Object_t/* 412*/,
	RuntimeInvoker_Enumerator_t3101/* 413*/,
	RuntimeInvoker_Int32_t253_UIVertex_t556/* 414*/,
	RuntimeInvoker_Void_t272_Int32_t253_UIVertex_t556/* 415*/,
	RuntimeInvoker_UIVertex_t556_Int32_t253/* 416*/,
	RuntimeInvoker_UIVertex_t556/* 417*/,
	RuntimeInvoker_Boolean_t273_UIVertex_t556_UIVertex_t556/* 418*/,
	RuntimeInvoker_Object_t_UIVertex_t556_Object_t_Object_t/* 419*/,
	RuntimeInvoker_Int32_t253_UIVertex_t556_UIVertex_t556/* 420*/,
	RuntimeInvoker_Object_t_UIVertex_t556_UIVertex_t556_Object_t_Object_t/* 421*/,
	RuntimeInvoker_Object_t_ColorTween_t506/* 422*/,
	RuntimeInvoker_UILineInfo_t687/* 423*/,
	RuntimeInvoker_UICharInfo_t689/* 424*/,
	RuntimeInvoker_Object_t_Single_t254_Object_t_Object_t/* 425*/,
	RuntimeInvoker_Object_t_SByte_t274_Object_t_Object_t/* 426*/,
	RuntimeInvoker_Single_t254_Object_t/* 427*/,
	RuntimeInvoker_GcAchievementData_t958/* 428*/,
	RuntimeInvoker_GcScoreData_t959/* 429*/,
	RuntimeInvoker_IntPtr_t/* 430*/,
	RuntimeInvoker_UICharInfo_t689_Object_t/* 431*/,
	RuntimeInvoker_Enumerator_t3234/* 432*/,
	RuntimeInvoker_Boolean_t273_UICharInfo_t689_UICharInfo_t689/* 433*/,
	RuntimeInvoker_Object_t_UICharInfo_t689_Object_t_Object_t/* 434*/,
	RuntimeInvoker_Int32_t253_UICharInfo_t689_UICharInfo_t689/* 435*/,
	RuntimeInvoker_Object_t_UICharInfo_t689_UICharInfo_t689_Object_t_Object_t/* 436*/,
	RuntimeInvoker_UILineInfo_t687_Object_t/* 437*/,
	RuntimeInvoker_Enumerator_t3243/* 438*/,
	RuntimeInvoker_Boolean_t273_UILineInfo_t687_UILineInfo_t687/* 439*/,
	RuntimeInvoker_Object_t_UILineInfo_t687_Object_t_Object_t/* 440*/,
	RuntimeInvoker_Int32_t253_UILineInfo_t687_UILineInfo_t687/* 441*/,
	RuntimeInvoker_Object_t_UILineInfo_t687_UILineInfo_t687_Object_t_Object_t/* 442*/,
	RuntimeInvoker_Int64_t1071_Object_t/* 443*/,
	RuntimeInvoker_Void_t272_Object_t_Int64_t1071/* 444*/,
	RuntimeInvoker_KeyValuePair_2_t3255_Object_t_Int64_t1071/* 445*/,
	RuntimeInvoker_Object_t_Object_t_Int64_t1071/* 446*/,
	RuntimeInvoker_Int64_t1071_Object_t_Int64_t1071/* 447*/,
	RuntimeInvoker_Boolean_t273_Object_t_Int64U26_t2404/* 448*/,
	RuntimeInvoker_Enumerator_t3260/* 449*/,
	RuntimeInvoker_DictionaryEntry_t1430_Object_t_Int64_t1071/* 450*/,
	RuntimeInvoker_KeyValuePair_2_t3255/* 451*/,
	RuntimeInvoker_Int64_t1071/* 452*/,
	RuntimeInvoker_Enumerator_t3259/* 453*/,
	RuntimeInvoker_Object_t_Object_t_Int64_t1071_Object_t_Object_t/* 454*/,
	RuntimeInvoker_Enumerator_t3263/* 455*/,
	RuntimeInvoker_KeyValuePair_2_t3255_Object_t/* 456*/,
	RuntimeInvoker_Boolean_t273_Int64_t1071_Int64_t1071/* 457*/,
	RuntimeInvoker_Object_t_Int64_t1071/* 458*/,
	RuntimeInvoker_Void_t272_Int64_t1071_Object_t/* 459*/,
	RuntimeInvoker_KeyValuePair_2_t3293_Int64_t1071_Object_t/* 460*/,
	RuntimeInvoker_UInt64_t1074_Int64_t1071_Object_t/* 461*/,
	RuntimeInvoker_Object_t_Int64_t1071_Object_t/* 462*/,
	RuntimeInvoker_Boolean_t273_Int64_t1071_ObjectU26_t1197/* 463*/,
	RuntimeInvoker_UInt64_t1074_Object_t/* 464*/,
	RuntimeInvoker_Enumerator_t3298/* 465*/,
	RuntimeInvoker_DictionaryEntry_t1430_Int64_t1071_Object_t/* 466*/,
	RuntimeInvoker_KeyValuePair_2_t3293/* 467*/,
	RuntimeInvoker_UInt64_t1074/* 468*/,
	RuntimeInvoker_Enumerator_t3297/* 469*/,
	RuntimeInvoker_Object_t_Int64_t1071_Object_t_Object_t_Object_t/* 470*/,
	RuntimeInvoker_Enumerator_t3301/* 471*/,
	RuntimeInvoker_KeyValuePair_2_t3293_Object_t/* 472*/,
	RuntimeInvoker_KeyValuePair_2_t3314/* 473*/,
	RuntimeInvoker_Void_t272_Object_t_KeyValuePair_2_t2815/* 474*/,
	RuntimeInvoker_KeyValuePair_2_t3314_Object_t_KeyValuePair_2_t2815/* 475*/,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2815/* 476*/,
	RuntimeInvoker_KeyValuePair_2_t2815_Object_t_KeyValuePair_2_t2815/* 477*/,
	RuntimeInvoker_Boolean_t273_Object_t_KeyValuePair_2U26_t3962/* 478*/,
	RuntimeInvoker_Enumerator_t3343/* 479*/,
	RuntimeInvoker_DictionaryEntry_t1430_Object_t_KeyValuePair_2_t2815/* 480*/,
	RuntimeInvoker_Enumerator_t3342/* 481*/,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2815_Object_t_Object_t/* 482*/,
	RuntimeInvoker_Enumerator_t3346/* 483*/,
	RuntimeInvoker_KeyValuePair_2_t3314_Object_t/* 484*/,
	RuntimeInvoker_Boolean_t273_KeyValuePair_2_t2815_KeyValuePair_2_t2815/* 485*/,
	RuntimeInvoker_ParameterModifier_t1914/* 486*/,
	RuntimeInvoker_HitInfo_t983/* 487*/,
	RuntimeInvoker_Object_t_Int32_t253_Object_t_Object_t/* 488*/,
	RuntimeInvoker_Void_t272_Object_t_SByte_t274/* 489*/,
	RuntimeInvoker_KeyValuePair_2_t3407_Object_t_SByte_t274/* 490*/,
	RuntimeInvoker_Object_t_Object_t_SByte_t274/* 491*/,
	RuntimeInvoker_Byte_t680_Object_t_SByte_t274/* 492*/,
	RuntimeInvoker_Boolean_t273_Object_t_ByteU26_t1656/* 493*/,
	RuntimeInvoker_Enumerator_t3411/* 494*/,
	RuntimeInvoker_DictionaryEntry_t1430_Object_t_SByte_t274/* 495*/,
	RuntimeInvoker_KeyValuePair_2_t3407/* 496*/,
	RuntimeInvoker_Enumerator_t3410/* 497*/,
	RuntimeInvoker_Object_t_Object_t_SByte_t274_Object_t_Object_t/* 498*/,
	RuntimeInvoker_Enumerator_t3414/* 499*/,
	RuntimeInvoker_KeyValuePair_2_t3407_Object_t/* 500*/,
	RuntimeInvoker_X509ChainStatus_t1331/* 501*/,
	RuntimeInvoker_KeyValuePair_2_t3427_Int32_t253_Int32_t253/* 502*/,
	RuntimeInvoker_Int32_t253_Int32_t253_Int32_t253/* 503*/,
	RuntimeInvoker_Boolean_t273_Int32_t253_Int32U26_t753/* 504*/,
	RuntimeInvoker_Enumerator_t3431/* 505*/,
	RuntimeInvoker_DictionaryEntry_t1430_Int32_t253_Int32_t253/* 506*/,
	RuntimeInvoker_KeyValuePair_2_t3427/* 507*/,
	RuntimeInvoker_Enumerator_t3430/* 508*/,
	RuntimeInvoker_Object_t_Int32_t253_Int32_t253_Object_t_Object_t/* 509*/,
	RuntimeInvoker_Enumerator_t3434/* 510*/,
	RuntimeInvoker_KeyValuePair_2_t3427_Object_t/* 511*/,
	RuntimeInvoker_Mark_t1382/* 512*/,
	RuntimeInvoker_UriScheme_t1418/* 513*/,
	RuntimeInvoker_UInt32_t1063/* 514*/,
	RuntimeInvoker_Int16_t752/* 515*/,
	RuntimeInvoker_SByte_t274/* 516*/,
	RuntimeInvoker_TableRange_t1690/* 517*/,
	RuntimeInvoker_Slot_t1767/* 518*/,
	RuntimeInvoker_Slot_t1776/* 519*/,
	RuntimeInvoker_MonoResource_t1838/* 520*/,
	RuntimeInvoker_ILTokenInfo_t1856/* 521*/,
	RuntimeInvoker_LabelData_t1858/* 522*/,
	RuntimeInvoker_LabelFixup_t1857/* 523*/,
	RuntimeInvoker_DateTime_t406/* 524*/,
	RuntimeInvoker_Decimal_t1073/* 525*/,
	RuntimeInvoker_Int32_t253_DateTime_t406_DateTime_t406/* 526*/,
	RuntimeInvoker_Boolean_t273_DateTime_t406_DateTime_t406/* 527*/,
	RuntimeInvoker_Int32_t253_DateTimeOffset_t1086_DateTimeOffset_t1086/* 528*/,
	RuntimeInvoker_Int32_t253_DateTimeOffset_t1086/* 529*/,
	RuntimeInvoker_Boolean_t273_DateTimeOffset_t1086_DateTimeOffset_t1086/* 530*/,
	RuntimeInvoker_Boolean_t273_Nullable_1_t2292/* 531*/,
	RuntimeInvoker_Int32_t253_Guid_t1087_Guid_t1087/* 532*/,
	RuntimeInvoker_Int32_t253_Guid_t1087/* 533*/,
	RuntimeInvoker_Boolean_t273_Guid_t1087_Guid_t1087/* 534*/,
	RuntimeInvoker_Int32_t253_TimeSpan_t1337_TimeSpan_t1337/* 535*/,
	RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337/* 536*/,
};
const Il2CppCodeRegistration g_CodeRegistration = 
{
	3424,
	g_Il2CppMethodPointers,
	537,
	g_Il2CppInvokerPointers,
};
extern const Il2CppMetadataRegistration g_MetadataRegistration;
static void s_Il2CppCodegenRegistration()
{
	il2cpp_codegen_register (&g_CodeRegistration, &g_MetadataRegistration);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_Il2CppCodegenRegistrationVariable (&s_Il2CppCodegenRegistration, NULL);
