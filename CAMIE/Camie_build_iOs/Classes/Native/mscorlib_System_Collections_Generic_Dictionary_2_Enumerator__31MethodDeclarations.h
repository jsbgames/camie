﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
struct Enumerator_t3431;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1366;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_35.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22071_gshared (Enumerator_t3431 * __this, Dictionary_2_t1366 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m22071(__this, ___dictionary, method) (( void (*) (Enumerator_t3431 *, Dictionary_2_t1366 *, const MethodInfo*))Enumerator__ctor_m22071_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22072_gshared (Enumerator_t3431 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22072(__this, method) (( Object_t * (*) (Enumerator_t3431 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22072_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1430  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22073_gshared (Enumerator_t3431 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22073(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3431 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22073_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22074_gshared (Enumerator_t3431 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22074(__this, method) (( Object_t * (*) (Enumerator_t3431 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22074_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22075_gshared (Enumerator_t3431 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22075(__this, method) (( Object_t * (*) (Enumerator_t3431 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22075_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22076_gshared (Enumerator_t3431 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22076(__this, method) (( bool (*) (Enumerator_t3431 *, const MethodInfo*))Enumerator_MoveNext_m22076_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t3427  Enumerator_get_Current_m22077_gshared (Enumerator_t3431 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22077(__this, method) (( KeyValuePair_2_t3427  (*) (Enumerator_t3431 *, const MethodInfo*))Enumerator_get_Current_m22077_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m22078_gshared (Enumerator_t3431 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m22078(__this, method) (( int32_t (*) (Enumerator_t3431 *, const MethodInfo*))Enumerator_get_CurrentKey_m22078_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m22079_gshared (Enumerator_t3431 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m22079(__this, method) (( int32_t (*) (Enumerator_t3431 *, const MethodInfo*))Enumerator_get_CurrentValue_m22079_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m22080_gshared (Enumerator_t3431 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22080(__this, method) (( void (*) (Enumerator_t3431 *, const MethodInfo*))Enumerator_VerifyState_m22080_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m22081_gshared (Enumerator_t3431 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m22081(__this, method) (( void (*) (Enumerator_t3431 *, const MethodInfo*))Enumerator_VerifyCurrent_m22081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m22082_gshared (Enumerator_t3431 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22082(__this, method) (( void (*) (Enumerator_t3431 *, const MethodInfo*))Enumerator_Dispose_m22082_gshared)(__this, method)
