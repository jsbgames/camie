﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.UILineInfo>
struct Comparison_1_t3251;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Comparison`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m19851_gshared (Comparison_1_t3251 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m19851(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3251 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m19851_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.UILineInfo>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m19852_gshared (Comparison_1_t3251 * __this, UILineInfo_t687  ___x, UILineInfo_t687  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m19852(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3251 *, UILineInfo_t687 , UILineInfo_t687 , const MethodInfo*))Comparison_1_Invoke_m19852_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.UILineInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m19853_gshared (Comparison_1_t3251 * __this, UILineInfo_t687  ___x, UILineInfo_t687  ___y, AsyncCallback_t547 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m19853(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3251 *, UILineInfo_t687 , UILineInfo_t687 , AsyncCallback_t547 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m19853_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m19854_gshared (Comparison_1_t3251 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m19854(__this, ___result, method) (( int32_t (*) (Comparison_1_t3251 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m19854_gshared)(__this, ___result, method)
