﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssemblyIsEditorAssembly
struct AssemblyIsEditorAssembly_t955;

// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern "C" void AssemblyIsEditorAssembly__ctor_m4883 (AssemblyIsEditorAssembly_t955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
