﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture2D
struct Texture2D_t63;
// UnityEngine.GUISkin
struct GUISkin_t287;
// System.Object
#include "mscorlib_System_Object.h"
// Images
struct  Images_t288  : public Object_t
{
	// UnityEngine.Texture2D Images::clearImage
	Texture2D_t63 * ___clearImage_0;
	// UnityEngine.Texture2D Images::collapseImage
	Texture2D_t63 * ___collapseImage_1;
	// UnityEngine.Texture2D Images::clearOnNewSceneImage
	Texture2D_t63 * ___clearOnNewSceneImage_2;
	// UnityEngine.Texture2D Images::showTimeImage
	Texture2D_t63 * ___showTimeImage_3;
	// UnityEngine.Texture2D Images::showSceneImage
	Texture2D_t63 * ___showSceneImage_4;
	// UnityEngine.Texture2D Images::userImage
	Texture2D_t63 * ___userImage_5;
	// UnityEngine.Texture2D Images::showMemoryImage
	Texture2D_t63 * ___showMemoryImage_6;
	// UnityEngine.Texture2D Images::softwareImage
	Texture2D_t63 * ___softwareImage_7;
	// UnityEngine.Texture2D Images::dateImage
	Texture2D_t63 * ___dateImage_8;
	// UnityEngine.Texture2D Images::showFpsImage
	Texture2D_t63 * ___showFpsImage_9;
	// UnityEngine.Texture2D Images::showGraphImage
	Texture2D_t63 * ___showGraphImage_10;
	// UnityEngine.Texture2D Images::graphImage
	Texture2D_t63 * ___graphImage_11;
	// UnityEngine.Texture2D Images::infoImage
	Texture2D_t63 * ___infoImage_12;
	// UnityEngine.Texture2D Images::searchImage
	Texture2D_t63 * ___searchImage_13;
	// UnityEngine.Texture2D Images::closeImage
	Texture2D_t63 * ___closeImage_14;
	// UnityEngine.Texture2D Images::buildFromImage
	Texture2D_t63 * ___buildFromImage_15;
	// UnityEngine.Texture2D Images::systemInfoImage
	Texture2D_t63 * ___systemInfoImage_16;
	// UnityEngine.Texture2D Images::graphicsInfoImage
	Texture2D_t63 * ___graphicsInfoImage_17;
	// UnityEngine.Texture2D Images::backImage
	Texture2D_t63 * ___backImage_18;
	// UnityEngine.Texture2D Images::cameraImage
	Texture2D_t63 * ___cameraImage_19;
	// UnityEngine.Texture2D Images::logImage
	Texture2D_t63 * ___logImage_20;
	// UnityEngine.Texture2D Images::warningImage
	Texture2D_t63 * ___warningImage_21;
	// UnityEngine.Texture2D Images::errorImage
	Texture2D_t63 * ___errorImage_22;
	// UnityEngine.Texture2D Images::barImage
	Texture2D_t63 * ___barImage_23;
	// UnityEngine.Texture2D Images::button_activeImage
	Texture2D_t63 * ___button_activeImage_24;
	// UnityEngine.Texture2D Images::even_logImage
	Texture2D_t63 * ___even_logImage_25;
	// UnityEngine.Texture2D Images::odd_logImage
	Texture2D_t63 * ___odd_logImage_26;
	// UnityEngine.Texture2D Images::selectedImage
	Texture2D_t63 * ___selectedImage_27;
	// UnityEngine.GUISkin Images::reporterScrollerSkin
	GUISkin_t287 * ___reporterScrollerSkin_28;
};
