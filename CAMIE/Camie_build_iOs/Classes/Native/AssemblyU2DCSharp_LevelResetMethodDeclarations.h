﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LevelReset
struct LevelReset_t318;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t215;

// System.Void LevelReset::.ctor()
extern "C" void LevelReset__ctor_m1168 (LevelReset_t318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelReset::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C" void LevelReset_OnPointerClick_m1169 (LevelReset_t318 * __this, PointerEventData_t215 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelReset::Update()
extern "C" void LevelReset_Update_m1170 (LevelReset_t318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
