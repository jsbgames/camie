﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Web
struct SCR_Web_t398;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void SCR_Web::.ctor()
extern "C" void SCR_Web__ctor_m1566 (SCR_Web_t398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Web::Start()
extern "C" void SCR_Web_Start_m1567 (SCR_Web_t398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Web::TriggerEnterEffect()
extern "C" void SCR_Web_TriggerEnterEffect_m1568 (SCR_Web_t398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Web::TriggerExitEffet()
extern "C" void SCR_Web_TriggerExitEffet_m1569 (SCR_Web_t398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_Web::DecreaseSize()
extern "C" Object_t * SCR_Web_DecreaseSize_m1570 (SCR_Web_t398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
