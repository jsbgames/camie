﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_t618;

// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
extern "C" void VerticalLayoutGroup__ctor_m3009 (VerticalLayoutGroup_t618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
extern "C" void VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3010 (VerticalLayoutGroup_t618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
extern "C" void VerticalLayoutGroup_CalculateLayoutInputVertical_m3011 (VerticalLayoutGroup_t618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
extern "C" void VerticalLayoutGroup_SetLayoutHorizontal_m3012 (VerticalLayoutGroup_t618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
extern "C" void VerticalLayoutGroup_SetLayoutVertical_m3013 (VerticalLayoutGroup_t618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
