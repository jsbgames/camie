﻿#pragma once
#include <stdint.h>
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t150;
// UnityEngine.ParticleSystem
struct ParticleSystem_t161;
// System.Object
struct Object_t;
// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct ParticleSystemDestroyer_t189;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8
struct  U3CStartU3Ec__Iterator8_t190  : public Object_t
{
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::<systems>__0
	ParticleSystemU5BU5D_t150* ___U3CsystemsU3E__0_0;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::<$s_25>__1
	ParticleSystemU5BU5D_t150* ___U3CU24s_25U3E__1_1;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::<$s_26>__2
	int32_t ___U3CU24s_26U3E__2_2;
	// UnityEngine.ParticleSystem UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::<system>__3
	ParticleSystem_t161 * ___U3CsystemU3E__3_3;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::<stopTime>__4
	float ___U3CstopTimeU3E__4_4;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::<$s_27>__5
	ParticleSystemU5BU5D_t150* ___U3CU24s_27U3E__5_5;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::<$s_28>__6
	int32_t ___U3CU24s_28U3E__6_6;
	// UnityEngine.ParticleSystem UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::<system>__7
	ParticleSystem_t161 * ___U3CsystemU3E__7_7;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::$PC
	int32_t ___U24PC_8;
	// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::$current
	Object_t * ___U24current_9;
	// UnityStandardAssets.Utility.ParticleSystemDestroyer UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator8::<>f__this
	ParticleSystemDestroyer_t189 * ___U3CU3Ef__this_10;
};
