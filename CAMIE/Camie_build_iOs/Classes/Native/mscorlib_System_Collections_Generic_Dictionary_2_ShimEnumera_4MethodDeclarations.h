﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>
struct ShimEnumerator_t3304;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t3292;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m20473_gshared (ShimEnumerator_t3304 * __this, Dictionary_2_t3292 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m20473(__this, ___host, method) (( void (*) (ShimEnumerator_t3304 *, Dictionary_2_t3292 *, const MethodInfo*))ShimEnumerator__ctor_m20473_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m20474_gshared (ShimEnumerator_t3304 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m20474(__this, method) (( bool (*) (ShimEnumerator_t3304 *, const MethodInfo*))ShimEnumerator_MoveNext_m20474_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1430  ShimEnumerator_get_Entry_m20475_gshared (ShimEnumerator_t3304 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m20475(__this, method) (( DictionaryEntry_t1430  (*) (ShimEnumerator_t3304 *, const MethodInfo*))ShimEnumerator_get_Entry_m20475_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m20476_gshared (ShimEnumerator_t3304 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m20476(__this, method) (( Object_t * (*) (ShimEnumerator_t3304 *, const MethodInfo*))ShimEnumerator_get_Key_m20476_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m20477_gshared (ShimEnumerator_t3304 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m20477(__this, method) (( Object_t * (*) (ShimEnumerator_t3304 *, const MethodInfo*))ShimEnumerator_get_Value_m20477_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m20478_gshared (ShimEnumerator_t3304 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m20478(__this, method) (( Object_t * (*) (ShimEnumerator_t3304 *, const MethodInfo*))ShimEnumerator_get_Current_m20478_gshared)(__this, method)
