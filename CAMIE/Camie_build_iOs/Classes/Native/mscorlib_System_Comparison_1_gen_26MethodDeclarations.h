﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.UI.Toggle>
struct Comparison_1_t3169;
// System.Object
struct Object_t;
// UnityEngine.UI.Toggle
struct Toggle_t312;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<UnityEngine.UI.Toggle>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m18655(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3169 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m14080_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.UI.Toggle>::Invoke(T,T)
#define Comparison_1_Invoke_m18656(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3169 *, Toggle_t312 *, Toggle_t312 *, const MethodInfo*))Comparison_1_Invoke_m14081_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.UI.Toggle>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m18657(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3169 *, Toggle_t312 *, Toggle_t312 *, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m14082_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.UI.Toggle>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m18658(__this, ___result, method) (( int32_t (*) (Comparison_1_t3169 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m14083_gshared)(__this, ___result, method)
