﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t1313;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t1311;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1310;
// System.Security.Cryptography.Oid
struct Oid_t1312;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1321;
// System.Byte[]
struct ByteU5BU5D_t850;
// System.Security.Cryptography.DSA
struct DSA_t1431;
// System.Security.Cryptography.RSA
struct RSA_t1432;

// System.Void System.Security.Cryptography.X509Certificates.PublicKey::.ctor(Mono.Security.X509.X509Certificate)
extern "C" void PublicKey__ctor_m5690 (PublicKey_t1313 * __this, X509Certificate_t1321 * ___certificate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedKeyValue()
extern "C" AsnEncodedData_t1311 * PublicKey_get_EncodedKeyValue_m5691 (PublicKey_t1313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedParameters()
extern "C" AsnEncodedData_t1311 * PublicKey_get_EncodedParameters_m5692 (PublicKey_t1313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::get_Key()
extern "C" AsymmetricAlgorithm_t1310 * PublicKey_get_Key_m5693 (PublicKey_t1313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::get_Oid()
extern "C" Oid_t1312 * PublicKey_get_Oid_m5694 (PublicKey_t1313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::GetUnsignedBigInteger(System.Byte[])
extern "C" ByteU5BU5D_t850* PublicKey_GetUnsignedBigInteger_m5695 (Object_t * __this /* static, unused */, ByteU5BU5D_t850* ___integer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeDSA(System.Byte[],System.Byte[])
extern "C" DSA_t1431 * PublicKey_DecodeDSA_m5696 (Object_t * __this /* static, unused */, ByteU5BU5D_t850* ___rawPublicKey, ByteU5BU5D_t850* ___rawParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeRSA(System.Byte[])
extern "C" RSA_t1432 * PublicKey_DecodeRSA_m5697 (Object_t * __this /* static, unused */, ByteU5BU5D_t850* ___rawPublicKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
