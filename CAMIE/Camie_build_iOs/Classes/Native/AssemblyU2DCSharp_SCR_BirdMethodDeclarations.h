﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Bird
struct SCR_Bird_t390;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void SCR_Bird::.ctor()
extern "C" void SCR_Bird__ctor_m1516 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::Start()
extern "C" void SCR_Bird_Start_m1517 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::Update()
extern "C" void SCR_Bird_Update_m1518 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::SetToIdlePosition()
extern "C" void SCR_Bird_SetToIdlePosition_m1519 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::Fly()
extern "C" void SCR_Bird_Fly_m1520 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCR_Bird::Landing()
extern "C" Object_t * SCR_Bird_Landing_m1521 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::AttackStart()
extern "C" void SCR_Bird_AttackStart_m1522 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::Attack()
extern "C" void SCR_Bird_Attack_m1523 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::GetTargetPoint()
extern "C" void SCR_Bird_GetTargetPoint_m1524 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::GoIdle()
extern "C" void SCR_Bird_GoIdle_m1525 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::CanMove()
extern "C" void SCR_Bird_CanMove_m1526 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::BlobShadow()
extern "C" void SCR_Bird_BlobShadow_m1527 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::EndAttack()
extern "C" void SCR_Bird_EndAttack_m1528 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::Pool()
extern "C" void SCR_Bird_Pool_m1529 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Bird::AddAttack()
extern "C" void SCR_Bird_AddAttack_m1530 (SCR_Bird_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
