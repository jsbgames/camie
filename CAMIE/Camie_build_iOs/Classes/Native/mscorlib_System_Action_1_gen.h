﻿#pragma once
#include <stdint.h>
// UnityEngine.Font
struct Font_t517;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.Font>
struct  Action_1_t668  : public MulticastDelegate_t549
{
};
