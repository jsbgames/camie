﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.ImageEffects.ImageEffects
struct ImageEffects_t108;
// UnityEngine.Material
struct Material_t55;
// UnityEngine.RenderTexture
struct RenderTexture_t101;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void UnityStandardAssets.ImageEffects.ImageEffects::.ctor()
extern "C" void ImageEffects__ctor_m308 (ImageEffects_t108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::RenderDistortion(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Single,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" void ImageEffects_RenderDistortion_m309 (Object_t * __this /* static, unused */, Material_t55 * ___material, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, float ___angle, Vector2_t6  ___center, Vector2_t6  ___radius, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ImageEffects_Blit_m310 (Object_t * __this /* static, unused */, RenderTexture_t101 * ___source, RenderTexture_t101 * ___dest, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::BlitWithMaterial(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void ImageEffects_BlitWithMaterial_m311 (Object_t * __this /* static, unused */, Material_t55 * ___material, RenderTexture_t101 * ___source, RenderTexture_t101 * ___dest, const MethodInfo* method) IL2CPP_METHOD_ATTR;
