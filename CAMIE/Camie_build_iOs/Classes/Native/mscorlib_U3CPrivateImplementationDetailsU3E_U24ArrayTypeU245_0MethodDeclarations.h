﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$52
struct U24ArrayTypeU2452_t2273;
struct U24ArrayTypeU2452_t2273_marshaled;

void U24ArrayTypeU2452_t2273_marshal(const U24ArrayTypeU2452_t2273& unmarshaled, U24ArrayTypeU2452_t2273_marshaled& marshaled);
void U24ArrayTypeU2452_t2273_marshal_back(const U24ArrayTypeU2452_t2273_marshaled& marshaled, U24ArrayTypeU2452_t2273& unmarshaled);
void U24ArrayTypeU2452_t2273_marshal_cleanup(U24ArrayTypeU2452_t2273_marshaled& marshaled);
