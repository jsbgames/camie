﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Effects.ExplosionFireAndDebris
struct ExplosionFireAndDebris_t139;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.Transform
struct Transform_t1;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::.ctor()
extern "C" void ExplosionFireAndDebris__ctor_m404 (ExplosionFireAndDebris_t139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionFireAndDebris::Start()
extern "C" Object_t * ExplosionFireAndDebris_Start_m405 (ExplosionFireAndDebris_t139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::AddFire(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void ExplosionFireAndDebris_AddFire_m406 (ExplosionFireAndDebris_t139 * __this, Transform_t1 * ___t, Vector3_t4  ___pos, Vector3_t4  ___normal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
