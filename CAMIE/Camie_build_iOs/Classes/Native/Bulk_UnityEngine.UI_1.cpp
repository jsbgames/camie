﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_CornerMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_AxisMethodDeclarations.h"



// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_ConstraintMethodDeclarations.h"



// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroupMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup.h"
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
#include "mscorlib_System_Collections_Generic_List_1_gen_24.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
#include "mscorlib_System_Collections_Generic_List_1_gen_24MethodDeclarations.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTrackerMethodDeclarations.h"
struct LayoutGroup_t608;
struct LayoutGroup_t608;
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Int32>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Int32>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisInt32_t253_m3428_gshared (LayoutGroup_t608 * __this, int32_t* p0, int32_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisInt32_t253_m3428(__this, p0, p1, method) (( void (*) (LayoutGroup_t608 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisInt32_t253_m3428_gshared)(__this, p0, p1, method)
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Corner>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Corner>(!!0&,!!0)
#define LayoutGroup_SetProperty_TisCorner_t604_m3427(__this, p0, p1, method) (( void (*) (LayoutGroup_t608 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisInt32_t253_m3428_gshared)(__this, p0, p1, method)
struct LayoutGroup_t608;
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Axis>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Axis>(!!0&,!!0)
#define LayoutGroup_SetProperty_TisAxis_t605_m3429(__this, p0, p1, method) (( void (*) (LayoutGroup_t608 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisInt32_t253_m3428_gshared)(__this, p0, p1, method)
struct LayoutGroup_t608;
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.Vector2>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.Vector2>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisVector2_t6_m3430_gshared (LayoutGroup_t608 * __this, Vector2_t6 * p0, Vector2_t6  p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisVector2_t6_m3430(__this, p0, p1, method) (( void (*) (LayoutGroup_t608 *, Vector2_t6 *, Vector2_t6 , const MethodInfo*))LayoutGroup_SetProperty_TisVector2_t6_m3430_gshared)(__this, p0, p1, method)
struct LayoutGroup_t608;
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Constraint>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Constraint>(!!0&,!!0)
#define LayoutGroup_SetProperty_TisConstraint_t606_m3431(__this, p0, p1, method) (( void (*) (LayoutGroup_t608 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisInt32_t253_m3428_gshared)(__this, p0, p1, method)


// System.Void UnityEngine.UI.GridLayoutGroup::.ctor()
extern "C" void GridLayoutGroup__ctor_m2885 (GridLayoutGroup_t607 * __this, const MethodInfo* method)
{
	{
		Vector2_t6  L_0 = {0};
		Vector2__ctor_m630(&L_0, (100.0f), (100.0f), /*hidden argument*/NULL);
		__this->___m_CellSize_12 = L_0;
		Vector2_t6  L_1 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Spacing_13 = L_1;
		__this->___m_ConstraintCount_15 = 2;
		LayoutGroup__ctor_m2941(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::get_startCorner()
extern "C" int32_t GridLayoutGroup_get_startCorner_m2886 (GridLayoutGroup_t607 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_StartCorner_10);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_startCorner(UnityEngine.UI.GridLayoutGroup/Corner)
extern const MethodInfo* LayoutGroup_SetProperty_TisCorner_t604_m3427_MethodInfo_var;
extern "C" void GridLayoutGroup_set_startCorner_m2887 (GridLayoutGroup_t607 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisCorner_t604_m3427_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_StartCorner_10);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisCorner_t604_m3427(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisCorner_t604_m3427_MethodInfo_var);
		return;
	}
}
// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::get_startAxis()
extern "C" int32_t GridLayoutGroup_get_startAxis_m2888 (GridLayoutGroup_t607 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_StartAxis_11);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_startAxis(UnityEngine.UI.GridLayoutGroup/Axis)
extern const MethodInfo* LayoutGroup_SetProperty_TisAxis_t605_m3429_MethodInfo_var;
extern "C" void GridLayoutGroup_set_startAxis_m2889 (GridLayoutGroup_t607 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisAxis_t605_m3429_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483934);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_StartAxis_11);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisAxis_t605_m3429(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisAxis_t605_m3429_MethodInfo_var);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_cellSize()
extern "C" Vector2_t6  GridLayoutGroup_get_cellSize_m2890 (GridLayoutGroup_t607 * __this, const MethodInfo* method)
{
	{
		Vector2_t6  L_0 = (__this->___m_CellSize_12);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_cellSize(UnityEngine.Vector2)
extern const MethodInfo* LayoutGroup_SetProperty_TisVector2_t6_m3430_MethodInfo_var;
extern "C" void GridLayoutGroup_set_cellSize_m2891 (GridLayoutGroup_t607 * __this, Vector2_t6  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisVector2_t6_m3430_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483935);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t6 * L_0 = &(__this->___m_CellSize_12);
		Vector2_t6  L_1 = ___value;
		LayoutGroup_SetProperty_TisVector2_t6_m3430(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisVector2_t6_m3430_MethodInfo_var);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_spacing()
extern "C" Vector2_t6  GridLayoutGroup_get_spacing_m2892 (GridLayoutGroup_t607 * __this, const MethodInfo* method)
{
	{
		Vector2_t6  L_0 = (__this->___m_Spacing_13);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_spacing(UnityEngine.Vector2)
extern const MethodInfo* LayoutGroup_SetProperty_TisVector2_t6_m3430_MethodInfo_var;
extern "C" void GridLayoutGroup_set_spacing_m2893 (GridLayoutGroup_t607 * __this, Vector2_t6  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisVector2_t6_m3430_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483935);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t6 * L_0 = &(__this->___m_Spacing_13);
		Vector2_t6  L_1 = ___value;
		LayoutGroup_SetProperty_TisVector2_t6_m3430(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisVector2_t6_m3430_MethodInfo_var);
		return;
	}
}
// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::get_constraint()
extern "C" int32_t GridLayoutGroup_get_constraint_m2894 (GridLayoutGroup_t607 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Constraint_14);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraint(UnityEngine.UI.GridLayoutGroup/Constraint)
extern const MethodInfo* LayoutGroup_SetProperty_TisConstraint_t606_m3431_MethodInfo_var;
extern "C" void GridLayoutGroup_set_constraint_m2895 (GridLayoutGroup_t607 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisConstraint_t606_m3431_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_Constraint_14);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisConstraint_t606_m3431(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisConstraint_t606_m3431_MethodInfo_var);
		return;
	}
}
// System.Int32 UnityEngine.UI.GridLayoutGroup::get_constraintCount()
extern "C" int32_t GridLayoutGroup_get_constraintCount_m2896 (GridLayoutGroup_t607 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_ConstraintCount_15);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraintCount(System.Int32)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutGroup_SetProperty_TisInt32_t253_m3428_MethodInfo_var;
extern "C" void GridLayoutGroup_set_constraintCount_m2897 (GridLayoutGroup_t607 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		LayoutGroup_SetProperty_TisInt32_t253_m3428_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_ConstraintCount_15);
		int32_t L_1 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m3310(NULL /*static, unused*/, 1, L_1, /*hidden argument*/NULL);
		LayoutGroup_SetProperty_TisInt32_t253_m3428(__this, L_0, L_2, /*hidden argument*/LayoutGroup_SetProperty_TisInt32_t253_m3428_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputHorizontal()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void GridLayoutGroup_CalculateLayoutInputHorizontal_m2898 (GridLayoutGroup_t607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector2_t6  V_2 = {0};
	Vector2_t6  V_3 = {0};
	Vector2_t6  V_4 = {0};
	Vector2_t6  V_5 = {0};
	Vector2_t6  V_6 = {0};
	Vector2_t6  V_7 = {0};
	{
		LayoutGroup_CalculateLayoutInputHorizontal_m2948(__this, /*hidden argument*/NULL);
		V_0 = 0;
		V_1 = 0;
		int32_t L_0 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_1 = (__this->___m_ConstraintCount_15);
		int32_t L_2 = L_1;
		V_1 = L_2;
		V_0 = L_2;
		goto IL_0070;
	}

IL_0024:
	{
		int32_t L_3 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0057;
		}
	}
	{
		List_1_t612 * L_4 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_4);
		int32_t L_6 = (__this->___m_ConstraintCount_15);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_CeilToInt_m867(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)L_5))/(float)(((float)L_6))))-(float)(0.001f))), /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		V_1 = L_8;
		V_0 = L_8;
		goto IL_0070;
	}

IL_0057:
	{
		V_0 = 1;
		List_1_t612 * L_9 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_11 = sqrtf((((float)L_10)));
		int32_t L_12 = Mathf_CeilToInt_m867(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
	}

IL_0070:
	{
		RectOffset_t404 * L_13 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_horizontal_m3432(L_13, /*hidden argument*/NULL);
		Vector2_t6  L_15 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_2 = L_15;
		float L_16 = ((&V_2)->___x_1);
		Vector2_t6  L_17 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_3 = L_17;
		float L_18 = ((&V_3)->___x_1);
		int32_t L_19 = V_0;
		Vector2_t6  L_20 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_4 = L_20;
		float L_21 = ((&V_4)->___x_1);
		RectOffset_t404 * L_22 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = RectOffset_get_horizontal_m3432(L_22, /*hidden argument*/NULL);
		Vector2_t6  L_24 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_5 = L_24;
		float L_25 = ((&V_5)->___x_1);
		Vector2_t6  L_26 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_6 = L_26;
		float L_27 = ((&V_6)->___x_1);
		int32_t L_28 = V_1;
		Vector2_t6  L_29 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_7 = L_29;
		float L_30 = ((&V_7)->___x_1);
		LayoutGroup_SetLayoutInputForAxis_m2963(__this, ((float)((float)((float)((float)(((float)L_14))+(float)((float)((float)((float)((float)L_16+(float)L_18))*(float)(((float)L_19))))))-(float)L_21)), ((float)((float)((float)((float)(((float)L_23))+(float)((float)((float)((float)((float)L_25+(float)L_27))*(float)(((float)L_28))))))-(float)L_30)), (-1.0f), 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputVertical()
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void GridLayoutGroup_CalculateLayoutInputVertical_m2899 (GridLayoutGroup_t607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	Rect_t304  V_4 = {0};
	Vector2_t6  V_5 = {0};
	Vector2_t6  V_6 = {0};
	Vector2_t6  V_7 = {0};
	Vector2_t6  V_8 = {0};
	Vector2_t6  V_9 = {0};
	Vector2_t6  V_10 = {0};
	Vector2_t6  V_11 = {0};
	{
		V_0 = 0;
		int32_t L_0 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0033;
		}
	}
	{
		List_1_t612 * L_1 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_1);
		int32_t L_3 = (__this->___m_ConstraintCount_15);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_CeilToInt_m867(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)L_2))/(float)(((float)L_3))))-(float)(0.001f))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_00ce;
	}

IL_0033:
	{
		int32_t L_5 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_6 = (__this->___m_ConstraintCount_15);
		V_0 = L_6;
		goto IL_00ce;
	}

IL_004b:
	{
		RectTransform_t364 * L_7 = LayoutGroup_get_rectTransform_m2946(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Rect_t304  L_8 = RectTransform_get_rect_m3184(L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		Vector2_t6  L_9 = Rect_get_size_m3243((&V_4), /*hidden argument*/NULL);
		V_5 = L_9;
		float L_10 = ((&V_5)->___x_1);
		V_1 = L_10;
		float L_11 = V_1;
		RectOffset_t404 * L_12 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_horizontal_m3432(L_12, /*hidden argument*/NULL);
		Vector2_t6  L_14 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_6 = L_14;
		float L_15 = ((&V_6)->___x_1);
		Vector2_t6  L_16 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_7 = L_16;
		float L_17 = ((&V_7)->___x_1);
		Vector2_t6  L_18 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_8 = L_18;
		float L_19 = ((&V_8)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_20 = Mathf_FloorToInt_m815(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)((float)((float)L_11-(float)(((float)L_13))))+(float)L_15))+(float)(0.001f)))/(float)((float)((float)L_17+(float)L_19)))), /*hidden argument*/NULL);
		int32_t L_21 = Mathf_Max_m3310(NULL /*static, unused*/, 1, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		List_1_t612 * L_22 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_22);
		int32_t L_24 = V_2;
		int32_t L_25 = Mathf_CeilToInt_m867(NULL /*static, unused*/, ((float)((float)(((float)L_23))/(float)(((float)L_24)))), /*hidden argument*/NULL);
		V_0 = L_25;
	}

IL_00ce:
	{
		RectOffset_t404 * L_26 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = RectOffset_get_vertical_m3433(L_26, /*hidden argument*/NULL);
		Vector2_t6  L_28 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_9 = L_28;
		float L_29 = ((&V_9)->___y_2);
		Vector2_t6  L_30 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_10 = L_30;
		float L_31 = ((&V_10)->___y_2);
		int32_t L_32 = V_0;
		Vector2_t6  L_33 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_11 = L_33;
		float L_34 = ((&V_11)->___y_2);
		V_3 = ((float)((float)((float)((float)(((float)L_27))+(float)((float)((float)((float)((float)L_29+(float)L_31))*(float)(((float)L_32))))))-(float)L_34));
		float L_35 = V_3;
		float L_36 = V_3;
		LayoutGroup_SetLayoutInputForAxis_m2963(__this, L_35, L_36, (-1.0f), 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutHorizontal()
extern "C" void GridLayoutGroup_SetLayoutHorizontal_m2900 (GridLayoutGroup_t607 * __this, const MethodInfo* method)
{
	{
		GridLayoutGroup_SetCellsAlongAxis_m2902(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutVertical()
extern "C" void GridLayoutGroup_SetLayoutVertical_m2901 (GridLayoutGroup_t607 * __this, const MethodInfo* method)
{
	{
		GridLayoutGroup_SetCellsAlongAxis_m2902(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::SetCellsAlongAxis(System.Int32)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void GridLayoutGroup_SetCellsAlongAxis_m2902 (GridLayoutGroup_t607 * __this, int32_t ___axis, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t364 * V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Vector2_t6  V_11 = {0};
	Vector2_t6  V_12 = {0};
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	Rect_t304  V_16 = {0};
	Vector2_t6  V_17 = {0};
	Rect_t304  V_18 = {0};
	Vector2_t6  V_19 = {0};
	Vector2_t6  V_20 = {0};
	Vector2_t6  V_21 = {0};
	Vector2_t6  V_22 = {0};
	Vector2_t6  V_23 = {0};
	Vector2_t6  V_24 = {0};
	Vector2_t6  V_25 = {0};
	Vector2_t6  V_26 = {0};
	Vector2_t6  V_27 = {0};
	Vector2_t6  V_28 = {0};
	Vector2_t6  V_29 = {0};
	Vector2_t6  V_30 = {0};
	Vector2_t6  V_31 = {0};
	Vector2_t6  V_32 = {0};
	Vector2_t6  V_33 = {0};
	Vector2_t6  V_34 = {0};
	Vector2_t6  V_35 = {0};
	Vector2_t6  V_36 = {0};
	Vector2_t6  V_37 = {0};
	Vector2_t6  V_38 = {0};
	Vector2_t6  V_39 = {0};
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_0064;
		}
	}
	{
		V_0 = 0;
		goto IL_0052;
	}

IL_000d:
	{
		List_1_t612 * L_1 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		RectTransform_t364 * L_3 = (RectTransform_t364 *)VirtFuncInvoker1< RectTransform_t364 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_1, L_2);
		V_1 = L_3;
		DrivenRectTransformTracker_t571 * L_4 = &(((LayoutGroup_t608 *)__this)->___m_Tracker_5);
		RectTransform_t364 * L_5 = V_1;
		DrivenRectTransformTracker_Add_m3352(L_4, __this, L_5, ((int32_t)16134), /*hidden argument*/NULL);
		RectTransform_t364 * L_6 = V_1;
		Vector2_t6  L_7 = Vector2_get_up_m3434(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectTransform_set_anchorMin_m3328(L_6, L_7, /*hidden argument*/NULL);
		RectTransform_t364 * L_8 = V_1;
		Vector2_t6  L_9 = Vector2_get_up_m3434(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		RectTransform_set_anchorMax_m3238(L_8, L_9, /*hidden argument*/NULL);
		RectTransform_t364 * L_10 = V_1;
		Vector2_t6  L_11 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		RectTransform_set_sizeDelta_m3239(L_10, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0052:
	{
		int32_t L_13 = V_0;
		List_1_t612 * L_14 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_14);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_0064:
	{
		RectTransform_t364 * L_16 = LayoutGroup_get_rectTransform_m2946(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Rect_t304  L_17 = RectTransform_get_rect_m3184(L_16, /*hidden argument*/NULL);
		V_16 = L_17;
		Vector2_t6  L_18 = Rect_get_size_m3243((&V_16), /*hidden argument*/NULL);
		V_17 = L_18;
		float L_19 = ((&V_17)->___x_1);
		V_2 = L_19;
		RectTransform_t364 * L_20 = LayoutGroup_get_rectTransform_m2946(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Rect_t304  L_21 = RectTransform_get_rect_m3184(L_20, /*hidden argument*/NULL);
		V_18 = L_21;
		Vector2_t6  L_22 = Rect_get_size_m3243((&V_18), /*hidden argument*/NULL);
		V_19 = L_22;
		float L_23 = ((&V_19)->___y_2);
		V_3 = L_23;
		V_4 = 1;
		V_5 = 1;
		int32_t L_24 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_24) == ((uint32_t)1))))
		{
			goto IL_00dc;
		}
	}
	{
		int32_t L_25 = (__this->___m_ConstraintCount_15);
		V_4 = L_25;
		List_1_t612 * L_26 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_26);
		int32_t L_28 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_29 = Mathf_CeilToInt_m867(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)L_27))/(float)(((float)L_28))))-(float)(0.001f))), /*hidden argument*/NULL);
		V_5 = L_29;
		goto IL_021e;
	}

IL_00dc:
	{
		int32_t L_30 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_30) == ((uint32_t)2))))
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_31 = (__this->___m_ConstraintCount_15);
		V_5 = L_31;
		List_1_t612 * L_32 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_32);
		int32_t L_34 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_35 = Mathf_CeilToInt_m867(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)L_33))/(float)(((float)L_34))))-(float)(0.001f))), /*hidden argument*/NULL);
		V_4 = L_35;
		goto IL_021e;
	}

IL_0112:
	{
		Vector2_t6  L_36 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_20 = L_36;
		float L_37 = ((&V_20)->___x_1);
		Vector2_t6  L_38 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_21 = L_38;
		float L_39 = ((&V_21)->___x_1);
		if ((!(((float)((float)((float)L_37+(float)L_39))) <= ((float)(0.0f)))))
		{
			goto IL_0147;
		}
	}
	{
		V_4 = ((int32_t)2147483647);
		goto IL_0198;
	}

IL_0147:
	{
		float L_40 = V_2;
		RectOffset_t404 * L_41 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		int32_t L_42 = RectOffset_get_horizontal_m3432(L_41, /*hidden argument*/NULL);
		Vector2_t6  L_43 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_22 = L_43;
		float L_44 = ((&V_22)->___x_1);
		Vector2_t6  L_45 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_23 = L_45;
		float L_46 = ((&V_23)->___x_1);
		Vector2_t6  L_47 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_24 = L_47;
		float L_48 = ((&V_24)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_49 = Mathf_FloorToInt_m815(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)((float)((float)L_40-(float)(((float)L_42))))+(float)L_44))+(float)(0.001f)))/(float)((float)((float)L_46+(float)L_48)))), /*hidden argument*/NULL);
		int32_t L_50 = Mathf_Max_m3310(NULL /*static, unused*/, 1, L_49, /*hidden argument*/NULL);
		V_4 = L_50;
	}

IL_0198:
	{
		Vector2_t6  L_51 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_25 = L_51;
		float L_52 = ((&V_25)->___y_2);
		Vector2_t6  L_53 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_26 = L_53;
		float L_54 = ((&V_26)->___y_2);
		if ((!(((float)((float)((float)L_52+(float)L_54))) <= ((float)(0.0f)))))
		{
			goto IL_01cd;
		}
	}
	{
		V_5 = ((int32_t)2147483647);
		goto IL_021e;
	}

IL_01cd:
	{
		float L_55 = V_3;
		RectOffset_t404 * L_56 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		int32_t L_57 = RectOffset_get_vertical_m3433(L_56, /*hidden argument*/NULL);
		Vector2_t6  L_58 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_27 = L_58;
		float L_59 = ((&V_27)->___y_2);
		Vector2_t6  L_60 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_28 = L_60;
		float L_61 = ((&V_28)->___y_2);
		Vector2_t6  L_62 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_29 = L_62;
		float L_63 = ((&V_29)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_64 = Mathf_FloorToInt_m815(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)((float)((float)L_55-(float)(((float)L_57))))+(float)L_59))+(float)(0.001f)))/(float)((float)((float)L_61+(float)L_63)))), /*hidden argument*/NULL);
		int32_t L_65 = Mathf_Max_m3310(NULL /*static, unused*/, 1, L_64, /*hidden argument*/NULL);
		V_5 = L_65;
	}

IL_021e:
	{
		int32_t L_66 = GridLayoutGroup_get_startCorner_m2886(__this, /*hidden argument*/NULL);
		V_6 = ((int32_t)((int32_t)L_66%(int32_t)2));
		int32_t L_67 = GridLayoutGroup_get_startCorner_m2886(__this, /*hidden argument*/NULL);
		V_7 = ((int32_t)((int32_t)L_67/(int32_t)2));
		int32_t L_68 = GridLayoutGroup_get_startAxis_m2888(__this, /*hidden argument*/NULL);
		if (L_68)
		{
			goto IL_027a;
		}
	}
	{
		int32_t L_69 = V_4;
		V_8 = L_69;
		int32_t L_70 = V_4;
		List_1_t612 * L_71 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_71);
		int32_t L_72 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_71);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_73 = Mathf_Clamp_m712(NULL /*static, unused*/, L_70, 1, L_72, /*hidden argument*/NULL);
		V_9 = L_73;
		int32_t L_74 = V_5;
		List_1_t612 * L_75 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_75);
		int32_t L_76 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_75);
		int32_t L_77 = V_8;
		int32_t L_78 = Mathf_CeilToInt_m867(NULL /*static, unused*/, ((float)((float)(((float)L_76))/(float)(((float)L_77)))), /*hidden argument*/NULL);
		int32_t L_79 = Mathf_Clamp_m712(NULL /*static, unused*/, L_74, 1, L_78, /*hidden argument*/NULL);
		V_10 = L_79;
		goto IL_02b2;
	}

IL_027a:
	{
		int32_t L_80 = V_5;
		V_8 = L_80;
		int32_t L_81 = V_5;
		List_1_t612 * L_82 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_82);
		int32_t L_83 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_82);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		int32_t L_84 = Mathf_Clamp_m712(NULL /*static, unused*/, L_81, 1, L_83, /*hidden argument*/NULL);
		V_10 = L_84;
		int32_t L_85 = V_4;
		List_1_t612 * L_86 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_86);
		int32_t L_87 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_86);
		int32_t L_88 = V_8;
		int32_t L_89 = Mathf_CeilToInt_m867(NULL /*static, unused*/, ((float)((float)(((float)L_87))/(float)(((float)L_88)))), /*hidden argument*/NULL);
		int32_t L_90 = Mathf_Clamp_m712(NULL /*static, unused*/, L_85, 1, L_89, /*hidden argument*/NULL);
		V_9 = L_90;
	}

IL_02b2:
	{
		int32_t L_91 = V_9;
		Vector2_t6  L_92 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_30 = L_92;
		float L_93 = ((&V_30)->___x_1);
		int32_t L_94 = V_9;
		Vector2_t6  L_95 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_31 = L_95;
		float L_96 = ((&V_31)->___x_1);
		int32_t L_97 = V_10;
		Vector2_t6  L_98 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_32 = L_98;
		float L_99 = ((&V_32)->___y_2);
		int32_t L_100 = V_10;
		Vector2_t6  L_101 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_33 = L_101;
		float L_102 = ((&V_33)->___y_2);
		Vector2__ctor_m630((&V_11), ((float)((float)((float)((float)(((float)L_91))*(float)L_93))+(float)((float)((float)(((float)((int32_t)((int32_t)L_94-(int32_t)1))))*(float)L_96)))), ((float)((float)((float)((float)(((float)L_97))*(float)L_99))+(float)((float)((float)(((float)((int32_t)((int32_t)L_100-(int32_t)1))))*(float)L_102)))), /*hidden argument*/NULL);
		float L_103 = ((&V_11)->___x_1);
		float L_104 = LayoutGroup_GetStartOffset_m2962(__this, 0, L_103, /*hidden argument*/NULL);
		float L_105 = ((&V_11)->___y_2);
		float L_106 = LayoutGroup_GetStartOffset_m2962(__this, 1, L_105, /*hidden argument*/NULL);
		Vector2__ctor_m630((&V_12), L_104, L_106, /*hidden argument*/NULL);
		V_13 = 0;
		goto IL_042c;
	}

IL_0336:
	{
		int32_t L_107 = GridLayoutGroup_get_startAxis_m2888(__this, /*hidden argument*/NULL);
		if (L_107)
		{
			goto IL_0354;
		}
	}
	{
		int32_t L_108 = V_13;
		int32_t L_109 = V_8;
		V_14 = ((int32_t)((int32_t)L_108%(int32_t)L_109));
		int32_t L_110 = V_13;
		int32_t L_111 = V_8;
		V_15 = ((int32_t)((int32_t)L_110/(int32_t)L_111));
		goto IL_0362;
	}

IL_0354:
	{
		int32_t L_112 = V_13;
		int32_t L_113 = V_8;
		V_14 = ((int32_t)((int32_t)L_112/(int32_t)L_113));
		int32_t L_114 = V_13;
		int32_t L_115 = V_8;
		V_15 = ((int32_t)((int32_t)L_114%(int32_t)L_115));
	}

IL_0362:
	{
		int32_t L_116 = V_6;
		if ((!(((uint32_t)L_116) == ((uint32_t)1))))
		{
			goto IL_0373;
		}
	}
	{
		int32_t L_117 = V_9;
		int32_t L_118 = V_14;
		V_14 = ((int32_t)((int32_t)((int32_t)((int32_t)L_117-(int32_t)1))-(int32_t)L_118));
	}

IL_0373:
	{
		int32_t L_119 = V_7;
		if ((!(((uint32_t)L_119) == ((uint32_t)1))))
		{
			goto IL_0384;
		}
	}
	{
		int32_t L_120 = V_10;
		int32_t L_121 = V_15;
		V_15 = ((int32_t)((int32_t)((int32_t)((int32_t)L_120-(int32_t)1))-(int32_t)L_121));
	}

IL_0384:
	{
		List_1_t612 * L_122 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		int32_t L_123 = V_13;
		NullCheck(L_122);
		RectTransform_t364 * L_124 = (RectTransform_t364 *)VirtFuncInvoker1< RectTransform_t364 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_122, L_123);
		float L_125 = ((&V_12)->___x_1);
		Vector2_t6  L_126 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_34 = L_126;
		float L_127 = Vector2_get_Item_m3248((&V_34), 0, /*hidden argument*/NULL);
		Vector2_t6  L_128 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_35 = L_128;
		float L_129 = Vector2_get_Item_m3248((&V_35), 0, /*hidden argument*/NULL);
		int32_t L_130 = V_14;
		Vector2_t6  L_131 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_36 = L_131;
		float L_132 = Vector2_get_Item_m3248((&V_36), 0, /*hidden argument*/NULL);
		LayoutGroup_SetChildAlongAxis_m2964(__this, L_124, 0, ((float)((float)L_125+(float)((float)((float)((float)((float)L_127+(float)L_129))*(float)(((float)L_130)))))), L_132, /*hidden argument*/NULL);
		List_1_t612 * L_133 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		int32_t L_134 = V_13;
		NullCheck(L_133);
		RectTransform_t364 * L_135 = (RectTransform_t364 *)VirtFuncInvoker1< RectTransform_t364 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_133, L_134);
		float L_136 = ((&V_12)->___y_2);
		Vector2_t6  L_137 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_37 = L_137;
		float L_138 = Vector2_get_Item_m3248((&V_37), 1, /*hidden argument*/NULL);
		Vector2_t6  L_139 = GridLayoutGroup_get_spacing_m2892(__this, /*hidden argument*/NULL);
		V_38 = L_139;
		float L_140 = Vector2_get_Item_m3248((&V_38), 1, /*hidden argument*/NULL);
		int32_t L_141 = V_15;
		Vector2_t6  L_142 = GridLayoutGroup_get_cellSize_m2890(__this, /*hidden argument*/NULL);
		V_39 = L_142;
		float L_143 = Vector2_get_Item_m3248((&V_39), 1, /*hidden argument*/NULL);
		LayoutGroup_SetChildAlongAxis_m2964(__this, L_135, 1, ((float)((float)L_136+(float)((float)((float)((float)((float)L_138+(float)L_140))*(float)(((float)L_141)))))), L_143, /*hidden argument*/NULL);
		int32_t L_144 = V_13;
		V_13 = ((int32_t)((int32_t)L_144+(int32_t)1));
	}

IL_042c:
	{
		int32_t L_145 = V_13;
		List_1_t612 * L_146 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_146);
		int32_t L_147 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_146);
		if ((((int32_t)L_145) < ((int32_t)L_147)))
		{
			goto IL_0336;
		}
	}
	{
		return;
	}
}
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroup.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroupMethodDeclarations.h"

// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"


// System.Void UnityEngine.UI.HorizontalLayoutGroup::.ctor()
extern "C" void HorizontalLayoutGroup__ctor_m2903 (HorizontalLayoutGroup_t609 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup__ctor_m2908(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputHorizontal()
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m2904 (HorizontalLayoutGroup_t609 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_CalculateLayoutInputHorizontal_m2948(__this, /*hidden argument*/NULL);
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m2915(__this, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputVertical()
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputVertical_m2905 (HorizontalLayoutGroup_t609 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m2915(__this, 1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutHorizontal()
extern "C" void HorizontalLayoutGroup_SetLayoutHorizontal_m2906 (HorizontalLayoutGroup_t609 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m2916(__this, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutVertical()
extern "C" void HorizontalLayoutGroup_SetLayoutVertical_m2907 (HorizontalLayoutGroup_t609 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m2916(__this, 1, 0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrou.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
struct LayoutGroup_t608;
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Single>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Single>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisSingle_t254_m3435_gshared (LayoutGroup_t608 * __this, float* p0, float p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisSingle_t254_m3435(__this, p0, p1, method) (( void (*) (LayoutGroup_t608 *, float*, float, const MethodInfo*))LayoutGroup_SetProperty_TisSingle_t254_m3435_gshared)(__this, p0, p1, method)
struct LayoutGroup_t608;
struct LayoutGroup_t608;
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Byte>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Byte>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisByte_t680_m3437_gshared (LayoutGroup_t608 * __this, uint8_t* p0, uint8_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisByte_t680_m3437(__this, p0, p1, method) (( void (*) (LayoutGroup_t608 *, uint8_t*, uint8_t, const MethodInfo*))LayoutGroup_SetProperty_TisByte_t680_m3437_gshared)(__this, p0, p1, method)
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Boolean>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Boolean>(!!0&,!!0)
#define LayoutGroup_SetProperty_TisBoolean_t273_m3436(__this, p0, p1, method) (( void (*) (LayoutGroup_t608 *, bool*, bool, const MethodInfo*))LayoutGroup_SetProperty_TisByte_t680_m3437_gshared)(__this, p0, p1, method)


// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
extern "C" void HorizontalOrVerticalLayoutGroup__ctor_m2908 (HorizontalOrVerticalLayoutGroup_t610 * __this, const MethodInfo* method)
{
	{
		__this->___m_ChildForceExpandWidth_11 = 1;
		__this->___m_ChildForceExpandHeight_12 = 1;
		LayoutGroup__ctor_m2941(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern "C" float HorizontalOrVerticalLayoutGroup_get_spacing_m2909 (HorizontalOrVerticalLayoutGroup_t610 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Spacing_10);
		return L_0;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
extern const MethodInfo* LayoutGroup_SetProperty_TisSingle_t254_m3435_MethodInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_set_spacing_m2910 (HorizontalOrVerticalLayoutGroup_t610 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisSingle_t254_m3435_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483938);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_Spacing_10);
		float L_1 = ___value;
		LayoutGroup_SetProperty_TisSingle_t254_m3435(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisSingle_t254_m3435_MethodInfo_var);
		return;
	}
}
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m2911 (HorizontalOrVerticalLayoutGroup_t610 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_ChildForceExpandWidth_11);
		return L_0;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
extern const MethodInfo* LayoutGroup_SetProperty_TisBoolean_t273_m3436_MethodInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m2912 (HorizontalOrVerticalLayoutGroup_t610 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisBoolean_t273_m3436_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool* L_0 = &(__this->___m_ChildForceExpandWidth_11);
		bool L_1 = ___value;
		LayoutGroup_SetProperty_TisBoolean_t273_m3436(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisBoolean_t273_m3436_MethodInfo_var);
		return;
	}
}
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m2913 (HorizontalOrVerticalLayoutGroup_t610 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_ChildForceExpandHeight_12);
		return L_0;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern const MethodInfo* LayoutGroup_SetProperty_TisBoolean_t273_m3436_MethodInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m2914 (HorizontalOrVerticalLayoutGroup_t610 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisBoolean_t273_m3436_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool* L_0 = &(__this->___m_ChildForceExpandHeight_12);
		bool L_1 = ___value;
		LayoutGroup_SetProperty_TisBoolean_t273_m3436(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisBoolean_t273_m3436_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m2915 (HorizontalOrVerticalLayoutGroup_t610 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	RectTransform_t364 * V_6 = {0};
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	int32_t G_B3_0 = 0;
	bool G_B7_0 = false;
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		RectOffset_t404 * L_1 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = RectOffset_get_horizontal_m3432(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0021;
	}

IL_0016:
	{
		RectOffset_t404 * L_3 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_vertical_m3433(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0021:
	{
		V_0 = (((float)G_B3_0));
		float L_5 = V_0;
		V_1 = L_5;
		float L_6 = V_0;
		V_2 = L_6;
		V_3 = (0.0f);
		bool L_7 = ___isVertical;
		int32_t L_8 = ___axis;
		V_4 = ((int32_t)((int32_t)L_7^(int32_t)((((int32_t)L_8) == ((int32_t)1))? 1 : 0)));
		V_5 = 0;
		goto IL_00e2;
	}

IL_003d:
	{
		List_1_t612 * L_9 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_5;
		NullCheck(L_9);
		RectTransform_t364 * L_11 = (RectTransform_t364 *)VirtFuncInvoker1< RectTransform_t364 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_9, L_10);
		V_6 = L_11;
		RectTransform_t364 * L_12 = V_6;
		int32_t L_13 = ___axis;
		float L_14 = LayoutUtility_GetMinSize_m2990(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		V_7 = L_14;
		RectTransform_t364 * L_15 = V_6;
		int32_t L_16 = ___axis;
		float L_17 = LayoutUtility_GetPreferredSize_m2991(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_8 = L_17;
		RectTransform_t364 * L_18 = V_6;
		int32_t L_19 = ___axis;
		float L_20 = LayoutUtility_GetFlexibleSize_m2992(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_9 = L_20;
		int32_t L_21 = ___axis;
		if (L_21)
		{
			goto IL_007b;
		}
	}
	{
		bool L_22 = HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m2911(__this, /*hidden argument*/NULL);
		G_B7_0 = L_22;
		goto IL_0081;
	}

IL_007b:
	{
		bool L_23 = HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m2913(__this, /*hidden argument*/NULL);
		G_B7_0 = L_23;
	}

IL_0081:
	{
		if (!G_B7_0)
		{
			goto IL_0094;
		}
	}
	{
		float L_24 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Max_m782(NULL /*static, unused*/, L_24, (1.0f), /*hidden argument*/NULL);
		V_9 = L_25;
	}

IL_0094:
	{
		bool L_26 = V_4;
		if (!L_26)
		{
			goto IL_00bf;
		}
	}
	{
		float L_27 = V_7;
		float L_28 = V_0;
		float L_29 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_30 = Mathf_Max_m782(NULL /*static, unused*/, ((float)((float)L_27+(float)L_28)), L_29, /*hidden argument*/NULL);
		V_1 = L_30;
		float L_31 = V_8;
		float L_32 = V_0;
		float L_33 = V_2;
		float L_34 = Mathf_Max_m782(NULL /*static, unused*/, ((float)((float)L_31+(float)L_32)), L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		float L_35 = V_9;
		float L_36 = V_3;
		float L_37 = Mathf_Max_m782(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_3 = L_37;
		goto IL_00dc;
	}

IL_00bf:
	{
		float L_38 = V_1;
		float L_39 = V_7;
		float L_40 = HorizontalOrVerticalLayoutGroup_get_spacing_m2909(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_38+(float)((float)((float)L_39+(float)L_40))));
		float L_41 = V_2;
		float L_42 = V_8;
		float L_43 = HorizontalOrVerticalLayoutGroup_get_spacing_m2909(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_41+(float)((float)((float)L_42+(float)L_43))));
		float L_44 = V_3;
		float L_45 = V_9;
		V_3 = ((float)((float)L_44+(float)L_45));
	}

IL_00dc:
	{
		int32_t L_46 = V_5;
		V_5 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_00e2:
	{
		int32_t L_47 = V_5;
		List_1_t612 * L_48 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_48);
		if ((((int32_t)L_47) < ((int32_t)L_49)))
		{
			goto IL_003d;
		}
	}
	{
		bool L_50 = V_4;
		if (L_50)
		{
			goto IL_011e;
		}
	}
	{
		List_1_t612 * L_51 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_51);
		int32_t L_52 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_51);
		if ((((int32_t)L_52) <= ((int32_t)0)))
		{
			goto IL_011e;
		}
	}
	{
		float L_53 = V_1;
		float L_54 = HorizontalOrVerticalLayoutGroup_get_spacing_m2909(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_53-(float)L_54));
		float L_55 = V_2;
		float L_56 = HorizontalOrVerticalLayoutGroup_get_spacing_m2909(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_55-(float)L_56));
	}

IL_011e:
	{
		float L_57 = V_1;
		float L_58 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_59 = Mathf_Max_m782(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		V_2 = L_59;
		float L_60 = V_1;
		float L_61 = V_2;
		float L_62 = V_3;
		int32_t L_63 = ___axis;
		LayoutGroup_SetLayoutInputForAxis_m2963(__this, L_60, L_61, L_62, L_63, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m2916 (HorizontalOrVerticalLayoutGroup_t610 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	RectTransform_t364 * V_4 = {0};
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	int32_t V_13 = 0;
	RectTransform_t364 * V_14 = {0};
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	Rect_t304  V_19 = {0};
	Vector2_t6  V_20 = {0};
	float G_B3_0 = 0.0f;
	float G_B2_0 = 0.0f;
	int32_t G_B4_0 = 0;
	float G_B4_1 = 0.0f;
	bool G_B8_0 = false;
	float G_B12_0 = 0.0f;
	float G_B12_1 = 0.0f;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	float G_B13_0 = 0.0f;
	float G_B13_1 = 0.0f;
	float G_B13_2 = 0.0f;
	int32_t G_B19_0 = 0;
	float G_B23_0 = 0.0f;
	int32_t G_B23_1 = 0;
	HorizontalOrVerticalLayoutGroup_t610 * G_B23_2 = {0};
	float G_B22_0 = 0.0f;
	int32_t G_B22_1 = 0;
	HorizontalOrVerticalLayoutGroup_t610 * G_B22_2 = {0};
	int32_t G_B24_0 = 0;
	float G_B24_1 = 0.0f;
	int32_t G_B24_2 = 0;
	HorizontalOrVerticalLayoutGroup_t610 * G_B24_3 = {0};
	bool G_B34_0 = false;
	{
		RectTransform_t364 * L_0 = LayoutGroup_get_rectTransform_m2946(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rect_t304  L_1 = RectTransform_get_rect_m3184(L_0, /*hidden argument*/NULL);
		V_19 = L_1;
		Vector2_t6  L_2 = Rect_get_size_m3243((&V_19), /*hidden argument*/NULL);
		V_20 = L_2;
		int32_t L_3 = ___axis;
		float L_4 = Vector2_get_Item_m3248((&V_20), L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = ___isVertical;
		int32_t L_6 = ___axis;
		V_1 = ((int32_t)((int32_t)L_5^(int32_t)((((int32_t)L_6) == ((int32_t)1))? 1 : 0)));
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_00fe;
		}
	}
	{
		float L_8 = V_0;
		int32_t L_9 = ___axis;
		G_B2_0 = L_8;
		if (L_9)
		{
			G_B3_0 = L_8;
			goto IL_0043;
		}
	}
	{
		RectOffset_t404 * L_10 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_horizontal_m3432(L_10, /*hidden argument*/NULL);
		G_B4_0 = L_11;
		G_B4_1 = G_B2_0;
		goto IL_004e;
	}

IL_0043:
	{
		RectOffset_t404 * L_12 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_vertical_m3433(L_12, /*hidden argument*/NULL);
		G_B4_0 = L_13;
		G_B4_1 = G_B3_0;
	}

IL_004e:
	{
		V_2 = ((float)((float)G_B4_1-(float)(((float)G_B4_0))));
		V_3 = 0;
		goto IL_00e8;
	}

IL_0058:
	{
		List_1_t612 * L_14 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		NullCheck(L_14);
		RectTransform_t364 * L_16 = (RectTransform_t364 *)VirtFuncInvoker1< RectTransform_t364 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_14, L_15);
		V_4 = L_16;
		RectTransform_t364 * L_17 = V_4;
		int32_t L_18 = ___axis;
		float L_19 = LayoutUtility_GetMinSize_m2990(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		RectTransform_t364 * L_20 = V_4;
		int32_t L_21 = ___axis;
		float L_22 = LayoutUtility_GetPreferredSize_m2991(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		V_6 = L_22;
		RectTransform_t364 * L_23 = V_4;
		int32_t L_24 = ___axis;
		float L_25 = LayoutUtility_GetFlexibleSize_m2992(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_7 = L_25;
		int32_t L_26 = ___axis;
		if (L_26)
		{
			goto IL_0095;
		}
	}
	{
		bool L_27 = HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m2911(__this, /*hidden argument*/NULL);
		G_B8_0 = L_27;
		goto IL_009b;
	}

IL_0095:
	{
		bool L_28 = HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m2913(__this, /*hidden argument*/NULL);
		G_B8_0 = L_28;
	}

IL_009b:
	{
		if (!G_B8_0)
		{
			goto IL_00ae;
		}
	}
	{
		float L_29 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_30 = Mathf_Max_m782(NULL /*static, unused*/, L_29, (1.0f), /*hidden argument*/NULL);
		V_7 = L_30;
	}

IL_00ae:
	{
		float L_31 = V_2;
		float L_32 = V_5;
		float L_33 = V_7;
		G_B11_0 = L_32;
		G_B11_1 = L_31;
		if ((!(((float)L_33) > ((float)(0.0f)))))
		{
			G_B12_0 = L_32;
			G_B12_1 = L_31;
			goto IL_00c3;
		}
	}
	{
		float L_34 = V_0;
		G_B13_0 = L_34;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c5;
	}

IL_00c3:
	{
		float L_35 = V_6;
		G_B13_0 = L_35;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_36 = Mathf_Clamp_m611(NULL /*static, unused*/, G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		V_8 = L_36;
		int32_t L_37 = ___axis;
		float L_38 = V_8;
		float L_39 = LayoutGroup_GetStartOffset_m2962(__this, L_37, L_38, /*hidden argument*/NULL);
		V_9 = L_39;
		RectTransform_t364 * L_40 = V_4;
		int32_t L_41 = ___axis;
		float L_42 = V_9;
		float L_43 = V_8;
		LayoutGroup_SetChildAlongAxis_m2964(__this, L_40, L_41, L_42, L_43, /*hidden argument*/NULL);
		int32_t L_44 = V_3;
		V_3 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_00e8:
	{
		int32_t L_45 = V_3;
		List_1_t612 * L_46 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		int32_t L_47 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_46);
		if ((((int32_t)L_45) < ((int32_t)L_47)))
		{
			goto IL_0058;
		}
	}
	{
		goto IL_028e;
	}

IL_00fe:
	{
		int32_t L_48 = ___axis;
		if (L_48)
		{
			goto IL_0114;
		}
	}
	{
		RectOffset_t404 * L_49 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		int32_t L_50 = RectOffset_get_left_m3438(L_49, /*hidden argument*/NULL);
		G_B19_0 = L_50;
		goto IL_011f;
	}

IL_0114:
	{
		RectOffset_t404 * L_51 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_51);
		int32_t L_52 = RectOffset_get_top_m3439(L_51, /*hidden argument*/NULL);
		G_B19_0 = L_52;
	}

IL_011f:
	{
		V_10 = (((float)G_B19_0));
		int32_t L_53 = ___axis;
		float L_54 = LayoutGroup_GetTotalFlexibleSize_m2961(__this, L_53, /*hidden argument*/NULL);
		if ((!(((float)L_54) == ((float)(0.0f)))))
		{
			goto IL_0173;
		}
	}
	{
		int32_t L_55 = ___axis;
		float L_56 = LayoutGroup_GetTotalPreferredSize_m2960(__this, L_55, /*hidden argument*/NULL);
		float L_57 = V_0;
		if ((!(((float)L_56) < ((float)L_57))))
		{
			goto IL_0173;
		}
	}
	{
		int32_t L_58 = ___axis;
		int32_t L_59 = ___axis;
		float L_60 = LayoutGroup_GetTotalPreferredSize_m2960(__this, L_59, /*hidden argument*/NULL);
		int32_t L_61 = ___axis;
		G_B22_0 = L_60;
		G_B22_1 = L_58;
		G_B22_2 = __this;
		if (L_61)
		{
			G_B23_0 = L_60;
			G_B23_1 = L_58;
			G_B23_2 = __this;
			goto IL_015f;
		}
	}
	{
		RectOffset_t404 * L_62 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = RectOffset_get_horizontal_m3432(L_62, /*hidden argument*/NULL);
		G_B24_0 = L_63;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		goto IL_016a;
	}

IL_015f:
	{
		RectOffset_t404 * L_64 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_64);
		int32_t L_65 = RectOffset_get_vertical_m3433(L_64, /*hidden argument*/NULL);
		G_B24_0 = L_65;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
	}

IL_016a:
	{
		NullCheck(G_B24_3);
		float L_66 = LayoutGroup_GetStartOffset_m2962(G_B24_3, G_B24_2, ((float)((float)G_B24_1-(float)(((float)G_B24_0)))), /*hidden argument*/NULL);
		V_10 = L_66;
	}

IL_0173:
	{
		V_11 = (0.0f);
		int32_t L_67 = ___axis;
		float L_68 = LayoutGroup_GetTotalMinSize_m2959(__this, L_67, /*hidden argument*/NULL);
		int32_t L_69 = ___axis;
		float L_70 = LayoutGroup_GetTotalPreferredSize_m2960(__this, L_69, /*hidden argument*/NULL);
		if ((((float)L_68) == ((float)L_70)))
		{
			goto IL_01ad;
		}
	}
	{
		float L_71 = V_0;
		int32_t L_72 = ___axis;
		float L_73 = LayoutGroup_GetTotalMinSize_m2959(__this, L_72, /*hidden argument*/NULL);
		int32_t L_74 = ___axis;
		float L_75 = LayoutGroup_GetTotalPreferredSize_m2960(__this, L_74, /*hidden argument*/NULL);
		int32_t L_76 = ___axis;
		float L_77 = LayoutGroup_GetTotalMinSize_m2959(__this, L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_78 = Mathf_Clamp01_m3227(NULL /*static, unused*/, ((float)((float)((float)((float)L_71-(float)L_73))/(float)((float)((float)L_75-(float)L_77)))), /*hidden argument*/NULL);
		V_11 = L_78;
	}

IL_01ad:
	{
		V_12 = (0.0f);
		float L_79 = V_0;
		int32_t L_80 = ___axis;
		float L_81 = LayoutGroup_GetTotalPreferredSize_m2960(__this, L_80, /*hidden argument*/NULL);
		if ((!(((float)L_79) > ((float)L_81))))
		{
			goto IL_01e5;
		}
	}
	{
		int32_t L_82 = ___axis;
		float L_83 = LayoutGroup_GetTotalFlexibleSize_m2961(__this, L_82, /*hidden argument*/NULL);
		if ((!(((float)L_83) > ((float)(0.0f)))))
		{
			goto IL_01e5;
		}
	}
	{
		float L_84 = V_0;
		int32_t L_85 = ___axis;
		float L_86 = LayoutGroup_GetTotalPreferredSize_m2960(__this, L_85, /*hidden argument*/NULL);
		int32_t L_87 = ___axis;
		float L_88 = LayoutGroup_GetTotalFlexibleSize_m2961(__this, L_87, /*hidden argument*/NULL);
		V_12 = ((float)((float)((float)((float)L_84-(float)L_86))/(float)L_88));
	}

IL_01e5:
	{
		V_13 = 0;
		goto IL_027c;
	}

IL_01ed:
	{
		List_1_t612 * L_89 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		int32_t L_90 = V_13;
		NullCheck(L_89);
		RectTransform_t364 * L_91 = (RectTransform_t364 *)VirtFuncInvoker1< RectTransform_t364 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_89, L_90);
		V_14 = L_91;
		RectTransform_t364 * L_92 = V_14;
		int32_t L_93 = ___axis;
		float L_94 = LayoutUtility_GetMinSize_m2990(NULL /*static, unused*/, L_92, L_93, /*hidden argument*/NULL);
		V_15 = L_94;
		RectTransform_t364 * L_95 = V_14;
		int32_t L_96 = ___axis;
		float L_97 = LayoutUtility_GetPreferredSize_m2991(NULL /*static, unused*/, L_95, L_96, /*hidden argument*/NULL);
		V_16 = L_97;
		RectTransform_t364 * L_98 = V_14;
		int32_t L_99 = ___axis;
		float L_100 = LayoutUtility_GetFlexibleSize_m2992(NULL /*static, unused*/, L_98, L_99, /*hidden argument*/NULL);
		V_17 = L_100;
		int32_t L_101 = ___axis;
		if (L_101)
		{
			goto IL_022b;
		}
	}
	{
		bool L_102 = HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m2911(__this, /*hidden argument*/NULL);
		G_B34_0 = L_102;
		goto IL_0231;
	}

IL_022b:
	{
		bool L_103 = HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m2913(__this, /*hidden argument*/NULL);
		G_B34_0 = L_103;
	}

IL_0231:
	{
		if (!G_B34_0)
		{
			goto IL_0244;
		}
	}
	{
		float L_104 = V_17;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_105 = Mathf_Max_m782(NULL /*static, unused*/, L_104, (1.0f), /*hidden argument*/NULL);
		V_17 = L_105;
	}

IL_0244:
	{
		float L_106 = V_15;
		float L_107 = V_16;
		float L_108 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_109 = Mathf_Lerp_m610(NULL /*static, unused*/, L_106, L_107, L_108, /*hidden argument*/NULL);
		V_18 = L_109;
		float L_110 = V_18;
		float L_111 = V_17;
		float L_112 = V_12;
		V_18 = ((float)((float)L_110+(float)((float)((float)L_111*(float)L_112))));
		RectTransform_t364 * L_113 = V_14;
		int32_t L_114 = ___axis;
		float L_115 = V_10;
		float L_116 = V_18;
		LayoutGroup_SetChildAlongAxis_m2964(__this, L_113, L_114, L_115, L_116, /*hidden argument*/NULL);
		float L_117 = V_10;
		float L_118 = V_18;
		float L_119 = HorizontalOrVerticalLayoutGroup_get_spacing_m2909(__this, /*hidden argument*/NULL);
		V_10 = ((float)((float)L_117+(float)((float)((float)L_118+(float)L_119))));
		int32_t L_120 = V_13;
		V_13 = ((int32_t)((int32_t)L_120+(int32_t)1));
	}

IL_027c:
	{
		int32_t L_121 = V_13;
		List_1_t612 * L_122 = LayoutGroup_get_rectChildren_m2947(__this, /*hidden argument*/NULL);
		NullCheck(L_122);
		int32_t L_123 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_122);
		if ((((int32_t)L_121) < ((int32_t)L_123)))
		{
			goto IL_01ed;
		}
	}

IL_028e:
	{
		return;
	}
}
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElementMethodDeclarations.h"

// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
struct SetPropertyUtility_t581;
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility.h"
struct SetPropertyUtility_t581;
// Declaration System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Byte>(!!0&,!!0)
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Byte>(!!0&,!!0)
extern "C" bool SetPropertyUtility_SetStruct_TisByte_t680_m3224_gshared (Object_t * __this /* static, unused */, uint8_t* p0, uint8_t p1, const MethodInfo* method);
#define SetPropertyUtility_SetStruct_TisByte_t680_m3224(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, uint8_t*, uint8_t, const MethodInfo*))SetPropertyUtility_SetStruct_TisByte_t680_m3224_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Boolean>(!!0&,!!0)
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Boolean>(!!0&,!!0)
#define SetPropertyUtility_SetStruct_TisBoolean_t273_m3223(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, bool*, bool, const MethodInfo*))SetPropertyUtility_SetStruct_TisByte_t680_m3224_gshared)(__this /* static, unused */, p0, p1, method)
struct SetPropertyUtility_t581;
// Declaration System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Single>(!!0&,!!0)
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Single>(!!0&,!!0)
extern "C" bool SetPropertyUtility_SetStruct_TisSingle_t254_m3226_gshared (Object_t * __this /* static, unused */, float* p0, float p1, const MethodInfo* method);
#define SetPropertyUtility_SetStruct_TisSingle_t254_m3226(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, float*, float, const MethodInfo*))SetPropertyUtility_SetStruct_TisSingle_t254_m3226_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void UnityEngine.UI.LayoutElement::.ctor()
extern "C" void LayoutElement__ctor_m2917 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		__this->___m_MinWidth_3 = (-1.0f);
		__this->___m_MinHeight_4 = (-1.0f);
		__this->___m_PreferredWidth_5 = (-1.0f);
		__this->___m_PreferredHeight_6 = (-1.0f);
		__this->___m_FlexibleWidth_7 = (-1.0f);
		__this->___m_FlexibleHeight_8 = (-1.0f);
		UIBehaviour__ctor_m1974(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutElement::get_ignoreLayout()
extern "C" bool LayoutElement_get_ignoreLayout_m2918 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_IgnoreLayout_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_ignoreLayout(System.Boolean)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisBoolean_t273_m3223_MethodInfo_var;
extern "C" void LayoutElement_set_ignoreLayout_m2919 (LayoutElement_t611 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisBoolean_t273_m3223_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483876);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool* L_0 = &(__this->___m_IgnoreLayout_2);
		bool L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisBoolean_t273_m3223(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisBoolean_t273_m3223_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputHorizontal()
extern "C" void LayoutElement_CalculateLayoutInputHorizontal_m2920 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputVertical()
extern "C" void LayoutElement_CalculateLayoutInputVertical_m2921 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_minWidth()
extern "C" float LayoutElement_get_minWidth_m2922 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_MinWidth_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var;
extern "C" void LayoutElement_set_minWidth_m2923 (LayoutElement_t611 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483878);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_MinWidth_3);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t254_m3226(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_minHeight()
extern "C" float LayoutElement_get_minHeight_m2924 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_MinHeight_4);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var;
extern "C" void LayoutElement_set_minHeight_m2925 (LayoutElement_t611 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483878);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_MinHeight_4);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t254_m3226(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_preferredWidth()
extern "C" float LayoutElement_get_preferredWidth_m2926 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_PreferredWidth_5);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var;
extern "C" void LayoutElement_set_preferredWidth_m2927 (LayoutElement_t611 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483878);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_PreferredWidth_5);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t254_m3226(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_preferredHeight()
extern "C" float LayoutElement_get_preferredHeight_m2928 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_PreferredHeight_6);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var;
extern "C" void LayoutElement_set_preferredHeight_m2929 (LayoutElement_t611 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483878);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_PreferredHeight_6);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t254_m3226(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth()
extern "C" float LayoutElement_get_flexibleWidth_m2930 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FlexibleWidth_7);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var;
extern "C" void LayoutElement_set_flexibleWidth_m2931 (LayoutElement_t611 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483878);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_FlexibleWidth_7);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t254_m3226(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight()
extern "C" float LayoutElement_get_flexibleHeight_m2932 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FlexibleHeight_8);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var;
extern "C" void LayoutElement_set_flexibleHeight_m2933 (LayoutElement_t611 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483878);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_FlexibleHeight_8);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t254_m3226(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t254_m3226_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Int32 UnityEngine.UI.LayoutElement::get_layoutPriority()
extern "C" int32_t LayoutElement_get_layoutPriority_m2934 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnEnable()
extern "C" void LayoutElement_OnEnable_m2935 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m1976(__this, /*hidden argument*/NULL);
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnTransformParentChanged()
extern "C" void LayoutElement_OnTransformParentChanged_m2936 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnDisable()
extern "C" void LayoutElement_OnDisable_m2937 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m1978(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnDidApplyAnimationProperties()
extern "C" void LayoutElement_OnDidApplyAnimationProperties_m2938 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnBeforeTransformParentChanged()
extern "C" void LayoutElement_OnBeforeTransformParentChanged_m2939 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m2940(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::SetDirty()
extern TypeInfo* RectTransform_t364_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern "C" void LayoutElement_SetDirty_m2940 (LayoutElement_t611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(687);
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Transform_t1 * L_1 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m2978(NULL /*static, unused*/, ((RectTransform_t364 *)IsInst(L_1, RectTransform_t364_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.RectTransform/Edge
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
struct LayoutGroup_t608;
struct RectOffset_t404;
struct LayoutGroup_t608;
struct Object_t;
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Object>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Object>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisObject_t_m3441_gshared (LayoutGroup_t608 * __this, Object_t ** p0, Object_t * p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisObject_t_m3441(__this, p0, p1, method) (( void (*) (LayoutGroup_t608 *, Object_t **, Object_t *, const MethodInfo*))LayoutGroup_SetProperty_TisObject_t_m3441_gshared)(__this, p0, p1, method)
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.RectOffset>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.RectOffset>(!!0&,!!0)
#define LayoutGroup_SetProperty_TisRectOffset_t404_m3440(__this, p0, p1, method) (( void (*) (LayoutGroup_t608 *, RectOffset_t404 **, RectOffset_t404 *, const MethodInfo*))LayoutGroup_SetProperty_TisObject_t_m3441_gshared)(__this, p0, p1, method)
struct LayoutGroup_t608;
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.TextAnchor>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.TextAnchor>(!!0&,!!0)
#define LayoutGroup_SetProperty_TisTextAnchor_t701_m3442(__this, p0, p1, method) (( void (*) (LayoutGroup_t608 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisInt32_t253_m3428_gshared)(__this, p0, p1, method)
struct Component_t219;
struct RectTransform_t364;
struct Component_t219;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m614_gshared (Component_t219 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m614(__this, method) (( Object_t * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t364_m1776(__this, method) (( RectTransform_t364 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)


// System.Void UnityEngine.UI.LayoutGroup::.ctor()
extern TypeInfo* RectOffset_t404_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t612_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3444_MethodInfo_var;
extern "C" void LayoutGroup__ctor_m2941 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t404_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(649);
		List_1_t612_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1144);
		List_1__ctor_m3444_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t404 * L_0 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m3443(L_0, /*hidden argument*/NULL);
		__this->___m_Padding_2 = L_0;
		Vector2_t6  L_1 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_TotalMinSize_6 = L_1;
		Vector2_t6  L_2 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_TotalPreferredSize_7 = L_2;
		Vector2_t6  L_3 = Vector2_get_zero_m1598(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_TotalFlexibleSize_8 = L_3;
		List_1_t612 * L_4 = (List_1_t612 *)il2cpp_codegen_object_new (List_1_t612_il2cpp_TypeInfo_var);
		List_1__ctor_m3444(L_4, /*hidden argument*/List_1__ctor_m3444_MethodInfo_var);
		__this->___m_RectChildren_9 = L_4;
		UIBehaviour__ctor_m1974(__this, /*hidden argument*/NULL);
		RectOffset_t404 * L_5 = (__this->___m_Padding_2);
		if (L_5)
		{
			goto IL_0053;
		}
	}
	{
		RectOffset_t404 * L_6 = (RectOffset_t404 *)il2cpp_codegen_object_new (RectOffset_t404_il2cpp_TypeInfo_var);
		RectOffset__ctor_m3443(L_6, /*hidden argument*/NULL);
		__this->___m_Padding_2 = L_6;
	}

IL_0053:
	{
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::get_padding()
extern "C" RectOffset_t404 * LayoutGroup_get_padding_m2942 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		RectOffset_t404 * L_0 = (__this->___m_Padding_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::set_padding(UnityEngine.RectOffset)
extern const MethodInfo* LayoutGroup_SetProperty_TisRectOffset_t404_m3440_MethodInfo_var;
extern "C" void LayoutGroup_set_padding_m2943 (LayoutGroup_t608 * __this, RectOffset_t404 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisRectOffset_t404_m3440_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t404 ** L_0 = &(__this->___m_Padding_2);
		RectOffset_t404 * L_1 = ___value;
		LayoutGroup_SetProperty_TisRectOffset_t404_m3440(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisRectOffset_t404_m3440_MethodInfo_var);
		return;
	}
}
// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::get_childAlignment()
extern "C" int32_t LayoutGroup_get_childAlignment_m2944 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_ChildAlignment_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::set_childAlignment(UnityEngine.TextAnchor)
extern const MethodInfo* LayoutGroup_SetProperty_TisTextAnchor_t701_m3442_MethodInfo_var;
extern "C" void LayoutGroup_set_childAlignment_m2945 (LayoutGroup_t608 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisTextAnchor_t701_m3442_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483942);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_ChildAlignment_3);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisTextAnchor_t701_m3442(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisTextAnchor_t701_m3442_MethodInfo_var);
		return;
	}
}
// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::get_rectTransform()
extern const MethodInfo* Component_GetComponent_TisRectTransform_t364_m1776_MethodInfo_var;
extern "C" RectTransform_t364 * LayoutGroup_get_rectTransform_m2946 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRectTransform_t364_m1776_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483718);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t364 * L_0 = (__this->___m_Rect_4);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t364 * L_2 = Component_GetComponent_TisRectTransform_t364_m1776(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t364_m1776_MethodInfo_var);
		__this->___m_Rect_4 = L_2;
	}

IL_001d:
	{
		RectTransform_t364 * L_3 = (__this->___m_Rect_4);
		return L_3;
	}
}
// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::get_rectChildren()
extern "C" List_1_t612 * LayoutGroup_get_rectChildren_m2947 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		List_1_t612 * L_0 = (__this->___m_RectChildren_9);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputHorizontal()
extern const Il2CppType* ILayoutIgnorer_t702_0_0_0_var;
extern TypeInfo* RectTransform_t364_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ILayoutIgnorer_t702_il2cpp_TypeInfo_var;
extern "C" void LayoutGroup_CalculateLayoutInputHorizontal_m2948 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutIgnorer_t702_0_0_0_var = il2cpp_codegen_type_from_index(1146);
		RectTransform_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(687);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		ILayoutIgnorer_t702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1146);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t364 * V_1 = {0};
	Object_t * V_2 = {0};
	{
		List_1_t612 * L_0 = (__this->___m_RectChildren_9);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.RectTransform>::Clear() */, L_0);
		V_0 = 0;
		goto IL_007c;
	}

IL_0012:
	{
		RectTransform_t364 * L_1 = LayoutGroup_get_rectTransform_m2946(__this, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Transform_t1 * L_3 = Transform_GetChild_m1774(L_1, L_2, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t364 *)IsInst(L_3, RectTransform_t364_il2cpp_TypeInfo_var));
		RectTransform_t364 * L_4 = V_1;
		bool L_5 = Object_op_Equality_m640(NULL /*static, unused*/, L_4, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		goto IL_0078;
	}

IL_0035:
	{
		RectTransform_t364 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m705(NULL /*static, unused*/, LoadTypeToken(ILayoutIgnorer_t702_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		Component_t219 * L_8 = Component_GetComponent_m3445(L_6, L_7, /*hidden argument*/NULL);
		V_2 = ((Object_t *)IsInst(L_8, ILayoutIgnorer_t702_il2cpp_TypeInfo_var));
		RectTransform_t364 * L_9 = V_1;
		NullCheck(L_9);
		GameObject_t78 * L_10 = Component_get_gameObject_m622(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = GameObject_get_activeInHierarchy_m778(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0078;
		}
	}
	{
		Object_t * L_12 = V_2;
		if (!L_12)
		{
			goto IL_006c;
		}
	}
	{
		Object_t * L_13 = V_2;
		NullCheck(L_13);
		bool L_14 = (bool)InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.UI.ILayoutIgnorer::get_ignoreLayout() */, ILayoutIgnorer_t702_il2cpp_TypeInfo_var, L_13);
		if (L_14)
		{
			goto IL_0078;
		}
	}

IL_006c:
	{
		List_1_t612 * L_15 = (__this->___m_RectChildren_9);
		RectTransform_t364 * L_16 = V_1;
		NullCheck(L_15);
		VirtActionInvoker1< RectTransform_t364 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.RectTransform>::Add(!0) */, L_15, L_16);
	}

IL_0078:
	{
		int32_t L_17 = V_0;
		V_0 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_007c:
	{
		int32_t L_18 = V_0;
		RectTransform_t364 * L_19 = LayoutGroup_get_rectTransform_m2946(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = Transform_get_childCount_m3446(L_19, /*hidden argument*/NULL);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_0012;
		}
	}
	{
		DrivenRectTransformTracker_t571 * L_21 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m3350(L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputVertical()
// System.Single UnityEngine.UI.LayoutGroup::get_minWidth()
extern "C" float LayoutGroup_get_minWidth_m2949 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalMinSize_m2959(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_preferredWidth()
extern "C" float LayoutGroup_get_preferredWidth_m2950 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalPreferredSize_m2960(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleWidth()
extern "C" float LayoutGroup_get_flexibleWidth_m2951 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalFlexibleSize_m2961(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_minHeight()
extern "C" float LayoutGroup_get_minHeight_m2952 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalMinSize_m2959(__this, 1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_preferredHeight()
extern "C" float LayoutGroup_get_preferredHeight_m2953 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalPreferredSize_m2960(__this, 1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleHeight()
extern "C" float LayoutGroup_get_flexibleHeight_m2954 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalFlexibleSize_m2961(__this, 1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.UI.LayoutGroup::get_layoutPriority()
extern "C" int32_t LayoutGroup_get_layoutPriority_m2955 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutHorizontal()
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutVertical()
// System.Void UnityEngine.UI.LayoutGroup::OnEnable()
extern "C" void LayoutGroup_OnEnable_m2956 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m1976(__this, /*hidden argument*/NULL);
		LayoutGroup_SetDirty_m2968(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnDisable()
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern "C" void LayoutGroup_OnDisable_m2957 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	{
		DrivenRectTransformTracker_t571 * L_0 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m3350(L_0, /*hidden argument*/NULL);
		RectTransform_t364 * L_1 = LayoutGroup_get_rectTransform_m2946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m2978(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m1978(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnDidApplyAnimationProperties()
extern "C" void LayoutGroup_OnDidApplyAnimationProperties_m2958 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_SetDirty_m2968(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetTotalMinSize(System.Int32)
extern "C" float LayoutGroup_GetTotalMinSize_m2959 (LayoutGroup_t608 * __this, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t6 * L_0 = &(__this->___m_TotalMinSize_6);
		int32_t L_1 = ___axis;
		float L_2 = Vector2_get_Item_m3248(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetTotalPreferredSize(System.Int32)
extern "C" float LayoutGroup_GetTotalPreferredSize_m2960 (LayoutGroup_t608 * __this, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t6 * L_0 = &(__this->___m_TotalPreferredSize_7);
		int32_t L_1 = ___axis;
		float L_2 = Vector2_get_Item_m3248(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetTotalFlexibleSize(System.Int32)
extern "C" float LayoutGroup_GetTotalFlexibleSize_m2961 (LayoutGroup_t608 * __this, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t6 * L_0 = &(__this->___m_TotalFlexibleSize_8);
		int32_t L_1 = ___axis;
		float L_2 = Vector2_get_Item_m3248(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetStartOffset(System.Int32,System.Single)
extern "C" float LayoutGroup_GetStartOffset_m2962 (LayoutGroup_t608 * __this, int32_t ___axis, float ___requiredSpaceWithoutPadding, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Rect_t304  V_4 = {0};
	Vector2_t6  V_5 = {0};
	float G_B2_0 = 0.0f;
	float G_B1_0 = 0.0f;
	int32_t G_B3_0 = 0;
	float G_B3_1 = 0.0f;
	int32_t G_B9_0 = 0;
	{
		float L_0 = ___requiredSpaceWithoutPadding;
		int32_t L_1 = ___axis;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0017;
		}
	}
	{
		RectOffset_t404 * L_2 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = RectOffset_get_horizontal_m3432(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_0022;
	}

IL_0017:
	{
		RectOffset_t404 * L_4 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_vertical_m3433(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_0022:
	{
		V_0 = ((float)((float)G_B3_1+(float)(((float)G_B3_0))));
		RectTransform_t364 * L_6 = LayoutGroup_get_rectTransform_m2946(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Rect_t304  L_7 = RectTransform_get_rect_m3184(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		Vector2_t6  L_8 = Rect_get_size_m3243((&V_4), /*hidden argument*/NULL);
		V_5 = L_8;
		int32_t L_9 = ___axis;
		float L_10 = Vector2_get_Item_m3248((&V_5), L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = V_1;
		float L_12 = V_0;
		V_2 = ((float)((float)L_11-(float)L_12));
		V_3 = (0.0f);
		int32_t L_13 = ___axis;
		if (L_13)
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_14 = LayoutGroup_get_childAlignment_m2944(__this, /*hidden argument*/NULL);
		V_3 = ((float)((float)(((float)((int32_t)((int32_t)L_14%(int32_t)3))))*(float)(0.5f)));
		goto IL_0079;
	}

IL_0069:
	{
		int32_t L_15 = LayoutGroup_get_childAlignment_m2944(__this, /*hidden argument*/NULL);
		V_3 = ((float)((float)(((float)((int32_t)((int32_t)L_15/(int32_t)3))))*(float)(0.5f)));
	}

IL_0079:
	{
		int32_t L_16 = ___axis;
		if (L_16)
		{
			goto IL_008f;
		}
	}
	{
		RectOffset_t404 * L_17 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		int32_t L_18 = RectOffset_get_left_m3438(L_17, /*hidden argument*/NULL);
		G_B9_0 = L_18;
		goto IL_009a;
	}

IL_008f:
	{
		RectOffset_t404 * L_19 = LayoutGroup_get_padding_m2942(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_top_m3439(L_19, /*hidden argument*/NULL);
		G_B9_0 = L_20;
	}

IL_009a:
	{
		float L_21 = V_2;
		float L_22 = V_3;
		return ((float)((float)(((float)G_B9_0))+(float)((float)((float)L_21*(float)L_22))));
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutInputForAxis(System.Single,System.Single,System.Single,System.Int32)
extern "C" void LayoutGroup_SetLayoutInputForAxis_m2963 (LayoutGroup_t608 * __this, float ___totalMin, float ___totalPreferred, float ___totalFlexible, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t6 * L_0 = &(__this->___m_TotalMinSize_6);
		int32_t L_1 = ___axis;
		float L_2 = ___totalMin;
		Vector2_set_Item_m3257(L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t6 * L_3 = &(__this->___m_TotalPreferredSize_7);
		int32_t L_4 = ___axis;
		float L_5 = ___totalPreferred;
		Vector2_set_Item_m3257(L_3, L_4, L_5, /*hidden argument*/NULL);
		Vector2_t6 * L_6 = &(__this->___m_TotalFlexibleSize_8);
		int32_t L_7 = ___axis;
		float L_8 = ___totalFlexible;
		Vector2_set_Item_m3257(L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetChildAlongAxis(UnityEngine.RectTransform,System.Int32,System.Single,System.Single)
extern "C" void LayoutGroup_SetChildAlongAxis_m2964 (LayoutGroup_t608 * __this, RectTransform_t364 * ___rect, int32_t ___axis, float ___pos, float ___size, const MethodInfo* method)
{
	RectTransform_t364 * G_B4_0 = {0};
	RectTransform_t364 * G_B3_0 = {0};
	int32_t G_B5_0 = 0;
	RectTransform_t364 * G_B5_1 = {0};
	{
		RectTransform_t364 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		DrivenRectTransformTracker_t571 * L_2 = &(__this->___m_Tracker_5);
		RectTransform_t364 * L_3 = ___rect;
		DrivenRectTransformTracker_Add_m3352(L_2, __this, L_3, ((int32_t)16134), /*hidden argument*/NULL);
		RectTransform_t364 * L_4 = ___rect;
		int32_t L_5 = ___axis;
		G_B3_0 = L_4;
		if (L_5)
		{
			G_B4_0 = L_4;
			goto IL_002c;
		}
	}
	{
		G_B5_0 = 0;
		G_B5_1 = G_B3_0;
		goto IL_002d;
	}

IL_002c:
	{
		G_B5_0 = 2;
		G_B5_1 = G_B4_0;
	}

IL_002d:
	{
		float L_6 = ___pos;
		float L_7 = ___size;
		NullCheck(G_B5_1);
		RectTransform_SetInsetAndSizeFromParentEdge_m3447(G_B5_1, G_B5_0, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutGroup::get_isRootLayoutGroup()
extern const Il2CppType* ILayoutGroup_t703_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" bool LayoutGroup_get_isRootLayoutGroup_m2965 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutGroup_t703_0_0_0_var = il2cpp_codegen_type_from_index(1147);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1 * V_0 = {0};
	{
		Transform_t1 * L_0 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1 * L_1 = Transform_get_parent_m676(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t1 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m640(NULL /*static, unused*/, L_2, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		return 1;
	}

IL_001a:
	{
		Transform_t1 * L_4 = Component_get_transform_m594(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1 * L_5 = Transform_get_parent_m676(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m705(NULL /*static, unused*/, LoadTypeToken(ILayoutGroup_t703_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		Component_t219 * L_7 = Component_GetComponent_m3445(L_5, L_6, /*hidden argument*/NULL);
		bool L_8 = Object_op_Equality_m640(NULL /*static, unused*/, L_7, (Object_t164 *)NULL, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnRectTransformDimensionsChange()
extern "C" void LayoutGroup_OnRectTransformDimensionsChange_m2966 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnRectTransformDimensionsChange_m1981(__this, /*hidden argument*/NULL);
		bool L_0 = LayoutGroup_get_isRootLayoutGroup_m2965(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		LayoutGroup_SetDirty_m2968(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnTransformChildrenChanged()
extern "C" void LayoutGroup_OnTransformChildrenChanged_m2967 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_SetDirty_m2968(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetDirty()
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern "C" void LayoutGroup_SetDirty_m2968 (LayoutGroup_t608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		RectTransform_t364 * L_1 = LayoutGroup_get_rectTransform_m2946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m2978(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertie.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UI.CanvasUpdate
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3.h"
// System.Collections.Generic.List`1<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_gen_16.h"
// System.Predicate`1<UnityEngine.Component>
#include "mscorlib_System_Predicate_1_gen_1.h"
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertieMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3MethodDeclarations.h"
// System.Predicate`1<UnityEngine.Component>
#include "mscorlib_System_Predicate_1_gen_1MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_gen_16MethodDeclarations.h"
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPoolMethodDeclarations.h"
// UnityEngine.UI.CanvasUpdateRegistry
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistryMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"


// System.Void UnityEngine.UI.LayoutRebuilder::.ctor(UnityEngine.RectTransform)
extern "C" void LayoutRebuilder__ctor_m2969 (LayoutRebuilder_t615 * __this, RectTransform_t364 * ___controller, const MethodInfo* method)
{
	{
		RectTransform_t364 * L_0 = ___controller;
		__this->___m_ToRebuild_0 = L_0;
		RectTransform_t364 * L_1 = (__this->___m_ToRebuild_0);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 UnityEngine.Object::GetHashCode() */, L_1);
		__this->___m_CachedHashFromTransform_1 = L_2;
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::.cctor()
extern TypeInfo* ReapplyDrivenProperties_t704_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutRebuilder_ReapplyDrivenProperties_m2972_MethodInfo_var;
extern "C" void LayoutRebuilder__cctor_m2970 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReapplyDrivenProperties_t704_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1148);
		LayoutRebuilder_ReapplyDrivenProperties_m2972_MethodInfo_var = il2cpp_codegen_method_info_from_index(295);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { (void*)LayoutRebuilder_ReapplyDrivenProperties_m2972_MethodInfo_var };
		ReapplyDrivenProperties_t704 * L_1 = (ReapplyDrivenProperties_t704 *)il2cpp_codegen_object_new (ReapplyDrivenProperties_t704_il2cpp_TypeInfo_var);
		ReapplyDrivenProperties__ctor_m3448(L_1, NULL, L_0, /*hidden argument*/NULL);
		RectTransform_add_reapplyDrivenProperties_m3449(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::UnityEngine.UI.ICanvasElement.Rebuild(UnityEngine.UI.CanvasUpdate)
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAction_1_t613_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__9_m2985_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3450_MethodInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__A_m2986_MethodInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__B_m2987_MethodInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__C_m2988_MethodInfo_var;
extern "C" void LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m2971 (LayoutRebuilder_t615 * __this, int32_t ___executing, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		UnityAction_1_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1149);
		LayoutRebuilder_U3CRebuildU3Em__9_m2985_MethodInfo_var = il2cpp_codegen_method_info_from_index(296);
		UnityAction_1__ctor_m3450_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483945);
		LayoutRebuilder_U3CRebuildU3Em__A_m2986_MethodInfo_var = il2cpp_codegen_method_info_from_index(298);
		LayoutRebuilder_U3CRebuildU3Em__B_m2987_MethodInfo_var = il2cpp_codegen_method_info_from_index(299);
		LayoutRebuilder_U3CRebuildU3Em__C_m2988_MethodInfo_var = il2cpp_codegen_method_info_from_index(300);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	RectTransform_t364 * G_B4_0 = {0};
	LayoutRebuilder_t615 * G_B4_1 = {0};
	RectTransform_t364 * G_B3_0 = {0};
	LayoutRebuilder_t615 * G_B3_1 = {0};
	RectTransform_t364 * G_B6_0 = {0};
	LayoutRebuilder_t615 * G_B6_1 = {0};
	RectTransform_t364 * G_B5_0 = {0};
	LayoutRebuilder_t615 * G_B5_1 = {0};
	RectTransform_t364 * G_B8_0 = {0};
	LayoutRebuilder_t615 * G_B8_1 = {0};
	RectTransform_t364 * G_B7_0 = {0};
	LayoutRebuilder_t615 * G_B7_1 = {0};
	RectTransform_t364 * G_B10_0 = {0};
	LayoutRebuilder_t615 * G_B10_1 = {0};
	RectTransform_t364 * G_B9_0 = {0};
	LayoutRebuilder_t615 * G_B9_1 = {0};
	{
		int32_t L_0 = ___executing;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		goto IL_00b7;
	}

IL_000e:
	{
		RectTransform_t364 * L_2 = (__this->___m_ToRebuild_0);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		UnityAction_1_t613 * L_3 = ((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2;
		G_B3_0 = L_2;
		G_B3_1 = __this;
		if (L_3)
		{
			G_B4_0 = L_2;
			G_B4_1 = __this;
			goto IL_002d;
		}
	}
	{
		IntPtr_t L_4 = { (void*)LayoutRebuilder_U3CRebuildU3Em__9_m2985_MethodInfo_var };
		UnityAction_1_t613 * L_5 = (UnityAction_1_t613 *)il2cpp_codegen_object_new (UnityAction_1_t613_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3450(L_5, NULL, L_4, /*hidden argument*/UnityAction_1__ctor_m3450_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2 = L_5;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		UnityAction_1_t613 * L_6 = ((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2;
		LayoutRebuilder_PerformLayoutCalculation_m2977(G_B4_1, G_B4_0, L_6, /*hidden argument*/NULL);
		RectTransform_t364 * L_7 = (__this->___m_ToRebuild_0);
		UnityAction_1_t613 * L_8 = ((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		G_B5_0 = L_7;
		G_B5_1 = __this;
		if (L_8)
		{
			G_B6_0 = L_7;
			G_B6_1 = __this;
			goto IL_0056;
		}
	}
	{
		IntPtr_t L_9 = { (void*)LayoutRebuilder_U3CRebuildU3Em__A_m2986_MethodInfo_var };
		UnityAction_1_t613 * L_10 = (UnityAction_1_t613 *)il2cpp_codegen_object_new (UnityAction_1_t613_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3450(L_10, NULL, L_9, /*hidden argument*/UnityAction_1__ctor_m3450_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3 = L_10;
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		UnityAction_1_t613 * L_11 = ((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		LayoutRebuilder_PerformLayoutControl_m2976(G_B6_1, G_B6_0, L_11, /*hidden argument*/NULL);
		RectTransform_t364 * L_12 = (__this->___m_ToRebuild_0);
		UnityAction_1_t613 * L_13 = ((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		G_B7_0 = L_12;
		G_B7_1 = __this;
		if (L_13)
		{
			G_B8_0 = L_12;
			G_B8_1 = __this;
			goto IL_007f;
		}
	}
	{
		IntPtr_t L_14 = { (void*)LayoutRebuilder_U3CRebuildU3Em__B_m2987_MethodInfo_var };
		UnityAction_1_t613 * L_15 = (UnityAction_1_t613 *)il2cpp_codegen_object_new (UnityAction_1_t613_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3450(L_15, NULL, L_14, /*hidden argument*/UnityAction_1__ctor_m3450_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4 = L_15;
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
	}

IL_007f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		UnityAction_1_t613 * L_16 = ((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		LayoutRebuilder_PerformLayoutCalculation_m2977(G_B8_1, G_B8_0, L_16, /*hidden argument*/NULL);
		RectTransform_t364 * L_17 = (__this->___m_ToRebuild_0);
		UnityAction_1_t613 * L_18 = ((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		G_B9_0 = L_17;
		G_B9_1 = __this;
		if (L_18)
		{
			G_B10_0 = L_17;
			G_B10_1 = __this;
			goto IL_00a8;
		}
	}
	{
		IntPtr_t L_19 = { (void*)LayoutRebuilder_U3CRebuildU3Em__C_m2988_MethodInfo_var };
		UnityAction_1_t613 * L_20 = (UnityAction_1_t613 *)il2cpp_codegen_object_new (UnityAction_1_t613_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3450(L_20, NULL, L_19, /*hidden argument*/UnityAction_1__ctor_m3450_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5 = L_20;
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		UnityAction_1_t613 * L_21 = ((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		LayoutRebuilder_PerformLayoutControl_m2976(G_B10_1, G_B10_0, L_21, /*hidden argument*/NULL);
		goto IL_00b7;
	}

IL_00b7:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::ReapplyDrivenProperties(UnityEngine.RectTransform)
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_ReapplyDrivenProperties_m2972 (Object_t * __this /* static, unused */, RectTransform_t364 * ___driven, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t364 * L_0 = ___driven;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m2978(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.UI.LayoutRebuilder::get_transform()
extern "C" Transform_t1 * LayoutRebuilder_get_transform_m2973 (LayoutRebuilder_t615 * __this, const MethodInfo* method)
{
	{
		RectTransform_t364 * L_0 = (__this->___m_ToRebuild_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::IsDestroyed()
extern "C" bool LayoutRebuilder_IsDestroyed_m2974 (LayoutRebuilder_t615 * __this, const MethodInfo* method)
{
	{
		RectTransform_t364 * L_0 = (__this->___m_ToRebuild_0);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::StripDisabledBehavioursFromList(System.Collections.Generic.List`1<UnityEngine.Component>)
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t614_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m2989_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m3451_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAll_m3452_MethodInfo_var;
extern "C" void LayoutRebuilder_StripDisabledBehavioursFromList_m2975 (Object_t * __this /* static, unused */, List_1_t651 * ___components, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		Predicate_1_t614_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1150);
		LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m2989_MethodInfo_var = il2cpp_codegen_method_info_from_index(301);
		Predicate_1__ctor_m3451_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483950);
		List_1_RemoveAll_m3452_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483951);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t651 * G_B2_0 = {0};
	List_1_t651 * G_B1_0 = {0};
	{
		List_1_t651 * L_0 = ___components;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		Predicate_1_t614 * L_1 = ((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m2989_MethodInfo_var };
		Predicate_1_t614 * L_3 = (Predicate_1_t614 *)il2cpp_codegen_object_new (Predicate_1_t614_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m3451(L_3, NULL, L_2, /*hidden argument*/Predicate_1__ctor_m3451_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		Predicate_1_t614 * L_4 = ((LayoutRebuilder_t615_StaticFields*)LayoutRebuilder_t615_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		NullCheck(G_B2_0);
		List_1_RemoveAll_m3452(G_B2_0, L_4, /*hidden argument*/List_1_RemoveAll_m3452_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutControl(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const Il2CppType* ILayoutController_t705_0_0_0_var;
extern TypeInfo* ComponentListPool_t625_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern TypeInfo* ILayoutSelfController_t706_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransform_t364_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_PerformLayoutControl_m2976 (LayoutRebuilder_t615 * __this, RectTransform_t364 * ___rect, UnityAction_1_t613 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t705_0_0_0_var = il2cpp_codegen_type_from_index(1151);
		ComponentListPool_t625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ILayoutSelfController_t706_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1152);
		RectTransform_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(687);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t651 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		RectTransform_t364 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		List_1_t651 * L_2 = ComponentListPool_Get_m3032(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		RectTransform_t364 * L_3 = ___rect;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m705(NULL /*static, unused*/, LoadTypeToken(ILayoutController_t705_0_0_0_var), /*hidden argument*/NULL);
		List_1_t651 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m3180(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t651 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m2975(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t651 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_00ca;
		}
	}
	{
		V_1 = 0;
		goto IL_005f;
	}

IL_003d:
	{
		List_1_t651 * L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Component_t219 * L_11 = (Component_t219 *)VirtFuncInvoker1< Component_t219 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_9, L_10);
		if (!((Object_t *)IsInst(L_11, ILayoutSelfController_t706_il2cpp_TypeInfo_var)))
		{
			goto IL_005b;
		}
	}
	{
		UnityAction_1_t613 * L_12 = ___action;
		List_1_t651 * L_13 = V_0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		Component_t219 * L_15 = (Component_t219 *)VirtFuncInvoker1< Component_t219 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_13, L_14);
		NullCheck(L_12);
		VirtActionInvoker1< Component_t219 * >::Invoke(10 /* System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Component>::Invoke(!0) */, L_12, L_15);
	}

IL_005b:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005f:
	{
		int32_t L_17 = V_1;
		List_1_t651 * L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_18);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_003d;
		}
	}
	{
		V_2 = 0;
		goto IL_0094;
	}

IL_0072:
	{
		List_1_t651 * L_20 = V_0;
		int32_t L_21 = V_2;
		NullCheck(L_20);
		Component_t219 * L_22 = (Component_t219 *)VirtFuncInvoker1< Component_t219 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_20, L_21);
		if (((Object_t *)IsInst(L_22, ILayoutSelfController_t706_il2cpp_TypeInfo_var)))
		{
			goto IL_0090;
		}
	}
	{
		UnityAction_1_t613 * L_23 = ___action;
		List_1_t651 * L_24 = V_0;
		int32_t L_25 = V_2;
		NullCheck(L_24);
		Component_t219 * L_26 = (Component_t219 *)VirtFuncInvoker1< Component_t219 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_24, L_25);
		NullCheck(L_23);
		VirtActionInvoker1< Component_t219 * >::Invoke(10 /* System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Component>::Invoke(!0) */, L_23, L_26);
	}

IL_0090:
	{
		int32_t L_27 = V_2;
		V_2 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0094:
	{
		int32_t L_28 = V_2;
		List_1_t651 * L_29 = V_0;
		NullCheck(L_29);
		int32_t L_30 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_29);
		if ((((int32_t)L_28) < ((int32_t)L_30)))
		{
			goto IL_0072;
		}
	}
	{
		V_3 = 0;
		goto IL_00be;
	}

IL_00a7:
	{
		RectTransform_t364 * L_31 = ___rect;
		int32_t L_32 = V_3;
		NullCheck(L_31);
		Transform_t1 * L_33 = Transform_GetChild_m1774(L_31, L_32, /*hidden argument*/NULL);
		UnityAction_1_t613 * L_34 = ___action;
		LayoutRebuilder_PerformLayoutControl_m2976(__this, ((RectTransform_t364 *)IsInst(L_33, RectTransform_t364_il2cpp_TypeInfo_var)), L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_3;
		V_3 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00be:
	{
		int32_t L_36 = V_3;
		RectTransform_t364 * L_37 = ___rect;
		NullCheck(L_37);
		int32_t L_38 = Transform_get_childCount_m3446(L_37, /*hidden argument*/NULL);
		if ((((int32_t)L_36) < ((int32_t)L_38)))
		{
			goto IL_00a7;
		}
	}

IL_00ca:
	{
		List_1_t651 * L_39 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		ComponentListPool_Release_m3033(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutCalculation(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const Il2CppType* ILayoutElement_t652_0_0_0_var;
extern TypeInfo* ComponentListPool_t625_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransform_t364_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_PerformLayoutCalculation_m2977 (LayoutRebuilder_t615 * __this, RectTransform_t364 * ___rect, UnityAction_1_t613 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_0_0_0_var = il2cpp_codegen_type_from_index(1153);
		ComponentListPool_t625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		RectTransform_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(687);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t651 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		RectTransform_t364 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		List_1_t651 * L_2 = ComponentListPool_Get_m3032(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		RectTransform_t364 * L_3 = ___rect;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m705(NULL /*static, unused*/, LoadTypeToken(ILayoutElement_t652_0_0_0_var), /*hidden argument*/NULL);
		List_1_t651 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m3180(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t651 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m2975(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t651 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0084;
		}
	}
	{
		V_1 = 0;
		goto IL_0054;
	}

IL_003d:
	{
		RectTransform_t364 * L_9 = ___rect;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Transform_t1 * L_11 = Transform_GetChild_m1774(L_9, L_10, /*hidden argument*/NULL);
		UnityAction_1_t613 * L_12 = ___action;
		LayoutRebuilder_PerformLayoutCalculation_m2977(__this, ((RectTransform_t364 *)IsInst(L_11, RectTransform_t364_il2cpp_TypeInfo_var)), L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_14 = V_1;
		RectTransform_t364 * L_15 = ___rect;
		NullCheck(L_15);
		int32_t L_16 = Transform_get_childCount_m3446(L_15, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_003d;
		}
	}
	{
		V_2 = 0;
		goto IL_0078;
	}

IL_0067:
	{
		UnityAction_1_t613 * L_17 = ___action;
		List_1_t651 * L_18 = V_0;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		Component_t219 * L_20 = (Component_t219 *)VirtFuncInvoker1< Component_t219 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_18, L_19);
		NullCheck(L_17);
		VirtActionInvoker1< Component_t219 * >::Invoke(10 /* System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Component>::Invoke(!0) */, L_17, L_20);
		int32_t L_21 = V_2;
		V_2 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_22 = V_2;
		List_1_t651 * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_23);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_0067;
		}
	}

IL_0084:
	{
		List_1_t651 * L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		ComponentListPool_Release_m3033(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutForRebuild(UnityEngine.RectTransform)
extern TypeInfo* RectTransform_t364_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_MarkLayoutForRebuild_m2978 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(687);
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t364 * V_0 = {0};
	RectTransform_t364 * V_1 = {0};
	{
		RectTransform_t364 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		RectTransform_t364 * L_2 = ___rect;
		V_0 = L_2;
	}

IL_000f:
	{
		RectTransform_t364 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t1 * L_4 = Transform_get_parent_m676(L_3, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t364 *)IsInst(L_4, RectTransform_t364_il2cpp_TypeInfo_var));
		RectTransform_t364 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		bool L_6 = LayoutRebuilder_ValidLayoutGroup_m2979(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_0032;
	}

IL_002b:
	{
		RectTransform_t364 * L_7 = V_1;
		V_0 = L_7;
		goto IL_000f;
	}

IL_0032:
	{
		RectTransform_t364 * L_8 = V_0;
		RectTransform_t364 * L_9 = ___rect;
		bool L_10 = Object_op_Equality_m640(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004a;
		}
	}
	{
		RectTransform_t364 * L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		bool L_12 = LayoutRebuilder_ValidController_m2980(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_004a;
		}
	}
	{
		return;
	}

IL_004a:
	{
		RectTransform_t364 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutRootForRebuild_m2981(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidLayoutGroup(UnityEngine.RectTransform)
extern const Il2CppType* ILayoutGroup_t703_0_0_0_var;
extern TypeInfo* ComponentListPool_t625_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern "C" bool LayoutRebuilder_ValidLayoutGroup_m2979 (Object_t * __this /* static, unused */, RectTransform_t364 * ___parent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutGroup_t703_0_0_0_var = il2cpp_codegen_type_from_index(1147);
		ComponentListPool_t625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t651 * V_0 = {0};
	bool V_1 = false;
	{
		RectTransform_t364 * L_0 = ___parent;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		List_1_t651 * L_2 = ComponentListPool_Get_m3032(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		RectTransform_t364 * L_3 = ___parent;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m705(NULL /*static, unused*/, LoadTypeToken(ILayoutGroup_t703_0_0_0_var), /*hidden argument*/NULL);
		List_1_t651 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m3180(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t651 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m2975(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t651 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		V_1 = ((((int32_t)L_8) > ((int32_t)0))? 1 : 0);
		List_1_t651 * L_9 = V_0;
		ComponentListPool_Release_m3033(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidController(UnityEngine.RectTransform)
extern const Il2CppType* ILayoutController_t705_0_0_0_var;
extern TypeInfo* ComponentListPool_t625_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern "C" bool LayoutRebuilder_ValidController_m2980 (Object_t * __this /* static, unused */, RectTransform_t364 * ___layoutRoot, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t705_0_0_0_var = il2cpp_codegen_type_from_index(1151);
		ComponentListPool_t625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t651 * V_0 = {0};
	bool V_1 = false;
	{
		RectTransform_t364 * L_0 = ___layoutRoot;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		List_1_t651 * L_2 = ComponentListPool_Get_m3032(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		RectTransform_t364 * L_3 = ___layoutRoot;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m705(NULL /*static, unused*/, LoadTypeToken(ILayoutController_t705_0_0_0_var), /*hidden argument*/NULL);
		List_1_t651 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m3180(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t651 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t615_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m2975(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t651 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		V_1 = ((((int32_t)L_8) > ((int32_t)0))? 1 : 0);
		List_1_t651 * L_9 = V_0;
		ComponentListPool_Release_m3033(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutRootForRebuild(UnityEngine.RectTransform)
extern TypeInfo* LayoutRebuilder_t615_il2cpp_TypeInfo_var;
extern TypeInfo* CanvasUpdateRegistry_t512_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_MarkLayoutRootForRebuild_m2981 (Object_t * __this /* static, unused */, RectTransform_t364 * ___controller, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		CanvasUpdateRegistry_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1048);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t364 * L_0 = ___controller;
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		RectTransform_t364 * L_2 = ___controller;
		LayoutRebuilder_t615  L_3 = {0};
		LayoutRebuilder__ctor_m2969(&L_3, L_2, /*hidden argument*/NULL);
		LayoutRebuilder_t615  L_4 = L_3;
		Object_t * L_5 = Box(LayoutRebuilder_t615_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(CanvasUpdateRegistry_t512_il2cpp_TypeInfo_var);
		CanvasUpdateRegistry_RegisterCanvasElementForLayoutRebuild_m2188(NULL /*static, unused*/, (Object_t *)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::Equals(UnityEngine.UI.LayoutRebuilder)
extern "C" bool LayoutRebuilder_Equals_m2982 (LayoutRebuilder_t615 * __this, LayoutRebuilder_t615  ___other, const MethodInfo* method)
{
	{
		RectTransform_t364 * L_0 = (__this->___m_ToRebuild_0);
		RectTransform_t364 * L_1 = ((&___other)->___m_ToRebuild_0);
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.UI.LayoutRebuilder::GetHashCode()
extern "C" int32_t LayoutRebuilder_GetHashCode_m2983 (LayoutRebuilder_t615 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_CachedHashFromTransform_1);
		return L_0;
	}
}
// System.String UnityEngine.UI.LayoutRebuilder::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* LayoutRebuilder_ToString_m2984 (LayoutRebuilder_t615 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t364 * L_0 = (__this->___m_ToRebuild_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m954(NULL /*static, unused*/, (String_t*) &_stringLiteral440, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__9(UnityEngine.Component)
extern TypeInfo* ILayoutElement_t652_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__9_m2985 (Object_t * __this /* static, unused */, Component_t219 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t219 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutElement_t652_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputHorizontal() */, ILayoutElement_t652_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutElement_t652_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__A(UnityEngine.Component)
extern TypeInfo* ILayoutController_t705_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__A_m2986 (Object_t * __this /* static, unused */, Component_t219 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1151);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t219 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutController_t705_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.UI.ILayoutController::SetLayoutHorizontal() */, ILayoutController_t705_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutController_t705_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__B(UnityEngine.Component)
extern TypeInfo* ILayoutElement_t652_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__B_m2987 (Object_t * __this /* static, unused */, Component_t219 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t219 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutElement_t652_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(1 /* System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputVertical() */, ILayoutElement_t652_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutElement_t652_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__C(UnityEngine.Component)
extern TypeInfo* ILayoutController_t705_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__C_m2988 (Object_t * __this /* static, unused */, Component_t219 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1151);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t219 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutController_t705_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(1 /* System.Void UnityEngine.UI.ILayoutController::SetLayoutVertical() */, ILayoutController_t705_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutController_t705_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::<StripDisabledBehavioursFromList>m__D(UnityEngine.Component)
extern TypeInfo* Behaviour_t250_il2cpp_TypeInfo_var;
extern "C" bool LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m2989 (Object_t * __this /* static, unused */, Component_t219 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Behaviour_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Component_t219 * L_0 = ___e;
		if (!((Behaviour_t250 *)IsInst(L_0, Behaviour_t250_il2cpp_TypeInfo_var)))
		{
			goto IL_002e;
		}
	}
	{
		Component_t219 * L_1 = ___e;
		NullCheck(((Behaviour_t250 *)IsInst(L_1, Behaviour_t250_il2cpp_TypeInfo_var)));
		bool L_2 = Behaviour_get_enabled_m911(((Behaviour_t250 *)IsInst(L_1, Behaviour_t250_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		Component_t219 * L_3 = ___e;
		NullCheck(((Behaviour_t250 *)IsInst(L_3, Behaviour_t250_il2cpp_TypeInfo_var)));
		bool L_4 = Behaviour_get_isActiveAndEnabled_m1740(((Behaviour_t250 *)IsInst(L_3, Behaviour_t250_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B4_0 = 1;
	}

IL_002c:
	{
		G_B6_0 = G_B4_0;
		goto IL_002f;
	}

IL_002e:
	{
		G_B6_0 = 0;
	}

IL_002f:
	{
		return G_B6_0;
	}
}
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility.h"
#ifndef _MSC_VER
#else
#endif

// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
#include "System_Core_System_Func_2_gen_0.h"
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
#include "System_Core_System_Func_2_gen_0MethodDeclarations.h"


// System.Single UnityEngine.UI.LayoutUtility::GetMinSize(UnityEngine.RectTransform,System.Int32)
extern "C" float LayoutUtility_GetMinSize_m2990 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, int32_t ___axis, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		RectTransform_t364 * L_1 = ___rect;
		float L_2 = LayoutUtility_GetMinWidth_m2993(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000d:
	{
		RectTransform_t364 * L_3 = ___rect;
		float L_4 = LayoutUtility_GetMinHeight_m2996(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredSize(UnityEngine.RectTransform,System.Int32)
extern "C" float LayoutUtility_GetPreferredSize_m2991 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, int32_t ___axis, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		RectTransform_t364 * L_1 = ___rect;
		float L_2 = LayoutUtility_GetPreferredWidth_m2994(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000d:
	{
		RectTransform_t364 * L_3 = ___rect;
		float L_4 = LayoutUtility_GetPreferredHeight_m2997(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleSize(UnityEngine.RectTransform,System.Int32)
extern "C" float LayoutUtility_GetFlexibleSize_m2992 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, int32_t ___axis, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		RectTransform_t364 * L_1 = ___rect;
		float L_2 = LayoutUtility_GetFlexibleWidth_m2995(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000d:
	{
		RectTransform_t364 * L_3 = ___rect;
		float L_4 = LayoutUtility_GetFlexibleHeight_m2998(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetMinWidth(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t617_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t616_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetMinWidthU3Em__E_m3001_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3453_MethodInfo_var;
extern "C" float LayoutUtility_GetMinWidth_m2993 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1154);
		Func_2_t616_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1155);
		LayoutUtility_U3CGetMinWidthU3Em__E_m3001_MethodInfo_var = il2cpp_codegen_method_info_from_index(304);
		Func_2__ctor_m3453_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483953);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t364 * G_B2_0 = {0};
	RectTransform_t364 * G_B1_0 = {0};
	{
		RectTransform_t364 * L_0 = ___rect;
		Func_2_t616 * L_1 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_0;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetMinWidthU3Em__E_m3001_MethodInfo_var };
		Func_2_t616 * L_3 = (Func_2_t616 *)il2cpp_codegen_object_new (Func_2_t616_il2cpp_TypeInfo_var);
		Func_2__ctor_m3453(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3453_MethodInfo_var);
		((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_0 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t616 * L_4 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_0;
		float L_5 = LayoutUtility_GetLayoutProperty_m2999(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredWidth(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t617_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t616_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredWidthU3Em__F_m3002_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3453_MethodInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredWidthU3Em__10_m3003_MethodInfo_var;
extern "C" float LayoutUtility_GetPreferredWidth_m2994 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1154);
		Func_2_t616_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1155);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		LayoutUtility_U3CGetPreferredWidthU3Em__F_m3002_MethodInfo_var = il2cpp_codegen_method_info_from_index(306);
		Func_2__ctor_m3453_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483953);
		LayoutUtility_U3CGetPreferredWidthU3Em__10_m3003_MethodInfo_var = il2cpp_codegen_method_info_from_index(307);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t364 * G_B2_0 = {0};
	RectTransform_t364 * G_B1_0 = {0};
	RectTransform_t364 * G_B4_0 = {0};
	float G_B4_1 = 0.0f;
	RectTransform_t364 * G_B3_0 = {0};
	float G_B3_1 = 0.0f;
	{
		RectTransform_t364 * L_0 = ___rect;
		Func_2_t616 * L_1 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetPreferredWidthU3Em__F_m3002_MethodInfo_var };
		Func_2_t616 * L_3 = (Func_2_t616 *)il2cpp_codegen_object_new (Func_2_t616_il2cpp_TypeInfo_var);
		Func_2__ctor_m3453(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3453_MethodInfo_var);
		((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t616 * L_4 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		float L_5 = LayoutUtility_GetLayoutProperty_m2999(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		RectTransform_t364 * L_6 = ___rect;
		Func_2_t616 * L_7 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2;
		G_B3_0 = L_6;
		G_B3_1 = L_5;
		if (L_7)
		{
			G_B4_0 = L_6;
			G_B4_1 = L_5;
			goto IL_0041;
		}
	}
	{
		IntPtr_t L_8 = { (void*)LayoutUtility_U3CGetPreferredWidthU3Em__10_m3003_MethodInfo_var };
		Func_2_t616 * L_9 = (Func_2_t616 *)il2cpp_codegen_object_new (Func_2_t616_il2cpp_TypeInfo_var);
		Func_2__ctor_m3453(L_9, NULL, L_8, /*hidden argument*/Func_2__ctor_m3453_MethodInfo_var);
		((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2 = L_9;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0041:
	{
		Func_2_t616 * L_10 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2;
		float L_11 = LayoutUtility_GetLayoutProperty_m2999(NULL /*static, unused*/, G_B4_0, L_10, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Max_m782(NULL /*static, unused*/, G_B4_1, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleWidth(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t617_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t616_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetFlexibleWidthU3Em__11_m3004_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3453_MethodInfo_var;
extern "C" float LayoutUtility_GetFlexibleWidth_m2995 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1154);
		Func_2_t616_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1155);
		LayoutUtility_U3CGetFlexibleWidthU3Em__11_m3004_MethodInfo_var = il2cpp_codegen_method_info_from_index(308);
		Func_2__ctor_m3453_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483953);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t364 * G_B2_0 = {0};
	RectTransform_t364 * G_B1_0 = {0};
	{
		RectTransform_t364 * L_0 = ___rect;
		Func_2_t616 * L_1 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetFlexibleWidthU3Em__11_m3004_MethodInfo_var };
		Func_2_t616 * L_3 = (Func_2_t616 *)il2cpp_codegen_object_new (Func_2_t616_il2cpp_TypeInfo_var);
		Func_2__ctor_m3453(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3453_MethodInfo_var);
		((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t616 * L_4 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		float L_5 = LayoutUtility_GetLayoutProperty_m2999(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetMinHeight(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t617_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t616_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetMinHeightU3Em__12_m3005_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3453_MethodInfo_var;
extern "C" float LayoutUtility_GetMinHeight_m2996 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1154);
		Func_2_t616_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1155);
		LayoutUtility_U3CGetMinHeightU3Em__12_m3005_MethodInfo_var = il2cpp_codegen_method_info_from_index(309);
		Func_2__ctor_m3453_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483953);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t364 * G_B2_0 = {0};
	RectTransform_t364 * G_B1_0 = {0};
	{
		RectTransform_t364 * L_0 = ___rect;
		Func_2_t616 * L_1 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetMinHeightU3Em__12_m3005_MethodInfo_var };
		Func_2_t616 * L_3 = (Func_2_t616 *)il2cpp_codegen_object_new (Func_2_t616_il2cpp_TypeInfo_var);
		Func_2__ctor_m3453(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3453_MethodInfo_var);
		((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t616 * L_4 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		float L_5 = LayoutUtility_GetLayoutProperty_m2999(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredHeight(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t617_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t616_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t218_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredHeightU3Em__13_m3006_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3453_MethodInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredHeightU3Em__14_m3007_MethodInfo_var;
extern "C" float LayoutUtility_GetPreferredHeight_m2997 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1154);
		Func_2_t616_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1155);
		Mathf_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		LayoutUtility_U3CGetPreferredHeightU3Em__13_m3006_MethodInfo_var = il2cpp_codegen_method_info_from_index(310);
		Func_2__ctor_m3453_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483953);
		LayoutUtility_U3CGetPreferredHeightU3Em__14_m3007_MethodInfo_var = il2cpp_codegen_method_info_from_index(311);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t364 * G_B2_0 = {0};
	RectTransform_t364 * G_B1_0 = {0};
	RectTransform_t364 * G_B4_0 = {0};
	float G_B4_1 = 0.0f;
	RectTransform_t364 * G_B3_0 = {0};
	float G_B3_1 = 0.0f;
	{
		RectTransform_t364 * L_0 = ___rect;
		Func_2_t616 * L_1 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetPreferredHeightU3Em__13_m3006_MethodInfo_var };
		Func_2_t616 * L_3 = (Func_2_t616 *)il2cpp_codegen_object_new (Func_2_t616_il2cpp_TypeInfo_var);
		Func_2__ctor_m3453(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3453_MethodInfo_var);
		((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t616 * L_4 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		float L_5 = LayoutUtility_GetLayoutProperty_m2999(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		RectTransform_t364 * L_6 = ___rect;
		Func_2_t616 * L_7 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		G_B3_0 = L_6;
		G_B3_1 = L_5;
		if (L_7)
		{
			G_B4_0 = L_6;
			G_B4_1 = L_5;
			goto IL_0041;
		}
	}
	{
		IntPtr_t L_8 = { (void*)LayoutUtility_U3CGetPreferredHeightU3Em__14_m3007_MethodInfo_var };
		Func_2_t616 * L_9 = (Func_2_t616 *)il2cpp_codegen_object_new (Func_2_t616_il2cpp_TypeInfo_var);
		Func_2__ctor_m3453(L_9, NULL, L_8, /*hidden argument*/Func_2__ctor_m3453_MethodInfo_var);
		((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6 = L_9;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0041:
	{
		Func_2_t616 * L_10 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		float L_11 = LayoutUtility_GetLayoutProperty_m2999(NULL /*static, unused*/, G_B4_0, L_10, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t218_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Max_m782(NULL /*static, unused*/, G_B4_1, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleHeight(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t617_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t616_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetFlexibleHeightU3Em__15_m3008_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3453_MethodInfo_var;
extern "C" float LayoutUtility_GetFlexibleHeight_m2998 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1154);
		Func_2_t616_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1155);
		LayoutUtility_U3CGetFlexibleHeightU3Em__15_m3008_MethodInfo_var = il2cpp_codegen_method_info_from_index(312);
		Func_2__ctor_m3453_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483953);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t364 * G_B2_0 = {0};
	RectTransform_t364 * G_B1_0 = {0};
	{
		RectTransform_t364 * L_0 = ___rect;
		Func_2_t616 * L_1 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetFlexibleHeightU3Em__15_m3008_MethodInfo_var };
		Func_2_t616 * L_3 = (Func_2_t616 *)il2cpp_codegen_object_new (Func_2_t616_il2cpp_TypeInfo_var);
		Func_2__ctor_m3453(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3453_MethodInfo_var);
		((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t616 * L_4 = ((LayoutUtility_t617_StaticFields*)LayoutUtility_t617_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
		float L_5 = LayoutUtility_GetLayoutProperty_m2999(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single)
extern "C" float LayoutUtility_GetLayoutProperty_m2999 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, Func_2_t616 * ___property, float ___defaultValue, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		RectTransform_t364 * L_0 = ___rect;
		Func_2_t616 * L_1 = ___property;
		float L_2 = ___defaultValue;
		float L_3 = LayoutUtility_GetLayoutProperty_m3000(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single,UnityEngine.UI.ILayoutElement&)
extern const Il2CppType* ILayoutElement_t652_0_0_0_var;
extern TypeInfo* ComponentListPool_t625_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ILayoutElement_t652_il2cpp_TypeInfo_var;
extern TypeInfo* Behaviour_t250_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_GetLayoutProperty_m3000 (Object_t * __this /* static, unused */, RectTransform_t364 * ___rect, Func_2_t616 * ___property, float ___defaultValue, Object_t ** ___source, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_0_0_0_var = il2cpp_codegen_type_from_index(1153);
		ComponentListPool_t625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		ILayoutElement_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		Behaviour_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	List_1_t651 * V_2 = {0};
	int32_t V_3 = 0;
	Object_t * V_4 = {0};
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	{
		Object_t ** L_0 = ___source;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		RectTransform_t364 * L_1 = ___rect;
		bool L_2 = Object_op_Equality_m640(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		return (0.0f);
	}

IL_0015:
	{
		float L_3 = ___defaultValue;
		V_0 = L_3;
		V_1 = ((int32_t)-2147483648);
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		List_1_t651 * L_4 = ComponentListPool_Get_m3032(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_4;
		RectTransform_t364 * L_5 = ___rect;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m705(NULL /*static, unused*/, LoadTypeToken(ILayoutElement_t652_0_0_0_var), /*hidden argument*/NULL);
		List_1_t651 * L_7 = V_2;
		NullCheck(L_5);
		Component_GetComponents_m3180(L_5, L_6, L_7, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_00d7;
	}

IL_003b:
	{
		List_1_t651 * L_8 = V_2;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		Component_t219 * L_10 = (Component_t219 *)VirtFuncInvoker1< Component_t219 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_8, L_9);
		V_4 = ((Object_t *)IsInst(L_10, ILayoutElement_t652_il2cpp_TypeInfo_var));
		Object_t * L_11 = V_4;
		if (!((Behaviour_t250 *)IsInst(L_11, Behaviour_t250_il2cpp_TypeInfo_var)))
		{
			goto IL_007c;
		}
	}
	{
		Object_t * L_12 = V_4;
		NullCheck(((Behaviour_t250 *)IsInst(L_12, Behaviour_t250_il2cpp_TypeInfo_var)));
		bool L_13 = Behaviour_get_enabled_m911(((Behaviour_t250 *)IsInst(L_12, Behaviour_t250_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0077;
		}
	}
	{
		Object_t * L_14 = V_4;
		NullCheck(((Behaviour_t250 *)IsInst(L_14, Behaviour_t250_il2cpp_TypeInfo_var)));
		bool L_15 = Behaviour_get_isActiveAndEnabled_m1740(((Behaviour_t250 *)IsInst(L_14, Behaviour_t250_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_007c;
		}
	}

IL_0077:
	{
		goto IL_00d3;
	}

IL_007c:
	{
		Object_t * L_16 = V_4;
		NullCheck(L_16);
		int32_t L_17 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 UnityEngine.UI.ILayoutElement::get_layoutPriority() */, ILayoutElement_t652_il2cpp_TypeInfo_var, L_16);
		V_5 = L_17;
		int32_t L_18 = V_5;
		int32_t L_19 = V_1;
		if ((((int32_t)L_18) >= ((int32_t)L_19)))
		{
			goto IL_0092;
		}
	}
	{
		goto IL_00d3;
	}

IL_0092:
	{
		Func_2_t616 * L_20 = ___property;
		Object_t * L_21 = V_4;
		NullCheck(L_20);
		float L_22 = (float)VirtFuncInvoker1< float, Object_t * >::Invoke(10 /* !1 System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::Invoke(!0) */, L_20, L_21);
		V_6 = L_22;
		float L_23 = V_6;
		if ((!(((float)L_23) < ((float)(0.0f)))))
		{
			goto IL_00ad;
		}
	}
	{
		goto IL_00d3;
	}

IL_00ad:
	{
		int32_t L_24 = V_5;
		int32_t L_25 = V_1;
		if ((((int32_t)L_24) <= ((int32_t)L_25)))
		{
			goto IL_00c4;
		}
	}
	{
		float L_26 = V_6;
		V_0 = L_26;
		int32_t L_27 = V_5;
		V_1 = L_27;
		Object_t ** L_28 = ___source;
		Object_t * L_29 = V_4;
		*((Object_t **)(L_28)) = (Object_t *)L_29;
		goto IL_00d3;
	}

IL_00c4:
	{
		float L_30 = V_6;
		float L_31 = V_0;
		if ((!(((float)L_30) > ((float)L_31))))
		{
			goto IL_00d3;
		}
	}
	{
		float L_32 = V_6;
		V_0 = L_32;
		Object_t ** L_33 = ___source;
		Object_t * L_34 = V_4;
		*((Object_t **)(L_33)) = (Object_t *)L_34;
	}

IL_00d3:
	{
		int32_t L_35 = V_3;
		V_3 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00d7:
	{
		int32_t L_36 = V_3;
		List_1_t651 * L_37 = V_2;
		NullCheck(L_37);
		int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_37);
		if ((((int32_t)L_36) < ((int32_t)L_38)))
		{
			goto IL_003b;
		}
	}
	{
		List_1_t651 * L_39 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		ComponentListPool_Release_m3033(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		float L_40 = V_0;
		return L_40;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetMinWidth>m__E(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t652_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetMinWidthU3Em__E_m3001 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(2 /* System.Single UnityEngine.UI.ILayoutElement::get_minWidth() */, ILayoutElement_t652_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__F(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t652_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredWidthU3Em__F_m3002 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(2 /* System.Single UnityEngine.UI.ILayoutElement::get_minWidth() */, ILayoutElement_t652_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__10(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t652_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredWidthU3Em__10_m3003 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(3 /* System.Single UnityEngine.UI.ILayoutElement::get_preferredWidth() */, ILayoutElement_t652_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleWidth>m__11(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t652_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetFlexibleWidthU3Em__11_m3004 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(4 /* System.Single UnityEngine.UI.ILayoutElement::get_flexibleWidth() */, ILayoutElement_t652_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetMinHeight>m__12(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t652_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetMinHeightU3Em__12_m3005 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(5 /* System.Single UnityEngine.UI.ILayoutElement::get_minHeight() */, ILayoutElement_t652_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__13(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t652_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredHeightU3Em__13_m3006 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(5 /* System.Single UnityEngine.UI.ILayoutElement::get_minHeight() */, ILayoutElement_t652_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__14(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t652_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredHeightU3Em__14_m3007 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(6 /* System.Single UnityEngine.UI.ILayoutElement::get_preferredHeight() */, ILayoutElement_t652_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleHeight>m__15(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t652_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetFlexibleHeightU3Em__15_m3008 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(7 /* System.Single UnityEngine.UI.ILayoutElement::get_flexibleHeight() */, ILayoutElement_t652_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroupMethodDeclarations.h"



// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
extern "C" void VerticalLayoutGroup__ctor_m3009 (VerticalLayoutGroup_t618 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup__ctor_m2908(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
extern "C" void VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3010 (VerticalLayoutGroup_t618 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_CalculateLayoutInputHorizontal_m2948(__this, /*hidden argument*/NULL);
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m2915(__this, 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
extern "C" void VerticalLayoutGroup_CalculateLayoutInputVertical_m3011 (VerticalLayoutGroup_t618 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m2915(__this, 1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
extern "C" void VerticalLayoutGroup_SetLayoutHorizontal_m3012 (VerticalLayoutGroup_t618 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m2916(__this, 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
extern "C" void VerticalLayoutGroup_SetLayoutVertical_m3013 (VerticalLayoutGroup_t618 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m2916(__this, 1, 1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_Mask.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_MaskMethodDeclarations.h"

// UnityEngine.UI.Graphic
#include "UnityEngine_UI_UnityEngine_UI_Graphic.h"
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// UnityEngine.UI.Graphic
#include "UnityEngine_UI_UnityEngine_UI_GraphicMethodDeclarations.h"
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRendererMethodDeclarations.h"
// UnityEngine.UI.Misc
#include "UnityEngine_UI_UnityEngine_UI_MiscMethodDeclarations.h"
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtilityMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
struct Component_t219;
struct Graphic_t418;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Graphic>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Graphic>()
#define Component_GetComponent_TisGraphic_t418_m3383(__this, method) (( Graphic_t418 * (*) (Component_t219 *, const MethodInfo*))Component_GetComponent_TisObject_t_m614_gshared)(__this, method)
struct Component_t219;
struct List_1_t651;
struct Component_t219;
struct List_1_t655;
// Declaration System.Void UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Collections.Generic.List`1<!!0>)
// System.Void UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Collections.Generic.List`1<!!0>)
extern "C" void Component_GetComponentsInChildren_TisObject_t_m3455_gshared (Component_t219 * __this, List_1_t655 * p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m3455(__this, p0, method) (( void (*) (Component_t219 *, List_1_t655 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m3455_gshared)(__this, p0, method)
// Declaration System.Void UnityEngine.Component::GetComponentsInChildren<UnityEngine.Component>(System.Collections.Generic.List`1<!!0>)
// System.Void UnityEngine.Component::GetComponentsInChildren<UnityEngine.Component>(System.Collections.Generic.List`1<!!0>)
#define Component_GetComponentsInChildren_TisComponent_t219_m3454(__this, p0, method) (( void (*) (Component_t219 *, List_1_t651 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m3455_gshared)(__this, p0, method)


// System.Void UnityEngine.UI.Mask::.ctor()
extern "C" void Mask__ctor_m3014 (Mask_t619 * __this, const MethodInfo* method)
{
	{
		__this->___m_ShowMaskGraphic_2 = 1;
		UIBehaviour__ctor_m1974(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Graphic UnityEngine.UI.Mask::get_graphic()
extern const MethodInfo* Component_GetComponent_TisGraphic_t418_m3383_MethodInfo_var;
extern "C" Graphic_t418 * Mask_get_graphic_m3015 (Mask_t619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisGraphic_t418_m3383_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483918);
		s_Il2CppMethodIntialized = true;
	}
	{
		Graphic_t418 * L_0 = (__this->___m_Graphic_4);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Graphic_t418 * L_2 = Component_GetComponent_TisGraphic_t418_m3383(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t418_m3383_MethodInfo_var);
		__this->___m_Graphic_4 = L_2;
	}

IL_001d:
	{
		Graphic_t418 * L_3 = (__this->___m_Graphic_4);
		return L_3;
	}
}
// System.Boolean UnityEngine.UI.Mask::get_showMaskGraphic()
extern "C" bool Mask_get_showMaskGraphic_m3016 (Mask_t619 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_ShowMaskGraphic_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Mask::set_showMaskGraphic(System.Boolean)
extern "C" void Mask_set_showMaskGraphic_m3017 (Mask_t619 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_ShowMaskGraphic_2);
		bool L_1 = ___value;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___value;
		__this->___m_ShowMaskGraphic_2 = L_2;
		Graphic_t418 * L_3 = Mask_get_graphic_m3015(__this, /*hidden argument*/NULL);
		bool L_4 = Object_op_Inequality_m623(NULL /*static, unused*/, L_3, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		Graphic_t418 * L_5 = Mask_get_graphic_m3015(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, L_5);
	}

IL_0030:
	{
		return;
	}
}
// UnityEngine.RectTransform UnityEngine.UI.Mask::get_rectTransform()
extern const MethodInfo* Component_GetComponent_TisRectTransform_t364_m1776_MethodInfo_var;
extern "C" RectTransform_t364 * Mask_get_rectTransform_m3018 (Mask_t619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRectTransform_t364_m1776_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483718);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t364 * V_0 = {0};
	RectTransform_t364 * G_B2_0 = {0};
	RectTransform_t364 * G_B1_0 = {0};
	{
		RectTransform_t364 * L_0 = (__this->___m_RectTransform_5);
		RectTransform_t364 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001c;
		}
	}
	{
		RectTransform_t364 * L_2 = Component_GetComponent_TisRectTransform_t364_m1776(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t364_m1776_MethodInfo_var);
		RectTransform_t364 * L_3 = L_2;
		V_0 = L_3;
		__this->___m_RectTransform_5 = L_3;
		RectTransform_t364 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001c:
	{
		return G_B2_0;
	}
}
// System.Boolean UnityEngine.UI.Mask::MaskEnabled()
extern "C" bool Mask_MaskEnabled_m3019 (Mask_t619 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Graphic_t418 * L_1 = Mask_get_graphic_m3015(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m623(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.UI.Mask::OnSiblingGraphicEnabledDisabled()
extern "C" void Mask_OnSiblingGraphicEnabledDisabled_m3020 (Mask_t619 * __this, const MethodInfo* method)
{
	{
		Mask_NotifyMaskStateChanged_m3021(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Mask::NotifyMaskStateChanged()
extern TypeInfo* ComponentListPool_t625_il2cpp_TypeInfo_var;
extern TypeInfo* IMaskable_t707_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisComponent_t219_m3454_MethodInfo_var;
extern "C" void Mask_NotifyMaskStateChanged_m3021 (Mask_t619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentListPool_t625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		IMaskable_t707_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1156);
		Component_GetComponentsInChildren_TisComponent_t219_m3454_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483961);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t651 * V_0 = {0};
	int32_t V_1 = 0;
	Object_t * V_2 = {0};
	{
		Graphic_t418 * L_0 = Mask_get_graphic_m3015(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m623(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		Graphic_t418 * L_2 = Mask_get_graphic_m3015(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		CanvasRenderer_t522 * L_3 = Graphic_get_canvasRenderer_m2256(L_2, /*hidden argument*/NULL);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		NullCheck(L_3);
		CanvasRenderer_set_isMask_m3456(L_3, L_4, /*hidden argument*/NULL);
		Graphic_t418 * L_5 = Mask_get_graphic_m3015(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(22 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, L_5);
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		List_1_t651 * L_6 = ComponentListPool_Get_m3032(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_6;
		List_1_t651 * L_7 = V_0;
		Component_GetComponentsInChildren_TisComponent_t219_m3454(__this, L_7, /*hidden argument*/Component_GetComponentsInChildren_TisComponent_t219_m3454_MethodInfo_var);
		V_1 = 0;
		goto IL_0096;
	}

IL_0046:
	{
		List_1_t651 * L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		Component_t219 * L_10 = (Component_t219 *)VirtFuncInvoker1< Component_t219 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_8, L_9);
		bool L_11 = Object_op_Equality_m640(NULL /*static, unused*/, L_10, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0074;
		}
	}
	{
		List_1_t651 * L_12 = V_0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		Component_t219 * L_14 = (Component_t219 *)VirtFuncInvoker1< Component_t219 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_12, L_13);
		NullCheck(L_14);
		GameObject_t78 * L_15 = Component_get_gameObject_m622(L_14, /*hidden argument*/NULL);
		GameObject_t78 * L_16 = Component_get_gameObject_m622(__this, /*hidden argument*/NULL);
		bool L_17 = Object_op_Equality_m640(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0079;
		}
	}

IL_0074:
	{
		goto IL_0092;
	}

IL_0079:
	{
		List_1_t651 * L_18 = V_0;
		int32_t L_19 = V_1;
		NullCheck(L_18);
		Component_t219 * L_20 = (Component_t219 *)VirtFuncInvoker1< Component_t219 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_18, L_19);
		V_2 = ((Object_t *)IsInst(L_20, IMaskable_t707_il2cpp_TypeInfo_var));
		Object_t * L_21 = V_2;
		if (!L_21)
		{
			goto IL_0092;
		}
	}
	{
		Object_t * L_22 = V_2;
		NullCheck(L_22);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.UI.IMaskable::ParentMaskStateChanged() */, IMaskable_t707_il2cpp_TypeInfo_var, L_22);
	}

IL_0092:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0096:
	{
		int32_t L_24 = V_1;
		List_1_t651 * L_25 = V_0;
		NullCheck(L_25);
		int32_t L_26 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_25);
		if ((((int32_t)L_24) < ((int32_t)L_26)))
		{
			goto IL_0046;
		}
	}
	{
		List_1_t651 * L_27 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		ComponentListPool_Release_m3033(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Mask::ClearCachedMaterial()
extern "C" void Mask_ClearCachedMaterial_m3022 (Mask_t619 * __this, const MethodInfo* method)
{
	{
		Material_t55 * L_0 = (__this->___m_RenderMaterial_3);
		bool L_1 = Object_op_Inequality_m623(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Material_t55 * L_2 = (__this->___m_RenderMaterial_3);
		Misc_DestroyImmediate_m2512(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		__this->___m_RenderMaterial_3 = (Material_t55 *)NULL;
		return;
	}
}
// System.Void UnityEngine.UI.Mask::OnEnable()
extern "C" void Mask_OnEnable_m3023 (Mask_t619 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m1976(__this, /*hidden argument*/NULL);
		Mask_NotifyMaskStateChanged_m3021(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Mask::OnDisable()
extern "C" void Mask_OnDisable_m3024 (Mask_t619 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnDisable_m1978(__this, /*hidden argument*/NULL);
		Mask_ClearCachedMaterial_m3022(__this, /*hidden argument*/NULL);
		Mask_NotifyMaskStateChanged_m3021(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.Mask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern TypeInfo* RectTransformUtility_t674_il2cpp_TypeInfo_var;
extern "C" bool Mask_IsRaycastLocationValid_m3025 (Mask_t619 * __this, Vector2_t6  ___sp, Camera_t27 * ___eventCamera, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t674_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1076);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t364 * L_0 = Mask_get_rectTransform_m3018(__this, /*hidden argument*/NULL);
		Vector2_t6  L_1 = ___sp;
		Camera_t27 * L_2 = ___eventCamera;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t674_il2cpp_TypeInfo_var);
		bool L_3 = RectTransformUtility_RectangleContainsScreenPoint_m3214(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Material UnityEngine.UI.Mask::GetModifiedMaterial(UnityEngine.Material)
extern TypeInfo* Material_t55_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" Material_t55 * Mask_GetModifiedMaterial_m3026 (Mask_t619 * __this, Material_t55 * ___baseMaterial, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	Material_t55 * V_0 = {0};
	String_t* G_B5_0 = {0};
	Material_t55 * G_B5_1 = {0};
	String_t* G_B4_0 = {0};
	Material_t55 * G_B4_1 = {0};
	int32_t G_B6_0 = 0;
	String_t* G_B6_1 = {0};
	Material_t55 * G_B6_2 = {0};
	{
		Mask_ClearCachedMaterial_m3022(__this, /*hidden argument*/NULL);
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Material_t55 * L_1 = ___baseMaterial;
		return L_1;
	}

IL_0013:
	{
		Material_t55 * L_2 = ___baseMaterial;
		Material_t55 * L_3 = (Material_t55 *)il2cpp_codegen_object_new (Material_t55_il2cpp_TypeInfo_var);
		Material__ctor_m3397(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Material_t55 * L_4 = V_0;
		Material_t55 * L_5 = ___baseMaterial;
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m798(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m734(NULL /*static, unused*/, (String_t*) &_stringLiteral441, L_6, (String_t*) &_stringLiteral185, /*hidden argument*/NULL);
		NullCheck(L_4);
		Object_set_name_m1795(L_4, L_7, /*hidden argument*/NULL);
		Material_t55 * L_8 = V_0;
		NullCheck(L_8);
		Object_set_hideFlags_m764(L_8, ((int32_t)61), /*hidden argument*/NULL);
		Material_t55 * L_9 = V_0;
		__this->___m_RenderMaterial_3 = L_9;
		Material_t55 * L_10 = (__this->___m_RenderMaterial_3);
		NullCheck(L_10);
		bool L_11 = Material_HasProperty_m3396(L_10, (String_t*) &_stringLiteral442, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0081;
		}
	}
	{
		Material_t55 * L_12 = (__this->___m_RenderMaterial_3);
		bool L_13 = (__this->___m_ShowMaskGraphic_2);
		G_B4_0 = (String_t*) &_stringLiteral442;
		G_B4_1 = L_12;
		if (!L_13)
		{
			G_B5_0 = (String_t*) &_stringLiteral442;
			G_B5_1 = L_12;
			goto IL_0076;
		}
	}
	{
		G_B6_0 = ((int32_t)15);
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_0077;
	}

IL_0076:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		Material_SetInt_m3398(G_B6_2, G_B6_1, G_B6_0, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_0081:
	{
		Material_t55 * L_14 = ___baseMaterial;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m1696(NULL /*static, unused*/, (String_t*) &_stringLiteral435, L_14, (String_t*) &_stringLiteral443, /*hidden argument*/NULL);
		Material_t55 * L_16 = ___baseMaterial;
		Debug_LogWarning_m855(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
	}

IL_0097:
	{
		Material_t55 * L_17 = (__this->___m_RenderMaterial_3);
		return L_17;
	}
}
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPool.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPoolMethodDeclarations.h"

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_4.h"
// System.Collections.Generic.List`1<UnityEngine.Canvas>
#include "mscorlib_System_Collections_Generic_List_1_gen_18.h"
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_1.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_4MethodDeclarations.h"
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_1MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Canvas>
#include "mscorlib_System_Collections_Generic_List_1_gen_18MethodDeclarations.h"


// System.Void UnityEngine.UI.CanvasListPool::.cctor()
extern TypeInfo* CanvasListPool_t622_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAction_1_t621_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectPool_1_t620_il2cpp_TypeInfo_var;
extern const MethodInfo* CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m3030_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3457_MethodInfo_var;
extern const MethodInfo* ObjectPool_1__ctor_m3458_MethodInfo_var;
extern "C" void CanvasListPool__cctor_m3027 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CanvasListPool_t622_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1067);
		UnityAction_1_t621_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1158);
		ObjectPool_1_t620_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1159);
		CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m3030_MethodInfo_var = il2cpp_codegen_method_info_from_index(314);
		UnityAction_1__ctor_m3457_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483963);
		ObjectPool_1__ctor_m3458_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483964);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	{
		UnityAction_1_t621 * L_0 = ((CanvasListPool_t622_StaticFields*)CanvasListPool_t622_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m3030_MethodInfo_var };
		UnityAction_1_t621 * L_2 = (UnityAction_1_t621 *)il2cpp_codegen_object_new (UnityAction_1_t621_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3457(L_2, NULL, L_1, /*hidden argument*/UnityAction_1__ctor_m3457_MethodInfo_var);
		((CanvasListPool_t622_StaticFields*)CanvasListPool_t622_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t621 * L_3 = ((CanvasListPool_t622_StaticFields*)CanvasListPool_t622_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		ObjectPool_1_t620 * L_4 = (ObjectPool_1_t620 *)il2cpp_codegen_object_new (ObjectPool_1_t620_il2cpp_TypeInfo_var);
		ObjectPool_1__ctor_m3458(L_4, (UnityAction_1_t621 *)G_B2_0, L_3, /*hidden argument*/ObjectPool_1__ctor_m3458_MethodInfo_var);
		((CanvasListPool_t622_StaticFields*)CanvasListPool_t622_il2cpp_TypeInfo_var->static_fields)->___s_CanvasListPool_0 = L_4;
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Canvas> UnityEngine.UI.CanvasListPool::Get()
extern TypeInfo* CanvasListPool_t622_il2cpp_TypeInfo_var;
extern const MethodInfo* ObjectPool_1_Get_m3459_MethodInfo_var;
extern "C" List_1_t653 * CanvasListPool_Get_m3028 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CanvasListPool_t622_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1067);
		ObjectPool_1_Get_m3459_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483965);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CanvasListPool_t622_il2cpp_TypeInfo_var);
		ObjectPool_1_t620 * L_0 = ((CanvasListPool_t622_StaticFields*)CanvasListPool_t622_il2cpp_TypeInfo_var->static_fields)->___s_CanvasListPool_0;
		NullCheck(L_0);
		List_1_t653 * L_1 = ObjectPool_1_Get_m3459(L_0, /*hidden argument*/ObjectPool_1_Get_m3459_MethodInfo_var);
		return L_1;
	}
}
// System.Void UnityEngine.UI.CanvasListPool::Release(System.Collections.Generic.List`1<UnityEngine.Canvas>)
extern TypeInfo* CanvasListPool_t622_il2cpp_TypeInfo_var;
extern const MethodInfo* ObjectPool_1_Release_m3460_MethodInfo_var;
extern "C" void CanvasListPool_Release_m3029 (Object_t * __this /* static, unused */, List_1_t653 * ___toRelease, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CanvasListPool_t622_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1067);
		ObjectPool_1_Release_m3460_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483966);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CanvasListPool_t622_il2cpp_TypeInfo_var);
		ObjectPool_1_t620 * L_0 = ((CanvasListPool_t622_StaticFields*)CanvasListPool_t622_il2cpp_TypeInfo_var->static_fields)->___s_CanvasListPool_0;
		List_1_t653 * L_1 = ___toRelease;
		NullCheck(L_0);
		ObjectPool_1_Release_m3460(L_0, L_1, /*hidden argument*/ObjectPool_1_Release_m3460_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasListPool::<s_CanvasListPool>m__16(System.Collections.Generic.List`1<UnityEngine.Canvas>)
extern "C" void CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m3030 (Object_t * __this /* static, unused */, List_1_t653 * ___l, const MethodInfo* method)
{
	{
		List_1_t653 * L_0 = ___l;
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::Clear() */, L_0);
		return;
	}
}
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPool.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_5.h"
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_2.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_5MethodDeclarations.h"
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_2MethodDeclarations.h"


// System.Void UnityEngine.UI.ComponentListPool::.cctor()
extern TypeInfo* ComponentListPool_t625_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAction_1_t624_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectPool_1_t623_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m3034_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3461_MethodInfo_var;
extern const MethodInfo* ObjectPool_1__ctor_m3462_MethodInfo_var;
extern "C" void ComponentListPool__cctor_m3031 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentListPool_t625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		UnityAction_1_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1161);
		ObjectPool_1_t623_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1162);
		ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m3034_MethodInfo_var = il2cpp_codegen_method_info_from_index(319);
		UnityAction_1__ctor_m3461_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483968);
		ObjectPool_1__ctor_m3462_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483969);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	{
		UnityAction_1_t624 * L_0 = ((ComponentListPool_t625_StaticFields*)ComponentListPool_t625_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m3034_MethodInfo_var };
		UnityAction_1_t624 * L_2 = (UnityAction_1_t624 *)il2cpp_codegen_object_new (UnityAction_1_t624_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3461(L_2, NULL, L_1, /*hidden argument*/UnityAction_1__ctor_m3461_MethodInfo_var);
		((ComponentListPool_t625_StaticFields*)ComponentListPool_t625_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t624 * L_3 = ((ComponentListPool_t625_StaticFields*)ComponentListPool_t625_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		ObjectPool_1_t623 * L_4 = (ObjectPool_1_t623 *)il2cpp_codegen_object_new (ObjectPool_1_t623_il2cpp_TypeInfo_var);
		ObjectPool_1__ctor_m3462(L_4, (UnityAction_1_t624 *)G_B2_0, L_3, /*hidden argument*/ObjectPool_1__ctor_m3462_MethodInfo_var);
		((ComponentListPool_t625_StaticFields*)ComponentListPool_t625_il2cpp_TypeInfo_var->static_fields)->___s_ComponentListPool_0 = L_4;
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Component> UnityEngine.UI.ComponentListPool::Get()
extern TypeInfo* ComponentListPool_t625_il2cpp_TypeInfo_var;
extern const MethodInfo* ObjectPool_1_Get_m3463_MethodInfo_var;
extern "C" List_1_t651 * ComponentListPool_Get_m3032 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentListPool_t625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		ObjectPool_1_Get_m3463_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483970);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		ObjectPool_1_t623 * L_0 = ((ComponentListPool_t625_StaticFields*)ComponentListPool_t625_il2cpp_TypeInfo_var->static_fields)->___s_ComponentListPool_0;
		NullCheck(L_0);
		List_1_t651 * L_1 = ObjectPool_1_Get_m3463(L_0, /*hidden argument*/ObjectPool_1_Get_m3463_MethodInfo_var);
		return L_1;
	}
}
// System.Void UnityEngine.UI.ComponentListPool::Release(System.Collections.Generic.List`1<UnityEngine.Component>)
extern TypeInfo* ComponentListPool_t625_il2cpp_TypeInfo_var;
extern const MethodInfo* ObjectPool_1_Release_m3464_MethodInfo_var;
extern "C" void ComponentListPool_Release_m3033 (Object_t * __this /* static, unused */, List_1_t651 * ___toRelease, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentListPool_t625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1070);
		ObjectPool_1_Release_m3464_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483971);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ComponentListPool_t625_il2cpp_TypeInfo_var);
		ObjectPool_1_t623 * L_0 = ((ComponentListPool_t625_StaticFields*)ComponentListPool_t625_il2cpp_TypeInfo_var->static_fields)->___s_ComponentListPool_0;
		List_1_t651 * L_1 = ___toRelease;
		NullCheck(L_0);
		ObjectPool_1_Release_m3464(L_0, L_1, /*hidden argument*/ObjectPool_1_Release_m3464_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.ComponentListPool::<s_ComponentListPool>m__17(System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C" void ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m3034 (Object_t * __this /* static, unused */, List_1_t651 * ___l, const MethodInfo* method)
{
	{
		List_1_t651 * L_0 = ___l;
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Clear() */, L_0);
		return;
	}
}
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffectMethodDeclarations.h"

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_17.h"


// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
extern "C" void BaseVertexEffect__ctor_m3035 (BaseVertexEffect_t626 * __this, const MethodInfo* method)
{
	{
		UIBehaviour__ctor_m1974(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::get_graphic()
extern const MethodInfo* Component_GetComponent_TisGraphic_t418_m3383_MethodInfo_var;
extern "C" Graphic_t418 * BaseVertexEffect_get_graphic_m3036 (BaseVertexEffect_t626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisGraphic_t418_m3383_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483918);
		s_Il2CppMethodIntialized = true;
	}
	{
		Graphic_t418 * L_0 = (__this->___m_Graphic_2);
		bool L_1 = Object_op_Equality_m640(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Graphic_t418 * L_2 = Component_GetComponent_TisGraphic_t418_m3383(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t418_m3383_MethodInfo_var);
		__this->___m_Graphic_2 = L_2;
	}

IL_001d:
	{
		Graphic_t418 * L_3 = (__this->___m_Graphic_2);
		return L_3;
	}
}
// System.Void UnityEngine.UI.BaseVertexEffect::OnEnable()
extern "C" void BaseVertexEffect_OnEnable_m3037 (BaseVertexEffect_t626 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m1976(__this, /*hidden argument*/NULL);
		Graphic_t418 * L_0 = BaseVertexEffect_get_graphic_m3036(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m623(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Graphic_t418 * L_2 = BaseVertexEffect_get_graphic_m3036(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_2);
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BaseVertexEffect::OnDisable()
extern "C" void BaseVertexEffect_OnDisable_m3038 (BaseVertexEffect_t626 * __this, const MethodInfo* method)
{
	{
		Graphic_t418 * L_0 = BaseVertexEffect_get_graphic_m3036(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m623(NULL /*static, unused*/, L_0, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Graphic_t418 * L_2 = BaseVertexEffect_get_graphic_m3036(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_2);
	}

IL_001c:
	{
		UIBehaviour_OnDisable_m1978(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BaseVertexEffect::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_Outline.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_OutlineMethodDeclarations.h"

// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_ShadowMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_17MethodDeclarations.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"


// System.Void UnityEngine.UI.Outline::.ctor()
extern "C" void Outline__ctor_m3039 (Outline_t627 * __this, const MethodInfo* method)
{
	{
		Shadow__ctor_m3043(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void Outline_ModifyVertices_m3040 (Outline_t627 * __this, List_1_t558 * ___verts, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector2_t6  V_2 = {0};
	Vector2_t6  V_3 = {0};
	Vector2_t6  V_4 = {0};
	Vector2_t6  V_5 = {0};
	Vector2_t6  V_6 = {0};
	Vector2_t6  V_7 = {0};
	Vector2_t6  V_8 = {0};
	Vector2_t6  V_9 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		V_0 = 0;
		List_1_t558 * L_1 = ___verts;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_1);
		V_1 = L_2;
		List_1_t558 * L_3 = ___verts;
		Color_t65  L_4 = Shadow_get_effectColor_m3044(__this, /*hidden argument*/NULL);
		Color32_t654  L_5 = Color32_op_Implicit_m3189(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		List_1_t558 * L_7 = ___verts;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_7);
		Vector2_t6  L_9 = Shadow_get_effectDistance_m3046(__this, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = ((&V_2)->___x_1);
		Vector2_t6  L_11 = Shadow_get_effectDistance_m3046(__this, /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = ((&V_3)->___y_2);
		Shadow_ApplyShadow_m3050(__this, L_3, L_5, L_6, L_8, L_10, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		V_0 = L_13;
		List_1_t558 * L_14 = ___verts;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_14);
		V_1 = L_15;
		List_1_t558 * L_16 = ___verts;
		Color_t65  L_17 = Shadow_get_effectColor_m3044(__this, /*hidden argument*/NULL);
		Color32_t654  L_18 = Color32_op_Implicit_m3189(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		int32_t L_19 = V_0;
		List_1_t558 * L_20 = ___verts;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_20);
		Vector2_t6  L_22 = Shadow_get_effectDistance_m3046(__this, /*hidden argument*/NULL);
		V_4 = L_22;
		float L_23 = ((&V_4)->___x_1);
		Vector2_t6  L_24 = Shadow_get_effectDistance_m3046(__this, /*hidden argument*/NULL);
		V_5 = L_24;
		float L_25 = ((&V_5)->___y_2);
		Shadow_ApplyShadow_m3050(__this, L_16, L_18, L_19, L_21, L_23, ((-L_25)), /*hidden argument*/NULL);
		int32_t L_26 = V_1;
		V_0 = L_26;
		List_1_t558 * L_27 = ___verts;
		NullCheck(L_27);
		int32_t L_28 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_27);
		V_1 = L_28;
		List_1_t558 * L_29 = ___verts;
		Color_t65  L_30 = Shadow_get_effectColor_m3044(__this, /*hidden argument*/NULL);
		Color32_t654  L_31 = Color32_op_Implicit_m3189(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		int32_t L_32 = V_0;
		List_1_t558 * L_33 = ___verts;
		NullCheck(L_33);
		int32_t L_34 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_33);
		Vector2_t6  L_35 = Shadow_get_effectDistance_m3046(__this, /*hidden argument*/NULL);
		V_6 = L_35;
		float L_36 = ((&V_6)->___x_1);
		Vector2_t6  L_37 = Shadow_get_effectDistance_m3046(__this, /*hidden argument*/NULL);
		V_7 = L_37;
		float L_38 = ((&V_7)->___y_2);
		Shadow_ApplyShadow_m3050(__this, L_29, L_31, L_32, L_34, ((-L_36)), L_38, /*hidden argument*/NULL);
		int32_t L_39 = V_1;
		V_0 = L_39;
		List_1_t558 * L_40 = ___verts;
		NullCheck(L_40);
		int32_t L_41 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_40);
		V_1 = L_41;
		List_1_t558 * L_42 = ___verts;
		Color_t65  L_43 = Shadow_get_effectColor_m3044(__this, /*hidden argument*/NULL);
		Color32_t654  L_44 = Color32_op_Implicit_m3189(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		int32_t L_45 = V_0;
		List_1_t558 * L_46 = ___verts;
		NullCheck(L_46);
		int32_t L_47 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_46);
		Vector2_t6  L_48 = Shadow_get_effectDistance_m3046(__this, /*hidden argument*/NULL);
		V_8 = L_48;
		float L_49 = ((&V_8)->___x_1);
		Vector2_t6  L_50 = Shadow_get_effectDistance_m3046(__this, /*hidden argument*/NULL);
		V_9 = L_50;
		float L_51 = ((&V_9)->___y_2);
		Shadow_ApplyShadow_m3050(__this, L_42, L_44, L_45, L_47, ((-L_49)), ((-L_51)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1MethodDeclarations.h"

// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"


// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
extern "C" void PositionAsUV1__ctor_m3041 (PositionAsUV1_t629 * __this, const MethodInfo* method)
{
	{
		BaseVertexEffect__ctor_m3035(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.PositionAsUV1::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void PositionAsUV1_ModifyVertices_m3042 (PositionAsUV1_t629 * __this, List_1_t558 * ___verts, const MethodInfo* method)
{
	int32_t V_0 = 0;
	UIVertex_t556  V_1 = {0};
	UIVertex_t556  V_2 = {0};
	UIVertex_t556  V_3 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		V_0 = 0;
		goto IL_005b;
	}

IL_0013:
	{
		List_1_t558 * L_1 = ___verts;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		UIVertex_t556  L_3 = (UIVertex_t556 )VirtFuncInvoker1< UIVertex_t556 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_1, L_2);
		V_1 = L_3;
		List_1_t558 * L_4 = ___verts;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		UIVertex_t556  L_6 = (UIVertex_t556 )VirtFuncInvoker1< UIVertex_t556 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_4, L_5);
		V_2 = L_6;
		Vector3_t4 * L_7 = &((&V_2)->___position_0);
		float L_8 = (L_7->___x_1);
		List_1_t558 * L_9 = ___verts;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		UIVertex_t556  L_11 = (UIVertex_t556 )VirtFuncInvoker1< UIVertex_t556 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_9, L_10);
		V_3 = L_11;
		Vector3_t4 * L_12 = &((&V_3)->___position_0);
		float L_13 = (L_12->___y_2);
		Vector2_t6  L_14 = {0};
		Vector2__ctor_m630(&L_14, L_8, L_13, /*hidden argument*/NULL);
		(&V_1)->___uv1_4 = L_14;
		List_1_t558 * L_15 = ___verts;
		int32_t L_16 = V_0;
		UIVertex_t556  L_17 = V_1;
		NullCheck(L_15);
		VirtActionInvoker2< int32_t, UIVertex_t556  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,!0) */, L_15, L_16, L_17);
		int32_t L_18 = V_0;
		V_0 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_19 = V_0;
		List_1_t558 * L_20 = ___verts;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_20);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_Shadow.h"
#ifndef _MSC_VER
#else
#endif

// System.Byte
#include "mscorlib_System_Byte.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"


// System.Void UnityEngine.UI.Shadow::.ctor()
extern "C" void Shadow__ctor_m3043 (Shadow_t628 * __this, const MethodInfo* method)
{
	{
		Color_t65  L_0 = {0};
		Color__ctor_m746(&L_0, (0.0f), (0.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		__this->___m_EffectColor_3 = L_0;
		Vector2_t6  L_1 = {0};
		Vector2__ctor_m630(&L_1, (1.0f), (-1.0f), /*hidden argument*/NULL);
		__this->___m_EffectDistance_4 = L_1;
		__this->___m_UseGraphicAlpha_5 = 1;
		BaseVertexEffect__ctor_m3035(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color UnityEngine.UI.Shadow::get_effectColor()
extern "C" Color_t65  Shadow_get_effectColor_m3044 (Shadow_t628 * __this, const MethodInfo* method)
{
	{
		Color_t65  L_0 = (__this->___m_EffectColor_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Shadow::set_effectColor(UnityEngine.Color)
extern "C" void Shadow_set_effectColor_m3045 (Shadow_t628 * __this, Color_t65  ___value, const MethodInfo* method)
{
	{
		Color_t65  L_0 = ___value;
		__this->___m_EffectColor_3 = L_0;
		Graphic_t418 * L_1 = BaseVertexEffect_get_graphic_m3036(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m623(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Graphic_t418 * L_3 = BaseVertexEffect_get_graphic_m3036(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_3);
	}

IL_0023:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.Shadow::get_effectDistance()
extern "C" Vector2_t6  Shadow_get_effectDistance_m3046 (Shadow_t628 * __this, const MethodInfo* method)
{
	{
		Vector2_t6  L_0 = (__this->___m_EffectDistance_4);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Shadow::set_effectDistance(UnityEngine.Vector2)
extern "C" void Shadow_set_effectDistance_m3047 (Shadow_t628 * __this, Vector2_t6  ___value, const MethodInfo* method)
{
	{
		float L_0 = ((&___value)->___x_1);
		if ((!(((float)L_0) > ((float)(600.0f)))))
		{
			goto IL_001d;
		}
	}
	{
		(&___value)->___x_1 = (600.0f);
	}

IL_001d:
	{
		float L_1 = ((&___value)->___x_1);
		if ((!(((float)L_1) < ((float)(-600.0f)))))
		{
			goto IL_003a;
		}
	}
	{
		(&___value)->___x_1 = (-600.0f);
	}

IL_003a:
	{
		float L_2 = ((&___value)->___y_2);
		if ((!(((float)L_2) > ((float)(600.0f)))))
		{
			goto IL_0057;
		}
	}
	{
		(&___value)->___y_2 = (600.0f);
	}

IL_0057:
	{
		float L_3 = ((&___value)->___y_2);
		if ((!(((float)L_3) < ((float)(-600.0f)))))
		{
			goto IL_0074;
		}
	}
	{
		(&___value)->___y_2 = (-600.0f);
	}

IL_0074:
	{
		Vector2_t6  L_4 = (__this->___m_EffectDistance_4);
		Vector2_t6  L_5 = ___value;
		bool L_6 = Vector2_op_Equality_m3465(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0086;
		}
	}
	{
		return;
	}

IL_0086:
	{
		Vector2_t6  L_7 = ___value;
		__this->___m_EffectDistance_4 = L_7;
		Graphic_t418 * L_8 = BaseVertexEffect_get_graphic_m3036(__this, /*hidden argument*/NULL);
		bool L_9 = Object_op_Inequality_m623(NULL /*static, unused*/, L_8, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00a9;
		}
	}
	{
		Graphic_t418 * L_10 = BaseVertexEffect_get_graphic_m3036(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_10);
	}

IL_00a9:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.Shadow::get_useGraphicAlpha()
extern "C" bool Shadow_get_useGraphicAlpha_m3048 (Shadow_t628 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_UseGraphicAlpha_5);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Shadow::set_useGraphicAlpha(System.Boolean)
extern "C" void Shadow_set_useGraphicAlpha_m3049 (Shadow_t628 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_UseGraphicAlpha_5 = L_0;
		Graphic_t418 * L_1 = BaseVertexEffect_get_graphic_m3036(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m623(NULL /*static, unused*/, L_1, (Object_t164 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Graphic_t418 * L_3 = BaseVertexEffect_get_graphic_m3036(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_3);
	}

IL_0023:
	{
		return;
	}
}
// System.Void UnityEngine.UI.Shadow::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color32,System.Int32,System.Int32,System.Single,System.Single)
extern const MethodInfo* List_1_get_Capacity_m3201_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m3202_MethodInfo_var;
extern "C" void Shadow_ApplyShadow_m3050 (Shadow_t628 * __this, List_1_t558 * ___verts, Color32_t654  ___color, int32_t ___start, int32_t ___end, float ___x, float ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_get_Capacity_m3201_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483865);
		List_1_set_Capacity_m3202_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483866);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t556  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t4  V_3 = {0};
	Color32_t654  V_4 = {0};
	UIVertex_t556  V_5 = {0};
	{
		List_1_t558 * L_0 = ___verts;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_0);
		V_1 = ((int32_t)((int32_t)L_1*(int32_t)2));
		List_1_t558 * L_2 = ___verts;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Capacity_m3201(L_2, /*hidden argument*/List_1_get_Capacity_m3201_MethodInfo_var);
		int32_t L_4 = V_1;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_001c;
		}
	}
	{
		List_1_t558 * L_5 = ___verts;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		List_1_set_Capacity_m3202(L_5, L_6, /*hidden argument*/List_1_set_Capacity_m3202_MethodInfo_var);
	}

IL_001c:
	{
		int32_t L_7 = ___start;
		V_2 = L_7;
		goto IL_00b0;
	}

IL_0023:
	{
		List_1_t558 * L_8 = ___verts;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		UIVertex_t556  L_10 = (UIVertex_t556 )VirtFuncInvoker1< UIVertex_t556 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_8, L_9);
		V_0 = L_10;
		List_1_t558 * L_11 = ___verts;
		UIVertex_t556  L_12 = V_0;
		NullCheck(L_11);
		VirtActionInvoker1< UIVertex_t556  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(!0) */, L_11, L_12);
		Vector3_t4  L_13 = ((&V_0)->___position_0);
		V_3 = L_13;
		Vector3_t4 * L_14 = (&V_3);
		float L_15 = (L_14->___x_1);
		float L_16 = ___x;
		L_14->___x_1 = ((float)((float)L_15+(float)L_16));
		Vector3_t4 * L_17 = (&V_3);
		float L_18 = (L_17->___y_2);
		float L_19 = ___y;
		L_17->___y_2 = ((float)((float)L_18+(float)L_19));
		Vector3_t4  L_20 = V_3;
		(&V_0)->___position_0 = L_20;
		Color32_t654  L_21 = ___color;
		V_4 = L_21;
		bool L_22 = (__this->___m_UseGraphicAlpha_5);
		if (!L_22)
		{
			goto IL_009b;
		}
	}
	{
		uint8_t L_23 = ((&V_4)->___a_3);
		List_1_t558 * L_24 = ___verts;
		int32_t L_25 = V_2;
		NullCheck(L_24);
		UIVertex_t556  L_26 = (UIVertex_t556 )VirtFuncInvoker1< UIVertex_t556 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_24, L_25);
		V_5 = L_26;
		Color32_t654 * L_27 = &((&V_5)->___color_2);
		uint8_t L_28 = (L_27->___a_3);
		(&V_4)->___a_3 = (((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_23*(int32_t)L_28))/(int32_t)((int32_t)255)))));
	}

IL_009b:
	{
		Color32_t654  L_29 = V_4;
		(&V_0)->___color_2 = L_29;
		List_1_t558 * L_30 = ___verts;
		int32_t L_31 = V_2;
		UIVertex_t556  L_32 = V_0;
		NullCheck(L_30);
		VirtActionInvoker2< int32_t, UIVertex_t556  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,!0) */, L_30, L_31, L_32);
		int32_t L_33 = V_2;
		V_2 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b0:
	{
		int32_t L_34 = V_2;
		int32_t L_35 = ___end;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_0023;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.Shadow::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void Shadow_ModifyVertices_m3051 (Shadow_t628 * __this, List_1_t558 * ___verts, const MethodInfo* method)
{
	Vector2_t6  V_0 = {0};
	Vector2_t6  V_1 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t558 * L_1 = ___verts;
		Color_t65  L_2 = Shadow_get_effectColor_m3044(__this, /*hidden argument*/NULL);
		Color32_t654  L_3 = Color32_op_Implicit_m3189(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		List_1_t558 * L_4 = ___verts;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_4);
		Vector2_t6  L_6 = Shadow_get_effectDistance_m3046(__this, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = ((&V_0)->___x_1);
		Vector2_t6  L_8 = Shadow_get_effectDistance_m3046(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = ((&V_1)->___y_2);
		Shadow_ApplyShadow_m3050(__this, L_1, L_3, 0, L_5, L_7, L_9, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
