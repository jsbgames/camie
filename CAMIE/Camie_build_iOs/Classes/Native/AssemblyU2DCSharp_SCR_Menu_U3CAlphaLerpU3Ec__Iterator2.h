﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t316;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// SCR_Menu/<AlphaLerp>c__Iterator2
struct  U3CAlphaLerpU3Ec__Iterator2_t332  : public Object_t
{
	// System.Single SCR_Menu/<AlphaLerp>c__Iterator2::<alphaValue>__0
	float ___U3CalphaValueU3E__0_0;
	// System.Single SCR_Menu/<AlphaLerp>c__Iterator2::<modifier>__1
	float ___U3CmodifierU3E__1_1;
	// UnityEngine.UI.Text SCR_Menu/<AlphaLerp>c__Iterator2::text
	Text_t316 * ___text_2;
	// System.Single SCR_Menu/<AlphaLerp>c__Iterator2::<dTime>__2
	float ___U3CdTimeU3E__2_3;
	// System.Single SCR_Menu/<AlphaLerp>c__Iterator2::speed
	float ___speed_4;
	// System.Boolean SCR_Menu/<AlphaLerp>c__Iterator2::isRepeating
	bool ___isRepeating_5;
	// UnityEngine.Color SCR_Menu/<AlphaLerp>c__Iterator2::<newColor>__3
	Color_t65  ___U3CnewColorU3E__3_6;
	// System.Int32 SCR_Menu/<AlphaLerp>c__Iterator2::$PC
	int32_t ___U24PC_7;
	// System.Object SCR_Menu/<AlphaLerp>c__Iterator2::$current
	Object_t * ___U24current_8;
	// UnityEngine.UI.Text SCR_Menu/<AlphaLerp>c__Iterator2::<$>text
	Text_t316 * ___U3CU24U3Etext_9;
	// System.Single SCR_Menu/<AlphaLerp>c__Iterator2::<$>speed
	float ___U3CU24U3Espeed_10;
	// System.Boolean SCR_Menu/<AlphaLerp>c__Iterator2::<$>isRepeating
	bool ___U3CU24U3EisRepeating_11;
};
