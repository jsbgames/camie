﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t880;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t881;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.AudioClip
struct  AudioClip_t248  : public Object_t164
{
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t880 * ___m_PCMReaderCallback_2;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t881 * ___m_PCMSetPositionCallback_3;
};
