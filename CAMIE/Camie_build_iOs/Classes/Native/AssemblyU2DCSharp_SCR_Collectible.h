﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_Collectible
struct  SCR_Collectible_t331  : public MonoBehaviour_t3
{
	// System.Int32 SCR_Collectible::pointValue
	int32_t ___pointValue_2;
	// System.Boolean SCR_Collectible::isTaken
	bool ___isTaken_3;
};
