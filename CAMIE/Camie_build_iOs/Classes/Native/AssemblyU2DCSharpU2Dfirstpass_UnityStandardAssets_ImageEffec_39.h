﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t54;
// UnityEngine.Material
struct Material_t55;
// UnityStandardAssets.ImageEffects.PostEffectsBase
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_ImageEffec_1.h"
// UnityStandardAssets.ImageEffects.Fisheye
struct  Fisheye_t105  : public PostEffectsBase_t57
{
	// System.Single UnityStandardAssets.ImageEffects.Fisheye::strengthX
	float ___strengthX_5;
	// System.Single UnityStandardAssets.ImageEffects.Fisheye::strengthY
	float ___strengthY_6;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Fisheye::fishEyeShader
	Shader_t54 * ___fishEyeShader_7;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Fisheye::fisheyeMaterial
	Material_t55 * ___fisheyeMaterial_8;
};
