﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_JoystickDirection
struct SCR_JoystickDirection_t366;

// System.Void SCR_JoystickDirection::.ctor()
extern "C" void SCR_JoystickDirection__ctor_m1363 (SCR_JoystickDirection_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
