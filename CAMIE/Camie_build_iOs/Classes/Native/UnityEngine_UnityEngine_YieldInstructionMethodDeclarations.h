﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.YieldInstruction
struct YieldInstruction_t783;
struct YieldInstruction_t783_marshaled;

// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m4432 (YieldInstruction_t783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void YieldInstruction_t783_marshal(const YieldInstruction_t783& unmarshaled, YieldInstruction_t783_marshaled& marshaled);
void YieldInstruction_t783_marshal_back(const YieldInstruction_t783_marshaled& marshaled, YieldInstruction_t783& unmarshaled);
void YieldInstruction_t783_marshal_cleanup(YieldInstruction_t783_marshaled& marshaled);
