﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Double>
struct InternalEnumerator_1_t2849;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m14065_gshared (InternalEnumerator_1_t2849 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m14065(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2849 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14065_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14066_gshared (InternalEnumerator_1_t2849 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14066(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2849 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14066_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m14067_gshared (InternalEnumerator_1_t2849 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14067(__this, method) (( void (*) (InternalEnumerator_1_t2849 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14067_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m14068_gshared (InternalEnumerator_1_t2849 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14068(__this, method) (( bool (*) (InternalEnumerator_1_t2849 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14068_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern "C" double InternalEnumerator_1_get_Current_m14069_gshared (InternalEnumerator_1_t2849 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14069(__this, method) (( double (*) (InternalEnumerator_1_t2849 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14069_gshared)(__this, method)
