﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t667;
// System.Object
struct Object_t;
// UnityEngine.UI.Text
struct Text_t316;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Text>
struct IEnumerable_1_t3659;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Text>
struct IEnumerator_1_t3658;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<UnityEngine.UI.Text>
struct ICollection_1_t3660;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.Text>
struct ReadOnlyCollection_1_t3089;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t369;
// System.Predicate`1<UnityEngine.UI.Text>
struct Predicate_1_t3090;
// System.Comparison`1<UnityEngine.UI.Text>
struct Comparison_1_t3092;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_20.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m3167(__this, method) (( void (*) (List_1_t667 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m17510(__this, ___collection, method) (( void (*) (List_1_t667 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::.ctor(System.Int32)
#define List_1__ctor_m17511(__this, ___capacity, method) (( void (*) (List_1_t667 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::.cctor()
#define List_1__cctor_m17512(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17513(__this, method) (( Object_t* (*) (List_1_t667 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m17514(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t667 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17515(__this, method) (( Object_t * (*) (List_1_t667 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m17516(__this, ___item, method) (( int32_t (*) (List_1_t667 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m17517(__this, ___item, method) (( bool (*) (List_1_t667 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m17518(__this, ___item, method) (( int32_t (*) (List_1_t667 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m17519(__this, ___index, ___item, method) (( void (*) (List_1_t667 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m17520(__this, ___item, method) (( void (*) (List_1_t667 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17521(__this, method) (( bool (*) (List_1_t667 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17522(__this, method) (( bool (*) (List_1_t667 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m17523(__this, method) (( Object_t * (*) (List_1_t667 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m17524(__this, method) (( bool (*) (List_1_t667 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m17525(__this, method) (( bool (*) (List_1_t667 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m17526(__this, ___index, method) (( Object_t * (*) (List_1_t667 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m17527(__this, ___index, ___value, method) (( void (*) (List_1_t667 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Add(T)
#define List_1_Add_m17528(__this, ___item, method) (( void (*) (List_1_t667 *, Text_t316 *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m17529(__this, ___newCount, method) (( void (*) (List_1_t667 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m17530(__this, ___collection, method) (( void (*) (List_1_t667 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m17531(__this, ___enumerable, method) (( void (*) (List_1_t667 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m17532(__this, ___collection, method) (( void (*) (List_1_t667 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Text>::AsReadOnly()
#define List_1_AsReadOnly_m17533(__this, method) (( ReadOnlyCollection_1_t3089 * (*) (List_1_t667 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Clear()
#define List_1_Clear_m17534(__this, method) (( void (*) (List_1_t667 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::Contains(T)
#define List_1_Contains_m17535(__this, ___item, method) (( bool (*) (List_1_t667 *, Text_t316 *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m17536(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t667 *, TextU5BU5D_t369*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Text>::Find(System.Predicate`1<T>)
#define List_1_Find_m17537(__this, ___match, method) (( Text_t316 * (*) (List_1_t667 *, Predicate_1_t3090 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m17538(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3090 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m17539(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t667 *, int32_t, int32_t, Predicate_1_t3090 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.Text>::GetEnumerator()
#define List_1_GetEnumerator_m17540(__this, method) (( Enumerator_t3091  (*) (List_1_t667 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::IndexOf(T)
#define List_1_IndexOf_m17541(__this, ___item, method) (( int32_t (*) (List_1_t667 *, Text_t316 *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m17542(__this, ___start, ___delta, method) (( void (*) (List_1_t667 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m17543(__this, ___index, method) (( void (*) (List_1_t667 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Insert(System.Int32,T)
#define List_1_Insert_m17544(__this, ___index, ___item, method) (( void (*) (List_1_t667 *, int32_t, Text_t316 *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m17545(__this, ___collection, method) (( void (*) (List_1_t667 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::Remove(T)
#define List_1_Remove_m17546(__this, ___item, method) (( bool (*) (List_1_t667 *, Text_t316 *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m17547(__this, ___match, method) (( int32_t (*) (List_1_t667 *, Predicate_1_t3090 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m17548(__this, ___index, method) (( void (*) (List_1_t667 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Reverse()
#define List_1_Reverse_m17549(__this, method) (( void (*) (List_1_t667 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Sort()
#define List_1_Sort_m17550(__this, method) (( void (*) (List_1_t667 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m17551(__this, ___comparison, method) (( void (*) (List_1_t667 *, Comparison_1_t3092 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.Text>::ToArray()
#define List_1_ToArray_m17552(__this, method) (( TextU5BU5D_t369* (*) (List_1_t667 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::TrimExcess()
#define List_1_TrimExcess_m17553(__this, method) (( void (*) (List_1_t667 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::get_Capacity()
#define List_1_get_Capacity_m17554(__this, method) (( int32_t (*) (List_1_t667 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m17555(__this, ___value, method) (( void (*) (List_1_t667 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::get_Count()
#define List_1_get_Count_m17556(__this, method) (( int32_t (*) (List_1_t667 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Text>::get_Item(System.Int32)
#define List_1_get_Item_m17557(__this, ___index, method) (( Text_t316 * (*) (List_1_t667 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::set_Item(System.Int32,T)
#define List_1_set_Item_m17558(__this, ___index, ___value, method) (( void (*) (List_1_t667 *, int32_t, Text_t316 *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
