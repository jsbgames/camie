﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>
struct ShimEnumerator_t3417;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t3406;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m21915_gshared (ShimEnumerator_t3417 * __this, Dictionary_2_t3406 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m21915(__this, ___host, method) (( void (*) (ShimEnumerator_t3417 *, Dictionary_2_t3406 *, const MethodInfo*))ShimEnumerator__ctor_m21915_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m21916_gshared (ShimEnumerator_t3417 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m21916(__this, method) (( bool (*) (ShimEnumerator_t3417 *, const MethodInfo*))ShimEnumerator_MoveNext_m21916_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Entry()
extern "C" DictionaryEntry_t1430  ShimEnumerator_get_Entry_m21917_gshared (ShimEnumerator_t3417 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m21917(__this, method) (( DictionaryEntry_t1430  (*) (ShimEnumerator_t3417 *, const MethodInfo*))ShimEnumerator_get_Entry_m21917_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m21918_gshared (ShimEnumerator_t3417 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m21918(__this, method) (( Object_t * (*) (ShimEnumerator_t3417 *, const MethodInfo*))ShimEnumerator_get_Key_m21918_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m21919_gshared (ShimEnumerator_t3417 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m21919(__this, method) (( Object_t * (*) (ShimEnumerator_t3417 *, const MethodInfo*))ShimEnumerator_get_Value_m21919_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m21920_gshared (ShimEnumerator_t3417 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m21920(__this, method) (( Object_t * (*) (ShimEnumerator_t3417 *, const MethodInfo*))ShimEnumerator_get_Current_m21920_gshared)(__this, method)
