﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MultiKeyDictionary`3<System.Object,System.Object,System.Object>
struct MultiKeyDictionary_3_t2886;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2814;
// System.Object
struct Object_t;

// System.Void MultiKeyDictionary`3<System.Object,System.Object,System.Object>::.ctor()
extern "C" void MultiKeyDictionary_3__ctor_m14472_gshared (MultiKeyDictionary_3_t2886 * __this, const MethodInfo* method);
#define MultiKeyDictionary_3__ctor_m14472(__this, method) (( void (*) (MultiKeyDictionary_3_t2886 *, const MethodInfo*))MultiKeyDictionary_3__ctor_m14472_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2<T2,T3> MultiKeyDictionary`3<System.Object,System.Object,System.Object>::get_Item(T1)
extern "C" Dictionary_2_t2814 * MultiKeyDictionary_3_get_Item_m14473_gshared (MultiKeyDictionary_3_t2886 * __this, Object_t * ___key, const MethodInfo* method);
#define MultiKeyDictionary_3_get_Item_m14473(__this, ___key, method) (( Dictionary_2_t2814 * (*) (MultiKeyDictionary_3_t2886 *, Object_t *, const MethodInfo*))MultiKeyDictionary_3_get_Item_m14473_gshared)(__this, ___key, method)
// System.Boolean MultiKeyDictionary`3<System.Object,System.Object,System.Object>::ContainsKey(T1,T2)
extern "C" bool MultiKeyDictionary_3_ContainsKey_m14474_gshared (MultiKeyDictionary_3_t2886 * __this, Object_t * ___key1, Object_t * ___key2, const MethodInfo* method);
#define MultiKeyDictionary_3_ContainsKey_m14474(__this, ___key1, ___key2, method) (( bool (*) (MultiKeyDictionary_3_t2886 *, Object_t *, Object_t *, const MethodInfo*))MultiKeyDictionary_3_ContainsKey_m14474_gshared)(__this, ___key1, ___key2, method)
