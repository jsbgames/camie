﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Menu/<AlphaLerp>c__Iterator2
struct U3CAlphaLerpU3Ec__Iterator2_t332;
// System.Object
struct Object_t;

// System.Void SCR_Menu/<AlphaLerp>c__Iterator2::.ctor()
extern "C" void U3CAlphaLerpU3Ec__Iterator2__ctor_m1192 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Menu/<AlphaLerp>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAlphaLerpU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1193 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_Menu/<AlphaLerp>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAlphaLerpU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1194 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Menu/<AlphaLerp>c__Iterator2::MoveNext()
extern "C" bool U3CAlphaLerpU3Ec__Iterator2_MoveNext_m1195 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu/<AlphaLerp>c__Iterator2::Dispose()
extern "C" void U3CAlphaLerpU3Ec__Iterator2_Dispose_m1196 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Menu/<AlphaLerp>c__Iterator2::Reset()
extern "C" void U3CAlphaLerpU3Ec__Iterator2_Reset_m1197 (U3CAlphaLerpU3Ec__Iterator2_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
