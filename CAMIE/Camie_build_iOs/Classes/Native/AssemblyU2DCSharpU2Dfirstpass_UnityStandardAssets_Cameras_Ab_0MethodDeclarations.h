﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Cameras.AbstractTargetFollower
struct AbstractTargetFollower_t15;
// UnityEngine.Transform
struct Transform_t1;

// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::.ctor()
extern "C" void AbstractTargetFollower__ctor_m20 (AbstractTargetFollower_t15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::Start()
extern "C" void AbstractTargetFollower_Start_m21 (AbstractTargetFollower_t15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FixedUpdate()
extern "C" void AbstractTargetFollower_FixedUpdate_m22 (AbstractTargetFollower_t15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::LateUpdate()
extern "C" void AbstractTargetFollower_LateUpdate_m23 (AbstractTargetFollower_t15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::ManualUpdate()
extern "C" void AbstractTargetFollower_ManualUpdate_m24 (AbstractTargetFollower_t15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single)
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FindAndTargetPlayer()
extern "C" void AbstractTargetFollower_FindAndTargetPlayer_m25 (AbstractTargetFollower_t15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform)
extern "C" void AbstractTargetFollower_SetTarget_m26 (AbstractTargetFollower_t15 * __this, Transform_t1 * ___newTransform, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::get_Target()
extern "C" Transform_t1 * AbstractTargetFollower_get_Target_m27 (AbstractTargetFollower_t15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
