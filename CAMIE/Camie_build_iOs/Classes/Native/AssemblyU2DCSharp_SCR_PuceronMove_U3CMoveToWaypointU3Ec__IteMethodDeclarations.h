﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_PuceronMove/<MoveToWaypoint>c__Iterator9
struct U3CMoveToWaypointU3Ec__Iterator9_t360;
// System.Object
struct Object_t;

// System.Void SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::.ctor()
extern "C" void U3CMoveToWaypointU3Ec__Iterator9__ctor_m1315 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CMoveToWaypointU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1316 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CMoveToWaypointU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1317 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::MoveNext()
extern "C" bool U3CMoveToWaypointU3Ec__Iterator9_MoveNext_m1318 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::Dispose()
extern "C" void U3CMoveToWaypointU3Ec__Iterator9_Dispose_m1319 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_PuceronMove/<MoveToWaypoint>c__Iterator9::Reset()
extern "C" void U3CMoveToWaypointU3Ec__Iterator9_Reset_m1320 (U3CMoveToWaypointU3Ec__Iterator9_t360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
