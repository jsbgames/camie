﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfo.h"
// Metadata Definition System.NumberFormatter/CustomInfo
extern TypeInfo CustomInfo_t2230_il2cpp_TypeInfo;
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfoMethodDeclarations.h"
extern const Il2CppType Void_t272_0_0_0;
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter/CustomInfo::.ctor()
extern const MethodInfo CustomInfo__ctor_m12343_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CustomInfo__ctor_m12343/* method */
	, &CustomInfo_t2230_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5963/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t273_1_0_0;
extern const Il2CppType Boolean_t273_1_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Int32_t253_1_0_0;
extern const Il2CppType Int32_t253_1_0_0;
static const ParameterInfo CustomInfo_t2230_CustomInfo_GetActiveSection_m12344_ParameterInfos[] = 
{
	{"format", 0, 134225158, 0, &String_t_0_0_0},
	{"positive", 1, 134225159, 0, &Boolean_t273_1_0_0},
	{"zero", 2, 134225160, 0, &Boolean_t273_0_0_0},
	{"offset", 3, 134225161, 0, &Int32_t253_1_0_0},
	{"length", 4, 134225162, 0, &Int32_t253_1_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_BooleanU26_t745_SByte_t274_Int32U26_t753_Int32U26_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter/CustomInfo::GetActiveSection(System.String,System.Boolean&,System.Boolean,System.Int32&,System.Int32&)
extern const MethodInfo CustomInfo_GetActiveSection_m12344_MethodInfo = 
{
	"GetActiveSection"/* name */
	, (methodPointerType)&CustomInfo_GetActiveSection_m12344/* method */
	, &CustomInfo_t2230_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_BooleanU26_t745_SByte_t274_Int32U26_t753_Int32U26_t753/* invoker_method */
	, CustomInfo_t2230_CustomInfo_GetActiveSection_m12344_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5964/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo CustomInfo_t2230_CustomInfo_Parse_m12345_ParameterInfos[] = 
{
	{"format", 0, 134225163, 0, &String_t_0_0_0},
	{"offset", 1, 134225164, 0, &Int32_t253_0_0_0},
	{"length", 2, 134225165, 0, &Int32_t253_0_0_0},
	{"nfi", 3, 134225166, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern const Il2CppType CustomInfo_t2230_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter/CustomInfo System.NumberFormatter/CustomInfo::Parse(System.String,System.Int32,System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo CustomInfo_Parse_m12345_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&CustomInfo_Parse_m12345/* method */
	, &CustomInfo_t2230_il2cpp_TypeInfo/* declaring_type */
	, &CustomInfo_t2230_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t253_Int32_t253_Object_t/* invoker_method */
	, CustomInfo_t2230_CustomInfo_Parse_m12345_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5965/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
static const ParameterInfo CustomInfo_t2230_CustomInfo_Format_m12346_ParameterInfos[] = 
{
	{"format", 0, 134225167, 0, &String_t_0_0_0},
	{"offset", 1, 134225168, 0, &Int32_t253_0_0_0},
	{"length", 2, 134225169, 0, &Int32_t253_0_0_0},
	{"nfi", 3, 134225170, 0, &NumberFormatInfo_t1793_0_0_0},
	{"positive", 4, 134225171, 0, &Boolean_t273_0_0_0},
	{"sb_int", 5, 134225172, 0, &StringBuilder_t657_0_0_0},
	{"sb_dec", 6, 134225173, 0, &StringBuilder_t657_0_0_0},
	{"sb_exp", 7, 134225174, 0, &StringBuilder_t657_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Int32_t253_Object_t_SByte_t274_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter/CustomInfo::Format(System.String,System.Int32,System.Int32,System.Globalization.NumberFormatInfo,System.Boolean,System.Text.StringBuilder,System.Text.StringBuilder,System.Text.StringBuilder)
extern const MethodInfo CustomInfo_Format_m12346_MethodInfo = 
{
	"Format"/* name */
	, (methodPointerType)&CustomInfo_Format_m12346/* method */
	, &CustomInfo_t2230_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t253_Int32_t253_Object_t_SByte_t274_Object_t_Object_t_Object_t/* invoker_method */
	, CustomInfo_t2230_CustomInfo_Format_m12346_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 8/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5966/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CustomInfo_t2230_MethodInfos[] =
{
	&CustomInfo__ctor_m12343_MethodInfo,
	&CustomInfo_GetActiveSection_m12344_MethodInfo,
	&CustomInfo_Parse_m12345_MethodInfo,
	&CustomInfo_Format_m12346_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
static const Il2CppMethodReference CustomInfo_t2230_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool CustomInfo_t2230_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CustomInfo_t2230_1_0_0;
extern const Il2CppType Object_t_0_0_0;
extern TypeInfo NumberFormatter_t2231_il2cpp_TypeInfo;
extern const Il2CppType NumberFormatter_t2231_0_0_0;
struct CustomInfo_t2230;
const Il2CppTypeDefinitionMetadata CustomInfo_t2230_DefinitionMetadata = 
{
	&NumberFormatter_t2231_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CustomInfo_t2230_VTable/* vtableMethods */
	, CustomInfo_t2230_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2972/* fieldStart */

};
TypeInfo CustomInfo_t2230_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CustomInfo"/* name */
	, ""/* namespaze */
	, CustomInfo_t2230_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CustomInfo_t2230_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CustomInfo_t2230_0_0_0/* byval_arg */
	, &CustomInfo_t2230_1_0_0/* this_arg */
	, &CustomInfo_t2230_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CustomInfo_t2230)/* instance_size */
	, sizeof (CustomInfo_t2230)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.NumberFormatter
#include "mscorlib_System_NumberFormatter.h"
// Metadata Definition System.NumberFormatter
// System.NumberFormatter
#include "mscorlib_System_NumberFormatterMethodDeclarations.h"
extern const Il2CppType Thread_t412_0_0_0;
extern const Il2CppType Thread_t412_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter__ctor_m12347_ParameterInfos[] = 
{
	{"current", 0, 134225008, 0, &Thread_t412_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::.ctor(System.Threading.Thread)
extern const MethodInfo NumberFormatter__ctor_m12347_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NumberFormatter__ctor_m12347/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter__ctor_m12347_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::.cctor()
extern const MethodInfo NumberFormatter__cctor_m12348_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&NumberFormatter__cctor_m12348/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64U2A_t2781_1_0_2;
extern const Il2CppType UInt64U2A_t2781_1_0_0;
extern const Il2CppType Int32U2A_t2782_1_0_2;
extern const Il2CppType Int32U2A_t2782_1_0_0;
extern const Il2CppType CharU2A_t2432_1_0_2;
extern const Il2CppType CharU2A_t2432_1_0_0;
extern const Il2CppType CharU2A_t2432_1_0_2;
extern const Il2CppType Int64U2A_t2783_1_0_2;
extern const Il2CppType Int64U2A_t2783_1_0_0;
extern const Il2CppType Int32U2A_t2782_1_0_2;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_GetFormatterTables_m12349_ParameterInfos[] = 
{
	{"MantissaBitsTable", 0, 134225009, 0, &UInt64U2A_t2781_1_0_2},
	{"TensExponentTable", 1, 134225010, 0, &Int32U2A_t2782_1_0_2},
	{"DigitLowerTable", 2, 134225011, 0, &CharU2A_t2432_1_0_2},
	{"DigitUpperTable", 3, 134225012, 0, &CharU2A_t2432_1_0_2},
	{"TenPowersList", 4, 134225013, 0, &Int64U2A_t2783_1_0_2},
	{"DecHexDigits", 5, 134225014, 0, &Int32U2A_t2782_1_0_2},
};
extern void* RuntimeInvoker_Void_t272_UInt64U2AU26_t2784_Int32U2AU26_t2785_CharU2AU26_t2786_CharU2AU26_t2786_Int64U2AU26_t2787_Int32U2AU26_t2785 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::GetFormatterTables(System.UInt64*&,System.Int32*&,System.Char*&,System.Char*&,System.Int64*&,System.Int32*&)
extern const MethodInfo NumberFormatter_GetFormatterTables_m12349_MethodInfo = 
{
	"GetFormatterTables"/* name */
	, (methodPointerType)&NumberFormatter_GetFormatterTables_m12349/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_UInt64U2AU26_t2784_Int32U2AU26_t2785_CharU2AU26_t2786_CharU2AU26_t2786_Int64U2AU26_t2787_Int32U2AU26_t2785/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_GetFormatterTables_m12349_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_GetTenPowerOf_m12350_ParameterInfos[] = 
{
	{"i", 0, 134225015, 0, &Int32_t253_0_0_0},
};
extern const Il2CppType Int64_t1071_0_0_0;
extern void* RuntimeInvoker_Int64_t1071_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.NumberFormatter::GetTenPowerOf(System.Int32)
extern const MethodInfo NumberFormatter_GetTenPowerOf_m12350_MethodInfo = 
{
	"GetTenPowerOf"/* name */
	, (methodPointerType)&NumberFormatter_GetTenPowerOf_m12350/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1071_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1071_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_GetTenPowerOf_m12350_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1063_0_0_0;
extern const Il2CppType UInt32_t1063_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_InitDecHexDigits_m12351_ParameterInfos[] = 
{
	{"value", 0, 134225016, 0, &UInt32_t1063_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt32)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m12351_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m12351/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_InitDecHexDigits_m12351_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t1074_0_0_0;
extern const Il2CppType UInt64_t1074_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_InitDecHexDigits_m12352_ParameterInfos[] = 
{
	{"value", 0, 134225017, 0, &UInt64_t1074_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt64)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m12352_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m12352/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int64_t1071/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_InitDecHexDigits_m12352_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1063_0_0_0;
extern const Il2CppType UInt64_t1074_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_InitDecHexDigits_m12353_ParameterInfos[] = 
{
	{"hi", 0, 134225018, 0, &UInt32_t1063_0_0_0},
	{"lo", 1, 134225019, 0, &UInt64_t1074_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt32,System.UInt64)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m12353_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m12353/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int64_t1071/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_InitDecHexDigits_m12353_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FastToDecHex_m12354_ParameterInfos[] = 
{
	{"val", 0, 134225020, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1063_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::FastToDecHex(System.Int32)
extern const MethodInfo NumberFormatter_FastToDecHex_m12354_MethodInfo = 
{
	"FastToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_FastToDecHex_m12354/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1063_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1063_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FastToDecHex_m12354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_ToDecHex_m12355_ParameterInfos[] = 
{
	{"val", 0, 134225021, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1063_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::ToDecHex(System.Int32)
extern const MethodInfo NumberFormatter_ToDecHex_m12355_MethodInfo = 
{
	"ToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_ToDecHex_m12355/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1063_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1063_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_ToDecHex_m12355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FastDecHexLen_m12356_ParameterInfos[] = 
{
	{"val", 0, 134225022, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::FastDecHexLen(System.Int32)
extern const MethodInfo NumberFormatter_FastDecHexLen_m12356_MethodInfo = 
{
	"FastDecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_FastDecHexLen_m12356/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FastDecHexLen_m12356_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1063_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_DecHexLen_m12357_ParameterInfos[] = 
{
	{"val", 0, 134225023, 0, &UInt32_t1063_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::DecHexLen(System.UInt32)
extern const MethodInfo NumberFormatter_DecHexLen_m12357_MethodInfo = 
{
	"DecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_DecHexLen_m12357/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_DecHexLen_m12357_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::DecHexLen()
extern const MethodInfo NumberFormatter_DecHexLen_m12358_MethodInfo = 
{
	"DecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_DecHexLen_m12358/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_ScaleOrder_m12359_ParameterInfos[] = 
{
	{"hi", 0, 134225024, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::ScaleOrder(System.Int64)
extern const MethodInfo NumberFormatter_ScaleOrder_m12359_MethodInfo = 
{
	"ScaleOrder"/* name */
	, (methodPointerType)&NumberFormatter_ScaleOrder_m12359/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int64_t1071/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_ScaleOrder_m12359_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::InitialFloatingPrecision()
extern const MethodInfo NumberFormatter_InitialFloatingPrecision_m12360_MethodInfo = 
{
	"InitialFloatingPrecision"/* name */
	, (methodPointerType)&NumberFormatter_InitialFloatingPrecision_m12360/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_ParsePrecision_m12361_ParameterInfos[] = 
{
	{"format", 0, 134225025, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::ParsePrecision(System.String)
extern const MethodInfo NumberFormatter_ParsePrecision_m12361_MethodInfo = 
{
	"ParsePrecision"/* name */
	, (methodPointerType)&NumberFormatter_ParsePrecision_m12361/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_ParsePrecision_m12361_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Init_m12362_ParameterInfos[] = 
{
	{"format", 0, 134225026, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String)
extern const MethodInfo NumberFormatter_Init_m12362_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m12362/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Init_m12362_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t1074_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_InitHex_m12363_ParameterInfos[] = 
{
	{"value", 0, 134225027, 0, &UInt64_t1074_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitHex(System.UInt64)
extern const MethodInfo NumberFormatter_InitHex_m12363_MethodInfo = 
{
	"InitHex"/* name */
	, (methodPointerType)&NumberFormatter_InitHex_m12363/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int64_t1071/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_InitHex_m12363_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Init_m12364_ParameterInfos[] = 
{
	{"format", 0, 134225028, 0, &String_t_0_0_0},
	{"value", 1, 134225029, 0, &Int32_t253_0_0_0},
	{"defPrecision", 2, 134225030, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_Init_m12364_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m12364/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Init_m12364_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt32_t1063_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Init_m12365_ParameterInfos[] = 
{
	{"format", 0, 134225031, 0, &String_t_0_0_0},
	{"value", 1, 134225032, 0, &UInt32_t1063_0_0_0},
	{"defPrecision", 2, 134225033, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.UInt32,System.Int32)
extern const MethodInfo NumberFormatter_Init_m12365_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m12365/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Init_m12365_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Init_m12366_ParameterInfos[] = 
{
	{"format", 0, 134225034, 0, &String_t_0_0_0},
	{"value", 1, 134225035, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Int64)
extern const MethodInfo NumberFormatter_Init_m12366_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m12366/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Init_m12366_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt64_t1074_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Init_m12367_ParameterInfos[] = 
{
	{"format", 0, 134225036, 0, &String_t_0_0_0},
	{"value", 1, 134225037, 0, &UInt64_t1074_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.UInt64)
extern const MethodInfo NumberFormatter_Init_m12367_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m12367/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int64_t1071/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Init_m12367_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1070_0_0_0;
extern const Il2CppType Double_t1070_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Init_m12368_ParameterInfos[] = 
{
	{"format", 0, 134225038, 0, &String_t_0_0_0},
	{"value", 1, 134225039, 0, &Double_t1070_0_0_0},
	{"defPrecision", 2, 134225040, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Double_t1070_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Double,System.Int32)
extern const MethodInfo NumberFormatter_Init_m12368_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m12368/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Double_t1070_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Init_m12368_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Decimal_t1073_0_0_0;
extern const Il2CppType Decimal_t1073_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Init_m12369_ParameterInfos[] = 
{
	{"format", 0, 134225041, 0, &String_t_0_0_0},
	{"value", 1, 134225042, 0, &Decimal_t1073_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Decimal_t1073 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Decimal)
extern const MethodInfo NumberFormatter_Init_m12369_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m12369/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Decimal_t1073/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Init_m12369_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_ResetCharBuf_m12370_ParameterInfos[] = 
{
	{"size", 0, 134225043, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::ResetCharBuf(System.Int32)
extern const MethodInfo NumberFormatter_ResetCharBuf_m12370_MethodInfo = 
{
	"ResetCharBuf"/* name */
	, (methodPointerType)&NumberFormatter_ResetCharBuf_m12370/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_ResetCharBuf_m12370_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Resize_m12371_ParameterInfos[] = 
{
	{"len", 0, 134225044, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Resize(System.Int32)
extern const MethodInfo NumberFormatter_Resize_m12371_MethodInfo = 
{
	"Resize"/* name */
	, (methodPointerType)&NumberFormatter_Resize_m12371/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Resize_m12371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType Char_t682_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Append_m12372_ParameterInfos[] = 
{
	{"c", 0, 134225045, 0, &Char_t682_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.Char)
extern const MethodInfo NumberFormatter_Append_m12372_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m12372/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Append_m12372_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t682_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Append_m12373_ParameterInfos[] = 
{
	{"c", 0, 134225046, 0, &Char_t682_0_0_0},
	{"cnt", 1, 134225047, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.Char,System.Int32)
extern const MethodInfo NumberFormatter_Append_m12373_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m12373/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Append_m12373_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Append_m12374_ParameterInfos[] = 
{
	{"s", 0, 134225048, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.String)
extern const MethodInfo NumberFormatter_Append_m12374_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m12374/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Append_m12374_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IFormatProvider_t2275_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_GetNumberFormatInstance_m12375_ParameterInfos[] = 
{
	{"fp", 0, 134225049, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Globalization.NumberFormatInfo System.NumberFormatter::GetNumberFormatInstance(System.IFormatProvider)
extern const MethodInfo NumberFormatter_GetNumberFormatInstance_m12375_MethodInfo = 
{
	"GetNumberFormatInstance"/* name */
	, (methodPointerType)&NumberFormatter_GetNumberFormatInstance_m12375/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatInfo_t1793_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_GetNumberFormatInstance_m12375_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CultureInfo_t1068_0_0_0;
extern const Il2CppType CultureInfo_t1068_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_set_CurrentCulture_m12376_ParameterInfos[] = 
{
	{"value", 0, 134225050, 0, &CultureInfo_t1068_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::set_CurrentCulture(System.Globalization.CultureInfo)
extern const MethodInfo NumberFormatter_set_CurrentCulture_m12376_MethodInfo = 
{
	"set_CurrentCulture"/* name */
	, (methodPointerType)&NumberFormatter_set_CurrentCulture_m12376/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_set_CurrentCulture_m12376_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::get_IntegerDigits()
extern const MethodInfo NumberFormatter_get_IntegerDigits_m12377_MethodInfo = 
{
	"get_IntegerDigits"/* name */
	, (methodPointerType)&NumberFormatter_get_IntegerDigits_m12377/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::get_DecimalDigits()
extern const MethodInfo NumberFormatter_get_DecimalDigits_m12378_MethodInfo = 
{
	"get_DecimalDigits"/* name */
	, (methodPointerType)&NumberFormatter_get_DecimalDigits_m12378/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsFloatingSource()
extern const MethodInfo NumberFormatter_get_IsFloatingSource_m12379_MethodInfo = 
{
	"get_IsFloatingSource"/* name */
	, (methodPointerType)&NumberFormatter_get_IsFloatingSource_m12379/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsZero()
extern const MethodInfo NumberFormatter_get_IsZero_m12380_MethodInfo = 
{
	"get_IsZero"/* name */
	, (methodPointerType)&NumberFormatter_get_IsZero_m12380/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsZeroInteger()
extern const MethodInfo NumberFormatter_get_IsZeroInteger_m12381_MethodInfo = 
{
	"get_IsZeroInteger"/* name */
	, (methodPointerType)&NumberFormatter_get_IsZeroInteger_m12381/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_RoundPos_m12382_ParameterInfos[] = 
{
	{"pos", 0, 134225051, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::RoundPos(System.Int32)
extern const MethodInfo NumberFormatter_RoundPos_m12382_MethodInfo = 
{
	"RoundPos"/* name */
	, (methodPointerType)&NumberFormatter_RoundPos_m12382/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_RoundPos_m12382_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_RoundDecimal_m12383_ParameterInfos[] = 
{
	{"decimals", 0, 134225052, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::RoundDecimal(System.Int32)
extern const MethodInfo NumberFormatter_RoundDecimal_m12383_MethodInfo = 
{
	"RoundDecimal"/* name */
	, (methodPointerType)&NumberFormatter_RoundDecimal_m12383/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_RoundDecimal_m12383_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_RoundBits_m12384_ParameterInfos[] = 
{
	{"shift", 0, 134225053, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::RoundBits(System.Int32)
extern const MethodInfo NumberFormatter_RoundBits_m12384_MethodInfo = 
{
	"RoundBits"/* name */
	, (methodPointerType)&NumberFormatter_RoundBits_m12384/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_RoundBits_m12384_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::RemoveTrailingZeros()
extern const MethodInfo NumberFormatter_RemoveTrailingZeros_m12385_MethodInfo = 
{
	"RemoveTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_RemoveTrailingZeros_m12385/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AddOneToDecHex()
extern const MethodInfo NumberFormatter_AddOneToDecHex_m12386_MethodInfo = 
{
	"AddOneToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_AddOneToDecHex_m12386/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1063_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_AddOneToDecHex_m12387_ParameterInfos[] = 
{
	{"val", 0, 134225054, 0, &UInt32_t1063_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1063_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::AddOneToDecHex(System.UInt32)
extern const MethodInfo NumberFormatter_AddOneToDecHex_m12387_MethodInfo = 
{
	"AddOneToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_AddOneToDecHex_m12387/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1063_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1063_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_AddOneToDecHex_m12387_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::CountTrailingZeros()
extern const MethodInfo NumberFormatter_CountTrailingZeros_m12388_MethodInfo = 
{
	"CountTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_CountTrailingZeros_m12388/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1063_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_CountTrailingZeros_m12389_ParameterInfos[] = 
{
	{"val", 0, 134225055, 0, &UInt32_t1063_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::CountTrailingZeros(System.UInt32)
extern const MethodInfo NumberFormatter_CountTrailingZeros_m12389_MethodInfo = 
{
	"CountTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_CountTrailingZeros_m12389/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_CountTrailingZeros_m12389_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter System.NumberFormatter::GetInstance()
extern const MethodInfo NumberFormatter_GetInstance_m12390_MethodInfo = 
{
	"GetInstance"/* name */
	, (methodPointerType)&NumberFormatter_GetInstance_m12390/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatter_t2231_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Release()
extern const MethodInfo NumberFormatter_Release_m12391_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&NumberFormatter_Release_m12391/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CultureInfo_t1068_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_SetThreadCurrentCulture_m12392_ParameterInfos[] = 
{
	{"culture", 0, 134225056, 0, &CultureInfo_t1068_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::SetThreadCurrentCulture(System.Globalization.CultureInfo)
extern const MethodInfo NumberFormatter_SetThreadCurrentCulture_m12392_MethodInfo = 
{
	"SetThreadCurrentCulture"/* name */
	, (methodPointerType)&NumberFormatter_SetThreadCurrentCulture_m12392/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_SetThreadCurrentCulture_m12392_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType SByte_t274_0_0_0;
extern const Il2CppType SByte_t274_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12393_ParameterInfos[] = 
{
	{"format", 0, 134225057, 0, &String_t_0_0_0},
	{"value", 1, 134225058, 0, &SByte_t274_0_0_0},
	{"fp", 2, 134225059, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.SByte,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12393_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12393/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Byte_t680_0_0_0;
extern const Il2CppType Byte_t680_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12394_ParameterInfos[] = 
{
	{"format", 0, 134225060, 0, &String_t_0_0_0},
	{"value", 1, 134225061, 0, &Byte_t680_0_0_0},
	{"fp", 2, 134225062, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Byte,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12394_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12394/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12394_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt16_t684_0_0_0;
extern const Il2CppType UInt16_t684_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12395_ParameterInfos[] = 
{
	{"format", 0, 134225063, 0, &String_t_0_0_0},
	{"value", 1, 134225064, 0, &UInt16_t684_0_0_0},
	{"fp", 2, 134225065, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int16_t752_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt16,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12395_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12395/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int16_t752_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12395_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int16_t752_0_0_0;
extern const Il2CppType Int16_t752_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12396_ParameterInfos[] = 
{
	{"format", 0, 134225066, 0, &String_t_0_0_0},
	{"value", 1, 134225067, 0, &Int16_t752_0_0_0},
	{"fp", 2, 134225068, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int16_t752_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int16,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12396_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12396/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int16_t752_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12396_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt32_t1063_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12397_ParameterInfos[] = 
{
	{"format", 0, 134225069, 0, &String_t_0_0_0},
	{"value", 1, 134225070, 0, &UInt32_t1063_0_0_0},
	{"fp", 2, 134225071, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12397_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12397/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12397_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12398_ParameterInfos[] = 
{
	{"format", 0, 134225072, 0, &String_t_0_0_0},
	{"value", 1, 134225073, 0, &Int32_t253_0_0_0},
	{"fp", 2, 134225074, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12398_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12398/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12398_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt64_t1074_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12399_ParameterInfos[] = 
{
	{"format", 0, 134225075, 0, &String_t_0_0_0},
	{"value", 1, 134225076, 0, &UInt64_t1074_0_0_0},
	{"fp", 2, 134225077, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12399_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12399/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int64_t1071_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12399_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12400_ParameterInfos[] = 
{
	{"format", 0, 134225078, 0, &String_t_0_0_0},
	{"value", 1, 134225079, 0, &Int64_t1071_0_0_0},
	{"fp", 2, 134225080, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12400_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12400/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int64_t1071_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12400_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12401_ParameterInfos[] = 
{
	{"format", 0, 134225081, 0, &String_t_0_0_0},
	{"value", 1, 134225082, 0, &Single_t254_0_0_0},
	{"fp", 2, 134225083, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Single,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12401_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12401/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t254_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12401_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1070_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12402_ParameterInfos[] = 
{
	{"format", 0, 134225084, 0, &String_t_0_0_0},
	{"value", 1, 134225085, 0, &Double_t1070_0_0_0},
	{"fp", 2, 134225086, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Double_t1070_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Double,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12402_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12402/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Double_t1070_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12402_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Decimal_t1073_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12403_ParameterInfos[] = 
{
	{"format", 0, 134225087, 0, &String_t_0_0_0},
	{"value", 1, 134225088, 0, &Decimal_t1073_0_0_0},
	{"fp", 2, 134225089, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Decimal_t1073_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Decimal,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12403_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12403/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Decimal_t1073_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12403_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1063_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12404_ParameterInfos[] = 
{
	{"value", 0, 134225090, 0, &UInt32_t1063_0_0_0},
	{"fp", 1, 134225091, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.UInt32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12404_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12404/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12404_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12405_ParameterInfos[] = 
{
	{"value", 0, 134225092, 0, &Int32_t253_0_0_0},
	{"fp", 1, 134225093, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12405_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12405/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12405_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t1074_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12406_ParameterInfos[] = 
{
	{"value", 0, 134225094, 0, &UInt64_t1074_0_0_0},
	{"fp", 1, 134225095, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.UInt64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12406_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12406/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t1071_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12406_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12407_ParameterInfos[] = 
{
	{"value", 0, 134225096, 0, &Int64_t1071_0_0_0},
	{"fp", 1, 134225097, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t1071_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Int64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12407_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12407/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t1071_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12407_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12408_ParameterInfos[] = 
{
	{"value", 0, 134225098, 0, &Single_t254_0_0_0},
	{"fp", 1, 134225099, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Single,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12408_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12408/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t254_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12408_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1070_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12409_ParameterInfos[] = 
{
	{"value", 0, 134225100, 0, &Double_t1070_0_0_0},
	{"fp", 1, 134225101, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Double_t1070_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Double,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m12409_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12409/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Double_t1070_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12409_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FastIntegerToString_m12410_ParameterInfos[] = 
{
	{"value", 0, 134225102, 0, &Int32_t253_0_0_0},
	{"fp", 1, 134225103, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FastIntegerToString(System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_FastIntegerToString_m12410_MethodInfo = 
{
	"FastIntegerToString"/* name */
	, (methodPointerType)&NumberFormatter_FastIntegerToString_m12410/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FastIntegerToString_m12410_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IFormatProvider_t2275_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_IntegerToString_m12411_ParameterInfos[] = 
{
	{"format", 0, 134225104, 0, &String_t_0_0_0},
	{"fp", 1, 134225105, 0, &IFormatProvider_t2275_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::IntegerToString(System.String,System.IFormatProvider)
extern const MethodInfo NumberFormatter_IntegerToString_m12411_MethodInfo = 
{
	"IntegerToString"/* name */
	, (methodPointerType)&NumberFormatter_IntegerToString_m12411/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_IntegerToString_m12411_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_NumberToString_m12412_ParameterInfos[] = 
{
	{"format", 0, 134225106, 0, &String_t_0_0_0},
	{"nfi", 1, 134225107, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_NumberToString_m12412_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m12412/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_NumberToString_m12412_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatCurrency_m12413_ParameterInfos[] = 
{
	{"precision", 0, 134225108, 0, &Int32_t253_0_0_0},
	{"nfi", 1, 134225109, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatCurrency(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatCurrency_m12413_MethodInfo = 
{
	"FormatCurrency"/* name */
	, (methodPointerType)&NumberFormatter_FormatCurrency_m12413/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatCurrency_m12413_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatDecimal_m12414_ParameterInfos[] = 
{
	{"precision", 0, 134225110, 0, &Int32_t253_0_0_0},
	{"nfi", 1, 134225111, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatDecimal(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatDecimal_m12414_MethodInfo = 
{
	"FormatDecimal"/* name */
	, (methodPointerType)&NumberFormatter_FormatDecimal_m12414/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatDecimal_m12414_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatHexadecimal_m12415_ParameterInfos[] = 
{
	{"precision", 0, 134225112, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatHexadecimal(System.Int32)
extern const MethodInfo NumberFormatter_FormatHexadecimal_m12415_MethodInfo = 
{
	"FormatHexadecimal"/* name */
	, (methodPointerType)&NumberFormatter_FormatHexadecimal_m12415/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatHexadecimal_m12415_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatFixedPoint_m12416_ParameterInfos[] = 
{
	{"precision", 0, 134225113, 0, &Int32_t253_0_0_0},
	{"nfi", 1, 134225114, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatFixedPoint(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatFixedPoint_m12416_MethodInfo = 
{
	"FormatFixedPoint"/* name */
	, (methodPointerType)&NumberFormatter_FormatFixedPoint_m12416/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatFixedPoint_m12416_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1070_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatRoundtrip_m12417_ParameterInfos[] = 
{
	{"origval", 0, 134225115, 0, &Double_t1070_0_0_0},
	{"nfi", 1, 134225116, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Double_t1070_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatRoundtrip(System.Double,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatRoundtrip_m12417_MethodInfo = 
{
	"FormatRoundtrip"/* name */
	, (methodPointerType)&NumberFormatter_FormatRoundtrip_m12417/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Double_t1070_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatRoundtrip_m12417_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t254_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatRoundtrip_m12418_ParameterInfos[] = 
{
	{"origval", 0, 134225117, 0, &Single_t254_0_0_0},
	{"nfi", 1, 134225118, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t254_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatRoundtrip(System.Single,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatRoundtrip_m12418_MethodInfo = 
{
	"FormatRoundtrip"/* name */
	, (methodPointerType)&NumberFormatter_FormatRoundtrip_m12418/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t254_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatRoundtrip_m12418_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatGeneral_m12419_ParameterInfos[] = 
{
	{"precision", 0, 134225119, 0, &Int32_t253_0_0_0},
	{"nfi", 1, 134225120, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatGeneral(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatGeneral_m12419_MethodInfo = 
{
	"FormatGeneral"/* name */
	, (methodPointerType)&NumberFormatter_FormatGeneral_m12419/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatGeneral_m12419_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatNumber_m12420_ParameterInfos[] = 
{
	{"precision", 0, 134225121, 0, &Int32_t253_0_0_0},
	{"nfi", 1, 134225122, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatNumber(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatNumber_m12420_MethodInfo = 
{
	"FormatNumber"/* name */
	, (methodPointerType)&NumberFormatter_FormatNumber_m12420/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatNumber_m12420_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatPercent_m12421_ParameterInfos[] = 
{
	{"precision", 0, 134225123, 0, &Int32_t253_0_0_0},
	{"nfi", 1, 134225124, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatPercent(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatPercent_m12421_MethodInfo = 
{
	"FormatPercent"/* name */
	, (methodPointerType)&NumberFormatter_FormatPercent_m12421/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatPercent_m12421_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatExponential_m12422_ParameterInfos[] = 
{
	{"precision", 0, 134225125, 0, &Int32_t253_0_0_0},
	{"nfi", 1, 134225126, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatExponential(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatExponential_m12422_MethodInfo = 
{
	"FormatExponential"/* name */
	, (methodPointerType)&NumberFormatter_FormatExponential_m12422/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatExponential_m12422_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatExponential_m12423_ParameterInfos[] = 
{
	{"precision", 0, 134225127, 0, &Int32_t253_0_0_0},
	{"nfi", 1, 134225128, 0, &NumberFormatInfo_t1793_0_0_0},
	{"expDigits", 2, 134225129, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatExponential(System.Int32,System.Globalization.NumberFormatInfo,System.Int32)
extern const MethodInfo NumberFormatter_FormatExponential_m12423_MethodInfo = 
{
	"FormatExponential"/* name */
	, (methodPointerType)&NumberFormatter_FormatExponential_m12423/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253_Object_t_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatExponential_m12423_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FormatCustom_m12424_ParameterInfos[] = 
{
	{"format", 0, 134225130, 0, &String_t_0_0_0},
	{"nfi", 1, 134225131, 0, &NumberFormatInfo_t1793_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatCustom(System.String,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatCustom_m12424_MethodInfo = 
{
	"FormatCustom"/* name */
	, (methodPointerType)&NumberFormatter_FormatCustom_m12424/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FormatCustom_m12424_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t657_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_ZeroTrimEnd_m12425_ParameterInfos[] = 
{
	{"sb", 0, 134225132, 0, &StringBuilder_t657_0_0_0},
	{"canEmpty", 1, 134225133, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::ZeroTrimEnd(System.Text.StringBuilder,System.Boolean)
extern const MethodInfo NumberFormatter_ZeroTrimEnd_m12425_MethodInfo = 
{
	"ZeroTrimEnd"/* name */
	, (methodPointerType)&NumberFormatter_ZeroTrimEnd_m12425/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_ZeroTrimEnd_m12425_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t657_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_IsZeroOnly_m12426_ParameterInfos[] = 
{
	{"sb", 0, 134225134, 0, &StringBuilder_t657_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::IsZeroOnly(System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_IsZeroOnly_m12426_MethodInfo = 
{
	"IsZeroOnly"/* name */
	, (methodPointerType)&NumberFormatter_IsZeroOnly_m12426/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_IsZeroOnly_m12426_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t657_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_AppendNonNegativeNumber_m12427_ParameterInfos[] = 
{
	{"sb", 0, 134225135, 0, &StringBuilder_t657_0_0_0},
	{"v", 1, 134225136, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendNonNegativeNumber(System.Text.StringBuilder,System.Int32)
extern const MethodInfo NumberFormatter_AppendNonNegativeNumber_m12427_MethodInfo = 
{
	"AppendNonNegativeNumber"/* name */
	, (methodPointerType)&NumberFormatter_AppendNonNegativeNumber_m12427/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_AppendNonNegativeNumber_m12427_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_AppendIntegerString_m12428_ParameterInfos[] = 
{
	{"minLength", 0, 134225137, 0, &Int32_t253_0_0_0},
	{"sb", 1, 134225138, 0, &StringBuilder_t657_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerString(System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendIntegerString_m12428_MethodInfo = 
{
	"AppendIntegerString"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerString_m12428/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_AppendIntegerString_m12428_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_AppendIntegerString_m12429_ParameterInfos[] = 
{
	{"minLength", 0, 134225139, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerString(System.Int32)
extern const MethodInfo NumberFormatter_AppendIntegerString_m12429_MethodInfo = 
{
	"AppendIntegerString"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerString_m12429/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_AppendIntegerString_m12429_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_AppendDecimalString_m12430_ParameterInfos[] = 
{
	{"precision", 0, 134225140, 0, &Int32_t253_0_0_0},
	{"sb", 1, 134225141, 0, &StringBuilder_t657_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDecimalString(System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendDecimalString_m12430_MethodInfo = 
{
	"AppendDecimalString"/* name */
	, (methodPointerType)&NumberFormatter_AppendDecimalString_m12430/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_AppendDecimalString_m12430_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_AppendDecimalString_m12431_ParameterInfos[] = 
{
	{"precision", 0, 134225142, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDecimalString(System.Int32)
extern const MethodInfo NumberFormatter_AppendDecimalString_m12431_MethodInfo = 
{
	"AppendDecimalString"/* name */
	, (methodPointerType)&NumberFormatter_AppendDecimalString_m12431/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_AppendDecimalString_m12431_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_AppendIntegerStringWithGroupSeparator_m12432_ParameterInfos[] = 
{
	{"groups", 0, 134225143, 0, &Int32U5BU5D_t242_0_0_0},
	{"groupSeparator", 1, 134225144, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerStringWithGroupSeparator(System.Int32[],System.String)
extern const MethodInfo NumberFormatter_AppendIntegerStringWithGroupSeparator_m12432_MethodInfo = 
{
	"AppendIntegerStringWithGroupSeparator"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerStringWithGroupSeparator_m12432/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_AppendIntegerStringWithGroupSeparator_m12432_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NumberFormatInfo_t1793_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_AppendExponent_m12433_ParameterInfos[] = 
{
	{"nfi", 0, 134225145, 0, &NumberFormatInfo_t1793_0_0_0},
	{"exponent", 1, 134225146, 0, &Int32_t253_0_0_0},
	{"minDigits", 2, 134225147, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendExponent(System.Globalization.NumberFormatInfo,System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_AppendExponent_m12433_MethodInfo = 
{
	"AppendExponent"/* name */
	, (methodPointerType)&NumberFormatter_AppendExponent_m12433/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Int32_t253_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_AppendExponent_m12433_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5955/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_AppendOneDigit_m12434_ParameterInfos[] = 
{
	{"start", 0, 134225148, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendOneDigit(System.Int32)
extern const MethodInfo NumberFormatter_AppendOneDigit_m12434_MethodInfo = 
{
	"AppendOneDigit"/* name */
	, (methodPointerType)&NumberFormatter_AppendOneDigit_m12434/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_AppendOneDigit_m12434_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5956/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_FastAppendDigits_m12435_ParameterInfos[] = 
{
	{"val", 0, 134225149, 0, &Int32_t253_0_0_0},
	{"force", 1, 134225150, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::FastAppendDigits(System.Int32,System.Boolean)
extern const MethodInfo NumberFormatter_FastAppendDigits_m12435_MethodInfo = 
{
	"FastAppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_FastAppendDigits_m12435/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_SByte_t274/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_FastAppendDigits_m12435_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5957/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_AppendDigits_m12436_ParameterInfos[] = 
{
	{"start", 0, 134225151, 0, &Int32_t253_0_0_0},
	{"end", 1, 134225152, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDigits(System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_AppendDigits_m12436_MethodInfo = 
{
	"AppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_AppendDigits_m12436/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_AppendDigits_m12436_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5958/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType StringBuilder_t657_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_AppendDigits_m12437_ParameterInfos[] = 
{
	{"start", 0, 134225153, 0, &Int32_t253_0_0_0},
	{"end", 1, 134225154, 0, &Int32_t253_0_0_0},
	{"sb", 2, 134225155, 0, &StringBuilder_t657_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDigits(System.Int32,System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendDigits_m12437_MethodInfo = 
{
	"AppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_AppendDigits_m12437/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Object_t/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_AppendDigits_m12437_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5959/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Multiply10_m12438_ParameterInfos[] = 
{
	{"count", 0, 134225156, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Multiply10(System.Int32)
extern const MethodInfo NumberFormatter_Multiply10_m12438_MethodInfo = 
{
	"Multiply10"/* name */
	, (methodPointerType)&NumberFormatter_Multiply10_m12438/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Multiply10_m12438_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5960/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo NumberFormatter_t2231_NumberFormatter_Divide10_m12439_ParameterInfos[] = 
{
	{"count", 0, 134225157, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Divide10(System.Int32)
extern const MethodInfo NumberFormatter_Divide10_m12439_MethodInfo = 
{
	"Divide10"/* name */
	, (methodPointerType)&NumberFormatter_Divide10_m12439/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, NumberFormatter_t2231_NumberFormatter_Divide10_m12439_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5961/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter System.NumberFormatter::GetClone()
extern const MethodInfo NumberFormatter_GetClone_m12440_MethodInfo = 
{
	"GetClone"/* name */
	, (methodPointerType)&NumberFormatter_GetClone_m12440/* method */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatter_t2231_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5962/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NumberFormatter_t2231_MethodInfos[] =
{
	&NumberFormatter__ctor_m12347_MethodInfo,
	&NumberFormatter__cctor_m12348_MethodInfo,
	&NumberFormatter_GetFormatterTables_m12349_MethodInfo,
	&NumberFormatter_GetTenPowerOf_m12350_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m12351_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m12352_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m12353_MethodInfo,
	&NumberFormatter_FastToDecHex_m12354_MethodInfo,
	&NumberFormatter_ToDecHex_m12355_MethodInfo,
	&NumberFormatter_FastDecHexLen_m12356_MethodInfo,
	&NumberFormatter_DecHexLen_m12357_MethodInfo,
	&NumberFormatter_DecHexLen_m12358_MethodInfo,
	&NumberFormatter_ScaleOrder_m12359_MethodInfo,
	&NumberFormatter_InitialFloatingPrecision_m12360_MethodInfo,
	&NumberFormatter_ParsePrecision_m12361_MethodInfo,
	&NumberFormatter_Init_m12362_MethodInfo,
	&NumberFormatter_InitHex_m12363_MethodInfo,
	&NumberFormatter_Init_m12364_MethodInfo,
	&NumberFormatter_Init_m12365_MethodInfo,
	&NumberFormatter_Init_m12366_MethodInfo,
	&NumberFormatter_Init_m12367_MethodInfo,
	&NumberFormatter_Init_m12368_MethodInfo,
	&NumberFormatter_Init_m12369_MethodInfo,
	&NumberFormatter_ResetCharBuf_m12370_MethodInfo,
	&NumberFormatter_Resize_m12371_MethodInfo,
	&NumberFormatter_Append_m12372_MethodInfo,
	&NumberFormatter_Append_m12373_MethodInfo,
	&NumberFormatter_Append_m12374_MethodInfo,
	&NumberFormatter_GetNumberFormatInstance_m12375_MethodInfo,
	&NumberFormatter_set_CurrentCulture_m12376_MethodInfo,
	&NumberFormatter_get_IntegerDigits_m12377_MethodInfo,
	&NumberFormatter_get_DecimalDigits_m12378_MethodInfo,
	&NumberFormatter_get_IsFloatingSource_m12379_MethodInfo,
	&NumberFormatter_get_IsZero_m12380_MethodInfo,
	&NumberFormatter_get_IsZeroInteger_m12381_MethodInfo,
	&NumberFormatter_RoundPos_m12382_MethodInfo,
	&NumberFormatter_RoundDecimal_m12383_MethodInfo,
	&NumberFormatter_RoundBits_m12384_MethodInfo,
	&NumberFormatter_RemoveTrailingZeros_m12385_MethodInfo,
	&NumberFormatter_AddOneToDecHex_m12386_MethodInfo,
	&NumberFormatter_AddOneToDecHex_m12387_MethodInfo,
	&NumberFormatter_CountTrailingZeros_m12388_MethodInfo,
	&NumberFormatter_CountTrailingZeros_m12389_MethodInfo,
	&NumberFormatter_GetInstance_m12390_MethodInfo,
	&NumberFormatter_Release_m12391_MethodInfo,
	&NumberFormatter_SetThreadCurrentCulture_m12392_MethodInfo,
	&NumberFormatter_NumberToString_m12393_MethodInfo,
	&NumberFormatter_NumberToString_m12394_MethodInfo,
	&NumberFormatter_NumberToString_m12395_MethodInfo,
	&NumberFormatter_NumberToString_m12396_MethodInfo,
	&NumberFormatter_NumberToString_m12397_MethodInfo,
	&NumberFormatter_NumberToString_m12398_MethodInfo,
	&NumberFormatter_NumberToString_m12399_MethodInfo,
	&NumberFormatter_NumberToString_m12400_MethodInfo,
	&NumberFormatter_NumberToString_m12401_MethodInfo,
	&NumberFormatter_NumberToString_m12402_MethodInfo,
	&NumberFormatter_NumberToString_m12403_MethodInfo,
	&NumberFormatter_NumberToString_m12404_MethodInfo,
	&NumberFormatter_NumberToString_m12405_MethodInfo,
	&NumberFormatter_NumberToString_m12406_MethodInfo,
	&NumberFormatter_NumberToString_m12407_MethodInfo,
	&NumberFormatter_NumberToString_m12408_MethodInfo,
	&NumberFormatter_NumberToString_m12409_MethodInfo,
	&NumberFormatter_FastIntegerToString_m12410_MethodInfo,
	&NumberFormatter_IntegerToString_m12411_MethodInfo,
	&NumberFormatter_NumberToString_m12412_MethodInfo,
	&NumberFormatter_FormatCurrency_m12413_MethodInfo,
	&NumberFormatter_FormatDecimal_m12414_MethodInfo,
	&NumberFormatter_FormatHexadecimal_m12415_MethodInfo,
	&NumberFormatter_FormatFixedPoint_m12416_MethodInfo,
	&NumberFormatter_FormatRoundtrip_m12417_MethodInfo,
	&NumberFormatter_FormatRoundtrip_m12418_MethodInfo,
	&NumberFormatter_FormatGeneral_m12419_MethodInfo,
	&NumberFormatter_FormatNumber_m12420_MethodInfo,
	&NumberFormatter_FormatPercent_m12421_MethodInfo,
	&NumberFormatter_FormatExponential_m12422_MethodInfo,
	&NumberFormatter_FormatExponential_m12423_MethodInfo,
	&NumberFormatter_FormatCustom_m12424_MethodInfo,
	&NumberFormatter_ZeroTrimEnd_m12425_MethodInfo,
	&NumberFormatter_IsZeroOnly_m12426_MethodInfo,
	&NumberFormatter_AppendNonNegativeNumber_m12427_MethodInfo,
	&NumberFormatter_AppendIntegerString_m12428_MethodInfo,
	&NumberFormatter_AppendIntegerString_m12429_MethodInfo,
	&NumberFormatter_AppendDecimalString_m12430_MethodInfo,
	&NumberFormatter_AppendDecimalString_m12431_MethodInfo,
	&NumberFormatter_AppendIntegerStringWithGroupSeparator_m12432_MethodInfo,
	&NumberFormatter_AppendExponent_m12433_MethodInfo,
	&NumberFormatter_AppendOneDigit_m12434_MethodInfo,
	&NumberFormatter_FastAppendDigits_m12435_MethodInfo,
	&NumberFormatter_AppendDigits_m12436_MethodInfo,
	&NumberFormatter_AppendDigits_m12437_MethodInfo,
	&NumberFormatter_Multiply10_m12438_MethodInfo,
	&NumberFormatter_Divide10_m12439_MethodInfo,
	&NumberFormatter_GetClone_m12440_MethodInfo,
	NULL
};
extern const MethodInfo NumberFormatter_set_CurrentCulture_m12376_MethodInfo;
static const PropertyInfo NumberFormatter_t2231____CurrentCulture_PropertyInfo = 
{
	&NumberFormatter_t2231_il2cpp_TypeInfo/* parent */
	, "CurrentCulture"/* name */
	, NULL/* get */
	, &NumberFormatter_set_CurrentCulture_m12376_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IntegerDigits_m12377_MethodInfo;
static const PropertyInfo NumberFormatter_t2231____IntegerDigits_PropertyInfo = 
{
	&NumberFormatter_t2231_il2cpp_TypeInfo/* parent */
	, "IntegerDigits"/* name */
	, &NumberFormatter_get_IntegerDigits_m12377_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_DecimalDigits_m12378_MethodInfo;
static const PropertyInfo NumberFormatter_t2231____DecimalDigits_PropertyInfo = 
{
	&NumberFormatter_t2231_il2cpp_TypeInfo/* parent */
	, "DecimalDigits"/* name */
	, &NumberFormatter_get_DecimalDigits_m12378_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsFloatingSource_m12379_MethodInfo;
static const PropertyInfo NumberFormatter_t2231____IsFloatingSource_PropertyInfo = 
{
	&NumberFormatter_t2231_il2cpp_TypeInfo/* parent */
	, "IsFloatingSource"/* name */
	, &NumberFormatter_get_IsFloatingSource_m12379_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsZero_m12380_MethodInfo;
static const PropertyInfo NumberFormatter_t2231____IsZero_PropertyInfo = 
{
	&NumberFormatter_t2231_il2cpp_TypeInfo/* parent */
	, "IsZero"/* name */
	, &NumberFormatter_get_IsZero_m12380_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsZeroInteger_m12381_MethodInfo;
static const PropertyInfo NumberFormatter_t2231____IsZeroInteger_PropertyInfo = 
{
	&NumberFormatter_t2231_il2cpp_TypeInfo/* parent */
	, "IsZeroInteger"/* name */
	, &NumberFormatter_get_IsZeroInteger_m12381_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* NumberFormatter_t2231_PropertyInfos[] =
{
	&NumberFormatter_t2231____CurrentCulture_PropertyInfo,
	&NumberFormatter_t2231____IntegerDigits_PropertyInfo,
	&NumberFormatter_t2231____DecimalDigits_PropertyInfo,
	&NumberFormatter_t2231____IsFloatingSource_PropertyInfo,
	&NumberFormatter_t2231____IsZero_PropertyInfo,
	&NumberFormatter_t2231____IsZeroInteger_PropertyInfo,
	NULL
};
static const Il2CppType* NumberFormatter_t2231_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CustomInfo_t2230_0_0_0,
};
static const Il2CppMethodReference NumberFormatter_t2231_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool NumberFormatter_t2231_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NumberFormatter_t2231_1_0_0;
struct NumberFormatter_t2231;
const Il2CppTypeDefinitionMetadata NumberFormatter_t2231_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NumberFormatter_t2231_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NumberFormatter_t2231_VTable/* vtableMethods */
	, NumberFormatter_t2231_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2986/* fieldStart */

};
TypeInfo NumberFormatter_t2231_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NumberFormatter"/* name */
	, "System"/* namespaze */
	, NumberFormatter_t2231_MethodInfos/* methods */
	, NumberFormatter_t2231_PropertyInfos/* properties */
	, NULL/* events */
	, &NumberFormatter_t2231_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NumberFormatter_t2231_0_0_0/* byval_arg */
	, &NumberFormatter_t2231_1_0_0/* this_arg */
	, &NumberFormatter_t2231_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NumberFormatter_t2231)/* instance_size */
	, sizeof (NumberFormatter_t2231)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(NumberFormatter_t2231_StaticFields)/* static_fields_size */
	, sizeof(NumberFormatter_t2231_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 94/* method_count */
	, 6/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
// Metadata Definition System.ObjectDisposedException
extern TypeInfo ObjectDisposedException_t1641_il2cpp_TypeInfo;
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectDisposedException_t1641_ObjectDisposedException__ctor_m7519_ParameterInfos[] = 
{
	{"objectName", 0, 134225175, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern const MethodInfo ObjectDisposedException__ctor_m7519_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m7519/* method */
	, &ObjectDisposedException_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ObjectDisposedException_t1641_ObjectDisposedException__ctor_m7519_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5967/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectDisposedException_t1641_ObjectDisposedException__ctor_m12441_ParameterInfos[] = 
{
	{"objectName", 0, 134225176, 0, &String_t_0_0_0},
	{"message", 1, 134225177, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.String,System.String)
extern const MethodInfo ObjectDisposedException__ctor_m12441_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m12441/* method */
	, &ObjectDisposedException_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, ObjectDisposedException_t1641_ObjectDisposedException__ctor_m12441_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5968/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo ObjectDisposedException_t1641_ObjectDisposedException__ctor_m12442_ParameterInfos[] = 
{
	{"info", 0, 134225178, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225179, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjectDisposedException__ctor_m12442_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m12442/* method */
	, &ObjectDisposedException_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, ObjectDisposedException_t1641_ObjectDisposedException__ctor_m12442_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5969/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.ObjectDisposedException::get_Message()
extern const MethodInfo ObjectDisposedException_get_Message_m12443_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&ObjectDisposedException_get_Message_m12443/* method */
	, &ObjectDisposedException_t1641_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5970/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo ObjectDisposedException_t1641_ObjectDisposedException_GetObjectData_m12444_ParameterInfos[] = 
{
	{"info", 0, 134225180, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225181, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjectDisposedException_GetObjectData_m12444_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ObjectDisposedException_GetObjectData_m12444/* method */
	, &ObjectDisposedException_t1641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, ObjectDisposedException_t1641_ObjectDisposedException_GetObjectData_m12444_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5971/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectDisposedException_t1641_MethodInfos[] =
{
	&ObjectDisposedException__ctor_m7519_MethodInfo,
	&ObjectDisposedException__ctor_m12441_MethodInfo,
	&ObjectDisposedException__ctor_m12442_MethodInfo,
	&ObjectDisposedException_get_Message_m12443_MethodInfo,
	&ObjectDisposedException_GetObjectData_m12444_MethodInfo,
	NULL
};
extern const MethodInfo ObjectDisposedException_get_Message_m12443_MethodInfo;
static const PropertyInfo ObjectDisposedException_t1641____Message_PropertyInfo = 
{
	&ObjectDisposedException_t1641_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &ObjectDisposedException_get_Message_m12443_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectDisposedException_t1641_PropertyInfos[] =
{
	&ObjectDisposedException_t1641____Message_PropertyInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m5385_MethodInfo;
extern const MethodInfo ObjectDisposedException_GetObjectData_m12444_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m5387_MethodInfo;
extern const MethodInfo Exception_get_Source_m5389_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m5390_MethodInfo;
extern const MethodInfo Exception_GetType_m5391_MethodInfo;
static const Il2CppMethodReference ObjectDisposedException_t1641_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&ObjectDisposedException_GetObjectData_m12444_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&ObjectDisposedException_get_Message_m12443_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&ObjectDisposedException_GetObjectData_m12444_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool ObjectDisposedException_t1641_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializable_t439_0_0_0;
extern const Il2CppType _Exception_t1147_0_0_0;
static Il2CppInterfaceOffsetPair ObjectDisposedException_t1641_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectDisposedException_t1641_0_0_0;
extern const Il2CppType ObjectDisposedException_t1641_1_0_0;
extern const Il2CppType InvalidOperationException_t1442_0_0_0;
struct ObjectDisposedException_t1641;
const Il2CppTypeDefinitionMetadata ObjectDisposedException_t1641_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObjectDisposedException_t1641_InterfacesOffsets/* interfaceOffsets */
	, &InvalidOperationException_t1442_0_0_0/* parent */
	, ObjectDisposedException_t1641_VTable/* vtableMethods */
	, ObjectDisposedException_t1641_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3012/* fieldStart */

};
TypeInfo ObjectDisposedException_t1641_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectDisposedException"/* name */
	, "System"/* namespaze */
	, ObjectDisposedException_t1641_MethodInfos/* methods */
	, ObjectDisposedException_t1641_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectDisposedException_t1641_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 980/* custom_attributes_cache */
	, &ObjectDisposedException_t1641_0_0_0/* byval_arg */
	, &ObjectDisposedException_t1641_1_0_0/* this_arg */
	, &ObjectDisposedException_t1641_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectDisposedException_t1641)/* instance_size */
	, sizeof (ObjectDisposedException_t1641)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OperatingSystem
#include "mscorlib_System_OperatingSystem.h"
// Metadata Definition System.OperatingSystem
extern TypeInfo OperatingSystem_t2210_il2cpp_TypeInfo;
// System.OperatingSystem
#include "mscorlib_System_OperatingSystemMethodDeclarations.h"
extern const Il2CppType PlatformID_t2234_0_0_0;
extern const Il2CppType PlatformID_t2234_0_0_0;
extern const Il2CppType Version_t1292_0_0_0;
extern const Il2CppType Version_t1292_0_0_0;
static const ParameterInfo OperatingSystem_t2210_OperatingSystem__ctor_m12445_ParameterInfos[] = 
{
	{"platform", 0, 134225182, 0, &PlatformID_t2234_0_0_0},
	{"version", 1, 134225183, 0, &Version_t1292_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.OperatingSystem::.ctor(System.PlatformID,System.Version)
extern const MethodInfo OperatingSystem__ctor_m12445_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OperatingSystem__ctor_m12445/* method */
	, &OperatingSystem_t2210_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Object_t/* invoker_method */
	, OperatingSystem_t2210_OperatingSystem__ctor_m12445_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5972/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_PlatformID_t2234 (const MethodInfo* method, void* obj, void** args);
// System.PlatformID System.OperatingSystem::get_Platform()
extern const MethodInfo OperatingSystem_get_Platform_m12446_MethodInfo = 
{
	"get_Platform"/* name */
	, (methodPointerType)&OperatingSystem_get_Platform_m12446/* method */
	, &OperatingSystem_t2210_il2cpp_TypeInfo/* declaring_type */
	, &PlatformID_t2234_0_0_0/* return_type */
	, RuntimeInvoker_PlatformID_t2234/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5973/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo OperatingSystem_t2210_OperatingSystem_GetObjectData_m12447_ParameterInfos[] = 
{
	{"info", 0, 134225184, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225185, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OperatingSystem::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OperatingSystem_GetObjectData_m12447_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&OperatingSystem_GetObjectData_m12447/* method */
	, &OperatingSystem_t2210_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, OperatingSystem_t2210_OperatingSystem_GetObjectData_m12447_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5974/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.OperatingSystem::ToString()
extern const MethodInfo OperatingSystem_ToString_m12448_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&OperatingSystem_ToString_m12448/* method */
	, &OperatingSystem_t2210_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5975/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OperatingSystem_t2210_MethodInfos[] =
{
	&OperatingSystem__ctor_m12445_MethodInfo,
	&OperatingSystem_get_Platform_m12446_MethodInfo,
	&OperatingSystem_GetObjectData_m12447_MethodInfo,
	&OperatingSystem_ToString_m12448_MethodInfo,
	NULL
};
extern const MethodInfo OperatingSystem_get_Platform_m12446_MethodInfo;
static const PropertyInfo OperatingSystem_t2210____Platform_PropertyInfo = 
{
	&OperatingSystem_t2210_il2cpp_TypeInfo/* parent */
	, "Platform"/* name */
	, &OperatingSystem_get_Platform_m12446_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* OperatingSystem_t2210_PropertyInfos[] =
{
	&OperatingSystem_t2210____Platform_PropertyInfo,
	NULL
};
extern const MethodInfo OperatingSystem_ToString_m12448_MethodInfo;
extern const MethodInfo OperatingSystem_GetObjectData_m12447_MethodInfo;
static const Il2CppMethodReference OperatingSystem_t2210_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&OperatingSystem_ToString_m12448_MethodInfo,
	&OperatingSystem_GetObjectData_m12447_MethodInfo,
};
static bool OperatingSystem_t2210_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t733_0_0_0;
static const Il2CppType* OperatingSystem_t2210_InterfacesTypeInfos[] = 
{
	&ICloneable_t733_0_0_0,
	&ISerializable_t439_0_0_0,
};
static Il2CppInterfaceOffsetPair OperatingSystem_t2210_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OperatingSystem_t2210_0_0_0;
extern const Il2CppType OperatingSystem_t2210_1_0_0;
struct OperatingSystem_t2210;
const Il2CppTypeDefinitionMetadata OperatingSystem_t2210_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, OperatingSystem_t2210_InterfacesTypeInfos/* implementedInterfaces */
	, OperatingSystem_t2210_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OperatingSystem_t2210_VTable/* vtableMethods */
	, OperatingSystem_t2210_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3014/* fieldStart */

};
TypeInfo OperatingSystem_t2210_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OperatingSystem"/* name */
	, "System"/* namespaze */
	, OperatingSystem_t2210_MethodInfos/* methods */
	, OperatingSystem_t2210_PropertyInfos/* properties */
	, NULL/* events */
	, &OperatingSystem_t2210_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 981/* custom_attributes_cache */
	, &OperatingSystem_t2210_0_0_0/* byval_arg */
	, &OperatingSystem_t2210_1_0_0/* this_arg */
	, &OperatingSystem_t2210_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OperatingSystem_t2210)/* instance_size */
	, sizeof (OperatingSystem_t2210)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryException.h"
// Metadata Definition System.OutOfMemoryException
extern TypeInfo OutOfMemoryException_t2232_il2cpp_TypeInfo;
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OutOfMemoryException::.ctor()
extern const MethodInfo OutOfMemoryException__ctor_m12449_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OutOfMemoryException__ctor_m12449/* method */
	, &OutOfMemoryException_t2232_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5976/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo OutOfMemoryException_t2232_OutOfMemoryException__ctor_m12450_ParameterInfos[] = 
{
	{"info", 0, 134225186, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225187, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OutOfMemoryException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OutOfMemoryException__ctor_m12450_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OutOfMemoryException__ctor_m12450/* method */
	, &OutOfMemoryException_t2232_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, OutOfMemoryException_t2232_OutOfMemoryException__ctor_m12450_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5977/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OutOfMemoryException_t2232_MethodInfos[] =
{
	&OutOfMemoryException__ctor_m12449_MethodInfo,
	&OutOfMemoryException__ctor_m12450_MethodInfo,
	NULL
};
extern const MethodInfo Exception_GetObjectData_m5386_MethodInfo;
extern const MethodInfo Exception_get_Message_m5388_MethodInfo;
static const Il2CppMethodReference OutOfMemoryException_t2232_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool OutOfMemoryException_t2232_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OutOfMemoryException_t2232_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OutOfMemoryException_t2232_0_0_0;
extern const Il2CppType OutOfMemoryException_t2232_1_0_0;
extern const Il2CppType SystemException_t1464_0_0_0;
struct OutOfMemoryException_t2232;
const Il2CppTypeDefinitionMetadata OutOfMemoryException_t2232_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OutOfMemoryException_t2232_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1464_0_0_0/* parent */
	, OutOfMemoryException_t2232_VTable/* vtableMethods */
	, OutOfMemoryException_t2232_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3017/* fieldStart */

};
TypeInfo OutOfMemoryException_t2232_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OutOfMemoryException"/* name */
	, "System"/* namespaze */
	, OutOfMemoryException_t2232_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OutOfMemoryException_t2232_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 982/* custom_attributes_cache */
	, &OutOfMemoryException_t2232_0_0_0/* byval_arg */
	, &OutOfMemoryException_t2232_1_0_0/* this_arg */
	, &OutOfMemoryException_t2232_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OutOfMemoryException_t2232)/* instance_size */
	, sizeof (OutOfMemoryException_t2232)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OverflowException
#include "mscorlib_System_OverflowException.h"
// Metadata Definition System.OverflowException
extern TypeInfo OverflowException_t2233_il2cpp_TypeInfo;
// System.OverflowException
#include "mscorlib_System_OverflowExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor()
extern const MethodInfo OverflowException__ctor_m12451_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m12451/* method */
	, &OverflowException_t2233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5978/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OverflowException_t2233_OverflowException__ctor_m12452_ParameterInfos[] = 
{
	{"message", 0, 134225188, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor(System.String)
extern const MethodInfo OverflowException__ctor_m12452_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m12452/* method */
	, &OverflowException_t2233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, OverflowException_t2233_OverflowException__ctor_m12452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5979/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo OverflowException_t2233_OverflowException__ctor_m12453_ParameterInfos[] = 
{
	{"info", 0, 134225189, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225190, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OverflowException__ctor_m12453_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m12453/* method */
	, &OverflowException_t2233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, OverflowException_t2233_OverflowException__ctor_m12453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5980/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OverflowException_t2233_MethodInfos[] =
{
	&OverflowException__ctor_m12451_MethodInfo,
	&OverflowException__ctor_m12452_MethodInfo,
	&OverflowException__ctor_m12453_MethodInfo,
	NULL
};
static const Il2CppMethodReference OverflowException_t2233_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool OverflowException_t2233_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OverflowException_t2233_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OverflowException_t2233_0_0_0;
extern const Il2CppType OverflowException_t2233_1_0_0;
extern const Il2CppType ArithmeticException_t1638_0_0_0;
struct OverflowException_t2233;
const Il2CppTypeDefinitionMetadata OverflowException_t2233_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OverflowException_t2233_InterfacesOffsets/* interfaceOffsets */
	, &ArithmeticException_t1638_0_0_0/* parent */
	, OverflowException_t2233_VTable/* vtableMethods */
	, OverflowException_t2233_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3018/* fieldStart */

};
TypeInfo OverflowException_t2233_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OverflowException"/* name */
	, "System"/* namespaze */
	, OverflowException_t2233_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OverflowException_t2233_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 983/* custom_attributes_cache */
	, &OverflowException_t2233_0_0_0/* byval_arg */
	, &OverflowException_t2233_1_0_0/* this_arg */
	, &OverflowException_t2233_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OverflowException_t2233)/* instance_size */
	, sizeof (OverflowException_t2233)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
// Metadata Definition System.PlatformID
extern TypeInfo PlatformID_t2234_il2cpp_TypeInfo;
// System.PlatformID
#include "mscorlib_System_PlatformIDMethodDeclarations.h"
static const MethodInfo* PlatformID_t2234_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m1051_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m1052_MethodInfo;
extern const MethodInfo Enum_ToString_m1053_MethodInfo;
extern const MethodInfo Enum_ToString_m1054_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m1055_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m1056_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m1057_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m1058_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m1059_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m1060_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m1061_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m1062_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m1063_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m1064_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m1065_MethodInfo;
extern const MethodInfo Enum_ToString_m1066_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m1067_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m1068_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m1069_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m1070_MethodInfo;
extern const MethodInfo Enum_CompareTo_m1071_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m1072_MethodInfo;
static const Il2CppMethodReference PlatformID_t2234_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool PlatformID_t2234_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t275_0_0_0;
extern const Il2CppType IConvertible_t276_0_0_0;
extern const Il2CppType IComparable_t277_0_0_0;
static Il2CppInterfaceOffsetPair PlatformID_t2234_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PlatformID_t2234_1_0_0;
extern const Il2CppType Enum_t278_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t253_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata PlatformID_t2234_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PlatformID_t2234_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, PlatformID_t2234_VTable/* vtableMethods */
	, PlatformID_t2234_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3019/* fieldStart */

};
TypeInfo PlatformID_t2234_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlatformID"/* name */
	, "System"/* namespaze */
	, PlatformID_t2234_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 984/* custom_attributes_cache */
	, &PlatformID_t2234_0_0_0/* byval_arg */
	, &PlatformID_t2234_1_0_0/* this_arg */
	, &PlatformID_t2234_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlatformID_t2234)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PlatformID_t2234)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Random
#include "mscorlib_System_Random.h"
// Metadata Definition System.Random
extern TypeInfo Random_t923_il2cpp_TypeInfo;
// System.Random
#include "mscorlib_System_RandomMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Random::.ctor()
extern const MethodInfo Random__ctor_m12454_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Random__ctor_m12454/* method */
	, &Random_t923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5981/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Random_t923_Random__ctor_m5158_ParameterInfos[] = 
{
	{"Seed", 0, 134225191, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Random::.ctor(System.Int32)
extern const MethodInfo Random__ctor_m5158_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Random__ctor_m5158/* method */
	, &Random_t923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253/* invoker_method */
	, Random_t923_Random__ctor_m5158_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5982/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Random_t923_MethodInfos[] =
{
	&Random__ctor_m12454_MethodInfo,
	&Random__ctor_m5158_MethodInfo,
	NULL
};
static const Il2CppMethodReference Random_t923_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Random_t923_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Random_t923_0_0_0;
extern const Il2CppType Random_t923_1_0_0;
struct Random_t923;
const Il2CppTypeDefinitionMetadata Random_t923_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Random_t923_VTable/* vtableMethods */
	, Random_t923_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3027/* fieldStart */

};
TypeInfo Random_t923_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Random"/* name */
	, "System"/* namespaze */
	, Random_t923_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Random_t923_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 985/* custom_attributes_cache */
	, &Random_t923_0_0_0/* byval_arg */
	, &Random_t923_1_0_0/* this_arg */
	, &Random_t923_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Random_t923)/* instance_size */
	, sizeof (Random_t923)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RankException
#include "mscorlib_System_RankException.h"
// Metadata Definition System.RankException
extern TypeInfo RankException_t2235_il2cpp_TypeInfo;
// System.RankException
#include "mscorlib_System_RankExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor()
extern const MethodInfo RankException__ctor_m12455_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m12455/* method */
	, &RankException_t2235_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5983/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RankException_t2235_RankException__ctor_m12456_ParameterInfos[] = 
{
	{"message", 0, 134225192, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor(System.String)
extern const MethodInfo RankException__ctor_m12456_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m12456/* method */
	, &RankException_t2235_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, RankException_t2235_RankException__ctor_m12456_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5984/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo RankException_t2235_RankException__ctor_m12457_ParameterInfos[] = 
{
	{"info", 0, 134225193, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225194, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RankException__ctor_m12457_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m12457/* method */
	, &RankException_t2235_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, RankException_t2235_RankException__ctor_m12457_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5985/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RankException_t2235_MethodInfos[] =
{
	&RankException__ctor_m12455_MethodInfo,
	&RankException__ctor_m12456_MethodInfo,
	&RankException__ctor_m12457_MethodInfo,
	NULL
};
static const Il2CppMethodReference RankException_t2235_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool RankException_t2235_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RankException_t2235_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RankException_t2235_0_0_0;
extern const Il2CppType RankException_t2235_1_0_0;
struct RankException_t2235;
const Il2CppTypeDefinitionMetadata RankException_t2235_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RankException_t2235_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1464_0_0_0/* parent */
	, RankException_t2235_VTable/* vtableMethods */
	, RankException_t2235_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RankException_t2235_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RankException"/* name */
	, "System"/* namespaze */
	, RankException_t2235_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RankException_t2235_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 986/* custom_attributes_cache */
	, &RankException_t2235_0_0_0/* byval_arg */
	, &RankException_t2235_1_0_0/* this_arg */
	, &RankException_t2235_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RankException_t2235)/* instance_size */
	, sizeof (RankException_t2235)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgs.h"
// Metadata Definition System.ResolveEventArgs
extern TypeInfo ResolveEventArgs_t2236_il2cpp_TypeInfo;
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgsMethodDeclarations.h"
static const MethodInfo* ResolveEventArgs_t2236_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ResolveEventArgs_t2236_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ResolveEventArgs_t2236_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventArgs_t2236_0_0_0;
extern const Il2CppType ResolveEventArgs_t2236_1_0_0;
extern const Il2CppType EventArgs_t1547_0_0_0;
struct ResolveEventArgs_t2236;
const Il2CppTypeDefinitionMetadata ResolveEventArgs_t2236_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t1547_0_0_0/* parent */
	, ResolveEventArgs_t2236_VTable/* vtableMethods */
	, ResolveEventArgs_t2236_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ResolveEventArgs_t2236_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventArgs"/* name */
	, "System"/* namespaze */
	, ResolveEventArgs_t2236_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ResolveEventArgs_t2236_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 987/* custom_attributes_cache */
	, &ResolveEventArgs_t2236_0_0_0/* byval_arg */
	, &ResolveEventArgs_t2236_1_0_0/* this_arg */
	, &ResolveEventArgs_t2236_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventArgs_t2236)/* instance_size */
	, sizeof (ResolveEventArgs_t2236)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
// Metadata Definition System.RuntimeMethodHandle
extern TypeInfo RuntimeMethodHandle_t1850_il2cpp_TypeInfo;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandleMethodDeclarations.h"
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t1850_RuntimeMethodHandle__ctor_m12458_ParameterInfos[] = 
{
	{"v", 0, 134225195, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::.ctor(System.IntPtr)
extern const MethodInfo RuntimeMethodHandle__ctor_m12458_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RuntimeMethodHandle__ctor_m12458/* method */
	, &RuntimeMethodHandle_t1850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_IntPtr_t/* invoker_method */
	, RuntimeMethodHandle_t1850_RuntimeMethodHandle__ctor_m12458_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5986/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t1850_RuntimeMethodHandle__ctor_m12459_ParameterInfos[] = 
{
	{"info", 0, 134225196, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225197, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RuntimeMethodHandle__ctor_m12459_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RuntimeMethodHandle__ctor_m12459/* method */
	, &RuntimeMethodHandle_t1850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, RuntimeMethodHandle_t1850_RuntimeMethodHandle__ctor_m12459_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5987/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.RuntimeMethodHandle::get_Value()
extern const MethodInfo RuntimeMethodHandle_get_Value_m12460_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&RuntimeMethodHandle_get_Value_m12460/* method */
	, &RuntimeMethodHandle_t1850_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5988/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t1850_RuntimeMethodHandle_GetObjectData_m12461_ParameterInfos[] = 
{
	{"info", 0, 134225198, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225199, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RuntimeMethodHandle_GetObjectData_m12461_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&RuntimeMethodHandle_GetObjectData_m12461/* method */
	, &RuntimeMethodHandle_t1850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, RuntimeMethodHandle_t1850_RuntimeMethodHandle_GetObjectData_m12461_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5989/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t1850_RuntimeMethodHandle_Equals_m12462_ParameterInfos[] = 
{
	{"obj", 0, 134225200, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.RuntimeMethodHandle::Equals(System.Object)
extern const MethodInfo RuntimeMethodHandle_Equals_m12462_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&RuntimeMethodHandle_Equals_m12462/* method */
	, &RuntimeMethodHandle_t1850_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, RuntimeMethodHandle_t1850_RuntimeMethodHandle_Equals_m12462_ParameterInfos/* parameters */
	, 989/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5990/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.RuntimeMethodHandle::GetHashCode()
extern const MethodInfo RuntimeMethodHandle_GetHashCode_m12463_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&RuntimeMethodHandle_GetHashCode_m12463/* method */
	, &RuntimeMethodHandle_t1850_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5991/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RuntimeMethodHandle_t1850_MethodInfos[] =
{
	&RuntimeMethodHandle__ctor_m12458_MethodInfo,
	&RuntimeMethodHandle__ctor_m12459_MethodInfo,
	&RuntimeMethodHandle_get_Value_m12460_MethodInfo,
	&RuntimeMethodHandle_GetObjectData_m12461_MethodInfo,
	&RuntimeMethodHandle_Equals_m12462_MethodInfo,
	&RuntimeMethodHandle_GetHashCode_m12463_MethodInfo,
	NULL
};
extern const MethodInfo RuntimeMethodHandle_get_Value_m12460_MethodInfo;
static const PropertyInfo RuntimeMethodHandle_t1850____Value_PropertyInfo = 
{
	&RuntimeMethodHandle_t1850_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &RuntimeMethodHandle_get_Value_m12460_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RuntimeMethodHandle_t1850_PropertyInfos[] =
{
	&RuntimeMethodHandle_t1850____Value_PropertyInfo,
	NULL
};
extern const MethodInfo RuntimeMethodHandle_Equals_m12462_MethodInfo;
extern const MethodInfo RuntimeMethodHandle_GetHashCode_m12463_MethodInfo;
extern const MethodInfo ValueType_ToString_m1078_MethodInfo;
extern const MethodInfo RuntimeMethodHandle_GetObjectData_m12461_MethodInfo;
static const Il2CppMethodReference RuntimeMethodHandle_t1850_VTable[] =
{
	&RuntimeMethodHandle_Equals_m12462_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&RuntimeMethodHandle_GetHashCode_m12463_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
	&RuntimeMethodHandle_GetObjectData_m12461_MethodInfo,
};
static bool RuntimeMethodHandle_t1850_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* RuntimeMethodHandle_t1850_InterfacesTypeInfos[] = 
{
	&ISerializable_t439_0_0_0,
};
static Il2CppInterfaceOffsetPair RuntimeMethodHandle_t1850_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeMethodHandle_t1850_0_0_0;
extern const Il2CppType RuntimeMethodHandle_t1850_1_0_0;
extern const Il2CppType ValueType_t285_0_0_0;
const Il2CppTypeDefinitionMetadata RuntimeMethodHandle_t1850_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RuntimeMethodHandle_t1850_InterfacesTypeInfos/* implementedInterfaces */
	, RuntimeMethodHandle_t1850_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, RuntimeMethodHandle_t1850_VTable/* vtableMethods */
	, RuntimeMethodHandle_t1850_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3030/* fieldStart */

};
TypeInfo RuntimeMethodHandle_t1850_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeMethodHandle"/* name */
	, "System"/* namespaze */
	, RuntimeMethodHandle_t1850_MethodInfos/* methods */
	, RuntimeMethodHandle_t1850_PropertyInfos/* properties */
	, NULL/* events */
	, &RuntimeMethodHandle_t1850_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 988/* custom_attributes_cache */
	, &RuntimeMethodHandle_t1850_0_0_0/* byval_arg */
	, &RuntimeMethodHandle_t1850_1_0_0/* this_arg */
	, &RuntimeMethodHandle_t1850_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeMethodHandle_t1850)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeMethodHandle_t1850)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeMethodHandle_t1850 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.StringComparer
#include "mscorlib_System_StringComparer.h"
// Metadata Definition System.StringComparer
extern TypeInfo StringComparer_t1052_il2cpp_TypeInfo;
// System.StringComparer
#include "mscorlib_System_StringComparerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.StringComparer::.ctor()
extern const MethodInfo StringComparer__ctor_m12464_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StringComparer__ctor_m12464/* method */
	, &StringComparer_t1052_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5992/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.StringComparer::.cctor()
extern const MethodInfo StringComparer__cctor_m12465_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StringComparer__cctor_m12465/* method */
	, &StringComparer_t1052_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5993/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringComparer_t1052_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.StringComparer System.StringComparer::get_InvariantCultureIgnoreCase()
extern const MethodInfo StringComparer_get_InvariantCultureIgnoreCase_m6412_MethodInfo = 
{
	"get_InvariantCultureIgnoreCase"/* name */
	, (methodPointerType)&StringComparer_get_InvariantCultureIgnoreCase_m6412/* method */
	, &StringComparer_t1052_il2cpp_TypeInfo/* declaring_type */
	, &StringComparer_t1052_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5994/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.StringComparer System.StringComparer::get_OrdinalIgnoreCase()
extern const MethodInfo StringComparer_get_OrdinalIgnoreCase_m5109_MethodInfo = 
{
	"get_OrdinalIgnoreCase"/* name */
	, (methodPointerType)&StringComparer_get_OrdinalIgnoreCase_m5109/* method */
	, &StringComparer_t1052_il2cpp_TypeInfo/* declaring_type */
	, &StringComparer_t1052_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5995/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t1052_StringComparer_Compare_m12466_ParameterInfos[] = 
{
	{"x", 0, 134225201, 0, &Object_t_0_0_0},
	{"y", 1, 134225202, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::Compare(System.Object,System.Object)
extern const MethodInfo StringComparer_Compare_m12466_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&StringComparer_Compare_m12466/* method */
	, &StringComparer_t1052_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Object_t/* invoker_method */
	, StringComparer_t1052_StringComparer_Compare_m12466_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5996/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t1052_StringComparer_Equals_m12467_ParameterInfos[] = 
{
	{"x", 0, 134225203, 0, &Object_t_0_0_0},
	{"y", 1, 134225204, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.StringComparer::Equals(System.Object,System.Object)
extern const MethodInfo StringComparer_Equals_m12467_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&StringComparer_Equals_m12467/* method */
	, &StringComparer_t1052_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, StringComparer_t1052_StringComparer_Equals_m12467_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5997/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t1052_StringComparer_GetHashCode_m12468_ParameterInfos[] = 
{
	{"obj", 0, 134225205, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::GetHashCode(System.Object)
extern const MethodInfo StringComparer_GetHashCode_m12468_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&StringComparer_GetHashCode_m12468/* method */
	, &StringComparer_t1052_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, StringComparer_t1052_StringComparer_GetHashCode_m12468_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5998/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t1052_StringComparer_Compare_m13295_ParameterInfos[] = 
{
	{"x", 0, 134225206, 0, &String_t_0_0_0},
	{"y", 1, 134225207, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::Compare(System.String,System.String)
extern const MethodInfo StringComparer_Compare_m13295_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &StringComparer_t1052_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Object_t/* invoker_method */
	, StringComparer_t1052_StringComparer_Compare_m13295_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5999/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t1052_StringComparer_Equals_m13296_ParameterInfos[] = 
{
	{"x", 0, 134225208, 0, &String_t_0_0_0},
	{"y", 1, 134225209, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.StringComparer::Equals(System.String,System.String)
extern const MethodInfo StringComparer_Equals_m13296_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &StringComparer_t1052_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, StringComparer_t1052_StringComparer_Equals_m13296_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6000/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t1052_StringComparer_GetHashCode_m13297_ParameterInfos[] = 
{
	{"obj", 0, 134225210, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::GetHashCode(System.String)
extern const MethodInfo StringComparer_GetHashCode_m13297_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &StringComparer_t1052_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, StringComparer_t1052_StringComparer_GetHashCode_m13297_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6001/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StringComparer_t1052_MethodInfos[] =
{
	&StringComparer__ctor_m12464_MethodInfo,
	&StringComparer__cctor_m12465_MethodInfo,
	&StringComparer_get_InvariantCultureIgnoreCase_m6412_MethodInfo,
	&StringComparer_get_OrdinalIgnoreCase_m5109_MethodInfo,
	&StringComparer_Compare_m12466_MethodInfo,
	&StringComparer_Equals_m12467_MethodInfo,
	&StringComparer_GetHashCode_m12468_MethodInfo,
	&StringComparer_Compare_m13295_MethodInfo,
	&StringComparer_Equals_m13296_MethodInfo,
	&StringComparer_GetHashCode_m13297_MethodInfo,
	NULL
};
extern const MethodInfo StringComparer_get_InvariantCultureIgnoreCase_m6412_MethodInfo;
static const PropertyInfo StringComparer_t1052____InvariantCultureIgnoreCase_PropertyInfo = 
{
	&StringComparer_t1052_il2cpp_TypeInfo/* parent */
	, "InvariantCultureIgnoreCase"/* name */
	, &StringComparer_get_InvariantCultureIgnoreCase_m6412_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo StringComparer_get_OrdinalIgnoreCase_m5109_MethodInfo;
static const PropertyInfo StringComparer_t1052____OrdinalIgnoreCase_PropertyInfo = 
{
	&StringComparer_t1052_il2cpp_TypeInfo/* parent */
	, "OrdinalIgnoreCase"/* name */
	, &StringComparer_get_OrdinalIgnoreCase_m5109_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* StringComparer_t1052_PropertyInfos[] =
{
	&StringComparer_t1052____InvariantCultureIgnoreCase_PropertyInfo,
	&StringComparer_t1052____OrdinalIgnoreCase_PropertyInfo,
	NULL
};
extern const MethodInfo StringComparer_Compare_m13295_MethodInfo;
extern const MethodInfo StringComparer_Equals_m13296_MethodInfo;
extern const MethodInfo StringComparer_GetHashCode_m13297_MethodInfo;
extern const MethodInfo StringComparer_Compare_m12466_MethodInfo;
extern const MethodInfo StringComparer_Equals_m12467_MethodInfo;
extern const MethodInfo StringComparer_GetHashCode_m12468_MethodInfo;
static const Il2CppMethodReference StringComparer_t1052_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&StringComparer_Compare_m13295_MethodInfo,
	&StringComparer_Equals_m13296_MethodInfo,
	&StringComparer_GetHashCode_m13297_MethodInfo,
	&StringComparer_Compare_m12466_MethodInfo,
	&StringComparer_Equals_m12467_MethodInfo,
	&StringComparer_GetHashCode_m12468_MethodInfo,
	NULL,
	NULL,
	NULL,
};
static bool StringComparer_t1052_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparer_1_t2788_0_0_0;
extern const Il2CppType IEqualityComparer_1_t2789_0_0_0;
extern const Il2CppType IComparer_t279_0_0_0;
extern const Il2CppType IEqualityComparer_t1273_0_0_0;
static const Il2CppType* StringComparer_t1052_InterfacesTypeInfos[] = 
{
	&IComparer_1_t2788_0_0_0,
	&IEqualityComparer_1_t2789_0_0_0,
	&IComparer_t279_0_0_0,
	&IEqualityComparer_t1273_0_0_0,
};
static Il2CppInterfaceOffsetPair StringComparer_t1052_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2788_0_0_0, 4},
	{ &IEqualityComparer_1_t2789_0_0_0, 5},
	{ &IComparer_t279_0_0_0, 7},
	{ &IEqualityComparer_t1273_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparer_t1052_1_0_0;
struct StringComparer_t1052;
const Il2CppTypeDefinitionMetadata StringComparer_t1052_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StringComparer_t1052_InterfacesTypeInfos/* implementedInterfaces */
	, StringComparer_t1052_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StringComparer_t1052_VTable/* vtableMethods */
	, StringComparer_t1052_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3031/* fieldStart */

};
TypeInfo StringComparer_t1052_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparer"/* name */
	, "System"/* namespaze */
	, StringComparer_t1052_MethodInfos/* methods */
	, StringComparer_t1052_PropertyInfos/* properties */
	, NULL/* events */
	, &StringComparer_t1052_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 990/* custom_attributes_cache */
	, &StringComparer_t1052_0_0_0/* byval_arg */
	, &StringComparer_t1052_1_0_0/* this_arg */
	, &StringComparer_t1052_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparer_t1052)/* instance_size */
	, sizeof (StringComparer_t1052)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StringComparer_t1052_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparer.h"
// Metadata Definition System.CultureAwareComparer
extern TypeInfo CultureAwareComparer_t2237_il2cpp_TypeInfo;
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparerMethodDeclarations.h"
extern const Il2CppType CultureInfo_t1068_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo CultureAwareComparer_t2237_CultureAwareComparer__ctor_m12469_ParameterInfos[] = 
{
	{"ci", 0, 134225211, 0, &CultureInfo_t1068_0_0_0},
	{"ignore_case", 1, 134225212, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CultureAwareComparer::.ctor(System.Globalization.CultureInfo,System.Boolean)
extern const MethodInfo CultureAwareComparer__ctor_m12469_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CultureAwareComparer__ctor_m12469/* method */
	, &CultureAwareComparer_t2237_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, CultureAwareComparer_t2237_CultureAwareComparer__ctor_m12469_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6002/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t2237_CultureAwareComparer_Compare_m12470_ParameterInfos[] = 
{
	{"x", 0, 134225213, 0, &String_t_0_0_0},
	{"y", 1, 134225214, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.CultureAwareComparer::Compare(System.String,System.String)
extern const MethodInfo CultureAwareComparer_Compare_m12470_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&CultureAwareComparer_Compare_m12470/* method */
	, &CultureAwareComparer_t2237_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Object_t/* invoker_method */
	, CultureAwareComparer_t2237_CultureAwareComparer_Compare_m12470_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6003/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t2237_CultureAwareComparer_Equals_m12471_ParameterInfos[] = 
{
	{"x", 0, 134225215, 0, &String_t_0_0_0},
	{"y", 1, 134225216, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.CultureAwareComparer::Equals(System.String,System.String)
extern const MethodInfo CultureAwareComparer_Equals_m12471_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&CultureAwareComparer_Equals_m12471/* method */
	, &CultureAwareComparer_t2237_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, CultureAwareComparer_t2237_CultureAwareComparer_Equals_m12471_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6004/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t2237_CultureAwareComparer_GetHashCode_m12472_ParameterInfos[] = 
{
	{"s", 0, 134225217, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.CultureAwareComparer::GetHashCode(System.String)
extern const MethodInfo CultureAwareComparer_GetHashCode_m12472_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&CultureAwareComparer_GetHashCode_m12472/* method */
	, &CultureAwareComparer_t2237_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, CultureAwareComparer_t2237_CultureAwareComparer_GetHashCode_m12472_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6005/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CultureAwareComparer_t2237_MethodInfos[] =
{
	&CultureAwareComparer__ctor_m12469_MethodInfo,
	&CultureAwareComparer_Compare_m12470_MethodInfo,
	&CultureAwareComparer_Equals_m12471_MethodInfo,
	&CultureAwareComparer_GetHashCode_m12472_MethodInfo,
	NULL
};
extern const MethodInfo CultureAwareComparer_Compare_m12470_MethodInfo;
extern const MethodInfo CultureAwareComparer_Equals_m12471_MethodInfo;
extern const MethodInfo CultureAwareComparer_GetHashCode_m12472_MethodInfo;
static const Il2CppMethodReference CultureAwareComparer_t2237_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&CultureAwareComparer_Compare_m12470_MethodInfo,
	&CultureAwareComparer_Equals_m12471_MethodInfo,
	&CultureAwareComparer_GetHashCode_m12472_MethodInfo,
	&StringComparer_Compare_m12466_MethodInfo,
	&StringComparer_Equals_m12467_MethodInfo,
	&StringComparer_GetHashCode_m12468_MethodInfo,
	&CultureAwareComparer_Compare_m12470_MethodInfo,
	&CultureAwareComparer_Equals_m12471_MethodInfo,
	&CultureAwareComparer_GetHashCode_m12472_MethodInfo,
};
static bool CultureAwareComparer_t2237_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CultureAwareComparer_t2237_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2788_0_0_0, 4},
	{ &IEqualityComparer_1_t2789_0_0_0, 5},
	{ &IComparer_t279_0_0_0, 7},
	{ &IEqualityComparer_t1273_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CultureAwareComparer_t2237_0_0_0;
extern const Il2CppType CultureAwareComparer_t2237_1_0_0;
struct CultureAwareComparer_t2237;
const Il2CppTypeDefinitionMetadata CultureAwareComparer_t2237_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CultureAwareComparer_t2237_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t1052_0_0_0/* parent */
	, CultureAwareComparer_t2237_VTable/* vtableMethods */
	, CultureAwareComparer_t2237_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3035/* fieldStart */

};
TypeInfo CultureAwareComparer_t2237_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CultureAwareComparer"/* name */
	, "System"/* namespaze */
	, CultureAwareComparer_t2237_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CultureAwareComparer_t2237_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CultureAwareComparer_t2237_0_0_0/* byval_arg */
	, &CultureAwareComparer_t2237_1_0_0/* this_arg */
	, &CultureAwareComparer_t2237_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CultureAwareComparer_t2237)/* instance_size */
	, sizeof (CultureAwareComparer_t2237)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparer.h"
// Metadata Definition System.OrdinalComparer
extern TypeInfo OrdinalComparer_t2238_il2cpp_TypeInfo;
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparerMethodDeclarations.h"
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo OrdinalComparer_t2238_OrdinalComparer__ctor_m12473_ParameterInfos[] = 
{
	{"ignoreCase", 0, 134225218, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OrdinalComparer::.ctor(System.Boolean)
extern const MethodInfo OrdinalComparer__ctor_m12473_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OrdinalComparer__ctor_m12473/* method */
	, &OrdinalComparer_t2238_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_SByte_t274/* invoker_method */
	, OrdinalComparer_t2238_OrdinalComparer__ctor_m12473_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6006/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t2238_OrdinalComparer_Compare_m12474_ParameterInfos[] = 
{
	{"x", 0, 134225219, 0, &String_t_0_0_0},
	{"y", 1, 134225220, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.OrdinalComparer::Compare(System.String,System.String)
extern const MethodInfo OrdinalComparer_Compare_m12474_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&OrdinalComparer_Compare_m12474/* method */
	, &OrdinalComparer_t2238_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t_Object_t/* invoker_method */
	, OrdinalComparer_t2238_OrdinalComparer_Compare_m12474_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6007/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t2238_OrdinalComparer_Equals_m12475_ParameterInfos[] = 
{
	{"x", 0, 134225221, 0, &String_t_0_0_0},
	{"y", 1, 134225222, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.OrdinalComparer::Equals(System.String,System.String)
extern const MethodInfo OrdinalComparer_Equals_m12475_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&OrdinalComparer_Equals_m12475/* method */
	, &OrdinalComparer_t2238_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, OrdinalComparer_t2238_OrdinalComparer_Equals_m12475_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6008/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t2238_OrdinalComparer_GetHashCode_m12476_ParameterInfos[] = 
{
	{"s", 0, 134225223, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.OrdinalComparer::GetHashCode(System.String)
extern const MethodInfo OrdinalComparer_GetHashCode_m12476_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&OrdinalComparer_GetHashCode_m12476/* method */
	, &OrdinalComparer_t2238_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, OrdinalComparer_t2238_OrdinalComparer_GetHashCode_m12476_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6009/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OrdinalComparer_t2238_MethodInfos[] =
{
	&OrdinalComparer__ctor_m12473_MethodInfo,
	&OrdinalComparer_Compare_m12474_MethodInfo,
	&OrdinalComparer_Equals_m12475_MethodInfo,
	&OrdinalComparer_GetHashCode_m12476_MethodInfo,
	NULL
};
extern const MethodInfo OrdinalComparer_Compare_m12474_MethodInfo;
extern const MethodInfo OrdinalComparer_Equals_m12475_MethodInfo;
extern const MethodInfo OrdinalComparer_GetHashCode_m12476_MethodInfo;
static const Il2CppMethodReference OrdinalComparer_t2238_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&OrdinalComparer_Compare_m12474_MethodInfo,
	&OrdinalComparer_Equals_m12475_MethodInfo,
	&OrdinalComparer_GetHashCode_m12476_MethodInfo,
	&StringComparer_Compare_m12466_MethodInfo,
	&StringComparer_Equals_m12467_MethodInfo,
	&StringComparer_GetHashCode_m12468_MethodInfo,
	&OrdinalComparer_Compare_m12474_MethodInfo,
	&OrdinalComparer_Equals_m12475_MethodInfo,
	&OrdinalComparer_GetHashCode_m12476_MethodInfo,
};
static bool OrdinalComparer_t2238_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OrdinalComparer_t2238_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2788_0_0_0, 4},
	{ &IEqualityComparer_1_t2789_0_0_0, 5},
	{ &IComparer_t279_0_0_0, 7},
	{ &IEqualityComparer_t1273_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OrdinalComparer_t2238_0_0_0;
extern const Il2CppType OrdinalComparer_t2238_1_0_0;
struct OrdinalComparer_t2238;
const Il2CppTypeDefinitionMetadata OrdinalComparer_t2238_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OrdinalComparer_t2238_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t1052_0_0_0/* parent */
	, OrdinalComparer_t2238_VTable/* vtableMethods */
	, OrdinalComparer_t2238_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3037/* fieldStart */

};
TypeInfo OrdinalComparer_t2238_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OrdinalComparer"/* name */
	, "System"/* namespaze */
	, OrdinalComparer_t2238_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OrdinalComparer_t2238_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OrdinalComparer_t2238_0_0_0/* byval_arg */
	, &OrdinalComparer_t2238_1_0_0/* this_arg */
	, &OrdinalComparer_t2238_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OrdinalComparer_t2238)/* instance_size */
	, sizeof (OrdinalComparer_t2238)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.StringComparison
#include "mscorlib_System_StringComparison.h"
// Metadata Definition System.StringComparison
extern TypeInfo StringComparison_t2239_il2cpp_TypeInfo;
// System.StringComparison
#include "mscorlib_System_StringComparisonMethodDeclarations.h"
static const MethodInfo* StringComparison_t2239_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference StringComparison_t2239_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool StringComparison_t2239_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringComparison_t2239_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparison_t2239_0_0_0;
extern const Il2CppType StringComparison_t2239_1_0_0;
const Il2CppTypeDefinitionMetadata StringComparison_t2239_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringComparison_t2239_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, StringComparison_t2239_VTable/* vtableMethods */
	, StringComparison_t2239_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3038/* fieldStart */

};
TypeInfo StringComparison_t2239_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparison"/* name */
	, "System"/* namespaze */
	, StringComparison_t2239_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 991/* custom_attributes_cache */
	, &StringComparison_t2239_0_0_0/* byval_arg */
	, &StringComparison_t2239_1_0_0/* this_arg */
	, &StringComparison_t2239_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparison_t2239)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringComparison_t2239)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptions.h"
// Metadata Definition System.StringSplitOptions
extern TypeInfo StringSplitOptions_t2240_il2cpp_TypeInfo;
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptionsMethodDeclarations.h"
static const MethodInfo* StringSplitOptions_t2240_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference StringSplitOptions_t2240_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool StringSplitOptions_t2240_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringSplitOptions_t2240_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringSplitOptions_t2240_0_0_0;
extern const Il2CppType StringSplitOptions_t2240_1_0_0;
const Il2CppTypeDefinitionMetadata StringSplitOptions_t2240_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringSplitOptions_t2240_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, StringSplitOptions_t2240_VTable/* vtableMethods */
	, StringSplitOptions_t2240_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3045/* fieldStart */

};
TypeInfo StringSplitOptions_t2240_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringSplitOptions"/* name */
	, "System"/* namespaze */
	, StringSplitOptions_t2240_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 992/* custom_attributes_cache */
	, &StringSplitOptions_t2240_0_0_0/* byval_arg */
	, &StringSplitOptions_t2240_1_0_0/* this_arg */
	, &StringSplitOptions_t2240_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringSplitOptions_t2240)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringSplitOptions_t2240)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.SystemException
#include "mscorlib_System_SystemException.h"
// Metadata Definition System.SystemException
extern TypeInfo SystemException_t1464_il2cpp_TypeInfo;
// System.SystemException
#include "mscorlib_System_SystemExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor()
extern const MethodInfo SystemException__ctor_m12477_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m12477/* method */
	, &SystemException_t1464_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6010/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SystemException_t1464_SystemException__ctor_m6509_ParameterInfos[] = 
{
	{"message", 0, 134225224, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.String)
extern const MethodInfo SystemException__ctor_m6509_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m6509/* method */
	, &SystemException_t1464_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, SystemException_t1464_SystemException__ctor_m6509_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6011/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo SystemException_t1464_SystemException__ctor_m12478_ParameterInfos[] = 
{
	{"info", 0, 134225225, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225226, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo SystemException__ctor_m12478_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m12478/* method */
	, &SystemException_t1464_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, SystemException_t1464_SystemException__ctor_m12478_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6012/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t232_0_0_0;
extern const Il2CppType Exception_t232_0_0_0;
static const ParameterInfo SystemException_t1464_SystemException__ctor_m12479_ParameterInfos[] = 
{
	{"message", 0, 134225227, 0, &String_t_0_0_0},
	{"innerException", 1, 134225228, 0, &Exception_t232_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.String,System.Exception)
extern const MethodInfo SystemException__ctor_m12479_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m12479/* method */
	, &SystemException_t1464_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, SystemException_t1464_SystemException__ctor_m12479_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6013/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SystemException_t1464_MethodInfos[] =
{
	&SystemException__ctor_m12477_MethodInfo,
	&SystemException__ctor_m6509_MethodInfo,
	&SystemException__ctor_m12478_MethodInfo,
	&SystemException__ctor_m12479_MethodInfo,
	NULL
};
static const Il2CppMethodReference SystemException_t1464_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool SystemException_t1464_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SystemException_t1464_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SystemException_t1464_1_0_0;
struct SystemException_t1464;
const Il2CppTypeDefinitionMetadata SystemException_t1464_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SystemException_t1464_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t232_0_0_0/* parent */
	, SystemException_t1464_VTable/* vtableMethods */
	, SystemException_t1464_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SystemException_t1464_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SystemException"/* name */
	, "System"/* namespaze */
	, SystemException_t1464_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SystemException_t1464_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 993/* custom_attributes_cache */
	, &SystemException_t1464_0_0_0/* byval_arg */
	, &SystemException_t1464_1_0_0/* this_arg */
	, &SystemException_t1464_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SystemException_t1464)/* instance_size */
	, sizeof (SystemException_t1464)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
// Metadata Definition System.ThreadStaticAttribute
extern TypeInfo ThreadStaticAttribute_t2241_il2cpp_TypeInfo;
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ThreadStaticAttribute::.ctor()
extern const MethodInfo ThreadStaticAttribute__ctor_m12480_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ThreadStaticAttribute__ctor_m12480/* method */
	, &ThreadStaticAttribute_t2241_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6014/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ThreadStaticAttribute_t2241_MethodInfos[] =
{
	&ThreadStaticAttribute__ctor_m12480_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5384_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5253_MethodInfo;
static const Il2CppMethodReference ThreadStaticAttribute_t2241_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ThreadStaticAttribute_t2241_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t1145_0_0_0;
static Il2CppInterfaceOffsetPair ThreadStaticAttribute_t2241_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadStaticAttribute_t2241_0_0_0;
extern const Il2CppType ThreadStaticAttribute_t2241_1_0_0;
extern const Il2CppType Attribute_t805_0_0_0;
struct ThreadStaticAttribute_t2241;
const Il2CppTypeDefinitionMetadata ThreadStaticAttribute_t2241_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadStaticAttribute_t2241_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, ThreadStaticAttribute_t2241_VTable/* vtableMethods */
	, ThreadStaticAttribute_t2241_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ThreadStaticAttribute_t2241_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadStaticAttribute"/* name */
	, "System"/* namespaze */
	, ThreadStaticAttribute_t2241_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ThreadStaticAttribute_t2241_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 994/* custom_attributes_cache */
	, &ThreadStaticAttribute_t2241_0_0_0/* byval_arg */
	, &ThreadStaticAttribute_t2241_1_0_0/* this_arg */
	, &ThreadStaticAttribute_t2241_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadStaticAttribute_t2241)/* instance_size */
	, sizeof (ThreadStaticAttribute_t2241)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// Metadata Definition System.TimeSpan
extern TypeInfo TimeSpan_t1337_il2cpp_TypeInfo;
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan__ctor_m12481_ParameterInfos[] = 
{
	{"ticks", 0, 134225229, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int64)
extern const MethodInfo TimeSpan__ctor_m12481_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m12481/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int64_t1071/* invoker_method */
	, TimeSpan_t1337_TimeSpan__ctor_m12481_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6015/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan__ctor_m12482_ParameterInfos[] = 
{
	{"hours", 0, 134225230, 0, &Int32_t253_0_0_0},
	{"minutes", 1, 134225231, 0, &Int32_t253_0_0_0},
	{"seconds", 2, 134225232, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan__ctor_m12482_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m12482/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, TimeSpan_t1337_TimeSpan__ctor_m12482_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6016/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan__ctor_m12483_ParameterInfos[] = 
{
	{"days", 0, 134225233, 0, &Int32_t253_0_0_0},
	{"hours", 1, 134225234, 0, &Int32_t253_0_0_0},
	{"minutes", 2, 134225235, 0, &Int32_t253_0_0_0},
	{"seconds", 3, 134225236, 0, &Int32_t253_0_0_0},
	{"milliseconds", 4, 134225237, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan__ctor_m12483_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m12483/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, TimeSpan_t1337_TimeSpan__ctor_m12483_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6017/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.cctor()
extern const MethodInfo TimeSpan__cctor_m12484_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TimeSpan__cctor_m12484/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6018/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_CalculateTicks_m12485_ParameterInfos[] = 
{
	{"days", 0, 134225238, 0, &Int32_t253_0_0_0},
	{"hours", 1, 134225239, 0, &Int32_t253_0_0_0},
	{"minutes", 2, 134225240, 0, &Int32_t253_0_0_0},
	{"seconds", 3, 134225241, 0, &Int32_t253_0_0_0},
	{"milliseconds", 4, 134225242, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1071_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.TimeSpan::CalculateTicks(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan_CalculateTicks_m12485_MethodInfo = 
{
	"CalculateTicks"/* name */
	, (methodPointerType)&TimeSpan_CalculateTicks_m12485/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1071_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1071_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, TimeSpan_t1337_TimeSpan_CalculateTicks_m12485_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6019/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Days()
extern const MethodInfo TimeSpan_get_Days_m12486_MethodInfo = 
{
	"get_Days"/* name */
	, (methodPointerType)&TimeSpan_get_Days_m12486/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6020/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Hours()
extern const MethodInfo TimeSpan_get_Hours_m12487_MethodInfo = 
{
	"get_Hours"/* name */
	, (methodPointerType)&TimeSpan_get_Hours_m12487/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6021/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Milliseconds()
extern const MethodInfo TimeSpan_get_Milliseconds_m12488_MethodInfo = 
{
	"get_Milliseconds"/* name */
	, (methodPointerType)&TimeSpan_get_Milliseconds_m12488/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6022/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Minutes()
extern const MethodInfo TimeSpan_get_Minutes_m12489_MethodInfo = 
{
	"get_Minutes"/* name */
	, (methodPointerType)&TimeSpan_get_Minutes_m12489/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6023/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Seconds()
extern const MethodInfo TimeSpan_get_Seconds_m12490_MethodInfo = 
{
	"get_Seconds"/* name */
	, (methodPointerType)&TimeSpan_get_Seconds_m12490/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6024/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.TimeSpan::get_Ticks()
extern const MethodInfo TimeSpan_get_Ticks_m12491_MethodInfo = 
{
	"get_Ticks"/* name */
	, (methodPointerType)&TimeSpan_get_Ticks_m12491/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1071_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1071/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6025/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1070 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalDays()
extern const MethodInfo TimeSpan_get_TotalDays_m12492_MethodInfo = 
{
	"get_TotalDays"/* name */
	, (methodPointerType)&TimeSpan_get_TotalDays_m12492/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1070_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1070/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6026/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1070 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalHours()
extern const MethodInfo TimeSpan_get_TotalHours_m12493_MethodInfo = 
{
	"get_TotalHours"/* name */
	, (methodPointerType)&TimeSpan_get_TotalHours_m12493/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1070_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1070/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6027/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1070 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalMilliseconds()
extern const MethodInfo TimeSpan_get_TotalMilliseconds_m12494_MethodInfo = 
{
	"get_TotalMilliseconds"/* name */
	, (methodPointerType)&TimeSpan_get_TotalMilliseconds_m12494/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1070_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1070/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6028/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1070 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalMinutes()
extern const MethodInfo TimeSpan_get_TotalMinutes_m12495_MethodInfo = 
{
	"get_TotalMinutes"/* name */
	, (methodPointerType)&TimeSpan_get_TotalMinutes_m12495/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1070_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1070/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6029/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1070 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalSeconds()
extern const MethodInfo TimeSpan_get_TotalSeconds_m12496_MethodInfo = 
{
	"get_TotalSeconds"/* name */
	, (methodPointerType)&TimeSpan_get_TotalSeconds_m12496/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1070_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1070/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6030/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_Add_m12497_ParameterInfos[] = 
{
	{"ts", 0, 134225243, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Add(System.TimeSpan)
extern const MethodInfo TimeSpan_Add_m12497_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&TimeSpan_Add_m12497/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_Add_m12497_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6031/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_Compare_m12498_ParameterInfos[] = 
{
	{"t1", 0, 134225244, 0, &TimeSpan_t1337_0_0_0},
	{"t2", 1, 134225245, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::Compare(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_Compare_m12498_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&TimeSpan_Compare_m12498/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_Compare_m12498_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6032/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_CompareTo_m12499_ParameterInfos[] = 
{
	{"value", 0, 134225246, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::CompareTo(System.Object)
extern const MethodInfo TimeSpan_CompareTo_m12499_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&TimeSpan_CompareTo_m12499/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, TimeSpan_t1337_TimeSpan_CompareTo_m12499_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6033/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_CompareTo_m12500_ParameterInfos[] = 
{
	{"value", 0, 134225247, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::CompareTo(System.TimeSpan)
extern const MethodInfo TimeSpan_CompareTo_m12500_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&TimeSpan_CompareTo_m12500/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_CompareTo_m12500_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6034/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_Equals_m12501_ParameterInfos[] = 
{
	{"obj", 0, 134225248, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::Equals(System.TimeSpan)
extern const MethodInfo TimeSpan_Equals_m12501_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TimeSpan_Equals_m12501/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_Equals_m12501_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6035/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Duration()
extern const MethodInfo TimeSpan_Duration_m12502_MethodInfo = 
{
	"Duration"/* name */
	, (methodPointerType)&TimeSpan_Duration_m12502/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6036/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_Equals_m12503_ParameterInfos[] = 
{
	{"value", 0, 134225249, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::Equals(System.Object)
extern const MethodInfo TimeSpan_Equals_m12503_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TimeSpan_Equals_m12503/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, TimeSpan_t1337_TimeSpan_Equals_m12503_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6037/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1070_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_FromMinutes_m12504_ParameterInfos[] = 
{
	{"value", 0, 134225250, 0, &Double_t1070_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_Double_t1070 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::FromMinutes(System.Double)
extern const MethodInfo TimeSpan_FromMinutes_m12504_MethodInfo = 
{
	"FromMinutes"/* name */
	, (methodPointerType)&TimeSpan_FromMinutes_m12504/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_Double_t1070/* invoker_method */
	, TimeSpan_t1337_TimeSpan_FromMinutes_m12504_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6038/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1070_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_FromSeconds_m12505_ParameterInfos[] = 
{
	{"value", 0, 134225251, 0, &Double_t1070_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_Double_t1070 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::FromSeconds(System.Double)
extern const MethodInfo TimeSpan_FromSeconds_m12505_MethodInfo = 
{
	"FromSeconds"/* name */
	, (methodPointerType)&TimeSpan_FromSeconds_m12505/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_Double_t1070/* invoker_method */
	, TimeSpan_t1337_TimeSpan_FromSeconds_m12505_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6039/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1070_0_0_0;
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_From_m12506_ParameterInfos[] = 
{
	{"value", 0, 134225252, 0, &Double_t1070_0_0_0},
	{"tickMultiplicator", 1, 134225253, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_Double_t1070_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::From(System.Double,System.Int64)
extern const MethodInfo TimeSpan_From_m12506_MethodInfo = 
{
	"From"/* name */
	, (methodPointerType)&TimeSpan_From_m12506/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_Double_t1070_Int64_t1071/* invoker_method */
	, TimeSpan_t1337_TimeSpan_From_m12506_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6040/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::GetHashCode()
extern const MethodInfo TimeSpan_GetHashCode_m12507_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&TimeSpan_GetHashCode_m12507/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6041/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Negate()
extern const MethodInfo TimeSpan_Negate_m12508_MethodInfo = 
{
	"Negate"/* name */
	, (methodPointerType)&TimeSpan_Negate_m12508/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6042/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_Subtract_m12509_ParameterInfos[] = 
{
	{"ts", 0, 134225254, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Subtract(System.TimeSpan)
extern const MethodInfo TimeSpan_Subtract_m12509_MethodInfo = 
{
	"Subtract"/* name */
	, (methodPointerType)&TimeSpan_Subtract_m12509/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_Subtract_m12509_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6043/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.TimeSpan::ToString()
extern const MethodInfo TimeSpan_ToString_m12510_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&TimeSpan_ToString_m12510/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6044/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_op_Addition_m12511_ParameterInfos[] = 
{
	{"t1", 0, 134225255, 0, &TimeSpan_t1337_0_0_0},
	{"t2", 1, 134225256, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::op_Addition(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Addition_m12511_MethodInfo = 
{
	"op_Addition"/* name */
	, (methodPointerType)&TimeSpan_op_Addition_m12511/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_op_Addition_m12511_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6045/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_op_Equality_m12512_ParameterInfos[] = 
{
	{"t1", 0, 134225257, 0, &TimeSpan_t1337_0_0_0},
	{"t2", 1, 134225258, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_Equality(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Equality_m12512_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&TimeSpan_op_Equality_m12512/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_op_Equality_m12512_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6046/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_op_GreaterThan_m12513_ParameterInfos[] = 
{
	{"t1", 0, 134225259, 0, &TimeSpan_t1337_0_0_0},
	{"t2", 1, 134225260, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_GreaterThan(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_GreaterThan_m12513_MethodInfo = 
{
	"op_GreaterThan"/* name */
	, (methodPointerType)&TimeSpan_op_GreaterThan_m12513/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_op_GreaterThan_m12513_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6047/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_op_GreaterThanOrEqual_m12514_ParameterInfos[] = 
{
	{"t1", 0, 134225261, 0, &TimeSpan_t1337_0_0_0},
	{"t2", 1, 134225262, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_GreaterThanOrEqual(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_GreaterThanOrEqual_m12514_MethodInfo = 
{
	"op_GreaterThanOrEqual"/* name */
	, (methodPointerType)&TimeSpan_op_GreaterThanOrEqual_m12514/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_op_GreaterThanOrEqual_m12514_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6048/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_op_Inequality_m12515_ParameterInfos[] = 
{
	{"t1", 0, 134225263, 0, &TimeSpan_t1337_0_0_0},
	{"t2", 1, 134225264, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_Inequality(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Inequality_m12515_MethodInfo = 
{
	"op_Inequality"/* name */
	, (methodPointerType)&TimeSpan_op_Inequality_m12515/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_op_Inequality_m12515_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6049/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_op_LessThan_m12516_ParameterInfos[] = 
{
	{"t1", 0, 134225265, 0, &TimeSpan_t1337_0_0_0},
	{"t2", 1, 134225266, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_LessThan(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_LessThan_m12516_MethodInfo = 
{
	"op_LessThan"/* name */
	, (methodPointerType)&TimeSpan_op_LessThan_m12516/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_op_LessThan_m12516_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6050/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_op_LessThanOrEqual_m12517_ParameterInfos[] = 
{
	{"t1", 0, 134225267, 0, &TimeSpan_t1337_0_0_0},
	{"t2", 1, 134225268, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_LessThanOrEqual(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_LessThanOrEqual_m12517_MethodInfo = 
{
	"op_LessThanOrEqual"/* name */
	, (methodPointerType)&TimeSpan_op_LessThanOrEqual_m12517/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_op_LessThanOrEqual_m12517_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6051/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1337_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeSpan_t1337_TimeSpan_op_Subtraction_m12518_ParameterInfos[] = 
{
	{"t1", 0, 134225269, 0, &TimeSpan_t1337_0_0_0},
	{"t2", 1, 134225270, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::op_Subtraction(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Subtraction_m12518_MethodInfo = 
{
	"op_Subtraction"/* name */
	, (methodPointerType)&TimeSpan_op_Subtraction_m12518/* method */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_TimeSpan_t1337_TimeSpan_t1337/* invoker_method */
	, TimeSpan_t1337_TimeSpan_op_Subtraction_m12518_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6052/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TimeSpan_t1337_MethodInfos[] =
{
	&TimeSpan__ctor_m12481_MethodInfo,
	&TimeSpan__ctor_m12482_MethodInfo,
	&TimeSpan__ctor_m12483_MethodInfo,
	&TimeSpan__cctor_m12484_MethodInfo,
	&TimeSpan_CalculateTicks_m12485_MethodInfo,
	&TimeSpan_get_Days_m12486_MethodInfo,
	&TimeSpan_get_Hours_m12487_MethodInfo,
	&TimeSpan_get_Milliseconds_m12488_MethodInfo,
	&TimeSpan_get_Minutes_m12489_MethodInfo,
	&TimeSpan_get_Seconds_m12490_MethodInfo,
	&TimeSpan_get_Ticks_m12491_MethodInfo,
	&TimeSpan_get_TotalDays_m12492_MethodInfo,
	&TimeSpan_get_TotalHours_m12493_MethodInfo,
	&TimeSpan_get_TotalMilliseconds_m12494_MethodInfo,
	&TimeSpan_get_TotalMinutes_m12495_MethodInfo,
	&TimeSpan_get_TotalSeconds_m12496_MethodInfo,
	&TimeSpan_Add_m12497_MethodInfo,
	&TimeSpan_Compare_m12498_MethodInfo,
	&TimeSpan_CompareTo_m12499_MethodInfo,
	&TimeSpan_CompareTo_m12500_MethodInfo,
	&TimeSpan_Equals_m12501_MethodInfo,
	&TimeSpan_Duration_m12502_MethodInfo,
	&TimeSpan_Equals_m12503_MethodInfo,
	&TimeSpan_FromMinutes_m12504_MethodInfo,
	&TimeSpan_FromSeconds_m12505_MethodInfo,
	&TimeSpan_From_m12506_MethodInfo,
	&TimeSpan_GetHashCode_m12507_MethodInfo,
	&TimeSpan_Negate_m12508_MethodInfo,
	&TimeSpan_Subtract_m12509_MethodInfo,
	&TimeSpan_ToString_m12510_MethodInfo,
	&TimeSpan_op_Addition_m12511_MethodInfo,
	&TimeSpan_op_Equality_m12512_MethodInfo,
	&TimeSpan_op_GreaterThan_m12513_MethodInfo,
	&TimeSpan_op_GreaterThanOrEqual_m12514_MethodInfo,
	&TimeSpan_op_Inequality_m12515_MethodInfo,
	&TimeSpan_op_LessThan_m12516_MethodInfo,
	&TimeSpan_op_LessThanOrEqual_m12517_MethodInfo,
	&TimeSpan_op_Subtraction_m12518_MethodInfo,
	NULL
};
extern const MethodInfo TimeSpan_get_Days_m12486_MethodInfo;
static const PropertyInfo TimeSpan_t1337____Days_PropertyInfo = 
{
	&TimeSpan_t1337_il2cpp_TypeInfo/* parent */
	, "Days"/* name */
	, &TimeSpan_get_Days_m12486_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Hours_m12487_MethodInfo;
static const PropertyInfo TimeSpan_t1337____Hours_PropertyInfo = 
{
	&TimeSpan_t1337_il2cpp_TypeInfo/* parent */
	, "Hours"/* name */
	, &TimeSpan_get_Hours_m12487_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Milliseconds_m12488_MethodInfo;
static const PropertyInfo TimeSpan_t1337____Milliseconds_PropertyInfo = 
{
	&TimeSpan_t1337_il2cpp_TypeInfo/* parent */
	, "Milliseconds"/* name */
	, &TimeSpan_get_Milliseconds_m12488_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Minutes_m12489_MethodInfo;
static const PropertyInfo TimeSpan_t1337____Minutes_PropertyInfo = 
{
	&TimeSpan_t1337_il2cpp_TypeInfo/* parent */
	, "Minutes"/* name */
	, &TimeSpan_get_Minutes_m12489_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Seconds_m12490_MethodInfo;
static const PropertyInfo TimeSpan_t1337____Seconds_PropertyInfo = 
{
	&TimeSpan_t1337_il2cpp_TypeInfo/* parent */
	, "Seconds"/* name */
	, &TimeSpan_get_Seconds_m12490_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Ticks_m12491_MethodInfo;
static const PropertyInfo TimeSpan_t1337____Ticks_PropertyInfo = 
{
	&TimeSpan_t1337_il2cpp_TypeInfo/* parent */
	, "Ticks"/* name */
	, &TimeSpan_get_Ticks_m12491_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalDays_m12492_MethodInfo;
static const PropertyInfo TimeSpan_t1337____TotalDays_PropertyInfo = 
{
	&TimeSpan_t1337_il2cpp_TypeInfo/* parent */
	, "TotalDays"/* name */
	, &TimeSpan_get_TotalDays_m12492_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalHours_m12493_MethodInfo;
static const PropertyInfo TimeSpan_t1337____TotalHours_PropertyInfo = 
{
	&TimeSpan_t1337_il2cpp_TypeInfo/* parent */
	, "TotalHours"/* name */
	, &TimeSpan_get_TotalHours_m12493_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalMilliseconds_m12494_MethodInfo;
static const PropertyInfo TimeSpan_t1337____TotalMilliseconds_PropertyInfo = 
{
	&TimeSpan_t1337_il2cpp_TypeInfo/* parent */
	, "TotalMilliseconds"/* name */
	, &TimeSpan_get_TotalMilliseconds_m12494_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalMinutes_m12495_MethodInfo;
static const PropertyInfo TimeSpan_t1337____TotalMinutes_PropertyInfo = 
{
	&TimeSpan_t1337_il2cpp_TypeInfo/* parent */
	, "TotalMinutes"/* name */
	, &TimeSpan_get_TotalMinutes_m12495_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalSeconds_m12496_MethodInfo;
static const PropertyInfo TimeSpan_t1337____TotalSeconds_PropertyInfo = 
{
	&TimeSpan_t1337_il2cpp_TypeInfo/* parent */
	, "TotalSeconds"/* name */
	, &TimeSpan_get_TotalSeconds_m12496_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TimeSpan_t1337_PropertyInfos[] =
{
	&TimeSpan_t1337____Days_PropertyInfo,
	&TimeSpan_t1337____Hours_PropertyInfo,
	&TimeSpan_t1337____Milliseconds_PropertyInfo,
	&TimeSpan_t1337____Minutes_PropertyInfo,
	&TimeSpan_t1337____Seconds_PropertyInfo,
	&TimeSpan_t1337____Ticks_PropertyInfo,
	&TimeSpan_t1337____TotalDays_PropertyInfo,
	&TimeSpan_t1337____TotalHours_PropertyInfo,
	&TimeSpan_t1337____TotalMilliseconds_PropertyInfo,
	&TimeSpan_t1337____TotalMinutes_PropertyInfo,
	&TimeSpan_t1337____TotalSeconds_PropertyInfo,
	NULL
};
extern const MethodInfo TimeSpan_Equals_m12503_MethodInfo;
extern const MethodInfo TimeSpan_GetHashCode_m12507_MethodInfo;
extern const MethodInfo TimeSpan_ToString_m12510_MethodInfo;
extern const MethodInfo TimeSpan_CompareTo_m12499_MethodInfo;
extern const MethodInfo TimeSpan_CompareTo_m12500_MethodInfo;
extern const MethodInfo TimeSpan_Equals_m12501_MethodInfo;
static const Il2CppMethodReference TimeSpan_t1337_VTable[] =
{
	&TimeSpan_Equals_m12503_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&TimeSpan_GetHashCode_m12507_MethodInfo,
	&TimeSpan_ToString_m12510_MethodInfo,
	&TimeSpan_CompareTo_m12499_MethodInfo,
	&TimeSpan_CompareTo_m12500_MethodInfo,
	&TimeSpan_Equals_m12501_MethodInfo,
};
static bool TimeSpan_t1337_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparable_1_t2790_0_0_0;
extern const Il2CppType IEquatable_1_t2791_0_0_0;
static const Il2CppType* TimeSpan_t1337_InterfacesTypeInfos[] = 
{
	&IComparable_t277_0_0_0,
	&IComparable_1_t2790_0_0_0,
	&IEquatable_1_t2791_0_0_0,
};
static Il2CppInterfaceOffsetPair TimeSpan_t1337_InterfacesOffsets[] = 
{
	{ &IComparable_t277_0_0_0, 4},
	{ &IComparable_1_t2790_0_0_0, 5},
	{ &IEquatable_1_t2791_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeSpan_t1337_1_0_0;
const Il2CppTypeDefinitionMetadata TimeSpan_t1337_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TimeSpan_t1337_InterfacesTypeInfos/* implementedInterfaces */
	, TimeSpan_t1337_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, TimeSpan_t1337_VTable/* vtableMethods */
	, TimeSpan_t1337_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3048/* fieldStart */

};
TypeInfo TimeSpan_t1337_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeSpan"/* name */
	, "System"/* namespaze */
	, TimeSpan_t1337_MethodInfos/* methods */
	, TimeSpan_t1337_PropertyInfos/* properties */
	, NULL/* events */
	, &TimeSpan_t1337_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 995/* custom_attributes_cache */
	, &TimeSpan_t1337_0_0_0/* byval_arg */
	, &TimeSpan_t1337_1_0_0/* this_arg */
	, &TimeSpan_t1337_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeSpan_t1337)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeSpan_t1337)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(TimeSpan_t1337 )/* native_size */
	, sizeof(TimeSpan_t1337_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 38/* method_count */
	, 11/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.TimeZone
#include "mscorlib_System_TimeZone.h"
// Metadata Definition System.TimeZone
extern TypeInfo TimeZone_t2242_il2cpp_TypeInfo;
// System.TimeZone
#include "mscorlib_System_TimeZoneMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeZone::.ctor()
extern const MethodInfo TimeZone__ctor_m12519_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeZone__ctor_m12519/* method */
	, &TimeZone_t2242_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6053/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeZone::.cctor()
extern const MethodInfo TimeZone__cctor_m12520_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TimeZone__cctor_m12520/* method */
	, &TimeZone_t2242_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6054/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeZone_t2242_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.TimeZone System.TimeZone::get_CurrentTimeZone()
extern const MethodInfo TimeZone_get_CurrentTimeZone_m12521_MethodInfo = 
{
	"get_CurrentTimeZone"/* name */
	, (methodPointerType)&TimeZone_get_CurrentTimeZone_m12521/* method */
	, &TimeZone_t2242_il2cpp_TypeInfo/* declaring_type */
	, &TimeZone_t2242_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6055/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo TimeZone_t2242_TimeZone_GetDaylightChanges_m13298_ParameterInfos[] = 
{
	{"year", 0, 134225271, 0, &Int32_t253_0_0_0},
};
extern const Il2CppType DaylightTime_t1798_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.TimeZone::GetDaylightChanges(System.Int32)
extern const MethodInfo TimeZone_GetDaylightChanges_m13298_MethodInfo = 
{
	"GetDaylightChanges"/* name */
	, NULL/* method */
	, &TimeZone_t2242_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t1798_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, TimeZone_t2242_TimeZone_GetDaylightChanges_m13298_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6056/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t406_0_0_0;
extern const Il2CppType DateTime_t406_0_0_0;
static const ParameterInfo TimeZone_t2242_TimeZone_GetUtcOffset_m13299_ParameterInfos[] = 
{
	{"time", 0, 134225272, 0, &DateTime_t406_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetUtcOffset(System.DateTime)
extern const MethodInfo TimeZone_GetUtcOffset_m13299_MethodInfo = 
{
	"GetUtcOffset"/* name */
	, NULL/* method */
	, &TimeZone_t2242_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_DateTime_t406/* invoker_method */
	, TimeZone_t2242_TimeZone_GetUtcOffset_m13299_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6057/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t406_0_0_0;
static const ParameterInfo TimeZone_t2242_TimeZone_IsDaylightSavingTime_m12522_ParameterInfos[] = 
{
	{"time", 0, 134225273, 0, &DateTime_t406_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeZone::IsDaylightSavingTime(System.DateTime)
extern const MethodInfo TimeZone_IsDaylightSavingTime_m12522_MethodInfo = 
{
	"IsDaylightSavingTime"/* name */
	, (methodPointerType)&TimeZone_IsDaylightSavingTime_m12522/* method */
	, &TimeZone_t2242_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_DateTime_t406/* invoker_method */
	, TimeZone_t2242_TimeZone_IsDaylightSavingTime_m12522_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6058/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t406_0_0_0;
extern const Il2CppType DaylightTime_t1798_0_0_0;
static const ParameterInfo TimeZone_t2242_TimeZone_IsDaylightSavingTime_m12523_ParameterInfos[] = 
{
	{"time", 0, 134225274, 0, &DateTime_t406_0_0_0},
	{"daylightTimes", 1, 134225275, 0, &DaylightTime_t1798_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_DateTime_t406_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeZone::IsDaylightSavingTime(System.DateTime,System.Globalization.DaylightTime)
extern const MethodInfo TimeZone_IsDaylightSavingTime_m12523_MethodInfo = 
{
	"IsDaylightSavingTime"/* name */
	, (methodPointerType)&TimeZone_IsDaylightSavingTime_m12523/* method */
	, &TimeZone_t2242_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_DateTime_t406_Object_t/* invoker_method */
	, TimeZone_t2242_TimeZone_IsDaylightSavingTime_m12523_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6059/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t406_0_0_0;
static const ParameterInfo TimeZone_t2242_TimeZone_ToLocalTime_m12524_ParameterInfos[] = 
{
	{"time", 0, 134225276, 0, &DateTime_t406_0_0_0},
};
extern void* RuntimeInvoker_DateTime_t406_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
// System.DateTime System.TimeZone::ToLocalTime(System.DateTime)
extern const MethodInfo TimeZone_ToLocalTime_m12524_MethodInfo = 
{
	"ToLocalTime"/* name */
	, (methodPointerType)&TimeZone_ToLocalTime_m12524/* method */
	, &TimeZone_t2242_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t406_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t406_DateTime_t406/* invoker_method */
	, TimeZone_t2242_TimeZone_ToLocalTime_m12524_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6060/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t406_0_0_0;
static const ParameterInfo TimeZone_t2242_TimeZone_ToUniversalTime_m12525_ParameterInfos[] = 
{
	{"time", 0, 134225277, 0, &DateTime_t406_0_0_0},
};
extern void* RuntimeInvoker_DateTime_t406_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
// System.DateTime System.TimeZone::ToUniversalTime(System.DateTime)
extern const MethodInfo TimeZone_ToUniversalTime_m12525_MethodInfo = 
{
	"ToUniversalTime"/* name */
	, (methodPointerType)&TimeZone_ToUniversalTime_m12525/* method */
	, &TimeZone_t2242_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t406_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t406_DateTime_t406/* invoker_method */
	, TimeZone_t2242_TimeZone_ToUniversalTime_m12525_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6061/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t406_0_0_0;
static const ParameterInfo TimeZone_t2242_TimeZone_GetLocalTimeDiff_m12526_ParameterInfos[] = 
{
	{"time", 0, 134225278, 0, &DateTime_t406_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetLocalTimeDiff(System.DateTime)
extern const MethodInfo TimeZone_GetLocalTimeDiff_m12526_MethodInfo = 
{
	"GetLocalTimeDiff"/* name */
	, (methodPointerType)&TimeZone_GetLocalTimeDiff_m12526/* method */
	, &TimeZone_t2242_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_DateTime_t406/* invoker_method */
	, TimeZone_t2242_TimeZone_GetLocalTimeDiff_m12526_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6062/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t406_0_0_0;
extern const Il2CppType TimeSpan_t1337_0_0_0;
static const ParameterInfo TimeZone_t2242_TimeZone_GetLocalTimeDiff_m12527_ParameterInfos[] = 
{
	{"time", 0, 134225279, 0, &DateTime_t406_0_0_0},
	{"utc_offset", 1, 134225280, 0, &TimeSpan_t1337_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_DateTime_t406_TimeSpan_t1337 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetLocalTimeDiff(System.DateTime,System.TimeSpan)
extern const MethodInfo TimeZone_GetLocalTimeDiff_m12527_MethodInfo = 
{
	"GetLocalTimeDiff"/* name */
	, (methodPointerType)&TimeZone_GetLocalTimeDiff_m12527/* method */
	, &TimeZone_t2242_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_DateTime_t406_TimeSpan_t1337/* invoker_method */
	, TimeZone_t2242_TimeZone_GetLocalTimeDiff_m12527_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6063/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TimeZone_t2242_MethodInfos[] =
{
	&TimeZone__ctor_m12519_MethodInfo,
	&TimeZone__cctor_m12520_MethodInfo,
	&TimeZone_get_CurrentTimeZone_m12521_MethodInfo,
	&TimeZone_GetDaylightChanges_m13298_MethodInfo,
	&TimeZone_GetUtcOffset_m13299_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m12522_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m12523_MethodInfo,
	&TimeZone_ToLocalTime_m12524_MethodInfo,
	&TimeZone_ToUniversalTime_m12525_MethodInfo,
	&TimeZone_GetLocalTimeDiff_m12526_MethodInfo,
	&TimeZone_GetLocalTimeDiff_m12527_MethodInfo,
	NULL
};
extern const MethodInfo TimeZone_get_CurrentTimeZone_m12521_MethodInfo;
static const PropertyInfo TimeZone_t2242____CurrentTimeZone_PropertyInfo = 
{
	&TimeZone_t2242_il2cpp_TypeInfo/* parent */
	, "CurrentTimeZone"/* name */
	, &TimeZone_get_CurrentTimeZone_m12521_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TimeZone_t2242_PropertyInfos[] =
{
	&TimeZone_t2242____CurrentTimeZone_PropertyInfo,
	NULL
};
extern const MethodInfo TimeZone_IsDaylightSavingTime_m12522_MethodInfo;
extern const MethodInfo TimeZone_ToLocalTime_m12524_MethodInfo;
extern const MethodInfo TimeZone_ToUniversalTime_m12525_MethodInfo;
static const Il2CppMethodReference TimeZone_t2242_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	NULL,
	NULL,
	&TimeZone_IsDaylightSavingTime_m12522_MethodInfo,
	&TimeZone_ToLocalTime_m12524_MethodInfo,
	&TimeZone_ToUniversalTime_m12525_MethodInfo,
};
static bool TimeZone_t2242_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeZone_t2242_1_0_0;
struct TimeZone_t2242;
const Il2CppTypeDefinitionMetadata TimeZone_t2242_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TimeZone_t2242_VTable/* vtableMethods */
	, TimeZone_t2242_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3052/* fieldStart */

};
TypeInfo TimeZone_t2242_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeZone"/* name */
	, "System"/* namespaze */
	, TimeZone_t2242_MethodInfos/* methods */
	, TimeZone_t2242_PropertyInfos/* properties */
	, NULL/* events */
	, &TimeZone_t2242_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 996/* custom_attributes_cache */
	, &TimeZone_t2242_0_0_0/* byval_arg */
	, &TimeZone_t2242_1_0_0/* this_arg */
	, &TimeZone_t2242_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeZone_t2242)/* instance_size */
	, sizeof (TimeZone_t2242)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TimeZone_t2242_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZone.h"
// Metadata Definition System.CurrentSystemTimeZone
extern TypeInfo CurrentSystemTimeZone_t2243_il2cpp_TypeInfo;
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZoneMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::.ctor()
extern const MethodInfo CurrentSystemTimeZone__ctor_m12528_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CurrentSystemTimeZone__ctor_m12528/* method */
	, &CurrentSystemTimeZone_t2243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6064/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1071_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2243_CurrentSystemTimeZone__ctor_m12529_ParameterInfos[] = 
{
	{"lnow", 0, 134225281, 0, &Int64_t1071_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int64_t1071 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::.ctor(System.Int64)
extern const MethodInfo CurrentSystemTimeZone__ctor_m12529_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CurrentSystemTimeZone__ctor_m12529/* method */
	, &CurrentSystemTimeZone_t2243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int64_t1071/* invoker_method */
	, CurrentSystemTimeZone_t2243_CurrentSystemTimeZone__ctor_m12529_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6065/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12530_ParameterInfos[] = 
{
	{"sender", 0, 134225282, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern const MethodInfo CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12530_MethodInfo = 
{
	"System.Runtime.Serialization.IDeserializationCallback.OnDeserialization"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12530/* method */
	, &CurrentSystemTimeZone_t2243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12530_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6066/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int64U5BU5D_t2278_1_0_2;
extern const Il2CppType Int64U5BU5D_t2278_1_0_0;
extern const Il2CppType StringU5BU5D_t243_1_0_2;
extern const Il2CppType StringU5BU5D_t243_1_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_GetTimeZoneData_m12531_ParameterInfos[] = 
{
	{"year", 0, 134225283, 0, &Int32_t253_0_0_0},
	{"data", 1, 134225284, 0, &Int64U5BU5D_t2278_1_0_2},
	{"names", 2, 134225285, 0, &StringU5BU5D_t243_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t273_Int32_t253_Int64U5BU5DU26_t2792_StringU5BU5DU26_t2793 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.CurrentSystemTimeZone::GetTimeZoneData(System.Int32,System.Int64[]&,System.String[]&)
extern const MethodInfo CurrentSystemTimeZone_GetTimeZoneData_m12531_MethodInfo = 
{
	"GetTimeZoneData"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetTimeZoneData_m12531/* method */
	, &CurrentSystemTimeZone_t2243_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Int32_t253_Int64U5BU5DU26_t2792_StringU5BU5DU26_t2793/* invoker_method */
	, CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_GetTimeZoneData_m12531_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6067/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_GetDaylightChanges_m12532_ParameterInfos[] = 
{
	{"year", 0, 134225286, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.CurrentSystemTimeZone::GetDaylightChanges(System.Int32)
extern const MethodInfo CurrentSystemTimeZone_GetDaylightChanges_m12532_MethodInfo = 
{
	"GetDaylightChanges"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetDaylightChanges_m12532/* method */
	, &CurrentSystemTimeZone_t2243_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t1798_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t253/* invoker_method */
	, CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_GetDaylightChanges_m12532_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6068/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t406_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_GetUtcOffset_m12533_ParameterInfos[] = 
{
	{"time", 0, 134225287, 0, &DateTime_t406_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1337_DateTime_t406 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.CurrentSystemTimeZone::GetUtcOffset(System.DateTime)
extern const MethodInfo CurrentSystemTimeZone_GetUtcOffset_m12533_MethodInfo = 
{
	"GetUtcOffset"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetUtcOffset_m12533/* method */
	, &CurrentSystemTimeZone_t2243_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1337_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1337_DateTime_t406/* invoker_method */
	, CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_GetUtcOffset_m12533_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6069/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DaylightTime_t1798_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_OnDeserialization_m12534_ParameterInfos[] = 
{
	{"dlt", 0, 134225288, 0, &DaylightTime_t1798_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::OnDeserialization(System.Globalization.DaylightTime)
extern const MethodInfo CurrentSystemTimeZone_OnDeserialization_m12534_MethodInfo = 
{
	"OnDeserialization"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_OnDeserialization_m12534/* method */
	, &CurrentSystemTimeZone_t2243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_OnDeserialization_m12534_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6070/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64U5BU5D_t2278_0_0_0;
extern const Il2CppType Int64U5BU5D_t2278_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_GetDaylightTimeFromData_m12535_ParameterInfos[] = 
{
	{"data", 0, 134225289, 0, &Int64U5BU5D_t2278_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.CurrentSystemTimeZone::GetDaylightTimeFromData(System.Int64[])
extern const MethodInfo CurrentSystemTimeZone_GetDaylightTimeFromData_m12535_MethodInfo = 
{
	"GetDaylightTimeFromData"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetDaylightTimeFromData_m12535/* method */
	, &CurrentSystemTimeZone_t2243_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t1798_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t2243_CurrentSystemTimeZone_GetDaylightTimeFromData_m12535_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6071/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CurrentSystemTimeZone_t2243_MethodInfos[] =
{
	&CurrentSystemTimeZone__ctor_m12528_MethodInfo,
	&CurrentSystemTimeZone__ctor_m12529_MethodInfo,
	&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12530_MethodInfo,
	&CurrentSystemTimeZone_GetTimeZoneData_m12531_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightChanges_m12532_MethodInfo,
	&CurrentSystemTimeZone_GetUtcOffset_m12533_MethodInfo,
	&CurrentSystemTimeZone_OnDeserialization_m12534_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightTimeFromData_m12535_MethodInfo,
	NULL
};
extern const MethodInfo CurrentSystemTimeZone_GetDaylightChanges_m12532_MethodInfo;
extern const MethodInfo CurrentSystemTimeZone_GetUtcOffset_m12533_MethodInfo;
extern const MethodInfo CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12530_MethodInfo;
static const Il2CppMethodReference CurrentSystemTimeZone_t2243_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightChanges_m12532_MethodInfo,
	&CurrentSystemTimeZone_GetUtcOffset_m12533_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m12522_MethodInfo,
	&TimeZone_ToLocalTime_m12524_MethodInfo,
	&TimeZone_ToUniversalTime_m12525_MethodInfo,
	&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m12530_MethodInfo,
};
static bool CurrentSystemTimeZone_t2243_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDeserializationCallback_t445_0_0_0;
static const Il2CppType* CurrentSystemTimeZone_t2243_InterfacesTypeInfos[] = 
{
	&IDeserializationCallback_t445_0_0_0,
};
static Il2CppInterfaceOffsetPair CurrentSystemTimeZone_t2243_InterfacesOffsets[] = 
{
	{ &IDeserializationCallback_t445_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CurrentSystemTimeZone_t2243_0_0_0;
extern const Il2CppType CurrentSystemTimeZone_t2243_1_0_0;
struct CurrentSystemTimeZone_t2243;
const Il2CppTypeDefinitionMetadata CurrentSystemTimeZone_t2243_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CurrentSystemTimeZone_t2243_InterfacesTypeInfos/* implementedInterfaces */
	, CurrentSystemTimeZone_t2243_InterfacesOffsets/* interfaceOffsets */
	, &TimeZone_t2242_0_0_0/* parent */
	, CurrentSystemTimeZone_t2243_VTable/* vtableMethods */
	, CurrentSystemTimeZone_t2243_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3053/* fieldStart */

};
TypeInfo CurrentSystemTimeZone_t2243_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CurrentSystemTimeZone"/* name */
	, "System"/* namespaze */
	, CurrentSystemTimeZone_t2243_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CurrentSystemTimeZone_t2243_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CurrentSystemTimeZone_t2243_0_0_0/* byval_arg */
	, &CurrentSystemTimeZone_t2243_1_0_0/* this_arg */
	, &CurrentSystemTimeZone_t2243_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CurrentSystemTimeZone_t2243)/* instance_size */
	, sizeof (CurrentSystemTimeZone_t2243)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CurrentSystemTimeZone_t2243_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
// Metadata Definition System.TypeCode
extern TypeInfo TypeCode_t2244_il2cpp_TypeInfo;
// System.TypeCode
#include "mscorlib_System_TypeCodeMethodDeclarations.h"
static const MethodInfo* TypeCode_t2244_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeCode_t2244_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool TypeCode_t2244_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeCode_t2244_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeCode_t2244_0_0_0;
extern const Il2CppType TypeCode_t2244_1_0_0;
const Il2CppTypeDefinitionMetadata TypeCode_t2244_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeCode_t2244_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, TypeCode_t2244_VTable/* vtableMethods */
	, TypeCode_t2244_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3061/* fieldStart */

};
TypeInfo TypeCode_t2244_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeCode"/* name */
	, "System"/* namespaze */
	, TypeCode_t2244_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 997/* custom_attributes_cache */
	, &TypeCode_t2244_0_0_0/* byval_arg */
	, &TypeCode_t2244_1_0_0/* this_arg */
	, &TypeCode_t2244_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeCode_t2244)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeCode_t2244)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 19/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.TypeInitializationException
#include "mscorlib_System_TypeInitializationException.h"
// Metadata Definition System.TypeInitializationException
extern TypeInfo TypeInitializationException_t2245_il2cpp_TypeInfo;
// System.TypeInitializationException
#include "mscorlib_System_TypeInitializationExceptionMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo TypeInitializationException_t2245_TypeInitializationException__ctor_m12536_ParameterInfos[] = 
{
	{"info", 0, 134225290, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225291, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeInitializationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeInitializationException__ctor_m12536_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInitializationException__ctor_m12536/* method */
	, &TypeInitializationException_t2245_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, TypeInitializationException_t2245_TypeInitializationException__ctor_m12536_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6072/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo TypeInitializationException_t2245_TypeInitializationException_GetObjectData_m12537_ParameterInfos[] = 
{
	{"info", 0, 134225292, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225293, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeInitializationException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeInitializationException_GetObjectData_m12537_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&TypeInitializationException_GetObjectData_m12537/* method */
	, &TypeInitializationException_t2245_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, TypeInitializationException_t2245_TypeInitializationException_GetObjectData_m12537_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6073/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeInitializationException_t2245_MethodInfos[] =
{
	&TypeInitializationException__ctor_m12536_MethodInfo,
	&TypeInitializationException_GetObjectData_m12537_MethodInfo,
	NULL
};
extern const MethodInfo TypeInitializationException_GetObjectData_m12537_MethodInfo;
static const Il2CppMethodReference TypeInitializationException_t2245_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&TypeInitializationException_GetObjectData_m12537_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&TypeInitializationException_GetObjectData_m12537_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool TypeInitializationException_t2245_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInitializationException_t2245_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInitializationException_t2245_0_0_0;
extern const Il2CppType TypeInitializationException_t2245_1_0_0;
struct TypeInitializationException_t2245;
const Il2CppTypeDefinitionMetadata TypeInitializationException_t2245_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInitializationException_t2245_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1464_0_0_0/* parent */
	, TypeInitializationException_t2245_VTable/* vtableMethods */
	, TypeInitializationException_t2245_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3080/* fieldStart */

};
TypeInfo TypeInitializationException_t2245_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInitializationException"/* name */
	, "System"/* namespaze */
	, TypeInitializationException_t2245_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeInitializationException_t2245_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 998/* custom_attributes_cache */
	, &TypeInitializationException_t2245_0_0_0/* byval_arg */
	, &TypeInitializationException_t2245_1_0_0/* this_arg */
	, &TypeInitializationException_t2245_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInitializationException_t2245)/* instance_size */
	, sizeof (TypeInitializationException_t2245)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.TypeLoadException
#include "mscorlib_System_TypeLoadException.h"
// Metadata Definition System.TypeLoadException
extern TypeInfo TypeLoadException_t2202_il2cpp_TypeInfo;
// System.TypeLoadException
#include "mscorlib_System_TypeLoadExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor()
extern const MethodInfo TypeLoadException__ctor_m12538_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m12538/* method */
	, &TypeLoadException_t2202_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6074/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TypeLoadException_t2202_TypeLoadException__ctor_m12539_ParameterInfos[] = 
{
	{"message", 0, 134225294, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor(System.String)
extern const MethodInfo TypeLoadException__ctor_m12539_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m12539/* method */
	, &TypeLoadException_t2202_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TypeLoadException_t2202_TypeLoadException__ctor_m12539_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6075/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo TypeLoadException_t2202_TypeLoadException__ctor_m12540_ParameterInfos[] = 
{
	{"info", 0, 134225295, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225296, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeLoadException__ctor_m12540_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m12540/* method */
	, &TypeLoadException_t2202_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, TypeLoadException_t2202_TypeLoadException__ctor_m12540_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6076/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.TypeLoadException::get_Message()
extern const MethodInfo TypeLoadException_get_Message_m12541_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&TypeLoadException_get_Message_m12541/* method */
	, &TypeLoadException_t2202_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6077/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo TypeLoadException_t2202_TypeLoadException_GetObjectData_m12542_ParameterInfos[] = 
{
	{"info", 0, 134225297, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225298, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeLoadException_GetObjectData_m12542_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&TypeLoadException_GetObjectData_m12542/* method */
	, &TypeLoadException_t2202_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, TypeLoadException_t2202_TypeLoadException_GetObjectData_m12542_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6078/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeLoadException_t2202_MethodInfos[] =
{
	&TypeLoadException__ctor_m12538_MethodInfo,
	&TypeLoadException__ctor_m12539_MethodInfo,
	&TypeLoadException__ctor_m12540_MethodInfo,
	&TypeLoadException_get_Message_m12541_MethodInfo,
	&TypeLoadException_GetObjectData_m12542_MethodInfo,
	NULL
};
extern const MethodInfo TypeLoadException_get_Message_m12541_MethodInfo;
static const PropertyInfo TypeLoadException_t2202____Message_PropertyInfo = 
{
	&TypeLoadException_t2202_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &TypeLoadException_get_Message_m12541_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TypeLoadException_t2202_PropertyInfos[] =
{
	&TypeLoadException_t2202____Message_PropertyInfo,
	NULL
};
extern const MethodInfo TypeLoadException_GetObjectData_m12542_MethodInfo;
static const Il2CppMethodReference TypeLoadException_t2202_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&TypeLoadException_GetObjectData_m12542_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&TypeLoadException_get_Message_m12541_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&TypeLoadException_GetObjectData_m12542_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool TypeLoadException_t2202_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeLoadException_t2202_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLoadException_t2202_0_0_0;
extern const Il2CppType TypeLoadException_t2202_1_0_0;
struct TypeLoadException_t2202;
const Il2CppTypeDefinitionMetadata TypeLoadException_t2202_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLoadException_t2202_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1464_0_0_0/* parent */
	, TypeLoadException_t2202_VTable/* vtableMethods */
	, TypeLoadException_t2202_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3081/* fieldStart */

};
TypeInfo TypeLoadException_t2202_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLoadException"/* name */
	, "System"/* namespaze */
	, TypeLoadException_t2202_MethodInfos/* methods */
	, TypeLoadException_t2202_PropertyInfos/* properties */
	, NULL/* events */
	, &TypeLoadException_t2202_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 999/* custom_attributes_cache */
	, &TypeLoadException_t2202_0_0_0/* byval_arg */
	, &TypeLoadException_t2202_1_0_0/* this_arg */
	, &TypeLoadException_t2202_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLoadException_t2202)/* instance_size */
	, sizeof (TypeLoadException_t2202)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnauthorizedAccessException
#include "mscorlib_System_UnauthorizedAccessException.h"
// Metadata Definition System.UnauthorizedAccessException
extern TypeInfo UnauthorizedAccessException_t2246_il2cpp_TypeInfo;
// System.UnauthorizedAccessException
#include "mscorlib_System_UnauthorizedAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor()
extern const MethodInfo UnauthorizedAccessException__ctor_m12543_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m12543/* method */
	, &UnauthorizedAccessException_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6079/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UnauthorizedAccessException_t2246_UnauthorizedAccessException__ctor_m12544_ParameterInfos[] = 
{
	{"message", 0, 134225299, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor(System.String)
extern const MethodInfo UnauthorizedAccessException__ctor_m12544_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m12544/* method */
	, &UnauthorizedAccessException_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UnauthorizedAccessException_t2246_UnauthorizedAccessException__ctor_m12544_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6080/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo UnauthorizedAccessException_t2246_UnauthorizedAccessException__ctor_m12545_ParameterInfos[] = 
{
	{"info", 0, 134225300, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225301, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnauthorizedAccessException__ctor_m12545_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m12545/* method */
	, &UnauthorizedAccessException_t2246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, UnauthorizedAccessException_t2246_UnauthorizedAccessException__ctor_m12545_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6081/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnauthorizedAccessException_t2246_MethodInfos[] =
{
	&UnauthorizedAccessException__ctor_m12543_MethodInfo,
	&UnauthorizedAccessException__ctor_m12544_MethodInfo,
	&UnauthorizedAccessException__ctor_m12545_MethodInfo,
	NULL
};
static const Il2CppMethodReference UnauthorizedAccessException_t2246_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Exception_ToString_m5385_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_get_InnerException_m5387_MethodInfo,
	&Exception_get_Message_m5388_MethodInfo,
	&Exception_get_Source_m5389_MethodInfo,
	&Exception_get_StackTrace_m5390_MethodInfo,
	&Exception_GetObjectData_m5386_MethodInfo,
	&Exception_GetType_m5391_MethodInfo,
};
static bool UnauthorizedAccessException_t2246_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnauthorizedAccessException_t2246_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &_Exception_t1147_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnauthorizedAccessException_t2246_0_0_0;
extern const Il2CppType UnauthorizedAccessException_t2246_1_0_0;
struct UnauthorizedAccessException_t2246;
const Il2CppTypeDefinitionMetadata UnauthorizedAccessException_t2246_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnauthorizedAccessException_t2246_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1464_0_0_0/* parent */
	, UnauthorizedAccessException_t2246_VTable/* vtableMethods */
	, UnauthorizedAccessException_t2246_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnauthorizedAccessException_t2246_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnauthorizedAccessException"/* name */
	, "System"/* namespaze */
	, UnauthorizedAccessException_t2246_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnauthorizedAccessException_t2246_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1000/* custom_attributes_cache */
	, &UnauthorizedAccessException_t2246_0_0_0/* byval_arg */
	, &UnauthorizedAccessException_t2246_1_0_0/* this_arg */
	, &UnauthorizedAccessException_t2246_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnauthorizedAccessException_t2246)/* instance_size */
	, sizeof (UnauthorizedAccessException_t2246)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnhandledExceptionEventArgs
#include "mscorlib_System_UnhandledExceptionEventArgs.h"
// Metadata Definition System.UnhandledExceptionEventArgs
extern TypeInfo UnhandledExceptionEventArgs_t2247_il2cpp_TypeInfo;
// System.UnhandledExceptionEventArgs
#include "mscorlib_System_UnhandledExceptionEventArgsMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo UnhandledExceptionEventArgs_t2247_UnhandledExceptionEventArgs__ctor_m12546_ParameterInfos[] = 
{
	{"exception", 0, 134225302, 0, &Object_t_0_0_0},
	{"isTerminating", 1, 134225303, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventArgs::.ctor(System.Object,System.Boolean)
extern const MethodInfo UnhandledExceptionEventArgs__ctor_m12546_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs__ctor_m12546/* method */
	, &UnhandledExceptionEventArgs_t2247_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, UnhandledExceptionEventArgs_t2247_UnhandledExceptionEventArgs__ctor_m12546_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6082/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.UnhandledExceptionEventArgs::get_ExceptionObject()
extern const MethodInfo UnhandledExceptionEventArgs_get_ExceptionObject_m12547_MethodInfo = 
{
	"get_ExceptionObject"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs_get_ExceptionObject_m12547/* method */
	, &UnhandledExceptionEventArgs_t2247_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 1002/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6083/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.UnhandledExceptionEventArgs::get_IsTerminating()
extern const MethodInfo UnhandledExceptionEventArgs_get_IsTerminating_m12548_MethodInfo = 
{
	"get_IsTerminating"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs_get_IsTerminating_m12548/* method */
	, &UnhandledExceptionEventArgs_t2247_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 1003/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6084/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnhandledExceptionEventArgs_t2247_MethodInfos[] =
{
	&UnhandledExceptionEventArgs__ctor_m12546_MethodInfo,
	&UnhandledExceptionEventArgs_get_ExceptionObject_m12547_MethodInfo,
	&UnhandledExceptionEventArgs_get_IsTerminating_m12548_MethodInfo,
	NULL
};
extern const MethodInfo UnhandledExceptionEventArgs_get_ExceptionObject_m12547_MethodInfo;
static const PropertyInfo UnhandledExceptionEventArgs_t2247____ExceptionObject_PropertyInfo = 
{
	&UnhandledExceptionEventArgs_t2247_il2cpp_TypeInfo/* parent */
	, "ExceptionObject"/* name */
	, &UnhandledExceptionEventArgs_get_ExceptionObject_m12547_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo UnhandledExceptionEventArgs_get_IsTerminating_m12548_MethodInfo;
static const PropertyInfo UnhandledExceptionEventArgs_t2247____IsTerminating_PropertyInfo = 
{
	&UnhandledExceptionEventArgs_t2247_il2cpp_TypeInfo/* parent */
	, "IsTerminating"/* name */
	, &UnhandledExceptionEventArgs_get_IsTerminating_m12548_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UnhandledExceptionEventArgs_t2247_PropertyInfos[] =
{
	&UnhandledExceptionEventArgs_t2247____ExceptionObject_PropertyInfo,
	&UnhandledExceptionEventArgs_t2247____IsTerminating_PropertyInfo,
	NULL
};
static const Il2CppMethodReference UnhandledExceptionEventArgs_t2247_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool UnhandledExceptionEventArgs_t2247_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnhandledExceptionEventArgs_t2247_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t2247_1_0_0;
struct UnhandledExceptionEventArgs_t2247;
const Il2CppTypeDefinitionMetadata UnhandledExceptionEventArgs_t2247_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t1547_0_0_0/* parent */
	, UnhandledExceptionEventArgs_t2247_VTable/* vtableMethods */
	, UnhandledExceptionEventArgs_t2247_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3084/* fieldStart */

};
TypeInfo UnhandledExceptionEventArgs_t2247_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnhandledExceptionEventArgs"/* name */
	, "System"/* namespaze */
	, UnhandledExceptionEventArgs_t2247_MethodInfos/* methods */
	, UnhandledExceptionEventArgs_t2247_PropertyInfos/* properties */
	, NULL/* events */
	, &UnhandledExceptionEventArgs_t2247_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1001/* custom_attributes_cache */
	, &UnhandledExceptionEventArgs_t2247_0_0_0/* byval_arg */
	, &UnhandledExceptionEventArgs_t2247_1_0_0/* this_arg */
	, &UnhandledExceptionEventArgs_t2247_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnhandledExceptionEventArgs_t2247)/* instance_size */
	, sizeof (UnhandledExceptionEventArgs_t2247)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityType.h"
// Metadata Definition System.UnitySerializationHolder/UnityType
extern TypeInfo UnityType_t2248_il2cpp_TypeInfo;
// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityTypeMethodDeclarations.h"
static const MethodInfo* UnityType_t2248_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UnityType_t2248_VTable[] =
{
	&Enum_Equals_m1051_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Enum_GetHashCode_m1052_MethodInfo,
	&Enum_ToString_m1053_MethodInfo,
	&Enum_ToString_m1054_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1055_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1056_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1057_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1058_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1059_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1060_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1061_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1062_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1063_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1064_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1065_MethodInfo,
	&Enum_ToString_m1066_MethodInfo,
	&Enum_System_IConvertible_ToType_m1067_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1068_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1069_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1070_MethodInfo,
	&Enum_CompareTo_m1071_MethodInfo,
	&Enum_GetTypeCode_m1072_MethodInfo,
};
static bool UnityType_t2248_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityType_t2248_InterfacesOffsets[] = 
{
	{ &IFormattable_t275_0_0_0, 4},
	{ &IConvertible_t276_0_0_0, 5},
	{ &IComparable_t277_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnityType_t2248_0_0_0;
extern const Il2CppType UnityType_t2248_1_0_0;
extern TypeInfo UnitySerializationHolder_t2249_il2cpp_TypeInfo;
extern const Il2CppType UnitySerializationHolder_t2249_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t680_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata UnityType_t2248_DefinitionMetadata = 
{
	&UnitySerializationHolder_t2249_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityType_t2248_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t278_0_0_0/* parent */
	, UnityType_t2248_VTable/* vtableMethods */
	, UnityType_t2248_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3086/* fieldStart */

};
TypeInfo UnityType_t2248_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityType"/* name */
	, ""/* namespaze */
	, UnityType_t2248_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityType_t2248_0_0_0/* byval_arg */
	, &UnityType_t2248_1_0_0/* this_arg */
	, &UnityType_t2248_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityType_t2248)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityType_t2248)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UnitySerializationHolder
#include "mscorlib_System_UnitySerializationHolder.h"
// Metadata Definition System.UnitySerializationHolder
// System.UnitySerializationHolder
#include "mscorlib_System_UnitySerializationHolderMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2249_UnitySerializationHolder__ctor_m12549_ParameterInfos[] = 
{
	{"info", 0, 134225304, 0, &SerializationInfo_t1043_0_0_0},
	{"ctx", 1, 134225305, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder__ctor_m12549_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnitySerializationHolder__ctor_m12549/* method */
	, &UnitySerializationHolder_t2249_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, UnitySerializationHolder_t2249_UnitySerializationHolder__ctor_m12549_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6085/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2249_UnitySerializationHolder_GetTypeData_m12550_ParameterInfos[] = 
{
	{"instance", 0, 134225306, 0, &Type_t_0_0_0},
	{"info", 1, 134225307, 0, &SerializationInfo_t1043_0_0_0},
	{"ctx", 2, 134225308, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetTypeData(System.Type,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetTypeData_m12550_MethodInfo = 
{
	"GetTypeData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetTypeData_m12550/* method */
	, &UnitySerializationHolder_t2249_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, UnitySerializationHolder_t2249_UnitySerializationHolder_GetTypeData_m12550_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6086/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DBNull_t2193_0_0_0;
extern const Il2CppType DBNull_t2193_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2249_UnitySerializationHolder_GetDBNullData_m12551_ParameterInfos[] = 
{
	{"instance", 0, 134225309, 0, &DBNull_t2193_0_0_0},
	{"info", 1, 134225310, 0, &SerializationInfo_t1043_0_0_0},
	{"ctx", 2, 134225311, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetDBNullData(System.DBNull,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetDBNullData_m12551_MethodInfo = 
{
	"GetDBNullData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetDBNullData_m12551/* method */
	, &UnitySerializationHolder_t2249_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, UnitySerializationHolder_t2249_UnitySerializationHolder_GetDBNullData_m12551_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6087/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Module_t1848_0_0_0;
extern const Il2CppType Module_t1848_0_0_0;
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2249_UnitySerializationHolder_GetModuleData_m12552_ParameterInfos[] = 
{
	{"instance", 0, 134225312, 0, &Module_t1848_0_0_0},
	{"info", 1, 134225313, 0, &SerializationInfo_t1043_0_0_0},
	{"ctx", 2, 134225314, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetModuleData(System.Reflection.Module,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetModuleData_m12552_MethodInfo = 
{
	"GetModuleData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetModuleData_m12552/* method */
	, &UnitySerializationHolder_t2249_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t_StreamingContext_t1044/* invoker_method */
	, UnitySerializationHolder_t2249_UnitySerializationHolder_GetModuleData_m12552_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6088/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2249_UnitySerializationHolder_GetObjectData_m12553_ParameterInfos[] = 
{
	{"info", 0, 134225315, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225316, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetObjectData_m12553_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetObjectData_m12553/* method */
	, &UnitySerializationHolder_t2249_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, UnitySerializationHolder_t2249_UnitySerializationHolder_GetObjectData_m12553_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6089/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2249_UnitySerializationHolder_GetRealObject_m12554_ParameterInfos[] = 
{
	{"context", 0, 134225317, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Object System.UnitySerializationHolder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetRealObject_m12554_MethodInfo = 
{
	"GetRealObject"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetRealObject_m12554/* method */
	, &UnitySerializationHolder_t2249_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t1044/* invoker_method */
	, UnitySerializationHolder_t2249_UnitySerializationHolder_GetRealObject_m12554_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6090/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnitySerializationHolder_t2249_MethodInfos[] =
{
	&UnitySerializationHolder__ctor_m12549_MethodInfo,
	&UnitySerializationHolder_GetTypeData_m12550_MethodInfo,
	&UnitySerializationHolder_GetDBNullData_m12551_MethodInfo,
	&UnitySerializationHolder_GetModuleData_m12552_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m12553_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m12554_MethodInfo,
	NULL
};
static const Il2CppType* UnitySerializationHolder_t2249_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UnityType_t2248_0_0_0,
};
extern const MethodInfo UnitySerializationHolder_GetObjectData_m12553_MethodInfo;
extern const MethodInfo UnitySerializationHolder_GetRealObject_m12554_MethodInfo;
static const Il2CppMethodReference UnitySerializationHolder_t2249_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m12553_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m12554_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m12553_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m12554_MethodInfo,
};
static bool UnitySerializationHolder_t2249_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IObjectReference_t2310_0_0_0;
static const Il2CppType* UnitySerializationHolder_t2249_InterfacesTypeInfos[] = 
{
	&ISerializable_t439_0_0_0,
	&IObjectReference_t2310_0_0_0,
};
static Il2CppInterfaceOffsetPair UnitySerializationHolder_t2249_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
	{ &IObjectReference_t2310_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnitySerializationHolder_t2249_1_0_0;
struct UnitySerializationHolder_t2249;
const Il2CppTypeDefinitionMetadata UnitySerializationHolder_t2249_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UnitySerializationHolder_t2249_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, UnitySerializationHolder_t2249_InterfacesTypeInfos/* implementedInterfaces */
	, UnitySerializationHolder_t2249_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnitySerializationHolder_t2249_VTable/* vtableMethods */
	, UnitySerializationHolder_t2249_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3091/* fieldStart */

};
TypeInfo UnitySerializationHolder_t2249_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnitySerializationHolder"/* name */
	, "System"/* namespaze */
	, UnitySerializationHolder_t2249_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnitySerializationHolder_t2249_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnitySerializationHolder_t2249_0_0_0/* byval_arg */
	, &UnitySerializationHolder_t2249_1_0_0/* this_arg */
	, &UnitySerializationHolder_t2249_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnitySerializationHolder_t2249)/* instance_size */
	, sizeof (UnitySerializationHolder_t2249)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Version
#include "mscorlib_System_Version.h"
// Metadata Definition System.Version
extern TypeInfo Version_t1292_il2cpp_TypeInfo;
// System.Version
#include "mscorlib_System_VersionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor()
extern const MethodInfo Version__ctor_m12555_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m12555/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6091/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Version_t1292_Version__ctor_m12556_ParameterInfos[] = 
{
	{"version", 0, 134225318, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.String)
extern const MethodInfo Version__ctor_m12556_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m12556/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, Version_t1292_Version__ctor_m12556_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6092/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Version_t1292_Version__ctor_m6403_ParameterInfos[] = 
{
	{"major", 0, 134225319, 0, &Int32_t253_0_0_0},
	{"minor", 1, 134225320, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.Int32,System.Int32)
extern const MethodInfo Version__ctor_m6403_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m6403/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253/* invoker_method */
	, Version_t1292_Version__ctor_m6403_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6093/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Version_t1292_Version__ctor_m12557_ParameterInfos[] = 
{
	{"major", 0, 134225321, 0, &Int32_t253_0_0_0},
	{"minor", 1, 134225322, 0, &Int32_t253_0_0_0},
	{"build", 2, 134225323, 0, &Int32_t253_0_0_0},
	{"revision", 3, 134225324, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Version__ctor_m12557_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m12557/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, Version_t1292_Version__ctor_m12557_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6094/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
extern const Il2CppType Int32_t253_0_0_0;
static const ParameterInfo Version_t1292_Version_CheckedSet_m12558_ParameterInfos[] = 
{
	{"defined", 0, 134225325, 0, &Int32_t253_0_0_0},
	{"major", 1, 134225326, 0, &Int32_t253_0_0_0},
	{"minor", 2, 134225327, 0, &Int32_t253_0_0_0},
	{"build", 3, 134225328, 0, &Int32_t253_0_0_0},
	{"revision", 4, 134225329, 0, &Int32_t253_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::CheckedSet(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Version_CheckedSet_m12558_MethodInfo = 
{
	"CheckedSet"/* name */
	, (methodPointerType)&Version_CheckedSet_m12558/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int32_t253_Int32_t253_Int32_t253_Int32_t253_Int32_t253/* invoker_method */
	, Version_t1292_Version_CheckedSet_m12558_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6095/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Build()
extern const MethodInfo Version_get_Build_m12559_MethodInfo = 
{
	"get_Build"/* name */
	, (methodPointerType)&Version_get_Build_m12559/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6096/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Major()
extern const MethodInfo Version_get_Major_m12560_MethodInfo = 
{
	"get_Major"/* name */
	, (methodPointerType)&Version_get_Major_m12560/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6097/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Minor()
extern const MethodInfo Version_get_Minor_m12561_MethodInfo = 
{
	"get_Minor"/* name */
	, (methodPointerType)&Version_get_Minor_m12561/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6098/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Revision()
extern const MethodInfo Version_get_Revision_m12562_MethodInfo = 
{
	"get_Revision"/* name */
	, (methodPointerType)&Version_get_Revision_m12562/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6099/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Version_t1292_Version_CompareTo_m12563_ParameterInfos[] = 
{
	{"version", 0, 134225330, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::CompareTo(System.Object)
extern const MethodInfo Version_CompareTo_m12563_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Version_CompareTo_m12563/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, Version_t1292_Version_CompareTo_m12563_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Version_t1292_Version_Equals_m12564_ParameterInfos[] = 
{
	{"obj", 0, 134225331, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::Equals(System.Object)
extern const MethodInfo Version_Equals_m12564_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Version_Equals_m12564/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Version_t1292_Version_Equals_m12564_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1292_0_0_0;
static const ParameterInfo Version_t1292_Version_CompareTo_m12565_ParameterInfos[] = 
{
	{"value", 0, 134225332, 0, &Version_t1292_0_0_0},
};
extern void* RuntimeInvoker_Int32_t253_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::CompareTo(System.Version)
extern const MethodInfo Version_CompareTo_m12565_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Version_CompareTo_m12565/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253_Object_t/* invoker_method */
	, Version_t1292_Version_CompareTo_m12565_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1292_0_0_0;
static const ParameterInfo Version_t1292_Version_Equals_m12566_ParameterInfos[] = 
{
	{"obj", 0, 134225333, 0, &Version_t1292_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::Equals(System.Version)
extern const MethodInfo Version_Equals_m12566_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Version_Equals_m12566/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, Version_t1292_Version_Equals_m12566_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::GetHashCode()
extern const MethodInfo Version_GetHashCode_m12567_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Version_GetHashCode_m12567/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Version::ToString()
extern const MethodInfo Version_ToString_m12568_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Version_ToString_m12568/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Version_t1292_Version_CreateFromString_m12569_ParameterInfos[] = 
{
	{"info", 0, 134225334, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Version System.Version::CreateFromString(System.String)
extern const MethodInfo Version_CreateFromString_m12569_MethodInfo = 
{
	"CreateFromString"/* name */
	, (methodPointerType)&Version_CreateFromString_m12569/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Version_t1292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Version_t1292_Version_CreateFromString_m12569_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1292_0_0_0;
extern const Il2CppType Version_t1292_0_0_0;
static const ParameterInfo Version_t1292_Version_op_Equality_m12570_ParameterInfos[] = 
{
	{"v1", 0, 134225335, 0, &Version_t1292_0_0_0},
	{"v2", 1, 134225336, 0, &Version_t1292_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::op_Equality(System.Version,System.Version)
extern const MethodInfo Version_op_Equality_m12570_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&Version_op_Equality_m12570/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, Version_t1292_Version_op_Equality_m12570_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1292_0_0_0;
extern const Il2CppType Version_t1292_0_0_0;
static const ParameterInfo Version_t1292_Version_op_Inequality_m12571_ParameterInfos[] = 
{
	{"v1", 0, 134225337, 0, &Version_t1292_0_0_0},
	{"v2", 1, 134225338, 0, &Version_t1292_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::op_Inequality(System.Version,System.Version)
extern const MethodInfo Version_op_Inequality_m12571_MethodInfo = 
{
	"op_Inequality"/* name */
	, (methodPointerType)&Version_op_Inequality_m12571/* method */
	, &Version_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, Version_t1292_Version_op_Inequality_m12571_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Version_t1292_MethodInfos[] =
{
	&Version__ctor_m12555_MethodInfo,
	&Version__ctor_m12556_MethodInfo,
	&Version__ctor_m6403_MethodInfo,
	&Version__ctor_m12557_MethodInfo,
	&Version_CheckedSet_m12558_MethodInfo,
	&Version_get_Build_m12559_MethodInfo,
	&Version_get_Major_m12560_MethodInfo,
	&Version_get_Minor_m12561_MethodInfo,
	&Version_get_Revision_m12562_MethodInfo,
	&Version_CompareTo_m12563_MethodInfo,
	&Version_Equals_m12564_MethodInfo,
	&Version_CompareTo_m12565_MethodInfo,
	&Version_Equals_m12566_MethodInfo,
	&Version_GetHashCode_m12567_MethodInfo,
	&Version_ToString_m12568_MethodInfo,
	&Version_CreateFromString_m12569_MethodInfo,
	&Version_op_Equality_m12570_MethodInfo,
	&Version_op_Inequality_m12571_MethodInfo,
	NULL
};
extern const MethodInfo Version_get_Build_m12559_MethodInfo;
static const PropertyInfo Version_t1292____Build_PropertyInfo = 
{
	&Version_t1292_il2cpp_TypeInfo/* parent */
	, "Build"/* name */
	, &Version_get_Build_m12559_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Major_m12560_MethodInfo;
static const PropertyInfo Version_t1292____Major_PropertyInfo = 
{
	&Version_t1292_il2cpp_TypeInfo/* parent */
	, "Major"/* name */
	, &Version_get_Major_m12560_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Minor_m12561_MethodInfo;
static const PropertyInfo Version_t1292____Minor_PropertyInfo = 
{
	&Version_t1292_il2cpp_TypeInfo/* parent */
	, "Minor"/* name */
	, &Version_get_Minor_m12561_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Revision_m12562_MethodInfo;
static const PropertyInfo Version_t1292____Revision_PropertyInfo = 
{
	&Version_t1292_il2cpp_TypeInfo/* parent */
	, "Revision"/* name */
	, &Version_get_Revision_m12562_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Version_t1292_PropertyInfos[] =
{
	&Version_t1292____Build_PropertyInfo,
	&Version_t1292____Major_PropertyInfo,
	&Version_t1292____Minor_PropertyInfo,
	&Version_t1292____Revision_PropertyInfo,
	NULL
};
extern const MethodInfo Version_Equals_m12564_MethodInfo;
extern const MethodInfo Version_GetHashCode_m12567_MethodInfo;
extern const MethodInfo Version_ToString_m12568_MethodInfo;
extern const MethodInfo Version_CompareTo_m12563_MethodInfo;
extern const MethodInfo Version_CompareTo_m12565_MethodInfo;
extern const MethodInfo Version_Equals_m12566_MethodInfo;
static const Il2CppMethodReference Version_t1292_VTable[] =
{
	&Version_Equals_m12564_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Version_GetHashCode_m12567_MethodInfo,
	&Version_ToString_m12568_MethodInfo,
	&Version_CompareTo_m12563_MethodInfo,
	&Version_CompareTo_m12565_MethodInfo,
	&Version_Equals_m12566_MethodInfo,
};
static bool Version_t1292_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparable_1_t2794_0_0_0;
extern const Il2CppType IEquatable_1_t2795_0_0_0;
static const Il2CppType* Version_t1292_InterfacesTypeInfos[] = 
{
	&IComparable_t277_0_0_0,
	&ICloneable_t733_0_0_0,
	&IComparable_1_t2794_0_0_0,
	&IEquatable_1_t2795_0_0_0,
};
static Il2CppInterfaceOffsetPair Version_t1292_InterfacesOffsets[] = 
{
	{ &IComparable_t277_0_0_0, 4},
	{ &ICloneable_t733_0_0_0, 5},
	{ &IComparable_1_t2794_0_0_0, 5},
	{ &IEquatable_1_t2795_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Version_t1292_1_0_0;
struct Version_t1292;
const Il2CppTypeDefinitionMetadata Version_t1292_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Version_t1292_InterfacesTypeInfos/* implementedInterfaces */
	, Version_t1292_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Version_t1292_VTable/* vtableMethods */
	, Version_t1292_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3094/* fieldStart */

};
TypeInfo Version_t1292_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Version"/* name */
	, "System"/* namespaze */
	, Version_t1292_MethodInfos/* methods */
	, Version_t1292_PropertyInfos/* properties */
	, NULL/* events */
	, &Version_t1292_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1004/* custom_attributes_cache */
	, &Version_t1292_0_0_0/* byval_arg */
	, &Version_t1292_1_0_0/* this_arg */
	, &Version_t1292_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Version_t1292)/* instance_size */
	, sizeof (Version_t1292)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.WeakReference
#include "mscorlib_System_WeakReference.h"
// Metadata Definition System.WeakReference
extern TypeInfo WeakReference_t2009_il2cpp_TypeInfo;
// System.WeakReference
#include "mscorlib_System_WeakReferenceMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor()
extern const MethodInfo WeakReference__ctor_m12572_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m12572/* method */
	, &WeakReference_t2009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WeakReference_t2009_WeakReference__ctor_m12573_ParameterInfos[] = 
{
	{"target", 0, 134225339, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Object)
extern const MethodInfo WeakReference__ctor_m12573_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m12573/* method */
	, &WeakReference_t2009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, WeakReference_t2009_WeakReference__ctor_m12573_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo WeakReference_t2009_WeakReference__ctor_m12574_ParameterInfos[] = 
{
	{"target", 0, 134225340, 0, &Object_t_0_0_0},
	{"trackResurrection", 1, 134225341, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Object,System.Boolean)
extern const MethodInfo WeakReference__ctor_m12574_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m12574/* method */
	, &WeakReference_t2009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, WeakReference_t2009_WeakReference__ctor_m12574_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo WeakReference_t2009_WeakReference__ctor_m12575_ParameterInfos[] = 
{
	{"info", 0, 134225342, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225343, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo WeakReference__ctor_m12575_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m12575/* method */
	, &WeakReference_t2009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, WeakReference_t2009_WeakReference__ctor_m12575_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WeakReference_t2009_WeakReference_AllocateHandle_m12576_ParameterInfos[] = 
{
	{"target", 0, 134225344, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::AllocateHandle(System.Object)
extern const MethodInfo WeakReference_AllocateHandle_m12576_MethodInfo = 
{
	"AllocateHandle"/* name */
	, (methodPointerType)&WeakReference_AllocateHandle_m12576/* method */
	, &WeakReference_t2009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, WeakReference_t2009_WeakReference_AllocateHandle_m12576_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.WeakReference::get_Target()
extern const MethodInfo WeakReference_get_Target_m12577_MethodInfo = 
{
	"get_Target"/* name */
	, (methodPointerType)&WeakReference_get_Target_m12577/* method */
	, &WeakReference_t2009_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t273 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.WeakReference::get_TrackResurrection()
extern const MethodInfo WeakReference_get_TrackResurrection_m12578_MethodInfo = 
{
	"get_TrackResurrection"/* name */
	, (methodPointerType)&WeakReference_get_TrackResurrection_m12578/* method */
	, &WeakReference_t2009_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::Finalize()
extern const MethodInfo WeakReference_Finalize_m12579_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&WeakReference_Finalize_m12579/* method */
	, &WeakReference_t2009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1043_0_0_0;
extern const Il2CppType StreamingContext_t1044_0_0_0;
static const ParameterInfo WeakReference_t2009_WeakReference_GetObjectData_m12580_ParameterInfos[] = 
{
	{"info", 0, 134225345, 0, &SerializationInfo_t1043_0_0_0},
	{"context", 1, 134225346, 0, &StreamingContext_t1044_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo WeakReference_GetObjectData_m12580_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&WeakReference_GetObjectData_m12580/* method */
	, &WeakReference_t2009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_StreamingContext_t1044/* invoker_method */
	, WeakReference_t2009_WeakReference_GetObjectData_m12580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WeakReference_t2009_MethodInfos[] =
{
	&WeakReference__ctor_m12572_MethodInfo,
	&WeakReference__ctor_m12573_MethodInfo,
	&WeakReference__ctor_m12574_MethodInfo,
	&WeakReference__ctor_m12575_MethodInfo,
	&WeakReference_AllocateHandle_m12576_MethodInfo,
	&WeakReference_get_Target_m12577_MethodInfo,
	&WeakReference_get_TrackResurrection_m12578_MethodInfo,
	&WeakReference_Finalize_m12579_MethodInfo,
	&WeakReference_GetObjectData_m12580_MethodInfo,
	NULL
};
extern const MethodInfo WeakReference_get_Target_m12577_MethodInfo;
static const PropertyInfo WeakReference_t2009____Target_PropertyInfo = 
{
	&WeakReference_t2009_il2cpp_TypeInfo/* parent */
	, "Target"/* name */
	, &WeakReference_get_Target_m12577_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WeakReference_get_TrackResurrection_m12578_MethodInfo;
static const PropertyInfo WeakReference_t2009____TrackResurrection_PropertyInfo = 
{
	&WeakReference_t2009_il2cpp_TypeInfo/* parent */
	, "TrackResurrection"/* name */
	, &WeakReference_get_TrackResurrection_m12578_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* WeakReference_t2009_PropertyInfos[] =
{
	&WeakReference_t2009____Target_PropertyInfo,
	&WeakReference_t2009____TrackResurrection_PropertyInfo,
	NULL
};
extern const MethodInfo WeakReference_Finalize_m12579_MethodInfo;
extern const MethodInfo WeakReference_GetObjectData_m12580_MethodInfo;
static const Il2CppMethodReference WeakReference_t2009_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&WeakReference_Finalize_m12579_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&WeakReference_GetObjectData_m12580_MethodInfo,
	&WeakReference_get_Target_m12577_MethodInfo,
	&WeakReference_get_TrackResurrection_m12578_MethodInfo,
	&WeakReference_GetObjectData_m12580_MethodInfo,
};
static bool WeakReference_t2009_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* WeakReference_t2009_InterfacesTypeInfos[] = 
{
	&ISerializable_t439_0_0_0,
};
static Il2CppInterfaceOffsetPair WeakReference_t2009_InterfacesOffsets[] = 
{
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WeakReference_t2009_0_0_0;
extern const Il2CppType WeakReference_t2009_1_0_0;
struct WeakReference_t2009;
const Il2CppTypeDefinitionMetadata WeakReference_t2009_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WeakReference_t2009_InterfacesTypeInfos/* implementedInterfaces */
	, WeakReference_t2009_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WeakReference_t2009_VTable/* vtableMethods */
	, WeakReference_t2009_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3099/* fieldStart */

};
TypeInfo WeakReference_t2009_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WeakReference"/* name */
	, "System"/* namespaze */
	, WeakReference_t2009_MethodInfos/* methods */
	, WeakReference_t2009_PropertyInfos/* properties */
	, NULL/* events */
	, &WeakReference_t2009_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1005/* custom_attributes_cache */
	, &WeakReference_t2009_0_0_0/* byval_arg */
	, &WeakReference_t2009_1_0_0/* this_arg */
	, &WeakReference_t2009_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WeakReference_t2009)/* instance_size */
	, sizeof (WeakReference_t2009)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTest
#include "mscorlib_Mono_Math_Prime_PrimalityTest.h"
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern TypeInfo PrimalityTest_t2250_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
#include "mscorlib_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrimalityTest_t2250_PrimalityTest__ctor_m12581_ParameterInfos[] = 
{
	{"object", 0, 134225347, 0, &Object_t_0_0_0},
	{"method", 1, 134225348, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Math.Prime.PrimalityTest::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrimalityTest__ctor_m12581_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrimalityTest__ctor_m12581/* method */
	, &PrimalityTest_t2250_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, PrimalityTest_t2250_PrimalityTest__ctor_m12581_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1716_0_0_0;
extern const Il2CppType BigInteger_t1716_0_0_0;
extern const Il2CppType ConfidenceFactor_t1713_0_0_0;
extern const Il2CppType ConfidenceFactor_t1713_0_0_0;
static const ParameterInfo PrimalityTest_t2250_PrimalityTest_Invoke_m12582_ParameterInfos[] = 
{
	{"bi", 0, 134225349, 0, &BigInteger_t1716_0_0_0},
	{"confidence", 1, 134225350, 0, &ConfidenceFactor_t1713_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::Invoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor)
extern const MethodInfo PrimalityTest_Invoke_m12582_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrimalityTest_Invoke_m12582/* method */
	, &PrimalityTest_t2250_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Int32_t253/* invoker_method */
	, PrimalityTest_t2250_PrimalityTest_Invoke_m12582_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1716_0_0_0;
extern const Il2CppType ConfidenceFactor_t1713_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrimalityTest_t2250_PrimalityTest_BeginInvoke_m12583_ParameterInfos[] = 
{
	{"bi", 0, 134225351, 0, &BigInteger_t1716_0_0_0},
	{"confidence", 1, 134225352, 0, &ConfidenceFactor_t1713_0_0_0},
	{"callback", 2, 134225353, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134225354, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t546_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Math.Prime.PrimalityTest::BeginInvoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor,System.AsyncCallback,System.Object)
extern const MethodInfo PrimalityTest_BeginInvoke_m12583_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrimalityTest_BeginInvoke_m12583/* method */
	, &PrimalityTest_t2250_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Object_t/* invoker_method */
	, PrimalityTest_t2250_PrimalityTest_BeginInvoke_m12583_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo PrimalityTest_t2250_PrimalityTest_EndInvoke_m12584_ParameterInfos[] = 
{
	{"result", 0, 134225355, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrimalityTest_EndInvoke_m12584_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrimalityTest_EndInvoke_m12584/* method */
	, &PrimalityTest_t2250_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, PrimalityTest_t2250_PrimalityTest_EndInvoke_m12584_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrimalityTest_t2250_MethodInfos[] =
{
	&PrimalityTest__ctor_m12581_MethodInfo,
	&PrimalityTest_Invoke_m12582_MethodInfo,
	&PrimalityTest_BeginInvoke_m12583_MethodInfo,
	&PrimalityTest_EndInvoke_m12584_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m3586_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m3587_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m3588_MethodInfo;
extern const MethodInfo Delegate_Clone_m3589_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m3590_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m3591_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m3592_MethodInfo;
extern const MethodInfo PrimalityTest_Invoke_m12582_MethodInfo;
extern const MethodInfo PrimalityTest_BeginInvoke_m12583_MethodInfo;
extern const MethodInfo PrimalityTest_EndInvoke_m12584_MethodInfo;
static const Il2CppMethodReference PrimalityTest_t2250_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&PrimalityTest_Invoke_m12582_MethodInfo,
	&PrimalityTest_BeginInvoke_m12583_MethodInfo,
	&PrimalityTest_EndInvoke_m12584_MethodInfo,
};
static bool PrimalityTest_t2250_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PrimalityTest_t2250_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrimalityTest_t2250_0_0_0;
extern const Il2CppType PrimalityTest_t2250_1_0_0;
extern const Il2CppType MulticastDelegate_t549_0_0_0;
struct PrimalityTest_t2250;
const Il2CppTypeDefinitionMetadata PrimalityTest_t2250_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrimalityTest_t2250_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, PrimalityTest_t2250_VTable/* vtableMethods */
	, PrimalityTest_t2250_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrimalityTest_t2250_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTest"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, PrimalityTest_t2250_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrimalityTest_t2250_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTest_t2250_0_0_0/* byval_arg */
	, &PrimalityTest_t2250_1_0_0/* this_arg */
	, &PrimalityTest_t2250_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrimalityTest_t2250/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTest_t2250)/* instance_size */
	, sizeof (PrimalityTest_t2250)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.MemberFilter
#include "mscorlib_System_Reflection_MemberFilter.h"
// Metadata Definition System.Reflection.MemberFilter
extern TypeInfo MemberFilter_t1666_il2cpp_TypeInfo;
// System.Reflection.MemberFilter
#include "mscorlib_System_Reflection_MemberFilterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MemberFilter_t1666_MemberFilter__ctor_m12585_ParameterInfos[] = 
{
	{"object", 0, 134225356, 0, &Object_t_0_0_0},
	{"method", 1, 134225357, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberFilter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo MemberFilter__ctor_m12585_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberFilter__ctor_m12585/* method */
	, &MemberFilter_t1666_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, MemberFilter_t1666_MemberFilter__ctor_m12585_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MemberFilter_t1666_MemberFilter_Invoke_m12586_ParameterInfos[] = 
{
	{"m", 0, 134225358, 0, &MemberInfo_t_0_0_0},
	{"filterCriteria", 1, 134225359, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MemberFilter::Invoke(System.Reflection.MemberInfo,System.Object)
extern const MethodInfo MemberFilter_Invoke_m12586_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MemberFilter_Invoke_m12586/* method */
	, &MemberFilter_t1666_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, MemberFilter_t1666_MemberFilter_Invoke_m12586_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MemberFilter_t1666_MemberFilter_BeginInvoke_m12587_ParameterInfos[] = 
{
	{"m", 0, 134225360, 0, &MemberInfo_t_0_0_0},
	{"filterCriteria", 1, 134225361, 0, &Object_t_0_0_0},
	{"callback", 2, 134225362, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134225363, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.MemberFilter::BeginInvoke(System.Reflection.MemberInfo,System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo MemberFilter_BeginInvoke_m12587_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&MemberFilter_BeginInvoke_m12587/* method */
	, &MemberFilter_t1666_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MemberFilter_t1666_MemberFilter_BeginInvoke_m12587_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo MemberFilter_t1666_MemberFilter_EndInvoke_m12588_ParameterInfos[] = 
{
	{"result", 0, 134225364, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MemberFilter::EndInvoke(System.IAsyncResult)
extern const MethodInfo MemberFilter_EndInvoke_m12588_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&MemberFilter_EndInvoke_m12588/* method */
	, &MemberFilter_t1666_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, MemberFilter_t1666_MemberFilter_EndInvoke_m12588_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MemberFilter_t1666_MethodInfos[] =
{
	&MemberFilter__ctor_m12585_MethodInfo,
	&MemberFilter_Invoke_m12586_MethodInfo,
	&MemberFilter_BeginInvoke_m12587_MethodInfo,
	&MemberFilter_EndInvoke_m12588_MethodInfo,
	NULL
};
extern const MethodInfo MemberFilter_Invoke_m12586_MethodInfo;
extern const MethodInfo MemberFilter_BeginInvoke_m12587_MethodInfo;
extern const MethodInfo MemberFilter_EndInvoke_m12588_MethodInfo;
static const Il2CppMethodReference MemberFilter_t1666_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&MemberFilter_Invoke_m12586_MethodInfo,
	&MemberFilter_BeginInvoke_m12587_MethodInfo,
	&MemberFilter_EndInvoke_m12588_MethodInfo,
};
static bool MemberFilter_t1666_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MemberFilter_t1666_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberFilter_t1666_0_0_0;
extern const Il2CppType MemberFilter_t1666_1_0_0;
struct MemberFilter_t1666;
const Il2CppTypeDefinitionMetadata MemberFilter_t1666_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberFilter_t1666_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, MemberFilter_t1666_VTable/* vtableMethods */
	, MemberFilter_t1666_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MemberFilter_t1666_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberFilter"/* name */
	, "System.Reflection"/* namespaze */
	, MemberFilter_t1666_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MemberFilter_t1666_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1006/* custom_attributes_cache */
	, &MemberFilter_t1666_0_0_0/* byval_arg */
	, &MemberFilter_t1666_1_0_0/* this_arg */
	, &MemberFilter_t1666_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MemberFilter_t1666/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberFilter_t1666)/* instance_size */
	, sizeof (MemberFilter_t1666)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TypeFilter
#include "mscorlib_System_Reflection_TypeFilter.h"
// Metadata Definition System.Reflection.TypeFilter
extern TypeInfo TypeFilter_t1904_il2cpp_TypeInfo;
// System.Reflection.TypeFilter
#include "mscorlib_System_Reflection_TypeFilterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo TypeFilter_t1904_TypeFilter__ctor_m12589_ParameterInfos[] = 
{
	{"object", 0, 134225365, 0, &Object_t_0_0_0},
	{"method", 1, 134225366, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TypeFilter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo TypeFilter__ctor_m12589_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeFilter__ctor_m12589/* method */
	, &TypeFilter_t1904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, TypeFilter_t1904_TypeFilter__ctor_m12589_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TypeFilter_t1904_TypeFilter_Invoke_m12590_ParameterInfos[] = 
{
	{"m", 0, 134225367, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134225368, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.TypeFilter::Invoke(System.Type,System.Object)
extern const MethodInfo TypeFilter_Invoke_m12590_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&TypeFilter_Invoke_m12590/* method */
	, &TypeFilter_t1904_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, TypeFilter_t1904_TypeFilter_Invoke_m12590_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TypeFilter_t1904_TypeFilter_BeginInvoke_m12591_ParameterInfos[] = 
{
	{"m", 0, 134225369, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134225370, 0, &Object_t_0_0_0},
	{"callback", 2, 134225371, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134225372, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.TypeFilter::BeginInvoke(System.Type,System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo TypeFilter_BeginInvoke_m12591_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&TypeFilter_BeginInvoke_m12591/* method */
	, &TypeFilter_t1904_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, TypeFilter_t1904_TypeFilter_BeginInvoke_m12591_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo TypeFilter_t1904_TypeFilter_EndInvoke_m12592_ParameterInfos[] = 
{
	{"result", 0, 134225373, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.TypeFilter::EndInvoke(System.IAsyncResult)
extern const MethodInfo TypeFilter_EndInvoke_m12592_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&TypeFilter_EndInvoke_m12592/* method */
	, &TypeFilter_t1904_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, TypeFilter_t1904_TypeFilter_EndInvoke_m12592_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeFilter_t1904_MethodInfos[] =
{
	&TypeFilter__ctor_m12589_MethodInfo,
	&TypeFilter_Invoke_m12590_MethodInfo,
	&TypeFilter_BeginInvoke_m12591_MethodInfo,
	&TypeFilter_EndInvoke_m12592_MethodInfo,
	NULL
};
extern const MethodInfo TypeFilter_Invoke_m12590_MethodInfo;
extern const MethodInfo TypeFilter_BeginInvoke_m12591_MethodInfo;
extern const MethodInfo TypeFilter_EndInvoke_m12592_MethodInfo;
static const Il2CppMethodReference TypeFilter_t1904_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&TypeFilter_Invoke_m12590_MethodInfo,
	&TypeFilter_BeginInvoke_m12591_MethodInfo,
	&TypeFilter_EndInvoke_m12592_MethodInfo,
};
static bool TypeFilter_t1904_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeFilter_t1904_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeFilter_t1904_0_0_0;
extern const Il2CppType TypeFilter_t1904_1_0_0;
struct TypeFilter_t1904;
const Il2CppTypeDefinitionMetadata TypeFilter_t1904_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeFilter_t1904_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, TypeFilter_t1904_VTable/* vtableMethods */
	, TypeFilter_t1904_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TypeFilter_t1904_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeFilter"/* name */
	, "System.Reflection"/* namespaze */
	, TypeFilter_t1904_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeFilter_t1904_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1007/* custom_attributes_cache */
	, &TypeFilter_t1904_0_0_0/* byval_arg */
	, &TypeFilter_t1904_1_0_0/* this_arg */
	, &TypeFilter_t1904_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_TypeFilter_t1904/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeFilter_t1904)/* instance_size */
	, sizeof (TypeFilter_t1904)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.HeaderHandler
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandler.h"
// Metadata Definition System.Runtime.Remoting.Messaging.HeaderHandler
extern TypeInfo HeaderHandler_t2252_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.HeaderHandler
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo HeaderHandler_t2252_HeaderHandler__ctor_m12593_ParameterInfos[] = 
{
	{"object", 0, 134225374, 0, &Object_t_0_0_0},
	{"method", 1, 134225375, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.HeaderHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo HeaderHandler__ctor_m12593_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HeaderHandler__ctor_m12593/* method */
	, &HeaderHandler_t2252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, HeaderHandler_t2252_HeaderHandler__ctor_m12593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HeaderU5BU5D_t2251_0_0_0;
extern const Il2CppType HeaderU5BU5D_t2251_0_0_0;
static const ParameterInfo HeaderHandler_t2252_HeaderHandler_Invoke_m12594_ParameterInfos[] = 
{
	{"headers", 0, 134225376, 0, &HeaderU5BU5D_t2251_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.HeaderHandler::Invoke(System.Runtime.Remoting.Messaging.Header[])
extern const MethodInfo HeaderHandler_Invoke_m12594_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&HeaderHandler_Invoke_m12594/* method */
	, &HeaderHandler_t2252_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t2252_HeaderHandler_Invoke_m12594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HeaderU5BU5D_t2251_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo HeaderHandler_t2252_HeaderHandler_BeginInvoke_m12595_ParameterInfos[] = 
{
	{"headers", 0, 134225377, 0, &HeaderU5BU5D_t2251_0_0_0},
	{"callback", 1, 134225378, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134225379, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Runtime.Remoting.Messaging.HeaderHandler::BeginInvoke(System.Runtime.Remoting.Messaging.Header[],System.AsyncCallback,System.Object)
extern const MethodInfo HeaderHandler_BeginInvoke_m12595_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&HeaderHandler_BeginInvoke_m12595/* method */
	, &HeaderHandler_t2252_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t2252_HeaderHandler_BeginInvoke_m12595_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo HeaderHandler_t2252_HeaderHandler_EndInvoke_m12596_ParameterInfos[] = 
{
	{"result", 0, 134225380, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.HeaderHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo HeaderHandler_EndInvoke_m12596_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&HeaderHandler_EndInvoke_m12596/* method */
	, &HeaderHandler_t2252_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t2252_HeaderHandler_EndInvoke_m12596_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HeaderHandler_t2252_MethodInfos[] =
{
	&HeaderHandler__ctor_m12593_MethodInfo,
	&HeaderHandler_Invoke_m12594_MethodInfo,
	&HeaderHandler_BeginInvoke_m12595_MethodInfo,
	&HeaderHandler_EndInvoke_m12596_MethodInfo,
	NULL
};
extern const MethodInfo HeaderHandler_Invoke_m12594_MethodInfo;
extern const MethodInfo HeaderHandler_BeginInvoke_m12595_MethodInfo;
extern const MethodInfo HeaderHandler_EndInvoke_m12596_MethodInfo;
static const Il2CppMethodReference HeaderHandler_t2252_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&HeaderHandler_Invoke_m12594_MethodInfo,
	&HeaderHandler_BeginInvoke_m12595_MethodInfo,
	&HeaderHandler_EndInvoke_m12596_MethodInfo,
};
static bool HeaderHandler_t2252_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HeaderHandler_t2252_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HeaderHandler_t2252_0_0_0;
extern const Il2CppType HeaderHandler_t2252_1_0_0;
struct HeaderHandler_t2252;
const Il2CppTypeDefinitionMetadata HeaderHandler_t2252_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HeaderHandler_t2252_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, HeaderHandler_t2252_VTable/* vtableMethods */
	, HeaderHandler_t2252_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HeaderHandler_t2252_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HeaderHandler"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, HeaderHandler_t2252_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HeaderHandler_t2252_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1008/* custom_attributes_cache */
	, &HeaderHandler_t2252_0_0_0/* byval_arg */
	, &HeaderHandler_t2252_1_0_0/* this_arg */
	, &HeaderHandler_t2252_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_HeaderHandler_t2252/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HeaderHandler_t2252)/* instance_size */
	, sizeof (HeaderHandler_t2252)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.ThreadStart
#include "mscorlib_System_Threading_ThreadStart.h"
// Metadata Definition System.Threading.ThreadStart
extern TypeInfo ThreadStart_t413_il2cpp_TypeInfo;
// System.Threading.ThreadStart
#include "mscorlib_System_Threading_ThreadStartMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ThreadStart_t413_ThreadStart__ctor_m1708_ParameterInfos[] = 
{
	{"object", 0, 134225381, 0, &Object_t_0_0_0},
	{"method", 1, 134225382, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.ThreadStart::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ThreadStart__ctor_m1708_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ThreadStart__ctor_m1708/* method */
	, &ThreadStart_t413_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, ThreadStart_t413_ThreadStart__ctor_m1708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.ThreadStart::Invoke()
extern const MethodInfo ThreadStart_Invoke_m12597_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ThreadStart_Invoke_m12597/* method */
	, &ThreadStart_t413_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ThreadStart_t413_ThreadStart_BeginInvoke_m12598_ParameterInfos[] = 
{
	{"callback", 0, 134225383, 0, &AsyncCallback_t547_0_0_0},
	{"object", 1, 134225384, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Threading.ThreadStart::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo ThreadStart_BeginInvoke_m12598_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&ThreadStart_BeginInvoke_m12598/* method */
	, &ThreadStart_t413_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ThreadStart_t413_ThreadStart_BeginInvoke_m12598_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo ThreadStart_t413_ThreadStart_EndInvoke_m12599_ParameterInfos[] = 
{
	{"result", 0, 134225385, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.ThreadStart::EndInvoke(System.IAsyncResult)
extern const MethodInfo ThreadStart_EndInvoke_m12599_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&ThreadStart_EndInvoke_m12599/* method */
	, &ThreadStart_t413_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, ThreadStart_t413_ThreadStart_EndInvoke_m12599_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ThreadStart_t413_MethodInfos[] =
{
	&ThreadStart__ctor_m1708_MethodInfo,
	&ThreadStart_Invoke_m12597_MethodInfo,
	&ThreadStart_BeginInvoke_m12598_MethodInfo,
	&ThreadStart_EndInvoke_m12599_MethodInfo,
	NULL
};
extern const MethodInfo ThreadStart_Invoke_m12597_MethodInfo;
extern const MethodInfo ThreadStart_BeginInvoke_m12598_MethodInfo;
extern const MethodInfo ThreadStart_EndInvoke_m12599_MethodInfo;
static const Il2CppMethodReference ThreadStart_t413_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&ThreadStart_Invoke_m12597_MethodInfo,
	&ThreadStart_BeginInvoke_m12598_MethodInfo,
	&ThreadStart_EndInvoke_m12599_MethodInfo,
};
static bool ThreadStart_t413_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ThreadStart_t413_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadStart_t413_0_0_0;
extern const Il2CppType ThreadStart_t413_1_0_0;
struct ThreadStart_t413;
const Il2CppTypeDefinitionMetadata ThreadStart_t413_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadStart_t413_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, ThreadStart_t413_VTable/* vtableMethods */
	, ThreadStart_t413_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ThreadStart_t413_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadStart"/* name */
	, "System.Threading"/* namespaze */
	, ThreadStart_t413_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ThreadStart_t413_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1009/* custom_attributes_cache */
	, &ThreadStart_t413_0_0_0/* byval_arg */
	, &ThreadStart_t413_1_0_0/* this_arg */
	, &ThreadStart_t413_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ThreadStart_t413/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadStart_t413)/* instance_size */
	, sizeof (ThreadStart_t413)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.TimerCallback
#include "mscorlib_System_Threading_TimerCallback.h"
// Metadata Definition System.Threading.TimerCallback
extern TypeInfo TimerCallback_t2174_il2cpp_TypeInfo;
// System.Threading.TimerCallback
#include "mscorlib_System_Threading_TimerCallbackMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo TimerCallback_t2174_TimerCallback__ctor_m12600_ParameterInfos[] = 
{
	{"object", 0, 134225386, 0, &Object_t_0_0_0},
	{"method", 1, 134225387, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.TimerCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo TimerCallback__ctor_m12600_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimerCallback__ctor_m12600/* method */
	, &TimerCallback_t2174_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, TimerCallback_t2174_TimerCallback__ctor_m12600_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TimerCallback_t2174_TimerCallback_Invoke_m12601_ParameterInfos[] = 
{
	{"state", 0, 134225388, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.TimerCallback::Invoke(System.Object)
extern const MethodInfo TimerCallback_Invoke_m12601_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&TimerCallback_Invoke_m12601/* method */
	, &TimerCallback_t2174_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TimerCallback_t2174_TimerCallback_Invoke_m12601_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TimerCallback_t2174_TimerCallback_BeginInvoke_m12602_ParameterInfos[] = 
{
	{"state", 0, 134225389, 0, &Object_t_0_0_0},
	{"callback", 1, 134225390, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134225391, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Threading.TimerCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo TimerCallback_BeginInvoke_m12602_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&TimerCallback_BeginInvoke_m12602/* method */
	, &TimerCallback_t2174_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, TimerCallback_t2174_TimerCallback_BeginInvoke_m12602_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo TimerCallback_t2174_TimerCallback_EndInvoke_m12603_ParameterInfos[] = 
{
	{"result", 0, 134225392, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.TimerCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo TimerCallback_EndInvoke_m12603_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&TimerCallback_EndInvoke_m12603/* method */
	, &TimerCallback_t2174_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TimerCallback_t2174_TimerCallback_EndInvoke_m12603_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TimerCallback_t2174_MethodInfos[] =
{
	&TimerCallback__ctor_m12600_MethodInfo,
	&TimerCallback_Invoke_m12601_MethodInfo,
	&TimerCallback_BeginInvoke_m12602_MethodInfo,
	&TimerCallback_EndInvoke_m12603_MethodInfo,
	NULL
};
extern const MethodInfo TimerCallback_Invoke_m12601_MethodInfo;
extern const MethodInfo TimerCallback_BeginInvoke_m12602_MethodInfo;
extern const MethodInfo TimerCallback_EndInvoke_m12603_MethodInfo;
static const Il2CppMethodReference TimerCallback_t2174_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&TimerCallback_Invoke_m12601_MethodInfo,
	&TimerCallback_BeginInvoke_m12602_MethodInfo,
	&TimerCallback_EndInvoke_m12603_MethodInfo,
};
static bool TimerCallback_t2174_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TimerCallback_t2174_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimerCallback_t2174_0_0_0;
extern const Il2CppType TimerCallback_t2174_1_0_0;
struct TimerCallback_t2174;
const Il2CppTypeDefinitionMetadata TimerCallback_t2174_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TimerCallback_t2174_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, TimerCallback_t2174_VTable/* vtableMethods */
	, TimerCallback_t2174_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TimerCallback_t2174_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimerCallback"/* name */
	, "System.Threading"/* namespaze */
	, TimerCallback_t2174_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TimerCallback_t2174_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1010/* custom_attributes_cache */
	, &TimerCallback_t2174_0_0_0/* byval_arg */
	, &TimerCallback_t2174_1_0_0/* this_arg */
	, &TimerCallback_t2174_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_TimerCallback_t2174/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimerCallback_t2174)/* instance_size */
	, sizeof (TimerCallback_t2174)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.WaitCallback
#include "mscorlib_System_Threading_WaitCallback.h"
// Metadata Definition System.Threading.WaitCallback
extern TypeInfo WaitCallback_t2253_il2cpp_TypeInfo;
// System.Threading.WaitCallback
#include "mscorlib_System_Threading_WaitCallbackMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo WaitCallback_t2253_WaitCallback__ctor_m12604_ParameterInfos[] = 
{
	{"object", 0, 134225393, 0, &Object_t_0_0_0},
	{"method", 1, 134225394, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.WaitCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo WaitCallback__ctor_m12604_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WaitCallback__ctor_m12604/* method */
	, &WaitCallback_t2253_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, WaitCallback_t2253_WaitCallback__ctor_m12604_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WaitCallback_t2253_WaitCallback_Invoke_m12605_ParameterInfos[] = 
{
	{"state", 0, 134225395, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.WaitCallback::Invoke(System.Object)
extern const MethodInfo WaitCallback_Invoke_m12605_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&WaitCallback_Invoke_m12605/* method */
	, &WaitCallback_t2253_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, WaitCallback_t2253_WaitCallback_Invoke_m12605_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WaitCallback_t2253_WaitCallback_BeginInvoke_m12606_ParameterInfos[] = 
{
	{"state", 0, 134225396, 0, &Object_t_0_0_0},
	{"callback", 1, 134225397, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134225398, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Threading.WaitCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo WaitCallback_BeginInvoke_m12606_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&WaitCallback_BeginInvoke_m12606/* method */
	, &WaitCallback_t2253_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, WaitCallback_t2253_WaitCallback_BeginInvoke_m12606_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo WaitCallback_t2253_WaitCallback_EndInvoke_m12607_ParameterInfos[] = 
{
	{"result", 0, 134225399, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.WaitCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo WaitCallback_EndInvoke_m12607_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&WaitCallback_EndInvoke_m12607/* method */
	, &WaitCallback_t2253_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, WaitCallback_t2253_WaitCallback_EndInvoke_m12607_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WaitCallback_t2253_MethodInfos[] =
{
	&WaitCallback__ctor_m12604_MethodInfo,
	&WaitCallback_Invoke_m12605_MethodInfo,
	&WaitCallback_BeginInvoke_m12606_MethodInfo,
	&WaitCallback_EndInvoke_m12607_MethodInfo,
	NULL
};
extern const MethodInfo WaitCallback_Invoke_m12605_MethodInfo;
extern const MethodInfo WaitCallback_BeginInvoke_m12606_MethodInfo;
extern const MethodInfo WaitCallback_EndInvoke_m12607_MethodInfo;
static const Il2CppMethodReference WaitCallback_t2253_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&WaitCallback_Invoke_m12605_MethodInfo,
	&WaitCallback_BeginInvoke_m12606_MethodInfo,
	&WaitCallback_EndInvoke_m12607_MethodInfo,
};
static bool WaitCallback_t2253_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair WaitCallback_t2253_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WaitCallback_t2253_0_0_0;
extern const Il2CppType WaitCallback_t2253_1_0_0;
struct WaitCallback_t2253;
const Il2CppTypeDefinitionMetadata WaitCallback_t2253_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WaitCallback_t2253_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, WaitCallback_t2253_VTable/* vtableMethods */
	, WaitCallback_t2253_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WaitCallback_t2253_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaitCallback"/* name */
	, "System.Threading"/* namespaze */
	, WaitCallback_t2253_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WaitCallback_t2253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1011/* custom_attributes_cache */
	, &WaitCallback_t2253_0_0_0/* byval_arg */
	, &WaitCallback_t2253_1_0_0/* this_arg */
	, &WaitCallback_t2253_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_WaitCallback_t2253/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaitCallback_t2253)/* instance_size */
	, sizeof (WaitCallback_t2253)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.WaitOrTimerCallback
#include "mscorlib_System_Threading_WaitOrTimerCallback.h"
// Metadata Definition System.Threading.WaitOrTimerCallback
extern TypeInfo WaitOrTimerCallback_t2163_il2cpp_TypeInfo;
// System.Threading.WaitOrTimerCallback
#include "mscorlib_System_Threading_WaitOrTimerCallbackMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo WaitOrTimerCallback_t2163_WaitOrTimerCallback__ctor_m12608_ParameterInfos[] = 
{
	{"object", 0, 134225400, 0, &Object_t_0_0_0},
	{"method", 1, 134225401, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.WaitOrTimerCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo WaitOrTimerCallback__ctor_m12608_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WaitOrTimerCallback__ctor_m12608/* method */
	, &WaitOrTimerCallback_t2163_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, WaitOrTimerCallback_t2163_WaitOrTimerCallback__ctor_m12608_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
static const ParameterInfo WaitOrTimerCallback_t2163_WaitOrTimerCallback_Invoke_m12609_ParameterInfos[] = 
{
	{"state", 0, 134225402, 0, &Object_t_0_0_0},
	{"timedOut", 1, 134225403, 0, &Boolean_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_SByte_t274 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.WaitOrTimerCallback::Invoke(System.Object,System.Boolean)
extern const MethodInfo WaitOrTimerCallback_Invoke_m12609_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&WaitOrTimerCallback_Invoke_m12609/* method */
	, &WaitOrTimerCallback_t2163_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_SByte_t274/* invoker_method */
	, WaitOrTimerCallback_t2163_WaitOrTimerCallback_Invoke_m12609_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t273_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WaitOrTimerCallback_t2163_WaitOrTimerCallback_BeginInvoke_m12610_ParameterInfos[] = 
{
	{"state", 0, 134225404, 0, &Object_t_0_0_0},
	{"timedOut", 1, 134225405, 0, &Boolean_t273_0_0_0},
	{"callback", 2, 134225406, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134225407, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t274_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Threading.WaitOrTimerCallback::BeginInvoke(System.Object,System.Boolean,System.AsyncCallback,System.Object)
extern const MethodInfo WaitOrTimerCallback_BeginInvoke_m12610_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&WaitOrTimerCallback_BeginInvoke_m12610/* method */
	, &WaitOrTimerCallback_t2163_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t274_Object_t_Object_t/* invoker_method */
	, WaitOrTimerCallback_t2163_WaitOrTimerCallback_BeginInvoke_m12610_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo WaitOrTimerCallback_t2163_WaitOrTimerCallback_EndInvoke_m12611_ParameterInfos[] = 
{
	{"result", 0, 134225408, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Threading.WaitOrTimerCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo WaitOrTimerCallback_EndInvoke_m12611_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&WaitOrTimerCallback_EndInvoke_m12611/* method */
	, &WaitOrTimerCallback_t2163_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, WaitOrTimerCallback_t2163_WaitOrTimerCallback_EndInvoke_m12611_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WaitOrTimerCallback_t2163_MethodInfos[] =
{
	&WaitOrTimerCallback__ctor_m12608_MethodInfo,
	&WaitOrTimerCallback_Invoke_m12609_MethodInfo,
	&WaitOrTimerCallback_BeginInvoke_m12610_MethodInfo,
	&WaitOrTimerCallback_EndInvoke_m12611_MethodInfo,
	NULL
};
extern const MethodInfo WaitOrTimerCallback_Invoke_m12609_MethodInfo;
extern const MethodInfo WaitOrTimerCallback_BeginInvoke_m12610_MethodInfo;
extern const MethodInfo WaitOrTimerCallback_EndInvoke_m12611_MethodInfo;
static const Il2CppMethodReference WaitOrTimerCallback_t2163_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&WaitOrTimerCallback_Invoke_m12609_MethodInfo,
	&WaitOrTimerCallback_BeginInvoke_m12610_MethodInfo,
	&WaitOrTimerCallback_EndInvoke_m12611_MethodInfo,
};
static bool WaitOrTimerCallback_t2163_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair WaitOrTimerCallback_t2163_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WaitOrTimerCallback_t2163_0_0_0;
extern const Il2CppType WaitOrTimerCallback_t2163_1_0_0;
struct WaitOrTimerCallback_t2163;
const Il2CppTypeDefinitionMetadata WaitOrTimerCallback_t2163_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WaitOrTimerCallback_t2163_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, WaitOrTimerCallback_t2163_VTable/* vtableMethods */
	, WaitOrTimerCallback_t2163_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WaitOrTimerCallback_t2163_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaitOrTimerCallback"/* name */
	, "System.Threading"/* namespaze */
	, WaitOrTimerCallback_t2163_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WaitOrTimerCallback_t2163_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1012/* custom_attributes_cache */
	, &WaitOrTimerCallback_t2163_0_0_0/* byval_arg */
	, &WaitOrTimerCallback_t2163_1_0_0/* this_arg */
	, &WaitOrTimerCallback_t2163_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_WaitOrTimerCallback_t2163/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaitOrTimerCallback_t2163)/* instance_size */
	, sizeof (WaitOrTimerCallback_t2163)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Action`1
extern TypeInfo Action_1_t2394_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Action_1_t2394_Il2CppGenericContainer;
extern TypeInfo Action_1_t2394_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Action_1_t2394_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Action_1_t2394_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Action_1_t2394_Il2CppGenericParametersArray[1] = 
{
	&Action_1_t2394_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Action_1_t2394_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Action_1_t2394_il2cpp_TypeInfo, 1, 0, Action_1_t2394_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Action_1_t2394_Action_1__ctor_m13300_ParameterInfos[] = 
{
	{"object", 0, 134225409, 0, &Object_t_0_0_0},
	{"method", 1, 134225410, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Action`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Action_1__ctor_m13300_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Action_1_t2394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2394_Action_1__ctor_m13300_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Action_1_t2394_gp_0_0_0_0;
extern const Il2CppType Action_1_t2394_gp_0_0_0_0;
static const ParameterInfo Action_1_t2394_Action_1_Invoke_m13301_ParameterInfos[] = 
{
	{"obj", 0, 134225411, 0, &Action_1_t2394_gp_0_0_0_0},
};
// System.Void System.Action`1::Invoke(T)
extern const MethodInfo Action_1_Invoke_m13301_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Action_1_t2394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2394_Action_1_Invoke_m13301_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Action_1_t2394_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Action_1_t2394_Action_1_BeginInvoke_m13302_ParameterInfos[] = 
{
	{"obj", 0, 134225412, 0, &Action_1_t2394_gp_0_0_0_0},
	{"callback", 1, 134225413, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134225414, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Action`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Action_1_BeginInvoke_m13302_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Action_1_t2394_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2394_Action_1_BeginInvoke_m13302_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo Action_1_t2394_Action_1_EndInvoke_m13303_ParameterInfos[] = 
{
	{"result", 0, 134225415, 0, &IAsyncResult_t546_0_0_0},
};
// System.Void System.Action`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Action_1_EndInvoke_m13303_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Action_1_t2394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2394_Action_1_EndInvoke_m13303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Action_1_t2394_MethodInfos[] =
{
	&Action_1__ctor_m13300_MethodInfo,
	&Action_1_Invoke_m13301_MethodInfo,
	&Action_1_BeginInvoke_m13302_MethodInfo,
	&Action_1_EndInvoke_m13303_MethodInfo,
	NULL
};
extern const MethodInfo Action_1_Invoke_m13301_MethodInfo;
extern const MethodInfo Action_1_BeginInvoke_m13302_MethodInfo;
extern const MethodInfo Action_1_EndInvoke_m13303_MethodInfo;
static const Il2CppMethodReference Action_1_t2394_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&Action_1_Invoke_m13301_MethodInfo,
	&Action_1_BeginInvoke_m13302_MethodInfo,
	&Action_1_EndInvoke_m13303_MethodInfo,
};
static bool Action_1_t2394_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Action_1_t2394_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Action_1_t2394_0_0_0;
extern const Il2CppType Action_1_t2394_1_0_0;
struct Action_1_t2394;
const Il2CppTypeDefinitionMetadata Action_1_t2394_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_1_t2394_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, Action_1_t2394_VTable/* vtableMethods */
	, Action_1_t2394_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Action_1_t2394_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t2394_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Action_1_t2394_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_1_t2394_0_0_0/* byval_arg */
	, &Action_1_t2394_1_0_0/* this_arg */
	, &Action_1_t2394_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Action_1_t2394_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AppDomainInitializer
#include "mscorlib_System_AppDomainInitializer.h"
// Metadata Definition System.AppDomainInitializer
extern TypeInfo AppDomainInitializer_t2184_il2cpp_TypeInfo;
// System.AppDomainInitializer
#include "mscorlib_System_AppDomainInitializerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo AppDomainInitializer_t2184_AppDomainInitializer__ctor_m12612_ParameterInfos[] = 
{
	{"object", 0, 134225416, 0, &Object_t_0_0_0},
	{"method", 1, 134225417, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::.ctor(System.Object,System.IntPtr)
extern const MethodInfo AppDomainInitializer__ctor_m12612_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AppDomainInitializer__ctor_m12612/* method */
	, &AppDomainInitializer_t2184_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, AppDomainInitializer_t2184_AppDomainInitializer__ctor_m12612_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t243_0_0_0;
extern const Il2CppType StringU5BU5D_t243_0_0_0;
static const ParameterInfo AppDomainInitializer_t2184_AppDomainInitializer_Invoke_m12613_ParameterInfos[] = 
{
	{"args", 0, 134225418, 0, &StringU5BU5D_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::Invoke(System.String[])
extern const MethodInfo AppDomainInitializer_Invoke_m12613_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&AppDomainInitializer_Invoke_m12613/* method */
	, &AppDomainInitializer_t2184_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AppDomainInitializer_t2184_AppDomainInitializer_Invoke_m12613_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t243_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AppDomainInitializer_t2184_AppDomainInitializer_BeginInvoke_m12614_ParameterInfos[] = 
{
	{"args", 0, 134225419, 0, &StringU5BU5D_t243_0_0_0},
	{"callback", 1, 134225420, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134225421, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.AppDomainInitializer::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern const MethodInfo AppDomainInitializer_BeginInvoke_m12614_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&AppDomainInitializer_BeginInvoke_m12614/* method */
	, &AppDomainInitializer_t2184_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, AppDomainInitializer_t2184_AppDomainInitializer_BeginInvoke_m12614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo AppDomainInitializer_t2184_AppDomainInitializer_EndInvoke_m12615_ParameterInfos[] = 
{
	{"result", 0, 134225422, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::EndInvoke(System.IAsyncResult)
extern const MethodInfo AppDomainInitializer_EndInvoke_m12615_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AppDomainInitializer_EndInvoke_m12615/* method */
	, &AppDomainInitializer_t2184_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AppDomainInitializer_t2184_AppDomainInitializer_EndInvoke_m12615_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AppDomainInitializer_t2184_MethodInfos[] =
{
	&AppDomainInitializer__ctor_m12612_MethodInfo,
	&AppDomainInitializer_Invoke_m12613_MethodInfo,
	&AppDomainInitializer_BeginInvoke_m12614_MethodInfo,
	&AppDomainInitializer_EndInvoke_m12615_MethodInfo,
	NULL
};
extern const MethodInfo AppDomainInitializer_Invoke_m12613_MethodInfo;
extern const MethodInfo AppDomainInitializer_BeginInvoke_m12614_MethodInfo;
extern const MethodInfo AppDomainInitializer_EndInvoke_m12615_MethodInfo;
static const Il2CppMethodReference AppDomainInitializer_t2184_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&AppDomainInitializer_Invoke_m12613_MethodInfo,
	&AppDomainInitializer_BeginInvoke_m12614_MethodInfo,
	&AppDomainInitializer_EndInvoke_m12615_MethodInfo,
};
static bool AppDomainInitializer_t2184_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AppDomainInitializer_t2184_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainInitializer_t2184_0_0_0;
extern const Il2CppType AppDomainInitializer_t2184_1_0_0;
struct AppDomainInitializer_t2184;
const Il2CppTypeDefinitionMetadata AppDomainInitializer_t2184_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AppDomainInitializer_t2184_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, AppDomainInitializer_t2184_VTable/* vtableMethods */
	, AppDomainInitializer_t2184_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AppDomainInitializer_t2184_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainInitializer"/* name */
	, "System"/* namespaze */
	, AppDomainInitializer_t2184_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AppDomainInitializer_t2184_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1013/* custom_attributes_cache */
	, &AppDomainInitializer_t2184_0_0_0/* byval_arg */
	, &AppDomainInitializer_t2184_1_0_0/* this_arg */
	, &AppDomainInitializer_t2184_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AppDomainInitializer_t2184/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainInitializer_t2184)/* instance_size */
	, sizeof (AppDomainInitializer_t2184)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AssemblyLoadEventHandler
#include "mscorlib_System_AssemblyLoadEventHandler.h"
// Metadata Definition System.AssemblyLoadEventHandler
extern TypeInfo AssemblyLoadEventHandler_t2180_il2cpp_TypeInfo;
// System.AssemblyLoadEventHandler
#include "mscorlib_System_AssemblyLoadEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2180_AssemblyLoadEventHandler__ctor_m12616_ParameterInfos[] = 
{
	{"object", 0, 134225423, 0, &Object_t_0_0_0},
	{"method", 1, 134225424, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo AssemblyLoadEventHandler__ctor_m12616_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler__ctor_m12616/* method */
	, &AssemblyLoadEventHandler_t2180_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, AssemblyLoadEventHandler_t2180_AssemblyLoadEventHandler__ctor_m12616_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t2188_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t2188_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2180_AssemblyLoadEventHandler_Invoke_m12617_ParameterInfos[] = 
{
	{"sender", 0, 134225425, 0, &Object_t_0_0_0},
	{"args", 1, 134225426, 0, &AssemblyLoadEventArgs_t2188_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::Invoke(System.Object,System.AssemblyLoadEventArgs)
extern const MethodInfo AssemblyLoadEventHandler_Invoke_m12617_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_Invoke_m12617/* method */
	, &AssemblyLoadEventHandler_t2180_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t2180_AssemblyLoadEventHandler_Invoke_m12617_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t2188_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2180_AssemblyLoadEventHandler_BeginInvoke_m12618_ParameterInfos[] = 
{
	{"sender", 0, 134225427, 0, &Object_t_0_0_0},
	{"args", 1, 134225428, 0, &AssemblyLoadEventArgs_t2188_0_0_0},
	{"callback", 2, 134225429, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134225430, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.AssemblyLoadEventHandler::BeginInvoke(System.Object,System.AssemblyLoadEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo AssemblyLoadEventHandler_BeginInvoke_m12618_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_BeginInvoke_m12618/* method */
	, &AssemblyLoadEventHandler_t2180_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t2180_AssemblyLoadEventHandler_BeginInvoke_m12618_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2180_AssemblyLoadEventHandler_EndInvoke_m12619_ParameterInfos[] = 
{
	{"result", 0, 134225431, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo AssemblyLoadEventHandler_EndInvoke_m12619_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_EndInvoke_m12619/* method */
	, &AssemblyLoadEventHandler_t2180_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t2180_AssemblyLoadEventHandler_EndInvoke_m12619_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyLoadEventHandler_t2180_MethodInfos[] =
{
	&AssemblyLoadEventHandler__ctor_m12616_MethodInfo,
	&AssemblyLoadEventHandler_Invoke_m12617_MethodInfo,
	&AssemblyLoadEventHandler_BeginInvoke_m12618_MethodInfo,
	&AssemblyLoadEventHandler_EndInvoke_m12619_MethodInfo,
	NULL
};
extern const MethodInfo AssemblyLoadEventHandler_Invoke_m12617_MethodInfo;
extern const MethodInfo AssemblyLoadEventHandler_BeginInvoke_m12618_MethodInfo;
extern const MethodInfo AssemblyLoadEventHandler_EndInvoke_m12619_MethodInfo;
static const Il2CppMethodReference AssemblyLoadEventHandler_t2180_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&AssemblyLoadEventHandler_Invoke_m12617_MethodInfo,
	&AssemblyLoadEventHandler_BeginInvoke_m12618_MethodInfo,
	&AssemblyLoadEventHandler_EndInvoke_m12619_MethodInfo,
};
static bool AssemblyLoadEventHandler_t2180_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyLoadEventHandler_t2180_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyLoadEventHandler_t2180_0_0_0;
extern const Il2CppType AssemblyLoadEventHandler_t2180_1_0_0;
struct AssemblyLoadEventHandler_t2180;
const Il2CppTypeDefinitionMetadata AssemblyLoadEventHandler_t2180_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyLoadEventHandler_t2180_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, AssemblyLoadEventHandler_t2180_VTable/* vtableMethods */
	, AssemblyLoadEventHandler_t2180_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AssemblyLoadEventHandler_t2180_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyLoadEventHandler"/* name */
	, "System"/* namespaze */
	, AssemblyLoadEventHandler_t2180_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyLoadEventHandler_t2180_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1014/* custom_attributes_cache */
	, &AssemblyLoadEventHandler_t2180_0_0_0/* byval_arg */
	, &AssemblyLoadEventHandler_t2180_1_0_0/* this_arg */
	, &AssemblyLoadEventHandler_t2180_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t2180/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyLoadEventHandler_t2180)/* instance_size */
	, sizeof (AssemblyLoadEventHandler_t2180)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Comparison`1
extern TypeInfo Comparison_1_t2395_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Comparison_1_t2395_Il2CppGenericContainer;
extern TypeInfo Comparison_1_t2395_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Comparison_1_t2395_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Comparison_1_t2395_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Comparison_1_t2395_Il2CppGenericParametersArray[1] = 
{
	&Comparison_1_t2395_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Comparison_1_t2395_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Comparison_1_t2395_il2cpp_TypeInfo, 1, 0, Comparison_1_t2395_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Comparison_1_t2395_Comparison_1__ctor_m13304_ParameterInfos[] = 
{
	{"object", 0, 134225432, 0, &Object_t_0_0_0},
	{"method", 1, 134225433, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Comparison`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Comparison_1__ctor_m13304_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Comparison_1_t2395_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2395_Comparison_1__ctor_m13304_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t2395_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2395_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2395_gp_0_0_0_0;
static const ParameterInfo Comparison_1_t2395_Comparison_1_Invoke_m13305_ParameterInfos[] = 
{
	{"x", 0, 134225434, 0, &Comparison_1_t2395_gp_0_0_0_0},
	{"y", 1, 134225435, 0, &Comparison_1_t2395_gp_0_0_0_0},
};
// System.Int32 System.Comparison`1::Invoke(T,T)
extern const MethodInfo Comparison_1_Invoke_m13305_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2395_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2395_Comparison_1_Invoke_m13305_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t2395_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2395_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Comparison_1_t2395_Comparison_1_BeginInvoke_m13306_ParameterInfos[] = 
{
	{"x", 0, 134225436, 0, &Comparison_1_t2395_gp_0_0_0_0},
	{"y", 1, 134225437, 0, &Comparison_1_t2395_gp_0_0_0_0},
	{"callback", 2, 134225438, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134225439, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Comparison`1::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern const MethodInfo Comparison_1_BeginInvoke_m13306_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2395_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2395_Comparison_1_BeginInvoke_m13306_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo Comparison_1_t2395_Comparison_1_EndInvoke_m13307_ParameterInfos[] = 
{
	{"result", 0, 134225440, 0, &IAsyncResult_t546_0_0_0},
};
// System.Int32 System.Comparison`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Comparison_1_EndInvoke_m13307_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2395_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t253_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2395_Comparison_1_EndInvoke_m13307_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Comparison_1_t2395_MethodInfos[] =
{
	&Comparison_1__ctor_m13304_MethodInfo,
	&Comparison_1_Invoke_m13305_MethodInfo,
	&Comparison_1_BeginInvoke_m13306_MethodInfo,
	&Comparison_1_EndInvoke_m13307_MethodInfo,
	NULL
};
extern const MethodInfo Comparison_1_Invoke_m13305_MethodInfo;
extern const MethodInfo Comparison_1_BeginInvoke_m13306_MethodInfo;
extern const MethodInfo Comparison_1_EndInvoke_m13307_MethodInfo;
static const Il2CppMethodReference Comparison_1_t2395_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&Comparison_1_Invoke_m13305_MethodInfo,
	&Comparison_1_BeginInvoke_m13306_MethodInfo,
	&Comparison_1_EndInvoke_m13307_MethodInfo,
};
static bool Comparison_1_t2395_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Comparison_1_t2395_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Comparison_1_t2395_0_0_0;
extern const Il2CppType Comparison_1_t2395_1_0_0;
struct Comparison_1_t2395;
const Il2CppTypeDefinitionMetadata Comparison_1_t2395_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Comparison_1_t2395_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, Comparison_1_t2395_VTable/* vtableMethods */
	, Comparison_1_t2395_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Comparison_1_t2395_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparison`1"/* name */
	, "System"/* namespaze */
	, Comparison_1_t2395_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Comparison_1_t2395_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Comparison_1_t2395_0_0_0/* byval_arg */
	, &Comparison_1_t2395_1_0_0/* this_arg */
	, &Comparison_1_t2395_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Comparison_1_t2395_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Converter`2
extern TypeInfo Converter_2_t2396_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Converter_2_t2396_Il2CppGenericContainer;
extern TypeInfo Converter_2_t2396_gp_TInput_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Converter_2_t2396_gp_TInput_0_il2cpp_TypeInfo_GenericParamFull = { &Converter_2_t2396_Il2CppGenericContainer, NULL, "TInput", 0, 0 };
extern TypeInfo Converter_2_t2396_gp_TOutput_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Converter_2_t2396_gp_TOutput_1_il2cpp_TypeInfo_GenericParamFull = { &Converter_2_t2396_Il2CppGenericContainer, NULL, "TOutput", 1, 0 };
static const Il2CppGenericParameter* Converter_2_t2396_Il2CppGenericParametersArray[2] = 
{
	&Converter_2_t2396_gp_TInput_0_il2cpp_TypeInfo_GenericParamFull,
	&Converter_2_t2396_gp_TOutput_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Converter_2_t2396_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Converter_2_t2396_il2cpp_TypeInfo, 2, 0, Converter_2_t2396_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Converter_2_t2396_Converter_2__ctor_m13308_ParameterInfos[] = 
{
	{"object", 0, 134225441, 0, &Object_t_0_0_0},
	{"method", 1, 134225442, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Converter`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Converter_2__ctor_m13308_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Converter_2_t2396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2396_Converter_2__ctor_m13308_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Converter_2_t2396_gp_0_0_0_0;
extern const Il2CppType Converter_2_t2396_gp_0_0_0_0;
static const ParameterInfo Converter_2_t2396_Converter_2_Invoke_m13309_ParameterInfos[] = 
{
	{"input", 0, 134225443, 0, &Converter_2_t2396_gp_0_0_0_0},
};
extern const Il2CppType Converter_2_t2396_gp_1_0_0_0;
// TOutput System.Converter`2::Invoke(TInput)
extern const MethodInfo Converter_2_Invoke_m13309_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Converter_2_t2396_il2cpp_TypeInfo/* declaring_type */
	, &Converter_2_t2396_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2396_Converter_2_Invoke_m13309_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Converter_2_t2396_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Converter_2_t2396_Converter_2_BeginInvoke_m13310_ParameterInfos[] = 
{
	{"input", 0, 134225444, 0, &Converter_2_t2396_gp_0_0_0_0},
	{"callback", 1, 134225445, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134225446, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Converter`2::BeginInvoke(TInput,System.AsyncCallback,System.Object)
extern const MethodInfo Converter_2_BeginInvoke_m13310_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Converter_2_t2396_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2396_Converter_2_BeginInvoke_m13310_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo Converter_2_t2396_Converter_2_EndInvoke_m13311_ParameterInfos[] = 
{
	{"result", 0, 134225447, 0, &IAsyncResult_t546_0_0_0},
};
// TOutput System.Converter`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo Converter_2_EndInvoke_m13311_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Converter_2_t2396_il2cpp_TypeInfo/* declaring_type */
	, &Converter_2_t2396_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2396_Converter_2_EndInvoke_m13311_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Converter_2_t2396_MethodInfos[] =
{
	&Converter_2__ctor_m13308_MethodInfo,
	&Converter_2_Invoke_m13309_MethodInfo,
	&Converter_2_BeginInvoke_m13310_MethodInfo,
	&Converter_2_EndInvoke_m13311_MethodInfo,
	NULL
};
extern const MethodInfo Converter_2_Invoke_m13309_MethodInfo;
extern const MethodInfo Converter_2_BeginInvoke_m13310_MethodInfo;
extern const MethodInfo Converter_2_EndInvoke_m13311_MethodInfo;
static const Il2CppMethodReference Converter_2_t2396_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&Converter_2_Invoke_m13309_MethodInfo,
	&Converter_2_BeginInvoke_m13310_MethodInfo,
	&Converter_2_EndInvoke_m13311_MethodInfo,
};
static bool Converter_2_t2396_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Converter_2_t2396_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Converter_2_t2396_0_0_0;
extern const Il2CppType Converter_2_t2396_1_0_0;
struct Converter_2_t2396;
const Il2CppTypeDefinitionMetadata Converter_2_t2396_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Converter_2_t2396_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, Converter_2_t2396_VTable/* vtableMethods */
	, Converter_2_t2396_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Converter_2_t2396_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Converter`2"/* name */
	, "System"/* namespaze */
	, Converter_2_t2396_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Converter_2_t2396_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Converter_2_t2396_0_0_0/* byval_arg */
	, &Converter_2_t2396_1_0_0/* this_arg */
	, &Converter_2_t2396_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Converter_2_t2396_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.EventHandler
#include "mscorlib_System_EventHandler.h"
// Metadata Definition System.EventHandler
extern TypeInfo EventHandler_t2182_il2cpp_TypeInfo;
// System.EventHandler
#include "mscorlib_System_EventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo EventHandler_t2182_EventHandler__ctor_m12620_ParameterInfos[] = 
{
	{"object", 0, 134225448, 0, &Object_t_0_0_0},
	{"method", 1, 134225449, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo EventHandler__ctor_m12620_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EventHandler__ctor_m12620/* method */
	, &EventHandler_t2182_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, EventHandler_t2182_EventHandler__ctor_m12620_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType EventArgs_t1547_0_0_0;
static const ParameterInfo EventHandler_t2182_EventHandler_Invoke_m12621_ParameterInfos[] = 
{
	{"sender", 0, 134225450, 0, &Object_t_0_0_0},
	{"e", 1, 134225451, 0, &EventArgs_t1547_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::Invoke(System.Object,System.EventArgs)
extern const MethodInfo EventHandler_Invoke_m12621_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&EventHandler_Invoke_m12621/* method */
	, &EventHandler_t2182_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, EventHandler_t2182_EventHandler_Invoke_m12621_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType EventArgs_t1547_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo EventHandler_t2182_EventHandler_BeginInvoke_m12622_ParameterInfos[] = 
{
	{"sender", 0, 134225452, 0, &Object_t_0_0_0},
	{"e", 1, 134225453, 0, &EventArgs_t1547_0_0_0},
	{"callback", 2, 134225454, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134225455, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.EventHandler::BeginInvoke(System.Object,System.EventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo EventHandler_BeginInvoke_m12622_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&EventHandler_BeginInvoke_m12622/* method */
	, &EventHandler_t2182_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, EventHandler_t2182_EventHandler_BeginInvoke_m12622_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo EventHandler_t2182_EventHandler_EndInvoke_m12623_ParameterInfos[] = 
{
	{"result", 0, 134225456, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo EventHandler_EndInvoke_m12623_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&EventHandler_EndInvoke_m12623/* method */
	, &EventHandler_t2182_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, EventHandler_t2182_EventHandler_EndInvoke_m12623_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EventHandler_t2182_MethodInfos[] =
{
	&EventHandler__ctor_m12620_MethodInfo,
	&EventHandler_Invoke_m12621_MethodInfo,
	&EventHandler_BeginInvoke_m12622_MethodInfo,
	&EventHandler_EndInvoke_m12623_MethodInfo,
	NULL
};
extern const MethodInfo EventHandler_Invoke_m12621_MethodInfo;
extern const MethodInfo EventHandler_BeginInvoke_m12622_MethodInfo;
extern const MethodInfo EventHandler_EndInvoke_m12623_MethodInfo;
static const Il2CppMethodReference EventHandler_t2182_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&EventHandler_Invoke_m12621_MethodInfo,
	&EventHandler_BeginInvoke_m12622_MethodInfo,
	&EventHandler_EndInvoke_m12623_MethodInfo,
};
static bool EventHandler_t2182_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair EventHandler_t2182_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventHandler_t2182_0_0_0;
extern const Il2CppType EventHandler_t2182_1_0_0;
struct EventHandler_t2182;
const Il2CppTypeDefinitionMetadata EventHandler_t2182_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventHandler_t2182_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, EventHandler_t2182_VTable/* vtableMethods */
	, EventHandler_t2182_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo EventHandler_t2182_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventHandler"/* name */
	, "System"/* namespaze */
	, EventHandler_t2182_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EventHandler_t2182_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1015/* custom_attributes_cache */
	, &EventHandler_t2182_0_0_0/* byval_arg */
	, &EventHandler_t2182_1_0_0/* this_arg */
	, &EventHandler_t2182_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_EventHandler_t2182/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventHandler_t2182)/* instance_size */
	, sizeof (EventHandler_t2182)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Predicate`1
extern TypeInfo Predicate_1_t2397_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Predicate_1_t2397_Il2CppGenericContainer;
extern TypeInfo Predicate_1_t2397_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Predicate_1_t2397_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Predicate_1_t2397_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Predicate_1_t2397_Il2CppGenericParametersArray[1] = 
{
	&Predicate_1_t2397_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Predicate_1_t2397_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Predicate_1_t2397_il2cpp_TypeInfo, 1, 0, Predicate_1_t2397_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Predicate_1_t2397_Predicate_1__ctor_m13312_ParameterInfos[] = 
{
	{"object", 0, 134225457, 0, &Object_t_0_0_0},
	{"method", 1, 134225458, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Predicate`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Predicate_1__ctor_m13312_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Predicate_1_t2397_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2397_Predicate_1__ctor_m13312_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t2397_gp_0_0_0_0;
extern const Il2CppType Predicate_1_t2397_gp_0_0_0_0;
static const ParameterInfo Predicate_1_t2397_Predicate_1_Invoke_m13313_ParameterInfos[] = 
{
	{"obj", 0, 134225459, 0, &Predicate_1_t2397_gp_0_0_0_0},
};
// System.Boolean System.Predicate`1::Invoke(T)
extern const MethodInfo Predicate_1_Invoke_m13313_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2397_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2397_Predicate_1_Invoke_m13313_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t2397_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Predicate_1_t2397_Predicate_1_BeginInvoke_m13314_ParameterInfos[] = 
{
	{"obj", 0, 134225460, 0, &Predicate_1_t2397_gp_0_0_0_0},
	{"callback", 1, 134225461, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134225462, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Predicate`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Predicate_1_BeginInvoke_m13314_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2397_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2397_Predicate_1_BeginInvoke_m13314_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo Predicate_1_t2397_Predicate_1_EndInvoke_m13315_ParameterInfos[] = 
{
	{"result", 0, 134225463, 0, &IAsyncResult_t546_0_0_0},
};
// System.Boolean System.Predicate`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Predicate_1_EndInvoke_m13315_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2397_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2397_Predicate_1_EndInvoke_m13315_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Predicate_1_t2397_MethodInfos[] =
{
	&Predicate_1__ctor_m13312_MethodInfo,
	&Predicate_1_Invoke_m13313_MethodInfo,
	&Predicate_1_BeginInvoke_m13314_MethodInfo,
	&Predicate_1_EndInvoke_m13315_MethodInfo,
	NULL
};
extern const MethodInfo Predicate_1_Invoke_m13313_MethodInfo;
extern const MethodInfo Predicate_1_BeginInvoke_m13314_MethodInfo;
extern const MethodInfo Predicate_1_EndInvoke_m13315_MethodInfo;
static const Il2CppMethodReference Predicate_1_t2397_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&Predicate_1_Invoke_m13313_MethodInfo,
	&Predicate_1_BeginInvoke_m13314_MethodInfo,
	&Predicate_1_EndInvoke_m13315_MethodInfo,
};
static bool Predicate_1_t2397_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Predicate_1_t2397_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Predicate_1_t2397_0_0_0;
extern const Il2CppType Predicate_1_t2397_1_0_0;
struct Predicate_1_t2397;
const Il2CppTypeDefinitionMetadata Predicate_1_t2397_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Predicate_1_t2397_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, Predicate_1_t2397_VTable/* vtableMethods */
	, Predicate_1_t2397_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Predicate_1_t2397_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Predicate`1"/* name */
	, "System"/* namespaze */
	, Predicate_1_t2397_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Predicate_1_t2397_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Predicate_1_t2397_0_0_0/* byval_arg */
	, &Predicate_1_t2397_1_0_0/* this_arg */
	, &Predicate_1_t2397_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Predicate_1_t2397_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ResolveEventHandler
#include "mscorlib_System_ResolveEventHandler.h"
// Metadata Definition System.ResolveEventHandler
extern TypeInfo ResolveEventHandler_t2181_il2cpp_TypeInfo;
// System.ResolveEventHandler
#include "mscorlib_System_ResolveEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ResolveEventHandler_t2181_ResolveEventHandler__ctor_m12624_ParameterInfos[] = 
{
	{"object", 0, 134225464, 0, &Object_t_0_0_0},
	{"method", 1, 134225465, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ResolveEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ResolveEventHandler__ctor_m12624_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ResolveEventHandler__ctor_m12624/* method */
	, &ResolveEventHandler_t2181_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, ResolveEventHandler_t2181_ResolveEventHandler__ctor_m12624_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ResolveEventArgs_t2236_0_0_0;
static const ParameterInfo ResolveEventHandler_t2181_ResolveEventHandler_Invoke_m12625_ParameterInfos[] = 
{
	{"sender", 0, 134225466, 0, &Object_t_0_0_0},
	{"args", 1, 134225467, 0, &ResolveEventArgs_t2236_0_0_0},
};
extern const Il2CppType Assembly_t1447_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.ResolveEventHandler::Invoke(System.Object,System.ResolveEventArgs)
extern const MethodInfo ResolveEventHandler_Invoke_m12625_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ResolveEventHandler_Invoke_m12625/* method */
	, &ResolveEventHandler_t2181_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t1447_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t2181_ResolveEventHandler_Invoke_m12625_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ResolveEventArgs_t2236_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ResolveEventHandler_t2181_ResolveEventHandler_BeginInvoke_m12626_ParameterInfos[] = 
{
	{"sender", 0, 134225468, 0, &Object_t_0_0_0},
	{"args", 1, 134225469, 0, &ResolveEventArgs_t2236_0_0_0},
	{"callback", 2, 134225470, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134225471, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.ResolveEventHandler::BeginInvoke(System.Object,System.ResolveEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo ResolveEventHandler_BeginInvoke_m12626_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&ResolveEventHandler_BeginInvoke_m12626/* method */
	, &ResolveEventHandler_t2181_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t2181_ResolveEventHandler_BeginInvoke_m12626_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo ResolveEventHandler_t2181_ResolveEventHandler_EndInvoke_m12627_ParameterInfos[] = 
{
	{"result", 0, 134225472, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.ResolveEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo ResolveEventHandler_EndInvoke_m12627_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&ResolveEventHandler_EndInvoke_m12627/* method */
	, &ResolveEventHandler_t2181_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t1447_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t2181_ResolveEventHandler_EndInvoke_m12627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ResolveEventHandler_t2181_MethodInfos[] =
{
	&ResolveEventHandler__ctor_m12624_MethodInfo,
	&ResolveEventHandler_Invoke_m12625_MethodInfo,
	&ResolveEventHandler_BeginInvoke_m12626_MethodInfo,
	&ResolveEventHandler_EndInvoke_m12627_MethodInfo,
	NULL
};
extern const MethodInfo ResolveEventHandler_Invoke_m12625_MethodInfo;
extern const MethodInfo ResolveEventHandler_BeginInvoke_m12626_MethodInfo;
extern const MethodInfo ResolveEventHandler_EndInvoke_m12627_MethodInfo;
static const Il2CppMethodReference ResolveEventHandler_t2181_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&ResolveEventHandler_Invoke_m12625_MethodInfo,
	&ResolveEventHandler_BeginInvoke_m12626_MethodInfo,
	&ResolveEventHandler_EndInvoke_m12627_MethodInfo,
};
static bool ResolveEventHandler_t2181_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ResolveEventHandler_t2181_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventHandler_t2181_0_0_0;
extern const Il2CppType ResolveEventHandler_t2181_1_0_0;
struct ResolveEventHandler_t2181;
const Il2CppTypeDefinitionMetadata ResolveEventHandler_t2181_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ResolveEventHandler_t2181_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, ResolveEventHandler_t2181_VTable/* vtableMethods */
	, ResolveEventHandler_t2181_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ResolveEventHandler_t2181_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventHandler"/* name */
	, "System"/* namespaze */
	, ResolveEventHandler_t2181_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ResolveEventHandler_t2181_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1016/* custom_attributes_cache */
	, &ResolveEventHandler_t2181_0_0_0/* byval_arg */
	, &ResolveEventHandler_t2181_1_0_0/* this_arg */
	, &ResolveEventHandler_t2181_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ResolveEventHandler_t2181/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventHandler_t2181)/* instance_size */
	, sizeof (ResolveEventHandler_t2181)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnhandledExceptionEventHandler
#include "mscorlib_System_UnhandledExceptionEventHandler.h"
// Metadata Definition System.UnhandledExceptionEventHandler
extern TypeInfo UnhandledExceptionEventHandler_t2183_il2cpp_TypeInfo;
// System.UnhandledExceptionEventHandler
#include "mscorlib_System_UnhandledExceptionEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2183_UnhandledExceptionEventHandler__ctor_m12628_ParameterInfos[] = 
{
	{"object", 0, 134225473, 0, &Object_t_0_0_0},
	{"method", 1, 134225474, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnhandledExceptionEventHandler__ctor_m12628_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler__ctor_m12628/* method */
	, &UnhandledExceptionEventHandler_t2183_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2183_UnhandledExceptionEventHandler__ctor_m12628_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t2247_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2183_UnhandledExceptionEventHandler_Invoke_m12629_ParameterInfos[] = 
{
	{"sender", 0, 134225475, 0, &Object_t_0_0_0},
	{"e", 1, 134225476, 0, &UnhandledExceptionEventArgs_t2247_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::Invoke(System.Object,System.UnhandledExceptionEventArgs)
extern const MethodInfo UnhandledExceptionEventHandler_Invoke_m12629_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_Invoke_m12629/* method */
	, &UnhandledExceptionEventHandler_t2183_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2183_UnhandledExceptionEventHandler_Invoke_m12629_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t2247_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2183_UnhandledExceptionEventHandler_BeginInvoke_m12630_ParameterInfos[] = 
{
	{"sender", 0, 134225477, 0, &Object_t_0_0_0},
	{"e", 1, 134225478, 0, &UnhandledExceptionEventArgs_t2247_0_0_0},
	{"callback", 2, 134225479, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134225480, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.UnhandledExceptionEventHandler::BeginInvoke(System.Object,System.UnhandledExceptionEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo UnhandledExceptionEventHandler_BeginInvoke_m12630_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_BeginInvoke_m12630/* method */
	, &UnhandledExceptionEventHandler_t2183_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2183_UnhandledExceptionEventHandler_BeginInvoke_m12630_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2183_UnhandledExceptionEventHandler_EndInvoke_m12631_ParameterInfos[] = 
{
	{"result", 0, 134225481, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnhandledExceptionEventHandler_EndInvoke_m12631_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_EndInvoke_m12631/* method */
	, &UnhandledExceptionEventHandler_t2183_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2183_UnhandledExceptionEventHandler_EndInvoke_m12631_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnhandledExceptionEventHandler_t2183_MethodInfos[] =
{
	&UnhandledExceptionEventHandler__ctor_m12628_MethodInfo,
	&UnhandledExceptionEventHandler_Invoke_m12629_MethodInfo,
	&UnhandledExceptionEventHandler_BeginInvoke_m12630_MethodInfo,
	&UnhandledExceptionEventHandler_EndInvoke_m12631_MethodInfo,
	NULL
};
extern const MethodInfo UnhandledExceptionEventHandler_Invoke_m12629_MethodInfo;
extern const MethodInfo UnhandledExceptionEventHandler_BeginInvoke_m12630_MethodInfo;
extern const MethodInfo UnhandledExceptionEventHandler_EndInvoke_m12631_MethodInfo;
static const Il2CppMethodReference UnhandledExceptionEventHandler_t2183_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&UnhandledExceptionEventHandler_Invoke_m12629_MethodInfo,
	&UnhandledExceptionEventHandler_BeginInvoke_m12630_MethodInfo,
	&UnhandledExceptionEventHandler_EndInvoke_m12631_MethodInfo,
};
static bool UnhandledExceptionEventHandler_t2183_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnhandledExceptionEventHandler_t2183_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnhandledExceptionEventHandler_t2183_0_0_0;
extern const Il2CppType UnhandledExceptionEventHandler_t2183_1_0_0;
struct UnhandledExceptionEventHandler_t2183;
const Il2CppTypeDefinitionMetadata UnhandledExceptionEventHandler_t2183_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnhandledExceptionEventHandler_t2183_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, UnhandledExceptionEventHandler_t2183_VTable/* vtableMethods */
	, UnhandledExceptionEventHandler_t2183_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnhandledExceptionEventHandler_t2183_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnhandledExceptionEventHandler"/* name */
	, "System"/* namespaze */
	, UnhandledExceptionEventHandler_t2183_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnhandledExceptionEventHandler_t2183_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1017/* custom_attributes_cache */
	, &UnhandledExceptionEventHandler_t2183_0_0_0/* byval_arg */
	, &UnhandledExceptionEventHandler_t2183_1_0_0/* this_arg */
	, &UnhandledExceptionEventHandler_t2183_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t2183/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnhandledExceptionEventHandler_t2183)/* instance_size */
	, sizeof (UnhandledExceptionEventHandler_t2183)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$56
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$56
extern TypeInfo U24ArrayTypeU2456_t2254_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$56
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2456_t2254_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m1076_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m1077_MethodInfo;
static const Il2CppMethodReference U24ArrayTypeU2456_t2254_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2456_t2254_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2456_t2254_0_0_0;
extern const Il2CppType U24ArrayTypeU2456_t2254_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t2274_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t2274_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2456_t2254_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2456_t2254_VTable/* vtableMethods */
	, U24ArrayTypeU2456_t2254_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2456_t2254_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$56"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2456_t2254_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2456_t2254_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2456_t2254_0_0_0/* byval_arg */
	, &U24ArrayTypeU2456_t2254_1_0_0/* this_arg */
	, &U24ArrayTypeU2456_t2254_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2456_t2254_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2456_t2254_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2456_t2254_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2456_t2254)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2456_t2254)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2456_t2254_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$24
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$24
extern TypeInfo U24ArrayTypeU2424_t2255_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$24
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2424_t2255_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2424_t2255_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2424_t2255_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2424_t2255_0_0_0;
extern const Il2CppType U24ArrayTypeU2424_t2255_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2424_t2255_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2424_t2255_VTable/* vtableMethods */
	, U24ArrayTypeU2424_t2255_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2424_t2255_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$24"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2424_t2255_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2424_t2255_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2424_t2255_0_0_0/* byval_arg */
	, &U24ArrayTypeU2424_t2255_1_0_0/* this_arg */
	, &U24ArrayTypeU2424_t2255_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2424_t2255_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2424_t2255_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2424_t2255_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2424_t2255)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2424_t2255)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2424_t2255_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$16
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
extern TypeInfo U24ArrayTypeU2416_t2256_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$16
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2416_t2256_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2416_t2256_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2416_t2256_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2416_t2256_0_0_0;
extern const Il2CppType U24ArrayTypeU2416_t2256_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2416_t2256_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2416_t2256_VTable/* vtableMethods */
	, U24ArrayTypeU2416_t2256_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2416_t2256_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$16"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2416_t2256_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2416_t2256_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2416_t2256_0_0_0/* byval_arg */
	, &U24ArrayTypeU2416_t2256_1_0_0/* this_arg */
	, &U24ArrayTypeU2416_t2256_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2416_t2256_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t2256_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t2256_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2416_t2256)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2416_t2256)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2416_t2256_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$120
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$120
extern TypeInfo U24ArrayTypeU24120_t2257_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$120
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24120_t2257_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24120_t2257_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU24120_t2257_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24120_t2257_0_0_0;
extern const Il2CppType U24ArrayTypeU24120_t2257_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24120_t2257_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU24120_t2257_VTable/* vtableMethods */
	, U24ArrayTypeU24120_t2257_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24120_t2257_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$120"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24120_t2257_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24120_t2257_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24120_t2257_0_0_0/* byval_arg */
	, &U24ArrayTypeU24120_t2257_1_0_0/* this_arg */
	, &U24ArrayTypeU24120_t2257_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24120_t2257_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t2257_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t2257_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24120_t2257)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24120_t2257)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24120_t2257_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$3132
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
extern TypeInfo U24ArrayTypeU243132_t2258_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$3132
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU243132_t2258_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU243132_t2258_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU243132_t2258_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU243132_t2258_0_0_0;
extern const Il2CppType U24ArrayTypeU243132_t2258_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU243132_t2258_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU243132_t2258_VTable/* vtableMethods */
	, U24ArrayTypeU243132_t2258_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU243132_t2258_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$3132"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU243132_t2258_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU243132_t2258_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU243132_t2258_0_0_0/* byval_arg */
	, &U24ArrayTypeU243132_t2258_1_0_0/* this_arg */
	, &U24ArrayTypeU243132_t2258_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU243132_t2258_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t2258_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t2258_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU243132_t2258)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU243132_t2258)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU243132_t2258_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t2259_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2420_t2259_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2420_t2259_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2420_t2259_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2420_t2259_0_0_0;
extern const Il2CppType U24ArrayTypeU2420_t2259_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t2259_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2420_t2259_VTable/* vtableMethods */
	, U24ArrayTypeU2420_t2259_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2420_t2259_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2420_t2259_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2420_t2259_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t2259_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t2259_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t2259_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t2259_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t2259_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t2259_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t2259)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t2259)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t2259_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
extern TypeInfo U24ArrayTypeU2432_t2260_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2432_t2260_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2432_t2260_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2432_t2260_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2432_t2260_0_0_0;
extern const Il2CppType U24ArrayTypeU2432_t2260_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2432_t2260_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2432_t2260_VTable/* vtableMethods */
	, U24ArrayTypeU2432_t2260_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2432_t2260_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$32"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2432_t2260_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2432_t2260_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2432_t2260_0_0_0/* byval_arg */
	, &U24ArrayTypeU2432_t2260_1_0_0/* this_arg */
	, &U24ArrayTypeU2432_t2260_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2432_t2260_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t2260_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t2260_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2432_t2260)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2432_t2260)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2432_t2260_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t2261_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2448_t2261_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2448_t2261_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2448_t2261_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2448_t2261_0_0_0;
extern const Il2CppType U24ArrayTypeU2448_t2261_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t2261_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2448_t2261_VTable/* vtableMethods */
	, U24ArrayTypeU2448_t2261_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2448_t2261_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2448_t2261_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2448_t2261_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t2261_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t2261_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t2261_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t2261_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t2261_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t2261_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t2261)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t2261)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t2261_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
extern TypeInfo U24ArrayTypeU2464_t2262_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2464_t2262_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2464_t2262_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2464_t2262_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2464_t2262_0_0_0;
extern const Il2CppType U24ArrayTypeU2464_t2262_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2464_t2262_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2464_t2262_VTable/* vtableMethods */
	, U24ArrayTypeU2464_t2262_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2464_t2262_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$64"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2464_t2262_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2464_t2262_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2464_t2262_0_0_0/* byval_arg */
	, &U24ArrayTypeU2464_t2262_1_0_0/* this_arg */
	, &U24ArrayTypeU2464_t2262_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2464_t2262_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t2262_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t2262_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2464_t2262)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2464_t2262)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2464_t2262_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t2263_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2412_t2263_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2412_t2263_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2412_t2263_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t2263_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t2263_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t2263_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2412_t2263_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t2263_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2412_t2263_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t2263_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2412_t2263_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t2263_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t2263_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t2263_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t2263_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2263_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2263_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t2263)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t2263)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t2263_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$136
extern TypeInfo U24ArrayTypeU24136_t2264_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24136_t2264_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24136_t2264_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU24136_t2264_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24136_t2264_0_0_0;
extern const Il2CppType U24ArrayTypeU24136_t2264_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24136_t2264_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU24136_t2264_VTable/* vtableMethods */
	, U24ArrayTypeU24136_t2264_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24136_t2264_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$136"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24136_t2264_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24136_t2264_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24136_t2264_0_0_0/* byval_arg */
	, &U24ArrayTypeU24136_t2264_1_0_0/* this_arg */
	, &U24ArrayTypeU24136_t2264_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24136_t2264_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t2264_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t2264_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24136_t2264)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24136_t2264)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24136_t2264_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$72
extern TypeInfo U24ArrayTypeU2472_t2265_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2472_t2265_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2472_t2265_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2472_t2265_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2472_t2265_0_0_0;
extern const Il2CppType U24ArrayTypeU2472_t2265_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2472_t2265_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2472_t2265_VTable/* vtableMethods */
	, U24ArrayTypeU2472_t2265_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2472_t2265_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$72"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2472_t2265_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2472_t2265_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2472_t2265_0_0_0/* byval_arg */
	, &U24ArrayTypeU2472_t2265_1_0_0/* this_arg */
	, &U24ArrayTypeU2472_t2265_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2472_t2265_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2472_t2265_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2472_t2265_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2472_t2265)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2472_t2265)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2472_t2265_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$124
extern TypeInfo U24ArrayTypeU24124_t2266_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24124_t2266_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24124_t2266_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU24124_t2266_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24124_t2266_0_0_0;
extern const Il2CppType U24ArrayTypeU24124_t2266_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24124_t2266_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU24124_t2266_VTable/* vtableMethods */
	, U24ArrayTypeU24124_t2266_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24124_t2266_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$124"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24124_t2266_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24124_t2266_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24124_t2266_0_0_0/* byval_arg */
	, &U24ArrayTypeU24124_t2266_1_0_0/* this_arg */
	, &U24ArrayTypeU24124_t2266_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24124_t2266_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t2266_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t2266_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24124_t2266)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24124_t2266)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24124_t2266_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$96
extern TypeInfo U24ArrayTypeU2496_t2267_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2496_t2267_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2496_t2267_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2496_t2267_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2496_t2267_0_0_0;
extern const Il2CppType U24ArrayTypeU2496_t2267_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2496_t2267_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2496_t2267_VTable/* vtableMethods */
	, U24ArrayTypeU2496_t2267_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2496_t2267_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$96"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2496_t2267_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2496_t2267_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2496_t2267_0_0_0/* byval_arg */
	, &U24ArrayTypeU2496_t2267_1_0_0/* this_arg */
	, &U24ArrayTypeU2496_t2267_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2496_t2267_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2496_t2267_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2496_t2267_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2496_t2267)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2496_t2267)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2496_t2267_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$2048
extern TypeInfo U24ArrayTypeU242048_t2268_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU242048_t2268_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU242048_t2268_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU242048_t2268_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU242048_t2268_0_0_0;
extern const Il2CppType U24ArrayTypeU242048_t2268_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU242048_t2268_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU242048_t2268_VTable/* vtableMethods */
	, U24ArrayTypeU242048_t2268_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU242048_t2268_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$2048"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU242048_t2268_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU242048_t2268_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU242048_t2268_0_0_0/* byval_arg */
	, &U24ArrayTypeU242048_t2268_1_0_0/* this_arg */
	, &U24ArrayTypeU242048_t2268_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU242048_t2268_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU242048_t2268_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU242048_t2268_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU242048_t2268)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU242048_t2268)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU242048_t2268_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t2269_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24256_t2269_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24256_t2269_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU24256_t2269_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t2269_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t2269_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t2269_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU24256_t2269_VTable/* vtableMethods */
	, U24ArrayTypeU24256_t2269_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24256_t2269_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24256_t2269_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24256_t2269_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t2269_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t2269_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t2269_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t2269_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t2269_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t2269_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t2269)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t2269)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t2269_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$1024
extern TypeInfo U24ArrayTypeU241024_t2270_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU241024_t2270_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU241024_t2270_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU241024_t2270_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU241024_t2270_0_0_0;
extern const Il2CppType U24ArrayTypeU241024_t2270_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU241024_t2270_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU241024_t2270_VTable/* vtableMethods */
	, U24ArrayTypeU241024_t2270_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU241024_t2270_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$1024"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU241024_t2270_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU241024_t2270_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU241024_t2270_0_0_0/* byval_arg */
	, &U24ArrayTypeU241024_t2270_1_0_0/* this_arg */
	, &U24ArrayTypeU241024_t2270_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU241024_t2270_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t2270_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t2270_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU241024_t2270)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU241024_t2270)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU241024_t2270_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$640
extern TypeInfo U24ArrayTypeU24640_t2271_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24640_t2271_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24640_t2271_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU24640_t2271_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24640_t2271_0_0_0;
extern const Il2CppType U24ArrayTypeU24640_t2271_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24640_t2271_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU24640_t2271_VTable/* vtableMethods */
	, U24ArrayTypeU24640_t2271_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24640_t2271_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$640"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24640_t2271_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24640_t2271_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24640_t2271_0_0_0/* byval_arg */
	, &U24ArrayTypeU24640_t2271_1_0_0/* this_arg */
	, &U24ArrayTypeU24640_t2271_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24640_t2271_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24640_t2271_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24640_t2271_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24640_t2271)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24640_t2271)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24640_t2271_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
extern TypeInfo U24ArrayTypeU24128_t2272_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24128_t2272_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24128_t2272_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU24128_t2272_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24128_t2272_0_0_0;
extern const Il2CppType U24ArrayTypeU24128_t2272_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24128_t2272_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU24128_t2272_VTable/* vtableMethods */
	, U24ArrayTypeU24128_t2272_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24128_t2272_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$128"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24128_t2272_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24128_t2272_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24128_t2272_0_0_0/* byval_arg */
	, &U24ArrayTypeU24128_t2272_1_0_0/* this_arg */
	, &U24ArrayTypeU24128_t2272_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24128_t2272_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t2272_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t2272_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24128_t2272)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24128_t2272)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24128_t2272_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$52
extern TypeInfo U24ArrayTypeU2452_t2273_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2452_t2273_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2452_t2273_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2452_t2273_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2452_t2273_0_0_0;
extern const Il2CppType U24ArrayTypeU2452_t2273_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2452_t2273_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2452_t2273_VTable/* vtableMethods */
	, U24ArrayTypeU2452_t2273_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2452_t2273_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$52"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2452_t2273_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2452_t2273_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2452_t2273_0_0_0/* byval_arg */
	, &U24ArrayTypeU2452_t2273_1_0_0/* this_arg */
	, &U24ArrayTypeU2452_t2273_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2452_t2273_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2452_t2273_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2452_t2273_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2452_t2273)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2452_t2273)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2452_t2273_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t2274_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t2274_il2cpp_TypeInfo__nestedTypes[20] =
{
	&U24ArrayTypeU2456_t2254_0_0_0,
	&U24ArrayTypeU2424_t2255_0_0_0,
	&U24ArrayTypeU2416_t2256_0_0_0,
	&U24ArrayTypeU24120_t2257_0_0_0,
	&U24ArrayTypeU243132_t2258_0_0_0,
	&U24ArrayTypeU2420_t2259_0_0_0,
	&U24ArrayTypeU2432_t2260_0_0_0,
	&U24ArrayTypeU2448_t2261_0_0_0,
	&U24ArrayTypeU2464_t2262_0_0_0,
	&U24ArrayTypeU2412_t2263_0_0_0,
	&U24ArrayTypeU24136_t2264_0_0_0,
	&U24ArrayTypeU2472_t2265_0_0_0,
	&U24ArrayTypeU24124_t2266_0_0_0,
	&U24ArrayTypeU2496_t2267_0_0_0,
	&U24ArrayTypeU242048_t2268_0_0_0,
	&U24ArrayTypeU24256_t2269_0_0_0,
	&U24ArrayTypeU241024_t2270_0_0_0,
	&U24ArrayTypeU24640_t2271_0_0_0,
	&U24ArrayTypeU24128_t2272_0_0_0,
	&U24ArrayTypeU2452_t2273_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t2274_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t2274_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t2274_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t2274;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t2274_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t2274_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t2274_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t2274_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 3101/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t2274_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t2274_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t2274_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1018/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t2274_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t2274_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t2274_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2274)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2274)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t2274_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 52/* field_count */
	, 0/* event_count */
	, 20/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
