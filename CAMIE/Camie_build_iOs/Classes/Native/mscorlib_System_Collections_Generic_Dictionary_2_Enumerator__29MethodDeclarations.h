﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
struct Enumerator_t3411;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t3406;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_33.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21868_gshared (Enumerator_t3411 * __this, Dictionary_2_t3406 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m21868(__this, ___dictionary, method) (( void (*) (Enumerator_t3411 *, Dictionary_2_t3406 *, const MethodInfo*))Enumerator__ctor_m21868_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21869_gshared (Enumerator_t3411 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21869(__this, method) (( Object_t * (*) (Enumerator_t3411 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21869_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1430  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21870_gshared (Enumerator_t3411 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21870(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3411 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21870_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21871_gshared (Enumerator_t3411 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21871(__this, method) (( Object_t * (*) (Enumerator_t3411 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21871_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21872_gshared (Enumerator_t3411 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21872(__this, method) (( Object_t * (*) (Enumerator_t3411 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21872_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21873_gshared (Enumerator_t3411 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21873(__this, method) (( bool (*) (Enumerator_t3411 *, const MethodInfo*))Enumerator_MoveNext_m21873_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_Current()
extern "C" KeyValuePair_2_t3407  Enumerator_get_Current_m21874_gshared (Enumerator_t3411 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21874(__this, method) (( KeyValuePair_2_t3407  (*) (Enumerator_t3411 *, const MethodInfo*))Enumerator_get_Current_m21874_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m21875_gshared (Enumerator_t3411 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m21875(__this, method) (( Object_t * (*) (Enumerator_t3411 *, const MethodInfo*))Enumerator_get_CurrentKey_m21875_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_CurrentValue()
extern "C" uint8_t Enumerator_get_CurrentValue_m21876_gshared (Enumerator_t3411 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m21876(__this, method) (( uint8_t (*) (Enumerator_t3411 *, const MethodInfo*))Enumerator_get_CurrentValue_m21876_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::VerifyState()
extern "C" void Enumerator_VerifyState_m21877_gshared (Enumerator_t3411 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m21877(__this, method) (( void (*) (Enumerator_t3411 *, const MethodInfo*))Enumerator_VerifyState_m21877_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m21878_gshared (Enumerator_t3411 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m21878(__this, method) (( void (*) (Enumerator_t3411 *, const MethodInfo*))Enumerator_VerifyCurrent_m21878_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m21879_gshared (Enumerator_t3411 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21879(__this, method) (( void (*) (Enumerator_t3411 *, const MethodInfo*))Enumerator_Dispose_m21879_gshared)(__this, method)
