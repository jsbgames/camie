﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>
struct ReadOnlyCollection_1_t2867;
// UnityEngine.Rigidbody
struct Rigidbody_t14;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.Rigidbody>
struct IList_1_t2866;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t2865;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Rigidbody>
struct IEnumerator_1_t3532;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m14211(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2867 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m13989_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14212(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2867 *, Rigidbody_t14 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m13990_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14213(__this, method) (( void (*) (ReadOnlyCollection_1_t2867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m13991_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14214(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2867 *, int32_t, Rigidbody_t14 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m13992_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14215(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2867 *, Rigidbody_t14 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m13993_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14216(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2867 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m13994_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14217(__this, ___index, method) (( Rigidbody_t14 * (*) (ReadOnlyCollection_1_t2867 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m13995_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14218(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2867 *, int32_t, Rigidbody_t14 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m13996_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14219(__this, method) (( bool (*) (ReadOnlyCollection_1_t2867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13997_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14220(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2867 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m13998_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14221(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m13999_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m14222(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2867 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m14000_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m14223(__this, method) (( void (*) (ReadOnlyCollection_1_t2867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m14001_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m14224(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2867 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m14002_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14225(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2867 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14003_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m14226(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2867 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m14004_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m14227(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2867 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m14005_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14228(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2867 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14006_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14229(__this, method) (( bool (*) (ReadOnlyCollection_1_t2867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14007_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14230(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14008_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14231(__this, method) (( bool (*) (ReadOnlyCollection_1_t2867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14009_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14232(__this, method) (( bool (*) (ReadOnlyCollection_1_t2867 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14010_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m14233(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2867 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m14011_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m14234(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2867 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m14012_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::Contains(T)
#define ReadOnlyCollection_1_Contains_m14235(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2867 *, Rigidbody_t14 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m14013_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m14236(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2867 *, RigidbodyU5BU5D_t2865*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m14014_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m14237(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2867 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m14015_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m14238(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2867 *, Rigidbody_t14 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m14016_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::get_Count()
#define ReadOnlyCollection_1_get_Count_m14239(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2867 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m14017_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m14240(__this, ___index, method) (( Rigidbody_t14 * (*) (ReadOnlyCollection_1_t2867 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m14018_gshared)(__this, ___index, method)
