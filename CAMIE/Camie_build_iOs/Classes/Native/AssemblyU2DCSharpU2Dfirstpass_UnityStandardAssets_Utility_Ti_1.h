﻿#pragma once
#include <stdint.h>
// UnityStandardAssets.Utility.TimedObjectActivator/Entry[]
struct EntryU5BU5D_t200;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct  Entries_t201  : public Object_t
{
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry[] UnityStandardAssets.Utility.TimedObjectActivator/Entries::entries
	EntryU5BU5D_t200* ___entries_0;
};
