﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1366;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3581;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t3429;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t3433;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2980;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t3830;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1043;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3831;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t3832;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1429;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_35.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__31.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m21991_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m21991(__this, method) (( void (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2__ctor_m21991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21992_gshared (Dictionary_2_t1366 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21992(__this, ___comparer, method) (( void (*) (Dictionary_2_t1366 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21992_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m21993_gshared (Dictionary_2_t1366 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m21993(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1366 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21993_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m21994_gshared (Dictionary_2_t1366 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m21994(__this, ___capacity, method) (( void (*) (Dictionary_2_t1366 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m21994_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21995_gshared (Dictionary_2_t1366 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21995(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1366 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21995_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m21996_gshared (Dictionary_2_t1366 * __this, SerializationInfo_t1043 * ___info, StreamingContext_t1044  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m21996(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1366 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2__ctor_m21996_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21997_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21997(__this, method) (( Object_t* (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21997_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21998_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21998(__this, method) (( Object_t* (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21998_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m21999_gshared (Dictionary_2_t1366 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m21999(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1366 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m21999_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22000_gshared (Dictionary_2_t1366 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22000(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1366 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m22000_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22001_gshared (Dictionary_2_t1366 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m22001(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1366 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m22001_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m22002_gshared (Dictionary_2_t1366 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m22002(__this, ___key, method) (( bool (*) (Dictionary_2_t1366 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m22002_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22003_gshared (Dictionary_2_t1366 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m22003(__this, ___key, method) (( void (*) (Dictionary_2_t1366 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m22003_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22004_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22004(__this, method) (( bool (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22004_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22005_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22005(__this, method) (( Object_t * (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22005_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22006_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22006(__this, method) (( bool (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22006_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22007_gshared (Dictionary_2_t1366 * __this, KeyValuePair_2_t3427  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22007(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1366 *, KeyValuePair_2_t3427 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22007_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22008_gshared (Dictionary_2_t1366 * __this, KeyValuePair_2_t3427  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22008(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1366 *, KeyValuePair_2_t3427 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22008_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22009_gshared (Dictionary_2_t1366 * __this, KeyValuePair_2U5BU5D_t3831* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22009(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1366 *, KeyValuePair_2U5BU5D_t3831*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22009_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22010_gshared (Dictionary_2_t1366 * __this, KeyValuePair_2_t3427  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22010(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1366 *, KeyValuePair_2_t3427 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22010_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22011_gshared (Dictionary_2_t1366 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22011(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1366 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m22011_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22012_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22012(__this, method) (( Object_t * (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22012_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22013_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22013(__this, method) (( Object_t* (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22013_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22014_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22014(__this, method) (( Object_t * (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22014_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m22015_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m22015(__this, method) (( int32_t (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_get_Count_m22015_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m22016_gshared (Dictionary_2_t1366 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m22016(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1366 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m22016_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m22017_gshared (Dictionary_2_t1366 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m22017(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1366 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m22017_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m22018_gshared (Dictionary_2_t1366 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m22018(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1366 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m22018_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m22019_gshared (Dictionary_2_t1366 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m22019(__this, ___size, method) (( void (*) (Dictionary_2_t1366 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m22019_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m22020_gshared (Dictionary_2_t1366 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m22020(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1366 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m22020_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3427  Dictionary_2_make_pair_m22021_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m22021(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3427  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m22021_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m22022_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m22022(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m22022_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m22023_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m22023(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m22023_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m22024_gshared (Dictionary_2_t1366 * __this, KeyValuePair_2U5BU5D_t3831* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m22024(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1366 *, KeyValuePair_2U5BU5D_t3831*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m22024_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m22025_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m22025(__this, method) (( void (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_Resize_m22025_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m22026_gshared (Dictionary_2_t1366 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m22026(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1366 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m22026_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m22027_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m22027(__this, method) (( void (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_Clear_m22027_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m22028_gshared (Dictionary_2_t1366 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m22028(__this, ___key, method) (( bool (*) (Dictionary_2_t1366 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m22028_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m22029_gshared (Dictionary_2_t1366 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m22029(__this, ___value, method) (( bool (*) (Dictionary_2_t1366 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m22029_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m22030_gshared (Dictionary_2_t1366 * __this, SerializationInfo_t1043 * ___info, StreamingContext_t1044  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m22030(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1366 *, SerializationInfo_t1043 *, StreamingContext_t1044 , const MethodInfo*))Dictionary_2_GetObjectData_m22030_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m22031_gshared (Dictionary_2_t1366 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m22031(__this, ___sender, method) (( void (*) (Dictionary_2_t1366 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m22031_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m22032_gshared (Dictionary_2_t1366 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m22032(__this, ___key, method) (( bool (*) (Dictionary_2_t1366 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m22032_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m22033_gshared (Dictionary_2_t1366 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m22033(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1366 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m22033_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Keys()
extern "C" KeyCollection_t3429 * Dictionary_2_get_Keys_m22034_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m22034(__this, method) (( KeyCollection_t3429 * (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_get_Keys_m22034_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Values()
extern "C" ValueCollection_t3433 * Dictionary_2_get_Values_m22035_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m22035(__this, method) (( ValueCollection_t3433 * (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_get_Values_m22035_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m22036_gshared (Dictionary_2_t1366 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m22036(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1366 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m22036_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m22037_gshared (Dictionary_2_t1366 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m22037(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1366 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m22037_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m22038_gshared (Dictionary_2_t1366 * __this, KeyValuePair_2_t3427  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m22038(__this, ___pair, method) (( bool (*) (Dictionary_2_t1366 *, KeyValuePair_2_t3427 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22038_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetEnumerator()
extern "C" Enumerator_t3431  Dictionary_2_GetEnumerator_m22039_gshared (Dictionary_2_t1366 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m22039(__this, method) (( Enumerator_t3431  (*) (Dictionary_2_t1366 *, const MethodInfo*))Dictionary_2_GetEnumerator_m22039_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1430  Dictionary_2_U3CCopyToU3Em__0_m22040_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m22040(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1430  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m22040_gshared)(__this /* static, unused */, ___key, ___value, method)
