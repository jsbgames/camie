﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_t455;

// System.Void UnityEngine.EventSystems.UIBehaviour::.ctor()
extern "C" void UIBehaviour__ctor_m1974 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Awake()
extern "C" void UIBehaviour_Awake_m1975 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnEnable()
extern "C" void UIBehaviour_OnEnable_m1976 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Start()
extern "C" void UIBehaviour_Start_m1977 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDisable()
extern "C" void UIBehaviour_OnDisable_m1978 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDestroy()
extern "C" void UIBehaviour_OnDestroy_m1979 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive()
extern "C" bool UIBehaviour_IsActive_m1980 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnRectTransformDimensionsChange()
extern "C" void UIBehaviour_OnRectTransformDimensionsChange_m1981 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnBeforeTransformParentChanged()
extern "C" void UIBehaviour_OnBeforeTransformParentChanged_m1982 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged()
extern "C" void UIBehaviour_OnTransformParentChanged_m1983 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDidApplyAnimationProperties()
extern "C" void UIBehaviour_OnDidApplyAnimationProperties_m1984 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnCanvasGroupChanged()
extern "C" void UIBehaviour_OnCanvasGroupChanged_m1985 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnCanvasHierarchyChanged()
extern "C" void UIBehaviour_OnCanvasHierarchyChanged_m1986 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsDestroyed()
extern "C" bool UIBehaviour_IsDestroyed_m1987 (UIBehaviour_t455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
