﻿#pragma once
#include <stdint.h>
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_t199;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9
struct  U3CActivateU3Ec__Iterator9_t202  : public Object_t
{
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::entry
	Entry_t199 * ___entry_0;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::$PC
	int32_t ___U24PC_1;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::$current
	Object_t * ___U24current_2;
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator9::<$>entry
	Entry_t199 * ___U3CU24U3Eentry_3;
};
