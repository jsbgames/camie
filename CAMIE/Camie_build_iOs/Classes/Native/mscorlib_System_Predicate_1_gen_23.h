﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t418;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Graphic>
struct  Predicate_1_t3117  : public MulticastDelegate_t549
{
};
