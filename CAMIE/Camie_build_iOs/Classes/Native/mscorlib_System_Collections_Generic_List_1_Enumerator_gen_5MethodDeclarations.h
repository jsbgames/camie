﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.String>
struct Enumerator_t2856;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t52;

// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m14118(__this, ___l, method) (( void (*) (Enumerator_t2856 *, List_1_t52 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14119(__this, method) (( Object_t * (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
#define Enumerator_Dispose_m14120(__this, method) (( void (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::VerifyState()
#define Enumerator_VerifyState_m14121(__this, method) (( void (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
#define Enumerator_MoveNext_m14122(__this, method) (( bool (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
#define Enumerator_get_Current_m14123(__this, method) (( String_t* (*) (Enumerator_t2856 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
