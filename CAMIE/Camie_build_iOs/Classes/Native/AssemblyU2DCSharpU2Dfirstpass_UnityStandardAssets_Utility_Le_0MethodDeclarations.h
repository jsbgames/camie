﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t185;
// System.Collections.IEnumerator
struct IEnumerator_t217;

// System.Void UnityStandardAssets.Utility.LerpControlledBob::.ctor()
extern "C" void LerpControlledBob__ctor_m504 (LerpControlledBob_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityStandardAssets.Utility.LerpControlledBob::Offset()
extern "C" float LerpControlledBob_Offset_m505 (LerpControlledBob_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityStandardAssets.Utility.LerpControlledBob::DoBobCycle()
extern "C" Object_t * LerpControlledBob_DoBobCycle_m506 (LerpControlledBob_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
