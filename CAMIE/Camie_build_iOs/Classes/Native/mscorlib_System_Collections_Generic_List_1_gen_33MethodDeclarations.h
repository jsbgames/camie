﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct List_1_t914;
// System.Object
struct Object_t;
// UnityEngine.Networking.Match.MatchDirectConnectInfo
struct MatchDirectConnectInfo_t913;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct IEnumerable_1_t3749;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct IEnumerator_1_t3750;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct ICollection_1_t3751;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct ReadOnlyCollection_1_t3276;
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
struct MatchDirectConnectInfoU5BU5D_t3274;
// System.Predicate`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct Predicate_1_t3277;
// System.Comparison`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct Comparison_1_t3279;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDirectConnectInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_34.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m20104(__this, method) (( void (*) (List_1_t914 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20105(__this, ___collection, method) (( void (*) (List_1_t914 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::.ctor(System.Int32)
#define List_1__ctor_m20106(__this, ___capacity, method) (( void (*) (List_1_t914 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::.cctor()
#define List_1__cctor_m20107(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20108(__this, method) (( Object_t* (*) (List_1_t914 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20109(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t914 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20110(__this, method) (( Object_t * (*) (List_1_t914 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20111(__this, ___item, method) (( int32_t (*) (List_1_t914 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20112(__this, ___item, method) (( bool (*) (List_1_t914 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20113(__this, ___item, method) (( int32_t (*) (List_1_t914 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20114(__this, ___index, ___item, method) (( void (*) (List_1_t914 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20115(__this, ___item, method) (( void (*) (List_1_t914 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20116(__this, method) (( bool (*) (List_1_t914 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20117(__this, method) (( bool (*) (List_1_t914 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20118(__this, method) (( Object_t * (*) (List_1_t914 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20119(__this, method) (( bool (*) (List_1_t914 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20120(__this, method) (( bool (*) (List_1_t914 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20121(__this, ___index, method) (( Object_t * (*) (List_1_t914 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20122(__this, ___index, ___value, method) (( void (*) (List_1_t914 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Add(T)
#define List_1_Add_m20123(__this, ___item, method) (( void (*) (List_1_t914 *, MatchDirectConnectInfo_t913 *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20124(__this, ___newCount, method) (( void (*) (List_1_t914 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20125(__this, ___collection, method) (( void (*) (List_1_t914 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20126(__this, ___enumerable, method) (( void (*) (List_1_t914 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20127(__this, ___collection, method) (( void (*) (List_1_t914 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::AsReadOnly()
#define List_1_AsReadOnly_m20128(__this, method) (( ReadOnlyCollection_1_t3276 * (*) (List_1_t914 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Clear()
#define List_1_Clear_m20129(__this, method) (( void (*) (List_1_t914 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Contains(T)
#define List_1_Contains_m20130(__this, ___item, method) (( bool (*) (List_1_t914 *, MatchDirectConnectInfo_t913 *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20131(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t914 *, MatchDirectConnectInfoU5BU5D_t3274*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Find(System.Predicate`1<T>)
#define List_1_Find_m20132(__this, ___match, method) (( MatchDirectConnectInfo_t913 * (*) (List_1_t914 *, Predicate_1_t3277 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20133(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3277 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20134(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t914 *, int32_t, int32_t, Predicate_1_t3277 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::GetEnumerator()
#define List_1_GetEnumerator_m20135(__this, method) (( Enumerator_t3278  (*) (List_1_t914 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::IndexOf(T)
#define List_1_IndexOf_m20136(__this, ___item, method) (( int32_t (*) (List_1_t914 *, MatchDirectConnectInfo_t913 *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20137(__this, ___start, ___delta, method) (( void (*) (List_1_t914 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20138(__this, ___index, method) (( void (*) (List_1_t914 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Insert(System.Int32,T)
#define List_1_Insert_m20139(__this, ___index, ___item, method) (( void (*) (List_1_t914 *, int32_t, MatchDirectConnectInfo_t913 *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20140(__this, ___collection, method) (( void (*) (List_1_t914 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Remove(T)
#define List_1_Remove_m20141(__this, ___item, method) (( bool (*) (List_1_t914 *, MatchDirectConnectInfo_t913 *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20142(__this, ___match, method) (( int32_t (*) (List_1_t914 *, Predicate_1_t3277 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20143(__this, ___index, method) (( void (*) (List_1_t914 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Reverse()
#define List_1_Reverse_m20144(__this, method) (( void (*) (List_1_t914 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Sort()
#define List_1_Sort_m20145(__this, method) (( void (*) (List_1_t914 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20146(__this, ___comparison, method) (( void (*) (List_1_t914 *, Comparison_1_t3279 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::ToArray()
#define List_1_ToArray_m20147(__this, method) (( MatchDirectConnectInfoU5BU5D_t3274* (*) (List_1_t914 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::TrimExcess()
#define List_1_TrimExcess_m20148(__this, method) (( void (*) (List_1_t914 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::get_Capacity()
#define List_1_get_Capacity_m20149(__this, method) (( int32_t (*) (List_1_t914 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20150(__this, ___value, method) (( void (*) (List_1_t914 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::get_Count()
#define List_1_get_Count_m20151(__this, method) (( int32_t (*) (List_1_t914 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::get_Item(System.Int32)
#define List_1_get_Item_m20152(__this, ___index, method) (( MatchDirectConnectInfo_t913 * (*) (List_1_t914 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::set_Item(System.Int32,T)
#define List_1_set_Item_m20153(__this, ___index, ___value, method) (( void (*) (List_1_t914 *, int32_t, MatchDirectConnectInfo_t913 *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
