﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>
struct Enumerator_t2877;
// System.Object
struct Object_t;
// UnityEngine.Material
struct Material_t55;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t251;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#define Enumerator__ctor_m14356(__this, ___l, method) (( void (*) (Enumerator_t2877 *, List_1_t251 *, const MethodInfo*))Enumerator__ctor_m13983_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14357(__this, method) (( Object_t * (*) (Enumerator_t2877 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>::Dispose()
#define Enumerator_Dispose_m14358(__this, method) (( void (*) (Enumerator_t2877 *, const MethodInfo*))Enumerator_Dispose_m13985_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>::VerifyState()
#define Enumerator_VerifyState_m14359(__this, method) (( void (*) (Enumerator_t2877 *, const MethodInfo*))Enumerator_VerifyState_m13986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>::MoveNext()
#define Enumerator_MoveNext_m14360(__this, method) (( bool (*) (Enumerator_t2877 *, const MethodInfo*))Enumerator_MoveNext_m13987_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Material>::get_Current()
#define Enumerator_get_Current_m14361(__this, method) (( Material_t55 * (*) (Enumerator_t2877 *, const MethodInfo*))Enumerator_get_Current_m13988_gshared)(__this, method)
