﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.CrossPlatformInput.TouchPad
struct TouchPad_t49;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t215;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::.ctor()
extern "C" void TouchPad__ctor_m162 (TouchPad_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnEnable()
extern "C" void TouchPad_OnEnable_m163 (TouchPad_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::CreateVirtualAxes()
extern "C" void TouchPad_CreateVirtualAxes_m164 (TouchPad_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::UpdateVirtualAxes(UnityEngine.Vector3)
extern "C" void TouchPad_UpdateVirtualAxes_m165 (TouchPad_t49 * __this, Vector3_t4  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" void TouchPad_OnPointerDown_m166 (TouchPad_t49 * __this, PointerEventData_t215 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::Update()
extern "C" void TouchPad_Update_m167 (TouchPad_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" void TouchPad_OnPointerUp_m168 (TouchPad_t49 * __this, PointerEventData_t215 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnDisable()
extern "C" void TouchPad_OnDisable_m169 (TouchPad_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
