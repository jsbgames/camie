﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__11.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Byte>
struct  ShimEnumerator_t2995  : public Object_t
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Byte>::host_enumerator
	Enumerator_t2989  ___host_enumerator_0;
};
