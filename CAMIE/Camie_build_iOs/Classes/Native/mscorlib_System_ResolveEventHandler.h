﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t1447;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t2236;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.ResolveEventHandler
struct  ResolveEventHandler_t2181  : public MulticastDelegate_t549
{
};
