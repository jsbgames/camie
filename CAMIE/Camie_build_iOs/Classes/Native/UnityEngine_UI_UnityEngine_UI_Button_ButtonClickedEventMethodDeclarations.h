﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t415;

// System.Void UnityEngine.UI.Button/ButtonClickedEvent::.ctor()
extern "C" void ButtonClickedEvent__ctor_m2168 (ButtonClickedEvent_t415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
