﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern TypeInfo* InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t1111_il2cpp_TypeInfo_var;
void g_UnityEngine_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1857);
		RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(107);
		ExtensionAttribute_t1111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1858);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 16;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Analytics"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud.Service"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t258 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t258 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1021(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1022(tmp, true, NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t1111 * tmp;
		tmp = (ExtensionAttribute_t1111 *)il2cpp_codegen_object_new (ExtensionAttribute_t1111_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m5256(tmp, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1110 * tmp;
		tmp = (InternalsVisibleToAttribute_t1110 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1110_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m5255(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AssetBundleCreateRequest_t774_CustomAttributesCacheGenerator_AssetBundleCreateRequest_get_assetBundle_m3644(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AssetBundleCreateRequest_t774_CustomAttributesCacheGenerator_AssetBundleCreateRequest_DisableCompatibilityChecks_m3645(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
extern TypeInfo* TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var;
void AssetBundle_t776_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_m3649(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1860);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1015 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1015 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m5084(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AssetBundle_t776_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_Internal_m3650(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1860);
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1015 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1015 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m5084(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AssetBundle_t776_CustomAttributesCacheGenerator_AssetBundle_LoadAssetWithSubAssets_Internal_m3651(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void LayerMask_t11_CustomAttributesCacheGenerator_LayerMask_LayerToName_m3653(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void LayerMask_t11_CustomAttributesCacheGenerator_LayerMask_NameToLayer_m3654(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void LayerMask_t11_CustomAttributesCacheGenerator_LayerMask_t11_LayerMask_GetMask_m3655_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void RuntimePlatform_t780_CustomAttributesCacheGenerator_NaCl(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("NaCl export is no longer supported in Unity 5.0+."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void RuntimePlatform_t780_CustomAttributesCacheGenerator_FlashPlayer(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("FlashPlayer export is no longer supported in Unity 5.0+."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void RuntimePlatform_t780_CustomAttributesCacheGenerator_MetroPlayerX86(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerX86 instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void RuntimePlatform_t780_CustomAttributesCacheGenerator_MetroPlayerX64(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerX64 instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void RuntimePlatform_t780_CustomAttributesCacheGenerator_MetroPlayerARM(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerARM instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_operatingSystem_m1659(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_systemMemorySize_m1617(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_graphicsMemorySize_m1614(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_graphicsDeviceName_m1657(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_graphicsShaderLevel_m862(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_supportsRenderTextures_m854(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_supportsImageEffects_m765(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_supports3DTextures_m811(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_supportsComputeShaders_m863(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_SupportsRenderTextureFormat_m781(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_deviceUniqueIdentifier_m3656(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_deviceName_m1613(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_deviceModel_m1611(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_deviceType_m1612(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_maxTextureSize_m1616(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Coroutine_t559_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m3658(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ScriptableObject_t784_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m3661(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
extern TypeInfo* WritableAttribute_t954_il2cpp_TypeInfo_var;
void ScriptableObject_t784_CustomAttributesCacheGenerator_ScriptableObject_t784_ScriptableObject_Internal_CreateScriptableObject_m3661_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t954_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1861);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t954 * tmp;
		tmp = (WritableAttribute_t954 *)il2cpp_codegen_object_new (WritableAttribute_t954_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m4882(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ScriptableObject_t784_CustomAttributesCacheGenerator_ScriptableObject_CreateInstance_m3662(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ScriptableObject_t784_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m3664(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Cursor_t786_CustomAttributesCacheGenerator_Cursor_set_visible_m660(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Cursor_t786_CustomAttributesCacheGenerator_Cursor_set_lockState_m659(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m3669(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m3670(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m3671(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m3672(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m3673(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m3674(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m3675(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m3676(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m3677(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m3678(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m3679(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m3680(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m3681(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m3682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m3683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m3684(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m3689(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GcLeaderboard_t798_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m3733(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GcLeaderboard_t798_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScoresWithUsers_m3734(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GcLeaderboard_t798_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m3735(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GcLeaderboard_t798_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m3736(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderSettings_t799_CustomAttributesCacheGenerator_RenderSettings_get_fogMode_m843(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderSettings_t799_CustomAttributesCacheGenerator_RenderSettings_get_fogDensity_m844(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderSettings_t799_CustomAttributesCacheGenerator_RenderSettings_get_fogStartDistance_m845(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderSettings_t799_CustomAttributesCacheGenerator_RenderSettings_get_fogEndDistance_m846(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void QualitySettings_t800_CustomAttributesCacheGenerator_QualitySettings_set_shadowDistance_m992(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void QualitySettings_t800_CustomAttributesCacheGenerator_QualitySettings_get_activeColorSpace_m818(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Mesh_t216_CustomAttributesCacheGenerator_Mesh_Internal_Create_m3737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t954_il2cpp_TypeInfo_var;
void Mesh_t216_CustomAttributesCacheGenerator_Mesh_t216_Mesh_Internal_Create_m3737_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t954_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1861);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t954 * tmp;
		tmp = (WritableAttribute_t954 *)il2cpp_codegen_object_new (WritableAttribute_t954_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m4882(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Mesh_t216_CustomAttributesCacheGenerator_Mesh_set_vertices_m869(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Mesh_t216_CustomAttributesCacheGenerator_Mesh_set_uv_m871(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Mesh_t216_CustomAttributesCacheGenerator_Mesh_set_uv2_m872(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Mesh_t216_CustomAttributesCacheGenerator_Mesh_set_triangles_m870(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_enabled_m926(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Renderer_t154_CustomAttributesCacheGenerator_Renderer_set_enabled_m927(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_materials_m957(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Renderer_t154_CustomAttributesCacheGenerator_Renderer_set_materials_m960(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_sharedMaterials_m956(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_bounds_m701(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_sortingLayerID_m3143(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_sortingOrder_m3144(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Projector_t395_CustomAttributesCacheGenerator_Projector_get_fieldOfView_m1822(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Projector_t395_CustomAttributesCacheGenerator_Projector_set_fieldOfView_m1821(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void LineRenderer_t397_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetWidth_m3758(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void LineRenderer_t397_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetVertexCount_m3759(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void LineRenderer_t397_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetPosition_m3760(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_INTERNAL_CALL_Internal_DrawMeshNow2_m3762(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_DrawProceduralIndirect_m832(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_t802_Graphics_DrawProceduralIndirect_m832_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_Blit_m737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_Blit_m739(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_t802_Graphics_Blit_m742_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("-1"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_Internal_BlitMaterial_m3763(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_t802_Graphics_BlitMultiTap_m768_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_Internal_BlitMultiTap_m3764(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_Internal_SetNullRT_m3765(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_Internal_SetRTSimple_m3766(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_ClearRandomWriteTargets_m827(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Graphics_t802_CustomAttributesCacheGenerator_Graphics_Internal_SetRandomWriteTargetBuffer_m3767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Screen_t803_CustomAttributesCacheGenerator_Screen_get_width_m723(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Screen_t803_CustomAttributesCacheGenerator_Screen_get_height_m859(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Screen_t803_CustomAttributesCacheGenerator_Screen_get_dpi_m3423(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_Vertex3_m850(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_TexCoord2_m864(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_MultiTexCoord2_m849(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_Begin_m848(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_End_m851(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_LoadOrtho_m847(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_LoadProjectionMatrix_m3770(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_LoadIdentity_m837(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_PushMatrix_m836(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_PopMatrix_m839(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_GetGPUProjectionMatrix_m3771(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_Clear_m755(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_t804_GL_Clear_m3772_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("1.0f"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_Internal_Clear_m3774(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GL_t804_CustomAttributesCacheGenerator_GL_ClearWithSkybox_m876(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture_t86_CustomAttributesCacheGenerator_Texture_Internal_GetWidth_m3776(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture_t86_CustomAttributesCacheGenerator_Texture_Internal_GetHeight_m3777(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture_t86_CustomAttributesCacheGenerator_Texture_set_filterMode_m762(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture_t86_CustomAttributesCacheGenerator_Texture_set_anisoLevel_m740(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture_t86_CustomAttributesCacheGenerator_Texture_set_wrapMode_m784(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture_t86_CustomAttributesCacheGenerator_Texture_get_texelSize_m852(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m3783(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t954_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_t63_Texture2D_Internal_Create_m3783_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t954_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1861);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t954 * tmp;
		tmp = (WritableAttribute_t954 *)il2cpp_codegen_object_new (WritableAttribute_t954_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m4882(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_get_whiteTexture_m3181(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_INTERNAL_CALL_SetPixel_m3784(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_GetPixelBilinear_m3255(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_GetPixels_m817(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_t63_Texture2D_GetPixels_m3785_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_GetPixels_m3786(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_t63_Texture2D_GetPixels_m3786_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_Apply_m3787(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_t63_Texture2D_Apply_m3787_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_t63_Texture2D_Apply_m3787_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_Apply_m809(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_SetPixels_m3788(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_t84_Texture3D_SetPixels_m3788_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_SetPixels_m813(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_Apply_m3789(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_t84_Texture3D_Apply_m3789_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_Apply_m814(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_Internal_Create_m3790(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t954_il2cpp_TypeInfo_var;
void Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_t84_Texture3D_Internal_Create_m3790_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t954_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1861);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t954 * tmp;
		tmp = (WritableAttribute_t954 *)il2cpp_codegen_object_new (WritableAttribute_t954_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m4882(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_CreateRenderTexture_m3791(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t954_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_t101_RenderTexture_Internal_CreateRenderTexture_m3791_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t954_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1861);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t954 * tmp;
		tmp = (WritableAttribute_t954 *)il2cpp_codegen_object_new (WritableAttribute_t954_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m4882(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m3792(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_t101_RenderTexture_GetTemporary_m3792_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_t101_RenderTexture_GetTemporary_m3792_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("RenderTextureFormat.Default"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_t101_RenderTexture_GetTemporary_m3792_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("RenderTextureReadWrite.Default"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_t101_RenderTexture_GetTemporary_m3792_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("1"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m749(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m769(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m741(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_ReleaseTemporary_m743(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_GetWidth_m3793(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_SetWidth_m3794(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_GetHeight_m3795(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_SetHeight_m3796(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_SetSRGBReadWrite_m3797(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_set_depth_m3802(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_get_format_m747(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_set_format_m3803(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_INTERNAL_CALL_DiscardContents_m3804(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m3805(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetColorBuffer_m3808(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetDepthBuffer_m3809(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_set_active_m835(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUILayer_t808_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m3811(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Gradient_t811_CustomAttributesCacheGenerator_Gradient_Init_m3815(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Gradient_t811_CustomAttributesCacheGenerator_Gradient_Cleanup_m3816(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void GUI_t408_CustomAttributesCacheGenerator_U3CnextScrollStepTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void GUI_t408_CustomAttributesCacheGenerator_U3CscrollTroughSideU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void GUI_t408_CustomAttributesCacheGenerator_GUI_get_nextScrollStepTime_m3824(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void GUI_t408_CustomAttributesCacheGenerator_GUI_set_nextScrollStepTime_m3825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void GUI_t408_CustomAttributesCacheGenerator_GUI_get_scrollTroughSide_m3826(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void GUI_t408_CustomAttributesCacheGenerator_GUI_set_scrollTroughSide_m3827(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUI_t408_CustomAttributesCacheGenerator_GUI_set_changed_m3829(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUI_t408_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoLabel_m3832(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUI_t408_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoButton_m3835(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUI_t408_CustomAttributesCacheGenerator_GUI_get_usePageScrollbars_m3842(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUI_t408_CustomAttributesCacheGenerator_GUI_InternalRepaintEditorWindow_m3844(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_Label_m1680_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_Label_m1654_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_Box_m1653_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_Button_m1677_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_Button_m1662_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_HorizontalSlider_m1661_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginHorizontal_m1650_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginHorizontal_m1675_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginHorizontal_m3857_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginVertical_m1672_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginVertical_m3858_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginScrollView_m1648_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginScrollView_m3860_Arg6_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUILayoutUtility_t411_CustomAttributesCacheGenerator_GUILayoutUtility_Internal_GetWindowRect_m3874(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUILayoutUtility_t411_CustomAttributesCacheGenerator_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m3876(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttribute.h"
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttributeMethodDeclarations.h"
extern TypeInfo* SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var;
void GUILayoutUtility_t411_CustomAttributesCacheGenerator_GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m3877(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1863);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1112 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1112 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m5258(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayoutUtility_t411_CustomAttributesCacheGenerator_GUILayoutUtility_t411_GUILayoutUtility_GetRect_m3881_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GUILayoutUtility_t411_CustomAttributesCacheGenerator_GUILayoutUtility_t411_GUILayoutUtility_GetRect_m3883_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_GetControlID_m3922(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m3926(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_Internal_GetHotControl_m3930(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_Internal_SetHotControl_m3931(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_get_keyboardControl_m3932(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_set_keyboardControl_m3933(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_get_systemCopyBuffer_m3934(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_set_systemCopyBuffer_m3935(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_Internal_GetDefaultSkin_m3937(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_Internal_ExitGUI_m3939(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_Internal_GetGUIDepth_m3943(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_set_mouseUsed_m3944(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIClip_t827_CustomAttributesCacheGenerator_GUIClip_INTERNAL_CALL_Push_m3946(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIClip_t827_CustomAttributesCacheGenerator_GUIClip_Pop_m3947(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISettings_t828_CustomAttributesCacheGenerator_m_DoubleClickSelectsWord(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISettings_t828_CustomAttributesCacheGenerator_m_TripleClickSelectsLine(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISettings_t828_CustomAttributesCacheGenerator_m_CursorColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISettings_t828_CustomAttributesCacheGenerator_m_CursorFlashSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISettings_t828_CustomAttributesCacheGenerator_m_SelectionColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
extern TypeInfo* ExecuteInEditMode_t262_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t262 * tmp;
		tmp = (ExecuteInEditMode_t262 *)il2cpp_codegen_object_new (ExecuteInEditMode_t262_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m1027(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_Font(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_box(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_button(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_toggle(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_label(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_textField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_textArea(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_window(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalSlider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalSliderThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_verticalSlider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_verticalSliderThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalScrollbarThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalScrollbarLeftButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalScrollbarRightButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_verticalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_verticalScrollbarThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_verticalScrollbarUpButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_verticalScrollbarDownButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_ScrollView(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_CustomStyles(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUISkin_t287_CustomAttributesCacheGenerator_m_Settings(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUIContent_t301_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUIContent_t301_CustomAttributesCacheGenerator_m_Image(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void GUIContent_t301_CustomAttributesCacheGenerator_m_Tooltip(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyleState_t405_CustomAttributesCacheGenerator_GUIStyleState_Init_m4010(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyleState_t405_CustomAttributesCacheGenerator_GUIStyleState_Cleanup_m4011(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyleState_t405_CustomAttributesCacheGenerator_GUIStyleState_SetBackgroundInternal_m4012(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyleState_t405_CustomAttributesCacheGenerator_GUIStyleState_GetBackgroundInternal_m4013(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyleState_t405_CustomAttributesCacheGenerator_GUIStyleState_INTERNAL_set_textColor_m4014(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_Init_m4017(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_Cleanup_m4018(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_left_m3438(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_set_left_m4019(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_right_m4020(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_set_right_m4021(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_top_m3439(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_set_top_m4022(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_bottom_m4023(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_set_bottom_m4024(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_horizontal_m3432(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_vertical_m3433(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_INTERNAL_CALL_Remove_m4026(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Init_m4030(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Cleanup_m4031(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_name_m4032(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_name_m4033(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_GetStyleStatePtr_m4034(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_GetRectOffsetPtr_m4037(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_AssignRectOffset_m4038(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_imagePosition_m4039(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_imagePosition_m1630(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_alignment_m1625(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_wordWrap_m4040(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_wordWrap_m1633(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_clipping_m1619(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_fixedWidth_m4041(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_fixedWidth_m1634(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_fixedHeight_m4042(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_fixedHeight_m1629(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_stretchWidth_m4043(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_stretchWidth_m4044(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_stretchHeight_m4045(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_stretchHeight_m4046(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Internal_GetLineHeight_m4047(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_fontSize_m1624(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_fontStyle_m1626(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Internal_Draw_m4050(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Draw_m4052(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_t302_GUIStyle_Draw_m4053_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_Draw2_m4055(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_SetDefaultFont_m4056(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m4060(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcSize_m4061(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcHeight_m4063(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcMinMaxWidth_m4066(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_Destroy_m4069(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m4071(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m3342(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m3343(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("TouchScreenKeyboardType.Default"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg6_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("\"\""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_text_m3280(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_text_m3281(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_hideInput_m3341(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_active_m3279(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_active_m3340(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_done_m3294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_wasCanceled_m3291(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_Init_m4073(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_Cleanup_m4075(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_get_rawType_m3306(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_get_type_m4076(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_GetTypeForControl_m4077(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_Internal_GetMousePosition_m4079(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_Internal_GetMouseDelta_m4081(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_get_modifiers_m3302(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_get_character_m3304(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_get_commandName_m4082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_get_keyCode_m3303(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_Internal_SetNativeEvent_m4084(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_Use_m4086(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Event_t560_CustomAttributesCacheGenerator_Event_PopEvent_m3307(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void EventModifiers_t838_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Gizmos_t839_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawLine_m4092(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Gizmos_t839_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawWireSphere_m4093(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Gizmos_t839_CustomAttributesCacheGenerator_Gizmos_INTERNAL_set_color_m4094(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void Vector2_t6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void Vector3_t4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Vector3_t4_CustomAttributesCacheGenerator_Vector3_INTERNAL_CALL_Slerp_m4100(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Vector3_t4_CustomAttributesCacheGenerator_Vector3_SmoothDamp_m606(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Vector3_t4_CustomAttributesCacheGenerator_Vector3_t4_Vector3_SmoothDamp_m4101_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Vector3_t4_CustomAttributesCacheGenerator_Vector3_t4_Vector3_SmoothDamp_m4101_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Time.deltaTime"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void Color_t65_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
extern TypeInfo* IL2CPPStructAlignmentAttribute_t948_il2cpp_TypeInfo_var;
void Color32_t654_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IL2CPPStructAlignmentAttribute_t948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1864);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		IL2CPPStructAlignmentAttribute_t948 * tmp;
		tmp = (IL2CPPStructAlignmentAttribute_t948 *)il2cpp_codegen_object_new (IL2CPPStructAlignmentAttribute_t948_il2cpp_TypeInfo_var);
		IL2CPPStructAlignmentAttribute__ctor_m4874(tmp, NULL);
		tmp->___Align_0 = 4;
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void Quaternion_t19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_AngleAxis_m4117(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_FromToRotation_m4119(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_t19_Quaternion_LookRotation_m654_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Vector3.up"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_LookRotation_m917(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_LookRotation_m4120(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Slerp_m4121(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Lerp_m4122(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Inverse_m4123(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m4127(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m4129(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void Matrix4x4_t80_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Inverse_m4143(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Transpose_m4145(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Invert_m4147(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_get_isIdentity_m4149(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_TRS_m4157(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_Ortho_m4160(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_Perspective_m4161(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Bounds_t225_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_Contains_m4175(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Bounds_t225_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_SqrDistance_m4178(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Bounds_t225_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_IntersectRay_m4181(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Bounds_t225_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m4185(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void Vector4_t236_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void Mathf_t218_CustomAttributesCacheGenerator_Mathf_t218_Mathf_Max_m704_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Mathf_t218_CustomAttributesCacheGenerator_Mathf_SmoothDamp_m649(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Mathf_t218_CustomAttributesCacheGenerator_Mathf_t218_Mathf_SmoothDamp_m3362_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Mathf_t218_CustomAttributesCacheGenerator_Mathf_t218_Mathf_SmoothDamp_m3362_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Time.deltaTime"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Mathf_t218_CustomAttributesCacheGenerator_Mathf_PerlinNoise_m669(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void DrivenTransformProperties_t841_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_rect_m4216(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMin_m4217(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMin_m4218(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMax_m4219(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMax_m4220(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchoredPosition_m4221(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchoredPosition_m4222(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_sizeDelta_m4223(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_sizeDelta_m4224(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_pivot_m4225(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_pivot_m4226(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Resources_t845_CustomAttributesCacheGenerator_Resources_Load_m4232(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1860);
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1015 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1015 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m5084(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void SerializePrivateVariables_t846_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("Use SerializeField on the private variables that you want to be serialized instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Shader_t54_CustomAttributesCacheGenerator_Shader_get_isSupported_m736(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Shader_t54_CustomAttributesCacheGenerator_Shader_PropertyToID_m4234(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_get_shader_m767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_set_shader_m958(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetColor_m4236(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_SetTexture_m4237(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_GetTexture_m4239(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetMatrix_m4241(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_SetFloat_m4242(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_SetBuffer_m829(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_HasProperty_m4243(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_get_passCount_m865(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_SetPass_m831(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_Internal_CreateWithShader_m4244(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t954_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_t55_Material_Internal_CreateWithShader_m4244_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t954_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1861);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t954 * tmp;
		tmp = (WritableAttribute_t954 *)il2cpp_codegen_object_new (WritableAttribute_t954_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m4882(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_Internal_CreateWithMaterial_m4245(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t954_il2cpp_TypeInfo_var;
void Material_t55_CustomAttributesCacheGenerator_Material_t55_Material_Internal_CreateWithMaterial_m4245_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t954_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1861);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t954 * tmp;
		tmp = (WritableAttribute_t954 *)il2cpp_codegen_object_new (WritableAttribute_t954_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m4882(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t847_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t847_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m4248(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t847_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m4251(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t847_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m4254(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Sprite_t329_CustomAttributesCacheGenerator_Sprite_get_rect_m3234(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Sprite_t329_CustomAttributesCacheGenerator_Sprite_get_pixelsPerUnit_m3231(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Sprite_t329_CustomAttributesCacheGenerator_Sprite_get_texture_m3228(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Sprite_t329_CustomAttributesCacheGenerator_Sprite_get_textureRect_m3252(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Sprite_t329_CustomAttributesCacheGenerator_Sprite_get_border_m3229(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void DataUtility_t848_CustomAttributesCacheGenerator_DataUtility_GetInnerUV_m3241(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void DataUtility_t848_CustomAttributesCacheGenerator_DataUtility_GetOuterUV_m3240(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void DataUtility_t848_CustomAttributesCacheGenerator_DataUtility_GetPadding_m3233(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void DataUtility_t848_CustomAttributesCacheGenerator_DataUtility_Internal_GetMinSize_m4264(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void WWW_t294_CustomAttributesCacheGenerator_WWW_DestroyWWW_m4268(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void WWW_t294_CustomAttributesCacheGenerator_WWW_InitWWW_m4269(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void WWW_t294_CustomAttributesCacheGenerator_WWW_get_responseHeadersString_m4271(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void WWW_t294_CustomAttributesCacheGenerator_WWW_get_bytes_m4274(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void WWW_t294_CustomAttributesCacheGenerator_WWW_get_error_m1586(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void WWW_t294_CustomAttributesCacheGenerator_WWW_get_isDone_m4275(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void WWWForm_t851_CustomAttributesCacheGenerator_WWWForm_AddField_m4279(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void WWWForm_t851_CustomAttributesCacheGenerator_WWWForm_t851_WWWForm_AddField_m4280_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("System.Text.Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void WWWTranscoder_t852_CustomAttributesCacheGenerator_WWWTranscoder_t852_WWWTranscoder_QPEncode_m4287_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void WWWTranscoder_t852_CustomAttributesCacheGenerator_WWWTranscoder_t852_WWWTranscoder_SevenBitClean_m4290_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void CacheIndex_t853_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("this API is not for public use."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void UnityString_t854_CustomAttributesCacheGenerator_UnityString_t854_UnityString_Format_m4292_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AsyncOperation_t775_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m4294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_loadedLevel_m1012(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_loadedLevelName_m637(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_LoadLevelAsync_m4300(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_isLoadingLevel_m1800(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_levelCount_m1601(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_isPlaying_m645(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_isEditor_m1759(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_platform_m1583(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_set_runInBackground_m1705(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_dataPath_m1581(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_streamingAssetsPath_m1580(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_persistentDataPath_m1806(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_unityVersion_m1660(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_OpenURL_m1722(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_get_systemLanguage_m1796(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_SetLogCallbackDefined_m4302(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Security.SecurityCriticalAttribute
#include "mscorlib_System_Security_SecurityCriticalAttribute.h"
// System.Security.SecurityCriticalAttribute
#include "mscorlib_System_Security_SecurityCriticalAttributeMethodDeclarations.h"
extern TypeInfo* SecurityCriticalAttribute_t1113_il2cpp_TypeInfo_var;
void Application_t855_CustomAttributesCacheGenerator_Application_t855____persistentDataPath_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityCriticalAttribute_t1113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1865);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecurityCriticalAttribute_t1113 * tmp;
		tmp = (SecurityCriticalAttribute_t1113 *)il2cpp_codegen_object_new (SecurityCriticalAttribute_t1113_il2cpp_TypeInfo_var);
		SecurityCriticalAttribute__ctor_m5262(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Behaviour_t250_CustomAttributesCacheGenerator_Behaviour_get_enabled_m911(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Behaviour_t250_CustomAttributesCacheGenerator_Behaviour_set_enabled_m766(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Behaviour_t250_CustomAttributesCacheGenerator_Behaviour_get_isActiveAndEnabled_m1740(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_fieldOfView_m699(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_set_fieldOfView_m700(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m823(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_hdr_m748(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_depth_m3059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_aspect_m840(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_cullingMask_m3146(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_set_cullingMask_m796(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_eventMask_m4308(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m4309(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_targetTexture_m4311(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_set_targetTexture_m795(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_get_worldToCameraMatrix_m4312(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_get_projectionMatrix_m4313(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_clearFlags_m4314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_set_clearFlags_m802(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_WorldToViewportPoint_m4315(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToWorldPoint_m4316(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToViewportPoint_m4317(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m4318(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_main_m989(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m4319(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_GetAllCameras_m4320(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_RenderWithShader_m797(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_CopyFrom_m801(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_get_depthTextureMode_m779(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_set_depthTextureMode_m780(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m4325(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m4327(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttribute.h"
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttributeMethodDeclarations.h"
extern TypeInfo* EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var;
void CameraCallback_t856_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1866);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1114 * tmp;
		tmp = (EditorBrowsableAttribute_t1114 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m5263(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void ComputeBufferType_t857_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ComputeBuffer_t95_CustomAttributesCacheGenerator_ComputeBuffer_InitBuffer_m4331(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ComputeBuffer_t95_CustomAttributesCacheGenerator_ComputeBuffer_DestroyBuffer_m4332(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var;
void ComputeBuffer_t95_CustomAttributesCacheGenerator_ComputeBuffer_SetData_m822(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1863);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1112 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1112 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m5258(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
extern TypeInfo* SecurityCriticalAttribute_t1113_il2cpp_TypeInfo_var;
void ComputeBuffer_t95_CustomAttributesCacheGenerator_ComputeBuffer_InternalSetData_m4333(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		SecurityCriticalAttribute_t1113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1865);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SecurityCriticalAttribute_t1113 * tmp;
		tmp = (SecurityCriticalAttribute_t1113 *)il2cpp_codegen_object_new (SecurityCriticalAttribute_t1113_il2cpp_TypeInfo_var);
		SecurityCriticalAttribute__ctor_m5262(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ComputeBuffer_t95_CustomAttributesCacheGenerator_ComputeBuffer_CopyCount_m828(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawLine_m4334_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Color.white"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawLine_m4334_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0.0f"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawLine_m4334_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_INTERNAL_CALL_DrawLine_m4335(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_DrawRay_m695(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawRay_m4336_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Color.white"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawRay_m4336_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0.0f"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawRay_m4336_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_Internal_Log_m4337(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t954_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_Internal_Log_m4337_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t954_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1861);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t954 * tmp;
		tmp = (WritableAttribute_t954 *)il2cpp_codegen_object_new (WritableAttribute_t954_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m4882(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_Internal_LogException_m4338(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t954_il2cpp_TypeInfo_var;
void Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_Internal_LogException_m4338_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t954_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1861);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t954 * tmp;
		tmp = (WritableAttribute_t954 *)il2cpp_codegen_object_new (WritableAttribute_t954_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m4882(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Display_t861_CustomAttributesCacheGenerator_Display_GetSystemExtImpl_m4363(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Display_t861_CustomAttributesCacheGenerator_Display_GetRenderingExtImpl_m4364(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Display_t861_CustomAttributesCacheGenerator_Display_GetRenderingBuffersImpl_m4365(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Display_t861_CustomAttributesCacheGenerator_Display_SetRenderingResolutionImpl_m4366(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Display_t861_CustomAttributesCacheGenerator_Display_ActivateDisplayImpl_m4367(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Display_t861_CustomAttributesCacheGenerator_Display_SetParamsImpl_m4368(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Display_t861_CustomAttributesCacheGenerator_Display_MultiDisplayLicenseImpl_m4369(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Display_t861_CustomAttributesCacheGenerator_Display_RelativeMouseAtImpl_m4370(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_Invoke_m1015(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_CancelInvoke_m1758(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_IsInvoking_m1763(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_Auto_m4371(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_m988(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_t3_MonoBehaviour_StartCoroutine_m988_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("null"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_m1641(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_m1768(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4373(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_Auto_m4374(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StopAllCoroutines_m1773(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_GetKeyInt_m4376(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_GetKeyDownInt_m4377(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_GetAxis_m715(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_GetAxisRaw_m714(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_GetButton_m716(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_GetButtonDown_m717(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_GetButtonUp_m718(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_GetMouseButton_m924(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m977(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_GetMouseButtonUp_m663(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_get_mousePosition_m720(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_get_mouseScrollDelta_m3104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_get_mousePresent_m3130(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_get_acceleration_m721(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_GetTouch_m3135(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_get_touchCount_m727(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_set_imeCompositionMode_m3344(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_get_compositionString_m3282(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Input_t220_CustomAttributesCacheGenerator_Input_INTERNAL_set_compositionCursorPos_m4378(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void HideFlags_t864_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_Internal_CloneSingle_m4380(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_INTERNAL_CALL_Internal_InstantiateSingle_m4382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_Destroy_m1014(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_t164_Object_Destroy_m1014_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0.0F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_Destroy_m1008(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_DestroyImmediate_m1605(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_t164_Object_DestroyImmediate_m1605_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_DestroyImmediate_m761(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_FindObjectsOfType_m706(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1860);
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1015 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1015 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m5084(tmp, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_get_name_m798(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_set_name_m1795(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_DontDestroyOnLoad_m1602(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_set_hideFlags_m764(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_DestroyObject_m4383(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_t164_Object_DestroyObject_m4383_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0.0F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_DestroyObject_m948(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_ToString_m1050(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_Instantiate_m899(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1860);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1015 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1015 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m5084(tmp, 3, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var;
void Object_t164_CustomAttributesCacheGenerator_Object_FindObjectOfType_m1706(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1860);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1015 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1015 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m5084(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_get_transform_m594(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_get_gameObject_m622(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_GetComponent_m3445(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1860);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1015 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1015 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m5084(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m4390(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_GetComponent_m5267(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1863);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1112 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1112 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m5258(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_GetComponentInChildren_m4391(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1860);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1015 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1015 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m5084(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_GetComponentsForListInternal_m4392(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_CompareTag_m688(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_SendMessage_m4393(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_t219_Component_SendMessage_m4393_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("null"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_t219_Component_SendMessage_m4393_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("SendMessageOptions.RequireReceiver"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_SendMessage_m918(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_BroadcastMessage_m4394(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_t219_Component_BroadcastMessage_m4394_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("null"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Component_t219_CustomAttributesCacheGenerator_Component_t219_Component_BroadcastMessage_m4394_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("SendMessageOptions.RequireReceiver"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Light_t152_CustomAttributesCacheGenerator_Light_set_intensity_m923(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Light_t152_CustomAttributesCacheGenerator_Light_get_shadowStrength_m990(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Light_t152_CustomAttributesCacheGenerator_Light_set_shadowStrength_m994(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Light_t152_CustomAttributesCacheGenerator_Light_set_shadowBias_m993(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject__ctor_m800_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
extern TypeInfo* TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_GetComponent_m4395(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1860);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeInferenceRuleAttribute_t1015 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1015 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m5084(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_GetComponentFastPath_m4396(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_GetComponent_m5274(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1863);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1112 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1112 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m5258(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_GetComponentInChildren_m4397(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1860);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1015 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1015 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m5084(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_GetComponentsInternal_m4398(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_get_transform_m609(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_get_layer_m3321(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_set_layer_m3322(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_SetActive_m713(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_get_activeSelf_m641(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_get_activeInHierarchy_m778(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_get_tag_m4399(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_FindGameObjectWithTag_m608(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_FindGameObjectsWithTag_m1769(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_SendMessage_m4400(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject_SendMessage_m4400_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("null"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject_SendMessage_m4400_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("SendMessageOptions.RequireReceiver"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_SendMessage_m1698(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_SendMessage_m1599(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_BroadcastMessage_m4401(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject_BroadcastMessage_m4401_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("null"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject_BroadcastMessage_m4401_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("SendMessageOptions.RequireReceiver"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_BroadcastMessage_m947(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_Internal_AddComponentWithType_m4402(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_AddComponent_m4403(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1860);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1015 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1015 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1015_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m5084(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m4404(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t954_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject_Internal_CreateGameObject_m4404_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t954_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1861);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t954 * tmp;
		tmp = (WritableAttribute_t954 *)il2cpp_codegen_object_new (WritableAttribute_t954_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m4882(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GameObject_t78_CustomAttributesCacheGenerator_GameObject_Find_m799(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_position_m4408(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_set_position_m4409(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localPosition_m4410(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localPosition_m4411(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_rotation_m4412(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_set_rotation_m4413(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localRotation_m4414(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localRotation_m4415(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localScale_m4416(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localScale_m4417(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_get_parentInternal_m4418(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_set_parentInternal_m4419(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_SetParent_m4420(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_worldToLocalMatrix_m4421(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_Translate_m1760(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_Translate_m962_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Space.Self"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_Translate_m1761(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_Translate_m4422_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Space.Self"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_Rotate_m963_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Space.Self"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_Rotate_m670(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_Rotate_m4423_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Space.Self"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_LookAt_m1011(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_LookAt_m4424_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Vector3.up"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_LookAt_m4425_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Vector3.up"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_LookAt_m965(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_LookAt_m4426(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformDirection_m4427(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformPoint_m4428(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_InverseTransformPoint_m4429(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_get_childCount_m3446(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_DetachChildren_m1016(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_SetAsFirstSibling_m3320(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_Find_m618(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Transform_t1_CustomAttributesCacheGenerator_Transform_GetChild_m1774(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Time_t866_CustomAttributesCacheGenerator_Time_get_time_m668(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Time_t866_CustomAttributesCacheGenerator_Time_get_deltaTime_m602(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Time_t866_CustomAttributesCacheGenerator_Time_get_unscaledTime_m3132(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Time_t866_CustomAttributesCacheGenerator_Time_get_unscaledDeltaTime_m3154(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Time_t866_CustomAttributesCacheGenerator_Time_get_fixedDeltaTime_m1737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Time_t866_CustomAttributesCacheGenerator_Time_get_timeScale_m664(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Time_t866_CustomAttributesCacheGenerator_Time_set_timeScale_m1716(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Time_t866_CustomAttributesCacheGenerator_Time_get_frameCount_m708(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Time_t866_CustomAttributesCacheGenerator_Time_get_realtimeSinceStartup_m961(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Random_t867_CustomAttributesCacheGenerator_Random_Range_m856(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Random_t867_CustomAttributesCacheGenerator_Random_RandomRangeInt_m4431(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Random_t867_CustomAttributesCacheGenerator_Random_get_value_m858(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Random_t867_CustomAttributesCacheGenerator_Random_get_insideUnitSphere_m897(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Random_t867_CustomAttributesCacheGenerator_Random_get_onUnitSphere_m903(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Random_t867_CustomAttributesCacheGenerator_Random_get_rotation_m898(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_TrySetInt_m4434(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_TrySetFloat_m4435(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_TrySetSetString_m4436(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_GetInt_m1607(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_t869_PlayerPrefs_GetInt_m1607_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_GetInt_m1608(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_GetFloat_m1610(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_t869_PlayerPrefs_GetFloat_m1610_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0.0F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m4437(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_t869_PlayerPrefs_GetString_m4437_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("\"\""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m1609(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_Save_m1702(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_get_isStopped_m1829(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_set_enableEmission_m921(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_get_startSpeed_m931(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_set_startSpeed_m928(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_get_startSize_m929(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_set_startSize_m930(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_get_startLifetime_m932(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_set_startLifetime_m933(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Internal_Play_m4438(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Internal_Stop_m4439(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Internal_Clear_m4440(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Play_m935(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_t161_ParticleSystem_Play_m4441_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Stop_m1825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_t161_ParticleSystem_Stop_m4442_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Clear_m934(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_t161_ParticleSystem_Clear_m4443_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_INTERNAL_CALL_Emit_m4444(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleCollisionEvent_t160_CustomAttributesCacheGenerator_ParticleCollisionEvent_InstanceIDToCollider_m4447(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystemExtensionsImpl_t870_CustomAttributesCacheGenerator_ParticleSystemExtensionsImpl_GetSafeCollisionEventSize_m4448(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void ParticleSystemExtensionsImpl_t870_CustomAttributesCacheGenerator_ParticleSystemExtensionsImpl_GetCollisionEvents_m4449(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t1111_il2cpp_TypeInfo_var;
void ParticlePhysicsExtensions_t871_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t1111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1858);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t1111 * tmp;
		tmp = (ExtensionAttribute_t1111 *)il2cpp_codegen_object_new (ExtensionAttribute_t1111_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m5256(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t1111_il2cpp_TypeInfo_var;
void ParticlePhysicsExtensions_t871_CustomAttributesCacheGenerator_ParticlePhysicsExtensions_GetSafeCollisionEventSize_m940(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t1111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1858);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t1111 * tmp;
		tmp = (ExtensionAttribute_t1111 *)il2cpp_codegen_object_new (ExtensionAttribute_t1111_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m5256(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t1111_il2cpp_TypeInfo_var;
void ParticlePhysicsExtensions_t871_CustomAttributesCacheGenerator_ParticlePhysicsExtensions_GetCollisionEvents_m941(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t1111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1858);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t1111 * tmp;
		tmp = (ExtensionAttribute_t1111 *)il2cpp_codegen_object_new (ExtensionAttribute_t1111_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m5256(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_Internal_Raycast_m4467(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_Raycast_m964(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_Raycast_m979_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_Raycast_m979_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_Raycast_m904(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_Raycast_m991(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_Raycast_m3209_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_Raycast_m3209_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_RaycastAll_m689(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_RaycastAll_m3147_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_RaycastAll_m3147_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_RaycastAll_m4468_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_RaycastAll_m4468_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_RaycastAll_m4469(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_OverlapSphere_m685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_OverlapSphere_m4470(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_CapsuleCastAll_m4471_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_CapsuleCastAll_m4471_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_CapsuleCastAll_m4472(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_SphereCastAll_m690(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_SphereCastAll_m4473_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_SphereCastAll_m4473_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_get_velocity_m4474(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_set_velocity_m4475(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_set_angularVelocity_m4476(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_get_drag_m967(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_set_drag_m969(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_get_angularDrag_m968(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_set_angularDrag_m970(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_get_mass_m1748(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_get_isKinematic_m981(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_set_isKinematic_m983(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_set_freezeRotation_m1756(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_t14_Rigidbody_AddForce_m944_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("ForceMode.Force"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_AddForce_m1749(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_CALL_AddForce_m4477(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_AddForceAtPosition_m887(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_CALL_AddForceAtPosition_m4478(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_t14_Rigidbody_AddExplosionForce_m909_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0.0F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_t14_Rigidbody_AddExplosionForce_m909_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("ForceMode.Force"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_CALL_AddExplosionForce_m4479(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Joint_t875_CustomAttributesCacheGenerator_Joint_get_connectedBody_m966(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Joint_t875_CustomAttributesCacheGenerator_Joint_set_connectedBody_m973(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Joint_t875_CustomAttributesCacheGenerator_Joint_INTERNAL_set_anchor_m4480(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SpringJoint_t176_CustomAttributesCacheGenerator_SpringJoint_set_spring_m985(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SpringJoint_t176_CustomAttributesCacheGenerator_SpringJoint_set_damper_m986(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SpringJoint_t176_CustomAttributesCacheGenerator_SpringJoint_set_maxDistance_m987(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Collider_t138_CustomAttributesCacheGenerator_Collider_set_enabled_m1747(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Collider_t138_CustomAttributesCacheGenerator_Collider_get_attachedRigidbody_m687(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Collider_t138_CustomAttributesCacheGenerator_Collider_get_isTrigger_m686(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Collider_t138_CustomAttributesCacheGenerator_Collider_INTERNAL_get_bounds_m4481(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Collider_t138_CustomAttributesCacheGenerator_Collider_INTERNAL_CALL_Internal_Raycast_m4483(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SphereCollider_t136_CustomAttributesCacheGenerator_SphereCollider_INTERNAL_get_center_m4484(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SphereCollider_t136_CustomAttributesCacheGenerator_SphereCollider_INTERNAL_set_center_m4485(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SphereCollider_t136_CustomAttributesCacheGenerator_SphereCollider_get_radius_m885(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void SphereCollider_t136_CustomAttributesCacheGenerator_SphereCollider_set_radius_m888(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_Internal_Raycast_m4490(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_Raycast_m3210(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_t222_Physics2D_Raycast_m4491_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_t222_Physics2D_Raycast_m4491_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_t222_Physics2D_Raycast_m4491_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("-Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_t222_Physics2D_Raycast_m4491_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_RaycastAll_m3138(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_RaycastAll_m4492(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_OverlapCircle_m628(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_OverlapCircle_m4493(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_OverlapCircleAll_m621(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_OverlapCircleAll_m4494(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody2D_t10_CustomAttributesCacheGenerator_Rigidbody2D_INTERNAL_get_velocity_m4496(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody2D_t10_CustomAttributesCacheGenerator_Rigidbody2D_INTERNAL_set_velocity_m4497(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Rigidbody2D_t10_CustomAttributesCacheGenerator_Rigidbody2D_AddForce_m632(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Rigidbody2D_t10_CustomAttributesCacheGenerator_Rigidbody2D_INTERNAL_CALL_AddForce_m4498(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Collider2D_t214_CustomAttributesCacheGenerator_Collider2D_get_attachedRigidbody_m4499(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AudioListener_t882_CustomAttributesCacheGenerator_AudioListener_get_volume_m1717(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AudioListener_t882_CustomAttributesCacheGenerator_AudioListener_set_volume_m1718(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_set_clip_m937(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_Play_m4515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_t247_AudioSource_Play_m4515_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_Play_m938(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_Stop_m1813(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_get_isPlaying_m1812(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void AnimationEvent_t886_CustomAttributesCacheGenerator_AnimationEvent_t886____data_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("Use stringParameter instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void AnimationCurve_t82_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_t82_AnimationCurve__ctor_m804_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m4542(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_Evaluate_m806(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_get_length_m878(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_GetKey_Internal_m4544(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_GetKeys_m4545(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_Init_m4546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void Animation_t249_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var;
void Animation_t249_CustomAttributesCacheGenerator_Animation_Play_m949(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1862);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1013 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1013 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1013_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m5083(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1012_il2cpp_TypeInfo_var;
void Animation_t249_CustomAttributesCacheGenerator_Animation_t249_Animation_Play_m4550_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1012_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1855);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1012 * tmp;
		tmp = (DefaultValueAttribute_t1012 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1012_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5079(tmp, il2cpp_codegen_string_new_wrapper("PlayMode.StopSameLayer"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Animation_t249_CustomAttributesCacheGenerator_Animation_PlayDefaultAnimation_m4551(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Animation_t249_CustomAttributesCacheGenerator_Animation_GetStateAtIndex_m4553(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Animation_t249_CustomAttributesCacheGenerator_Animation_GetStateCount_m4554(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void AnimatorStateInfo_t887_CustomAttributesCacheGenerator_AnimatorStateInfo_t887____nameHash_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("Use AnimatorStateInfo.fullPathHash instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Animator_t9_CustomAttributesCacheGenerator_Animator_GetCurrentAnimatorClipInfo_m1819(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Animator_t9_CustomAttributesCacheGenerator_Animator_get_runtimeAnimatorController_m3392(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Animator_t9_CustomAttributesCacheGenerator_Animator_StringToHash_m4573(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Animator_t9_CustomAttributesCacheGenerator_Animator_SetFloatString_m4574(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Animator_t9_CustomAttributesCacheGenerator_Animator_SetBoolString_m4575(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Animator_t9_CustomAttributesCacheGenerator_Animator_GetBoolString_m4576(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Animator_t9_CustomAttributesCacheGenerator_Animator_SetTriggerString_m4577(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Animator_t9_CustomAttributesCacheGenerator_Animator_ResetTriggerString_m4578(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void GUIText_t181_CustomAttributesCacheGenerator_GUIText_set_text_m998(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void CharacterInfo_t896_CustomAttributesCacheGenerator_uv(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.uv is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void CharacterInfo_t896_CustomAttributesCacheGenerator_vert(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.vert is deprecated. Use minX, maxX, minY, maxY instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void CharacterInfo_t896_CustomAttributesCacheGenerator_width(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.width is deprecated. Use advance instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t267_il2cpp_TypeInfo_var;
void CharacterInfo_t896_CustomAttributesCacheGenerator_flipped(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t267 * tmp;
		tmp = (ObsoleteAttribute_t267 *)il2cpp_codegen_object_new (ObsoleteAttribute_t267_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1043(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.flipped is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead, which will be correct regardless of orientation."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Font_t517_CustomAttributesCacheGenerator_Font_get_material_m3401(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Font_t517_CustomAttributesCacheGenerator_Font_HasCharacter_m3305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Font_t517_CustomAttributesCacheGenerator_Font_get_dynamic_m3404(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Font_t517_CustomAttributesCacheGenerator_Font_get_fontSize_m3406(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var;
void FontTextureRebuildCallback_t897_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1866);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1114 * tmp;
		tmp = (EditorBrowsableAttribute_t1114 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m5263(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_Init_m4606(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_Dispose_cpp_m4607(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m4610(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_get_rectExtents_m3319(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_get_vertexCount_m4611(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetVerticesInternal_m4612(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetVerticesArray_m4613(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_get_characterCount_m4614(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetCharactersInternal_m4615(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetCharactersArray_m4616(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_get_lineCount_m3298(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetLinesInternal_m4617(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetLinesArray_m4618(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_get_fontSizeUsedForBestFit_m3332(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_renderMode_m3205(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_isRootCanvas_m3422(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_worldCamera_m3213(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_scaleFactor_m3405(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_set_scaleFactor_m3424(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_referencePixelsPerUnit_m3232(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_set_referencePixelsPerUnit_m3425(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_pixelPerfect_m3191(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_renderOrder_m3207(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_sortingOrder_m3206(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_cachedSortingLayerValue_m3212(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasMaterial_m3178(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void Canvas_t414_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasTextMaterial_m3400(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void CanvasGroup_t672_CustomAttributesCacheGenerator_CanvasGroup_get_interactable_m3387(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void CanvasGroup_t672_CustomAttributesCacheGenerator_CanvasGroup_get_blocksRaycasts_m4630(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void CanvasGroup_t672_CustomAttributesCacheGenerator_CanvasGroup_get_ignoreParentGroups_m3190(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_INTERNAL_CALL_SetColor_m4633(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_GetColor_m3194(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_set_isMask_m3456(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_SetMaterial_m3188(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternal_m4634(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternalArray_m4635(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_Clear_m3182(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_get_absoluteDepth_m3179(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransformUtility_t674_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4637(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransformUtility_t674_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4639(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t947_il2cpp_TypeInfo_var;
void RectTransformUtility_t674_CustomAttributesCacheGenerator_RectTransformUtility_PixelAdjustRect_m3193(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1859);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t947 * tmp;
		tmp = (WrapperlessIcall_t947 *)il2cpp_codegen_object_new (WrapperlessIcall_t947_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m4873(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Request_t901_CustomAttributesCacheGenerator_U3CsourceIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Request_t901_CustomAttributesCacheGenerator_U3CappIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Request_t901_CustomAttributesCacheGenerator_U3CdomainU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Request_t901_CustomAttributesCacheGenerator_Request_get_sourceId_m4644(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Request_t901_CustomAttributesCacheGenerator_Request_get_appId_m4645(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Request_t901_CustomAttributesCacheGenerator_Request_get_domain_m4646(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Response_t903_CustomAttributesCacheGenerator_U3CsuccessU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Response_t903_CustomAttributesCacheGenerator_U3CextendedInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Response_t903_CustomAttributesCacheGenerator_Response_get_success_m4655(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Response_t903_CustomAttributesCacheGenerator_Response_set_success_m4656(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Response_t903_CustomAttributesCacheGenerator_Response_get_extendedInfo_m4657(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Response_t903_CustomAttributesCacheGenerator_Response_set_extendedInfo_m4658(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_U3CsizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_U3CadvertiseU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_get_name_m4663(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_set_name_m4664(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_get_size_m4665(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_set_size_m4666(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_get_advertise_m4667(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_set_advertise_m4668(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_get_password_m4669(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_set_password_m4670(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_get_matchAttributes_m4671(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_address_m4674(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_address_m4675(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_port_m4676(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_port_m4677(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_networkId_m4678(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_networkId_m4679(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_accessTokenString_m4680(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_accessTokenString_m4681(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_nodeId_m4682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_nodeId_m4683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_usingRelay_m4684(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_usingRelay_m4685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchRequest_t908_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchRequest_t908_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchRequest_t908_CustomAttributesCacheGenerator_JoinMatchRequest_get_networkId_m4689(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchRequest_t908_CustomAttributesCacheGenerator_JoinMatchRequest_set_networkId_m4690(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchRequest_t908_CustomAttributesCacheGenerator_JoinMatchRequest_get_password_m4691(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchRequest_t908_CustomAttributesCacheGenerator_JoinMatchRequest_set_password_m4692(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_address_m4695(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_address_m4696(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_port_m4697(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_port_m4698(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_networkId_m4699(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_networkId_m4700(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_accessTokenString_m4701(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_accessTokenString_m4702(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_nodeId_m4703(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_nodeId_m4704(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_usingRelay_m4705(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_usingRelay_m4706(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t910_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t910_CustomAttributesCacheGenerator_DestroyMatchRequest_get_networkId_m4710(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t910_CustomAttributesCacheGenerator_DestroyMatchRequest_set_networkId_m4711(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void DropConnectionRequest_t911_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void DropConnectionRequest_t911_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void DropConnectionRequest_t911_CustomAttributesCacheGenerator_DropConnectionRequest_get_networkId_m4714(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void DropConnectionRequest_t911_CustomAttributesCacheGenerator_DropConnectionRequest_set_networkId_m4715(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void DropConnectionRequest_t911_CustomAttributesCacheGenerator_DropConnectionRequest_get_nodeId_m4716(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void DropConnectionRequest_t911_CustomAttributesCacheGenerator_DropConnectionRequest_set_nodeId_m4717(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_U3CpageSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_U3CpageNumU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_U3CnameFilterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_U3CmatchAttributeFilterLessThanU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_U3CmatchAttributeFilterGreaterThanU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_get_pageSize_m4720(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_set_pageSize_m4721(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_get_pageNum_m4722(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_set_pageNum_m4723(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_get_nameFilter_m4724(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_set_nameFilter_m4725(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterLessThan_m4726(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterGreaterThan_m4727(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_U3CpublicAddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_U3CprivateAddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_nodeId_m4730(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_nodeId_m4731(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_publicAddress_m4732(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_publicAddress_m4733(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_privateAddress_m4734(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_privateAddress_m4735(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_U3CaverageEloScoreU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_U3CmaxSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_U3CcurrentSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_U3CisPrivateU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_U3ChostNodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_U3CdirectConnectInfosU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_networkId_m4739(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_networkId_m4740(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_name_m4741(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_name_m4742(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_averageEloScore_m4743(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_maxSize_m4744(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_maxSize_m4745(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_currentSize_m4746(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_currentSize_m4747(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_isPrivate_m4748(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_isPrivate_m4749(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_matchAttributes_m4750(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_hostNodeId_m4751(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_directConnectInfos_m4752(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_directConnectInfos_m4753(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchResponse_t917_CustomAttributesCacheGenerator_U3CmatchesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchResponse_t917_CustomAttributesCacheGenerator_ListMatchResponse_get_matches_m4757(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void ListMatchResponse_t917_CustomAttributesCacheGenerator_ListMatchResponse_set_matches_m4758(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttribute.h"
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttributeMethodDeclarations.h"
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
extern TypeInfo* DefaultValueAttribute_t1116_il2cpp_TypeInfo_var;
extern TypeInfo* AppID_t918_il2cpp_TypeInfo_var;
void AppID_t918_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1867);
		AppID_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1742);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t1116 * tmp;
		tmp = (DefaultValueAttribute_t1116 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1116_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5284(tmp, Box(AppID_t918_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
extern TypeInfo* DefaultValueAttribute_t1116_il2cpp_TypeInfo_var;
extern TypeInfo* SourceID_t919_il2cpp_TypeInfo_var;
void SourceID_t919_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1867);
		SourceID_t919_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1741);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t1116 * tmp;
		tmp = (DefaultValueAttribute_t1116 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1116_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5284(tmp, Box(SourceID_t919_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
extern TypeInfo* DefaultValueAttribute_t1116_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t920_il2cpp_TypeInfo_var;
void NetworkID_t920_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1867);
		NetworkID_t920_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1746);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t1116 * tmp;
		tmp = (DefaultValueAttribute_t1116 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1116_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5284(tmp, Box(NetworkID_t920_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
extern TypeInfo* DefaultValueAttribute_t1116_il2cpp_TypeInfo_var;
extern TypeInfo* NodeID_t921_il2cpp_TypeInfo_var;
void NodeID_t921_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1867);
		NodeID_t921_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1747);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint16_t _tmp_0 = 0;
		DefaultValueAttribute_t1116 * tmp;
		tmp = (DefaultValueAttribute_t1116 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1116_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m5284(tmp, Box(NodeID_t921_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void NetworkMatch_t927_CustomAttributesCacheGenerator_NetworkMatch_ProcessMatchResponse_m5285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5291(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m5292(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m5294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttribute.h"
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttributeMethodDeclarations.h"
extern TypeInfo* EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var;
extern TypeInfo* GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var;
void JsonArray_t928_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1866);
		GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1868);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1114 * tmp;
		tmp = (EditorBrowsableAttribute_t1114 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m5263(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GeneratedCodeAttribute_t1119 * tmp;
		tmp = (GeneratedCodeAttribute_t1119 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m5295(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
extern TypeInfo* GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var;
extern TypeInfo* EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var;
void JsonObject_t930_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1868);
		EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1866);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GeneratedCodeAttribute_t1119 * tmp;
		tmp = (GeneratedCodeAttribute_t1119 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m5295(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		EditorBrowsableAttribute_t1114 * tmp;
		tmp = (EditorBrowsableAttribute_t1114 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m5263(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var;
void SimpleJson_t933_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1868);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1119 * tmp;
		tmp = (GeneratedCodeAttribute_t1119 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m5295(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttr.h"
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttrMethodDeclarations.h"
extern TypeInfo* SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var;
void SimpleJson_t933_CustomAttributesCacheGenerator_SimpleJson_TryDeserializeObject_m4802(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1869);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1120 * tmp;
		tmp = (SuppressMessageAttribute_t1120 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m5296(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m5297(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var;
void SimpleJson_t933_CustomAttributesCacheGenerator_SimpleJson_NextToken_m4814(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1869);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1120 * tmp;
		tmp = (SuppressMessageAttribute_t1120 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m5296(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Maintainability"), il2cpp_codegen_string_new_wrapper("CA1502:AvoidExcessiveComplexity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var;
void SimpleJson_t933_CustomAttributesCacheGenerator_SimpleJson_t933____PocoJsonSerializerStrategy_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1866);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1114 * tmp;
		tmp = (EditorBrowsableAttribute_t1114 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1114_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m5263(tmp, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var;
void IJsonSerializerStrategy_t931_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1868);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1119 * tmp;
		tmp = (GeneratedCodeAttribute_t1119 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m5295(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var;
void IJsonSerializerStrategy_t931_CustomAttributesCacheGenerator_IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m5298(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1869);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1120 * tmp;
		tmp = (SuppressMessageAttribute_t1120 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m5296(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m5297(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t932_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1868);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1119 * tmp;
		tmp = (GeneratedCodeAttribute_t1119 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m5295(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t932_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m4831(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1869);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1120 * tmp;
		tmp = (SuppressMessageAttribute_t1120 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m5296(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m5297(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t932_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4832(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1869);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1120 * tmp;
		tmp = (SuppressMessageAttribute_t1120 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1120_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m5296(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m5297(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var;
void ReflectionUtils_t946_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1868);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1119 * tmp;
		tmp = (GeneratedCodeAttribute_t1119 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1119_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m5295(tmp, il2cpp_codegen_string_new_wrapper("reflection-utils"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void ReflectionUtils_t946_CustomAttributesCacheGenerator_ReflectionUtils_t946_ReflectionUtils_GetConstructorInfo_m4857_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void ReflectionUtils_t946_CustomAttributesCacheGenerator_ReflectionUtils_t946_ReflectionUtils_GetContructor_m4862_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void ReflectionUtils_t946_CustomAttributesCacheGenerator_ReflectionUtils_t946_ReflectionUtils_GetConstructorByReflection_m4864_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void ThreadSafeDictionary_2_t1121_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void ConstructorDelegate_t939_CustomAttributesCacheGenerator_ConstructorDelegate_t939_ConstructorDelegate_Invoke_m4842_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t725_il2cpp_TypeInfo_var;
void ConstructorDelegate_t939_CustomAttributesCacheGenerator_ConstructorDelegate_t939_ConstructorDelegate_BeginInvoke_m4843_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t725_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1176);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t725 * tmp;
		tmp = (ParamArrayAttribute_t725 *)il2cpp_codegen_object_new (ParamArrayAttribute_t725_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m3532(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void IL2CPPStructAlignmentAttribute_t948_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 8, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void DisallowMultipleComponent_t724_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m5323(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void RequireComponent_t259_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 4, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void WritableAttribute_t954_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 2048, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void AssemblyIsEditorAssembly_t955_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t718_il2cpp_TypeInfo_var;
void DepthTextureMode_t964_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1173);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t718 * tmp;
		tmp = (FlagsAttribute_t718 *)il2cpp_codegen_object_new (FlagsAttribute_t718_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m3476(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var;
void GUIStateObjects_t974_CustomAttributesCacheGenerator_GUIStateObjects_GetStateObject_m4897(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1863);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1112 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1112 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m5258(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Achievement_t977_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Achievement_t977_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Achievement_t977_CustomAttributesCacheGenerator_Achievement_get_id_m4917(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Achievement_t977_CustomAttributesCacheGenerator_Achievement_set_id_m4918(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Achievement_t977_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m4919(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Achievement_t977_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m4920(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void AchievementDescription_t978_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void AchievementDescription_t978_CustomAttributesCacheGenerator_AchievementDescription_get_id_m4927(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void AchievementDescription_t978_CustomAttributesCacheGenerator_AchievementDescription_set_id_m4928(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Score_t979_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Score_t979_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Score_t979_CustomAttributesCacheGenerator_Score_get_leaderboardID_m4937(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Score_t979_CustomAttributesCacheGenerator_Score_set_leaderboardID_m4938(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Score_t979_CustomAttributesCacheGenerator_Score_get_value_m4939(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Score_t979_CustomAttributesCacheGenerator_Score_set_value_m4940(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_get_id_m4948(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_set_id_m4949(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m4950(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m4951(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_get_range_m4952(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_set_range_m4953(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m4954(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m4955(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void PropertyAttribute_t990_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m5323(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void TooltipAttribute_t266_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m5323(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void SpaceAttribute_t726_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m5323(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void RangeAttribute_t261_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m5323(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void TextAreaAttribute_t728_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m5323(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void SelectionBaseAttribute_t727_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m5323(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var;
void StackTraceUtility_t993_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m4995(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1863);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1112 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1112 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m5258(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var;
void StackTraceUtility_t993_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m4998(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1863);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1112 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1112 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m5258(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var;
void StackTraceUtility_t993_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m5000(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1863);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1112 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1112 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1112_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m5258(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void SharedBetweenAnimatorsAttribute_t994_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 4, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ArgumentCache_t1001_CustomAttributesCacheGenerator_m_ObjectArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("objectArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ArgumentCache_t1001_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("objectArgumentAssemblyTypeName"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void ArgumentCache_t1001_CustomAttributesCacheGenerator_m_IntArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("intArgument"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ArgumentCache_t1001_CustomAttributesCacheGenerator_m_FloatArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("floatArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ArgumentCache_t1001_CustomAttributesCacheGenerator_m_StringArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("stringArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void ArgumentCache_t1001_CustomAttributesCacheGenerator_m_BoolArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PersistentCall_t1005_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("instance"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PersistentCall_t1005_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("methodName"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PersistentCall_t1005_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("mode"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PersistentCall_t1005_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("arguments"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void PersistentCall_t1005_CustomAttributesCacheGenerator_m_CallState(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_Enabled"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("enabled"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void PersistentCallGroup_t1007_CustomAttributesCacheGenerator_m_Calls(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_Listeners"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var;
void UnityEventBase_t1010_CustomAttributesCacheGenerator_m_PersistentCalls(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1174);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t719 * tmp;
		tmp = (FormerlySerializedAsAttribute_t719 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t719_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m3494(tmp, il2cpp_codegen_string_new_wrapper("m_PersistentListeners"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t260_il2cpp_TypeInfo_var;
void UnityEventBase_t1010_CustomAttributesCacheGenerator_m_TypeName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t260 * tmp;
		tmp = (SerializeField_t260 *)il2cpp_codegen_object_new (SerializeField_t260_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1024(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern TypeInfo* AddComponentMenu_t264_il2cpp_TypeInfo_var;
void UserAuthorizationDialog_t1011_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t264 * tmp;
		tmp = (AddComponentMenu_t264 *)il2cpp_codegen_object_new (AddComponentMenu_t264_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m1040(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void DefaultValueAttribute_t1012_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 18432, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void ExcludeFromDocsAttribute_t1013_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 64, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void FormerlySerializedAsAttribute_t719_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 256, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m5324(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m5323(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var;
void TypeInferenceRuleAttribute_t1015_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1123 * tmp;
		tmp = (AttributeUsageAttribute_t1123 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1123_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m5322(tmp, 64, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_UnityEngine_Assembly_AttributeGenerators[1062] = 
{
	NULL,
	g_UnityEngine_Assembly_CustomAttributesCacheGenerator,
	AssetBundleCreateRequest_t774_CustomAttributesCacheGenerator_AssetBundleCreateRequest_get_assetBundle_m3644,
	AssetBundleCreateRequest_t774_CustomAttributesCacheGenerator_AssetBundleCreateRequest_DisableCompatibilityChecks_m3645,
	AssetBundle_t776_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_m3649,
	AssetBundle_t776_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_Internal_m3650,
	AssetBundle_t776_CustomAttributesCacheGenerator_AssetBundle_LoadAssetWithSubAssets_Internal_m3651,
	LayerMask_t11_CustomAttributesCacheGenerator_LayerMask_LayerToName_m3653,
	LayerMask_t11_CustomAttributesCacheGenerator_LayerMask_NameToLayer_m3654,
	LayerMask_t11_CustomAttributesCacheGenerator_LayerMask_t11_LayerMask_GetMask_m3655_Arg0_ParameterInfo,
	RuntimePlatform_t780_CustomAttributesCacheGenerator_NaCl,
	RuntimePlatform_t780_CustomAttributesCacheGenerator_FlashPlayer,
	RuntimePlatform_t780_CustomAttributesCacheGenerator_MetroPlayerX86,
	RuntimePlatform_t780_CustomAttributesCacheGenerator_MetroPlayerX64,
	RuntimePlatform_t780_CustomAttributesCacheGenerator_MetroPlayerARM,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_operatingSystem_m1659,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_systemMemorySize_m1617,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_graphicsMemorySize_m1614,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_graphicsDeviceName_m1657,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_graphicsShaderLevel_m862,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_supportsRenderTextures_m854,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_supportsImageEffects_m765,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_supports3DTextures_m811,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_supportsComputeShaders_m863,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_SupportsRenderTextureFormat_m781,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_deviceUniqueIdentifier_m3656,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_deviceName_m1613,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_deviceModel_m1611,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_deviceType_m1612,
	SystemInfo_t782_CustomAttributesCacheGenerator_SystemInfo_get_maxTextureSize_m1616,
	Coroutine_t559_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m3658,
	ScriptableObject_t784_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m3661,
	ScriptableObject_t784_CustomAttributesCacheGenerator_ScriptableObject_t784_ScriptableObject_Internal_CreateScriptableObject_m3661_Arg0_ParameterInfo,
	ScriptableObject_t784_CustomAttributesCacheGenerator_ScriptableObject_CreateInstance_m3662,
	ScriptableObject_t784_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m3664,
	Cursor_t786_CustomAttributesCacheGenerator_Cursor_set_visible_m660,
	Cursor_t786_CustomAttributesCacheGenerator_Cursor_set_lockState_m659,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m3669,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m3670,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m3671,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m3672,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m3673,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m3674,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m3675,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m3676,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m3677,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m3678,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m3679,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m3680,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m3681,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m3682,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m3683,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m3684,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3685,
	GameCenterPlatform_t796_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m3689,
	GcLeaderboard_t798_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m3733,
	GcLeaderboard_t798_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScoresWithUsers_m3734,
	GcLeaderboard_t798_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m3735,
	GcLeaderboard_t798_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m3736,
	RenderSettings_t799_CustomAttributesCacheGenerator_RenderSettings_get_fogMode_m843,
	RenderSettings_t799_CustomAttributesCacheGenerator_RenderSettings_get_fogDensity_m844,
	RenderSettings_t799_CustomAttributesCacheGenerator_RenderSettings_get_fogStartDistance_m845,
	RenderSettings_t799_CustomAttributesCacheGenerator_RenderSettings_get_fogEndDistance_m846,
	QualitySettings_t800_CustomAttributesCacheGenerator_QualitySettings_set_shadowDistance_m992,
	QualitySettings_t800_CustomAttributesCacheGenerator_QualitySettings_get_activeColorSpace_m818,
	Mesh_t216_CustomAttributesCacheGenerator_Mesh_Internal_Create_m3737,
	Mesh_t216_CustomAttributesCacheGenerator_Mesh_t216_Mesh_Internal_Create_m3737_Arg0_ParameterInfo,
	Mesh_t216_CustomAttributesCacheGenerator_Mesh_set_vertices_m869,
	Mesh_t216_CustomAttributesCacheGenerator_Mesh_set_uv_m871,
	Mesh_t216_CustomAttributesCacheGenerator_Mesh_set_uv2_m872,
	Mesh_t216_CustomAttributesCacheGenerator_Mesh_set_triangles_m870,
	Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_enabled_m926,
	Renderer_t154_CustomAttributesCacheGenerator_Renderer_set_enabled_m927,
	Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_materials_m957,
	Renderer_t154_CustomAttributesCacheGenerator_Renderer_set_materials_m960,
	Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_sharedMaterials_m956,
	Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_bounds_m701,
	Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_sortingLayerID_m3143,
	Renderer_t154_CustomAttributesCacheGenerator_Renderer_get_sortingOrder_m3144,
	Projector_t395_CustomAttributesCacheGenerator_Projector_get_fieldOfView_m1822,
	Projector_t395_CustomAttributesCacheGenerator_Projector_set_fieldOfView_m1821,
	LineRenderer_t397_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetWidth_m3758,
	LineRenderer_t397_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetVertexCount_m3759,
	LineRenderer_t397_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetPosition_m3760,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_INTERNAL_CALL_Internal_DrawMeshNow2_m3762,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_DrawProceduralIndirect_m832,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_t802_Graphics_DrawProceduralIndirect_m832_Arg2_ParameterInfo,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_Blit_m737,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_Blit_m739,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_t802_Graphics_Blit_m742_Arg3_ParameterInfo,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_Internal_BlitMaterial_m3763,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_t802_Graphics_BlitMultiTap_m768_Arg3_ParameterInfo,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_Internal_BlitMultiTap_m3764,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_Internal_SetNullRT_m3765,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_Internal_SetRTSimple_m3766,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_ClearRandomWriteTargets_m827,
	Graphics_t802_CustomAttributesCacheGenerator_Graphics_Internal_SetRandomWriteTargetBuffer_m3767,
	Screen_t803_CustomAttributesCacheGenerator_Screen_get_width_m723,
	Screen_t803_CustomAttributesCacheGenerator_Screen_get_height_m859,
	Screen_t803_CustomAttributesCacheGenerator_Screen_get_dpi_m3423,
	GL_t804_CustomAttributesCacheGenerator_GL_Vertex3_m850,
	GL_t804_CustomAttributesCacheGenerator_GL_TexCoord2_m864,
	GL_t804_CustomAttributesCacheGenerator_GL_MultiTexCoord2_m849,
	GL_t804_CustomAttributesCacheGenerator_GL_Begin_m848,
	GL_t804_CustomAttributesCacheGenerator_GL_End_m851,
	GL_t804_CustomAttributesCacheGenerator_GL_LoadOrtho_m847,
	GL_t804_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_LoadProjectionMatrix_m3770,
	GL_t804_CustomAttributesCacheGenerator_GL_LoadIdentity_m837,
	GL_t804_CustomAttributesCacheGenerator_GL_PushMatrix_m836,
	GL_t804_CustomAttributesCacheGenerator_GL_PopMatrix_m839,
	GL_t804_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_GetGPUProjectionMatrix_m3771,
	GL_t804_CustomAttributesCacheGenerator_GL_Clear_m755,
	GL_t804_CustomAttributesCacheGenerator_GL_t804_GL_Clear_m3772_Arg3_ParameterInfo,
	GL_t804_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_Internal_Clear_m3774,
	GL_t804_CustomAttributesCacheGenerator_GL_ClearWithSkybox_m876,
	Texture_t86_CustomAttributesCacheGenerator_Texture_Internal_GetWidth_m3776,
	Texture_t86_CustomAttributesCacheGenerator_Texture_Internal_GetHeight_m3777,
	Texture_t86_CustomAttributesCacheGenerator_Texture_set_filterMode_m762,
	Texture_t86_CustomAttributesCacheGenerator_Texture_set_anisoLevel_m740,
	Texture_t86_CustomAttributesCacheGenerator_Texture_set_wrapMode_m784,
	Texture_t86_CustomAttributesCacheGenerator_Texture_get_texelSize_m852,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m3783,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_t63_Texture2D_Internal_Create_m3783_Arg0_ParameterInfo,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_get_whiteTexture_m3181,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_INTERNAL_CALL_SetPixel_m3784,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_GetPixelBilinear_m3255,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_GetPixels_m817,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_t63_Texture2D_GetPixels_m3785_Arg0_ParameterInfo,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_GetPixels_m3786,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_t63_Texture2D_GetPixels_m3786_Arg4_ParameterInfo,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_Apply_m3787,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_t63_Texture2D_Apply_m3787_Arg0_ParameterInfo,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_t63_Texture2D_Apply_m3787_Arg1_ParameterInfo,
	Texture2D_t63_CustomAttributesCacheGenerator_Texture2D_Apply_m809,
	Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_SetPixels_m3788,
	Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_t84_Texture3D_SetPixels_m3788_Arg1_ParameterInfo,
	Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_SetPixels_m813,
	Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_Apply_m3789,
	Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_t84_Texture3D_Apply_m3789_Arg0_ParameterInfo,
	Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_Apply_m814,
	Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_Internal_Create_m3790,
	Texture3D_t84_CustomAttributesCacheGenerator_Texture3D_t84_Texture3D_Internal_Create_m3790_Arg0_ParameterInfo,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_CreateRenderTexture_m3791,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_t101_RenderTexture_Internal_CreateRenderTexture_m3791_Arg0_ParameterInfo,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m3792,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_t101_RenderTexture_GetTemporary_m3792_Arg2_ParameterInfo,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_t101_RenderTexture_GetTemporary_m3792_Arg3_ParameterInfo,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_t101_RenderTexture_GetTemporary_m3792_Arg4_ParameterInfo,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_t101_RenderTexture_GetTemporary_m3792_Arg5_ParameterInfo,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m749,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m769,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m741,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_ReleaseTemporary_m743,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_GetWidth_m3793,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_SetWidth_m3794,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_GetHeight_m3795,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_SetHeight_m3796,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_Internal_SetSRGBReadWrite_m3797,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_set_depth_m3802,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_get_format_m747,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_set_format_m3803,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_INTERNAL_CALL_DiscardContents_m3804,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m3805,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetColorBuffer_m3808,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_GetDepthBuffer_m3809,
	RenderTexture_t101_CustomAttributesCacheGenerator_RenderTexture_set_active_m835,
	GUILayer_t808_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m3811,
	Gradient_t811_CustomAttributesCacheGenerator_Gradient_Init_m3815,
	Gradient_t811_CustomAttributesCacheGenerator_Gradient_Cleanup_m3816,
	GUI_t408_CustomAttributesCacheGenerator_U3CnextScrollStepTimeU3Ek__BackingField,
	GUI_t408_CustomAttributesCacheGenerator_U3CscrollTroughSideU3Ek__BackingField,
	GUI_t408_CustomAttributesCacheGenerator_GUI_get_nextScrollStepTime_m3824,
	GUI_t408_CustomAttributesCacheGenerator_GUI_set_nextScrollStepTime_m3825,
	GUI_t408_CustomAttributesCacheGenerator_GUI_get_scrollTroughSide_m3826,
	GUI_t408_CustomAttributesCacheGenerator_GUI_set_scrollTroughSide_m3827,
	GUI_t408_CustomAttributesCacheGenerator_GUI_set_changed_m3829,
	GUI_t408_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoLabel_m3832,
	GUI_t408_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoButton_m3835,
	GUI_t408_CustomAttributesCacheGenerator_GUI_get_usePageScrollbars_m3842,
	GUI_t408_CustomAttributesCacheGenerator_GUI_InternalRepaintEditorWindow_m3844,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_Label_m1680_Arg1_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_Label_m1654_Arg2_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_Box_m1653_Arg2_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_Button_m1677_Arg2_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_Button_m1662_Arg2_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_HorizontalSlider_m1661_Arg5_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginHorizontal_m1650_Arg0_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginHorizontal_m1675_Arg1_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginHorizontal_m3857_Arg2_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginVertical_m1672_Arg0_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginVertical_m3858_Arg2_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginScrollView_m1648_Arg1_ParameterInfo,
	GUILayout_t815_CustomAttributesCacheGenerator_GUILayout_t815_GUILayout_BeginScrollView_m3860_Arg6_ParameterInfo,
	GUILayoutUtility_t411_CustomAttributesCacheGenerator_GUILayoutUtility_Internal_GetWindowRect_m3874,
	GUILayoutUtility_t411_CustomAttributesCacheGenerator_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m3876,
	GUILayoutUtility_t411_CustomAttributesCacheGenerator_GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m3877,
	GUILayoutUtility_t411_CustomAttributesCacheGenerator_GUILayoutUtility_t411_GUILayoutUtility_GetRect_m3881_Arg2_ParameterInfo,
	GUILayoutUtility_t411_CustomAttributesCacheGenerator_GUILayoutUtility_t411_GUILayoutUtility_GetRect_m3883_Arg3_ParameterInfo,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_GetControlID_m3922,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m3926,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_Internal_GetHotControl_m3930,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_Internal_SetHotControl_m3931,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_get_keyboardControl_m3932,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_set_keyboardControl_m3933,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_get_systemCopyBuffer_m3934,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_set_systemCopyBuffer_m3935,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_Internal_GetDefaultSkin_m3937,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_Internal_ExitGUI_m3939,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_Internal_GetGUIDepth_m3943,
	GUIUtility_t826_CustomAttributesCacheGenerator_GUIUtility_set_mouseUsed_m3944,
	GUIClip_t827_CustomAttributesCacheGenerator_GUIClip_INTERNAL_CALL_Push_m3946,
	GUIClip_t827_CustomAttributesCacheGenerator_GUIClip_Pop_m3947,
	GUISettings_t828_CustomAttributesCacheGenerator_m_DoubleClickSelectsWord,
	GUISettings_t828_CustomAttributesCacheGenerator_m_TripleClickSelectsLine,
	GUISettings_t828_CustomAttributesCacheGenerator_m_CursorColor,
	GUISettings_t828_CustomAttributesCacheGenerator_m_CursorFlashSpeed,
	GUISettings_t828_CustomAttributesCacheGenerator_m_SelectionColor,
	GUISkin_t287_CustomAttributesCacheGenerator,
	GUISkin_t287_CustomAttributesCacheGenerator_m_Font,
	GUISkin_t287_CustomAttributesCacheGenerator_m_box,
	GUISkin_t287_CustomAttributesCacheGenerator_m_button,
	GUISkin_t287_CustomAttributesCacheGenerator_m_toggle,
	GUISkin_t287_CustomAttributesCacheGenerator_m_label,
	GUISkin_t287_CustomAttributesCacheGenerator_m_textField,
	GUISkin_t287_CustomAttributesCacheGenerator_m_textArea,
	GUISkin_t287_CustomAttributesCacheGenerator_m_window,
	GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalSlider,
	GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalSliderThumb,
	GUISkin_t287_CustomAttributesCacheGenerator_m_verticalSlider,
	GUISkin_t287_CustomAttributesCacheGenerator_m_verticalSliderThumb,
	GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalScrollbar,
	GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalScrollbarThumb,
	GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalScrollbarLeftButton,
	GUISkin_t287_CustomAttributesCacheGenerator_m_horizontalScrollbarRightButton,
	GUISkin_t287_CustomAttributesCacheGenerator_m_verticalScrollbar,
	GUISkin_t287_CustomAttributesCacheGenerator_m_verticalScrollbarThumb,
	GUISkin_t287_CustomAttributesCacheGenerator_m_verticalScrollbarUpButton,
	GUISkin_t287_CustomAttributesCacheGenerator_m_verticalScrollbarDownButton,
	GUISkin_t287_CustomAttributesCacheGenerator_m_ScrollView,
	GUISkin_t287_CustomAttributesCacheGenerator_m_CustomStyles,
	GUISkin_t287_CustomAttributesCacheGenerator_m_Settings,
	GUIContent_t301_CustomAttributesCacheGenerator_m_Text,
	GUIContent_t301_CustomAttributesCacheGenerator_m_Image,
	GUIContent_t301_CustomAttributesCacheGenerator_m_Tooltip,
	GUIStyleState_t405_CustomAttributesCacheGenerator_GUIStyleState_Init_m4010,
	GUIStyleState_t405_CustomAttributesCacheGenerator_GUIStyleState_Cleanup_m4011,
	GUIStyleState_t405_CustomAttributesCacheGenerator_GUIStyleState_SetBackgroundInternal_m4012,
	GUIStyleState_t405_CustomAttributesCacheGenerator_GUIStyleState_GetBackgroundInternal_m4013,
	GUIStyleState_t405_CustomAttributesCacheGenerator_GUIStyleState_INTERNAL_set_textColor_m4014,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_Init_m4017,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_Cleanup_m4018,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_left_m3438,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_set_left_m4019,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_right_m4020,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_set_right_m4021,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_top_m3439,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_set_top_m4022,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_bottom_m4023,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_set_bottom_m4024,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_horizontal_m3432,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_get_vertical_m3433,
	RectOffset_t404_CustomAttributesCacheGenerator_RectOffset_INTERNAL_CALL_Remove_m4026,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Init_m4030,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Cleanup_m4031,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_name_m4032,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_name_m4033,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_GetStyleStatePtr_m4034,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_GetRectOffsetPtr_m4037,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_AssignRectOffset_m4038,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_imagePosition_m4039,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_imagePosition_m1630,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_alignment_m1625,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_wordWrap_m4040,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_wordWrap_m1633,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_clipping_m1619,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_fixedWidth_m4041,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_fixedWidth_m1634,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_fixedHeight_m4042,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_fixedHeight_m1629,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_stretchWidth_m4043,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_stretchWidth_m4044,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_get_stretchHeight_m4045,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_stretchHeight_m4046,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Internal_GetLineHeight_m4047,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_fontSize_m1624,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_set_fontStyle_m1626,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Internal_Draw_m4050,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Draw_m4052,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_t302_GUIStyle_Draw_m4053_Arg3_ParameterInfo,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_Draw2_m4055,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_SetDefaultFont_m4056,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m4060,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcSize_m4061,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcHeight_m4063,
	GUIStyle_t302_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcMinMaxWidth_m4066,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_Destroy_m4069,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m4071,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m3342,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m3343,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg1_ParameterInfo,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg2_ParameterInfo,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg3_ParameterInfo,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg4_ParameterInfo,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg5_ParameterInfo,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_t553_TouchScreenKeyboard_Open_m4072_Arg6_ParameterInfo,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_text_m3280,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_text_m3281,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_hideInput_m3341,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_active_m3279,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_active_m3340,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_done_m3294,
	TouchScreenKeyboard_t553_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_wasCanceled_m3291,
	Event_t560_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0,
	Event_t560_CustomAttributesCacheGenerator_Event_Init_m4073,
	Event_t560_CustomAttributesCacheGenerator_Event_Cleanup_m4075,
	Event_t560_CustomAttributesCacheGenerator_Event_get_rawType_m3306,
	Event_t560_CustomAttributesCacheGenerator_Event_get_type_m4076,
	Event_t560_CustomAttributesCacheGenerator_Event_GetTypeForControl_m4077,
	Event_t560_CustomAttributesCacheGenerator_Event_Internal_GetMousePosition_m4079,
	Event_t560_CustomAttributesCacheGenerator_Event_Internal_GetMouseDelta_m4081,
	Event_t560_CustomAttributesCacheGenerator_Event_get_modifiers_m3302,
	Event_t560_CustomAttributesCacheGenerator_Event_get_character_m3304,
	Event_t560_CustomAttributesCacheGenerator_Event_get_commandName_m4082,
	Event_t560_CustomAttributesCacheGenerator_Event_get_keyCode_m3303,
	Event_t560_CustomAttributesCacheGenerator_Event_Internal_SetNativeEvent_m4084,
	Event_t560_CustomAttributesCacheGenerator_Event_Use_m4086,
	Event_t560_CustomAttributesCacheGenerator_Event_PopEvent_m3307,
	EventModifiers_t838_CustomAttributesCacheGenerator,
	Gizmos_t839_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawLine_m4092,
	Gizmos_t839_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawWireSphere_m4093,
	Gizmos_t839_CustomAttributesCacheGenerator_Gizmos_INTERNAL_set_color_m4094,
	Vector2_t6_CustomAttributesCacheGenerator,
	Vector3_t4_CustomAttributesCacheGenerator,
	Vector3_t4_CustomAttributesCacheGenerator_Vector3_INTERNAL_CALL_Slerp_m4100,
	Vector3_t4_CustomAttributesCacheGenerator_Vector3_SmoothDamp_m606,
	Vector3_t4_CustomAttributesCacheGenerator_Vector3_t4_Vector3_SmoothDamp_m4101_Arg4_ParameterInfo,
	Vector3_t4_CustomAttributesCacheGenerator_Vector3_t4_Vector3_SmoothDamp_m4101_Arg5_ParameterInfo,
	Color_t65_CustomAttributesCacheGenerator,
	Color32_t654_CustomAttributesCacheGenerator,
	Quaternion_t19_CustomAttributesCacheGenerator,
	Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_AngleAxis_m4117,
	Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_FromToRotation_m4119,
	Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_t19_Quaternion_LookRotation_m654_Arg1_ParameterInfo,
	Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_LookRotation_m917,
	Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_LookRotation_m4120,
	Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Slerp_m4121,
	Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Lerp_m4122,
	Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Inverse_m4123,
	Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m4127,
	Quaternion_t19_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m4129,
	Matrix4x4_t80_CustomAttributesCacheGenerator,
	Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Inverse_m4143,
	Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Transpose_m4145,
	Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Invert_m4147,
	Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_get_isIdentity_m4149,
	Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_TRS_m4157,
	Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_Ortho_m4160,
	Matrix4x4_t80_CustomAttributesCacheGenerator_Matrix4x4_Perspective_m4161,
	Bounds_t225_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_Contains_m4175,
	Bounds_t225_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_SqrDistance_m4178,
	Bounds_t225_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_IntersectRay_m4181,
	Bounds_t225_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m4185,
	Vector4_t236_CustomAttributesCacheGenerator,
	Mathf_t218_CustomAttributesCacheGenerator_Mathf_t218_Mathf_Max_m704_Arg0_ParameterInfo,
	Mathf_t218_CustomAttributesCacheGenerator_Mathf_SmoothDamp_m649,
	Mathf_t218_CustomAttributesCacheGenerator_Mathf_t218_Mathf_SmoothDamp_m3362_Arg4_ParameterInfo,
	Mathf_t218_CustomAttributesCacheGenerator_Mathf_t218_Mathf_SmoothDamp_m3362_Arg5_ParameterInfo,
	Mathf_t218_CustomAttributesCacheGenerator_Mathf_PerlinNoise_m669,
	DrivenTransformProperties_t841_CustomAttributesCacheGenerator,
	RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_rect_m4216,
	RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMin_m4217,
	RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMin_m4218,
	RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMax_m4219,
	RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMax_m4220,
	RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchoredPosition_m4221,
	RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchoredPosition_m4222,
	RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_sizeDelta_m4223,
	RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_sizeDelta_m4224,
	RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_pivot_m4225,
	RectTransform_t364_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_pivot_m4226,
	Resources_t845_CustomAttributesCacheGenerator_Resources_Load_m4232,
	SerializePrivateVariables_t846_CustomAttributesCacheGenerator,
	Shader_t54_CustomAttributesCacheGenerator_Shader_get_isSupported_m736,
	Shader_t54_CustomAttributesCacheGenerator_Shader_PropertyToID_m4234,
	Material_t55_CustomAttributesCacheGenerator_Material_get_shader_m767,
	Material_t55_CustomAttributesCacheGenerator_Material_set_shader_m958,
	Material_t55_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetColor_m4236,
	Material_t55_CustomAttributesCacheGenerator_Material_SetTexture_m4237,
	Material_t55_CustomAttributesCacheGenerator_Material_GetTexture_m4239,
	Material_t55_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetMatrix_m4241,
	Material_t55_CustomAttributesCacheGenerator_Material_SetFloat_m4242,
	Material_t55_CustomAttributesCacheGenerator_Material_SetBuffer_m829,
	Material_t55_CustomAttributesCacheGenerator_Material_HasProperty_m4243,
	Material_t55_CustomAttributesCacheGenerator_Material_get_passCount_m865,
	Material_t55_CustomAttributesCacheGenerator_Material_SetPass_m831,
	Material_t55_CustomAttributesCacheGenerator_Material_Internal_CreateWithShader_m4244,
	Material_t55_CustomAttributesCacheGenerator_Material_t55_Material_Internal_CreateWithShader_m4244_Arg0_ParameterInfo,
	Material_t55_CustomAttributesCacheGenerator_Material_Internal_CreateWithMaterial_m4245,
	Material_t55_CustomAttributesCacheGenerator_Material_t55_Material_Internal_CreateWithMaterial_m4245_Arg0_ParameterInfo,
	SphericalHarmonicsL2_t847_CustomAttributesCacheGenerator,
	SphericalHarmonicsL2_t847_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m4248,
	SphericalHarmonicsL2_t847_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m4251,
	SphericalHarmonicsL2_t847_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m4254,
	Sprite_t329_CustomAttributesCacheGenerator_Sprite_get_rect_m3234,
	Sprite_t329_CustomAttributesCacheGenerator_Sprite_get_pixelsPerUnit_m3231,
	Sprite_t329_CustomAttributesCacheGenerator_Sprite_get_texture_m3228,
	Sprite_t329_CustomAttributesCacheGenerator_Sprite_get_textureRect_m3252,
	Sprite_t329_CustomAttributesCacheGenerator_Sprite_get_border_m3229,
	DataUtility_t848_CustomAttributesCacheGenerator_DataUtility_GetInnerUV_m3241,
	DataUtility_t848_CustomAttributesCacheGenerator_DataUtility_GetOuterUV_m3240,
	DataUtility_t848_CustomAttributesCacheGenerator_DataUtility_GetPadding_m3233,
	DataUtility_t848_CustomAttributesCacheGenerator_DataUtility_Internal_GetMinSize_m4264,
	WWW_t294_CustomAttributesCacheGenerator_WWW_DestroyWWW_m4268,
	WWW_t294_CustomAttributesCacheGenerator_WWW_InitWWW_m4269,
	WWW_t294_CustomAttributesCacheGenerator_WWW_get_responseHeadersString_m4271,
	WWW_t294_CustomAttributesCacheGenerator_WWW_get_bytes_m4274,
	WWW_t294_CustomAttributesCacheGenerator_WWW_get_error_m1586,
	WWW_t294_CustomAttributesCacheGenerator_WWW_get_isDone_m4275,
	WWWForm_t851_CustomAttributesCacheGenerator_WWWForm_AddField_m4279,
	WWWForm_t851_CustomAttributesCacheGenerator_WWWForm_t851_WWWForm_AddField_m4280_Arg2_ParameterInfo,
	WWWTranscoder_t852_CustomAttributesCacheGenerator_WWWTranscoder_t852_WWWTranscoder_QPEncode_m4287_Arg1_ParameterInfo,
	WWWTranscoder_t852_CustomAttributesCacheGenerator_WWWTranscoder_t852_WWWTranscoder_SevenBitClean_m4290_Arg1_ParameterInfo,
	CacheIndex_t853_CustomAttributesCacheGenerator,
	UnityString_t854_CustomAttributesCacheGenerator_UnityString_t854_UnityString_Format_m4292_Arg1_ParameterInfo,
	AsyncOperation_t775_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m4294,
	Application_t855_CustomAttributesCacheGenerator_Application_get_loadedLevel_m1012,
	Application_t855_CustomAttributesCacheGenerator_Application_get_loadedLevelName_m637,
	Application_t855_CustomAttributesCacheGenerator_Application_LoadLevelAsync_m4300,
	Application_t855_CustomAttributesCacheGenerator_Application_get_isLoadingLevel_m1800,
	Application_t855_CustomAttributesCacheGenerator_Application_get_levelCount_m1601,
	Application_t855_CustomAttributesCacheGenerator_Application_get_isPlaying_m645,
	Application_t855_CustomAttributesCacheGenerator_Application_get_isEditor_m1759,
	Application_t855_CustomAttributesCacheGenerator_Application_get_platform_m1583,
	Application_t855_CustomAttributesCacheGenerator_Application_set_runInBackground_m1705,
	Application_t855_CustomAttributesCacheGenerator_Application_get_dataPath_m1581,
	Application_t855_CustomAttributesCacheGenerator_Application_get_streamingAssetsPath_m1580,
	Application_t855_CustomAttributesCacheGenerator_Application_get_persistentDataPath_m1806,
	Application_t855_CustomAttributesCacheGenerator_Application_get_unityVersion_m1660,
	Application_t855_CustomAttributesCacheGenerator_Application_OpenURL_m1722,
	Application_t855_CustomAttributesCacheGenerator_Application_get_systemLanguage_m1796,
	Application_t855_CustomAttributesCacheGenerator_Application_SetLogCallbackDefined_m4302,
	Application_t855_CustomAttributesCacheGenerator_Application_t855____persistentDataPath_PropertyInfo,
	Behaviour_t250_CustomAttributesCacheGenerator_Behaviour_get_enabled_m911,
	Behaviour_t250_CustomAttributesCacheGenerator_Behaviour_set_enabled_m766,
	Behaviour_t250_CustomAttributesCacheGenerator_Behaviour_get_isActiveAndEnabled_m1740,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_fieldOfView_m699,
	Camera_t27_CustomAttributesCacheGenerator_Camera_set_fieldOfView_m700,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m823,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m825,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_hdr_m748,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_depth_m3059,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_aspect_m840,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_cullingMask_m3146,
	Camera_t27_CustomAttributesCacheGenerator_Camera_set_cullingMask_m796,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_eventMask_m4308,
	Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m4309,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_targetTexture_m4311,
	Camera_t27_CustomAttributesCacheGenerator_Camera_set_targetTexture_m795,
	Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_get_worldToCameraMatrix_m4312,
	Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_get_projectionMatrix_m4313,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_clearFlags_m4314,
	Camera_t27_CustomAttributesCacheGenerator_Camera_set_clearFlags_m802,
	Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_WorldToViewportPoint_m4315,
	Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToWorldPoint_m4316,
	Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToViewportPoint_m4317,
	Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m4318,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_main_m989,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m4319,
	Camera_t27_CustomAttributesCacheGenerator_Camera_GetAllCameras_m4320,
	Camera_t27_CustomAttributesCacheGenerator_Camera_RenderWithShader_m797,
	Camera_t27_CustomAttributesCacheGenerator_Camera_CopyFrom_m801,
	Camera_t27_CustomAttributesCacheGenerator_Camera_get_depthTextureMode_m779,
	Camera_t27_CustomAttributesCacheGenerator_Camera_set_depthTextureMode_m780,
	Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m4325,
	Camera_t27_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m4327,
	CameraCallback_t856_CustomAttributesCacheGenerator,
	ComputeBufferType_t857_CustomAttributesCacheGenerator,
	ComputeBuffer_t95_CustomAttributesCacheGenerator_ComputeBuffer_InitBuffer_m4331,
	ComputeBuffer_t95_CustomAttributesCacheGenerator_ComputeBuffer_DestroyBuffer_m4332,
	ComputeBuffer_t95_CustomAttributesCacheGenerator_ComputeBuffer_SetData_m822,
	ComputeBuffer_t95_CustomAttributesCacheGenerator_ComputeBuffer_InternalSetData_m4333,
	ComputeBuffer_t95_CustomAttributesCacheGenerator_ComputeBuffer_CopyCount_m828,
	Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawLine_m4334_Arg2_ParameterInfo,
	Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawLine_m4334_Arg3_ParameterInfo,
	Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawLine_m4334_Arg4_ParameterInfo,
	Debug_t858_CustomAttributesCacheGenerator_Debug_INTERNAL_CALL_DrawLine_m4335,
	Debug_t858_CustomAttributesCacheGenerator_Debug_DrawRay_m695,
	Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawRay_m4336_Arg2_ParameterInfo,
	Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawRay_m4336_Arg3_ParameterInfo,
	Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_DrawRay_m4336_Arg4_ParameterInfo,
	Debug_t858_CustomAttributesCacheGenerator_Debug_Internal_Log_m4337,
	Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_Internal_Log_m4337_Arg2_ParameterInfo,
	Debug_t858_CustomAttributesCacheGenerator_Debug_Internal_LogException_m4338,
	Debug_t858_CustomAttributesCacheGenerator_Debug_t858_Debug_Internal_LogException_m4338_Arg1_ParameterInfo,
	Display_t861_CustomAttributesCacheGenerator_Display_GetSystemExtImpl_m4363,
	Display_t861_CustomAttributesCacheGenerator_Display_GetRenderingExtImpl_m4364,
	Display_t861_CustomAttributesCacheGenerator_Display_GetRenderingBuffersImpl_m4365,
	Display_t861_CustomAttributesCacheGenerator_Display_SetRenderingResolutionImpl_m4366,
	Display_t861_CustomAttributesCacheGenerator_Display_ActivateDisplayImpl_m4367,
	Display_t861_CustomAttributesCacheGenerator_Display_SetParamsImpl_m4368,
	Display_t861_CustomAttributesCacheGenerator_Display_MultiDisplayLicenseImpl_m4369,
	Display_t861_CustomAttributesCacheGenerator_Display_RelativeMouseAtImpl_m4370,
	MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_Invoke_m1015,
	MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_CancelInvoke_m1758,
	MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_IsInvoking_m1763,
	MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_Auto_m4371,
	MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_m988,
	MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_t3_MonoBehaviour_StartCoroutine_m988_Arg1_ParameterInfo,
	MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_m1641,
	MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_m1768,
	MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4373,
	MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_Auto_m4374,
	MonoBehaviour_t3_CustomAttributesCacheGenerator_MonoBehaviour_StopAllCoroutines_m1773,
	Input_t220_CustomAttributesCacheGenerator_Input_GetKeyInt_m4376,
	Input_t220_CustomAttributesCacheGenerator_Input_GetKeyDownInt_m4377,
	Input_t220_CustomAttributesCacheGenerator_Input_GetAxis_m715,
	Input_t220_CustomAttributesCacheGenerator_Input_GetAxisRaw_m714,
	Input_t220_CustomAttributesCacheGenerator_Input_GetButton_m716,
	Input_t220_CustomAttributesCacheGenerator_Input_GetButtonDown_m717,
	Input_t220_CustomAttributesCacheGenerator_Input_GetButtonUp_m718,
	Input_t220_CustomAttributesCacheGenerator_Input_GetMouseButton_m924,
	Input_t220_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m977,
	Input_t220_CustomAttributesCacheGenerator_Input_GetMouseButtonUp_m663,
	Input_t220_CustomAttributesCacheGenerator_Input_get_mousePosition_m720,
	Input_t220_CustomAttributesCacheGenerator_Input_get_mouseScrollDelta_m3104,
	Input_t220_CustomAttributesCacheGenerator_Input_get_mousePresent_m3130,
	Input_t220_CustomAttributesCacheGenerator_Input_get_acceleration_m721,
	Input_t220_CustomAttributesCacheGenerator_Input_GetTouch_m3135,
	Input_t220_CustomAttributesCacheGenerator_Input_get_touchCount_m727,
	Input_t220_CustomAttributesCacheGenerator_Input_set_imeCompositionMode_m3344,
	Input_t220_CustomAttributesCacheGenerator_Input_get_compositionString_m3282,
	Input_t220_CustomAttributesCacheGenerator_Input_INTERNAL_set_compositionCursorPos_m4378,
	HideFlags_t864_CustomAttributesCacheGenerator,
	Object_t164_CustomAttributesCacheGenerator_Object_Internal_CloneSingle_m4380,
	Object_t164_CustomAttributesCacheGenerator_Object_INTERNAL_CALL_Internal_InstantiateSingle_m4382,
	Object_t164_CustomAttributesCacheGenerator_Object_Destroy_m1014,
	Object_t164_CustomAttributesCacheGenerator_Object_t164_Object_Destroy_m1014_Arg1_ParameterInfo,
	Object_t164_CustomAttributesCacheGenerator_Object_Destroy_m1008,
	Object_t164_CustomAttributesCacheGenerator_Object_DestroyImmediate_m1605,
	Object_t164_CustomAttributesCacheGenerator_Object_t164_Object_DestroyImmediate_m1605_Arg1_ParameterInfo,
	Object_t164_CustomAttributesCacheGenerator_Object_DestroyImmediate_m761,
	Object_t164_CustomAttributesCacheGenerator_Object_FindObjectsOfType_m706,
	Object_t164_CustomAttributesCacheGenerator_Object_get_name_m798,
	Object_t164_CustomAttributesCacheGenerator_Object_set_name_m1795,
	Object_t164_CustomAttributesCacheGenerator_Object_DontDestroyOnLoad_m1602,
	Object_t164_CustomAttributesCacheGenerator_Object_set_hideFlags_m764,
	Object_t164_CustomAttributesCacheGenerator_Object_DestroyObject_m4383,
	Object_t164_CustomAttributesCacheGenerator_Object_t164_Object_DestroyObject_m4383_Arg1_ParameterInfo,
	Object_t164_CustomAttributesCacheGenerator_Object_DestroyObject_m948,
	Object_t164_CustomAttributesCacheGenerator_Object_ToString_m1050,
	Object_t164_CustomAttributesCacheGenerator_Object_Instantiate_m899,
	Object_t164_CustomAttributesCacheGenerator_Object_FindObjectOfType_m1706,
	Component_t219_CustomAttributesCacheGenerator_Component_get_transform_m594,
	Component_t219_CustomAttributesCacheGenerator_Component_get_gameObject_m622,
	Component_t219_CustomAttributesCacheGenerator_Component_GetComponent_m3445,
	Component_t219_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m4390,
	Component_t219_CustomAttributesCacheGenerator_Component_GetComponent_m5267,
	Component_t219_CustomAttributesCacheGenerator_Component_GetComponentInChildren_m4391,
	Component_t219_CustomAttributesCacheGenerator_Component_GetComponentsForListInternal_m4392,
	Component_t219_CustomAttributesCacheGenerator_Component_CompareTag_m688,
	Component_t219_CustomAttributesCacheGenerator_Component_SendMessage_m4393,
	Component_t219_CustomAttributesCacheGenerator_Component_t219_Component_SendMessage_m4393_Arg1_ParameterInfo,
	Component_t219_CustomAttributesCacheGenerator_Component_t219_Component_SendMessage_m4393_Arg2_ParameterInfo,
	Component_t219_CustomAttributesCacheGenerator_Component_SendMessage_m918,
	Component_t219_CustomAttributesCacheGenerator_Component_BroadcastMessage_m4394,
	Component_t219_CustomAttributesCacheGenerator_Component_t219_Component_BroadcastMessage_m4394_Arg1_ParameterInfo,
	Component_t219_CustomAttributesCacheGenerator_Component_t219_Component_BroadcastMessage_m4394_Arg2_ParameterInfo,
	Light_t152_CustomAttributesCacheGenerator_Light_set_intensity_m923,
	Light_t152_CustomAttributesCacheGenerator_Light_get_shadowStrength_m990,
	Light_t152_CustomAttributesCacheGenerator_Light_set_shadowStrength_m994,
	Light_t152_CustomAttributesCacheGenerator_Light_set_shadowBias_m993,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject__ctor_m800_Arg1_ParameterInfo,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_GetComponent_m4395,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_GetComponentFastPath_m4396,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_GetComponent_m5274,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_GetComponentInChildren_m4397,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_GetComponentsInternal_m4398,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_get_transform_m609,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_get_layer_m3321,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_set_layer_m3322,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_SetActive_m713,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_get_activeSelf_m641,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_get_activeInHierarchy_m778,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_get_tag_m4399,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_FindGameObjectWithTag_m608,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_FindGameObjectsWithTag_m1769,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_SendMessage_m4400,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject_SendMessage_m4400_Arg1_ParameterInfo,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject_SendMessage_m4400_Arg2_ParameterInfo,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_SendMessage_m1698,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_SendMessage_m1599,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_BroadcastMessage_m4401,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject_BroadcastMessage_m4401_Arg1_ParameterInfo,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject_BroadcastMessage_m4401_Arg2_ParameterInfo,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_BroadcastMessage_m947,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_Internal_AddComponentWithType_m4402,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_AddComponent_m4403,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m4404,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_t78_GameObject_Internal_CreateGameObject_m4404_Arg0_ParameterInfo,
	GameObject_t78_CustomAttributesCacheGenerator_GameObject_Find_m799,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_position_m4408,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_set_position_m4409,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localPosition_m4410,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localPosition_m4411,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_rotation_m4412,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_set_rotation_m4413,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localRotation_m4414,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localRotation_m4415,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localScale_m4416,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localScale_m4417,
	Transform_t1_CustomAttributesCacheGenerator_Transform_get_parentInternal_m4418,
	Transform_t1_CustomAttributesCacheGenerator_Transform_set_parentInternal_m4419,
	Transform_t1_CustomAttributesCacheGenerator_Transform_SetParent_m4420,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_get_worldToLocalMatrix_m4421,
	Transform_t1_CustomAttributesCacheGenerator_Transform_Translate_m1760,
	Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_Translate_m962_Arg1_ParameterInfo,
	Transform_t1_CustomAttributesCacheGenerator_Transform_Translate_m1761,
	Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_Translate_m4422_Arg3_ParameterInfo,
	Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_Rotate_m963_Arg1_ParameterInfo,
	Transform_t1_CustomAttributesCacheGenerator_Transform_Rotate_m670,
	Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_Rotate_m4423_Arg3_ParameterInfo,
	Transform_t1_CustomAttributesCacheGenerator_Transform_LookAt_m1011,
	Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_LookAt_m4424_Arg1_ParameterInfo,
	Transform_t1_CustomAttributesCacheGenerator_Transform_t1_Transform_LookAt_m4425_Arg1_ParameterInfo,
	Transform_t1_CustomAttributesCacheGenerator_Transform_LookAt_m965,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_LookAt_m4426,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformDirection_m4427,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformPoint_m4428,
	Transform_t1_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_InverseTransformPoint_m4429,
	Transform_t1_CustomAttributesCacheGenerator_Transform_get_childCount_m3446,
	Transform_t1_CustomAttributesCacheGenerator_Transform_DetachChildren_m1016,
	Transform_t1_CustomAttributesCacheGenerator_Transform_SetAsFirstSibling_m3320,
	Transform_t1_CustomAttributesCacheGenerator_Transform_Find_m618,
	Transform_t1_CustomAttributesCacheGenerator_Transform_GetChild_m1774,
	Time_t866_CustomAttributesCacheGenerator_Time_get_time_m668,
	Time_t866_CustomAttributesCacheGenerator_Time_get_deltaTime_m602,
	Time_t866_CustomAttributesCacheGenerator_Time_get_unscaledTime_m3132,
	Time_t866_CustomAttributesCacheGenerator_Time_get_unscaledDeltaTime_m3154,
	Time_t866_CustomAttributesCacheGenerator_Time_get_fixedDeltaTime_m1737,
	Time_t866_CustomAttributesCacheGenerator_Time_get_timeScale_m664,
	Time_t866_CustomAttributesCacheGenerator_Time_set_timeScale_m1716,
	Time_t866_CustomAttributesCacheGenerator_Time_get_frameCount_m708,
	Time_t866_CustomAttributesCacheGenerator_Time_get_realtimeSinceStartup_m961,
	Random_t867_CustomAttributesCacheGenerator_Random_Range_m856,
	Random_t867_CustomAttributesCacheGenerator_Random_RandomRangeInt_m4431,
	Random_t867_CustomAttributesCacheGenerator_Random_get_value_m858,
	Random_t867_CustomAttributesCacheGenerator_Random_get_insideUnitSphere_m897,
	Random_t867_CustomAttributesCacheGenerator_Random_get_onUnitSphere_m903,
	Random_t867_CustomAttributesCacheGenerator_Random_get_rotation_m898,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_TrySetInt_m4434,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_TrySetFloat_m4435,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_TrySetSetString_m4436,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_GetInt_m1607,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_t869_PlayerPrefs_GetInt_m1607_Arg1_ParameterInfo,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_GetInt_m1608,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_GetFloat_m1610,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_t869_PlayerPrefs_GetFloat_m1610_Arg1_ParameterInfo,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m4437,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_t869_PlayerPrefs_GetString_m4437_Arg1_ParameterInfo,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m1609,
	PlayerPrefs_t869_CustomAttributesCacheGenerator_PlayerPrefs_Save_m1702,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_get_isStopped_m1829,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_set_enableEmission_m921,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_get_startSpeed_m931,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_set_startSpeed_m928,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_get_startSize_m929,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_set_startSize_m930,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_get_startLifetime_m932,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_set_startLifetime_m933,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Internal_Play_m4438,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Internal_Stop_m4439,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Internal_Clear_m4440,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Play_m935,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_t161_ParticleSystem_Play_m4441_Arg0_ParameterInfo,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Stop_m1825,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_t161_ParticleSystem_Stop_m4442_Arg0_ParameterInfo,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_Clear_m934,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_t161_ParticleSystem_Clear_m4443_Arg0_ParameterInfo,
	ParticleSystem_t161_CustomAttributesCacheGenerator_ParticleSystem_INTERNAL_CALL_Emit_m4444,
	ParticleCollisionEvent_t160_CustomAttributesCacheGenerator_ParticleCollisionEvent_InstanceIDToCollider_m4447,
	ParticleSystemExtensionsImpl_t870_CustomAttributesCacheGenerator_ParticleSystemExtensionsImpl_GetSafeCollisionEventSize_m4448,
	ParticleSystemExtensionsImpl_t870_CustomAttributesCacheGenerator_ParticleSystemExtensionsImpl_GetCollisionEvents_m4449,
	ParticlePhysicsExtensions_t871_CustomAttributesCacheGenerator,
	ParticlePhysicsExtensions_t871_CustomAttributesCacheGenerator_ParticlePhysicsExtensions_GetSafeCollisionEventSize_m940,
	ParticlePhysicsExtensions_t871_CustomAttributesCacheGenerator_ParticlePhysicsExtensions_GetCollisionEvents_m941,
	Physics_t874_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_Internal_Raycast_m4467,
	Physics_t874_CustomAttributesCacheGenerator_Physics_Raycast_m964,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_Raycast_m979_Arg3_ParameterInfo,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_Raycast_m979_Arg4_ParameterInfo,
	Physics_t874_CustomAttributesCacheGenerator_Physics_Raycast_m904,
	Physics_t874_CustomAttributesCacheGenerator_Physics_Raycast_m991,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_Raycast_m3209_Arg2_ParameterInfo,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_Raycast_m3209_Arg3_ParameterInfo,
	Physics_t874_CustomAttributesCacheGenerator_Physics_RaycastAll_m689,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_RaycastAll_m3147_Arg1_ParameterInfo,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_RaycastAll_m3147_Arg2_ParameterInfo,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_RaycastAll_m4468_Arg2_ParameterInfo,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_RaycastAll_m4468_Arg3_ParameterInfo,
	Physics_t874_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_RaycastAll_m4469,
	Physics_t874_CustomAttributesCacheGenerator_Physics_OverlapSphere_m685,
	Physics_t874_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_OverlapSphere_m4470,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_CapsuleCastAll_m4471_Arg4_ParameterInfo,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_CapsuleCastAll_m4471_Arg5_ParameterInfo,
	Physics_t874_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_CapsuleCastAll_m4472,
	Physics_t874_CustomAttributesCacheGenerator_Physics_SphereCastAll_m690,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_SphereCastAll_m4473_Arg2_ParameterInfo,
	Physics_t874_CustomAttributesCacheGenerator_Physics_t874_Physics_SphereCastAll_m4473_Arg3_ParameterInfo,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_get_velocity_m4474,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_set_velocity_m4475,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_set_angularVelocity_m4476,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_get_drag_m967,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_set_drag_m969,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_get_angularDrag_m968,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_set_angularDrag_m970,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_get_mass_m1748,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_get_isKinematic_m981,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_set_isKinematic_m983,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_set_freezeRotation_m1756,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_t14_Rigidbody_AddForce_m944_Arg1_ParameterInfo,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_AddForce_m1749,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_CALL_AddForce_m4477,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_AddForceAtPosition_m887,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_CALL_AddForceAtPosition_m4478,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_t14_Rigidbody_AddExplosionForce_m909_Arg3_ParameterInfo,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_t14_Rigidbody_AddExplosionForce_m909_Arg4_ParameterInfo,
	Rigidbody_t14_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_CALL_AddExplosionForce_m4479,
	Joint_t875_CustomAttributesCacheGenerator_Joint_get_connectedBody_m966,
	Joint_t875_CustomAttributesCacheGenerator_Joint_set_connectedBody_m973,
	Joint_t875_CustomAttributesCacheGenerator_Joint_INTERNAL_set_anchor_m4480,
	SpringJoint_t176_CustomAttributesCacheGenerator_SpringJoint_set_spring_m985,
	SpringJoint_t176_CustomAttributesCacheGenerator_SpringJoint_set_damper_m986,
	SpringJoint_t176_CustomAttributesCacheGenerator_SpringJoint_set_maxDistance_m987,
	Collider_t138_CustomAttributesCacheGenerator_Collider_set_enabled_m1747,
	Collider_t138_CustomAttributesCacheGenerator_Collider_get_attachedRigidbody_m687,
	Collider_t138_CustomAttributesCacheGenerator_Collider_get_isTrigger_m686,
	Collider_t138_CustomAttributesCacheGenerator_Collider_INTERNAL_get_bounds_m4481,
	Collider_t138_CustomAttributesCacheGenerator_Collider_INTERNAL_CALL_Internal_Raycast_m4483,
	SphereCollider_t136_CustomAttributesCacheGenerator_SphereCollider_INTERNAL_get_center_m4484,
	SphereCollider_t136_CustomAttributesCacheGenerator_SphereCollider_INTERNAL_set_center_m4485,
	SphereCollider_t136_CustomAttributesCacheGenerator_SphereCollider_get_radius_m885,
	SphereCollider_t136_CustomAttributesCacheGenerator_SphereCollider_set_radius_m888,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_Internal_Raycast_m4490,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_Raycast_m3210,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_t222_Physics2D_Raycast_m4491_Arg2_ParameterInfo,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_t222_Physics2D_Raycast_m4491_Arg3_ParameterInfo,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_t222_Physics2D_Raycast_m4491_Arg4_ParameterInfo,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_t222_Physics2D_Raycast_m4491_Arg5_ParameterInfo,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_RaycastAll_m3138,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_RaycastAll_m4492,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_OverlapCircle_m628,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_OverlapCircle_m4493,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_OverlapCircleAll_m621,
	Physics2D_t222_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_OverlapCircleAll_m4494,
	Rigidbody2D_t10_CustomAttributesCacheGenerator_Rigidbody2D_INTERNAL_get_velocity_m4496,
	Rigidbody2D_t10_CustomAttributesCacheGenerator_Rigidbody2D_INTERNAL_set_velocity_m4497,
	Rigidbody2D_t10_CustomAttributesCacheGenerator_Rigidbody2D_AddForce_m632,
	Rigidbody2D_t10_CustomAttributesCacheGenerator_Rigidbody2D_INTERNAL_CALL_AddForce_m4498,
	Collider2D_t214_CustomAttributesCacheGenerator_Collider2D_get_attachedRigidbody_m4499,
	AudioListener_t882_CustomAttributesCacheGenerator_AudioListener_get_volume_m1717,
	AudioListener_t882_CustomAttributesCacheGenerator_AudioListener_set_volume_m1718,
	AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_set_clip_m937,
	AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_Play_m4515,
	AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_t247_AudioSource_Play_m4515_Arg0_ParameterInfo,
	AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_Play_m938,
	AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_Stop_m1813,
	AudioSource_t247_CustomAttributesCacheGenerator_AudioSource_get_isPlaying_m1812,
	AnimationEvent_t886_CustomAttributesCacheGenerator_AnimationEvent_t886____data_PropertyInfo,
	AnimationCurve_t82_CustomAttributesCacheGenerator,
	AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_t82_AnimationCurve__ctor_m804_Arg0_ParameterInfo,
	AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m4542,
	AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_Evaluate_m806,
	AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_get_length_m878,
	AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_GetKey_Internal_m4544,
	AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_GetKeys_m4545,
	AnimationCurve_t82_CustomAttributesCacheGenerator_AnimationCurve_Init_m4546,
	Animation_t249_CustomAttributesCacheGenerator,
	Animation_t249_CustomAttributesCacheGenerator_Animation_Play_m949,
	Animation_t249_CustomAttributesCacheGenerator_Animation_t249_Animation_Play_m4550_Arg0_ParameterInfo,
	Animation_t249_CustomAttributesCacheGenerator_Animation_PlayDefaultAnimation_m4551,
	Animation_t249_CustomAttributesCacheGenerator_Animation_GetStateAtIndex_m4553,
	Animation_t249_CustomAttributesCacheGenerator_Animation_GetStateCount_m4554,
	AnimatorStateInfo_t887_CustomAttributesCacheGenerator_AnimatorStateInfo_t887____nameHash_PropertyInfo,
	Animator_t9_CustomAttributesCacheGenerator_Animator_GetCurrentAnimatorClipInfo_m1819,
	Animator_t9_CustomAttributesCacheGenerator_Animator_get_runtimeAnimatorController_m3392,
	Animator_t9_CustomAttributesCacheGenerator_Animator_StringToHash_m4573,
	Animator_t9_CustomAttributesCacheGenerator_Animator_SetFloatString_m4574,
	Animator_t9_CustomAttributesCacheGenerator_Animator_SetBoolString_m4575,
	Animator_t9_CustomAttributesCacheGenerator_Animator_GetBoolString_m4576,
	Animator_t9_CustomAttributesCacheGenerator_Animator_SetTriggerString_m4577,
	Animator_t9_CustomAttributesCacheGenerator_Animator_ResetTriggerString_m4578,
	GUIText_t181_CustomAttributesCacheGenerator_GUIText_set_text_m998,
	CharacterInfo_t896_CustomAttributesCacheGenerator_uv,
	CharacterInfo_t896_CustomAttributesCacheGenerator_vert,
	CharacterInfo_t896_CustomAttributesCacheGenerator_width,
	CharacterInfo_t896_CustomAttributesCacheGenerator_flipped,
	Font_t517_CustomAttributesCacheGenerator_Font_get_material_m3401,
	Font_t517_CustomAttributesCacheGenerator_Font_HasCharacter_m3305,
	Font_t517_CustomAttributesCacheGenerator_Font_get_dynamic_m3404,
	Font_t517_CustomAttributesCacheGenerator_Font_get_fontSize_m3406,
	FontTextureRebuildCallback_t897_CustomAttributesCacheGenerator,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_Init_m4606,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_Dispose_cpp_m4607,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m4610,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_get_rectExtents_m3319,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_get_vertexCount_m4611,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetVerticesInternal_m4612,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetVerticesArray_m4613,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_get_characterCount_m4614,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetCharactersInternal_m4615,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetCharactersArray_m4616,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_get_lineCount_m3298,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetLinesInternal_m4617,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_GetLinesArray_m4618,
	TextGenerator_t557_CustomAttributesCacheGenerator_TextGenerator_get_fontSizeUsedForBestFit_m3332,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_renderMode_m3205,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_isRootCanvas_m3422,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_worldCamera_m3213,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_scaleFactor_m3405,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_set_scaleFactor_m3424,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_referencePixelsPerUnit_m3232,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_set_referencePixelsPerUnit_m3425,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_pixelPerfect_m3191,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_renderOrder_m3207,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_sortingOrder_m3206,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_get_cachedSortingLayerValue_m3212,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasMaterial_m3178,
	Canvas_t414_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasTextMaterial_m3400,
	CanvasGroup_t672_CustomAttributesCacheGenerator_CanvasGroup_get_interactable_m3387,
	CanvasGroup_t672_CustomAttributesCacheGenerator_CanvasGroup_get_blocksRaycasts_m4630,
	CanvasGroup_t672_CustomAttributesCacheGenerator_CanvasGroup_get_ignoreParentGroups_m3190,
	CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_INTERNAL_CALL_SetColor_m4633,
	CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_GetColor_m3194,
	CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_set_isMask_m3456,
	CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_SetMaterial_m3188,
	CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternal_m4634,
	CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternalArray_m4635,
	CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_Clear_m3182,
	CanvasRenderer_t522_CustomAttributesCacheGenerator_CanvasRenderer_get_absoluteDepth_m3179,
	RectTransformUtility_t674_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4637,
	RectTransformUtility_t674_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4639,
	RectTransformUtility_t674_CustomAttributesCacheGenerator_RectTransformUtility_PixelAdjustRect_m3193,
	Request_t901_CustomAttributesCacheGenerator_U3CsourceIdU3Ek__BackingField,
	Request_t901_CustomAttributesCacheGenerator_U3CappIdU3Ek__BackingField,
	Request_t901_CustomAttributesCacheGenerator_U3CdomainU3Ek__BackingField,
	Request_t901_CustomAttributesCacheGenerator_Request_get_sourceId_m4644,
	Request_t901_CustomAttributesCacheGenerator_Request_get_appId_m4645,
	Request_t901_CustomAttributesCacheGenerator_Request_get_domain_m4646,
	Response_t903_CustomAttributesCacheGenerator_U3CsuccessU3Ek__BackingField,
	Response_t903_CustomAttributesCacheGenerator_U3CextendedInfoU3Ek__BackingField,
	Response_t903_CustomAttributesCacheGenerator_Response_get_success_m4655,
	Response_t903_CustomAttributesCacheGenerator_Response_set_success_m4656,
	Response_t903_CustomAttributesCacheGenerator_Response_get_extendedInfo_m4657,
	Response_t903_CustomAttributesCacheGenerator_Response_set_extendedInfo_m4658,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_U3CsizeU3Ek__BackingField,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_U3CadvertiseU3Ek__BackingField,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_get_name_m4663,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_set_name_m4664,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_get_size_m4665,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_set_size_m4666,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_get_advertise_m4667,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_set_advertise_m4668,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_get_password_m4669,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_set_password_m4670,
	CreateMatchRequest_t906_CustomAttributesCacheGenerator_CreateMatchRequest_get_matchAttributes_m4671,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_address_m4674,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_address_m4675,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_port_m4676,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_port_m4677,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_networkId_m4678,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_networkId_m4679,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_accessTokenString_m4680,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_accessTokenString_m4681,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_nodeId_m4682,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_nodeId_m4683,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_get_usingRelay_m4684,
	CreateMatchResponse_t907_CustomAttributesCacheGenerator_CreateMatchResponse_set_usingRelay_m4685,
	JoinMatchRequest_t908_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	JoinMatchRequest_t908_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField,
	JoinMatchRequest_t908_CustomAttributesCacheGenerator_JoinMatchRequest_get_networkId_m4689,
	JoinMatchRequest_t908_CustomAttributesCacheGenerator_JoinMatchRequest_set_networkId_m4690,
	JoinMatchRequest_t908_CustomAttributesCacheGenerator_JoinMatchRequest_get_password_m4691,
	JoinMatchRequest_t908_CustomAttributesCacheGenerator_JoinMatchRequest_set_password_m4692,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_address_m4695,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_address_m4696,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_port_m4697,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_port_m4698,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_networkId_m4699,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_networkId_m4700,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_accessTokenString_m4701,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_accessTokenString_m4702,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_nodeId_m4703,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_nodeId_m4704,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_get_usingRelay_m4705,
	JoinMatchResponse_t909_CustomAttributesCacheGenerator_JoinMatchResponse_set_usingRelay_m4706,
	DestroyMatchRequest_t910_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	DestroyMatchRequest_t910_CustomAttributesCacheGenerator_DestroyMatchRequest_get_networkId_m4710,
	DestroyMatchRequest_t910_CustomAttributesCacheGenerator_DestroyMatchRequest_set_networkId_m4711,
	DropConnectionRequest_t911_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	DropConnectionRequest_t911_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	DropConnectionRequest_t911_CustomAttributesCacheGenerator_DropConnectionRequest_get_networkId_m4714,
	DropConnectionRequest_t911_CustomAttributesCacheGenerator_DropConnectionRequest_set_networkId_m4715,
	DropConnectionRequest_t911_CustomAttributesCacheGenerator_DropConnectionRequest_get_nodeId_m4716,
	DropConnectionRequest_t911_CustomAttributesCacheGenerator_DropConnectionRequest_set_nodeId_m4717,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_U3CpageSizeU3Ek__BackingField,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_U3CpageNumU3Ek__BackingField,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_U3CnameFilterU3Ek__BackingField,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_U3CmatchAttributeFilterLessThanU3Ek__BackingField,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_U3CmatchAttributeFilterGreaterThanU3Ek__BackingField,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_get_pageSize_m4720,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_set_pageSize_m4721,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_get_pageNum_m4722,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_set_pageNum_m4723,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_get_nameFilter_m4724,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_set_nameFilter_m4725,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterLessThan_m4726,
	ListMatchRequest_t912_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterGreaterThan_m4727,
	MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_U3CpublicAddressU3Ek__BackingField,
	MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_U3CprivateAddressU3Ek__BackingField,
	MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_nodeId_m4730,
	MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_nodeId_m4731,
	MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_publicAddress_m4732,
	MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_publicAddress_m4733,
	MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_privateAddress_m4734,
	MatchDirectConnectInfo_t913_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_privateAddress_m4735,
	MatchDesc_t915_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	MatchDesc_t915_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField,
	MatchDesc_t915_CustomAttributesCacheGenerator_U3CaverageEloScoreU3Ek__BackingField,
	MatchDesc_t915_CustomAttributesCacheGenerator_U3CmaxSizeU3Ek__BackingField,
	MatchDesc_t915_CustomAttributesCacheGenerator_U3CcurrentSizeU3Ek__BackingField,
	MatchDesc_t915_CustomAttributesCacheGenerator_U3CisPrivateU3Ek__BackingField,
	MatchDesc_t915_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField,
	MatchDesc_t915_CustomAttributesCacheGenerator_U3ChostNodeIdU3Ek__BackingField,
	MatchDesc_t915_CustomAttributesCacheGenerator_U3CdirectConnectInfosU3Ek__BackingField,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_networkId_m4739,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_networkId_m4740,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_name_m4741,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_name_m4742,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_averageEloScore_m4743,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_maxSize_m4744,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_maxSize_m4745,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_currentSize_m4746,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_currentSize_m4747,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_isPrivate_m4748,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_isPrivate_m4749,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_matchAttributes_m4750,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_hostNodeId_m4751,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_get_directConnectInfos_m4752,
	MatchDesc_t915_CustomAttributesCacheGenerator_MatchDesc_set_directConnectInfos_m4753,
	ListMatchResponse_t917_CustomAttributesCacheGenerator_U3CmatchesU3Ek__BackingField,
	ListMatchResponse_t917_CustomAttributesCacheGenerator_ListMatchResponse_get_matches_m4757,
	ListMatchResponse_t917_CustomAttributesCacheGenerator_ListMatchResponse_set_matches_m4758,
	AppID_t918_CustomAttributesCacheGenerator,
	SourceID_t919_CustomAttributesCacheGenerator,
	NetworkID_t920_CustomAttributesCacheGenerator,
	NodeID_t921_CustomAttributesCacheGenerator,
	NetworkMatch_t927_CustomAttributesCacheGenerator_NetworkMatch_ProcessMatchResponse_m5285,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_CustomAttributesCacheGenerator,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5291,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m5292,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1118_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m5294,
	JsonArray_t928_CustomAttributesCacheGenerator,
	JsonObject_t930_CustomAttributesCacheGenerator,
	SimpleJson_t933_CustomAttributesCacheGenerator,
	SimpleJson_t933_CustomAttributesCacheGenerator_SimpleJson_TryDeserializeObject_m4802,
	SimpleJson_t933_CustomAttributesCacheGenerator_SimpleJson_NextToken_m4814,
	SimpleJson_t933_CustomAttributesCacheGenerator_SimpleJson_t933____PocoJsonSerializerStrategy_PropertyInfo,
	IJsonSerializerStrategy_t931_CustomAttributesCacheGenerator,
	IJsonSerializerStrategy_t931_CustomAttributesCacheGenerator_IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m5298,
	PocoJsonSerializerStrategy_t932_CustomAttributesCacheGenerator,
	PocoJsonSerializerStrategy_t932_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m4831,
	PocoJsonSerializerStrategy_t932_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4832,
	ReflectionUtils_t946_CustomAttributesCacheGenerator,
	ReflectionUtils_t946_CustomAttributesCacheGenerator_ReflectionUtils_t946_ReflectionUtils_GetConstructorInfo_m4857_Arg1_ParameterInfo,
	ReflectionUtils_t946_CustomAttributesCacheGenerator_ReflectionUtils_t946_ReflectionUtils_GetContructor_m4862_Arg1_ParameterInfo,
	ReflectionUtils_t946_CustomAttributesCacheGenerator_ReflectionUtils_t946_ReflectionUtils_GetConstructorByReflection_m4864_Arg1_ParameterInfo,
	ThreadSafeDictionary_2_t1121_CustomAttributesCacheGenerator,
	ConstructorDelegate_t939_CustomAttributesCacheGenerator_ConstructorDelegate_t939_ConstructorDelegate_Invoke_m4842_Arg0_ParameterInfo,
	ConstructorDelegate_t939_CustomAttributesCacheGenerator_ConstructorDelegate_t939_ConstructorDelegate_BeginInvoke_m4843_Arg0_ParameterInfo,
	U3CGetConstructorByReflectionU3Ec__AnonStorey1_t941_CustomAttributesCacheGenerator,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t942_CustomAttributesCacheGenerator,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t943_CustomAttributesCacheGenerator,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t944_CustomAttributesCacheGenerator,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t945_CustomAttributesCacheGenerator,
	IL2CPPStructAlignmentAttribute_t948_CustomAttributesCacheGenerator,
	DisallowMultipleComponent_t724_CustomAttributesCacheGenerator,
	RequireComponent_t259_CustomAttributesCacheGenerator,
	WritableAttribute_t954_CustomAttributesCacheGenerator,
	AssemblyIsEditorAssembly_t955_CustomAttributesCacheGenerator,
	DepthTextureMode_t964_CustomAttributesCacheGenerator,
	GUIStateObjects_t974_CustomAttributesCacheGenerator_GUIStateObjects_GetStateObject_m4897,
	Achievement_t977_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Achievement_t977_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField,
	Achievement_t977_CustomAttributesCacheGenerator_Achievement_get_id_m4917,
	Achievement_t977_CustomAttributesCacheGenerator_Achievement_set_id_m4918,
	Achievement_t977_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m4919,
	Achievement_t977_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m4920,
	AchievementDescription_t978_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	AchievementDescription_t978_CustomAttributesCacheGenerator_AchievementDescription_get_id_m4927,
	AchievementDescription_t978_CustomAttributesCacheGenerator_AchievementDescription_set_id_m4928,
	Score_t979_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField,
	Score_t979_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField,
	Score_t979_CustomAttributesCacheGenerator_Score_get_leaderboardID_m4937,
	Score_t979_CustomAttributesCacheGenerator_Score_set_leaderboardID_m4938,
	Score_t979_CustomAttributesCacheGenerator_Score_get_value_m4939,
	Score_t979_CustomAttributesCacheGenerator_Score_set_value_m4940,
	Leaderboard_t797_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Leaderboard_t797_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField,
	Leaderboard_t797_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField,
	Leaderboard_t797_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField,
	Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_get_id_m4948,
	Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_set_id_m4949,
	Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m4950,
	Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m4951,
	Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_get_range_m4952,
	Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_set_range_m4953,
	Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m4954,
	Leaderboard_t797_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m4955,
	PropertyAttribute_t990_CustomAttributesCacheGenerator,
	TooltipAttribute_t266_CustomAttributesCacheGenerator,
	SpaceAttribute_t726_CustomAttributesCacheGenerator,
	RangeAttribute_t261_CustomAttributesCacheGenerator,
	TextAreaAttribute_t728_CustomAttributesCacheGenerator,
	SelectionBaseAttribute_t727_CustomAttributesCacheGenerator,
	StackTraceUtility_t993_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m4995,
	StackTraceUtility_t993_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m4998,
	StackTraceUtility_t993_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m5000,
	SharedBetweenAnimatorsAttribute_t994_CustomAttributesCacheGenerator,
	ArgumentCache_t1001_CustomAttributesCacheGenerator_m_ObjectArgument,
	ArgumentCache_t1001_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName,
	ArgumentCache_t1001_CustomAttributesCacheGenerator_m_IntArgument,
	ArgumentCache_t1001_CustomAttributesCacheGenerator_m_FloatArgument,
	ArgumentCache_t1001_CustomAttributesCacheGenerator_m_StringArgument,
	ArgumentCache_t1001_CustomAttributesCacheGenerator_m_BoolArgument,
	PersistentCall_t1005_CustomAttributesCacheGenerator_m_Target,
	PersistentCall_t1005_CustomAttributesCacheGenerator_m_MethodName,
	PersistentCall_t1005_CustomAttributesCacheGenerator_m_Mode,
	PersistentCall_t1005_CustomAttributesCacheGenerator_m_Arguments,
	PersistentCall_t1005_CustomAttributesCacheGenerator_m_CallState,
	PersistentCallGroup_t1007_CustomAttributesCacheGenerator_m_Calls,
	UnityEventBase_t1010_CustomAttributesCacheGenerator_m_PersistentCalls,
	UnityEventBase_t1010_CustomAttributesCacheGenerator_m_TypeName,
	UserAuthorizationDialog_t1011_CustomAttributesCacheGenerator,
	DefaultValueAttribute_t1012_CustomAttributesCacheGenerator,
	ExcludeFromDocsAttribute_t1013_CustomAttributesCacheGenerator,
	FormerlySerializedAsAttribute_t719_CustomAttributesCacheGenerator,
	TypeInferenceRuleAttribute_t1015_CustomAttributesCacheGenerator,
};
