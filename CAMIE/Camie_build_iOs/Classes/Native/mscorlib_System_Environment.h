﻿#pragma once
#include <stdint.h>
// System.OperatingSystem
struct OperatingSystem_t2210;
// System.Object
#include "mscorlib_System_Object.h"
// System.Environment
struct  Environment_t2211  : public Object_t
{
};
struct Environment_t2211_StaticFields{
	// System.OperatingSystem System.Environment::os
	OperatingSystem_t2210 * ___os_0;
};
