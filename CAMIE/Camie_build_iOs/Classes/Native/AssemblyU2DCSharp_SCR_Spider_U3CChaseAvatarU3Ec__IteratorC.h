﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// SCR_Spider
struct SCR_Spider_t384;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// SCR_Spider/<ChaseAvatar>c__IteratorC
struct  U3CChaseAvatarU3Ec__IteratorC_t396  : public Object_t
{
	// System.Single SCR_Spider/<ChaseAvatar>c__IteratorC::<speed>__0
	float ___U3CspeedU3E__0_0;
	// UnityEngine.Vector3 SCR_Spider/<ChaseAvatar>c__IteratorC::<targetX>__1
	Vector3_t4  ___U3CtargetXU3E__1_1;
	// System.Int32 SCR_Spider/<ChaseAvatar>c__IteratorC::$PC
	int32_t ___U24PC_2;
	// System.Object SCR_Spider/<ChaseAvatar>c__IteratorC::$current
	Object_t * ___U24current_3;
	// SCR_Spider SCR_Spider/<ChaseAvatar>c__IteratorC::<>f__this
	SCR_Spider_t384 * ___U3CU3Ef__this_4;
};
