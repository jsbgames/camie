﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
struct InternalEnumerator_1_t3446;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Int32>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_8MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22157(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3446 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13662_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22158(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3446 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13663_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
#define InternalEnumerator_1_Dispose_m22159(__this, method) (( void (*) (InternalEnumerator_1_t3446 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13664_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22160(__this, method) (( bool (*) (InternalEnumerator_1_t3446 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13665_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
#define InternalEnumerator_1_get_Current_m22161(__this, method) (( int32_t (*) (InternalEnumerator_1_t3446 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13666_gshared)(__this, method)
