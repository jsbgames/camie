﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Rigidbody>
struct List_1_t142;
// UnityEngine.Rigidbody
struct Rigidbody_t14;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>
struct  Enumerator_t145 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>::l
	List_1_t142 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody>::current
	Rigidbody_t14 * ___current_3;
};
