﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Enumerator_t3378;
// System.Object
struct Object_t;
// UnityEngine.Event
struct Event_t560;
struct Event_t560_marshaled;
// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t999;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_32.h"
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9MethodDeclarations.h"
#define Enumerator__ctor_m21493(__this, ___dictionary, method) (( void (*) (Enumerator_t3378 *, Dictionary_2_t999 *, const MethodInfo*))Enumerator__ctor_m15593_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21494(__this, method) (( Object_t * (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15594_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21495(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15595_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21496(__this, method) (( Object_t * (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15596_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21497(__this, method) (( Object_t * (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15597_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m21498(__this, method) (( bool (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_MoveNext_m15598_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m21499(__this, method) (( KeyValuePair_2_t3375  (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_get_Current_m15599_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m21500(__this, method) (( Event_t560 * (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_get_CurrentKey_m15600_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m21501(__this, method) (( int32_t (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_get_CurrentValue_m15601_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m21502(__this, method) (( void (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_VerifyState_m15602_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m21503(__this, method) (( void (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_VerifyCurrent_m15603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m21504(__this, method) (( void (*) (Enumerator_t3378 *, const MethodInfo*))Enumerator_Dispose_m15604_gshared)(__this, method)
