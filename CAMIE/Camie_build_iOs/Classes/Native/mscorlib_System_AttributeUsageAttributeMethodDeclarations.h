﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AttributeUsageAttribute
struct AttributeUsageAttribute_t1123;
// System.AttributeTargets
#include "mscorlib_System_AttributeTargets.h"

// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
extern "C" void AttributeUsageAttribute__ctor_m5322 (AttributeUsageAttribute_t1123 * __this, int32_t ___validOn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_AllowMultiple()
extern "C" bool AttributeUsageAttribute_get_AllowMultiple_m7667 (AttributeUsageAttribute_t1123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m5324 (AttributeUsageAttribute_t1123 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_Inherited()
extern "C" bool AttributeUsageAttribute_get_Inherited_m7668 (AttributeUsageAttribute_t1123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
extern "C" void AttributeUsageAttribute_set_Inherited_m5323 (AttributeUsageAttribute_t1123 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
