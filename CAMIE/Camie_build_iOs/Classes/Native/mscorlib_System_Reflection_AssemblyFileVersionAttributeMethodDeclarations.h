﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_t716;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
extern "C" void AssemblyFileVersionAttribute__ctor_m3474 (AssemblyFileVersionAttribute_t716 * __this, String_t* ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
