﻿#pragma once
#include <stdint.h>
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_t199;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB
struct  U3CReloadLevelU3Ec__IteratorB_t204  : public Object_t
{
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::entry
	Entry_t199 * ___entry_0;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::$PC
	int32_t ___U24PC_1;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::$current
	Object_t * ___U24current_2;
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__IteratorB::<$>entry
	Entry_t199 * ___U3CU24U3Eentry_3;
};
