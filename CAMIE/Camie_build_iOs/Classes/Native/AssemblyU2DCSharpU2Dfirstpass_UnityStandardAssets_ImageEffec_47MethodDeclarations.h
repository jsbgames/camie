﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.ImageEffects.Quads
struct Quads_t114;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t113;
// UnityEngine.Mesh
struct Mesh_t216;

// System.Void UnityStandardAssets.ImageEffects.Quads::.ctor()
extern "C" void Quads__ctor_m346 (Quads_t114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.Quads::.cctor()
extern "C" void Quads__cctor_m347 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.ImageEffects.Quads::HasMeshes()
extern "C" bool Quads_HasMeshes_m348 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.Quads::Cleanup()
extern "C" void Quads_Cleanup_m349 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Quads::GetMeshes(System.Int32,System.Int32)
extern "C" MeshU5BU5D_t113* Quads_GetMeshes_m350 (Object_t * __this /* static, unused */, int32_t ___totalWidth, int32_t ___totalHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityStandardAssets.ImageEffects.Quads::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" Mesh_t216 * Quads_GetMesh_m351 (Object_t * __this /* static, unused */, int32_t ___triCount, int32_t ___triOffset, int32_t ___totalWidth, int32_t ___totalHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
