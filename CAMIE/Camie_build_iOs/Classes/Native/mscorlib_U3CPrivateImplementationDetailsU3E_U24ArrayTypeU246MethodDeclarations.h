﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$64
struct U24ArrayTypeU2464_t2262;
struct U24ArrayTypeU2464_t2262_marshaled;

void U24ArrayTypeU2464_t2262_marshal(const U24ArrayTypeU2464_t2262& unmarshaled, U24ArrayTypeU2464_t2262_marshaled& marshaled);
void U24ArrayTypeU2464_t2262_marshal_back(const U24ArrayTypeU2464_t2262_marshaled& marshaled, U24ArrayTypeU2464_t2262& unmarshaled);
void U24ArrayTypeU2464_t2262_marshal_cleanup(U24ArrayTypeU2464_t2262_marshaled& marshaled);
