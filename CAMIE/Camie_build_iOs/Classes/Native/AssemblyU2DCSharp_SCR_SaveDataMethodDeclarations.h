﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_SaveData
struct SCR_SaveData_t382;

// System.Void SCR_SaveData::.ctor()
extern "C" void SCR_SaveData__ctor_m1480 (SCR_SaveData_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SaveData::Awake()
extern "C" void SCR_SaveData_Awake_m1481 (SCR_SaveData_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SaveData::Start()
extern "C" void SCR_SaveData_Start_m1482 (SCR_SaveData_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SaveData::GetDataValues(System.Int32)
extern "C" void SCR_SaveData_GetDataValues_m1483 (SCR_SaveData_t382 * __this, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SaveData::SetDataValues()
extern "C" void SCR_SaveData_SetDataValues_m1484 (SCR_SaveData_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SaveData::Save(System.Int32)
extern "C" void SCR_SaveData_Save_m1485 (SCR_SaveData_t382 * __this, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SaveData::Load()
extern "C" void SCR_SaveData_Load_m1486 (SCR_SaveData_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_SaveData::ResetSave()
extern "C" void SCR_SaveData_ResetSave_m1487 (SCR_SaveData_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
