﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_Joystick
struct SCR_Joystick_t346;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t215;
// SCR_Joystick/Direction
#include "AssemblyU2DCSharp_SCR_Joystick_Direction.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void SCR_Joystick::.ctor()
extern "C" void SCR_Joystick__ctor_m1347 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Joystick::Start()
extern "C" void SCR_Joystick_Start_m1348 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Joystick::OnLevelWasLoaded()
extern "C" void SCR_Joystick_OnLevelWasLoaded_m1349 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Joystick::OnDisable()
extern "C" void SCR_Joystick_OnDisable_m1350 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Joystick::Update()
extern "C" void SCR_Joystick_Update_m1351 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" void SCR_Joystick_OnPointerDown_m1352 (SCR_Joystick_t346 * __this, PointerEventData_t215 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" void SCR_Joystick_OnPointerUp_m1353 (SCR_Joystick_t346 * __this, PointerEventData_t215 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Joystick::FollowTouch()
extern "C" void SCR_Joystick_FollowTouch_m1354 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_Joystick::AdjustPosition()
extern "C" void SCR_Joystick_AdjustPosition_m1355 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SCR_Joystick::ToAvatarAngle(UnityEngine.Vector3)
extern "C" Vector3_t4  SCR_Joystick_ToAvatarAngle_m1356 (SCR_Joystick_t346 * __this, Vector3_t4  ___vector, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_Joystick/Direction SCR_Joystick::GetClosestDirection()
extern "C" int32_t SCR_Joystick_GetClosestDirection_m1357 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_Joystick/Direction SCR_Joystick::SwitchDirections(SCR_Joystick/Direction)
extern "C" int32_t SCR_Joystick_SwitchDirections_m1358 (SCR_Joystick_t346 * __this, int32_t ___newDir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_Joystick::get_IsDragging()
extern "C" bool SCR_Joystick_get_IsDragging_m1359 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SCR_Joystick::get_HorizontalAxis()
extern "C" float SCR_Joystick_get_HorizontalAxis_m1360 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SCR_Joystick::get_VerticalAxis()
extern "C" float SCR_Joystick_get_VerticalAxis_m1361 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_Joystick/Direction SCR_Joystick::get_PointingTo()
extern "C" int32_t SCR_Joystick_get_PointingTo_m1362 (SCR_Joystick_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
