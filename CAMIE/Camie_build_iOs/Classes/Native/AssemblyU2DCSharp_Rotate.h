﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Rotate
struct  Rotate_t307  : public MonoBehaviour_t3
{
	// UnityEngine.Vector3 Rotate::angle
	Vector3_t4  ___angle_2;
};
