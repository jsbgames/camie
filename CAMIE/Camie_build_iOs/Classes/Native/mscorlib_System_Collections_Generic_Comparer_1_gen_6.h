﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.DateTime>
struct Comparer_1_t3497;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.DateTime>
struct  Comparer_1_t3497  : public Object_t
{
};
struct Comparer_1_t3497_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.DateTime>::_default
	Comparer_1_t3497 * ____default_0;
};
