﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
struct Enumerator_t3260;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t3254;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m19990_gshared (Enumerator_t3260 * __this, Dictionary_2_t3254 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m19990(__this, ___dictionary, method) (( void (*) (Enumerator_t3260 *, Dictionary_2_t3254 *, const MethodInfo*))Enumerator__ctor_m19990_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m19991_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m19991(__this, method) (( Object_t * (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19991_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1430  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19992_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19992(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19992_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19993_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19993(__this, method) (( Object_t * (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19994_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19994(__this, method) (( Object_t * (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19994_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool Enumerator_MoveNext_m19995_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m19995(__this, method) (( bool (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_MoveNext_m19995_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_Current()
extern "C" KeyValuePair_2_t3255  Enumerator_get_Current_m19996_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m19996(__this, method) (( KeyValuePair_2_t3255  (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_get_Current_m19996_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m19997_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m19997(__this, method) (( Object_t * (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_get_CurrentKey_m19997_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentValue()
extern "C" int64_t Enumerator_get_CurrentValue_m19998_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m19998(__this, method) (( int64_t (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_get_CurrentValue_m19998_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyState()
extern "C" void Enumerator_VerifyState_m19999_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m19999(__this, method) (( void (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_VerifyState_m19999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m20000_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m20000(__this, method) (( void (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_VerifyCurrent_m20000_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::Dispose()
extern "C" void Enumerator_Dispose_m20001_gshared (Enumerator_t3260 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20001(__this, method) (( void (*) (Enumerator_t3260 *, const MethodInfo*))Enumerator_Dispose_m20001_gshared)(__this, method)
