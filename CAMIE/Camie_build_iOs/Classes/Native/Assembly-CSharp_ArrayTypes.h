﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Reporter/Sample[]
// Reporter/Sample[]
struct  SampleU5BU5D_t2894  : public Array_t
{
};
// Reporter/Log[]
// Reporter/Log[]
struct  LogU5BU5D_t2900  : public Array_t
{
};
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem[]
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem[]
struct  DemoParticleSystemU5BU5D_t322  : public Array_t
{
};
// SCR_Waypoint[]
// SCR_Waypoint[]
struct  SCR_WaypointU5BU5D_t361  : public Array_t
{
};
// SCR_Wings[]
// SCR_Wings[]
struct  SCR_WingsU5BU5D_t342  : public Array_t
{
};
// SCR_Puceron[]
// SCR_Puceron[]
struct  SCR_PuceronU5BU5D_t2971  : public Array_t
{
};
// SCR_JoystickDirection[]
// SCR_JoystickDirection[]
struct  SCR_JoystickDirectionU5BU5D_t365  : public Array_t
{
};
// SCR_Menu[]
// SCR_Menu[]
struct  SCR_MenuU5BU5D_t3004  : public Array_t
{
};
