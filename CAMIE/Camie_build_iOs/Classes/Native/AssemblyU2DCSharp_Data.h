﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t242;
// System.Object
#include "mscorlib_System_Object.h"
// Data
struct  Data_t388  : public Object_t
{
	// System.Int32[] Data::highscores
	Int32U5BU5D_t242* ___highscores_0;
};
