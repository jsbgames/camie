﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Enumerator_t662;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t215;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t495;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14MethodDeclarations.h"
#define Enumerator__ctor_m17153(__this, ___dictionary, method) (( void (*) (Enumerator_t662 *, Dictionary_2_t495 *, const MethodInfo*))Enumerator__ctor_m17066_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17154(__this, method) (( Object_t * (*) (Enumerator_t662 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17067_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17155(__this, method) (( DictionaryEntry_t1430  (*) (Enumerator_t662 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17068_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17156(__this, method) (( Object_t * (*) (Enumerator_t662 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17069_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17157(__this, method) (( Object_t * (*) (Enumerator_t662 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17070_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m3115(__this, method) (( bool (*) (Enumerator_t662 *, const MethodInfo*))Enumerator_MoveNext_m17071_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m3112(__this, method) (( KeyValuePair_2_t661  (*) (Enumerator_t662 *, const MethodInfo*))Enumerator_get_Current_m17072_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17158(__this, method) (( int32_t (*) (Enumerator_t662 *, const MethodInfo*))Enumerator_get_CurrentKey_m17073_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17159(__this, method) (( PointerEventData_t215 * (*) (Enumerator_t662 *, const MethodInfo*))Enumerator_get_CurrentValue_m17074_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyState()
#define Enumerator_VerifyState_m17160(__this, method) (( void (*) (Enumerator_t662 *, const MethodInfo*))Enumerator_VerifyState_m17075_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17161(__this, method) (( void (*) (Enumerator_t662 *, const MethodInfo*))Enumerator_VerifyCurrent_m17076_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m17162(__this, method) (( void (*) (Enumerator_t662 *, const MethodInfo*))Enumerator_Dispose_m17077_gshared)(__this, method)
