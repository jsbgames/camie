﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Reporter/Log>
struct List_1_t298;
// System.Object
struct Object_t;
// Reporter/Log
struct Log_t291;
// System.Collections.Generic.IEnumerable`1<Reporter/Log>
struct IEnumerable_1_t3549;
// System.Collections.Generic.IEnumerator`1<Reporter/Log>
struct IEnumerator_1_t3550;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<Reporter/Log>
struct ICollection_1_t3551;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Reporter/Log>
struct ReadOnlyCollection_1_t2902;
// Reporter/Log[]
struct LogU5BU5D_t2900;
// System.Predicate`1<Reporter/Log>
struct Predicate_1_t2903;
// System.Comparison`1<Reporter/Log>
struct Comparison_1_t2905;
// System.Collections.Generic.List`1/Enumerator<Reporter/Log>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"

// System.Void System.Collections.Generic.List`1<Reporter/Log>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m1593(__this, method) (( void (*) (List_1_t298 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m14668(__this, ___collection, method) (( void (*) (List_1_t298 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::.ctor(System.Int32)
#define List_1__ctor_m14669(__this, ___capacity, method) (( void (*) (List_1_t298 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::.cctor()
#define List_1__cctor_m14670(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Reporter/Log>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14671(__this, method) (( Object_t* (*) (List_1_t298 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m14672(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t298 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m14673(__this, method) (( Object_t * (*) (List_1_t298 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m14674(__this, ___item, method) (( int32_t (*) (List_1_t298 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m14675(__this, ___item, method) (( bool (*) (List_1_t298 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m14676(__this, ___item, method) (( int32_t (*) (List_1_t298 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m14677(__this, ___index, ___item, method) (( void (*) (List_1_t298 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m14678(__this, ___item, method) (( void (*) (List_1_t298 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14679(__this, method) (( bool (*) (List_1_t298 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14680(__this, method) (( bool (*) (List_1_t298 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Reporter/Log>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m14681(__this, method) (( Object_t * (*) (List_1_t298 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m14682(__this, method) (( bool (*) (List_1_t298 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m14683(__this, method) (( bool (*) (List_1_t298 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m14684(__this, ___index, method) (( Object_t * (*) (List_1_t298 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m14685(__this, ___index, ___value, method) (( void (*) (List_1_t298 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Add(T)
#define List_1_Add_m14686(__this, ___item, method) (( void (*) (List_1_t298 *, Log_t291 *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m14687(__this, ___newCount, method) (( void (*) (List_1_t298 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m14688(__this, ___collection, method) (( void (*) (List_1_t298 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m14689(__this, ___enumerable, method) (( void (*) (List_1_t298 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m14690(__this, ___collection, method) (( void (*) (List_1_t298 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Reporter/Log>::AsReadOnly()
#define List_1_AsReadOnly_m14691(__this, method) (( ReadOnlyCollection_1_t2902 * (*) (List_1_t298 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Clear()
#define List_1_Clear_m14692(__this, method) (( void (*) (List_1_t298 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::Contains(T)
#define List_1_Contains_m14693(__this, ___item, method) (( bool (*) (List_1_t298 *, Log_t291 *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m14694(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t298 *, LogU5BU5D_t2900*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Reporter/Log>::Find(System.Predicate`1<T>)
#define List_1_Find_m14695(__this, ___match, method) (( Log_t291 * (*) (List_1_t298 *, Predicate_1_t2903 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m14696(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2903 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m14697(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t298 *, int32_t, int32_t, Predicate_1_t2903 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Reporter/Log>::GetEnumerator()
#define List_1_GetEnumerator_m14698(__this, method) (( Enumerator_t2904  (*) (List_1_t298 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::IndexOf(T)
#define List_1_IndexOf_m14699(__this, ___item, method) (( int32_t (*) (List_1_t298 *, Log_t291 *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m14700(__this, ___start, ___delta, method) (( void (*) (List_1_t298 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m14701(__this, ___index, method) (( void (*) (List_1_t298 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Insert(System.Int32,T)
#define List_1_Insert_m14702(__this, ___index, ___item, method) (( void (*) (List_1_t298 *, int32_t, Log_t291 *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m14703(__this, ___collection, method) (( void (*) (List_1_t298 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Reporter/Log>::Remove(T)
#define List_1_Remove_m14704(__this, ___item, method) (( bool (*) (List_1_t298 *, Log_t291 *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m14705(__this, ___match, method) (( int32_t (*) (List_1_t298 *, Predicate_1_t2903 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m14706(__this, ___index, method) (( void (*) (List_1_t298 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Reverse()
#define List_1_Reverse_m14707(__this, method) (( void (*) (List_1_t298 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Sort()
#define List_1_Sort_m14708(__this, method) (( void (*) (List_1_t298 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m14709(__this, ___comparison, method) (( void (*) (List_1_t298 *, Comparison_1_t2905 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Reporter/Log>::ToArray()
#define List_1_ToArray_m14710(__this, method) (( LogU5BU5D_t2900* (*) (List_1_t298 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::TrimExcess()
#define List_1_TrimExcess_m14711(__this, method) (( void (*) (List_1_t298 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Capacity()
#define List_1_get_Capacity_m14712(__this, method) (( int32_t (*) (List_1_t298 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m14713(__this, ___value, method) (( void (*) (List_1_t298 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Reporter/Log>::get_Count()
#define List_1_get_Count_m14714(__this, method) (( int32_t (*) (List_1_t298 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<Reporter/Log>::get_Item(System.Int32)
#define List_1_get_Item_m14715(__this, ___index, method) (( Log_t291 * (*) (List_1_t298 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Reporter/Log>::set_Item(System.Int32,T)
#define List_1_set_Item_m14716(__this, ___index, ___value, method) (( void (*) (List_1_t298 *, int32_t, Log_t291 *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
