﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
extern TypeInfo TlsServerCertificate_t1619_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4MethodDeclarations.h"
extern const Il2CppType Context_t1569_0_0_0;
extern const Il2CppType Context_t1569_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo TlsServerCertificate_t1619_TlsServerCertificate__ctor_m7446_ParameterInfos[] = 
{
	{"context", 0, 134218531, 0, &Context_t1569_0_0_0},
	{"buffer", 1, 134218532, 0, &ByteU5BU5D_t850_0_0_0},
};
extern const Il2CppType Void_t272_0_0_0;
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerCertificate__ctor_m7446_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerCertificate__ctor_m7446/* method */
	, &TlsServerCertificate_t1619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, TlsServerCertificate_t1619_TlsServerCertificate__ctor_m7446_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::Update()
extern const MethodInfo TlsServerCertificate_Update_m7447_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerCertificate_Update_m7447/* method */
	, &TlsServerCertificate_t1619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::ProcessAsSsl3()
extern const MethodInfo TlsServerCertificate_ProcessAsSsl3_m7448_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerCertificate_ProcessAsSsl3_m7448/* method */
	, &TlsServerCertificate_t1619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::ProcessAsTls1()
extern const MethodInfo TlsServerCertificate_ProcessAsTls1_m7449_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerCertificate_ProcessAsTls1_m7449/* method */
	, &TlsServerCertificate_t1619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1321_0_0_0;
extern const Il2CppType X509Certificate_t1321_0_0_0;
static const ParameterInfo TlsServerCertificate_t1619_TlsServerCertificate_checkCertificateUsage_m7450_ParameterInfos[] = 
{
	{"cert", 0, 134218533, 0, &X509Certificate_t1321_0_0_0},
};
extern const Il2CppType Boolean_t273_0_0_0;
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkCertificateUsage(Mono.Security.X509.X509Certificate)
extern const MethodInfo TlsServerCertificate_checkCertificateUsage_m7450_MethodInfo = 
{
	"checkCertificateUsage"/* name */
	, (methodPointerType)&TlsServerCertificate_checkCertificateUsage_m7450/* method */
	, &TlsServerCertificate_t1619_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, TlsServerCertificate_t1619_TlsServerCertificate_checkCertificateUsage_m7450_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1457_0_0_0;
extern const Il2CppType X509CertificateCollection_t1457_0_0_0;
static const ParameterInfo TlsServerCertificate_t1619_TlsServerCertificate_validateCertificates_m7451_ParameterInfos[] = 
{
	{"certificates", 0, 134218534, 0, &X509CertificateCollection_t1457_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::validateCertificates(Mono.Security.X509.X509CertificateCollection)
extern const MethodInfo TlsServerCertificate_validateCertificates_m7451_MethodInfo = 
{
	"validateCertificates"/* name */
	, (methodPointerType)&TlsServerCertificate_validateCertificates_m7451/* method */
	, &TlsServerCertificate_t1619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t/* invoker_method */
	, TlsServerCertificate_t1619_TlsServerCertificate_validateCertificates_m7451_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1321_0_0_0;
static const ParameterInfo TlsServerCertificate_t1619_TlsServerCertificate_checkServerIdentity_m7452_ParameterInfos[] = 
{
	{"cert", 0, 134218535, 0, &X509Certificate_t1321_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkServerIdentity(Mono.Security.X509.X509Certificate)
extern const MethodInfo TlsServerCertificate_checkServerIdentity_m7452_MethodInfo = 
{
	"checkServerIdentity"/* name */
	, (methodPointerType)&TlsServerCertificate_checkServerIdentity_m7452/* method */
	, &TlsServerCertificate_t1619_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, TlsServerCertificate_t1619_TlsServerCertificate_checkServerIdentity_m7452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TlsServerCertificate_t1619_TlsServerCertificate_checkDomainName_m7453_ParameterInfos[] = 
{
	{"subjectName", 0, 134218536, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkDomainName(System.String)
extern const MethodInfo TlsServerCertificate_checkDomainName_m7453_MethodInfo = 
{
	"checkDomainName"/* name */
	, (methodPointerType)&TlsServerCertificate_checkDomainName_m7453/* method */
	, &TlsServerCertificate_t1619_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, TlsServerCertificate_t1619_TlsServerCertificate_checkDomainName_m7453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TlsServerCertificate_t1619_TlsServerCertificate_Match_m7454_ParameterInfos[] = 
{
	{"hostname", 0, 134218537, 0, &String_t_0_0_0},
	{"pattern", 1, 134218538, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::Match(System.String,System.String)
extern const MethodInfo TlsServerCertificate_Match_m7454_MethodInfo = 
{
	"Match"/* name */
	, (methodPointerType)&TlsServerCertificate_Match_m7454/* method */
	, &TlsServerCertificate_t1619_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, TlsServerCertificate_t1619_TlsServerCertificate_Match_m7454_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerCertificate_t1619_MethodInfos[] =
{
	&TlsServerCertificate__ctor_m7446_MethodInfo,
	&TlsServerCertificate_Update_m7447_MethodInfo,
	&TlsServerCertificate_ProcessAsSsl3_m7448_MethodInfo,
	&TlsServerCertificate_ProcessAsTls1_m7449_MethodInfo,
	&TlsServerCertificate_checkCertificateUsage_m7450_MethodInfo,
	&TlsServerCertificate_validateCertificates_m7451_MethodInfo,
	&TlsServerCertificate_checkServerIdentity_m7452_MethodInfo,
	&TlsServerCertificate_checkDomainName_m7453_MethodInfo,
	&TlsServerCertificate_Match_m7454_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
extern const MethodInfo Stream_Dispose_m7616_MethodInfo;
extern const MethodInfo TlsStream_get_CanRead_m7387_MethodInfo;
extern const MethodInfo TlsStream_get_CanSeek_m7388_MethodInfo;
extern const MethodInfo TlsStream_get_CanWrite_m7386_MethodInfo;
extern const MethodInfo TlsStream_get_Length_m7391_MethodInfo;
extern const MethodInfo TlsStream_get_Position_m7389_MethodInfo;
extern const MethodInfo TlsStream_set_Position_m7390_MethodInfo;
extern const MethodInfo Stream_Dispose_m7569_MethodInfo;
extern const MethodInfo Stream_Close_m7568_MethodInfo;
extern const MethodInfo TlsStream_Flush_m7404_MethodInfo;
extern const MethodInfo TlsStream_Read_m7407_MethodInfo;
extern const MethodInfo Stream_ReadByte_m7617_MethodInfo;
extern const MethodInfo TlsStream_Seek_m7406_MethodInfo;
extern const MethodInfo TlsStream_SetLength_m7405_MethodInfo;
extern const MethodInfo TlsStream_Write_m7408_MethodInfo;
extern const MethodInfo Stream_WriteByte_m7618_MethodInfo;
extern const MethodInfo Stream_BeginRead_m7619_MethodInfo;
extern const MethodInfo Stream_BeginWrite_m7620_MethodInfo;
extern const MethodInfo Stream_EndRead_m7621_MethodInfo;
extern const MethodInfo Stream_EndWrite_m7622_MethodInfo;
extern const MethodInfo TlsServerCertificate_ProcessAsTls1_m7449_MethodInfo;
extern const MethodInfo TlsServerCertificate_ProcessAsSsl3_m7448_MethodInfo;
extern const MethodInfo TlsServerCertificate_Update_m7447_MethodInfo;
extern const MethodInfo HandshakeMessage_EncodeMessage_m7417_MethodInfo;
static const Il2CppMethodReference TlsServerCertificate_t1619_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Stream_Dispose_m7616_MethodInfo,
	&TlsStream_get_CanRead_m7387_MethodInfo,
	&TlsStream_get_CanSeek_m7388_MethodInfo,
	&TlsStream_get_CanWrite_m7386_MethodInfo,
	&TlsStream_get_Length_m7391_MethodInfo,
	&TlsStream_get_Position_m7389_MethodInfo,
	&TlsStream_set_Position_m7390_MethodInfo,
	&Stream_Dispose_m7569_MethodInfo,
	&Stream_Close_m7568_MethodInfo,
	&TlsStream_Flush_m7404_MethodInfo,
	&TlsStream_Read_m7407_MethodInfo,
	&Stream_ReadByte_m7617_MethodInfo,
	&TlsStream_Seek_m7406_MethodInfo,
	&TlsStream_SetLength_m7405_MethodInfo,
	&TlsStream_Write_m7408_MethodInfo,
	&Stream_WriteByte_m7618_MethodInfo,
	&Stream_BeginRead_m7619_MethodInfo,
	&Stream_BeginWrite_m7620_MethodInfo,
	&Stream_EndRead_m7621_MethodInfo,
	&Stream_EndWrite_m7622_MethodInfo,
	&TlsServerCertificate_ProcessAsTls1_m7449_MethodInfo,
	&TlsServerCertificate_ProcessAsSsl3_m7448_MethodInfo,
	&TlsServerCertificate_Update_m7447_MethodInfo,
	&HandshakeMessage_EncodeMessage_m7417_MethodInfo,
};
static bool TlsServerCertificate_t1619_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t233_0_0_0;
static Il2CppInterfaceOffsetPair TlsServerCertificate_t1619_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerCertificate_t1619_0_0_0;
extern const Il2CppType TlsServerCertificate_t1619_1_0_0;
extern const Il2CppType HandshakeMessage_t1593_0_0_0;
struct TlsServerCertificate_t1619;
const Il2CppTypeDefinitionMetadata TlsServerCertificate_t1619_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificate_t1619_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1593_0_0_0/* parent */
	, TlsServerCertificate_t1619_VTable/* vtableMethods */
	, TlsServerCertificate_t1619_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 458/* fieldStart */

};
TypeInfo TlsServerCertificate_t1619_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificate"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerCertificate_t1619_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerCertificate_t1619_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificate_t1619_0_0_0/* byval_arg */
	, &TlsServerCertificate_t1619_1_0_0/* this_arg */
	, &TlsServerCertificate_t1619_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificate_t1619)/* instance_size */
	, sizeof (TlsServerCertificate_t1619)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
extern TypeInfo TlsServerCertificateRequest_t1620_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5MethodDeclarations.h"
extern const Il2CppType Context_t1569_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo TlsServerCertificateRequest_t1620_TlsServerCertificateRequest__ctor_m7455_ParameterInfos[] = 
{
	{"context", 0, 134218539, 0, &Context_t1569_0_0_0},
	{"buffer", 1, 134218540, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerCertificateRequest__ctor_m7455_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerCertificateRequest__ctor_m7455/* method */
	, &TlsServerCertificateRequest_t1620_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, TlsServerCertificateRequest_t1620_TlsServerCertificateRequest__ctor_m7455_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::Update()
extern const MethodInfo TlsServerCertificateRequest_Update_m7456_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_Update_m7456/* method */
	, &TlsServerCertificateRequest_t1620_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::ProcessAsSsl3()
extern const MethodInfo TlsServerCertificateRequest_ProcessAsSsl3_m7457_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_ProcessAsSsl3_m7457/* method */
	, &TlsServerCertificateRequest_t1620_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::ProcessAsTls1()
extern const MethodInfo TlsServerCertificateRequest_ProcessAsTls1_m7458_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_ProcessAsTls1_m7458/* method */
	, &TlsServerCertificateRequest_t1620_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerCertificateRequest_t1620_MethodInfos[] =
{
	&TlsServerCertificateRequest__ctor_m7455_MethodInfo,
	&TlsServerCertificateRequest_Update_m7456_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsSsl3_m7457_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsTls1_m7458_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerCertificateRequest_ProcessAsTls1_m7458_MethodInfo;
extern const MethodInfo TlsServerCertificateRequest_ProcessAsSsl3_m7457_MethodInfo;
extern const MethodInfo TlsServerCertificateRequest_Update_m7456_MethodInfo;
static const Il2CppMethodReference TlsServerCertificateRequest_t1620_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Stream_Dispose_m7616_MethodInfo,
	&TlsStream_get_CanRead_m7387_MethodInfo,
	&TlsStream_get_CanSeek_m7388_MethodInfo,
	&TlsStream_get_CanWrite_m7386_MethodInfo,
	&TlsStream_get_Length_m7391_MethodInfo,
	&TlsStream_get_Position_m7389_MethodInfo,
	&TlsStream_set_Position_m7390_MethodInfo,
	&Stream_Dispose_m7569_MethodInfo,
	&Stream_Close_m7568_MethodInfo,
	&TlsStream_Flush_m7404_MethodInfo,
	&TlsStream_Read_m7407_MethodInfo,
	&Stream_ReadByte_m7617_MethodInfo,
	&TlsStream_Seek_m7406_MethodInfo,
	&TlsStream_SetLength_m7405_MethodInfo,
	&TlsStream_Write_m7408_MethodInfo,
	&Stream_WriteByte_m7618_MethodInfo,
	&Stream_BeginRead_m7619_MethodInfo,
	&Stream_BeginWrite_m7620_MethodInfo,
	&Stream_EndRead_m7621_MethodInfo,
	&Stream_EndWrite_m7622_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsTls1_m7458_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsSsl3_m7457_MethodInfo,
	&TlsServerCertificateRequest_Update_m7456_MethodInfo,
	&HandshakeMessage_EncodeMessage_m7417_MethodInfo,
};
static bool TlsServerCertificateRequest_t1620_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerCertificateRequest_t1620_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerCertificateRequest_t1620_0_0_0;
extern const Il2CppType TlsServerCertificateRequest_t1620_1_0_0;
struct TlsServerCertificateRequest_t1620;
const Il2CppTypeDefinitionMetadata TlsServerCertificateRequest_t1620_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificateRequest_t1620_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1593_0_0_0/* parent */
	, TlsServerCertificateRequest_t1620_VTable/* vtableMethods */
	, TlsServerCertificateRequest_t1620_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 459/* fieldStart */

};
TypeInfo TlsServerCertificateRequest_t1620_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificateRequest"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerCertificateRequest_t1620_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerCertificateRequest_t1620_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificateRequest_t1620_0_0_0/* byval_arg */
	, &TlsServerCertificateRequest_t1620_1_0_0/* this_arg */
	, &TlsServerCertificateRequest_t1620_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificateRequest_t1620)/* instance_size */
	, sizeof (TlsServerCertificateRequest_t1620)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
extern TypeInfo TlsServerFinished_t1621_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6MethodDeclarations.h"
extern const Il2CppType Context_t1569_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo TlsServerFinished_t1621_TlsServerFinished__ctor_m7459_ParameterInfos[] = 
{
	{"context", 0, 134218541, 0, &Context_t1569_0_0_0},
	{"buffer", 1, 134218542, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerFinished__ctor_m7459_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerFinished__ctor_m7459/* method */
	, &TlsServerFinished_t1621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, TlsServerFinished_t1621_TlsServerFinished__ctor_m7459_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::.cctor()
extern const MethodInfo TlsServerFinished__cctor_m7460_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TlsServerFinished__cctor_m7460/* method */
	, &TlsServerFinished_t1621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::Update()
extern const MethodInfo TlsServerFinished_Update_m7461_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerFinished_Update_m7461/* method */
	, &TlsServerFinished_t1621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::ProcessAsSsl3()
extern const MethodInfo TlsServerFinished_ProcessAsSsl3_m7462_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerFinished_ProcessAsSsl3_m7462/* method */
	, &TlsServerFinished_t1621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::ProcessAsTls1()
extern const MethodInfo TlsServerFinished_ProcessAsTls1_m7463_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerFinished_ProcessAsTls1_m7463/* method */
	, &TlsServerFinished_t1621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerFinished_t1621_MethodInfos[] =
{
	&TlsServerFinished__ctor_m7459_MethodInfo,
	&TlsServerFinished__cctor_m7460_MethodInfo,
	&TlsServerFinished_Update_m7461_MethodInfo,
	&TlsServerFinished_ProcessAsSsl3_m7462_MethodInfo,
	&TlsServerFinished_ProcessAsTls1_m7463_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerFinished_ProcessAsTls1_m7463_MethodInfo;
extern const MethodInfo TlsServerFinished_ProcessAsSsl3_m7462_MethodInfo;
extern const MethodInfo TlsServerFinished_Update_m7461_MethodInfo;
static const Il2CppMethodReference TlsServerFinished_t1621_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Stream_Dispose_m7616_MethodInfo,
	&TlsStream_get_CanRead_m7387_MethodInfo,
	&TlsStream_get_CanSeek_m7388_MethodInfo,
	&TlsStream_get_CanWrite_m7386_MethodInfo,
	&TlsStream_get_Length_m7391_MethodInfo,
	&TlsStream_get_Position_m7389_MethodInfo,
	&TlsStream_set_Position_m7390_MethodInfo,
	&Stream_Dispose_m7569_MethodInfo,
	&Stream_Close_m7568_MethodInfo,
	&TlsStream_Flush_m7404_MethodInfo,
	&TlsStream_Read_m7407_MethodInfo,
	&Stream_ReadByte_m7617_MethodInfo,
	&TlsStream_Seek_m7406_MethodInfo,
	&TlsStream_SetLength_m7405_MethodInfo,
	&TlsStream_Write_m7408_MethodInfo,
	&Stream_WriteByte_m7618_MethodInfo,
	&Stream_BeginRead_m7619_MethodInfo,
	&Stream_BeginWrite_m7620_MethodInfo,
	&Stream_EndRead_m7621_MethodInfo,
	&Stream_EndWrite_m7622_MethodInfo,
	&TlsServerFinished_ProcessAsTls1_m7463_MethodInfo,
	&TlsServerFinished_ProcessAsSsl3_m7462_MethodInfo,
	&TlsServerFinished_Update_m7461_MethodInfo,
	&HandshakeMessage_EncodeMessage_m7417_MethodInfo,
};
static bool TlsServerFinished_t1621_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerFinished_t1621_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerFinished_t1621_0_0_0;
extern const Il2CppType TlsServerFinished_t1621_1_0_0;
struct TlsServerFinished_t1621;
const Il2CppTypeDefinitionMetadata TlsServerFinished_t1621_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerFinished_t1621_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1593_0_0_0/* parent */
	, TlsServerFinished_t1621_VTable/* vtableMethods */
	, TlsServerFinished_t1621_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 461/* fieldStart */

};
TypeInfo TlsServerFinished_t1621_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerFinished"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerFinished_t1621_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerFinished_t1621_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerFinished_t1621_0_0_0/* byval_arg */
	, &TlsServerFinished_t1621_1_0_0/* this_arg */
	, &TlsServerFinished_t1621_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerFinished_t1621)/* instance_size */
	, sizeof (TlsServerFinished_t1621)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TlsServerFinished_t1621_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
extern TypeInfo TlsServerHello_t1622_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7MethodDeclarations.h"
extern const Il2CppType Context_t1569_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo TlsServerHello_t1622_TlsServerHello__ctor_m7464_ParameterInfos[] = 
{
	{"context", 0, 134218543, 0, &Context_t1569_0_0_0},
	{"buffer", 1, 134218544, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerHello__ctor_m7464_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerHello__ctor_m7464/* method */
	, &TlsServerHello_t1622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, TlsServerHello_t1622_TlsServerHello__ctor_m7464_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::Update()
extern const MethodInfo TlsServerHello_Update_m7465_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerHello_Update_m7465/* method */
	, &TlsServerHello_t1622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::ProcessAsSsl3()
extern const MethodInfo TlsServerHello_ProcessAsSsl3_m7466_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerHello_ProcessAsSsl3_m7466/* method */
	, &TlsServerHello_t1622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::ProcessAsTls1()
extern const MethodInfo TlsServerHello_ProcessAsTls1_m7467_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerHello_ProcessAsTls1_m7467/* method */
	, &TlsServerHello_t1622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int16_t752_0_0_0;
extern const Il2CppType Int16_t752_0_0_0;
static const ParameterInfo TlsServerHello_t1622_TlsServerHello_processProtocol_m7468_ParameterInfos[] = 
{
	{"protocol", 0, 134218545, 0, &Int16_t752_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Int16_t752 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::processProtocol(System.Int16)
extern const MethodInfo TlsServerHello_processProtocol_m7468_MethodInfo = 
{
	"processProtocol"/* name */
	, (methodPointerType)&TlsServerHello_processProtocol_m7468/* method */
	, &TlsServerHello_t1622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Int16_t752/* invoker_method */
	, TlsServerHello_t1622_TlsServerHello_processProtocol_m7468_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerHello_t1622_MethodInfos[] =
{
	&TlsServerHello__ctor_m7464_MethodInfo,
	&TlsServerHello_Update_m7465_MethodInfo,
	&TlsServerHello_ProcessAsSsl3_m7466_MethodInfo,
	&TlsServerHello_ProcessAsTls1_m7467_MethodInfo,
	&TlsServerHello_processProtocol_m7468_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerHello_ProcessAsTls1_m7467_MethodInfo;
extern const MethodInfo TlsServerHello_ProcessAsSsl3_m7466_MethodInfo;
extern const MethodInfo TlsServerHello_Update_m7465_MethodInfo;
static const Il2CppMethodReference TlsServerHello_t1622_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Stream_Dispose_m7616_MethodInfo,
	&TlsStream_get_CanRead_m7387_MethodInfo,
	&TlsStream_get_CanSeek_m7388_MethodInfo,
	&TlsStream_get_CanWrite_m7386_MethodInfo,
	&TlsStream_get_Length_m7391_MethodInfo,
	&TlsStream_get_Position_m7389_MethodInfo,
	&TlsStream_set_Position_m7390_MethodInfo,
	&Stream_Dispose_m7569_MethodInfo,
	&Stream_Close_m7568_MethodInfo,
	&TlsStream_Flush_m7404_MethodInfo,
	&TlsStream_Read_m7407_MethodInfo,
	&Stream_ReadByte_m7617_MethodInfo,
	&TlsStream_Seek_m7406_MethodInfo,
	&TlsStream_SetLength_m7405_MethodInfo,
	&TlsStream_Write_m7408_MethodInfo,
	&Stream_WriteByte_m7618_MethodInfo,
	&Stream_BeginRead_m7619_MethodInfo,
	&Stream_BeginWrite_m7620_MethodInfo,
	&Stream_EndRead_m7621_MethodInfo,
	&Stream_EndWrite_m7622_MethodInfo,
	&TlsServerHello_ProcessAsTls1_m7467_MethodInfo,
	&TlsServerHello_ProcessAsSsl3_m7466_MethodInfo,
	&TlsServerHello_Update_m7465_MethodInfo,
	&HandshakeMessage_EncodeMessage_m7417_MethodInfo,
};
static bool TlsServerHello_t1622_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerHello_t1622_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerHello_t1622_0_0_0;
extern const Il2CppType TlsServerHello_t1622_1_0_0;
struct TlsServerHello_t1622;
const Il2CppTypeDefinitionMetadata TlsServerHello_t1622_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHello_t1622_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1593_0_0_0/* parent */
	, TlsServerHello_t1622_VTable/* vtableMethods */
	, TlsServerHello_t1622_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 462/* fieldStart */

};
TypeInfo TlsServerHello_t1622_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHello"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerHello_t1622_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerHello_t1622_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHello_t1622_0_0_0/* byval_arg */
	, &TlsServerHello_t1622_1_0_0/* this_arg */
	, &TlsServerHello_t1622_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHello_t1622)/* instance_size */
	, sizeof (TlsServerHello_t1622)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
extern TypeInfo TlsServerHelloDone_t1623_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8MethodDeclarations.h"
extern const Il2CppType Context_t1569_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo TlsServerHelloDone_t1623_TlsServerHelloDone__ctor_m7469_ParameterInfos[] = 
{
	{"context", 0, 134218546, 0, &Context_t1569_0_0_0},
	{"buffer", 1, 134218547, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerHelloDone__ctor_m7469_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerHelloDone__ctor_m7469/* method */
	, &TlsServerHelloDone_t1623_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, TlsServerHelloDone_t1623_TlsServerHelloDone__ctor_m7469_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::ProcessAsSsl3()
extern const MethodInfo TlsServerHelloDone_ProcessAsSsl3_m7470_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerHelloDone_ProcessAsSsl3_m7470/* method */
	, &TlsServerHelloDone_t1623_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::ProcessAsTls1()
extern const MethodInfo TlsServerHelloDone_ProcessAsTls1_m7471_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerHelloDone_ProcessAsTls1_m7471/* method */
	, &TlsServerHelloDone_t1623_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerHelloDone_t1623_MethodInfos[] =
{
	&TlsServerHelloDone__ctor_m7469_MethodInfo,
	&TlsServerHelloDone_ProcessAsSsl3_m7470_MethodInfo,
	&TlsServerHelloDone_ProcessAsTls1_m7471_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerHelloDone_ProcessAsTls1_m7471_MethodInfo;
extern const MethodInfo TlsServerHelloDone_ProcessAsSsl3_m7470_MethodInfo;
extern const MethodInfo HandshakeMessage_Update_m7416_MethodInfo;
static const Il2CppMethodReference TlsServerHelloDone_t1623_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Stream_Dispose_m7616_MethodInfo,
	&TlsStream_get_CanRead_m7387_MethodInfo,
	&TlsStream_get_CanSeek_m7388_MethodInfo,
	&TlsStream_get_CanWrite_m7386_MethodInfo,
	&TlsStream_get_Length_m7391_MethodInfo,
	&TlsStream_get_Position_m7389_MethodInfo,
	&TlsStream_set_Position_m7390_MethodInfo,
	&Stream_Dispose_m7569_MethodInfo,
	&Stream_Close_m7568_MethodInfo,
	&TlsStream_Flush_m7404_MethodInfo,
	&TlsStream_Read_m7407_MethodInfo,
	&Stream_ReadByte_m7617_MethodInfo,
	&TlsStream_Seek_m7406_MethodInfo,
	&TlsStream_SetLength_m7405_MethodInfo,
	&TlsStream_Write_m7408_MethodInfo,
	&Stream_WriteByte_m7618_MethodInfo,
	&Stream_BeginRead_m7619_MethodInfo,
	&Stream_BeginWrite_m7620_MethodInfo,
	&Stream_EndRead_m7621_MethodInfo,
	&Stream_EndWrite_m7622_MethodInfo,
	&TlsServerHelloDone_ProcessAsTls1_m7471_MethodInfo,
	&TlsServerHelloDone_ProcessAsSsl3_m7470_MethodInfo,
	&HandshakeMessage_Update_m7416_MethodInfo,
	&HandshakeMessage_EncodeMessage_m7417_MethodInfo,
};
static bool TlsServerHelloDone_t1623_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerHelloDone_t1623_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerHelloDone_t1623_0_0_0;
extern const Il2CppType TlsServerHelloDone_t1623_1_0_0;
struct TlsServerHelloDone_t1623;
const Il2CppTypeDefinitionMetadata TlsServerHelloDone_t1623_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHelloDone_t1623_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1593_0_0_0/* parent */
	, TlsServerHelloDone_t1623_VTable/* vtableMethods */
	, TlsServerHelloDone_t1623_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TlsServerHelloDone_t1623_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHelloDone"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerHelloDone_t1623_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerHelloDone_t1623_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHelloDone_t1623_0_0_0/* byval_arg */
	, &TlsServerHelloDone_t1623_1_0_0/* this_arg */
	, &TlsServerHelloDone_t1623_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHelloDone_t1623)/* instance_size */
	, sizeof (TlsServerHelloDone_t1623)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
extern TypeInfo TlsServerKeyExchange_t1624_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9MethodDeclarations.h"
extern const Il2CppType Context_t1569_0_0_0;
extern const Il2CppType ByteU5BU5D_t850_0_0_0;
static const ParameterInfo TlsServerKeyExchange_t1624_TlsServerKeyExchange__ctor_m7472_ParameterInfos[] = 
{
	{"context", 0, 134218548, 0, &Context_t1569_0_0_0},
	{"buffer", 1, 134218549, 0, &ByteU5BU5D_t850_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerKeyExchange__ctor_m7472_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerKeyExchange__ctor_m7472/* method */
	, &TlsServerKeyExchange_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, TlsServerKeyExchange_t1624_TlsServerKeyExchange__ctor_m7472_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::Update()
extern const MethodInfo TlsServerKeyExchange_Update_m7473_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerKeyExchange_Update_m7473/* method */
	, &TlsServerKeyExchange_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::ProcessAsSsl3()
extern const MethodInfo TlsServerKeyExchange_ProcessAsSsl3_m7474_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerKeyExchange_ProcessAsSsl3_m7474/* method */
	, &TlsServerKeyExchange_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::ProcessAsTls1()
extern const MethodInfo TlsServerKeyExchange_ProcessAsTls1_m7475_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerKeyExchange_ProcessAsTls1_m7475/* method */
	, &TlsServerKeyExchange_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::verifySignature()
extern const MethodInfo TlsServerKeyExchange_verifySignature_m7476_MethodInfo = 
{
	"verifySignature"/* name */
	, (methodPointerType)&TlsServerKeyExchange_verifySignature_m7476/* method */
	, &TlsServerKeyExchange_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerKeyExchange_t1624_MethodInfos[] =
{
	&TlsServerKeyExchange__ctor_m7472_MethodInfo,
	&TlsServerKeyExchange_Update_m7473_MethodInfo,
	&TlsServerKeyExchange_ProcessAsSsl3_m7474_MethodInfo,
	&TlsServerKeyExchange_ProcessAsTls1_m7475_MethodInfo,
	&TlsServerKeyExchange_verifySignature_m7476_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerKeyExchange_ProcessAsTls1_m7475_MethodInfo;
extern const MethodInfo TlsServerKeyExchange_ProcessAsSsl3_m7474_MethodInfo;
extern const MethodInfo TlsServerKeyExchange_Update_m7473_MethodInfo;
static const Il2CppMethodReference TlsServerKeyExchange_t1624_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&Stream_Dispose_m7616_MethodInfo,
	&TlsStream_get_CanRead_m7387_MethodInfo,
	&TlsStream_get_CanSeek_m7388_MethodInfo,
	&TlsStream_get_CanWrite_m7386_MethodInfo,
	&TlsStream_get_Length_m7391_MethodInfo,
	&TlsStream_get_Position_m7389_MethodInfo,
	&TlsStream_set_Position_m7390_MethodInfo,
	&Stream_Dispose_m7569_MethodInfo,
	&Stream_Close_m7568_MethodInfo,
	&TlsStream_Flush_m7404_MethodInfo,
	&TlsStream_Read_m7407_MethodInfo,
	&Stream_ReadByte_m7617_MethodInfo,
	&TlsStream_Seek_m7406_MethodInfo,
	&TlsStream_SetLength_m7405_MethodInfo,
	&TlsStream_Write_m7408_MethodInfo,
	&Stream_WriteByte_m7618_MethodInfo,
	&Stream_BeginRead_m7619_MethodInfo,
	&Stream_BeginWrite_m7620_MethodInfo,
	&Stream_EndRead_m7621_MethodInfo,
	&Stream_EndWrite_m7622_MethodInfo,
	&TlsServerKeyExchange_ProcessAsTls1_m7475_MethodInfo,
	&TlsServerKeyExchange_ProcessAsSsl3_m7474_MethodInfo,
	&TlsServerKeyExchange_Update_m7473_MethodInfo,
	&HandshakeMessage_EncodeMessage_m7417_MethodInfo,
};
static bool TlsServerKeyExchange_t1624_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerKeyExchange_t1624_InterfacesOffsets[] = 
{
	{ &IDisposable_t233_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerKeyExchange_t1624_0_0_0;
extern const Il2CppType TlsServerKeyExchange_t1624_1_0_0;
struct TlsServerKeyExchange_t1624;
const Il2CppTypeDefinitionMetadata TlsServerKeyExchange_t1624_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerKeyExchange_t1624_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1593_0_0_0/* parent */
	, TlsServerKeyExchange_t1624_VTable/* vtableMethods */
	, TlsServerKeyExchange_t1624_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 466/* fieldStart */

};
TypeInfo TlsServerKeyExchange_t1624_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerKeyExchange"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerKeyExchange_t1624_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerKeyExchange_t1624_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerKeyExchange_t1624_0_0_0/* byval_arg */
	, &TlsServerKeyExchange_t1624_1_0_0/* this_arg */
	, &TlsServerKeyExchange_t1624_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerKeyExchange_t1624)/* instance_size */
	, sizeof (TlsServerKeyExchange_t1624)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTest.h"
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern TypeInfo PrimalityTest_t1625_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrimalityTest_t1625_PrimalityTest__ctor_m7477_ParameterInfos[] = 
{
	{"object", 0, 134218550, 0, &Object_t_0_0_0},
	{"method", 1, 134218551, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Math.Prime.PrimalityTest::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrimalityTest__ctor_m7477_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrimalityTest__ctor_m7477/* method */
	, &PrimalityTest_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, PrimalityTest_t1625_PrimalityTest__ctor_m7477_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1520_0_0_0;
extern const Il2CppType BigInteger_t1520_0_0_0;
extern const Il2CppType ConfidenceFactor_t1525_0_0_0;
extern const Il2CppType ConfidenceFactor_t1525_0_0_0;
static const ParameterInfo PrimalityTest_t1625_PrimalityTest_Invoke_m7478_ParameterInfos[] = 
{
	{"bi", 0, 134218552, 0, &BigInteger_t1520_0_0_0},
	{"confidence", 1, 134218553, 0, &ConfidenceFactor_t1525_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Int32_t253 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::Invoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor)
extern const MethodInfo PrimalityTest_Invoke_m7478_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrimalityTest_Invoke_m7478/* method */
	, &PrimalityTest_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Int32_t253/* invoker_method */
	, PrimalityTest_t1625_PrimalityTest_Invoke_m7478_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1520_0_0_0;
extern const Il2CppType ConfidenceFactor_t1525_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrimalityTest_t1625_PrimalityTest_BeginInvoke_m7479_ParameterInfos[] = 
{
	{"bi", 0, 134218554, 0, &BigInteger_t1520_0_0_0},
	{"confidence", 1, 134218555, 0, &ConfidenceFactor_t1525_0_0_0},
	{"callback", 2, 134218556, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134218557, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t546_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Math.Prime.PrimalityTest::BeginInvoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor,System.AsyncCallback,System.Object)
extern const MethodInfo PrimalityTest_BeginInvoke_m7479_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrimalityTest_BeginInvoke_m7479/* method */
	, &PrimalityTest_t1625_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t253_Object_t_Object_t/* invoker_method */
	, PrimalityTest_t1625_PrimalityTest_BeginInvoke_m7479_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo PrimalityTest_t1625_PrimalityTest_EndInvoke_m7480_ParameterInfos[] = 
{
	{"result", 0, 134218558, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrimalityTest_EndInvoke_m7480_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrimalityTest_EndInvoke_m7480/* method */
	, &PrimalityTest_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, PrimalityTest_t1625_PrimalityTest_EndInvoke_m7480_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrimalityTest_t1625_MethodInfos[] =
{
	&PrimalityTest__ctor_m7477_MethodInfo,
	&PrimalityTest_Invoke_m7478_MethodInfo,
	&PrimalityTest_BeginInvoke_m7479_MethodInfo,
	&PrimalityTest_EndInvoke_m7480_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m3586_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m3587_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m3588_MethodInfo;
extern const MethodInfo Delegate_Clone_m3589_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m3590_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m3591_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m3592_MethodInfo;
extern const MethodInfo PrimalityTest_Invoke_m7478_MethodInfo;
extern const MethodInfo PrimalityTest_BeginInvoke_m7479_MethodInfo;
extern const MethodInfo PrimalityTest_EndInvoke_m7480_MethodInfo;
static const Il2CppMethodReference PrimalityTest_t1625_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&PrimalityTest_Invoke_m7478_MethodInfo,
	&PrimalityTest_BeginInvoke_m7479_MethodInfo,
	&PrimalityTest_EndInvoke_m7480_MethodInfo,
};
static bool PrimalityTest_t1625_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t733_0_0_0;
extern const Il2CppType ISerializable_t439_0_0_0;
static Il2CppInterfaceOffsetPair PrimalityTest_t1625_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrimalityTest_t1625_0_0_0;
extern const Il2CppType PrimalityTest_t1625_1_0_0;
extern const Il2CppType MulticastDelegate_t549_0_0_0;
struct PrimalityTest_t1625;
const Il2CppTypeDefinitionMetadata PrimalityTest_t1625_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrimalityTest_t1625_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, PrimalityTest_t1625_VTable/* vtableMethods */
	, PrimalityTest_t1625_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrimalityTest_t1625_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTest"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, PrimalityTest_t1625_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrimalityTest_t1625_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTest_t1625_0_0_0/* byval_arg */
	, &PrimalityTest_t1625_1_0_0/* this_arg */
	, &PrimalityTest_t1625_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrimalityTest_t1625/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTest_t1625)/* instance_size */
	, sizeof (PrimalityTest_t1625)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback
extern TypeInfo CertificateValidationCallback_t1603_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidatiMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1603_CertificateValidationCallback__ctor_m7481_ParameterInfos[] = 
{
	{"object", 0, 134218559, 0, &Object_t_0_0_0},
	{"method", 1, 134218560, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateValidationCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateValidationCallback__ctor_m7481_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateValidationCallback__ctor_m7481/* method */
	, &CertificateValidationCallback_t1603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, CertificateValidationCallback_t1603_CertificateValidationCallback__ctor_m7481_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1323_0_0_0;
extern const Il2CppType X509Certificate_t1323_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1603_CertificateValidationCallback_Invoke_m7482_ParameterInfos[] = 
{
	{"certificate", 0, 134218561, 0, &X509Certificate_t1323_0_0_0},
	{"certificateErrors", 1, 134218562, 0, &Int32U5BU5D_t242_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.CertificateValidationCallback::Invoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[])
extern const MethodInfo CertificateValidationCallback_Invoke_m7482_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_Invoke_m7482/* method */
	, &CertificateValidationCallback_t1603_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback_t1603_CertificateValidationCallback_Invoke_m7482_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1323_0_0_0;
extern const Il2CppType Int32U5BU5D_t242_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1603_CertificateValidationCallback_BeginInvoke_m7483_ParameterInfos[] = 
{
	{"certificate", 0, 134218563, 0, &X509Certificate_t1323_0_0_0},
	{"certificateErrors", 1, 134218564, 0, &Int32U5BU5D_t242_0_0_0},
	{"callback", 2, 134218565, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134218566, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateValidationCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[],System.AsyncCallback,System.Object)
extern const MethodInfo CertificateValidationCallback_BeginInvoke_m7483_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_BeginInvoke_m7483/* method */
	, &CertificateValidationCallback_t1603_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback_t1603_CertificateValidationCallback_BeginInvoke_m7483_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1603_CertificateValidationCallback_EndInvoke_m7484_ParameterInfos[] = 
{
	{"result", 0, 134218567, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.CertificateValidationCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateValidationCallback_EndInvoke_m7484_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_EndInvoke_m7484/* method */
	, &CertificateValidationCallback_t1603_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t273_Object_t/* invoker_method */
	, CertificateValidationCallback_t1603_CertificateValidationCallback_EndInvoke_m7484_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateValidationCallback_t1603_MethodInfos[] =
{
	&CertificateValidationCallback__ctor_m7481_MethodInfo,
	&CertificateValidationCallback_Invoke_m7482_MethodInfo,
	&CertificateValidationCallback_BeginInvoke_m7483_MethodInfo,
	&CertificateValidationCallback_EndInvoke_m7484_MethodInfo,
	NULL
};
extern const MethodInfo CertificateValidationCallback_Invoke_m7482_MethodInfo;
extern const MethodInfo CertificateValidationCallback_BeginInvoke_m7483_MethodInfo;
extern const MethodInfo CertificateValidationCallback_EndInvoke_m7484_MethodInfo;
static const Il2CppMethodReference CertificateValidationCallback_t1603_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&CertificateValidationCallback_Invoke_m7482_MethodInfo,
	&CertificateValidationCallback_BeginInvoke_m7483_MethodInfo,
	&CertificateValidationCallback_EndInvoke_m7484_MethodInfo,
};
static bool CertificateValidationCallback_t1603_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback_t1603_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateValidationCallback_t1603_0_0_0;
extern const Il2CppType CertificateValidationCallback_t1603_1_0_0;
struct CertificateValidationCallback_t1603;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback_t1603_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback_t1603_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, CertificateValidationCallback_t1603_VTable/* vtableMethods */
	, CertificateValidationCallback_t1603_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateValidationCallback_t1603_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateValidationCallback_t1603_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateValidationCallback_t1603_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback_t1603_0_0_0/* byval_arg */
	, &CertificateValidationCallback_t1603_1_0_0/* this_arg */
	, &CertificateValidationCallback_t1603_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback_t1603/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback_t1603)/* instance_size */
	, sizeof (CertificateValidationCallback_t1603)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback2
extern TypeInfo CertificateValidationCallback2_t1604_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0MethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1604_CertificateValidationCallback2__ctor_m7485_ParameterInfos[] = 
{
	{"object", 0, 134218568, 0, &Object_t_0_0_0},
	{"method", 1, 134218569, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateValidationCallback2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateValidationCallback2__ctor_m7485_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateValidationCallback2__ctor_m7485/* method */
	, &CertificateValidationCallback2_t1604_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, CertificateValidationCallback2_t1604_CertificateValidationCallback2__ctor_m7485_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1457_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1604_CertificateValidationCallback2_Invoke_m7486_ParameterInfos[] = 
{
	{"collection", 0, 134218570, 0, &X509CertificateCollection_t1457_0_0_0},
};
extern const Il2CppType ValidationResult_t1602_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Mono.Security.Protocol.Tls.ValidationResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::Invoke(Mono.Security.X509.X509CertificateCollection)
extern const MethodInfo CertificateValidationCallback2_Invoke_m7486_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_Invoke_m7486/* method */
	, &CertificateValidationCallback2_t1604_il2cpp_TypeInfo/* declaring_type */
	, &ValidationResult_t1602_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1604_CertificateValidationCallback2_Invoke_m7486_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1457_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1604_CertificateValidationCallback2_BeginInvoke_m7487_ParameterInfos[] = 
{
	{"collection", 0, 134218571, 0, &X509CertificateCollection_t1457_0_0_0},
	{"callback", 1, 134218572, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134218573, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::BeginInvoke(Mono.Security.X509.X509CertificateCollection,System.AsyncCallback,System.Object)
extern const MethodInfo CertificateValidationCallback2_BeginInvoke_m7487_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_BeginInvoke_m7487/* method */
	, &CertificateValidationCallback2_t1604_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1604_CertificateValidationCallback2_BeginInvoke_m7487_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1604_CertificateValidationCallback2_EndInvoke_m7488_ParameterInfos[] = 
{
	{"result", 0, 134218574, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Mono.Security.Protocol.Tls.ValidationResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateValidationCallback2_EndInvoke_m7488_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_EndInvoke_m7488/* method */
	, &CertificateValidationCallback2_t1604_il2cpp_TypeInfo/* declaring_type */
	, &ValidationResult_t1602_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1604_CertificateValidationCallback2_EndInvoke_m7488_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateValidationCallback2_t1604_MethodInfos[] =
{
	&CertificateValidationCallback2__ctor_m7485_MethodInfo,
	&CertificateValidationCallback2_Invoke_m7486_MethodInfo,
	&CertificateValidationCallback2_BeginInvoke_m7487_MethodInfo,
	&CertificateValidationCallback2_EndInvoke_m7488_MethodInfo,
	NULL
};
extern const MethodInfo CertificateValidationCallback2_Invoke_m7486_MethodInfo;
extern const MethodInfo CertificateValidationCallback2_BeginInvoke_m7487_MethodInfo;
extern const MethodInfo CertificateValidationCallback2_EndInvoke_m7488_MethodInfo;
static const Il2CppMethodReference CertificateValidationCallback2_t1604_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&CertificateValidationCallback2_Invoke_m7486_MethodInfo,
	&CertificateValidationCallback2_BeginInvoke_m7487_MethodInfo,
	&CertificateValidationCallback2_EndInvoke_m7488_MethodInfo,
};
static bool CertificateValidationCallback2_t1604_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback2_t1604_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateValidationCallback2_t1604_0_0_0;
extern const Il2CppType CertificateValidationCallback2_t1604_1_0_0;
struct CertificateValidationCallback2_t1604;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback2_t1604_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback2_t1604_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, CertificateValidationCallback2_t1604_VTable/* vtableMethods */
	, CertificateValidationCallback2_t1604_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateValidationCallback2_t1604_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback2"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateValidationCallback2_t1604_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateValidationCallback2_t1604_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback2_t1604_0_0_0/* byval_arg */
	, &CertificateValidationCallback2_t1604_1_0_0/* this_arg */
	, &CertificateValidationCallback2_t1604_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback2_t1604/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback2_t1604)/* instance_size */
	, sizeof (CertificateValidationCallback2_t1604)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectio.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateSelectionCallback
extern TypeInfo CertificateSelectionCallback_t1588_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectioMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1588_CertificateSelectionCallback__ctor_m7489_ParameterInfos[] = 
{
	{"object", 0, 134218575, 0, &Object_t_0_0_0},
	{"method", 1, 134218576, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateSelectionCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateSelectionCallback__ctor_m7489_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateSelectionCallback__ctor_m7489/* method */
	, &CertificateSelectionCallback_t1588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, CertificateSelectionCallback_t1588_CertificateSelectionCallback__ctor_m7489_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1294_0_0_0;
extern const Il2CppType X509CertificateCollection_t1294_0_0_0;
extern const Il2CppType X509Certificate_t1323_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType X509CertificateCollection_t1294_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1588_CertificateSelectionCallback_Invoke_m7490_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218577, 0, &X509CertificateCollection_t1294_0_0_0},
	{"serverCertificate", 1, 134218578, 0, &X509Certificate_t1323_0_0_0},
	{"targetHost", 2, 134218579, 0, &String_t_0_0_0},
	{"serverRequestedCertificates", 3, 134218580, 0, &X509CertificateCollection_t1294_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.CertificateSelectionCallback::Invoke(System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern const MethodInfo CertificateSelectionCallback_Invoke_m7490_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_Invoke_m7490/* method */
	, &CertificateSelectionCallback_t1588_il2cpp_TypeInfo/* declaring_type */
	, &X509Certificate_t1323_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1588_CertificateSelectionCallback_Invoke_m7490_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1294_0_0_0;
extern const Il2CppType X509Certificate_t1323_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType X509CertificateCollection_t1294_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1588_CertificateSelectionCallback_BeginInvoke_m7491_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218581, 0, &X509CertificateCollection_t1294_0_0_0},
	{"serverCertificate", 1, 134218582, 0, &X509Certificate_t1323_0_0_0},
	{"targetHost", 2, 134218583, 0, &String_t_0_0_0},
	{"serverRequestedCertificates", 3, 134218584, 0, &X509CertificateCollection_t1294_0_0_0},
	{"callback", 4, 134218585, 0, &AsyncCallback_t547_0_0_0},
	{"object", 5, 134218586, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateSelectionCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.AsyncCallback,System.Object)
extern const MethodInfo CertificateSelectionCallback_BeginInvoke_m7491_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_BeginInvoke_m7491/* method */
	, &CertificateSelectionCallback_t1588_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1588_CertificateSelectionCallback_BeginInvoke_m7491_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1588_CertificateSelectionCallback_EndInvoke_m7492_ParameterInfos[] = 
{
	{"result", 0, 134218587, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.CertificateSelectionCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateSelectionCallback_EndInvoke_m7492_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_EndInvoke_m7492/* method */
	, &CertificateSelectionCallback_t1588_il2cpp_TypeInfo/* declaring_type */
	, &X509Certificate_t1323_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1588_CertificateSelectionCallback_EndInvoke_m7492_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateSelectionCallback_t1588_MethodInfos[] =
{
	&CertificateSelectionCallback__ctor_m7489_MethodInfo,
	&CertificateSelectionCallback_Invoke_m7490_MethodInfo,
	&CertificateSelectionCallback_BeginInvoke_m7491_MethodInfo,
	&CertificateSelectionCallback_EndInvoke_m7492_MethodInfo,
	NULL
};
extern const MethodInfo CertificateSelectionCallback_Invoke_m7490_MethodInfo;
extern const MethodInfo CertificateSelectionCallback_BeginInvoke_m7491_MethodInfo;
extern const MethodInfo CertificateSelectionCallback_EndInvoke_m7492_MethodInfo;
static const Il2CppMethodReference CertificateSelectionCallback_t1588_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&CertificateSelectionCallback_Invoke_m7490_MethodInfo,
	&CertificateSelectionCallback_BeginInvoke_m7491_MethodInfo,
	&CertificateSelectionCallback_EndInvoke_m7492_MethodInfo,
};
static bool CertificateSelectionCallback_t1588_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateSelectionCallback_t1588_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateSelectionCallback_t1588_0_0_0;
extern const Il2CppType CertificateSelectionCallback_t1588_1_0_0;
struct CertificateSelectionCallback_t1588;
const Il2CppTypeDefinitionMetadata CertificateSelectionCallback_t1588_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateSelectionCallback_t1588_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, CertificateSelectionCallback_t1588_VTable/* vtableMethods */
	, CertificateSelectionCallback_t1588_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateSelectionCallback_t1588_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateSelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateSelectionCallback_t1588_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateSelectionCallback_t1588_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateSelectionCallback_t1588_0_0_0/* byval_arg */
	, &CertificateSelectionCallback_t1588_1_0_0/* this_arg */
	, &CertificateSelectionCallback_t1588_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateSelectionCallback_t1588/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateSelectionCallback_t1588)/* instance_size */
	, sizeof (CertificateSelectionCallback_t1588)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelection.h"
// Metadata Definition Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
extern TypeInfo PrivateKeySelectionCallback_t1589_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelectionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1589_PrivateKeySelectionCallback__ctor_m7493_ParameterInfos[] = 
{
	{"object", 0, 134218588, 0, &Object_t_0_0_0},
	{"method", 1, 134218589, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrivateKeySelectionCallback__ctor_m7493_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback__ctor_m7493/* method */
	, &PrivateKeySelectionCallback_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_IntPtr_t/* invoker_method */
	, PrivateKeySelectionCallback_t1589_PrivateKeySelectionCallback__ctor_m7493_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1323_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1589_PrivateKeySelectionCallback_Invoke_m7494_ParameterInfos[] = 
{
	{"certificate", 0, 134218590, 0, &X509Certificate_t1323_0_0_0},
	{"targetHost", 1, 134218591, 0, &String_t_0_0_0},
};
extern const Il2CppType AsymmetricAlgorithm_t1310_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::Invoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.String)
extern const MethodInfo PrivateKeySelectionCallback_Invoke_m7494_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_Invoke_m7494/* method */
	, &PrivateKeySelectionCallback_t1589_il2cpp_TypeInfo/* declaring_type */
	, &AsymmetricAlgorithm_t1310_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1589_PrivateKeySelectionCallback_Invoke_m7494_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1323_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1589_PrivateKeySelectionCallback_BeginInvoke_m7495_ParameterInfos[] = 
{
	{"certificate", 0, 134218592, 0, &X509Certificate_t1323_0_0_0},
	{"targetHost", 1, 134218593, 0, &String_t_0_0_0},
	{"callback", 2, 134218594, 0, &AsyncCallback_t547_0_0_0},
	{"object", 3, 134218595, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.AsyncCallback,System.Object)
extern const MethodInfo PrivateKeySelectionCallback_BeginInvoke_m7495_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_BeginInvoke_m7495/* method */
	, &PrivateKeySelectionCallback_t1589_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1589_PrivateKeySelectionCallback_BeginInvoke_m7495_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1589_PrivateKeySelectionCallback_EndInvoke_m7496_ParameterInfos[] = 
{
	{"result", 0, 134218596, 0, &IAsyncResult_t546_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrivateKeySelectionCallback_EndInvoke_m7496_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_EndInvoke_m7496/* method */
	, &PrivateKeySelectionCallback_t1589_il2cpp_TypeInfo/* declaring_type */
	, &AsymmetricAlgorithm_t1310_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1589_PrivateKeySelectionCallback_EndInvoke_m7496_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrivateKeySelectionCallback_t1589_MethodInfos[] =
{
	&PrivateKeySelectionCallback__ctor_m7493_MethodInfo,
	&PrivateKeySelectionCallback_Invoke_m7494_MethodInfo,
	&PrivateKeySelectionCallback_BeginInvoke_m7495_MethodInfo,
	&PrivateKeySelectionCallback_EndInvoke_m7496_MethodInfo,
	NULL
};
extern const MethodInfo PrivateKeySelectionCallback_Invoke_m7494_MethodInfo;
extern const MethodInfo PrivateKeySelectionCallback_BeginInvoke_m7495_MethodInfo;
extern const MethodInfo PrivateKeySelectionCallback_EndInvoke_m7496_MethodInfo;
static const Il2CppMethodReference PrivateKeySelectionCallback_t1589_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&PrivateKeySelectionCallback_Invoke_m7494_MethodInfo,
	&PrivateKeySelectionCallback_BeginInvoke_m7495_MethodInfo,
	&PrivateKeySelectionCallback_EndInvoke_m7496_MethodInfo,
};
static bool PrivateKeySelectionCallback_t1589_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PrivateKeySelectionCallback_t1589_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrivateKeySelectionCallback_t1589_0_0_0;
extern const Il2CppType PrivateKeySelectionCallback_t1589_1_0_0;
struct PrivateKeySelectionCallback_t1589;
const Il2CppTypeDefinitionMetadata PrivateKeySelectionCallback_t1589_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrivateKeySelectionCallback_t1589_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, PrivateKeySelectionCallback_t1589_VTable/* vtableMethods */
	, PrivateKeySelectionCallback_t1589_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrivateKeySelectionCallback_t1589_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrivateKeySelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, PrivateKeySelectionCallback_t1589_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrivateKeySelectionCallback_t1589_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrivateKeySelectionCallback_t1589_0_0_0/* byval_arg */
	, &PrivateKeySelectionCallback_t1589_1_0_0/* this_arg */
	, &PrivateKeySelectionCallback_t1589_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1589/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrivateKeySelectionCallback_t1589)/* instance_size */
	, sizeof (PrivateKeySelectionCallback_t1589)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
extern TypeInfo U24ArrayTypeU243132_t1626_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTypMethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU243132_t1626_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m1076_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m1077_MethodInfo;
extern const MethodInfo ValueType_ToString_m1078_MethodInfo;
static const Il2CppMethodReference U24ArrayTypeU243132_t1626_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU243132_t1626_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU243132_t1626_0_0_0;
extern const Il2CppType U24ArrayTypeU243132_t1626_1_0_0;
extern const Il2CppType ValueType_t285_0_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1635_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1635_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU243132_t1626_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1635_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU243132_t1626_VTable/* vtableMethods */
	, U24ArrayTypeU243132_t1626_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU243132_t1626_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$3132"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU243132_t1626_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU243132_t1626_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU243132_t1626_0_0_0/* byval_arg */
	, &U24ArrayTypeU243132_t1626_1_0_0/* this_arg */
	, &U24ArrayTypeU243132_t1626_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU243132_t1626_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1626_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1626_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU243132_t1626)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU243132_t1626)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU243132_t1626_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t1627_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24256_t1627_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24256_t1627_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU24256_t1627_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t1627_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t1627_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t1627_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1635_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU24256_t1627_VTable/* vtableMethods */
	, U24ArrayTypeU24256_t1627_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24256_t1627_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24256_t1627_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24256_t1627_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t1627_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t1627_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t1627_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t1627_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1627_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1627_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t1627)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t1627)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t1627_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t1628_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2420_t1628_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2420_t1628_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2420_t1628_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2420_t1628_0_0_0;
extern const Il2CppType U24ArrayTypeU2420_t1628_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t1628_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1635_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2420_t1628_VTable/* vtableMethods */
	, U24ArrayTypeU2420_t1628_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2420_t1628_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2420_t1628_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2420_t1628_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t1628_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t1628_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t1628_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t1628_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1628_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1628_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t1628)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t1628)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t1628_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
extern TypeInfo U24ArrayTypeU2432_t1629_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2432_t1629_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2432_t1629_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2432_t1629_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2432_t1629_0_0_0;
extern const Il2CppType U24ArrayTypeU2432_t1629_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2432_t1629_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1635_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2432_t1629_VTable/* vtableMethods */
	, U24ArrayTypeU2432_t1629_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2432_t1629_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$32"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2432_t1629_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2432_t1629_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2432_t1629_0_0_0/* byval_arg */
	, &U24ArrayTypeU2432_t1629_1_0_0/* this_arg */
	, &U24ArrayTypeU2432_t1629_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2432_t1629_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1629_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1629_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2432_t1629)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2432_t1629)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2432_t1629_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t1630_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2448_t1630_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2448_t1630_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2448_t1630_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2448_t1630_0_0_0;
extern const Il2CppType U24ArrayTypeU2448_t1630_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t1630_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1635_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2448_t1630_VTable/* vtableMethods */
	, U24ArrayTypeU2448_t1630_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2448_t1630_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2448_t1630_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2448_t1630_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t1630_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t1630_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t1630_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t1630_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1630_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1630_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t1630)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t1630)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t1630_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
extern TypeInfo U24ArrayTypeU2464_t1631_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2464_t1631_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2464_t1631_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2464_t1631_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2464_t1631_0_0_0;
extern const Il2CppType U24ArrayTypeU2464_t1631_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2464_t1631_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1635_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2464_t1631_VTable/* vtableMethods */
	, U24ArrayTypeU2464_t1631_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2464_t1631_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$64"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2464_t1631_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2464_t1631_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2464_t1631_0_0_0/* byval_arg */
	, &U24ArrayTypeU2464_t1631_1_0_0/* this_arg */
	, &U24ArrayTypeU2464_t1631_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2464_t1631_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1631_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1631_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2464_t1631)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2464_t1631)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2464_t1631_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t1632_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2412_t1632_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2412_t1632_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2412_t1632_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t1632_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t1632_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t1632_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1635_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2412_t1632_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t1632_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2412_t1632_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t1632_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2412_t1632_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t1632_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t1632_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t1632_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t1632_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1632_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1632_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t1632)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t1632)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t1632_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
extern TypeInfo U24ArrayTypeU2416_t1633_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2416_t1633_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2416_t1633_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU2416_t1633_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2416_t1633_0_0_0;
extern const Il2CppType U24ArrayTypeU2416_t1633_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2416_t1633_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1635_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU2416_t1633_VTable/* vtableMethods */
	, U24ArrayTypeU2416_t1633_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2416_t1633_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$16"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2416_t1633_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2416_t1633_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2416_t1633_0_0_0/* byval_arg */
	, &U24ArrayTypeU2416_t1633_1_0_0/* this_arg */
	, &U24ArrayTypeU2416_t1633_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2416_t1633_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1633_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1633_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2416_t1633)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2416_t1633)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2416_t1633_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$4
extern TypeInfo U24ArrayTypeU244_t1634_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU244_t1634_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU244_t1634_VTable[] =
{
	&ValueType_Equals_m1076_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&ValueType_GetHashCode_m1077_MethodInfo,
	&ValueType_ToString_m1078_MethodInfo,
};
static bool U24ArrayTypeU244_t1634_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU244_t1634_0_0_0;
extern const Il2CppType U24ArrayTypeU244_t1634_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU244_t1634_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1635_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t285_0_0_0/* parent */
	, U24ArrayTypeU244_t1634_VTable/* vtableMethods */
	, U24ArrayTypeU244_t1634_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU244_t1634_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$4"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU244_t1634_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU244_t1634_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU244_t1634_0_0_0/* byval_arg */
	, &U24ArrayTypeU244_t1634_1_0_0/* this_arg */
	, &U24ArrayTypeU244_t1634_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU244_t1634_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU244_t1634_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU244_t1634_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU244_t1634)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU244_t1634)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU244_t1634_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t1635_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1635_il2cpp_TypeInfo__nestedTypes[9] =
{
	&U24ArrayTypeU243132_t1626_0_0_0,
	&U24ArrayTypeU24256_t1627_0_0_0,
	&U24ArrayTypeU2420_t1628_0_0_0,
	&U24ArrayTypeU2432_t1629_0_0_0,
	&U24ArrayTypeU2448_t1630_0_0_0,
	&U24ArrayTypeU2464_t1631_0_0_0,
	&U24ArrayTypeU2412_t1632_0_0_0,
	&U24ArrayTypeU2416_t1633_0_0_0,
	&U24ArrayTypeU244_t1634_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t1635_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t1635_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1635_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1635;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1635_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1635_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1635_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t1635_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 468/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1635_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1635_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t1635_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 38/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1635_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1635_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1635_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1635)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1635)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1635_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 9/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
