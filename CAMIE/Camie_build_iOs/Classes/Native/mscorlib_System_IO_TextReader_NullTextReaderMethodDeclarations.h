﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.TextReader/NullTextReader
struct NullTextReader_t1832;
// System.String
struct String_t;

// System.Void System.IO.TextReader/NullTextReader::.ctor()
extern "C" void NullTextReader__ctor_m9675 (NullTextReader_t1832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.TextReader/NullTextReader::ReadLine()
extern "C" String_t* NullTextReader_ReadLine_m9676 (NullTextReader_t1832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
