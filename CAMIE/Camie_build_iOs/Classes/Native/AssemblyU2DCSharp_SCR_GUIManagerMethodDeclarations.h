﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SCR_GUIManager
struct SCR_GUIManager_t378;
// SCR_LevelEndMenu
struct SCR_LevelEndMenu_t367;
// SCR_MainMenu
struct SCR_MainMenu_t372;
// System.String
struct String_t;
// SCR_GUIManager/TextType
#include "AssemblyU2DCSharp_SCR_GUIManager_TextType.h"

// System.Void SCR_GUIManager::.ctor()
extern "C" void SCR_GUIManager__ctor_m1404 (SCR_GUIManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_GUIManager::Awake()
extern "C" void SCR_GUIManager_Awake_m1405 (SCR_GUIManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_GUIManager::OnLevelWasLoaded()
extern "C" void SCR_GUIManager_OnLevelWasLoaded_m1406 (SCR_GUIManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_GUIManager::LevelInit()
extern "C" void SCR_GUIManager_LevelInit_m1407 (SCR_GUIManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_GUIManager::CloseAllMenus()
extern "C" void SCR_GUIManager_CloseAllMenus_m1408 (SCR_GUIManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_GUIManager::OpenMenu(System.String)
extern "C" void SCR_GUIManager_OpenMenu_m1409 (SCR_GUIManager_t378 * __this, String_t* ___menuName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_GUIManager::OpenLevelMenus()
extern "C" void SCR_GUIManager_OpenLevelMenus_m1410 (SCR_GUIManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_GUIManager::RelayTextInfo(SCR_GUIManager/TextType)
extern "C" void SCR_GUIManager_RelayTextInfo_m1411 (SCR_GUIManager_t378 * __this, int32_t ___collectibleType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCR_GUIManager::ToggleLanguage(System.Boolean)
extern "C" void SCR_GUIManager_ToggleLanguage_m1412 (SCR_GUIManager_t378 * __this, bool ___french, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCR_GUIManager::get_IsFrench()
extern "C" bool SCR_GUIManager_get_IsFrench_m1413 (SCR_GUIManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_LevelEndMenu SCR_GUIManager::get_LevelEndMenu()
extern "C" SCR_LevelEndMenu_t367 * SCR_GUIManager_get_LevelEndMenu_m1414 (SCR_GUIManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SCR_MainMenu SCR_GUIManager::get_MainMenu()
extern "C" SCR_MainMenu_t372 * SCR_GUIManager_get_MainMenu_m1415 (SCR_GUIManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
