﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>
struct Enumerator_t2913;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t303;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15004_gshared (Enumerator_t2913 * __this, List_1_t303 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15004(__this, ___l, method) (( void (*) (Enumerator_t2913 *, List_1_t303 *, const MethodInfo*))Enumerator__ctor_m15004_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15005_gshared (Enumerator_t2913 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15005(__this, method) (( Object_t * (*) (Enumerator_t2913 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15005_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::Dispose()
extern "C" void Enumerator_Dispose_m15006_gshared (Enumerator_t2913 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15006(__this, method) (( void (*) (Enumerator_t2913 *, const MethodInfo*))Enumerator_Dispose_m15006_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::VerifyState()
extern "C" void Enumerator_VerifyState_m15007_gshared (Enumerator_t2913 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15007(__this, method) (( void (*) (Enumerator_t2913 *, const MethodInfo*))Enumerator_VerifyState_m15007_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15008_gshared (Enumerator_t2913 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15008(__this, method) (( bool (*) (Enumerator_t2913 *, const MethodInfo*))Enumerator_MoveNext_m15008_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t6  Enumerator_get_Current_m15009_gshared (Enumerator_t2913 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15009(__this, method) (( Vector2_t6  (*) (Enumerator_t2913 *, const MethodInfo*))Enumerator_get_Current_m15009_gshared)(__this, method)
