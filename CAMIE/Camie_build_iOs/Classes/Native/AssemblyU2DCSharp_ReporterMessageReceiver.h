﻿#pragma once
#include <stdint.h>
// Reporter
struct Reporter_t295;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ReporterMessageReceiver
struct  ReporterMessageReceiver_t306  : public MonoBehaviour_t3
{
	// Reporter ReporterMessageReceiver::reporter
	Reporter_t295 * ___reporter_2;
};
