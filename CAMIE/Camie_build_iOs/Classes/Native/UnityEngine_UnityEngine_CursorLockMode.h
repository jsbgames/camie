﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.CursorLockMode
#include "UnityEngine_UnityEngine_CursorLockMode.h"
// UnityEngine.CursorLockMode
struct  CursorLockMode_t785 
{
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___1;
};
