﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ChannelInfo
struct ChannelInfo_t1957;
// System.Object[]
struct ObjectU5BU5D_t224;

// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
extern "C" void ChannelInfo__ctor_m10463 (ChannelInfo_t1957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
extern "C" ObjectU5BU5D_t224* ChannelInfo_get_ChannelData_m10464 (ChannelInfo_t1957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
