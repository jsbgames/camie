﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct U24ArrayTypeU2412_t2263;
struct U24ArrayTypeU2412_t2263_marshaled;

void U24ArrayTypeU2412_t2263_marshal(const U24ArrayTypeU2412_t2263& unmarshaled, U24ArrayTypeU2412_t2263_marshaled& marshaled);
void U24ArrayTypeU2412_t2263_marshal_back(const U24ArrayTypeU2412_t2263_marshaled& marshaled, U24ArrayTypeU2412_t2263& unmarshaled);
void U24ArrayTypeU2412_t2263_marshal_cleanup(U24ArrayTypeU2412_t2263_marshaled& marshaled);
