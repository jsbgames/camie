﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyValuePair_2_t3375;
// UnityEngine.Event
struct Event_t560;
struct Event_t560_marshaled;
// System.String
struct String_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11MethodDeclarations.h"
#define KeyValuePair_2__ctor_m21459(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3375 *, Event_t560 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m15568_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m21460(__this, method) (( Event_t560 * (*) (KeyValuePair_2_t3375 *, const MethodInfo*))KeyValuePair_2_get_Key_m15569_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21461(__this, ___value, method) (( void (*) (KeyValuePair_2_t3375 *, Event_t560 *, const MethodInfo*))KeyValuePair_2_set_Key_m15570_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m21462(__this, method) (( int32_t (*) (KeyValuePair_2_t3375 *, const MethodInfo*))KeyValuePair_2_get_Value_m15571_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21463(__this, ___value, method) (( void (*) (KeyValuePair_2_t3375 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m15572_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m21464(__this, method) (( String_t* (*) (KeyValuePair_2_t3375 *, const MethodInfo*))KeyValuePair_2_ToString_m15573_gshared)(__this, method)
