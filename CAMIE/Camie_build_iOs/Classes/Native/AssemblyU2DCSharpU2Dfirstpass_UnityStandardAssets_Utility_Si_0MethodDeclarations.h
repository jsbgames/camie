﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.Utility.SimpleMouseRotator
struct SimpleMouseRotator_t196;

// System.Void UnityStandardAssets.Utility.SimpleMouseRotator::.ctor()
extern "C" void SimpleMouseRotator__ctor_m533 (SimpleMouseRotator_t196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Start()
extern "C" void SimpleMouseRotator_Start_m534 (SimpleMouseRotator_t196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Update()
extern "C" void SimpleMouseRotator_Update_m535 (SimpleMouseRotator_t196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
