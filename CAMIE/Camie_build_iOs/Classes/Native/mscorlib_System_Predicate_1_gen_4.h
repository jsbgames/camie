﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.String>
struct  Predicate_1_t2855  : public MulticastDelegate_t549
{
};
