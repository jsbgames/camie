﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList
struct DemoParticleSystemList_t323;

// System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList::.ctor()
extern "C" void DemoParticleSystemList__ctor_m1172 (DemoParticleSystemList_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
