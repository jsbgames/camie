﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityStandardAssets.ImageEffects.BlurOptimized
struct BlurOptimized_t76;
// UnityEngine.RenderTexture
struct RenderTexture_t101;

// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::.ctor()
extern "C" void BlurOptimized__ctor_m214 (BlurOptimized_t76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.ImageEffects.BlurOptimized::CheckResources()
extern "C" bool BlurOptimized_CheckResources_m215 (BlurOptimized_t76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnDisable()
extern "C" void BlurOptimized_OnDisable_m216 (BlurOptimized_t76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void BlurOptimized_OnRenderImage_m217 (BlurOptimized_t76 * __this, RenderTexture_t101 * ___source, RenderTexture_t101 * ___destination, const MethodInfo* method) IL2CPP_METHOD_ATTR;
