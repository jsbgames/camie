﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t242;
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecord.h"
// System.Runtime.Serialization.MultiArrayFixupRecord
struct  MultiArrayFixupRecord_t2052  : public BaseFixupRecord_t2050
{
	// System.Int32[] System.Runtime.Serialization.MultiArrayFixupRecord::_indices
	Int32U5BU5D_t242* ____indices_4;
};
