﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t3389;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t3164;
// System.Object[]
struct ObjectU5BU5D_t224;

// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
// UnityEngine.Events.InvokableCall`1<System.Byte>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_3MethodDeclarations.h"
#define InvokableCall_1__ctor_m21546(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t3389 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m18560_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
#define InvokableCall_1__ctor_m21547(__this, ___callback, method) (( void (*) (InvokableCall_1_t3389 *, UnityAction_1_t3164 *, const MethodInfo*))InvokableCall_1__ctor_m18561_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
#define InvokableCall_1_Invoke_m21548(__this, ___args, method) (( void (*) (InvokableCall_1_t3389 *, ObjectU5BU5D_t224*, const MethodInfo*))InvokableCall_1_Invoke_m18562_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
#define InvokableCall_1_Find_m21549(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t3389 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m18563_gshared)(__this, ___targetObj, ___method, method)
