﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$32
struct U24ArrayTypeU2432_t2260;
struct U24ArrayTypeU2432_t2260_marshaled;

void U24ArrayTypeU2432_t2260_marshal(const U24ArrayTypeU2432_t2260& unmarshaled, U24ArrayTypeU2432_t2260_marshaled& marshaled);
void U24ArrayTypeU2432_t2260_marshal_back(const U24ArrayTypeU2432_t2260_marshaled& marshaled, U24ArrayTypeU2432_t2260& unmarshaled);
void U24ArrayTypeU2432_t2260_marshal_cleanup(U24ArrayTypeU2432_t2260_marshaled& marshaled);
