﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// SCR_Bird
struct SCR_Bird_t390;
// System.Object
#include "mscorlib_System_Object.h"
// SCR_Bird/<Landing>c__IteratorB
struct  U3CLandingU3Ec__IteratorB_t391  : public Object_t
{
	// System.Int32 SCR_Bird/<Landing>c__IteratorB::$PC
	int32_t ___U24PC_0;
	// System.Object SCR_Bird/<Landing>c__IteratorB::$current
	Object_t * ___U24current_1;
	// SCR_Bird SCR_Bird/<Landing>c__IteratorB::<>f__this
	SCR_Bird_t390 * ___U3CU3Ef__this_2;
};
