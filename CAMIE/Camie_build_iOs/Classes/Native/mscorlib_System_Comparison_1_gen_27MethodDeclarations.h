﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.RectTransform>
struct Comparison_1_t3177;
// System.Object
struct Object_t;
// UnityEngine.RectTransform
struct RectTransform_t364;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<UnityEngine.RectTransform>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m18766(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3177 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m14080_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.RectTransform>::Invoke(T,T)
#define Comparison_1_Invoke_m18767(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3177 *, RectTransform_t364 *, RectTransform_t364 *, const MethodInfo*))Comparison_1_Invoke_m14081_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.RectTransform>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m18768(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3177 *, RectTransform_t364 *, RectTransform_t364 *, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m14082_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.RectTransform>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m18769(__this, ___result, method) (( int32_t (*) (Comparison_1_t3177 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m14083_gshared)(__this, ___result, method)
