﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUI/ScrollViewState
struct ScrollViewState_t812;

// System.Void UnityEngine.GUI/ScrollViewState::.ctor()
extern "C" void ScrollViewState__ctor_m3818 (ScrollViewState_t812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
