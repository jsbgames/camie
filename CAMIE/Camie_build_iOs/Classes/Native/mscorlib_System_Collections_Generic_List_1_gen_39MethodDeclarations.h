﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct List_1_t3079;
// System.Object
struct Object_t;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t646;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ICanvasElement>
struct IEnumerable_1_t3646;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ICanvasElement>
struct IEnumerator_1_t3645;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// System.Collections.Generic.ICollection`1<UnityEngine.UI.ICanvasElement>
struct ICollection_1_t3647;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.ICanvasElement>
struct ReadOnlyCollection_1_t3648;
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t3081;
// System.Predicate`1<UnityEngine.UI.ICanvasElement>
struct Predicate_1_t515;
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct Comparison_1_t514;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.ICanvasElement>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_39.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#define List_1__ctor_m17307(__this, method) (( void (*) (List_1_t3079 *, const MethodInfo*))List_1__ctor_m5170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m17308(__this, ___collection, method) (( void (*) (List_1_t3079 *, Object_t*, const MethodInfo*))List_1__ctor_m13913_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::.ctor(System.Int32)
#define List_1__ctor_m17309(__this, ___capacity, method) (( void (*) (List_1_t3079 *, int32_t, const MethodInfo*))List_1__ctor_m13915_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::.cctor()
#define List_1__cctor_m17310(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13917_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17311(__this, method) (( Object_t* (*) (List_1_t3079 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m17312(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3079 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m5408_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17313(__this, method) (( Object_t * (*) (List_1_t3079 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m5404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m17314(__this, ___item, method) (( int32_t (*) (List_1_t3079 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m5413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m17315(__this, ___item, method) (( bool (*) (List_1_t3079 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m5415_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m17316(__this, ___item, method) (( int32_t (*) (List_1_t3079 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m5416_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m17317(__this, ___index, ___item, method) (( void (*) (List_1_t3079 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m5417_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m17318(__this, ___item, method) (( void (*) (List_1_t3079 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m5418_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17319(__this, method) (( bool (*) (List_1_t3079 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m5420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17320(__this, method) (( bool (*) (List_1_t3079 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m5406_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m17321(__this, method) (( Object_t * (*) (List_1_t3079 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m5407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m17322(__this, method) (( bool (*) (List_1_t3079 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m5409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m17323(__this, method) (( bool (*) (List_1_t3079 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m5410_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m17324(__this, ___index, method) (( Object_t * (*) (List_1_t3079 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m5411_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m17325(__this, ___index, ___value, method) (( void (*) (List_1_t3079 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m5412_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define List_1_Add_m17326(__this, ___item, method) (( void (*) (List_1_t3079 *, Object_t *, const MethodInfo*))List_1_Add_m5421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m17327(__this, ___newCount, method) (( void (*) (List_1_t3079 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13935_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m17328(__this, ___collection, method) (( void (*) (List_1_t3079 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13937_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m17329(__this, ___enumerable, method) (( void (*) (List_1_t3079 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13939_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m17330(__this, ___collection, method) (( void (*) (List_1_t3079 *, Object_t*, const MethodInfo*))List_1_AddRange_m13941_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::AsReadOnly()
#define List_1_AsReadOnly_m17331(__this, method) (( ReadOnlyCollection_1_t3648 * (*) (List_1_t3079 *, const MethodInfo*))List_1_AsReadOnly_m13943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Clear()
#define List_1_Clear_m17332(__this, method) (( void (*) (List_1_t3079 *, const MethodInfo*))List_1_Clear_m5414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define List_1_Contains_m17333(__this, ___item, method) (( bool (*) (List_1_t3079 *, Object_t *, const MethodInfo*))List_1_Contains_m5422_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m17334(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3079 *, ICanvasElementU5BU5D_t3081*, int32_t, const MethodInfo*))List_1_CopyTo_m5423_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Find(System.Predicate`1<T>)
#define List_1_Find_m17335(__this, ___match, method) (( Object_t * (*) (List_1_t3079 *, Predicate_1_t515 *, const MethodInfo*))List_1_Find_m13948_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m17336(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t515 *, const MethodInfo*))List_1_CheckMatch_m13950_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m17337(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t3079 *, int32_t, int32_t, Predicate_1_t515 *, const MethodInfo*))List_1_GetIndex_m13952_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define List_1_GetEnumerator_m17338(__this, method) (( Enumerator_t3649  (*) (List_1_t3079 *, const MethodInfo*))List_1_GetEnumerator_m13954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define List_1_IndexOf_m17339(__this, ___item, method) (( int32_t (*) (List_1_t3079 *, Object_t *, const MethodInfo*))List_1_IndexOf_m5426_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m17340(__this, ___start, ___delta, method) (( void (*) (List_1_t3079 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13957_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m17341(__this, ___index, method) (( void (*) (List_1_t3079 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13959_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define List_1_Insert_m17342(__this, ___index, ___item, method) (( void (*) (List_1_t3079 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m5427_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m17343(__this, ___collection, method) (( void (*) (List_1_t3079 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13962_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define List_1_Remove_m17344(__this, ___item, method) (( bool (*) (List_1_t3079 *, Object_t *, const MethodInfo*))List_1_Remove_m5424_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m17345(__this, ___match, method) (( int32_t (*) (List_1_t3079 *, Predicate_1_t515 *, const MethodInfo*))List_1_RemoveAll_m13965_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m17346(__this, ___index, method) (( void (*) (List_1_t3079 *, int32_t, const MethodInfo*))List_1_RemoveAt_m5419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Reverse()
#define List_1_Reverse_m17347(__this, method) (( void (*) (List_1_t3079 *, const MethodInfo*))List_1_Reverse_m13968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Sort()
#define List_1_Sort_m17348(__this, method) (( void (*) (List_1_t3079 *, const MethodInfo*))List_1_Sort_m13970_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m17349(__this, ___comparison, method) (( void (*) (List_1_t3079 *, Comparison_1_t514 *, const MethodInfo*))List_1_Sort_m13972_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::ToArray()
#define List_1_ToArray_m17350(__this, method) (( ICanvasElementU5BU5D_t3081* (*) (List_1_t3079 *, const MethodInfo*))List_1_ToArray_m13973_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::TrimExcess()
#define List_1_TrimExcess_m17351(__this, method) (( void (*) (List_1_t3079 *, const MethodInfo*))List_1_TrimExcess_m13975_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::get_Capacity()
#define List_1_get_Capacity_m17352(__this, method) (( int32_t (*) (List_1_t3079 *, const MethodInfo*))List_1_get_Capacity_m13977_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m17353(__this, ___value, method) (( void (*) (List_1_t3079 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13979_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define List_1_get_Count_m17354(__this, method) (( int32_t (*) (List_1_t3079 *, const MethodInfo*))List_1_get_Count_m5405_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define List_1_get_Item_m17355(__this, ___index, method) (( Object_t * (*) (List_1_t3079 *, int32_t, const MethodInfo*))List_1_get_Item_m5428_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define List_1_set_Item_m17356(__this, ___index, ___value, method) (( void (*) (List_1_t3079 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m5429_gshared)(__this, ___index, ___value, method)
