﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t185;
// System.Object
#include "mscorlib_System_Object.h"
// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6
struct  U3CDoBobCycleU3Ec__Iterator6_t186  : public Object_t
{
	// System.Single UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::<t>__0
	float ___U3CtU3E__0_0;
	// System.Int32 UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::$PC
	int32_t ___U24PC_1;
	// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::$current
	Object_t * ___U24current_2;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator6::<>f__this
	LerpControlledBob_t185 * ___U3CU3Ef__this_3;
};
