﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t653;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct  UnityAction_1_t621  : public MulticastDelegate_t549
{
};
