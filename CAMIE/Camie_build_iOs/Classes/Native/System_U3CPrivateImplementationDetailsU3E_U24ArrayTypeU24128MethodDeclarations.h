﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$128
struct U24ArrayTypeU24128_t1426;
struct U24ArrayTypeU24128_t1426_marshaled;

void U24ArrayTypeU24128_t1426_marshal(const U24ArrayTypeU24128_t1426& unmarshaled, U24ArrayTypeU24128_t1426_marshaled& marshaled);
void U24ArrayTypeU24128_t1426_marshal_back(const U24ArrayTypeU24128_t1426_marshaled& marshaled, U24ArrayTypeU24128_t1426& unmarshaled);
void U24ArrayTypeU24128_t1426_marshal_cleanup(U24ArrayTypeU24128_t1426_marshaled& marshaled);
