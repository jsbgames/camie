﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t523;
// System.Collections.IEnumerator
struct IEnumerator_t217;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void TweenRunner_1__ctor_m3174_gshared (TweenRunner_1_t523 * __this, const MethodInfo* method);
#define TweenRunner_1__ctor_m3174(__this, method) (( void (*) (TweenRunner_1_t523 *, const MethodInfo*))TweenRunner_1__ctor_m3174_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C" Object_t * TweenRunner_1_Start_m17769_gshared (Object_t * __this /* static, unused */, ColorTween_t506  ___tweenInfo, const MethodInfo* method);
#define TweenRunner_1_Start_m17769(__this /* static, unused */, ___tweenInfo, method) (( Object_t * (*) (Object_t * /* static, unused */, ColorTween_t506 , const MethodInfo*))TweenRunner_1_Start_m17769_gshared)(__this /* static, unused */, ___tweenInfo, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C" void TweenRunner_1_Init_m3175_gshared (TweenRunner_1_t523 * __this, MonoBehaviour_t3 * ___coroutineContainer, const MethodInfo* method);
#define TweenRunner_1_Init_m3175(__this, ___coroutineContainer, method) (( void (*) (TweenRunner_1_t523 *, MonoBehaviour_t3 *, const MethodInfo*))TweenRunner_1_Init_m3175_gshared)(__this, ___coroutineContainer, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern "C" void TweenRunner_1_StartTween_m3198_gshared (TweenRunner_1_t523 * __this, ColorTween_t506  ___info, const MethodInfo* method);
#define TweenRunner_1_StartTween_m3198(__this, ___info, method) (( void (*) (TweenRunner_1_t523 *, ColorTween_t506 , const MethodInfo*))TweenRunner_1_StartTween_m3198_gshared)(__this, ___info, method)
