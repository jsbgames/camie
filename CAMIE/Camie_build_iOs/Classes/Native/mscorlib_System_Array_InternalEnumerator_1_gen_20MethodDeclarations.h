﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Mesh>
struct InternalEnumerator_1_t2862;
// System.Object
struct Object_t;
// UnityEngine.Mesh
struct Mesh_t216;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Mesh>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14148(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2862 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13513_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Mesh>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14149(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2862 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13515_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Mesh>::Dispose()
#define InternalEnumerator_1_Dispose_m14150(__this, method) (( void (*) (InternalEnumerator_1_t2862 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13517_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Mesh>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14151(__this, method) (( bool (*) (InternalEnumerator_1_t2862 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13519_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Mesh>::get_Current()
#define InternalEnumerator_1_get_Current_m14152(__this, method) (( Mesh_t216 * (*) (InternalEnumerator_1_t2862 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13521_gshared)(__this, method)
