﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t645;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
struct  InvokableCall_1_t3078  : public BaseInvokableCall_t1002
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Delegate
	UnityAction_1_t645 * ___Delegate_0;
};
