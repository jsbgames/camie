﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
extern TypeInfo* RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var;
void g_AssemblyU2DCSharp_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(107);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RuntimeCompatibilityAttribute_t258 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t258 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t258_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1021(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1022(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t432_il2cpp_TypeInfo_var;
void MultiKeyDictionary_3_t431_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t432 * tmp;
		tmp = (DefaultMemberAttribute_t432 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t432_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1832(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspector.h"
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
extern TypeInfo* HideInInspector_t270_il2cpp_TypeInfo_var;
void Reporter_t295_CustomAttributesCacheGenerator_show(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t270_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t270 * tmp;
		tmp = (HideInInspector_t270 *)il2cpp_codegen_object_new (HideInInspector_t270_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m1046(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void Reporter_t295_CustomAttributesCacheGenerator_Reporter_readInfo_m1123(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CreadInfoU3Ec__Iterator0_t296_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CreadInfoU3Ec__Iterator0_t296_CustomAttributesCacheGenerator_U3CreadInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1086(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CreadInfoU3Ec__Iterator0_t296_CustomAttributesCacheGenerator_U3CreadInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1087(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CreadInfoU3Ec__Iterator0_t296_CustomAttributesCacheGenerator_U3CreadInfoU3Ec__Iterator0_Dispose_m1089(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CreadInfoU3Ec__Iterator0_t296_CustomAttributesCacheGenerator_U3CreadInfoU3Ec__Iterator0_Reset_m1090(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void EventSystemChecker_t309_CustomAttributesCacheGenerator_EventSystemChecker_Start_m1148(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator1_t310_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator1_t310_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1142(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator1_t310_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1143(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator1_t310_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_Dispose_m1145(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator1_t310_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_Reset_m1146(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_Menu_t336_CustomAttributesCacheGenerator_SCR_Menu_AlphaLerp_m1212(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_Menu_t336_CustomAttributesCacheGenerator_SCR_Menu_AlphaLerp_m1213(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CAlphaLerpU3Ec__Iterator2_t332_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CAlphaLerpU3Ec__Iterator2_t332_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1193(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CAlphaLerpU3Ec__Iterator2_t332_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1194(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CAlphaLerpU3Ec__Iterator2_t332_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator2_Dispose_m1196(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CAlphaLerpU3Ec__Iterator2_t332_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator2_Reset_m1197(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CAlphaLerpU3Ec__Iterator3_t333_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CAlphaLerpU3Ec__Iterator3_t333_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CAlphaLerpU3Ec__Iterator3_t333_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1200(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CAlphaLerpU3Ec__Iterator3_t333_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator3_Dispose_m1202(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CAlphaLerpU3Ec__Iterator3_t333_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator3_Reset_m1203(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void SCR_SlowAvatar_t337_CustomAttributesCacheGenerator_speedDecreaseToPercent(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 100.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_Camie_t338_CustomAttributesCacheGenerator_SCR_Camie_GoToFrontCollisionPoint_m1246(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_Camie_t338_CustomAttributesCacheGenerator_SCR_Camie_LandOnCollisionPoint_m1248(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_Camie_t338_CustomAttributesCacheGenerator_SCR_Camie_Jump_m1249(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_CustomAttributesCacheGenerator_U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1225(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_CustomAttributesCacheGenerator_U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1226(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_CustomAttributesCacheGenerator_U3CGoToFrontCollisionPointU3Ec__Iterator4_Dispose_m1228(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_CustomAttributesCacheGenerator_U3CGoToFrontCollisionPointU3Ec__Iterator4_Reset_m1229(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CLandOnCollisionPointU3Ec__Iterator5_t340_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CLandOnCollisionPointU3Ec__Iterator5_t340_CustomAttributesCacheGenerator_U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1231(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CLandOnCollisionPointU3Ec__Iterator5_t340_CustomAttributesCacheGenerator_U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1232(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CLandOnCollisionPointU3Ec__Iterator5_t340_CustomAttributesCacheGenerator_U3CLandOnCollisionPointU3Ec__Iterator5_Dispose_m1234(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CLandOnCollisionPointU3Ec__Iterator5_t340_CustomAttributesCacheGenerator_U3CLandOnCollisionPointU3Ec__Iterator5_Reset_m1235(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CJumpU3Ec__Iterator6_t341_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CJumpU3Ec__Iterator6_t341_CustomAttributesCacheGenerator_U3CJumpU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1237(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CJumpU3Ec__Iterator6_t341_CustomAttributesCacheGenerator_U3CJumpU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1238(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CJumpU3Ec__Iterator6_t341_CustomAttributesCacheGenerator_U3CJumpU3Ec__Iterator6_Dispose_m1240(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CJumpU3Ec__Iterator6_t341_CustomAttributesCacheGenerator_U3CJumpU3Ec__Iterator6_Reset_m1241(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void SCR_FollowAvatar_t352_CustomAttributesCacheGenerator_distanceDuFrame(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 10.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void SCR_FollowAvatar_t352_CustomAttributesCacheGenerator_plusHautQueCamie(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 3.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void SCR_FollowAvatar_t352_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_FollowAvatar_t352_CustomAttributesCacheGenerator_SCR_FollowAvatar_ZoomInOn_m1304(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_FollowAvatar_t352_CustomAttributesCacheGenerator_SCR_FollowAvatar_FollowAvatar_m1305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CZoomInOnU3Ec__Iterator7_t353_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CZoomInOnU3Ec__Iterator7_t353_CustomAttributesCacheGenerator_U3CZoomInOnU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1289(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CZoomInOnU3Ec__Iterator7_t353_CustomAttributesCacheGenerator_U3CZoomInOnU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1290(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CZoomInOnU3Ec__Iterator7_t353_CustomAttributesCacheGenerator_U3CZoomInOnU3Ec__Iterator7_Dispose_m1292(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CZoomInOnU3Ec__Iterator7_t353_CustomAttributesCacheGenerator_U3CZoomInOnU3Ec__Iterator7_Reset_m1293(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CFollowAvatarU3Ec__Iterator8_t354_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFollowAvatarU3Ec__Iterator8_t354_CustomAttributesCacheGenerator_U3CFollowAvatarU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1295(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFollowAvatarU3Ec__Iterator8_t354_CustomAttributesCacheGenerator_U3CFollowAvatarU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1296(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFollowAvatarU3Ec__Iterator8_t354_CustomAttributesCacheGenerator_U3CFollowAvatarU3Ec__Iterator8_Dispose_m1298(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CFollowAvatarU3Ec__Iterator8_t354_CustomAttributesCacheGenerator_U3CFollowAvatarU3Ec__Iterator8_Reset_m1299(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_PuceronMove_t359_CustomAttributesCacheGenerator_SCR_PuceronMove_MoveToWaypoint_m1330(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CMoveToWaypointU3Ec__Iterator9_t360_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CMoveToWaypointU3Ec__Iterator9_t360_CustomAttributesCacheGenerator_U3CMoveToWaypointU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1316(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CMoveToWaypointU3Ec__Iterator9_t360_CustomAttributesCacheGenerator_U3CMoveToWaypointU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1317(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CMoveToWaypointU3Ec__Iterator9_t360_CustomAttributesCacheGenerator_U3CMoveToWaypointU3Ec__Iterator9_Dispose_m1319(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CMoveToWaypointU3Ec__Iterator9_t360_CustomAttributesCacheGenerator_U3CMoveToWaypointU3Ec__Iterator9_Reset_m1320(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_LevelStartCountdown_t370_CustomAttributesCacheGenerator_SCR_LevelStartCountdown_Rescale_m1382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CRescaleU3Ec__IteratorA_t371_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CRescaleU3Ec__IteratorA_t371_CustomAttributesCacheGenerator_U3CRescaleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1372(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CRescaleU3Ec__IteratorA_t371_CustomAttributesCacheGenerator_U3CRescaleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1373(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CRescaleU3Ec__IteratorA_t371_CustomAttributesCacheGenerator_U3CRescaleU3Ec__IteratorA_Dispose_m1375(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CRescaleU3Ec__IteratorA_t371_CustomAttributesCacheGenerator_U3CRescaleU3Ec__IteratorA_Reset_m1376(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t261_il2cpp_TypeInfo_var;
void SCR_Bird_t390_CustomAttributesCacheGenerator_attaqueApresXSecondes(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t261 * tmp;
		tmp = (RangeAttribute_t261 *)il2cpp_codegen_object_new (RangeAttribute_t261_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m1025(tmp, 0.0f, 100.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_Bird_t390_CustomAttributesCacheGenerator_SCR_Bird_Landing_m1521(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CLandingU3Ec__IteratorB_t391_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CLandingU3Ec__IteratorB_t391_CustomAttributesCacheGenerator_U3CLandingU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1511(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CLandingU3Ec__IteratorB_t391_CustomAttributesCacheGenerator_U3CLandingU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1512(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CLandingU3Ec__IteratorB_t391_CustomAttributesCacheGenerator_U3CLandingU3Ec__IteratorB_Dispose_m1514(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CLandingU3Ec__IteratorB_t391_CustomAttributesCacheGenerator_U3CLandingU3Ec__IteratorB_Reset_m1515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_Spider_t384_CustomAttributesCacheGenerator_SCR_Spider_ChaseAvatar_m1559(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CChaseAvatarU3Ec__IteratorC_t396_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CChaseAvatarU3Ec__IteratorC_t396_CustomAttributesCacheGenerator_U3CChaseAvatarU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CChaseAvatarU3Ec__IteratorC_t396_CustomAttributesCacheGenerator_U3CChaseAvatarU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1547(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CChaseAvatarU3Ec__IteratorC_t396_CustomAttributesCacheGenerator_U3CChaseAvatarU3Ec__IteratorC_Dispose_m1549(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CChaseAvatarU3Ec__IteratorC_t396_CustomAttributesCacheGenerator_U3CChaseAvatarU3Ec__IteratorC_Reset_m1550(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void SCR_Web_t398_CustomAttributesCacheGenerator_SCR_Web_DecreaseSize_m1570(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var;
void U3CDecreaseSizeU3Ec__IteratorD_t399_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t263 * tmp;
		tmp = (CompilerGeneratedAttribute_t263 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t263_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1028(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDecreaseSizeU3Ec__IteratorD_t399_CustomAttributesCacheGenerator_U3CDecreaseSizeU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1561(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDecreaseSizeU3Ec__IteratorD_t399_CustomAttributesCacheGenerator_U3CDecreaseSizeU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1562(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDecreaseSizeU3Ec__IteratorD_t399_CustomAttributesCacheGenerator_U3CDecreaseSizeU3Ec__IteratorD_Dispose_m1564(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var;
void U3CDecreaseSizeU3Ec__IteratorD_t399_CustomAttributesCacheGenerator_U3CDecreaseSizeU3Ec__IteratorD_Reset_m1565(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t269 * tmp;
		tmp = (DebuggerHiddenAttribute_t269 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t269_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1045(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_Assembly_AttributeGenerators[93] = 
{
	NULL,
	g_AssemblyU2DCSharp_Assembly_CustomAttributesCacheGenerator,
	MultiKeyDictionary_3_t431_CustomAttributesCacheGenerator,
	Reporter_t295_CustomAttributesCacheGenerator_show,
	Reporter_t295_CustomAttributesCacheGenerator_Reporter_readInfo_m1123,
	U3CreadInfoU3Ec__Iterator0_t296_CustomAttributesCacheGenerator,
	U3CreadInfoU3Ec__Iterator0_t296_CustomAttributesCacheGenerator_U3CreadInfoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1086,
	U3CreadInfoU3Ec__Iterator0_t296_CustomAttributesCacheGenerator_U3CreadInfoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1087,
	U3CreadInfoU3Ec__Iterator0_t296_CustomAttributesCacheGenerator_U3CreadInfoU3Ec__Iterator0_Dispose_m1089,
	U3CreadInfoU3Ec__Iterator0_t296_CustomAttributesCacheGenerator_U3CreadInfoU3Ec__Iterator0_Reset_m1090,
	EventSystemChecker_t309_CustomAttributesCacheGenerator_EventSystemChecker_Start_m1148,
	U3CStartU3Ec__Iterator1_t310_CustomAttributesCacheGenerator,
	U3CStartU3Ec__Iterator1_t310_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1142,
	U3CStartU3Ec__Iterator1_t310_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1143,
	U3CStartU3Ec__Iterator1_t310_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_Dispose_m1145,
	U3CStartU3Ec__Iterator1_t310_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator1_Reset_m1146,
	SCR_Menu_t336_CustomAttributesCacheGenerator_SCR_Menu_AlphaLerp_m1212,
	SCR_Menu_t336_CustomAttributesCacheGenerator_SCR_Menu_AlphaLerp_m1213,
	U3CAlphaLerpU3Ec__Iterator2_t332_CustomAttributesCacheGenerator,
	U3CAlphaLerpU3Ec__Iterator2_t332_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1193,
	U3CAlphaLerpU3Ec__Iterator2_t332_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1194,
	U3CAlphaLerpU3Ec__Iterator2_t332_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator2_Dispose_m1196,
	U3CAlphaLerpU3Ec__Iterator2_t332_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator2_Reset_m1197,
	U3CAlphaLerpU3Ec__Iterator3_t333_CustomAttributesCacheGenerator,
	U3CAlphaLerpU3Ec__Iterator3_t333_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1199,
	U3CAlphaLerpU3Ec__Iterator3_t333_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1200,
	U3CAlphaLerpU3Ec__Iterator3_t333_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator3_Dispose_m1202,
	U3CAlphaLerpU3Ec__Iterator3_t333_CustomAttributesCacheGenerator_U3CAlphaLerpU3Ec__Iterator3_Reset_m1203,
	SCR_SlowAvatar_t337_CustomAttributesCacheGenerator_speedDecreaseToPercent,
	SCR_Camie_t338_CustomAttributesCacheGenerator_SCR_Camie_GoToFrontCollisionPoint_m1246,
	SCR_Camie_t338_CustomAttributesCacheGenerator_SCR_Camie_LandOnCollisionPoint_m1248,
	SCR_Camie_t338_CustomAttributesCacheGenerator_SCR_Camie_Jump_m1249,
	U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_CustomAttributesCacheGenerator,
	U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_CustomAttributesCacheGenerator_U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1225,
	U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_CustomAttributesCacheGenerator_U3CGoToFrontCollisionPointU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1226,
	U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_CustomAttributesCacheGenerator_U3CGoToFrontCollisionPointU3Ec__Iterator4_Dispose_m1228,
	U3CGoToFrontCollisionPointU3Ec__Iterator4_t339_CustomAttributesCacheGenerator_U3CGoToFrontCollisionPointU3Ec__Iterator4_Reset_m1229,
	U3CLandOnCollisionPointU3Ec__Iterator5_t340_CustomAttributesCacheGenerator,
	U3CLandOnCollisionPointU3Ec__Iterator5_t340_CustomAttributesCacheGenerator_U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1231,
	U3CLandOnCollisionPointU3Ec__Iterator5_t340_CustomAttributesCacheGenerator_U3CLandOnCollisionPointU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1232,
	U3CLandOnCollisionPointU3Ec__Iterator5_t340_CustomAttributesCacheGenerator_U3CLandOnCollisionPointU3Ec__Iterator5_Dispose_m1234,
	U3CLandOnCollisionPointU3Ec__Iterator5_t340_CustomAttributesCacheGenerator_U3CLandOnCollisionPointU3Ec__Iterator5_Reset_m1235,
	U3CJumpU3Ec__Iterator6_t341_CustomAttributesCacheGenerator,
	U3CJumpU3Ec__Iterator6_t341_CustomAttributesCacheGenerator_U3CJumpU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1237,
	U3CJumpU3Ec__Iterator6_t341_CustomAttributesCacheGenerator_U3CJumpU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1238,
	U3CJumpU3Ec__Iterator6_t341_CustomAttributesCacheGenerator_U3CJumpU3Ec__Iterator6_Dispose_m1240,
	U3CJumpU3Ec__Iterator6_t341_CustomAttributesCacheGenerator_U3CJumpU3Ec__Iterator6_Reset_m1241,
	SCR_FollowAvatar_t352_CustomAttributesCacheGenerator_distanceDuFrame,
	SCR_FollowAvatar_t352_CustomAttributesCacheGenerator_plusHautQueCamie,
	SCR_FollowAvatar_t352_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0,
	SCR_FollowAvatar_t352_CustomAttributesCacheGenerator_SCR_FollowAvatar_ZoomInOn_m1304,
	SCR_FollowAvatar_t352_CustomAttributesCacheGenerator_SCR_FollowAvatar_FollowAvatar_m1305,
	U3CZoomInOnU3Ec__Iterator7_t353_CustomAttributesCacheGenerator,
	U3CZoomInOnU3Ec__Iterator7_t353_CustomAttributesCacheGenerator_U3CZoomInOnU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1289,
	U3CZoomInOnU3Ec__Iterator7_t353_CustomAttributesCacheGenerator_U3CZoomInOnU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1290,
	U3CZoomInOnU3Ec__Iterator7_t353_CustomAttributesCacheGenerator_U3CZoomInOnU3Ec__Iterator7_Dispose_m1292,
	U3CZoomInOnU3Ec__Iterator7_t353_CustomAttributesCacheGenerator_U3CZoomInOnU3Ec__Iterator7_Reset_m1293,
	U3CFollowAvatarU3Ec__Iterator8_t354_CustomAttributesCacheGenerator,
	U3CFollowAvatarU3Ec__Iterator8_t354_CustomAttributesCacheGenerator_U3CFollowAvatarU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1295,
	U3CFollowAvatarU3Ec__Iterator8_t354_CustomAttributesCacheGenerator_U3CFollowAvatarU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1296,
	U3CFollowAvatarU3Ec__Iterator8_t354_CustomAttributesCacheGenerator_U3CFollowAvatarU3Ec__Iterator8_Dispose_m1298,
	U3CFollowAvatarU3Ec__Iterator8_t354_CustomAttributesCacheGenerator_U3CFollowAvatarU3Ec__Iterator8_Reset_m1299,
	SCR_PuceronMove_t359_CustomAttributesCacheGenerator_SCR_PuceronMove_MoveToWaypoint_m1330,
	U3CMoveToWaypointU3Ec__Iterator9_t360_CustomAttributesCacheGenerator,
	U3CMoveToWaypointU3Ec__Iterator9_t360_CustomAttributesCacheGenerator_U3CMoveToWaypointU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1316,
	U3CMoveToWaypointU3Ec__Iterator9_t360_CustomAttributesCacheGenerator_U3CMoveToWaypointU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1317,
	U3CMoveToWaypointU3Ec__Iterator9_t360_CustomAttributesCacheGenerator_U3CMoveToWaypointU3Ec__Iterator9_Dispose_m1319,
	U3CMoveToWaypointU3Ec__Iterator9_t360_CustomAttributesCacheGenerator_U3CMoveToWaypointU3Ec__Iterator9_Reset_m1320,
	SCR_LevelStartCountdown_t370_CustomAttributesCacheGenerator_SCR_LevelStartCountdown_Rescale_m1382,
	U3CRescaleU3Ec__IteratorA_t371_CustomAttributesCacheGenerator,
	U3CRescaleU3Ec__IteratorA_t371_CustomAttributesCacheGenerator_U3CRescaleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1372,
	U3CRescaleU3Ec__IteratorA_t371_CustomAttributesCacheGenerator_U3CRescaleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1373,
	U3CRescaleU3Ec__IteratorA_t371_CustomAttributesCacheGenerator_U3CRescaleU3Ec__IteratorA_Dispose_m1375,
	U3CRescaleU3Ec__IteratorA_t371_CustomAttributesCacheGenerator_U3CRescaleU3Ec__IteratorA_Reset_m1376,
	SCR_Bird_t390_CustomAttributesCacheGenerator_attaqueApresXSecondes,
	SCR_Bird_t390_CustomAttributesCacheGenerator_SCR_Bird_Landing_m1521,
	U3CLandingU3Ec__IteratorB_t391_CustomAttributesCacheGenerator,
	U3CLandingU3Ec__IteratorB_t391_CustomAttributesCacheGenerator_U3CLandingU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1511,
	U3CLandingU3Ec__IteratorB_t391_CustomAttributesCacheGenerator_U3CLandingU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1512,
	U3CLandingU3Ec__IteratorB_t391_CustomAttributesCacheGenerator_U3CLandingU3Ec__IteratorB_Dispose_m1514,
	U3CLandingU3Ec__IteratorB_t391_CustomAttributesCacheGenerator_U3CLandingU3Ec__IteratorB_Reset_m1515,
	SCR_Spider_t384_CustomAttributesCacheGenerator_SCR_Spider_ChaseAvatar_m1559,
	U3CChaseAvatarU3Ec__IteratorC_t396_CustomAttributesCacheGenerator,
	U3CChaseAvatarU3Ec__IteratorC_t396_CustomAttributesCacheGenerator_U3CChaseAvatarU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1546,
	U3CChaseAvatarU3Ec__IteratorC_t396_CustomAttributesCacheGenerator_U3CChaseAvatarU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1547,
	U3CChaseAvatarU3Ec__IteratorC_t396_CustomAttributesCacheGenerator_U3CChaseAvatarU3Ec__IteratorC_Dispose_m1549,
	U3CChaseAvatarU3Ec__IteratorC_t396_CustomAttributesCacheGenerator_U3CChaseAvatarU3Ec__IteratorC_Reset_m1550,
	SCR_Web_t398_CustomAttributesCacheGenerator_SCR_Web_DecreaseSize_m1570,
	U3CDecreaseSizeU3Ec__IteratorD_t399_CustomAttributesCacheGenerator,
	U3CDecreaseSizeU3Ec__IteratorD_t399_CustomAttributesCacheGenerator_U3CDecreaseSizeU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1561,
	U3CDecreaseSizeU3Ec__IteratorD_t399_CustomAttributesCacheGenerator_U3CDecreaseSizeU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1562,
	U3CDecreaseSizeU3Ec__IteratorD_t399_CustomAttributesCacheGenerator_U3CDecreaseSizeU3Ec__IteratorD_Dispose_m1564,
	U3CDecreaseSizeU3Ec__IteratorD_t399_CustomAttributesCacheGenerator_U3CDecreaseSizeU3Ec__IteratorD_Reset_m1565,
};
