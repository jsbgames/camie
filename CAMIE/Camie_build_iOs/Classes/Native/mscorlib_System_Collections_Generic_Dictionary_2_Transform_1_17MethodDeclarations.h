﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Byte,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>>
struct Transform_1_t2994;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Byte,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m15991_gshared (Transform_1_t2994 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m15991(__this, ___object, ___method, method) (( void (*) (Transform_1_t2994 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m15991_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Byte,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t2984  Transform_1_Invoke_m15992_gshared (Transform_1_t2994 * __this, int32_t ___key, uint8_t ___value, const MethodInfo* method);
#define Transform_1_Invoke_m15992(__this, ___key, ___value, method) (( KeyValuePair_2_t2984  (*) (Transform_1_t2994 *, int32_t, uint8_t, const MethodInfo*))Transform_1_Invoke_m15992_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Byte,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m15993_gshared (Transform_1_t2994 * __this, int32_t ___key, uint8_t ___value, AsyncCallback_t547 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m15993(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2994 *, int32_t, uint8_t, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m15993_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Byte,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Byte>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t2984  Transform_1_EndInvoke_m15994_gshared (Transform_1_t2994 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m15994(__this, ___result, method) (( KeyValuePair_2_t2984  (*) (Transform_1_t2994 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m15994_gshared)(__this, ___result, method)
