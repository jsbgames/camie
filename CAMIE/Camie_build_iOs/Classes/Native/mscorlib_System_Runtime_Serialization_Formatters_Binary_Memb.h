﻿#pragma once
#include <stdint.h>
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t2030;
// System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_ClrT.h"
// System.Runtime.Serialization.Formatters.Binary.MemberTypeMetadata
struct  MemberTypeMetadata_t2038  : public ClrTypeMetadata_t2036
{
	// System.Reflection.MemberInfo[] System.Runtime.Serialization.Formatters.Binary.MemberTypeMetadata::members
	MemberInfoU5BU5D_t2030* ___members_3;
};
