﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t2847;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Object>
struct  Comparer_1_t2847  : public Object_t
{
};
struct Comparer_1_t2847_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::_default
	Comparer_1_t2847 * ____default_0;
};
