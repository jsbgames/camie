﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t86;
// UnityEngine.UI.MaskableGraphic
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.UI.RawImage
struct  RawImage_t564  : public MaskableGraphic_t537
{
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t86 * ___m_Texture_23;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t304  ___m_UVRect_24;
};
