﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct  GetDelegate_t937  : public MulticastDelegate_t549
{
};
