﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.IntPtr>
struct InternalEnumerator_1_t3222;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19379_gshared (InternalEnumerator_1_t3222 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19379(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3222 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19379_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19380_gshared (InternalEnumerator_1_t3222 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19380(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3222 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19380_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19381_gshared (InternalEnumerator_1_t3222 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19381(__this, method) (( void (*) (InternalEnumerator_1_t3222 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19381_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19382_gshared (InternalEnumerator_1_t3222 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19382(__this, method) (( bool (*) (InternalEnumerator_1_t3222 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19382_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern "C" IntPtr_t InternalEnumerator_1_get_Current_m19383_gshared (InternalEnumerator_1_t3222 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19383(__this, method) (( IntPtr_t (*) (InternalEnumerator_1_t3222 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19383_gshared)(__this, method)
