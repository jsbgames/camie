﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Replacements.TypeBuilder
struct TypeBuilder_t1513;
// System.Type
struct Type_t;
// System.Object
struct Object_t;

// System.Type Replacements.TypeBuilder::CreateType(System.Object)
extern "C" Type_t * TypeBuilder_CreateType_m6673 (Object_t * __this /* static, unused */, Object_t * _____this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
