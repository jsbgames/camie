﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.MatchDesc
struct MatchDesc_t915;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Networking.Match.MatchDesc>
struct  Predicate_1_t3283  : public MulticastDelegate_t549
{
};
