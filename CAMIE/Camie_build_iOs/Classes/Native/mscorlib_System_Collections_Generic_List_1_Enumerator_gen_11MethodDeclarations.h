﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
struct Enumerator_t2935;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t417;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15261_gshared (Enumerator_t2935 * __this, List_1_t417 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15261(__this, ___l, method) (( void (*) (Enumerator_t2935 *, List_1_t417 *, const MethodInfo*))Enumerator__ctor_m15261_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15262_gshared (Enumerator_t2935 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15262(__this, method) (( Object_t * (*) (Enumerator_t2935 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15262_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void Enumerator_Dispose_m15263_gshared (Enumerator_t2935 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15263(__this, method) (( void (*) (Enumerator_t2935 *, const MethodInfo*))Enumerator_Dispose_m15263_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m15264_gshared (Enumerator_t2935 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15264(__this, method) (( void (*) (Enumerator_t2935 *, const MethodInfo*))Enumerator_VerifyState_m15264_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15265_gshared (Enumerator_t2935 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15265(__this, method) (( bool (*) (Enumerator_t2935 *, const MethodInfo*))Enumerator_MoveNext_m15265_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t486  Enumerator_get_Current_m15266_gshared (Enumerator_t2935 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15266(__this, method) (( RaycastResult_t486  (*) (Enumerator_t2935 *, const MethodInfo*))Enumerator_get_Current_m15266_gshared)(__this, method)
