﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t247;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEngine.AudioSource,System.Collections.DictionaryEntry>
struct  Transform_1_t3009  : public MulticastDelegate_t549
{
};
