﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAParameters
struct RSAParameters_t1451;
struct RSAParameters_t1451_marshaled;

void RSAParameters_t1451_marshal(const RSAParameters_t1451& unmarshaled, RSAParameters_t1451_marshaled& marshaled);
void RSAParameters_t1451_marshal_back(const RSAParameters_t1451_marshaled& marshaled, RSAParameters_t1451& unmarshaled);
void RSAParameters_t1451_marshal_cleanup(RSAParameters_t1451_marshaled& marshaled);
