﻿#pragma once
#include <stdint.h>
// SCR_MainMenu
struct SCR_MainMenu_t372;
// UnityEngine.UI.Text
struct Text_t316;
// UnityEngine.UI.Image
struct Image_t48;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t192;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t373;
// SCR_Menu
#include "AssemblyU2DCSharp_SCR_Menu.h"
// SCR_MainMenu
struct  SCR_MainMenu_t372  : public SCR_Menu_t336
{
	// System.Boolean SCR_MainMenu::timerOn
	bool ___timerOn_7;
	// System.Single SCR_MainMenu::time
	float ___time_8;
	// System.Single SCR_MainMenu::timeBeforePressToContinue
	float ___timeBeforePressToContinue_9;
	// UnityEngine.UI.Text SCR_MainMenu::pressToContinue
	Text_t316 * ___pressToContinue_10;
	// System.Boolean SCR_MainMenu::selectedIsLocked
	bool ___selectedIsLocked_11;
	// UnityEngine.UI.Image SCR_MainMenu::selectedLevelImg
	Image_t48 * ___selectedLevelImg_12;
	// System.Int32 SCR_MainMenu::selectedLevel
	int32_t ___selectedLevel_13;
	// System.Boolean SCR_MainMenu::hasSelectedALevel
	bool ___hasSelectedALevel_14;
	// UnityEngine.GameObject[] SCR_MainMenu::levelSets
	GameObjectU5BU5D_t192* ___levelSets_15;
	// System.Int32 SCR_MainMenu::currentLevelSet
	int32_t ___currentLevelSet_16;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> SCR_MainMenu::levelsLocked
	Dictionary_2_t373 * ___levelsLocked_17;
};
struct SCR_MainMenu_t372_StaticFields{
	// SCR_MainMenu SCR_MainMenu::instance
	SCR_MainMenu_t372 * ___instance_5;
	// System.Boolean SCR_MainMenu::isFromSplashScreen
	bool ___isFromSplashScreen_6;
};
