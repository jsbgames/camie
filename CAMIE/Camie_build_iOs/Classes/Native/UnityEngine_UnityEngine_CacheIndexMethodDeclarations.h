﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CacheIndex
struct CacheIndex_t853;
struct CacheIndex_t853_marshaled;

void CacheIndex_t853_marshal(const CacheIndex_t853& unmarshaled, CacheIndex_t853_marshaled& marshaled);
void CacheIndex_t853_marshal_back(const CacheIndex_t853_marshaled& marshaled, CacheIndex_t853& unmarshaled);
void CacheIndex_t853_marshal_cleanup(CacheIndex_t853_marshaled& marshaled);
