﻿#pragma once
#include <stdint.h>
// SCR_Camie
struct SCR_Camie_t338;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_ResetIdle
struct  SCR_ResetIdle_t350  : public MonoBehaviour_t3
{
	// SCR_Camie SCR_ResetIdle::camie
	SCR_Camie_t338 * ___camie_2;
};
