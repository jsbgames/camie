﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.ContextLevelActivator
struct ContextLevelActivator_t1953;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t1949;

// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
extern "C" void ContextLevelActivator__ctor_m10456 (ContextLevelActivator_t1953 * __this, Object_t * ___next, const MethodInfo* method) IL2CPP_METHOD_ATTR;
