﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "System_Core_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t1494_il2cpp_TypeInfo;
// <Module>
#include "System_Core_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t1494_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CModuleU3E_t1494_0_0_0;
extern const Il2CppType U3CModuleU3E_t1494_1_0_0;
struct U3CModuleU3E_t1494;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t1494_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t1494_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t1494_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t1494_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t1494_0_0_0/* byval_arg */
	, &U3CModuleU3E_t1494_1_0_0/* this_arg */
	, &U3CModuleU3E_t1494_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t1494)/* instance_size */
	, sizeof (U3CModuleU3E_t1494)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// Metadata Definition System.Runtime.CompilerServices.ExtensionAttribute
extern TypeInfo ExtensionAttribute_t1111_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern const Il2CppType Void_t272_0_0_0;
extern void* RuntimeInvoker_Void_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern const MethodInfo ExtensionAttribute__ctor_m5256_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExtensionAttribute__ctor_m5256/* method */
	, &ExtensionAttribute_t1111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExtensionAttribute_t1111_MethodInfos[] =
{
	&ExtensionAttribute__ctor_m5256_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5384_MethodInfo;
extern const MethodInfo Object_Finalize_m1048_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5253_MethodInfo;
extern const MethodInfo Object_ToString_m1075_MethodInfo;
static const Il2CppMethodReference ExtensionAttribute_t1111_VTable[] =
{
	&Attribute_Equals_m5384_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Attribute_GetHashCode_m5253_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool ExtensionAttribute_t1111_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t1145_0_0_0;
static Il2CppInterfaceOffsetPair ExtensionAttribute_t1111_InterfacesOffsets[] = 
{
	{ &_Attribute_t1145_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType ExtensionAttribute_t1111_0_0_0;
extern const Il2CppType ExtensionAttribute_t1111_1_0_0;
extern const Il2CppType Attribute_t805_0_0_0;
struct ExtensionAttribute_t1111;
const Il2CppTypeDefinitionMetadata ExtensionAttribute_t1111_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExtensionAttribute_t1111_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t805_0_0_0/* parent */
	, ExtensionAttribute_t1111_VTable/* vtableMethods */
	, ExtensionAttribute_t1111_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExtensionAttribute_t1111_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtensionAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, ExtensionAttribute_t1111_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExtensionAttribute_t1111_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2/* custom_attributes_cache */
	, &ExtensionAttribute_t1111_0_0_0/* byval_arg */
	, &ExtensionAttribute_t1111_1_0_0/* this_arg */
	, &ExtensionAttribute_t1111_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtensionAttribute_t1111)/* instance_size */
	, sizeof (ExtensionAttribute_t1111)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Linq.Check
#include "System_Core_System_Linq_Check.h"
// Metadata Definition System.Linq.Check
extern TypeInfo Check_t1495_il2cpp_TypeInfo;
// System.Linq.Check
#include "System_Core_System_Linq_CheckMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Check_t1495_Check_SourceAndPredicate_m6653_ParameterInfos[] = 
{
	{"source", 0, 134217729, 0, &Object_t_0_0_0},
	{"predicate", 1, 134217730, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t272_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
extern const MethodInfo Check_SourceAndPredicate_m6653_MethodInfo = 
{
	"SourceAndPredicate"/* name */
	, (methodPointerType)&Check_SourceAndPredicate_m6653/* method */
	, &Check_t1495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, RuntimeInvoker_Void_t272_Object_t_Object_t/* invoker_method */
	, Check_t1495_Check_SourceAndPredicate_m6653_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Check_t1495_MethodInfos[] =
{
	&Check_SourceAndPredicate_m6653_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m1073_MethodInfo;
extern const MethodInfo Object_GetHashCode_m1074_MethodInfo;
static const Il2CppMethodReference Check_t1495_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Check_t1495_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Check_t1495_0_0_0;
extern const Il2CppType Check_t1495_1_0_0;
struct Check_t1495;
const Il2CppTypeDefinitionMetadata Check_t1495_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Check_t1495_VTable/* vtableMethods */
	, Check_t1495_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Check_t1495_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Check"/* name */
	, "System.Linq"/* namespaze */
	, Check_t1495_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Check_t1495_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Check_t1495_0_0_0/* byval_arg */
	, &Check_t1495_1_0_0/* this_arg */
	, &Check_t1495_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Check_t1495)/* instance_size */
	, sizeof (Check_t1495)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo;
extern const Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_Il2CppGenericContainer;
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_Il2CppGenericParametersArray[1] = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo, 1, 0, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_Il2CppGenericParametersArray };
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::.ctor()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m6656_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_gp_0_0_0_0;
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6657_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TSource>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 7/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6658_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 8/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_t217_0_0_0;
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6659_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t217_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 9/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t1499_0_0_0;
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6660_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TSource>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1499_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 10/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t273_0_0_0;
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::MoveNext()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m6661_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t273_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::Dispose()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6662_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 11/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_MethodInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m6656_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6657_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6658_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6659_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6660_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m6661_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6662_MethodInfo,
	NULL
};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6657_MethodInfo;
static const PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TSource>.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6657_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6658_MethodInfo;
static const PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6658_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_PropertyInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m6661_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6662_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6659_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6660_MethodInfo;
static const Il2CppMethodReference U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6658_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m6661_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6662_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6659_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6660_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6657_MethodInfo,
};
static bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t233_0_0_0;
extern const Il2CppType IEnumerable_t438_0_0_0;
extern const Il2CppType IEnumerable_1_t1500_0_0_0;
static const Il2CppType* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_InterfacesTypeInfos[] = 
{
	&IEnumerator_t217_0_0_0,
	&IDisposable_t233_0_0_0,
	&IEnumerable_t438_0_0_0,
	&IEnumerable_1_t1500_0_0_0,
	&IEnumerator_1_t1499_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_InterfacesOffsets[] = 
{
	{ &IEnumerator_t217_0_0_0, 4},
	{ &IDisposable_t233_0_0_0, 6},
	{ &IEnumerable_t438_0_0_0, 7},
	{ &IEnumerable_1_t1500_0_0_0, 8},
	{ &IEnumerator_1_t1499_0_0_0, 9},
};
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6667_GenericMethod;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1501_0_0_0;
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m6668_GenericMethod;
extern const Il2CppGenericMethod Func_2_Invoke_m6669_GenericMethod;
static Il2CppRGCTXDefinition U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6667_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1501_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m6668_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t1500_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerator_1_t1499_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2_Invoke_m6669_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_1_0_0;
extern TypeInfo Enumerable_t697_il2cpp_TypeInfo;
extern const Il2CppType Enumerable_t697_0_0_0;
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496;
const Il2CppTypeDefinitionMetadata U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_DefinitionMetadata = 
{
	&Enumerable_t697_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_VTable/* vtableMethods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_RGCTXData/* rgctxDefinition */
	, 0/* fieldStart */

};
TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateWhereIterator>c__Iterator1D`1"/* name */
	, ""/* namespaze */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_MethodInfos/* methods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 6/* custom_attributes_cache */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_0_0_0/* byval_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_1_0_0/* this_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
// Metadata Definition System.Linq.Enumerable
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
extern const Il2CppType IEnumerable_1_t1502_0_0_0;
extern const Il2CppType IEnumerable_1_t1502_0_0_0;
extern const Il2CppType Func_2_t1503_0_0_0;
extern const Il2CppType Func_2_t1503_0_0_0;
static const ParameterInfo Enumerable_t697_Enumerable_Where_m6654_ParameterInfos[] = 
{
	{"source", 0, 134217731, 0, &IEnumerable_1_t1502_0_0_0},
	{"predicate", 1, 134217732, 0, &Func_2_t1503_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_Where_m6654_Il2CppGenericContainer;
extern TypeInfo Enumerable_Where_m6654_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Where_m6654_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Where_m6654_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Where_m6654_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Where_m6654_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Where_m6654_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Where_m6654_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Where_m6654_MethodInfo, 1, 1, Enumerable_Where_m6654_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod Enumerable_CreateWhereIterator_TisTSource_t1504_m6670_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Where_m6654_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateWhereIterator_TisTSource_t1504_m6670_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern const MethodInfo Enumerable_Where_m6654_MethodInfo = 
{
	"Where"/* name */
	, NULL/* method */
	, &Enumerable_t697_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1502_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t697_Enumerable_Where_m6654_ParameterInfos/* parameters */
	, 4/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, Enumerable_Where_m6654_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Where_m6654_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1505_0_0_0;
extern const Il2CppType IEnumerable_1_t1505_0_0_0;
extern const Il2CppType Func_2_t1506_0_0_0;
extern const Il2CppType Func_2_t1506_0_0_0;
static const ParameterInfo Enumerable_t697_Enumerable_CreateWhereIterator_m6655_ParameterInfos[] = 
{
	{"source", 0, 134217733, 0, &IEnumerable_1_t1505_0_0_0},
	{"predicate", 1, 134217734, 0, &Func_2_t1506_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_CreateWhereIterator_m6655_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateWhereIterator_m6655_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_CreateWhereIterator_m6655_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_CreateWhereIterator_m6655_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_CreateWhereIterator_m6655_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_CreateWhereIterator_m6655_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_CreateWhereIterator_m6655_MethodInfo;
extern const Il2CppGenericContainer Enumerable_CreateWhereIterator_m6655_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_CreateWhereIterator_m6655_MethodInfo, 1, 1, Enumerable_CreateWhereIterator_m6655_Il2CppGenericParametersArray };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1508_0_0_0;
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m6671_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateWhereIterator_m6655_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1508_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m6671_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::CreateWhereIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern const MethodInfo Enumerable_CreateWhereIterator_m6655_MethodInfo = 
{
	"CreateWhereIterator"/* name */
	, NULL/* method */
	, &Enumerable_t697_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1505_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t697_Enumerable_CreateWhereIterator_m6655_ParameterInfos/* parameters */
	, 5/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, Enumerable_CreateWhereIterator_m6655_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateWhereIterator_m6655_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* Enumerable_t697_MethodInfos[] =
{
	&Enumerable_Where_m6654_MethodInfo,
	&Enumerable_CreateWhereIterator_m6655_MethodInfo,
	NULL
};
static const Il2CppType* Enumerable_t697_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1496_0_0_0,
};
static const Il2CppMethodReference Enumerable_t697_VTable[] =
{
	&Object_Equals_m1073_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&Object_GetHashCode_m1074_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
};
static bool Enumerable_t697_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Enumerable_t697_1_0_0;
struct Enumerable_t697;
const Il2CppTypeDefinitionMetadata Enumerable_t697_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Enumerable_t697_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerable_t697_VTable/* vtableMethods */
	, Enumerable_t697_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Enumerable_t697_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerable"/* name */
	, "System.Linq"/* namespaze */
	, Enumerable_t697_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Enumerable_t697_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3/* custom_attributes_cache */
	, &Enumerable_t697_0_0_0/* byval_arg */
	, &Enumerable_t697_1_0_0/* this_arg */
	, &Enumerable_t697_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerable_t697)/* instance_size */
	, sizeof (Enumerable_t697)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Func`2
extern TypeInfo Func_2_t1497_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Func_2_t1497_Il2CppGenericContainer;
extern TypeInfo Func_2_t1497_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Func_2_t1497_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Func_2_t1497_Il2CppGenericContainer, NULL, "T", 0, 0 };
extern TypeInfo Func_2_t1497_gp_TResult_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Func_2_t1497_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull = { &Func_2_t1497_Il2CppGenericContainer, NULL, "TResult", 1, 0 };
static const Il2CppGenericParameter* Func_2_t1497_Il2CppGenericParametersArray[2] = 
{
	&Func_2_t1497_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
	&Func_2_t1497_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Func_2_t1497_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Func_2_t1497_il2cpp_TypeInfo, 2, 0, Func_2_t1497_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Func_2_t1497_Func_2__ctor_m6663_ParameterInfos[] = 
{
	{"object", 0, 134217735, 0, &Object_t_0_0_0},
	{"method", 1, 134217736, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Func`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Func_2__ctor_m6663_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Func_2_t1497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t272_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1497_Func_2__ctor_m6663_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Func_2_t1497_gp_0_0_0_0;
extern const Il2CppType Func_2_t1497_gp_0_0_0_0;
static const ParameterInfo Func_2_t1497_Func_2_Invoke_m6664_ParameterInfos[] = 
{
	{"arg1", 0, 134217737, 0, &Func_2_t1497_gp_0_0_0_0},
};
extern const Il2CppType Func_2_t1497_gp_1_0_0_0;
// TResult System.Func`2::Invoke(T)
extern const MethodInfo Func_2_Invoke_m6664_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Func_2_t1497_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t1497_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1497_Func_2_Invoke_m6664_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Func_2_t1497_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType AsyncCallback_t547_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Func_2_t1497_Func_2_BeginInvoke_m6665_ParameterInfos[] = 
{
	{"arg1", 0, 134217738, 0, &Func_2_t1497_gp_0_0_0_0},
	{"callback", 1, 134217739, 0, &AsyncCallback_t547_0_0_0},
	{"object", 2, 134217740, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t546_0_0_0;
// System.IAsyncResult System.Func`2::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Func_2_BeginInvoke_m6665_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Func_2_t1497_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t546_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1497_Func_2_BeginInvoke_m6665_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t546_0_0_0;
static const ParameterInfo Func_2_t1497_Func_2_EndInvoke_m6666_ParameterInfos[] = 
{
	{"result", 0, 134217741, 0, &IAsyncResult_t546_0_0_0},
};
// TResult System.Func`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo Func_2_EndInvoke_m6666_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Func_2_t1497_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t1497_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1497_Func_2_EndInvoke_m6666_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Func_2_t1497_MethodInfos[] =
{
	&Func_2__ctor_m6663_MethodInfo,
	&Func_2_Invoke_m6664_MethodInfo,
	&Func_2_BeginInvoke_m6665_MethodInfo,
	&Func_2_EndInvoke_m6666_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m3586_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m3587_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m3588_MethodInfo;
extern const MethodInfo Delegate_Clone_m3589_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m3590_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m3591_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m3592_MethodInfo;
extern const MethodInfo Func_2_Invoke_m6664_MethodInfo;
extern const MethodInfo Func_2_BeginInvoke_m6665_MethodInfo;
extern const MethodInfo Func_2_EndInvoke_m6666_MethodInfo;
static const Il2CppMethodReference Func_2_t1497_VTable[] =
{
	&MulticastDelegate_Equals_m3586_MethodInfo,
	&Object_Finalize_m1048_MethodInfo,
	&MulticastDelegate_GetHashCode_m3587_MethodInfo,
	&Object_ToString_m1075_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&Delegate_Clone_m3589_MethodInfo,
	&MulticastDelegate_GetObjectData_m3588_MethodInfo,
	&MulticastDelegate_GetInvocationList_m3590_MethodInfo,
	&MulticastDelegate_CombineImpl_m3591_MethodInfo,
	&MulticastDelegate_RemoveImpl_m3592_MethodInfo,
	&Func_2_Invoke_m6664_MethodInfo,
	&Func_2_BeginInvoke_m6665_MethodInfo,
	&Func_2_EndInvoke_m6666_MethodInfo,
};
static bool Func_2_t1497_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t733_0_0_0;
extern const Il2CppType ISerializable_t439_0_0_0;
static Il2CppInterfaceOffsetPair Func_2_t1497_InterfacesOffsets[] = 
{
	{ &ICloneable_t733_0_0_0, 4},
	{ &ISerializable_t439_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Func_2_t1497_0_0_0;
extern const Il2CppType Func_2_t1497_1_0_0;
extern const Il2CppType MulticastDelegate_t549_0_0_0;
struct Func_2_t1497;
const Il2CppTypeDefinitionMetadata Func_2_t1497_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Func_2_t1497_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t549_0_0_0/* parent */
	, Func_2_t1497_VTable/* vtableMethods */
	, Func_2_t1497_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Func_2_t1497_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Func`2"/* name */
	, "System"/* namespaze */
	, Func_2_t1497_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Func_2_t1497_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Func_2_t1497_0_0_0/* byval_arg */
	, &Func_2_t1497_1_0_0/* this_arg */
	, &Func_2_t1497_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Func_2_t1497_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
