﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.UI.Toggle>
struct Predicate_1_t594;
// System.Object
struct Object_t;
// UnityEngine.UI.Toggle
struct Toggle_t312;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<UnityEngine.UI.Toggle>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#define Predicate_1__ctor_m3417(__this, ___object, ___method, method) (( void (*) (Predicate_1_t594 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m14055_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.Toggle>::Invoke(T)
#define Predicate_1_Invoke_m18646(__this, ___obj, method) (( bool (*) (Predicate_1_t594 *, Toggle_t312 *, const MethodInfo*))Predicate_1_Invoke_m14056_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UI.Toggle>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m18647(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t594 *, Toggle_t312 *, AsyncCallback_t547 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m14057_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.Toggle>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m18648(__this, ___result, method) (( bool (*) (Predicate_1_t594 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m14058_gshared)(__this, ___result, method)
