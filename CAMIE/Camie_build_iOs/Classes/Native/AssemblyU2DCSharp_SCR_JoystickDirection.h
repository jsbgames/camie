﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SCR_Joystick/Direction
#include "AssemblyU2DCSharp_SCR_Joystick_Direction.h"
// SCR_JoystickDirection
struct  SCR_JoystickDirection_t366  : public MonoBehaviour_t3
{
	// SCR_Joystick/Direction SCR_JoystickDirection::myDirection
	int32_t ___myDirection_2;
};
