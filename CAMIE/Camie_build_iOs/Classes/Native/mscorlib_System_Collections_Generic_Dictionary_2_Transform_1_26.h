﻿#pragma once
#include <stdint.h>
// UnityEngine.Font
struct Font_t517;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t667;
// System.IAsyncResult
struct IAsyncResult_t546;
// System.AsyncCallback
struct AsyncCallback_t547;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,System.Collections.DictionaryEntry>
struct  Transform_1_t3087  : public MulticastDelegate_t549
{
};
