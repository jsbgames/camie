﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t777;
// UnityEngine.Object
struct Object_t164;
struct Object_t164_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t230;

// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m3646 (AssetBundleRequest_t777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t164 * AssetBundleRequest_get_asset_m3647 (AssetBundleRequest_t777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t230* AssetBundleRequest_get_allAssets_m3648 (AssetBundleRequest_t777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
