﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>
struct  KeyValuePair_2_t2999 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::value
	bool ___value_1;
};
